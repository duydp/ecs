﻿namespace Company.Interface.KDT.SXXK
{
    partial class ThanhKhoanSendForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThanhKhoanSendForm));
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MaHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TriGiaHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoCTTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NoiPhatHanh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HinhThucTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TriGiaTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grvChungTuThanhToan = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SoHopDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayHopDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TriGiaHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.donViHaiQuanNewControl1 = new Company.Interface.Controls.DonViHaiQuanNewControl();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedBack1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedBack");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdDelete = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedBack = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedBack");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdChungtu = new Janus.Windows.UI.CommandBars.UICommand("cmdChungtu");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TabBKToKhaiXuat = new Janus.Windows.UI.Tab.UITab();
            this.TabBKToKhaiNhap = new Janus.Windows.UI.Tab.UITabPage();
            this.grvBKNhap = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SoToKhai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LoaiHinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NamDK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaHaiQuan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayHoanThanh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.grvBKXuat = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TabBKChungTuThanhToan = new Janus.Windows.UI.Tab.UITabPage();
            this.TabNPLXuatTheoHDGC = new Janus.Windows.UI.Tab.UITabPage();
            this.TabBKNPLCungUngTheoHD = new Janus.Windows.UI.Tab.UITabPage();
            this.TabBKNPLChuaThanhKhoan = new Janus.Windows.UI.Tab.UITabPage();
            this.grvNPLChuaThanhLy = new DevExpress.XtraGrid.GridControl();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TabBKNPLXinNopThue = new Janus.Windows.UI.Tab.UITabPage();
            this.grvBKNPLNopThue = new DevExpress.XtraGrid.GridControl();
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TabBKNPLTaiXuat = new Janus.Windows.UI.Tab.UITabPage();
            this.grvNPLTaiXuat = new DevExpress.XtraGrid.GridControl();
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TabXuatNhapTon = new Janus.Windows.UI.Tab.UITabPage();
            this.grvXuatNhapTon = new DevExpress.XtraGrid.GridControl();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MaNPL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenNPL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TonDau = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LuongSanXuat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TaiXuat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LuongNopThue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LuongXuatTheoHDGC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LuongTonCuoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DonGiaTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvChungTuThanhToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabBKToKhaiXuat)).BeginInit();
            this.TabBKToKhaiXuat.SuspendLayout();
            this.TabBKToKhaiNhap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvBKNhap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvBKXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            this.TabBKChungTuThanhToan.SuspendLayout();
            this.TabBKNPLChuaThanhKhoan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvNPLChuaThanhLy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            this.TabBKNPLXinNopThue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvBKNPLNopThue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            this.TabBKNPLTaiXuat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvNPLTaiXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            this.TabXuatNhapTon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvXuatNhapTon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.TabBKToKhaiXuat);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(1118, 568);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MaHang,
            this.TriGiaHang,
            this.SoCTTT,
            this.NgayCT,
            this.NoiPhatHanh,
            this.HinhThucTT,
            this.TriGiaTT,
            this.GhiChu});
            this.gridView2.GridControl = this.grvChungTuThanhToan;
            this.gridView2.Name = "gridView2";
            // 
            // MaHang
            // 
            this.MaHang.AppearanceCell.Options.UseTextOptions = true;
            this.MaHang.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MaHang.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MaHang.AppearanceHeader.Options.UseTextOptions = true;
            this.MaHang.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MaHang.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MaHang.Caption = "Mã hàng XK";
            this.MaHang.FieldName = "MaHang";
            this.MaHang.Name = "MaHang";
            this.MaHang.Visible = true;
            this.MaHang.VisibleIndex = 0;
            // 
            // TriGiaHang
            // 
            this.TriGiaHang.AppearanceCell.Options.UseTextOptions = true;
            this.TriGiaHang.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TriGiaHang.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaHang.AppearanceHeader.Options.UseTextOptions = true;
            this.TriGiaHang.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TriGiaHang.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaHang.Caption = "Trị giá hàng";
            this.TriGiaHang.FieldName = "TriGiaHang";
            this.TriGiaHang.Name = "TriGiaHang";
            this.TriGiaHang.Visible = true;
            this.TriGiaHang.VisibleIndex = 1;
            // 
            // SoCTTT
            // 
            this.SoCTTT.AppearanceCell.Options.UseTextOptions = true;
            this.SoCTTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.SoCTTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoCTTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SoCTTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoCTTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoCTTT.Caption = "Số chứng từ TT";
            this.SoCTTT.FieldName = "SoCT";
            this.SoCTTT.Name = "SoCTTT";
            this.SoCTTT.Visible = true;
            this.SoCTTT.VisibleIndex = 2;
            // 
            // NgayCT
            // 
            this.NgayCT.AppearanceCell.Options.UseTextOptions = true;
            this.NgayCT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayCT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NgayCT.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayCT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayCT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NgayCT.Caption = "Ngày chứng từ";
            this.NgayCT.FieldName = "NgayCT";
            this.NgayCT.Name = "NgayCT";
            this.NgayCT.Visible = true;
            this.NgayCT.VisibleIndex = 3;
            // 
            // NoiPhatHanh
            // 
            this.NoiPhatHanh.AppearanceCell.Options.UseTextOptions = true;
            this.NoiPhatHanh.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NoiPhatHanh.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NoiPhatHanh.AppearanceHeader.Options.UseTextOptions = true;
            this.NoiPhatHanh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NoiPhatHanh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NoiPhatHanh.Caption = "Nơi phát hành";
            this.NoiPhatHanh.FieldName = "NoiPhatHanh";
            this.NoiPhatHanh.Name = "NoiPhatHanh";
            this.NoiPhatHanh.Visible = true;
            this.NoiPhatHanh.VisibleIndex = 4;
            // 
            // HinhThucTT
            // 
            this.HinhThucTT.AppearanceCell.Options.UseTextOptions = true;
            this.HinhThucTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HinhThucTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HinhThucTT.AppearanceHeader.Options.UseTextOptions = true;
            this.HinhThucTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HinhThucTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HinhThucTT.Caption = "Hình thức TT";
            this.HinhThucTT.FieldName = "HinhThucPhatHanh";
            this.HinhThucTT.Name = "HinhThucTT";
            this.HinhThucTT.Visible = true;
            this.HinhThucTT.VisibleIndex = 5;
            // 
            // TriGiaTT
            // 
            this.TriGiaTT.AppearanceCell.Options.UseTextOptions = true;
            this.TriGiaTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TriGiaTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaTT.AppearanceHeader.Options.UseTextOptions = true;
            this.TriGiaTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TriGiaTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaTT.Caption = "Trị giá TT";
            this.TriGiaTT.FieldName = "TriGiaTT";
            this.TriGiaTT.Name = "TriGiaTT";
            this.TriGiaTT.Visible = true;
            this.TriGiaTT.VisibleIndex = 6;
            // 
            // GhiChu
            // 
            this.GhiChu.AppearanceCell.Options.UseTextOptions = true;
            this.GhiChu.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GhiChu.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GhiChu.AppearanceHeader.Options.UseTextOptions = true;
            this.GhiChu.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GhiChu.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GhiChu.Caption = "Ghi chú";
            this.GhiChu.FieldName = "GhiChu";
            this.GhiChu.Name = "GhiChu";
            this.GhiChu.Visible = true;
            this.GhiChu.VisibleIndex = 7;
            // 
            // grvChungTuThanhToan
            // 
            this.grvChungTuThanhToan.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "Level1";
            this.grvChungTuThanhToan.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grvChungTuThanhToan.Location = new System.Drawing.Point(0, 0);
            this.grvChungTuThanhToan.LookAndFeel.SkinName = "Office 2007 Blue";
            this.grvChungTuThanhToan.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grvChungTuThanhToan.MainView = this.gridView1;
            this.grvChungTuThanhToan.Name = "grvChungTuThanhToan";
            this.grvChungTuThanhToan.Size = new System.Drawing.Size(1075, 408);
            this.grvChungTuThanhToan.TabIndex = 0;
            this.grvChungTuThanhToan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SoHopDong,
            this.NgayHopDong,
            this.TriGiaHD});
            this.gridView1.GridControl = this.grvChungTuThanhToan;
            this.gridView1.Name = "gridView1";
            // 
            // SoHopDong
            // 
            this.SoHopDong.AppearanceCell.Options.UseTextOptions = true;
            this.SoHopDong.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoHopDong.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoHopDong.AppearanceHeader.Options.UseTextOptions = true;
            this.SoHopDong.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoHopDong.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoHopDong.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.SoHopDong.Caption = "Số hợp đồng";
            this.SoHopDong.FieldName = "SoHD";
            this.SoHopDong.Name = "SoHopDong";
            this.SoHopDong.Visible = true;
            this.SoHopDong.VisibleIndex = 0;
            // 
            // NgayHopDong
            // 
            this.NgayHopDong.AppearanceCell.Options.UseTextOptions = true;
            this.NgayHopDong.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayHopDong.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NgayHopDong.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayHopDong.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayHopDong.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NgayHopDong.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.NgayHopDong.Caption = "Ngày hợp đồng";
            this.NgayHopDong.FieldName = "NgayHD";
            this.NgayHopDong.Name = "NgayHopDong";
            this.NgayHopDong.Visible = true;
            this.NgayHopDong.VisibleIndex = 1;
            // 
            // TriGiaHD
            // 
            this.TriGiaHD.AppearanceCell.Options.UseTextOptions = true;
            this.TriGiaHD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TriGiaHD.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaHD.AppearanceHeader.Options.UseTextOptions = true;
            this.TriGiaHD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TriGiaHD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaHD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.TriGiaHD.Caption = "Trị giá hợp đồng";
            this.TriGiaHD.FieldName = "TriGiaHD";
            this.TriGiaHD.Name = "TriGiaHD";
            this.TriGiaHD.Visible = true;
            this.TriGiaHD.VisibleIndex = 2;
            // 
            // donViHaiQuanNewControl1
            // 
            this.donViHaiQuanNewControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanNewControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanNewControl1.Location = new System.Drawing.Point(124, 37);
            this.donViHaiQuanNewControl1.Ma = "";
            this.donViHaiQuanNewControl1.MaCuc = "";
            this.donViHaiQuanNewControl1.Name = "donViHaiQuanNewControl1";
            this.donViHaiQuanNewControl1.ReadOnly = true;
            this.donViHaiQuanNewControl1.Size = new System.Drawing.Size(516, 22);
            this.donViHaiQuanNewControl1.TabIndex = 26;
            this.donViHaiQuanNewControl1.Ten = "";
            this.donViHaiQuanNewControl1.VisualStyleManager = this.vsmMain;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(251, 15);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(133, 13);
            this.lblTrangThai.TabIndex = 24;
            this.lblTrangThai.Text = "Chưa gửi đến Hải quan";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(180, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Trạng thái";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(125, 10);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(49, 21);
            this.txtSoTiepNhan.TabIndex = 22;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Số tiếp nhận";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Hải quan tiếp nhận";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.donViHaiQuanNewControl1);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Location = new System.Drawing.Point(6, 6);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1109, 79);
            this.uiGroupBox1.TabIndex = 27;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(1027, 533);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(79, 23);
            this.btnClose.TabIndex = 29;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdDelete,
            this.cmdSend,
            this.cmdFeedBack,
            this.cmdCancel,
            this.cmdChungtu});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Separator1,
            this.cmdSend1,
            this.cmdFeedBack1});
            this.uiCommandBar1.Key = "cmdMain";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.MergeRowOrder = 1;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(217, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend1.Icon")));
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdFeedBack1
            // 
            this.cmdFeedBack1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdFeedBack1.Icon")));
            this.cmdFeedBack1.Key = "cmdFeedBack";
            this.cmdFeedBack1.Name = "cmdFeedBack1";
            // 
            // cmdSave
            // 
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu";
            // 
            // cmdDelete
            // 
            this.cmdDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdDelete.Icon")));
            this.cmdDelete.Key = "cmdDelete";
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Text = "Xóa";
            // 
            // cmdSend
            // 
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdFeedBack
            // 
            this.cmdFeedBack.Key = "cmdFeedBack";
            this.cmdFeedBack.Name = "cmdFeedBack";
            this.cmdFeedBack.Text = "Nhận phản hồi";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Text = "Hủy khai báo";
            // 
            // cmdChungtu
            // 
            this.cmdChungtu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChungtu.Icon")));
            this.cmdChungtu.Key = "cmdChungtu";
            this.cmdChungtu.Name = "cmdChungtu";
            this.cmdChungtu.Text = "Thêm chứng từ";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1118, 32);
            // 
            // uiRebar1
            // 
            this.uiRebar1.CommandManager = this.cmMain;
            this.uiRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiRebar1.Location = new System.Drawing.Point(0, 0);
            this.uiRebar1.Name = "uiRebar1";
            this.uiRebar1.Size = new System.Drawing.Size(661, 0);
            // 
            // TabBKToKhaiXuat
            // 
            this.TabBKToKhaiXuat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TabBKToKhaiXuat.BackColor = System.Drawing.Color.Transparent;
            this.TabBKToKhaiXuat.Location = new System.Drawing.Point(6, 91);
            this.TabBKToKhaiXuat.Name = "TabBKToKhaiXuat";
            this.TabBKToKhaiXuat.Size = new System.Drawing.Size(1109, 430);
            this.TabBKToKhaiXuat.TabIndex = 30;
            this.TabBKToKhaiXuat.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.TabBKToKhaiNhap,
            this.uiTabPage2,
            this.TabBKChungTuThanhToan,
            this.TabNPLXuatTheoHDGC,
            this.TabBKNPLCungUngTheoHD,
            this.TabBKNPLChuaThanhKhoan,
            this.TabBKNPLXinNopThue,
            this.TabBKNPLTaiXuat,
            this.TabXuatNhapTon});
            this.TabBKToKhaiXuat.VisualStyleManager = this.vsmMain;
            this.TabBKToKhaiXuat.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.TabBKToKhaiXuat_SelectedTabChanged);
            // 
            // TabBKToKhaiNhap
            // 
            this.TabBKToKhaiNhap.Controls.Add(this.grvBKNhap);
            this.TabBKToKhaiNhap.Location = new System.Drawing.Point(1, 21);
            this.TabBKToKhaiNhap.Name = "TabBKToKhaiNhap";
            this.TabBKToKhaiNhap.Size = new System.Drawing.Size(1107, 408);
            this.TabBKToKhaiNhap.TabStop = true;
            this.TabBKToKhaiNhap.Text = "Bảng kê tờ khai nhập";
            // 
            // grvBKNhap
            // 
            this.grvBKNhap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grvBKNhap.Location = new System.Drawing.Point(0, 0);
            this.grvBKNhap.LookAndFeel.SkinName = "Office 2007 Blue";
            this.grvBKNhap.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grvBKNhap.MainView = this.gridView3;
            this.grvBKNhap.Name = "grvBKNhap";
            this.grvBKNhap.Size = new System.Drawing.Size(1107, 408);
            this.grvBKNhap.TabIndex = 1;
            this.grvBKNhap.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3,
            this.gridView4});
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SoToKhai,
            this.LoaiHinh,
            this.NamDK,
            this.MaHaiQuan,
            this.NgayHoanThanh});
            this.gridView3.GridControl = this.grvBKNhap;
            this.gridView3.Name = "gridView3";
            // 
            // SoToKhai
            // 
            this.SoToKhai.AppearanceCell.Options.UseTextOptions = true;
            this.SoToKhai.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SoToKhai.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoToKhai.AppearanceHeader.Options.UseTextOptions = true;
            this.SoToKhai.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoToKhai.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoToKhai.Caption = "Số tờ khai nhập";
            this.SoToKhai.FieldName = "SoToKhai";
            this.SoToKhai.Name = "SoToKhai";
            this.SoToKhai.Visible = true;
            this.SoToKhai.VisibleIndex = 0;
            // 
            // LoaiHinh
            // 
            this.LoaiHinh.AppearanceCell.Options.UseTextOptions = true;
            this.LoaiHinh.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LoaiHinh.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LoaiHinh.AppearanceHeader.Options.UseTextOptions = true;
            this.LoaiHinh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LoaiHinh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LoaiHinh.Caption = "Loại hình";
            this.LoaiHinh.FieldName = "MaLoaiHinh";
            this.LoaiHinh.Name = "LoaiHinh";
            this.LoaiHinh.Visible = true;
            this.LoaiHinh.VisibleIndex = 1;
            // 
            // NamDK
            // 
            this.NamDK.AppearanceCell.Options.UseTextOptions = true;
            this.NamDK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NamDK.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NamDK.AppearanceHeader.Options.UseTextOptions = true;
            this.NamDK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NamDK.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NamDK.Caption = "Năm đăng ký";
            this.NamDK.FieldName = "NamDangKy";
            this.NamDK.Name = "NamDK";
            this.NamDK.Visible = true;
            this.NamDK.VisibleIndex = 2;
            // 
            // MaHaiQuan
            // 
            this.MaHaiQuan.AppearanceCell.Options.UseTextOptions = true;
            this.MaHaiQuan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MaHaiQuan.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MaHaiQuan.AppearanceHeader.Options.UseTextOptions = true;
            this.MaHaiQuan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MaHaiQuan.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MaHaiQuan.Caption = "Mã Hải Quan";
            this.MaHaiQuan.FieldName = "MaHaiQuan";
            this.MaHaiQuan.Name = "MaHaiQuan";
            this.MaHaiQuan.Visible = true;
            this.MaHaiQuan.VisibleIndex = 3;
            // 
            // NgayHoanThanh
            // 
            this.NgayHoanThanh.AppearanceCell.Options.UseTextOptions = true;
            this.NgayHoanThanh.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayHoanThanh.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NgayHoanThanh.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayHoanThanh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayHoanThanh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NgayHoanThanh.Caption = "Ngày hoàn thành";
            this.NgayHoanThanh.FieldName = "NgayHoanThanh";
            this.NgayHoanThanh.Name = "NgayHoanThanh";
            this.NgayHoanThanh.Visible = true;
            this.NgayHoanThanh.VisibleIndex = 4;
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.grvBKNhap;
            this.gridView4.Name = "gridView4";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.grvBKXuat);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1075, 408);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Bảng kê tờ khai xuất";
            // 
            // grvBKXuat
            // 
            this.grvBKXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grvBKXuat.Location = new System.Drawing.Point(0, 0);
            this.grvBKXuat.LookAndFeel.SkinName = "Office 2007 Blue";
            this.grvBKXuat.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grvBKXuat.MainView = this.gridView5;
            this.grvBKXuat.Name = "grvBKXuat";
            this.grvBKXuat.Size = new System.Drawing.Size(1075, 408);
            this.grvBKXuat.TabIndex = 2;
            this.grvBKXuat.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5,
            this.gridView6});
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView5.GridControl = this.grvBKXuat;
            this.gridView5.Name = "gridView5";
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "Số tờ khai xuất";
            this.gridColumn2.FieldName = "SoToKhai";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "Loại hình";
            this.gridColumn3.FieldName = "MaLoaiHinh";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.Caption = "Năm đăng ký";
            this.gridColumn4.FieldName = "NamDangKy";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.Caption = "Mã Hải Quan";
            this.gridColumn5.FieldName = "MaHaiQuan";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn6.Caption = "Ngày hoàn thành";
            this.gridColumn6.FieldName = "NgayHoanThanh";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // gridView6
            // 
            this.gridView6.GridControl = this.grvBKXuat;
            this.gridView6.Name = "gridView6";
            // 
            // TabBKChungTuThanhToan
            // 
            this.TabBKChungTuThanhToan.Controls.Add(this.grvChungTuThanhToan);
            this.TabBKChungTuThanhToan.Location = new System.Drawing.Point(1, 21);
            this.TabBKChungTuThanhToan.Name = "TabBKChungTuThanhToan";
            this.TabBKChungTuThanhToan.Size = new System.Drawing.Size(1075, 408);
            this.TabBKChungTuThanhToan.TabStop = true;
            this.TabBKChungTuThanhToan.Text = "Chứng từ thanh toán";
            // 
            // TabNPLXuatTheoHDGC
            // 
            this.TabNPLXuatTheoHDGC.Location = new System.Drawing.Point(1, 21);
            this.TabNPLXuatTheoHDGC.Name = "TabNPLXuatTheoHDGC";
            this.TabNPLXuatTheoHDGC.Size = new System.Drawing.Size(912, 408);
            this.TabNPLXuatTheoHDGC.TabStop = true;
            this.TabNPLXuatTheoHDGC.TabVisible = false;
            this.TabNPLXuatTheoHDGC.Text = "NPL XK qua SP theo HDGC";
            // 
            // TabBKNPLCungUngTheoHD
            // 
            this.TabBKNPLCungUngTheoHD.Location = new System.Drawing.Point(1, 21);
            this.TabBKNPLCungUngTheoHD.Name = "TabBKNPLCungUngTheoHD";
            this.TabBKNPLCungUngTheoHD.Size = new System.Drawing.Size(912, 408);
            this.TabBKNPLCungUngTheoHD.TabStop = true;
            this.TabBKNPLCungUngTheoHD.TabVisible = false;
            this.TabBKNPLCungUngTheoHD.Text = "NPL Cung ứng theo HĐ mua bán";
            // 
            // TabBKNPLChuaThanhKhoan
            // 
            this.TabBKNPLChuaThanhKhoan.Controls.Add(this.grvNPLChuaThanhLy);
            this.TabBKNPLChuaThanhKhoan.Location = new System.Drawing.Point(1, 21);
            this.TabBKNPLChuaThanhKhoan.Name = "TabBKNPLChuaThanhKhoan";
            this.TabBKNPLChuaThanhKhoan.Size = new System.Drawing.Size(1075, 408);
            this.TabBKNPLChuaThanhKhoan.TabStop = true;
            this.TabBKNPLChuaThanhKhoan.Text = "NPL Chưa đưa vào TK";
            // 
            // grvNPLChuaThanhLy
            // 
            this.grvNPLChuaThanhLy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grvNPLChuaThanhLy.Location = new System.Drawing.Point(0, 0);
            this.grvNPLChuaThanhLy.LookAndFeel.SkinName = "Office 2007 Blue";
            this.grvNPLChuaThanhLy.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grvNPLChuaThanhLy.MainView = this.gridView9;
            this.grvNPLChuaThanhLy.Name = "grvNPLChuaThanhLy";
            this.grvNPLChuaThanhLy.Size = new System.Drawing.Size(1075, 408);
            this.grvNPLChuaThanhLy.TabIndex = 2;
            this.grvNPLChuaThanhLy.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9,
            this.gridView10});
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn7,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridView9.GridControl = this.grvNPLChuaThanhLy;
            this.gridView9.Name = "gridView9";
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "Số tờ khai nhập";
            this.gridColumn1.FieldName = "SoToKhai";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn7.Caption = "Loại hình";
            this.gridColumn7.FieldName = "MaLoaiHinh";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.Caption = "Mã Hải Quan";
            this.gridColumn9.FieldName = "MaHaiQuan";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn10.Caption = "Ngày hoàn thành";
            this.gridColumn10.FieldName = "NgayDangKy";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn14.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn14.Caption = "Mã NPL";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 4;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn15.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn15.Caption = "Tên NPL";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 5;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn16.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn16.Caption = "Lượng không thanh lý";
            this.gridColumn16.FieldName = "Luong";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 6;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn17.Caption = "ĐVT";
            this.gridColumn17.FieldName = "DVT_ID";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 7;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13});
            this.gridView10.GridControl = this.grvNPLChuaThanhLy;
            this.gridView10.Name = "gridView10";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Mã NPL";
            this.gridColumn8.FieldName = "MaNPL";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Tên NPL";
            this.gridColumn11.FieldName = "TenNPL";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Lượng chưa sử dụng";
            this.gridColumn12.FieldName = "Luong";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Tên đơn vị tính";
            this.gridColumn13.FieldName = "TenDVT";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 3;
            // 
            // TabBKNPLXinNopThue
            // 
            this.TabBKNPLXinNopThue.Controls.Add(this.grvBKNPLNopThue);
            this.TabBKNPLXinNopThue.Location = new System.Drawing.Point(1, 21);
            this.TabBKNPLXinNopThue.Name = "TabBKNPLXinNopThue";
            this.TabBKNPLXinNopThue.Size = new System.Drawing.Size(1075, 408);
            this.TabBKNPLXinNopThue.TabStop = true;
            this.TabBKNPLXinNopThue.Text = "Bảng kê NPL Xin nộp thuế";
            // 
            // grvBKNPLNopThue
            // 
            this.grvBKNPLNopThue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grvBKNPLNopThue.Location = new System.Drawing.Point(0, 0);
            this.grvBKNPLNopThue.LookAndFeel.SkinName = "Office 2007 Blue";
            this.grvBKNPLNopThue.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grvBKNPLNopThue.MainView = this.gridView12;
            this.grvBKNPLNopThue.Name = "grvBKNPLNopThue";
            this.grvBKNPLNopThue.Size = new System.Drawing.Size(1075, 408);
            this.grvBKNPLNopThue.TabIndex = 3;
            this.grvBKNPLNopThue.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12,
            this.gridView11});
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29});
            this.gridView12.GridControl = this.grvBKNPLNopThue;
            this.gridView12.Name = "gridView12";
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn22.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn22.Caption = "Số tờ khai nhập";
            this.gridColumn22.FieldName = "SoToKhai";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 0;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn23.Caption = "Loại hình";
            this.gridColumn23.FieldName = "MaLoaiHinh";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn24.Caption = "Mã Hải Quan";
            this.gridColumn24.FieldName = "MaHaiQuan";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 2;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn25.Caption = "Ngày hoàn thành";
            this.gridColumn25.FieldName = "NgayDangKy";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 3;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn26.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn26.Caption = "Mã NPL";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 4;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn27.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn27.Caption = "Tên NPL";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 5;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn28.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn28.Caption = "Lượng nộp thuế";
            this.gridColumn28.FieldName = "LuongNopThue";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 6;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn29.Caption = "ĐVT";
            this.gridColumn29.FieldName = "DVT_ID";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 7;
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21});
            this.gridView11.GridControl = this.grvBKNPLNopThue;
            this.gridView11.Name = "gridView11";
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Mã NPL";
            this.gridColumn18.FieldName = "MaNPL";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 0;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Tên NPL";
            this.gridColumn19.FieldName = "TenNPL";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 1;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Lượng chưa sử dụng";
            this.gridColumn20.FieldName = "Luong";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 2;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Tên đơn vị tính";
            this.gridColumn21.FieldName = "TenDVT";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 3;
            // 
            // TabBKNPLTaiXuat
            // 
            this.TabBKNPLTaiXuat.Controls.Add(this.grvNPLTaiXuat);
            this.TabBKNPLTaiXuat.Location = new System.Drawing.Point(1, 21);
            this.TabBKNPLTaiXuat.Name = "TabBKNPLTaiXuat";
            this.TabBKNPLTaiXuat.Size = new System.Drawing.Size(1075, 408);
            this.TabBKNPLTaiXuat.TabStop = true;
            this.TabBKNPLTaiXuat.Text = "NPL Tái xuất";
            // 
            // grvNPLTaiXuat
            // 
            this.grvNPLTaiXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grvNPLTaiXuat.Location = new System.Drawing.Point(0, 0);
            this.grvNPLTaiXuat.LookAndFeel.SkinName = "Office 2007 Blue";
            this.grvNPLTaiXuat.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grvNPLTaiXuat.MainView = this.gridView13;
            this.grvNPLTaiXuat.Name = "grvNPLTaiXuat";
            this.grvNPLTaiXuat.Size = new System.Drawing.Size(1075, 408);
            this.grvNPLTaiXuat.TabIndex = 4;
            this.grvNPLTaiXuat.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView13,
            this.gridView14});
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn37,
            this.gridColumn36,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44});
            this.gridView13.GridControl = this.grvNPLTaiXuat;
            this.gridView13.Name = "gridView13";
            // 
            // gridColumn30
            // 
            this.gridColumn30.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn30.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn30.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn30.Caption = "Số tờ khai nhập";
            this.gridColumn30.FieldName = "SoToKhai";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 0;
            // 
            // gridColumn31
            // 
            this.gridColumn31.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn31.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn31.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn31.Caption = "Loại hình";
            this.gridColumn31.FieldName = "MaLoaiHinh";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 1;
            // 
            // gridColumn32
            // 
            this.gridColumn32.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn32.Caption = "Mã Hải Quan";
            this.gridColumn32.FieldName = "MaHaiQuan";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 2;
            // 
            // gridColumn33
            // 
            this.gridColumn33.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn33.Caption = "Ngày hoàn thành";
            this.gridColumn33.FieldName = "NgayDangKy";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 3;
            // 
            // gridColumn34
            // 
            this.gridColumn34.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn34.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn34.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn34.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn34.Caption = "Mã NPL";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 4;
            // 
            // gridColumn35
            // 
            this.gridColumn35.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn35.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn35.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn35.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn35.Caption = "Tên NPL";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 5;
            // 
            // gridColumn37
            // 
            this.gridColumn37.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn37.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn37.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn37.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn37.Caption = "ĐVT";
            this.gridColumn37.FieldName = "DVT_ID";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 6;
            // 
            // gridColumn36
            // 
            this.gridColumn36.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn36.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn36.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn36.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn36.Caption = "Số TK xuất";
            this.gridColumn36.FieldName = "Số TK Xuất";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 7;
            // 
            // gridColumn42
            // 
            this.gridColumn42.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn42.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn42.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn42.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn42.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn42.Caption = "Mã LH Xuất";
            this.gridColumn42.FieldName = "MaLoaiHinhXuat";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 8;
            // 
            // gridColumn43
            // 
            this.gridColumn43.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn43.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn43.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn43.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn43.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn43.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn43.Caption = "Hải quan xuất";
            this.gridColumn43.FieldName = "MaHaiQuanXuat";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 9;
            // 
            // gridColumn44
            // 
            this.gridColumn44.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn44.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn44.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn44.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn44.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn44.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn44.Caption = "NgayDangKyXuat";
            this.gridColumn44.FieldName = "NgayDangKyXuat";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 10;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41});
            this.gridView14.GridControl = this.grvNPLTaiXuat;
            this.gridView14.Name = "gridView14";
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Mã NPL";
            this.gridColumn38.FieldName = "MaNPL";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 0;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Tên NPL";
            this.gridColumn39.FieldName = "TenNPL";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 1;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Lượng chưa sử dụng";
            this.gridColumn40.FieldName = "Luong";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 2;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Tên đơn vị tính";
            this.gridColumn41.FieldName = "TenDVT";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 3;
            // 
            // TabXuatNhapTon
            // 
            this.TabXuatNhapTon.Controls.Add(this.grvXuatNhapTon);
            this.TabXuatNhapTon.Location = new System.Drawing.Point(1, 21);
            this.TabXuatNhapTon.Name = "TabXuatNhapTon";
            this.TabXuatNhapTon.Size = new System.Drawing.Size(1075, 408);
            this.TabXuatNhapTon.TabStop = true;
            this.TabXuatNhapTon.Text = "NPL Xuất-Nhập-Tồn";
            // 
            // grvXuatNhapTon
            // 
            this.grvXuatNhapTon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grvXuatNhapTon.Location = new System.Drawing.Point(0, 0);
            this.grvXuatNhapTon.LookAndFeel.SkinName = "Office 2007 Blue";
            this.grvXuatNhapTon.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grvXuatNhapTon.MainView = this.gridView7;
            this.grvXuatNhapTon.Name = "grvXuatNhapTon";
            this.grvXuatNhapTon.Size = new System.Drawing.Size(1075, 408);
            this.grvXuatNhapTon.TabIndex = 3;
            this.grvXuatNhapTon.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7,
            this.gridView8});
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MaNPL,
            this.TenNPL,
            this.TonDau,
            this.LuongSanXuat,
            this.TaiXuat,
            this.LuongNopThue,
            this.LuongXuatTheoHDGC,
            this.LuongTonCuoi,
            this.DVT,
            this.DonGiaTT});
            this.gridView7.GridControl = this.grvXuatNhapTon;
            this.gridView7.Name = "gridView7";
            // 
            // MaNPL
            // 
            this.MaNPL.AppearanceCell.Options.UseTextOptions = true;
            this.MaNPL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MaNPL.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MaNPL.AppearanceHeader.Options.UseTextOptions = true;
            this.MaNPL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MaNPL.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MaNPL.Caption = "NPL";
            this.MaNPL.FieldName = "MaNPL";
            this.MaNPL.Name = "MaNPL";
            this.MaNPL.Visible = true;
            this.MaNPL.VisibleIndex = 0;
            this.MaNPL.Width = 109;
            // 
            // TenNPL
            // 
            this.TenNPL.AppearanceCell.Options.UseTextOptions = true;
            this.TenNPL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TenNPL.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TenNPL.AppearanceHeader.Options.UseTextOptions = true;
            this.TenNPL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TenNPL.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TenNPL.Caption = "Tên ";
            this.TenNPL.FieldName = "TenNPL";
            this.TenNPL.Name = "TenNPL";
            this.TenNPL.Visible = true;
            this.TenNPL.VisibleIndex = 1;
            this.TenNPL.Width = 109;
            // 
            // TonDau
            // 
            this.TonDau.AppearanceCell.Options.UseTextOptions = true;
            this.TonDau.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TonDau.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TonDau.AppearanceHeader.Options.UseTextOptions = true;
            this.TonDau.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TonDau.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TonDau.Caption = "Lượng tồn đầu";
            this.TonDau.FieldName = "TonDau";
            this.TonDau.Name = "TonDau";
            this.TonDau.Visible = true;
            this.TonDau.VisibleIndex = 3;
            this.TonDau.Width = 109;
            // 
            // LuongSanXuat
            // 
            this.LuongSanXuat.AppearanceCell.Options.UseTextOptions = true;
            this.LuongSanXuat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LuongSanXuat.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LuongSanXuat.AppearanceHeader.Options.UseTextOptions = true;
            this.LuongSanXuat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LuongSanXuat.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LuongSanXuat.Caption = "Lượng sản xuất";
            this.LuongSanXuat.FieldName = "LuongSanXuat";
            this.LuongSanXuat.Name = "LuongSanXuat";
            this.LuongSanXuat.Visible = true;
            this.LuongSanXuat.VisibleIndex = 4;
            this.LuongSanXuat.Width = 109;
            // 
            // TaiXuat
            // 
            this.TaiXuat.AppearanceCell.Options.UseTextOptions = true;
            this.TaiXuat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TaiXuat.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TaiXuat.AppearanceHeader.Options.UseTextOptions = true;
            this.TaiXuat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TaiXuat.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TaiXuat.Caption = "Lượng tái xuất";
            this.TaiXuat.FieldName = "LuongTaiXuat";
            this.TaiXuat.Name = "TaiXuat";
            this.TaiXuat.Visible = true;
            this.TaiXuat.VisibleIndex = 5;
            this.TaiXuat.Width = 109;
            // 
            // LuongNopThue
            // 
            this.LuongNopThue.AppearanceCell.Options.UseTextOptions = true;
            this.LuongNopThue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LuongNopThue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LuongNopThue.AppearanceHeader.Options.UseTextOptions = true;
            this.LuongNopThue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LuongNopThue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LuongNopThue.Caption = "Lượng nộp thuế";
            this.LuongNopThue.FieldName = "LuongNopThue";
            this.LuongNopThue.Name = "LuongNopThue";
            this.LuongNopThue.Visible = true;
            this.LuongNopThue.VisibleIndex = 6;
            this.LuongNopThue.Width = 109;
            // 
            // LuongXuatTheoHDGC
            // 
            this.LuongXuatTheoHDGC.AppearanceCell.Options.UseTextOptions = true;
            this.LuongXuatTheoHDGC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LuongXuatTheoHDGC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LuongXuatTheoHDGC.AppearanceHeader.Options.UseTextOptions = true;
            this.LuongXuatTheoHDGC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LuongXuatTheoHDGC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LuongXuatTheoHDGC.Caption = "Lượng xuất theo HĐGC";
            this.LuongXuatTheoHDGC.FieldName = "LuongXuatTheoHDGC";
            this.LuongXuatTheoHDGC.Name = "LuongXuatTheoHDGC";
            this.LuongXuatTheoHDGC.Visible = true;
            this.LuongXuatTheoHDGC.VisibleIndex = 7;
            this.LuongXuatTheoHDGC.Width = 126;
            // 
            // LuongTonCuoi
            // 
            this.LuongTonCuoi.AppearanceCell.Options.UseTextOptions = true;
            this.LuongTonCuoi.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LuongTonCuoi.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LuongTonCuoi.AppearanceHeader.Options.UseTextOptions = true;
            this.LuongTonCuoi.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LuongTonCuoi.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LuongTonCuoi.Caption = "Lượng tồn cuối";
            this.LuongTonCuoi.FieldName = "LuongTonCuoi";
            this.LuongTonCuoi.Name = "LuongTonCuoi";
            this.LuongTonCuoi.Visible = true;
            this.LuongTonCuoi.VisibleIndex = 8;
            this.LuongTonCuoi.Width = 100;
            // 
            // DVT
            // 
            this.DVT.AppearanceCell.Options.UseTextOptions = true;
            this.DVT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DVT.AppearanceHeader.Options.UseTextOptions = true;
            this.DVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DVT.Caption = "ĐVT";
            this.DVT.FieldName = "DVT";
            this.DVT.Name = "DVT";
            this.DVT.Visible = true;
            this.DVT.VisibleIndex = 9;
            this.DVT.Width = 102;
            // 
            // DonGiaTT
            // 
            this.DonGiaTT.AppearanceCell.Options.UseTextOptions = true;
            this.DonGiaTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DonGiaTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DonGiaTT.AppearanceHeader.Options.UseTextOptions = true;
            this.DonGiaTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DonGiaTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DonGiaTT.Caption = "Đơn giá TT";
            this.DonGiaTT.FieldName = "DonGiaTT";
            this.DonGiaTT.Name = "DonGiaTT";
            this.DonGiaTT.Visible = true;
            this.DonGiaTT.VisibleIndex = 2;
            this.DonGiaTT.Width = 109;
            // 
            // gridView8
            // 
            this.gridView8.GridControl = this.grvXuatNhapTon;
            this.gridView8.Name = "gridView8";
            // 
            // ThanhKhoanSendForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 600);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ThanhKhoanSendForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Khai báo thanh khoản";
            this.Load += new System.EventHandler(this.ThanhKhoanSendForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvChungTuThanhToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabBKToKhaiXuat)).EndInit();
            this.TabBKToKhaiXuat.ResumeLayout(false);
            this.TabBKToKhaiNhap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvBKNhap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvBKXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            this.TabBKChungTuThanhToan.ResumeLayout(false);
            this.TabBKNPLChuaThanhKhoan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvNPLChuaThanhLy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            this.TabBKNPLXinNopThue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvBKNPLNopThue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            this.TabBKNPLTaiXuat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvNPLTaiXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            this.TabXuatNhapTon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvXuatNhapTon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Company.Interface.Controls.DonViHaiQuanNewControl donViHaiQuanNewControl1;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar uiRebar1;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedBack1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedBack;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungtu;
        private Janus.Windows.UI.Tab.UITab TabBKToKhaiXuat;
        private Janus.Windows.UI.Tab.UITabPage TabBKToKhaiNhap;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.UI.Tab.UITabPage TabBKChungTuThanhToan;
        private Janus.Windows.UI.Tab.UITabPage TabNPLXuatTheoHDGC;
        private Janus.Windows.UI.Tab.UITabPage TabBKNPLCungUngTheoHD;
        private Janus.Windows.UI.Tab.UITabPage TabBKNPLChuaThanhKhoan;
        private Janus.Windows.UI.Tab.UITabPage TabBKNPLXinNopThue;
        private Janus.Windows.UI.Tab.UITabPage TabBKNPLTaiXuat;
        private Janus.Windows.UI.Tab.UITabPage TabXuatNhapTon;
        private DevExpress.XtraGrid.GridControl grvBKNhap;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn SoToKhai;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.GridControl grvChungTuThanhToan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn SoHopDong;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn LoaiHinh;
        private DevExpress.XtraGrid.Columns.GridColumn NamDK;
        private DevExpress.XtraGrid.Columns.GridColumn MaHaiQuan;
        private DevExpress.XtraGrid.Columns.GridColumn NgayHoanThanh;
        private DevExpress.XtraGrid.GridControl grvBKXuat;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.GridControl grvXuatNhapTon;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn MaNPL;
        private DevExpress.XtraGrid.Columns.GridColumn TenNPL;
        private DevExpress.XtraGrid.Columns.GridColumn TonDau;
        private DevExpress.XtraGrid.Columns.GridColumn LuongSanXuat;
        private DevExpress.XtraGrid.Columns.GridColumn TaiXuat;
        private DevExpress.XtraGrid.Columns.GridColumn LuongNopThue;
        private DevExpress.XtraGrid.Columns.GridColumn LuongXuatTheoHDGC;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn LuongTonCuoi;
        private DevExpress.XtraGrid.Columns.GridColumn DVT;
        private DevExpress.XtraGrid.Columns.GridColumn DonGiaTT;
        private DevExpress.XtraGrid.Columns.GridColumn NgayHopDong;
        private DevExpress.XtraGrid.Columns.GridColumn TriGiaHD;
        private DevExpress.XtraGrid.Columns.GridColumn MaHang;
        private DevExpress.XtraGrid.Columns.GridColumn TriGiaHang;
        private DevExpress.XtraGrid.Columns.GridColumn SoCTTT;
        private DevExpress.XtraGrid.Columns.GridColumn NgayCT;
        private DevExpress.XtraGrid.Columns.GridColumn NoiPhatHanh;
        private DevExpress.XtraGrid.Columns.GridColumn HinhThucTT;
        private DevExpress.XtraGrid.Columns.GridColumn TriGiaTT;
        private DevExpress.XtraGrid.Columns.GridColumn GhiChu;
        private DevExpress.XtraGrid.GridControl grvNPLChuaThanhLy;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.GridControl grvBKNPLNopThue;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.GridControl grvNPLTaiXuat;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
    }
}