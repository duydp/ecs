using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;

namespace Company.Interface.Report.GC
{
    public partial class BangKe02_NVLCUTT74 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewBC02_NVLCUTT74Form report;
        private int STT = 0;
        public DataRow drToKhai;
        public DataTable Source = new DataTable();
        public HopDong HD = new HopDong();
        public string _Sotokhai = "";
        public string _MaLoaiHinh = "";
        public string _NgayToKhai = "";

        public BangKe02_NVLCUTT74()
        {
            InitializeComponent();
        }
       
        public void BindReport()
        {
            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamDangKyInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }
            
            this.DataSource = Source;

            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;
            lblSoToKhai.Text = this._Sotokhai + " / " + LoaiHinhMauDich.GetTenVietTac(this._MaLoaiHinh);
            lblNgayToKhai.Text = this._NgayToKhai;
            lblMathang.Text = loaiSP;
            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lblThoiHan.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            lblSTT.DataBindings.Add("Text", this.DataSource, Source.TableName + ".STT");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, Source.TableName + ".Ten");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, Source.TableName + ".Ma");
            lblDVT.DataBindings.Add("Text", this.DataSource, Source.TableName + ".DVT");
            lblLuongCU.DataBindings.Add("Text", this.DataSource, Source.TableName + ".SoLuong", "{0:N3}");
            lblDonGia.DataBindings.Add("Text", this.DataSource, Source.TableName + ".DonGia", "{0:N3}");
            lblTriGia.DataBindings.Add("Text", this.DataSource, Source.TableName + ".TriGia", "{0:N3}");
            lblHTCU.DataBindings.Add("Text", this.DataSource, Source.TableName + ".HinhThuc");
            xrLabel2.Text = GlobalSettings.TieudeNgay;
        }

        private void lblSoToKhai_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label1 = (XRLabel)sender;
            report.label = label1;
            report.txtName.Text = label1.Text;
            report.lblName.Text = label1.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            label.Text = text;
        }
    }
}
