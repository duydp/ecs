﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace Company.KDT.SHARE.Components.Messages
{
    public class ToKhaiCTMsg
    {
        XmlDocument doc = null;
        ApproverMessage gui, layphanhoi, gaploi, tuchoi, capsotokhai, dangxuly, capsotiepnhan;
        public ToKhaiCTMsg(string xml)
        {
            doc.LoadXml(xml);
            gaploi = new ToKhaiGapLoi();
            capsotiepnhan = new ToKhaiCTCapSoTiepNhan();
        }
    }
   
    public class ToKhaiGapLoi : ApproverMessage
    {
        public override string MessageXmlHandler(object sender, MessageXmlEventArgs e)
        {
            if (e.MessageXml.Statement != null && e.MessageXml.Statement.Value == 12)
            {
                string msg = string.Empty;
                XmlNode node = e.MessageXml.XmlDocument.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content");
                if (node != null)
                    msg = node.InnerText;
                if (!string.IsNullOrEmpty(msg))
                    return msg;
                node = e.MessageXml.XmlDocument.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content");

                if (node.InnerText == "")
                {
                    foreach (XmlNode item in e.MessageXml.XmlDocument.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").ChildNodes)
                    {
                        msg += node.InnerText + "; \n";
                    }
                    return msg + "|";
                }
                else if (node.InnerText.Contains("lỗi"))
                {
                    foreach (XmlNode item in node.ChildNodes)
                    {
                        msg += node.InnerText + "; \n";
                    }
                    return msg + "|";
                }

                node = e.MessageXml.XmlDocument.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content/XMLERRORINFORMATION");
                if (node != null)
                {
                    foreach (XmlNode item in node.ChildNodes)
                    {
                        msg += item.InnerText + "; \n";
                    }
                    return msg + "|";
                }
                else
                    return string.Empty;

            }
            else if (Successor != null)
                return Successor.MessageXmlHandler(this, e);
            else return string.Empty;
            
        }
    }
    public class ToKhaiCTCapSoTiepNhan : ApproverMessage
    {
        public override string MessageXmlHandler(object sender, MessageXmlEventArgs e)
        {
            return string.Empty;
        }
    }
    public class ToKhaiCTKhaoBao : ApproverMessage
    {
        public override string MessageXmlHandler(object sender, MessageXmlEventArgs e)
        {
            
            return string.Empty;
        }
    }
    
}
