﻿//Lypt created
//Date 07/01/2010
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;

namespace Company.Interface.Report.KDDT
{
    public partial class TQDTToKhaiXK : DevExpress.XtraReports.UI.XtraReport
    {
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //public string PTVT_Name = "";
        //public string PTTT_Name = "";
        //public string DKGH = "";
        //public string DongTienTT = "";
        //public string NuocXK = "";

        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKXTQDTForm report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public bool isCuaKhau = false;
        public TQDTToKhaiXK()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            try
            {
                this.PrintingSystem.ShowMarginsWarning = false;
                decimal tongTriGiaNT = 0;
                decimal tongTienThueXNK = 0;
                decimal tongTienThueTatCa = 0;
                decimal tongTriGiaTT = 0;
                decimal tongTriGiaTTGTGT = 0;
                decimal tongThueGTGT = 0;
                decimal tongTriGiaThuKhac = 0;

                lblMienThueNK.Visible = MienThue1;
                lblMienThueGTGT.Visible = MienThue2;
                lblMienThueNK.Text = Properties.Settings.Default.TieuDeInDinhMuc;
                lblMienThueGTGT.Text = Properties.Settings.Default.MienThueGTGT;
                lblMienThueGTGT.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblMienThueNK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                DateTime minDate = new DateTime(1900, 1, 1);
                
                if (this.TKMD.SoTiepNhan > 0)
                {
                    this.lblThamChieu.Text = this.TKMD.SoTiepNhan.ToString();
                    //this.lblThamChieu.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                }
                else
                {
                    this.lblThamChieu.Text = this.TKMD.ID.ToString();
                    //this.lblThamChieu.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                }

                /*Thông tin Hải quan*/
                lblChiCucHQ.Text = Company.KD.BLL.DuLieuChuan.DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                lblChiCucHQ.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (isCuaKhau)
                {
                    string chicuc = Company.BLL.KDT.SXXK.NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                    if (chicuc == string.Empty)
                        chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                    lblChiCucHQCK.Text = chicuc;
                    lblChiCucHQCK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                }
                else
                    lblChiCucHQCK.Text = string.Empty;

                //Ngày tiếp nhận
                if (this.TKMD.NgayTiepNhan > minDate)
                    lblNgayGui.Text = this.TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
                else
                    lblNgayGui.Text = "";

                //Số tờ khai
                if (this.TKMD.SoToKhai > 0)
                    lblToKhai.Text = this.TKMD.SoToKhai + "";
                else
                    lblToKhai.Text = "";

                //Ngày đăng ký
                if (this.TKMD.NgayDangKy > minDate)
                    lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                else
                    lblNgayDangKy.Text = "";

                //1. Người xuất khẩu
                lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep);
                lblMaDoanhNghiep.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("WordWrap")))
                {
                    if (GlobalSettings.MaMID.Equals(""))
                        lblNguoiXK.Text = this.TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI;
                    else
                        lblNguoiXK.Text = this.TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI + "\r\n" + "Mã MID: " + GlobalSettings.MaMID;
                    lblNguoiXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                }
                else
                {
                    if (GlobalSettings.MaMID.Equals(""))
                        lblNguoiXK.Text = this.TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI;
                    else
                        lblNguoiXK.Text = this.TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI + ". " + "Mã MID: " + GlobalSettings.MaMID;
                    lblNguoiXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                }
                
                //2.Người nhập khẩu
                lblNguoiNK.Text = TKMD.TenDonViDoiTac.ToString();
                lblNguoiNK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //3. Người ủy thác
                if ((!TKMD.MaDonViUT.Equals("")) && (!TKMD.TenDonViUT.Equals("")))
                {
                    lblNguoiUyThac.Text = TKMD.MaDonViUT + " - " + TKMD.TenDonViUT;
                    lblNguoiUyThac.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                }

                //4. Dai ly lam thu tuc hai quan


                //5. Loai hinh
                string stlh = "";
                //if (TKMD.MaLoaiHinh == "NKD01")
                //    stlh = "  Nhập kinh doanh";
                stlh = Company.KD.BLL.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                lblLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + stlh;
                lblLoaiHinh.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //6. Giay phep
                if (TKMD.SoGiayPhep != "")
                {
                    lblGiayPhep.Text = "" + this.TKMD.SoGiayPhep;
                    lblGiayPhep.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                }
                else
                {
                    lblGiayPhep.Text = "";
                    lblGiayPhep.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                }
                if (this.TKMD.NgayGiayPhep > minDate)
                    lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                else
                    lblNgayGiayPhep.Text = "";
                if (this.TKMD.NgayHetHanGiayPhep > minDate)
                    lblNgayHetHanGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanGiayPhep.Text = "";

                //7. Hop dong
                if (this.TKMD.SoHopDong.Length > 36)
                    lblHopDong.Font = new Font("Times New Roman", 6.5f);
                lblHopDong.Text = "" + this.TKMD.SoHopDong;
                if (this.TKMD.NgayHopDong > minDate)
                    lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
                else
                    lblNgayHopDong.Text = " ";
                if (this.TKMD.NgayHetHanHopDong > minDate)
                    lblNgayHetHanHopDong.Text = "" + this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanHopDong.Text = " ";

                //8. Hoa don thuong mai
                lblHoaDonThuongMai.Text = "" + this.TKMD.SoHoaDonThuongMai;
                lblHoaDonThuongMai.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (this.TKMD.NgayHoaDonThuongMai > minDate)
                {
                    lblNgayHDTM.Text = this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
                    lblNgayHDTM.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                }
                else
                    lblNgayHDTM.Text = "";

                //9. Cang xep hang
                lblMaCuaKhauXepHang.Text = TKMD.CuaKhau_ID;
                lblMaCuaKhauXepHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblCuaKhauXepHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                lblCuaKhauXepHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //10. Nuoc nhap khau
                lblNuocNK.Text = this.TKMD.NuocNK_ID;
                lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocNK_ID);

                //11. Dieu kien giao hang
                lblDieuKienGiaoHang.Text = this.TKMD.DKGH_ID;
                lblDieuKienGiaoHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //12. Phuong thuc thanh toan
                lblPhuongThucThanhToan.Text = this.TKMD.PTTT_ID;
                lblPhuongThucThanhToan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //13. Dong tien thanh toan
                lblDongTienThanhToan.Text = this.TKMD.NguyenTe_ID;
                lblDongTienThanhToan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //14. Ty gia tinh thue
                lblTyGiaTinhThue.Text = this.TKMD.TyGiaTinhThue.ToString("G10");
                lblTyGiaTinhThue.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //15. Ket qua phan luong
                //if (TKMD.PhanLuong == "1")
                //    lblLyDoTK.Text = "Tờ khai được thông quan";
                //else if (TKMD.PhanLuong == "2")
                //    lblLyDoTK.Text = "Tờ khai phải xuất trình giấy tờ";
                //else if (TKMD.PhanLuong == "3")
                //    lblLyDoTK.Text = "Tờ khai phải kiểm hóa";
                //else
                //lblLyDoTK.Text = "";
                //lblLyDoTK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblHuongDan.Text = TKMD.HUONGDAN;
                lblHuongDan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //Tong so CONTAINER
                string tsContainer = "";
                string cont20 = "", cont40 = "", soKien = "";
                if (TKMD.SoContainer20 > 0)
                {
                    cont20 = "Cont20: " + TKMD.SoContainer20;

                    tsContainer += cont20 + "; ";
                }
                else
                    cont20 = "";

                if (TKMD.SoContainer40 > 0)
                {
                    cont40 = "Cont40: " + Convert.ToInt32(TKMD.SoContainer40);

                    tsContainer += cont40 + "; ";
                }
                else
                    cont40 = "";

                if (TKMD.SoKien > 0)
                {
                    soKien = "Tổng số kiện: " + TKMD.SoKien;

                    tsContainer += soKien;
                }
                else
                    soKien = "";
                lblTongSoContainer.Text = tsContainer.Length > 0 ? "Tổng số container: " + tsContainer : "Tổng số container:";

                //27. Tong trong luong
                if (TKMD.TrongLuong > 0)
                    lbltongtrongluong.Text = TKMD.TrongLuong + " kg ";
                else
                    lbltongtrongluong.Text = "";

                //Chi Tiet phu luc dinh kem
                if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
                    lblChitietCon.Text = "danh sách container theo bảng kê đính kèm";
                else
                {
                    string soHieuKienCont = "";
                    Company.KDT.SHARE.QuanLyChungTu.Container objCont = null;

                    if (TKMD.VanTaiDon == null)
                    {
                        System.Data.DataTable dt = TKMD.GetContainerInfo(TKMD.ID);

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            for (int cont = 0; cont < dt.Rows.Count; cont++)
                            {
                                System.Data.DataRow row = dt.Rows[cont];

                                soHieuKienCont += row["SoHieu"].ToString() + "/" + row["Seal_No"].ToString();

                                if (cont < dt.Rows.Count - 1)
                                    soHieuKienCont += "; ";
                            }
                        }
                    }
                    else if (TKMD.VanTaiDon != null)
                    {
                        for (int cont = 0; cont < TKMD.VanTaiDon.ContainerCollection.Count; cont++)
                        {
                            objCont = TKMD.VanTaiDon.ContainerCollection[cont];

                            soHieuKienCont += objCont.SoHieu + "/" + objCont.Seal_No;

                            if (cont < TKMD.VanTaiDon.ContainerCollection.Count - 1)
                                soHieuKienCont += "; ";
                        }
                    }

                    lblChitietCon.Text = soHieuKienCont;
                }

                //Chi Phi Khac
                string st = "";
                if (this.TKMD.PhiBaoHiem > 0)
                    st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
                if (this.TKMD.PhiVanChuyen > 0)
                    st += " F = " + this.TKMD.PhiVanChuyen.ToString("N2");
                if (this.TKMD.PhiKhac > 0)
                    st += " Phí khác = " + this.TKMD.PhiKhac.ToString("N2");
                //lblPhiBaoHiem.Text = st;

                //THONG TIN HANG
                if (this.TKMD.HMDCollection.Count <= 3)
                {
                    #region Chi tiet hang hoa
                    if (this.TKMD.HMDCollection.Count >= 1)
                    {
                        #region THONG TIN HANG
                        HangMauDich hmd = this.TKMD.HMDCollection[0];
                        if (!inMaHang)
                            TenHang1.Text = hmd.TenHang;
                        else
                        {
                            if (hmd.MaPhu.Trim().Length > 0)
                                TenHang1.Text = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                TenHang1.Text = hmd.TenHang;
                        }
                        TenHang1.WordWrap = true;
                        TenHang1.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                        MaHS1.Text = hmd.MaHS;
                        XuatXu1.Text = hmd.NuocXX_ID;
                        Luong1.Text = hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.SoLuongHMD));
                        DVT1.Text = DonViTinh.GetName(hmd.DVT_ID);

                        DonGiaNT1.Text = hmd.DonGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.DonGiaNT));
                        TriGiaNT1.Text = hmd.TriGiaKB.ToString("N2");
                        TriGiaTT1.Text = hmd.TriGiaTT.ToString("N0");
                        ThueSuatXNK1.Text = hmd.ThueSuatXNK.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(5));// Linhhtn - Thêm Thuế suất ở bảng in

                        //if (hmd.ThueTuyetDoi)
                        //{
                        //    ThueSuatXNK1.Text = "";
                        //}
                        //else
                        //{
                        //if (hmd.ThueSuatXNKGiam.Trim() == "")
                        //    ThueSuatXNK1.Text = hmd.ThueSuatXNK.ToString("N0");
                        //else
                        //    ThueSuatXNK1.Text = hmd.ThueSuatXNKGiam;
                        //}
                        TienThueXNK1.Text = hmd.ThueXNK.ToString("N0");
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            //TriGiaTTGTGT1.Text = TriGiaTTGTGT.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Trim() == "")
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatVATGiam;
                            //TienThueGTGT1.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            //TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam;
                            //TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            //TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam;
                            //TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Length == 0)
                            //    TyLeThuKhac1.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    TyLeThuKhac1.Text = hmd.ThueSuatVATGiam;
                            TriGiaThuKhac1.Text = hmd.ThueGTGT.ToString("N0");
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }
                        //if (hmd.MienThue == 1)
                        //{
                        //    TriGiaTT1.Text = "";
                        //    ThueSuatXNK1.Text = "";
                        //    TienThueXNK1.Text = "";
                        //    TyLeThuKhac1.Text = "";
                        //    TriGiaThuKhac1.Text = "";
                        //}

                        tongTriGiaNT += Convert.ToDecimal(Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero));
                        tongTienThueTatCa = tongTienThueTatCa + Convert.ToDecimal(hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB);// +hmd.ThueGTGT;
                        #endregion
                    }
                    if (MienThue1)
                    {
                        TriGiaTT1.Text = "";
                        ThueSuatXNK1.Text = "";
                        TienThueXNK1.Text = "";
                    }
                    if (MienThue2)
                    {

                    }

                    //}
                    if (this.TKMD.HMDCollection.Count >= 2)
                    {
                        HangMauDich hmd = this.TKMD.HMDCollection[1];
                        if (!inMaHang)
                            TenHang2.Text = hmd.TenHang;
                        else
                        {
                            if (hmd.MaPhu.Trim().Length > 0)
                                TenHang2.Text = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                TenHang2.Text = hmd.TenHang;
                        }
                        TenHang2.WordWrap = true;
                        TenHang2.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                        MaHS2.Text = hmd.MaHS;
                        XuatXu2.Text = hmd.NuocXX_ID;
                        Luong2.Text = hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.SoLuongHMD));
                        DVT2.Text = DonViTinh.GetName(hmd.DVT_ID);

                        DonGiaNT2.Text = hmd.DonGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.DonGiaNT));
                        TriGiaNT2.Text = hmd.TriGiaKB.ToString("N2");
                        TriGiaTT2.Text = hmd.TriGiaTT.ToString("N0");
                        //if (hmd.ThueTuyetDoi)
                        //{
                        //    ThueSuatXNK2.Text = "";
                        //}
                        //else
                        //{
                        //if (hmd.ThueSuatXNKGiam.Trim() == "")
                        //    ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString("N0");
                        //else
                        //    ThueSuatXNK2.Text = hmd.ThueSuatXNKGiam;
                        //}
                        ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(5));// KhanhHN - 12/10
                        TienThueXNK2.Text = hmd.ThueXNK.ToString("N0");
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            //TriGiaTTGTGT2.Text = TriGiaTTGTGT.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Trim() == "")
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatVATGiam;
                            //TienThueGTGT2.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            //TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam;
                            //TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);

                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            //TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam;
                            //TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Length == 0)
                            //    TyLeThuKhac2.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    TyLeThuKhac2.Text = hmd.ThueSuatVATGiam;
                            TriGiaThuKhac2.Text = hmd.ThueGTGT.ToString("N0");
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }
                        //}
                        //if (hmd.MienThue == 1)
                        //{
                        //    TriGiaTT2.Text = "";
                        //    ThueSuatXNK2.Text = "";
                        //    TienThueXNK2.Text = "";
                        //    TyLeThuKhac2.Text = "";
                        //    TriGiaThuKhac2.Text = "";
                        //}

                        tongTriGiaNT += Convert.ToDecimal(Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero));
                        tongTienThueTatCa = tongTienThueTatCa + Convert.ToDecimal(hmd.ThueXNK + hmd.TriGiaThuKhac);// +hmd.ThueTTDB + hmd.ThueGTGT;
                    }

                    if (this.TKMD.HMDCollection.Count == 3)
                    {
                        HangMauDich hmd = this.TKMD.HMDCollection[2];
                        if (!inMaHang)
                            TenHang3.Text = hmd.TenHang;
                        else
                        {
                            if (hmd.MaPhu.Trim().Length > 0)
                                TenHang3.Text = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                TenHang3.Text = hmd.TenHang;
                        }
                        TenHang3.WordWrap = true;
                        TenHang3.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                        MaHS3.Text = hmd.MaHS;
                        XuatXu3.Text = hmd.NuocXX_ID;
                        Luong3.Text = hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.SoLuongHMD));
                        DVT3.Text = DonViTinh.GetName(hmd.DVT_ID);

                        DonGiaNT3.Text = hmd.DonGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.DonGiaNT));
                        TriGiaNT3.Text = hmd.TriGiaKB.ToString("N2");
                        TriGiaTT3.Text = hmd.TriGiaTT.ToString("N0");
                        //if (hmd.ThueTuyetDoi)
                        //{
                        //    ThueSuatXNK3.Text = "";
                        //}
                        //else
                        //{
                        //if (hmd.ThueSuatXNKGiam.Trim() == "")
                        //    ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString("N0");
                        //else
                        //    ThueSuatXNK3.Text = hmd.ThueSuatXNKGiam;
                        //}
                        ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(5));// Khanhhn - 12/10/2012
                        TienThueXNK3.Text = hmd.ThueXNK.ToString("N0");
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            //TriGiaTTGTGT3.Text = TriGiaTTGTGT.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Trim() == "")
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatVATGiam;
                            //TienThueGTGT3.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            //TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam;
                            //TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            //TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam;
                            //TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Length == 0)
                            //    TyLeThuKhac3.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    TyLeThuKhac3.Text = hmd.ThueSuatVATGiam;
                            TriGiaThuKhac3.Text = hmd.ThueGTGT.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(5));
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }
                        //}
                        //if (hmd.MienThue == 1)
                        //{
                        //    TriGiaTT3.Text = "";
                        //    ThueSuatXNK3.Text = "";
                        //    TienThueXNK3.Text = "";
                        //    TyLeThuKhac3.Text = "";
                        //    TriGiaThuKhac3.Text = "";
                        //}

                        tongTriGiaNT += Convert.ToDecimal(Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero));

                        tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK + hmd.TriGiaThuKhac);// +hmd.ThueTTDB + hmd.ThueGTGT;
                    }
                    #endregion
                }
                //CHI TIET DINH KEM
                else
                {
                    #region CHI TIET DINH KEM
                    //TenHang1.Text = "HÀNG HÓA XUẤT";
                    TenHang1.Text = "(Chi tiết theo phụ lục đính kèm)";
                    //TenHang2.Text = "(Chi tiết theo phụ lục đính kèm)";
                    foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                    {
                        //if (!hmd.FOC)
                        //{
                        tongTriGiaNT += Convert.ToDecimal(Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero));
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);
                        tongTriGiaTT += Convert.ToDecimal(hmd.TriGiaTT);
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }

                        //}
                    }
                    //XuatXu1.Text = Nuoc.GetName(this.TKMD.HMDCollection[0].NuocXX_ID);


                    //Khi co phu luc thi khong hien thi :
                    //TriGiaNT1.Text = tongTriGiaNT.ToString("N2");

                    //TriGiaTT1.Text = tongTriGiaTT.ToString("N0");
                    //TienThueXNK1.Text = tongTienThueXNK.ToString("N0");
                    //TriGiaTTGTGT1.Text = tongTriGiaTTGTGT.ToString("N0");
                    //TienThueGTGT1.Text = tongThueGTGT.ToString("N0");
                    //TriGiaThuKhac1.Text = tongTriGiaThuKhac.ToString("N0");

                    lblTongThueXNK.Text = lblTongTriGiaThuKhac.Text = "";
                    #endregion
                }
                // - Linhhtn mark 20100622 - Tổng trị giá NT không được cộng các loại phí
                //tongTriGiaNT += Convert.ToDecimal(this.TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
                lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");

                lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");
                //if (tongThueGTGT > 0)
                //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                //else
                //    lblTongTienThueGTGT.Text = "";
                if (tongTriGiaThuKhac > 0)
                    lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
                else
                    lblTongTriGiaThuKhac.Text = "";
                lblTongThueXNKSo.Text = this.TinhTongThueHMD().ToString("N0");
                //lblTongThueXNKSo.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                string s = Company.KD.BLL.Utils.VNCurrency.ToString((decimal)this.TinhTongThueHMD()).Trim();
                s = s[0].ToString().ToUpper() + s.Substring(1);
                lblTongThueXNKChu.Text = s.Replace("  ", " ");

                if (MienThue1)
                {
                    TriGiaTT1.Text = "";
                    ThueSuatXNK1.Text = "";
                    TienThueXNK1.Text = "";
                    TriGiaTT2.Text = "";
                    ThueSuatXNK2.Text = "";
                    TienThueXNK2.Text = "";
                    TriGiaTT3.Text = "";
                    ThueSuatXNK3.Text = "";
                    TienThueXNK3.Text = "";
                    lblTongThueXNK.Text = "";
                }
                //if (MienThue2)
                //{
                //    TriGiaTTGTGT1.Text = "";
                //    ThueSuatGTGT1.Text = "";
                //    TienThueGTGT1.Text = "";
                //    TriGiaTTGTGT2.Text = "";
                //    ThueSuatGTGT2.Text = "";
                //    TienThueGTGT2.Text = "";
                //    TriGiaTTGTGT3.Text = "";
                //    ThueSuatGTGT3.Text = "";
                //    TienThueGTGT3.Text = "";
                //    lblTongTienThueGTGT.Text = "";
                //}
                if (MienThue1 && MienThue2)
                {
                    lblTongThueXNKSo.Text = lblTongThueXNKChu.Text = "";
                }

                if (this.TKMD.HMDCollection.Count > 3)
                {
                    lblTongThueXNK.Text = lblTongTriGiaThuKhac.Text = "";
                }

                //datlmq update 29072010
                lblDeXuatKhac.Text = "28. Ghi chép khác: " + TKMD.DeXuatKhac;
                if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Length > 0)
                    lblDeXuatKhac.Text += "; " + "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();
                else if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Equals(""))
                    lblDeXuatKhac.Text += "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();

                //Ngay thang nam in to khai
                if (TKMD.NgayTiepNhan == new DateTime(1900, 1, 1))
                    lbl30NgayThang.Text = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
                else
                    lbl30NgayThang.Text = "Ngày " + TKMD.NgayTiepNhan.Day + " tháng " + TKMD.NgayTiepNhan.Month + " năm " + TKMD.NgayTiepNhan.Year;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        public decimal TinhTongThueHMD()
        {
            decimal tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                tong += Convert.ToDecimal(hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB);// + hmd.ThueGTGT
            }
            return tong;
        }
        public void setNhomHang(string tenNhomHang)
        {
            TenHang1.Text = tenNhomHang;
        }

        public void setThongTin(XRControl cell, string thongTin)
        {
            if (cell.Name.Equals("lblDeXuatKhac"))
                cell.Text = "28. Đề xuất khác: " + thongTin;
            else
                cell.Text = thongTin;
        }

        //datlmq update 29072010
        public void setDeXuatKhac(string deXuatKhac)
        {
            lblDeXuatKhac.Text = "28. Đề xuất khác: " + deXuatKhac;
        }

        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            //lblMienThue2.Visible = t;
            //xrTable2.Visible = !t;
            //lblTongThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = lblTongThueXNKChu.Visible = !t;//xrTablesaaa.Visible = lblTongThueXNKChu.Visible = !t;

        }
        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }
        public void setLuong1(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }
        public void setLuong2(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }
        public void setLuong3(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void Luong1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void Luong2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void Luong3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblDeXuatKhac_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtDeXuatKhac.Text = cell.Text;
            lblDeXuatKhac.Text = "Đề xuất khác";
        }

        private void label_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtDeXuatKhac.Text = cell.Text;
        }

        private void lblThamChieu_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        //public void BindData()
        //{
        //    DateTime minDate = new DateTime(1900, 1, 1);            
        //    //BindData of Detail
        //    lblSTT.DataBindings.Add("Text", this.DataSource, "SoThuTuHang");
        //    lblTenHang.DataBindings.Add("Text", this.DataSource, "TenHang");
        //    lblMaSoHH.DataBindings.Add("Text", this.DataSource, "MaHS");
        //    lblXuatXu.DataBindings.Add("Text", this.DataSource, "XuatXu");
        //    lblSoLuong.DataBindings.Add("Text", this.DataSource, "SoLuong");
        //    lblDonViTinh.DataBindings.Add("Text", this.DataSource, "DonViTinh");
        //    lblDonGiaNT.DataBindings.Add("Text", this.DataSource, "DonGiaTT", "{0:N2}");
        //    lblTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGiaTT", "{0:N2}");

        //    //BindData header
        //    lblCangDoHang.Text = TKMD.TenDaiLyTTHQ.ToString();
        //    lblCangXepHang.Text = TKMD.DiaDiemXepHang.ToString();
        //    //lblChiCucHQ.Text
        //    //lblChiCucHQCK.Text
        //    lblDieuKienGiaoHang.Text = DKGH;
        //    lblDongTienThanhToan.Text = DongTienTT;
        //    lblGiayPhep.Text = TKMD.SoGiayPhep;
        //    lblHoaDonThuongMai.Text = TKMD.SoHoaDonThuongMai;
        //    lblHopDong.Tag = TKMD.SoHopDong;
        //    lblLoaiHinh.Text =  TKMD.LoaiVanDon;
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayDangKy.Text = TKMD.NgayDangKy.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGiayPhep.Text = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGui.Text = TKMD.NgayTiepNhan.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanGiayPhep.Text = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanHopDong.Text = TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHoaDon.Text = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHopDong.Text = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayVanTaiDon.Text = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
        //    lblNguoiNK.Text = TKMD.TenChuHang;
        //    lblNguoiUyThac.Text = TKMD.TenDonViUT;
        //    lblNguoiXK.Text = TKMD.TenDonViDoiTac;
        //    lblNuocXK.Text = NuocXK;
        //    lblPhuongThucThanhToan.Text = PTTT_Name;
        //    lblPhuongTienVanTai.Text = PTVT_Name;
        //    //lblThamChieu.Text
        //    lblToKhai.Text = TKMD.SoToKhai.ToString();
        //    lblVanTaiDon.Text = TKMD.LoaiVanDon;
        //    lblTyGiaTinhThue.Text = TKMD.TyGiaTinhThue.ToString();
        //    lblHuongDan.Text = TKMD.HUONGDAN;
        //    string ctu = "";
        //    foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
        //    {
        //        ctu += ct.TenChungTu + ", ";
        //    }
        //    lblChungTu.Text = ctu;

        //}
    }
}
