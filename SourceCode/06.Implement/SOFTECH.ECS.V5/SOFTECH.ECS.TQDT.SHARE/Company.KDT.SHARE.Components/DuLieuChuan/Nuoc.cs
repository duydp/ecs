﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class Nuoc : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_Nuoc ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }

        public static string GetName(string id)
        {
            //string query = string.Format("SELECT Ten FROM t_HaiQuan_Nuoc WHERE [ID] = '{0}'", id.PadRight(3));
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //IDataReader reader = db.ExecuteReader(dbCommand);
            //if (reader.Read())
            //{
            //    return reader["Ten"].ToString();
            //}
            //return string.Empty;

            try
            {
                string query = string.Format("[ID] = '{0}'", id.Trim().PadRight(3));

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["Nuoc"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["Ten"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }

        public static string GetID(string id)
        {
            //string query = string.Format("SELECT ID, Ten FROM t_HaiQuan_Nuoc WHERE [ID] = '{0}'", id.PadRight(3));
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //IDataReader reader = db.ExecuteReader(dbCommand);
            //if (reader.Read())
            //{
            //    return reader["ID"].ToString();
            //}
            //return string.Empty;

            try
            {
                string query = string.Format("[ID] = '{0}'", id.PadRight(3));

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["Nuoc"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["ID"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(id, ex); }

            return string.Empty;
        }

        public static void Update(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_Nuoc VALUES(@Id,@Ten,@DateCreated,@DateModified)";
            string update = "UPDATE t_HaiQuan_Nuoc SET Ten = @Ten, DateModified = @DateModified WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_Nuoc WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
    }
}