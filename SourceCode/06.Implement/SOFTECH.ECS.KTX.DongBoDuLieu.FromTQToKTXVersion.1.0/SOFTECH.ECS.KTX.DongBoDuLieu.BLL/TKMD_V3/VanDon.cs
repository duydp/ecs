﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Data;
namespace DongBoDuLieu.BLL.TKMD_V3
{
    public class ChungTuVanDon
    {
         public static long InsertUpdatevd_SXXK(long IDToKhai, DataRow drvd, SqlTransaction transaction, SqlDatabase db)
        {
            return InsertUpdatevd_SXXK(IDToKhai, drvd, null, transaction, db);
        }
        public static long InsertUpdatevd_SXXK(long IDToKhai, DataRow drvd,DataRow[] drContainer, SqlTransaction transaction, SqlDatabase db)
        {
            VanDon vd = ConvertTovd(drvd,drContainer);
            List<VanDon> listvd = VanDon.SelectCollectionDynamic(" TKMD_ID = " + IDToKhai,null,transaction,db);
            if (listvd != null && listvd.Count > 0)
            {
                return 0;
            }
            else
            {
                vd.TKMD_ID = IDToKhai;
                vd.InsertFullTrasactionV3(transaction, db);
                return 1;
            }
         
        }
        private static VanDon ConvertTovd(DataRow drvd, DataRow[] drContainer)
        {
            VanDon vd = new VanDon();
            vd.SoVanDon = string.IsNullOrEmpty(drvd["So_CT"].ToString()) ? "." : drvd["So_CT"].ToString();
            vd.NgayVanDon = Convert.ToDateTime(drvd["Ngay_CT"]);
            if (vd.NgayVanDon.Year == 1753)
                vd.NgayVanDon = new DateTime(1900, 1, 1);
            vd.MaNguoiNhanHang = drvd["MaNguoiNhan"].ToString();
            vd.TenNguoiNhanHang = drvd["TenNguoiNhan"].ToString();
            vd.MaNguoiGiaoHang = drvd["MaNguoiGiao"].ToString();
            vd.TenNguoiGiaoHang = drvd["TenNguoiGiao"].ToString();
            vd.DKGH_ID = drvd["Ma_GH"].ToString();
            vd.MaNguoiNhanHangTrungGian = drvd["MaNguoiNhanTG"].ToString();
            vd.TenNguoiNhanHangTrungGian = drvd["TenNguoiNhanTG"].ToString();
            vd.DiaDiemGiaoHang = drvd["DiaDiem"].ToString();
            vd.TenPTVT = drvd["Ten_PTVT"].ToString();
            vd.SoHieuPTVT = drvd["SoHieuPTVT"].ToString();
            vd.SoHieuChuyenDi = drvd["SoHieuChuyenDi"].ToString();
            vd.QuocTichPTVT = drvd["Nuoc_PTVT"].ToString();
            vd.MaCangDoHang = drvd["MaCangDo"].ToString();


            vd.TenCangDoHang = drvd["TenCangDo"].ToString();
            if (string.IsNullOrEmpty(drvd["NgayDen"].ToString()))
                vd.NgayDenPTVT = vd.NgayVanDon.AddDays(10);
            else
                vd.NgayDenPTVT = Convert.ToDateTime(drvd["NgayDen"]);
            if (vd.NgayDenPTVT.Year == 1753)
                vd.NgayDenPTVT = new DateTime(1900, 1, 1);
            vd.MaCangXepHang = drvd["MaCangXep"].ToString();
            vd.TenCangXepHang = drvd["TenCangXep"].ToString();
            vd.MaHangVT = drvd["MaHangVT"].ToString();
            vd.TenHangVT = drvd["TenHangVT"].ToString();
            vd.NuocXuat_ID= drvd["Nuoc_XN"].ToString();
            vd.NoiDi = drvd["NoiDi"].ToString();
            if (string.IsNullOrEmpty(drvd["NgayDi"].ToString()))
                vd.NgayKhoiHanh = vd.NgayVanDon;
            else
                vd.NgayKhoiHanh = Convert.ToDateTime(drvd["NgayDi"]);
            if (vd.NgayKhoiHanh.Year == 1753)
                vd.NgayKhoiHanh = new DateTime(1900, 1, 1);
            vd.ContainerCollection = ConvertToContainer(drContainer);
            return vd;
        }
        private static List<Container> ConvertToContainer(DataRow[] drContainer)
        {
            List<Container> listContainer = new List<Container>();
            if (drContainer != null)
            {
                foreach (DataRow item in drContainer)
                {
                    Container cont = new Container();
                    cont.SoHieu = item["SoHieuCont"].ToString();
                    cont.LoaiContainer = item["LoaiCont"].ToString();
                    cont.Seal_No = item["SoSeal"].ToString();
                    cont.Trang_thai = System.Convert.ToInt32(item["TrangThaiCont"]);
                    listContainer.Add(cont);
                }
            }
            return listContainer;
        }
    }
}
