using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.SXXK;
using Janus.Windows.GridEX;
using Company.KD.BLL;
using Company.Interface.Report.SXXK;

namespace Company.Interface.SXXK
{
    public partial class NguyenPhuLieuRegistedForm : BaseForm
    {
        public HangHoaNhap NguyenPhuLieuSelected = new HangHoaNhap();
        public HangHoaNhapCollection NPLCollection = new HangHoaNhapCollection();
        private DataSet dsRegistedList;
        public bool isBrower=false;
        public NguyenPhuLieuRegistedForm()
        {
            InitializeComponent();
            
        }

        public void BindData()
        {
            string where = string.Format("maDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
            this.NPLCollection = new HangHoaNhap().SelectCollectionDynamic(where,"");
            dgList.DataSource = this.NPLCollection;
        }

        //-----------------------------------------------------------------------------------------
        
        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                      
        }

        //-----------------------------------------------------------------------------------------
        
        private void NguyenPhuLieuRegistedForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            // Nguyên phụ liệu đã đăng ký.
            this.BindData();
            //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.CapNhatDuLieu)))
            //{
                //dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                //uiButton1.Visible = false;
                //uiButton3.Visible = false;
            //}      

        }

        //-----------------------------------------------------------------------------------------
        
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------
        
        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {
           
        }

        //-----------------------------------------------------------------------------------------
        
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string maNPL = e.Row.Cells["Ma"].Text;
                    this.NguyenPhuLieuSelected.Ma = maNPL;
                    this.NguyenPhuLieuSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.NguyenPhuLieuSelected.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.NguyenPhuLieuSelected.Ten = e.Row.Cells["Ten"].Text;
                    this.NguyenPhuLieuSelected.MaHS = e.Row.Cells["MaHS"].Text;
                    this.NguyenPhuLieuSelected.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                    this.NguyenPhuLieuSelected.Load();
                    this.Hide();
                }
            }
            else
            {
                NguyenPhuLieuChangeForm f = new NguyenPhuLieuChangeForm();
                f.HangNhap = new HangHoaNhap();
                f.HangNhap.Ma = e.Row.Cells["Ma"].Text;
                f.HangNhap.Ten = e.Row.Cells["Ten"].Text;
                f.HangNhap.MaHS = e.Row.Cells["MaHS"].Text;
                f.HangNhap.DVT_ID = e.Row.Cells["DVT_ID"].Value.ToString();
                f.ShowDialog(this);
                this.BindData();
            }
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            //ImportNPLForm f = new ImportNPLForm();
            //f.NPLCollection = this.NPLCollection;
            //f.ShowDialog(this);
            //BindData();
            //dgList.Refetch();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            uiButton2.Enabled = false;
            this.BindData();
            dgList.Refetch();
            uiButton2.Enabled = true;
            this.Cursor = Cursors.Default;
            
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            NguyenPhuLieuEditForm f = new NguyenPhuLieuEditForm();
            f.ShowDialog(this);
            this.BindData();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MLMessages("Bạn có muốn xóa các thông tin đã chọn không?","MSG_CAN01","", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangHoaNhap hmd = (HangHoaNhap)i.GetRow().DataRow;
                        //if (hmd.CheckTKMDUserMa(GlobalSettings.MA_DON_VI))
                        //{
                        //    ShowMessage("Mặt hàng này đang được sử dụng để khai tờ khai nên không xóa được.", false);
                        //    e.Cancel = true;
                        //    return;
                        //}
                        //else
                            hmd.Delete();
                    }
                }                
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            //SelectNPLForm f = new SelectNPLForm();
            //f.ShowDialog(this);
            //ReportNPL f2 = new ReportNPL();
            //if (f.NPLCollectionSelect.Count > 0)
            //{
            //    f2.NPLCollection = f.NPLCollectionSelect;
            //    f2.BindReportDinhMucDaDangKy();
            //    f2.ShowPreview();
            //}
        }

        //-----------------------------------------------------------------------------------------
    }
}