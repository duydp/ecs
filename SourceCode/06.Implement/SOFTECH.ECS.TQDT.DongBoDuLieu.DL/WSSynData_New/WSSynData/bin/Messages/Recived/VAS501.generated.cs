using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAS501 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute AB { get; set; }
public PropertiesAttribute KA { get; set; }
public PropertiesAttribute AD { get; set; }
public PropertiesAttribute AE { get; set; }
public PropertiesAttribute KB { get; set; }
public PropertiesAttribute ED { get; set; }
public PropertiesAttribute AF { get; set; }
public PropertiesAttribute AG { get; set; }
public PropertiesAttribute AH { get; set; }
public PropertiesAttribute AI { get; set; }
public PropertiesAttribute AL { get; set; }
public PropertiesAttribute AM { get; set; }
public PropertiesAttribute AN { get; set; }
public PropertiesAttribute AO { get; set; }
public PropertiesAttribute AP { get; set; }
public PropertiesAttribute AQ { get; set; }
public PropertiesAttribute AR { get; set; }
public PropertiesAttribute AS { get; set; }
public PropertiesAttribute AT { get; set; }
public PropertiesAttribute AU { get; set; }
public PropertiesAttribute AV { get; set; }
public PropertiesAttribute AW { get; set; }
public PropertiesAttribute AX { get; set; }
public PropertiesAttribute AY { get; set; }
public PropertiesAttribute AZ { get; set; }
public PropertiesAttribute BA { get; set; }
public PropertiesAttribute ZJ { get; set; }
public PropertiesAttribute BC { get; set; }
public PropertiesAttribute BD { get; set; }
public PropertiesAttribute BE { get; set; }
public PropertiesAttribute BF { get; set; }
public PropertiesAttribute BG { get; set; }
public PropertiesAttribute BH { get; set; }
public PropertiesAttribute FA { get; set; }
public PropertiesAttribute FB { get; set; }
public PropertiesAttribute BI { get; set; }
public PropertiesAttribute BJ { get; set; }
public PropertiesAttribute BK { get; set; }
public GroupAttribute DG { get; set; }
public PropertiesAttribute CW { get; set; }
public PropertiesAttribute CX { get; set; }
public PropertiesAttribute CY { get; set; }
public PropertiesAttribute CZ { get; set; }
public GroupAttribute Z01 { get; set; }

public VAS501()
        {
AB = new PropertiesAttribute(144, typeof(string));
KA = new PropertiesAttribute(2, typeof(string));
AD = new PropertiesAttribute(10, typeof(string));
AE = new PropertiesAttribute(12, typeof(int));
KB = new PropertiesAttribute(1, typeof(string));
ED = new PropertiesAttribute(1, typeof(string));
AF = new PropertiesAttribute(8, typeof(DateTime));
AG = new PropertiesAttribute(5, typeof(string));
AH = new PropertiesAttribute(50, typeof(string));
AI = new PropertiesAttribute(300, typeof(string));
AL = new PropertiesAttribute(13, typeof(string));
AM = new PropertiesAttribute(300, typeof(string));
AN = new PropertiesAttribute(300, typeof(string));
AO = new PropertiesAttribute(11, typeof(string));
AP = new PropertiesAttribute(8, typeof(DateTime));
AQ = new PropertiesAttribute(8, typeof(DateTime));
AR = new PropertiesAttribute(2, typeof(string));
AS = new PropertiesAttribute(12, typeof(string));
AT = new PropertiesAttribute(3, typeof(string));
AU = new PropertiesAttribute(210, typeof(string));
AV = new PropertiesAttribute(2, typeof(string));
AW = new PropertiesAttribute(210, typeof(string));
AX = new PropertiesAttribute(7, typeof(string));
AY = new PropertiesAttribute(6, typeof(string));
AZ = new PropertiesAttribute(6, typeof(string));
BA = new PropertiesAttribute(1, typeof(string));
ZJ = new PropertiesAttribute(105, typeof(string));
BC = new PropertiesAttribute(7, typeof(string));
BD = new PropertiesAttribute(6, typeof(string));
BE = new PropertiesAttribute(6, typeof(string));
BF = new PropertiesAttribute(1, typeof(string));
BG = new PropertiesAttribute(105, typeof(string));
BH = new PropertiesAttribute(35, typeof(string));
FA = new PropertiesAttribute(1, typeof(string));
FB = new PropertiesAttribute(11, typeof(int));
BI = new PropertiesAttribute(1, typeof(int));
BJ = new PropertiesAttribute(3, typeof(int));
BK = new PropertiesAttribute(765, typeof(string));
CW = new PropertiesAttribute(8, typeof(DateTime));
CX = new PropertiesAttribute(2, typeof(int));
CY = new PropertiesAttribute(8, typeof(DateTime));
CZ = new PropertiesAttribute(2, typeof(int));
#region DG
List<PropertiesAttribute> listDG = new List<PropertiesAttribute>();
listDG.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAS501_DG1, typeof(string)));
listDG.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAS501_BP1, typeof(string)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS501_BQ1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(210, 5, EnumGroupID.VAS501_BT1, typeof(string)));
listDG.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAS501_BV1, typeof(int)));
listDG.Add(new PropertiesAttribute(140, 5, EnumGroupID.VAS501_BU1, typeof(string)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS501_BR1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAS501_BS1, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS501_BW1, typeof(string)));
listDG.Add(new PropertiesAttribute(7, 5, EnumGroupID.VAS501_BX1, typeof(string)));
listDG.Add(new PropertiesAttribute(6, 5, EnumGroupID.VAS501_BY1, typeof(string)));
listDG.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAS501_BZ1, typeof(string)));
listDG.Add(new PropertiesAttribute(6, 5, EnumGroupID.VAS501_CA1, typeof(string)));
listDG.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAS501_CB1, typeof(string)));
listDG.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAS501_CC1, typeof(string)));
listDG.Add(new PropertiesAttribute(38, 5, EnumGroupID.VAS501_CD1, typeof(string)));
listDG.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAS501_CE1, typeof(string)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS501_CF1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(13, 5, EnumGroupID.VAS501_YA1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS501_YB1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS501_YC1, typeof(string)));
listDG.Add(new PropertiesAttribute(13, 5, EnumGroupID.VAS501_WA1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS501_WB1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS501_WC1, typeof(string)));
listDG.Add(new PropertiesAttribute(13, 5, EnumGroupID.VAS501_WD1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS501_WE1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS501_WF1, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS501_OA1, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS501_OA2, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS501_OA3, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS501_OA4, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS501_OA5, typeof(string)));
listDG.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAS501_CM1, typeof(string)));
listDG.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAS501_CN1, typeof(int)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS501_CO1, typeof(int)));
listDG.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAS501_CP1, typeof(string)));
listDG.Add(new PropertiesAttribute(10, 5, EnumGroupID.VAS501_CQ1, typeof(int)));
listDG.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAS501_CR1, typeof(string)));
listDG.Add(new PropertiesAttribute(10, 5, EnumGroupID.VAS501_CS1, typeof(int)));
listDG.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAS501_CT1, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS501_RA1, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS501_RA2, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS501_RA3, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS501_RA4, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS501_RA5, typeof(string)));
listDG.Add(new PropertiesAttribute(11, 5, EnumGroupID.VAS501_BM1, typeof(string)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS501_BN1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS501_BO1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(765, 5, EnumGroupID.VAS501_CV1, typeof(string)));
DG = new GroupAttribute("DG", 5, listDG);
#endregion DG
#region Z01
List<PropertiesAttribute> listZ01 = new List<PropertiesAttribute>();
listZ01.Add(new PropertiesAttribute(12, 50, EnumGroupID.VAS501_Z01, typeof(int)));
Z01 = new GroupAttribute("Z01", 50, listZ01);
#endregion Z01
TongSoByte = 20502;
}

}public partial class EnumGroupID
    {
public static readonly string VAS501_DG1 = "VAS501_DG1";
public static readonly string VAS501_BP1 = "VAS501_BP1";
public static readonly string VAS501_BQ1 = "VAS501_BQ1";
public static readonly string VAS501_BT1 = "VAS501_BT1";
public static readonly string VAS501_BV1 = "VAS501_BV1";
public static readonly string VAS501_BU1 = "VAS501_BU1";
public static readonly string VAS501_BR1 = "VAS501_BR1";
public static readonly string VAS501_BS1 = "VAS501_BS1";
public static readonly string VAS501_BW1 = "VAS501_BW1";
public static readonly string VAS501_BX1 = "VAS501_BX1";
public static readonly string VAS501_BY1 = "VAS501_BY1";
public static readonly string VAS501_BZ1 = "VAS501_BZ1";
public static readonly string VAS501_CA1 = "VAS501_CA1";
public static readonly string VAS501_CB1 = "VAS501_CB1";
public static readonly string VAS501_CC1 = "VAS501_CC1";
public static readonly string VAS501_CD1 = "VAS501_CD1";
public static readonly string VAS501_CE1 = "VAS501_CE1";
public static readonly string VAS501_CF1 = "VAS501_CF1";
public static readonly string VAS501_YA1 = "VAS501_YA1";
public static readonly string VAS501_YB1 = "VAS501_YB1";
public static readonly string VAS501_YC1 = "VAS501_YC1";
public static readonly string VAS501_WA1 = "VAS501_WA1";
public static readonly string VAS501_WB1 = "VAS501_WB1";
public static readonly string VAS501_WC1 = "VAS501_WC1";
public static readonly string VAS501_WD1 = "VAS501_WD1";
public static readonly string VAS501_WE1 = "VAS501_WE1";
public static readonly string VAS501_WF1 = "VAS501_WF1";
public static readonly string VAS501_OA1 = "VAS501_OA1";
public static readonly string VAS501_OA2 = "VAS501_OA2";
public static readonly string VAS501_OA3 = "VAS501_OA3";
public static readonly string VAS501_OA4 = "VAS501_OA4";
public static readonly string VAS501_OA5 = "VAS501_OA5";
public static readonly string VAS501_CM1 = "VAS501_CM1";
public static readonly string VAS501_CN1 = "VAS501_CN1";
public static readonly string VAS501_CO1 = "VAS501_CO1";
public static readonly string VAS501_CP1 = "VAS501_CP1";
public static readonly string VAS501_CQ1 = "VAS501_CQ1";
public static readonly string VAS501_CR1 = "VAS501_CR1";
public static readonly string VAS501_CS1 = "VAS501_CS1";
public static readonly string VAS501_CT1 = "VAS501_CT1";
public static readonly string VAS501_RA1 = "VAS501_RA1";
public static readonly string VAS501_RA2 = "VAS501_RA2";
public static readonly string VAS501_RA3 = "VAS501_RA3";
public static readonly string VAS501_RA4 = "VAS501_RA4";
public static readonly string VAS501_RA5 = "VAS501_RA5";
public static readonly string VAS501_BM1 = "VAS501_BM1";
public static readonly string VAS501_BN1 = "VAS501_BN1";
public static readonly string VAS501_BO1 = "VAS501_BO1";
public static readonly string VAS501_CV1 = "VAS501_CV1";
public static readonly string VAS501_Z01 = "VAS501_Z01";
}

}
