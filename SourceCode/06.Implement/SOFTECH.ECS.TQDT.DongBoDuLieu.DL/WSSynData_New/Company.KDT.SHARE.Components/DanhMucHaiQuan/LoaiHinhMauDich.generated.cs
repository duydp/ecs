using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
	public partial class LoaiHinhMauDich : ICloneable
	{
		#region Properties.
		
		public string ID { set; get; }
		public string Ten { set; get; }
		public string Ten_VT { set; get; }
		public DateTime DateCreated { set; get; }
		public DateTime DateModified { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<LoaiHinhMauDich> ConvertToCollection(IDataReader reader)
		{
			IList<LoaiHinhMauDich> collection = new List<LoaiHinhMauDich>();
			while (reader.Read())
			{
				LoaiHinhMauDich entity = new LoaiHinhMauDich();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetString(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten_VT"))) entity.Ten_VT = reader.GetString(reader.GetOrdinal("Ten_VT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateCreated"))) entity.DateCreated = reader.GetDateTime(reader.GetOrdinal("DateCreated"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateModified"))) entity.DateModified = reader.GetDateTime(reader.GetOrdinal("DateModified"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<LoaiHinhMauDich> collection, string id)
        {
            foreach (LoaiHinhMauDich item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_LoaiHinhMauDich VALUES(@ID, @Ten, @Ten_VT, @DateCreated, @DateModified)";
            string update = "UPDATE t_HaiQuan_LoaiHinhMauDich SET Ten = @Ten, Ten_VT = @Ten_VT, DateCreated = @DateCreated, DateModified = @DateModified WHERE ID = @ID";
            string delete = "DELETE FROM t_HaiQuan_LoaiHinhMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Char, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten_VT", SqlDbType.VarChar, "Ten_VT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Char, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten_VT", SqlDbType.VarChar, "Ten_VT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Char, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_HaiQuan_LoaiHinhMauDich VALUES(@ID, @Ten, @Ten_VT, @DateCreated, @DateModified)";
            string update = "UPDATE t_HaiQuan_LoaiHinhMauDich SET Ten = @Ten, Ten_VT = @Ten_VT, DateCreated = @DateCreated, DateModified = @DateModified WHERE ID = @ID";
            string delete = "DELETE FROM t_HaiQuan_LoaiHinhMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Char, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten_VT", SqlDbType.VarChar, "Ten_VT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Char, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten_VT", SqlDbType.VarChar, "Ten_VT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Char, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static LoaiHinhMauDich Load(string id)
		{
			const string spName = "[dbo].[p_HaiQuan_LoaiHinhMauDich_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Char, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<LoaiHinhMauDich> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<LoaiHinhMauDich> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<LoaiHinhMauDich> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_HaiQuan_LoaiHinhMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HaiQuan_LoaiHinhMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_HaiQuan_LoaiHinhMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HaiQuan_LoaiHinhMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertLoaiHinhMauDich(string ten, string ten_VT, DateTime dateCreated, DateTime dateModified)
		{
			LoaiHinhMauDich entity = new LoaiHinhMauDich();	
			entity.Ten = ten;
			entity.Ten_VT = ten_VT;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_HaiQuan_LoaiHinhMauDich_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Char, ID);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@Ten_VT", SqlDbType.VarChar, Ten_VT);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<LoaiHinhMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LoaiHinhMauDich item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateLoaiHinhMauDich(string id, string ten, string ten_VT, DateTime dateCreated, DateTime dateModified)
		{
			LoaiHinhMauDich entity = new LoaiHinhMauDich();			
			entity.ID = id;
			entity.Ten = ten;
			entity.Ten_VT = ten_VT;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_HaiQuan_LoaiHinhMauDich_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Char, ID);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@Ten_VT", SqlDbType.VarChar, Ten_VT);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<LoaiHinhMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LoaiHinhMauDich item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateLoaiHinhMauDich(string id, string ten, string ten_VT, DateTime dateCreated, DateTime dateModified)
		{
			LoaiHinhMauDich entity = new LoaiHinhMauDich();			
			entity.ID = id;
			entity.Ten = ten;
			entity.Ten_VT = ten_VT;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HaiQuan_LoaiHinhMauDich_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Char, ID);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@Ten_VT", SqlDbType.VarChar, Ten_VT);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<LoaiHinhMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LoaiHinhMauDich item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteLoaiHinhMauDich(string id)
		{
			LoaiHinhMauDich entity = new LoaiHinhMauDich();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HaiQuan_LoaiHinhMauDich_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Char, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_HaiQuan_LoaiHinhMauDich_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<LoaiHinhMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LoaiHinhMauDich item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}