﻿// Assembly Naccs.Core.XmlSerializers, Version 1.0.0.0

[assembly: System.Reflection.AssemblyVersion("1.0.0.0")]
[assembly: System.Security.SecurityTransparent]
[assembly: System.Xml.Serialization.XmlSerializerVersion(ParentAssemblyId="09a9374f-a002-48ae-91c4-926738a47027,", Version="2.0.0.0")]
[assembly: System.Security.AllowPartiallyTrustedCallers]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]

