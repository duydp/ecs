﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.WareHouse
{
	public partial class T_KHOKETOAN_NGUYENPHULIEU_MAP : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long KHONPL_ID { set; get; }
		public long HOPDONG_ID { set; get; }
		public string MANPL { set; get; }
		public string TENNPL { set; get; }
		public string DVT { set; get; }
		public string MANPLMAP { set; get; }
		public string TENNPLMAP { set; get; }
		public string DVTMAP { set; get; }
		public decimal TYLEQD { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KHOKETOAN_NGUYENPHULIEU_MAP> ConvertToCollection(IDataReader reader)
		{
			List<T_KHOKETOAN_NGUYENPHULIEU_MAP> collection = new List<T_KHOKETOAN_NGUYENPHULIEU_MAP>();
			while (reader.Read())
			{
				T_KHOKETOAN_NGUYENPHULIEU_MAP entity = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("KHONPL_ID"))) entity.KHONPL_ID = reader.GetInt64(reader.GetOrdinal("KHONPL_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HOPDONG_ID"))) entity.HOPDONG_ID = reader.GetInt64(reader.GetOrdinal("HOPDONG_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MANPL"))) entity.MANPL = reader.GetString(reader.GetOrdinal("MANPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENNPL"))) entity.TENNPL = reader.GetString(reader.GetOrdinal("TENNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MANPLMAP"))) entity.MANPLMAP = reader.GetString(reader.GetOrdinal("MANPLMAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENNPLMAP"))) entity.TENNPLMAP = reader.GetString(reader.GetOrdinal("TENNPLMAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTMAP"))) entity.DVTMAP = reader.GetString(reader.GetOrdinal("DVTMAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TYLEQD"))) entity.TYLEQD = reader.GetDecimal(reader.GetOrdinal("TYLEQD"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KHOKETOAN_NGUYENPHULIEU_MAP> collection, long id)
        {
            foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KHOKETOAN_NGUYENPHULIEU_MAP VALUES(@KHONPL_ID, @HOPDONG_ID, @MANPL, @TENNPL, @DVT, @MANPLMAP, @TENNPLMAP, @DVTMAP, @TYLEQD)";
            string update = "UPDATE T_KHOKETOAN_NGUYENPHULIEU_MAP SET KHONPL_ID = @KHONPL_ID, HOPDONG_ID = @HOPDONG_ID, MANPL = @MANPL, TENNPL = @TENNPL, DVT = @DVT, MANPLMAP = @MANPLMAP, TENNPLMAP = @TENNPLMAP, DVTMAP = @DVTMAP, TYLEQD = @TYLEQD WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_NGUYENPHULIEU_MAP WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KHONPL_ID", SqlDbType.BigInt, "KHONPL_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPLMAP", SqlDbType.NVarChar, "MANPLMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPLMAP", SqlDbType.NVarChar, "TENNPLMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTMAP", SqlDbType.VarChar, "DVTMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYLEQD", SqlDbType.Decimal, "TYLEQD", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KHONPL_ID", SqlDbType.BigInt, "KHONPL_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPLMAP", SqlDbType.NVarChar, "MANPLMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPLMAP", SqlDbType.NVarChar, "TENNPLMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTMAP", SqlDbType.VarChar, "DVTMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYLEQD", SqlDbType.Decimal, "TYLEQD", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KHOKETOAN_NGUYENPHULIEU_MAP VALUES(@KHONPL_ID, @HOPDONG_ID, @MANPL, @TENNPL, @DVT, @MANPLMAP, @TENNPLMAP, @DVTMAP, @TYLEQD)";
            string update = "UPDATE T_KHOKETOAN_NGUYENPHULIEU_MAP SET KHONPL_ID = @KHONPL_ID, HOPDONG_ID = @HOPDONG_ID, MANPL = @MANPL, TENNPL = @TENNPL, DVT = @DVT, MANPLMAP = @MANPLMAP, TENNPLMAP = @TENNPLMAP, DVTMAP = @DVTMAP, TYLEQD = @TYLEQD WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_NGUYENPHULIEU_MAP WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KHONPL_ID", SqlDbType.BigInt, "KHONPL_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPLMAP", SqlDbType.NVarChar, "MANPLMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPLMAP", SqlDbType.NVarChar, "TENNPLMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTMAP", SqlDbType.VarChar, "DVTMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYLEQD", SqlDbType.Decimal, "TYLEQD", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KHONPL_ID", SqlDbType.BigInt, "KHONPL_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPLMAP", SqlDbType.NVarChar, "MANPLMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPLMAP", SqlDbType.NVarChar, "TENNPLMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTMAP", SqlDbType.VarChar, "DVTMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYLEQD", SqlDbType.Decimal, "TYLEQD", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KHOKETOAN_NGUYENPHULIEU_MAP Load(long id)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KHOKETOAN_NGUYENPHULIEU_MAP> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KHOKETOAN_NGUYENPHULIEU_MAP> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KHOKETOAN_NGUYENPHULIEU_MAP> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<T_KHOKETOAN_NGUYENPHULIEU_MAP> SelectCollectionBy_KHONPL_ID(long kHONPL_ID)
		{
            IDataReader reader = SelectReaderBy_KHONPL_ID(kHONPL_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_KHONPL_ID(long kHONPL_ID)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectBy_KHONPL_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@KHONPL_ID", SqlDbType.BigInt, kHONPL_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_KHONPL_ID(long kHONPL_ID)
		{
			const string spName = "p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectBy_KHONPL_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@KHONPL_ID", SqlDbType.BigInt, kHONPL_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KHOKETOAN_NGUYENPHULIEU_MAP(long kHONPL_ID, long hOPDONG_ID, string mANPL, string tENNPL, string dVT, string mANPLMAP, string tENNPLMAP, string dVTMAP, decimal tYLEQD)
		{
			T_KHOKETOAN_NGUYENPHULIEU_MAP entity = new T_KHOKETOAN_NGUYENPHULIEU_MAP();	
			entity.KHONPL_ID = kHONPL_ID;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVT = dVT;
			entity.MANPLMAP = mANPLMAP;
			entity.TENNPLMAP = tENNPLMAP;
			entity.DVTMAP = dVTMAP;
			entity.TYLEQD = tYLEQD;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@KHONPL_ID", SqlDbType.BigInt, KHONPL_ID);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.NVarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.VarChar, DVT);
			db.AddInParameter(dbCommand, "@MANPLMAP", SqlDbType.NVarChar, MANPLMAP);
			db.AddInParameter(dbCommand, "@TENNPLMAP", SqlDbType.NVarChar, TENNPLMAP);
			db.AddInParameter(dbCommand, "@DVTMAP", SqlDbType.VarChar, DVTMAP);
			db.AddInParameter(dbCommand, "@TYLEQD", SqlDbType.Decimal, TYLEQD);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KHOKETOAN_NGUYENPHULIEU_MAP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KHOKETOAN_NGUYENPHULIEU_MAP(long id, long kHONPL_ID, long hOPDONG_ID, string mANPL, string tENNPL, string dVT, string mANPLMAP, string tENNPLMAP, string dVTMAP, decimal tYLEQD)
		{
			T_KHOKETOAN_NGUYENPHULIEU_MAP entity = new T_KHOKETOAN_NGUYENPHULIEU_MAP();			
			entity.ID = id;
			entity.KHONPL_ID = kHONPL_ID;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVT = dVT;
			entity.MANPLMAP = mANPLMAP;
			entity.TENNPLMAP = tENNPLMAP;
			entity.DVTMAP = dVTMAP;
			entity.TYLEQD = tYLEQD;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KHOKETOAN_NGUYENPHULIEU_MAP_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@KHONPL_ID", SqlDbType.BigInt, KHONPL_ID);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.NVarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.VarChar, DVT);
			db.AddInParameter(dbCommand, "@MANPLMAP", SqlDbType.NVarChar, MANPLMAP);
			db.AddInParameter(dbCommand, "@TENNPLMAP", SqlDbType.NVarChar, TENNPLMAP);
			db.AddInParameter(dbCommand, "@DVTMAP", SqlDbType.VarChar, DVTMAP);
			db.AddInParameter(dbCommand, "@TYLEQD", SqlDbType.Decimal, TYLEQD);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KHOKETOAN_NGUYENPHULIEU_MAP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KHOKETOAN_NGUYENPHULIEU_MAP(long id, long kHONPL_ID, long hOPDONG_ID, string mANPL, string tENNPL, string dVT, string mANPLMAP, string tENNPLMAP, string dVTMAP, decimal tYLEQD)
		{
			T_KHOKETOAN_NGUYENPHULIEU_MAP entity = new T_KHOKETOAN_NGUYENPHULIEU_MAP();			
			entity.ID = id;
			entity.KHONPL_ID = kHONPL_ID;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVT = dVT;
			entity.MANPLMAP = mANPLMAP;
			entity.TENNPLMAP = tENNPLMAP;
			entity.DVTMAP = dVTMAP;
			entity.TYLEQD = tYLEQD;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@KHONPL_ID", SqlDbType.BigInt, KHONPL_ID);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.NVarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.VarChar, DVT);
			db.AddInParameter(dbCommand, "@MANPLMAP", SqlDbType.NVarChar, MANPLMAP);
			db.AddInParameter(dbCommand, "@TENNPLMAP", SqlDbType.NVarChar, TENNPLMAP);
			db.AddInParameter(dbCommand, "@DVTMAP", SqlDbType.VarChar, DVTMAP);
			db.AddInParameter(dbCommand, "@TYLEQD", SqlDbType.Decimal, TYLEQD);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KHOKETOAN_NGUYENPHULIEU_MAP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KHOKETOAN_NGUYENPHULIEU_MAP(long id)
		{
			T_KHOKETOAN_NGUYENPHULIEU_MAP entity = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_KHONPL_ID(long kHONPL_ID)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteBy_KHONPL_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@KHONPL_ID", SqlDbType.BigInt, kHONPL_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KHOKETOAN_NGUYENPHULIEU_MAP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}