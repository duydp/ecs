﻿using System;
using Company.GC.BLL.GC;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.Data;
using Company.Interface.KDT.GC;
using Company.Interface.Report.GC;
using Company.Interface.Report;
using Janus.Windows.EditControls;
using System.IO;
using System.Diagnostics;
using Company.KDT.SHARE.Components;

namespace Company.Interface.GC
{
    public partial class HopDongRegistedDetailForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public bool IsBrowseForm = false;
        public Company.Interface.GC.ToKhaiMauDichForm tkmdForm;
        public bool boolFlag;
        public bool boolFlagEdit;
        public HopDongRegistedDetailForm()
        {
            InitializeComponent();
        }

        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }

        private void HopDong_Form_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.FormatGrid();
            txtSoHopDong.Text = this.HD.SoHopDong;
            ccNgayKyHD.Text = this.HD.NgayKy.ToShortDateString();
            ccNgayKetThucHD.Text = this.HD.NgayHetHan.ToShortDateString();
            nuocThue.Ma = HD.NuocThue_ID;
            nguyenTeControl1.Ma = HD.NguyenTe_ID;
            txtTenDoiTac.Text = HD.DonViDoiTac;
            txtDiaChi.Text = HD.DiaChiDoiTac;
            if (this.HD.NgayGiaHan.Year != 1900)
                ccNgayGiaHan.Text = this.HD.NgayGiaHan.ToShortDateString(); ;
            RefreshStatus();
            System.Drawing.Printing.PageSettings ps = new System.Drawing.Printing.PageSettings();
            ps.Landscape = true;
            gridEXPrintNPL.DefaultPageSettings = gridEXPrintSP.DefaultPageSettings = ps;

            tabHopDong.SelectedIndex = 0;
            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            dgNguyenPhuLieu.DataSource = npl.SelectCollectionDynamic1(this.HD.ID);
            BindThongTinHopDong();

            //Company.KDT.SHARE.Components.DoanhNghiepCfgs.Load(@"ConfigDoanhNghiep\\ConfigDoanhNghiep.xml");
            //Company.KDT.SHARE.Components.Config cfg = Company.KDT.SHARE.Components.DoanhNghiepCfgs.Install.Find(cf => cf.Key == "Express");
            //if (cfg == null || cfg.Value != "yJSXAArLTPqZ1q1U5L0WOw==")
            //{

            //    if (cfg == null)
            //    {
            //        Company.KDT.SHARE.Components.DoanhNghiepCfgs.Install.Add(new Company.KDT.SHARE.Components.Config { Key = "Express", Value = "true", Note = "Phiên bản express" });
            //        Company.KDT.SHARE.Components.DoanhNghiepCfgs.Install.SaveConfig(@"ConfigDoanhNghiep\\ConfigDoanhNghiep.xml");
            //    }
            //    cmdThanhKhoan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdLayToKhaiNhapSXXK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //}

        }

        private void BindThongTinHopDong()
        {
            DataTable dt = new DataTable("ThongTinHopDong");
            DataColumn[] dcCol = new DataColumn[4];
            dcCol[0] = new DataColumn("DuLieu", typeof(string));
            dcCol[1] = new DataColumn("ChuaKhaiBao", typeof(int));
            dcCol[2] = new DataColumn("ChuaDuyet", typeof(int));
            dcCol[3] = new DataColumn("DaDuyet", typeof(int));
            dt.Columns.AddRange(dcCol);
            DataRow dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số nguyên phụ liệu";
            dr["ChuaKhaiBao"] = 0;
            dr["ChuaDuyet"] = 0;
            dr["DaDuyet"] = this.HD.GetThongTinCoBanHopDong("t_GC_NguyenPhuLieu");
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số sản phẩm";
            dr["ChuaKhaiBao"] = 0;
            dr["ChuaDuyet"] = 0;
            dr["DaDuyet"] = this.HD.GetThongTinCoBanHopDong("t_GC_SanPham"); ;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số thiết bị";
            dr["ChuaKhaiBao"] = 0;
            dr["ChuaDuyet"] = 0;
            dr["DaDuyet"] = this.HD.GetThongTinCoBanHopDong("t_GC_ThietBi"); ;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số sản phẩm có định mức";
            dr["ChuaKhaiBao"] = 0;
            dr["ChuaDuyet"] = 0;
            dr["DaDuyet"] = this.HD.GetTongSoSanPhamCoDinhMuc();
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số tờ khai nhập NPL";
            dr["ChuaKhaiBao"] = this.HD.GetThongTinToKhaiHopDong(-1, "NGC", "N"); ;
            dr["ChuaDuyet"] = this.HD.GetThongTinToKhaiHopDong(0, "NGC", "N"); ;
            dr["DaDuyet"] = this.HD.GetThongTinToKhaiHopDong(1, "NGC", "N");
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số tờ khai chuyển tiếp nhập NPL";
            dr["ChuaKhaiBao"] = this.HD.GetThongTinToKhaiChuyenTiepHopDong(-1, "PHPLN");
            dr["ChuaDuyet"] = this.HD.GetThongTinToKhaiChuyenTiepHopDong(0, "PHPLN");
            dr["DaDuyet"] = this.HD.GetThongTinToKhaiChuyenTiepHopDong(1, "PHPLN");
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số tờ khai tái xuất NPL";
            dr["ChuaKhaiBao"] = this.HD.GetThongTinToKhaiHopDong(-1, "XGC", "N");
            dr["ChuaDuyet"] = this.HD.GetThongTinToKhaiHopDong(0, "XGC", "N");
            dr["DaDuyet"] = this.HD.GetThongTinToKhaiHopDong(1, "XGC", "N");
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số tờ khai chuyển tiếp xuất NPL";
            dr["ChuaKhaiBao"] = this.HD.GetThongTinToKhaiChuyenTiepHopDong(-1, "PHPLX");
            dr["ChuaDuyet"] = this.HD.GetThongTinToKhaiChuyenTiepHopDong(0, "PHPLX");
            dr["DaDuyet"] = this.HD.GetThongTinToKhaiChuyenTiepHopDong(1, "PHPLX");
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số tờ khai xuất sản phẩm";
            dr["ChuaKhaiBao"] = this.HD.GetThongTinToKhaiHopDong(-1, "XGC", "S");
            dr["ChuaDuyet"] = this.HD.GetThongTinToKhaiHopDong(0, "XGC", "S");
            dr["DaDuyet"] = this.HD.GetThongTinToKhaiHopDong(1, "XGC", "S");
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số tờ khai nhập thiết bị";
            dr["ChuaKhaiBao"] = this.HD.GetThongTinToKhaiHopDong(-1, "NGC", "T");
            dr["ChuaDuyet"] = this.HD.GetThongTinToKhaiHopDong(0, "NGC", "T");
            dr["DaDuyet"] = this.HD.GetThongTinToKhaiHopDong(1, "NGC", "T");
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["DuLieu"] = "Tổng số tờ khai xuất thiết bị";
            dr["ChuaKhaiBao"] = this.HD.GetThongTinToKhaiHopDong(-1, "XGC", "T");
            dr["ChuaDuyet"] = this.HD.GetThongTinToKhaiHopDong(0, "XGC", "T");
            dr["DaDuyet"] = this.HD.GetThongTinToKhaiHopDong(1, "XGC", "T");
            dt.Rows.Add(dr);

            dgThongTinHopDong.DataSource = dt;


        }

        private void FormatGrid()
        {
            for (int i = 5; i <= 12; i++)
            {
                dgNguyenPhuLieu.Tables[0].Columns[i].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                dgNguyenPhuLieu.Tables[0].Columns[i].TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            }
            for (int i = 6; i <= 8; i++)
            {
                dgSanPham.Tables[0].Columns[i].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                dgSanPham.Tables[0].Columns[i].TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            }

        }
        private void RefreshStatus()
        {
            if (this.HD.TrangThaiThanhKhoan == 0) lblTrangThaiThanhKhoan.Text = setText("Chưa thanh khoản", "Not liquidated yet");
            else lblTrangThaiThanhKhoan.Text = setText("Đã chạy thanh khoản", "Liquidated");
        }
        private void tabHopDong_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            lblGhiChu.Visible = false;
            btnTinhLuongTon.Visible = false;
            if (e == null)
            {
                return;
            }
            switch (e.Page.Key)
            {
                case "tpNPL":
                    {
                        btnNThem.Visible = false;
                        btnXoa.Visible = false;
                        dgNguyenPhuLieu.DataSource = this.HD.GetNPL();
                    }
                    break;
                case "tpSP":
                    {
                        btnNThem.Visible = false;
                        btnXoa.Visible = false;
                        lblGhiChu.Visible = true;
                        dgSanPham.DataSource = this.HD.GetSP();
                    }
                    break;
                case "tpTB":
                    {
                        btnNThem.Visible = false;
                        btnXoa.Visible = false;
                        dgThietBi.DataSource = this.HD.GetTB();
                    }
                    break;
                case "tpTKNK":
                    {
                        btnNThem.Visible = true;
                        btnXoa.Visible = true;
                        dgTKN.DataSource = this.HD.GetTKNK();
                    }
                    break;
                case "tpTKX":
                    {
                        btnNThem.Visible = true;
                        btnXoa.Visible = true;
                        dgTKX.DataSource = this.HD.GetTKXK();
                    }
                    break;
                case "tpPhuKien":
                    {
                        btnNThem.Visible = true;
                        btnXoa.Visible = true;
                        dgPhuKien.DataSource = this.HD.GetPK();
                    }
                    break;
                case "tpTKCTNhap":
                    {
                        btnNThem.Visible = true;
                        btnXoa.Visible = true;
                        dgTKCT.DataSource = this.HD.GetTKCTNhap();
                    }
                    break;
                case "tpTKCTXuat":
                    {
                        btnNThem.Visible = true;
                        btnXoa.Visible = true;
                        dgTKCTXuat.DataSource = this.HD.GetTKCTXuat();
                    }
                    break;
                case "tpNPLCU":
                    {
                        btnNThem.Visible = false;
                        btnXoa.Visible = false;
                        dgNPLCungUng.DataSource = ViewNPLCungUng();
                    }
                    break;
                case "tpThanhKhoan":
                    {
                        btnNThem.Visible = false;
                        btnXoa.Visible = false;
                        LoadReportTemplate();
                    }
                    break;
                case "tpThanhKhoanMoi":
                    {
                        btnNThem.Visible = false;
                        btnXoa.Visible = false;
                        LoadReportTemplateMoi();
                    }
                    break;
                case "tpThongTinHopDong":
                    {
                        btnNThem.Visible = false;
                        btnXoa.Visible = false;
                        btnTinhLuongTon.Visible = true;
                        BindThongTinHopDong();
                    }
                    break;
            }
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dgNguyenPhuLieu_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //decimal luongNhap = Math.Round(Convert.ToDecimal(e.Row.Cells["SoLuongDaNhap"].Value), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                //decimal luongCungUng = Math.Round(Convert.ToDecimal(e.Row.Cells["SoLuongCungUng"].Value), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                //decimal luongDaDung = Math.Round(Convert.ToDecimal(e.Row.Cells["SoLuongDaDung"].Value), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                //decimal TongNhuCau = Math.Round(Convert.ToDecimal(e.Row.Cells["TongNhuCau"].Value), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                //e.Row.Cells["SoLuongConLai"].Text = (luongNhap + luongCungUng - luongDaDung) + "";
                //e.Row.Cells["LuongTonNhuCau"].Text = (luongNhap + luongCungUng - TongNhuCau) + "";

            }
        }

        private void dgSanPham_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                e.Row.Cells["NhomSanPham_ID"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value.ToString());
                //decimal luongDangKy = Math.Round(Convert.ToDecimal(e.Row.Cells["SoLuongDangKy"].Value), GlobalSettings.SoThapPhan.LuongSP, MidpointRounding.AwayFromZero);
                //decimal luongDaXuat = Math.Round(Convert.ToDecimal(e.Row.Cells["SoLuongDaXuat"].Value), GlobalSettings.SoThapPhan.LuongSP, MidpointRounding.AwayFromZero);
                //e.Row.Cells["SoLuongConLai"].Text = (luongDangKy - luongDaXuat) + "";
                //e.Row.Cells["SoLuongConLai"].Value = (luongDangKy - luongDaXuat);
            }
        }

        private void dgNguyenPhuLieu_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //}
        }

        private void dgThietBi_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                e.Row.Cells["XuatXu"].Text = this.Nuoc_GetName(e.Row.Cells["XuatXu"].Value.ToString());
                e.Row.Cells["NguyenTe"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe"].Value.ToString());
            }

        }

        private void showFukien()
        {

        }

        private void dgPhuKien_LoadingRow(object sender, RowLoadEventArgs e)
        {
            string st = e.Row.Cells[4].Value.ToString();
            if (st == "-1")
                e.Row.Cells[4].Text = setText("Chưa khai báo", "Not declared yet");
            else if (st == "0")
                e.Row.Cells[4].Text = setText("Chưa duyệt", "Pending approval");
            else if (st == "1")
                e.Row.Cells[4].Text = setText("Đã duyệt", "Approved");
        }

        private void dgPhuKien_RowDoubleClick(object sender, RowActionEventArgs e)
        {

        }
        private void XemTKN_NPL()
        {
            GridEXRow row = dgNguyenPhuLieu.GetRow();
            if (row == null) return;
            string maNPL = row.Cells["Ma"].Value.ToString();
            Company.Interface.ToKhaiMauDichRegistedForm f = new Company.Interface.ToKhaiMauDichRegistedForm();
            f.Text = setText("Danh sách tờ khai nhập NPL '" + maNPL + "'", "List of import raw material declaration  '" + maNPL + "'");
            f.dt = this.HD.GetToKhaiNhapNPL(maNPL);
            f.ShowDialog();
        }
        private void XemTKTX_NPL()
        {
            GridEXRow row = dgNguyenPhuLieu.GetRow();
            if (row == null) return;
            string maNPL = row.Cells["Ma"].Value.ToString();
            Company.Interface.ToKhaiMauDichRegistedForm f = new Company.Interface.ToKhaiMauDichRegistedForm();
            f.Text = setText("Danh sách tờ khai tái xuất NPL '" + maNPL + "'", "List of export reproductive raw material declaration '" + maNPL + "'");
            f.dt = this.HD.GetToKhaiTaiXuatNPL(maNPL);
            f.ShowDialog();
        }
        private void XemPKChuaNPL()
        {
            GridEXRow row = dgNguyenPhuLieu.GetRow();
            if (row == null) return;
            if (row.RowType == RowType.Record)
            {
                string maNPL = row.Cells["Ma"].Value.ToString();
                string where = "HopDong_ID = " + HD.ID + " AND TrangThaiXuLy = 1 AND ID IN (SELECT Master_ID FROM t_KDT_GC_LoaiPhuKien WHERE MaPhuKien LIKE 'N%' " +
                                "AND ID IN (SELECT DISTINCT Master_ID FROM t_KDT_GC_HangPhuKien WHERE MaHang = '" + maNPL + "'))";
                PhuKienChuaMaHangForm f = new PhuKienChuaMaHangForm();
                f.HD = this.HD;
                f.PKDKCollection = new PhuKienDangKy().SelectCollectionDynamic(where, "");
                f.ShowDialog();
            }
        }

        private void xemTKN_Click(object sender, EventArgs e)
        {
            XemTKN_NPL();

        }

        private void xemTKTXNPL_Click(object sender, EventArgs e)
        {
            XemTKTX_NPL();
        }

        private void xemPKChuaNPL_Click(object sender, EventArgs e)
        {
            XemPKChuaNPL();
        }

        private void XemTKXSP()
        {
            GridEXRow row = dgSanPham.GetRow();
            if (row == null || row.RowType != RowType.Record) return;
            string maNPL = row.Cells["Ma"].Value.ToString();
            Company.Interface.ToKhaiMauDichRegistedForm f = new Company.Interface.ToKhaiMauDichRegistedForm();
            f.Text = setText("Danh sách tờ khai xuất sản phẩm '" + maNPL + "'", "List of export product declaration'" + maNPL + "'");
            f.dt = this.HD.GetToKhaiXuatSP(maNPL);
            f.ShowDialog();
        }
        private void XemPKSP()
        {
            GridEXRow row = dgSanPham.GetRow();
            if (row == null) return;
            if (row.RowType == RowType.Record)
            {
                string maNPL = row.Cells["Ma"].Value.ToString();
                string where = "HopDong_ID = " + HD.ID + " AND TrangThaiXuLy = 1 AND  ID IN (SELECT Master_ID FROM t_KDT_GC_LoaiPhuKien WHERE MaPhuKien LIKE 'S%' " +
                                "AND ID IN (SELECT DISTINCT Master_ID FROM t_KDT_GC_HangPhuKien WHERE MaHang = '" + maNPL + "'))";
                PhuKienChuaMaHangForm f = new PhuKienChuaMaHangForm();
                f.HD = this.HD;
                f.PKDKCollection = new PhuKienDangKy().SelectCollectionDynamic(where, "");
                f.ShowDialog();
            }
        }
        private void XemTKNTB()
        {
            GridEXRow row = dgSanPham.GetRow();
            if (row == null) return;
            string maNPL = row.Cells["Ma"].Value.ToString();
            Company.Interface.ToKhaiMauDichRegistedForm f = new Company.Interface.ToKhaiMauDichRegistedForm();
            f.Text = setText("Danh sách tờ khai nhập thiết bị'" + maNPL + "'", "List of import equipment declaration'" + maNPL + "'");
            f.dt = this.HD.GetToKhaiNhapTB(maNPL);
            f.ShowDialog();
        }
        private void XemTKXTB()
        {
            GridEXRow row = dgSanPham.GetRow();
            if (row == null) return;
            string maNPL = row.Cells["Ma"].Value.ToString();
            Company.Interface.ToKhaiMauDichRegistedForm f = new Company.Interface.ToKhaiMauDichRegistedForm();
            f.Text = setText("Danh sách tờ khai xuất thiết bị '" + maNPL + "'", "List of export equipment declaration '" + maNPL + "'");
            f.dt = this.HD.GetToKhaiXuatTB(maNPL);
            f.ShowDialog();
        }
        private void XemPKTB()
        {
            GridEXRow row = dgThietBi.GetRow();
            if (row == null) return;
            if (row.RowType == RowType.Record)
            {
                string maNPL = row.Cells["Ma"].Value.ToString();
                string where = "HopDong_ID = " + HD.ID + " AND TrangThaiXuLy = 1 AND ID IN (SELECT Master_ID FROM t_KDT_GC_LoaiPhuKien WHERE MaPhuKien LIKE 'T%' " +
                                "AND ID IN (SELECT DISTINCT Master_ID FROM t_KDT_GC_HangPhuKien WHERE MaHang = '" + maNPL + "'))";
                PhuKienChuaMaHangForm f = new PhuKienChuaMaHangForm();
                f.HD = this.HD;
                f.PKDKCollection = new PhuKienDangKy().SelectCollectionDynamic(where, "");
                f.ShowDialog();
            }
        }
        private void xemTKXSP_Click(object sender, EventArgs e)
        {
            XemTKXSP();
        }

        private void xemPKSP_Click(object sender, EventArgs e)
        {
            XemPKSP();
        }

        private void xemTKNTB_Click(object sender, EventArgs e)
        {
            XemTKNTB();
        }

        private void xemTKXTB_Click(object sender, EventArgs e)
        {
            XemTKXTB();
        }

        private void xemPKTB_Click(object sender, EventArgs e)
        {
            XemPKTB();
        }

        private void xemChiTietPK_Click(object sender, EventArgs e)
        {
            PhuKienDangKy PKDK = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            PhuKienGCDetailForm pkForm = new PhuKienGCDetailForm();
            pkForm.HD = this.HD;
            pkForm.PKDK = PKDK;
            pkForm.ShowDialog();

        }

        private void dgPhuKien_RowDoubleClick_1(object sender, RowActionEventArgs e)
        {
            PhuKienDangKy PKDK = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            PhuKienGCDetailForm pkForm = new PhuKienGCDetailForm();
            pkForm.HD = this.HD;
            pkForm.PKDK = PKDK;
            pkForm.ShowDialog();
        }
        private void ViewTKN(string maLoaiHinh)
        {
            GridEXRow row = dgTKN.GetRow();
            if (row.RowType == RowType.Record)
            {
                if (row.Cells["TrangThaiXuLy"].Value.ToString() != TrangThaiXuLy.DA_DUYET.ToString())
                {

                    Company.Interface.ToKhaiMauDichForm f = new Company.Interface.ToKhaiMauDichForm();
                    f.TKMD = (ToKhaiMauDich)row.DataRow;
                    if (f.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        f.OpenType = OpenFormType.Edit;
                    else
                        f.OpenType = OpenFormType.View;
                    f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                    f.ShowDialog();


                }
                else
                {
                    tkmdForm = new Company.Interface.GC.ToKhaiMauDichForm();
                    tkmdForm.OpenType = OpenFormType.Edit;
                    //tkmdForm.MdiParent = this;
                    tkmdForm.HDGC = this.HD;

                    tkmdForm.TKMD = (ToKhaiMauDich)row.DataRow;
                    tkmdForm.NhomLoaiHinh = tkmdForm.TKMD.MaLoaiHinh.Substring(0, 3);
                    tkmdForm.ShowDialog();

                }
                dgTKN.DataSource = this.HD.GetTKNK();
                dgTKN.Refetch();
            }
        }
        private void ViewTKX(string maLoaiHinh)
        {
            GridEXRow row = dgTKX.GetRow();
            if (row.RowType == RowType.Record)
            {
                if (row.Cells["TrangThaiXuLy"].Value.ToString() != TrangThaiXuLy.DA_DUYET.ToString())
                {

                    Company.Interface.ToKhaiMauDichForm f = new Company.Interface.ToKhaiMauDichForm();
                    f.TKMD = (ToKhaiMauDich)row.DataRow;
                    if (f.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        f.OpenType = OpenFormType.Edit;
                    else
                        f.OpenType = OpenFormType.View;
                    f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                    f.ShowDialog();


                }
                else
                {
                    tkmdForm = new Company.Interface.GC.ToKhaiMauDichForm();
                    tkmdForm.OpenType = OpenFormType.Edit;
                    //tkmdForm.MdiParent = this;
                    tkmdForm.HDGC = this.HD;

                    tkmdForm.TKMD = (ToKhaiMauDich)row.DataRow;
                    tkmdForm.NhomLoaiHinh = tkmdForm.TKMD.MaLoaiHinh.Substring(0, 3);
                    tkmdForm.ShowDialog();
                }
                dgTKX.DataSource = this.HD.GetTKXK();
                dgTKX.Refresh();
            }

        }
        private void xemChiTietTKN_Click(object sender, EventArgs e)
        {
            ViewTKN("NGC");
        }

        private void dgTKN_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ViewTKN("NGC");
        }

        private void xemChiTietTKX_Click(object sender, EventArgs e)
        {
            ViewTKX("XGC");
        }

        private void dgTKX_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ViewTKX("XGC");
        }

        private void XemLuongNPLTKX()
        {
            if (dgTKX.GetRow() == null) return;
            ToKhaiMauDich TKMD = (ToKhaiMauDich)dgTKX.GetRow().DataRow;
            if (TKMD.LoaiHangHoa == "S")
            {
                DataSet ds = TKMD.GetNPLXuatToKhai();
                NPLXuatForm f = new NPLXuatForm();
                f.ds = ds;
                f.ShowDialog();
            }
            else if (TKMD.LoaiHangHoa == "N")
            {
                DataSet ds = HangMauDich.SelectBy_TKMD_ID(TKMD.ID);
                NPLXuatForm f = new NPLXuatForm();
                f.ds = ds;
                f.ShowDialog();
            }
            else
            {
                showMsg("MSG_2702009");
                //ShowMessage("Tờ khai này xuất thiết bị.", false);
            }
        }
        private void xemLuongNPLTKX_Click(object sender, EventArgs e)
        {
            XemLuongNPLTKX();
        }

        private void dgTKCT_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaLoaiHinh"].Text = LoaiPhieuChuyenTiep.GetName(e.Row.Cells["MaLoaiHinh"].Value.ToString()) + string.Format(" ({0})", e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());

            }
            if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 0) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ duyệt", "Pending approval");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value < 0) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chưa khai báo", "Not declared yet");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 1) e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã duyệt", "Approved");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 2) e.Row.Cells["TrangThaiXuLy"].Text = setText("Không phê duyệt", "Not Approve");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 5) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ sửa", "Not declared yet");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 10) e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã hủy", "Not declared yet");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 11) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ hủy", "Not declared yet");
        }
        private void ViewTKCT()
        {
            try
            {
                GridEXRow row = dgTKCT.GetRow();
                if (row == null) return;
                if (row.RowType == RowType.Record)
                {
                    ToKhaiChuyenTiep TKCT = (ToKhaiChuyenTiep)row.DataRow;
                    TKCT.LoadHCTCollection();
                    TKCT.LoadChungTuKem();

                    string nhomLoaiHinh = TKCT.MaLoaiHinh.Substring(0, 1);

                    ToKhaiGCChuyenTiepNhapForm tkct = new ToKhaiGCChuyenTiepNhapForm();
                    tkct.NhomLoaiHinh = nhomLoaiHinh;
                    tkct.Name = "TKCX" + nhomLoaiHinh;
                    tkct.OpenType = OpenFormType.Edit;
                    tkct.TKCT = TKCT;
                    tkct.isByHand = true;
                    tkct.ShowDialog(this);
                    dgTKCT.DataSource = HD.GetTKCTNhap();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi khi mở tờ khai chuyển tiếp.", false);
            }
        }
        private void ViewTKCTXuat()
        {
            try
            {
                GridEXRow row = dgTKCTXuat.GetRow();
                if (row == null) return;
                if (row.RowType == RowType.Record)
                {
                    ToKhaiChuyenTiep TKCT = (ToKhaiChuyenTiep)row.DataRow;
                    //if (TKCT.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                    //{
                    //    ToKhaiGCChuyenTiepNhapForm f = new ToKhaiGCChuyenTiepNhapForm();
                    //    f.OpenType = OpenFormType.Edit;
                    //    f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                    //    f.TKCT = ToKhaiChuyenTiep.Load(f.TKCT.ID);
                    //    f.TKCT.LoadHCTCollection();
                    //    if (f.TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                    //        f.OpenType = OpenFormType.View;
                    //    f.ShowDialog();
                    //}
                    //else
                    //{
                    //    Company.Interface.GC.ToKhaiGCChuyentiepNhapDaduyetForm tkgcCTNhapForm;
                    //    tkgcCTNhapForm = new Company.Interface.GC.ToKhaiGCChuyentiepNhapDaduyetForm();
                    //    tkgcCTNhapForm.NhomLoaiHinh = "X";
                    //    tkgcCTNhapForm.OpenType = OpenFormType.Insert;
                    //    tkgcCTNhapForm.HDGC = this.HD;
                    //    tkgcCTNhapForm.TKCT = TKCT;
                    //    tkgcCTNhapForm.ShowDialog();

                    //}
                    TKCT.LoadHCTCollection();
                    TKCT.LoadChungTuKem();

                    string nhomLoaiHinh = TKCT.MaLoaiHinh.Substring(0, 1);

                    ToKhaiGCChuyenTiepNhapForm tkct = new ToKhaiGCChuyenTiepNhapForm();
                    tkct.NhomLoaiHinh = nhomLoaiHinh;
                    tkct.Name = "TKCX" + nhomLoaiHinh;
                    tkct.OpenType = OpenFormType.Edit;
                    tkct.TKCT = TKCT;
                    tkct.isByHand = true;
                    tkct.ShowDialog(this);

                    dgTKCT.DataSource = HD.GetTKCTXuat();
                    dgTKCT.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi khi mở tờ khai chuyển tiếp.", false);
            }
        }
        private void xemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewTKCT();
        }

        private void dgTKCT_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ViewTKCT();
        }

        private void XemDMSP()
        {
            if (dgSanPham.GetRow() == null || dgSanPham.GetRow().RowType != RowType.Record) return;
            Company.GC.BLL.GC.SanPham sp = ((Company.GC.BLL.GC.SanPham)dgSanPham.GetRow().DataRow);
            string where = "MaSanPham = '" + sp.Ma + "' AND HopDong_ID = " + this.HD.ID;
            DinhMucRegistedDetailForm f = new DinhMucRegistedDetailForm();
            f.DMCollection = new Company.GC.BLL.GC.DinhMuc().SelectCollectionDynamic(where, "");
            f.SP = sp;
            f.ShowDialog();
        }
        private void xemDMItem_Click(object sender, EventArgs e)
        {
            XemDMSP();
        }

        private void xemNPLCungUngItem_Click(object sender, EventArgs e)
        {
            ToKhaiMauDich TKMD = (ToKhaiMauDich)dgTKX.GetRow().DataRow;
            long id = TKMD.ID;
            try
            {
                if (TKMD.GetNPLCungUngTK(id).Tables[0].Rows.Count < 1)
                {
                    showMsg("MSG_0203090");
                    return;
                }

            }
            catch (Exception ex)
            { }
            NPLCungUngForm f = new NPLCungUngForm();

            f.TKMD_ID = id;
            f.ShowDialog();

        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //NPLCungUngTheoTKRegisterForm f = new NPLCungUngTheoTKRegisterForm();
            //NPLCungUngTheoTKSendForm f1 = new NPLCungUngTheoTKSendForm();
            //BKCungUngDangKy BKCU = (BKCungUngDangKy)e.Row.DataRow;
            //BKCU.Load();
            //f.BKCU = BKCU;
            //if (BKCU.TKMD_ID > 0)
            //{
            //    ToKhaiMauDich TKMD = new ToKhaiMauDich();
            //    TKMD.ID = BKCU.TKMD_ID;
            //    TKMD.Load();
            //    f.TKMD = TKMD;
            //    f1.TKMD = TKMD;
            //}
            //else
            //{
            //    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
            //    TKCT.ID = BKCU.TKCT_ID;
            //    TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
            //    f.TKCT = TKCT;
            //    f1.TKCT = TKCT;
            //}
            //if (BKCU.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
            //{
            //    f1.ShowDialog();
            //}
            //else
            //    f.ShowDialog();
            //dgNPLCungUng.DataSource = this.HD.GetNPLCU();
            //dgNPLCungUng.Refetch();
        }
        private void LoadReportTemplate()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Mau", typeof(string));
            dt.Columns.Add("Ten", typeof(string));
            DataRow dr1 = dt.NewRow();
            dr1["Mau"] = "01 HQ-GC";
            dr1["Ten"] = "BẢNG TỔNG HỢP  NGUYÊN LIỆU NHẬP KHẨU (In theo khổ giấy dọc) ";
            dt.Rows.Add(dr1);

            DataRow dr1h = dt.NewRow();
            dr1h["Mau"] = "01H HQ-GC";
            dr1h["Ten"] = "BẢNG TỔNG HỢP  NGUYÊN LIỆU NHẬP KHẨU (In theo khổ giấy ngang) ";
            dt.Rows.Add(dr1h);

            DataRow dr2 = dt.NewRow();
            dr2["Mau"] = "02 HQ-GC";
            dr2["Ten"] = "BẢNG TỔNG HỢP SẢN PHẨM GIA CÔNG XUẤT KHẨU (In theo khổ giấy dọc)";
            dt.Rows.Add(dr2);

            DataRow dr2h = dt.NewRow();
            dr2h["Mau"] = "02H HQ-GC";
            dr2h["Ten"] = "BẢNG TỔNG HỢP SẢN PHẨM GIA CÔNG XUẤT KHẨU (In theo khổ giấy ngang)";
            dt.Rows.Add(dr2h);


            DataRow dr3 = dt.NewRow();
            dr3["Mau"] = "03 HQ-GC";
            dr3["Ten"] = "BẢNG TỔNG HỢP MÁY MÓC, THIẾT BỊ TẠM NHẬP (In theo khổ giấy dọc)";
            dt.Rows.Add(dr3);

            DataRow dr3h = dt.NewRow();
            dr3h["Mau"] = "03H HQ-GC";
            dr3h["Ten"] = "BẢNG TỔNG HỢP MÁY MÓC, THIẾT BỊ TẠM NHẬP (In theo khổ giấy ngang) ";
            dt.Rows.Add(dr3h);


            DataRow dr4 = dt.NewRow();
            dr4["Mau"] = "04 HQ-GC";
            dr4["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU DO BÊN NHẬN GIA CÔNG CUNG ỨNG ";
            dt.Rows.Add(dr4);

            DataRow dr5 = dt.NewRow();
            dr5["Mau"] = "05 HQ-GC";
            dr5["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU ĐÃ SỬ DỤNG ĐỂ SẢN XUẤT THÀNH SẢN PHẨM XUẤT KHẨU ";
            dt.Rows.Add(dr5);

            DataRow dr6 = dt.NewRow();
            dr6["MaU"] = "06 HQ-GC";
            dr6["Ten"] = "BẢNG THANH KHOẢN HỢP ĐỒNG GIA CÔNG ";
            dt.Rows.Add(dr6);

            DataRow dr7 = dt.NewRow();
            dr7["Mau"] = "07 HQ-GC";
            dr7["Ten"] = "PHIẾU LẤY MẪU NGUYÊN LIỆU GIA CÔNG ";
            dt.Rows.Add(dr7);

            DataRow dr8 = dt.NewRow();
            dr8["Mau"] = "08 HQ-GC";
            dr8["Ten"] = "BẢNG THỐNG KÊ TỜ KHAI NHẬP KHẨU ";
            dt.Rows.Add(dr8);

            DataRow dr9 = dt.NewRow();
            dr9["Mau"] = "09 HQ-GC";
            dr9["Ten"] = "BẢNG THỐNG KÊ TỜ KHAI XUẤT KHẨU ";
            dt.Rows.Add(dr9);

            DataRow dr10 = dt.NewRow();
            dr10["Mau"] = "10 HQ-GC";
            dr10["Ten"] = "BẢNG ĐỊNH MỨC VÀ TỶ LỆ HAO HỤT CỦA TỪNG MÃ HÀNG ";
            dt.Rows.Add(dr10);

            DataRow dr11 = dt.NewRow();
            dr11["Mau"] = "11 HQ-GC";
            dr11["Ten"] = "BẢNG KÊ KHAI NGUYÊN LIỆU DO BÊN NHẬN GIA CÔNG CUNG ỨNG TƯƠNG ỨNG VỚI LƯỢNG SẢN PHẨM TRÊN TỜ KHAI XUẤT KHẨU  ";
            dt.Rows.Add(dr11);
            dgThanhKhoan.DataSource = dt;

        }
        private void LoadReportTemplateMoi()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Mau", typeof(string));
            dt.Columns.Add("Ten", typeof(string));
            DataRow dr1 = dt.NewRow();
            dr1["Mau"] = "01/HSTK-GC";
            dr1["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ NHẬP KHẨU";
            dt.Rows.Add(dr1);

            DataRow dr1h = dt.NewRow();
            dr1h["Mau"] = "02/HSTK-GC";
            dr1h["Ten"] = "BẢNG TỔNG HỢP SẢN PHẨM GIA CÔNG XUẤT KHẨU";
            dt.Rows.Add(dr1h);

            DataRow dr2 = dt.NewRow();
            dr2["Mau"] = "03/HSTK-GC";
            dr2["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ XUẤT TRẢ RA NƯỚC NGOÀI VÀ CHUYỂN SANG " + "\n\r" +
                         "HỢP ĐỒNG GIA CÔNG KHÁC TRONG KHI ĐANG THỰC HIỆN HỢP ĐỒNG GIA CÔNG ";
            dt.Rows.Add(dr2);

            DataRow dr2h = dt.NewRow();
            dr2h["Mau"] = "04/HSTK-GC";
            dr2h["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ DO BÊN NHẬN GIA CÔNG CUNG ỨNG";
            dt.Rows.Add(dr2h);

            if (GlobalSettings.MA_DON_VI == "4000395355")
            {
                DataRow dr5 = dt.NewRow();
                dr5["Mau"] = "04/HSTK-GC/PhanBo";
                dr5["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ DO BÊN NHẬN GIA CÔNG CUNG ỨNG (THEO BẢNG PHÂN BỔ)";
                dt.Rows.Add(dr5);
            }
            DataRow dr3 = dt.NewRow();
            dr3["Mau"] = "05/HSTK-GC";
            dr3["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ ĐÃ SỬ DỤNG ĐỂ SẢN XUẤT THÀNH SẢN PHẨM XUẤT KHẨU";
            dt.Rows.Add(dr3);

            DataRow dr3h = dt.NewRow();
            dr3h["Mau"] = "06/HSTK-GC";
            dr3h["Ten"] = "BẢNG THANH KHOẢN HỢP ĐỒNG GIA CÔNG";
            dt.Rows.Add(dr3h);

            if (GlobalSettings.MA_DON_VI == "4000395355")
            {
                DataRow dr6 = dt.NewRow();
                dr6["Mau"] = "06/HSTK-GC/PhanBo";
                dr6["Ten"] = "BẢNG THANH KHOẢN HỢP ĐỒNG GIA CÔNG (THEO BẢNG PHÂN BỔ)";
                dt.Rows.Add(dr6);
            }

            DataRow dr4 = dt.NewRow();
            dr4["Mau"] = "07/HSTK-GC";
            dr4["Ten"] = "BẢNG THANH KHOẢN MÁY MÓC, THIẾT BỊ TẠM NHẬP, TÁI XUẤT";
            dt.Rows.Add(dr4);
            //Theo TT 74
            // Mẫu: 01/HSTK-GC
            DataRow dr01 = dt.NewRow();
            dr01["Mau"] = "01/HSTK-GC TT74";
            dr01["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ NHẬP KHẨU";
            dt.Rows.Add(dr01);
            // Mẫu: 02/HSTK-GC
            DataRow dr02 = dt.NewRow();
            dr02["Mau"] = "02/HSTK-GC TT74";
            dr02["Ten"] = "BẢNG TỔNG HỢP SẢN PHẨM GIA CÔNG XUẤT KHẨU";
            dt.Rows.Add(dr02);
            // Mẫu: 03/HSTK-GC
            DataRow dr03 = dt.NewRow();
            dr03["Mau"] = "03/HSTK-GC TT74";
            dr03["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ XUẤT TRẢ RA NƯỚC NGOÀI VÀ CHUYỂN SANG " + "\n\r" +
                         "HỢP ĐỒNG GIA CÔNG KHÁC TRONG KHI ĐANG THỰC HIỆN HỢP ĐỒNG GIA CÔNG ";
            dt.Rows.Add(dr03);
            // Mẫu: 04/HSTK-GC
            DataRow dr04 = dt.NewRow();
            dr04["Mau"] = "04/HSTK-GC TT74";
            dr04["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ DO BÊN NHẬN GIA CÔNG CUNG ỨNG";
            dt.Rows.Add(dr04);
            if (GlobalSettings.MA_DON_VI == "4000395355")
            {
                DataRow dr04a = dt.NewRow();
                dr04a["Mau"] = "04/HSTK-GC TT74/PhanBo";
                dr04a["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ DO BÊN NHẬN GIA CÔNG CUNG ỨNG (THEO BẢNG PHÂN BỔ)";
                dt.Rows.Add(dr04a);
            }

            // Mẫu: 08/HSTK-GC
            DataRow dr08 = dt.NewRow();
            dr08["Mau"] = "08/HSTK-GC TT74";
            dr08["Ten"] = "BẢNG KÊ TỜ KHAI XUẤT KHẨU SẢN PHẨM GIA CÔNG";
            dt.Rows.Add(dr08);


            //Theo TT 117
            // Mẫu: 01/HSTK-GC
            dr01 = dt.NewRow();
            dr01["Mau"] = "01/HSTK-GC/2011 TT117";
            dr01["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ NHẬP KHẨU";
            dt.Rows.Add(dr01);

            dr01 = dt.NewRow();
            dr01["Mau"] = "02/HSTK-GC/2011 TT117";
            dr01["Ten"] = "BẢNG TỔNG HỢP SẢN PHẨM GIA CÔNG XUẤT KHẨU";
            dt.Rows.Add(dr01);

            // Mẫu: 03/HSTK-GC/2011 TT117
            dr01 = dt.NewRow();
            dr01["Mau"] = "03/HSTK-GC/2011 TT117";
            dr01["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ XUẤT TRẢ RA NƯỚC NGOÀI VÀ CHUYỂN SANG " + "\n\r" +
                         "HỢP ĐỒNG GIA CÔNG KHÁC TRONG KHI ĐANG THỰC HIỆN HỢP ĐỒNG GIA CÔNG ";
            dt.Rows.Add(dr01);

            // Mẫu: 04/HSTK-GC/2011 TT117
            dr01 = dt.NewRow();
            dr01["Mau"] = "04/HSTK-GC/2011 TT117";
            dr01["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ DO BÊN NHẬN GIA CÔNG CUNG ỨNG";
            dt.Rows.Add(dr01);
            // Mẫu: 05/HSTK-GC/2011 TT117
            dr01 = dt.NewRow();
            dr01["Mau"] = "05/HSTK-GC/2011 TT117";
            dr01["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ ĐÃ SỬ DỤNG ĐỂ SẢN XUẤT THÀNH SẢN PHẨM XUẤT KHẨU";
            dt.Rows.Add(dr01);
            // Mẫu: 06/HSTK-GC/2011 TT117
            dr01 = dt.NewRow();
            dr01["Mau"] = "06/HSTK-GC/2011 TT117";
            dr01["Ten"] = "BẢNG THANH KHOẢN HỢP ĐỒNG GIA CÔNG";
            dt.Rows.Add(dr01);
            // Mẫu: 07/HSTK-GC/2011 TT117
            dr01 = dt.NewRow();
            dr01["Mau"] = "07/HSTK-GC/2011 TT117";
            dr01["Ten"] = "BẢNG THANH KHOẢN MÁY MÓC, THIẾT BỊ TẠM NHẬP, TÁI XUẤT";
            dt.Rows.Add(dr01);
            // Mẫu: 09/HSTK-GC/2011 TT117
            dr01 = dt.NewRow();
            dr01["Mau"] = "09/HSTK-GC/2011 TT117";
            dr01["Ten"] = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ NHẬP KHẨU";
            dt.Rows.Add(dr01);
            dgThanhKhoanMoi.DataSource = dt;

        }
        private void dgList_RowDoubleClick_1(object sender, RowActionEventArgs e)
        {
            if (this.HD.TrangThaiThanhKhoan == 0)
            {
                showMsg("MSG_WRN18");
                //ShowMessage("Hợp đồng chưa chạy thanh khoản.", false);
                return;
            }
            if (e.Row.RowType == RowType.Record)
            {
                this.Cursor = Cursors.WaitCursor;
                string mau = e.Row.Cells["Mau"].Value.ToString();
                switch (mau)
                {

                    case "01 HQ-GC":
                        this.ShowReport01();
                        break;
                    case "02 HQ-GC":
                        this.ShowReport02();
                        break;
                    case "03 HQ-GC":
                        this.ShowReport03();
                        break;
                    case "01H HQ-GC":
                        this.ShowReport01H();
                        break;
                    case "02H HQ-GC":
                        this.ShowReport02H();
                        break;
                    case "03H HQ-GC":
                        this.ShowReport03H();
                        break;
                    case "04 HQ-GC":
                        this.ShowReport04();
                        break;
                    case "05 HQ-GC":
                        this.ShowReport05();
                        break;
                    case "06 HQ-GC":
                        this.ShowReport06();
                        break;
                    case "07 HQ-GC":
                        this.ShowReport07();
                        break;
                    case "08 HQ-GC":
                        this.ShowReport08();
                        break;
                    case "09 HQ-GC":
                        this.ShowReport09();
                        break;
                    case "10 HQ-GC":
                        this.ShowReport10();
                        break;
                    case "11 HQ-GC":
                        this.ShowReport11();
                        break;
                }
                this.Cursor = Cursors.Default;
            }
        }



        public DataSet CreatSchemaDataSetRP01()
        {
            DataSet dsSTK = new Company.GC.BLL.KDT.ToKhaiMauDich().GetSoToKhaiNhapTheoHopDongUnion(this.HD.ID);
            //Quantity of TK
            int soLuongTK = dsSTK.Tables[0].Rows.Count;// số lượng tờ khai
            // Create new DataSet to Storage :
            int soTable = 0;// Số lượng bảng
            soTable = (soLuongTK - 1) / 4 + 1;

            DataSet dsBC01 = new DataSet();
            DataTable dttemp;
            int z = 0;
            decimal tongNPL = 0;
            for (z = 0; z < soTable; z++)
            {

                //Get LoaiHinhTK :
                dttemp = new DataTable("DataTableNPL" + z.ToString());
                DataColumn[] dcCol = new DataColumn[9];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int j = 0;
                for (int y = z * 4; y < (z + 1) * 4; y++)
                {
                    string keyCol = "";
                    string caption = "";
                    if (y < soLuongTK)
                    {
                        DataRow dr = dsSTK.Tables[0].Rows[y];
                        string soToKhai = dr["ID"].ToString();
                        string maLoaiHinh = dr["MaLoaiHinh"].ToString().Trim();
                        caption = "Tờ khai số " + dr["SoToKhai"].ToString() + "/" + maLoaiHinh + "\n\r " + ((DateTime)dr["NgayDangKy"]).ToString("dd/MM/yyyy");
                        int lengthSoToKhai = soToKhai.Length;
                        for (int i = 0; i < 5 - lengthSoToKhai; i++)
                        {
                            soToKhai = "0" + soToKhai;
                        }
                        keyCol = soToKhai + maLoaiHinh;

                        dcCol[j + 4] = new DataColumn();
                        dcCol[j + 4].ColumnName = keyCol;
                        dcCol[j + 4].DataType = Type.GetType("System.Decimal");
                        dcCol[j + 4].Caption = caption.ToString();

                    }
                    else
                    {

                        keyCol = "00000Empty";
                        dcCol[j + 4] = new DataColumn();
                        dcCol[j + 4].ColumnName = keyCol + j;
                        dcCol[j + 4].DataType = Type.GetType("System.Decimal");
                        dcCol[j + 4].Caption = "";
                    }

                    j++;
                }

                dcCol[j + 4] = new DataColumn();
                dcCol[j + 4].ColumnName = "Tong";
                dcCol[j + 4].DataType = typeof(decimal);
                dcCol[j + 4].Caption = "Tong";
                dttemp.Columns.AddRange(dcCol);
                ///  Send Get NPL List :
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                DataSet ds = npl.GetNPLTK(this.HD.ID);
                int ii = 0;
                //Bind Data into DataTable and Show on Grid :

                foreach (DataRow drHMD in ds.Tables[0].Rows)
                {
                    DataRow dr1 = dttemp.NewRow();
                    dr1["STT"] = ++ii;
                    dr1["TenNPL"] = drHMD["Ten"].ToString();
                    dr1["MaNPL"] = drHMD["Ma"].ToString();
                    dr1["DVT"] = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(drHMD["DVT_ID"].ToString());
                    dttemp.Rows.Add(dr1);
                    foreach (DataColumn col in dttemp.Columns)
                    {
                        if (col.ColumnName != "STT" && col.ColumnName != "TenNPL" && col.ColumnName != "DVT" && col.ColumnName != "ID" && col.ColumnName != "MaNPL" && col.ColumnName != "Tong")
                        {
                            if (col.ColumnName.Contains("PHPLN") || col.ColumnName.Contains("NGC18"))
                            {
                                decimal soLuongNPLTK = new HangChuyenTiep().GetSoluongHangTKGCCT(Convert.ToInt64(processKeyCol(col.ColumnName)[0].ToString()), dr1["MaNPL"].ToString().Trim());
                                dr1[col.ColumnName] = soLuongNPLTK;
                            }
                            else
                            {
                                decimal soLuongNPLTK = new HangMauDich().GetSoluongHangTKMD(Convert.ToInt64(processKeyCol(col.ColumnName)[0].ToString()), dr1["MaNPL"].ToString().Trim());
                                dr1[col.ColumnName] = soLuongNPLTK;
                            }

                        }
                    }
                }

                dsBC01.Tables.Add(dttemp);

            }
            if (dsBC01.Tables.Count > 0)
            {
                for (int t = 0; t < dsBC01.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;
                    for (int i = 0; i < dsBC01.Tables.Count; i++)
                    {
                        DataRow dr = dsBC01.Tables[i].Rows[t];
                        tongTungNPL = tongTungNPL + Convert.ToDecimal(dr[4]) + Convert.ToDecimal(dr[5]) + Convert.ToDecimal(dr[6]) + Convert.ToDecimal(dr[7]);
                    }
                    dsBC01.Tables[dsBC01.Tables.Count - 1].Rows[t]["Tong"] = tongTungNPL;
                }
            }
            return dsBC01;
        }

        public DataSet CreatSchemaDataSetRP02()
        {
            DataSet dsSTK = new Company.GC.BLL.KDT.ToKhaiMauDich().GetSoToKhaiXuatTheoHopDong(this.HD.ID);
            int soluongTK = dsSTK.Tables[0].Rows.Count;

            // Create new DataSet to Storage :
            int sotable = 0;
            if (soluongTK <= 4)
            {
                sotable = 1;
            }
            else
            {
                sotable = soluongTK / 4 + 1;
            }

            DataSet dsTotal02 = new DataSet();
            decimal tongNPL = 0;
            //Create DataTables :
            DataTable dttemp;
            for (int z = 0; z < sotable; z++)
            {
                dttemp = new DataTable("DataTableSanPham" + z.ToString());
                DataColumn[] dcCol = new DataColumn[12];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaSP", Type.GetType("System.String"));
                dcCol[1].Caption = "MaSP";
                dcCol[2] = new DataColumn("TenSP", Type.GetType("System.String"));
                dcCol[2].Caption = "TenSP";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";
                int j = 0;
                for (int y = z * 4; y < (z + 1) * 4; y++)
                {
                    string keyCol = "";
                    string caption = "";
                    if (y < soluongTK)
                    {
                        DataRow dr = dsSTK.Tables[0].Rows[y];
                        string soToKhai = dr["ID"].ToString();
                        string maLoaiHinh = dr["MaLoaiHinh"].ToString().Trim();
                        caption = "Tờ khai số " + dr["SoToKhai"].ToString() + "/" + maLoaiHinh + "\n\r " + ((DateTime)dr["NgayDangKy"]).ToString("dd/MM/yyyy");
                        int lengthSoToKhai = soToKhai.Length;

                        for (int i = 0; i < 5 - lengthSoToKhai; i++)
                        {
                            soToKhai = "0" + soToKhai;
                        }
                        keyCol = soToKhai + maLoaiHinh;

                        dcCol[j + 4] = new DataColumn();
                        dcCol[j + 4].ColumnName = keyCol;
                        dcCol[j + 4].DataType = Type.GetType("System.Decimal");
                        dcCol[j + 4].Caption = caption.ToString();
                    }
                    else
                    {
                        keyCol = "0000" + j + "0000";
                        dcCol[j + 4] = new DataColumn();
                        dcCol[j + 4].ColumnName = keyCol + j;
                        dcCol[j + 4].DataType = Type.GetType("System.Decimal");
                        dcCol[j + 4].Caption = " ";
                    }

                    j++;
                }

                dcCol[j + 4] = new DataColumn();
                dcCol[j + 4].ColumnName = "Tong";
                dcCol[j + 4].DataType = typeof(decimal);
                dcCol[j + 4].Caption = "Tong";
                dttemp.Columns.AddRange(dcCol);

                //Send Get SP List :               
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                DataSet dsSP = sp.GetSanPham(this.HD.ID);
                int ii = 0;
                //Bind Data into DataTable and Show on Grid :
                foreach (DataRow drHMD in dsSP.Tables[0].Rows)
                {
                    DataRow dr1 = dttemp.NewRow();
                    dr1["STT"] = ++ii;
                    dr1["TenSP"] = drHMD["Ten"].ToString();
                    dr1["MaSP"] = drHMD["Ma"].ToString();
                    dr1["DVT"] = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(drHMD["DVT_ID"].ToString());
                    dttemp.Rows.Add(dr1);
                    //decimal tempMD = 0;
                    foreach (DataColumn col in dttemp.Columns)
                    {
                        if (col.ColumnName != "STT" && col.ColumnName != "TenSP" && col.ColumnName != "DVT" && col.ColumnName != "MaSP" && col.ColumnName != "Tong" && col.ColumnName != "GhiChu")
                        {
                            if (col.ColumnName.Contains("PHSPX") || col.ColumnName.Contains("XGC19"))
                            {
                                decimal tongluongSP = new Company.GC.BLL.KDT.GC.HangChuyenTiep().GetSoluongHangTKGCCT(Convert.ToInt64(processKeyCol(col.ColumnName)[0].ToString()), dr1["MaSP"].ToString().Trim());
                                dr1[col.ColumnName] = tongluongSP;
                            }
                            else
                            {
                                decimal tongluongSP = new Company.GC.BLL.KDT.HangMauDich().GetSoluongHangTKMD(Convert.ToInt64(processKeyCol(col.ColumnName)[0].ToString()), dr1["MaSP"].ToString().Trim());
                                dr1[col.ColumnName] = tongluongSP;
                            }
                        }
                    }

                }
                dsTotal02.Tables.Add(dttemp);
            }
            if (dsTotal02.Tables.Count > 0)
            {
                for (int t = 0; t < dsTotal02.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;
                    for (int i = 0; i < dsTotal02.Tables.Count; i++)
                    {
                        DataRow dr = dsTotal02.Tables[i].Rows[t];
                        tongTungNPL = tongTungNPL + Convert.ToDecimal(dr[4]) + Convert.ToDecimal(dr[5]) + Convert.ToDecimal(dr[6]) + Convert.ToDecimal(dr[7]);
                    }
                    dsTotal02.Tables[dsTotal02.Tables.Count - 1].Rows[t]["Tong"] = tongTungNPL;
                }
            }
            return dsTotal02;
        }

        public DataSet CreatSchemaDataSetRP03()
        {
            DataSet dsSTK = new Company.GC.BLL.KDT.ToKhaiMauDich().GetSoToKhaiNhapTheoHopDongT(this.HD.ID);
            int soluongTK = dsSTK.Tables[0].Rows.Count;
            int sotable = 0;
            if (soluongTK <= 4)
            {
                sotable = 1;
            }
            else
            {
                sotable = soluongTK / 4 + 1;
            }

            DataSet dsTotal03 = new DataSet();
            // decimal tongTungNPL = 0;
            //Create DataTables :
            DataTable dttemp;
            for (int z = 0; z < sotable; z++)
            {
                dttemp = new DataTable("DataTableSanPham" + z.ToString());
                DataColumn[] dcCol = new DataColumn[12];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaTB", Type.GetType("System.String"));
                dcCol[1].Caption = "MaTB";
                dcCol[2] = new DataColumn("TenTB", Type.GetType("System.String"));
                dcCol[2].Caption = "TenTB";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";
                int j = 0;

                for (int y = z * 4; y < (z + 1) * 4; y++)
                {
                    string keyCol = "";
                    string caption = "";
                    if (y < soluongTK)
                    {
                        DataRow dr = dsSTK.Tables[0].Rows[y];
                        string soToKhai = dr["ID"].ToString();
                        string maLoaiHinh = dr["MaLoaiHinh"].ToString();
                        caption = "Tờ khai số " + dr["SoToKhai"].ToString() + "/" + maLoaiHinh + "\n\r " + ((DateTime)dr["NgayDangKy"]).ToString("dd/MM/yyyy");
                        int lengthSoToKhai = soToKhai.Length;
                        for (int i = 0; i < 5 - lengthSoToKhai; i++)
                        {
                            soToKhai = "0" + soToKhai;
                        }
                        keyCol = soToKhai + maLoaiHinh;
                        dcCol[j + 4] = new DataColumn();
                        dcCol[j + 4].ColumnName = keyCol;
                        dcCol[j + 4].DataType = Type.GetType("System.Decimal");
                        dcCol[j + 4].Caption = caption.ToString();
                    }
                    else
                    {
                        keyCol = "0000" + j + "0000";
                        dcCol[j + 4] = new DataColumn();
                        dcCol[j + 4].ColumnName = keyCol + j;
                        dcCol[j + 4].DataType = Type.GetType("System.Decimal");
                        dcCol[j + 4].Caption = " ";
                    }

                    j++;
                }

                dcCol[j + 4] = new DataColumn();
                dcCol[j + 4].ColumnName = "Tong";
                dcCol[j + 4].DataType = typeof(decimal);
                dcCol[j + 4].Caption = "Tong";
                dttemp.Columns.AddRange(dcCol);

                //Send Get TB List : 
                DataSet dsTB;
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                dsTB = tb.GetThietBi(this.HD.ID);

                int ii = 0;
                //Bind Data into DataTable and Show on Grid :
                foreach (DataRow drHMD in dsTB.Tables[0].Rows)
                {
                    DataRow dr1 = dttemp.NewRow();
                    dr1["STT"] = ++ii;
                    dr1["TenTB"] = drHMD["Ten"].ToString();
                    dr1["MaTB"] = drHMD["Ma"].ToString();
                    dr1["DVT"] = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(drHMD["DVT_ID"].ToString());
                    dttemp.Rows.Add(dr1);
                    // decimal tempMD = 0;
                    foreach (DataColumn col in dttemp.Columns)
                    {
                        if (col.ColumnName != "STT" && col.ColumnName != "TenTB" && col.ColumnName != "DVT" && col.ColumnName != "MaTB" && col.ColumnName != "Tong")
                        {

                            if (col.ColumnName.Contains("PHTBN") || col.ColumnName.Contains("NGC20"))
                            {
                                decimal tongluongTB = new Company.GC.BLL.KDT.GC.HangChuyenTiep().GetSoluongHangTKGCCT(Convert.ToInt64(processKeyCol(col.ColumnName)[0].ToString()), dr1["MaTB"].ToString().Trim());
                                dr1[col.ColumnName] = tongluongTB;
                            }
                            else
                            {
                                decimal tongluongTB = new Company.GC.BLL.KDT.HangMauDich().GetSoluongHangTKMD(Convert.ToInt64(processKeyCol(col.ColumnName)[0].ToString()), dr1["MaTB"].ToString().Trim());
                                dr1[col.ColumnName] = tongluongTB;

                            }


                        }
                    }

                }
                dsTotal03.Tables.Add(dttemp);
            }
            if (dsTotal03.Tables.Count > 0)
            {
                for (int t = 0; t < dsTotal03.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;
                    for (int i = 0; i < dsTotal03.Tables.Count; i++)
                    {
                        DataRow dr = dsTotal03.Tables[i].Rows[t];
                        tongTungNPL = tongTungNPL + Convert.ToDecimal(dr[4]) + Convert.ToDecimal(dr[5]) + Convert.ToDecimal(dr[6]) + Convert.ToDecimal(dr[7]);
                    }
                    dsTotal03.Tables[dsTotal03.Tables.Count - 1].Rows[t]["Tong"] = tongTungNPL;
                }
            }



            return dsTotal03;
        }

        //public DataSet CreatSchemaDataSetRP04()
        //{

        //    DataSet dsSTK = new DataSet();
        //    ToKhaiMauDichCollection tkmdColl = HD.GetTKXK();
        //    BKCungUngDangKyCollection bkcudkColl = HD.GetNPLCU();

        //    int soTbl = (bkcudkColl.Count - 1) / 7 + 1;

        //    DataTable dttemp;
        //    for (int i = 0; i < soTbl; i++)
        //    {
        //        dttemp = new DataTable("tbl_" + i.ToString());
        //        ToKhaiMauDich tk;
        //        //System.in
        //        dttemp.Columns.Add("MaTKX", Type.GetType("System.Int64"));
        //        dttemp.Columns.Add("SoTKX", Type.GetType("System.Int64"));

        //        for (int j = i * 7; j < (i + 1) * 7; j++)
        //        {
        //            DataColumn col = new DataColumn();
        //        }

        //    }



        //    foreach (ToKhaiMauDich tkmd in tkmdColl)
        //    {
        //        //tkmd.GetNPLCungUngTK();
        //    }

        //    return dsSTK;
        //}

        public DataSet CreatSchemaDataSetRP05()
        {
            DataSet dsSoSP = new Company.GC.BLL.GC.SanPham().GetSanPham(this.HD.ID);
            int soluongSP = dsSoSP.Tables[0].Rows.Count;

            // Create new DataSet to Storage :
            int sotable = 0;
            sotable = (soluongSP + 1) / 2;
            DataSet dsTotal05 = new DataSet();
            //Create DataTables :
            DataTable dttemp;
            for (int z = 0; z < sotable; z++)
            {
                dttemp = new DataTable("DataTableDinhMuc" + z.ToString());
                DataColumn[] dcCol = new DataColumn[17];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                DataRow dr1 = dsSoSP.Tables[0].Rows[2 * z];
                dcCol[4] = new DataColumn();
                dcCol[4].ColumnName = dr1["Ma"].ToString();
                dcCol[4].DataType = Type.GetType("System.String");
                dcCol[4].Caption = dr1["Ma"].ToString();

                dcCol[5] = new DataColumn();
                dcCol[5].ColumnName = "SoLuongSP1";
                dcCol[5].DataType = Type.GetType("System.Decimal");
                dcCol[5].Caption = "SoLuongSP1";

                dcCol[6] = new DataColumn();
                dcCol[6].ColumnName = "DVTSP1";
                dcCol[6].DataType = Type.GetType("System.String");
                dcCol[6].Caption = "DVTSP1";

                dcCol[7] = new DataColumn();
                dcCol[7].ColumnName = "DinhMuc1";
                dcCol[7].DataType = Type.GetType("System.Decimal");
                dcCol[7].Caption = "DinhMuc1";

                dcCol[8] = new DataColumn();
                dcCol[8].ColumnName = "TLHH1";
                dcCol[8].DataType = Type.GetType("System.Decimal");
                dcCol[8].Caption = "TLHH1";

                dcCol[9] = new DataColumn();
                dcCol[9].ColumnName = "LuongSD1";
                dcCol[9].DataType = Type.GetType("System.Decimal");
                dcCol[9].Caption = "LuongSD1";

                DataRow dr2 = null;
                if (z * 2 + 1 < soluongSP)
                {
                    dr2 = dsSoSP.Tables[0].Rows[2 * z + 1];
                    dcCol[10] = new DataColumn();
                    dcCol[10].ColumnName = dr2["Ma"].ToString();
                    dcCol[10].DataType = Type.GetType("System.String");
                    dcCol[10].Caption = dr2["Ma"].ToString();
                }
                else
                {
                    dcCol[10] = new DataColumn();
                    dcCol[10].ColumnName = "MaSP2";
                    dcCol[10].DataType = Type.GetType("System.String");
                    dcCol[10].Caption = "";
                }
                dcCol[11] = new DataColumn();
                dcCol[11].ColumnName = "SoLuongSP2";
                dcCol[11].DataType = Type.GetType("System.Decimal");
                dcCol[11].Caption = "SoLuongSP2";

                dcCol[12] = new DataColumn();
                dcCol[12].ColumnName = "DVTSP2";
                dcCol[12].DataType = Type.GetType("System.String");
                dcCol[12].Caption = "DVTSP2";

                dcCol[13] = new DataColumn();
                dcCol[13].ColumnName = "DinhMuc2";
                dcCol[13].DataType = Type.GetType("System.Decimal");
                dcCol[13].Caption = "DinhMuc2";

                dcCol[14] = new DataColumn();
                dcCol[14].ColumnName = "TLHH2";
                dcCol[14].DataType = Type.GetType("System.Decimal");
                dcCol[14].Caption = "TLHH2";

                dcCol[15] = new DataColumn();
                dcCol[15].ColumnName = "LuongSD2";
                dcCol[15].DataType = Type.GetType("System.Decimal");
                dcCol[15].Caption = "LuongSD2";

                dcCol[16] = new DataColumn();
                dcCol[16].ColumnName = "Tong";
                dcCol[16].DataType = Type.GetType("System.Decimal");
                dcCol[16].Caption = "Tong";
                dttemp.Columns.AddRange(dcCol);

                //Send Get SP List :               
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                DataSet dsNPL;
                dsNPL = npl.GetNPLDM(this.HD.ID);
                int ii = 0;
                //Bind Data into DataTable and Show on Grid :
                DataRow drData;
                foreach (DataRow drNPL in dsNPL.Tables[0].Rows)
                {
                    try
                    {
                        //DataRow drData = dttemp.NewRow();
                        drData = dttemp.NewRow();
                        drData[0] = ++ii;
                        drData[1] = drNPL["Ma"].ToString();
                        drData[2] = drNPL["Ten"].ToString();
                        drData["DVT"] = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(drNPL["DVT_ID"].ToString());
                        //dttemp.Rows.Add(drData);

                        Company.GC.BLL.GC.SanPham sptemp = new Company.GC.BLL.GC.SanPham();
                        DataSet dsSLSP1 = sptemp.GetSoLuongSanPhamByHDAndMaSP(this.HD.ID, dttemp.Columns[4].Caption);
                        DataSet ds1 = new Company.GC.BLL.GC.DinhMuc().GetThongTinDinhMuc(this.HD.ID, drNPL["Ma"].ToString(), dttemp.Columns[4].Caption);
                        drData[4] = dttemp.Columns[4].Caption;
                        try
                        {
                            DataRow drSLSP1 = dsSLSP1.Tables[0].Rows[0];
                            drData[5] = Convert.ToDecimal(drSLSP1["SoLuongDaXuat"]);
                            drData[6] = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(drSLSP1["DVT_ID"].ToString());
                            if (ds1.Tables[0].Rows.Count > 0)
                            {
                                //drSLSP1["Ma"].ToString();                                                       
                                DataRow drtemp1 = ds1.Tables[0].Rows[0];
                                if (drtemp1["DinhMucSuDung"].ToString() != null)
                                    drData[7] = Convert.ToDecimal(drtemp1["DinhMucSuDung"]);
                                else
                                    drData[7] = 0;
                                if (drtemp1["TyLeHaoHut"].ToString() != null)
                                    drData[8] = Convert.ToDecimal(drtemp1["TyLeHaoHut"]);
                                else
                                    drData[8] = 0;
                                if (drtemp1["TyLeHaoHut"].ToString() != null && drSLSP1["SoLuongDaXuat"].ToString() != null)
                                    drData[9] = (Convert.ToDecimal(drSLSP1["SoLuongDaXuat"]) * Convert.ToDecimal(drtemp1["DinhMucSuDung"]) * (100 + Convert.ToDecimal(drtemp1["TyLeHaoHut"]))) / 100;
                                else
                                    drData[9] = 0;
                                // dttemp.Rows.Add(drData);
                            }
                        }
                        catch { }

                        Company.GC.BLL.GC.SanPham sptemp2 = new Company.GC.BLL.GC.SanPham();
                        DataSet dsSLSP2 = sptemp2.GetSoLuongSanPhamByHDAndMaSP(this.HD.ID, dttemp.Columns[10].Caption);
                        DataSet ds2 = new Company.GC.BLL.GC.DinhMuc().GetThongTinDinhMuc(this.HD.ID, drNPL["Ma"].ToString(), dttemp.Columns[10].Caption);
                        try
                        {
                            drData[10] = dttemp.Columns[10].Caption;
                            DataRow drSLSP2 = dsSLSP2.Tables[0].Rows[0];
                            // drSLSP2["Ma"].ToString();
                            drData[11] = Convert.ToDecimal(drSLSP2["SoLuongDaXuat"]);
                            drData[12] = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(drSLSP2["DVT_ID"].ToString());
                            if (ds2.Tables[0].Rows.Count > 0)
                            {


                                DataRow drtemp2 = ds2.Tables[0].Rows[0];
                                if (drtemp2["DinhMucSuDung"].ToString() != null)
                                    drData[13] = Convert.ToDecimal(drtemp2["DinhMucSuDung"]);
                                else
                                    drData[13] = 0;
                                if (drtemp2["TyLeHaoHut"].ToString() != null)
                                    drData[14] = Convert.ToDecimal(drtemp2["TyLeHaoHut"]);
                                else
                                    drData[14] = 0;
                                if (drtemp2["TyLeHaoHut"].ToString() != null && drSLSP2["SoLuongDaXuat"].ToString() != null)
                                    // drData[15] = (Convert.ToDecimal(drSLSP2["SoLuongDaXuat"]) * Convert.ToDecimal(drtemp2["TyLeHaoHut"])) / 100 + Convert.ToDecimal(drSLSP2["SoLuongDaXuat"]);
                                    drData[15] = (Convert.ToDecimal(drSLSP2["SoLuongDaXuat"]) * Convert.ToDecimal(drtemp2["DinhMucSuDung"]) * (100 + Convert.ToDecimal(drtemp2["TyLeHaoHut"]))) / 100;
                                else
                                    drData[15] = 0;

                                //dttemp.Rows.Add(drData);
                            }
                        }
                        catch { }
                        dttemp.Rows.Add(drData);
                    }
                    catch
                    {

                    }


                }
                dsTotal05.Tables.Add(dttemp);
            }

            if (dsTotal05.Tables.Count > 0)
            {
                for (int t = 0; t < dsTotal05.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;
                    for (int i = 0; i < dsTotal05.Tables.Count; i++)
                    {
                        try
                        {
                            DataRow dr = dsTotal05.Tables[i].Rows[t];
                            if (dr[9] != DBNull.Value)
                                tongTungNPL += Convert.ToDecimal(dr[9]);
                            if (dr[15] != DBNull.Value)
                                tongTungNPL += Convert.ToDecimal(dr[15]);
                        }
                        catch { }
                    }
                    try
                    {
                        dsTotal05.Tables[dsTotal05.Tables.Count - 1].Rows[t]["Tong"] = tongTungNPL;
                    }
                    catch { }
                }
            }
            return dsTotal05;
        }
        public DataSet CreatSchemaDataSetRP05New()
        {
            DataSet dsSoSP = new Company.GC.BLL.GC.SanPham().GetSanPham(this.HD.ID);
            int soluongSP = dsSoSP.Tables[0].Rows.Count;

            int soTable = 0;
            soTable = (soluongSP - 1) / 4 + 1;
            DataSet ds05 = new DataSet();
            DataTable dttemp = new DataTable();
            for (int z = 0; z < soTable; z++)
            {
                dttemp = new DataTable("dt05" + z.ToString());
                DataColumn[] dcCol = new DataColumn[17];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 0;
                for (int k = z * 4; k < (z + 1) * 4; k++)
                {

                    if (k < soluongSP)
                    {
                        DataRow drSP = dsSoSP.Tables[0].Rows[k];

                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "DinhMuc" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = drSP["Ma"].ToString();
                        t++;
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "LuongSD" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = Convert.ToDecimal(drSP["SoLuongDaXuat"]).ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
                        t++;
                    }
                    else
                    {
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "DinhMuc" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = "";
                        t++;
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "LuongSD" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = "";
                        t++;

                    }
                }
                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "TongNPL";
                dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                dcCol[4 + t].Caption = "TongNPL";
                dttemp.Columns.AddRange(dcCol);

                int stt = 0;
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                DataSet dsNPL;
                dsNPL = npl.GetNPLDM(this.HD.ID);
                foreach (DataRow dr in dsNPL.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drData = dttemp.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["Ma"].ToString();
                        drData[2] = dr["Ten"].ToString();
                        drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        for (int n = 0; n < 4; n++)
                        {
                            if (dttemp.Columns["DinhMuc" + n].Caption != "")
                            {
                                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                                dm.MaSanPham = dttemp.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["Ma"].ToString();
                                dm.HopDong_ID = this.HD.ID;
                                dm.Load();
                                decimal DMChung = dm.DinhMucSuDung * (dm.TyLeHaoHut + 100) / 100;
                                drData["DinhMuc" + n] = DMChung;
                                drData["LuongSD" + n] = Convert.ToDecimal(dttemp.Columns["LuongSD" + n].Caption) * DMChung;
                            }
                            else
                            {

                            }
                        }
                        dttemp.Rows.Add(drData);
                    }
                    catch
                    {

                    }


                }
                ds05.Tables.Add(dttemp);
            }

            if (ds05.Tables.Count > 0)
            {
                for (int t = 0; t < ds05.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;
                    for (int i = 0; i < ds05.Tables.Count; i++)
                    {
                        try
                        {
                            DataRow dr = ds05.Tables[i].Rows[t];
                            for (int n = 0; n < 4; n++)
                            {
                                if (dr["LuongSD" + n] != DBNull.Value)
                                    tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                            }
                        }
                        catch { }
                    }
                    try
                    {
                        ds05.Tables[ds05.Tables.Count - 1].Rows[t]["TongNPL"] = tongTungNPL;
                    }
                    catch { }
                }
            }
            return ds05;
        }
        public DataSet CreatSchemaDataSetRP11()
        {

            DataSet dsSoSP = new Company.GC.BLL.GC.SanPham().GetSanPham(this.HD.ID);
            int soluongSP = dsSoSP.Tables[0].Rows.Count;

            // Create new DataSet to Storage :
            int sotable = 0;
            sotable = (soluongSP + 1) / 2;

            DataSet dsTotal11 = new DataSet();
            //Create DataTables :
            DataTable dttemp;
            for (int z = 0; z < sotable; z++)
            {
                dttemp = new DataTable("DataTableDinhMuc" + z.ToString());
                DataColumn[] dcCol = new DataColumn[20];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                DataRow dr1 = dsSoSP.Tables[0].Rows[2 * z];
                dcCol[4] = new DataColumn();
                dcCol[4].ColumnName = dr1["Ma"].ToString();
                dcCol[4].DataType = Type.GetType("System.String");
                dcCol[4].Caption = dr1["Ma"].ToString();

                dcCol[5] = new DataColumn();
                dcCol[5].ColumnName = "SoLuongSP1";
                dcCol[5].DataType = Type.GetType("System.Decimal");
                dcCol[5].Caption = "SoLuongSP1";

                dcCol[6] = new DataColumn();
                dcCol[6].ColumnName = "DVTSP1";
                dcCol[6].DataType = Type.GetType("System.String");
                dcCol[6].Caption = "DVTSP1";

                dcCol[7] = new DataColumn();
                dcCol[7].ColumnName = "DinhMuc1";
                dcCol[7].DataType = Type.GetType("System.Decimal");
                dcCol[7].Caption = "DinhMuc1";

                dcCol[8] = new DataColumn();
                dcCol[8].ColumnName = "TLHH1";
                dcCol[8].DataType = Type.GetType("System.Decimal");
                dcCol[8].Caption = "TLHH1";

                dcCol[9] = new DataColumn();
                dcCol[9].ColumnName = "LuongSD1";
                dcCol[9].DataType = Type.GetType("System.Decimal");
                dcCol[9].Caption = "LuongSD1";

                DataRow dr2 = null;
                if (z * 2 + 1 < soluongSP)
                {
                    dr2 = dsSoSP.Tables[0].Rows[2 * z + 1];
                    dcCol[10] = new DataColumn();
                    dcCol[10].ColumnName = dr2["Ma"].ToString();
                    dcCol[10].DataType = Type.GetType("System.String");
                    dcCol[10].Caption = dr2["Ma"].ToString();
                }
                else
                {
                    dcCol[10] = new DataColumn();
                    dcCol[10].ColumnName = "MaSP2";
                    dcCol[10].DataType = Type.GetType("System.String");
                    dcCol[10].Caption = "";
                }
                dcCol[11] = new DataColumn();
                dcCol[11].ColumnName = "SoLuongSP2";
                dcCol[11].DataType = Type.GetType("System.Decimal");
                dcCol[11].Caption = "SoLuongSP2";

                dcCol[12] = new DataColumn();
                dcCol[12].ColumnName = "DVTSP2";
                dcCol[12].DataType = Type.GetType("System.String");
                dcCol[12].Caption = "DVTSP2";

                dcCol[13] = new DataColumn();
                dcCol[13].ColumnName = "DinhMuc2";
                dcCol[13].DataType = Type.GetType("System.Decimal");
                dcCol[13].Caption = "DinhMuc2";

                dcCol[14] = new DataColumn();
                dcCol[14].ColumnName = "TLHH2";
                dcCol[14].DataType = Type.GetType("System.Decimal");
                dcCol[14].Caption = "TLHH2";

                dcCol[15] = new DataColumn();
                dcCol[15].ColumnName = "LuongSD2";
                dcCol[15].DataType = Type.GetType("System.Decimal");
                dcCol[15].Caption = "LuongSD2";

                //
                dcCol[16] = new DataColumn();
                dcCol[16].ColumnName = "TongLuongCU";
                dcCol[16].DataType = Type.GetType("System.Decimal");
                dcCol[16].Caption = "TongLuongCU";

                dcCol[17] = new DataColumn();
                dcCol[17].ColumnName = "DonGia";
                dcCol[17].DataType = Type.GetType("System.Decimal");
                dcCol[17].Caption = "DonGia";

                dcCol[18] = new DataColumn();
                dcCol[18].ColumnName = "TriGia";
                dcCol[18].DataType = Type.GetType("System.Decimal");
                dcCol[18].Caption = "TriGia";

                dcCol[19] = new DataColumn();
                dcCol[19].ColumnName = "HinhThucCU";
                dcCol[19].DataType = Type.GetType("System.String");
                dcCol[19].Caption = "HinhThucCU";
                dttemp.Columns.AddRange(dcCol);

                //Send Get SP List :               
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                DataSet dsNPL;
                //
                //dsNPL = npl.GetNPLDM(this.HD.ID);
                dsNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetDanhSachNPLCU(this.HD.ID);
                int ii = 0;
                //Bind Data into DataTable and Show on Grid :
                decimal temptotal = 0;
                foreach (DataRow drNPL in dsNPL.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drData = dttemp.NewRow();
                        drData[0] = ++ii;
                        drData[1] = drNPL["Ma"].ToString();
                        drData[2] = drNPL["Ten"].ToString();
                        drData["DVT"] = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(drNPL["DVT_ID"].ToString());
                        //dttemp.Rows.Add(drData);
                        decimal luongCU1 = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNPLCUBySPAndNPL(this.HD.ID, drNPL["Ma"].ToString(), dttemp.Columns[4].Caption);

                        Company.GC.BLL.GC.SanPham sptemp = new Company.GC.BLL.GC.SanPham();
                        DataSet dsSLSP1 = sptemp.GetSoLuongSanPhamByHDAndMaSP(this.HD.ID, dttemp.Columns[4].Caption);
                        DataSet ds1 = new Company.GC.BLL.GC.DinhMuc().GetThongTinDinhMuc(this.HD.ID, drNPL["Ma"].ToString(), dttemp.Columns[4].Caption);
                        //DataSet ds11 = new Company.GC.BLL.GC.NguyenPhuLieu().GetNPLTK();  
                        if (ds1.Tables[0].Rows.Count > 0)
                        {
                            DataRow drSLSP1 = dsSLSP1.Tables[0].Rows[0];
                            drData[4] = drSLSP1["Ma"].ToString();
                            drData[5] = Convert.ToDecimal(drSLSP1["SoLuongDaXuat"]);
                            drData[6] = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(drSLSP1["DVT_ID"].ToString());

                            DataRow drtemp1 = ds1.Tables[0].Rows[0];

                            if (drtemp1["DinhMucSuDung"].ToString() != null)
                                drData[7] = Convert.ToDecimal(drtemp1["DinhMucSuDung"]);
                            else
                                drData[7] = 0;
                            if (drtemp1["TyLeHaoHut"].ToString() != null)
                                drData[8] = Convert.ToDecimal(drtemp1["TyLeHaoHut"]);
                            else
                                drData[8] = 0;
                            try
                            {

                                drData[9] = luongCU1;
                            }
                            catch { }

                        }
                        decimal luongCU2 = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNPLCUBySPAndNPL(this.HD.ID, drNPL["Ma"].ToString(), dttemp.Columns[10].Caption);
                        Company.GC.BLL.GC.SanPham sptemp2 = new Company.GC.BLL.GC.SanPham();
                        DataSet dsSLSP2 = sptemp2.GetSoLuongSanPhamByHDAndMaSP(this.HD.ID, dttemp.Columns[10].Caption);
                        DataSet ds2 = new Company.GC.BLL.GC.DinhMuc().GetThongTinDinhMuc(this.HD.ID, drNPL["Ma"].ToString(), dttemp.Columns[10].Caption);
                        if (ds2.Tables[0].Rows.Count > 0)
                        {
                            DataRow drSLSP2 = dsSLSP2.Tables[0].Rows[0];
                            drData[10] = drSLSP2["Ma"].ToString();
                            drData[11] = Convert.ToDecimal(drSLSP2["SoLuongDaXuat"]);
                            drData[12] = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(drSLSP2["DVT_ID"].ToString());

                            DataRow drtemp2 = ds2.Tables[0].Rows[0];
                            if (drtemp2["DinhMucSuDung"].ToString() != null)
                                drData[13] = Convert.ToDecimal(drtemp2["DinhMucSuDung"]);
                            else
                                drData[13] = 0;
                            if (drtemp2["TyLeHaoHut"].ToString() != null)
                                drData[14] = Convert.ToDecimal(drtemp2["TyLeHaoHut"]);
                            else
                                drData[14] = 0;
                            try
                            {

                                drData[15] = luongCU2;
                            }
                            catch { }
                            //dttemp.Rows.Add(drData);


                        }
                        if (drNPL["GiaTB"].ToString() != null)
                            drData[17] = Convert.ToDecimal(drNPL["GiaTB"]);
                        drData[19] = drNPL["HinhThuCungUng"].ToString();

                        dttemp.Rows.Add(drData);
                    }
                    catch
                    {

                    }

                }
                dsTotal11.Tables.Add(dttemp);
            }
            if (dsTotal11.Tables.Count > 0)
            {
                for (int t = 0; t < dsTotal11.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;
                    for (int i = 0; i < dsTotal11.Tables.Count; i++)
                    {
                        try
                        {
                            DataRow dr = dsTotal11.Tables[i].Rows[t];
                            if (dr[9] != DBNull.Value)
                                tongTungNPL += Convert.ToDecimal(dr[9]);
                            if (dr[15] != DBNull.Value)
                                tongTungNPL += Convert.ToDecimal(dr[15]);
                        }
                        catch { }
                    }
                    try
                    {
                        dsTotal11.Tables[dsTotal11.Tables.Count - 1].Rows[t]["TongLuongCU"] = tongTungNPL;
                        if (dsTotal11.Tables[dsTotal11.Tables.Count - 1].Rows[t]["DonGia"] != null)
                            dsTotal11.Tables[dsTotal11.Tables.Count - 1].Rows[t]["TriGia"] = tongTungNPL * Convert.ToDecimal(dsTotal11.Tables[dsTotal11.Tables.Count - 1].Rows[t]["DonGia"]);
                    }
                    catch { }
                }
            }

            return dsTotal11;
        }

        //Show reports :
        private void ShowReport01()
        {
            ReportViewBC01Form f = new ReportViewBC01Form();
            f.ds = this.CreatSchemaDataSetRP01();
            f.HD = this.HD;
            f.Show();
            //BangKe01H_HQGC bk = new BangKe01H_HQGC();
            //bk.HD = this.HD;
            //bk.BindReport("");
            //bk.ShowPreview();

        }
        private void ShowReport02()
        {
            ReportViewBC02Form f = new ReportViewBC02Form();
            f.ds = this.CreatSchemaDataSetRP02();
            f.HD = this.HD;
            f.Show();
        }
        private void ShowReport03()
        {
            ReportViewBC03Form f = new ReportViewBC03Form();
            f.ds = this.CreatSchemaDataSetRP03();
            f.HD = this.HD;
            f.Show();
        }

        //In Ngang
        private void ShowReport01H()
        {
            ReportViewBC01HForm f = new ReportViewBC01HForm();
            f.HD = this.HD;
            f.Show();

        }
        private void ShowReport02H()
        {
            ReportViewBC02HForm f = new ReportViewBC02HForm();
            f.HD = this.HD;
            f.Show();
        }
        private void ShowReport03H()
        {
            ReportViewBC03HForm f = new ReportViewBC03HForm();
            f.HD = this.HD;
            f.Show();
        }
        //
        private void ShowReport04()
        {

            BK04Form BK04 = new BK04Form();
            BK04.HD = this.HD;
            BK04.ShowDialog();
        }
        private void ShowReport05()
        {
            ReportViewBC05Form f = new ReportViewBC05Form();
            f.ds = this.CreatSchemaDataSetRP05();
            f.HD = this.HD;
            f.Show();
        }
        private void ShowReport06()
        {

            BK06Form BK06 = new BK06Form();
            BK06.HD = this.HD;
            BK06.ShowDialog();

        }
        private void ShowReport07()
        {
            //BangKe07_HQGC BK07 = new BangKe07_HQGC();
            //BK07.BindReport(this.HD.ID, this.HD.SoHopDong);
            //BK07.ShowPreview();
            ReportViewBC07Form f = new ReportViewBC07Form();
            f.HD = this.HD;
            f.Show();
        }
        private void ShowReport08()
        {

            BK08Form BK08 = new BK08Form();
            BK08.HD = this.HD;
            BK08.Show();
        }
        private void ShowReport09()
        {
            BK09Form BK09 = new BK09Form();
            BK09.HD = this.HD;
            BK09.Show();
        }
        private void ShowReport10()
        {
            BK10Form BK10 = new BK10Form();
            BK10.HD = this.HD;
            BK10.Show();
        }
        private void ShowReport11()
        {
            ReportViewBC11Form f = new ReportViewBC11Form();
            f.ds = this.CreatSchemaDataSetRP11();
            f.HD = this.HD;
            f.Show();
        }
        private string[] processKeyCol(string strKeyCol)
        {
            string[] returnValues = new string[2];
            returnValues[0] = strKeyCol.Substring(0, 5);
            returnValues[1] = strKeyCol.Substring(5, strKeyCol.Length - 5);
            return returnValues;
        }

        private void dgTKN_RecordUpdated(object sender, EventArgs e)
        {
            ToKhaiMauDich TKMD = (ToKhaiMauDich)dgTKN.GetRow().DataRow;
            TKMD.Update();
        }
        private void dgTKX_RecordUpdated(object sender, EventArgs e)
        {
            ToKhaiMauDich TKMD = (ToKhaiMauDich)dgTKX.GetRow().DataRow;
            TKMD.Update();
        }
        private void uiCommandManager1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThanhKhoan":
                    if (MainForm.versionHD != 2)
                    {
                        try
                        {
                            string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                            if (st != "")
                            {
                                ShowMessage(st, false);
                                this.Cursor = Cursors.Default;
                                return;
                            }

                        }
                        catch { }
                    }
                    ChayThanhLyForm f = new ChayThanhLyForm();
                    f.HD = this.HD;
                    f.ShowDialog();
                    if (f.IsSuccess)
                    {
                        this.HD.TrangThaiThanhKhoan = 1;
                    }
                    else
                    {
                        this.HD.TrangThaiThanhKhoan = 0;
                    }
                    this.HD.Update();
                    RefreshStatus();
                    break;
                case "cmdXuLyHD":
                    try
                    {
                        HD.TinhToanCanDoiHopDong(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.LuongSP, GlobalSettings.SoThapPhan.DinhMuc, GlobalSettings.SoThapPhan.TLHH);
                        showMsg("MSG_2702010");
                        //ShowMessage("Xứ lý hợp đồng thành công.", false);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                    }
                    break;
                case "cmdLayToKhaiNhapSXXK":
                    SelectDayForm f1 = new SelectDayForm();
                    f1.HD = this.HD;
                    f1.ShowDialog();
                    break;
                case "LuuThongTin":
                    HD.SoHopDong = txtSoHopDong.Text.Trim();
                    HD.NgayDangKy = ccNgayKyHD.Value;
                    HD.NgayGiaHan = ccNgayGiaHan.Value;
                    HD.NgayHetHan = ccNgayKetThucHD.Value;
                    HD.NuocThue_ID = nuocThue.Ma;
                    HD.NguyenTe_ID = nguyenTeControl1.Ma;
                    HD.DonViDoiTac = txtTenDoiTac.Text.Trim();
                    HD.DiaChiDoiTac = txtDiaChi.Text.Trim();
                    HD.Update();
                    ShowMessage("Cập nhật thành công .", false);
                    break;
            }
        }


        private void dgTKN_LoadingRow(object sender, RowLoadEventArgs e)
        {

            if (((DateTime)e.Row.Cells["Ngay_THN_THX"].Value).Year == 1900) e.Row.Cells["Ngay_THN_THX"].Text = "";
            if (((DateTime)e.Row.Cells["NgayDangKy"].Value).Year == 1900) e.Row.Cells["NgayDangKy"].Text = "";

            if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 0) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ duyệt", "Pending approval");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value < 0) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chưa khai báo", "Not declared yet");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 1) e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã duyệt", "Approved");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 2) e.Row.Cells["TrangThaiXuLy"].Text = setText("Không phê duyệt", "Not Approve");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 5) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ sửa", "Not declared yet");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 10) e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã hủy", "Not declared yet");
            else if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 11) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ hủy", "Not declared yet");
        }

        private void dgPhuKien_LoadingRow_1(object sender, RowLoadEventArgs e)
        {
            //if ((int)e.Row.Cells["TrangThaiXuLy"].Value == 0) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ duyệt", "Pending approval");
            //else if ((int)e.Row.Cells["TrangThaiXuLy"].Value < 0) e.Row.Cells["TrangThaiXuLy"].Text = setText("Chưa khai báo", "Not declared yet");
            // else e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã duyệt", "Approved");
        }

        private void btnNThem_Click(object sender, EventArgs e)
        {

        }

        private void btnNThem_Click_1(object sender, EventArgs e)
        {
            switch (tabHopDong.SelectedIndex)
            {
                case 0:
                    // this.showFormNguyenPhuLieu();
                    break;
                case 1:
                    // this.showFormSanPham();
                    break;
                case 2:
                    //this.showFormThietBi();
                    break;
                case 4:
                    this.show_ToKhaiMauDichForm("NGC");
                    break;
                case 5:
                    this.show_ToKhaiMauDichForm("XGC");
                    break;
                case 6:
                    this.show_PhuKienGCSendForm();
                    break;
                case 7:
                    this.show_ToKhaiGCChuyenTiepNhap("N");
                    break;
                case 8:
                    this.show_ToKhaiGCChuyenTiepNhap("X");
                    break;
                //case 9:
                //    this.show_KhaiBaoDMCU();
                //    break;

            }
        }

        private void showFormNguyenPhuLieu()
        {
            NguyenPhuLieuGCEditForm npl = new NguyenPhuLieuGCEditForm();
            npl.HD = HD;
            npl.HD.NPLCollection = this.HD.NPLCollection;
            if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                npl.OpenType = OpenFormType.Edit;
            else

                npl.OpenType = OpenFormType.View;
            npl.ShowDialog();
            try
            {
                dgNguyenPhuLieu.DataSource = npl.HD.NPLCollection;// this.HD.NPLCollection;
                dgNguyenPhuLieu.Refetch();
            }
            catch
            {
                dgNguyenPhuLieu.Refresh();
            }
        }

        private void showFormThietBi()
        {
            ThietBiGCEditForm f = null;
            f = new ThietBiGCEditForm();
            f.HD = HD;
            if (HD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog();

            try
            {
                dgThietBi.Refetch();
            }
            catch
            {
                dgThietBi.Refresh();
            }

        }
        private void showFormSanPham()
        {
            SanPhamGCEditForm f = null;
            f = new SanPhamGCEditForm();
            f.HD = HD;
            if (HD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog();

            try
            {
                dgSanPham.Refetch();
            }
            catch
            {
                dgSanPham.Refresh();
            }
        }

        private void show_ToKhaiMauDichForm(string nhomLoaiHinh)
        {

            tkmdForm = new Company.Interface.GC.ToKhaiMauDichForm();
            tkmdForm.OpenType = OpenFormType.Insert;
            tkmdForm.Name = nhomLoaiHinh;
            tkmdForm.NhomLoaiHinh = nhomLoaiHinh;
            //tkmdForm.MdiParent = this;
            tkmdForm.HDGC = this.HD;
            tkmdForm.ShowDialog();
            if (nhomLoaiHinh.StartsWith("N"))
            {
                dgTKN.DataSource = this.HD.GetTKNK();
                dgTKN.Refetch();
            }
            else
            {
                dgTKX.DataSource = this.HD.GetTKXK();
                dgTKX.Refresh();
            }
        }
        private void show_PhuKienGCSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhuKienGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            //HopDongManageForm f = new HopDongManageForm();
            //f.IsBrowseForm = true;
            //f.IsDaDuyet = true;
            //f.ShowDialog();

            Company.Interface.GC.PhuKienGCDaDangKyForm pkgcSendForm;
            pkgcSendForm = new Company.Interface.GC.PhuKienGCDaDangKyForm();
            boolFlag = true;
            pkgcSendForm.boolFlag = this.boolFlag;
            pkgcSendForm.HD.ID = this.HD.ID;
            pkgcSendForm.ShowDialog();
            dgPhuKien.DataSource = HD.GetPK();


        }
        private void show_ToKhaiGCChuyenTiepNhap(string nhomLoaiHinh)
        {
            //this.boolFlag = true;
            Form[] forms = this.MdiChildren;

            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TKCX" + nhomLoaiHinh))
                {
                    forms[i].Activate();
                    return;
                }
            }

            //Company.Interface.GC.ToKhaiGCChuyentiepNhapDaduyetForm tkgcCTNhapForm;
            //tkgcCTNhapForm = new Company.Interface.GC.ToKhaiGCChuyentiepNhapDaduyetForm();
            //tkgcCTNhapForm.NhomLoaiHinh = nhomLoaiHinh;
            //tkgcCTNhapForm.Name = "TKCX" + nhomLoaiHinh;
            //tkgcCTNhapForm.OpenType = OpenFormType.Insert;
            //tkgcCTNhapForm.HDGC = this.HD;
            //tkgcCTNhapForm.ShowDialog();
            ToKhaiGCChuyenTiepNhapForm tkct = new ToKhaiGCChuyenTiepNhapForm();
            tkct.NhomLoaiHinh = nhomLoaiHinh;
            tkct.Name = "TKCX" + nhomLoaiHinh;
            tkct.OpenType = OpenFormType.Insert;
            tkct.TKCT.IDHopDong = HD.ID;
            tkct.TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
            tkct.isByHand = true;
            tkct.ShowDialog(this);
            dgTKCT.DataSource = HD.GetTKCTNhap();
        }

        private void show_KhaiBaoDMCU()
        {
            NPLCungUngTheoTKRegisterForm nplCUSendForm;
            this.boolFlag = true;
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NPLCungUngTheoTKSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplCUSendForm = new NPLCungUngTheoTKRegisterForm();
            nplCUSendForm.HD = this.HD;
            nplCUSendForm.ShowDialog();
            dgNPLCungUng.DataSource = ViewNPLCungUng();
            try
            {
                dgNPLCungUng.Refetch();
            }
            catch (System.Exception ex)
            {
                dgNPLCungUng.Refetch();
            }
            dgNPLCungUng.Refetch();
        }
        private void btnNLuu_Click(object sender, EventArgs e)
        {
            switch (tabHopDong.SelectedIndex)
            {
                case 0:

                    break;
                case 1:

                    break;
                case 2:

                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    break;

            }
        }


        private void Save()
        {

        }
        private void btnNXoa_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            switch (tabHopDong.SelectedIndex)
            {
                case 0:
                    break;
                case 1:

                    break;
                case 2:

                    break;
                case 4:
                    XoaTKN();
                    break;
                case 5:
                    XoaTKX();
                    break;
                case 6:
                    XoaPhuKien();
                    break;
                case 7:
                    XoaTKCT();
                    break;
                case 8:
                    XoaTKCTXuat();
                    break;
                //case 9:
                //    XoaNPLCungUng();
                //    break;
                case 10:
                    break;
            }
            tabHopDong_SelectedTabChanged(null, null);
        }

        private void btn_Click(object sender, EventArgs e)
        {
            UIButton button = (UIButton)sender;
            switch (button.Name)
            {
                case "btnXemTKNNPL":
                    XemTKN_NPL();
                    break;
                case "btnXemTKTXNPL":
                    XemTKTX_NPL();
                    break;
                case "btnXemPKNPL":
                    XemPKChuaNPL();
                    break;
                case "btnXemTKXSP":
                    XemTKXSP();
                    break;
                case "btnXemDMSP":
                    XemDMSP();
                    break;
                case "btnXemPKSP":
                    XemPKSP();
                    break;
                case "btnXemChiTietTK":
                    xemChiTietTKX_Click(null, null);
                    break;

                case "btnXemLuongNPLCungUngTK":
                    xemNPLCungUngItem_Click(null, null);
                    break;

                case "btnXemLuongNPLXuatTK":
                    XemLuongNPLTKX();
                    break;

                case "btnXemTKCTNPL":
                    XemTKCTNPL();
                    break;
                case "btnXemTKCTSP":
                    XemTKCTSP();
                    break;
                case "btnInNPL":
                    InNPL();
                    break;
                case "btnInSP":
                    InSP();
                    break;
            }
        }

        private void InSP()
        {
            gridEXPrintSP.Print();
        }

        private void InNPL()
        {
            gridEXPrintNPL.DefaultPageSettings.Landscape = true;
            gridEXPrintNPL.Print();
        }

        private void XemTKCTSP()
        {
            GridEXRow row = dgSanPham.GetRow();
            if (row == null || row.RowType != RowType.Record) return;
            string maNPL = row.Cells["Ma"].Value.ToString();
            Company.Interface.ToKhaiMauDichRegistedForm f = new Company.Interface.ToKhaiMauDichRegistedForm();
            f.Text = "Danh sách tờ khai chuyển tiếp SP '" + maNPL + "'";
            f.dt = this.HD.GetToKhaiChuyenTiepSP(maNPL);
            f.ShowDialog();
        }

        private void XemTKCTNPL()
        {
            GridEXRow row = dgNguyenPhuLieu.GetRow();
            if (row == null) return;
            string maNPL = row.Cells["Ma"].Value.ToString();
            Company.Interface.ToKhaiMauDichRegistedForm f = new Company.Interface.ToKhaiMauDichRegistedForm();
            f.Text = "Danh sách tờ khai chuyển tiếp NPL '" + maNPL + "'";
            f.dt = this.HD.GetToKhaiChuyenTiepNPL(maNPL);
            f.ShowDialog();
        }
        private void XoaTKN()
        {
            GridEXSelectedItemCollection items = dgTKN.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các tờ khai này không?", true) == "Yes")
                {

                    foreach (GridEXSelectedItem item in items)
                    {
                        ToKhaiMauDich tkmd = (ToKhaiMauDich)item.GetRow().DataRow;
                        if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.MaHaiQuan, (short)tkmd.NgayDangKy.Year, tkmd.IDHopDong))
                        {
                            ShowMessage("Tờ khai này có id= " + tkmd.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                            continue;
                        }
                        else
                            tkmd.Delete();
                    }
                    dgTKN.DataSource = this.HD.GetTKNK();
                }


            }
        }
        private void XoaTKX()
        {
            GridEXSelectedItemCollection items = dgTKX.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các tờ khai này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        ToKhaiMauDich tkmd = (ToKhaiMauDich)item.GetRow().DataRow;

                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(tkmd.ID))
                        {
                            //showMsg("MSG_ALL01");
                            ShowMessage("Tờ khai có id = " + tkmd.ID + " này đã được phân bổ nên không thể xóa được.", false);
                            continue;
                        }

                        else tkmd.Delete();
                    }

                    dgTKX.DataSource = HD.GetTKXK();
                }

            }
        }
        private void XoaTKCT()
        {
            GridEXSelectedItemCollection items = dgTKCT.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các tờ khai chuyển tiếp này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)item.GetRow().DataRow;
                        if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap((int)tkct.SoToKhai, tkct.MaLoaiHinh, tkct.MaHaiQuanTiepNhan, (short)tkct.NgayDangKy.Year, tkct.IDHopDong))
                        {
                            ShowMessage("Tờ khai chuyển tiếp có id= " + tkct.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                            continue;
                        }
                        tkct.Delete();
                    }
                    dgTKCT.DataSource = HD.GetTKCTNhap();
                }

            }
        }
        private void XoaTKCTXuat()
        {
            GridEXSelectedItemCollection items = dgTKCTXuat.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các tờ khai chuyển tiếp này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)item.GetRow().DataRow;
                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(tkct.ID))
                        {
                            //showMsg("MSG_ALL01");
                            ShowMessage("Tờ khai chuyển tiếp có id = " + tkct.ID + " này đã được phân bổ nên không thể xóa được.", false);
                            continue;
                        }
                        tkct.Delete();
                    }
                    dgTKCTXuat.DataSource = HD.GetTKCTXuat();
                }

            }
        }
        private void XoaPhuKien()
        {
            GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các phụ kiện này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        PhuKienDangKy pkdk = (PhuKienDangKy)item.GetRow().DataRow;
                        pkdk.Delete();
                    }
                }
                dgPhuKien.DataSource = this.HD.GetPK();
            }
        }
        private void XoaNPLCungUng()
        {
            GridEXSelectedItemCollection items = dgNPLCungUng.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các nguyên phụ liệu cung ứng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        BKCungUngDangKy bkcu = (BKCungUngDangKy)item.GetRow().DataRow;
                        bkcu.Delete();
                    }
                }
                dgNPLCungUng.DataSource = this.HD.GetNPLCU();
            }
        }

        private void dgTKN_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgTKN.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các tờ khai nhập này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        ToKhaiMauDich tkmd = (ToKhaiMauDich)item.GetRow().DataRow;
                        if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.MaHaiQuan, (short)tkmd.NgayDangKy.Year, tkmd.IDHopDong))
                        {
                            ShowMessage("Tờ khai này có id= " + tkmd.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                            continue;
                        }
                        else
                            tkmd.Delete();
                    }
                    dgTKN.DataSource = HD.GetTKNK();
                }


            }
        }

        private void dgPhuKien_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các phụ kiện này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        PhuKienDangKy pkdk = (PhuKienDangKy)item.GetRow().DataRow;
                        pkdk.Delete();
                    }
                }
                else
                { dgPhuKien.DataSource = HD.GetPK(); }

            }

        }

        private void dgTKCT_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgTKCT.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các tờ khai chuyển tiếp này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)item.GetRow().DataRow;
                        if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap((int)tkct.SoToKhai, tkct.MaLoaiHinh, tkct.MaHaiQuanTiepNhan, (short)tkct.NgayDangKy.Year, tkct.IDHopDong))
                        {
                            ShowMessage("Tờ khai chuyển tiếp có id= " + tkct.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                            continue;
                        }
                        tkct.Delete();
                    }
                    dgTKCT.DataSource = HD.GetTKCTNhap();
                }


            }
        }

        private void dgNPLCungUng_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgNPLCungUng.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các nguyên phụ liệu cung ứng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        BKCungUngDangKy bkcu = (BKCungUngDangKy)item.GetRow().DataRow;
                        bkcu.Delete();
                    }
                }
                dgNPLCungUng.DataSource = HD.GetNPLCU();
            }
        }

        private void dgTKX_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgTKX.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các tờ khai xuất này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem item in items)
                    {
                        ToKhaiMauDich tkmd = (ToKhaiMauDich)item.GetRow().DataRow;

                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(tkmd.ID))
                        {
                            //showMsg("MSG_ALL01");
                            ShowMessage("Tờ khai có id = " + tkmd.ID + " này đã được phân bổ nên không thể xóa được.", false);
                            continue;
                        }

                        else tkmd.Delete();
                    }

                    dgTKX.DataSource = HD.GetTKXK();
                }
            }
        }

        private void xemTKCTSPItem_Click(object sender, EventArgs e)
        {
            XemTKCTSP();
        }

        private void xemTKCTNPLItem_Click(object sender, EventArgs e)
        {
            XemTKCTNPL();
        }

        private void btnInDMSP_Click(object sender, EventArgs e)
        {
            InDinhMuc();
        }

        private void inDMSPItem_Click(object sender, EventArgs e)
        {
            InDinhMuc();
        }
        private void InDinhMuc()
        {
            GridEXRow row = dgSanPham.GetRow();
            if (row == null || row.RowType != RowType.Record) return;
            Company.GC.BLL.GC.SanPham SP = (Company.GC.BLL.GC.SanPham)row.DataRow;
            DataTable dt = new Company.GC.BLL.GC.DinhMuc().getDinhMuc(this.HD.ID, SP.Ma);
            if (dt.Rows.Count == 0)
            {
                showMsg("MSG_WRN17");
                //ShowMessage("Sản phẩm chưa có định mức.", false);
                return;
            }
            Report.GC.DinhMucReport rp = new DinhMucReport();
            rp.dt = dt;
            rp.HD = this.HD;
            rp.BindReport(SP);
            rp.ShowPreview();
        }

        private void dgNguyenPhuLieu_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                HD.TinhToanTongNhuCauNPLTheoSoLuongDangKyCuaSanPham();
                showMsg("MSG_WRN34");
                //ShowMessage("Thực hiện thành công.", false);
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                dgNguyenPhuLieu.DataSource = npl.SelectCollectionDynamic1(this.HD.ID);

            }
            catch (Exception ex)
            {
                showMsg("MSG_2702011", ex.Message);
                //ShowMessage("Tính toán lượng NPL theo lượng sản phẩm đăng ký bị lỗi : " + ex.Message, false);
            }
        }

        private void dgTKCTXuat_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgTKCTXuat.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa các tờ khai chuyển tiếp này không?", true) == "Yes")
                {


                    foreach (GridEXSelectedItem item in items)
                    {
                        ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)item.GetRow().DataRow;
                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(tkct.ID))
                        {
                            //showMsg("MSG_ALL01");
                            ShowMessage("Tờ khai chuyển tiếp có id = " + tkct.ID + " này đã được phân bổ nên không thể xóa được.", false);
                            continue;
                        }
                        tkct.Delete();
                    }
                    dgTKCTXuat.DataSource = HD.GetTKCTXuat();
                }

            }
        }

        private void dgTKCTXuat_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ViewTKCTXuat();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ViewTKCTXuat();
        }

        private void dgThanhKhoanMoi_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (this.HD.TrangThaiThanhKhoan == 0)
            {
                showMsg("MSG_WRN18");
                //ShowMessage("Hợp đồng chưa chạy thanh khoản.", false);
                return;
            }
            if (e.Row.RowType == RowType.Record)
            {
                this.Cursor = Cursors.WaitCursor;
                string mau = e.Row.Cells["Mau"].Value.ToString();
                switch (mau)
                {

                    case "01/HSTK-GC":
                        this.ShowReport01New();
                        break;
                    case "02/HSTK-GC":
                        this.ShowReport02New();
                        break;
                    case "03/HSTK-GC":
                        this.ShowReport03New();
                        break;
                    case "04/HSTK-GC":
                        this.ShowReport04New();
                        break;
                    case "05/HSTK-GC":
                        this.ShowReport05New();
                        break;
                    case "06/HSTK-GC":
                        this.ShowReport06New();
                        break;
                    case "07/HSTK-GC":
                        this.ShowReport07New();
                        break;
                    case "04/HSTK-GC/PhanBo":
                        this.ShowReport04New1();
                        break;
                    case "06/HSTK-GC/PhanBo":
                        this.ShowReport06New1();
                        break;
                    //Theo TT 74
                    case "01/HSTK-GC TT74":
                        this.ShowReport01TT117();
                        break;
                    case "02/HSTK-GC TT74":
                        this.ShowReport02TT74();
                        break;
                    case "03/HSTK-GC TT74":
                        this.ShowReport03TT74();
                        break;
                    case "04/HSTK-GC TT74":
                        this.ShowReport04TT74();
                        break;
                    case "04/HSTK-GC TT74/PhanBo":
                        this.ShowReport04TT74PhanBo(false);
                        break;
                    case "08/HSTK-GC TT74":
                        this.ShowReport08TT74();
                        break;
                    //TT 117
                    case "01/HSTK-GC/2011 TT117":
                        this.ShowReport01TT117();
                        break;
                    case "02/HSTK-GC/2011 TT117":
                        this.ShowReport02TT117();
                        break;
                    case "03/HSTK-GC/2011 TT117":
                        this.ShowReport03TT117();
                        break;
                    case "04/HSTK-GC/2011 TT117":
                        this.ShowReport04TT74PhanBo(true);
                        break;
                    case "05/HSTK-GC/2011 TT117":
                        this.ShowReport05TT117();
                        break;
                    case "06/HSTK-GC/2011 TT117":
                        this.ShowReport06New1();
                        break;
                    case "07/HSTK-GC/2011 TT117":
                        this.ShowReport07TT117();
                        break;
                    case "09/HSTK-GC/2011 TT117":
                        this.ShowReport09TT117();
                        break;
                }
                this.Cursor = Cursors.Default;
            }
        }

        private void ShowReport10New()
        {
            ReportViewBC03_DMDKForm Report10 = new ReportViewBC03_DMDKForm();
            Report10.HD = this.HD;
            Report10.Show();
        }

        private void ShowReport09New()
        {
            ReportViewBC02_NVLCUForm Report09 = new ReportViewBC02_NVLCUForm();
            //Report09.HD = this.HD;
            Report09.Show();
        }

        private void ShowReport08New()
        {
            ReportViewBC01_DKNVLForm Report08 = new ReportViewBC01_DKNVLForm();
            Report08.HD = this.HD;
            Report08.Show();
        }

        private DataSet CreateReport02New()
        {
            DataSet ds = new DataSet();
            return ds;
        }
        private DataSet CreateReport03New()
        {
            DataSet ds = new DataSet();
            return ds;
        }
        private DataSet CreateReport04New()
        {
            DataSet ds = new DataSet();
            return ds;
        }
        private void ShowReport01New()
        {
            ReportViewBC01NewForm Report01New = new ReportViewBC01NewForm();
            Report01New.HD = this.HD;
            Report01New.Show();
        }
        private void ShowReport02New()
        {
            ReportViewBC02NewForm Report02New = new ReportViewBC02NewForm();
            Report02New.HD = this.HD;
            Report02New.Show();
        }
        private void ShowReport03New()
        {
            ReportViewBC03NewForm Report03New = new ReportViewBC03NewForm();
            Report03New.HD = this.HD;
            Report03New.Show();
        }
        private void ShowReport04New()
        {
            ReportViewBC04NewForm Report04New = new ReportViewBC04NewForm();
            Report04New.HD = this.HD;
            Report04New.Show();
        }
        private void ShowReport04New1()
        {
            ReportViewBC04New1Form Report04New = new ReportViewBC04New1Form();

            Report04New.HD = this.HD;
            Report04New.Show();
            Report04New.Activate();
        }
        private void ShowReport05New()
        {
            ReportViewBC05NewForm Report05New = new ReportViewBC05NewForm();
            Report05New.ds = CreatSchemaDataSetRP05New();
            Report05New.HD = this.HD;
            Report05New.Show();
        }
        private void ShowReport06New()
        {
            this.ShowReport06();
        }
        private void ShowReport06New1()
        {
            this.ShowReport061();
        }
        private void ShowReport07TT117()
        {
            this.Cursor = Cursors.WaitCursor;
            ReportViewBC07_SHTK_TT117Form rpt = new ReportViewBC07_SHTK_TT117Form();
            rpt.HD = this.HD;
            BKToKhai tokhai = new BKToKhai();
            rpt.dsBK = tokhai.BaoCao07TT117(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            this.Cursor = Cursors.Default;
            rpt.Show();
        }
        private void ShowReport09TT117()
        {
            this.Cursor = Cursors.WaitCursor;
            ReportViewBC09_SHTK_TT117Form rpt = new ReportViewBC09_SHTK_TT117Form();
            rpt.HD = this.HD;
            BKToKhai tokhai = new BKToKhai();
            rpt.dsBK = tokhai.BaoCao09TT117(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            this.Cursor = Cursors.Default;
            rpt.Show();

        }
        private void ShowReport061()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dt = new DataTable("BangThanhKhoan");
            DataColumn[] dcCol = new DataColumn[10];
            dcCol[0] = new DataColumn("HopDong_ID", typeof(long));
            dcCol[1] = new DataColumn("Ten", typeof(string));
            dcCol[2] = new DataColumn("DVT", typeof(string));
            dcCol[3] = new DataColumn("TongLuongNK", typeof(decimal));
            dcCol[4] = new DataColumn("TongLuongCU", typeof(decimal));
            dcCol[5] = new DataColumn("TongLuongXK", typeof(decimal));
            dcCol[6] = new DataColumn("ChenhLech", typeof(decimal));
            dcCol[7] = new DataColumn("KetLuanXLCL", typeof(string));
            dcCol[8] = new DataColumn("OldHD_ID", typeof(long));
            dcCol[9] = new DataColumn("STT", typeof(int));
            dt.Columns.AddRange(dcCol);
            int STT = 1;
            foreach (Company.GC.BLL.GC.NguyenPhuLieu NPL in this.HD.GetNPL())
            {

                DataRow dr = dt.NewRow();
                dr["HopDong_ID"] = 0;
                dr["Ten"] = NPL.Ten + " (" + NPL.Ma + ")";
                dr["DVT"] = DonViTinh.GetName(NPL.DVT_ID);
                dr["TongLuongNK"] = NPL.SoLuongDaNhap;
                dr["TongLuongXK"] = NPL.SoLuongDaDung;
                decimal luongCU = NPL.GetNPLCungUngPhanBo();
                dr["TongLuongCU"] = luongCU;
                dr["ChenhLech"] = NPL.SoLuongDaNhap + luongCU - NPL.SoLuongDaDung;
                dr["KetLuanXLCL"] = "";
                dr["OldHD_ID"] = NPL.HopDong_ID;
                dr["STT"] = STT;
                STT++;
                dt.Rows.Add(dr);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            ReportViewBC06Form f = new ReportViewBC06Form();
            f.HD = this.HD;
            f.dsBK = ds;
            this.Cursor = Cursors.Default;
            f.Show();

        }
        private void ShowReport07New()
        {
            ReportViewBC07FormRieKer Report07New = new ReportViewBC07FormRieKer();
            Report07New.HD = this.HD;
            Report07New.Show();
        }

        private void ShowReport01TT117()
        {
            ReportViewBC01TT74Form Report01New = new ReportViewBC01TT74Form();
            Report01New.HD = this.HD;
            Report01New.Show();
        }

        private void ShowReport02TT74()
        {
            ReportViewBC02TT74Form Report02New = new ReportViewBC02TT74Form();
            Report02New.HD = this.HD;
            Report02New.Show();
        }
        private void ShowReport02TT117()
        {
            ReportViewBC02_HSTK_GC_TT117Form rpt = new ReportViewBC02_HSTK_GC_TT117Form();
            rpt.HD = this.HD;
            rpt.Show();
        }
        private void ShowReport03TT117()
        {
            ReportViewBC03_HSTK_GC_TT117Form rpt = new ReportViewBC03_HSTK_GC_TT117Form();
            rpt.HD = this.HD;
            rpt.Show();
        }
        //private void ShowReport04TT117()
        //{
        //    ReportViewBC04_HSTK_GC_TT117Form rpt = new ReportViewBC04_HSTK_GC_TT117Form();
        //    rpt.HD = this.HD;
        //    rpt.Show();
        //}
        private void ShowReport05TT117()
        {
            ReportViewBC05TT74 rpt = new ReportViewBC05TT74();
            rpt.HD = this.HD;
            rpt.Show();
        }
        private void ShowReport03TT74()
        {
            ReportViewBC03TT74Form Report03New = new ReportViewBC03TT74Form();
            Report03New.HD = this.HD;
            Report03New.Show();
        }

        private void ShowReport04TT74()
        {
            ReportViewBC04TT74Form Report04New = new ReportViewBC04TT74Form();
            Report04New.HD = this.HD;
            Report04New.Show();
        }

        private void ShowReport04TT74PhanBo(bool is117)
        {
            ReportViewBC04New1TT74Form Report04New = new ReportViewBC04New1TT74Form();
            Report04New.HD = this.HD;
            Report04New.Is117 = is117;
            Report04New.Show();
        }

        private void ShowReport08TT74()
        {
            ReportViewBC08TT74Form Report08New = new ReportViewBC08TT74Form();
            Report08New.HD = this.HD;
            Report08New.Show();
        }

        private void dgNguyenPhuLieu_RecordUpdated(object sender, EventArgs e)
        {

        }

        private void dgNguyenPhuLieu_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "Ma")
            {
                string maCu = e.InitialValue.ToString();
                string maMoi = e.Value.ToString();

                if (maCu.ToLower().Trim() == maMoi.ToLower().Trim())
                {
                    e.Cancel = true;
                    return;
                }
                else if (this.HD.CheckMaNPLIsExist(maMoi))
                {
                    ShowMessage(setText("Mã NPL '" + maMoi + "' đã tồn tại.", "Material code: '" + maMoi + "' is exist."), false);
                    e.Cancel = true;
                    return;
                }
                else
                {
                    try
                    {
                        this.HD.UpdateMaNPLFull(maCu, maMoi);
                        string msg = setText("Điều chỉnh mã NPL từ mã '" + maCu + "' sang mã mới '" + maMoi + "' thành công.", "Change Material code from '" + maCu + "' to '" + maMoi + "' successfully.");
                        ShowMessage(msg, false);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi: " + ex.Message, false);
                        e.Cancel = true;
                    }
                }
            }
            else if (e.Column.Key == "Ten")
            {
                GridEXRow row = dgNguyenPhuLieu.GetRow();
                if (row == null || row.RowType != RowType.Record) return;
                Company.GC.BLL.GC.NguyenPhuLieu NPL = (Company.GC.BLL.GC.NguyenPhuLieu)row.DataRow;
                try
                {
                    this.HD.UpdateTenNPLFull(NPL.Ma, e.Value.ToString());
                    string msg = setText("Điều chỉnh tên NPL từ tên '" + e.InitialValue.ToString() + "' sang tên mới '" + e.Value.ToString() + "' thành công.", "Change Material name from '" + e.InitialValue.ToString() + "' to '" + e.Value.ToString() + "' successfully.");
                    ShowMessage(msg, false);
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                    e.Cancel = true;
                }
            }
        }

        private void dgSanPham_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "Ma")
            {
                string maCu = e.InitialValue.ToString();
                string maMoi = e.Value.ToString();

                if (maCu.ToLower().Trim() == maMoi.ToLower().Trim())
                {
                    e.Cancel = true;
                    return;
                }
                else if (this.HD.CheckMaNPLIsExist(maMoi))
                {

                    ShowMessage(setText("Mã sản phẩm '" + maMoi + "' đã tồn tại.", "Product code: '" + maMoi + "' is exist"), false);
                    e.Cancel = true;
                    return;
                }
                else
                {
                    try
                    {
                        this.HD.UpdateMaSPFull(maCu, maMoi);
                        string msg = setText("Điều chỉnh mã sản phẩm từ mã '" + maCu + "' sang mã mới '" + maMoi + "' thành công.", "Change Product code from '" + maCu + "' to '" + maMoi + "' successfully. ");
                        ShowMessage(msg, false);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi: " + ex.Message, false);
                        e.Cancel = true;
                    }
                }
            }
            else if (e.Column.Key == "Ten")
            {
                GridEXRow row = dgSanPham.GetRow();
                if (row == null || row.RowType != RowType.Record) return;
                Company.GC.BLL.GC.SanPham SP = (Company.GC.BLL.GC.SanPham)row.DataRow;
                try
                {
                    this.HD.UpdateTenSPFull(SP.Ma, e.Value.ToString());
                    string msg = setText("Điều chỉnh tên sản phẩm từ tên '" + e.InitialValue.ToString() + "' sang tên mới '" + e.Value.ToString() + "' thành công.", "Change Product name from '" + e.InitialValue.ToString() + "' to '" + e.Value.ToString() + "' successfully. ");
                    ShowMessage(msg, false);
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                    e.Cancel = true;
                }
            }
            else if (e.Column.Key == "SoLuongDangKy")
            {
                GridEXRow row = dgSanPham.GetRow();
                if (row == null || row.RowType != RowType.Record) return;
                Company.GC.BLL.GC.SanPham SP = (Company.GC.BLL.GC.SanPham)row.DataRow;
                try
                {
                    this.HD.UpdateSoLuongDangKySP(SP.Ma, Convert.ToDecimal(e.Value));
                    MLMessages("Điều chỉnh số lượng đăng ký sản phẩm thành công.", "MSG_0203093", "", false);
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                    e.Cancel = true;
                }
            }
        }

        private void xemTKXuatSPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgNguyenPhuLieu.GetRow();
            if (row == null) return;
            string maNPL = row.Cells["Ma"].Value.ToString();

            NPLSuDungTrongTKXuatForm f = new NPLSuDungTrongTKXuatForm();
            f.IDHopDong = HD.ID;
            f.MaNPL = maNPL;
            f.ShowDialog();
        }

        private void btnTinhLuongTon_Click(object sender, EventArgs e)
        {
            TinhToanNhuCauNguyenPhuLieuForm f = new TinhToanNhuCauNguyenPhuLieuForm();
            f.HD = HD;
            f.ShowDialog();
        }

        private void btnXuatExcelSanPham_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.RestoreDirectory = true;
            sfNPL.InitialDirectory = Application.StartupPath;
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog();
            if (sfNPL.FileName != "")
            {
                Stream str = sfNPL.OpenFile();
                gridEXExporterSP.Export(str);
                str.Close();
                if (showMsg("MSG_MAL08", true) == "Yes")
                //if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
        }

        private void btnXuatExcelNPL_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.RestoreDirectory = true;
            sfNPL.InitialDirectory = Application.StartupPath;
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog();
            if (sfNPL.FileName != "")
            {
                Stream str = sfNPL.OpenFile();
                gridEXExporterNPL.Export(str);
                str.Close();
                if (showMsg("MSG_MAL08", true) == "Yes")
                //if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
        }

        private DataTable ViewNPLCungUng()
        {
            NguyenPhuLieuCungUng nplCU = new NguyenPhuLieuCungUng();
            return nplCU.ViewNPLCungUng(this.HD.ID).Tables[0];
        }
    }
}