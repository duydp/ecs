using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAL0880 : BasicVNACC
    {
        public static int TongSoByte { get; set; }
        public PropertiesAttribute SOTIEPNHANHOADONDIENTU { get; set; }
        public PropertiesAttribute NGAYTHANGNAM { get; set; }
        public PropertiesAttribute NGUOISUDUNG { get; set; }
        public PropertiesAttribute MANGUOIXUATKHAU { get; set; }
        public PropertiesAttribute TENNGUOIXUATKHAU { get; set; }
        public PropertiesAttribute PHANLOAIVANCHUYEN { get; set; }
        public PropertiesAttribute PHANLOAIXUATNHAPKHAU { get; set; }
        public PropertiesAttribute SOHOADON { get; set; }
        public PropertiesAttribute SOPL { get; set; }

        public VAL0880()
        {
            SOTIEPNHANHOADONDIENTU = new PropertiesAttribute(12, typeof(int));
            NGAYTHANGNAM = new PropertiesAttribute(8, typeof(DateTime));
            NGUOISUDUNG = new PropertiesAttribute(8, typeof(string));
            MANGUOIXUATKHAU = new PropertiesAttribute(13, typeof(string));
            TENNGUOIXUATKHAU = new PropertiesAttribute(300, typeof(string));
            PHANLOAIVANCHUYEN = new PropertiesAttribute(4, typeof(string));
            PHANLOAIXUATNHAPKHAU = new PropertiesAttribute(1, typeof(string));
            SOHOADON = new PropertiesAttribute(35, typeof(string));
            SOPL = new PropertiesAttribute(10, typeof(string));
            TongSoByte = 209;
        }

    }
    public partial class EnumGroupID
    {
    }

}
