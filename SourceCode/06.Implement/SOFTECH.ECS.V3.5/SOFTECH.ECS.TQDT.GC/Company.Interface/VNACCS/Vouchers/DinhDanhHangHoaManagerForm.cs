﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class DinhDanhHangHoaManagerForm : BaseForm
    {
        public bool isBrower = false;
        public KDT_VNACCS_CapSoDinhDanh capSoDinhDanh;
        public string SoDinhDanhHangHoa;
        public DinhDanhHangHoaManagerForm()
        {
            InitializeComponent();
        }

        private void DinhDanhHangHoaManagerForm_Load(object sender, EventArgs e)
        {
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            clcTuNgay.Value = DateTime.Now;
            clcDenNgay.Value = clcTuNgay.Value.AddDays(30);
            btnSearch_Click(null, null);
            if (isBrower)
            {
                cbStatus.SelectedIndex = 1;
                cbbLoaiDoiTuong.SelectedIndex = 0;
                cbbLoaiHH.SelectedIndex = 1;
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            string LoaiDoiTuong = e.Row.Cells["LoaiDoiTuong"].Value.ToString();
            switch (LoaiDoiTuong)
            {
                case "1":
                    e.Row.Cells["LoaiDoiTuong"].Text = "Doanh nghiệp xuất nhập khẩu";
                    break;
                case "2":
                    e.Row.Cells["LoaiDoiTuong"].Text = "Doanh nghiệp kho, bãi, cảng";
                    break;
                default:
                    break;
            }
            string LoaiTTHH = e.Row.Cells["LoaiTTHH"].Value.ToString();
            switch (LoaiTTHH)
            {
                case "1":
                    e.Row.Cells["LoaiTTHH"].Text = "Nhập khẩu";
                    break;
                case "2":
                    e.Row.Cells["LoaiTTHH"].Text = "Xuất khẩu";
                    break;
                case "3":
                    e.Row.Cells["LoaiTTHH"].Text = "Hàng nội địa";
                    break;
                default:
                    break;
            }
            if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
            {
                string pl = e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString();
                if (pl == "1")
                    e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Xanh";
                else if (pl == "2")
                    e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Vàng";
                else if (pl == "3")
                    e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Đỏ";
                else
                    e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Chưa phân luồng";
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (capSoDinhDanh==null)
                    {
                        capSoDinhDanh = new KDT_VNACCS_CapSoDinhDanh();
                    }
                    capSoDinhDanh.ID = Convert.ToInt64(dgList.GetRow().Cells["ID"].Value.ToString());
                    capSoDinhDanh = KDT_VNACCS_CapSoDinhDanh.Load(capSoDinhDanh.ID);
                    if (isBrower)
                    {
                        SoDinhDanhHangHoa = capSoDinhDanh.SoDinhDanh;
                        this.Close();
                    }
                    else
                    {
                        CapSoDinhDanhHangHoaForm f = new CapSoDinhDanhHangHoaForm();
                        f.capSoDinhDanh = capSoDinhDanh;
                        f.ShowDialog(this);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " 1= 1 ";
                if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code.ToString()))
                    where += " AND CSDD.MaHQ ='" + ctrCoQuanHaiQuan.Code.ToString() + "'";
                if (!String.IsNullOrEmpty(txtSoTN.Text.ToString()))
                    where += " AND CSDD.SoDinhDanh LIKE '%" + txtSoTN.Text.ToString() + "%'";
                if (cbStatus.SelectedValue != null)
                    where += " AND CSDD.TrangThaiXuLy =" + cbStatus.SelectedValue.ToString() + "";
                if (cbbLoaiDoiTuong.SelectedValue != null)
                    where += " AND CSDD.LoaiDoiTuong =" + cbbLoaiDoiTuong.SelectedValue.ToString() + "";
                if (cbbLoaiHH.SelectedValue != null)
                    where += " AND CSDD.LoaiTTHH =" + cbbLoaiHH.SelectedValue.ToString() + "";

                where += " AND CSDD.NgayTiepNhan BETWEEN '" + clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59") + "'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1";
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = KDT_VNACCS_CapSoDinhDanh.SelectDynamic(GetSearchWhere(),"").Tables[0];
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH KHAI BÁO ĐỊNH DANH HÀNG HÓA TỪ NGÀY_" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + " ĐẾN NGÀY _" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Doanh nghiệp có muốn xoá dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem row in items)
                    {
                        if (row.RowType == RowType.Record)
                        {
                            capSoDinhDanh = new KDT_VNACCS_CapSoDinhDanh();
                            capSoDinhDanh = (KDT_VNACCS_CapSoDinhDanh)row.GetRow().DataRow;
                            capSoDinhDanh.Delete();
                        }
                    }
                    btnSearch_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
