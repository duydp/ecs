﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAJP030 : BasicVNACC
    {

        public List<VAJP030_HANG> HangHoa { get; set; }
        public void LoadVAJP030(string strResult)
        {
            try
            {
                this.GetObject<VAJP030>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAJP030, false, VAJP030.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAJP030.TongSoByte);
                while (true)
                {
                    VAJP030_HANG ivaHang = new VAJP030_HANG();
                    ivaHang.GetObject<VAJP030_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAJP030_HANG", false, VAJP030_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAJP030_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAJP030_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAJP030_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAJP030_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
