﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.BLL.DuLieuChuan;
using Company.Interface.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Collections.Generic;


namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucManageForm : BaseForm
    {
        /// <summary>
        /// Dùng cho kết quả tìm kiếm.
        /// </summary>
        private DinhMucDangKyCollection dmDangKyCollection = new DinhMucDangKyCollection();
        private DinhMucDangKyCollection tmpCollection = new DinhMucDangKyCollection();

        /// <summary>
        /// Thông tin sản phẩm đang dược chọn.
        /// </summary>
        /// 
        private readonly DinhMucDangKy currentDMDangKy = new DinhMucDangKy();
        private string xmlCurrent = "";

        public DinhMucManageForm()
        {
            this.InitializeComponent();
        }
        private int CheckNPLVaSPDuyet(DinhMucCollection collection, string mahaiquan)
        {
            foreach (DinhMuc dm in collection)
            {
                BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.Ma = dm.MaNguyenPhuLieu;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = mahaiquan;
                if (!npl.Load())
                    return 0;
                BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = dm.MaSanPham;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = mahaiquan;
                if (!sp.Load())
                    return 1;
            }
            return 2;
        }
        public DinhMucDangKy dmDangKy = new DinhMucDangKy();

        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            DinhMucDangKy dmDangKy = new DinhMucDangKy();
            MsgSend sendXML = new MsgSend();
            string password = "";
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;

                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                if (sendXML.Load())
                {
                    MLMessages("Định mức đã gửi thông tin tới hải quan nhưng chưa có thông tin phản hồi.\nHãy chọn chức năng 'Nhận dữ liệu' cho định mức này.", "MSG_SEN03", "", false);
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    return;
                }
            }
            else
            {
                MLMessages("Chưa chọn thông tin để gửi", "MSG_REC02", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dmDangKy.LoadDMCollection();

                if (dmDangKy.DMCollection.Count == 0)
                {
                    this.Cursor = Cursors.Default;
                    MLMessages("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;

                xmlCurrent = dmDangKy.WSSendXML(password);

                this.Cursor = Cursors.Default;

                sendXML = new MsgSend();
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = dmDangKy.ID;
                                //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                //hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.KHAI_BAO;
                                //hd.PassWord = password;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                                GlobalSettings.PassWordDT = "";
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách DM. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        //-----------------------------------------------------------------------------------------
        private void DinhMucManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.khoitao_DuLieuChuan();

                this.cbStatus.SelectedIndex = 0;

                //An nut Xac nhan
                XacNhan.Visible = InheritableBoolean.False;
                xacnhanToolStripMenuItem.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            setCommandStatus();
            saveFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            // Đơn vị Hải quan.
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            txtNamTiepNhan.Value = DateTime.Today.Year;

        }

        //-----------------------------------------------------------------------------------------      
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            this.Cursor = Cursors.WaitCursor;
            string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
            where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

            if (this.txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan like '%" + this.txtSoTiepNhan.Value + "%'";
            }

            if (Convert.ToInt32(this.cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                if (this.txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND YEAR(NgayTiepNhan) = " + this.txtNamTiepNhan.Value;
                }
            }

            where += " AND TrangThaiXuLy = " + this.cbStatus.SelectedValue;

            // Thực hiện tìm kiếm.
            this.dmDangKyCollection = DinhMucDangKy.SelectCollectionDynamic(where, "");
            this.dgList.DataSource = this.dmDangKyCollection;


            // Định dạng kết quả.
            this.setCommandStatus();

            this.currentDMDangKy.TrangThaiXuLy = Convert.ToInt32(this.cbStatus.SelectedValue);
            this.Cursor = Cursors.Default;
        }


        private void setCommandStatus()
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            DongBoDuLieu.Enabled = InheritableBoolean.True;
            if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET)
            {
                this.dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                this.dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                this.cmdSend.Enabled = InheritableBoolean.False;
                this.cmdSingleDownload.Enabled = InheritableBoolean.True;
                this.cmdCancel.Enabled = InheritableBoolean.True;
                this.cmdXoa.Enabled = InheritableBoolean.False;
                XoaToolStripMenuItem.Enabled = false;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = true;
                cmdXuatDinhMucChoPhongKhai.Enabled = InheritableBoolean.False;
                khaibaoCTMenu.Enabled = false;
                NhanDuLieuCTMenu.Enabled = true;
                HuyCTMenu.Enabled = true;
                PhieuTNMenuItem.Enabled = true;
            }
            else if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                this.dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                this.dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                this.cmdSend.Enabled = InheritableBoolean.False;
                this.cmdSingleDownload.Enabled = InheritableBoolean.False;
                this.cmdCancel.Enabled = InheritableBoolean.False;
                this.cmdXoa.Enabled = InheritableBoolean.False;
                XoaToolStripMenuItem.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhan.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                mnuCSDaDuyet.Enabled = false;
                cmdXuatDinhMucChoPhongKhai.Enabled = InheritableBoolean.False;
                khaibaoCTMenu.Enabled = false;
                NhanDuLieuCTMenu.Enabled = false;
                HuyCTMenu.Enabled = false;
                InPhieuTN.Enabled = InheritableBoolean.True;
                PhieuTNMenuItem.Enabled = true;
            }
            else if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                this.dgList.RootTable.Columns["SoTiepNhan"].Visible = false;
                this.dgList.RootTable.Columns["NgayTiepNhan"].Visible = false;
                this.cmdSend.Enabled = InheritableBoolean.True;
                this.cmdSingleDownload.Enabled = InheritableBoolean.False;
                this.cmdCancel.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                this.cmdXoa.Enabled = InheritableBoolean.True;
                XoaToolStripMenuItem.Enabled = true;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = true;
                cmdXuatDinhMucChoPhongKhai.Enabled = InheritableBoolean.True;
                khaibaoCTMenu.Enabled = true;
                NhanDuLieuCTMenu.Enabled = false;
                HuyCTMenu.Enabled = false;
                InPhieuTN.Enabled = InheritableBoolean.False;
                PhieuTNMenuItem.Enabled = false;
            }
            else if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                this.dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                this.dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                this.cmdSend.Enabled = InheritableBoolean.True;
                this.cmdSingleDownload.Enabled = InheritableBoolean.False;
                this.cmdCancel.Enabled = InheritableBoolean.True;
                this.cmdXoa.Enabled = InheritableBoolean.False;
                XoaToolStripMenuItem.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                mnuCSDaDuyet.Enabled = false;
                cmdXuatDinhMucChoPhongKhai.Enabled = InheritableBoolean.False;
                khaibaoCTMenu.Enabled = true;
                NhanDuLieuCTMenu.Enabled = true;
                HuyCTMenu.Enabled = true;
                InPhieuTN.Enabled = InheritableBoolean.True;
                PhieuTNMenuItem.Enabled = true;
            }
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)))
                {
                    uiCommandBar1.Visible = false;
                    this.cmMain.SetContextMenu(dgList, null);
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    DongBoDuLieu.Enabled = InheritableBoolean.False;
                }
            }
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString());
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {

                    case -2:
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt có sửa chữa";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait to approve(can update)";
                        break;
                    case -1:
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not declarate yet";
                        break;
                    case 0:
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait to approve ";
                        break;
                    case 1:
                        // e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Approved";
                        break;
                    case 2:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = " Not Approved";
                        break;

                }

                //TODO: Cao Hữu Tú updated:15-09-2011
                //Contents: bổ sung cột mã sản phẩm vào Grid Định mức đăng ký
                //Methods: dựa vào ID trong dmDangKyCollection so sánh với master_id trong  DinhMucCollection
                //         lấy ra mã sản phẩm và gán vào cột Mã Sản Phẩm

                Company.BLL.KDT.SXXK.DinhMucCollection DinhMucCollection = new DinhMucCollection();
                int ValueIdCell = Int32.Parse(e.Row.Cells["ID"].Value.ToString());
                string TatCaMaSanPham = "";

                DinhMucCollection = Company.BLL.KDT.SXXK.DinhMuc.SelectCollectionBy_Master_ID(ValueIdCell);
                Company.BLL.KDT.SXXK.DinhMuc EntityDinhMuc = new DinhMuc();
                if (DinhMucCollection.Count > 0)
                {


                    //lấy mã đầu tiên trong DinhMucCollection đưa vào Cột MaSanPham
                    EntityDinhMuc = DinhMucCollection[0];
                    e.Row.Cells["MaSanPham"].Text = EntityDinhMuc.MaSanPham;

                    //lấy mã sản phẩm bắt đầu từ phần tử thứ 2 đưa vào 
                    foreach (Company.BLL.KDT.SXXK.DinhMuc EntityDM in DinhMucCollection)
                    {

                        TatCaMaSanPham = TatCaMaSanPham + EntityDM.MaSanPham + "\n";

                    }
                    e.Row.Cells["MaSanPham"].ToolTipText = TatCaMaSanPham;
                }
            }
        }


        /// <summary>
        /// Hủy thông tin đã đăng ký.
        /// </summary>
        private void cancel()
        {
            DinhMucDangKy dmDangKy = new DinhMucDangKy();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                if (sendXML.Load())
                {
                    // ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    //thoilv
                    // Message("MSG_SEN03","", false); 
                    MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                    return;
                }
            }
            else
            {
                //ShowMessage("Chưa chọn thông tin để hủy.", false);
                //thoilv
                // Message("MSG_CNL01","",false);
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    this.Cursor = Cursors.WaitCursor;
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    xmlCurrent = dmDangKy.WSCancelXML(password);
                    this.Cursor = Cursors.Default;
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = "DM";
                    sendXML.master_id = dmDangKy.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password); ;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Hủy thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = dmDangKy.ID;
                                //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                //hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                                //hd.PassWord = password;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                                GlobalSettings.PassWordDT = "";
                        }
                    }
                    else
                    {
                        // ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi Hủy danh sách DM. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------

        private void download()
        {
            DinhMucDangKy dmDangKy = new DinhMucDangKy();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                if (!sendXML.Load())
                {
                    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                    //MLMessages("Định mức chưa được gửi thông tin tới hải quan.\nHãy chọn chức năng 'Khai báo' cho định mức này.", "MSG_SEN03", "", false);
                    //return;
                }
            }
            else
            {
                MLMessages("Chưa chọn thông tin định mức.", "MSG_REC01", "", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                int ttxl = dmDangKy.TrangThaiXuLy;
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;

                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                //xmlCurrent = dmDangKy.WSRequestXML(password);

                this.Cursor = Cursors.Default;
                sendXML = new MsgSend();
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Nhận dữ liệu không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = dmDangKy.ID;
                                //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                //hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                                //hd.PassWord = password;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //  ShowMessage("Có lỗi trong nhận thông tin : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                                GlobalSettings.PassWordDT = "";
                        }
                    }
                    else
                    {
                        // ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo danh sách DM. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void sendItemsSelect()
        {
            FormSendDinhMuc f = new FormSendDinhMuc();
            f.btnSend.Enabled = true;
            f.ShowDialog();
            this.search();
        }
        private void XacNhanThongTin()
        {
            FormSendDinhMuc f = new FormSendDinhMuc();
            f.btnXacNhan.Enabled = true;
            f.ShowDialog();
            this.search();
        }
        private void cancelItemsSelect()
        {
            FormSendDinhMuc f = new FormSendDinhMuc();
            f.btnHuy.Enabled = true;
            f.ShowDialog();
            this.search();
        }
        //-----------------------------------------------------------------------------------------
        private void downloadItemsSelect()
        {
            FormSendDinhMuc f = new FormSendDinhMuc();
            f.btnNhan.Enabled = true;
            f.ShowDialog();
            this.search();
        }
        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSingleDownload":
                    this.download();
                    break;
                case "cmdCancel":
                    this.cancelItemsSelect();
                    break;
                case "cmdSend":
                    this.sendItemsSelect();
                    break;
                case "XacNhan":
                    this.XacNhanThongTin();
                    break;
                case "DinhMuc":
                    this.InDinhMuc();
                    break;
                case "InDM":
                    this.InDinhMuc();
                    break;
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "Delete":
                    this.Delete();
                    break;
                case "cmdCSDaDuyet":
                    this.ChuyenTrangThai();
                    break;
                case "cmdXuatDinhMucChoPhongKhai":
                    XuatDinhMucChoPhongKhai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
            }
        }
        private void inPhieuTN()
        {
            if (dgList.GetRows().Length < 1) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "SẢN PHẨM";
            Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = dmDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = dmDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();
        }
        private void XuatDinhMucChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách định mức cần xuất.", false);
                //msg
                //Message("MSG_DMC01","", false);
                MLMessages("Chưa chọn danh sách định mức cần xuất. ", "MSG_DMC01", "", false);
                return;
            }
            try
            {
                DinhMucDangKyCollection col = new DinhMucDangKyCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sodinhmuc = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMucDangKy dmSelected = (DinhMucDangKy)i.GetRow().DataRow;
                            dmSelected.LoadDMCollection();
                            col.Add(dmSelected);
                            sodinhmuc++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    // ShowMessage("Xuất thành công " + sodinhmuc + " định mức.",false);
                    //msg
                    // Message("MSG_DMC02", "", false);
                    MLMessages("Xuất thành công " + sodinhmuc + " định mức.", "MSG_DMC02", sodinhmuc.ToString(), false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }

        }

        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                DinhMucDangKyCollection dmDKColl = new DinhMucDangKyCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    dmDKColl.Add((DinhMucDangKy)grItem.GetRow().DataRow);
                }

                for (int i = 0; i < dmDKColl.Count; i++)
                {
                    string msg = "";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        msg = "Bạn có muốn chuyển trạng thái của danh sách được chọn sang đã duyệt không?";
                        msg += "\n\nSố thứ tự danh sách: " + dmDKColl[i].ID.ToString();
                        //msg += "\n----------------------";
                        dmDKColl[i].LoadDMCollection();
                        msg += "\nCó " + dmDKColl[i].DMCollection.Count.ToString() + " định mức được khai báo";
                    }
                    else
                    {
                        msg = "Do you want to change the status of selected list to Approved?";
                        msg += "\n\n List number: " + dmDKColl[i].ID.ToString();
                        //msg += "\n----------------------";
                        dmDKColl[i].LoadDMCollection();
                        msg += "\n There are" + dmDKColl[i].DMCollection.Count.ToString() + "declared norm(s)";
                    }
                    if (ShowMessage(msg, true) == "Yes")
                    {
                        if (dmDKColl[i].TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            dmDKColl[i].NgayTiepNhan = DateTime.Now;
                        }
                        string fmtMsg = string.Format("Thông tin trước khi chuyển-->GUIDSTR:{0},Mã hải quan: {1},Mã doanh nghiệp: {2}," +
                              "ID: {3},Năm đăng ký:{4},Ngày tiếp nhận:{5},Số tiếp nhận:{6}",
                           new object[]{
                                                       dmDKColl[i].GUIDSTR,
                                                       dmDKColl[i].MaHaiQuan,                                                       
                                                       dmDKColl[i].MaDoanhNghiep,
                                                       dmDKColl[i].ID,
                                                       dmDKColl[i].NamDK,
                                                       dmDKColl[i].NgayTiepNhan,                                                       
                                                       dmDKColl[i].SoTiepNhan});

                        Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty, dmDKColl[i].ID, dmDKColl[i].GUIDSTR,
                        Company.KDT.SHARE.Components.MessageTypes.DanhMucSanPham,
                        Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                        Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay, fmtMsg);

                        dmDKColl[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

                        dmDKColl[i].TransferDataToSXXK();
                    }
                }
                this.search();
            }
            else
            {
                // ShowMessage("Không có dữ liệu được chọn", false);
                //msg
                MLMessages("Không có dữ liệu được chọn", "MSG_PUB01", "", false);
            }
        }
        public void XoaDinhMucDangKy(GridEXSelectedItemCollection items)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            MsgSend sendXML = new MsgSend();

            List<DinhMucDangKy> itemsDelete = new List<DinhMucDangKy>();

            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    DinhMucDangKy item = (DinhMucDangKy)i.GetRow().DataRow;
                    sendXML.LoaiHS = "DM";
                    sendXML.master_id = item.ID;
                    if (!sendXML.Load())
                    {
                        msgWarning += string.Format(" ID ={0} [Chấp nhận xóa]\r\n", item.ID);
                        itemsDelete.Add(item);
                    }
                    else
                    {
                        msgWarning += string.Format(" ID ={0} [Không thể xóa]\r\n", item.ID);
                    }
                }
            }
            msgWarning += "* Ghi chú: Những định mức [Không thể xóa] vì đã gửi thông tin đến hải quan\r\n";
            if (itemsDelete.Count == 0)
            {
                Globals.ShowMessage(msgWarning, false);
                return;
            }
            else
            {
                msgWarning += "Bạn đồng ý thực hiện không?";
                if (Globals.ShowMessage(msgWarning, true) != "Yes") return;
            }
            try
            {
                msgWarning = string.Empty;
                foreach (DinhMucDangKy item in itemsDelete)
                {
                    if (item.CloneToDB(null))
                        item.Delete();
                    else msgWarning += string.Format("ID={0} [thực hiện không thành công]", item.ID);
                }
                if (msgWarning != string.Empty)

                    Globals.ShowMessage(msgWarning, false);
            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Delete()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaDinhMucDangKy(items);
            this.search();
            #region Old

            //try
            //{
            //    this.Cursor = Cursors.WaitCursor;
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    if (items.Count > 0)
            //        //  if (ShowMessage("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", true) == "Yes")
            //        if (MLMessages("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", "MSG_DEL01", "", true) == "Yes")
            //        {

            //            foreach (GridEXSelectedItem i in items)
            //            {
            //                if (i.RowType == RowType.Record)
            //                {
            //                    DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
            //                    MsgSend sendXML = new MsgSend();
            //                    sendXML.LoaiHS = "DM";
            //                    sendXML.master_id = dmDangKySelected.ID;
            //                    if (sendXML.Load())
            //                    {
            //                        if (dmDangKySelected.SoTiepNhan != 0)
            //                            //   ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);                                    
            //                            MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_SEN03", "", false);
            //                    }
            //                    else
            //                    {
            //                        if (dmDangKySelected.ID > 0)
            //                        {
            //                            dmDangKySelected.CloneToDB(null);
            //                            dmDangKySelected.Delete();
            //                        }
            //                    }

            //                    if (dmDangKySelected.SoTiepNhan == 0 && dmDangKySelected.ID > 0)
            //                        dmDangKySelected.Delete();

            //                    //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
            //                    try
            //                    {
            //                        string where = "1 = 1";
            //                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", dmDangKySelected.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.DinhMuc);
            //                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
            //                        if (listLog.Count > 0)
            //                        {
            //                            long idLog = listLog[0].IDLog;
            //                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
            //                            long idDK = listLog[0].ID_DK;
            //                            string guidstr = listLog[0].GUIDSTR_DK;
            //                            string userKhaiBao = listLog[0].UserNameKhaiBao;
            //                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
            //                            string userSuaDoi = GlobalSettings.UserLog;
            //                            DateTime ngaySuaDoi = DateTime.Now;
            //                            string ghiChu = listLog[0].GhiChu;
            //                            bool isDelete = true;
            //                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
            //                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
            //                        }
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
            //                        return;
            //                    }
            //                }
            //            }
            //            this.search();
            //        }
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage(ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            #endregion Old
        }

        private string checkDataHangImport(DinhMucDangKy dmDangky)
        {
            string st = "";
            foreach (DinhMuc dm in dmDangky.DMCollection)
            {
                BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                ttdm.MaSanPham = dm.MaSanPham;
                ttdm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (ttdm.Load())
                {
                    if (dm.STTHang == 1)
                        st = "Danh sách có ID='" + dmDangky.ID + "'\n";
                    st += "Sản phẩm có mã '" + dm.MaSanPham + "' đã có định mức trong hệ thống.\n";
                }
            }
            return st;
        }
        private string checkDataImport(DinhMucDangKyCollection collection)
        {
            string st = "";
            foreach (DinhMucDangKy dmDangky in collection)
            {
                DinhMucDangKy dmInDatabase = DinhMucDangKy.Load(dmDangky.ID);
                if (dmInDatabase != null)
                {
                    if (dmInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        st += "Danh sách có ID=" + dmDangky.ID + " đã được duyệt.\n";
                    }
                    else
                    {
                        string tmp = checkDataHangImport(dmDangky);
                        st += tmp;
                        if (tmp == "")
                            tmpCollection.Add(dmDangky);
                    }
                }
                else
                {
                    if (dmDangky.ID > 0)
                        dmDangky.ID = 0;
                    tmpCollection.Add(dmDangky);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                tmpCollection.Clear();
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    DinhMucDangKyCollection dmDKCollection = (DinhMucDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(dmDKCollection);
                    if (st != "")
                    {
                        // if (ShowMessage("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", true) == "Yes")
                        if (MLMessages("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "MSG_PUB04", "", true) == "Yes")
                        {
                            DinhMucDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                            //  ShowMessage("Import thành công", false);
                            //msg
                            MLMessages("Import thành công", "MSG_PUB02", "", false);
                        }
                    }
                    else
                    {
                        DinhMucDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                        // ShowMessage("Import thành công", false);
                        //msg
                        //Message("MSG_PUB02", "", false);
                        MLMessages("Import thành công", "MSG_PUB02", "", false);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(" : " + ex.Message, false);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                //ShowMessage("Chưa chọn danh sách định mức", false);
                //msg
                // Message("MSG_DMC01", "", false);
                MLMessages("Chưa chọn danh sách định mức", "MSG_DMC01", "", false);
                return;
            }
            try
            {
                DinhMucDangKyCollection col = new DinhMucDangKyCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                            dmDangKySelected.LoadDMCollection();
                            col.Add(dmDangKySelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }

        }
        private void InDinhMuc()
        {
            DinhMucDangKy dmDangKy = new DinhMucDangKy();
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;
            }
            else
                return;
            Report.ReportViewDinhMucForm f = new Company.Interface.Report.ReportViewDinhMucForm();
            f.DMDangKy = dmDangKy;
            f.ShowDialog();
        }
        //-----------------------------------------------------------------------------------------
        private void LaySoTiepNhanDT()
        {
            DinhMucDangKy dmDangKy = new DinhMucDangKy();
            MsgSend sendXML = new MsgSend();
            string password = "";
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                if (!sendXML.Load())
                {
                    MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_SEN03", "", false);
                    return;
                }
            }
            else
            {
                MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_PUB01", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = dmDangKy.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    // string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }


                if (sendXML.func == 1)
                {
                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức. ", "MSG_SEN07", "", false);
                    else
                        MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmDangKy.SoTiepNhan, "MSG_SEN05", "" + dmDangKy.SoTiepNhan, false);
                    this.search();
                }
                else if (sendXML.func == 3)
                {
                    //  ShowMessage("Đã hủy danh sách sản phẩm này", false);
                    MLMessages("Đã hủy danh sách sản phẩm này ", "MSG_SEN06", "", false);
                    // Message("MSG_SEN06", "", false);
                    this.search();
                }
                else if (sendXML.func == 2)
                {
                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức. ", "MSG_SEN07", "", false);
                        this.search();
                    }
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa xử lý danh sách sản phẩm này! ", "MSG_SEN08", "", false);
                    }
                    else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu ", "MSG_SEN09", "", false);
                        this.search();
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = dmDangKy.ID;
                                //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                //hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //hd.PassWord = password;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //  ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                                sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            DinhMucDangKy dmDangKy = new DinhMucDangKy();
            try
            {

                if (dgList.GetRow() != null)
                {
                    dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;
                    dmDangKy.Load();
                    sendXML.LoaiHS = "DM";
                    sendXML.master_id = dmDangKy.ID;
                    sendXML.Load();
                    this.Cursor = Cursors.WaitCursor;

                    //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 18/02/2011
                    //Tao XML Header
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(Globals.ConfigPhongBiPhanHoi(MessgaseType.DinhMuc, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, dmDangKy.GUIDSTR));

                    //Tao Body XML
                    XmlDocument docNPL = new XmlDocument();
                    string path = EntityBase.GetPathProram();
                    docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                    XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                    root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI.Trim();
                    root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI.Trim();

                    root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN.Trim();
                    root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));
                    root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = dmDangKy.GUIDSTR.Trim();

                    XmlNode Content = xml.GetElementsByTagName("Content")[0];
                    Content.AppendChild(root);

                    xmlCurrent = dmDangKy.LayPhanHoi(pass, xml.InnerXml);
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                        if (kq == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        return;
                    }

                    if (sendXML.func == 1)
                    {
                        MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmDangKy.SoTiepNhan, "MSG_SEN05", "" + dmDangKy.SoTiepNhan, false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        MLMessages("Đã hủy danh sách định mức này", "MSG_SEN06", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN18", "", false);
                            this.search();
                        }
                        else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            MLMessages("Hải quan chưa xử lý danh sách này!", "MSG_SEN08", "", false);
                        }
                        else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN09", "", false);
                            this.search();
                        }
                        //DATLMQ bổ sung thông báo Định mức bị Hải quan từ chối ngày 08/08/2011
                        else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO && !(dmDangKy.HUONGDAN.Equals("")))
                        {
                            MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN09", "", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = dmDangKy.ID;
                                //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                //hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //hd.PassWord = pass;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                DinhMucDangKyDetailForm f = new DinhMucDangKyDetailForm();
                f.DMDangKy = (DinhMucDangKy)e.Row.DataRow;
                int ttxl = f.DMDangKy.TrangThaiXuLy;
                if (f.DMDangKy.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                {
                    f.OpenType = OpenFormType.Edit;
                }
                else
                {
                    f.OpenType = OpenFormType.View;
                }
                if (f.DMDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    f.OpenType = OpenFormType.View;
                }
                f.ShowDialog(this);
                if (ttxl != f.DMDangKy.TrangThaiXuLy) this.search();
            }
        }

        //-----------------------------------------------------------------------------------------

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                this.txtNamTiepNhan.Text = string.Empty;
                this.txtNamTiepNhan.Value = 0;
                this.txtNamTiepNhan.Enabled = false;
                this.txtSoTiepNhan.Text = string.Empty;
                this.txtSoTiepNhan.Value = 0;
                this.txtSoTiepNhan.Enabled = false;
            }
            else
            {
                this.txtNamTiepNhan.Value = DateTime.Today.Year;
                this.txtNamTiepNhan.Enabled = true;
                this.txtSoTiepNhan.Enabled = true;
            }
            btnSearch_Click(null, null);
        }

        private void DinhMucManageForm_Shown(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (e.Row.RowType == RowType.Record)
                {
                    DinhMucDangKy dmDangKySelected = (DinhMucDangKy)e.Row.DataRow;
                    if (dmDangKySelected.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                    {
                        // if (ShowMessage("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", true) == "Yes")
                        if (MLMessages("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", "MSG_DEL01", "", true) == "Yes")
                        {
                            dmDangKySelected.Delete();
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        // ShowMessage("Danh sách đã được duyệt. Không được xóa",false);
                        MLMessages("Danh sách đã được duyệt. Không được xóa", "MSG_SEN12", "", false);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                {

                    {
                        // if (ShowMessage("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", true) == "Yes")
                        if (MLMessages("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", "MSG_DEL01", "", true) == "Yes")
                        {
                            GridEXSelectedItemCollection items = dgList.SelectedItems;
                            foreach (GridEXSelectedItem i in items)
                            {
                                if (i.RowType == RowType.Record)
                                {
                                    DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                                    MsgSend sendXML = new MsgSend();
                                    sendXML.LoaiHS = "DM";
                                    sendXML.master_id = dmDangKySelected.ID;
                                    if (sendXML.Load())
                                    {
                                        //ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
                                        // Message("MSG_SEN03", "", false);
                                        MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_SEN03", "", false);
                                    }
                                    else
                                    {
                                        if (dmDangKySelected.ID > 0)
                                        {
                                            dmDangKySelected.Delete();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void khaibaoCTMenu_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                SendV3();
            else
                send();
        }

        private void NhanDuLieuCTMenu_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                LayPhanHoiV3();
            else
                download();
        }

        private void HuyCTMenu_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                cancelV3();
            else
                cancel();
        }

        private void xacnhanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaySoTiepNhanDT();
        }

        private void DinhMucMenuItem_Click(object sender, EventArgs e)
        {
            InDinhMuc();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void xóaĐịnhMứcToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void mnuCSDaDuyet_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangThai();
        }

        private void uiCommandBar1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void PhieuTNMenuItem_Click(object sender, EventArgs e)
        {
            this.inPhieuTN();
        }

        private void DinhMucMenuItem_Click_1(object sender, EventArgs e)
        {
            InDinhMuc();
        }

        private void btnSuaDM_Click(object sender, EventArgs e)
        {
            DinhMucSuaListForm f = new DinhMucSuaListForm();
            f.ShowDialog(this);
        }

        private void uiMessage_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;

            DinhMucDangKy nplDangKySelected = (DinhMucDangKy)dgList.CurrentRow.DataRow;
            Globals.KetQuaXuLyChung(nplDangKySelected.ID);
        }
        //-------------------------------------------------------------------------------------
        #region Send V3 Create by LANNT
        private void SendV3()
        {
            dmDangKy = new DinhMucDangKy();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;

                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                if (sendXML.Load())
                {
                    MLMessages("Định mức đã gửi thông tin tới hải quan nhưng chưa có thông tin phản hồi.\nHãy chọn chức năng 'Nhận dữ liệu' cho định mức này.", "MSG_SEN03", "", false);
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    return;
                }
            }
            else
            {
                MLMessages("Chưa chọn thông tin để gửi", "MSG_REC02", "", false);
                return;
            }
            dmDangKy.LoadDMCollection();
            if (dmDangKy.DMCollection.Count == 0)
            {
                MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                this.Cursor = Cursors.Default;
                return;
            }
            try
            {
                dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.SXXK_DinhMucSP dmsp = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_DinhMuc(dmDangKy,false);
                string msgSend = Company.KDT.SHARE.Components.Helpers.BuildSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dmDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                                     Identity = dmDangKy.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DINHMUC_SP,
                                    Function = Company.KDT.SHARE.Components.DeclarationFunction.KHAI_BAO,
                                    Reference = dmDangKy.GUIDSTR,
                                }
                                ,
                                dmsp, null, true);
                dmDangKy.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                sendForm.DoSend(msgSend);
                Company.KDT.SHARE.Components.Globals.XmlSaveMessage(msgSend, dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoDinhMuc);
                NhanDuLieuCTMenu.Enabled = true;
                khaibaoCTMenu.Enabled = false;
                sendXML = new MsgSend();
                sendXML.LoaiHS = "DMSP";
                sendXML.master_id = dmDangKy.ID;
                sendXML.func = 1;
                sendXML.InsertUpdate();


            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }

        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    Company.KDT.SHARE.Components.FeedBackContent feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);

                    switch (feedbackContent.AdditionalInformation.Statement.Trim())
                    {
                        case Company.KDT.SHARE.Components.DeclarationFunction.KHONG_CHAP_NHAN:
                            this.ShowMessageTQDT(feedbackContent.AdditionalInformation.Content.Text, false);
                            dmDangKy.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.CHUA_XU_LY:
                            this.ShowMessageTQDT(feedbackContent.AdditionalInformation.Content.Text, false);
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = feedbackContent.AdditionalInformation.Content.Text.Split('/');
                            string noidung = "\nSố Tiếp Nhận: " + ketqua[0] + "\nNăm Ðăng Ký: " + ketqua[1];
                            if ((dmDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Ðã Tiếp Nhận Hủy Khai Báo." + noidung;
                                dmDangKy.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                            }
                            else if (dmDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                noidung = "Ðã Tiếp Nhận Khai Báo" + noidung;
                                dmDangKy.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                            }
                            dmDangKy.SoTiepNhan = long.Parse(ketqua[0].Trim());
                            dmDangKy.NamDK = short.Parse(ketqua[1].Trim());
                            dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiNguyenPhuLieu, noidung);
                            this.ShowMessageTQDT(noidung, false);
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.THONG_QUAN:
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, feedbackContent.AdditionalInformation.Content.Text);
                            if (dmDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                this.ShowMessageTQDT("Ðã Duyệt Khai Báo", false);
                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (dmDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                this.ShowMessageTQDT("Chấp Nhận Hủy Khai Báo", false);
                                dmDangKy.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        default:
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dmDangKy.ID, "Error");
                            break;

                    }
                    dmDangKy.Update();
                }
                else
                {
                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }
        private void LayPhanHoiV3()
        {
            dmDangKy = new DinhMucDangKy();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                if (!sendXML.Load())
                {
                    MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_SEN03", "", false);
                    return;
                }
            }
            else
            {
                MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_PUB01", "", false);
                return;
            }
            dmDangKy.LoadDMCollection();
            string reference = dmDangKy.GUIDSTR;
            Company.KDT.SHARE.Components.SubjectBase subjectBase = new Company.KDT.SHARE.Components.SubjectBase
            {
                Issuer = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL,
                Reference = reference,
                Function = Company.KDT.SHARE.Components.DeclarationFunction.HOI_TRANG_THAI,
                Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL,
            };
            string msgSend = Company.KDT.SHARE.Components.Helpers.BuildFeedBack(
                                        new Company.KDT.SHARE.Components.NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = dmDangKy.MaDoanhNghiep
                                        },
                                          new Company.KDT.SHARE.Components.NameBase()
                                          {
                                              Name = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan.Trim()),
                                              Identity = dmDangKy.MaHaiQuan
                                          },
                                          subjectBase, null);
            SendMessageForm sendForm = new SendMessageForm();
            sendForm.Send += SendMessage;
            sendForm.DoSend(msgSend);
        }
        private void cancelV3()
        {
            dmDangKy = new DinhMucDangKy();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                if (sendXML.Load())
                {
                    // ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    //thoilv
                    // Message("MSG_SEN03","", false); 
                    MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                    return;
                }
            }
            else
            {
                //ShowMessage("Chưa chọn thông tin để hủy.", false);
                //thoilv
                // Message("MSG_CNL01","",false);
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            dmDangKy.LoadDMCollection();
            Company.KDT.SHARE.Components.DeclarationBase dmsp = Company.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DINHMUC_SP,dmDangKy.GUIDSTR,dmDangKy.SoTiepNhan,dmDangKy.MaHaiQuan,dmDangKy.NgayTiepNhan);
            string msgSend = Company.KDT.SHARE.Components.Helpers.BuildSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = dmDangKy.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                                 Identity = dmDangKy.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = dmDangKy.GUIDSTR,
                            }
                            ,
                            dmsp, null, true);
            SendMessageForm sendForm = new SendMessageForm();
            sendForm.Send += SendMessage;
            sendForm.DoSend(msgSend);
            Company.KDT.SHARE.Components.Globals.XmlSaveMessage(msgSend, dmDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoToKhai);
            sendXML = new MsgSend();
            sendXML.LoaiHS = "DM";
            sendXML.master_id = dmDangKy.ID;
            sendXML.msg = xmlCurrent;
            sendXML.func = 3;
            xmlCurrent = "";
            sendXML.InsertUpdate();
        }
        #endregion

    }
}

