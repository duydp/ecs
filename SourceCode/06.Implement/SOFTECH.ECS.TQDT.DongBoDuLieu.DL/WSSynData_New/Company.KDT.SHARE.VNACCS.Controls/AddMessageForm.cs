﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public partial class AddMessageForm : Form
    {
        public AddMessageForm()
        {
            InitializeComponent();
            //text = msg;
        }
        public KDT_VNACC_ToKhaiMauDich tkmd;
        public bool isCapNhatTK;
        string text;
        private void AddMessageForm_Load(object sender, EventArgs e)
        {
            richTextBox1.Text = text;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ProcessMSG(richTextBox1.Text))
                {
                    Helper.Controls.MessageBoxControlH.ShowMessage("Thêm message thành công", false);
                    this.Close();
                }

                else
                {
                    Helper.Controls.MessageBoxControlH.ShowMessage("Thêm message Thất bại", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlH.ShowMessage("Thêm message Thất bại", false);
 
            }
        
        }
        public bool ProcessMSG(string MsgFeedBack)
        {
            tkmd = null;
            isCapNhatTK = false;
            MsgLog log = new MsgLog();
            try
            {

                if (!string.IsNullOrEmpty(MsgFeedBack) && !MsgFeedBack.Contains(EnumThongBao.KetThucPhanHoi))
                {
                    #region log

                    log.Log_Messages = MsgFeedBack;
                    log.MaNghiepVu = "Return";
                    log.MessagesTag = "";
                    log.NoiDungThongBao = "";
                    log.TieuDeThongBao = "Thông tin phản hồi từ HQ";
                    log.InputMessagesID = "";
                    log.Item_id = 0;
                    log.IndexTag = "";
                    log.CreatedTime = DateTime.Now;
                    #endregion
                    //if (MsgFeedBack.Contains("\n\n")) MsgFeedBack = MsgFeedBack.Replace("\n\n", "\r\n");
                    string[] lines = MsgFeedBack.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                    ReturnMessages msgReturn = new ReturnMessages(lines);
                    if (msgReturn == null)
                    {
                        long idLog = log.Insert();
                        if (idLog == 0)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(new Exception("Không lưu được messages phản hồi"));
                            return false;
                        }
                        Logger.LocalLogger.Instance().WriteMessage(new Exception("Không xử lý được messages có ID = " + idLog));
                        return false;
                    }
                    #region Lưu msg chờ xử lý

                    MsgPhanBo msgPB = new MsgPhanBo();
                    IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic("RTPTag = '" + msgReturn.GetRTPtag().Trim() + "'", null);
                    if (listPB != null && listPB.Count > 0)
                    {
                        msgPB = listPB[0];
                        msgPB.SoTiepNhan = msgReturn.GetSoTNHeader();
                        msgPB.MessagesInputID = msgReturn.header.InputMessagesID.GetValue().ToString();
                        msgPB.messagesTag = msgReturn.header.MessagesTag.GetValue().ToString();
                        msgPB.TerminalID = msgReturn.header.TerminalID.GetValue().ToString();
                        msgPB.MaNghiepVu = msgReturn.getMaNVPhanHoi();
                        msgPB.Update();
                    }
                    else
                    {
                        long idLog = log.Insert();
                        if (idLog == 0)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(new Exception("Không lưu được messages phản hồi"));
                            return false;
                        }
                        msgPB.Master_ID = idLog;
                        msgPB.CreatedTime = DateTime.Now;
                        msgPB.SoTiepNhan = msgReturn.GetSoTNHeader();
                        msgPB.RTPTag = msgReturn.GetRTPtag();
                        msgPB.MessagesInputID = msgReturn.header.InputMessagesID.GetValue().ToString();
                        msgPB.messagesTag = msgReturn.header.MessagesTag.GetValue().ToString();
                        msgPB.TerminalID = msgReturn.header.TerminalID.GetValue().ToString();
                        msgPB.IndexTag = msgReturn.header.IndexTag.GetValue().ToString();
                        msgPB.TrangThai = EnumTrangThaiXuLyMessage.ChuaXuLy;
                        msgPB.GhiChu = "Nhập thủ công";
                        KDT_VNACC_ToKhaiMauDich TKMD = ProcessMessages.XuLyMsg(msgReturn);
                        if (TKMD != null)
                        {
                            msgPB.TrangThai = EnumTrangThaiXuLyMessage.DaXem;
                            if (TKMD.TrangThaiXuLy.Contains("3")) //Thong quan
                            {
                                tkmd = TKMD;
                                isCapNhatTK = true;
                            }
                        }
                        msgPB.MaNghiepVu = msgReturn.getMaNVPhanHoi();
                        msgPB.Insert();
                    }
                    #endregion

                    #region hiển thị thông báo
                    string ShowPopUpContent = ReturnMessages.GetThongBaoPhanHoi(msgPB.MaNghiepVu);
                    ShowPopUpContent += Environment.NewLine + "Số tờ khai: " + msgPB.SoTiepNhan;
                    ShowPopUpContent += Environment.NewLine + DateTime.Now.ToString("dd/MM/yyyy");
                    //if (this.InvokeRequired)
                    //    this.Invoke(new MethodInvoker(delegate { bindingThongBao(); PopUpThongBao(ShowPopUpContent); }));
                    //else
                    //{
                    //    bindingThongBao();
                    //    PopUpThongBao(ShowPopUpContent);
                    //}

                    #endregion

                    return true;
                }
                else
                    return false;
            }
            catch (System.Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                if (log.ID > 0) Logger.LocalLogger.Instance().WriteMessage(new Exception("Không xử lý được messages có ID = " + log.ID));
                // isError = true;
                throw ex;
            }
        }
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
