/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 11/21/2012 10:06:15 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_GC_NguyenPhuLieu_GetNPLDinhMuc]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[[p_GC_NguyenPhuLieu_GetNPLDinhMuc]]
-- Database: TQDT_SXXK_V3
-- Author: Huynh Ngoc Khanh
-- Time created: Monday, July 14, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_NguyenPhuLieu_GetNPLDinhMuc]
	@IDDinhMuc INT,
	@MaSanPham nvarchar(250),
	@MaDoanhNghiep nvarchar(20),
	@MaHaiQuan nvarchar(20)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT     t_SXXK_NguyenPhuLieu.MaHS, 
           t_SXXK_NguyenPhuLieu.DVT_ID, 
           t_SXXK_NguyenPhuLieu.Ma, 
           t_SXXK_NguyenPhuLieu.Ten, 
           t_KDT_SXXK_DinhMuc.DinhMucSuDung, 
           t_KDT_SXXK_DinhMuc.TyLeHaoHut
           FROM    t_SXXK_NguyenPhuLieu INNER JOIN
           t_KDT_SXXK_DinhMuc ON t_SXXK_NguyenPhuLieu.Ma = t_KDT_SXXK_DinhMuc.MaNguyenPhuLieu INNER JOIN
           t_KDT_SXXK_DinhMucDangKy ON t_KDT_SXXK_DinhMuc.Master_ID = t_KDT_SXXK_DinhMucDangKy.ID
WHERE    (t_KDT_SXXK_DinhMucDangKy.ID = @IDDinhMuc) AND (t_KDT_SXXK_DinhMuc.MaSanPham = @MaSanPham) AND (dbo.t_SXXK_NguyenPhuLieu.MaDoanhNghiep = @MaDoanhNghiep) AND (dbo.t_SXXK_NguyenPhuLieu.MaHaiQuan = @MaHaiQuan)
ORDER BY t_SXXK_NguyenPhuLieu.Ma
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_NPLNhapTon_SelectNPLTon]'
GO
  
  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_SXXK_NPLNhapTon_SelectAll]  
-- Database: SXXK V3 
-- Author: Huynh Ngoc Khanh  
-- Time created: 17/11/2012
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_SelectNPLTon]  
  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
SELECT     MAX(DISTINCT t_SXXK_NguyenPhuLieu.Ma) AS MaNPL, MAX(DISTINCT t_SXXK_NguyenPhuLieu.Ten) AS TenNPL, SUM(t_KDT_SXXK_NPLNhapTon.Luong) AS Luong,   
                      SUM(t_KDT_SXXK_NPLNhapTon.TonCuoi) AS TonCuoi, t_KDT_SXXK_NPLNhapTon.LanThanhLy  
FROM         t_KDT_SXXK_NPLNhapTon INNER JOIN  
                      t_SXXK_NguyenPhuLieu ON t_KDT_SXXK_NPLNhapTon.MaHaiQuan = t_SXXK_NguyenPhuLieu.MaHaiQuan AND   
                      t_KDT_SXXK_NPLNhapTon.MaNPL = t_SXXK_NguyenPhuLieu.Ma AND   
                      t_KDT_SXXK_NPLNhapTon.MaDoanhNghiep = t_SXXK_NguyenPhuLieu.MaDoanhNghiep  
GROUP BY t_KDT_SXXK_NPLNhapTon.MaNPL,  t_KDT_SXXK_NPLNhapTon.LanThanhLy  
HAVING      (t_KDT_SXXK_NPLNhapTon.LanThanhLy = (SELECT MAX(Temp.LanThanhLy) FROM t_KDT_SXXK_NPLNhapTon AS Temp WHERE Temp.MaNPL = t_KDT_SXXK_NPLNhapTon.MaNPL))-- AND (dbo.t_KDT_SXXK_NPLNhapTon.MaDoanhNghiep IS NOT null) AND (dbo.t_KDT_SXXK_NPLNhapTon.MaHaiQuan = 'C34C')  
ORDER BY MaNPL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='5.5', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('5.5', getdate(), null)
	end 
