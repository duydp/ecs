﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class GiayKiemTra
    {
        List<HangGiayKiemTra> _HangCollection = new List<HangGiayKiemTra>();
        public List<HangGiayKiemTra> HangCollection { get { return _HangCollection; } set { this._HangCollection = value; } }

        List<ChungTuGiayKiemTra> _ChungTuCollection = new List<ChungTuGiayKiemTra>();
        public List<ChungTuGiayKiemTra> ChungTuCollection { get { return this._ChungTuCollection; } set { this._ChungTuCollection = value; } }

        public void LoadFull()
        {
            this._HangCollection = new List<HangGiayKiemTra>();
            this._ChungTuCollection = new List<ChungTuGiayKiemTra>();
            if (ID > 0)
            {
                this._HangCollection = (List<HangGiayKiemTra>)HangGiayKiemTra.SelectCollectionBy_GiayKiemTra_ID(this.ID);
                this._ChungTuCollection = (List<ChungTuGiayKiemTra>)ChungTuGiayKiemTra.SelectCollectionBy_GiayKiemTra_ID(this.ID);
            }
        }
        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (this.ID == 0)
                            this.ID = this.Insert(transaction);
                        else
                            this.Update(transaction);
                        foreach (HangGiayKiemTra item in this.HangCollection)
                        {
                            item.GiayKiemTra_ID = this.ID;
                            item.InsertUpdate(transaction);
                        }
                        foreach (ChungTuGiayKiemTra item in ChungTuCollection)
                        {
                            item.GiayKiemTra_ID = this.ID;
                            item.InsertUpdate(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (System.Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public DataTable ConvertListToDataSet(System.Data.DataTable dsHMD)
        {
            if ((_HangCollection != null && _HangCollection.Count == 0) && (_ChungTuCollection != null && _ChungTuCollection.Count == 0))
            {
                LoadFull();
            }

            DataTable tmp = dsHMD.Copy();
            tmp.Rows.Clear();
            tmp.Columns.Add("GhiChu");
            dsHMD.Columns.Add("GhiChu");
            dsHMD.Columns.Add("HMD_ID");
            tmp.Columns["ID"].ColumnName = "HMD_ID";
            tmp.Columns["HMD_ID"].Caption = "HMD_ID";
            tmp.Columns.Add("ID");

            tmp.Columns.Add("DiaDiemKiemTra");
            tmp.Columns.Add("MaCoQuanKT");
            tmp.Columns.Add("TenCoQuanKT");
            tmp.Columns.Add("KetQuaKT");
            tmp.Columns.Add("LoaiChungTu");
            tmp.Columns.Add("TenChungTu");
            tmp.Columns.Add("SoChungTu");
            tmp.Columns.Add("NgayPhatHanh", typeof(DateTime));


            dsHMD.Columns.Add("DiaDiemKiemTra");
            dsHMD.Columns.Add("MaCoQuanKT");
            dsHMD.Columns.Add("TenCoQuanKT");
            dsHMD.Columns.Add("KetQuaKT");
            dsHMD.Columns.Add("LoaiChungTu");
            dsHMD.Columns.Add("TenChungTu");
            dsHMD.Columns.Add("SoChungTu");
            dsHMD.Columns.Add("NgayPhatHanh", typeof(DateTime));

            foreach (HangGiayKiemTra item in this.HangCollection)
            {
                DataRow[] row = dsHMD.Select("ID=" + item.HMD_ID);
                if (row != null && row.Length != 0)
                {
                    row[0]["GhiChu"] = item.GhiChu;
                    row[0]["HMD_ID"] = row[0]["ID"];
                    row[0]["ID"] = item.ID.ToString();

                    row[0]["MaHS"] = item.MaHS;
                    row[0]["MaPhu"] = item.MaHang;
                    row[0]["TenHang"] = item.TenHang;
                    row[0]["NuocXX_ID"] = item.NuocXX_ID;
                    row[0]["DVT_ID"] = item.DVT_ID;
                    row[0]["SoLuong"] = item.SoLuong;

                    row[0]["DiaDiemKiemTra"] = item.DiaDiemKiemTra;
                    row[0]["MaCoQuanKT"] = item.MaCoQuanKT;
                    row[0]["TenCoQuanKT"] = item.TenCoQuanKT;
                    row[0]["KetQuaKT"] = item.KetQuaKT;
                    row[0]["LoaiChungTu"] = item.LoaiChungTu;
                    row[0]["TenChungTu"] = item.TenChungTu;
                    row[0]["SoChungTu"] = item.SoChungTu;
                    row[0]["NgayPhatHanh"] = item.NgayPhatHanh;
                    tmp.ImportRow(row[0]);
                }
            }
            return tmp;
        }

        public void DeleteAll()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        HangGiayKiemTra.DeleteBy_GiayKiemTra_ID(this.ID);
                        ChungTuGiayKiemTra.DeleteBy_GiayKiemTra_ID(this.ID);
                        this.Delete(null);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public static bool KiemTraChungTuCoBoSung(List<GiayKiemTra> collections)
        {
            foreach (GiayKiemTra item in collections)
            {
                if (item.LoaiKB == 1)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
