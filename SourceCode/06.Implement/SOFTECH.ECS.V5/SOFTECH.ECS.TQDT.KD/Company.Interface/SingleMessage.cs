﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.Interface.DanhMucChuan;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Messages.Send;
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
using System.IO;
using Company.KDT.SHARE.QuanLyChungTu.CX;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using System.IO;
#endif
namespace Company.Interface
{
    public class SingleMessage
    {
        static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        static string sfmtDate = "yyyy-MM-dd";
        static string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";
        public static ObjectSend SendMessageV3(ToKhaiMauDich TKMD)
        {

            TKMD.GUIDSTR = Guid.NewGuid().ToString();
            //TKMD.Update();
            ToKhai toKhaiDto = new ToKhai();
            toKhaiDto = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferObject(TKMD) : Mapper.ToDataTransferObject(TKMD);
            bool isToKhaiSua = TKMD.SoToKhai != 0 && TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            ObjectSend objSend = new ObjectSend(new NameBase()
                           {
                               Name = TKMD.TenDoanhNghiep,
                               Identity = TKMD.MaDoanhNghiep
                           },
                            new NameBase()
                             {
                                 Name = DonViHaiQuan.GetName(TKMD.MaHaiQuan),
                                 Identity = TKMD.MaHaiQuan
                             },
                             new SubjectBase()
                            {
                                Type = toKhaiDto.Issuer,
                                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                                Reference = TKMD.GUIDSTR
                            },
                            toKhaiDto);
            return objSend;
        }
        public static ObjectSend CancelMessageV3(ToKhaiMauDich TKMD)
        {
            return CancelMessageV3(TKMD, string.Empty);
        }
        public static ObjectSend CancelMessageV3(ToKhaiMauDich TKMD, string guidHuyTK)
        {
            bool IsToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
            bool IsHuyToKhai = (TKMD.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            string issuer = string.Empty;
#if KD_V3 || KD_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT;
#elif SXXK_V3 || SXXK_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.SXXK_TOKHAI_NHAP : DeclarationIssuer.SXXK_TOKHAI_XUAT;
#elif GC_V3 || GC_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_NHAP : DeclarationIssuer.GC_TOKHAI_XUAT;
#endif
            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = issuer,
                Reference = TKMD.GUIDSTR,
                IssueLocation = string.Empty,
                Agents = new List<Agent>(),

                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(TKMD.SoTiepNhan, 0),
                Acceptance = TKMD.NgayTiepNhan.ToString(sfmtDate),
                DeclarationOffice = TKMD.MaHaiQuan.Trim(),
                AdditionalInformations = new List<AdditionalInformation>()
            };
            declaredCancel.Agents.Add(new Agent()
            {
                Name = TKMD.TenDoanhNghiep,
                Identity = TKMD.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            declaredCancel.AdditionalInformations.Add(
                new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                }
                );
            if (IsHuyToKhai)
            {
                List<HuyToKhai> cancelContent = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_TKMD_ID(TKMD.ID);
                if (cancelContent.Count > 0)
                {
                    declaredCancel.AdditionalInformations[0].Content.Text = cancelContent[0].LyDoHuy;
                    declaredCancel.Reference = guidHuyTK;
                }
            }
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = TKMD.TenDoanhNghiep,
                Identity = TKMD.MaDoanhNghiep
            },
            new NameBase()
            {
                Name = DonViHaiQuan.GetName(TKMD.MaHaiQuan),
                Identity = TKMD.MaHaiQuan
            },
            new SubjectBase()
            {
                Type = issuer,
                Function = declaredCancel.Function,
                Reference = declaredCancel.Reference //TKMD.GUIDSTR
            }, declaredCancel);
            return objSend;
        }
        public static ObjectSend FeedBackMessageV3(ToKhaiMauDich TKMD)
        {
            return FeedBack(TKMD, TKMD.GUIDSTR);
        }
        public static ObjectSend FeedBack(ToKhaiMauDich TKMD, string reference)
        {
            if (TKMD == null) return null;
            bool IsToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            string issuer = string.Empty;

#if KD_V3 || KD_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT;
#elif SXXK_V3 || SXXK_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.SXXK_TOKHAI_NHAP : DeclarationIssuer.SXXK_TOKHAI_XUAT;
#elif GC_V3 || GC_V4
            issuer = IsToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_NHAP : DeclarationIssuer.GC_TOKHAI_XUAT;
#endif
            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = issuer,
                Reference = reference,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = issuer
            };

            ObjectSend objSend = new ObjectSend(new NameBase()
                                        {
                                            Name = TKMD.TenDoanhNghiep,
                                            Identity = TKMD.MaDoanhNghiep
                                        },
                                        new NameBase()
                                          {
                                              Name = DonViHaiQuan.GetName(TKMD.MaHaiQuan.Trim()),
                                              Identity = TKMD.MaHaiQuan
                                          },
                                          subjectBase, null);
            return objSend;
        }
        public static ObjectSend FeedBackBoSungCo(Company.KDT.SHARE.QuanLyChungTu.CO Co, ToKhaiMauDich TKMD)
        {
            return FeedBack(TKMD, Co.GuidStr);
        }
        private static ObjectSend BuildBoSungChungTu(ToKhaiMauDich TKMD, BoSungChungTu bosung)
        {
            ObjectSend objSend = new ObjectSend(new NameBase()
               {
                   Identity = TKMD.MaDoanhNghiep,
                   Name = TKMD.TenDoanhNghiep
               },
               new NameBase()
               {
                   Name = DonViHaiQuan.GetName(TKMD.MaHaiQuan),
                   Identity = TKMD.MaHaiQuan
               },
               new SubjectBase()
            {
                Type = bosung.Issuer,
                Function = bosung.Function,
                Reference = bosung.Reference
            }, bosung);
            return objSend;

        }
        public static ObjectSend BoSungCO(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.CO Co)
        {
            BoSungChungTu bosungCo = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, Co) : Mapper.ToDataTransferBoSung(TKMD, Co);
            return BuildBoSungChungTu(TKMD, bosungCo);
        }
        public static ObjectSend BoSungGiayPhep(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep)
        {
            BoSungChungTu boSungGiayPhep = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, giayphep) : Mapper.ToDataTransferBoSung(TKMD, giayphep);
            return BuildBoSungChungTu(TKMD, boSungGiayPhep);
        }
        public static ObjectSend BoSungHopDong(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong)
        {
            BoSungChungTu bosungHopDong = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, hopdong) : Mapper.ToDataTransferBoSung(TKMD, hopdong);
            return BuildBoSungChungTu(TKMD, bosungHopDong);
        }
        public static ObjectSend BoSungHoaDonTM(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon)
        {
            BoSungChungTu bosungHopDong = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, hoadon) : Mapper.ToDataTransferBoSung(TKMD, hoadon);
            return BuildBoSungChungTu(TKMD, bosungHopDong);
        }
        public static ObjectSend BoSungChungTuKem(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem)
        {
            BoSungChungTu bosungHopDong = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, chungtukem) : Mapper.ToDataTransferBoSung(TKMD, chungtukem);
            return BuildBoSungChungTu(TKMD, bosungHopDong);
        }
        public static ObjectSend BoSungXinChuyenCuaKhau(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau chuyencuakhau)
        {
            BoSungChungTu bosungXinChuyenCuaKhau = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, chuyencuakhau) : Mapper.ToDataTransferBoSung(TKMD, chuyencuakhau);
            return BuildBoSungChungTu(TKMD, bosungXinChuyenCuaKhau);
        }
        public static ObjectSend BoSungGiayNopTien(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.GiayNopTien GiayNopTien)
        {
            BoSungChungTu boSungGiayNopTien = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferBoSung(TKMD, GiayNopTien) : Mapper_V4.ToDataTransferBoSung(TKMD, GiayNopTien);
            return BuildBoSungChungTu(TKMD, boSungGiayNopTien);
        }
        public static ObjectSend BoSungGiayKiemTra(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.GiayKiemTra GiayKT)
        {
            BoSungChungTu BoSungGiayKT = Mapper_V4.ToDataTransferBoSung(TKMD, GiayKT);
            return BuildBoSungChungTu(TKMD, BoSungGiayKT);
        }
        public static ObjectSend BoSungChungThuGiamDinh(ToKhaiMauDich TKMD, Company.KDT.SHARE.QuanLyChungTu.ChungThuGiamDinh ChungThuGD)
        {
            BoSungChungTu boSungChungThuGiamDinh = Mapper_V4.ToDataTransferBoSung(TKMD, ChungThuGD);
            return BuildBoSungChungTu(TKMD, boSungChungThuGiamDinh);
        }

        public static void DeleteMsgSend(string LoaiKhaiBao, long id)
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao;
            sendXML.master_id = id;
            sendXML.Delete();
        }
#if GC_V3 || GC_V4
        #region Tờ khai chuyển tiếp
        public static ObjectSend SendMessageCT(ToKhaiChuyenTiep TKCT)
        {
            TKCT.GUIDSTR = Guid.NewGuid().ToString();
            ToKhai toKhaiDto = GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferToKhaiCT(TKCT, GlobalSettings.TEN_DON_VI) :  Mapper.ToDataTransferToKhaiCT(TKCT, GlobalSettings.TEN_DON_VI);
            bool isToKhaiSua = TKCT.SoToKhai != 0 && TKCT.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = TKCT.MaDoanhNghiep
            },
                            new NameBase()
                            {
                                Name = DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan),
                                Identity = TKCT.MaHaiQuanTiepNhan
                            },
                             new SubjectBase()
                             {
                                 Type = toKhaiDto.Issuer,
                                 Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                                 Reference = TKCT.GUIDSTR
                             },
                            toKhaiDto);
            return objSend;
        }
        public static ObjectSend CancelMessageCT(ToKhaiChuyenTiep TKCT)
        {
            return CancelMessageCT(TKCT, string.Empty);
        }
        public static ObjectSend CancelMessageCT(ToKhaiChuyenTiep TKCT, string guidHTK)
        {
            bool IsToKhaiNhap = TKCT.MaLoaiHinh.StartsWith("N");
            TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
            bool IsHuyToKhai = (TKCT.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            string issuer = string.Empty;
            issuer = TKCT.MaLoaiHinh.Substring(0, 1).ToUpper() == "N" ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT;
            string guid;
            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = issuer,
                Reference = TKCT.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(TKCT.SoTiepNhan, 0),
                Acceptance = TKCT.NgayTiepNhan.ToString(sfmtDate),
                DeclarationOffice = TKCT.MaHaiQuanTiepNhan.Trim(),
                AdditionalInformations = new List<AdditionalInformation>()
            };
            declaredCancel.AdditionalInformations.Add(
                new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                }
                );
            if (IsHuyToKhai)
            {
                string sfmtWhere = string.Format("TKCT_ID={0}", TKCT.ID);
                List<HuyToKhaiCT> listHuyTK = (List<HuyToKhaiCT>)HuyToKhaiCT.SelectCollectionDynamic(sfmtWhere, "");
                if (listHuyTK.Count > 0)
                    declaredCancel.AdditionalInformations[0].Content.Text = listHuyTK[0].LyDoHuy;
                declaredCancel.Reference = guidHTK;
            }


            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = TKCT.MaDoanhNghiep
            },
            new NameBase()
            {
                Name = DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan),
                Identity = TKCT.MaHaiQuanTiepNhan
            },
            new SubjectBase()
            {
                Type = issuer,
                Function = declaredCancel.Function,
                Reference = string.IsNullOrEmpty(guidHTK) ? TKCT.GUIDSTR : guidHTK
            }, declaredCancel);
            return objSend;
        }
        public static ObjectSend FeedBackCT(ToKhaiChuyenTiep TKMD, string reference)
        {
            if (TKMD == null) return null;
            bool IsToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            string issuer = IsToKhaiNhap? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = issuer,
                Reference = reference,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = issuer
            };
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = TKMD.MaDoanhNghiep
            },
                                        new NameBase()
                                        {
                                            Name = DonViHaiQuan.GetName(TKMD.MaHaiQuanTiepNhan.Trim()),
                                            Identity = TKMD.MaHaiQuanTiepNhan
                                        },
                                          subjectBase, null);
            return objSend;
        }
        #endregion

        #region Bổ sung tờ khai chuyển tiếp
        public static ObjectSend BoSungChungTuGCCT(ToKhaiChuyenTiep TKCT, object ChungTuBoSung, string TenDoanhNghiep)
        {
            BoSungChungTu boSungCT = Mapper.ToDataTransferBoSungChuyenTiep(TKCT, ChungTuBoSung, TenDoanhNghiep);
            return BuildBoSungChungTuChuyenTiep(TKCT, boSungCT);
        }
        public static ObjectSend BuildBoSungChungTuChuyenTiep(ToKhaiChuyenTiep TKCT, BoSungChungTu bosung)
        {
            ObjectSend msgSend = new ObjectSend(
               new NameBase()
               {
                   Identity = TKCT.MaDoanhNghiep,
                   Name = GlobalSettings.TEN_DON_VI
               },
               new NameBase()
               {
                   Name = DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan),
                   Identity = TKCT.MaHaiQuanTiepNhan
               },
            new SubjectBase()
            {
                Type = bosung.Issuer,
                Function = bosung.Function,
                Reference = bosung.Reference
            },
             bosung);
            return msgSend;
        }
        #region Feedback gia công chuyển tiếp
        public static ObjectSend FeedBackGCCT(ToKhaiChuyenTiep TKCT, string reference)
        {
            if (TKCT == null) return null;
            bool IsToKhaiNhap = TKCT.MaLoaiHinh.StartsWith("N");
            string issuer = string.Empty;
            issuer = IsToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = issuer,
                Reference = reference,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = issuer
            };
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = TKCT.MaDoanhNghiep
            },
                                        new NameBase()
                                        {
                                            Name = DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan.Trim()),
                                            Identity = TKCT.MaHaiQuanTiepNhan
                                        },
                                          subjectBase, null);
            return objSend;
        }
        #endregion
        #endregion
#endif
        #region Process Message
        public static string GetErrorContent(FeedBackContent feedbackContent)
        {
            string noidung = string.Empty;
            noidung = feedbackContent.AdditionalInformations[0].Content.Text;
            if (feedbackContent.AdditionalInformations[0].Content.ErrorInformations != null &&
                                    feedbackContent.AdditionalInformations[0].Content.ErrorInformations.Count > 0)
            {
                if (!string.IsNullOrEmpty(noidung)) noidung += "\r\n";
                foreach (ErrorInformation error in feedbackContent.AdditionalInformations[0].Content.ErrorInformations)
                {
                    if (error.ReQuired != null)
                        noidung += string.Format("{0}. {1}\r\n", error.ReQuired.SeQuence, error.ReQuired.Description);
                    else if (error.CheckNoDeNotExist != null)
                        noidung += string.Format("{0}. {1}\r\n", error.CheckNoDeNotExist.SeQuence, error.CheckNoDeNotExist.Description);
                    else if (error.MaxLength != null)
                        noidung += string.Format("{0}. {1}\r\n", error.MaxLength.SeQuence, error.MaxLength.Description);
                    else if (error.PositiveDouble != null)
                        noidung += string.Format("{0}. {1}\r\n", error.PositiveDouble.SeQuence, error.PositiveDouble.Description);
                    else if (error.IN != null)
                        noidung += string.Format("{0}. {1}\r\n", error.IN.SeQuence, error.IN.Description);
                    else if (error.DATETIME != null)
                        noidung += string.Format("{0}. {1}\r\n", error.DATETIME.SeQuence, error.DATETIME.Description);
                    else if (error.LE_CURRENT_DATETIME != null)
                        noidung += string.Format("{0}. {1}\r\n", error.LE_CURRENT_DATETIME.SeQuence, error.LE_CURRENT_DATETIME.Description);
                    else if (error.GE_CURRENT_DATETIME != null)
                        noidung += string.Format("{0}. {1}\r\n", error.GE_CURRENT_DATETIME.SeQuence, error.GE_CURRENT_DATETIME.Description);

                }
            }
            if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                noidung = "Hệ thống Hải quan từ chối với lý do:\r\n" + noidung;
            return noidung;
        }
        public static void SendMail(string mahaiquan, SendEventArgs e)
        {
            SendMail("Thông báo lỗi", mahaiquan, e);
        }
        private static string Zip(string sourceFile)
        {
            try
            {
                if (System.IO.File.Exists(sourceFile))
                {
                    string zipFile = sourceFile + ".zip";
                    System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("zip.exe", "-j -r -9 -D \"" +
                                                             zipFile + "\" \"" +
                                                             sourceFile + "\"");
                    psi.CreateNoWindow = true;
                    psi.ErrorDialog = true;
                    psi.UseShellExecute = false;
                    System.Diagnostics.Process ps = System.Diagnostics.Process.Start(psi);
                    ps.WaitForExit();
                    System.IO.FileInfo fi = new System.IO.FileInfo(zipFile);
                    if (fi.Exists)
                        return zipFile;

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return string.Empty;
        }
        public static void SendMail(string caption, string mahaiquan, SendEventArgs e)
        {
            if (e.Error == null && string.IsNullOrEmpty(e.FeedBackMessage)) return;
            Company.Controls.KDTMessageBoxControl msg = new Company.Controls.KDTMessageBoxControl();
            msg.HQMessageString = caption;
            msg.ShowYesNoButton = false;
            msg.ShowErrorButton = true;
            msg.MaHQ = mahaiquan;
            if (!string.IsNullOrEmpty(e.FeedBackMessage))
            {
                string fileZip = Zip(e.FeedBackMessage);
                if (fileZip != string.Empty && System.IO.File.Exists(fileZip))
                    msg.XmlFiles.Add(fileZip);
                else
                    msg.XmlFiles.Add(e.FeedBackMessage);
            }
            msg.MessageString = e.Error.Message;
            msg.exceptionString = e.Error.StackTrace == null ? e.Error.Message : e.Error.StackTrace;
            msg.ShowDialog();
        }
        public static DateTime GetDate(string value)
        {
            return GetDate(value, DateTime.Now.ToString(sfmtDateTime),sfmtDate);
        }
        public static DateTime GetDate(string value, string defaultValue, string formatdate)
        {

            try
            {
                if (value.Length == 10)
                    return DateTime.ParseExact(value, formatdate, null);
                else if (value.Length == 19)
                    return DateTime.ParseExact(value, formatdate, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return DateTime.ParseExact(defaultValue, formatdate, null);

        }
        public static DateTime GetNgayAnDinhThue(string value)
        {
            DateTime date = new DateTime(1900,1,1);
            try
            {
                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out date))
                    return date;
                else if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out date))
                    return date;
                else if (DateTime.TryParseExact(value, "dd-MM-yyyy", null, System.Globalization.DateTimeStyles.None, out date))
                    return date;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return date;

        }
        public static FeedBackContent ToKhaiSendHandler(ToKhaiMauDich TKMD, ref string msgInfor, object sender, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;

                    //if (Company.KDT.SHARE.Components.Globals.FontName == "TCVN3")
                    //    noidung = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(noidung);
                    bool SaiMatKhau = false;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                        SaiMatKhau = true;
                        //feedbackContent.Function = DeclarationFunction.CHUA_XU_LY;
                    }
                    bool isDeleteMsg = false;
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {
                                if (!SaiMatKhau)
                                {
                                    isDeleteMsg = true;
                                    noidung = GetErrorContent(feedbackContent);
                                    e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && TKMD.SoToKhai > 0)
                                    {
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                                    }
                                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || TKMD.SoToKhai == 0)
                                    {
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                        TKMD.NgayTiepNhan = DateTime.Now;
                                    }
                                }
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                
                                string[] vals = noidung.Split('/');

                                TKMD.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                                TKMD.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                               // TKMD.NamDK = TKMD.NgayTiepNhan.Year;

                                noidung = string.Format("Số tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nLoại hình: {3}\r\nHải quan: {4}", new object[] {
                                    TKMD.SoTiepNhan, TKMD.NgayTiepNhan.ToString(sfmtVnDateTime), TKMD.NgayTiepNhan.Year, TKMD.MaLoaiHinh, TKMD.MaHaiQuan });
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, noidung);
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    noidung = "Tờ khai được cấp số chờ hủy khai báo\r\n" + noidung;
                                    //TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else
                                {
                                    noidung = "Tờ khai được cấp số tiếp nhận\r\n" + noidung;
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                }
                                break;
                            }
                        case DeclarationFunction.CAP_SO_TO_KHAI:
                            {
                                TKMD.SoToKhai = int.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                noidung = string.Format("Tờ khai được cấp số\r\nSố tờ khai: {0} \r\nNgày tiếp nhận: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nNội dung hq: {4}",
                                    new object[] { feedbackContent.CustomsReference, feedbackContent.Issue, feedbackContent.NatureOfTransaction, feedbackContent.DeclarationOffice, noidung });
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocCapSo, noidung);
                                isDeleteMsg = true;
                                TKMD.NamDK = TKMD.NgayDangKy.Year;
                                //Bo sung: Hungtq, 4/2/2013, Cap nhat thong tin NPL vao ton thuc te
#if SXXK_V3 || SXXK_V4
                                TKMD.TransgferDataToSXXK();
#elif GC_V3 || GC_V4
                                // Loai tru reiker - Bổ sung chức năng mới cho công ty reiker
                                if (GlobalSettings.MA_DON_VI != "4000395355")
                                    TKMD.TransgferDataToNPLTonThucTe();
#endif
                            }
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            {
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    //if (TKMD.SoTiepNhan.ToString() == feedbackContent.CustomsReference)
                                    //{
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                    noidung = "Tờ khai được chấp nhận hủy khai báo\r\n";
                                    e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.HQHuyKhaiBaoToKhai, noidung);
                                    //}
                                }
                                else //if ((TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)/* && TKMD.SoToKhai.ToString() == feedbackContent.CustomsReference*/)
                                {
                                    KhaiBaoSua.XoaSoTNCu(TKMD.SoToKhai, TKMD.NgayDangKy.Year, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep, LoaiKhaiBao.ToKhai);
                                    noidung = "Số tờ khai: " + feedbackContent.CustomsReference;
                                    noidung += "\r\nNgày đăng ký " + feedbackContent.Acceptance;
                                    if (!string.IsNullOrEmpty(noidung))
                                    {
                                        //TKMD.HUONGDAN = noidung;
//                                         noidung = "Số tờ khai: " + feedbackContent.CustomsReference;
//                                         noidung += "\r\nNgày đăng ký " + feedbackContent.Acceptance;
                                        noidung += "\r\nTờ khai được chấp nhận thông quan nội dung: " + noidung + "\r\n";
                                    }
                                    else noidung = "Tờ khai được chấp nhận thông quan";
                                    if (TKMD.SoToKhai == 0 && !string.IsNullOrEmpty(feedbackContent.CustomsReference) )
                                    {
                                        if (feedbackContent.Acceptance.Length == 10)
                                            TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                        else if (feedbackContent.Acceptance.Length == 19)
                                            TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                        else TKMD.NgayDangKy = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                                        TKMD.SoToKhai = int.Parse(feedbackContent.CustomsReference.Trim());
                                    }
                                    e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.TKChapNhanThongQuan, noidung);
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                    TKMD.NamDK = TKMD.NgayDangKy.Year;
                                    //Updated by Hungtq, 24/08/2012. Cap nhat thong tin to khai sua doi bo sung trong da dang ky
#if SXXK_V3 || SXXK_V4
                                    try
                                    {
                                        if (TKMD.ActionStatus == (short)ActionStatus.ToKhaiSua)
                                            TKMD.CapNhatThongTinHangToKhaiSua();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                        e.Error.Message.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiSuaDuocDuyet, string.Format("Tờ khai sửa:\r\nMã Hải quan: {0}\r\nMã doanh nghiệp: {1}\r\nTên doanh nghiệp: {2}\r\nSố tờ khai: {3}\r\nMã loại hình: {4}\r\nNăm đăng ký: {5}", TKMD.MaHaiQuan, TKMD.MaDoanhNghiep, TKMD.TenDoanhNghiep, TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK));
                                        SendMail("Lỗi cập nhật dữ liệu tờ khai sửa được duyệt", TKMD.MaHaiQuan, e);
                                    }
#endif
                                }
                             /*   else*/
                                    //e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.None, noidung);

                                isDeleteMsg = true;
                            }
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            isDeleteMsg = true;
                            //Khanhhn - Bổ sung không cập nhật phân luồng khi statement rỗng
                            if (TKMD.ActionStatus != (short)ActionStatus.ToKhaiSua && !string.IsNullOrEmpty(feedbackContent.AdditionalInformations[0].Statement))
                                TKMD.HUONGDAN = noidung;
                            else
                            {
                                //Khanhhn - Bổ sung chuyển trạng thái tờ khai sửa sau khi phân luồng lại
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                KhaiBaoSua.XoaSoTNCu(TKMD.SoToKhai, TKMD.NgayDangKy.Year, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep, LoaiKhaiBao.ToKhai);
                                NoiDungChinhSuaTKForm ndDieuChinh = new NoiDungChinhSuaTKForm();
                                ndDieuChinh.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
                                foreach (NoiDungDieuChinhTK item in ndDieuChinh.ListNoiDungDieuChinhTK)
                                {
                                    item.TrangThai = TrangThaiXuLy.DA_DUYET;
                                }
#if SXXK_V3 || SXXK_V4
                                    try
                                    {
                                        
                                            TKMD.CapNhatThongTinHangToKhaiSua();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                        e.Error.Message.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiSuaDuocDuyet, string.Format("Tờ khai sửa:\r\nMã Hải quan: {0}\r\nMã doanh nghiệp: {1}\r\nTên doanh nghiệp: {2}\r\nSố tờ khai: {3}\r\nMã loại hình: {4}\r\nNăm đăng ký: {5}", TKMD.MaHaiQuan, TKMD.MaDoanhNghiep, TKMD.TenDoanhNghiep, TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK));
                                        SendMail("Lỗi cập nhật dữ liệu tờ khai sửa được duyệt", TKMD.MaHaiQuan, e);
                                    }
#endif
                                    if (!string.IsNullOrEmpty(feedbackContent.AdditionalInformations[0].Statement))
                                        TKMD.HUONGDAN = noidung;
                            }
                            string idTax = string.Empty;
                            string tenKhobac = string.Empty;
                            foreach (AdditionalInformation item in feedbackContent.AdditionalInformations)
                            {
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                if (item.Statement == AdditionalInformationStatement.LUONG_XANH)
                                    TKMD.PhanLuong = TrangThaiPhanLuong.LUONG_XANH;
                                else if (item.Statement == AdditionalInformationStatement.LUONG_VANG)
                                    TKMD.PhanLuong = TrangThaiPhanLuong.LUONG_VANG;
                                else if (item.Statement == AdditionalInformationStatement.LUONG_DO)
                                    TKMD.PhanLuong = TrangThaiPhanLuong.LUONG_DO;
                                else if (item.Statement == AdditionalInformationStatement.TAI_KHOAN_KHO_BAC)
                                {
                                    idTax = item.Content.Text;
                                }
                                else if (item.Statement == AdditionalInformationStatement.TEN_KHO_BAC)
                                {
                                    tenKhobac = item.Content.Text;
                                }
                            }
                            if (idTax != string.Empty && feedbackContent.AdditionalDocument != null)
                            {
                                noidung += "\r\nThông báo thuế của hệ thống Hải quan:\r\n";



                                AnDinhThue andinhthue = new AnDinhThue();
                                andinhthue.TaiKhoanKhoBac = idTax;
                                andinhthue.TKMD_ID = TKMD.ID;
                                andinhthue.TKMD_Ref = TKMD.GUIDSTR;
                                andinhthue.NgayQuyetDinh = GetNgayAnDinhThue(feedbackContent.AdditionalDocument.Issue);
                                andinhthue.NgayHetHan = GetNgayAnDinhThue(feedbackContent.AdditionalDocument.Expire);
                                andinhthue.SoQuyetDinh = feedbackContent.AdditionalDocument.Reference.Trim();
                                andinhthue.TenKhoBac = Company.KDT.SHARE.Components.ConvertFont.TCVN3ToUnicode(tenKhobac);
                                noidung += "Số quyết định: " + andinhthue.SoQuyetDinh;
                                noidung += "\r\nTài khoản kho bạc: " + idTax;
                                noidung += "\r\nTên kho bạc: " + tenKhobac;
                                noidung += andinhthue.NgayQuyetDinh.Year > 1900 ? "\r\nNgày quyết định: " + andinhthue.NgayQuyetDinh.ToString("dd/MM/yyyy") : "\r\nNgày quyết định: N/N";
                                noidung += andinhthue.NgayHetHan.Year > 1900 ? "\r\nNgày hết hạn: " + andinhthue.NgayHetHan.ToString("dd/MM/yyyy") : "\r\nNgày hết hạn: N/N"; 

                                andinhthue.Insert();


                                foreach (DutyTaxFee item in feedbackContent.AdditionalDocument.DutyTaxFees)
                                {
                                    ChiTiet chiTietThue = new ChiTiet();
                                    chiTietThue.IDAnDinhThue = andinhthue.ID;

                                    //Updated by Hungtq, 02/01/2013. Cap nhat tien thue, loai bo so thap phan
                                    string[] spl = item.AdValoremTaxBase.Split(new char[] { '.' });

                                    if (spl.Length == 2)
                                        if (System.Convert.ToDecimal(spl[1]) == 0)
                                            chiTietThue.TienThue = System.Convert.ToDecimal(spl[0]);
                                        else
                                            chiTietThue.TienThue = System.Convert.ToDecimal(item.AdValoremTaxBase);

                                    switch (item.Type.Trim())
                                    {
                                        case DutyTaxFeeType.THUE_XNK:
                                            chiTietThue.SacThue = "XNK";
                                            break;
                                        case DutyTaxFeeType.THUE_BAO_VE_MOI_TRUONG:
                                            chiTietThue.SacThue = "BVMT";
                                            break;
                                        case DutyTaxFeeType.THUE_VAT:
                                            chiTietThue.SacThue = "VAT";
                                            break;
                                        case DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET:
                                            chiTietThue.SacThue = "TTDB";
                                            break;
                                        case DutyTaxFeeType.THUE_KHAC:
                                            chiTietThue.SacThue = "KHAC";
                                            break;
                                        case DutyTaxFeeType.THUE_CHENH_LECH_GIA:
                                            chiTietThue.SacThue = "CLG";
                                            break;
                                        case DutyTaxFeeType.THUE_CHONG_BAN_PHA_GIA:
                                            chiTietThue.SacThue = "TVCBPG";
                                            break;
                                        default:
                                            break;
                                    }

                                    foreach (AdditionalInformation additionalInfo in item.AdditionalInformations)
                                    {
                                        if (!string.IsNullOrEmpty(additionalInfo.Content.Text))
                                            switch (additionalInfo.Statement)
                                            {
                                                case "211"://Chuong
                                                    chiTietThue.Chuong = additionalInfo.Content.Text;
                                                    break;
                                                case "212"://Loai
                                                    chiTietThue.Loai = Convert.ToInt32(additionalInfo.Content.Text);
                                                    break;
                                                case "213"://Khoan
                                                    chiTietThue.Khoan = Convert.ToInt32(additionalInfo.Content.Text);
                                                    break;
                                                case "214"://Muc
                                                    chiTietThue.Muc = Convert.ToInt32(additionalInfo.Content.Text);
                                                    break;
                                                case "215"://Tieu muc
                                                    chiTietThue.TieuMuc = Convert.ToInt32(additionalInfo.Content.Text);
                                                    break;
                                            }
                                    }
                                    chiTietThue.Insert();
                                }
                            }
                            TKMD.NamDK = TKMD.NgayDangKy.Year;
                            e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocPhanLuong, noidung);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", TKMD.MaHaiQuan, e);
                                break;
                            }
                    }
                    msgInfor = noidung;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    {
                        TKMD.Update();
                        if (GlobalSettings.SOTOKHAI_DONGBO > 0 && GlobalSettings.MA_DON_VI != null)
                        {
                            GlobalSettings.ISKHAIBAO = (GlobalSettings.SOTOKHAI_DONGBO > new ToKhaiMauDich().SelectCountSoTKThongQuan(GlobalSettings.MA_DON_VI));
                        }
                    }
                    if (isDeleteMsg)
                    {
                        DeleteMsgSend(LoaiKhaiBao.ToKhai, TKMD.ID);
                    }

                }
                else
                {
                    //DeleteMsgSend(LoaiKhaiBao.ToKhai, TKMD.ID);
                    e.Error.Message.XmlSaveMessage(TKMD.ID, MessageTitle.Error);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", TKMD.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, TKMD.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }


#if SXXK_V3 || SXXK_V4
        public static FeedBackContent NguyenPhuLieuSendHandler(NguyenPhuLieuDangKy nplDangKy, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                // nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            nplDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);


                            nplDangKy.NamDK = (short)nplDangKy.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                nplDangKy.SoTiepNhan,nplDangKy.NgayTiepNhan.ToString(sfmtVnDateTime),nplDangKy.NamDK
                            });
                            e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiNguyenPhuLieu, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;

                            if (nplDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt nguyên phụ liệu";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            }
                            else if (nplDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo nguyên phụ liệu";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoChapNhanHuyNguyenPhuLieu, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", nplDangKy.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, nplDangKy.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        nplDangKy.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, nplDangKy.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", nplDangKy.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, nplDangKy.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent SanPhamSendHandler(SanPhamDangKy sanpham, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (sanpham.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(sanpham.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((sanpham.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (sanpham.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || sanpham.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || sanpham.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            sanpham.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                sanpham.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                sanpham.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else sanpham.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            sanpham.NamDK = (short)sanpham.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                sanpham.SoTiepNhan,sanpham.NgayTiepNhan.ToString(sfmtVnDateTime),sanpham.NamDK
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, sanpham.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiSanPham, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (sanpham.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt sản phẩm";
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(sanpham.ID, MessageTitle.KhaiBaoHQDuyet, noidung);

                            }
                            else if (sanpham.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo sản phẩm";
                                sanpham.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(sanpham.ID, MessageTitle.KhaiBaoHQHuySanPham, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(sanpham.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", sanpham.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.SanPham, sanpham.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        sanpham.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.SanPham, sanpham.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", sanpham.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, sanpham.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent DinhMucSendHandler(DinhMucDangKy dinhmuc, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((dinhmuc.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (dinhmuc.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dinhmuc.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || dinhmuc.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            dinhmuc.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                dinhmuc.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                dinhmuc.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else dinhmuc.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            dinhmuc.NamDK = (short)dinhmuc.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                dinhmuc.SoTiepNhan,dinhmuc.NgayTiepNhan.ToString(sfmtVnDateTime),dinhmuc.NamDK
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dinhmuc.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiDinhMuc, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (dinhmuc.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt định mức";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.KhaiBaoHQDuyetDinhMuc, noidung);
                                dinhmuc.TransferDataToSXXK();
                            }
                            else if (dinhmuc.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo định mức";
                                dinhmuc.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.KhaiBaoHQHuyDinhMuc, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(dinhmuc.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", dinhmuc.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.DinhMuc, dinhmuc.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        dinhmuc.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.DinhMuc, dinhmuc.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", dinhmuc.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, dinhmuc.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent HangDuaVaoCX(HangDuaVaoDangKy nplDangKy, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                // nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            nplDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else nplDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);


                            nplDangKy.NamDK = (short)nplDangKy.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                nplDangKy.SoTiepNhan,nplDangKy.NgayTiepNhan.ToString(sfmtVnDateTime),nplDangKy.NamDK
                            });
                            e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiNguyenPhuLieu, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;

                            if (nplDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt nguyên phụ liệu";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            }
                            else if (nplDangKy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo nguyên phụ liệu";
                                nplDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoChapNhanHuyNguyenPhuLieu, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(nplDangKy.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", nplDangKy.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, nplDangKy.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        nplDangKy.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, nplDangKy.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", nplDangKy.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, nplDangKy.MaHaiQuan, new SendEventArgs(e.FeedBackMessage, new TimeSpan(), ex));
            }
            return feedbackContent;
        }

#endif

#if GC_V3 || GC_V4
        public static FeedBackContent DinhMucSendHandler(DinhMucDangKy dmdk, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            dmdk.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(dmdk.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((dmdk.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (dmdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dmdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || dmdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            dmdk.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                dmdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                dmdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                dmdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            //dmdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            dmdk.NamTN = dmdk.NgayTiepNhan.Year.ToString();

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                dmdk.SoTiepNhan,dmdk.NgayTiepNhan.ToString(sfmtVnDateTime),dmdk.NamTN
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dmdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiDinhMuc, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, dmdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            if (dmdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt định mức";
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (dmdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo định mức";
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(dmdk.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", dmdk.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.DinhMuc, dmdk.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        dmdk.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.DinhMuc, dmdk.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", dmdk.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, dmdk.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent HopDongSendHandler(HopDong hopdong, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            hopdong.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(hopdong.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if (hopdong.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                hopdong.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (hopdong.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || hopdong.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || hopdong.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                hopdong.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            hopdong.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                hopdong.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                hopdong.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                hopdong.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            //hopdong.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            hopdong.NamTN = hopdong.NgayTiepNhan.Year;

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                hopdong.SoTiepNhan,hopdong.NgayTiepNhan.ToString(sfmtVnDateTime),hopdong.NamTN
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, hopdong.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiHopDong, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            if (hopdong.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt hợp đồng";
                                hopdong.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, hopdong.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            }
                            else if (hopdong.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo hợp đồng";
                                hopdong.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, hopdong.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQHuyHopDong, noidung);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(hopdong.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", hopdong.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.HopDong, hopdong.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        hopdong.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.HopDong, hopdong.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", hopdong.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, hopdong.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent PhuKienSendHandler(PhuKienDangKy pkdk, ref string msgInfor, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            pkdk.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((pkdk.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            pkdk.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            //pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            //pkdk.NamDK = short.Parse(ketqua[1].Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            pkdk.NamDK = (short)(pkdk.NgayTiepNhan.Year);

                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                pkdk.SoTiepNhan,pkdk.NgayTiepNhan.ToString(sfmtVnDateTime),pkdk.NamDK
                            });
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiPhuKien, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            if (pkdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (pkdk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo";
                                pkdk.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(pkdk.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", pkdk.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.PhuKien, pkdk.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        pkdk.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.HopDong, pkdk.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", pkdk.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, pkdk.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent DNGiamSatTieuHuySendHandler(GiamSatTieuHuy gsTieuHuy, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            gsTieuHuy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());

                            if (feedbackContent.Acceptance.Length == 10)
                                gsTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                gsTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                gsTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            //gsTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);


                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                gsTieuHuy.SoTiepNhan,gsTieuHuy.NgayTiepNhan.ToString(sfmtVnDateTime),gsTieuHuy.NgayTiepNhan.Year
                            });
                            e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.CapSoTNDNTieuHuy, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.DuyetDNTieuHuy, noidung);
                            if (gsTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt giám sát tiêu hủy";
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

                            }
                            else if (gsTieuHuy.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo giám sát tiêu hủy";
                                gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.DuyetDNTieuHuy, noidung);
                            if (feedbackContent.AdditionalInformations[0].Statement == AdditionalInformationStatement.LUONG_DO)
                            {

                            }
                            else if (feedbackContent.AdditionalInformations[0].Statement == AdditionalInformationStatement.LUONG_XANH)
                            {

                            }
                            else if (feedbackContent.AdditionalInformations[0].Statement == AdditionalInformationStatement.LUONG_VANG)
                            {

                            }
                            gsTieuHuy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(gsTieuHuy.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", gsTieuHuy.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.GiamSatTieuHuy, gsTieuHuy.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        gsTieuHuy.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, gsTieuHuy.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", gsTieuHuy.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, gsTieuHuy.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent ThanhKhoanHDSendHandler(ThanhKhoan thanhKhoan, ref string msgInfor, SendEventArgs e)
        {
            Company.KDT.SHARE.Components.FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = GetErrorContent(feedbackContent);
                            if (thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            else
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(thanhKhoan.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = noidung.Split('/');

                            if ((thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Cấp số tiếp nhận hủy khai báo";
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                            }
                            else if (thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                noidung = "Cấp số tiếp nhận khai báo";
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            }

                            thanhKhoan.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            //thanhKhoan.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);

                            if (feedbackContent.Acceptance.Length == 10)
                                thanhKhoan.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                thanhKhoan.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else
                                thanhKhoan.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);


                            noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày : {1}\r\nNăm: {2}", new object[]{
                                thanhKhoan.SoTiepNhan,thanhKhoan.NgayTiepNhan.ToString(sfmtVnDateTime),thanhKhoan.NgayTiepNhan.Year
                            });
                            e.FeedBackMessage.XmlSaveMessage(thanhKhoan.ID, MessageTitle.KhaiBaoHQCapSoTiepNhan, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            e.FeedBackMessage.XmlSaveMessage(thanhKhoan.ID, MessageTitle.DuyetDNTieuHuy, noidung);
                            if (thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                noidung = "Hải quan đã duyệt yêu cầu thanh khoản";
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

                            }
                            else if (thanhKhoan.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                noidung = "Đã hủy khai báo yêu cầu thanh khoản";
                                thanhKhoan.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(thanhKhoan.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", thanhKhoan.MaHaiQuan, e);
                                break;
                            }

                    }
                    if (isDeleteMsg)
                        DeleteMsgSend(LoaiKhaiBao.ThanhKhoanHopDong, thanhKhoan.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        thanhKhoan.Update();
                    msgInfor = noidung;
                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.ThanhKhoanHopDong, thanhKhoan.ID);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", thanhKhoan.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, thanhKhoan.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        #region Tờ khai chuyển tiếp
        public static FeedBackContent ToKhaiSendHandlerCT(ToKhaiChuyenTiep TKCT, ref string msgInfor, object sender, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                        
                    }
                    bool isDeleteMsg = false;
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                isDeleteMsg = true;
                                noidung = GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && TKCT.SoToKhai > 0)
                                {
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                                }
                                else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || TKCT.SoToKhai == 0)
                                {
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                    TKCT.NgayTiepNhan = DateTime.Now;
                                }
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {

                                string[] vals = noidung.Split('/');

                                TKCT.SoTiepNhan = long.Parse(vals[0].Trim());
                                TKCT.NamDK = short.Parse(vals[1].Trim());

                                if (feedbackContent.Acceptance.Length == 10)
                                    TKCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    TKCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else TKCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                                //TKCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);

                                noidung = string.Format("Số tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nLoại hình: {3}\r\nHải quan: {4}", new object[] {
                                    TKCT.SoTiepNhan, TKCT.NgayTiepNhan.ToString(sfmtVnDateTime), TKCT.NamDK, TKCT.MaLoaiHinh, TKCT.MaHaiQuanTiepNhan });
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, noidung);
                                if (feedbackContent.Function == "29")
                                    if (TKCT.TrangThaiXuLy != TrangThaiXuLy.HUY_KHAI_BAO)
                                    {
                                        noidung = "Tờ khai được cấp số tiếp nhận\r\n" + noidung;
                                        TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                    }
                                    else
                                    {
                                        noidung = "Tờ khai được cấp số chờ hủy khai báo\r\n" + noidung;
                                        TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                    }
                                break;
                            }
                        case DeclarationFunction.CAP_SO_TO_KHAI:
                            {
                                TKCT.SoToKhai = int.Parse(feedbackContent.CustomsReference.Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    TKCT.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    TKCT.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                                else TKCT.NgayDangKy = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                                TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                noidung = string.Format("Tờ khai được cấp số\r\nSố tờ khai: {0} \r\nNgày tiếp nhận: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nNội dung hq: {4}",
                                    new object[] { feedbackContent.CustomsReference, feedbackContent.Acceptance, feedbackContent.NatureOfTransaction, feedbackContent.DeclarationOffice, noidung });
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiDuocCapSo, noidung);
                                isDeleteMsg = true;

                                //Bo sung: Hungtq, 4/2/2013, Cap nhat thong tin NPL vao ton thuc te
#if GC_V3 || GC_V4
                                if (GlobalSettings.MA_DON_VI != "4000395355")
                                    TKCT.TransferToNPLTon();
#endif
                            }
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            {
                                if (TKCT.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO || TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    //if (TKCT.SoTiepNhan.ToString() == feedbackContent.CustomsReference)
                                    //{
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                    noidung = "Tờ khai được chấp nhận hủy khai báo\r\n";
                                    e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.HQHuyKhaiBaoToKhai, noidung);
                                    //}
                                }
                                else if ((TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || TKCT.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET) && TKCT.SoToKhai.ToString() == feedbackContent.CustomsReference)
                                {
                                    if (!string.IsNullOrEmpty(noidung))
                                    {
                                        //TKCT.HUONGDAN = noidung;
                                        noidung = "Tờ khai được chấp nhận thông quan nội dung: " + noidung + "\r\n";
                                    }
                                    else noidung = "Tờ khai được chấp nhận thông quan";
                                    e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiSuaDuocDuyet, noidung);
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }
                                else
                                    e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.None, noidung);
                                

                                isDeleteMsg = true;
                            }
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            {
                                TKCT.HUONGDAN = noidung;
                                string idTax = string.Empty;
                                string tenKhobac = string.Empty;
                                foreach (AdditionalInformation item in feedbackContent.AdditionalInformations)
                                {
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                    if (item.Statement == AdditionalInformationStatement.LUONG_XANH)
                                        TKCT.PhanLuong = TrangThaiPhanLuong.LUONG_XANH;
                                    else if (item.Statement == AdditionalInformationStatement.LUONG_VANG)
                                        TKCT.PhanLuong = TrangThaiPhanLuong.LUONG_VANG;
                                    else if (item.Statement == AdditionalInformationStatement.LUONG_DO)
                                        TKCT.PhanLuong = TrangThaiPhanLuong.LUONG_DO;
                                    else if (item.Statement == AdditionalInformationStatement.TAI_KHOAN_KHO_BAC)
                                    {
                                        idTax = item.Content.Text;
                                    }
                                    else if (item.Statement == AdditionalInformationStatement.TEN_KHO_BAC)
                                    {
                                        tenKhobac = item.Content.Text;
                                    }
                                }
                                if (idTax != string.Empty && feedbackContent.AdditionalDocument != null)
                                {
                                    noidung += "\r\nThông báo thuế của hệ thống hải quan:\r\n";
                                }
                                if (idTax != string.Empty && feedbackContent.AdditionalDocument != null)
                                {
                                    noidung += "\r\nThông báo thuế của hệ thống hải quan:\r\n";



                                    AnDinhThue andinhthue = new AnDinhThue();
                                    andinhthue.TaiKhoanKhoBac = idTax;
                                    andinhthue.TKMD_ID = TKCT.ID;
                                    andinhthue.TKMD_Ref = TKCT.GUIDSTR;
                                    andinhthue.NgayQuyetDinh = GetDate(feedbackContent.AdditionalDocument.Issue);
                                    andinhthue.NgayHetHan = GetDate(feedbackContent.AdditionalDocument.Expire);
                                    andinhthue.SoQuyetDinh = feedbackContent.AdditionalDocument.Reference.Trim();
                                    andinhthue.TenKhoBac = tenKhobac;
                                    noidung += "Số quyết định: " + andinhthue.SoQuyetDinh;
                                    noidung += "\r\nTài khoản kho bạc: " + idTax;
                                    noidung += "\r\nTên kho bạc: " + tenKhobac;
                                    noidung += "\r\nNgày quyết định: " + andinhthue.NgayQuyetDinh.ToLongDateString();
                                    noidung += "\r\nNgày hết hạn: " + andinhthue.NgayHetHan.ToLongDateString();

                                    andinhthue.Insert();


                                    foreach (DutyTaxFee item in feedbackContent.AdditionalDocument.DutyTaxFees)
                                    {
                                        ChiTiet chiTietThue = new ChiTiet();
                                        chiTietThue.IDAnDinhThue = andinhthue.ID;
                                        chiTietThue.TienThue = Convert.ToDecimal(item.AdValoremTaxBase);

                                        switch (item.Type.Trim())
                                        {
                                            case DutyTaxFeeType.THUE_XNK:
                                                chiTietThue.SacThue = "XNK";
                                                break;
                                            case DutyTaxFeeType.THUE_VAT:
                                                chiTietThue.SacThue = "VAT";
                                                break;
                                            case DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET:
                                                chiTietThue.SacThue = "TTDB";
                                                break;
                                            case DutyTaxFeeType.THUE_KHAC:
                                                chiTietThue.SacThue = "TVCBPG";
                                                break;
                                            default:
                                                break;
                                        }

                                        foreach (AdditionalInformation additionalInfo in item.AdditionalInformations)
                                        {
                                            if (!string.IsNullOrEmpty(additionalInfo.Content.Text))
                                                switch (additionalInfo.Statement)
                                                {
                                                    case "211"://Chuong
                                                        chiTietThue.Chuong = additionalInfo.Content.Text;
                                                        break;
                                                    case "212"://Loai
                                                        chiTietThue.Loai = Convert.ToInt32(additionalInfo.Content.Text);
                                                        break;
                                                    case "213"://Khoan
                                                        chiTietThue.Khoan = Convert.ToInt32(additionalInfo.Content.Text);
                                                        break;
                                                    case "214"://Muc
                                                        chiTietThue.Muc = Convert.ToInt32(additionalInfo.Content.Text);
                                                        break;
                                                    case "215"://Tieu muc
                                                        chiTietThue.TieuMuc = Convert.ToInt32(additionalInfo.Content.Text);
                                                        break;
                                                }
                                        }
                                        chiTietThue.Insert();
                                    }
                                }
                            }
                            e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiDuocPhanLuong, noidung);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", TKCT.MaHaiQuanTiepNhan, e);
                                break;
                            }
                    }
                    msgInfor = noidung;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        TKCT.Update();
                    if (isDeleteMsg)
                    {
                        DeleteMsgSend(LoaiKhaiBao.ToKhaiCT, TKCT.ID);
                    }

                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.ToKhai, TKCT.ID);
                    e.Error.Message.XmlSaveMessage(TKCT.ID, MessageTitle.Error);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", TKCT.MaHaiQuanTiepNhan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, TKCT.MaHaiQuanTiepNhan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        public static FeedBackContent GiaHanThanhKhoan(GiaHanThanhKhoan giaHanThanhKhoan, ref string msgInfor, SendEventArgs e)
        {
            FeedBackContent feedbackContent = null;
            try
            {
                msgInfor = string.Empty;
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    bool isDeleteMsg = false;
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                isDeleteMsg = true;
                                noidung = GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.TuChoiGiaHanTK, noidung);
                                giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {

                                string[] vals = noidung.Split('/');

                                giaHanThanhKhoan.SoTiepNhan = long.Parse(vals[0].Trim());

                                giaHanThanhKhoan.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);

                                noidung = string.Format("Số tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nHải quan: {3}", new object[] {
                                    giaHanThanhKhoan.SoTiepNhan, giaHanThanhKhoan.NgayTiepNhan.ToString(sfmtVnDateTime), vals[1].Trim(),giaHanThanhKhoan.MaHaiQuan });
                                e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.CapSoTNGiaHanTK, noidung);
                                if (giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                {
                                    noidung = "Cấp số tiếp nhận hủy khai báo\r\n" + noidung;
                                    giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                else
                                {
                                    noidung = "Cấp số tiếp nhận khai báo\r\n" + noidung;
                                    giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                }
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                if (
                                    (
                                    giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO
                                    || giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY
                                    )
                                    && giaHanThanhKhoan.SoTiepNhan.ToString() == feedbackContent.CustomsReference)
                                {
                                    giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                    noidung = "Chấp nhận hủy khai báo\r\n";
                                    e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.HuyKhaiBaoGiaHanTK, noidung);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(noidung))
                                    {
                                        noidung = "Hải quan đã duyệt nội dung: " + noidung + "\r\n";
                                    }
                                    else noidung = "Hải quan đã duyệt";
                                    e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.DuyetGiaHanTK, noidung);
                                    giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                }

                                isDeleteMsg = true;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", giaHanThanhKhoan.MaHaiQuan, e);
                                break;
                            }
                    }
                    msgInfor = noidung;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        giaHanThanhKhoan.Update();
                    if (isDeleteMsg)
                    {
                        DeleteMsgSend(LoaiKhaiBao.GiaHanThanhKhoan, giaHanThanhKhoan.ID);
                    }

                }
                else
                {
                    DeleteMsgSend(LoaiKhaiBao.GiaHanThanhKhoan, giaHanThanhKhoan.ID);
                    e.Error.Message.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.Error);
                    SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", giaHanThanhKhoan.MaHaiQuan, e);

                }

            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SendMail(msgTitle, giaHanThanhKhoan.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
            return feedbackContent;
        }
        #endregion
#endif
        #endregion Process Message
    }
}
