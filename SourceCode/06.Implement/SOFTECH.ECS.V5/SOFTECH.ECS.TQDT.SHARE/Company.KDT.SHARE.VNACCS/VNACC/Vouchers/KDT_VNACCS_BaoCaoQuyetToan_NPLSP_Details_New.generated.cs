﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GoodItem_ID { set; get; }
		public decimal STT { set; get; }
		public string TenHangHoa { set; get; }
		public string MaHangHoa { set; get; }
		public string DVT { set; get; }
		public decimal TonDauKy { set; get; }
		public decimal NhapTrongKy { set; get; }
		public decimal TaiXuat { set; get; }
		public decimal ChuyenMDSD { set; get; }
		public decimal XuatKhac { set; get; }
		public decimal XuatTrongKy { set; get; }
		public decimal TonCuoiKy { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> collection = new List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New>();
			while (reader.Read())
			{
				KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New entity = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GoodItem_ID"))) entity.GoodItem_ID = reader.GetInt64(reader.GetOrdinal("GoodItem_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetDecimal(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangHoa"))) entity.TenHangHoa = reader.GetString(reader.GetOrdinal("TenHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangHoa"))) entity.MaHangHoa = reader.GetString(reader.GetOrdinal("MaHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonDauKy"))) entity.TonDauKy = reader.GetDecimal(reader.GetOrdinal("TonDauKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhapTrongKy"))) entity.NhapTrongKy = reader.GetDecimal(reader.GetOrdinal("NhapTrongKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TaiXuat"))) entity.TaiXuat = reader.GetDecimal(reader.GetOrdinal("TaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMDSD"))) entity.ChuyenMDSD = reader.GetDecimal(reader.GetOrdinal("ChuyenMDSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("XuatKhac"))) entity.XuatKhac = reader.GetDecimal(reader.GetOrdinal("XuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("XuatTrongKy"))) entity.XuatTrongKy = reader.GetDecimal(reader.GetOrdinal("XuatTrongKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonCuoiKy"))) entity.TonCuoiKy = reader.GetDecimal(reader.GetOrdinal("TonCuoiKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> collection, long id)
        {
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New VALUES(@GoodItem_ID, @STT, @TenHangHoa, @MaHangHoa, @DVT, @TonDauKy, @NhapTrongKy, @TaiXuat, @ChuyenMDSD, @XuatKhac, @XuatTrongKy, @TonCuoiKy, @GhiChu)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New SET GoodItem_ID = @GoodItem_ID, STT = @STT, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, DVT = @DVT, TonDauKy = @TonDauKy, NhapTrongKy = @NhapTrongKy, TaiXuat = @TaiXuat, ChuyenMDSD = @ChuyenMDSD, XuatKhac = @XuatKhac, XuatTrongKy = @XuatTrongKy, TonCuoiKy = @TonCuoiKy, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GoodItem_ID", SqlDbType.BigInt, "GoodItem_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TonDauKy", SqlDbType.Decimal, "TonDauKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhapTrongKy", SqlDbType.Decimal, "NhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaiXuat", SqlDbType.Decimal, "TaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuyenMDSD", SqlDbType.Decimal, "ChuyenMDSD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatKhac", SqlDbType.Decimal, "XuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatTrongKy", SqlDbType.Decimal, "XuatTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TonCuoiKy", SqlDbType.Decimal, "TonCuoiKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GoodItem_ID", SqlDbType.BigInt, "GoodItem_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TonDauKy", SqlDbType.Decimal, "TonDauKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhapTrongKy", SqlDbType.Decimal, "NhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaiXuat", SqlDbType.Decimal, "TaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuyenMDSD", SqlDbType.Decimal, "ChuyenMDSD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatKhac", SqlDbType.Decimal, "XuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatTrongKy", SqlDbType.Decimal, "XuatTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TonCuoiKy", SqlDbType.Decimal, "TonCuoiKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New VALUES(@GoodItem_ID, @STT, @TenHangHoa, @MaHangHoa, @DVT, @TonDauKy, @NhapTrongKy, @TaiXuat, @ChuyenMDSD, @XuatKhac, @XuatTrongKy, @TonCuoiKy, @GhiChu)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New SET GoodItem_ID = @GoodItem_ID, STT = @STT, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, DVT = @DVT, TonDauKy = @TonDauKy, NhapTrongKy = @NhapTrongKy, TaiXuat = @TaiXuat, ChuyenMDSD = @ChuyenMDSD, XuatKhac = @XuatKhac, XuatTrongKy = @XuatTrongKy, TonCuoiKy = @TonCuoiKy, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GoodItem_ID", SqlDbType.BigInt, "GoodItem_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TonDauKy", SqlDbType.Decimal, "TonDauKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhapTrongKy", SqlDbType.Decimal, "NhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaiXuat", SqlDbType.Decimal, "TaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuyenMDSD", SqlDbType.Decimal, "ChuyenMDSD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatKhac", SqlDbType.Decimal, "XuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatTrongKy", SqlDbType.Decimal, "XuatTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TonCuoiKy", SqlDbType.Decimal, "TonCuoiKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GoodItem_ID", SqlDbType.BigInt, "GoodItem_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TonDauKy", SqlDbType.Decimal, "TonDauKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhapTrongKy", SqlDbType.Decimal, "NhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaiXuat", SqlDbType.Decimal, "TaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuyenMDSD", SqlDbType.Decimal, "ChuyenMDSD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatKhac", SqlDbType.Decimal, "XuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatTrongKy", SqlDbType.Decimal, "XuatTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TonCuoiKy", SqlDbType.Decimal, "TonCuoiKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> SelectCollectionBy_GoodItem_ID(long goodItem_ID)
		{
            IDataReader reader = SelectReaderBy_GoodItem_ID(goodItem_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_GoodItem_ID(long goodItem_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectBy_GoodItem_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GoodItem_ID", SqlDbType.BigInt, goodItem_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_GoodItem_ID(long goodItem_ID)
		{
			const string spName = "p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectBy_GoodItem_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GoodItem_ID", SqlDbType.BigInt, goodItem_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New(long goodItem_ID, decimal sTT, string tenHangHoa, string maHangHoa, string dVT, decimal tonDauKy, decimal nhapTrongKy, decimal taiXuat, decimal chuyenMDSD, decimal xuatKhac, decimal xuatTrongKy, decimal tonCuoiKy, string ghiChu)
		{
			KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New entity = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();	
			entity.GoodItem_ID = goodItem_ID;
			entity.STT = sTT;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.DVT = dVT;
			entity.TonDauKy = tonDauKy;
			entity.NhapTrongKy = nhapTrongKy;
			entity.TaiXuat = taiXuat;
			entity.ChuyenMDSD = chuyenMDSD;
			entity.XuatKhac = xuatKhac;
			entity.XuatTrongKy = xuatTrongKy;
			entity.TonCuoiKy = tonCuoiKy;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GoodItem_ID", SqlDbType.BigInt, GoodItem_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@TonDauKy", SqlDbType.Decimal, TonDauKy);
			db.AddInParameter(dbCommand, "@NhapTrongKy", SqlDbType.Decimal, NhapTrongKy);
			db.AddInParameter(dbCommand, "@TaiXuat", SqlDbType.Decimal, TaiXuat);
			db.AddInParameter(dbCommand, "@ChuyenMDSD", SqlDbType.Decimal, ChuyenMDSD);
			db.AddInParameter(dbCommand, "@XuatKhac", SqlDbType.Decimal, XuatKhac);
			db.AddInParameter(dbCommand, "@XuatTrongKy", SqlDbType.Decimal, XuatTrongKy);
			db.AddInParameter(dbCommand, "@TonCuoiKy", SqlDbType.Decimal, TonCuoiKy);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New(long id, long goodItem_ID, decimal sTT, string tenHangHoa, string maHangHoa, string dVT, decimal tonDauKy, decimal nhapTrongKy, decimal taiXuat, decimal chuyenMDSD, decimal xuatKhac, decimal xuatTrongKy, decimal tonCuoiKy, string ghiChu)
		{
			KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New entity = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();			
			entity.ID = id;
			entity.GoodItem_ID = goodItem_ID;
			entity.STT = sTT;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.DVT = dVT;
			entity.TonDauKy = tonDauKy;
			entity.NhapTrongKy = nhapTrongKy;
			entity.TaiXuat = taiXuat;
			entity.ChuyenMDSD = chuyenMDSD;
			entity.XuatKhac = xuatKhac;
			entity.XuatTrongKy = xuatTrongKy;
			entity.TonCuoiKy = tonCuoiKy;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GoodItem_ID", SqlDbType.BigInt, GoodItem_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@TonDauKy", SqlDbType.Decimal, TonDauKy);
			db.AddInParameter(dbCommand, "@NhapTrongKy", SqlDbType.Decimal, NhapTrongKy);
			db.AddInParameter(dbCommand, "@TaiXuat", SqlDbType.Decimal, TaiXuat);
			db.AddInParameter(dbCommand, "@ChuyenMDSD", SqlDbType.Decimal, ChuyenMDSD);
			db.AddInParameter(dbCommand, "@XuatKhac", SqlDbType.Decimal, XuatKhac);
			db.AddInParameter(dbCommand, "@XuatTrongKy", SqlDbType.Decimal, XuatTrongKy);
			db.AddInParameter(dbCommand, "@TonCuoiKy", SqlDbType.Decimal, TonCuoiKy);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New(long id, long goodItem_ID, decimal sTT, string tenHangHoa, string maHangHoa, string dVT, decimal tonDauKy, decimal nhapTrongKy, decimal taiXuat, decimal chuyenMDSD, decimal xuatKhac, decimal xuatTrongKy, decimal tonCuoiKy, string ghiChu)
		{
			KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New entity = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();			
			entity.ID = id;
			entity.GoodItem_ID = goodItem_ID;
			entity.STT = sTT;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.DVT = dVT;
			entity.TonDauKy = tonDauKy;
			entity.NhapTrongKy = nhapTrongKy;
			entity.TaiXuat = taiXuat;
			entity.ChuyenMDSD = chuyenMDSD;
			entity.XuatKhac = xuatKhac;
			entity.XuatTrongKy = xuatTrongKy;
			entity.TonCuoiKy = tonCuoiKy;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GoodItem_ID", SqlDbType.BigInt, GoodItem_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@TonDauKy", SqlDbType.Decimal, TonDauKy);
			db.AddInParameter(dbCommand, "@NhapTrongKy", SqlDbType.Decimal, NhapTrongKy);
			db.AddInParameter(dbCommand, "@TaiXuat", SqlDbType.Decimal, TaiXuat);
			db.AddInParameter(dbCommand, "@ChuyenMDSD", SqlDbType.Decimal, ChuyenMDSD);
			db.AddInParameter(dbCommand, "@XuatKhac", SqlDbType.Decimal, XuatKhac);
			db.AddInParameter(dbCommand, "@XuatTrongKy", SqlDbType.Decimal, XuatTrongKy);
			db.AddInParameter(dbCommand, "@TonCuoiKy", SqlDbType.Decimal, TonCuoiKy);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New(long id)
		{
			KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New entity = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int DeleteBy_GoodItem_ID(long goodItem_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteBy_GoodItem_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GoodItem_ID", SqlDbType.BigInt, goodItem_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}