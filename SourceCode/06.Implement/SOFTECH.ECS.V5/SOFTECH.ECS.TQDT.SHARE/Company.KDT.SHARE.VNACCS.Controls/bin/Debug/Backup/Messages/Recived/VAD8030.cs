﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS;

namespace Company.KDT.SHARE.VNACCS.Messages.Recived
{
   public partial class VAD8030 :BasicVNACC
    {
       public List<VAD8030_HANG> HangHoa { get; set; }
       public void LoadVAD8030(string strResult)
       {
           try
           {
               this.GetObject<VAD8030>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD8030, false, VAD8030.TongSoByte);
               strResult = HelperVNACCS.SubStringByBytes(strResult, VAD8030.TongSoByte);
               while (true)
               {
                   VAD8030_HANG ivaHang = new VAD8030_HANG();
                   ivaHang.GetObject<VAD8030_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD8030_HANG", false, VAD8030_HANG.TongSoByte);
                   if (this.HangHoa == null) this.HangHoa = new List<VAD8030_HANG>();
                   this.HangHoa.Add(ivaHang);
                   if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD8030_HANG.TongSoByte)
                   {
                       if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD8030_HANG.TongSoByte > 4)
                       {
                           strResult = HelperVNACCS.SubStringByBytes(strResult, VAD8030_HANG.TongSoByte);
                           continue;
                       }
                   }
                   break;
               }
           }
           catch (System.Exception ex)
           {
               throw ex;
           }



       }
    }
}
