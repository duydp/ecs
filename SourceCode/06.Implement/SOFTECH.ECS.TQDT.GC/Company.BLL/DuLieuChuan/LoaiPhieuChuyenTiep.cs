using System.Data;
using System.Data.Common;

namespace Company.GC.BLL.DuLieuChuan
{
    public class LoaiPhieuChuyenTiep : BaseClass
    {
        public static DataTable SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_LoaiPhieuChuyenTiep ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static DataSet SelectBy_ID(string ID)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_LoaiPhieuChuyenTiep WHERE ID LIKE '%{0}%'", ID);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }
        public static string GetName(string ID)
        {

            string query = string.Format("SELECT Ten FROM t_HaiQuan_LoaiPhieuChuyenTiep WHERE ID = '{0}'", ID);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteScalar(dbCommand).ToString();
        }
    }
}