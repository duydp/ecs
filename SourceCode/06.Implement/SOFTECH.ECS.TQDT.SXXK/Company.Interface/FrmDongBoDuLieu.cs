﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class FrmDongBoDuLieu : BaseForm
    {
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        private string nhomLoaiHinh = "";

        public FrmDongBoDuLieu()
        {
            InitializeComponent();
        }

        private void FrmDongBoDuLieu_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                txtNamTiepNhan.Text = DateTime.Today.Year.ToString();

                //Mac dinh chi hien thi trang thai to khai "DA DUYET" = 1
                cbStatus.SelectedValue = 1;

                this.setDataToCboPhanLuong();
                cboPhanLuong.SelectedIndex = 0;

                this.search();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Đồng bộ dữ liệu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.GroupHeader)
                    {
                        Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Chưa chọn thông tin tờ khai.", false);
                        break;
                    }

                    if (i.RowType == RowType.Record)
                    {
                        if (Convert.ToBoolean(i.GetRow().Cells["chkTK"].Value) == true)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Lỗi trong quá trình thực hiện:\r\n" + ex.Message + "\nChi tiết:" + ex.StackTrace, false);
            }
        }

        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "1 = 1";
            where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan = " + txtSoTiepNhan.Value;
            }

            if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                }
            }
            if (cbStatus.SelectedValue != null)
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;
            if (this.nhomLoaiHinh != "") where += " AND MaLoaiHinh LIKE '%" + this.nhomLoaiHinh + "%'";
            if (cboPhanLuong.SelectedValue != null && cboPhanLuong.SelectedIndex != 0)
                where += " AND PhanLuong=" + cboPhanLuong.SelectedValue;

            // Thực hiện tìm kiếm.            
            this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");

            dgList.DataSource = this.tkmdCollection;

        }

        //Lypt create date 20/01/2010
        private void setDataToCboPhanLuong()
        {
            DataTable dtPL = new DataTable();
            dtPL.Columns.Add("ID", typeof(System.String));
            dtPL.Columns.Add("GhiChu", typeof(System.String));
            DataRow drNull = dtPL.NewRow();
            drNull["ID"] = -1;
            drNull["GhiChu"] = "--Tất cả--";
            dtPL.Rows.Add(drNull);
            for (int i = 1; i <= 3; i++)
            {
                DataRow dr = dtPL.NewRow();
                dr["ID"] = i.ToString();
                if (i == 1)
                    dr["GhiChu"] = "Luồng xanh";
                if (i == 2)
                    dr["GhiChu"] = "Luồng vàng";
                if (i == 3)
                    dr["GhiChu"] = "Luồng đỏ";
                dtPL.Rows.Add(dr);
            }
            cboPhanLuong.DataSource = dtPL;
            cboPhanLuong.DisplayMember = dtPL.Columns["GhiChu"].ToString();
            cboPhanLuong.ValueMember = dtPL.Columns["ID"].ToString();

        }


    }
}
