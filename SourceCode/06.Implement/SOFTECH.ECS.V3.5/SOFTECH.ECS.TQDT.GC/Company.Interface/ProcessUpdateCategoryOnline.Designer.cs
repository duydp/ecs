﻿namespace Company.Interface
{
    partial class ProcessUpdateCategoryOnline
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grdProcess_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcessUpdateCategoryOnline));
            this.pbProgress = new Janus.Windows.EditControls.UIProgressBar();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grdProcess = new Janus.Windows.GridEX.GridEX();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dataSet1 = new System.Data.DataSet();
            this.dtProcess = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.pbProgress);
            this.grbMain.Size = new System.Drawing.Size(596, 224);
            // 
            // pbProgress
            // 
            this.pbProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pbProgress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbProgress.Location = new System.Drawing.Point(0, 201);
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.ShowPercentage = true;
            this.pbProgress.Size = new System.Drawing.Size(596, 23);
            this.pbProgress.TabIndex = 31;
            this.pbProgress.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.grdProcess);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(596, 201);
            this.uiGroupBox1.TabIndex = 34;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // grdProcess
            // 
            this.grdProcess.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdProcess.ColumnAutoResize = true;
            this.grdProcess.DataMember = "Table1";
            this.grdProcess.DataSource = this.dataSet1;
            grdProcess_DesignTimeLayout.LayoutString = resources.GetString("grdProcess_DesignTimeLayout.LayoutString");
            this.grdProcess.DesignTimeLayout = grdProcess_DesignTimeLayout;
            this.grdProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdProcess.GroupByBoxVisible = false;
            this.grdProcess.Location = new System.Drawing.Point(3, 8);
            this.grdProcess.Name = "grdProcess";
            this.grdProcess.Size = new System.Drawing.Size(590, 190);
            this.grdProcess.TabIndex = 32;
            this.grdProcess.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grdProcess.VisualStyleManager = this.vsmMain;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.dtProcess});
            // 
            // dtProcess
            // 
            this.dtProcess.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3});
            this.dtProcess.TableName = "Table1";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "TenProcess";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "TrangThai";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "TotalRecord";
            // 
            // ProcessUpdateCategoryOnline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 224);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximumSize = new System.Drawing.Size(602, 253);
            this.MinimumSize = new System.Drawing.Size(602, 253);
            this.Name = "ProcessUpdateCategoryOnline";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cập nhật danh mục Online";
            this.Load += new System.EventHandler(this.ProcessUpdateCategoryOnline_Load);
            this.Shown += new System.EventHandler(this.ProcessUpdateCategoryOnline_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtProcess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIProgressBar pbProgress;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX grdProcess;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable dtProcess;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
    }
}