using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_CX_TaiXuatDangKy : ICloneable
	{
        private List<KDT_CX_TaiXuat> _ListTaiXuat = new List<KDT_CX_TaiXuat>();

        public List<KDT_CX_TaiXuat> TaiXuatList
        {
            set { this._ListTaiXuat = value; }
            get { return this._ListTaiXuat; }
        }

        public static KDT_CX_TaiXuatDangKy LoadFull(long id)
        {
            try
            {
                KDT_CX_TaiXuatDangKy TXDK = new KDT_CX_TaiXuatDangKy();
                TXDK = KDT_CX_TaiXuatDangKy.Load(id);
                List<KDT_CX_TaiXuat> ListTX = new List<KDT_CX_TaiXuat>();
                ListTX =  KDT_CX_TaiXuat.SelectCollectionDynamic( " Master_id="+TXDK.ID,"");
                if (ListTX.Count > 0)
                {
                    TXDK.TaiXuatList = ListTX;
                }
                return TXDK;

            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public bool InsertUpdatFull()
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update(transaction);
                    foreach (KDT_CX_TaiXuat item in this.TaiXuatList)
                    {
                        item.Master_id = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                    throw;
                }
            }
            return ret;
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            KDT_CX_TaiXuat.DeleteDynamic("Master_ID = " + id);
            this.Delete();
        }
            

	}	
}