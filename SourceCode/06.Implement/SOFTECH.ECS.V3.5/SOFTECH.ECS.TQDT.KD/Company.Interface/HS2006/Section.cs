using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;

namespace HaiQuan.HS
{
	public class Sections
	{
		private string _ConnectionString = ConfigurationSettings.AppSettings["HSConnectionString"];
		public OleDbDataReader SelectAll()
		{
			string query = "SELECT * FROM t_Sections";
			return OleDbHelper.ExecuteReader(this._ConnectionString, CommandType.Text, query);
		}
	}
}
