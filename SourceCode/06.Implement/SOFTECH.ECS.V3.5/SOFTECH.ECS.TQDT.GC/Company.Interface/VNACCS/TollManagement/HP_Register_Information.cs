﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
#elif SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
#elif KD_V4
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
#endif
using Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP;
using System.Security.Cryptography.X509Certificates;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components.Messages;
using System.Security.Cryptography;

namespace Company.Interface.VNACCS.TollManagement
{
    public partial class HP_Register_Information : Company.Interface.BaseFormHaveGuidPanel
    {
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public IntPtr parentFormHandler;
        public T_KDT_THUPHI_DOANHNGHIEP customer = new T_KDT_THUPHI_DOANHNGHIEP();
        public HP_Register_Information()
        {
            InitializeComponent();
        }

        private void HP_Register_Information_Load(object sender, EventArgs e)
        {
            if (customer.ID >0)
            {
                SetCustomer();
            }
            else
            {
                txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
                txtTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                txtDiaChi.Text = GlobalSettings.DIA_CHI;                
                txtSoDienThoai.Text = GlobalSettings.SoDienThoaiDN;
                txtNguoiLienHe.Text = GlobalSettings.NguoiLienHe;
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send();
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidstring":
                    UpdateGuidstring();
                    break;
            }
        }

        private void GetCustomer()
        {
            try
            {
                if (customer == null)
                    customer = new T_KDT_THUPHI_DOANHNGHIEP();
                customer.NgayTiepNhan = DateTime.Now;
                customer.MaDoanhNghiep = txtMaDoanhNghiep.Text.ToString();
                customer.TenDoanhNghiep = txtTenDoanhNghiep.Text.ToString();
                customer.DiaChi = txtDiaChi.Text.ToString();
                customer.NguoiLienHe = txtNguoiLienHe.Text.ToString();
                customer.SoDienThoai = txtSoDienThoai.Text.ToString();
                customer.Email = txtEmail.Text.ToString();

                customer.Serial = txtSoSerial.Text.ToString();
                customer.Subject = txtTenChuThe.Text.ToString();
                customer.CertString = txtCert.Text.ToString();
                customer.Issuer = txtDonViCap.Text.ToString();
                customer.ValidFrom = clcNgayBatDau.Value;
                customer.ValidTo = clcNgayHetHan.Value;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetCustomer()
        {
            try
            {
                txtMaDoanhNghiep.Text = customer.MaDoanhNghiep;
                txtTenDoanhNghiep.Text = customer.TenDoanhNghiep;
                txtDiaChi.Text = customer.DiaChi;
                txtNguoiLienHe.Text = customer.NguoiLienHe;
                txtSoDienThoai.Text = customer.SoDienThoai;
                txtEmail.Text = customer.Email;

                txtSoSerial.Text = customer.Serial;
                txtTenChuThe.Text = customer.Subject;
                txtCert.Text = customer.CertString;
                txtDonViCap.Text = customer.Issuer;
                clcNgayBatDau.Value = customer.ValidFrom;
                clcNgayHetHan.Value = customer.ValidTo;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void UpdateGuidstring()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("T_KDT_ThuPhi_DoanhNghiep", "", Convert.ToInt32(customer.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = customer.ID;
                form.DeclarationIssuer = DeclarationIssuer.RegisterInformation;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.RegisterInformaton;
            sendXML.master_id = customer.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                return;
            }
            while (isFeedBack)
            {
                string reference = customer.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.RegisterInformation,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    //Type = DeclarationIssuer.RegisterInformation,
                };
                //subjectBase.Type = DeclarationIssuer.RegisterInformation;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = customer.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(customer.MaHQ.Trim())),
                                                  Identity = customer.MaHQ
                                              }, subjectBase, null);
                if (customer.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(customer.MaHQ));
                    msgSend.To.Identity = customer.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                sendForm.IsPort = true;
                sendForm.RegisterAccount = true;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không ?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        Feedback();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không ?", true) == "Yes";
                    }
                }
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMaDoanhNghiep, errorProvider, "MÃ DOANH NGHIỆP", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDoanhNghiep, errorProvider, "TÊN DOANH NGHIỆP", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChi, errorProvider, "ĐỊA CHỈ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoDienThoai, errorProvider, "SỐ ĐIỆN THOẠI", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNguoiLienHe, errorProvider, "NGƯỜI LIÊN HỆ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtEmail, errorProvider, "EMAIL", isOnlyWarning);

                isValid &= ValidateControl.ValidateNull(txtCert, errorProvider, "CHỮ KÝ SỐ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetCustomer();
                customer.InsertUpdate();
                ShowMessageTQDT("Lưu thành công !", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Send()
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (customer.ID == 0)
                {
                    GetCustomer();
                    customer.InsertUpdate();
                }
                else
                {
                    customer = T_KDT_THUPHI_DOANHNGHIEP.Load(customer.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.RegisterInformaton;
                sendXML.master_id = customer.ID;

                if (sendXML.Load())
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    customer.GuidStr = Guid.NewGuid().ToString();
                    Register_Information register_Information = new Register_Information();
                    register_Information = Mapper_V4.ToDataTransferRegisterInformation(customer, GlobalSettings.TEN_DON_VI);
                    ObjectSend msgSend = new ObjectSend(
                        new MessageHeader()
                        {
                            ProcedureType = "2",
                            Reference = new Reference()
                            {
                                Version = "3.00",
                                MessageID = customer.GuidStr,
                            },
                            SendApplication = new SendApplication()
                            {
                                Name = "ECUS3",
                                Version = "3.00",
                                CompanyName = "ThaiSon",
                                CompanyIdentity = "0101300842",
                                CreateMessageIssue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            },
                            From = new NameBase()
                            {
                                Name = customer.TenDoanhNghiep,
                                Identity = customer.MaDoanhNghiep,
                            },
                            To = new NameBase()
                            {
                                Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(GlobalSettings.MA_HAI_QUAN)),
                                Identity = GlobalSettings.MA_HAI_QUAN,
                            },
                            Subject = new Subject()
                            {
                                Type = DeclarationIssuer.RegisterInformation,
                                Function = DeclarationFunction.KHAI_BAO,
                                Reference = customer.GuidStr,
                            }
                        },
                        new NameBase()
                        {
                            Name = customer.TenDoanhNghiep,
                            Identity = customer.MaDoanhNghiep,
                        },
                        new NameBase()
                        {
                            Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(GlobalSettings.MA_HAI_QUAN)),
                            Identity = GlobalSettings.MA_HAI_QUAN,
                        },
                        new Subject()
                        {
                            Type = DeclarationIssuer.RegisterInformation,
                            Function = DeclarationFunction.KHAI_BAO,
                            Reference = customer.GuidStr,
                        },
                        register_Information
                        );
                    customer.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();                    
                    sendForm.Send += SendMessage;
                    sendForm.IsPort = true;
                    sendForm.RegisterAccount = true;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN && feedbackContent.Function != DeclarationFunction.LOI_KHI_XU_LY)
                    {
                        sendForm.Message.XmlSaveMessage(customer.ID, MessageTitle.RegisterInformation);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.RegisterInformaton;
                        sendXML.master_id = customer.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(register_Information.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            ShowMessageTQDT(msgInfor, false);
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI)
                        {
                            ShowMessageTQDT(msgInfor, false);                            
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.RegisterInformationSendHandler(customer, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void btnSelectCert_Click(object sender, EventArgs e)
        {
            try
            {
                X509Certificate2 x509Certificate2 = SoftechSignLibrary.GetCertificate(parentFormHandler);
                if (x509Certificate2 != null)
                {
                    txtSoSerial.Text = x509Certificate2.SerialNumber;
                    txtTenChuThe.Text = x509Certificate2.Subject;
                    txtCert.Text = Convert.ToBase64String(x509Certificate2.RawData);
                    txtDonViCap.Text = x509Certificate2.Issuer;
                    clcNgayBatDau.Value = Convert.ToDateTime(x509Certificate2.GetEffectiveDateString());
                    clcNgayHetHan.Value = Convert.ToDateTime(x509Certificate2.GetExpirationDateString());     
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
