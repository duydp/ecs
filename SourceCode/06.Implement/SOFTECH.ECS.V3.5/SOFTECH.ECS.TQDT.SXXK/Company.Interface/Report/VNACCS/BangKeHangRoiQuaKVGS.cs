using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using BarcodeLib;
using System.Data;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.VNACCS
{
    public partial class BangKeHangRoiQuaKVGS : DevExpress.XtraReports.UI.XtraReport
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public BangKeHangRoiQuaKVGS()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            try
            {
                BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
                {
                    IncludeLabel = true,
                    Alignment = AlignmentPositions.CENTER,
                    Width = 175,
                    Height = 30,
                    RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                    BackColor = Color.White,
                    ForeColor = Color.Black,
                };

                Image img = barcode.Encode(TYPE.CODE128B, Text);
                return img;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GeneralQrCode(string QrCode)
        {
            try
            {
                Zen.Barcode.CodeQrBarcodeDraw qrCode = Zen.Barcode.BarcodeDrawFactory.CodeQr;
                picQrCode.Image = qrCode.Draw(QrCode, 50);
                picQrCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public void BindReport(KDT_ContainerDangKy cont)
        {
            try
            {

                lblNotes.Text = cont.Note == null ? "(Tờ khai không phải niêm phong)" : "(" + cont.Note.ToString() + ")";
                lblNgay.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Day.ToString() : cont.ThoiGianXuatDL.Day.ToString();
                lblThang.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Month.ToString() : cont.ThoiGianXuatDL.Month.ToString();
                lblNam.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Year.ToString() : cont.ThoiGianXuatDL.Year.ToString();
                string CodeHQV4 = "";
                if (cont.MaHQ != null)
                {
                    CodeHQV4 = "Z" + cont.MaHQ.Substring(0, 2) + "Z";
                }
                else
                {
                    CodeHQV4 = "Z" + TKMD.CoQuanHaiQuan.Substring(0, 2) + "Z";
                }
                IList<Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan> DVHQCollection = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.SelectCollectionDynamic("ID = '" + CodeHQV4 + "'", "");
                if (DVHQCollection.Count >= 1)
                {
                    lblTenCucHQ.Text = DVHQCollection[0].Ten.ToUpper();
                    lblChiCucHQ.Text = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(cont.MaHQ.Trim())).ToUpper().Trim();
                }
                else
                {
                    lblTenCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
                    lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN.ToUpper();
                }
                if (cont.LocationControl != null)
                {
                    List<VNACC_Category_Cargo> CargoCollection = VNACC_Category_Cargo.SelectCollectionDynamic("BondedAreaCode='" + cont.LocationControl.Trim() + "'", "");
                    if (CargoCollection.Count >= 1)
                    {
                        List<VNACC_Category_CustomsOffice> CustomsOfficeCollction = VNACC_Category_CustomsOffice.SelectCollectionDynamic("CustomsCode='" + cont.LocationControl.Trim().Substring(0, 4) + "'", "");
                        lblChiCucHQGiamSat.Text = CustomsOfficeCollction[0].CustomsOfficeNameInVietnamese + " - " + cont.LocationControl.Trim() + ": " + CargoCollection[0].BondedAreaName.ToString().ToUpper().Trim();
                    }
                    else
                    {
                        lblChiCucHQGiamSat.Text = GlobalSettings.TEN_HAI_QUAN + " - " + TKMD.MaDDLuuKho + "";
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(TKMD.DiaDiemDichVC))
                    {
                        List<VNACC_Category_Cargo> CargoCollection = VNACC_Category_Cargo.SelectCollectionDynamic("BondedAreaCode='" + TKMD.DiaDiemDichVC.Trim() + "'", "");
                        if (CargoCollection.Count >= 1)
                        {
                            List<VNACC_Category_CustomsOffice> CustomsOfficeCollction = VNACC_Category_CustomsOffice.SelectCollectionDynamic("CustomsCode='" + TKMD.DiaDiemDichVC.Trim().Substring(0, 4) + "'", "");
                            lblChiCucHQGiamSat.Text = CustomsOfficeCollction[0].CustomsOfficeNameInVietnamese + " - " + TKMD.DiaDiemDichVC.Trim() + ": " + CargoCollection[0].BondedAreaName.ToString().ToUpper().Trim();
                        }
                        else
                        {
                            lblChiCucHQGiamSat.Text = GlobalSettings.TEN_HAI_QUAN + " - " + TKMD.MaDDLuuKho + "";
                        }
                    }
                    else
                    {
                        List<VNACC_Category_Cargo> CargoCollection = VNACC_Category_Cargo.SelectCollectionDynamic("BondedAreaCode='" + TKMD.MaDDLuuKho.Trim() + "'", "");
                        if (CargoCollection.Count >= 1)
                        {
                            List<VNACC_Category_CustomsOffice> CustomsOfficeCollction = VNACC_Category_CustomsOffice.SelectCollectionDynamic("CustomsCode='" + TKMD.MaDDLuuKho.Trim().Substring(0, 4) + "'", "");
                            lblChiCucHQGiamSat.Text = CustomsOfficeCollction[0].CustomsOfficeNameInVietnamese + " - " + TKMD.MaDDLuuKho.Trim() + ": " + CargoCollection[0].BondedAreaName.ToString().ToUpper().Trim();
                        }
                        else
                        {
                            lblChiCucHQGiamSat.Text = GlobalSettings.TEN_HAI_QUAN + " - " + TKMD.MaDDLuuKho + "";
                        }
                    }
                }
                //lblTenCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
                //lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN.ToUpper();
                //List<VNACC_Category_Cargo> CargoCollection = VNACC_Category_Cargo.SelectCollectionDynamic("BondedAreaCode='" + TKMD.MaDDLuuKho + "'", "");
                //if (CargoCollection.Count >= 1)
                //{
                //    lblChiCucHQGiamSat.Text = GlobalSettings.TEN_HAI_QUAN + " - " + TKMD.MaDDLuuKho + ": " + CargoCollection[0].BondedAreaName.ToString();
                //}
                //else
                //{
                //    lblChiCucHQGiamSat.Text = GlobalSettings.TEN_HAI_QUAN + " - " + TKMD.MaDDLuuKho + "";
                //}
                lblTenDonViXNK.Text = GlobalSettings.TEN_DON_VI;
                lblMaDoanhNghiepXNK.Text = GlobalSettings.MA_DON_VI;
                lblSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
                Zen.Barcode.Code128BarcodeDraw barCode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
                picBar.Image = barCode.Draw(TKMD.SoToKhai.ToString(), 50);
                picBar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                GeneralQrCode(TKMD.SoToKhai.ToString() + TKMD.CoQuanHaiQuan);
                string CustomsStatus = cont.CustomsStatus == null || String.IsNullOrEmpty(cont.CustomsStatus) ? "TQ" : cont.CustomsStatus.ToString();
                switch (CustomsStatus)
                {
                    case "TQ":
                        lblTrangThaiTK.Text = "Thông quan";
                        break;
                    case "MHBQ":
                        lblTrangThaiTK.Text = "Mang hàng bảo quan";
                        break;
                    case "GPH":
                        lblTrangThaiTK.Text = "Giải phòng hàng";
                        break;
                    case "CCK":
                        lblTrangThaiTK.Text = "Chuyển địa điểm kiểm tra";
                        break;
                    case "GSTP":
                        lblTrangThaiTK.Text = "Qua KVGS từng phần";
                        break;
                    case "KHH":
                        lblTrangThaiTK.Text = "Chờ thông quan sau khi kiểm hóa hộ";
                        break;
                    default:
                        break;
                }
                lblNgayKhai.Text = TKMD.NgayDangKy.ToString("dd/MM/yyyy") + " - " + (cont.ArrivalDeparture.Year == 1 ? TKMD.NgayCapPhep.ToString("dd/MM/yyyy") : cont.ArrivalDeparture.ToString("dd/MM/yyyy"));
                List<VNACC_Category_Common> CommonCollection = VNACC_Category_Common.SelectCollectionDynamic("Code='" + TKMD.MaLoaiHinh.ToString() + "'", "ID");
                if (CommonCollection.Count >= 1)
                {
                    lblLoaiHinh.Text = CommonCollection[0].Name_VN.ToString();
                }
                else
                {
                    lblLoaiHinh.Text = TKMD.MaLoaiHinh.ToString();
                }
                int PhanLuong = Convert.ToInt32(TKMD.MaPhanLoaiKiemTra);
                switch (PhanLuong)
                {
                    case 1:
                        lblLuong.Text = "Luồng xanh";
                        break;
                    case 2:
                        lblLuong.Text = "Luồng vàng";
                        break;
                    case 3:
                        lblLuong.Text = "Luồng đỏ";
                        break;
                    default:
                        break;
                }
                lblDay.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Day.ToString() : cont.ThoiGianXuatDL.Day.ToString();
                lblMonth.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Month.ToString() : cont.ThoiGianXuatDL.Month.ToString();
                lblYear.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Year.ToString() : cont.ThoiGianXuatDL.Year.ToString();
                lblHour.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.TimeOfDay.ToString() : cont.ThoiGianXuatDL.TimeOfDay.ToString();


                VNACC_Category_PackagesUnit packagesUnit = VNACC_Category_PackagesUnit.SelectCollectionAllMinimize().Find(delegate(VNACC_Category_PackagesUnit c)
                {
                    return c.NumberOfPackagesUnitCode == TKMD.MaDVTSoLuong;
                });

                VNACC_Category_QuantityUnit quantityUnit = VNACC_Category_QuantityUnit.SelectCollectionAllMinimize().Find(delegate(VNACC_Category_QuantityUnit c)
                {
                    return c.Code == TKMD.MaDVTTrongLuong;
                });
                ;
                lblSoQuanLyHH.Text = cont.CargoCtrlNo == null || String.IsNullOrEmpty(cont.CargoCtrlNo) ? TKMD.VanDonCollection.Count == 0 ? "" : TKMD.VanDonCollection[0].SoDinhDanh : cont.CargoCtrlNo.ToString();
                lblSoLuong.Text = (cont.CargoPiece == null || String.IsNullOrEmpty(cont.CargoPiece) ? TKMD.SoLuong.ToString() : cont.CargoPiece.ToString()) + " " + (cont.PieceUnitCode == null || String.IsNullOrEmpty(cont.PieceUnitCode) ? packagesUnit.NumberOfPackagesUnitName : cont.PieceUnitCode.ToString());
                lblTongTrongLuong.Text = (cont.CargoWeight == null || String.IsNullOrEmpty(cont.CargoWeight) ? String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), TKMD.TrongLuong) : cont.CargoWeight.ToString()) + " " + (cont.WeightUnitCode == null || String.IsNullOrEmpty(cont.WeightUnitCode) ? quantityUnit.Notes : cont.WeightUnitCode);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
