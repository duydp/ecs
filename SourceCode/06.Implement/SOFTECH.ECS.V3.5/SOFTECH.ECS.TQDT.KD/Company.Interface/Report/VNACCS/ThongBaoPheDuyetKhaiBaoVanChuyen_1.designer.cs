namespace Company.Interface.Report.VNACCS
{
    partial class ThongBaoPheDuyetKhaiBaoVanChuyen_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongBaoPheDuyetKhaiBaoVanChuyen_1));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaCoBaoYeuCauXN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCoBaoYeuCauXN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoBaoXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayLapTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHopDongVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHopDongVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHetHanHD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhuongTienVC1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenPhuongTienVC1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaMucDichVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenMucDichVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiHinhVanTai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenLoaiHinhVanTai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaViTriXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaCangXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaXacDinhCangXHKoCo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDemXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDiDDXH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemTrungChuyen1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDiaDiemTrungChuyen1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDenDiaDiemTrungChuyen1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDiDiaDiemTrungChuyen1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemTrungChuyen2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDiaDiemTrungChuyen2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDenDiaDiemTrungChuyen2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDiDiaDiemTrungChuyen2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemTrungChuyen3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDiaDiemTrungChuyen3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDenDiaDiemTrungChuyen3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDiDiaDiemTrungChuyen3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemDoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblViTriDoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCangDoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaXacDinhCangDHKoCo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemDoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDenDDDH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTuyenDuongVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiBaoLanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienBaoLanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongCotTrongTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongContainer = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.TongSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 9F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 110F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TongSoTrang,
            this.lblSoTrang,
            this.lblTenThongTinXuat});
            this.ReportHeader.HeightF = 31.25001F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(205.1877F, 5.000003F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(420F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Thông báo phê duyệt khai báo vận chuyển";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.HeightF = 626.0417F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 10.00001F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow3,
            this.xrTableRow2,
            this.xrTableRow4,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow5,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26});
            this.xrTable1.SizeF = new System.Drawing.SizeF(815F, 598F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell13,
            this.lblMaCoBaoYeuCauXN,
            this.xrTableCell11,
            this.lblTenCoBaoYeuCauXN,
            this.xrTableCell14,
            this.xrTableCell12,
            this.xrTableCell23,
            this.lblCoQuanHQ});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.8800001727532315;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "Cờ báo yêu cầu xác nhận/ niêm phong";
            this.xrTableCell10.Weight = 0.747358117851237;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.019462449449940092;
            // 
            // lblMaCoBaoYeuCauXN
            // 
            this.lblMaCoBaoYeuCauXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaCoBaoYeuCauXN.Name = "lblMaCoBaoYeuCauXN";
            this.lblMaCoBaoYeuCauXN.StylePriority.UseFont = false;
            this.lblMaCoBaoYeuCauXN.Text = "X";
            this.lblMaCoBaoYeuCauXN.Weight = 0.0389249059514799;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "-";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell11.Weight = 0.031139920527839073;
            // 
            // lblTenCoBaoYeuCauXN
            // 
            this.lblTenCoBaoYeuCauXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCoBaoYeuCauXN.Name = "lblTenCoBaoYeuCauXN";
            this.lblTenCoBaoYeuCauXN.StylePriority.UseFont = false;
            this.lblTenCoBaoYeuCauXN.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWE";
            this.lblTenCoBaoYeuCauXN.Weight = 1.3353387680599869;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Weight = 0.018292309039450907;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "Cơ quan Hải quan";
            this.xrTableCell12.Weight = 0.35061975540357093;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.018292308903027535;
            // 
            // lblCoQuanHQ
            // 
            this.lblCoQuanHQ.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoQuanHQ.Name = "lblCoQuanHQ";
            this.lblCoQuanHQ.StylePriority.UseFont = false;
            this.lblCoQuanHQ.Text = "XXXXXXXXXE";
            this.lblCoQuanHQ.Weight = 0.4222182902102935;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell19,
            this.lblSoToKhaiVC,
            this.xrTableCell20,
            this.xrTableCell16,
            this.xrTableCell22,
            this.lblCoBaoXNK,
            this.xrTableCell21,
            this.xrTableCell17,
            this.xrTableCell24,
            this.lblNgayLapTK});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.80000018065225509;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Text = "Số tờ khai vận chuyển";
            this.xrTableCell15.Weight = 0.4610750083213746;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.019462748266735781;
            // 
            // lblSoToKhaiVC
            // 
            this.lblSoToKhaiVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiVC.Name = "lblSoToKhaiVC";
            this.lblSoToKhaiVC.StylePriority.UseFont = false;
            this.lblSoToKhaiVC.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiVC.Weight = 0.51946224341188962;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Weight = 0.01946245940065694;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "Cờ báo nhập khẩu/xuất khẩu";
            this.xrTableCell16.Weight = 0.67921675068331111;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Weight = 0.019462459400656884;
            // 
            // lblCoBaoXNK
            // 
            this.lblCoBaoXNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoBaoXNK.Name = "lblCoBaoXNK";
            this.lblCoBaoXNK.StylePriority.UseFont = false;
            this.lblCoBaoXNK.Text = "X";
            this.lblCoBaoXNK.Weight = 0.4540823875263329;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 0.01829231181225046;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Text = "Ngày lập tờ khai";
            this.xrTableCell17.Weight = 0.35061976599782346;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.018292315280719951;
            // 
            // lblNgayLapTK
            // 
            this.lblNgayLapTK.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayLapTK.Name = "lblNgayLapTK";
            this.lblNgayLapTK.StylePriority.UseFont = false;
            this.lblNgayLapTK.Text = "dd/MM/yyyy";
            this.lblNgayLapTK.Weight = 0.42221837529507356;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell18,
            this.lblMaNguoiKhai,
            this.xrTableCell25,
            this.lblTenNguoiKhai});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.79999964659451306;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "Người khai";
            this.xrTableCell4.Weight = 0.46107490574671911;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 0.019462809027471717;
            // 
            // lblMaNguoiKhai
            // 
            this.lblMaNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiKhai.Name = "lblMaNguoiKhai";
            this.lblMaNguoiKhai.StylePriority.UseFont = false;
            this.lblMaNguoiKhai.Text = "XXXXE";
            this.lblMaNguoiKhai.Weight = 0.23354910506349949;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Text = "-";
            this.xrTableCell25.Weight = 0.0527335559688948;
            // 
            // lblTenNguoiKhai
            // 
            this.lblTenNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiKhai.Name = "lblTenNguoiKhai";
            this.lblTenNguoiKhai.StylePriority.UseFont = false;
            this.lblTenNguoiKhai.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblTenNguoiKhai.Weight = 2.21482644959024;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell27,
            this.lblDiaChiNguoiKhai});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1.2800001607259282;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Text = "Địa chỉ";
            this.xrTableCell26.Weight = 0.46107478695734355;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.019462690238096152;
            // 
            // lblDiaChiNguoiKhai
            // 
            this.lblDiaChiNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiKhai.Name = "lblDiaChiNguoiKhai";
            this.lblDiaChiNguoiKhai.StylePriority.UseFont = false;
            this.lblDiaChiNguoiKhai.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiKhai.Weight = 2.501109348201386;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.lblMaNguoiVC,
            this.xrTableCell40,
            this.lblTenNguoiVC});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1.2800001607259279;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "Nhà vận chuyển";
            this.xrTableCell37.Weight = 0.46107466816796794;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Weight = 0.019462452659345014;
            // 
            // lblMaNguoiVC
            // 
            this.lblMaNguoiVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiVC.Name = "lblMaNguoiVC";
            this.lblMaNguoiVC.StylePriority.UseFont = false;
            this.lblMaNguoiVC.Text = "XXXXXXXXX1 - XXE";
            this.lblMaNguoiVC.Weight = 0.47877637063965939;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "-";
            this.xrTableCell40.Weight = 0.060149038112606731;
            // 
            // lblTenNguoiVC
            // 
            this.lblTenNguoiVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiVC.Name = "lblTenNguoiVC";
            this.lblTenNguoiVC.StylePriority.UseFont = false;
            this.lblTenNguoiVC.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiVC.Weight = 1.962184295817246;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29,
            this.lblDiaChiNguoiVC});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1.2800001607259282;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Text = "Địa chỉ";
            this.xrTableCell28.Weight = 0.46152706520438291;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.019462385409222009;
            // 
            // lblDiaChiNguoiVC
            // 
            this.lblDiaChiNguoiVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiVC.Name = "lblDiaChiNguoiVC";
            this.lblDiaChiNguoiVC.StylePriority.UseFont = false;
            this.lblDiaChiNguoiVC.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiVC.Weight = 2.5006573747832204;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.xrTableCell32,
            this.lblSoHopDongVC});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.680000071207691;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "Số hợp đồng vận chuyển/ giấy tờ tương đương";
            this.xrTableCell31.Weight = 0.90782541002330486;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 0.019236430137537841;
            // 
            // lblSoHopDongVC
            // 
            this.lblSoHopDongVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHopDongVC.Name = "lblSoHopDongVC";
            this.lblSoHopDongVC.StylePriority.UseFont = false;
            this.lblSoHopDongVC.Text = "XXXXXXXXX1E";
            this.lblSoHopDongVC.Weight = 2.0545849852359828;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell34,
            this.lblNgayHopDongVC,
            this.xrTableCell51,
            this.xrTableCell54,
            this.xrTableCell53,
            this.lblNgayHetHanHD});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.680000071207691;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Text = "Ngày hợp đồng vận chuyển/ giấy tờ tương đương";
            this.xrTableCell33.Weight = 0.9593135072732476;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Weight = 0.019236430137537841;
            // 
            // lblNgayHopDongVC
            // 
            this.lblNgayHopDongVC.Name = "lblNgayHopDongVC";
            this.lblNgayHopDongVC.Text = "dd/MM/yyyy";
            this.lblNgayHopDongVC.Weight = 0.46871362574579134;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.043282204770591109;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Ngày hết hạn hợp đồng vận chuyển/ giấy tờ tương đương";
            this.xrTableCell54.Weight = 1.0505904195875544;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Weight = 0.018292308838403909;
            // 
            // lblNgayHetHanHD
            // 
            this.lblNgayHetHanHD.Name = "lblNgayHetHanHD";
            this.lblNgayHetHanHD.Text = "dd/MM/yyyy";
            this.lblNgayHetHanHD.Weight = 0.4222183290436991;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.lblMaPhuongTienVC1,
            this.xrTableCell55,
            this.lblTenPhuongTienVC1});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.680000071207691;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "Phương tiện vận chuyển";
            this.xrTableCell45.Weight = 0.46936892098574079;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 0.019236430137537786;
            // 
            // lblMaPhuongTienVC1
            // 
            this.lblMaPhuongTienVC1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhuongTienVC1.Name = "lblMaPhuongTienVC1";
            this.lblMaPhuongTienVC1.StylePriority.UseFont = false;
            this.lblMaPhuongTienVC1.Text = "XE";
            this.lblMaPhuongTienVC1.Weight = 0.12231166075147715;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Text = "-";
            this.xrTableCell55.Weight = 0.038472920872905769;
            // 
            // lblTenPhuongTienVC1
            // 
            this.lblTenPhuongTienVC1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenPhuongTienVC1.Name = "lblTenPhuongTienVC1";
            this.lblTenPhuongTienVC1.StylePriority.UseFont = false;
            this.lblTenPhuongTienVC1.Text = "XXXXXXXXX1E";
            this.lblTenPhuongTienVC1.Weight = 2.332256892649164;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49,
            this.lblMaMucDichVC,
            this.xrTableCell57,
            this.lblTenMucDichVC});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.80000017556596292;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Text = "Mục đích vận chuyển";
            this.xrTableCell48.Weight = 0.42320148626991894;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.019236430137537827;
            // 
            // lblMaMucDichVC
            // 
            this.lblMaMucDichVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaMucDichVC.Name = "lblMaMucDichVC";
            this.lblMaMucDichVC.StylePriority.UseFont = false;
            this.lblMaMucDichVC.Text = "XXE";
            this.lblMaMucDichVC.Weight = 0.11541859403246296;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Text = "-";
            this.xrTableCell57.Weight = 0.01923643103638463;
            // 
            // lblTenMucDichVC
            // 
            this.lblTenMucDichVC.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblTenMucDichVC.Name = "lblTenMucDichVC";
            this.lblTenMucDichVC.StylePriority.UseFont = false;
            this.lblTenMucDichVC.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblTenMucDichVC.Weight = 2.4045538839205212;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59,
            this.xrTableCell60,
            this.lblLoaiHinhVanTai,
            this.xrTableCell62,
            this.lblTenLoaiHinhVanTai});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.800000175565963;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Text = "Loại hình vận tải";
            this.xrTableCell59.Weight = 0.46936894673667584;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 0.019236402225685078;
            // 
            // lblLoaiHinhVanTai
            // 
            this.lblLoaiHinhVanTai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiHinhVanTai.Name = "lblLoaiHinhVanTai";
            this.lblLoaiHinhVanTai.StylePriority.UseFont = false;
            this.lblLoaiHinhVanTai.Text = "XE";
            this.lblLoaiHinhVanTai.Weight = 0.069251133565706044;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "-";
            this.xrTableCell62.Weight = 0.019236458948237365;
            // 
            // lblTenLoaiHinhVanTai
            // 
            this.lblTenLoaiHinhVanTai.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblTenLoaiHinhVanTai.Name = "lblTenLoaiHinhVanTai";
            this.lblTenLoaiHinhVanTai.StylePriority.UseFont = false;
            this.lblTenLoaiHinhVanTai.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblTenLoaiHinhVanTai.Weight = 2.4045538839205212;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell64});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.68000014750163407;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Text = "Địa điểm xếp hàng";
            this.xrTableCell64.Weight = 2.9816468253968256;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell69,
            this.xrTableCell68,
            this.xrTableCell67,
            this.xrTableCell75,
            this.lblMaDiaDiemXepHang,
            this.xrTableCell74,
            this.xrTableCell71,
            this.xrTableCell66,
            this.lblMaViTriXepHang,
            this.xrTableCell77,
            this.xrTableCell72,
            this.xrTableCell78,
            this.lblMaCangXepHang,
            this.xrTableCell79,
            this.lblMaXacDinhCangXHKoCo});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1.2800003099499246;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 0.0931764632936508;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Text = "Mã";
            this.xrTableCell69.Weight = 0.0931764632936508;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.018292311812250477;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Text = "(Khu vực chịu sự giám sát Hải quan)";
            this.xrTableCell67.Weight = 0.75466816989736352;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 0.040686549503297836;
            // 
            // lblMaDiaDiemXepHang
            // 
            this.lblMaDiaDiemXepHang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemXepHang.Name = "lblMaDiaDiemXepHang";
            this.lblMaDiaDiemXepHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemXepHang.Text = "XXXXXXE";
            this.lblMaDiaDiemXepHang.Weight = 0.52885614544289616;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Weight = 0.018292311812250422;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Text = "(Vị trí xếp hàng)";
            this.xrTableCell71.Weight = 0.33219615317027146;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Weight = 0.018292311812250439;
            // 
            // lblMaViTriXepHang
            // 
            this.lblMaViTriXepHang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaViTriXepHang.Name = "lblMaViTriXepHang";
            this.lblMaViTriXepHang.StylePriority.UseFont = false;
            this.lblMaViTriXepHang.Text = "XXXXXE";
            this.lblMaViTriXepHang.Weight = 0.27458698229242279;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 0.018292311812250474;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Text = "(Cảng/ cửa khẩu/ ga xếp hàng)";
            this.xrTableCell72.Weight = 0.35062020892745871;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.018292088517428556;
            // 
            // lblMaCangXepHang
            // 
            this.lblMaCangXepHang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaCangXepHang.Name = "lblMaCangXepHang";
            this.lblMaCangXepHang.StylePriority.UseFont = false;
            this.lblMaCangXepHang.Text = "XXXXXE";
            this.lblMaCangXepHang.Weight = 0.20937975850886784;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 0.045950278340580311;
            // 
            // lblMaXacDinhCangXHKoCo
            // 
            this.lblMaXacDinhCangXHKoCo.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaXacDinhCangXHKoCo.Name = "lblMaXacDinhCangXHKoCo";
            this.lblMaXacDinhCangXHKoCo.StylePriority.UseFont = false;
            this.lblMaXacDinhCangXHKoCo.Text = "[X]";
            this.lblMaXacDinhCangXHKoCo.Weight = 0.166888316959935;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.lblDiaDemXepHang,
            this.xrTableCell85,
            this.xrTableCell94,
            this.xrTableCell95,
            this.lblNgayDiDDXH});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1.0400004827031562;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 0.0931764632936508;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Text = "Tên";
            this.xrTableCell82.Weight = 0.0931764632936508;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.018292311812250477;
            // 
            // lblDiaDemXepHang
            // 
            this.lblDiaDemXepHang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDemXepHang.Name = "lblDiaDemXepHang";
            this.lblDiaDemXepHang.StylePriority.UseFont = false;
            this.lblDiaDemXepHang.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWE";
            this.lblDiaDemXepHang.Weight = 1.3242108648435573;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 0.018292311812250456;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Text = "Đi (dự kiến)";
            this.xrTableCell94.Weight = 0.33219637646509359;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.016987376872909625;
            // 
            // lblNgayDiDDXH
            // 
            this.lblNgayDiDDXH.Name = "lblNgayDiDDXH";
            this.lblNgayDiDDXH.Text = "dd/MM/yyyy";
            this.lblNgayDiDDXH.Weight = 1.0853146570034624;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell93});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.68000021316464;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Text = "Địa điểm trung chuyển cho vận chuyển (Khai báo nộp)";
            this.xrTableCell86.Weight = 1.4051885087205205;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 0.042074773411217181;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Text = "Đến (dự kiến)";
            this.xrTableCell91.Weight = 0.43208150938871576;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.016987376872909625;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Text = "Đi (dự kiến)";
            this.xrTableCell93.Weight = 1.0853146570034624;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell100,
            this.xrTableCell103,
            this.lblMaDiaDiemTrungChuyen1,
            this.xrTableCell99,
            this.lblTenDiaDiemTrungChuyen1,
            this.xrTableCell88,
            this.lblNgayDenDiaDiemTrungChuyen1,
            this.xrTableCell97,
            this.lblNgayDiDiaDiemTrungChuyen1});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.68000021316464;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 0.093176407469945308;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Text = "1";
            this.xrTableCell100.Weight = 0.093176533073282608;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Weight = 0.018292297856324108;
            // 
            // lblMaDiaDiemTrungChuyen1
            // 
            this.lblMaDiaDiemTrungChuyen1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemTrungChuyen1.Name = "lblMaDiaDiemTrungChuyen1";
            this.lblMaDiaDiemTrungChuyen1.StylePriority.UseFont = false;
            this.lblMaDiaDiemTrungChuyen1.Text = "XXXXXXE";
            this.lblMaDiaDiemTrungChuyen1.Weight = 0.28396011738503657;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Text = "-";
            this.xrTableCell99.Weight = 0.069251204656061743;
            // 
            // lblTenDiaDiemTrungChuyen1
            // 
            this.lblTenDiaDiemTrungChuyen1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDiaDiemTrungChuyen1.Name = "lblTenDiaDiemTrungChuyen1";
            this.lblTenDiaDiemTrungChuyen1.StylePriority.UseFont = false;
            this.lblTenDiaDiemTrungChuyen1.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTenDiaDiemTrungChuyen1.Weight = 0.84733194827987013;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 0.042074773411217181;
            // 
            // lblNgayDenDiaDiemTrungChuyen1
            // 
            this.lblNgayDenDiaDiemTrungChuyen1.Name = "lblNgayDenDiaDiemTrungChuyen1";
            this.lblNgayDenDiaDiemTrungChuyen1.Text = "dd/MM/yyyy";
            this.lblNgayDenDiaDiemTrungChuyen1.Weight = 0.43208150938871576;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 0.016987376872909625;
            // 
            // lblNgayDiDiaDiemTrungChuyen1
            // 
            this.lblNgayDiDiaDiemTrungChuyen1.Name = "lblNgayDiDiaDiemTrungChuyen1";
            this.lblNgayDiDiaDiemTrungChuyen1.Text = "dd/MM/yyyy";
            this.lblNgayDiDiaDiemTrungChuyen1.Weight = 1.0853146570034624;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.xrTableCell105,
            this.xrTableCell106,
            this.lblMaDiaDiemTrungChuyen2,
            this.xrTableCell108,
            this.lblTenDiaDiemTrungChuyen2,
            this.xrTableCell110,
            this.lblNgayDenDiaDiemTrungChuyen2,
            this.xrTableCell112,
            this.lblNgayDiDiaDiemTrungChuyen2});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.68000021316464;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Weight = 0.093176407469945308;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Text = "2";
            this.xrTableCell105.Weight = 0.093176533073282608;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 0.018292297856324108;
            // 
            // lblMaDiaDiemTrungChuyen2
            // 
            this.lblMaDiaDiemTrungChuyen2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemTrungChuyen2.Name = "lblMaDiaDiemTrungChuyen2";
            this.lblMaDiaDiemTrungChuyen2.StylePriority.UseFont = false;
            this.lblMaDiaDiemTrungChuyen2.Text = "XXXXXXE";
            this.lblMaDiaDiemTrungChuyen2.Weight = 0.28396011738503657;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Text = "-";
            this.xrTableCell108.Weight = 0.069251204656061743;
            // 
            // lblTenDiaDiemTrungChuyen2
            // 
            this.lblTenDiaDiemTrungChuyen2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDiaDiemTrungChuyen2.Name = "lblTenDiaDiemTrungChuyen2";
            this.lblTenDiaDiemTrungChuyen2.StylePriority.UseFont = false;
            this.lblTenDiaDiemTrungChuyen2.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTenDiaDiemTrungChuyen2.Weight = 0.84733194827987013;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 0.042074773411217181;
            // 
            // lblNgayDenDiaDiemTrungChuyen2
            // 
            this.lblNgayDenDiaDiemTrungChuyen2.Name = "lblNgayDenDiaDiemTrungChuyen2";
            this.lblNgayDenDiaDiemTrungChuyen2.Text = "dd/MM/yyyy";
            this.lblNgayDenDiaDiemTrungChuyen2.Weight = 0.43208150938871576;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Weight = 0.016987376872909625;
            // 
            // lblNgayDiDiaDiemTrungChuyen2
            // 
            this.lblNgayDiDiaDiemTrungChuyen2.Name = "lblNgayDiDiaDiemTrungChuyen2";
            this.lblNgayDiDiaDiemTrungChuyen2.Text = "dd/MM/yyyy";
            this.lblNgayDiDiaDiemTrungChuyen2.Weight = 1.0853146570034624;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell114,
            this.xrTableCell115,
            this.xrTableCell116,
            this.lblMaDiaDiemTrungChuyen3,
            this.xrTableCell118,
            this.lblTenDiaDiemTrungChuyen3,
            this.xrTableCell120,
            this.lblNgayDenDiaDiemTrungChuyen3,
            this.xrTableCell122,
            this.lblNgayDiDiaDiemTrungChuyen3});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.68000021316464;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Weight = 0.093176407469945308;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Text = "3";
            this.xrTableCell115.Weight = 0.093176533073282608;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Weight = 0.018292297856324108;
            // 
            // lblMaDiaDiemTrungChuyen3
            // 
            this.lblMaDiaDiemTrungChuyen3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemTrungChuyen3.Name = "lblMaDiaDiemTrungChuyen3";
            this.lblMaDiaDiemTrungChuyen3.StylePriority.UseFont = false;
            this.lblMaDiaDiemTrungChuyen3.Text = "XXXXXXE";
            this.lblMaDiaDiemTrungChuyen3.Weight = 0.28396011738503657;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Text = "-";
            this.xrTableCell118.Weight = 0.069251204656061743;
            // 
            // lblTenDiaDiemTrungChuyen3
            // 
            this.lblTenDiaDiemTrungChuyen3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDiaDiemTrungChuyen3.Name = "lblTenDiaDiemTrungChuyen3";
            this.lblTenDiaDiemTrungChuyen3.StylePriority.UseFont = false;
            this.lblTenDiaDiemTrungChuyen3.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTenDiaDiemTrungChuyen3.Weight = 0.84733194827987013;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Weight = 0.042074773411217181;
            // 
            // lblNgayDenDiaDiemTrungChuyen3
            // 
            this.lblNgayDenDiaDiemTrungChuyen3.Name = "lblNgayDenDiaDiemTrungChuyen3";
            this.lblNgayDenDiaDiemTrungChuyen3.Text = "dd/MM/yyyy";
            this.lblNgayDenDiaDiemTrungChuyen3.Weight = 0.43208150938871576;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Weight = 0.016987376872909625;
            // 
            // lblNgayDiDiaDiemTrungChuyen3
            // 
            this.lblNgayDiDiaDiemTrungChuyen3.Name = "lblNgayDiDiaDiemTrungChuyen3";
            this.lblNgayDiDiaDiemTrungChuyen3.Text = "dd/MM/yyyy";
            this.lblNgayDiDiaDiemTrungChuyen3.Weight = 1.0853146570034624;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.40000008320277552;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Weight = 2.9816468253968256;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell124});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.6800002894586;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Text = "Địa điểm dỡ hàng";
            this.xrTableCell124.Weight = 2.9816468253968256;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell131,
            this.xrTableCell130,
            this.xrTableCell129,
            this.xrTableCell128,
            this.xrTableCell127,
            this.lblMaDiaDiemDoHang,
            this.xrTableCell134,
            this.xrTableCell132,
            this.xrTableCell135,
            this.lblViTriDoHang,
            this.xrTableCell136,
            this.xrTableCell140,
            this.xrTableCell138,
            this.lblCangDoHang,
            this.xrTableCell141,
            this.lblMaXacDinhCangDHKoCo});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1.2800004099356424;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Weight = 0.0931764632936508;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "Mã";
            this.xrTableCell130.Weight = 0.0931764632936508;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Weight = 0.018292311812250467;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Text = "(Khu vực chịu sự giám sát Hải quan)";
            this.xrTableCell128.Weight = 0.75466816989736352;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Weight = 0.040686549503297809;
            // 
            // lblMaDiaDiemDoHang
            // 
            this.lblMaDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemDoHang.Name = "lblMaDiaDiemDoHang";
            this.lblMaDiaDiemDoHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemDoHang.Text = "XXXXXXE";
            this.lblMaDiaDiemDoHang.Weight = 0.52885625709030715;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Weight = 0.018292200164839512;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Text = "(Vị trí dỡ hàng)";
            this.xrTableCell132.Weight = 0.33219637646509342;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Weight = 0.018292088517428584;
            // 
            // lblViTriDoHang
            // 
            this.lblViTriDoHang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViTriDoHang.Name = "lblViTriDoHang";
            this.lblViTriDoHang.StylePriority.UseFont = false;
            this.lblViTriDoHang.Text = "XXXXXE";
            this.lblViTriDoHang.Weight = 0.27458675899760071;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Weight = 0.018292981696716226;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Text = "(Cảng/ cửa khẩu/ ga dỡ hàng)";
            this.xrTableCell140.Weight = 0.35061953904299303;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Weight = 0.018292535107072381;
            // 
            // lblCangDoHang
            // 
            this.lblCangDoHang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCangDoHang.Name = "lblCangDoHang";
            this.lblCangDoHang.StylePriority.UseFont = false;
            this.lblCangDoHang.Text = "XXXXXE";
            this.lblCangDoHang.Weight = 0.20937379932830766;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Weight = 0.045955735107791135;
            // 
            // lblMaXacDinhCangDHKoCo
            // 
            this.lblMaXacDinhCangDHKoCo.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaXacDinhCangDHKoCo.Name = "lblMaXacDinhCangDHKoCo";
            this.lblMaXacDinhCangDHKoCo.StylePriority.UseFont = false;
            this.lblMaXacDinhCangDHKoCo.Text = "[X]";
            this.lblMaXacDinhCangDHKoCo.Weight = 0.1668885960784624;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144,
            this.lblDiaDiemDoHang,
            this.xrTableCell145,
            this.xrTableCell149,
            this.xrTableCell150,
            this.lblNgayDenDDDH});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1.2800004099356424;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 0.0931764632936508;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Text = "Tên";
            this.xrTableCell143.Weight = 0.0931764632936508;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Weight = 0.018292311812250467;
            // 
            // lblDiaDiemDoHang
            // 
            this.lblDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDiemDoHang.Name = "lblDiaDiemDoHang";
            this.lblDiaDiemDoHang.StylePriority.UseFont = false;
            this.lblDiaDiemDoHang.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWE";
            this.lblDiaDiemDoHang.Weight = 1.3242108648435575;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Weight = 0.018292200164839456;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Text = "Đến (dự kiến)";
            this.xrTableCell149.Weight = 0.33219648811250446;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 0.018292088517428584;
            // 
            // lblNgayDenDDDH
            // 
            this.lblNgayDenDDDH.Name = "lblNgayDenDDDH";
            this.lblNgayDenDDDH.Text = "dd/MM/yyyy";
            this.lblNgayDenDDDH.Weight = 1.0840099453589436;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell147,
            this.xrTableCell152,
            this.lblTuyenDuongVC});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 0.68000028055075812;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Text = "Tuyến đường";
            this.xrTableCell147.Weight = 0.29260078963802483;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Weight = 0.018292311812250515;
            // 
            // lblTuyenDuongVC
            // 
            this.lblTuyenDuongVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTuyenDuongVC.Name = "lblTuyenDuongVC";
            this.lblTuyenDuongVC.StylePriority.UseFont = false;
            this.lblTuyenDuongVC.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTuyenDuongVC.Weight = 2.67075372394655;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148,
            this.xrTableCell153,
            this.lblLoaiBaoLanh,
            this.xrTableCell165,
            this.xrTableCell163,
            this.lblSoTienBaoLanh,
            this.xrTableCell162,
            this.xrTableCell154});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.68000028055075812;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Text = "Loại bảo lãnh";
            this.xrTableCell148.Weight = 0.29260078963802483;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Weight = 0.018292311812250515;
            // 
            // lblLoaiBaoLanh
            // 
            this.lblLoaiBaoLanh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiBaoLanh.Name = "lblLoaiBaoLanh";
            this.lblLoaiBaoLanh.StylePriority.UseFont = false;
            this.lblLoaiBaoLanh.Text = "X";
            this.lblLoaiBaoLanh.Weight = 0.11230835665838318;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Weight = 0.018292311812250456;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Text = "Số tiền bảo lãnh";
            this.xrTableCell163.Weight = 0.36425162511996073;
            // 
            // lblSoTienBaoLanh
            // 
            this.lblSoTienBaoLanh.Name = "lblSoTienBaoLanh";
            this.lblSoTienBaoLanh.Text = "12.345.678.901";
            this.lblSoTienBaoLanh.Weight = 0.33011738304757987;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.018292304834287343;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Text = "VND";
            this.xrTableCell154.Weight = 1.8274917424740886;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.xrTableCell157,
            this.lblSoLuongCotTrongTK,
            this.xrTableCell169,
            this.xrTableCell167,
            this.xrTableCell170,
            this.lblSoLuongContainer});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.960000193446266;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Text = "Số lượng cột trong tờ khai";
            this.xrTableCell155.Weight = 0.55785650461694491;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Weight = 0.018292311812250495;
            // 
            // lblSoLuongCotTrongTK
            // 
            this.lblSoLuongCotTrongTK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuongCotTrongTK.Name = "lblSoLuongCotTrongTK";
            this.lblSoLuongCotTrongTK.StylePriority.UseFont = false;
            this.lblSoLuongCotTrongTK.Text = "N";
            this.lblSoLuongCotTrongTK.Weight = 0.073241022578324988;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Weight = 0.018292304834287343;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Text = "Số lượng container";
            this.xrTableCell167.Weight = 0.39370013700161671;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 0.018292311812250731;
            // 
            // lblSoLuongContainer
            // 
            this.lblSoLuongContainer.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuongContainer.Name = "lblSoLuongContainer";
            this.lblSoLuongContainer.StylePriority.UseFont = false;
            this.lblSoLuongContainer.Text = "NNE";
            this.lblSoLuongContainer.Weight = 1.9019722327411506;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.xrTableCell160,
            this.lblGhiChu1});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 2.2800008073297366;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Text = "Ghi chú 1";
            this.xrTableCell159.Weight = 0.29260078963802483;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Weight = 0.018292311812250515;
            // 
            // lblGhiChu1
            // 
            this.lblGhiChu1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChu1.Name = "lblGhiChu1";
            this.lblGhiChu1.StylePriority.UseFont = false;
            this.lblGhiChu1.Text = resources.GetString("lblGhiChu1.Text");
            this.lblGhiChu1.Weight = 2.67075372394655;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(785.0767F, 5.000003F);
            this.lblSoTrang.Multiline = true;
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTrang.SizeF = new System.Drawing.SizeF(19.92328F, 21.25F);
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "1 /";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // TongSoTrang
            // 
            this.TongSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(805F, 5.000003F);
            this.TongSoTrang.Multiline = true;
            this.TongSoTrang.Name = "TongSoTrang";
            this.TongSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TongSoTrang.SizeF = new System.Drawing.SizeF(20F, 21.25F);
            this.TongSoTrang.StylePriority.UseTextAlignment = false;
            this.TongSoTrang.Text = "5";
            this.TongSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ThongBaoPheDuyetKhaiBaoVanChuyen_1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(14, 11, 9, 110);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaCoBaoYeuCauXN;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCoBaoYeuCauXN;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanHQ;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiVC;
        private DevExpress.XtraReports.UI.XRTableCell lblCoBaoXNK;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayLapTK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiKhai;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiVC;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiVC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiVC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHopDongVC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHopDongVC;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHetHanHD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell lblTenPhuongTienVC1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell lblTenMucDichVC;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhuongTienVC1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblMaMucDichVC;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiHinhVanTai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell lblTenLoaiHinhVanTai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell lblMaXacDinhCangXHKoCo;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemXepHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell lblMaViTriXepHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell lblMaCangXepHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDemXepHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDiDDXH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemTrungChuyen1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDiaDiemTrungChuyen1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDenDiaDiemTrungChuyen1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDiDiaDiemTrungChuyen1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemTrungChuyen2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDiaDiemTrungChuyen2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDenDiaDiemTrungChuyen2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDiDiaDiemTrungChuyen2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemTrungChuyen3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDiaDiemTrungChuyen3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDenDiaDiemTrungChuyen3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDiDiaDiemTrungChuyen3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell lblMaXacDinhCangDHKoCo;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell lblViTriDoHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell lblCangDoHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDenDDDH;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell lblTuyenDuongVC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiBaoLanh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienBaoLanh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongContainer;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChu1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongCotTrongTK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRLabel TongSoTrang;
        private DevExpress.XtraReports.UI.XRLabel lblSoTrang;
    }
}
