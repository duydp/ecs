namespace Company.Interface.Report.VNACCS
{
    partial class QuyetDinhChapThuanKhaiSuaDoiBoSungTrongTruongHopTangThue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblManuocxuatxusau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMotahanghoasau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblManuocxuatxutruoc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMotahanghoatruoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSTTdonghang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.lblMahetthoihan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDauhieu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiodangky = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaydangky = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiohoanthanhktra = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoihantainhap = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaytokhaiXN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNhomxulyhoso = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayhoanthanhktra = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaloaihinh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhaiXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaycapphep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaiXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCoquannhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhaibosung = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSothongbao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHienthimienthue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaapdungthuesuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThuesuattruockhaibosung = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotienthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenkhoanmuctiepnhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadvtinhsoluongtruoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrigiathuetruockhaibosung = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSLtinhthuetruockhikhaibosung = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMakhoanmuctiepnhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSlthuesau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrigiathuesau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadvsoluong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaapdungthuesau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThuesau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotienthesau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHienthimienthuesau2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotientanggiamthue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuthisotien2 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHienthimienthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotienthuetruoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThuesuattruoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMasohanghoatruoc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoluongtinhthuetruoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrigiatinhthuetruoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadvtinhSL = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHienthimienthuesau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMasohanghoasau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoluongtinhthuesau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrigiatinhthuesau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadvtinhSLsau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThuesuatsau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotienthuesau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotientanggiamthue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuthisotien = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblSodong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblManuocxuatxusau,
            this.lblMotahanghoasau,
            this.xrLabel38,
            this.lblManuocxuatxutruoc,
            this.lblMotahanghoatruoc,
            this.xrLabel35,
            this.xrLabel34,
            this.xrLabel33,
            this.lblSTTdonghang,
            this.xrLabel31});
            this.Detail.HeightF = 93.75F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblManuocxuatxusau
            // 
            this.lblManuocxuatxusau.LocationFloat = new DevExpress.Utils.PointFloat(662.6758F, 59.30495F);
            this.lblManuocxuatxusau.Name = "lblManuocxuatxusau";
            this.lblManuocxuatxusau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblManuocxuatxusau.SizeF = new System.Drawing.SizeF(37.53766F, 23F);
            this.lblManuocxuatxusau.StylePriority.UseTextAlignment = false;
            this.lblManuocxuatxusau.Text = "XE";
            this.lblManuocxuatxusau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMotahanghoasau
            // 
            this.lblMotahanghoasau.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMotahanghoasau.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 59.30495F);
            this.lblMotahanghoasau.Multiline = true;
            this.lblMotahanghoasau.Name = "lblMotahanghoasau";
            this.lblMotahanghoasau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMotahanghoasau.SizeF = new System.Drawing.SizeF(530.1977F, 34.42417F);
            this.lblMotahanghoasau.StylePriority.UseFont = false;
            this.lblMotahanghoasau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6X" +
                "XXXXXXXX7XXXXXXXXX8XXXXXXXXX9XXXXXXXXXE";
            // 
            // xrLabel38
            // 
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(9.99999F, 59.30493F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(110.1169F, 11.54169F);
            this.xrLabel38.Text = "Sau khi khai báo";
            // 
            // lblManuocxuatxutruoc
            // 
            this.lblManuocxuatxutruoc.LocationFloat = new DevExpress.Utils.PointFloat(662.6758F, 23.08336F);
            this.lblManuocxuatxutruoc.Name = "lblManuocxuatxutruoc";
            this.lblManuocxuatxutruoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblManuocxuatxutruoc.SizeF = new System.Drawing.SizeF(37.53766F, 23F);
            this.lblManuocxuatxutruoc.StylePriority.UseTextAlignment = false;
            this.lblManuocxuatxutruoc.Text = "XE";
            this.lblManuocxuatxutruoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMotahanghoatruoc
            // 
            this.lblMotahanghoatruoc.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMotahanghoatruoc.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 23.08336F);
            this.lblMotahanghoatruoc.Multiline = true;
            this.lblMotahanghoatruoc.Name = "lblMotahanghoatruoc";
            this.lblMotahanghoatruoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMotahanghoatruoc.SizeF = new System.Drawing.SizeF(530.1977F, 34.42417F);
            this.lblMotahanghoatruoc.StylePriority.UseFont = false;
            this.lblMotahanghoatruoc.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6X" +
                "XXXXXXXX7XXXXXXXXX8XXXXXXXXX9XXXXXXXXXE";
            // 
            // xrLabel35
            // 
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 23.08334F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(110.1169F, 11.54169F);
            this.xrLabel35.Text = "Trước khi khai báo";
            // 
            // xrLabel34
            // 
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(616.4066F, 11.54169F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(105.5934F, 11.54167F);
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "Mã nước xuất xứ";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel33
            // 
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(220.4166F, 11.54168F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(173.6586F, 11.54167F);
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "Mô tả hàng hóa";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSTTdonghang
            // 
            this.lblSTTdonghang.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTTdonghang.LocationFloat = new DevExpress.Utils.PointFloat(220.4166F, 0F);
            this.lblSTTdonghang.Name = "lblSTTdonghang";
            this.lblSTTdonghang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTTdonghang.SizeF = new System.Drawing.SizeF(38.81895F, 11.54167F);
            this.lblSTTdonghang.StylePriority.UseFont = false;
            this.lblSTTdonghang.Text = "XE";
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(210.1169F, 11.54167F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.Text = "Số thứ tự dòng/hàng trên tờ khai gốc";
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMahetthoihan,
            this.lblDauhieu,
            this.xrLabel28,
            this.lblGiodangky,
            this.lblNgaydangky,
            this.lblGiohoanthanhktra,
            this.lblThoihantainhap,
            this.lblNgaytokhaiXN,
            this.xrLabel23,
            this.xrLabel22,
            this.lblNhomxulyhoso,
            this.lblNgayhoanthanhktra,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.lblMaloaihinh,
            this.xrLabel14,
            this.lblSotokhaiXNK,
            this.xrLabel12,
            this.lblNgaycapphep,
            this.lblPhanloaiXNK,
            this.lblCoquannhan,
            this.lblSotokhaibosung,
            this.lblSothongbao,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.xrLine1});
            this.TopMargin.HeightF = 135F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMahetthoihan
            // 
            this.lblMahetthoihan.LocationFloat = new DevExpress.Utils.PointFloat(685.2988F, 69.63281F);
            this.lblMahetthoihan.Name = "lblMahetthoihan";
            this.lblMahetthoihan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMahetthoihan.SizeF = new System.Drawing.SizeF(34.98431F, 11.54167F);
            this.lblMahetthoihan.Text = "X";
            // 
            // lblDauhieu
            // 
            this.lblDauhieu.LocationFloat = new DevExpress.Utils.PointFloat(650.3145F, 69.63272F);
            this.lblDauhieu.Name = "lblDauhieu";
            this.lblDauhieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDauhieu.SizeF = new System.Drawing.SizeF(34.98431F, 11.54167F);
            this.lblDauhieu.Text = "X";
            // 
            // xrLabel28
            // 
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(629.9009F, 69.63272F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(20.41366F, 11.54167F);
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "-";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblGiodangky
            // 
            this.lblGiodangky.LocationFloat = new DevExpress.Utils.PointFloat(629.9009F, 46.54945F);
            this.lblGiodangky.Name = "lblGiodangky";
            this.lblGiodangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiodangky.SizeF = new System.Drawing.SizeF(70.31262F, 11.54166F);
            this.lblGiodangky.Text = "hh:mm:ss";
            // 
            // lblNgaydangky
            // 
            this.lblNgaydangky.LocationFloat = new DevExpress.Utils.PointFloat(546.1801F, 46.54943F);
            this.lblNgaydangky.Name = "lblNgaydangky";
            this.lblNgaydangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaydangky.SizeF = new System.Drawing.SizeF(83.72092F, 11.54166F);
            this.lblNgaydangky.Text = "dd/MM/yyyy";
            // 
            // lblGiohoanthanhktra
            // 
            this.lblGiohoanthanhktra.LocationFloat = new DevExpress.Utils.PointFloat(629.9009F, 35.00781F);
            this.lblGiohoanthanhktra.Name = "lblGiohoanthanhktra";
            this.lblGiohoanthanhktra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiohoanthanhktra.SizeF = new System.Drawing.SizeF(70.31262F, 11.54166F);
            this.lblGiohoanthanhktra.Text = "hh:mm:ss";
            // 
            // lblThoihantainhap
            // 
            this.lblThoihantainhap.LocationFloat = new DevExpress.Utils.PointFloat(546.1801F, 81.17439F);
            this.lblThoihantainhap.Name = "lblThoihantainhap";
            this.lblThoihantainhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoihantainhap.SizeF = new System.Drawing.SizeF(139.1187F, 11.54167F);
            this.lblThoihantainhap.Text = "dd/MM/yyyy";
            // 
            // lblNgaytokhaiXN
            // 
            this.lblNgaytokhaiXN.LocationFloat = new DevExpress.Utils.PointFloat(546.1801F, 69.63277F);
            this.lblNgaytokhaiXN.Name = "lblNgaytokhaiXN";
            this.lblNgaytokhaiXN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaytokhaiXN.SizeF = new System.Drawing.SizeF(83.72092F, 11.54166F);
            this.lblNgaytokhaiXN.Text = "dd/MM/yyyy";
            // 
            // xrLabel23
            // 
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(377.8246F, 81.17444F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(168.3554F, 11.54167F);
            this.xrLabel23.Text = "Thời hạn tái nhập/ tái xuất";
            // 
            // xrLabel22
            // 
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(377.8246F, 69.63277F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(168.3555F, 11.54167F);
            this.xrLabel22.Text = "Ngày tờ khai xuất nhập khấu";
            // 
            // lblNhomxulyhoso
            // 
            this.lblNhomxulyhoso.LocationFloat = new DevExpress.Utils.PointFloat(546.1801F, 58.0911F);
            this.lblNhomxulyhoso.Name = "lblNhomxulyhoso";
            this.lblNhomxulyhoso.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNhomxulyhoso.SizeF = new System.Drawing.SizeF(139.1187F, 11.54167F);
            this.lblNhomxulyhoso.Text = "XE";
            // 
            // lblNgayhoanthanhktra
            // 
            this.lblNgayhoanthanhktra.LocationFloat = new DevExpress.Utils.PointFloat(546.1801F, 35.00778F);
            this.lblNgayhoanthanhktra.Name = "lblNgayhoanthanhktra";
            this.lblNgayhoanthanhktra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayhoanthanhktra.SizeF = new System.Drawing.SizeF(83.72092F, 11.54166F);
            this.lblNgayhoanthanhktra.Text = "dd/MM/yyyy";
            // 
            // xrLabel18
            // 
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(377.8246F, 58.09114F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(168.3555F, 11.54167F);
            this.xrLabel18.Text = "Nhóm xử lý hồ sơ";
            // 
            // xrLabel17
            // 
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(377.8246F, 46.54947F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(168.3554F, 11.54167F);
            this.xrLabel17.Text = "Ngày đăng ký";
            // 
            // xrLabel16
            // 
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(377.8246F, 35.00778F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(168.3554F, 11.54168F);
            this.xrLabel16.Text = "Ngày hoàn thành kiểm tra";
            // 
            // lblMaloaihinh
            // 
            this.lblMaloaihinh.LocationFloat = new DevExpress.Utils.PointFloat(304.2896F, 69.6328F);
            this.lblMaloaihinh.Name = "lblMaloaihinh";
            this.lblMaloaihinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaloaihinh.SizeF = new System.Drawing.SizeF(36.60071F, 11.54168F);
            this.lblMaloaihinh.Text = "XXE";
            // 
            // xrLabel14
            // 
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(283.8759F, 69.63279F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(20.41366F, 11.54167F);
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "-";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSotokhaiXNK
            // 
            this.lblSotokhaiXNK.LocationFloat = new DevExpress.Utils.PointFloat(160.9442F, 69.63277F);
            this.lblSotokhaiXNK.Name = "lblSotokhaiXNK";
            this.lblSotokhaiXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhaiXNK.SizeF = new System.Drawing.SizeF(122.9317F, 11.54167F);
            this.lblSotokhaiXNK.Text = "XXXXXXXXX1XE";
            // 
            // xrLabel12
            // 
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(140.5306F, 69.63277F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(20.41366F, 11.54167F);
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "-";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgaycapphep
            // 
            this.lblNgaycapphep.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 81.17445F);
            this.lblNgaycapphep.Name = "lblNgaycapphep";
            this.lblNgaycapphep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaycapphep.SizeF = new System.Drawing.SizeF(100F, 11.54167F);
            this.lblNgaycapphep.Text = "dd/MM/yyyy";
            // 
            // lblPhanloaiXNK
            // 
            this.lblPhanloaiXNK.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 69.63279F);
            this.lblPhanloaiXNK.Name = "lblPhanloaiXNK";
            this.lblPhanloaiXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaiXNK.SizeF = new System.Drawing.SizeF(20.41366F, 11.54167F);
            this.lblPhanloaiXNK.Text = "X";
            // 
            // lblCoquannhan
            // 
            this.lblCoquannhan.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 58.09114F);
            this.lblCoquannhan.Name = "lblCoquannhan";
            this.lblCoquannhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCoquannhan.SizeF = new System.Drawing.SizeF(139.1187F, 11.54167F);
            this.lblCoquannhan.Text = "XXXXXXXXXE";
            // 
            // lblSotokhaibosung
            // 
            this.lblSotokhaibosung.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 46.54948F);
            this.lblSotokhaibosung.Name = "lblSotokhaibosung";
            this.lblSotokhaibosung.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhaibosung.SizeF = new System.Drawing.SizeF(139.1187F, 11.54167F);
            this.lblSotokhaibosung.Text = "XXXXXXXXX1XE";
            // 
            // lblSothongbao
            // 
            this.lblSothongbao.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 35.00781F);
            this.lblSothongbao.Name = "lblSothongbao";
            this.lblSothongbao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSothongbao.SizeF = new System.Drawing.SizeF(139.1187F, 11.54166F);
            this.lblSothongbao.Text = "XXXXXXXXX1XE";
            // 
            // xrLabel6
            // 
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 81.17445F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(110.1169F, 11.54166F);
            this.xrLabel6.Text = "Ngày cấp phép";
            // 
            // xrLabel5
            // 
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 69.63279F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(110.1169F, 11.54167F);
            this.xrLabel5.Text = "Số tờ khai";
            // 
            // xrLabel4
            // 
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 58.09114F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(110.1169F, 11.54167F);
            this.xrLabel4.Text = "Cơ quan nhận";
            // 
            // xrLabel3
            // 
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 46.54946F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(110.1169F, 11.54167F);
            this.xrLabel3.Text = "Số tờ khai bổ sung";
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 35.0078F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(110.1169F, 11.54167F);
            this.xrLabel2.Text = "Số thông báo";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(104.1667F, 10.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(532.2917F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Quyết định chấp thuận khai sửa đổi bổ sung trong trường hợp tăng thuế";
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 92.71613F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(722F, 9F);
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.DetailReport1,
            this.ReportHeader1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail1.HeightF = 127.0833F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(721.9999F, 127.0833F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblHienthimienthue2,
            this.xrLabel11,
            this.xrLabel10,
            this.lblMaapdungthuesuat,
            this.lblThuesuattruockhaibosung,
            this.lblSotienthue,
            this.xrLabel8,
            this.lblTenkhoanmuctiepnhan,
            this.xrLabel21,
            this.lblMadvtinhsoluongtruoc,
            this.xrLabel25,
            this.lblTrigiathuetruockhaibosung,
            this.lblSLtinhthuetruockhikhaibosung,
            this.lblMakhoanmuctiepnhan});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.Weight = 1.1795399360584828;
            // 
            // lblHienthimienthue2
            // 
            this.lblHienthimienthue2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblHienthimienthue2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHienthimienthue2.LocationFloat = new DevExpress.Utils.PointFloat(239.4005F, 85.4166F);
            this.lblHienthimienthue2.Name = "lblHienthimienthue2";
            this.lblHienthimienthue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHienthimienthue2.SizeF = new System.Drawing.SizeF(37.84723F, 15.70831F);
            this.lblHienthimienthue2.StylePriority.UseBorders = false;
            this.lblHienthimienthue2.StylePriority.UseFont = false;
            this.lblHienthimienthue2.Text = "X";
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(3.288532E-05F, 69.70827F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(67.29168F, 15.70832F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.Text = "Thuế suất";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.4166F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(103.3646F, 15.70831F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.Text = "Số tiền thuế";
            // 
            // lblMaapdungthuesuat
            // 
            this.lblMaapdungthuesuat.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaapdungthuesuat.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaapdungthuesuat.LocationFloat = new DevExpress.Utils.PointFloat(125F, 53.9999F);
            this.lblMaapdungthuesuat.Name = "lblMaapdungthuesuat";
            this.lblMaapdungthuesuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaapdungthuesuat.SizeF = new System.Drawing.SizeF(121.1866F, 15.70831F);
            this.lblMaapdungthuesuat.StylePriority.UseBorders = false;
            this.lblMaapdungthuesuat.StylePriority.UseFont = false;
            this.lblMaapdungthuesuat.StylePriority.UseTextAlignment = false;
            this.lblMaapdungthuesuat.Text = "XXXXXXXXX1XE";
            this.lblMaapdungthuesuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblThuesuattruockhaibosung
            // 
            this.lblThuesuattruockhaibosung.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblThuesuattruockhaibosung.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThuesuattruockhaibosung.LocationFloat = new DevExpress.Utils.PointFloat(67.29171F, 69.70827F);
            this.lblThuesuattruockhaibosung.Name = "lblThuesuattruockhaibosung";
            this.lblThuesuattruockhaibosung.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThuesuattruockhaibosung.SizeF = new System.Drawing.SizeF(210.2557F, 15.70834F);
            this.lblThuesuattruockhaibosung.StylePriority.UseBorders = false;
            this.lblThuesuattruockhaibosung.StylePriority.UseFont = false;
            this.lblThuesuattruockhaibosung.StylePriority.UseTextAlignment = false;
            this.lblThuesuattruockhaibosung.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThuesuattruockhaibosung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSotienthue
            // 
            this.lblSotienthue.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSotienthue.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotienthue.LocationFloat = new DevExpress.Utils.PointFloat(103.3646F, 85.4166F);
            this.lblSotienthue.Name = "lblSotienthue";
            this.lblSotienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotienthue.SizeF = new System.Drawing.SizeF(136.0358F, 15.70831F);
            this.lblSotienthue.StylePriority.UseBorders = false;
            this.lblSotienthue.StylePriority.UseFont = false;
            this.lblSotienthue.StylePriority.UseTextAlignment = false;
            this.lblSotienthue.Text = "1,234,567,890,123,456";
            this.lblSotienthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 53.99997F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(125F, 15.70831F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.Text = "Mã xác định thuế suất";
            // 
            // lblTenkhoanmuctiepnhan
            // 
            this.lblTenkhoanmuctiepnhan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTenkhoanmuctiepnhan.LocationFloat = new DevExpress.Utils.PointFloat(140.5306F, 10.00001F);
            this.lblTenkhoanmuctiepnhan.Name = "lblTenkhoanmuctiepnhan";
            this.lblTenkhoanmuctiepnhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenkhoanmuctiepnhan.SizeF = new System.Drawing.SizeF(119.4195F, 12.58331F);
            this.lblTenkhoanmuctiepnhan.StylePriority.UseBorders = false;
            this.lblTenkhoanmuctiepnhan.StylePriority.UseTextAlignment = false;
            this.lblTenkhoanmuctiepnhan.Text = "XXXXXXXXE";
            this.lblTenkhoanmuctiepnhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 38.29167F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(125F, 15.7083F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Số lượng tính thuế";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMadvtinhsoluongtruoc
            // 
            this.lblMadvtinhsoluongtruoc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMadvtinhsoluongtruoc.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadvtinhsoluongtruoc.LocationFloat = new DevExpress.Utils.PointFloat(234.5173F, 38.29162F);
            this.lblMadvtinhsoluongtruoc.Name = "lblMadvtinhsoluongtruoc";
            this.lblMadvtinhsoluongtruoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadvtinhsoluongtruoc.SizeF = new System.Drawing.SizeF(42.73044F, 15.70829F);
            this.lblMadvtinhsoluongtruoc.StylePriority.UseBorders = false;
            this.lblMadvtinhsoluongtruoc.StylePriority.UseFont = false;
            this.lblMadvtinhsoluongtruoc.Text = "XXXE";
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(0F, 22.58334F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(125F, 15.70833F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Trị giá tính thuế";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTrigiathuetruockhaibosung
            // 
            this.lblTrigiathuetruockhaibosung.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTrigiathuetruockhaibosung.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrigiathuetruockhaibosung.LocationFloat = new DevExpress.Utils.PointFloat(125F, 22.58335F);
            this.lblTrigiathuetruockhaibosung.Name = "lblTrigiathuetruockhaibosung";
            this.lblTrigiathuetruockhaibosung.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrigiathuetruockhaibosung.SizeF = new System.Drawing.SizeF(139.8332F, 15.70832F);
            this.lblTrigiathuetruockhaibosung.StylePriority.UseBorders = false;
            this.lblTrigiathuetruockhaibosung.StylePriority.UseFont = false;
            this.lblTrigiathuetruockhaibosung.StylePriority.UseTextAlignment = false;
            this.lblTrigiathuetruockhaibosung.Text = "12,345,678,901,234,567 ";
            this.lblTrigiathuetruockhaibosung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSLtinhthuetruockhikhaibosung
            // 
            this.lblSLtinhthuetruockhikhaibosung.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSLtinhthuetruockhikhaibosung.LocationFloat = new DevExpress.Utils.PointFloat(125F, 38.2916F);
            this.lblSLtinhthuetruockhikhaibosung.Name = "lblSLtinhthuetruockhikhaibosung";
            this.lblSLtinhthuetruockhikhaibosung.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSLtinhthuetruockhikhaibosung.SizeF = new System.Drawing.SizeF(109.5173F, 15.70837F);
            this.lblSLtinhthuetruockhikhaibosung.StylePriority.UseBorders = false;
            this.lblSLtinhthuetruockhikhaibosung.StylePriority.UseTextAlignment = false;
            this.lblSLtinhthuetruockhikhaibosung.Text = "123,456,789,012";
            this.lblSLtinhthuetruockhikhaibosung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMakhoanmuctiepnhan
            // 
            this.lblMakhoanmuctiepnhan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMakhoanmuctiepnhan.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 10.00001F);
            this.lblMakhoanmuctiepnhan.Name = "lblMakhoanmuctiepnhan";
            this.lblMakhoanmuctiepnhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMakhoanmuctiepnhan.SizeF = new System.Drawing.SizeF(20.41364F, 12.58331F);
            this.lblMakhoanmuctiepnhan.StylePriority.UseBorders = false;
            this.lblMakhoanmuctiepnhan.StylePriority.UseTextAlignment = false;
            this.lblMakhoanmuctiepnhan.Text = "X ";
            this.lblMakhoanmuctiepnhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSlthuesau,
            this.lblTrigiathuesau,
            this.lblMadvsoluong,
            this.lblMaapdungthuesau,
            this.lblThuesau,
            this.lblSotienthesau,
            this.lblHienthimienthuesau2});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.Weight = 0.97194961463486651;
            // 
            // lblSlthuesau
            // 
            this.lblSlthuesau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSlthuesau.LocationFloat = new DevExpress.Utils.PointFloat(79.89601F, 38.29159F);
            this.lblSlthuesau.Name = "lblSlthuesau";
            this.lblSlthuesau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSlthuesau.SizeF = new System.Drawing.SizeF(106.0557F, 15.70832F);
            this.lblSlthuesau.StylePriority.UseBorders = false;
            this.lblSlthuesau.StylePriority.UseTextAlignment = false;
            this.lblSlthuesau.Text = "123,456,789,012";
            this.lblSlthuesau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTrigiathuesau
            // 
            this.lblTrigiathuesau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTrigiathuesau.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrigiathuesau.LocationFloat = new DevExpress.Utils.PointFloat(79.89604F, 22.58334F);
            this.lblTrigiathuesau.Name = "lblTrigiathuesau";
            this.lblTrigiathuesau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrigiathuesau.SizeF = new System.Drawing.SizeF(147.8907F, 15.70829F);
            this.lblTrigiathuesau.StylePriority.UseBorders = false;
            this.lblTrigiathuesau.StylePriority.UseFont = false;
            this.lblTrigiathuesau.StylePriority.UseTextAlignment = false;
            this.lblTrigiathuesau.Text = "12,345,678,901,234,567 ";
            this.lblTrigiathuesau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMadvsoluong
            // 
            this.lblMadvsoluong.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMadvsoluong.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadvsoluong.LocationFloat = new DevExpress.Utils.PointFloat(185.9517F, 38.29159F);
            this.lblMadvsoluong.Name = "lblMadvsoluong";
            this.lblMadvsoluong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadvsoluong.SizeF = new System.Drawing.SizeF(41.83502F, 15.70831F);
            this.lblMadvsoluong.StylePriority.UseBorders = false;
            this.lblMadvsoluong.StylePriority.UseFont = false;
            this.lblMadvsoluong.StylePriority.UseTextAlignment = false;
            this.lblMadvsoluong.Text = "XXXE";
            this.lblMadvsoluong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaapdungthuesau
            // 
            this.lblMaapdungthuesau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaapdungthuesau.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaapdungthuesau.LocationFloat = new DevExpress.Utils.PointFloat(106.6001F, 53.9999F);
            this.lblMaapdungthuesau.Name = "lblMaapdungthuesau";
            this.lblMaapdungthuesau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaapdungthuesau.SizeF = new System.Drawing.SizeF(121.1866F, 15.70831F);
            this.lblMaapdungthuesau.StylePriority.UseBorders = false;
            this.lblMaapdungthuesau.StylePriority.UseFont = false;
            this.lblMaapdungthuesau.StylePriority.UseTextAlignment = false;
            this.lblMaapdungthuesau.Text = "XXXXXXXXX1XE";
            this.lblMaapdungthuesau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblThuesau
            // 
            this.lblThuesau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblThuesau.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThuesau.LocationFloat = new DevExpress.Utils.PointFloat(12.94767F, 69.70821F);
            this.lblThuesau.Name = "lblThuesau";
            this.lblThuesau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThuesau.SizeF = new System.Drawing.SizeF(214.8391F, 15.70832F);
            this.lblThuesau.StylePriority.UseBorders = false;
            this.lblThuesau.StylePriority.UseFont = false;
            this.lblThuesau.StylePriority.UseTextAlignment = false;
            this.lblThuesau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThuesau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSotienthesau
            // 
            this.lblSotienthesau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSotienthesau.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotienthesau.LocationFloat = new DevExpress.Utils.PointFloat(53.90368F, 85.41653F);
            this.lblSotienthesau.Name = "lblSotienthesau";
            this.lblSotienthesau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotienthesau.SizeF = new System.Drawing.SizeF(136.0358F, 15.70831F);
            this.lblSotienthesau.StylePriority.UseBorders = false;
            this.lblSotienthesau.StylePriority.UseFont = false;
            this.lblSotienthesau.StylePriority.UseTextAlignment = false;
            this.lblSotienthesau.Text = "1,234,567,890,123,456";
            this.lblSotienthesau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblHienthimienthuesau2
            // 
            this.lblHienthimienthuesau2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblHienthimienthuesau2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHienthimienthuesau2.LocationFloat = new DevExpress.Utils.PointFloat(189.9395F, 85.41653F);
            this.lblHienthimienthuesau2.Name = "lblHienthimienthuesau2";
            this.lblHienthimienthuesau2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHienthimienthuesau2.SizeF = new System.Drawing.SizeF(37.84723F, 15.70831F);
            this.lblHienthimienthuesau2.StylePriority.UseBorders = false;
            this.lblHienthimienthuesau2.StylePriority.UseFont = false;
            this.lblHienthimienthuesau2.Text = "X";
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSotientanggiamthue2,
            this.lblBieuthisotien2});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "xrTableCell9";
            this.xrTableCell9.Weight = 0.84851044930665065;
            // 
            // lblSotientanggiamthue2
            // 
            this.lblSotientanggiamthue2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSotientanggiamthue2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotientanggiamthue2.LocationFloat = new DevExpress.Utils.PointFloat(71.26341F, 85.41653F);
            this.lblSotientanggiamthue2.Name = "lblSotientanggiamthue2";
            this.lblSotientanggiamthue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotientanggiamthue2.SizeF = new System.Drawing.SizeF(132.9448F, 15.70829F);
            this.lblSotientanggiamthue2.StylePriority.UseBorders = false;
            this.lblSotientanggiamthue2.StylePriority.UseFont = false;
            this.lblSotientanggiamthue2.StylePriority.UseTextAlignment = false;
            this.lblSotientanggiamthue2.Text = "1,234,567,890,123,456";
            this.lblSotientanggiamthue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblBieuthisotien2
            // 
            this.lblBieuthisotien2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblBieuthisotien2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBieuthisotien2.LocationFloat = new DevExpress.Utils.PointFloat(38.4782F, 85.41653F);
            this.lblBieuthisotien2.Name = "lblBieuthisotien2";
            this.lblBieuthisotien2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuthisotien2.SizeF = new System.Drawing.SizeF(32.78522F, 15.70832F);
            this.lblBieuthisotien2.StylePriority.UseBorders = false;
            this.lblBieuthisotien2.StylePriority.UseFont = false;
            this.lblBieuthisotien2.Text = "X";
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2});
            this.DetailReport1.Expanded = false;
            this.DetailReport1.Level = 0;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Name = "Detail2";
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTable2});
            this.ReportHeader1.HeightF = 125.7948F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(722F, 21.875F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Trước khi khai báo ";
            this.xrTableCell1.Weight = 1.213144738873509;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Text = "Sau khi khai báo";
            this.xrTableCell2.Weight = 0.99964030992023523;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.Text = "Tăng/giảm thuế";
            this.xrTableCell3.Weight = 0.87268497055199123;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21.875F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(721.9999F, 103.9198F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblHienthimienthue,
            this.xrLabel52,
            this.lblSotienthuetruoc,
            this.xrLabel41,
            this.xrLabel49,
            this.lblThuesuattruoc,
            this.xrLabel47,
            this.lblMasohanghoatruoc,
            this.lblSoluongtinhthuetruoc,
            this.xrLabel44,
            this.lblTrigiatinhthuetruoc,
            this.xrLabel42,
            this.lblMadvtinhSL});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.Weight = 1.179539682449779;
            // 
            // lblHienthimienthue
            // 
            this.lblHienthimienthue.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblHienthimienthue.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHienthimienthue.LocationFloat = new DevExpress.Utils.PointFloat(259.2355F, 85.4166F);
            this.lblHienthimienthue.Name = "lblHienthimienthue";
            this.lblHienthimienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHienthimienthue.SizeF = new System.Drawing.SizeF(18.01224F, 15.70831F);
            this.lblHienthimienthue.StylePriority.UseBorders = false;
            this.lblHienthimienthue.StylePriority.UseFont = false;
            this.lblHienthimienthue.Text = "X";
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.4166F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(90.32217F, 15.70832F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.Text = "Số tiền thuế";
            // 
            // lblSotienthuetruoc
            // 
            this.lblSotienthuetruoc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSotienthuetruoc.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotienthuetruoc.LocationFloat = new DevExpress.Utils.PointFloat(90.32217F, 85.4166F);
            this.lblSotienthuetruoc.Name = "lblSotienthuetruoc";
            this.lblSotienthuetruoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotienthuetruoc.SizeF = new System.Drawing.SizeF(168.9133F, 15.70831F);
            this.lblSotienthuetruoc.StylePriority.UseBorders = false;
            this.lblSotienthuetruoc.StylePriority.UseFont = false;
            this.lblSotienthuetruoc.StylePriority.UseTextAlignment = false;
            this.lblSotienthuetruoc.Text = "1,234,567,890,123,456";
            this.lblSotienthuetruoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(114.4888F, 10.00001F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(150.3444F, 12.58332F);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.Text = "Thuế xuất nhập khẩu";
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(3.207425E-05F, 69.7083F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(67.29167F, 15.70832F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.Text = "Thuế suất";
            // 
            // lblThuesuattruoc
            // 
            this.lblThuesuattruoc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblThuesuattruoc.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThuesuattruoc.LocationFloat = new DevExpress.Utils.PointFloat(67.29169F, 69.7083F);
            this.lblThuesuattruoc.Name = "lblThuesuattruoc";
            this.lblThuesuattruoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThuesuattruoc.SizeF = new System.Drawing.SizeF(206.5842F, 15.70832F);
            this.lblThuesuattruoc.StylePriority.UseBorders = false;
            this.lblThuesuattruoc.StylePriority.UseFont = false;
            this.lblThuesuattruoc.StylePriority.UseTextAlignment = false;
            this.lblThuesuattruoc.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblThuesuattruoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(0F, 53.99997F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(120.1169F, 15.70831F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.Text = "Mã số hàng hóa";
            // 
            // lblMasohanghoatruoc
            // 
            this.lblMasohanghoatruoc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMasohanghoatruoc.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMasohanghoatruoc.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 53.99997F);
            this.lblMasohanghoatruoc.Name = "lblMasohanghoatruoc";
            this.lblMasohanghoatruoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMasohanghoatruoc.SizeF = new System.Drawing.SizeF(139.1187F, 15.70832F);
            this.lblMasohanghoatruoc.StylePriority.UseBorders = false;
            this.lblMasohanghoatruoc.StylePriority.UseFont = false;
            this.lblMasohanghoatruoc.StylePriority.UseTextAlignment = false;
            this.lblMasohanghoatruoc.Text = "XXXXXXXXX1XE";
            this.lblMasohanghoatruoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblSoluongtinhthuetruoc
            // 
            this.lblSoluongtinhthuetruoc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSoluongtinhthuetruoc.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 38.29166F);
            this.lblSoluongtinhthuetruoc.Name = "lblSoluongtinhthuetruoc";
            this.lblSoluongtinhthuetruoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoluongtinhthuetruoc.SizeF = new System.Drawing.SizeF(119.2835F, 15.70834F);
            this.lblSoluongtinhthuetruoc.StylePriority.UseBorders = false;
            this.lblSoluongtinhthuetruoc.Text = "123,456,789,012";
            // 
            // xrLabel44
            // 
            this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(0F, 38.29166F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(120.1169F, 15.70832F);
            this.xrLabel44.StylePriority.UseBorders = false;
            this.xrLabel44.Text = "Số lượng tính thuế";
            // 
            // lblTrigiatinhthuetruoc
            // 
            this.lblTrigiatinhthuetruoc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTrigiatinhthuetruoc.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrigiatinhthuetruoc.LocationFloat = new DevExpress.Utils.PointFloat(120.1169F, 22.58332F);
            this.lblTrigiatinhthuetruoc.Name = "lblTrigiatinhthuetruoc";
            this.lblTrigiatinhthuetruoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrigiatinhthuetruoc.SizeF = new System.Drawing.SizeF(139.8332F, 15.70833F);
            this.lblTrigiatinhthuetruoc.StylePriority.UseBorders = false;
            this.lblTrigiatinhthuetruoc.StylePriority.UseFont = false;
            this.lblTrigiatinhthuetruoc.Text = "12,345,678,901,234,567 ";
            // 
            // xrLabel42
            // 
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(3.207425E-05F, 22.58334F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(120.1169F, 15.70832F);
            this.xrLabel42.StylePriority.UseBorders = false;
            this.xrLabel42.Text = "Trị giá tính thuế";
            // 
            // lblMadvtinhSL
            // 
            this.lblMadvtinhSL.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMadvtinhSL.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadvtinhSL.LocationFloat = new DevExpress.Utils.PointFloat(239.4004F, 38.29166F);
            this.lblMadvtinhSL.Name = "lblMadvtinhSL";
            this.lblMadvtinhSL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadvtinhSL.SizeF = new System.Drawing.SizeF(42.43076F, 15.70834F);
            this.lblMadvtinhSL.StylePriority.UseBorders = false;
            this.lblMadvtinhSL.StylePriority.UseFont = false;
            this.lblMadvtinhSL.Text = "XXXE";
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblHienthimienthuesau,
            this.lblMasohanghoasau,
            this.lblSoluongtinhthuesau,
            this.lblTrigiatinhthuesau,
            this.lblMadvtinhSLsau,
            this.lblThuesuatsau,
            this.lblSotienthuesau});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Text = "xrTableCell5";
            this.xrTableCell5.Weight = 0.97194986803437822;
            // 
            // lblHienthimienthuesau
            // 
            this.lblHienthimienthuesau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblHienthimienthuesau.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHienthimienthuesau.LocationFloat = new DevExpress.Utils.PointFloat(180.9837F, 85.41663F);
            this.lblHienthimienthuesau.Name = "lblHienthimienthuesau";
            this.lblHienthimienthuesau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHienthimienthuesau.SizeF = new System.Drawing.SizeF(38.74554F, 15.70833F);
            this.lblHienthimienthuesau.StylePriority.UseBorders = false;
            this.lblHienthimienthuesau.StylePriority.UseFont = false;
            this.lblHienthimienthuesau.StylePriority.UseTextAlignment = false;
            this.lblHienthimienthuesau.Text = "X";
            this.lblHienthimienthuesau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMasohanghoasau
            // 
            this.lblMasohanghoasau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMasohanghoasau.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMasohanghoasau.LocationFloat = new DevExpress.Utils.PointFloat(68.04089F, 53.62167F);
            this.lblMasohanghoasau.Name = "lblMasohanghoasau";
            this.lblMasohanghoasau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMasohanghoasau.SizeF = new System.Drawing.SizeF(151.6883F, 15.70834F);
            this.lblMasohanghoasau.StylePriority.UseBorders = false;
            this.lblMasohanghoasau.StylePriority.UseFont = false;
            this.lblMasohanghoasau.StylePriority.UseTextAlignment = false;
            this.lblMasohanghoasau.Text = "XXXXXXXXX1XE";
            this.lblMasohanghoasau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblSoluongtinhthuesau
            // 
            this.lblSoluongtinhthuesau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSoluongtinhthuesau.LocationFloat = new DevExpress.Utils.PointFloat(68.04087F, 38.29166F);
            this.lblSoluongtinhthuesau.Name = "lblSoluongtinhthuesau";
            this.lblSoluongtinhthuesau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoluongtinhthuesau.SizeF = new System.Drawing.SizeF(106.0557F, 15.33F);
            this.lblSoluongtinhthuesau.StylePriority.UseBorders = false;
            this.lblSoluongtinhthuesau.StylePriority.UseTextAlignment = false;
            this.lblSoluongtinhthuesau.Text = "123,456,789,012";
            this.lblSoluongtinhthuesau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTrigiatinhthuesau
            // 
            this.lblTrigiatinhthuesau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTrigiatinhthuesau.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrigiatinhthuesau.LocationFloat = new DevExpress.Utils.PointFloat(68.04089F, 22.58335F);
            this.lblTrigiatinhthuesau.Name = "lblTrigiatinhthuesau";
            this.lblTrigiatinhthuesau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrigiatinhthuesau.SizeF = new System.Drawing.SizeF(151.2915F, 15.70832F);
            this.lblTrigiatinhthuesau.StylePriority.UseBorders = false;
            this.lblTrigiatinhthuesau.StylePriority.UseFont = false;
            this.lblTrigiatinhthuesau.Text = "12,345,678,901,234,567 ";
            // 
            // lblMadvtinhSLsau
            // 
            this.lblMadvtinhSLsau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMadvtinhSLsau.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadvtinhSLsau.LocationFloat = new DevExpress.Utils.PointFloat(174.0966F, 38.29167F);
            this.lblMadvtinhSLsau.Name = "lblMadvtinhSLsau";
            this.lblMadvtinhSLsau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadvtinhSLsau.SizeF = new System.Drawing.SizeF(45.23584F, 15.33F);
            this.lblMadvtinhSLsau.StylePriority.UseBorders = false;
            this.lblMadvtinhSLsau.StylePriority.UseFont = false;
            this.lblMadvtinhSLsau.StylePriority.UseTextAlignment = false;
            this.lblMadvtinhSLsau.Text = "XXXE";
            this.lblMadvtinhSLsau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblThuesuatsau
            // 
            this.lblThuesuatsau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblThuesuatsau.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThuesuatsau.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69.32999F);
            this.lblThuesuatsau.Name = "lblThuesuatsau";
            this.lblThuesuatsau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThuesuatsau.SizeF = new System.Drawing.SizeF(219.7292F, 16.08664F);
            this.lblThuesuatsau.StylePriority.UseBorders = false;
            this.lblThuesuatsau.StylePriority.UseFont = false;
            this.lblThuesuatsau.StylePriority.UseTextAlignment = false;
            this.lblThuesuatsau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblThuesuatsau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblSotienthuesau
            // 
            this.lblSotienthuesau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSotienthuesau.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotienthuesau.LocationFloat = new DevExpress.Utils.PointFloat(20.41383F, 85.41663F);
            this.lblSotienthuesau.Name = "lblSotienthuesau";
            this.lblSotienthuesau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotienthuesau.SizeF = new System.Drawing.SizeF(160.173F, 15.70831F);
            this.lblSotienthuesau.StylePriority.UseBorders = false;
            this.lblSotienthuesau.StylePriority.UseFont = false;
            this.lblSotienthuesau.StylePriority.UseTextAlignment = false;
            this.lblSotienthuesau.Text = "1,234,567,890,123,456";
            this.lblSotienthuesau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSotientanggiamthue,
            this.lblBieuthisotien});
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 0.84851044951584287;
            // 
            // lblSotientanggiamthue
            // 
            this.lblSotientanggiamthue.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSotientanggiamthue.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotientanggiamthue.LocationFloat = new DevExpress.Utils.PointFloat(69.54651F, 85.4166F);
            this.lblSotientanggiamthue.Name = "lblSotientanggiamthue";
            this.lblSotientanggiamthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotientanggiamthue.SizeF = new System.Drawing.SizeF(132.9448F, 15.70832F);
            this.lblSotientanggiamthue.StylePriority.UseBorders = false;
            this.lblSotientanggiamthue.StylePriority.UseFont = false;
            this.lblSotientanggiamthue.StylePriority.UseTextAlignment = false;
            this.lblSotientanggiamthue.Text = "1,234,567,890,123,456";
            this.lblSotientanggiamthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblBieuthisotien
            // 
            this.lblBieuthisotien.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblBieuthisotien.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBieuthisotien.LocationFloat = new DevExpress.Utils.PointFloat(36.76128F, 85.4166F);
            this.lblBieuthisotien.Name = "lblBieuthisotien";
            this.lblBieuthisotien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuthisotien.SizeF = new System.Drawing.SizeF(32.78522F, 15.70832F);
            this.lblBieuthisotien.StylePriority.UseBorders = false;
            this.lblBieuthisotien.StylePriority.UseFont = false;
            this.lblBieuthisotien.StylePriority.UseTextAlignment = false;
            this.lblBieuthisotien.Text = "X";
            this.lblBieuthisotien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSodong,
            this.xrLine2});
            this.ReportHeader.HeightF = 26.04167F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblSodong
            // 
            this.lblSodong.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.lblSodong.Name = "lblSodong";
            this.lblSodong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSodong.SizeF = new System.Drawing.SizeF(57.29166F, 11.54167F);
            this.lblSodong.Text = "<NE >";
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 11.54165F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(722F, 9F);
            // 
            // QuyetDinhChapThuanKhaiSuaDoiBoSungTrongTruongHopTangThue
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.ReportHeader});
            this.Margins = new System.Drawing.Printing.Margins(60, 68, 135, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhaiXNK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblNgaycapphep;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaiXNK;
        private DevExpress.XtraReports.UI.XRLabel lblCoquannhan;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhaibosung;
        private DevExpress.XtraReports.UI.XRLabel lblSothongbao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblGiohoanthanhktra;
        private DevExpress.XtraReports.UI.XRLabel lblThoihantainhap;
        private DevExpress.XtraReports.UI.XRLabel lblNgaytokhaiXN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel lblNhomxulyhoso;
        private DevExpress.XtraReports.UI.XRLabel lblNgayhoanthanhktra;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblMaloaihinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lblDauhieu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lblGiodangky;
        private DevExpress.XtraReports.UI.XRLabel lblNgaydangky;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel lblSTTdonghang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel lblSodong;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lblMotahanghoatruoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel lblManuocxuatxusau;
        private DevExpress.XtraReports.UI.XRLabel lblMotahanghoasau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel lblManuocxuatxutruoc;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel lblMadvtinhSL;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel lblThuesuattruoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel lblMasohanghoatruoc;
        private DevExpress.XtraReports.UI.XRLabel lblSoluongtinhthuetruoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel lblTrigiatinhthuetruoc;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel lblSotienthuetruoc;
        private DevExpress.XtraReports.UI.XRLabel lblMasohanghoasau;
        private DevExpress.XtraReports.UI.XRLabel lblSoluongtinhthuesau;
        private DevExpress.XtraReports.UI.XRLabel lblTrigiatinhthuesau;
        private DevExpress.XtraReports.UI.XRLabel lblMadvtinhSLsau;
        private DevExpress.XtraReports.UI.XRLabel lblThuesuatsau;
        private DevExpress.XtraReports.UI.XRLabel lblSotienthuesau;
        private DevExpress.XtraReports.UI.XRLabel lblSotientanggiamthue;
        private DevExpress.XtraReports.UI.XRLabel lblMahetthoihan;
        private DevExpress.XtraReports.UI.XRLabel lblHienthimienthue;
        private DevExpress.XtraReports.UI.XRLabel lblHienthimienthuesau;
        private DevExpress.XtraReports.UI.XRLabel lblBieuthisotien;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRLabel lblTenkhoanmuctiepnhan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel lblMadvtinhsoluongtruoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel lblTrigiathuetruockhaibosung;
        private DevExpress.XtraReports.UI.XRLabel lblSLtinhthuetruockhikhaibosung;
        private DevExpress.XtraReports.UI.XRLabel lblMakhoanmuctiepnhan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblMaapdungthuesuat;
        private DevExpress.XtraReports.UI.XRLabel lblThuesuattruockhaibosung;
        private DevExpress.XtraReports.UI.XRLabel lblSotienthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lblHienthimienthue2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel lblSlthuesau;
        private DevExpress.XtraReports.UI.XRLabel lblTrigiathuesau;
        private DevExpress.XtraReports.UI.XRLabel lblMadvsoluong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel lblSotientanggiamthue2;
        private DevExpress.XtraReports.UI.XRLabel lblBieuthisotien2;
        private DevExpress.XtraReports.UI.XRLabel lblMaapdungthuesau;
        private DevExpress.XtraReports.UI.XRLabel lblThuesau;
        private DevExpress.XtraReports.UI.XRLabel lblSotienthesau;
        private DevExpress.XtraReports.UI.XRLabel lblHienthimienthuesau2;
    }
}
