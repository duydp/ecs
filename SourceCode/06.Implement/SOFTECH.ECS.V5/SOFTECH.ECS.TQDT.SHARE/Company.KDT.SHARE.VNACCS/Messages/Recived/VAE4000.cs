﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAE4000 : BasicVNACC
    {
        public List<VAE4000_HANG> HangMD { get; set; }
        public void LoadVAE4000(string strResult)
        {
            try
            {
                this.GetObject<VAE4000>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAE4000, false, VAE4000.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAE4000.TongSoByte);
                while (true)
                {
                    VAE4000_HANG vad1agHang = new VAE4000_HANG();
                    vad1agHang.GetObject<VAE4000_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAE4000_HANG", false, VAE4000_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAE4000_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAE4000_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAE4000_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAE4000_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}
