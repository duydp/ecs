-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsSubSectionCode varchar(5),
	@ImportExportClassification varchar(5),
	@Name nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_CustomsSubSection]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@CustomsCode,
	@CustomsSubSectionCode,
	@ImportExportClassification,
	@Name,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Update]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsSubSectionCode varchar(5),
	@ImportExportClassification varchar(5),
	@Name nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CustomsSubSection]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[CustomsCode] = @CustomsCode,
	[CustomsSubSectionCode] = @CustomsSubSectionCode,
	[ImportExportClassification] = @ImportExportClassification,
	[Name] = @Name,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsSubSectionCode varchar(5),
	@ImportExportClassification varchar(5),
	@Name nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_CustomsSubSection] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CustomsSubSection] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[CustomsCode] = @CustomsCode,
			[CustomsSubSectionCode] = @CustomsSubSectionCode,
			[ImportExportClassification] = @ImportExportClassification,
			[Name] = @Name,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_CustomsSubSection]
		(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[CustomsCode],
			[CustomsSubSectionCode],
			[ImportExportClassification],
			[Name],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@CustomsCode,
			@CustomsSubSectionCode,
			@ImportExportClassification,
			@Name,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CustomsSubSection]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CustomsSubSection] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsSubSection]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CustomsSubSection] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsSubSection]	

GO

