/****** Object:  Table [dbo].[t_KDT_SXXK_BKHangTonKhoDangKy]    Script Date: 08/15/2014 15:20:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_BKHangTonKhoDangKy]') AND type in (N'U'))
CREATE TABLE [dbo].[t_KDT_SXXK_BKHangTonKhoDangKy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [bigint] NULL,
	[NgayTiepNhan] [datetime] NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaDoanhNghiep] [varchar](20) NULL,
	[LanThanhLy] [int] NULL,
	[TrangThaiXuLy] [int] NULL,
	[GUIDSTR] [nvarchar](200) NULL,
	[QuyQuyetToan] [int] NULL,
	[NamQuyetToan] [int] NULL,
 CONSTRAINT [PK_t_KDT_SXXK_BKHangTonKhoDangKy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 15, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200),
	@QuyQuyetToan int,
	@NamQuyetToan int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_BKHangTonKhoDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[QuyQuyetToan],
	[NamQuyetToan]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@LanThanhLy,
	@TrangThaiXuLy,
	@GUIDSTR,
	@QuyQuyetToan,
	@NamQuyetToan
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 15, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200),
	@QuyQuyetToan int,
	@NamQuyetToan int
AS

UPDATE
	[dbo].[t_KDT_SXXK_BKHangTonKhoDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[LanThanhLy] = @LanThanhLy,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GUIDSTR] = @GUIDSTR,
	[QuyQuyetToan] = @QuyQuyetToan,
	[NamQuyetToan] = @NamQuyetToan
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 15, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200),
	@QuyQuyetToan int,
	@NamQuyetToan int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_BKHangTonKhoDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_BKHangTonKhoDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[LanThanhLy] = @LanThanhLy,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GUIDSTR] = @GUIDSTR,
			[QuyQuyetToan] = @QuyQuyetToan,
			[NamQuyetToan] = @NamQuyetToan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_BKHangTonKhoDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[LanThanhLy],
			[TrangThaiXuLy],
			[GUIDSTR],
			[QuyQuyetToan],
			[NamQuyetToan]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@LanThanhLy,
			@TrangThaiXuLy,
			@GUIDSTR,
			@QuyQuyetToan,
			@NamQuyetToan
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 15, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_BKHangTonKhoDangKy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 15, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_BKHangTonKhoDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 15, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[QuyQuyetToan],
	[NamQuyetToan]
FROM
	[dbo].[t_KDT_SXXK_BKHangTonKhoDangKy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 15, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[QuyQuyetToan],
	[NamQuyetToan]
FROM [dbo].[t_KDT_SXXK_BKHangTonKhoDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 15, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[QuyQuyetToan],
	[NamQuyetToan]
FROM
	[dbo].[t_KDT_SXXK_BKHangTonKhoDangKy]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.2',GETDATE(), N' Cập nhật table hàng tồn kho, ')
END	
