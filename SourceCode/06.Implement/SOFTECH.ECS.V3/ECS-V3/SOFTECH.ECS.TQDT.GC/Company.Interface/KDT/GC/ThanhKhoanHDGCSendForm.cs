﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.KDT.GC.PCTFormTemplate;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class ThanhKhoanHDGCSendForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public ThanhKhoan KhaiBaoTK = new ThanhKhoan() { TrangThaiXuLy = -1 };
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        //Comment
        //-----------------------------------------------------------------------------------------
        public ThanhKhoanHDGCSendForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            HD = HopDong.Load(HD.ID);
            if (HD != null)
            {
                txtSoHopDong.Text = HD.SoHopDong;
                ccNgayHetHanHD.Value = HD.NgayHetHan;
                ccNgayKiHD.Value = HD.NgayKy;
                dgList.DataSource = KhaiBaoTK.HangCollection;
            }
            if (KhaiBaoTK != null)
            {
                KhaiBaoTK.LoadHangCollection();
                dgList.DataSource = KhaiBaoTK.HangCollection;
            }

        }

        private void setCommandStatus()
        {
            if (KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
            }
            else if (KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                lblTrangThai.Text = KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Wait for approval ") :
                    setText("Chờ hủy", "Wait for cancel");
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? Janus.Windows.UI.InheritableBoolean.True :
                     Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET ? setText("Không phê duyệt", "Not approval") :
                    setText("Đã hủy", "Canceled");
            }
            else
            {
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
            }
        }
        //-----------------------------------------------------------------------------------------
        private void PhuKienGCSendForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();

            if (KhaiBaoTK.ID > 0)
            {
                txtGhiChu.Text = KhaiBaoTK.GhiChu;
                ccNgayKhaiBao.Value = KhaiBaoTK.NgayDangKy;
                txtSoTiepNhan.Text = KhaiBaoTK.SoTiepNhan.ToString();
                //cbLoaiPhuKien.Value = pkdk.MaP
            }
            if (KhaiBaoTK.HangCollection == null || KhaiBaoTK.HangCollection.Count == 0)
                KhaiBaoTK.LoadHangCollection();
            dgList.DataSource = KhaiBaoTK.HangCollection;

            if (KhaiBaoTK.ID > 0)
            {
                Company.GC.BLL.KDT.SXXK.MsgSend msg = new Company.GC.BLL.KDT.SXXK.MsgSend();
                msg.master_id = this.KhaiBaoTK.ID;
                msg.LoaiHS = LoaiKhaiBao.ThanhKhoanHopDong;
                if (msg.Load())
                {
                    lblTrangThai.Text = setText("Chờ xác nhận ", "Wait for confirmation");
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
            }
            setCommandStatus();

        }

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["LoaiHang"].Text = (int)e.Row.Cells["LoaiHang"].Value == 1 ? "Nguyên phụ liệu" : "Thiết bị";
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            string id_loaiphukien = e.Row.Cells[1].Text.Trim();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        //private static string loaipk = "";
        private void save()
        {
            try
            {
                if (KhaiBaoTK.HangCollection == null || KhaiBaoTK.HangCollection.Count == 0)
                {
                    ShowMessage("Bạn chưa nhập hàng thanh khoản", false);
                    return;
                }
                HD = HopDong.Load(HD.ID);
                KhaiBaoTK.HopDong_ID = HD.ID;
                this.Cursor = Cursors.WaitCursor;
                {
                    KhaiBaoTK.GhiChu = txtGhiChu.Text;
                    KhaiBaoTK.HangCollection = (List<HangThanhKhoan>)dgList.DataSource;
                    KhaiBaoTK.NgayDangKy = DateTime.Now;
                    KhaiBaoTK.NgayTiepNhan = new DateTime(1900, 1, 1);
                    if (KhaiBaoTK.SoTiepNhan > 0) KhaiBaoTK.NgayTiepNhan = ccNgayTiepNhan.Value;
                    KhaiBaoTK.MaDoanhNghiep = HD.MaDoanhNghiep;
                    KhaiBaoTK.TenDoanhNghiep = HD.TenDoanhNghiep;
                    KhaiBaoTK.MaHaiQuan = HD.MaHaiQuan;
                    KhaiBaoTK.GUIDSTR = Guid.NewGuid().ToString();
                    if (KhaiBaoTK.ID == 0)
                        KhaiBaoTK.Insert();
                    else
                        KhaiBaoTK.Update();
                    foreach (HangThanhKhoan item in KhaiBaoTK.HangCollection)
                    {
                        item.Master_ID = KhaiBaoTK.ID;
                        if (item.ID == 0)
                            item.Insert();
                        else item.Update();
                    }
                    showMsg("MSG_SAV02");
                    #region Lưu log thao tác
                    if (this.OpenType == OpenFormType.Insert)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", KhaiBaoTK.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ThanhKhoanHopDong);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.ThanhKhoanHopDong;
                            log.ID_DK = KhaiBaoTK.ID;
                            log.GUIDSTR_DK = KhaiBaoTK.GUIDSTR;
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                    }
                    else if (this.OpenType == OpenFormType.Edit)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", KhaiBaoTK.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ThanhKhoanHopDong);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                    }
                    #endregion

                    setCommandStatus();
                }
            }

            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //MessageBox.Show("Có lỗi:" + ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdSave":
                    this.save();
                    break;
                case "cmdSend":
                    this.SendV3();
                    //this.send();
                    break;
                case "HuyKhaiBao":
                    this.CanceldV3();
                    //this.Huy();
                    break;
                case "NhanDuLieuPK":
                    //this.NhanDuLieuPK12();
                    FeedBackV3();
                    break;
                case "XacNhanThongTin":
                    FeedBackV3();
                    //this.LaySoTiepNhanDT();
                    break;
                case "InPhieuTN": inPhieuTN(); break;
            }
        }
        private void Add()
        {
            HangThanhKhoanHDForm f = new HangThanhKhoanHDForm();
            f.KhaiBaoTK = this.KhaiBaoTK;
            f.KhaiBaoTK.HopDong_ID = this.HD.ID;
            f.ShowDialog();
            dgList.DataSource = KhaiBaoTK.HangCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                ShowMessage("Hàng đã khai báo không thể xóa", false);
                return;
            }
            HangThanhKhoan hang = (HangThanhKhoan)e.Row.DataRow;
            if (hang.ID > 0) hang.Delete();
            KhaiBaoTK.HangCollection.Remove(hang);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

        }

        private void txtSoPhuKien_TextChanged(object sender, EventArgs e)
        {
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            dgList.DataSource = KhaiBaoTK.HangCollection;
            dgList.Refetch();

        }
        private void inPhieuTN()
        {
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "THANH KHOẢN HỢP ĐỒNG";
            phieuTN.soTN = this.KhaiBaoTK.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.KhaiBaoTK.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = KhaiBaoTK.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        #region V3 - 14/03/2012
        private void SendV3()
        {

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = LoaiKhaiBao.ThanhKhoanHopDong;
                sendXML.master_id = KhaiBaoTK.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    NhanDuLieuPK1.Enabled = NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    //MLMessages("Thông tin này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_SEN11","", false);
                    return;
                }
            }
            if (KhaiBaoTK.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi khai báo", false);
                return;
            }

            if (KhaiBaoTK.HangCollection.Count == 0)
            {
                showMsg("MSG_2702015");
                //MLMessages("Chưa nhập phụ kiện","MSG_WRN19","", false);
                return;
            }
            try
            {
                KhaiBaoTK.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.GC_ThanhKhoan tk = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferThanhKhoan(KhaiBaoTK, HD);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = KhaiBaoTK.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(KhaiBaoTK.MaHaiQuan),
                                     Identity = KhaiBaoTK.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,
                                    Function = tk.Function,
                                    Reference = KhaiBaoTK.GUIDSTR,
                                }
                                ,
                                tk);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(KhaiBaoTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoThanhKhoanHopDong);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ThanhKhoanHopDong;
                    sendXML.master_id = KhaiBaoTK.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.ThanhKhoanHDSendHandler(KhaiBaoTK, ref msgInfor, e);

        }
        private void FeedBackV3()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,
                Reference = KhaiBaoTK.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = KhaiBaoTK.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(KhaiBaoTK.MaHaiQuan.Trim()),
                                              Identity = KhaiBaoTK.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        this.setCommandStatus();
                }
            }
        }
        private void CanceldV3()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.ThanhKhoanHopDong;
            sendXML.master_id = KhaiBaoTK.ID;
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            Company.KDT.SHARE.Components.DeclarationBase dinhmuc = Company.GC.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD, KhaiBaoTK.GUIDSTR, KhaiBaoTK.SoTiepNhan, KhaiBaoTK.MaHaiQuan, KhaiBaoTK.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = KhaiBaoTK.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(KhaiBaoTK.MaHaiQuan),
                                 Identity = KhaiBaoTK.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = KhaiBaoTK.GUIDSTR,
                            }
                            ,
                            null);
            SendMessageForm sendForm = new SendMessageForm();
            KhaiBaoTK.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO;
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && KhaiBaoTK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                sendForm.Message.XmlSaveMessage(KhaiBaoTK.ID, Company.KDT.SHARE.Components.MessageTitle.HQHuyKhaiBaoToKhai);
                sendXML.func = 3;
                sendXML.InsertUpdate();
                KhaiBaoTK.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }

        #endregion

    }
}
