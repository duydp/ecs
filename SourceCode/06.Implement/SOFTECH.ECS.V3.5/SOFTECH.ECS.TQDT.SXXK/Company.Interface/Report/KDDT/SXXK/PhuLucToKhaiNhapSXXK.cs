﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ToKhai;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Collections.Generic;

namespace Company.Interface.Report.SXXK
{
    public partial class PhuLucToKhaiNhapSXXK : DevExpress.XtraReports.UI.XtraReport
    {
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public int SoToKhai;
        public DateTime NgayDangKy;
        public string MaLoaiHinh = "";
        public PhuLucToKhaiNhapSXXK()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongThueXNK = 0;
            if (this.MaLoaiHinh.Trim() == "NSX03")
            {
                lblTongTienThueXNK.Visible = false;
                xrTable2.Visible = false;
                xrLabel2.Location = new Point(167, 683);
                xrLabel2.Size = new Size(457, 83);
                xrLabel2.Text = "DOANH NGHIỆP KHU CHẾ XUẤT KHÔNG NỘP THUẾ";
            }
            lblMaHaiQuan.Text = GlobalSettings.MA_HAI_QUAN;
            if(NgayDangKy > new DateTime(1900,1,1))
                lblNgayDangKy.Text = NgayDangKy.ToString("dd/MM/yyyy");
            if(SoToKhai!=0)
                lblSoToKhai.Text = SoToKhai + "";
            for (int i = 0; i < this.HMDCollection.Count; i++)
            {
                XRControl control = new XRControl();
                HangMauDich hmd = this.HMDCollection[i];
                control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                control.Text = hmd.TenHang;
                control = this.xrTable1.Rows[i].Controls["MaHS" + (i + 1)];
                control.Text = hmd.MaHS;
                control = this.xrTable1.Rows[i].Controls["XuatXu" + (i + 1)];
                control.Text = hmd.NuocXX_ID;
                control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                control.Text = hmd.SoLuong.ToString("G20");
                control = this.xrTable1.Rows[i].Controls["DVT" + (i + 1)];
                control.Text = DonViTinh.GetName((object)hmd.DVT_ID);
                control = this.xrTable1.Rows[i].Controls["DonGiaNT" + (i + 1)];
                control.Text = hmd.DonGiaKB.ToString("G20");
                control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                control.Text = hmd.TriGiaKB.ToString("N2");
                control = this.xrTable2.Rows[i].Controls["TriGiaTT" + (i + 1)];
                control.Text = hmd.TriGiaTT.ToString("N0");
                control = this.xrTable2.Rows[i].Controls["ThueSuatXNK" + (i + 1)];
                control.Text = hmd.ThueSuatXNK.ToString("N0");
                control = this.xrTable2.Rows[i].Controls["TienThueXNK" + (i + 1)];
                control.Text = hmd.ThueXNK.ToString("N0");
                tongTriGiaNT += hmd.TriGiaKB;
                tongThueXNK += hmd.ThueXNK;
            }
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
        }
        public void setVisibleImage(bool t)
        {
            xrPictureBox1.Visible = t;
        }
    }
}
