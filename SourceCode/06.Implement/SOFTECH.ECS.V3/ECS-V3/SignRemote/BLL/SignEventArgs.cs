﻿using System;
using System.Collections.Generic;
using System.Text;
using SignRemote;
namespace SignRemote.BLL
{

  public  class SignEventArgs : EventArgs
    {
        public Exception Error { get; private set; }
        public TimeSpan TotalTime { get; private set; }
        public MessageSend Content { get; set; }
        public SignEventArgs(MessageSend msgSend, Exception ex)
            : this(msgSend, new TimeSpan(), ex)
        {

        }
        public SignEventArgs(MessageSend content, TimeSpan totalTime, Exception ex)
        {
            this.Content = content;
            this.TotalTime = totalTime;
            this.Error = ex;
        }
    }
}
