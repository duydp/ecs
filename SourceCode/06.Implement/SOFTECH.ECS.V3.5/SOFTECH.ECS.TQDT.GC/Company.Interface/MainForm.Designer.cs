﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
//
//
using eDeclaration;
using Janus.Windows.EditControls;
using Janus.Windows.ExplorerBar;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;

namespace Company.Interface
{
    partial class MainForm
    {
        private UICommandManager cmMain;
        private UIRebar TopRebar1;
        private UICommand cmdHeThong;
        private UICommand cmdHeThong1;
        private UICommand cmdThoat;
        private UICommand cmdThoat1;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private UIPanelManager pmMain;
        private UICommandBar cmbMenu;
        private UICommand cmdLoaiHinh;
        private UIPanel pnlSXXK;
        private UIPanelInnerContainer pnlSXXKContainer;
        private UIPanel pnlGiaCong;
        private UIPanelInnerContainer pnlGiaCongContainer;
        private UIPanel pnlKinhDoanh;
        private UIPanelInnerContainer pnlKinhDoanhContainer;
        private UIPanel pnlDauTu;
        internal ImageList ilSmall;
        internal ImageList ilMedium;
        internal ImageList ilLarge;
        private UICommand cmdReceiveAll;
        private Janus.Windows.ExplorerBar.ExplorerBar expSXXK;
        private Janus.Windows.ExplorerBar.ExplorerBar expGiaCong;
        private Janus.Windows.ExplorerBar.ExplorerBar expKD;
        private Janus.Windows.UI.CommandBars.UICommand cmdThoat2;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup132 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem358 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem359 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem360 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup133 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem361 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup134 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem362 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem363 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem364 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem365 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem366 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup135 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem367 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem368 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem369 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem370 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem371 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem372 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup136 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem373 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem374 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem375 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem376 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup137 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem377 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem378 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem379 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem380 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup138 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem381 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem382 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem383 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup139 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem384 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem385 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem386 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem387 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup140 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem388 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem389 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem390 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem391 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup141 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem392 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem393 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem394 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup142 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem395 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem396 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup143 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem397 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem398 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup144 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem399 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem400 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup145 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem401 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem402 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup146 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem403 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem404 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup147 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem405 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem406 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem407 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem408 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup148 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem409 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem410 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem411 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup83 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem232 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem233 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup149 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem234 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem412 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup150 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem413 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem414 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem415 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem416 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup151 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem417 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem418 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem419 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup152 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem420 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem421 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup153 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem422 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem423 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup154 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem424 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem425 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem426 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem427 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup155 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem428 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem429 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem430 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup156 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem431 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem432 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup157 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem433 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem434 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup158 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem435 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem436 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup159 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem437 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem438 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup160 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem439 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem440 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup161 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem441 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem442 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem443 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem444 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem445 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem446 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup162 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem447 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem448 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup163 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem449 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem450 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup164 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem451 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem452 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup165 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem453 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem454 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup166 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem455 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem456 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup167 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem457 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem458 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup168 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem459 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem460 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem461 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup169 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem462 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem463 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup170 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem464 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup171 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem465 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem466 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem467 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem468 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup172 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem469 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem470 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup173 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem471 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem472 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem473 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup174 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem474 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup175 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem475 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem476 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup176 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem477 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem478 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem479 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem480 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup177 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem481 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem482 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup178 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem483 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem484 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup179 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem485 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem486 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem487 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup180 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem488 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem489 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem490 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup181 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem491 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem492 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem493 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup182 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem494 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem495 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem496 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup183 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem497 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem498 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem499 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup184 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem500 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem501 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem502 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem503 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem504 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup185 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem505 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem506 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem507 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem508 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup186 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem509 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem510 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem511 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup187 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem512 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem513 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem514 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup188 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem515 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem516 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem517 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup189 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem518 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem519 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup190 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem520 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem521 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem522 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup191 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem523 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem524 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup192 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem525 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem526 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup193 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem527 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem528 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup194 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem529 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem530 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup195 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem531 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem532 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup196 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem533 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem534 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel17 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel18 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel19 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel20 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel21 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel22 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel23 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel24 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbMenu = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdHeThong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdDanhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.cmdBieuThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThue");
            this.QuanTri1 = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.Command11 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.Command01 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.DongBoDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.cmdHeThong = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdDataVersion1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.cmdBackUp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdUpdateDatabase1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateDatabase");
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdCapNhatHS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS");
            this.Separator10 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdNhapXuat2 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.Separator11 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdTLTTDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTLTTDN");
            this.cmdCauHinh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.mnuQuerySQL1 = new Janus.Windows.UI.CommandBars.UICommand("mnuQuerySQL");
            this.cmdLog1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdDaily1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDaily");
            this.Separator13 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.LoginUser1 = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.Separator7 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdNhanPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanPhanHoi");
            this.cmdThoat2 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdThoat = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdLoaiHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiHinh");
            this.cmdReceiveAll = new Janus.Windows.UI.CommandBars.UICommand("cmdReceiveAll");
            this.NhacNho = new Janus.Windows.UI.CommandBars.UICommand("NhacNho");
            this.DongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.cmdImport = new Janus.Windows.UI.CommandBars.UICommand("cmdImport");
            this.cmdImportNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.cmdImportNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportTTDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportTTDM");
            this.cmdImportToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.Command1 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.cmd20071 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd20031 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.cmdVN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdEng1 = new Janus.Windows.UI.CommandBars.UICommand("cmdEng");
            this.cmd2007 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd2003 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.Command0 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.cmdHelp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdHelpVideo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHelpVideo");
            this.cmdHDSDCKS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHDSDCKS");
            this.cmdHDSDVNACCS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHDSDVNACCS");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdAutoUpdate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.Separator9 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdTeamview1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTeamview");
            this.cmdTool1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTool");
            this.Separator12 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdActivate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdAbout1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdThongBaoVNACCS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThongBaoVNACCS");
            this.cmdUpdateCertificates1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateCertificates");
            this.cmdHelp = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdAbout = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdNPLNhapTon = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLNhapTon");
            this.cmdDanhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.cmdGetCategoryOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGetCategoryOnline");
            this.Separator6 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNuoc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNguyenTe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdNhomCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhomCuaKhau");
            this.cmdCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdDMSPGC1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDMSPGC");
            this.cmdBerth1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBerth");
            this.cmdCargo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCargo");
            this.cmdCityUNLOCODE1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCityUNLOCODE");
            this.cmdCommon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCommon");
            this.cmdContainerSize1 = new Janus.Windows.UI.CommandBars.UICommand("cmdContainerSize");
            this.cmdCustomsSubSection1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCustomsSubSection");
            this.cmdOGAUser1 = new Janus.Windows.UI.CommandBars.UICommand("cmdOGAUser");
            this.cmdStations1 = new Janus.Windows.UI.CommandBars.UICommand("cmdStations");
            this.cmdTaxClassificationCode1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTaxClassificationCode");
            this.cmdReloadData1 = new Janus.Windows.UI.CommandBars.UICommand("cmdReloadData");
            this.cmdUpdateCategoryOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateCategoryOnline");
            this.cmdMaHS = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNuoc = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNguyenTe = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdBackUp = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdRestore = new Janus.Windows.UI.CommandBars.UICommand("cmdRestore");
            this.ThongSoKetNoi = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdThietLapIn = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdExportExccel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdImportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdDongBoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdDongBoPhongKhai");
            this.cmdImportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdExportExccel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdCauHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.ThongSoKetNoi1 = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ1 = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdCauHinhToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.cmdThietLapIn1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdChuKySo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuKySo");
            this.cmdTimer1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTimer");
            this.cmdConfigReadExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigReadExcel");
            this.cmdCauHinhToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.QuanTri = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.QuanLyNguoiDung1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.QuanLyNguoiDung = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.LoginUser = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.TraCuuMaHS = new Janus.Windows.UI.CommandBars.UICommand("TraCuuMaHS");
            this.cmdAutoUpdate = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.NhapXML = new Janus.Windows.UI.CommandBars.UICommand("NhapXML");
            this.NhapHDXML = new Janus.Windows.UI.CommandBars.UICommand("NhapHDXML");
            this.NhapDMXML = new Janus.Windows.UI.CommandBars.UICommand("NhapDMXML");
            this.NhapPKXML = new Janus.Windows.UI.CommandBars.UICommand("NhapPKXML");
            this.XuatTKXML = new Janus.Windows.UI.CommandBars.UICommand("XuatTKXML");
            this.XuatTKMD1 = new Janus.Windows.UI.CommandBars.UICommand("XuatTKMD");
            this.XuatTKGCCT1 = new Janus.Windows.UI.CommandBars.UICommand("XuatTKGCCT");
            this.XuatTKMD = new Janus.Windows.UI.CommandBars.UICommand("XuatTKMD");
            this.XuatTKGCCT = new Janus.Windows.UI.CommandBars.UICommand("XuatTKGCCT");
            this.NhapTKXML = new Janus.Windows.UI.CommandBars.UICommand("NhapTKXML");
            this.NhapTKMD1 = new Janus.Windows.UI.CommandBars.UICommand("NhapTKMD");
            this.NhapTKGCCT1 = new Janus.Windows.UI.CommandBars.UICommand("NhapTKGCCT");
            this.NhapTKMD = new Janus.Windows.UI.CommandBars.UICommand("NhapTKMD");
            this.NhapTKGCCT = new Janus.Windows.UI.CommandBars.UICommand("NhapTKGCCT");
            this.cmdEnglish = new Janus.Windows.UI.CommandBars.UICommand("cmdEng");
            this.cmdVN = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdNhapXuat = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.NhapXML2 = new Janus.Windows.UI.CommandBars.UICommand("NhapXML");
            this.cmdNhapDuLieuTuDoangNghiep1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieuTuDoangNghiep");
            this.Separator8 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdXuatDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDN");
            this.cmdXuatDuLieuPhongKHai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuPhongKHai");
            this.cmdXuatDuLieuPhongKHai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuPhongKHai");
            this.cmdXuatHopDong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatHopDong");
            this.cmdXuatDinhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDinhMuc");
            this.cmdXuatPhuKien1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatPhuKien");
            this.cmdXuatToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.cmdXuatHopDong = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatHopDong");
            this.cmdXuatDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDinhMuc");
            this.cmdXuatPhuKien = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatPhuKien");
            this.cmdXuatToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.XuatTKGCCT2 = new Janus.Windows.UI.CommandBars.UICommand("XuatTKGCCT");
            this.XuatTKMD2 = new Janus.Windows.UI.CommandBars.UICommand("XuatTKMD");
            this.cmdActivate = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdDMSPGC = new Janus.Windows.UI.CommandBars.UICommand("cmdDMSPGC");
            this.cmdNhapDuLieuDaiLy = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieuDaiLy");
            this.cmdCloseMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.cmdCloseAllButMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.cmdCloseAll = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.cmdXuatDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDN");
            this.QuanLyMess = new Janus.Windows.UI.CommandBars.UICommand("QuanLyMess");
            this.cmdNhapDuLieuTuDoangNghiep = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieuTuDoangNghiep");
            this.cmdNhapHopDong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapHopDong");
            this.cmdNhapDinhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapPhuKien1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapPhuKien");
            this.cmdNhapToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.cmdNhapHopDong = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapHopDong");
            this.cmdNhapDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapPhuKien = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapPhuKien");
            this.cmdNhapToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.cmdNhapToKhaiGCCT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiGCCT");
            this.cmdNhapToKhaiMauDich1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiMauDich");
            this.cmdNhapToKhaiMauDich = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiMauDich");
            this.cmdNhapToKhaiGCCT = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiGCCT");
            this.cmdTLTTDN = new Janus.Windows.UI.CommandBars.UICommand("cmdTLTTDN");
            this.mnuQuerySQL = new Janus.Windows.UI.CommandBars.UICommand("mnuQuerySQL");
            this.cmdLog = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.cmdDataVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.cmdChuKySo = new Janus.Windows.UI.CommandBars.UICommand("cmdChuKySo");
            this.cmdTimer = new Janus.Windows.UI.CommandBars.UICommand("cmdTimer");
            this.cmdNhomCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdNhomCuaKhau");
            this.cmdGetCategoryOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdGetCategoryOnline");
            this.cmdBieuThue = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThue");
            this.TraCuuMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("TraCuuMaHS");
            this.Separator5 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdTraCuuXNKOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuXNKOnline");
            this.cmdTraCuuNoThueOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuNoThueOnline");
            this.cmdTraCuuVanBanOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuVanBanOnline");
            this.cmdTuVanHQOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTuVanHQOnline");
            this.cmdBieuThueXNK20181 = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThueXNK2018");
            this.cmdTraCuuXNKOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuXNKOnline");
            this.cmdTraCuuNoThueOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuNoThueOnline");
            this.cmdTraCuuVanBanOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuVanBanOnline");
            this.cmdTuVanHQOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTuVanHQOnline");
            this.cmdTeamview = new Janus.Windows.UI.CommandBars.UICommand("cmdTeamview");
            this.cmdCapNhatHS = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS");
            this.cmdCapNhatHS8Auto1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8Auto");
            this.cmdCapNhatHS8SoManual1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoManual");
            this.cmdCapNhatHS8Auto = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8Auto");
            this.cmdCapNhatHS8SoManual = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoManual");
            this.cmdTool = new Janus.Windows.UI.CommandBars.UICommand("cmdTool");
            this.cmdImageResizeHelp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImageResizeHelp");
            this.cmdSignFile1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSignFile");
            this.cmdHelpSignFile1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHelpSignFile");
            this.cmdImageResizeHelp = new Janus.Windows.UI.CommandBars.UICommand("cmdImageResizeHelp");
            this.cmdDaily = new Janus.Windows.UI.CommandBars.UICommand("cmdDaily");
            this.cmdUpdateDatabase = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateDatabase");
            this.cmdHelpVideo = new Janus.Windows.UI.CommandBars.UICommand("cmdHelpVideo");
            this.cmdHDSDCKS = new Janus.Windows.UI.CommandBars.UICommand("cmdHDSDCKS");
            this.cmdHDSDVNACCS = new Janus.Windows.UI.CommandBars.UICommand("cmdHDSDVNACCS");
            this.cmdThongBaoVNACCS = new Janus.Windows.UI.CommandBars.UICommand("cmdThongBaoVNACCS");
            this.cmdSignFile = new Janus.Windows.UI.CommandBars.UICommand("cmdSignFile");
            this.cmdHelpSignFile = new Janus.Windows.UI.CommandBars.UICommand("cmdHelpSignFile");
            this.cmdBerth = new Janus.Windows.UI.CommandBars.UICommand("cmdBerth");
            this.cmdCargo = new Janus.Windows.UI.CommandBars.UICommand("cmdCargo");
            this.cmdCityUNLOCODE = new Janus.Windows.UI.CommandBars.UICommand("cmdCityUNLOCODE");
            this.cmdCommon = new Janus.Windows.UI.CommandBars.UICommand("cmdCommon");
            this.cmdContainerSize = new Janus.Windows.UI.CommandBars.UICommand("cmdContainerSize");
            this.cmdCustomsSubSection = new Janus.Windows.UI.CommandBars.UICommand("cmdCustomsSubSection");
            this.cmdOGAUser = new Janus.Windows.UI.CommandBars.UICommand("cmdOGAUser");
            this.cmdPackagesUnit = new Janus.Windows.UI.CommandBars.UICommand("cmdPackagesUnit");
            this.cmdStations = new Janus.Windows.UI.CommandBars.UICommand("cmdStations");
            this.cmdTaxClassificationCode = new Janus.Windows.UI.CommandBars.UICommand("cmdTaxClassificationCode");
            this.cmdReloadData = new Janus.Windows.UI.CommandBars.UICommand("cmdReloadData");
            this.cmdBieuThueXNK2018 = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThueXNK2018");
            this.cmdUpdateCategoryOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateCategoryOnline");
            this.cmdNhanPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanPhanHoi");
            this.cmdUpdateCertificates = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateCertificates");
            this.cmdConfigReadExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigReadExcel");
            this.mnuRightClick = new Janus.Windows.UI.CommandBars.UIContextMenu();
            this.cmdCloseMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.cmdCloseAllButMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.cmdCloseAll1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.ilSmall = new System.Windows.Forms.ImageList(this.components);
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdThoat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.pmMain = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBar1 = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel2 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel2Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBar2 = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel3 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel3Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarVNACCS_TKMD = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel4 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel4Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarVNACCS_GiayPhep = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel5 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel5Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarVNACCS_HoaDon = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel6 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel6Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarVNACCS_ChungTu = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel7 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel7Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarVNACC_KetXuatDuLieu = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel8 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel8Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarKhoKeToan = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSXXK = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSXXKContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expSXXK = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlGiaCong = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlGiaCongContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expGiaCong = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlKinhDoanh = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlKinhDoanhContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKD = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlDauTu = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlDauTuContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expDT = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSend = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSendContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKhaiBao_TheoDoi = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.ilMedium = new System.Windows.Forms.ImageList(this.components);
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.statusBar = new Janus.Windows.UI.StatusBar.UIStatusBar();
            this.cmdThoat3 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmdPerformanceDatabase = new Janus.Windows.UI.CommandBars.UICommand("cmdPerformanceDatabase");
            this.cmdPerformanceDatabase1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPerformanceDatabase");
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).BeginInit();
            this.uiPanel2.SuspendLayout();
            this.uiPanel2Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel3)).BeginInit();
            this.uiPanel3.SuspendLayout();
            this.uiPanel3Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_TKMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel4)).BeginInit();
            this.uiPanel4.SuspendLayout();
            this.uiPanel4Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_GiayPhep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel5)).BeginInit();
            this.uiPanel5.SuspendLayout();
            this.uiPanel5Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_HoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel6)).BeginInit();
            this.uiPanel6.SuspendLayout();
            this.uiPanel6Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_ChungTu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel7)).BeginInit();
            this.uiPanel7.SuspendLayout();
            this.uiPanel7Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACC_KetXuatDuLieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel8)).BeginInit();
            this.uiPanel8.SuspendLayout();
            this.uiPanel8Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarKhoKeToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).BeginInit();
            this.pnlSXXK.SuspendLayout();
            this.pnlSXXKContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).BeginInit();
            this.pnlGiaCong.SuspendLayout();
            this.pnlGiaCongContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).BeginInit();
            this.pnlKinhDoanh.SuspendLayout();
            this.pnlKinhDoanhContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).BeginInit();
            this.pnlDauTu.SuspendLayout();
            this.pnlDauTuContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).BeginInit();
            this.pnlSend.SuspendLayout();
            this.pnlSendContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.SystemColors.Control;
            this.grbMain.Location = new System.Drawing.Point(253, 29);
            this.grbMain.Size = new System.Drawing.Size(591, 499);
            this.grbMain.Visible = false;
            this.grbMain.Click += new System.EventHandler(this.grbMain_Click_1);
            // 
            // vsmMain
            // 
            this.vsmMain.DefaultColorScheme = "Office2007";
            // 
            // cmMain
            // 
            this.cmMain.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowMerge = false;
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong,
            this.cmdThoat,
            this.cmdLoaiHinh,
            this.cmdReceiveAll,
            this.NhacNho,
            this.DongBoDuLieu,
            this.cmdImport,
            this.cmdImportNPL,
            this.cmdImportSP,
            this.cmdImportDM,
            this.cmdImportTTDM,
            this.cmdImportToKhai,
            this.cmdImportHangHoa,
            this.Command1,
            this.cmd2007,
            this.cmd2003,
            this.Command0,
            this.cmdHelp,
            this.cmdAbout,
            this.cmdNPLNhapTon,
            this.cmdDanhMuc,
            this.cmdMaHS,
            this.cmdNuoc,
            this.cmdHaiQuan,
            this.cmdNguyenTe,
            this.cmdDVT,
            this.cmdPTTT,
            this.cmdPTVT,
            this.cmdDKGH,
            this.cmdCuaKhau,
            this.cmdBackUp,
            this.cmdRestore,
            this.ThongSoKetNoi,
            this.TLThongTinDNHQ,
            this.cmdThietLapIn,
            this.cmdExportExccel,
            this.cmdImportExcel,
            this.cmdDongBoPhongKhai,
            this.cmdCauHinh,
            this.cmdCauHinhToKhai,
            this.QuanTri,
            this.QuanLyNguoiDung,
            this.QuanLyNhom,
            this.LoginUser,
            this.cmdChangePass,
            this.TraCuuMaHS,
            this.cmdAutoUpdate,
            this.NhapXML,
            this.NhapHDXML,
            this.NhapDMXML,
            this.NhapPKXML,
            this.XuatTKXML,
            this.XuatTKMD,
            this.XuatTKGCCT,
            this.NhapTKXML,
            this.NhapTKMD,
            this.NhapTKGCCT,
            this.cmdEnglish,
            this.cmdVN,
            this.cmdNhapXuat,
            this.cmdXuatDuLieuPhongKHai,
            this.cmdXuatHopDong,
            this.cmdXuatDinhMuc,
            this.cmdXuatPhuKien,
            this.cmdXuatToKhai,
            this.cmdActivate,
            this.cmdDMSPGC,
            this.cmdNhapDuLieuDaiLy,
            this.cmdCloseMe,
            this.cmdCloseAllButMe,
            this.cmdCloseAll,
            this.cmdXuatDN,
            this.QuanLyMess,
            this.cmdNhapDuLieuTuDoangNghiep,
            this.cmdNhapHopDong,
            this.cmdNhapDinhMuc,
            this.cmdNhapPhuKien,
            this.cmdNhapToKhai,
            this.cmdNhapToKhaiMauDich,
            this.cmdNhapToKhaiGCCT,
            this.cmdTLTTDN,
            this.mnuQuerySQL,
            this.cmdLog,
            this.cmdDataVersion,
            this.cmdChuKySo,
            this.cmdTimer,
            this.cmdNhomCuaKhau,
            this.cmdGetCategoryOnline,
            this.cmdBieuThue,
            this.cmdTraCuuXNKOnline,
            this.cmdTraCuuNoThueOnline,
            this.cmdTraCuuVanBanOnline,
            this.cmdTuVanHQOnline,
            this.cmdTeamview,
            this.cmdCapNhatHS,
            this.cmdCapNhatHS8Auto,
            this.cmdCapNhatHS8SoManual,
            this.cmdTool,
            this.cmdImageResizeHelp,
            this.cmdDaily,
            this.cmdUpdateDatabase,
            this.cmdHelpVideo,
            this.cmdHDSDCKS,
            this.cmdHDSDVNACCS,
            this.cmdThongBaoVNACCS,
            this.cmdSignFile,
            this.cmdHelpSignFile,
            this.cmdBerth,
            this.cmdCargo,
            this.cmdCityUNLOCODE,
            this.cmdCommon,
            this.cmdContainerSize,
            this.cmdCustomsSubSection,
            this.cmdOGAUser,
            this.cmdPackagesUnit,
            this.cmdStations,
            this.cmdTaxClassificationCode,
            this.cmdReloadData,
            this.cmdBieuThueXNK2018,
            this.cmdUpdateCategoryOnline,
            this.cmdNhanPhanHoi,
            this.cmdUpdateCertificates,
            this.cmdConfigReadExcel,
            this.cmdPerformanceDatabase});
            this.cmMain.ContainerControl = this;
            this.cmMain.ContextMenus.AddRange(new Janus.Windows.UI.CommandBars.UIContextMenu[] {
            this.mnuRightClick});
            this.cmMain.Id = new System.Guid("eae49f54-3bfa-4a6a-8b46-89b443ba80cd");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbMenu
            // 
            this.cmbMenu.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowMerge = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.CommandBarType = Janus.Windows.UI.CommandBars.CommandBarType.Menu;
            this.cmbMenu.CommandManager = this.cmMain;
            this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong1,
            this.cmdDanhMuc1,
            this.cmdBieuThue1,
            this.QuanTri1,
            this.Command11,
            this.Command01,
            this.DongBoDuLieu1});
            this.cmbMenu.Key = "cmbMenu";
            this.cmbMenu.Location = new System.Drawing.Point(0, 0);
            this.cmbMenu.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbMenu.MergeRowOrder = 0;
            this.cmbMenu.Name = "cmbMenu";
            this.cmbMenu.RowIndex = 0;
            this.cmbMenu.Size = new System.Drawing.Size(847, 26);
            this.cmbMenu.Text = "cmbMenu";
            this.cmbMenu.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmbMenu.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMenu_CommandClick);
            // 
            // cmdHeThong1
            // 
            this.cmdHeThong1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHeThong1.Image")));
            this.cmdHeThong1.Key = "cmdHeThong";
            this.cmdHeThong1.Name = "cmdHeThong1";
            this.cmdHeThong1.Text = "&Hệ thống";
            // 
            // cmdDanhMuc1
            // 
            this.cmdDanhMuc1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDanhMuc1.Image")));
            this.cmdDanhMuc1.Key = "cmdDanhMuc";
            this.cmdDanhMuc1.Name = "cmdDanhMuc1";
            this.cmdDanhMuc1.Text = "&Danh mục";
            // 
            // cmdBieuThue1
            // 
            this.cmdBieuThue1.Image = ((System.Drawing.Image)(resources.GetObject("cmdBieuThue1.Image")));
            this.cmdBieuThue1.Key = "cmdBieuThue";
            this.cmdBieuThue1.Name = "cmdBieuThue1";
            // 
            // QuanTri1
            // 
            this.QuanTri1.Image = ((System.Drawing.Image)(resources.GetObject("QuanTri1.Image")));
            this.QuanTri1.Key = "QuanTri";
            this.QuanTri1.Name = "QuanTri1";
            this.QuanTri1.Text = "&Quản trị";
            // 
            // Command11
            // 
            this.Command11.Image = ((System.Drawing.Image)(resources.GetObject("Command11.Image")));
            this.Command11.Key = "Command1";
            this.Command11.Name = "Command11";
            this.Command11.Text = "&Giao diện";
            // 
            // Command01
            // 
            this.Command01.Image = ((System.Drawing.Image)(resources.GetObject("Command01.Image")));
            this.Command01.Key = "Command0";
            this.Command01.Name = "Command01";
            this.Command01.Text = "&Trợ giúp";
            // 
            // DongBoDuLieu1
            // 
            this.DongBoDuLieu1.Icon = ((System.Drawing.Icon)(resources.GetObject("DongBoDuLieu1.Icon")));
            this.DongBoDuLieu1.Key = "DongBoDuLieu";
            this.DongBoDuLieu1.Name = "DongBoDuLieu1";
            // 
            // cmdHeThong
            // 
            this.cmdHeThong.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdDataVersion1,
            this.cmdBackUp1,
            this.cmdUpdateDatabase1,
            this.Separator3,
            this.cmdCapNhatHS1,
            this.Separator10,
            this.cmdNhapXuat2,
            this.Separator11,
            this.cmdTLTTDN1,
            this.cmdCauHinh1,
            this.mnuQuerySQL1,
            this.cmdLog1,
            this.cmdPerformanceDatabase1,
            this.Separator4,
            this.cmdDaily1,
            this.Separator13,
            this.LoginUser1,
            this.cmdChangePass1,
            this.Separator7,
            this.cmdNhanPhanHoi1,
            this.cmdThoat2});
            this.cmdHeThong.Image = ((System.Drawing.Image)(resources.GetObject("cmdHeThong.Image")));
            this.cmdHeThong.Key = "cmdHeThong";
            this.cmdHeThong.Name = "cmdHeThong";
            this.cmdHeThong.Text = "&Hệ thống";
            // 
            // cmdDataVersion1
            // 
            this.cmdDataVersion1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDataVersion1.Image")));
            this.cmdDataVersion1.Key = "cmdDataVersion";
            this.cmdDataVersion1.Name = "cmdDataVersion1";
            // 
            // cmdBackUp1
            // 
            this.cmdBackUp1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBackUp1.Icon")));
            this.cmdBackUp1.Image = ((System.Drawing.Image)(resources.GetObject("cmdBackUp1.Image")));
            this.cmdBackUp1.Key = "cmdBackUp";
            this.cmdBackUp1.Name = "cmdBackUp1";
            // 
            // cmdUpdateDatabase1
            // 
            this.cmdUpdateDatabase1.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateDatabase1.Image")));
            this.cmdUpdateDatabase1.Key = "cmdUpdateDatabase";
            this.cmdUpdateDatabase1.Name = "cmdUpdateDatabase1";
            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // cmdCapNhatHS1
            // 
            this.cmdCapNhatHS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCapNhatHS1.Image")));
            this.cmdCapNhatHS1.Key = "cmdCapNhatHS";
            this.cmdCapNhatHS1.Name = "cmdCapNhatHS1";
            // 
            // Separator10
            // 
            this.Separator10.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator10.Key = "Separator";
            this.Separator10.Name = "Separator10";
            // 
            // cmdNhapXuat2
            // 
            this.cmdNhapXuat2.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapXuat2.Image")));
            this.cmdNhapXuat2.ImageIndex = 20;
            this.cmdNhapXuat2.Key = "cmdNhapXuat";
            this.cmdNhapXuat2.Name = "cmdNhapXuat2";
            // 
            // Separator11
            // 
            this.Separator11.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator11.Key = "Separator";
            this.Separator11.Name = "Separator11";
            // 
            // cmdTLTTDN1
            // 
            this.cmdTLTTDN1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTLTTDN1.Image")));
            this.cmdTLTTDN1.ImageIndex = 6;
            this.cmdTLTTDN1.Key = "cmdTLTTDN";
            this.cmdTLTTDN1.Name = "cmdTLTTDN1";
            // 
            // cmdCauHinh1
            // 
            this.cmdCauHinh1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCauHinh1.Image")));
            this.cmdCauHinh1.Key = "cmdCauHinh";
            this.cmdCauHinh1.Name = "cmdCauHinh1";
            // 
            // mnuQuerySQL1
            // 
            this.mnuQuerySQL1.Image = ((System.Drawing.Image)(resources.GetObject("mnuQuerySQL1.Image")));
            this.mnuQuerySQL1.Key = "mnuQuerySQL";
            this.mnuQuerySQL1.Name = "mnuQuerySQL1";
            // 
            // cmdLog1
            // 
            this.cmdLog1.Image = ((System.Drawing.Image)(resources.GetObject("cmdLog1.Image")));
            this.cmdLog1.ImageIndex = 7;
            this.cmdLog1.Key = "cmdLog";
            this.cmdLog1.Name = "cmdLog1";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdDaily1
            // 
            this.cmdDaily1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDaily1.Image")));
            this.cmdDaily1.Key = "cmdDaily";
            this.cmdDaily1.Name = "cmdDaily1";
            // 
            // Separator13
            // 
            this.Separator13.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator13.Key = "Separator";
            this.Separator13.Name = "Separator13";
            // 
            // LoginUser1
            // 
            this.LoginUser1.Icon = ((System.Drawing.Icon)(resources.GetObject("LoginUser1.Icon")));
            this.LoginUser1.Image = ((System.Drawing.Image)(resources.GetObject("LoginUser1.Image")));
            this.LoginUser1.Key = "LoginUser";
            this.LoginUser1.Name = "LoginUser1";
            // 
            // cmdChangePass1
            // 
            this.cmdChangePass1.Image = ((System.Drawing.Image)(resources.GetObject("cmdChangePass1.Image")));
            this.cmdChangePass1.Key = "cmdChangePass";
            this.cmdChangePass1.Name = "cmdChangePass1";
            // 
            // Separator7
            // 
            this.Separator7.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator7.Key = "Separator";
            this.Separator7.Name = "Separator7";
            // 
            // cmdNhanPhanHoi1
            // 
            this.cmdNhanPhanHoi1.Key = "cmdNhanPhanHoi";
            this.cmdNhanPhanHoi1.Name = "cmdNhanPhanHoi1";
            // 
            // cmdThoat2
            // 
            this.cmdThoat2.Image = ((System.Drawing.Image)(resources.GetObject("cmdThoat2.Image")));
            this.cmdThoat2.Key = "cmdThoat";
            this.cmdThoat2.Name = "cmdThoat2";
            // 
            // cmdThoat
            // 
            this.cmdThoat.Key = "cmdThoat";
            this.cmdThoat.Name = "cmdThoat";
            this.cmdThoat.Text = "Thoát";
            // 
            // cmdLoaiHinh
            // 
            this.cmdLoaiHinh.Key = "cmdLoaiHinh";
            this.cmdLoaiHinh.Name = "cmdLoaiHinh";
            this.cmdLoaiHinh.Text = "Loại hình";
            // 
            // cmdReceiveAll
            // 
            this.cmdReceiveAll.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdReceiveAll.Icon")));
            this.cmdReceiveAll.Key = "cmdReceiveAll";
            this.cmdReceiveAll.Name = "cmdReceiveAll";
            this.cmdReceiveAll.Text = "Cập nhật thông tin";
            // 
            // NhacNho
            // 
            this.NhacNho.Key = "NhacNho";
            this.NhacNho.Name = "NhacNho";
            this.NhacNho.Text = "Nhắc nhở";
            // 
            // DongBoDuLieu
            // 
            this.DongBoDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("DongBoDuLieu.Icon")));
            this.DongBoDuLieu.Key = "DongBoDuLieu";
            this.DongBoDuLieu.Name = "DongBoDuLieu";
            this.DongBoDuLieu.Text = "Đồng bộ dữ liệu";
            // 
            // cmdImport
            // 
            this.cmdImport.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportNPL1,
            this.cmdImportSP1,
            this.cmdImportDM1,
            this.cmdImportToKhai1,
            this.cmdImportHangHoa1});
            this.cmdImport.Key = "cmdImport";
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Text = "Import from Excel";
            // 
            // cmdImportNPL1
            // 
            this.cmdImportNPL1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportNPL1.Icon")));
            this.cmdImportNPL1.Key = "cmdImportNPL";
            this.cmdImportNPL1.Name = "cmdImportNPL1";
            this.cmdImportNPL1.Text = "Nguyên phụ liệu";
            // 
            // cmdImportSP1
            // 
            this.cmdImportSP1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportSP1.Icon")));
            this.cmdImportSP1.Key = "cmdImportSP";
            this.cmdImportSP1.Name = "cmdImportSP1";
            this.cmdImportSP1.Text = "Sản phẩm";
            // 
            // cmdImportDM1
            // 
            this.cmdImportDM1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportDM1.Icon")));
            this.cmdImportDM1.Key = "cmdImportDM";
            this.cmdImportDM1.Name = "cmdImportDM1";
            this.cmdImportDM1.Text = "Định mức";
            // 
            // cmdImportToKhai1
            // 
            this.cmdImportToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportToKhai1.Icon")));
            this.cmdImportToKhai1.Key = "cmdImportToKhai";
            this.cmdImportToKhai1.Name = "cmdImportToKhai1";
            this.cmdImportToKhai1.Text = "Tờ khai";
            // 
            // cmdImportHangHoa1
            // 
            this.cmdImportHangHoa1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportHangHoa1.Icon")));
            this.cmdImportHangHoa1.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa1.Name = "cmdImportHangHoa1";
            this.cmdImportHangHoa1.Text = "Hàng của tờ khai";
            // 
            // cmdImportNPL
            // 
            this.cmdImportNPL.Key = "cmdImportNPL";
            this.cmdImportNPL.Name = "cmdImportNPL";
            this.cmdImportNPL.Text = "Import NPL";
            // 
            // cmdImportSP
            // 
            this.cmdImportSP.Key = "cmdImportSP";
            this.cmdImportSP.Name = "cmdImportSP";
            this.cmdImportSP.Text = "Import Sản phẩm";
            // 
            // cmdImportDM
            // 
            this.cmdImportDM.Key = "cmdImportDM";
            this.cmdImportDM.Name = "cmdImportDM";
            this.cmdImportDM.Text = "Import Định mức";
            // 
            // cmdImportTTDM
            // 
            this.cmdImportTTDM.Key = "cmdImportTTDM";
            this.cmdImportTTDM.Name = "cmdImportTTDM";
            this.cmdImportTTDM.Text = "Import thông tin định mức";
            // 
            // cmdImportToKhai
            // 
            this.cmdImportToKhai.Key = "cmdImportToKhai";
            this.cmdImportToKhai.Name = "cmdImportToKhai";
            this.cmdImportToKhai.Text = "Import tờ khai";
            // 
            // cmdImportHangHoa
            // 
            this.cmdImportHangHoa.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa.Name = "cmdImportHangHoa";
            this.cmdImportHangHoa.Text = "Import hàng tờ khai";
            // 
            // Command1
            // 
            this.Command1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmd20071,
            this.cmd20031,
            this.cmdVN1,
            this.cmdEng1});
            this.Command1.Key = "Command1";
            this.Command1.Name = "Command1";
            this.Command1.Text = "Giao diện";
            // 
            // cmd20071
            // 
            this.cmd20071.Image = ((System.Drawing.Image)(resources.GetObject("cmd20071.Image")));
            this.cmd20071.Key = "cmd2007";
            this.cmd20071.Name = "cmd20071";
            // 
            // cmd20031
            // 
            this.cmd20031.Image = ((System.Drawing.Image)(resources.GetObject("cmd20031.Image")));
            this.cmd20031.Key = "cmd2003";
            this.cmd20031.Name = "cmd20031";
            // 
            // cmdVN1
            // 
            this.cmdVN1.Image = ((System.Drawing.Image)(resources.GetObject("cmdVN1.Image")));
            this.cmdVN1.ImageIndex = 11;
            this.cmdVN1.Key = "cmdVN";
            this.cmdVN1.Name = "cmdVN1";
            // 
            // cmdEng1
            // 
            this.cmdEng1.Image = ((System.Drawing.Image)(resources.GetObject("cmdEng1.Image")));
            this.cmdEng1.ImageIndex = 10;
            this.cmdEng1.Key = "cmdEng";
            this.cmdEng1.Name = "cmdEng1";
            // 
            // cmd2007
            // 
            this.cmd2007.Key = "cmd2007";
            this.cmd2007.Name = "cmd2007";
            this.cmd2007.Text = "Office 2007";
            // 
            // cmd2003
            // 
            this.cmd2003.Key = "cmd2003";
            this.cmd2003.Name = "cmd2003";
            this.cmd2003.Text = "Office 2003";
            // 
            // Command0
            // 
            this.Command0.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHelp1,
            this.cmdHelpVideo1,
            this.cmdHDSDCKS1,
            this.cmdHDSDVNACCS1,
            this.Separator1,
            this.cmdAutoUpdate1,
            this.Separator9,
            this.cmdTeamview1,
            this.cmdTool1,
            this.Separator12,
            this.cmdActivate1,
            this.cmdAbout1,
            this.cmdThongBaoVNACCS1,
            this.cmdUpdateCertificates1});
            this.Command0.Key = "Command0";
            this.Command0.Name = "Command0";
            this.Command0.Text = "Trợ giúp";
            // 
            // cmdHelp1
            // 
            this.cmdHelp1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHelp1.Image")));
            this.cmdHelp1.Key = "cmdHelp";
            this.cmdHelp1.Name = "cmdHelp1";
            this.cmdHelp1.Text = "&Hướng dẫn sử dụng";
            // 
            // cmdHelpVideo1
            // 
            this.cmdHelpVideo1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHelpVideo1.Image")));
            this.cmdHelpVideo1.Key = "cmdHelpVideo";
            this.cmdHelpVideo1.Name = "cmdHelpVideo1";
            // 
            // cmdHDSDCKS1
            // 
            this.cmdHDSDCKS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHDSDCKS1.Image")));
            this.cmdHDSDCKS1.Key = "cmdHDSDCKS";
            this.cmdHDSDCKS1.Name = "cmdHDSDCKS1";
            // 
            // cmdHDSDVNACCS1
            // 
            this.cmdHDSDVNACCS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHDSDVNACCS1.Image")));
            this.cmdHDSDVNACCS1.Key = "cmdHDSDVNACCS";
            this.cmdHDSDVNACCS1.Name = "cmdHDSDVNACCS1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdAutoUpdate1
            // 
            this.cmdAutoUpdate1.Image = ((System.Drawing.Image)(resources.GetObject("cmdAutoUpdate1.Image")));
            this.cmdAutoUpdate1.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate1.Name = "cmdAutoUpdate1";
            this.cmdAutoUpdate1.Text = "&Cập nhật chương trình";
            // 
            // Separator9
            // 
            this.Separator9.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator9.Key = "Separator";
            this.Separator9.Name = "Separator9";
            // 
            // cmdTeamview1
            // 
            this.cmdTeamview1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTeamview1.Image")));
            this.cmdTeamview1.Key = "cmdTeamview";
            this.cmdTeamview1.Name = "cmdTeamview1";
            // 
            // cmdTool1
            // 
            this.cmdTool1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTool1.Image")));
            this.cmdTool1.Key = "cmdTool";
            this.cmdTool1.Name = "cmdTool1";
            // 
            // Separator12
            // 
            this.Separator12.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator12.Key = "Separator";
            this.Separator12.Name = "Separator12";
            // 
            // cmdActivate1
            // 
            this.cmdActivate1.Image = ((System.Drawing.Image)(resources.GetObject("cmdActivate1.Image")));
            this.cmdActivate1.ImageIndex = 18;
            this.cmdActivate1.Key = "cmdActivate";
            this.cmdActivate1.Name = "cmdActivate1";
            // 
            // cmdAbout1
            // 
            this.cmdAbout1.Image = ((System.Drawing.Image)(resources.GetObject("cmdAbout1.Image")));
            this.cmdAbout1.Key = "cmdAbout";
            this.cmdAbout1.Name = "cmdAbout1";
            this.cmdAbout1.Text = "&Thông tin sản phẩm";
            // 
            // cmdThongBaoVNACCS1
            // 
            this.cmdThongBaoVNACCS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThongBaoVNACCS1.Image")));
            this.cmdThongBaoVNACCS1.Key = "cmdThongBaoVNACCS";
            this.cmdThongBaoVNACCS1.Name = "cmdThongBaoVNACCS1";
            // 
            // cmdUpdateCertificates1
            // 
            this.cmdUpdateCertificates1.Key = "cmdUpdateCertificates";
            this.cmdUpdateCertificates1.Name = "cmdUpdateCertificates1";
            // 
            // cmdHelp
            // 
            this.cmdHelp.Key = "cmdHelp";
            this.cmdHelp.Name = "cmdHelp";
            this.cmdHelp.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.cmdHelp.Text = "Hướng dẫn sử dụng";
            // 
            // cmdAbout
            // 
            this.cmdAbout.Key = "cmdAbout";
            this.cmdAbout.Name = "cmdAbout";
            this.cmdAbout.Text = "Thông tin sản phẩm";
            // 
            // cmdNPLNhapTon
            // 
            this.cmdNPLNhapTon.Key = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Name = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Text = "NPL nhập tồn";
            // 
            // cmdDanhMuc
            // 
            this.cmdDanhMuc.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdGetCategoryOnline1,
            this.Separator6,
            this.cmdHaiQuan1,
            this.cmdNuoc1,
            this.cmdMaHS1,
            this.cmdNguyenTe1,
            this.cmdDVT1,
            this.cmdPTTT1,
            this.cmdPTVT1,
            this.cmdDKGH1,
            this.cmdNhomCuaKhau1,
            this.cmdCuaKhau1,
            this.cmdDMSPGC1,
            this.cmdBerth1,
            this.cmdCargo1,
            this.cmdCityUNLOCODE1,
            this.cmdCommon1,
            this.cmdContainerSize1,
            this.cmdCustomsSubSection1,
            this.cmdOGAUser1,
            this.cmdStations1,
            this.cmdTaxClassificationCode1,
            this.cmdReloadData1,
            this.cmdUpdateCategoryOnline1});
            this.cmdDanhMuc.Key = "cmdDanhMuc";
            this.cmdDanhMuc.Name = "cmdDanhMuc";
            this.cmdDanhMuc.Text = "DanhMuc";
            // 
            // cmdGetCategoryOnline1
            // 
            this.cmdGetCategoryOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdGetCategoryOnline1.Image")));
            this.cmdGetCategoryOnline1.Key = "cmdGetCategoryOnline";
            this.cmdGetCategoryOnline1.Name = "cmdGetCategoryOnline1";
            this.cmdGetCategoryOnline1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // Separator6
            // 
            this.Separator6.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator6.Key = "Separator";
            this.Separator6.Name = "Separator6";
            // 
            // cmdHaiQuan1
            // 
            this.cmdHaiQuan1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHaiQuan1.Image")));
            this.cmdHaiQuan1.ImageIndex = 5;
            this.cmdHaiQuan1.Key = "cmdHaiQuan";
            this.cmdHaiQuan1.Name = "cmdHaiQuan1";
            // 
            // cmdNuoc1
            // 
            this.cmdNuoc1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNuoc1.Image")));
            this.cmdNuoc1.ImageIndex = 5;
            this.cmdNuoc1.Key = "cmdNuoc";
            this.cmdNuoc1.Name = "cmdNuoc1";
            // 
            // cmdMaHS1
            // 
            this.cmdMaHS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdMaHS1.Image")));
            this.cmdMaHS1.ImageIndex = 5;
            this.cmdMaHS1.Key = "cmdMaHS";
            this.cmdMaHS1.Name = "cmdMaHS1";
            // 
            // cmdNguyenTe1
            // 
            this.cmdNguyenTe1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNguyenTe1.Image")));
            this.cmdNguyenTe1.ImageIndex = 5;
            this.cmdNguyenTe1.Key = "cmdNguyenTe";
            this.cmdNguyenTe1.Name = "cmdNguyenTe1";
            // 
            // cmdDVT1
            // 
            this.cmdDVT1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDVT1.Image")));
            this.cmdDVT1.ImageIndex = 5;
            this.cmdDVT1.Key = "cmdDVT";
            this.cmdDVT1.Name = "cmdDVT1";
            // 
            // cmdPTTT1
            // 
            this.cmdPTTT1.Image = ((System.Drawing.Image)(resources.GetObject("cmdPTTT1.Image")));
            this.cmdPTTT1.ImageIndex = 5;
            this.cmdPTTT1.Key = "cmdPTTT";
            this.cmdPTTT1.Name = "cmdPTTT1";
            // 
            // cmdPTVT1
            // 
            this.cmdPTVT1.Image = ((System.Drawing.Image)(resources.GetObject("cmdPTVT1.Image")));
            this.cmdPTVT1.ImageIndex = 5;
            this.cmdPTVT1.Key = "cmdPTVT";
            this.cmdPTVT1.Name = "cmdPTVT1";
            // 
            // cmdDKGH1
            // 
            this.cmdDKGH1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDKGH1.Image")));
            this.cmdDKGH1.ImageIndex = 5;
            this.cmdDKGH1.Key = "cmdDKGH";
            this.cmdDKGH1.Name = "cmdDKGH1";
            // 
            // cmdNhomCuaKhau1
            // 
            this.cmdNhomCuaKhau1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhomCuaKhau1.Image")));
            this.cmdNhomCuaKhau1.Key = "cmdNhomCuaKhau";
            this.cmdNhomCuaKhau1.Name = "cmdNhomCuaKhau1";
            // 
            // cmdCuaKhau1
            // 
            this.cmdCuaKhau1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCuaKhau1.Image")));
            this.cmdCuaKhau1.ImageIndex = 5;
            this.cmdCuaKhau1.Key = "cmdCuaKhau";
            this.cmdCuaKhau1.Name = "cmdCuaKhau1";
            // 
            // cmdDMSPGC1
            // 
            this.cmdDMSPGC1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDMSPGC1.Image")));
            this.cmdDMSPGC1.ImageIndex = 5;
            this.cmdDMSPGC1.Key = "cmdDMSPGC";
            this.cmdDMSPGC1.Name = "cmdDMSPGC1";
            this.cmdDMSPGC1.Text = "Sản phẩm gia công";
            // 
            // cmdBerth1
            // 
            this.cmdBerth1.Key = "cmdBerth";
            this.cmdBerth1.Name = "cmdBerth1";
            // 
            // cmdCargo1
            // 
            this.cmdCargo1.Key = "cmdCargo";
            this.cmdCargo1.Name = "cmdCargo1";
            // 
            // cmdCityUNLOCODE1
            // 
            this.cmdCityUNLOCODE1.Key = "cmdCityUNLOCODE";
            this.cmdCityUNLOCODE1.Name = "cmdCityUNLOCODE1";
            // 
            // cmdCommon1
            // 
            this.cmdCommon1.Key = "cmdCommon";
            this.cmdCommon1.Name = "cmdCommon1";
            // 
            // cmdContainerSize1
            // 
            this.cmdContainerSize1.Key = "cmdContainerSize";
            this.cmdContainerSize1.Name = "cmdContainerSize1";
            // 
            // cmdCustomsSubSection1
            // 
            this.cmdCustomsSubSection1.Key = "cmdCustomsSubSection";
            this.cmdCustomsSubSection1.Name = "cmdCustomsSubSection1";
            // 
            // cmdOGAUser1
            // 
            this.cmdOGAUser1.Key = "cmdOGAUser";
            this.cmdOGAUser1.Name = "cmdOGAUser1";
            // 
            // cmdStations1
            // 
            this.cmdStations1.Key = "cmdStations";
            this.cmdStations1.Name = "cmdStations1";
            // 
            // cmdTaxClassificationCode1
            // 
            this.cmdTaxClassificationCode1.Key = "cmdTaxClassificationCode";
            this.cmdTaxClassificationCode1.Name = "cmdTaxClassificationCode1";
            // 
            // cmdReloadData1
            // 
            this.cmdReloadData1.Key = "cmdReloadData";
            this.cmdReloadData1.Name = "cmdReloadData1";
            // 
            // cmdUpdateCategoryOnline1
            // 
            this.cmdUpdateCategoryOnline1.Key = "cmdUpdateCategoryOnline";
            this.cmdUpdateCategoryOnline1.Name = "cmdUpdateCategoryOnline1";
            // 
            // cmdMaHS
            // 
            this.cmdMaHS.Key = "cmdMaHS";
            this.cmdMaHS.Name = "cmdMaHS";
            this.cmdMaHS.Text = "Mã HS";
            // 
            // cmdNuoc
            // 
            this.cmdNuoc.Key = "cmdNuoc";
            this.cmdNuoc.Name = "cmdNuoc";
            this.cmdNuoc.Text = "Nước";
            // 
            // cmdHaiQuan
            // 
            this.cmdHaiQuan.Key = "cmdHaiQuan";
            this.cmdHaiQuan.Name = "cmdHaiQuan";
            this.cmdHaiQuan.Text = "Đơn vị Hải quan";
            // 
            // cmdNguyenTe
            // 
            this.cmdNguyenTe.Key = "cmdNguyenTe";
            this.cmdNguyenTe.Name = "cmdNguyenTe";
            this.cmdNguyenTe.Text = "Nguyên tệ";
            // 
            // cmdDVT
            // 
            this.cmdDVT.Key = "cmdDVT";
            this.cmdDVT.Name = "cmdDVT";
            this.cmdDVT.Text = "Đơn vị tính";
            // 
            // cmdPTTT
            // 
            this.cmdPTTT.Key = "cmdPTTT";
            this.cmdPTTT.Name = "cmdPTTT";
            this.cmdPTTT.Text = "Phương thức thanh toán";
            // 
            // cmdPTVT
            // 
            this.cmdPTVT.Key = "cmdPTVT";
            this.cmdPTVT.Name = "cmdPTVT";
            this.cmdPTVT.Text = "Phương thức vận tải";
            // 
            // cmdDKGH
            // 
            this.cmdDKGH.Key = "cmdDKGH";
            this.cmdDKGH.Name = "cmdDKGH";
            this.cmdDKGH.Text = "Điều kiện giao hàng";
            // 
            // cmdCuaKhau
            // 
            this.cmdCuaKhau.Key = "cmdCuaKhau";
            this.cmdCuaKhau.Name = "cmdCuaKhau";
            this.cmdCuaKhau.Text = "Cửa khẩu";
            // 
            // cmdBackUp
            // 
            this.cmdBackUp.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBackUp.Icon")));
            this.cmdBackUp.Key = "cmdBackUp";
            this.cmdBackUp.Name = "cmdBackUp";
            this.cmdBackUp.Text = "Sao lưu dữ liệu";
            // 
            // cmdRestore
            // 
            this.cmdRestore.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdRestore.Icon")));
            this.cmdRestore.Key = "cmdRestore";
            this.cmdRestore.Name = "cmdRestore";
            this.cmdRestore.Text = "Phục hồi dữ liệu";
            // 
            // ThongSoKetNoi
            // 
            this.ThongSoKetNoi.Icon = ((System.Drawing.Icon)(resources.GetObject("ThongSoKetNoi.Icon")));
            this.ThongSoKetNoi.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi.Name = "ThongSoKetNoi";
            this.ThongSoKetNoi.Text = "Thiết lập thông số kết nối";
            // 
            // TLThongTinDNHQ
            // 
            this.TLThongTinDNHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("TLThongTinDNHQ.Icon")));
            this.TLThongTinDNHQ.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Name = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Text = "Thiết lập thông tin doanh nghiệp và hải quan";
            // 
            // cmdThietLapIn
            // 
            this.cmdThietLapIn.Key = "cmdThietLapIn";
            this.cmdThietLapIn.Name = "cmdThietLapIn";
            this.cmdThietLapIn.Text = "Thiết lập thông số in báo cáo";
            // 
            // cmdExportExccel
            // 
            this.cmdExportExccel.Key = "cmdExportExccel";
            this.cmdExportExccel.Name = "cmdExportExccel";
            this.cmdExportExccel.Text = "Xuất dữ liệu ra file Excel";
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Key = "cmdImportExcel";
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Text = "Nhập dữ liêu từ file Excel";
            // 
            // cmdDongBoPhongKhai
            // 
            this.cmdDongBoPhongKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportExcel1,
            this.cmdExportExccel1});
            this.cmdDongBoPhongKhai.Key = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Name = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Text = "Đồng bộ dữ liệu với phòng khai";
            // 
            // cmdImportExcel1
            // 
            this.cmdImportExcel1.Key = "cmdImportExcel";
            this.cmdImportExcel1.Name = "cmdImportExcel1";
            // 
            // cmdExportExccel1
            // 
            this.cmdExportExccel1.Key = "cmdExportExccel";
            this.cmdExportExccel1.Name = "cmdExportExccel1";
            // 
            // cmdCauHinh
            // 
            this.cmdCauHinh.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThongSoKetNoi1,
            this.TLThongTinDNHQ1,
            this.cmdCauHinhToKhai1,
            this.cmdThietLapIn1,
            this.cmdChuKySo1,
            this.cmdTimer1,
            this.cmdConfigReadExcel1});
            this.cmdCauHinh.Key = "cmdCauHinh";
            this.cmdCauHinh.Name = "cmdCauHinh";
            this.cmdCauHinh.Text = "Cấu hình hệ thống";
            // 
            // ThongSoKetNoi1
            // 
            this.ThongSoKetNoi1.Icon = ((System.Drawing.Icon)(resources.GetObject("ThongSoKetNoi1.Icon")));
            this.ThongSoKetNoi1.Image = ((System.Drawing.Image)(resources.GetObject("ThongSoKetNoi1.Image")));
            this.ThongSoKetNoi1.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi1.Name = "ThongSoKetNoi1";
            this.ThongSoKetNoi1.Text = "Cấu hình thông số kết nối";
            // 
            // TLThongTinDNHQ1
            // 
            this.TLThongTinDNHQ1.Icon = ((System.Drawing.Icon)(resources.GetObject("TLThongTinDNHQ1.Icon")));
            this.TLThongTinDNHQ1.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ1.Name = "TLThongTinDNHQ1";
            this.TLThongTinDNHQ1.Text = "Cấu hình thông tin doanh nghiệp và hải quan";
            // 
            // cmdCauHinhToKhai1
            // 
            this.cmdCauHinhToKhai1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCauHinhToKhai1.Image")));
            this.cmdCauHinhToKhai1.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai1.Name = "cmdCauHinhToKhai1";
            // 
            // cmdThietLapIn1
            // 
            this.cmdThietLapIn1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThietLapIn1.Image")));
            this.cmdThietLapIn1.Key = "cmdThietLapIn";
            this.cmdThietLapIn1.Name = "cmdThietLapIn1";
            this.cmdThietLapIn1.Text = "Cấu hình thông số in báo cáo";
            // 
            // cmdChuKySo1
            // 
            this.cmdChuKySo1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChuKySo1.Icon")));
            this.cmdChuKySo1.Key = "cmdChuKySo";
            this.cmdChuKySo1.Name = "cmdChuKySo1";
            // 
            // cmdTimer1
            // 
            this.cmdTimer1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTimer1.Icon")));
            this.cmdTimer1.Key = "cmdTimer";
            this.cmdTimer1.Name = "cmdTimer1";
            // 
            // cmdConfigReadExcel1
            // 
            this.cmdConfigReadExcel1.Key = "cmdConfigReadExcel";
            this.cmdConfigReadExcel1.Name = "cmdConfigReadExcel1";
            // 
            // cmdCauHinhToKhai
            // 
            this.cmdCauHinhToKhai.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Name = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Text = "Cấu hình tham số mặc định của tờ khai";
            // 
            // QuanTri
            // 
            this.QuanTri.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.QuanLyNguoiDung1,
            this.QuanLyNhom1});
            this.QuanTri.Key = "QuanTri";
            this.QuanTri.Name = "QuanTri";
            this.QuanTri.Text = "Quản trị";
            // 
            // QuanLyNguoiDung1
            // 
            this.QuanLyNguoiDung1.Image = ((System.Drawing.Image)(resources.GetObject("QuanLyNguoiDung1.Image")));
            this.QuanLyNguoiDung1.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung1.Name = "QuanLyNguoiDung1";
            // 
            // QuanLyNhom1
            // 
            this.QuanLyNhom1.Image = ((System.Drawing.Image)(resources.GetObject("QuanLyNhom1.Image")));
            this.QuanLyNhom1.Key = "QuanLyNhom";
            this.QuanLyNhom1.Name = "QuanLyNhom1";
            // 
            // QuanLyNguoiDung
            // 
            this.QuanLyNguoiDung.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Name = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Text = "Quản lý người dùng";
            // 
            // QuanLyNhom
            // 
            this.QuanLyNhom.Key = "QuanLyNhom";
            this.QuanLyNhom.Name = "QuanLyNhom";
            this.QuanLyNhom.Text = "Quản lý nhóm người dùng";
            // 
            // LoginUser
            // 
            this.LoginUser.Icon = ((System.Drawing.Icon)(resources.GetObject("LoginUser.Icon")));
            this.LoginUser.Key = "LoginUser";
            this.LoginUser.Name = "LoginUser";
            this.LoginUser.Text = "Đăng nhập người dùng khác";
            // 
            // cmdChangePass
            // 
            this.cmdChangePass.Key = "cmdChangePass";
            this.cmdChangePass.Name = "cmdChangePass";
            this.cmdChangePass.Text = "Đổi mật khẩu";
            // 
            // TraCuuMaHS
            // 
            this.TraCuuMaHS.ImageIndex = 18;
            this.TraCuuMaHS.Key = "TraCuuMaHS";
            this.TraCuuMaHS.Name = "TraCuuMaHS";
            this.TraCuuMaHS.Text = "Tra cứu biểu thuế (Mã HS)";
            // 
            // cmdAutoUpdate
            // 
            this.cmdAutoUpdate.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate.Name = "cmdAutoUpdate";
            this.cmdAutoUpdate.Text = "Cập nhật chương trình";
            // 
            // NhapXML
            // 
            this.NhapXML.Key = "NhapXML";
            this.NhapXML.Name = "NhapXML";
            this.NhapXML.Text = "Nhập dữ liệu từ đại lý";
            // 
            // NhapHDXML
            // 
            this.NhapHDXML.Key = "NhapHDXML";
            this.NhapHDXML.Name = "NhapHDXML";
            this.NhapHDXML.Text = "Hợp đồng";
            // 
            // NhapDMXML
            // 
            this.NhapDMXML.Key = "NhapDMXML";
            this.NhapDMXML.Name = "NhapDMXML";
            this.NhapDMXML.Text = "Định mức";
            // 
            // NhapPKXML
            // 
            this.NhapPKXML.Key = "NhapPKXML";
            this.NhapPKXML.Name = "NhapPKXML";
            this.NhapPKXML.Text = "Phụ kiện";
            // 
            // XuatTKXML
            // 
            this.XuatTKXML.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.XuatTKMD1,
            this.XuatTKGCCT1});
            this.XuatTKXML.Key = "XuatTKXML";
            this.XuatTKXML.Name = "XuatTKXML";
            this.XuatTKXML.Text = "Tờ khai";
            // 
            // XuatTKMD1
            // 
            this.XuatTKMD1.Key = "XuatTKMD";
            this.XuatTKMD1.Name = "XuatTKMD1";
            // 
            // XuatTKGCCT1
            // 
            this.XuatTKGCCT1.Key = "XuatTKGCCT";
            this.XuatTKGCCT1.Name = "XuatTKGCCT1";
            // 
            // XuatTKMD
            // 
            this.XuatTKMD.Key = "XuatTKMD";
            this.XuatTKMD.Name = "XuatTKMD";
            this.XuatTKMD.Text = "Tờ khai mậu dịch";
            // 
            // XuatTKGCCT
            // 
            this.XuatTKGCCT.Key = "XuatTKGCCT";
            this.XuatTKGCCT.Name = "XuatTKGCCT";
            this.XuatTKGCCT.Text = "Tờ khai GCCT";
            // 
            // NhapTKXML
            // 
            this.NhapTKXML.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.NhapTKMD1,
            this.NhapTKGCCT1});
            this.NhapTKXML.Key = "NhapTKXML";
            this.NhapTKXML.Name = "NhapTKXML";
            this.NhapTKXML.Text = "Tờ khai";
            // 
            // NhapTKMD1
            // 
            this.NhapTKMD1.Key = "NhapTKMD";
            this.NhapTKMD1.Name = "NhapTKMD1";
            // 
            // NhapTKGCCT1
            // 
            this.NhapTKGCCT1.Key = "NhapTKGCCT";
            this.NhapTKGCCT1.Name = "NhapTKGCCT1";
            // 
            // NhapTKMD
            // 
            this.NhapTKMD.Key = "NhapTKMD";
            this.NhapTKMD.Name = "NhapTKMD";
            this.NhapTKMD.Text = "Tờ khai mậu dịch";
            // 
            // NhapTKGCCT
            // 
            this.NhapTKGCCT.Key = "NhapTKGCCT";
            this.NhapTKGCCT.Name = "NhapTKGCCT";
            this.NhapTKGCCT.Text = "Tờ khai GCCT";
            // 
            // cmdEnglish
            // 
            this.cmdEnglish.Key = "cmdEng";
            this.cmdEnglish.Name = "cmdEnglish";
            this.cmdEnglish.Text = "Tiếng Anh";
            // 
            // cmdVN
            // 
            this.cmdVN.Image = ((System.Drawing.Image)(resources.GetObject("cmdVN.Image")));
            this.cmdVN.Key = "cmdVN";
            this.cmdVN.Name = "cmdVN";
            this.cmdVN.Text = "Tiếng Việt";
            // 
            // cmdNhapXuat
            // 
            this.cmdNhapXuat.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.NhapXML2,
            this.cmdNhapDuLieuTuDoangNghiep1,
            this.Separator8,
            this.cmdXuatDN1,
            this.cmdXuatDuLieuPhongKHai1});
            this.cmdNhapXuat.Key = "cmdNhapXuat";
            this.cmdNhapXuat.Name = "cmdNhapXuat";
            this.cmdNhapXuat.Text = "Nhập xuất";
            // 
            // NhapXML2
            // 
            this.NhapXML2.Image = ((System.Drawing.Image)(resources.GetObject("NhapXML2.Image")));
            this.NhapXML2.ImageIndex = 21;
            this.NhapXML2.Key = "NhapXML";
            this.NhapXML2.Name = "NhapXML2";
            // 
            // cmdNhapDuLieuTuDoangNghiep1
            // 
            this.cmdNhapDuLieuTuDoangNghiep1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapDuLieuTuDoangNghiep1.Image")));
            this.cmdNhapDuLieuTuDoangNghiep1.ImageIndex = 21;
            this.cmdNhapDuLieuTuDoangNghiep1.Key = "cmdNhapDuLieuTuDoangNghiep";
            this.cmdNhapDuLieuTuDoangNghiep1.Name = "cmdNhapDuLieuTuDoangNghiep1";
            // 
            // Separator8
            // 
            this.Separator8.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator8.Key = "Separator";
            this.Separator8.Name = "Separator8";
            // 
            // cmdXuatDN1
            // 
            this.cmdXuatDN1.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatDN1.Image")));
            this.cmdXuatDN1.ImageIndex = 20;
            this.cmdXuatDN1.Key = "cmdXuatDN";
            this.cmdXuatDN1.Name = "cmdXuatDN1";
            // 
            // cmdXuatDuLieuPhongKHai1
            // 
            this.cmdXuatDuLieuPhongKHai1.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatDuLieuPhongKHai1.Image")));
            this.cmdXuatDuLieuPhongKHai1.ImageIndex = 20;
            this.cmdXuatDuLieuPhongKHai1.Key = "cmdXuatDuLieuPhongKHai";
            this.cmdXuatDuLieuPhongKHai1.Name = "cmdXuatDuLieuPhongKHai1";
            // 
            // cmdXuatDuLieuPhongKHai
            // 
            this.cmdXuatDuLieuPhongKHai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdXuatHopDong1,
            this.cmdXuatDinhMuc1,
            this.cmdXuatPhuKien1,
            this.cmdXuatToKhai1});
            this.cmdXuatDuLieuPhongKHai.Key = "cmdXuatDuLieuPhongKHai";
            this.cmdXuatDuLieuPhongKHai.Name = "cmdXuatDuLieuPhongKHai";
            this.cmdXuatDuLieuPhongKHai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdXuatHopDong1
            // 
            this.cmdXuatHopDong1.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatHopDong1.Image")));
            this.cmdXuatHopDong1.ImageIndex = 23;
            this.cmdXuatHopDong1.Key = "cmdXuatHopDong";
            this.cmdXuatHopDong1.Name = "cmdXuatHopDong1";
            // 
            // cmdXuatDinhMuc1
            // 
            this.cmdXuatDinhMuc1.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatDinhMuc1.Image")));
            this.cmdXuatDinhMuc1.ImageIndex = 22;
            this.cmdXuatDinhMuc1.Key = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc1.Name = "cmdXuatDinhMuc1";
            // 
            // cmdXuatPhuKien1
            // 
            this.cmdXuatPhuKien1.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatPhuKien1.Image")));
            this.cmdXuatPhuKien1.ImageIndex = 25;
            this.cmdXuatPhuKien1.Key = "cmdXuatPhuKien";
            this.cmdXuatPhuKien1.Name = "cmdXuatPhuKien1";
            // 
            // cmdXuatToKhai1
            // 
            this.cmdXuatToKhai1.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatToKhai1.Image")));
            this.cmdXuatToKhai1.ImageIndex = 20;
            this.cmdXuatToKhai1.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai1.Name = "cmdXuatToKhai1";
            // 
            // cmdXuatHopDong
            // 
            this.cmdXuatHopDong.Key = "cmdXuatHopDong";
            this.cmdXuatHopDong.Name = "cmdXuatHopDong";
            this.cmdXuatHopDong.Text = "Hợp đồng";
            // 
            // cmdXuatDinhMuc
            // 
            this.cmdXuatDinhMuc.Key = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Name = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Text = "Định mức";
            // 
            // cmdXuatPhuKien
            // 
            this.cmdXuatPhuKien.Key = "cmdXuatPhuKien";
            this.cmdXuatPhuKien.Name = "cmdXuatPhuKien";
            this.cmdXuatPhuKien.Text = "Phụ kiện";
            // 
            // cmdXuatToKhai
            // 
            this.cmdXuatToKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.XuatTKGCCT2,
            this.XuatTKMD2});
            this.cmdXuatToKhai.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai.Name = "cmdXuatToKhai";
            this.cmdXuatToKhai.Text = "Tờ khai";
            // 
            // XuatTKGCCT2
            // 
            this.XuatTKGCCT2.Image = ((System.Drawing.Image)(resources.GetObject("XuatTKGCCT2.Image")));
            this.XuatTKGCCT2.ImageIndex = 24;
            this.XuatTKGCCT2.Key = "XuatTKGCCT";
            this.XuatTKGCCT2.Name = "XuatTKGCCT2";
            this.XuatTKGCCT2.Text = "Tờ khai gia công chuyển tiếp";
            // 
            // XuatTKMD2
            // 
            this.XuatTKMD2.Image = ((System.Drawing.Image)(resources.GetObject("XuatTKMD2.Image")));
            this.XuatTKMD2.ImageIndex = 26;
            this.XuatTKMD2.Key = "XuatTKMD";
            this.XuatTKMD2.Name = "XuatTKMD2";
            // 
            // cmdActivate
            // 
            this.cmdActivate.Key = "cmdActivate";
            this.cmdActivate.Name = "cmdActivate";
            this.cmdActivate.Shortcut = System.Windows.Forms.Shortcut.CtrlF10;
            this.cmdActivate.Text = "Kích hoạt phần mềm";
            // 
            // cmdDMSPGC
            // 
            this.cmdDMSPGC.Key = "cmdDMSPGC";
            this.cmdDMSPGC.Name = "cmdDMSPGC";
            this.cmdDMSPGC.Text = "Danh mục sản phẩm gia công";
            // 
            // cmdNhapDuLieuDaiLy
            // 
            this.cmdNhapDuLieuDaiLy.Key = "cmdNhapDuLieuDaiLy";
            this.cmdNhapDuLieuDaiLy.Name = "cmdNhapDuLieuDaiLy";
            this.cmdNhapDuLieuDaiLy.Text = "Nhập dữ liệu từ đại lý";
            // 
            // cmdCloseMe
            // 
            this.cmdCloseMe.Key = "cmdCloseMe";
            this.cmdCloseMe.Name = "cmdCloseMe";
            this.cmdCloseMe.Text = "Đóng cửa sổ này";
            // 
            // cmdCloseAllButMe
            // 
            this.cmdCloseAllButMe.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Name = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Text = "Đóng các cửa sổ khác";
            // 
            // cmdCloseAll
            // 
            this.cmdCloseAll.Key = "cmdCloseAll";
            this.cmdCloseAll.Name = "cmdCloseAll";
            this.cmdCloseAll.Text = "Đóng hết các cửa sổ";
            // 
            // cmdXuatDN
            // 
            this.cmdXuatDN.Key = "cmdXuatDN";
            this.cmdXuatDN.Name = "cmdXuatDN";
            this.cmdXuatDN.Text = "Xuất dữ liệu cho doanh nghiệp";
            // 
            // QuanLyMess
            // 
            this.QuanLyMess.Key = "QuanLyMess";
            this.QuanLyMess.Name = "QuanLyMess";
            this.QuanLyMess.Text = "Quản lý message khai báo";
            // 
            // cmdNhapDuLieuTuDoangNghiep
            // 
            this.cmdNhapDuLieuTuDoangNghiep.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNhapHopDong1,
            this.cmdNhapDinhMuc1,
            this.cmdNhapPhuKien1,
            this.cmdNhapToKhai1});
            this.cmdNhapDuLieuTuDoangNghiep.Key = "cmdNhapDuLieuTuDoangNghiep";
            this.cmdNhapDuLieuTuDoangNghiep.Name = "cmdNhapDuLieuTuDoangNghiep";
            this.cmdNhapDuLieuTuDoangNghiep.Text = "Nhập dữ liệu từ Doanh Nghiệp";
            // 
            // cmdNhapHopDong1
            // 
            this.cmdNhapHopDong1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapHopDong1.Image")));
            this.cmdNhapHopDong1.ImageIndex = 23;
            this.cmdNhapHopDong1.Key = "cmdNhapHopDong";
            this.cmdNhapHopDong1.Name = "cmdNhapHopDong1";
            // 
            // cmdNhapDinhMuc1
            // 
            this.cmdNhapDinhMuc1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapDinhMuc1.Image")));
            this.cmdNhapDinhMuc1.ImageIndex = 22;
            this.cmdNhapDinhMuc1.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc1.Name = "cmdNhapDinhMuc1";
            // 
            // cmdNhapPhuKien1
            // 
            this.cmdNhapPhuKien1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapPhuKien1.Image")));
            this.cmdNhapPhuKien1.ImageIndex = 25;
            this.cmdNhapPhuKien1.Key = "cmdNhapPhuKien";
            this.cmdNhapPhuKien1.Name = "cmdNhapPhuKien1";
            // 
            // cmdNhapToKhai1
            // 
            this.cmdNhapToKhai1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapToKhai1.Image")));
            this.cmdNhapToKhai1.ImageIndex = 21;
            this.cmdNhapToKhai1.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai1.Name = "cmdNhapToKhai1";
            // 
            // cmdNhapHopDong
            // 
            this.cmdNhapHopDong.Key = "cmdNhapHopDong";
            this.cmdNhapHopDong.Name = "cmdNhapHopDong";
            this.cmdNhapHopDong.Text = "Hợp đồng";
            // 
            // cmdNhapDinhMuc
            // 
            this.cmdNhapDinhMuc.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Name = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Text = "Định mức";
            // 
            // cmdNhapPhuKien
            // 
            this.cmdNhapPhuKien.Key = "cmdNhapPhuKien";
            this.cmdNhapPhuKien.Name = "cmdNhapPhuKien";
            this.cmdNhapPhuKien.Text = "Phụ kiện";
            // 
            // cmdNhapToKhai
            // 
            this.cmdNhapToKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNhapToKhaiGCCT1,
            this.cmdNhapToKhaiMauDich1});
            this.cmdNhapToKhai.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai.Name = "cmdNhapToKhai";
            this.cmdNhapToKhai.Text = "Tờ khai";
            // 
            // cmdNhapToKhaiGCCT1
            // 
            this.cmdNhapToKhaiGCCT1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapToKhaiGCCT1.Image")));
            this.cmdNhapToKhaiGCCT1.ImageIndex = 24;
            this.cmdNhapToKhaiGCCT1.Key = "cmdNhapToKhaiGCCT";
            this.cmdNhapToKhaiGCCT1.Name = "cmdNhapToKhaiGCCT1";
            // 
            // cmdNhapToKhaiMauDich1
            // 
            this.cmdNhapToKhaiMauDich1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapToKhaiMauDich1.Image")));
            this.cmdNhapToKhaiMauDich1.ImageIndex = 26;
            this.cmdNhapToKhaiMauDich1.Key = "cmdNhapToKhaiMauDich";
            this.cmdNhapToKhaiMauDich1.Name = "cmdNhapToKhaiMauDich1";
            // 
            // cmdNhapToKhaiMauDich
            // 
            this.cmdNhapToKhaiMauDich.Key = "cmdNhapToKhaiMauDich";
            this.cmdNhapToKhaiMauDich.Name = "cmdNhapToKhaiMauDich";
            this.cmdNhapToKhaiMauDich.Text = "Tờ khai mậu dịch";
            // 
            // cmdNhapToKhaiGCCT
            // 
            this.cmdNhapToKhaiGCCT.Key = "cmdNhapToKhaiGCCT";
            this.cmdNhapToKhaiGCCT.Name = "cmdNhapToKhaiGCCT";
            this.cmdNhapToKhaiGCCT.Text = "Tờ khai gia công chuyển tiếp";
            // 
            // cmdTLTTDN
            // 
            this.cmdTLTTDN.Key = "cmdTLTTDN";
            this.cmdTLTTDN.Name = "cmdTLTTDN";
            this.cmdTLTTDN.Text = "Thiêt lập thông tin Doanh Nghiệp";
            // 
            // mnuQuerySQL
            // 
            this.mnuQuerySQL.Image = ((System.Drawing.Image)(resources.GetObject("mnuQuerySQL.Image")));
            this.mnuQuerySQL.Key = "mnuQuerySQL";
            this.mnuQuerySQL.Name = "mnuQuerySQL";
            this.mnuQuerySQL.Text = "Thực hiện truy vấn SQL";
            // 
            // cmdLog
            // 
            this.cmdLog.Key = "cmdLog";
            this.cmdLog.Name = "cmdLog";
            this.cmdLog.Text = "Nhật ký chương trình";
            // 
            // cmdDataVersion
            // 
            this.cmdDataVersion.Key = "cmdDataVersion";
            this.cmdDataVersion.Name = "cmdDataVersion";
            this.cmdDataVersion.Text = "Dữ liệu phiên bản: ?";
            // 
            // cmdChuKySo
            // 
            this.cmdChuKySo.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChuKySo.Icon")));
            this.cmdChuKySo.Key = "cmdChuKySo";
            this.cmdChuKySo.Name = "cmdChuKySo";
            this.cmdChuKySo.Text = "Cấu hình chữ ký số";
            // 
            // cmdTimer
            // 
            this.cmdTimer.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTimer.Icon")));
            this.cmdTimer.Key = "cmdTimer";
            this.cmdTimer.Name = "cmdTimer";
            this.cmdTimer.Text = "Cấu hình thời gian";
            // 
            // cmdNhomCuaKhau
            // 
            this.cmdNhomCuaKhau.ImageIndex = 5;
            this.cmdNhomCuaKhau.Key = "cmdNhomCuaKhau";
            this.cmdNhomCuaKhau.Name = "cmdNhomCuaKhau";
            this.cmdNhomCuaKhau.Text = "Nhóm cửa khẩu";
            // 
            // cmdGetCategoryOnline
            // 
            this.cmdGetCategoryOnline.ImageIndex = 19;
            this.cmdGetCategoryOnline.Key = "cmdGetCategoryOnline";
            this.cmdGetCategoryOnline.Name = "cmdGetCategoryOnline";
            this.cmdGetCategoryOnline.Text = "Cập nhật danh mục trực tuyến";
            // 
            // cmdBieuThue
            // 
            this.cmdBieuThue.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.TraCuuMaHS1,
            this.Separator5,
            this.cmdTraCuuXNKOnline1,
            this.cmdTraCuuNoThueOnline1,
            this.cmdTraCuuVanBanOnline1,
            this.cmdTuVanHQOnline1,
            this.cmdBieuThueXNK20181});
            this.cmdBieuThue.Key = "cmdBieuThue";
            this.cmdBieuThue.Name = "cmdBieuThue";
            this.cmdBieuThue.Text = "&Biểu thuế (Mã HS)";
            // 
            // TraCuuMaHS1
            // 
            this.TraCuuMaHS1.Image = ((System.Drawing.Image)(resources.GetObject("TraCuuMaHS1.Image")));
            this.TraCuuMaHS1.ImageIndex = 17;
            this.TraCuuMaHS1.Key = "TraCuuMaHS";
            this.TraCuuMaHS1.Name = "TraCuuMaHS1";
            // 
            // Separator5
            // 
            this.Separator5.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator5.Key = "Separator";
            this.Separator5.Name = "Separator5";
            // 
            // cmdTraCuuXNKOnline1
            // 
            this.cmdTraCuuXNKOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTraCuuXNKOnline1.Image")));
            this.cmdTraCuuXNKOnline1.ImageIndex = 16;
            this.cmdTraCuuXNKOnline1.Key = "cmdTraCuuXNKOnline";
            this.cmdTraCuuXNKOnline1.Name = "cmdTraCuuXNKOnline1";
            // 
            // cmdTraCuuNoThueOnline1
            // 
            this.cmdTraCuuNoThueOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTraCuuNoThueOnline1.Image")));
            this.cmdTraCuuNoThueOnline1.ImageIndex = 16;
            this.cmdTraCuuNoThueOnline1.Key = "cmdTraCuuNoThueOnline";
            this.cmdTraCuuNoThueOnline1.Name = "cmdTraCuuNoThueOnline1";
            // 
            // cmdTraCuuVanBanOnline1
            // 
            this.cmdTraCuuVanBanOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTraCuuVanBanOnline1.Image")));
            this.cmdTraCuuVanBanOnline1.ImageIndex = 16;
            this.cmdTraCuuVanBanOnline1.Key = "cmdTraCuuVanBanOnline";
            this.cmdTraCuuVanBanOnline1.Name = "cmdTraCuuVanBanOnline1";
            // 
            // cmdTuVanHQOnline1
            // 
            this.cmdTuVanHQOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTuVanHQOnline1.Image")));
            this.cmdTuVanHQOnline1.ImageIndex = 16;
            this.cmdTuVanHQOnline1.Key = "cmdTuVanHQOnline";
            this.cmdTuVanHQOnline1.Name = "cmdTuVanHQOnline1";
            // 
            // cmdBieuThueXNK20181
            // 
            this.cmdBieuThueXNK20181.Key = "cmdBieuThueXNK2018";
            this.cmdBieuThueXNK20181.Name = "cmdBieuThueXNK20181";
            // 
            // cmdTraCuuXNKOnline
            // 
            this.cmdTraCuuXNKOnline.ImageIndex = 17;
            this.cmdTraCuuXNKOnline.Key = "cmdTraCuuXNKOnline";
            this.cmdTraCuuXNKOnline.Name = "cmdTraCuuXNKOnline";
            this.cmdTraCuuXNKOnline.Text = "Tra cứu biểu thuế Xuất nhập khẩu trực tuyến";
            // 
            // cmdTraCuuNoThueOnline
            // 
            this.cmdTraCuuNoThueOnline.ImageIndex = 17;
            this.cmdTraCuuNoThueOnline.Key = "cmdTraCuuNoThueOnline";
            this.cmdTraCuuNoThueOnline.Name = "cmdTraCuuNoThueOnline";
            this.cmdTraCuuNoThueOnline.Text = "Tra cứu nợ thuế trực tuyến";
            // 
            // cmdTraCuuVanBanOnline
            // 
            this.cmdTraCuuVanBanOnline.ImageIndex = 17;
            this.cmdTraCuuVanBanOnline.Key = "cmdTraCuuVanBanOnline";
            this.cmdTraCuuVanBanOnline.Name = "cmdTraCuuVanBanOnline";
            this.cmdTraCuuVanBanOnline.Text = "Tra cứu văn bản trực tuyến";
            // 
            // cmdTuVanHQOnline
            // 
            this.cmdTuVanHQOnline.ImageIndex = 17;
            this.cmdTuVanHQOnline.Key = "cmdTuVanHQOnline";
            this.cmdTuVanHQOnline.Name = "cmdTuVanHQOnline";
            this.cmdTuVanHQOnline.Text = "Tư vấn Hải quan trực tuyến";
            // 
            // cmdTeamview
            // 
            this.cmdTeamview.ImageIndex = 27;
            this.cmdTeamview.Key = "cmdTeamview";
            this.cmdTeamview.Name = "cmdTeamview";
            this.cmdTeamview.Text = "Hỗ trợ trực tuyến qua Teamview";
            // 
            // cmdCapNhatHS
            // 
            this.cmdCapNhatHS.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCapNhatHS8Auto1,
            this.cmdCapNhatHS8SoManual1});
            this.cmdCapNhatHS.ImageIndex = 31;
            this.cmdCapNhatHS.Key = "cmdCapNhatHS";
            this.cmdCapNhatHS.Name = "cmdCapNhatHS";
            this.cmdCapNhatHS.Text = "Cập nhật biểu thuế (Mã HS 8 số)";
            // 
            // cmdCapNhatHS8Auto1
            // 
            this.cmdCapNhatHS8Auto1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCapNhatHS8Auto1.Image")));
            this.cmdCapNhatHS8Auto1.Key = "cmdCapNhatHS8Auto";
            this.cmdCapNhatHS8Auto1.Name = "cmdCapNhatHS8Auto1";
            // 
            // cmdCapNhatHS8SoManual1
            // 
            this.cmdCapNhatHS8SoManual1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCapNhatHS8SoManual1.Image")));
            this.cmdCapNhatHS8SoManual1.Key = "cmdCapNhatHS8SoManual";
            this.cmdCapNhatHS8SoManual1.Name = "cmdCapNhatHS8SoManual1";
            // 
            // cmdCapNhatHS8Auto
            // 
            this.cmdCapNhatHS8Auto.ImageIndex = 31;
            this.cmdCapNhatHS8Auto.Key = "cmdCapNhatHS8Auto";
            this.cmdCapNhatHS8Auto.Name = "cmdCapNhatHS8Auto";
            this.cmdCapNhatHS8Auto.Text = "Cập nhật mã HS 8 số tự động";
            // 
            // cmdCapNhatHS8SoManual
            // 
            this.cmdCapNhatHS8SoManual.ImageIndex = 31;
            this.cmdCapNhatHS8SoManual.Key = "cmdCapNhatHS8SoManual";
            this.cmdCapNhatHS8SoManual.Name = "cmdCapNhatHS8SoManual";
            this.cmdCapNhatHS8SoManual.Text = "Cập nhật mã HS 8 số theo lựa chọn";
            // 
            // cmdTool
            // 
            this.cmdTool.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImageResizeHelp1,
            this.cmdSignFile1,
            this.cmdHelpSignFile1});
            this.cmdTool.ImageIndex = 29;
            this.cmdTool.Key = "cmdTool";
            this.cmdTool.Name = "cmdTool";
            this.cmdTool.Text = "Công cụ hỗ trợ";
            // 
            // cmdImageResizeHelp1
            // 
            this.cmdImageResizeHelp1.Image = ((System.Drawing.Image)(resources.GetObject("cmdImageResizeHelp1.Image")));
            this.cmdImageResizeHelp1.Key = "cmdImageResizeHelp";
            this.cmdImageResizeHelp1.Name = "cmdImageResizeHelp1";
            // 
            // cmdSignFile1
            // 
            this.cmdSignFile1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSignFile1.Image")));
            this.cmdSignFile1.Key = "cmdSignFile";
            this.cmdSignFile1.Name = "cmdSignFile1";
            // 
            // cmdHelpSignFile1
            // 
            this.cmdHelpSignFile1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHelpSignFile1.Image")));
            this.cmdHelpSignFile1.Key = "cmdHelpSignFile";
            this.cmdHelpSignFile1.Name = "cmdHelpSignFile1";
            // 
            // cmdImageResizeHelp
            // 
            this.cmdImageResizeHelp.ImageIndex = 30;
            this.cmdImageResizeHelp.Key = "cmdImageResizeHelp";
            this.cmdImageResizeHelp.Name = "cmdImageResizeHelp";
            this.cmdImageResizeHelp.Text = "Hướng dẫn sử dụng điều chỉnh dung lượng ảnh";
            // 
            // cmdDaily
            // 
            this.cmdDaily.Key = "cmdDaily";
            this.cmdDaily.Name = "cmdDaily";
            this.cmdDaily.Text = "Doanh nghiệp khai Đại Lý";
            // 
            // cmdUpdateDatabase
            // 
            this.cmdUpdateDatabase.ImageIndex = 32;
            this.cmdUpdateDatabase.Key = "cmdUpdateDatabase";
            this.cmdUpdateDatabase.Name = "cmdUpdateDatabase";
            this.cmdUpdateDatabase.Text = "Cập nhật Cơ sở dữ liệu";
            // 
            // cmdHelpVideo
            // 
            this.cmdHelpVideo.ImageIndex = 38;
            this.cmdHelpVideo.Key = "cmdHelpVideo";
            this.cmdHelpVideo.Name = "cmdHelpVideo";
            this.cmdHelpVideo.Text = "Video Hướng dẫn sử dụng chương trình";
            // 
            // cmdHDSDCKS
            // 
            this.cmdHDSDCKS.ImageIndex = 33;
            this.cmdHDSDCKS.Key = "cmdHDSDCKS";
            this.cmdHDSDCKS.Name = "cmdHDSDCKS";
            this.cmdHDSDCKS.Text = "Hướng dẫn đăng ký và sử dụng Chữ ký số (CA)";
            // 
            // cmdHDSDVNACCS
            // 
            this.cmdHDSDVNACCS.ImageIndex = 30;
            this.cmdHDSDVNACCS.Key = "cmdHDSDVNACCS";
            this.cmdHDSDVNACCS.Name = "cmdHDSDVNACCS";
            this.cmdHDSDVNACCS.Text = "Hướng dẫn sử dụng VNACCS";
            // 
            // cmdThongBaoVNACCS
            // 
            this.cmdThongBaoVNACCS.Key = "cmdThongBaoVNACCS";
            this.cmdThongBaoVNACCS.Name = "cmdThongBaoVNACCS";
            this.cmdThongBaoVNACCS.Text = "Thông báo từ hệ thống VANCCS";
            // 
            // cmdSignFile
            // 
            this.cmdSignFile.Key = "cmdSignFile";
            this.cmdSignFile.Name = "cmdSignFile";
            this.cmdSignFile.Text = "Phần mềm hỗ trợ ký chữ ký số cho File đính kèm gửi lên hải quan";
            // 
            // cmdHelpSignFile
            // 
            this.cmdHelpSignFile.Key = "cmdHelpSignFile";
            this.cmdHelpSignFile.Name = "cmdHelpSignFile";
            this.cmdHelpSignFile.Text = "Hướng dẫn sử dụng phần mềm ký chữ ký số cho Fil đính kèm";
            // 
            // cmdBerth
            // 
            this.cmdBerth.Image = ((System.Drawing.Image)(resources.GetObject("cmdBerth.Image")));
            this.cmdBerth.Key = "cmdBerth";
            this.cmdBerth.Name = "cmdBerth";
            this.cmdBerth.Text = "Cảng biển";
            // 
            // cmdCargo
            // 
            this.cmdCargo.Image = ((System.Drawing.Image)(resources.GetObject("cmdCargo.Image")));
            this.cmdCargo.Key = "cmdCargo";
            this.cmdCargo.Name = "cmdCargo";
            this.cmdCargo.Text = "Địa điểm lưu kho chờ thông quan";
            // 
            // cmdCityUNLOCODE
            // 
            this.cmdCityUNLOCODE.Image = ((System.Drawing.Image)(resources.GetObject("cmdCityUNLOCODE.Image")));
            this.cmdCityUNLOCODE.Key = "cmdCityUNLOCODE";
            this.cmdCityUNLOCODE.Name = "cmdCityUNLOCODE";
            this.cmdCityUNLOCODE.Text = "Địa điểm xếp hàng";
            // 
            // cmdCommon
            // 
            this.cmdCommon.Image = ((System.Drawing.Image)(resources.GetObject("cmdCommon.Image")));
            this.cmdCommon.Key = "cmdCommon";
            this.cmdCommon.Name = "cmdCommon";
            this.cmdCommon.Text = "Tổng hợp danh mục";
            // 
            // cmdContainerSize
            // 
            this.cmdContainerSize.Image = ((System.Drawing.Image)(resources.GetObject("cmdContainerSize.Image")));
            this.cmdContainerSize.Key = "cmdContainerSize";
            this.cmdContainerSize.Name = "cmdContainerSize";
            this.cmdContainerSize.Text = "Container Size";
            // 
            // cmdCustomsSubSection
            // 
            this.cmdCustomsSubSection.Image = ((System.Drawing.Image)(resources.GetObject("cmdCustomsSubSection.Image")));
            this.cmdCustomsSubSection.Key = "cmdCustomsSubSection";
            this.cmdCustomsSubSection.Name = "cmdCustomsSubSection";
            this.cmdCustomsSubSection.Text = "Đội thủ tục HQ";
            // 
            // cmdOGAUser
            // 
            this.cmdOGAUser.Image = ((System.Drawing.Image)(resources.GetObject("cmdOGAUser.Image")));
            this.cmdOGAUser.Key = "cmdOGAUser";
            this.cmdOGAUser.Name = "cmdOGAUser";
            this.cmdOGAUser.Text = "Mã kiểm dịch động vật";
            // 
            // cmdPackagesUnit
            // 
            this.cmdPackagesUnit.Image = ((System.Drawing.Image)(resources.GetObject("cmdPackagesUnit.Image")));
            this.cmdPackagesUnit.Key = "cmdPackagesUnit";
            this.cmdPackagesUnit.Name = "cmdPackagesUnit";
            this.cmdPackagesUnit.Text = "ĐVT Lượng kiện";
            // 
            // cmdStations
            // 
            this.cmdStations.Image = ((System.Drawing.Image)(resources.GetObject("cmdStations.Image")));
            this.cmdStations.Key = "cmdStations";
            this.cmdStations.Name = "cmdStations";
            this.cmdStations.Text = "Kho ngoại quan";
            // 
            // cmdTaxClassificationCode
            // 
            this.cmdTaxClassificationCode.Image = ((System.Drawing.Image)(resources.GetObject("cmdTaxClassificationCode.Image")));
            this.cmdTaxClassificationCode.Key = "cmdTaxClassificationCode";
            this.cmdTaxClassificationCode.Name = "cmdTaxClassificationCode";
            this.cmdTaxClassificationCode.Text = "Biểu thuế XNK";
            // 
            // cmdReloadData
            // 
            this.cmdReloadData.Image = ((System.Drawing.Image)(resources.GetObject("cmdReloadData.Image")));
            this.cmdReloadData.Key = "cmdReloadData";
            this.cmdReloadData.Name = "cmdReloadData";
            this.cmdReloadData.Text = "Cập nhật lại dữ liệu";
            // 
            // cmdBieuThueXNK2018
            // 
            this.cmdBieuThueXNK2018.Image = ((System.Drawing.Image)(resources.GetObject("cmdBieuThueXNK2018.Image")));
            this.cmdBieuThueXNK2018.Key = "cmdBieuThueXNK2018";
            this.cmdBieuThueXNK2018.Name = "cmdBieuThueXNK2018";
            this.cmdBieuThueXNK2018.Text = "Biểu thuế XNK 2018";
            // 
            // cmdUpdateCategoryOnline
            // 
            this.cmdUpdateCategoryOnline.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateCategoryOnline.Image")));
            this.cmdUpdateCategoryOnline.Key = "cmdUpdateCategoryOnline";
            this.cmdUpdateCategoryOnline.Name = "cmdUpdateCategoryOnline";
            this.cmdUpdateCategoryOnline.Text = "Cập nhật bảng mã chuẩn VNACC Online";
            // 
            // cmdNhanPhanHoi
            // 
            this.cmdNhanPhanHoi.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhanPhanHoi.Image")));
            this.cmdNhanPhanHoi.Key = "cmdNhanPhanHoi";
            this.cmdNhanPhanHoi.Name = "cmdNhanPhanHoi";
            this.cmdNhanPhanHoi.Text = "Nhận phản hồi từ HQ";
            // 
            // cmdUpdateCertificates
            // 
            this.cmdUpdateCertificates.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateCertificates.Image")));
            this.cmdUpdateCertificates.Key = "cmdUpdateCertificates";
            this.cmdUpdateCertificates.Name = "cmdUpdateCertificates";
            this.cmdUpdateCertificates.Text = "Cập nhật Chứng thư số khai HQ";
            // 
            // cmdConfigReadExcel
            // 
            this.cmdConfigReadExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdConfigReadExcel.Image")));
            this.cmdConfigReadExcel.Key = "cmdConfigReadExcel";
            this.cmdConfigReadExcel.Name = "cmdConfigReadExcel";
            this.cmdConfigReadExcel.Text = "Cấu hình Thông số đọc Excel";
            // 
            // mnuRightClick
            // 
            this.mnuRightClick.CommandManager = this.cmMain;
            this.mnuRightClick.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCloseMe1,
            this.cmdCloseAllButMe1,
            this.cmdCloseAll1});
            this.mnuRightClick.Key = "mnuRightClick";
            this.mnuRightClick.LargeImageSize = new System.Drawing.Size(16, 16);
            this.mnuRightClick.SmallImageSize = new System.Drawing.Size(16, 16);
            // 
            // cmdCloseMe1
            // 
            this.cmdCloseMe1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseMe1.Image")));
            this.cmdCloseMe1.Key = "cmdCloseMe";
            this.cmdCloseMe1.Name = "cmdCloseMe1";
            // 
            // cmdCloseAllButMe1
            // 
            this.cmdCloseAllButMe1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseAllButMe1.Image")));
            this.cmdCloseAllButMe1.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe1.Name = "cmdCloseAllButMe1";
            // 
            // cmdCloseAll1
            // 
            this.cmdCloseAll1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseAll1.Image")));
            this.cmdCloseAll1.Key = "cmdCloseAll";
            this.cmdCloseAll1.Name = "cmdCloseAll1";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 24);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 494);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbMenu);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(847, 26);
            // 
            // ilSmall
            // 
            this.ilSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmall.ImageStream")));
            this.ilSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmall.Images.SetKeyName(0, "explorerBarItem1.Icon.ico");
            this.ilSmall.Images.SetKeyName(1, "explorerBarItem2.Icon.ico");
            this.ilSmall.Images.SetKeyName(2, "explorerBarItem3.Icon.ico");
            this.ilSmall.Images.SetKeyName(3, "explorerBarItem10.Icon.ico");
            this.ilSmall.Images.SetKeyName(4, "explorerBarItem11.Icon.ico");
            this.ilSmall.Images.SetKeyName(5, "folder_page.png");
            this.ilSmall.Images.SetKeyName(6, "shell32_279.ico");
            this.ilSmall.Images.SetKeyName(7, "shell32_21.ico");
            this.ilSmall.Images.SetKeyName(8, "RightDatabase32.gif");
            this.ilSmall.Images.SetKeyName(9, "key.png");
            this.ilSmall.Images.SetKeyName(10, "en-US.gif");
            this.ilSmall.Images.SetKeyName(11, "vi-VN.gif");
            this.ilSmall.Images.SetKeyName(12, "application_view_tile.png");
            this.ilSmall.Images.SetKeyName(13, "import.png");
            this.ilSmall.Images.SetKeyName(14, "export.png");
            this.ilSmall.Images.SetKeyName(15, "DatabaseLinkerDatabasesOrphan.png");
            this.ilSmall.Images.SetKeyName(16, "web_find.png");
            this.ilSmall.Images.SetKeyName(17, "file_find.png");
            this.ilSmall.Images.SetKeyName(18, "86.ico");
            this.ilSmall.Images.SetKeyName(19, "cmdAutoUpdate1.Icon.ico");
            this.ilSmall.Images.SetKeyName(20, "export.ico");
            this.ilSmall.Images.SetKeyName(21, "import.ico");
            this.ilSmall.Images.SetKeyName(22, "cmdImportDM1.Icon.ico");
            this.ilSmall.Images.SetKeyName(23, "cmdImportHangHoa1.Icon.ico");
            this.ilSmall.Images.SetKeyName(24, "cmdImportNPL1.Icon.ico");
            this.ilSmall.Images.SetKeyName(25, "cmdImportSP1.Icon.ico");
            this.ilSmall.Images.SetKeyName(26, "cmdImportToKhai1.Icon.ico");
            this.ilSmall.Images.SetKeyName(27, "TeamViewer.ico");
            this.ilSmall.Images.SetKeyName(28, "cmdRestore.Icon.ico");
            this.ilSmall.Images.SetKeyName(29, "TienIch1.Icon.ico");
            this.ilSmall.Images.SetKeyName(30, "help_16.png");
            this.ilSmall.Images.SetKeyName(31, "page_edit.png");
            this.ilSmall.Images.SetKeyName(32, "database_save.png");
            this.ilSmall.Images.SetKeyName(33, "Demo_Rule_Unique_Value.png");
            this.ilSmall.Images.SetKeyName(34, "date.png");
            this.ilSmall.Images.SetKeyName(35, "haiquanVN.jpg");
            this.ilSmall.Images.SetKeyName(36, "Display.ico");
            this.ilSmall.Images.SetKeyName(37, "ie.ico");
            this.ilSmall.Images.SetKeyName(38, "video.png");
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdExportExcel1.Icon")));
            this.cmdExportExcel1.Key = "cmdExportExccel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdThoat1
            // 
            this.cmdThoat1.Key = "cmdThoat";
            this.cmdThoat1.Name = "cmdThoat1";
            // 
            // pmMain
            // 
            this.pmMain.BackColorAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(218)))), ((int)(((byte)(254)))));
            this.pmMain.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.pmMain.BackColorGradientResizeBar = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(214)))), ((int)(((byte)(255)))));
            this.pmMain.BackColorGradientSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(145)))), ((int)(((byte)(205)))));
            this.pmMain.BackColorResizeBar = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.pmMain.BackColorSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(218)))), ((int)(((byte)(254)))));
            this.pmMain.ContainerControl = this;
            this.pmMain.DefaultPanelSettings.ActiveCaptionMode = Janus.Windows.UI.Dock.ActiveCaptionMode.Never;
            this.pmMain.DefaultPanelSettings.CaptionDisplayMode = Janus.Windows.UI.Dock.PanelCaptionDisplayMode.Text;
            this.pmMain.DefaultPanelSettings.CaptionHeight = 30;
            this.pmMain.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.pmMain.DefaultPanelSettings.DarkCaptionFormatStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.pmMain.DefaultPanelSettings.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.Window;
            this.pmMain.DefaultPanelSettings.TabStateStyles.FormatStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pmMain.TabbedMdi = true;
            this.pmMain.TabbedMdiSettings.TabStateStyles.SelectedFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.pmMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.pmMain.MdiTabMouseDown += new Janus.Windows.UI.Dock.MdiTabMouseEventHandler(this.pmMain_MdiTabMouseDown);
            this.uiPanel0.Id = new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("61981317-cb9d-40d9-9bdb-f3341c4a539c");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanel2.Id = new System.Guid("5671da6c-3bdd-401e-9876-b3de34ebbd02");
            this.uiPanel0.Panels.Add(this.uiPanel2);
            this.uiPanel3.Id = new System.Guid("959bfeec-7a83-4a72-92d4-7287207f21ba");
            this.uiPanel0.Panels.Add(this.uiPanel3);
            this.uiPanel4.Id = new System.Guid("94ca22fd-9014-47cc-b6f9-9dbf946c419c");
            this.uiPanel0.Panels.Add(this.uiPanel4);
            this.uiPanel5.Id = new System.Guid("4e6d4adf-9ba2-405e-9451-be53779b6040");
            this.uiPanel0.Panels.Add(this.uiPanel5);
            this.uiPanel6.Id = new System.Guid("bc16c510-97a5-4b85-9d55-de1cedb56fa0");
            this.uiPanel0.Panels.Add(this.uiPanel6);
            this.uiPanel7.Id = new System.Guid("3c3c7c90-1c20-4e73-ae33-a99906903781");
            this.uiPanel0.Panels.Add(this.uiPanel7);
            this.uiPanel8.Id = new System.Guid("80182d20-926a-4e13-b6c5-c0a7e0a38d02");
            this.uiPanel0.Panels.Add(this.uiPanel8);
            this.pmMain.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.pmMain.BeginPanelInfo();
            this.pmMain.AddDockPanelInfo(new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, Janus.Windows.UI.Dock.PanelDockStyle.Left, true, new System.Drawing.Size(250, 499), true);
            this.pmMain.AddDockPanelInfo(new System.Guid("61981317-cb9d-40d9-9bdb-f3341c4a539c"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("5671da6c-3bdd-401e-9876-b3de34ebbd02"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("959bfeec-7a83-4a72-92d4-7287207f21ba"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("94ca22fd-9014-47cc-b6f9-9dbf946c419c"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("4e6d4adf-9ba2-405e-9451-be53779b6040"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("bc16c510-97a5-4b85-9d55-de1cedb56fa0"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("3c3c7c90-1c20-4e73-ae33-a99906903781"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("80182d20-926a-4e13-b6c5-c0a7e0a38d02"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("61981317-cb9d-40d9-9bdb-f3341c4a539c"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("5671da6c-3bdd-401e-9876-b3de34ebbd02"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("959bfeec-7a83-4a72-92d4-7287207f21ba"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("94ca22fd-9014-47cc-b6f9-9dbf946c419c"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("4e6d4adf-9ba2-405e-9451-be53779b6040"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("bc16c510-97a5-4b85-9d55-de1cedb56fa0"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("3c3c7c90-1c20-4e73-ae33-a99906903781"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("80182d20-926a-4e13-b6c5-c0a7e0a38d02"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.GroupStyle = Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator;
            this.uiPanel0.Location = new System.Drawing.Point(3, 29);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.SelectedPanel = this.uiPanel1;
            this.uiPanel0.Size = new System.Drawing.Size(250, 499);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Panel 0";
            this.uiPanel0.SelectedPanelChanged += new Janus.Windows.UI.Dock.PanelActionEventHandler(this.uiPanel0_SelectedPanelChanged);
            // 
            // uiPanel1
            // 
            this.uiPanel1.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel1.Image")));
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(246, 203);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Khai báo Thông quan điện tử";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.explorerBar1);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(244, 172);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // explorerBar1
            // 
            this.explorerBar1.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar1.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBar1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem358.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem358.Image")));
            explorerBarItem358.ImageIndex = 0;
            explorerBarItem358.Key = "hdgcNhap";
            explorerBarItem358.Text = "Khai báo";
            explorerBarItem359.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem359.Image")));
            explorerBarItem359.ImageIndex = 1;
            explorerBarItem359.Key = "hdgcManage";
            explorerBarItem359.Text = "Theo dõi";
            explorerBarItem360.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem360.Image")));
            explorerBarItem360.ImageIndex = 2;
            explorerBarItem360.Key = "hdgcRegisted";
            explorerBarItem360.Text = "Đã đăng ký";
            explorerBarGroup132.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem358,
            explorerBarItem359,
            explorerBarItem360});
            explorerBarGroup132.Key = "grpHopDong";
            explorerBarGroup132.Text = "Hợp đồng";
            explorerBarItem361.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem361.Image")));
            explorerBarItem361.Key = "cmdKhoKT";
            explorerBarItem361.Text = "Kho kế toán";
            explorerBarGroup133.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem361});
            explorerBarGroup133.Key = "grpKhoKT";
            explorerBarGroup133.Text = "Kho kế toán";
            explorerBarItem362.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem362.Image")));
            explorerBarItem362.Key = "mau15";
            explorerBarItem362.Text = "Tạo mẫu 15";
            explorerBarItem363.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem363.Image")));
            explorerBarItem363.Key = "mau16";
            explorerBarItem363.Text = "Tạo mẫu 16";
            explorerBarItem364.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem364.Image")));
            explorerBarItem364.Key = "cmdTheoDoiQuyetToan_Mau15";
            explorerBarItem364.Text = "Theo dõi hồ sơ Quyết Toán";
            explorerBarItem365.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem365.Image")));
            explorerBarItem365.Key = "cmdMMTB";
            explorerBarItem365.Text = "Theo dõi quyết toán MMTB";
            explorerBarItem366.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem366.Image")));
            explorerBarItem366.Key = "cmdNPLTon";
            explorerBarItem366.Text = "Theo dõi NPL tồn";
            explorerBarGroup134.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem362,
            explorerBarItem363,
            explorerBarItem364,
            explorerBarItem365,
            explorerBarItem366});
            explorerBarGroup134.Key = "grpQuyetToan";
            explorerBarGroup134.Text = "Báo cáo quyết toán TT38";
            explorerBarItem367.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem367.Image")));
            explorerBarItem367.Key = "dmRegister";
            explorerBarItem367.Text = "Đăng ký";
            explorerBarItem368.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem368.Image")));
            explorerBarItem368.Key = "dmSend";
            explorerBarItem368.Text = "Khai báo";
            explorerBarItem369.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem369.Image")));
            explorerBarItem369.Key = "dmEdit";
            explorerBarItem369.Text = "Khai báo sửa";
            explorerBarItem370.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem370.Image")));
            explorerBarItem370.Key = "dmCancel";
            explorerBarItem370.Text = "Khai báo hủy";
            explorerBarItem371.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem371.Image")));
            explorerBarItem371.ImageIndex = 1;
            explorerBarItem371.Key = "dmManage";
            explorerBarItem371.Text = "Theo dõi";
            explorerBarItem372.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem372.Image")));
            explorerBarItem372.ImageIndex = 2;
            explorerBarItem372.Key = "dmRegisted";
            explorerBarItem372.Text = "Đã đăng ký";
            explorerBarGroup135.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem367,
            explorerBarItem368,
            explorerBarItem369,
            explorerBarItem370,
            explorerBarItem371,
            explorerBarItem372});
            explorerBarGroup135.Key = "grpDinhMuc";
            explorerBarGroup135.Text = "Định mức";
            explorerBarItem373.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem373.Image")));
            explorerBarItem373.Key = "dmTTDangKy";
            explorerBarItem373.Text = "Khai báo định mức thực tế theo từng giai đoạn";
            explorerBarItem374.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem374.Image")));
            explorerBarItem374.Key = "dmTTEdit";
            explorerBarItem374.Text = "Khai báo sửa";
            explorerBarItem375.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem375.Image")));
            explorerBarItem375.Key = "dmTTCancel";
            explorerBarItem375.Text = "Khai báo hủy";
            explorerBarItem376.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem376.Image")));
            explorerBarItem376.Key = "dmTTTheoDoi";
            explorerBarItem376.Text = "Theo dõi";
            explorerBarGroup136.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem373,
            explorerBarItem374,
            explorerBarItem375,
            explorerBarItem376});
            explorerBarGroup136.Key = "drpDinhMucThucTe";
            explorerBarGroup136.Text = "Định mức thực tế";
            explorerBarItem377.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem377.Image")));
            explorerBarItem377.Key = "lsxDangKy";
            explorerBarItem377.Text = "Đăng ký";
            explorerBarItem378.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem378.Image")));
            explorerBarItem378.Key = "lsxTheoDoi";
            explorerBarItem378.Text = "Theo dõi";
            explorerBarItem379.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem379.Image")));
            explorerBarItem379.Key = "lsxCapNhat";
            explorerBarItem379.Text = "Cập nhật lệnh sản xuất";
            explorerBarItem380.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem380.Image")));
            explorerBarItem380.Key = "lsxQuyTac";
            explorerBarItem380.Text = "Cấu hình quy tắc tạo lệnh sản xuất";
            explorerBarGroup137.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem377,
            explorerBarItem378,
            explorerBarItem379,
            explorerBarItem380});
            explorerBarGroup137.Key = "grpLenhSanXuat";
            explorerBarGroup137.Text = "Lệnh sản xuất";
            explorerBarItem381.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem381.Image")));
            explorerBarItem381.Key = "pkgcNhap";
            explorerBarItem381.Text = "Khai báo";
            explorerBarItem382.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem382.Image")));
            explorerBarItem382.Key = "pkgcManage";
            explorerBarItem382.Text = "Theo dõi";
            explorerBarItem383.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem383.Image")));
            explorerBarItem383.Key = "pkgcRegisted";
            explorerBarItem383.Text = "Đã đăng ký";
            explorerBarGroup138.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem381,
            explorerBarItem382,
            explorerBarItem383});
            explorerBarGroup138.Key = "grpPhuKien";
            explorerBarGroup138.Text = "Phụ kiện";
            explorerBarItem384.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem384.Image")));
            explorerBarItem384.Key = "tkNhapKhau_GC";
            explorerBarItem384.Text = "Nhập khẩu";
            explorerBarItem385.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem385.Image")));
            explorerBarItem385.Key = "tkXuatKhau_GC";
            explorerBarItem385.Text = "Xuất khẩu";
            explorerBarItem386.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem386.Image")));
            explorerBarItem386.Key = "tkTheoDoi";
            explorerBarItem386.Text = "Theo dõi";
            explorerBarItem387.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem387.Image")));
            explorerBarItem387.Key = "tkDaDangKy";
            explorerBarItem387.Text = "Đã đăng ký";
            explorerBarGroup139.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem384,
            explorerBarItem385,
            explorerBarItem386,
            explorerBarItem387});
            explorerBarGroup139.Key = "grpToKhai";
            explorerBarGroup139.Text = "Tờ khai có Hợp đồng";
            explorerBarItem388.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem388.Image")));
            explorerBarItem388.Key = "tkGCCTNhap";
            explorerBarItem388.Text = "Tờ khai nhập";
            explorerBarItem389.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem389.Image")));
            explorerBarItem389.Key = "tkGCCTXuat";
            explorerBarItem389.Text = "Tờ khai xuất";
            explorerBarItem390.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem390.Image")));
            explorerBarItem390.Key = "theodoiTKCT";
            explorerBarItem390.Text = "Theo dõi";
            explorerBarItem391.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem391.Image")));
            explorerBarItem391.Key = "tkGCCTDaDangKy";
            explorerBarItem391.Text = "Đã đăng ký";
            explorerBarGroup140.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem388,
            explorerBarItem389,
            explorerBarItem390,
            explorerBarItem391});
            explorerBarGroup140.Key = "grpGCCT";
            explorerBarGroup140.Text = "Tờ khai GC chuyển tiếp";
            explorerBarItem392.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem392.Icon")));
            explorerBarItem392.Key = "dmCUKhaibao";
            explorerBarItem392.Text = "Khai báo";
            explorerBarItem393.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem393.Icon")));
            explorerBarItem393.Key = "dmCUTheoDoi";
            explorerBarItem393.Text = "Theo dõi";
            explorerBarItem394.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem394.Icon")));
            explorerBarItem394.Key = "dmCUDaDangKy";
            explorerBarItem394.Text = "Đã đăng ký";
            explorerBarGroup141.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem392,
            explorerBarItem393,
            explorerBarItem394});
            explorerBarGroup141.Key = "DinhMucCU";
            explorerBarGroup141.Text = "NPL tự cung ứng";
            explorerBarGroup141.Visible = false;
            explorerBarItem395.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem395.Image")));
            explorerBarItem395.Key = "thanhKhoanSend";
            explorerBarItem395.Text = "Khai báo";
            explorerBarItem396.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem396.Image")));
            explorerBarItem396.Key = "thanhKhoanManager";
            explorerBarItem396.Text = "Theo dõi";
            explorerBarGroup142.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem395,
            explorerBarItem396});
            explorerBarGroup142.Key = "grpThanhKhoan";
            explorerBarGroup142.Text = "Thanh Khoản";
            explorerBarItem397.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem397.Image")));
            explorerBarItem397.Key = "cmdKhaiBaoCungUng";
            explorerBarItem397.Text = "Khai báo";
            explorerBarItem398.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem398.Image")));
            explorerBarItem398.Key = "cmdTheoDoiBKNPLCungUng";
            explorerBarItem398.Text = "Theo dõi";
            explorerBarGroup143.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem397,
            explorerBarItem398});
            explorerBarGroup143.Key = "grpBKNPLCungUng";
            explorerBarGroup143.Text = "Bảng kê NPL cung ứng";
            explorerBarItem399.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem399.Image")));
            explorerBarItem399.Key = "cmdKhaiBaoBKTX";
            explorerBarItem399.Text = "Khai báo";
            explorerBarItem400.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem400.Image")));
            explorerBarItem400.Key = "cmdTheoDoiBKTX";
            explorerBarItem400.Text = "Theo dõi";
            explorerBarGroup144.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem399,
            explorerBarItem400});
            explorerBarGroup144.Key = "grpBangKeTaiXuat";
            explorerBarGroup144.Text = "Bảng kê tái xuất";
            explorerBarItem401.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem401.Image")));
            explorerBarItem401.Key = "giaHanSend";
            explorerBarItem401.Text = "Khai báo";
            explorerBarItem402.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem402.Image")));
            explorerBarItem402.Key = "giaHanManager";
            explorerBarItem402.Text = "Theo dõi";
            explorerBarGroup145.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem401,
            explorerBarItem402});
            explorerBarGroup145.Key = "grpGiaHanHD";
            explorerBarGroup145.Text = "Gia hạn HĐ thanh khoản";
            explorerBarItem403.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem403.Image")));
            explorerBarItem403.Key = "dngsTieuHuy";
            explorerBarItem403.Text = "Khai báo";
            explorerBarItem404.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem404.Image")));
            explorerBarItem404.Key = "dngsthManage";
            explorerBarItem404.Text = "Theo dõi";
            explorerBarGroup146.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem403,
            explorerBarItem404});
            explorerBarGroup146.Key = "grpDNGiamSatTieuHuy";
            explorerBarGroup146.Text = "Đề nghị giám sát tiêu hủy";
            explorerBarItem405.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem405.Image")));
            explorerBarItem405.Key = "cmdBCQTHopDong";
            explorerBarItem405.Text = "Báo cáo quyết toán NPLSP";
            explorerBarItem406.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem406.Image")));
            explorerBarItem406.Key = "cmdBCQTMMTB";
            explorerBarItem406.Text = "Báo cáo quyết toán MMTB";
            explorerBarItem407.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem407.Image")));
            explorerBarItem407.Key = "cmdTheoDoiBCQTHopDong";
            explorerBarItem407.Text = "Theo dõi báo cáo ";
            explorerBarItem408.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem408.Image")));
            explorerBarItem408.Key = "cmdTheoDoiBCQTMMTB";
            explorerBarItem408.Text = "Theo dõi Báo cáo MMTB";
            explorerBarGroup147.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem405,
            explorerBarItem406,
            explorerBarItem407,
            explorerBarItem408});
            explorerBarGroup147.Key = "grpBCQTNew";
            explorerBarGroup147.Text = "Báo cáo quyết toán TT39";
            this.explorerBar1.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup132,
            explorerBarGroup133,
            explorerBarGroup134,
            explorerBarGroup135,
            explorerBarGroup136,
            explorerBarGroup137,
            explorerBarGroup138,
            explorerBarGroup139,
            explorerBarGroup140,
            explorerBarGroup141,
            explorerBarGroup142,
            explorerBarGroup143,
            explorerBarGroup144,
            explorerBarGroup145,
            explorerBarGroup146,
            explorerBarGroup147});
            this.explorerBar1.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar1.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBar1.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBar1.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBar1.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBar1.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBar1.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBar1.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBar1.Location = new System.Drawing.Point(0, 0);
            this.explorerBar1.Name = "explorerBar1";
            this.explorerBar1.Size = new System.Drawing.Size(244, 172);
            this.explorerBar1.TabIndex = 0;
            this.explorerBar1.Text = "explorerBar2";
            this.explorerBar1.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBar1.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expGiaCong_ItemClick);
            // 
            // uiPanel2
            // 
            this.uiPanel2.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel2.Image")));
            this.uiPanel2.InnerContainer = this.uiPanel2Container;
            this.uiPanel2.Location = new System.Drawing.Point(0, 0);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(246, 203);
            this.uiPanel2.TabIndex = 4;
            this.uiPanel2.Text = "Theo dõi phân bổ";
            // 
            // uiPanel2Container
            // 
            this.uiPanel2Container.Controls.Add(this.explorerBar2);
            this.uiPanel2Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel2Container.Name = "uiPanel2Container";
            this.uiPanel2Container.Size = new System.Drawing.Size(244, 172);
            this.uiPanel2Container.TabIndex = 0;
            // 
            // explorerBar2
            // 
            this.explorerBar2.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar2.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar2.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBar2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBar2.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem409.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem409.Image")));
            explorerBarItem409.Key = "ThucHienPhanBo";
            explorerBarItem409.Text = "Phân bổ tờ khai xuất";
            explorerBarItem410.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem410.Image")));
            explorerBarItem410.Key = "TheoDoiPhanBo";
            explorerBarItem410.Text = "Theo dõi phân bổ";
            explorerBarItem410.Visible = false;
            explorerBarItem411.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem411.Image")));
            explorerBarItem411.Key = "QuanLyNPLTon";
            explorerBarItem411.Text = "Quản lý NPL Tồn";
            explorerBarGroup148.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem409,
            explorerBarItem410,
            explorerBarItem411});
            explorerBarGroup148.Key = "grpTKLoaiHinhKhac";
            explorerBarGroup148.Text = "Quản lý phân bổ tờ khai xuất";
            this.explorerBar2.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup148});
            this.explorerBar2.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar2.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBar2.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBar2.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBar2.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBar2.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBar2.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBar2.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar2.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBar2.Location = new System.Drawing.Point(0, 0);
            this.explorerBar2.Name = "explorerBar2";
            this.explorerBar2.Size = new System.Drawing.Size(244, 172);
            this.explorerBar2.TabIndex = 2;
            this.explorerBar2.Text = "explorerBar1";
            this.explorerBar2.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBar2.VisualStyleManager = this.vsmMain;
            this.explorerBar2.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expKhaiBao_TheoDoi_ItemClick);
            // 
            // uiPanel3
            // 
            this.uiPanel3.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel3.Image")));
            this.uiPanel3.InnerContainer = this.uiPanel3Container;
            this.uiPanel3.Location = new System.Drawing.Point(0, 0);
            this.uiPanel3.Name = "uiPanel3";
            this.uiPanel3.Size = new System.Drawing.Size(246, 203);
            this.uiPanel3.TabIndex = 4;
            this.uiPanel3.Text = "VNACCS - Tờ khai";
            // 
            // uiPanel3Container
            // 
            this.uiPanel3Container.Controls.Add(this.explorerBarVNACCS_TKMD);
            this.uiPanel3Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel3Container.Name = "uiPanel3Container";
            this.uiPanel3Container.Size = new System.Drawing.Size(244, 172);
            this.uiPanel3Container.TabIndex = 0;
            // 
            // explorerBarVNACCS_TKMD
            // 
            this.explorerBarVNACCS_TKMD.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_TKMD.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_TKMD.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_TKMD.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarVNACCS_TKMD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarVNACCS_TKMD.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem232.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem232.Image")));
            explorerBarItem232.Key = "KhaiBaoTEA";
            explorerBarItem232.Text = "Khai báo";
            explorerBarItem233.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem233.Image")));
            explorerBarItem233.Key = "TheoDoiTEA";
            explorerBarItem233.Text = "Theo dõi";
            explorerBarGroup83.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem232,
            explorerBarItem233});
            explorerBarGroup83.Key = "grpTEA";
            explorerBarGroup83.Text = "Hàng miễn thuế (TEA)";
            explorerBarItem234.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem234.Image")));
            explorerBarItem234.Key = "KhaiBaoTIA";
            explorerBarItem234.Text = "Khai báo";
            explorerBarItem412.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem412.Image")));
            explorerBarItem412.Key = "TheoDoiTIA";
            explorerBarItem412.Text = "Theo dõi";
            explorerBarGroup149.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem234,
            explorerBarItem412});
            explorerBarGroup149.Key = "grpTIA";
            explorerBarGroup149.Text = "Hàng tạm nhập/ tái xuất (TIA)";
            explorerBarItem413.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem413.Image")));
            explorerBarItem413.Key = "ToKhaiNhap";
            explorerBarItem413.Text = "Tờ khai nhập (IDA)";
            explorerBarItem414.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem414.Image")));
            explorerBarItem414.Key = "ToKhaiXuat";
            explorerBarItem414.Text = "Tờ khai xuất (EDA)";
            explorerBarItem415.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem415.Image")));
            explorerBarItem415.Key = "TheoDoiToKhai";
            explorerBarItem415.Text = "Theo dõi ";
            explorerBarItem416.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem416.Image")));
            explorerBarItem416.Key = "TheoDoiChungTu";
            explorerBarItem416.Text = "Theo dõi chứng từ tờ khai";
            explorerBarGroup150.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem413,
            explorerBarItem414,
            explorerBarItem415,
            explorerBarItem416});
            explorerBarGroup150.Key = "grpToKhaiVNACC";
            explorerBarGroup150.Text = "Tờ khai mậu dich VNACCS";
            explorerBarItem417.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem417.Image")));
            explorerBarItem417.Key = "ToKhaiNhapTriGiaThap";
            explorerBarItem417.Text = "Tờ khai nhập ";
            explorerBarItem418.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem418.Image")));
            explorerBarItem418.Key = "ToKhaiXuatTriGiaThap";
            explorerBarItem418.Text = "Tờ khai xuất";
            explorerBarItem419.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem419.Image")));
            explorerBarItem419.Key = "TheoDoiTKTG";
            explorerBarItem419.Text = "Theo dõi";
            explorerBarGroup151.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem417,
            explorerBarItem418,
            explorerBarItem419});
            explorerBarGroup151.Key = "grpToKhaiTriGia";
            explorerBarGroup151.Text = "Tờ khai trị giá thấp (MIC)";
            explorerBarItem420.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem420.Image")));
            explorerBarItem420.Key = "KhaiBao_TKVC";
            explorerBarItem420.Text = "Khai báo tờ khai vận chuyển";
            explorerBarItem421.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem421.Image")));
            explorerBarItem421.Key = "TheoDoi_TKVC";
            explorerBarItem421.Text = "Theo dõi";
            explorerBarGroup152.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem420,
            explorerBarItem421});
            explorerBarGroup152.Key = "grpToKhaiVanChuyen";
            explorerBarGroup152.Text = "Tờ khai vận chuyển (OLA)";
            explorerBarItem422.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem422.Image")));
            explorerBarItem422.Key = "KhaiBao_TK_KhaiBoSung_ThueHangHoa";
            explorerBarItem422.Text = "Khai báo";
            explorerBarItem423.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem423.Image")));
            explorerBarItem423.Key = "TheoDoi_TK_KhaiBoSung_ThueHangHoa";
            explorerBarItem423.Text = "Theo dõi";
            explorerBarGroup153.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem422,
            explorerBarItem423});
            explorerBarGroup153.Key = "grpTKKhaiBoSung";
            explorerBarGroup153.Text = "Sửa đổi/ bổ sung tờ khai (AMA)";
            explorerBarItem424.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem424.Image")));
            explorerBarItem424.Key = "KhaiBao_ChungTuKem_HYS";
            explorerBarItem424.Text = "Khai báo đính kèm điện tử";
            explorerBarItem425.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem425.Image")));
            explorerBarItem425.Key = "KhaiBao_ChungTuKem_MSB";
            explorerBarItem425.Text = "MSB";
            explorerBarItem426.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem426.Image")));
            explorerBarItem426.Key = "TheoDoi_ChungTuKem";
            explorerBarItem426.Text = "Theo dõi";
            explorerBarItem427.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem427.Image")));
            explorerBarItem427.Key = "cmdSignFile";
            explorerBarItem427.Text = "Ký chữ ký số cho File đính kèm";
            explorerBarGroup154.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem424,
            explorerBarItem425,
            explorerBarItem426,
            explorerBarItem427});
            explorerBarGroup154.Key = "grpChungTuDinhKem";
            explorerBarGroup154.Text = "Chứng từ đính kèm";
            explorerBarItem428.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem428.Image")));
            explorerBarItem428.Key = "KhaiBao_CTTT_IAS";
            explorerBarItem428.Text = "Chứng từ bảo lãnh";
            explorerBarItem429.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem429.Image")));
            explorerBarItem429.Key = "KhaiBao_CTTT_IBA";
            explorerBarItem429.Text = "Hạn mức ngân hàng";
            explorerBarItem430.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem430.Image")));
            explorerBarItem430.Key = "TheoDoi_ChungTuThanhToan";
            explorerBarItem430.Text = "Theo dõi";
            explorerBarItem430.Visible = false;
            explorerBarGroup155.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem428,
            explorerBarItem429,
            explorerBarItem430});
            explorerBarGroup155.Key = "grpChungTuThanhToan";
            explorerBarGroup155.Text = "Chứng từ thanh toán";
            explorerBarItem431.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem431.Image")));
            explorerBarItem431.Key = "KhaiBao_PK";
            explorerBarItem431.Text = "Khai báo";
            explorerBarItem432.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem432.Image")));
            explorerBarItem432.Key = "TheoDoi_PK";
            explorerBarItem432.Text = "Theo dõi";
            explorerBarGroup156.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem431,
            explorerBarItem432});
            explorerBarGroup156.Key = "KhoCFS";
            explorerBarGroup156.Text = "Khai báo hàng vào  kho CFS";
            explorerBarItem433.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem433.Image")));
            explorerBarItem433.Key = "KhaiBao_TKVCQKVGS";
            explorerBarItem433.Text = "Khai báo";
            explorerBarItem434.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem434.Image")));
            explorerBarItem434.Key = "TheoDoi_TKVCQKVGS";
            explorerBarItem434.Text = "Theo dõi";
            explorerBarGroup157.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem433,
            explorerBarItem434});
            explorerBarGroup157.Key = "grpToKhaiVCQuaKVGS";
            explorerBarGroup157.Text = "Tờ khai vận chuyển qua KVGS";
            explorerBarItem435.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem435.Image")));
            explorerBarItem435.Key = "KhaiBao_KhoCFS";
            explorerBarItem435.Text = "Khai báo";
            explorerBarItem436.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem436.Image")));
            explorerBarItem436.Key = "TheoDoi_KhoCFS";
            explorerBarItem436.Text = "Theo dõi";
            explorerBarGroup158.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem435,
            explorerBarItem436});
            explorerBarGroup158.Key = "grpKhaiBaoKhoCFS";
            explorerBarGroup158.Text = "Khai báo hàng vào kho CFS";
            explorerBarItem437.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem437.Image")));
            explorerBarItem437.Key = "cmdKhaiBaoDinhDanh";
            explorerBarItem437.Text = "Khai báo lấy số định danh hàng hóa";
            explorerBarItem438.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem438.Image")));
            explorerBarItem438.Key = "cmdTheoDoiDinhDanh";
            explorerBarItem438.Text = "Theo dõi";
            explorerBarGroup159.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem437,
            explorerBarItem438});
            explorerBarGroup159.Key = "grpKhaiBaoDDHH";
            explorerBarGroup159.Text = "Định danh hàng hóa";
            explorerBarItem439.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem439.Image")));
            explorerBarItem439.Key = "cmdKhaiBaoTachVanDon";
            explorerBarItem439.Text = "Khai báo";
            explorerBarItem440.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem440.Image")));
            explorerBarItem440.Key = "cmdTheoDoiTachVanDon";
            explorerBarItem440.Text = "Theo dõi";
            explorerBarGroup160.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem439,
            explorerBarItem440});
            explorerBarGroup160.Key = "grpTachVanDon";
            explorerBarGroup160.Text = "Tách vận đơn";
            explorerBarItem441.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem441.Image")));
            explorerBarItem441.Key = "cmdHangContainer";
            explorerBarItem441.Text = "Tờ khai nộp phí hàng Container";
            explorerBarItem442.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem442.Image")));
            explorerBarItem442.Key = "cmdHangRoi";
            explorerBarItem442.Text = "Tờ khai nộp phí hàng rời , lỏng";
            explorerBarItem443.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem443.Image")));
            explorerBarItem443.Key = "cmdDanhSachTKNP";
            explorerBarItem443.Text = "Danh sách tờ khai";
            explorerBarItem444.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem444.Image")));
            explorerBarItem444.Key = "cmdTraCuuBL";
            explorerBarItem444.Text = "Tra cứu biên lai";
            explorerBarItem445.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem445.Image")));
            explorerBarItem445.Key = "cmdRegisterInformation";
            explorerBarItem445.Text = "Đăng ký tài khoản";
            explorerBarItem446.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem446.Image")));
            explorerBarItem446.Key = "cmdRegisterManagement";
            explorerBarItem446.Text = "Quản lý doanh nghiệp";
            explorerBarGroup161.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem441,
            explorerBarItem442,
            explorerBarItem443,
            explorerBarItem444,
            explorerBarItem445,
            explorerBarItem446});
            explorerBarGroup161.Key = "grpThuPhiHP";
            explorerBarGroup161.Text = "Quản lý thu phí";
            this.explorerBarVNACCS_TKMD.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup83,
            explorerBarGroup149,
            explorerBarGroup150,
            explorerBarGroup151,
            explorerBarGroup152,
            explorerBarGroup153,
            explorerBarGroup154,
            explorerBarGroup155,
            explorerBarGroup156,
            explorerBarGroup157,
            explorerBarGroup158,
            explorerBarGroup159,
            explorerBarGroup160,
            explorerBarGroup161});
            this.explorerBarVNACCS_TKMD.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_TKMD.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarVNACCS_TKMD.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarVNACCS_TKMD.Location = new System.Drawing.Point(0, 0);
            this.explorerBarVNACCS_TKMD.Name = "explorerBarVNACCS_TKMD";
            this.explorerBarVNACCS_TKMD.Size = new System.Drawing.Size(244, 172);
            this.explorerBarVNACCS_TKMD.TabIndex = 1;
            this.explorerBarVNACCS_TKMD.Text = "explorerBar2";
            this.explorerBarVNACCS_TKMD.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarVNACCS_TKMD.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarVNACCS_TKMD_ItemClick);
            // 
            // uiPanel4
            // 
            this.uiPanel4.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel4.Image")));
            this.uiPanel4.InnerContainer = this.uiPanel4Container;
            this.uiPanel4.Location = new System.Drawing.Point(0, 0);
            this.uiPanel4.Name = "uiPanel4";
            this.uiPanel4.Size = new System.Drawing.Size(246, 203);
            this.uiPanel4.TabIndex = 4;
            this.uiPanel4.Text = "VNACCS - Giấy phép";
            // 
            // uiPanel4Container
            // 
            this.uiPanel4Container.Controls.Add(this.explorerBarVNACCS_GiayPhep);
            this.uiPanel4Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel4Container.Name = "uiPanel4Container";
            this.uiPanel4Container.Size = new System.Drawing.Size(244, 172);
            this.uiPanel4Container.TabIndex = 0;
            // 
            // explorerBarVNACCS_GiayPhep
            // 
            this.explorerBarVNACCS_GiayPhep.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_GiayPhep.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_GiayPhep.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_GiayPhep.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarVNACCS_GiayPhep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarVNACCS_GiayPhep.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem447.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem447.Image")));
            explorerBarItem447.Key = "KhaiBao_GiayPhep_SEA";
            explorerBarItem447.Text = "Khai báo";
            explorerBarItem448.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem448.Image")));
            explorerBarItem448.Key = "TheoDoi_GiayPhep_SEA";
            explorerBarItem448.Text = "Theo dõi";
            explorerBarGroup162.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem447,
            explorerBarItem448});
            explorerBarGroup162.Key = "grpGiayPhep";
            explorerBarGroup162.Text = "Giấy phép vật liệu nổ";
            explorerBarItem449.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem449.Image")));
            explorerBarItem449.Key = "KhaiBao_GiayPhep_SFA";
            explorerBarItem449.Text = "Khai báo";
            explorerBarItem450.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem450.Image")));
            explorerBarItem450.Key = "TheoDoi_GiayPhep_SFA";
            explorerBarItem450.Text = "Theo dõi";
            explorerBarGroup163.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem449,
            explorerBarItem450});
            explorerBarGroup163.Key = "grpGiayPhepSFA";
            explorerBarGroup163.Text = "Giấy phép thực phẩm nhập khẩu";
            explorerBarItem451.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem451.Image")));
            explorerBarItem451.Key = "KhaiBao_GiayPhep_SAA";
            explorerBarItem451.Text = "Khai báo";
            explorerBarItem452.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem452.Image")));
            explorerBarItem452.Key = "TheoDoi_GiayPhep_SAA";
            explorerBarItem452.Text = "Theo dõi";
            explorerBarGroup164.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem451,
            explorerBarItem452});
            explorerBarGroup164.Key = "grpGiayPhepSAA";
            explorerBarGroup164.Text = "Giấy phép kiểm dịch động vật";
            explorerBarItem453.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem453.Image")));
            explorerBarItem453.Key = "KhaiBao_GiayPhep_SMA";
            explorerBarItem453.Text = "Khai báo";
            explorerBarItem454.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem454.Image")));
            explorerBarItem454.Key = "TheoDoi_GiayPhep_SMA";
            explorerBarItem454.Text = "Theo dõi";
            explorerBarGroup165.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem453,
            explorerBarItem454});
            explorerBarGroup165.Key = "grpGiayPhepSMA";
            explorerBarGroup165.Text = "Giấy phép nhập khẩu thuốc";
            this.explorerBarVNACCS_GiayPhep.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup162,
            explorerBarGroup163,
            explorerBarGroup164,
            explorerBarGroup165});
            this.explorerBarVNACCS_GiayPhep.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_GiayPhep.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarVNACCS_GiayPhep.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarVNACCS_GiayPhep.Location = new System.Drawing.Point(0, 0);
            this.explorerBarVNACCS_GiayPhep.Name = "explorerBarVNACCS_GiayPhep";
            this.explorerBarVNACCS_GiayPhep.Size = new System.Drawing.Size(244, 172);
            this.explorerBarVNACCS_GiayPhep.TabIndex = 1;
            this.explorerBarVNACCS_GiayPhep.Text = "explorerBar3";
            this.explorerBarVNACCS_GiayPhep.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarVNACCS_GiayPhep.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarVNACCS_GiayPhep_ItemClick);
            // 
            // uiPanel5
            // 
            this.uiPanel5.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel5.Image")));
            this.uiPanel5.InnerContainer = this.uiPanel5Container;
            this.uiPanel5.Location = new System.Drawing.Point(0, 0);
            this.uiPanel5.Name = "uiPanel5";
            this.uiPanel5.Size = new System.Drawing.Size(246, 203);
            this.uiPanel5.TabIndex = 4;
            this.uiPanel5.Text = "VNACCS - Hóa đơn";
            // 
            // uiPanel5Container
            // 
            this.uiPanel5Container.Controls.Add(this.explorerBarVNACCS_HoaDon);
            this.uiPanel5Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel5Container.Name = "uiPanel5Container";
            this.uiPanel5Container.Size = new System.Drawing.Size(244, 172);
            this.uiPanel5Container.TabIndex = 0;
            // 
            // explorerBarVNACCS_HoaDon
            // 
            this.explorerBarVNACCS_HoaDon.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_HoaDon.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_HoaDon.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_HoaDon.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarVNACCS_HoaDon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarVNACCS_HoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem455.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem455.Image")));
            explorerBarItem455.Key = "KhaiBaoHoaDon";
            explorerBarItem455.Text = "Khai báo";
            explorerBarItem456.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem456.Image")));
            explorerBarItem456.Key = "TheoDoiHoaDon";
            explorerBarItem456.Text = "Theo dõi";
            explorerBarGroup166.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem455,
            explorerBarItem456});
            explorerBarGroup166.Key = "grpHoaDon";
            explorerBarGroup166.Text = "Hóa đơn";
            this.explorerBarVNACCS_HoaDon.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup166});
            this.explorerBarVNACCS_HoaDon.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_HoaDon.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarVNACCS_HoaDon.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarVNACCS_HoaDon.Location = new System.Drawing.Point(0, 0);
            this.explorerBarVNACCS_HoaDon.Name = "explorerBarVNACCS_HoaDon";
            this.explorerBarVNACCS_HoaDon.Size = new System.Drawing.Size(244, 172);
            this.explorerBarVNACCS_HoaDon.TabIndex = 1;
            this.explorerBarVNACCS_HoaDon.Text = "explorerBar4";
            this.explorerBarVNACCS_HoaDon.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarVNACCS_HoaDon.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarVNACCS_HoaDon_ItemClick);
            // 
            // uiPanel6
            // 
            this.uiPanel6.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel6.Image")));
            this.uiPanel6.InnerContainer = this.uiPanel6Container;
            this.uiPanel6.Location = new System.Drawing.Point(0, 0);
            this.uiPanel6.Name = "uiPanel6";
            this.uiPanel6.Size = new System.Drawing.Size(246, 203);
            this.uiPanel6.TabIndex = 4;
            this.uiPanel6.Text = "VNACCS -Chứng từ";
            this.uiPanel6.Visible = false;
            // 
            // uiPanel6Container
            // 
            this.uiPanel6Container.Controls.Add(this.explorerBarVNACCS_ChungTu);
            this.uiPanel6Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel6Container.Name = "uiPanel6Container";
            this.uiPanel6Container.Size = new System.Drawing.Size(244, 172);
            this.uiPanel6Container.TabIndex = 0;
            // 
            // explorerBarVNACCS_ChungTu
            // 
            this.explorerBarVNACCS_ChungTu.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_ChungTu.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_ChungTu.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_ChungTu.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarVNACCS_ChungTu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarVNACCS_ChungTu.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem457.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem457.Image")));
            explorerBarItem457.Key = "Send";
            explorerBarItem457.Text = "Khai báo";
            explorerBarItem458.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem458.Image")));
            explorerBarItem458.Key = "Manager";
            explorerBarItem458.Text = "Theo dõi";
            explorerBarGroup167.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem457,
            explorerBarItem458});
            explorerBarGroup167.Key = "Group1";
            explorerBarGroup167.Text = "THÔNG BÁO CƠ SỞ SẢN XUẤT";
            explorerBarItem459.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem459.Image")));
            explorerBarItem459.Key = "PhieuNhapKho";
            explorerBarItem459.Text = "Phiếu nhập kho";
            explorerBarItem460.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem460.Image")));
            explorerBarItem460.Key = "PhieuXuatKho";
            explorerBarItem460.Text = "Phiếu xuất kho";
            explorerBarItem461.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem461.Image")));
            explorerBarItem461.Key = "TheoDoiPhieuKho";
            explorerBarItem461.Text = "Theo dõi";
            explorerBarGroup168.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem459,
            explorerBarItem460,
            explorerBarItem461});
            explorerBarGroup168.Key = "Group2";
            explorerBarGroup168.Text = "PHIẾU NHẬP XUẤT KHO";
            explorerBarItem462.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem462.Image")));
            explorerBarItem462.Key = "cmdBaoCaoChotTon";
            explorerBarItem462.Text = "Khai báo";
            explorerBarItem463.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem463.Image")));
            explorerBarItem463.Key = "cmdTheoDoiBCCT";
            explorerBarItem463.Text = "Theo dõi";
            explorerBarGroup169.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem462,
            explorerBarItem463});
            explorerBarGroup169.Key = "Group3";
            explorerBarGroup169.Text = "BÁO CÁO CHỐT TỒN";
            explorerBarItem464.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem464.Image")));
            explorerBarItem464.Key = "cmdQuanLyMessage";
            explorerBarItem464.Text = "Danh sách";
            explorerBarGroup170.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem464});
            explorerBarGroup170.Key = "Group4";
            explorerBarGroup170.Text = "Quản lý Message khai báo chứng từ";
            this.explorerBarVNACCS_ChungTu.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup167,
            explorerBarGroup168,
            explorerBarGroup169,
            explorerBarGroup170});
            this.explorerBarVNACCS_ChungTu.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_ChungTu.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarVNACCS_ChungTu.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarVNACCS_ChungTu.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarVNACCS_ChungTu.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarVNACCS_ChungTu.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarVNACCS_ChungTu.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarVNACCS_ChungTu.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_ChungTu.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarVNACCS_ChungTu.Location = new System.Drawing.Point(0, 0);
            this.explorerBarVNACCS_ChungTu.Name = "explorerBarVNACCS_ChungTu";
            this.explorerBarVNACCS_ChungTu.Size = new System.Drawing.Size(244, 172);
            this.explorerBarVNACCS_ChungTu.TabIndex = 2;
            this.explorerBarVNACCS_ChungTu.Text = "explorerBar4";
            this.explorerBarVNACCS_ChungTu.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarVNACCS_ChungTu.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarVNACCS_ChungTu_ItemClick);
            // 
            // uiPanel7
            // 
            this.uiPanel7.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel7.Image")));
            this.uiPanel7.InnerContainer = this.uiPanel7Container;
            this.uiPanel7.Location = new System.Drawing.Point(0, 0);
            this.uiPanel7.Name = "uiPanel7";
            this.uiPanel7.Size = new System.Drawing.Size(246, 203);
            this.uiPanel7.TabIndex = 4;
            this.uiPanel7.Text = "Báo cáo Tổng hợp dữ liệu";
            // 
            // uiPanel7Container
            // 
            this.uiPanel7Container.Controls.Add(this.explorerBarVNACC_KetXuatDuLieu);
            this.uiPanel7Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel7Container.Name = "uiPanel7Container";
            this.uiPanel7Container.Size = new System.Drawing.Size(244, 172);
            this.uiPanel7Container.TabIndex = 0;
            // 
            // explorerBarVNACC_KetXuatDuLieu
            // 
            this.explorerBarVNACC_KetXuatDuLieu.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACC_KetXuatDuLieu.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACC_KetXuatDuLieu.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarVNACC_KetXuatDuLieu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarVNACC_KetXuatDuLieu.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem465.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem465.Image")));
            explorerBarItem465.Key = "ToKhaiXuatNhapKhau";
            explorerBarItem465.Text = "Thống kê Tờ khai xuất nhập khẩu";
            explorerBarItem466.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem466.Image")));
            explorerBarItem466.Key = "ToKhaiAMA";
            explorerBarItem466.Text = "Thống kê Tờ khai AMA";
            explorerBarItem467.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem467.Image")));
            explorerBarItem467.Key = "TongHangHoaXNK";
            explorerBarItem467.Text = "Báo cáo Tổng hàng hóa xuất nhập khẩu";
            explorerBarItem468.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem468.Image")));
            explorerBarItem468.Key = "ChiTietHangHoaXNK";
            explorerBarItem468.Text = "Báo cáo Chi tiết hàng hóa xuất nhập khẩu";
            explorerBarGroup171.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem465,
            explorerBarItem466,
            explorerBarItem467,
            explorerBarItem468});
            explorerBarGroup171.Key = "grpTK";
            explorerBarGroup171.Text = "Tờ khai VNACCS";
            explorerBarItem469.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem469.Image")));
            explorerBarItem469.Key = "TongHopXNT";
            explorerBarItem469.Text = "Tổng hợp xuất nhập tồn";
            explorerBarItem470.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem470.Image")));
            explorerBarItem470.Key = "QuyDoiNPLSP";
            explorerBarItem470.Text = "Bảng quy đổi nguyên phụ liệu từ thành phẩm";
            explorerBarGroup172.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem469,
            explorerBarItem470});
            explorerBarGroup172.Key = "grpTongHop";
            explorerBarGroup172.Text = "Tổng hợp báo cáo tờ khai VNACCS";
            explorerBarItem471.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem471.Image")));
            explorerBarItem471.Key = "ThongKeTKXNK";
            explorerBarItem471.Text = "Thống kê tờ khai XNK";
            explorerBarItem472.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem472.Image")));
            explorerBarItem472.Key = "BaoCaoTongHopHHXNK";
            explorerBarItem472.Text = "Báo cáo Tổng hợp hàng hóa xuất nhập khẩu";
            explorerBarItem473.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem473.Image")));
            explorerBarItem473.Key = "BaoCaoChiTietHHXNK";
            explorerBarItem473.Text = "Báo cáo Chi tiết hàng hóa xuất nhập khẩu";
            explorerBarGroup173.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem471,
            explorerBarItem472,
            explorerBarItem473});
            explorerBarGroup173.Key = "grpTKMD";
            explorerBarGroup173.Text = "Tờ khai mậu dịch V4";
            this.explorerBarVNACC_KetXuatDuLieu.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup171,
            explorerBarGroup172,
            explorerBarGroup173});
            this.explorerBarVNACC_KetXuatDuLieu.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACC_KetXuatDuLieu.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarVNACC_KetXuatDuLieu.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarVNACC_KetXuatDuLieu.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarVNACC_KetXuatDuLieu.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarVNACC_KetXuatDuLieu.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarVNACC_KetXuatDuLieu.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarVNACC_KetXuatDuLieu.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACC_KetXuatDuLieu.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarVNACC_KetXuatDuLieu.Location = new System.Drawing.Point(0, 0);
            this.explorerBarVNACC_KetXuatDuLieu.Name = "explorerBarVNACC_KetXuatDuLieu";
            this.explorerBarVNACC_KetXuatDuLieu.Size = new System.Drawing.Size(244, 172);
            this.explorerBarVNACC_KetXuatDuLieu.TabIndex = 3;
            this.explorerBarVNACC_KetXuatDuLieu.Text = "explorerBar1";
            this.explorerBarVNACC_KetXuatDuLieu.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarVNACC_KetXuatDuLieu.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarVNACC_KetXuatDuLieu_ItemClick);
            // 
            // uiPanel8
            // 
            this.uiPanel8.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel8.Image")));
            this.uiPanel8.InnerContainer = this.uiPanel8Container;
            this.uiPanel8.Location = new System.Drawing.Point(0, 0);
            this.uiPanel8.Name = "uiPanel8";
            this.uiPanel8.Size = new System.Drawing.Size(246, 203);
            this.uiPanel8.TabIndex = 4;
            this.uiPanel8.Text = "Kho kế toán";
            // 
            // uiPanel8Container
            // 
            this.uiPanel8Container.Controls.Add(this.explorerBarKhoKeToan);
            this.uiPanel8Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel8Container.Name = "uiPanel8Container";
            this.uiPanel8Container.Size = new System.Drawing.Size(244, 172);
            this.uiPanel8Container.TabIndex = 0;
            // 
            // explorerBarKhoKeToan
            // 
            this.explorerBarKhoKeToan.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarKhoKeToan.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarKhoKeToan.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarKhoKeToan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarKhoKeToan.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem474.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem474.Image")));
            explorerBarItem474.Key = "cmdCauHinh";
            explorerBarItem474.Text = "Cấu hình";
            explorerBarGroup174.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem474});
            explorerBarGroup174.Key = "grpCauHinh";
            explorerBarGroup174.Text = "Cấu hình thông số mặc định";
            explorerBarItem475.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem475.Image")));
            explorerBarItem475.ImageIndex = 0;
            explorerBarItem475.Key = "cmdDanhSachKho";
            explorerBarItem475.Text = "Thêm mới";
            explorerBarItem476.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem476.Image")));
            explorerBarItem476.ImageIndex = 1;
            explorerBarItem476.Key = "cmdTheoDoiKho";
            explorerBarItem476.Text = "Theo dõi";
            explorerBarGroup175.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem475,
            explorerBarItem476});
            explorerBarGroup175.Key = "grpKho";
            explorerBarGroup175.Text = "Danh sách kho";
            explorerBarItem477.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem477.Image")));
            explorerBarItem477.Key = "cmdKhoNPL";
            explorerBarItem477.Text = "Danh mục Nguyên phụ liệu";
            explorerBarItem478.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem478.Image")));
            explorerBarItem478.Key = "cmdKhoSanPham";
            explorerBarItem478.Text = "Danh mục Sản phẩm";
            explorerBarItem479.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem479.Image")));
            explorerBarItem479.Key = "cmdKhoThietBi";
            explorerBarItem479.Text = "Danh mục Thiết bị";
            explorerBarItem480.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem480.Image")));
            explorerBarItem480.Key = "cmdKhoHangMau";
            explorerBarItem480.Text = "Danh mục Hàng mẫu";
            explorerBarGroup176.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem477,
            explorerBarItem478,
            explorerBarItem479,
            explorerBarItem480});
            explorerBarGroup176.Key = "grpDanhMucKho";
            explorerBarGroup176.Text = "Danh mục";
            explorerBarItem481.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem481.Image")));
            explorerBarItem481.Key = "cmdPhieuNhapKho";
            explorerBarItem481.Text = "Tạo mới";
            explorerBarItem482.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem482.Image")));
            explorerBarItem482.Key = "cmdTheoDoiPNK";
            explorerBarItem482.Text = "Theo dõi";
            explorerBarGroup177.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem481,
            explorerBarItem482});
            explorerBarGroup177.Key = "grpPhieuNhapKho";
            explorerBarGroup177.Text = "Phiếu Nhập kho";
            explorerBarItem483.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem483.Image")));
            explorerBarItem483.Key = "cmdPhieuXuatKho";
            explorerBarItem483.Text = "Tạo mới";
            explorerBarItem484.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem484.Image")));
            explorerBarItem484.Key = "cmdTheoDoiPXK";
            explorerBarItem484.Text = "Theo dõi";
            explorerBarGroup178.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem483,
            explorerBarItem484});
            explorerBarGroup178.Key = "grpPhieuXuatKho";
            explorerBarGroup178.Text = "Phiếu Xuất kho";
            explorerBarItem485.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem485.Image")));
            explorerBarItem485.Key = "cmdThemMoiDM";
            explorerBarItem485.Text = "Thêm mới";
            explorerBarItem486.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem486.Image")));
            explorerBarItem486.Key = "cmdTheoDoiDinhMuc";
            explorerBarItem486.Text = "Theo dõi định mức";
            explorerBarItem487.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem487.Image")));
            explorerBarItem487.Key = "cmdTinhToan";
            explorerBarItem487.Text = "Tính toán định mức";
            explorerBarGroup179.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem485,
            explorerBarItem486,
            explorerBarItem487});
            explorerBarGroup179.Key = "grpDinhMuc";
            explorerBarGroup179.Text = "Định mức";
            explorerBarItem488.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem488.Image")));
            explorerBarItem488.Key = "cmdCapNhatTonDK";
            explorerBarItem488.Text = "Cập nhật Tồn đầu kỳ";
            explorerBarItem489.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem489.Image")));
            explorerBarItem489.Key = "cmdTheKho";
            explorerBarItem489.Text = "Báo cáo Thẻ kho";
            explorerBarItem490.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem490.Image")));
            explorerBarItem490.Key = "cmdBaoCaoQT";
            explorerBarItem490.Text = "Báo cáo Quyết toán";
            explorerBarGroup180.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem488,
            explorerBarItem489,
            explorerBarItem490});
            explorerBarGroup180.Key = "grpBaoCao";
            explorerBarGroup180.Text = "Báo cáo Kho kế toán";
            this.explorerBarKhoKeToan.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup174,
            explorerBarGroup175,
            explorerBarGroup176,
            explorerBarGroup177,
            explorerBarGroup178,
            explorerBarGroup179,
            explorerBarGroup180});
            this.explorerBarKhoKeToan.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarKhoKeToan.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarKhoKeToan.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarKhoKeToan.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarKhoKeToan.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarKhoKeToan.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarKhoKeToan.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarKhoKeToan.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarKhoKeToan.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarKhoKeToan.Location = new System.Drawing.Point(0, 0);
            this.explorerBarKhoKeToan.Name = "explorerBarKhoKeToan";
            this.explorerBarKhoKeToan.Size = new System.Drawing.Size(244, 172);
            this.explorerBarKhoKeToan.TabIndex = 1;
            this.explorerBarKhoKeToan.Text = "explorerBar2";
            this.explorerBarKhoKeToan.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarKhoKeToan.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarKhoKeToan_ItemClick);
            // 
            // pnlSXXK
            // 
            this.pnlSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSXXK.Icon")));
            this.pnlSXXK.InnerContainer = this.pnlSXXKContainer;
            this.pnlSXXK.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXK.Name = "pnlSXXK";
            this.pnlSXXK.Size = new System.Drawing.Size(209, 239);
            this.pnlSXXK.TabIndex = 4;
            this.pnlSXXK.Text = "Loại hình SXXK";
            // 
            // pnlSXXKContainer
            // 
            this.pnlSXXKContainer.Controls.Add(this.expSXXK);
            this.pnlSXXKContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXKContainer.Name = "pnlSXXKContainer";
            this.pnlSXXKContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlSXXKContainer.TabIndex = 0;
            // 
            // expSXXK
            // 
            this.expSXXK.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expSXXK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expSXXK.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarGroup181.Expanded = false;
            explorerBarItem491.Key = "nplKhaiBao";
            explorerBarItem491.Text = "Khai báo ";
            explorerBarItem492.Key = "nplTheoDoi";
            explorerBarItem492.Text = "Theo dõi";
            explorerBarItem493.Key = "nplDaDangKy";
            explorerBarItem493.Text = "Đã đăng ký";
            explorerBarGroup181.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem491,
            explorerBarItem492,
            explorerBarItem493});
            explorerBarGroup181.Key = "grpNguyenPhuLieu";
            explorerBarGroup181.Text = "Nguyên phụ liệu";
            explorerBarGroup182.Expanded = false;
            explorerBarItem494.Key = "spKhaiBao";
            explorerBarItem494.Text = "Khai báo";
            explorerBarItem495.Key = "spTheoDoi";
            explorerBarItem495.Text = "Theo dõi";
            explorerBarItem496.Key = "spDaDangKy";
            explorerBarItem496.Text = "Đã đăng ký";
            explorerBarGroup182.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem494,
            explorerBarItem495,
            explorerBarItem496});
            explorerBarGroup182.Key = "grpSanPham";
            explorerBarGroup182.Text = "Sản phẩm";
            explorerBarItem497.Key = "dmKhaiBao";
            explorerBarItem497.Text = "Khai báo";
            explorerBarItem498.Key = "dmTheoDoi";
            explorerBarItem498.Text = "Theo dõi";
            explorerBarItem499.Key = "dmDaDangKy";
            explorerBarItem499.Text = "Đã đăng ký";
            explorerBarGroup183.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem497,
            explorerBarItem498,
            explorerBarItem499});
            explorerBarGroup183.Key = "grpDinhMuc";
            explorerBarGroup183.Text = "Định mức";
            explorerBarItem500.Key = "tkNhapKhau";
            explorerBarItem500.Text = "Nhập khẩu";
            explorerBarItem501.Key = "tkXuatKhau";
            explorerBarItem501.Text = "Xuất khẩu";
            explorerBarItem502.Key = "TheoDoiTKSXXK";
            explorerBarItem502.Text = "Theo dõi ";
            explorerBarItem503.Key = "ToKhaiSXXKDangKy";
            explorerBarItem503.Text = "Đã đăng ký";
            explorerBarItem504.Key = "tkHetHan";
            explorerBarItem504.Text = "Săp hết hạn TK";
            explorerBarGroup184.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem500,
            explorerBarItem501,
            explorerBarItem502,
            explorerBarItem503,
            explorerBarItem504});
            explorerBarGroup184.Key = "grpToKhai";
            explorerBarGroup184.Text = "Tờ khai";
            explorerBarItem505.Key = "AddHSTL";
            explorerBarItem505.Text = "Tạo mới";
            explorerBarItem506.Key = "UpdateHSTL";
            explorerBarItem506.Text = "Cập nhật";
            explorerBarItem507.Key = "HSTLManage";
            explorerBarItem507.Text = "Theo dõi";
            explorerBarItem508.Key = "HSTLClosed";
            explorerBarItem508.Text = "Đã đóng";
            explorerBarGroup185.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem505,
            explorerBarItem506,
            explorerBarItem507,
            explorerBarItem508});
            explorerBarGroup185.Key = "grpThanhLy";
            explorerBarGroup185.Text = "Hồ sơ thanh lý";
            this.expSXXK.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup181,
            explorerBarGroup182,
            explorerBarGroup183,
            explorerBarGroup184,
            explorerBarGroup185});
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expSXXK.ImageSize = new System.Drawing.Size(16, 16);
            this.expSXXK.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expSXXK.Location = new System.Drawing.Point(0, 0);
            this.expSXXK.Name = "expSXXK";
            this.expSXXK.Size = new System.Drawing.Size(209, 239);
            this.expSXXK.TabIndex = 0;
            this.expSXXK.Text = "explorerBar1";
            this.expSXXK.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expSXXK.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expSXXK_ItemClick);
            // 
            // pnlGiaCong
            // 
            this.pnlGiaCong.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlGiaCong.Icon")));
            this.pnlGiaCong.InnerContainer = this.pnlGiaCongContainer;
            this.pnlGiaCong.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCong.Name = "pnlGiaCong";
            this.pnlGiaCong.Size = new System.Drawing.Size(209, 239);
            this.pnlGiaCong.TabIndex = 4;
            this.pnlGiaCong.Text = "Loại hình gia công";
            // 
            // pnlGiaCongContainer
            // 
            this.pnlGiaCongContainer.Controls.Add(this.expGiaCong);
            this.pnlGiaCongContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCongContainer.Name = "pnlGiaCongContainer";
            this.pnlGiaCongContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlGiaCongContainer.TabIndex = 0;
            // 
            // expGiaCong
            // 
            this.expGiaCong.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expGiaCong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expGiaCong.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem509.Key = "hdgcNhap";
            explorerBarItem509.Text = "Khai báo";
            explorerBarItem510.Key = "hdgcManage";
            explorerBarItem510.Text = "Theo dõi";
            explorerBarItem511.Key = "hdgcRegisted";
            explorerBarItem511.Text = "Đã đăng ký";
            explorerBarGroup186.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem509,
            explorerBarItem510,
            explorerBarItem511});
            explorerBarGroup186.Key = "grpHopDong";
            explorerBarGroup186.Text = "Hợp đồng";
            explorerBarItem512.Key = "dmSend";
            explorerBarItem512.Text = "Khai báo";
            explorerBarItem513.Key = "dmManage";
            explorerBarItem513.Text = "Theo dõi";
            explorerBarItem514.Key = "dmRegisted";
            explorerBarItem514.Text = "Đã đăng ký";
            explorerBarGroup187.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem512,
            explorerBarItem513,
            explorerBarItem514});
            explorerBarGroup187.Key = "grpDinhMuc";
            explorerBarGroup187.Text = "Định mức";
            explorerBarItem515.Key = "pkgcNhap";
            explorerBarItem515.Text = "Khai báo";
            explorerBarItem516.Key = "pkgcManage";
            explorerBarItem516.Text = "Theo dõi";
            explorerBarItem517.Key = "pkgcRegisted";
            explorerBarItem517.Text = "Đã đăng ký";
            explorerBarGroup188.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem515,
            explorerBarItem516,
            explorerBarItem517});
            explorerBarGroup188.Key = "grpPhuKien";
            explorerBarGroup188.Text = "Phụ kiện";
            explorerBarItem518.Key = "tkNhapKhau_GC";
            explorerBarItem518.Text = "Nhập khẩu";
            explorerBarItem519.Key = "tkXuatKhau_GC";
            explorerBarItem519.Text = "Xuất khẩu";
            explorerBarGroup189.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem518,
            explorerBarItem519});
            explorerBarGroup189.Key = "grpToKhai";
            explorerBarGroup189.Text = "Tờ khai";
            explorerBarItem520.Key = "tkGCCTNhap";
            explorerBarItem520.Text = "Tờ khai GCCT nhập";
            explorerBarItem521.Key = "tkGCCTXuat";
            explorerBarItem521.Text = "Tờ khai GCCT xuất";
            explorerBarItem522.Key = "theodoiTKCT";
            explorerBarItem522.Text = "Theo dõi";
            explorerBarGroup190.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem520,
            explorerBarItem521,
            explorerBarItem522});
            explorerBarGroup190.Key = "grpGCCT";
            explorerBarGroup190.Text = "Tờ khai GCCT";
            this.expGiaCong.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup186,
            explorerBarGroup187,
            explorerBarGroup188,
            explorerBarGroup189,
            explorerBarGroup190});
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expGiaCong.ImageSize = new System.Drawing.Size(16, 16);
            this.expGiaCong.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expGiaCong.Location = new System.Drawing.Point(0, 0);
            this.expGiaCong.Name = "expGiaCong";
            this.expGiaCong.Size = new System.Drawing.Size(209, 239);
            this.expGiaCong.TabIndex = 1;
            this.expGiaCong.Text = "explorerBar2";
            this.expGiaCong.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expGiaCong.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expGiaCong_ItemClick);
            // 
            // pnlKinhDoanh
            // 
            this.pnlKinhDoanh.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlKinhDoanh.Icon")));
            this.pnlKinhDoanh.InnerContainer = this.pnlKinhDoanhContainer;
            this.pnlKinhDoanh.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanh.Name = "pnlKinhDoanh";
            this.pnlKinhDoanh.Size = new System.Drawing.Size(209, 239);
            this.pnlKinhDoanh.TabIndex = 4;
            this.pnlKinhDoanh.Text = "Loại hình kinh doanh";
            // 
            // pnlKinhDoanhContainer
            // 
            this.pnlKinhDoanhContainer.Controls.Add(this.expKD);
            this.pnlKinhDoanhContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanhContainer.Name = "pnlKinhDoanhContainer";
            this.pnlKinhDoanhContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlKinhDoanhContainer.TabIndex = 0;
            // 
            // expKD
            // 
            this.expKD.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKD.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem523.Key = "tkNhapKhau_KD";
            explorerBarItem523.Text = "Nhập khẩu";
            explorerBarItem524.Key = "tkXuatKhau_KD";
            explorerBarItem524.Text = "Xuất khẩu";
            explorerBarGroup191.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem523,
            explorerBarItem524});
            explorerBarGroup191.Key = "grpToKhai";
            explorerBarGroup191.Text = "Tờ khai";
            this.expKD.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup191});
            this.expKD.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKD.ImageSize = new System.Drawing.Size(16, 16);
            this.expKD.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKD.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKD.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKD.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKD.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKD.Location = new System.Drawing.Point(0, 0);
            this.expKD.Name = "expKD";
            this.expKD.Size = new System.Drawing.Size(209, 239);
            this.expKD.TabIndex = 1;
            this.expKD.Text = "explorerBar1";
            this.expKD.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            // 
            // pnlDauTu
            // 
            this.pnlDauTu.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlDauTu.Icon")));
            this.pnlDauTu.InnerContainer = this.pnlDauTuContainer;
            this.pnlDauTu.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTu.Name = "pnlDauTu";
            this.pnlDauTu.Size = new System.Drawing.Size(209, 239);
            this.pnlDauTu.TabIndex = 4;
            this.pnlDauTu.Text = "Loại hình đầu tư";
            // 
            // pnlDauTuContainer
            // 
            this.pnlDauTuContainer.Controls.Add(this.expDT);
            this.pnlDauTuContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTuContainer.Name = "pnlDauTuContainer";
            this.pnlDauTuContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlDauTuContainer.TabIndex = 0;
            // 
            // expDT
            // 
            this.expDT.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expDT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expDT.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem525.Key = "tkNhapKhau_DT";
            explorerBarItem525.Text = "Nhập khẩu";
            explorerBarItem526.Key = "tkXuatKhau_DT";
            explorerBarItem526.Text = "Xuất khẩu";
            explorerBarGroup192.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem525,
            explorerBarItem526});
            explorerBarGroup192.Key = "grpToKhai";
            explorerBarGroup192.Text = "Tờ khai";
            this.expDT.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup192});
            this.expDT.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expDT.ImageSize = new System.Drawing.Size(16, 16);
            this.expDT.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expDT.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expDT.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expDT.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expDT.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expDT.Location = new System.Drawing.Point(0, 0);
            this.expDT.Name = "expDT";
            this.expDT.Size = new System.Drawing.Size(209, 239);
            this.expDT.TabIndex = 2;
            this.expDT.Text = "explorerBar1";
            this.expDT.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            // 
            // pnlSend
            // 
            this.pnlSend.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSend.Icon")));
            this.pnlSend.InnerContainer = this.pnlSendContainer;
            this.pnlSend.Location = new System.Drawing.Point(0, 0);
            this.pnlSend.Name = "pnlSend";
            this.pnlSend.Size = new System.Drawing.Size(209, 239);
            this.pnlSend.TabIndex = 4;
            this.pnlSend.Text = "Khai báo / Theo dõi tờ khai";
            // 
            // pnlSendContainer
            // 
            this.pnlSendContainer.Controls.Add(this.expKhaiBao_TheoDoi);
            this.pnlSendContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSendContainer.Name = "pnlSendContainer";
            this.pnlSendContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlSendContainer.TabIndex = 0;
            // 
            // expKhaiBao_TheoDoi
            // 
            this.expKhaiBao_TheoDoi.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKhaiBao_TheoDoi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKhaiBao_TheoDoi.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem527.Key = "tkManage";
            explorerBarItem527.Text = "Theo dõi tờ khai";
            explorerBarItem528.Key = "tkDaDangKy";
            explorerBarItem528.Text = "Tờ khai đã đăng ký";
            explorerBarGroup193.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem527,
            explorerBarItem528});
            explorerBarGroup193.Key = "grpToKhai";
            explorerBarGroup193.Text = "Tờ khai";
            explorerBarItem529.Key = "tdNPLton";
            explorerBarItem529.Text = "Theo dõi NPL Tồn";
            explorerBarItem530.Key = "tkNPLton";
            explorerBarItem530.Text = "Thống kê NPL tồn";
            explorerBarGroup194.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem529,
            explorerBarItem530});
            explorerBarGroup194.Key = "grpNPLTon";
            explorerBarGroup194.Text = "Nguyên Phụ Liệu Tồn";
            explorerBarItem531.Key = "tkNhap";
            explorerBarItem531.Text = "Theo tờ khai nhập";
            explorerBarItem532.Key = "tkXuat";
            explorerBarItem532.Text = "Theo tờ khai xuất";
            explorerBarGroup195.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem531,
            explorerBarItem532});
            explorerBarGroup195.Key = "grpPhanBo";
            explorerBarGroup195.Text = "Theo dõi phân bổ NPL";
            explorerBarGroup195.Visible = false;
            explorerBarItem533.Key = "tkLHKNhap";
            explorerBarItem533.Text = "Tờ Khai Nhập";
            explorerBarItem534.Key = "tkLHKXuat";
            explorerBarItem534.Text = "Tờ Khai Xuất";
            explorerBarGroup196.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem533,
            explorerBarItem534});
            explorerBarGroup196.Key = "grpTKLoaiHinhKhac";
            explorerBarGroup196.Text = "Tờ khai nhập từ hệ thống khác";
            this.expKhaiBao_TheoDoi.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup193,
            explorerBarGroup194,
            explorerBarGroup195,
            explorerBarGroup196});
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKhaiBao_TheoDoi.ImageSize = new System.Drawing.Size(16, 16);
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKhaiBao_TheoDoi.Location = new System.Drawing.Point(0, 0);
            this.expKhaiBao_TheoDoi.Name = "expKhaiBao_TheoDoi";
            this.expKhaiBao_TheoDoi.Size = new System.Drawing.Size(209, 239);
            this.expKhaiBao_TheoDoi.TabIndex = 1;
            this.expKhaiBao_TheoDoi.Text = "explorerBar1";
            this.expKhaiBao_TheoDoi.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expKhaiBao_TheoDoi.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expKhaiBao_TheoDoi_ItemClick);
            // 
            // ilMedium
            // 
            this.ilMedium.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilMedium.ImageSize = new System.Drawing.Size(24, 24);
            this.ilMedium.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // ilLarge
            // 
            this.ilLarge.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilLarge.ImageSize = new System.Drawing.Size(24, 24);
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 531);
            this.statusBar.Name = "statusBar";
            uiStatusBarPanel17.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel17.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel17.DrawBorder = false;
            uiStatusBarPanel17.Image = ((System.Drawing.Image)(resources.GetObject("uiStatusBarPanel17.Image")));
            uiStatusBarPanel17.Key = "DoanhNghiep";
            uiStatusBarPanel17.ProgressBarValue = 0;
            uiStatusBarPanel17.Width = 152;
            uiStatusBarPanel18.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel18.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel18.DrawBorder = false;
            uiStatusBarPanel18.Image = ((System.Drawing.Image)(resources.GetObject("uiStatusBarPanel18.Image")));
            uiStatusBarPanel18.Key = "HaiQuan";
            uiStatusBarPanel18.ProgressBarValue = 0;
            uiStatusBarPanel18.ToggleKeyValue = Janus.Windows.UI.StatusBar.ToggleKeyValue.NumLock;
            uiStatusBarPanel18.Width = 150;
            uiStatusBarPanel19.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel19.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel19.FormatStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiStatusBarPanel19.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiStatusBarPanel19.Image = ((System.Drawing.Image)(resources.GetObject("uiStatusBarPanel19.Image")));
            uiStatusBarPanel19.Key = "Support";
            uiStatusBarPanel19.ProgressBarValue = 0;
            uiStatusBarPanel19.Text = "   GỬI YÊU CẦU HỖ TRỢ";
            uiStatusBarPanel19.Width = 150;
            uiStatusBarPanel20.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel20.DrawBorder = false;
            uiStatusBarPanel20.Image = ((System.Drawing.Image)(resources.GetObject("uiStatusBarPanel20.Image")));
            uiStatusBarPanel20.Key = "Terminal";
            uiStatusBarPanel20.ProgressBarValue = 0;
            uiStatusBarPanel20.Text = "Termina ID: ?";
            uiStatusBarPanel21.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel21.DrawBorder = false;
            uiStatusBarPanel21.Image = ((System.Drawing.Image)(resources.GetObject("uiStatusBarPanel21.Image")));
            uiStatusBarPanel21.Key = "Service";
            uiStatusBarPanel21.ProgressBarValue = 0;
            uiStatusBarPanel22.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            uiStatusBarPanel22.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel22.DrawBorder = false;
            uiStatusBarPanel22.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            uiStatusBarPanel22.Key = "Version";
            uiStatusBarPanel22.ProgressBarValue = 0;
            uiStatusBarPanel22.Text = "V?";
            uiStatusBarPanel22.Width = 30;
            uiStatusBarPanel23.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            uiStatusBarPanel23.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel23.DrawBorder = false;
            uiStatusBarPanel23.Key = "CKS";
            uiStatusBarPanel23.ProgressBarValue = 0;
            uiStatusBarPanel23.Text = "CKS";
            uiStatusBarPanel23.Width = 30;
            uiStatusBarPanel24.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel24.DrawBorder = false;
            uiStatusBarPanel24.Key = "DateTime";
            uiStatusBarPanel24.ProgressBarValue = 0;
            uiStatusBarPanel24.Text = "08/11/2013 09:41:00 AM";
            this.statusBar.Panels.AddRange(new Janus.Windows.UI.StatusBar.UIStatusBarPanel[] {
            uiStatusBarPanel17,
            uiStatusBarPanel18,
            uiStatusBarPanel19,
            uiStatusBarPanel20,
            uiStatusBarPanel21,
            uiStatusBarPanel22,
            uiStatusBarPanel23,
            uiStatusBarPanel24});
            this.statusBar.PanelsBorderColor = System.Drawing.SystemColors.ControlDark;
            this.statusBar.Size = new System.Drawing.Size(847, 23);
            this.statusBar.TabIndex = 7;
            this.statusBar.MouseLeave += new System.EventHandler(this.statusBar_MouseLeave);
            this.statusBar.PanelClick += new Janus.Windows.UI.StatusBar.StatusBarEventHandler(this.statusBar_PanelClick);
            this.statusBar.MouseHover += new System.EventHandler(this.statusBar_MouseHover);
            // 
            // cmdThoat3
            // 
            this.cmdThoat3.Key = "cmdThoat";
            this.cmdThoat3.Name = "cmdThoat3";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cmdPerformanceDatabase
            // 
            this.cmdPerformanceDatabase.Image = ((System.Drawing.Image)(resources.GetObject("cmdPerformanceDatabase.Image")));
            this.cmdPerformanceDatabase.Key = "cmdPerformanceDatabase";
            this.cmdPerformanceDatabase.Name = "cmdPerformanceDatabase";
            this.cmdPerformanceDatabase.Text = "Tối ưu hoá cơ sở dữ liệu";
            // 
            // cmdPerformanceDatabase1
            // 
            this.cmdPerformanceDatabase1.Key = "cmdPerformanceDatabase";
            this.cmdPerformanceDatabase1.Name = "cmdPerformanceDatabase1";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(847, 554);
            this.Controls.Add(this.uiPanel0);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "THÔNG QUAN ĐIỆN TỬ - GIA CÔNG 5.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.statusBar, 0);
            this.Controls.SetChildIndex(this.uiPanel0, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).EndInit();
            this.uiPanel2.ResumeLayout(false);
            this.uiPanel2Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel3)).EndInit();
            this.uiPanel3.ResumeLayout(false);
            this.uiPanel3Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_TKMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel4)).EndInit();
            this.uiPanel4.ResumeLayout(false);
            this.uiPanel4Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_GiayPhep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel5)).EndInit();
            this.uiPanel5.ResumeLayout(false);
            this.uiPanel5Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_HoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel6)).EndInit();
            this.uiPanel6.ResumeLayout(false);
            this.uiPanel6Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_ChungTu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel7)).EndInit();
            this.uiPanel7.ResumeLayout(false);
            this.uiPanel7Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACC_KetXuatDuLieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel8)).EndInit();
            this.uiPanel8.ResumeLayout(false);
            this.uiPanel8Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarKhoKeToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).EndInit();
            this.pnlSXXK.ResumeLayout(false);
            this.pnlSXXKContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).EndInit();
            this.pnlGiaCong.ResumeLayout(false);
            this.pnlGiaCongContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).EndInit();
            this.pnlKinhDoanh.ResumeLayout(false);
            this.pnlKinhDoanhContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).EndInit();
            this.pnlDauTu.ResumeLayout(false);
            this.pnlDauTuContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).EndInit();
            this.pnlSend.ResumeLayout(false);
            this.pnlSendContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UICommand cmdThoat3;
        private UIPanelInnerContainer pnlDauTuContainer;
        private ExplorerBar expDT;
        private UIPanel pnlSend;
        private UIPanelInnerContainer pnlSendContainer;
        private ExplorerBar expKhaiBao_TheoDoi;
        private NotifyIcon notifyIcon1;
        private UICommand NhacNho;
        private UICommand DongBoDuLieu;
        private UICommand cmdImport;
        private UICommand cmdImportNPL1;
        private UICommand cmdImportSP1;
        private UICommand cmdImportNPL;
        private UICommand cmdImportSP;
        private UICommand cmdImportDM1;
        private UICommand cmdImportDM;
        private UICommand cmdImportToKhai1;
        private UICommand cmdImportTTDM;
        private UICommand cmdImportToKhai;
        private UICommand cmdImportHangHoa1;
        private UICommand cmdImportHangHoa;
        private UICommand Command01;
        private UICommand Command0;
        private UICommand cmdHelp1;
        private UICommand cmdAbout1;
        private UICommand cmdHelp;
        private UICommand cmdAbout;
        private UICommand Command11;
        private UICommand Command1;
        private UICommand cmd20071;
        private UICommand cmd2007;
        private UICommand cmd2003;
        private UICommand cmd20031;
        private UICommand cmdNPLNhapTon;
        private UICommand cmdDanhMuc1;
        private UICommand cmdDanhMuc;
        private UICommand cmdMaHS;
        private UICommand cmdHaiQuan1;
        private UICommand cmdNuoc1;
        private UICommand cmdMaHS1;
        private UICommand cmdNguyenTe1;
        private UICommand cmdDVT1;
        private UICommand cmdPTTT1;
        private UICommand cmdPTVT1;
        private UICommand cmdDKGH1;
        private UICommand cmdCuaKhau1;
        private UICommand cmdNuoc;
        private UICommand cmdHaiQuan;
        private UICommand cmdNguyenTe;
        private UICommand cmdDVT;
        private UICommand cmdPTTT;
        private UICommand cmdPTVT;
        private UICommand cmdDKGH;
        private UICommand cmdCuaKhau;
        private UICommand cmdBackUp1;
        private UICommand cmdBackUp;
        private UICommand cmdRestore;
        private UICommand ThongSoKetNoi;
        private UICommand TLThongTinDNHQ;
        private UICommand cmdThietLapIn;
        private UICommand cmdExportExccel;
        private UICommand cmdExportExcel1;
        private UICommand cmdImportExcel;
        private UICommand cmdDongBoPhongKhai;
        private UICommand cmdImportExcel1;
        private UICommand cmdExportExccel1;
        private UICommand cmdCauHinh1;
        private UICommand cmdCauHinh;
        private UICommand ThongSoKetNoi1;
        private UICommand TLThongTinDNHQ1;
        private UICommand cmdCauHinhToKhai1;
        private UICommand cmdThietLapIn1;
        private UICommand cmdCauHinhToKhai;
        public Janus.Windows.UI.StatusBar.UIStatusBar statusBar;
        private UICommand QuanTri1;
        private UICommand QuanTri;
        private UICommand QuanLyNguoiDung1;
        private UICommand QuanLyNhom1;
        private UICommand QuanLyNguoiDung;
        private UICommand QuanLyNhom;
        private UICommand LoginUser1;
        private UICommand LoginUser;
        private UICommand cmdChangePass1;
        private UICommand cmdChangePass;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private ExplorerBar explorerBar1;
        private UIPanel uiPanel2;
        private UIPanelInnerContainer uiPanel2Container;
        private ExplorerBar explorerBar2;
        private UICommand TraCuuMaHS;
        private UICommand cmdAutoUpdate1;
        private UICommand cmdAutoUpdate;
        private UICommand NhapXML;
        private UICommand NhapHDXML;
        private UICommand NhapDMXML;
        private UICommand NhapPKXML;
        private UICommand XuatTKXML;
        private UICommand XuatTKMD1;
        private UICommand XuatTKGCCT1;
        private UICommand XuatTKMD;
        private UICommand XuatTKGCCT;
        private UICommand NhapTKXML;
        private UICommand NhapTKMD1;
        private UICommand NhapTKGCCT1;
        private UICommand NhapTKMD;
        private UICommand NhapTKGCCT;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private UICommand cmdVN1;
        private UICommand cmdEnglish;
        private UICommand cmdVN;
        private UICommand cmdEng1;
        private UICommand cmdNhapXuat;
        private UICommand NhapXML2;
        private UICommand cmdXuatDuLieuPhongKHai1;
        private UICommand cmdXuatDuLieuPhongKHai;
        private UICommand cmdXuatHopDong1;
        private UICommand cmdXuatDinhMuc1;
        private UICommand cmdXuatPhuKien1;
        private UICommand cmdXuatToKhai1;
        private UICommand cmdXuatHopDong;
        private UICommand cmdXuatDinhMuc;
        private UICommand cmdXuatPhuKien;
        private UICommand cmdXuatToKhai;
        private UICommand Separator1;
        private UICommand cmdActivate1;
        private UICommand cmdActivate;
        private UICommand XuatTKGCCT2;
        private UICommand XuatTKMD2;
        private UICommand cmdDMSPGC1;
        private UICommand cmdDMSPGC;
        private UICommand cmdNhapDuLieuDaiLy;
        private UICommand cmdCloseMe;
        private UICommand cmdCloseAllButMe;
        private UICommand cmdCloseAll;
        private UIContextMenu mnuRightClick;
        private UICommand cmdCloseMe1;
        private UICommand cmdCloseAllButMe1;
        private UICommand cmdCloseAll1;
        private BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer timer1;
        private UICommand cmdXuatDN1;
        private UICommand cmdXuatDN;
        private UICommand cmdNhapDuLieuTuDoangNghiep1;
        private UICommand QuanLyMess;
        private UICommand cmdNhapDuLieuTuDoangNghiep;
        private UICommand cmdNhapHopDong1;
        private UICommand cmdNhapDinhMuc1;
        private UICommand cmdNhapPhuKien1;
        private UICommand cmdNhapToKhai1;
        private UICommand cmdNhapHopDong;
        private UICommand cmdNhapDinhMuc;
        private UICommand cmdNhapPhuKien;
        private UICommand cmdNhapToKhai;
        private UICommand cmdNhapToKhaiGCCT1;
        private UICommand cmdNhapToKhaiMauDich1;
        private UICommand cmdNhapToKhaiMauDich;
        private UICommand cmdNhapToKhaiGCCT;
        private UICommand cmdTLTTDN1;
        private UICommand cmdTLTTDN;
        private UICommand mnuQuerySQL;
        private UICommand mnuQuerySQL1;
        private UICommand cmdLog1;
        private UICommand cmdLog;
        private UICommand cmdDataVersion1;
        private UICommand cmdDataVersion;
        private UICommand cmdChuKySo1;
        private UICommand cmdChuKySo;
        private UICommand cmdTimer1;
        private UICommand cmdTimer;
        private UICommand DongBoDuLieu1;
        private UICommand cmdNhomCuaKhau1;
        private UICommand cmdNhomCuaKhau;
        private UICommand Separator3;
        private UICommand Separator4;
        private UICommand cmdNhapXuat2;
        private UICommand cmdGetCategoryOnline;
        private UICommand cmdBieuThue;
        private UICommand cmdBieuThue1;
        private UICommand TraCuuMaHS1;
        private UICommand Separator5;
        private UICommand cmdTraCuuXNKOnline1;
        private UICommand cmdTraCuuNoThueOnline1;
        private UICommand cmdTraCuuVanBanOnline1;
        private UICommand cmdTuVanHQOnline1;
        private UICommand cmdTraCuuXNKOnline;
        private UICommand cmdTraCuuNoThueOnline;
        private UICommand cmdTraCuuVanBanOnline;
        private UICommand cmdTuVanHQOnline;
        private UICommand cmdGetCategoryOnline1;
        private UICommand Separator6;
        private UICommand Separator7;
        private UICommand Separator8;
        private UICommand cmdTeamview1;
        private UICommand Separator9;
        private UICommand cmdTeamview;
        private UICommand cmdCapNhatHS;
        private UICommand cmdCapNhatHS8Auto;
        private UICommand cmdCapNhatHS8SoManual;
        private UICommand cmdTool;
        private UICommand cmdImageResizeHelp;
        private UICommand cmdCapNhatHS1;
        private UICommand Separator10;
        private UICommand Separator11;
        private UICommand cmdTool1;
        private UICommand Separator12;
        private UICommand cmdCapNhatHS8Auto1;
        private UICommand cmdCapNhatHS8SoManual1;
        private UICommand cmdImageResizeHelp1;
        private UICommand cmdDaily1;
        private UICommand Separator13;
        private UICommand cmdDaily;
        private UICommand cmdUpdateDatabase1;
        private UICommand cmdUpdateDatabase;
        private UIPanel uiPanel3;
        private UIPanelInnerContainer uiPanel3Container;
        private UIPanel uiPanel4;
        private UIPanelInnerContainer uiPanel4Container;
        private UIPanel uiPanel5;
        private UIPanelInnerContainer uiPanel5Container;
        private ExplorerBar explorerBarVNACCS_HoaDon;
        private ExplorerBar explorerBarVNACCS_GiayPhep;
        private ExplorerBar explorerBarVNACCS_TKMD;
        private UICommand cmdHelpVideo1;
        private UICommand cmdHelpVideo;
        private UICommand cmdHDSDCKS1;
        private UICommand cmdHDSDCKS;
        private UICommand cmdHDSDVNACCS1;
        private UICommand cmdHDSDVNACCS;
        private UICommand cmdThongBaoVNACCS1;
        private UICommand cmdThongBaoVNACCS;
        private UIPanel uiPanel6;
        private UIPanelInnerContainer uiPanel6Container;
        private ExplorerBar explorerBarVNACCS_ChungTu;
        private UICommand cmdSignFile1;
        private UICommand cmdHelpSignFile1;
        private UICommand cmdSignFile;
        private UICommand cmdHelpSignFile;
        private UICommand cmdBerth;
        private UICommand cmdCargo;
        private UICommand cmdCityUNLOCODE;
        private UICommand cmdCommon;
        private UICommand cmdContainerSize;
        private UICommand cmdCustomsSubSection;
        private UICommand cmdOGAUser;
        private UICommand cmdPackagesUnit;
        private UICommand cmdStations;
        private UICommand cmdTaxClassificationCode;
        private UICommand cmdBerth1;
        private UICommand cmdCargo1;
        private UICommand cmdCityUNLOCODE1;
        private UICommand cmdCommon1;
        private UICommand cmdContainerSize1;
        private UICommand cmdCustomsSubSection1;
        private UICommand cmdOGAUser1;
        private UICommand cmdStations1;
        private UICommand cmdTaxClassificationCode1;
        private UICommand cmdReloadData;
        private UICommand cmdReloadData1;
        private UICommand cmdBieuThueXNK2018;
        private UICommand cmdBieuThueXNK20181;
        private UICommand cmdUpdateCategoryOnline1;
        private UICommand cmdUpdateCategoryOnline;
        private UICommand cmdNhanPhanHoi;
        private UICommand cmdNhanPhanHoi1;
        private UIPanel uiPanel7;
        private UIPanelInnerContainer uiPanel7Container;
        private ExplorerBar explorerBarVNACC_KetXuatDuLieu;
        private UIPanel uiPanel8;
        private UIPanelInnerContainer uiPanel8Container;
        private ExplorerBar explorerBarKhoKeToan;
        private UICommand cmdUpdateCertificates1;
        private UICommand cmdUpdateCertificates;
        private UICommand cmdConfigReadExcel;
        private UICommand cmdConfigReadExcel1;
        private UICommand cmdPerformanceDatabase;
        private UICommand cmdPerformanceDatabase1;
    }
}
