﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.QuanLyChungTu.CX;

namespace Company.KDT.SHARE.QuanLyChungTu.CX
{
    public partial class HangDuaRaDangKy
    {
        public List<HangDuaRa> DanhSachHangDuaRa { get; set; }

        public void LoadHang()
        {
            if (ID > 0)
            {
                DanhSachHangDuaRa = new List<HangDuaRa>();
                DanhSachHangDuaRa = (List<HangDuaRa>)HangDuaRa.SelectCollectionBy_Master_ID(this.ID);
            }
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert(transaction);
                    else
                        this.Update(transaction);

                    foreach (HangDuaRa spDetail in this.DanhSachHangDuaRa)
                    {
                        if (spDetail.ID == 0)
                        {
                            spDetail.Master_ID = this.ID;
                            spDetail.ID = spDetail.Insert(transaction);
                        }
                        else
                        {                           
                            spDetail.Update(transaction);
                        }
                    }

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }
}
