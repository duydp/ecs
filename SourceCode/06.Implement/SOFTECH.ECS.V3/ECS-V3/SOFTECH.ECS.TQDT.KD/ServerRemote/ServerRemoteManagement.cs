﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.Interface;
using SignRemote;
using Company.KDT.SHARE.Components;

namespace ServerRemote
{
    public partial class ServerRemoteManagement : BaseForm
    {
        public ServerRemoteManagement()
        {
            InitializeComponent();
        }

        private void gridList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                User_DaiLy user = User_DaiLy.Load(id);
                UserDaiLyForm form = new UserDaiLyForm();
                form.user = user;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            gridList.Refresh();
            ISignMessage service = WebService.SignMessage();
            DataTable dt = service.GetAllCustommer(Globals.UserNameLogin, Globals.PasswordLogin);
            gridList.DataSource = dt;
            gridList.Refetch();
        }
        private void ServerRemoteManagement_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string where = "";
                if (txtUserName.Text.Length > 0)
                {
                    where += " USER_NAME = " + txtUserName.Text.ToString();
                }
                if (txtMaDoanhNghiep.Text.Length > 0)
                {
                    if (where.Length > 0)
                        where += " AND MaDoanhNghiep = '" + txtMaDoanhNghiep.Text.ToString()+"'";
                    where += "MaDoanhNghiep = '" + txtMaDoanhNghiep.Text.ToString()+"'";
                }
                if (txtTenDoanhNghiep.Text.Length > 0)
                {
                    if (where.Length > 0)
                        where += " AND HO_TEN = " + txtTenDoanhNghiep.Text.ToString();
                    where += "HO_TEN = " + txtTenDoanhNghiep.Text.ToString();
                }
                ISignMessage service = WebService.SignMessage();
                DataTable dt = service.SearchCustommer(where, Globals.UserNameLogin, Globals.PasswordLogin);
                gridList.Refresh();
                gridList.DataSource = dt;
                gridList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }
    }
}
