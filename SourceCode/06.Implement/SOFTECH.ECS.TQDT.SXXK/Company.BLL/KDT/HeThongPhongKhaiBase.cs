using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.BLL.KDT;

namespace Company.BLL.KDT
{
    public partial class HeThongPhongKhai
    {
        #region Private members.

        protected string _MaDoanhNghiep = String.Empty;
        protected string _PassWord = String.Empty;
        protected string _TenDoanhNghiep = String.Empty;
        protected string _Key_Config = String.Empty;
        protected string _Value_Config = String.Empty;
        protected string _Vaitro = String.Empty;
        protected short _Role;
        protected bool _UuTien = false;

        public bool UuTien
        {
            get { return _UuTien; }
            set { _UuTien = value; }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.
        public string vaitro
        {
            set { this._Vaitro = value; }
            get { return this._Vaitro; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public string PassWord
        {
            set { this._PassWord = value; }
            get { return this._PassWord; }
        }
        public string TenDoanhNghiep
        {
            set { this._TenDoanhNghiep = value; }
            get { return this._TenDoanhNghiep; }
        }
        public string Key_Config
        {
            set { this._Key_Config = value; }
            get { return this._Key_Config; }
        }
        public string Value_Config
        {
            set { this._Value_Config = value; }
            get { return this._Value_Config; }
        }
        public short Role
        {
            set { this._Role = value; }
            get { return this._Role; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");

            string spName = "p_HeThongPhongKhai_Load";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.Char, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Key_Config", SqlDbType.VarChar, this._Key_Config);

            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) this._PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Key_Config"))) this._Key_Config = reader.GetString(reader.GetOrdinal("Key_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Value_Config"))) this._Value_Config = reader.GetString(reader.GetOrdinal("Value_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Role"))) this._Role = reader.GetInt16(reader.GetOrdinal("Role"));
                if (!reader.IsDBNull(reader.GetOrdinal("UuTien"))) this._UuTien = reader.GetBoolean(reader.GetOrdinal("UuTien"));
                return true;
            }
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spName = "p_HeThongPhongKhai_SelectAll";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spName = "p_HeThongPhongKhai_SelectAll";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spName = "p_HeThongPhongKhai_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spName = "p_HeThongPhongKhai_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public HeThongPhongKhaiCollection SelectCollectionAll()
        {
            HeThongPhongKhaiCollection collection = new HeThongPhongKhaiCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                HeThongPhongKhai entity = new HeThongPhongKhai();

                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) entity.PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Key_Config"))) entity.Key_Config = reader.GetString(reader.GetOrdinal("Key_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Value_Config"))) entity.Value_Config = reader.GetString(reader.GetOrdinal("Value_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Role"))) entity.Role = reader.GetInt16(reader.GetOrdinal("Role"));
                if (!reader.IsDBNull(reader.GetOrdinal("UuTien"))) entity.UuTien = reader.GetBoolean(reader.GetOrdinal("UuTien"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public HeThongPhongKhaiCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            HeThongPhongKhaiCollection collection = new HeThongPhongKhaiCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                HeThongPhongKhai entity = new HeThongPhongKhai();

                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) entity.PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Key_Config"))) entity.Key_Config = reader.GetString(reader.GetOrdinal("Key_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Value_Config"))) entity.Value_Config = reader.GetString(reader.GetOrdinal("Value_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Role"))) entity.Role = reader.GetInt16(reader.GetOrdinal("Role"));
                if (!reader.IsDBNull(reader.GetOrdinal("UuTien"))) entity.UuTien = reader.GetBoolean(reader.GetOrdinal("UuTien"));
                collection.Add(entity);
            }
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spName = "p_HeThongPhongKhai_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.Char, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@PassWord", SqlDbType.VarChar, this._PassWord);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@Key_Config", SqlDbType.VarChar, this._Key_Config);
            db.AddInParameter(dbCommand, "@Value_Config", SqlDbType.NVarChar, this._Value_Config);
            db.AddInParameter(dbCommand, "@Role", SqlDbType.SmallInt, this._Role);
            db.AddInParameter(dbCommand, "@UuTien", SqlDbType.SmallInt, this._UuTien);

            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(HeThongPhongKhaiCollection collection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HeThongPhongKhai item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, HeThongPhongKhaiCollection collection)
        {
            foreach (HeThongPhongKhai item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spName = "p_HeThongPhongKhai_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.Char, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@PassWord", SqlDbType.VarChar, this._PassWord);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@Key_Config", SqlDbType.VarChar, this._Key_Config);
            db.AddInParameter(dbCommand, "@Value_Config", SqlDbType.NVarChar, this._Value_Config);
            db.AddInParameter(dbCommand, "@Role", SqlDbType.SmallInt, this._Role);
            db.AddInParameter(dbCommand, "@UuTien", SqlDbType.SmallInt, this._UuTien);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(HeThongPhongKhaiCollection collection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HeThongPhongKhai item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spName = "p_HeThongPhongKhai_Update";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.Char, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@PassWord", SqlDbType.VarChar, this._PassWord);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@Key_Config", SqlDbType.VarChar, this._Key_Config);
            db.AddInParameter(dbCommand, "@Value_Config", SqlDbType.NVarChar, this._Value_Config);
            db.AddInParameter(dbCommand, "@Role", SqlDbType.SmallInt, this._Role);
            db.AddInParameter(dbCommand, "@UuTien", SqlDbType.SmallInt, this._UuTien);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(HeThongPhongKhaiCollection collection, SqlTransaction transaction)
        {
            foreach (HeThongPhongKhai item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spName = "p_HeThongPhongKhai_Delete";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.Char, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Key_Config", SqlDbType.VarChar, this._Key_Config);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(HeThongPhongKhaiCollection collection, SqlTransaction transaction)
        {
            foreach (HeThongPhongKhai item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(HeThongPhongKhaiCollection collection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HeThongPhongKhai item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion
    }
}