-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]
	@ChungTuThanhToan_ID bigint,
	@SoThuTuGiaoDich numeric(4, 0),
	@NgayTaoDuLieu datetime,
	@ThoiGianTaoDuLieu datetime,
	@SoTienTruLui numeric(13, 0),
	@SoTienTangLen numeric(13, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
(
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
)
VALUES 
(
	@ChungTuThanhToan_ID,
	@SoThuTuGiaoDich,
	@NgayTaoDuLieu,
	@ThoiGianTaoDuLieu,
	@SoTienTruLui,
	@SoTienTangLen,
	@SoToKhai,
	@NgayDangKyToKhai,
	@MaLoaiHinh,
	@CoQuanHaiQuan,
	@TenCucHaiQuan
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]
	@ID bigint,
	@ChungTuThanhToan_ID bigint,
	@SoThuTuGiaoDich numeric(4, 0),
	@NgayTaoDuLieu datetime,
	@ThoiGianTaoDuLieu datetime,
	@SoTienTruLui numeric(13, 0),
	@SoTienTangLen numeric(13, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
SET
	[ChungTuThanhToan_ID] = @ChungTuThanhToan_ID,
	[SoThuTuGiaoDich] = @SoThuTuGiaoDich,
	[NgayTaoDuLieu] = @NgayTaoDuLieu,
	[ThoiGianTaoDuLieu] = @ThoiGianTaoDuLieu,
	[SoTienTruLui] = @SoTienTruLui,
	[SoTienTangLen] = @SoTienTangLen,
	[SoToKhai] = @SoToKhai,
	[NgayDangKyToKhai] = @NgayDangKyToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[TenCucHaiQuan] = @TenCucHaiQuan
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]
	@ID bigint,
	@ChungTuThanhToan_ID bigint,
	@SoThuTuGiaoDich numeric(4, 0),
	@NgayTaoDuLieu datetime,
	@ThoiGianTaoDuLieu datetime,
	@SoTienTruLui numeric(13, 0),
	@SoTienTangLen numeric(13, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] 
		SET
			[ChungTuThanhToan_ID] = @ChungTuThanhToan_ID,
			[SoThuTuGiaoDich] = @SoThuTuGiaoDich,
			[NgayTaoDuLieu] = @NgayTaoDuLieu,
			[ThoiGianTaoDuLieu] = @ThoiGianTaoDuLieu,
			[SoTienTruLui] = @SoTienTruLui,
			[SoTienTangLen] = @SoTienTangLen,
			[SoToKhai] = @SoToKhai,
			[NgayDangKyToKhai] = @NgayDangKyToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[TenCucHaiQuan] = @TenCucHaiQuan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
		(
			[ChungTuThanhToan_ID],
			[SoThuTuGiaoDich],
			[NgayTaoDuLieu],
			[ThoiGianTaoDuLieu],
			[SoTienTruLui],
			[SoTienTangLen],
			[SoToKhai],
			[NgayDangKyToKhai],
			[MaLoaiHinh],
			[CoQuanHaiQuan],
			[TenCucHaiQuan]
		)
		VALUES 
		(
			@ChungTuThanhToan_ID,
			@SoThuTuGiaoDich,
			@NgayTaoDuLieu,
			@ThoiGianTaoDuLieu,
			@SoTienTruLui,
			@SoTienTangLen,
			@SoToKhai,
			@NgayDangKyToKhai,
			@MaLoaiHinh,
			@CoQuanHaiQuan,
			@TenCucHaiQuan
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]
	@ChungTuThanhToan_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
WHERE
	[ChungTuThanhToan_ID] = @ChungTuThanhToan_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]
	@ChungTuThanhToan_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
WHERE
	[ChungTuThanhToan_ID] = @ChungTuThanhToan_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]	

GO

