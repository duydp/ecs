﻿namespace Company.Interface
{
    partial class VNACC_HoaDonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_HoaDonForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.cmdToolBar = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang2 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdPhanLoaiHoaDon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanLoaiHoaDon");
            this.cmdLuu2 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao2 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdLayPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoi");
            this.cmdPhanLoaiHoaDon = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanLoaiHoaDon");
            this.cmdLayPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoi");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbToolBar = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.cbbPhanLoaiXuatNhap = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMa_XNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTen_XNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinh_XNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi_XNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDienThoai_XNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiLap_HD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtQuocGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMa_GuiNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtten_GuiNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtMaBuuChinh_GuiNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDaiChi_2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi_1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDienThoai_GuiNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNuoc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTinh_Thanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaTT_SoTien_PhiBH = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_SoTien_FOB = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_PVC_NoiDia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_KhauTru = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_DieuChinhKhac = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_PhiVanChuyen = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_PhiXepHang2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_PhiXepHang1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_PhiBaoHiem = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_TriGiaFOB = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoTienDieuChinh_FOB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiVC_NoiDia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienKhauTru = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDieuChinhKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienDieuChinh_PhiBH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtChiPhiXepHang2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtChiPhiXepHang1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiVanChuyen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaFOB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtLoaiDieuChinhKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLoaiKhauTru = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLoaiPhiXepHang2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLoaiPhiXepHang1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNoiThanhToan_PVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTongSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrLoaiVanChuyen = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTongTheTich = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTrongLuongThuan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.clcNgayXepHang = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtTrongLuong_Gross = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrDiaDiemTrungChuyen = new Company.Interface.Controls.CuaKhauHControl();
            this.label37 = new System.Windows.Forms.Label();
            this.ctrDiaDiemXemHang = new Company.Interface.Controls.CuaKhauHControl();
            this.cbbDVT_KienHang = new Janus.Windows.EditControls.UIComboBox();
            this.cbbDVT_TheTich = new Janus.Windows.EditControls.UIComboBox();
            this.cbbDVT_TrongLuongThuan = new Janus.Windows.EditControls.UIComboBox();
            this.ctrDiaDiemDoHang = new Company.Interface.Controls.CuaKhauHControl();
            this.cbbDVT_TrongLuongGross = new Janus.Windows.EditControls.UIComboBox();
            this.txtMaKyHieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSoPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNganHang_PL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu_ChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtSoHieuChuyenDi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtTen_PTVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaTT_TongTriGia_HD = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrPTTT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txtMaDaiLy_HQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaDiemLap_HD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTongDongHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongTriGia_HD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDieuKienGia_HD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtDiaDiemGiaoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayLapHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvSoHoaDon = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNgayLapHD = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvLoaiHinh = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDiaDiemLapHD = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTongTriGiaHoaDon = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).BeginInit();
            this.cmbToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayLapHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvLoaiHinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemLapHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTongTriGiaHoaDon)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 703), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 703);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 679);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 679);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(792, 703);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 679);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(792, 24);
            this.uiGroupBox2.TabIndex = 327;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLuu.Icon")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 741);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(998, 0);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmdToolBar});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdPhanLoaiHoaDon,
            this.cmdLayPhanHoi});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.cmbToolBar;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // cmdToolBar
            // 
            this.cmdToolBar.CommandManager = this.cmbMain;
            this.cmdToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang2,
            this.cmdPhanLoaiHoaDon1,
            this.cmdLuu2,
            this.cmdKhaiBao2,
            this.cmdLayPhanHoi1});
            this.cmdToolBar.FullRow = true;
            this.cmdToolBar.Key = "CommandBar1";
            this.cmdToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmdToolBar.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmdToolBar.Name = "cmdToolBar";
            this.cmdToolBar.RowIndex = 0;
            this.cmdToolBar.Size = new System.Drawing.Size(998, 32);
            this.cmdToolBar.Tag = "0";
            this.cmdToolBar.Text = "CommandBar1";
            this.cmdToolBar.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            // 
            // cmdThemHang2
            // 
            this.cmdThemHang2.Key = "cmdThemHang";
            this.cmdThemHang2.Name = "cmdThemHang2";
            // 
            // cmdPhanLoaiHoaDon1
            // 
            this.cmdPhanLoaiHoaDon1.Key = "cmdPhanLoaiHoaDon";
            this.cmdPhanLoaiHoaDon1.Name = "cmdPhanLoaiHoaDon1";
            // 
            // cmdLuu2
            // 
            this.cmdLuu2.Key = "cmdLuu";
            this.cmdLuu2.Name = "cmdLuu2";
            // 
            // cmdKhaiBao2
            // 
            this.cmdKhaiBao2.Key = "cmdKhaiBao";
            this.cmdKhaiBao2.Name = "cmdKhaiBao2";
            // 
            // cmdLayPhanHoi1
            // 
            this.cmdLayPhanHoi1.Key = "cmdLayPhanHoi";
            this.cmdLayPhanHoi1.Name = "cmdLayPhanHoi1";
            // 
            // cmdPhanLoaiHoaDon
            // 
            this.cmdPhanLoaiHoaDon.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPhanLoaiHoaDon.Icon")));
            this.cmdPhanLoaiHoaDon.Key = "cmdPhanLoaiHoaDon";
            this.cmdPhanLoaiHoaDon.Name = "cmdPhanLoaiHoaDon";
            this.cmdPhanLoaiHoaDon.Text = "Phân loại hóa đơn";
            // 
            // cmdLayPhanHoi
            // 
            this.cmdLayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLayPhanHoi.Icon")));
            this.cmdLayPhanHoi.Key = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Name = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Text = "Lấy phản hồi";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 32);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 709);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(998, 32);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 709);
            // 
            // cmbToolBar
            // 
            this.cmbToolBar.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmdToolBar});
            this.cmbToolBar.CommandManager = this.cmbMain;
            this.cmbToolBar.Controls.Add(this.cmdToolBar);
            this.cmbToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmbToolBar.Name = "cmbToolBar";
            this.cmbToolBar.Size = new System.Drawing.Size(998, 32);
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(208, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Quốc gia";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Số tiếp nhận";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(482, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Trọng lượng(Gross)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Số hóa đơn";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Mã bưu chính";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(579, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Mã đại lý Hải quan";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Địa chỉ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(368, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(107, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Địa điểm lập hóa đơn";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Điện thoại";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(19, 71);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(125, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Phương thức thanh toán";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(206, 44);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Ngày lập";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Người lập hóa đơn";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 152);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Tỉnh/Thành";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(368, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(128, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Phân loại xuất nhập khẩu";
            // 
            // cbbPhanLoaiXuatNhap
            // 
            this.cbbPhanLoaiXuatNhap.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Xuất khẩu";
            uiComboBoxItem12.Value = "E";
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Nhập khẩu";
            uiComboBoxItem13.Value = "I";
            this.cbbPhanLoaiXuatNhap.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem12,
            uiComboBoxItem13});
            this.cbbPhanLoaiXuatNhap.Location = new System.Drawing.Point(493, 13);
            this.cbbPhanLoaiXuatNhap.Name = "cbbPhanLoaiXuatNhap";
            this.cbbPhanLoaiXuatNhap.SelectedIndex = 1;
            this.cbbPhanLoaiXuatNhap.Size = new System.Drawing.Size(84, 21);
            this.cbbPhanLoaiXuatNhap.TabIndex = 0;
            this.cbbPhanLoaiXuatNhap.Text = "Nhập khẩu";
            this.cbbPhanLoaiXuatNhap.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(232)))), ((int)(((byte)(226)))));
            this.uiGroupBox3.Controls.Add(this.panel1);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(792, 679);
            this.uiGroupBox3.TabIndex = 328;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.uiGroupBox1);
            this.panel1.Controls.Add(this.uiGroupBox4);
            this.panel1.Controls.Add(this.uiGroupBox6);
            this.panel1.Controls.Add(this.uiGroupBox5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 153);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(792, 526);
            this.panel1.TabIndex = 41;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.txtMa_XNK);
            this.uiGroupBox1.Controls.Add(this.txtTen_XNK);
            this.uiGroupBox1.Controls.Add(this.txtMaBuuChinh_XNK);
            this.uiGroupBox1.Controls.Add(this.txtDiaChi_XNK);
            this.uiGroupBox1.Controls.Add(this.txtDienThoai_XNK);
            this.uiGroupBox1.Controls.Add(this.txtNguoiLap_HD);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Location = new System.Drawing.Point(14, 9);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(378, 198);
            this.uiGroupBox1.TabIndex = 4;
            this.uiGroupBox1.Text = "Người xuất nhập khẩu";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtMa_XNK
            // 
            this.txtMa_XNK.Location = new System.Drawing.Point(116, 20);
            this.txtMa_XNK.Name = "txtMa_XNK";
            this.txtMa_XNK.Size = new System.Drawing.Size(121, 21);
            this.txtMa_XNK.TabIndex = 0;
            this.txtMa_XNK.VisualStyleManager = this.vsmMain;
            // 
            // txtTen_XNK
            // 
            this.txtTen_XNK.Location = new System.Drawing.Point(116, 45);
            this.txtTen_XNK.Name = "txtTen_XNK";
            this.txtTen_XNK.Size = new System.Drawing.Size(256, 21);
            this.txtTen_XNK.TabIndex = 1;
            this.txtTen_XNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTen_XNK.VisualStyleManager = this.vsmMain;
            // 
            // txtMaBuuChinh_XNK
            // 
            this.txtMaBuuChinh_XNK.Location = new System.Drawing.Point(116, 70);
            this.txtMaBuuChinh_XNK.Name = "txtMaBuuChinh_XNK";
            this.txtMaBuuChinh_XNK.Size = new System.Drawing.Size(121, 21);
            this.txtMaBuuChinh_XNK.TabIndex = 2;
            this.txtMaBuuChinh_XNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaBuuChinh_XNK.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChi_XNK
            // 
            this.txtDiaChi_XNK.Location = new System.Drawing.Point(116, 96);
            this.txtDiaChi_XNK.Name = "txtDiaChi_XNK";
            this.txtDiaChi_XNK.Size = new System.Drawing.Size(256, 21);
            this.txtDiaChi_XNK.TabIndex = 3;
            this.txtDiaChi_XNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChi_XNK.VisualStyleManager = this.vsmMain;
            // 
            // txtDienThoai_XNK
            // 
            this.txtDienThoai_XNK.Location = new System.Drawing.Point(116, 122);
            this.txtDienThoai_XNK.Name = "txtDienThoai_XNK";
            this.txtDienThoai_XNK.Size = new System.Drawing.Size(121, 21);
            this.txtDienThoai_XNK.TabIndex = 4;
            this.txtDienThoai_XNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDienThoai_XNK.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiLap_HD
            // 
            this.txtNguoiLap_HD.Location = new System.Drawing.Point(116, 147);
            this.txtNguoiLap_HD.Name = "txtNguoiLap_HD";
            this.txtNguoiLap_HD.Size = new System.Drawing.Size(256, 21);
            this.txtNguoiLap_HD.TabIndex = 5;
            this.txtNguoiLap_HD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNguoiLap_HD.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.txtQuocGia);
            this.uiGroupBox4.Controls.Add(this.txtMa_GuiNhan);
            this.uiGroupBox4.Controls.Add(this.label25);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Controls.Add(this.txtten_GuiNhan);
            this.uiGroupBox4.Controls.Add(this.label27);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.label24);
            this.uiGroupBox4.Controls.Add(this.txtMaBuuChinh_GuiNhan);
            this.uiGroupBox4.Controls.Add(this.txtDaiChi_2);
            this.uiGroupBox4.Controls.Add(this.txtDiaChi_1);
            this.uiGroupBox4.Controls.Add(this.txtDienThoai_GuiNhan);
            this.uiGroupBox4.Controls.Add(this.txtMaNuoc);
            this.uiGroupBox4.Controls.Add(this.txtTinh_Thanh);
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Location = new System.Drawing.Point(397, 9);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(375, 198);
            this.uiGroupBox4.TabIndex = 5;
            this.uiGroupBox4.Text = "Người gửi (Người nhận)";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtQuocGia
            // 
            this.txtQuocGia.Location = new System.Drawing.Point(257, 146);
            this.txtQuocGia.Name = "txtQuocGia";
            this.txtQuocGia.Size = new System.Drawing.Size(112, 21);
            this.txtQuocGia.TabIndex = 6;
            this.txtQuocGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtQuocGia.VisualStyleManager = this.vsmMain;
            // 
            // txtMa_GuiNhan
            // 
            this.txtMa_GuiNhan.Location = new System.Drawing.Point(93, 19);
            this.txtMa_GuiNhan.Name = "txtMa_GuiNhan";
            this.txtMa_GuiNhan.Size = new System.Drawing.Size(113, 21);
            this.txtMa_GuiNhan.TabIndex = 0;
            this.txtMa_GuiNhan.VisualStyleManager = this.vsmMain;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(9, 125);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(48, 13);
            this.label25.TabIndex = 1;
            this.label25.Text = "Địa chỉ 2";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 100);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Địa chỉ 1";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(9, 74);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Mã bưu chính";
            // 
            // txtten_GuiNhan
            // 
            this.txtten_GuiNhan.Location = new System.Drawing.Point(93, 45);
            this.txtten_GuiNhan.Name = "txtten_GuiNhan";
            this.txtten_GuiNhan.Size = new System.Drawing.Size(276, 21);
            this.txtten_GuiNhan.TabIndex = 1;
            this.txtten_GuiNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(9, 176);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(69, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "Số điện thoại";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(9, 49);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Tên";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(9, 26);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(21, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Mã";
            // 
            // txtMaBuuChinh_GuiNhan
            // 
            this.txtMaBuuChinh_GuiNhan.Location = new System.Drawing.Point(93, 70);
            this.txtMaBuuChinh_GuiNhan.Name = "txtMaBuuChinh_GuiNhan";
            this.txtMaBuuChinh_GuiNhan.Size = new System.Drawing.Size(113, 21);
            this.txtMaBuuChinh_GuiNhan.TabIndex = 2;
            this.txtMaBuuChinh_GuiNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDaiChi_2
            // 
            this.txtDaiChi_2.Location = new System.Drawing.Point(93, 121);
            this.txtDaiChi_2.Name = "txtDaiChi_2";
            this.txtDaiChi_2.Size = new System.Drawing.Size(276, 21);
            this.txtDaiChi_2.TabIndex = 4;
            this.txtDaiChi_2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChi_1
            // 
            this.txtDiaChi_1.Location = new System.Drawing.Point(93, 96);
            this.txtDiaChi_1.Name = "txtDiaChi_1";
            this.txtDiaChi_1.Size = new System.Drawing.Size(276, 21);
            this.txtDiaChi_1.TabIndex = 3;
            this.txtDiaChi_1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDienThoai_GuiNhan
            // 
            this.txtDienThoai_GuiNhan.Location = new System.Drawing.Point(93, 171);
            this.txtDienThoai_GuiNhan.Name = "txtDienThoai_GuiNhan";
            this.txtDienThoai_GuiNhan.Size = new System.Drawing.Size(113, 21);
            this.txtDienThoai_GuiNhan.TabIndex = 7;
            this.txtDienThoai_GuiNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNuoc
            // 
            this.txtMaNuoc.Location = new System.Drawing.Point(257, 170);
            this.txtMaNuoc.Name = "txtMaNuoc";
            this.txtMaNuoc.Size = new System.Drawing.Size(112, 21);
            this.txtMaNuoc.TabIndex = 8;
            this.txtMaNuoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTinh_Thanh
            // 
            this.txtTinh_Thanh.Location = new System.Drawing.Point(93, 147);
            this.txtTinh_Thanh.Name = "txtTinh_Thanh";
            this.txtTinh_Thanh.Size = new System.Drawing.Size(113, 21);
            this.txtTinh_Thanh.TabIndex = 5;
            this.txtTinh_Thanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTinh_Thanh.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(208, 174);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(48, 13);
            this.label26.TabIndex = 1;
            this.label26.Text = "Mã nước";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_SoTien_PhiBH);
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_SoTien_FOB);
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_PVC_NoiDia);
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_KhauTru);
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_DieuChinhKhac);
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_PhiVanChuyen);
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_PhiXepHang2);
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_PhiXepHang1);
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_PhiBaoHiem);
            this.uiGroupBox6.Controls.Add(this.ctrMaTT_TriGiaFOB);
            this.uiGroupBox6.Controls.Add(this.txtSoTienDieuChinh_FOB);
            this.uiGroupBox6.Controls.Add(this.txtPhiVC_NoiDia);
            this.uiGroupBox6.Controls.Add(this.txtTienKhauTru);
            this.uiGroupBox6.Controls.Add(this.txtDieuChinhKhac);
            this.uiGroupBox6.Controls.Add(this.txtSoTienDieuChinh_PhiBH);
            this.uiGroupBox6.Controls.Add(this.txtChiPhiXepHang2);
            this.uiGroupBox6.Controls.Add(this.txtChiPhiXepHang1);
            this.uiGroupBox6.Controls.Add(this.txtPhiBaoHiem);
            this.uiGroupBox6.Controls.Add(this.txtPhiVanChuyen);
            this.uiGroupBox6.Controls.Add(this.txtTriGiaFOB);
            this.uiGroupBox6.Controls.Add(this.txtLoaiDieuChinhKhac);
            this.uiGroupBox6.Controls.Add(this.txtLoaiKhauTru);
            this.uiGroupBox6.Controls.Add(this.txtLoaiPhiXepHang2);
            this.uiGroupBox6.Controls.Add(this.txtLoaiPhiXepHang1);
            this.uiGroupBox6.Controls.Add(this.txtNoiThanhToan_PVC);
            this.uiGroupBox6.Controls.Add(this.label56);
            this.uiGroupBox6.Controls.Add(this.label55);
            this.uiGroupBox6.Controls.Add(this.label54);
            this.uiGroupBox6.Controls.Add(this.label53);
            this.uiGroupBox6.Controls.Add(this.label52);
            this.uiGroupBox6.Controls.Add(this.label46);
            this.uiGroupBox6.Controls.Add(this.label51);
            this.uiGroupBox6.Controls.Add(this.label45);
            this.uiGroupBox6.Controls.Add(this.label44);
            this.uiGroupBox6.Controls.Add(this.label47);
            this.uiGroupBox6.Controls.Add(this.label49);
            this.uiGroupBox6.Controls.Add(this.label43);
            this.uiGroupBox6.Controls.Add(this.label42);
            this.uiGroupBox6.Controls.Add(this.label50);
            this.uiGroupBox6.Controls.Add(this.label48);
            this.uiGroupBox6.Location = new System.Drawing.Point(14, 446);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(762, 220);
            this.uiGroupBox6.TabIndex = 9;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaTT_SoTien_PhiBH
            // 
            this.ctrMaTT_SoTien_PhiBH.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_SoTien_PhiBH.Code = "";
            this.ctrMaTT_SoTien_PhiBH.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_SoTien_PhiBH.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_SoTien_PhiBH.IsValidate = true;
            this.ctrMaTT_SoTien_PhiBH.Location = new System.Drawing.Point(686, 34);
            this.ctrMaTT_SoTien_PhiBH.Name = "ctrMaTT_SoTien_PhiBH";
            this.ctrMaTT_SoTien_PhiBH.Name_VN = "";
            this.ctrMaTT_SoTien_PhiBH.SetValidate = false;
            this.ctrMaTT_SoTien_PhiBH.ShowColumnCode = true;
            this.ctrMaTT_SoTien_PhiBH.ShowColumnName = false;
            this.ctrMaTT_SoTien_PhiBH.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_SoTien_PhiBH.TabIndex = 74;
            this.ctrMaTT_SoTien_PhiBH.TagName = "";
            this.ctrMaTT_SoTien_PhiBH.WhereCondition = "";
            // 
            // ctrMaTT_SoTien_FOB
            // 
            this.ctrMaTT_SoTien_FOB.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_SoTien_FOB.Code = "";
            this.ctrMaTT_SoTien_FOB.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_SoTien_FOB.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_SoTien_FOB.IsValidate = true;
            this.ctrMaTT_SoTien_FOB.Location = new System.Drawing.Point(686, 8);
            this.ctrMaTT_SoTien_FOB.Name = "ctrMaTT_SoTien_FOB";
            this.ctrMaTT_SoTien_FOB.Name_VN = "";
            this.ctrMaTT_SoTien_FOB.SetValidate = false;
            this.ctrMaTT_SoTien_FOB.ShowColumnCode = true;
            this.ctrMaTT_SoTien_FOB.ShowColumnName = false;
            this.ctrMaTT_SoTien_FOB.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_SoTien_FOB.TabIndex = 74;
            this.ctrMaTT_SoTien_FOB.TagName = "";
            this.ctrMaTT_SoTien_FOB.WhereCondition = "";
            // 
            // ctrMaTT_PVC_NoiDia
            // 
            this.ctrMaTT_PVC_NoiDia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_PVC_NoiDia.Code = "";
            this.ctrMaTT_PVC_NoiDia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_PVC_NoiDia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_PVC_NoiDia.IsValidate = true;
            this.ctrMaTT_PVC_NoiDia.Location = new System.Drawing.Point(686, 190);
            this.ctrMaTT_PVC_NoiDia.Name = "ctrMaTT_PVC_NoiDia";
            this.ctrMaTT_PVC_NoiDia.Name_VN = "";
            this.ctrMaTT_PVC_NoiDia.SetValidate = false;
            this.ctrMaTT_PVC_NoiDia.ShowColumnCode = true;
            this.ctrMaTT_PVC_NoiDia.ShowColumnName = false;
            this.ctrMaTT_PVC_NoiDia.Size = new System.Drawing.Size(72, 26);
            this.ctrMaTT_PVC_NoiDia.TabIndex = 74;
            this.ctrMaTT_PVC_NoiDia.TagName = "";
            this.ctrMaTT_PVC_NoiDia.WhereCondition = "";
            // 
            // ctrMaTT_KhauTru
            // 
            this.ctrMaTT_KhauTru.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_KhauTru.Code = "";
            this.ctrMaTT_KhauTru.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_KhauTru.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_KhauTru.IsValidate = true;
            this.ctrMaTT_KhauTru.Location = new System.Drawing.Point(298, 164);
            this.ctrMaTT_KhauTru.Name = "ctrMaTT_KhauTru";
            this.ctrMaTT_KhauTru.Name_VN = "";
            this.ctrMaTT_KhauTru.SetValidate = false;
            this.ctrMaTT_KhauTru.ShowColumnCode = true;
            this.ctrMaTT_KhauTru.ShowColumnName = false;
            this.ctrMaTT_KhauTru.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_KhauTru.TabIndex = 74;
            this.ctrMaTT_KhauTru.TagName = "";
            this.ctrMaTT_KhauTru.WhereCondition = "";
            // 
            // ctrMaTT_DieuChinhKhac
            // 
            this.ctrMaTT_DieuChinhKhac.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_DieuChinhKhac.Code = "";
            this.ctrMaTT_DieuChinhKhac.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_DieuChinhKhac.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_DieuChinhKhac.IsValidate = true;
            this.ctrMaTT_DieuChinhKhac.Location = new System.Drawing.Point(298, 138);
            this.ctrMaTT_DieuChinhKhac.Name = "ctrMaTT_DieuChinhKhac";
            this.ctrMaTT_DieuChinhKhac.Name_VN = "";
            this.ctrMaTT_DieuChinhKhac.SetValidate = false;
            this.ctrMaTT_DieuChinhKhac.ShowColumnCode = true;
            this.ctrMaTT_DieuChinhKhac.ShowColumnName = false;
            this.ctrMaTT_DieuChinhKhac.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_DieuChinhKhac.TabIndex = 74;
            this.ctrMaTT_DieuChinhKhac.TagName = "";
            this.ctrMaTT_DieuChinhKhac.WhereCondition = "";
            // 
            // ctrMaTT_PhiVanChuyen
            // 
            this.ctrMaTT_PhiVanChuyen.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_PhiVanChuyen.Code = "";
            this.ctrMaTT_PhiVanChuyen.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_PhiVanChuyen.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_PhiVanChuyen.IsValidate = true;
            this.ctrMaTT_PhiVanChuyen.Location = new System.Drawing.Point(298, 61);
            this.ctrMaTT_PhiVanChuyen.Name = "ctrMaTT_PhiVanChuyen";
            this.ctrMaTT_PhiVanChuyen.Name_VN = "";
            this.ctrMaTT_PhiVanChuyen.SetValidate = false;
            this.ctrMaTT_PhiVanChuyen.ShowColumnCode = true;
            this.ctrMaTT_PhiVanChuyen.ShowColumnName = false;
            this.ctrMaTT_PhiVanChuyen.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_PhiVanChuyen.TabIndex = 74;
            this.ctrMaTT_PhiVanChuyen.TagName = "";
            this.ctrMaTT_PhiVanChuyen.WhereCondition = "";
            // 
            // ctrMaTT_PhiXepHang2
            // 
            this.ctrMaTT_PhiXepHang2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_PhiXepHang2.Code = "";
            this.ctrMaTT_PhiXepHang2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_PhiXepHang2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_PhiXepHang2.IsValidate = true;
            this.ctrMaTT_PhiXepHang2.Location = new System.Drawing.Point(298, 112);
            this.ctrMaTT_PhiXepHang2.Name = "ctrMaTT_PhiXepHang2";
            this.ctrMaTT_PhiXepHang2.Name_VN = "";
            this.ctrMaTT_PhiXepHang2.SetValidate = false;
            this.ctrMaTT_PhiXepHang2.ShowColumnCode = true;
            this.ctrMaTT_PhiXepHang2.ShowColumnName = false;
            this.ctrMaTT_PhiXepHang2.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_PhiXepHang2.TabIndex = 74;
            this.ctrMaTT_PhiXepHang2.TagName = "";
            this.ctrMaTT_PhiXepHang2.WhereCondition = "";
            // 
            // ctrMaTT_PhiXepHang1
            // 
            this.ctrMaTT_PhiXepHang1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_PhiXepHang1.Code = "";
            this.ctrMaTT_PhiXepHang1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_PhiXepHang1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_PhiXepHang1.IsValidate = true;
            this.ctrMaTT_PhiXepHang1.Location = new System.Drawing.Point(298, 86);
            this.ctrMaTT_PhiXepHang1.Name = "ctrMaTT_PhiXepHang1";
            this.ctrMaTT_PhiXepHang1.Name_VN = "";
            this.ctrMaTT_PhiXepHang1.SetValidate = false;
            this.ctrMaTT_PhiXepHang1.ShowColumnCode = true;
            this.ctrMaTT_PhiXepHang1.ShowColumnName = false;
            this.ctrMaTT_PhiXepHang1.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_PhiXepHang1.TabIndex = 74;
            this.ctrMaTT_PhiXepHang1.TagName = "";
            this.ctrMaTT_PhiXepHang1.WhereCondition = "";
            // 
            // ctrMaTT_PhiBaoHiem
            // 
            this.ctrMaTT_PhiBaoHiem.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_PhiBaoHiem.Code = "";
            this.ctrMaTT_PhiBaoHiem.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_PhiBaoHiem.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_PhiBaoHiem.IsValidate = true;
            this.ctrMaTT_PhiBaoHiem.Location = new System.Drawing.Point(298, 34);
            this.ctrMaTT_PhiBaoHiem.Name = "ctrMaTT_PhiBaoHiem";
            this.ctrMaTT_PhiBaoHiem.Name_VN = "";
            this.ctrMaTT_PhiBaoHiem.SetValidate = false;
            this.ctrMaTT_PhiBaoHiem.ShowColumnCode = true;
            this.ctrMaTT_PhiBaoHiem.ShowColumnName = false;
            this.ctrMaTT_PhiBaoHiem.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_PhiBaoHiem.TabIndex = 74;
            this.ctrMaTT_PhiBaoHiem.TagName = "";
            this.ctrMaTT_PhiBaoHiem.WhereCondition = "";
            // 
            // ctrMaTT_TriGiaFOB
            // 
            this.ctrMaTT_TriGiaFOB.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_TriGiaFOB.Code = "";
            this.ctrMaTT_TriGiaFOB.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_TriGiaFOB.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_TriGiaFOB.IsValidate = true;
            this.ctrMaTT_TriGiaFOB.Location = new System.Drawing.Point(298, 8);
            this.ctrMaTT_TriGiaFOB.Name = "ctrMaTT_TriGiaFOB";
            this.ctrMaTT_TriGiaFOB.Name_VN = "";
            this.ctrMaTT_TriGiaFOB.SetValidate = false;
            this.ctrMaTT_TriGiaFOB.ShowColumnCode = true;
            this.ctrMaTT_TriGiaFOB.ShowColumnName = false;
            this.ctrMaTT_TriGiaFOB.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_TriGiaFOB.TabIndex = 74;
            this.ctrMaTT_TriGiaFOB.TagName = "";
            this.ctrMaTT_TriGiaFOB.WhereCondition = "";
            // 
            // txtSoTienDieuChinh_FOB
            // 
            this.txtSoTienDieuChinh_FOB.DecimalDigits = 0;
            this.txtSoTienDieuChinh_FOB.Location = new System.Drawing.Point(542, 11);
            this.txtSoTienDieuChinh_FOB.Name = "txtSoTienDieuChinh_FOB";
            this.txtSoTienDieuChinh_FOB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSoTienDieuChinh_FOB.Size = new System.Drawing.Size(138, 21);
            this.txtSoTienDieuChinh_FOB.TabIndex = 14;
            this.txtSoTienDieuChinh_FOB.Text = "0";
            this.txtSoTienDieuChinh_FOB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienDieuChinh_FOB.VisualStyleManager = this.vsmMain;
            // 
            // txtPhiVC_NoiDia
            // 
            this.txtPhiVC_NoiDia.DecimalDigits = 0;
            this.txtPhiVC_NoiDia.Location = new System.Drawing.Point(542, 193);
            this.txtPhiVC_NoiDia.Name = "txtPhiVC_NoiDia";
            this.txtPhiVC_NoiDia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPhiVC_NoiDia.Size = new System.Drawing.Size(138, 21);
            this.txtPhiVC_NoiDia.TabIndex = 38;
            this.txtPhiVC_NoiDia.Text = "0";
            this.txtPhiVC_NoiDia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPhiVC_NoiDia.VisualStyleManager = this.vsmMain;
            // 
            // txtTienKhauTru
            // 
            this.txtTienKhauTru.DecimalDigits = 0;
            this.txtTienKhauTru.Location = new System.Drawing.Point(116, 167);
            this.txtTienKhauTru.Name = "txtTienKhauTru";
            this.txtTienKhauTru.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTienKhauTru.Size = new System.Drawing.Size(176, 21);
            this.txtTienKhauTru.TabIndex = 31;
            this.txtTienKhauTru.Text = "0";
            this.txtTienKhauTru.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienKhauTru.VisualStyleManager = this.vsmMain;
            // 
            // txtDieuChinhKhac
            // 
            this.txtDieuChinhKhac.DecimalDigits = 0;
            this.txtDieuChinhKhac.Location = new System.Drawing.Point(116, 141);
            this.txtDieuChinhKhac.Name = "txtDieuChinhKhac";
            this.txtDieuChinhKhac.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDieuChinhKhac.Size = new System.Drawing.Size(176, 21);
            this.txtDieuChinhKhac.TabIndex = 28;
            this.txtDieuChinhKhac.Text = "0";
            this.txtDieuChinhKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDieuChinhKhac.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTienDieuChinh_PhiBH
            // 
            this.txtSoTienDieuChinh_PhiBH.DecimalDigits = 0;
            this.txtSoTienDieuChinh_PhiBH.Location = new System.Drawing.Point(542, 37);
            this.txtSoTienDieuChinh_PhiBH.Name = "txtSoTienDieuChinh_PhiBH";
            this.txtSoTienDieuChinh_PhiBH.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSoTienDieuChinh_PhiBH.Size = new System.Drawing.Size(138, 21);
            this.txtSoTienDieuChinh_PhiBH.TabIndex = 36;
            this.txtSoTienDieuChinh_PhiBH.Text = "0";
            this.txtSoTienDieuChinh_PhiBH.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienDieuChinh_PhiBH.VisualStyleManager = this.vsmMain;
            // 
            // txtChiPhiXepHang2
            // 
            this.txtChiPhiXepHang2.DecimalDigits = 0;
            this.txtChiPhiXepHang2.Location = new System.Drawing.Point(116, 115);
            this.txtChiPhiXepHang2.Name = "txtChiPhiXepHang2";
            this.txtChiPhiXepHang2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtChiPhiXepHang2.Size = new System.Drawing.Size(176, 21);
            this.txtChiPhiXepHang2.TabIndex = 25;
            this.txtChiPhiXepHang2.Text = "0";
            this.txtChiPhiXepHang2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtChiPhiXepHang2.VisualStyleManager = this.vsmMain;
            // 
            // txtChiPhiXepHang1
            // 
            this.txtChiPhiXepHang1.DecimalDigits = 0;
            this.txtChiPhiXepHang1.Location = new System.Drawing.Point(116, 89);
            this.txtChiPhiXepHang1.Name = "txtChiPhiXepHang1";
            this.txtChiPhiXepHang1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtChiPhiXepHang1.Size = new System.Drawing.Size(176, 21);
            this.txtChiPhiXepHang1.TabIndex = 22;
            this.txtChiPhiXepHang1.Text = "0";
            this.txtChiPhiXepHang1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtChiPhiXepHang1.VisualStyleManager = this.vsmMain;
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.DecimalDigits = 0;
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(116, 37);
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(176, 21);
            this.txtPhiBaoHiem.TabIndex = 34;
            this.txtPhiBaoHiem.Text = "0";
            this.txtPhiBaoHiem.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPhiBaoHiem.VisualStyleManager = this.vsmMain;
            // 
            // txtPhiVanChuyen
            // 
            this.txtPhiVanChuyen.DecimalDigits = 0;
            this.txtPhiVanChuyen.Location = new System.Drawing.Point(116, 64);
            this.txtPhiVanChuyen.Name = "txtPhiVanChuyen";
            this.txtPhiVanChuyen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPhiVanChuyen.Size = new System.Drawing.Size(176, 21);
            this.txtPhiVanChuyen.TabIndex = 19;
            this.txtPhiVanChuyen.Text = "0";
            this.txtPhiVanChuyen.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPhiVanChuyen.VisualStyleManager = this.vsmMain;
            // 
            // txtTriGiaFOB
            // 
            this.txtTriGiaFOB.DecimalDigits = 0;
            this.txtTriGiaFOB.Location = new System.Drawing.Point(116, 11);
            this.txtTriGiaFOB.Name = "txtTriGiaFOB";
            this.txtTriGiaFOB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTriGiaFOB.Size = new System.Drawing.Size(176, 21);
            this.txtTriGiaFOB.TabIndex = 12;
            this.txtTriGiaFOB.Text = "0";
            this.txtTriGiaFOB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaFOB.VisualStyleManager = this.vsmMain;
            // 
            // txtLoaiDieuChinhKhac
            // 
            this.txtLoaiDieuChinhKhac.Location = new System.Drawing.Point(542, 141);
            this.txtLoaiDieuChinhKhac.Name = "txtLoaiDieuChinhKhac";
            this.txtLoaiDieuChinhKhac.Size = new System.Drawing.Size(213, 21);
            this.txtLoaiDieuChinhKhac.TabIndex = 30;
            this.txtLoaiDieuChinhKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtLoaiKhauTru
            // 
            this.txtLoaiKhauTru.Location = new System.Drawing.Point(542, 167);
            this.txtLoaiKhauTru.Name = "txtLoaiKhauTru";
            this.txtLoaiKhauTru.Size = new System.Drawing.Size(213, 21);
            this.txtLoaiKhauTru.TabIndex = 33;
            this.txtLoaiKhauTru.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtLoaiPhiXepHang2
            // 
            this.txtLoaiPhiXepHang2.Location = new System.Drawing.Point(542, 115);
            this.txtLoaiPhiXepHang2.Name = "txtLoaiPhiXepHang2";
            this.txtLoaiPhiXepHang2.Size = new System.Drawing.Size(213, 21);
            this.txtLoaiPhiXepHang2.TabIndex = 27;
            this.txtLoaiPhiXepHang2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtLoaiPhiXepHang1
            // 
            this.txtLoaiPhiXepHang1.Location = new System.Drawing.Point(542, 89);
            this.txtLoaiPhiXepHang1.Name = "txtLoaiPhiXepHang1";
            this.txtLoaiPhiXepHang1.Size = new System.Drawing.Size(213, 21);
            this.txtLoaiPhiXepHang1.TabIndex = 24;
            this.txtLoaiPhiXepHang1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNoiThanhToan_PVC
            // 
            this.txtNoiThanhToan_PVC.Location = new System.Drawing.Point(542, 63);
            this.txtNoiThanhToan_PVC.Name = "txtNoiThanhToan_PVC";
            this.txtNoiThanhToan_PVC.Size = new System.Drawing.Size(213, 21);
            this.txtNoiThanhToan_PVC.TabIndex = 21;
            this.txtNoiThanhToan_PVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(379, 145);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(137, 13);
            this.label56.TabIndex = 1;
            this.label56.Text = "Loại số tiền điều chỉnh khác";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(379, 171);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(70, 13);
            this.label55.TabIndex = 1;
            this.label55.Text = "Loại khấu trừ";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(379, 119);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(116, 13);
            this.label54.TabIndex = 1;
            this.label54.Text = "Loại chi phí xếp hàng 2";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(379, 93);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(116, 13);
            this.label53.TabIndex = 1;
            this.label53.Text = "Loại chi phí xếp hàng 1";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(379, 67);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(154, 13);
            this.label52.TabIndex = 1;
            this.label52.Text = "Nơi thanh toán phí vận chuyển";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(2, 119);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(96, 13);
            this.label46.TabIndex = 3;
            this.label46.Text = "Chi phí xếp hàng 2";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(2, 175);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(84, 13);
            this.label51.TabIndex = 3;
            this.label51.Text = "Số tiền khấu trừ";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(2, 93);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(96, 13);
            this.label45.TabIndex = 3;
            this.label45.Text = "Chi phí xếp hàng 1";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(2, 67);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(80, 13);
            this.label44.TabIndex = 3;
            this.label44.Text = "Phí vận chuyển";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(379, 198);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(163, 13);
            this.label47.TabIndex = 3;
            this.label47.Text = "Phí vận chuyển đường bộ nội địa";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(2, 145);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(116, 13);
            this.label49.TabIndex = 3;
            this.label49.Text = "Số tiền điều chỉnh khác";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(379, 15);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(114, 13);
            this.label43.TabIndex = 3;
            this.label43.Text = "Số tiền điều chỉnh FOB";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(2, 15);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(59, 13);
            this.label42.TabIndex = 3;
            this.label42.Text = "Trị giá FOB";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(2, 41);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(67, 13);
            this.label50.TabIndex = 3;
            this.label50.Text = "Phí bảo hiểm";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(379, 41);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(154, 13);
            this.label48.TabIndex = 3;
            this.label48.Text = "Số tiền điều chỉnh phí bảo hiểm";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtTongSoKien);
            this.uiGroupBox5.Controls.Add(this.ctrLoaiVanChuyen);
            this.uiGroupBox5.Controls.Add(this.txtTongTheTich);
            this.uiGroupBox5.Controls.Add(this.txtTrongLuongThuan);
            this.uiGroupBox5.Controls.Add(this.clcNgayXepHang);
            this.uiGroupBox5.Controls.Add(this.txtTrongLuong_Gross);
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemTrungChuyen);
            this.uiGroupBox5.Controls.Add(this.label37);
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemXemHang);
            this.uiGroupBox5.Controls.Add(this.cbbDVT_KienHang);
            this.uiGroupBox5.Controls.Add(this.cbbDVT_TheTich);
            this.uiGroupBox5.Controls.Add(this.cbbDVT_TrongLuongThuan);
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemDoHang);
            this.uiGroupBox5.Controls.Add(this.cbbDVT_TrongLuongGross);
            this.uiGroupBox5.Controls.Add(this.txtMaKyHieu);
            this.uiGroupBox5.Controls.Add(this.label38);
            this.uiGroupBox5.Controls.Add(this.label36);
            this.uiGroupBox5.Controls.Add(this.label30);
            this.uiGroupBox5.Controls.Add(this.label29);
            this.uiGroupBox5.Controls.Add(this.label39);
            this.uiGroupBox5.Controls.Add(this.label35);
            this.uiGroupBox5.Controls.Add(this.label41);
            this.uiGroupBox5.Controls.Add(this.label40);
            this.uiGroupBox5.Controls.Add(this.label31);
            this.uiGroupBox5.Controls.Add(this.txtSoPL);
            this.uiGroupBox5.Controls.Add(this.txtNganHang_PL);
            this.uiGroupBox5.Controls.Add(this.txtGhiChu_ChuHang);
            this.uiGroupBox5.Controls.Add(this.label32);
            this.uiGroupBox5.Controls.Add(this.txtSoHieuChuyenDi);
            this.uiGroupBox5.Controls.Add(this.label34);
            this.uiGroupBox5.Controls.Add(this.txtTen_PTVC);
            this.uiGroupBox5.Controls.Add(this.label33);
            this.uiGroupBox5.Controls.Add(this.label8);
            this.uiGroupBox5.Controls.Add(this.label28);
            this.uiGroupBox5.Location = new System.Drawing.Point(14, 209);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(762, 236);
            this.uiGroupBox5.TabIndex = 8;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtTongSoKien
            // 
            this.txtTongSoKien.DecimalDigits = 0;
            this.txtTongSoKien.Location = new System.Drawing.Point(580, 150);
            this.txtTongSoKien.Name = "txtTongSoKien";
            this.txtTongSoKien.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTongSoKien.Size = new System.Drawing.Size(100, 21);
            this.txtTongSoKien.TabIndex = 14;
            this.txtTongSoKien.Text = "0";
            this.txtTongSoKien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoKien.VisualStyleManager = this.vsmMain;
            // 
            // ctrLoaiVanChuyen
            // 
            this.ctrLoaiVanChuyen.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E028;
            this.ctrLoaiVanChuyen.Code = "";
            this.ctrLoaiVanChuyen.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrLoaiVanChuyen.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrLoaiVanChuyen.IsValidate = true;
            this.ctrLoaiVanChuyen.Location = new System.Drawing.Point(117, 38);
            this.ctrLoaiVanChuyen.Name = "ctrLoaiVanChuyen";
            this.ctrLoaiVanChuyen.Name_VN = "";
            this.ctrLoaiVanChuyen.SetValidate = false;
            this.ctrLoaiVanChuyen.ShowColumnCode = true;
            this.ctrLoaiVanChuyen.ShowColumnName = true;
            this.ctrLoaiVanChuyen.Size = new System.Drawing.Size(176, 26);
            this.ctrLoaiVanChuyen.TabIndex = 72;
            this.ctrLoaiVanChuyen.TagName = "";
            this.ctrLoaiVanChuyen.WhereCondition = "";
            // 
            // txtTongTheTich
            // 
            this.txtTongTheTich.DecimalDigits = 0;
            this.txtTongTheTich.Location = new System.Drawing.Point(580, 123);
            this.txtTongTheTich.Name = "txtTongTheTich";
            this.txtTongTheTich.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTongTheTich.Size = new System.Drawing.Size(100, 21);
            this.txtTongTheTich.TabIndex = 12;
            this.txtTongTheTich.Text = "0";
            this.txtTongTheTich.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTheTich.VisualStyleManager = this.vsmMain;
            // 
            // txtTrongLuongThuan
            // 
            this.txtTrongLuongThuan.DecimalDigits = 0;
            this.txtTrongLuongThuan.Location = new System.Drawing.Point(580, 96);
            this.txtTrongLuongThuan.Name = "txtTrongLuongThuan";
            this.txtTrongLuongThuan.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTrongLuongThuan.Size = new System.Drawing.Size(100, 21);
            this.txtTrongLuongThuan.TabIndex = 10;
            this.txtTrongLuongThuan.Text = "0";
            this.txtTrongLuongThuan.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTrongLuongThuan.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayXepHang
            // 
            // 
            // 
            // 
            this.clcNgayXepHang.DropDownCalendar.Name = "";
            this.clcNgayXepHang.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayXepHang.Location = new System.Drawing.Point(640, 40);
            this.clcNgayXepHang.Name = "clcNgayXepHang";
            this.clcNgayXepHang.Nullable = true;
            this.clcNgayXepHang.NullButtonText = "Xóa";
            this.clcNgayXepHang.ShowNullButton = true;
            this.clcNgayXepHang.Size = new System.Drawing.Size(115, 21);
            this.clcNgayXepHang.TabIndex = 3;
            this.clcNgayXepHang.Value = new System.DateTime(2013, 8, 22, 0, 0, 0, 0);
            this.clcNgayXepHang.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtTrongLuong_Gross
            // 
            this.txtTrongLuong_Gross.DecimalDigits = 0;
            this.txtTrongLuong_Gross.Location = new System.Drawing.Point(580, 69);
            this.txtTrongLuong_Gross.Name = "txtTrongLuong_Gross";
            this.txtTrongLuong_Gross.Size = new System.Drawing.Size(100, 21);
            this.txtTrongLuong_Gross.TabIndex = 8;
            this.txtTrongLuong_Gross.Text = "0";
            this.txtTrongLuong_Gross.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTrongLuong_Gross.VisualStyleManager = this.vsmMain;
            // 
            // ctrDiaDiemTrungChuyen
            // 
            this.ctrDiaDiemTrungChuyen.BackColor = System.Drawing.Color.Transparent;
            this.ctrDiaDiemTrungChuyen.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.ctrDiaDiemTrungChuyen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDiaDiemTrungChuyen.IsLoadData = true;
            this.ctrDiaDiemTrungChuyen.Location = new System.Drawing.Point(116, 149);
            this.ctrDiaDiemTrungChuyen.Ma = "";
            this.ctrDiaDiemTrungChuyen.Name = "ctrDiaDiemTrungChuyen";
            this.ctrDiaDiemTrungChuyen.ReadOnly = false;
            this.ctrDiaDiemTrungChuyen.Size = new System.Drawing.Size(354, 25);
            this.ctrDiaDiemTrungChuyen.TabIndex = 7;
            this.ctrDiaDiemTrungChuyen.Ten = "";
            this.ctrDiaDiemTrungChuyen.VisualStyleManager = this.vsmMain;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(1, 74);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Tên PTVC";
            // 
            // ctrDiaDiemXemHang
            // 
            this.ctrDiaDiemXemHang.BackColor = System.Drawing.Color.Transparent;
            this.ctrDiaDiemXemHang.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.ctrDiaDiemXemHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDiaDiemXemHang.IsLoadData = true;
            this.ctrDiaDiemXemHang.Location = new System.Drawing.Point(116, 96);
            this.ctrDiaDiemXemHang.Ma = "";
            this.ctrDiaDiemXemHang.Name = "ctrDiaDiemXemHang";
            this.ctrDiaDiemXemHang.ReadOnly = false;
            this.ctrDiaDiemXemHang.Size = new System.Drawing.Size(354, 25);
            this.ctrDiaDiemXemHang.TabIndex = 5;
            this.ctrDiaDiemXemHang.Ten = "";
            this.ctrDiaDiemXemHang.VisualStyleManager = this.vsmMain;
            // 
            // cbbDVT_KienHang
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Bao, hộp";
            uiComboBoxItem1.Value = "CS";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Thùng, hộp giấy";
            uiComboBoxItem2.Value = "CT";
            this.cbbDVT_KienHang.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbDVT_KienHang.Location = new System.Drawing.Point(686, 150);
            this.cbbDVT_KienHang.Name = "cbbDVT_KienHang";
            this.cbbDVT_KienHang.Size = new System.Drawing.Size(69, 21);
            this.cbbDVT_KienHang.TabIndex = 15;
            this.cbbDVT_KienHang.VisualStyleManager = this.vsmMain;
            // 
            // cbbDVT_TheTich
            // 
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Met khối";
            uiComboBoxItem3.Value = "MTQ";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Cubic Foot";
            uiComboBoxItem4.Value = "TNE";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Foot, board measure";
            uiComboBoxItem5.Value = "BFT";
            this.cbbDVT_TheTich.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5});
            this.cbbDVT_TheTich.Location = new System.Drawing.Point(686, 123);
            this.cbbDVT_TheTich.Name = "cbbDVT_TheTich";
            this.cbbDVT_TheTich.Size = new System.Drawing.Size(69, 21);
            this.cbbDVT_TheTich.TabIndex = 13;
            this.cbbDVT_TheTich.VisualStyleManager = this.vsmMain;
            // 
            // cbbDVT_TrongLuongThuan
            // 
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Kilogam";
            uiComboBoxItem6.Value = "KGM";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Tấn";
            uiComboBoxItem7.Value = "TNE";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Pao";
            uiComboBoxItem8.Value = "LBR";
            this.cbbDVT_TrongLuongThuan.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbbDVT_TrongLuongThuan.Location = new System.Drawing.Point(686, 96);
            this.cbbDVT_TrongLuongThuan.Name = "cbbDVT_TrongLuongThuan";
            this.cbbDVT_TrongLuongThuan.Size = new System.Drawing.Size(69, 21);
            this.cbbDVT_TrongLuongThuan.TabIndex = 11;
            this.cbbDVT_TrongLuongThuan.VisualStyleManager = this.vsmMain;
            // 
            // ctrDiaDiemDoHang
            // 
            this.ctrDiaDiemDoHang.BackColor = System.Drawing.Color.Transparent;
            this.ctrDiaDiemDoHang.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.ctrDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDiaDiemDoHang.IsLoadData = true;
            this.ctrDiaDiemDoHang.Location = new System.Drawing.Point(116, 121);
            this.ctrDiaDiemDoHang.Ma = "";
            this.ctrDiaDiemDoHang.Name = "ctrDiaDiemDoHang";
            this.ctrDiaDiemDoHang.ReadOnly = false;
            this.ctrDiaDiemDoHang.Size = new System.Drawing.Size(354, 25);
            this.ctrDiaDiemDoHang.TabIndex = 6;
            this.ctrDiaDiemDoHang.Ten = "";
            this.ctrDiaDiemDoHang.VisualStyleManager = this.vsmMain;
            // 
            // cbbDVT_TrongLuongGross
            // 
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Kilogam";
            uiComboBoxItem9.Value = "KGM";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Tấn";
            uiComboBoxItem10.Value = "TNE";
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Pao";
            uiComboBoxItem11.Value = "LBR";
            this.cbbDVT_TrongLuongGross.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem9,
            uiComboBoxItem10,
            uiComboBoxItem11});
            this.cbbDVT_TrongLuongGross.Location = new System.Drawing.Point(686, 69);
            this.cbbDVT_TrongLuongGross.Name = "cbbDVT_TrongLuongGross";
            this.cbbDVT_TrongLuongGross.Size = new System.Drawing.Size(69, 21);
            this.cbbDVT_TrongLuongGross.TabIndex = 9;
            this.cbbDVT_TrongLuongGross.VisualStyleManager = this.vsmMain;
            // 
            // txtMaKyHieu
            // 
            this.txtMaKyHieu.Location = new System.Drawing.Point(116, 13);
            this.txtMaKyHieu.Name = "txtMaKyHieu";
            this.txtMaKyHieu.Size = new System.Drawing.Size(639, 21);
            this.txtMaKyHieu.TabIndex = 0;
            this.txtMaKyHieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(295, 43);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(91, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "Số hiệu chuyến đi";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(545, 43);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(89, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "Thời kỳ xếp hàng";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(1, 209);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(37, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Số P/L";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(308, 209);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(77, 13);
            this.label29.TabIndex = 1;
            this.label29.Text = "Ngân hàng P/L";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(1, 181);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(109, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "Ghi chú của chủ hàng";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(1, 152);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(115, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Địa điểm trung chuyển";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(2, 18);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(58, 13);
            this.label41.TabIndex = 1;
            this.label41.Text = "Mã ký hiệu";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(2, 45);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(109, 13);
            this.label40.TabIndex = 1;
            this.label40.Text = "Phân loại vận chuyển";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(1, 99);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(96, 13);
            this.label31.TabIndex = 1;
            this.label31.Text = "Địa điểm xếp hàng";
            // 
            // txtSoPL
            // 
            this.txtSoPL.Location = new System.Drawing.Point(116, 205);
            this.txtSoPL.Name = "txtSoPL";
            this.txtSoPL.Size = new System.Drawing.Size(176, 21);
            this.txtSoPL.TabIndex = 17;
            this.txtSoPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNganHang_PL
            // 
            this.txtNganHang_PL.Location = new System.Drawing.Point(388, 204);
            this.txtNganHang_PL.Name = "txtNganHang_PL";
            this.txtNganHang_PL.Size = new System.Drawing.Size(367, 21);
            this.txtNganHang_PL.TabIndex = 18;
            this.txtNganHang_PL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu_ChuHang
            // 
            this.txtGhiChu_ChuHang.Location = new System.Drawing.Point(116, 177);
            this.txtGhiChu_ChuHang.Name = "txtGhiChu_ChuHang";
            this.txtGhiChu_ChuHang.Size = new System.Drawing.Size(639, 21);
            this.txtGhiChu_ChuHang.TabIndex = 16;
            this.txtGhiChu_ChuHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(1, 124);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(90, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "Địa điểm dở hàng";
            // 
            // txtSoHieuChuyenDi
            // 
            this.txtSoHieuChuyenDi.Location = new System.Drawing.Point(388, 40);
            this.txtSoHieuChuyenDi.Name = "txtSoHieuChuyenDi";
            this.txtSoHieuChuyenDi.Size = new System.Drawing.Size(157, 21);
            this.txtSoHieuChuyenDi.TabIndex = 2;
            this.txtSoHieuChuyenDi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(482, 154);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(67, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "Tổng số kiện";
            // 
            // txtTen_PTVC
            // 
            this.txtTen_PTVC.Location = new System.Drawing.Point(116, 69);
            this.txtTen_PTVC.Name = "txtTen_PTVC";
            this.txtTen_PTVC.Size = new System.Drawing.Size(353, 21);
            this.txtTen_PTVC.TabIndex = 4;
            this.txtTen_PTVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTen_PTVC.VisualStyleManager = this.vsmMain;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(482, 127);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(70, 13);
            this.label33.TabIndex = 1;
            this.label33.Text = "Tổng thể tích";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(482, 100);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(96, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "Trọng lượng thuần";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.ctrMaTT_TongTriGia_HD);
            this.uiGroupBox7.Controls.Add(this.ctrPTTT);
            this.uiGroupBox7.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox7.Controls.Add(this.txtSoHoaDon);
            this.uiGroupBox7.Controls.Add(this.txtGhiChu);
            this.uiGroupBox7.Controls.Add(this.label58);
            this.uiGroupBox7.Controls.Add(this.txtMaDaiLy_HQ);
            this.uiGroupBox7.Controls.Add(this.txtDiaDiemLap_HD);
            this.uiGroupBox7.Controls.Add(this.txtTongDongHang);
            this.uiGroupBox7.Controls.Add(this.txtTongTriGia_HD);
            this.uiGroupBox7.Controls.Add(this.txtDieuKienGia_HD);
            this.uiGroupBox7.Controls.Add(this.clcNgayTiepNhan);
            this.uiGroupBox7.Controls.Add(this.txtDiaDiemGiaoHang);
            this.uiGroupBox7.Controls.Add(this.clcNgayLapHD);
            this.uiGroupBox7.Controls.Add(this.label13);
            this.uiGroupBox7.Controls.Add(this.cbbPhanLoaiXuatNhap);
            this.uiGroupBox7.Controls.Add(this.label14);
            this.uiGroupBox7.Controls.Add(this.label20);
            this.uiGroupBox7.Controls.Add(this.label15);
            this.uiGroupBox7.Controls.Add(this.label9);
            this.uiGroupBox7.Controls.Add(this.label16);
            this.uiGroupBox7.Controls.Add(this.label19);
            this.uiGroupBox7.Controls.Add(this.label17);
            this.uiGroupBox7.Controls.Add(this.label11);
            this.uiGroupBox7.Controls.Add(this.label12);
            this.uiGroupBox7.Controls.Add(this.label57);
            this.uiGroupBox7.Controls.Add(this.label10);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(792, 153);
            this.uiGroupBox7.TabIndex = 42;
            // 
            // ctrMaTT_TongTriGia_HD
            // 
            this.ctrMaTT_TongTriGia_HD.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_TongTriGia_HD.Code = "";
            this.ctrMaTT_TongTriGia_HD.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_TongTriGia_HD.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_TongTriGia_HD.IsValidate = true;
            this.ctrMaTT_TongTriGia_HD.Location = new System.Drawing.Point(297, 91);
            this.ctrMaTT_TongTriGia_HD.Name = "ctrMaTT_TongTriGia_HD";
            this.ctrMaTT_TongTriGia_HD.Name_VN = "";
            this.ctrMaTT_TongTriGia_HD.SetValidate = false;
            this.ctrMaTT_TongTriGia_HD.ShowColumnCode = true;
            this.ctrMaTT_TongTriGia_HD.ShowColumnName = false;
            this.ctrMaTT_TongTriGia_HD.Size = new System.Drawing.Size(73, 26);
            this.ctrMaTT_TongTriGia_HD.TabIndex = 73;
            this.ctrMaTT_TongTriGia_HD.TagName = "";
            this.ctrMaTT_TongTriGia_HD.WhereCondition = "";
            // 
            // ctrPTTT
            // 
            this.ctrPTTT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E009;
            this.ctrPTTT.Code = "";
            this.ctrPTTT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrPTTT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrPTTT.IsValidate = true;
            this.ctrPTTT.Location = new System.Drawing.Point(145, 62);
            this.ctrPTTT.Name = "ctrPTTT";
            this.ctrPTTT.Name_VN = "";
            this.ctrPTTT.SetValidate = false;
            this.ctrPTTT.ShowColumnCode = true;
            this.ctrPTTT.ShowColumnName = true;
            this.ctrPTTT.Size = new System.Drawing.Size(215, 26);
            this.ctrPTTT.TabIndex = 72;
            this.ctrPTTT.TagName = "";
            this.ctrPTTT.WhereCondition = "";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Enabled = false;
            this.txtSoTiepNhan.Location = new System.Drawing.Point(82, 13);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(123, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.Location = new System.Drawing.Point(82, 40);
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(123, 21);
            this.txtSoHoaDon.TabIndex = 2;
            this.txtSoHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHoaDon.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(145, 121);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(627, 21);
            this.txtGhiChu.TabIndex = 40;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(20, 125);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(83, 13);
            this.label58.TabIndex = 3;
            this.label58.Text = "Ghi chú đặc biệt";
            // 
            // txtMaDaiLy_HQ
            // 
            this.txtMaDaiLy_HQ.Location = new System.Drawing.Point(673, 13);
            this.txtMaDaiLy_HQ.Name = "txtMaDaiLy_HQ";
            this.txtMaDaiLy_HQ.Size = new System.Drawing.Size(99, 21);
            this.txtMaDaiLy_HQ.TabIndex = 1;
            this.txtMaDaiLy_HQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDaiLy_HQ.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaDiemLap_HD
            // 
            this.txtDiaDiemLap_HD.Location = new System.Drawing.Point(493, 40);
            this.txtDiaDiemLap_HD.Name = "txtDiaDiemLap_HD";
            this.txtDiaDiemLap_HD.Size = new System.Drawing.Size(279, 21);
            this.txtDiaDiemLap_HD.TabIndex = 4;
            this.txtDiaDiemLap_HD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaDiemLap_HD.VisualStyleManager = this.vsmMain;
            // 
            // txtTongDongHang
            // 
            this.txtTongDongHang.DecimalDigits = 0;
            this.txtTongDongHang.Location = new System.Drawing.Point(700, 94);
            this.txtTongDongHang.Name = "txtTongDongHang";
            this.txtTongDongHang.Size = new System.Drawing.Size(72, 21);
            this.txtTongDongHang.TabIndex = 9;
            this.txtTongDongHang.Text = "0";
            this.txtTongDongHang.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongDongHang.VisualStyleManager = this.vsmMain;
            // 
            // txtTongTriGia_HD
            // 
            this.txtTongTriGia_HD.DecimalDigits = 0;
            this.txtTongTriGia_HD.Location = new System.Drawing.Point(145, 94);
            this.txtTongTriGia_HD.Name = "txtTongTriGia_HD";
            this.txtTongTriGia_HD.Size = new System.Drawing.Size(149, 21);
            this.txtTongTriGia_HD.TabIndex = 6;
            this.txtTongTriGia_HD.Text = "0";
            this.txtTongTriGia_HD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTriGia_HD.VisualStyleManager = this.vsmMain;
            // 
            // txtDieuKienGia_HD
            // 
            this.txtDieuKienGia_HD.Location = new System.Drawing.Point(493, 94);
            this.txtDieuKienGia_HD.Name = "txtDieuKienGia_HD";
            this.txtDieuKienGia_HD.Size = new System.Drawing.Size(103, 21);
            this.txtDieuKienGia_HD.TabIndex = 8;
            this.txtDieuKienGia_HD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayTiepNhan
            // 
            // 
            // 
            // 
            this.clcNgayTiepNhan.DropDownCalendar.Name = "";
            this.clcNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTiepNhan.IsNullDate = true;
            this.clcNgayTiepNhan.Location = new System.Drawing.Point(280, 13);
            this.clcNgayTiepNhan.Name = "clcNgayTiepNhan";
            this.clcNgayTiepNhan.Nullable = true;
            this.clcNgayTiepNhan.ReadOnly = true;
            this.clcNgayTiepNhan.Size = new System.Drawing.Size(90, 21);
            this.clcNgayTiepNhan.TabIndex = 3;
            this.clcNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtDiaDiemGiaoHang
            // 
            this.txtDiaDiemGiaoHang.Location = new System.Drawing.Point(493, 67);
            this.txtDiaDiemGiaoHang.Name = "txtDiaDiemGiaoHang";
            this.txtDiaDiemGiaoHang.Size = new System.Drawing.Size(279, 21);
            this.txtDiaDiemGiaoHang.TabIndex = 7;
            this.txtDiaDiemGiaoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayLapHD
            // 
            // 
            // 
            // 
            this.clcNgayLapHD.DropDownCalendar.Name = "";
            this.clcNgayLapHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayLapHD.IsNullDate = true;
            this.clcNgayLapHD.Location = new System.Drawing.Point(280, 40);
            this.clcNgayLapHD.Name = "clcNgayLapHD";
            this.clcNgayLapHD.Nullable = true;
            this.clcNgayLapHD.ShowNullButton = true;
            this.clcNgayLapHD.Size = new System.Drawing.Size(90, 21);
            this.clcNgayLapHD.TabIndex = 3;
            this.clcNgayLapHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayLapHD.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(203, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Ngày tiếp nhận";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(369, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Địa điểm giao hàng";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 98);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Tổng trị giá hóa đơn";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(600, 98);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(99, 13);
            this.label57.TabIndex = 1;
            this.label57.Text = "Tổng số dòng hàng";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(369, 98);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Điều kiện giá hóa đơn";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvSoHoaDon
            // 
            this.rfvSoHoaDon.ControlToValidate = this.txtSoHoaDon;
            this.rfvSoHoaDon.ErrorMessage = "\"Mã số hóa đơn\" không được để trống.";
            this.rfvSoHoaDon.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHoaDon.Icon")));
            this.rfvSoHoaDon.Tag = "rfvSoHoaDon";
            // 
            // rfvNgayLapHD
            // 
            this.rfvNgayLapHD.ControlToValidate = this.clcNgayLapHD;
            this.rfvNgayLapHD.ErrorMessage = "\"Ngày lập hóa đơn\" không được để trống.";
            this.rfvNgayLapHD.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayLapHD.Icon")));
            this.rfvNgayLapHD.Tag = "rfvNgayLapHD";
            // 
            // rfvLoaiHinh
            // 
            this.rfvLoaiHinh.ControlToValidate = this.cbbPhanLoaiXuatNhap;
            this.rfvLoaiHinh.ErrorMessage = "\"Phân loại xuất nhập khẩu\" không được để trống.";
            this.rfvLoaiHinh.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvLoaiHinh.Icon")));
            this.rfvLoaiHinh.Tag = "rfvLoaiHinh";
            // 
            // rfvDiaDiemLapHD
            // 
            this.rfvDiaDiemLapHD.ControlToValidate = this.txtSoHoaDon;
            this.rfvDiaDiemLapHD.ErrorMessage = "\"Địa điểm lập hóa đơn\" không được để trống.";
            this.rfvDiaDiemLapHD.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaDiemLapHD.Icon")));
            this.rfvDiaDiemLapHD.Tag = "rfvDiaDiemLapHD";
            // 
            // rfvTongTriGiaHoaDon
            // 
            this.rfvTongTriGiaHoaDon.ControlToValidate = this.txtTongTriGia_HD;
            this.rfvTongTriGiaHoaDon.ErrorMessage = "\"Tổng trị giá hóa đơn\" không được để trống.";
            this.rfvTongTriGiaHoaDon.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTongTriGiaHoaDon.Icon")));
            this.rfvTongTriGiaHoaDon.Tag = "rfvTongTriGiaHoaDon";
            // 
            // VNACC_HoaDonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 741);
            this.Controls.Add(this.LeftRebar1);
            this.Controls.Add(this.RightRebar1);
            this.Controls.Add(this.cmbToolBar);
            this.Controls.Add(this.BottomRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_HoaDonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hóa đơn";
            this.Load += new System.EventHandler(this.VNACC_HoaDonForm_Load);
            this.Controls.SetChildIndex(this.BottomRebar1, 0);
            this.Controls.SetChildIndex(this.cmbToolBar, 0);
            this.Controls.SetChildIndex(this.RightRebar1, 0);
            this.Controls.SetChildIndex(this.LeftRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).EndInit();
            this.cmbToolBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayLapHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvLoaiHinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemLapHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTongTriGiaHoaDon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UICommandBar cmdToolBar;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang2;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu2;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao2;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar cmbToolBar;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIComboBox cbbPhanLoaiXuatNhap;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMa_XNK;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuocGia;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtTen_XNK;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinh_XNK;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDaiLy_HQ;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi_XNK;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemLap_HD;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoai_XNK;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiLap_HD;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinh_Thanh;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayLapHD;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMa_GuiNhan;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.EditBox txtten_GuiNhan;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinh_GuiNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi_1;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.EditBox txtDaiChi_2;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoai_GuiNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNuoc;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.EditControls.UIComboBox cbbDVT_TrongLuongThuan;
        private Janus.Windows.EditControls.UIComboBox cbbDVT_TrongLuongGross;
        private Company.Interface.Controls.CuaKhauHControl ctrDiaDiemDoHang;
        private Company.Interface.Controls.CuaKhauHControl ctrDiaDiemXemHang;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIComboBox cbbDVT_KienHang;
        private Janus.Windows.EditControls.UIComboBox cbbDVT_TheTich;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoKien;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTheTich;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuongThuan;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuong_Gross;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaKyHieu;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu_ChuHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuChuyenDi;
        private Janus.Windows.GridEX.EditControls.EditBox txtTen_PTVC;
        private Company.Interface.Controls.CuaKhauHControl ctrDiaDiemTrungChuyen;
        private System.Windows.Forms.Label label35;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayXepHang;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemGiaoHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtDieuKienGia_HD;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGia_HD;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtNganHang_PL;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiDieuChinhKhac;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiKhauTru;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiPhiXepHang2;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiPhiXepHang1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiThanhToan_PVC;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongDongHang;
        private System.Windows.Forms.Label label57;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label58;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanLoaiHoaDon1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanLoaiHoaDon;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienDieuChinh_FOB;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiVC_NoiDia;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienDieuChinh_PhiBH;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiBaoHiem;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTienKhauTru;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDieuChinhKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChiPhiXepHang2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChiPhiXepHang1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiVanChuyen;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaFOB;
        private System.Windows.Forms.Panel panel1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayPhanHoi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayPhanHoi;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoHoaDon;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayLapHD;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvLoaiHinh;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaDiemLapHD;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTongTriGiaHoaDon;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTiepNhan;
        private System.Windows.Forms.Label label9;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPTTT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_SoTien_PhiBH;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_SoTien_FOB;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_PVC_NoiDia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_KhauTru;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_DieuChinhKhac;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_PhiVanChuyen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_PhiXepHang2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_PhiXepHang1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_PhiBaoHiem;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_TriGiaFOB;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrLoaiVanChuyen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_TongTriGia_HD;
    }
}