﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.DuLieuChuan;

using System.Data;
using Janus.Windows.GridEX;


namespace Company.Interface.KDT.GC.PCTFormTemplate
{
    public partial class FormDieuChinhDVTSP : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();    
        private string dvt = string.Empty;
        public bool boolFlag;
        public FormDieuChinhDVTSP()
        {
            InitializeComponent();
        }
      

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            Company.Interface.GC.SanPhamRegistedGCForm f = new Company.Interface.GC.SanPhamRegistedGCForm();
            f.SanPhamSelected.HopDong_ID = this.HD.ID;
            f.isDisplayAll = 2;
            f.ShowDialog();
            if (f.SanPhamSelected != null && f.SanPhamSelected.Ma != "")
            {
                txtMaNPL.Text = f.SanPhamSelected.Ma.Trim();
                txtTenNPL.Text = f.SanPhamSelected.Ten.Trim();
                txtMaHSNPL.Text = f.SanPhamSelected.MaHS;
                txtSoLuongCu.Text = f.SanPhamSelected.SoLuongDangKy.ToString();
                txtDonViTinhCu.Text = this.DonViTinh_GetName(f.SanPhamSelected.DVT_ID).Trim();
                cbNhomSP.SelectedValue = f.SanPhamSelected.NhomSanPham_ID;
            }
            
        }
        private bool CheckNPLExit(string maNPL)
        {
            foreach(HangPhuKien pk1 in LoaiPK.HPKCollection)
            {
                if (pk1.MaHang.Trim() == maNPL.Trim())
                {
                    return true;
                }
            }
            return false; ;
        }
        

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            if (txtMaNPL.Text.Trim() == "")
                return;
            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
            sp.HopDong_ID = this.HD.ID;
            sp.Ma = txtMaNPL.Text.Trim();
            if (sp.Load())
            {
                if (sp.TrangThai ==0)
                {
                    txtMaNPL.Text = sp.Ma;
                    txtTenNPL.Text = sp.Ten;
                    txtDonViTinhCu.Text = DonViTinh_GetName(sp.DVT_ID);
                    txtMaHSNPL.Text = sp.MaHS;
                    txtSoLuongCu.Text = sp.SoLuongDangKy.ToString();
                    cbNhomSP.SelectedValue = sp.NhomSanPham_ID;
                    error.SetError(txtMaNPL,null);
                    cbDonViTinh.Focus();                    
                }
                else if (sp.TrangThai != 0 && HangPK.ID > 0)
                {
                    txtMaNPL.Text = sp.Ma;
                    txtTenNPL.Text = sp.Ten;
                    txtMaHSNPL.Text = sp.MaHS;
                    txtSoLuongCu.Text = sp.SoLuongDangKy.ToString();
                    cbNhomSP.SelectedValue = sp.NhomSanPham_ID;
                    //txtSoLuongCu.Text = npl.SoLuongDangKy.ToString();
                    //decimal LuongDaSuDung = npl.SoLuongDaNhap + npl.SoLuongCungUng;
                    //txtLuongDaNhap.Text = LuongDaSuDung.ToString();
                    error.SetError(txtMaNPL, null);
                    cbDonViTinh.Focus();
                }
                else
                {
                    error.SetError(txtMaNPL, setText("Sản phẩm đang điều chỉnh nhưng chưa được hải quan duyệt nên không thể điều chỉnh đơn vị tính.", "This product have been updated and is pending approval so can not update unit now "));
                    txtMaNPL.Focus();
                    return;
                }
            }
            else
            {
                error.SetError(txtMaNPL,setText("Không tồn tại sản phẩm này.","This value is not exist"));
                txtMaNPL.Focus();
                return;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                txtMaNPL.Focus();
                cvError.Validate();
                if (!cvError.IsValid) return;
                if (txtDonViTinhCu.Text == cbDonViTinh.Text)
                {
                    showMsg("MSG_240216");
                    //ShowMessage("Phải chọn đơn vị tính khác với đơn vị tinh ban đầu.", false);
                    return;
                }
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = this.HD.ID;
                sp.Ma = txtMaNPL.Text.Trim();
                if (!sp.Load())
                {
                    showMsg("MSG_240217");
                    //ShowMessage("Không có sản phẩm này trong hệ thống.", false);
                    return;
                }
               
                LoaiPK.HPKCollection.Remove(HangPK);
                if (CheckNPLExit(txtMaNPL.Text.Trim()))
                {
                    showMsg("MSG_240218");
                    //ShowMessage("Đã điều chỉnh sản phẩm này rồi", false);
                    if (HangPK.MaHang.Trim() != "")
                        LoaiPK.HPKCollection.Add(HangPK);
                    return;
                }
                HangPK.MaHang = txtMaNPL.Text.Trim();
                HangPK.TenHang = txtTenNPL.Text.Trim();
                HangPK.SoLuong = Convert.ToDecimal(txtSoLuongCu.Text);
                HangPK.MaHS = txtMaHSNPL.Text.Trim();
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                HangPK.ThongTinCu = txtDonViTinhCu.Text.Trim();
                HangPK.NhomSP = cbNhomSP.SelectedValue.ToString();
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                if (boolFlag == true)
                {
                    LoaiPK.InsertUpdateDieuChinhDVTSPDaDangKy(pkdk.HopDong_ID);
                    boolFlag = false;
                }
                else 
                {
                    LoaiPK.InsertUpdateDieuChinhDVTSP(pkdk.HopDong_ID);
                }
                
                //PhuKienGCSendForm.isEdit = true;
                reset();
                this.setErro(); 
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
        private void setErro()
        {
            //error.Clear();
            error.SetError(txtMaNPL , null);
            error.SetError(txtTenNPL , null);
            error.SetError(txtMaHSNPL , null);
            error.SetError(cbDonViTinh , null);
            //error.SetError(txtTGNT, null);

        }
        private void reset()
        {
            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtDonViTinhCu.Text = "";
            txtMaHSNPL.Text = "";
            txtDonViTinhCu.Text = "";
            cbDonViTinh.Text = "";

            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            txtMaNPL.Focus();
            HangPK = new HangPhuKien();
        }
        

        private void DieuChinhSoLuongNPLForm_Load(object sender, EventArgs e)
        {
            this._DonViTinh = DonViTinh.SelectAll();
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
           
            Company.GC.BLL.GC.NhomSanPham nhomSP=new Company.GC.BLL.GC.NhomSanPham();
            nhomSP.HopDong_ID = this.HD.ID;
            cbNhomSP.DataSource = nhomSP.SelectCollectionBy_HopDong_ID();

            cbNhomSP.DisplayMember = "TenSanPham";
            cbNhomSP.ValueMember = "MaSanPham";                             
 

            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "S06")
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                LoaiPK.LoadCollection();
                btnXoa.Enabled = false;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                btnXoa.Enabled = true;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                btnXoa.Enabled = false;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            //Registered
            if (boolFlag == true)
            {
                btnUpdate.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            LoaiPK.MaPhuKien = "S06";
            dgList.DataSource = LoaiPK.HPKCollection; 
        }

     

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                HangPK = (Company.GC.BLL.KDT.GC.HangPhuKien)e.Row.DataRow;
                txtMaNPL.Text = HangPK.MaHang.Trim();
                txtTenNPL.Text = HangPK.TenHang.Trim();
                txtMaHSNPL.Text = HangPK.MaHS.Trim();
                txtSoLuongCu.Text = HangPK.SoLuong.ToString();
                txtDonViTinhCu.Text = HangPK.ThongTinCu.Trim();
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                cbNhomSP.SelectedValue = HangPK.NhomSP;
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                e.Row.Cells["NhomSanPham_ID"].Text = Company.GC.BLL.DuLieuChuan.NhomSanPham.getTenSanPham(e.Row.Cells["NhomSanPham_ID"].Text);
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa sản phẩm này không?", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien,pkdk.HopDong_ID);
                            //pkDelete.DeleteTransaction(null);
                        }
                    }
                }

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void FormDieuChinhDVTSP_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (LoaiPK.HPKCollection.Count > 0)
            {
                LoaiPK.MaPhuKien = "S06";
                LoaiPK.NoiDung = LoaiPhuKien_GetName("S06");
                pkdk.PKCollection.Remove(LoaiPK);
                pkdk.PKCollection.Add(LoaiPK);
            }
            else
            {
                pkdk.PKCollection.Remove(LoaiPK);
                LoaiPK.Delete(pkdk.HopDong_ID);;
                //LoaiPK.DeleteTransaction(null);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            HangPhuKienCollection hpkColl = new HangPhuKienCollection();
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa sản phẩm này không?", true) == "Yes")
            {               
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        try
                        {
                            if (pkDelete.ID > 0)
                            {
                                pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                                //pkDelete.DeleteTransaction(null);
                            }
                            hpkColl.Add(pkDelete);
                            //LoaiPK.HPKCollection.Remove(pkDelete);
                        }                       
                        catch { }
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.HangPhuKien hpkk in hpkColl)
                {
                    try
                    {
                        LoaiPK.HPKCollection.Remove(hpkk);
                    }
                    catch { }

                }
                dgList.DataSource = LoaiPK.HPKCollection;
                reset();
            }
           
        }
        
    }
}