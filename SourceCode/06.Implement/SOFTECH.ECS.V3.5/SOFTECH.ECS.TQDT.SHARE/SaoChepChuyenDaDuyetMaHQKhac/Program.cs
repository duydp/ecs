﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SaoChepChuyenDaDuyetMaHQKhac
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                string PhanHe = "";
//#if KD_V3 || KD_V4
//            PhanHe = "KD";
//#elif SXXK_V3 || SXXK_V4
                PhanHe = "SXXK";
//#elif GC_V3 || GC_V4
//            PhanHe = "GC";
//#endif
                GlobalSettings.AupdateConfigXMLToExe(PhanHe);

                GlobalSettings.RefreshKey();

                Application.Run(new CopyAndTransfer());
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
