using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class HangChuyenTiep
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public int SoThuTuHang { set; get; }
		public string MaHang { set; get; }
		public string TenHang { set; get; }
		public string MaHS { set; get; }
		public decimal SoLuong { set; get; }
		public string ID_NuocXX { set; get; }
		public string ID_DVT { set; get; }
		public decimal DonGia { set; get; }
		public decimal TriGia { set; get; }
		public decimal TrongLuong { set; get; }
		public decimal DonGiaTT { set; get; }
		public decimal TriGiaTT { set; get; }
		public decimal TriGiaKB_VND { set; get; }
		public decimal ThueSuatXNK { set; get; }
		public decimal ThueSuatTTDB { set; get; }
		public decimal ThueSuatGTGT { set; get; }
		public decimal ThueXNK { set; get; }
		public decimal ThueTTDB { set; get; }
		public decimal ThueGTGT { set; get; }
		public decimal PhuThu { set; get; }
		public decimal TyLeThuKhac { set; get; }
		public decimal TriGiaThuKhac { set; get; }
		public byte MienThue { set; get; }
		public string Ma_HTS { set; get; }
		public string DVT_HTS { set; get; }
		public decimal SoLuong_HTS { set; get; }
		public string ThueSuatXNKGiam { set; get; }
		public string ThueSuatTTDBGiam { set; get; }
		public string ThueSuatVATGiam { set; get; }
		public double DonGiaTuyetDoi { set; get; }
		public bool FOC { set; get; }
		public bool ThueTuyetDoi { set; get; }
		public string MaHSMoRong { set; get; }
		public string NhanHieu { set; get; }
		public string QuyCachPhamChat { set; get; }
		public string ThanhPhan { set; get; }
		public string Model { set; get; }
		public string TenHangSX { set; get; }
		public string MaHangSX { set; get; }
		public string MaLoaiCo { set; get; }
		public bool NoCo { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HangChuyenTiep Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			HangChuyenTiep entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new HangChuyenTiep();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) entity.ID_NuocXX = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_DVT"))) entity.ID_DVT = reader.GetString(reader.GetOrdinal("ID_DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiCo"))) entity.MaLoaiCo = reader.GetString(reader.GetOrdinal("MaLoaiCo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetBoolean(reader.GetOrdinal("NoCo"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<HangChuyenTiep> SelectCollectionAll()
		{
			List<HangChuyenTiep> collection = new List<HangChuyenTiep>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				HangChuyenTiep entity = new HangChuyenTiep();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) entity.ID_NuocXX = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_DVT"))) entity.ID_DVT = reader.GetString(reader.GetOrdinal("ID_DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiCo"))) entity.MaLoaiCo = reader.GetString(reader.GetOrdinal("MaLoaiCo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetBoolean(reader.GetOrdinal("NoCo"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<HangChuyenTiep> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<HangChuyenTiep> collection = new List<HangChuyenTiep>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HangChuyenTiep entity = new HangChuyenTiep();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) entity.ID_NuocXX = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_DVT"))) entity.ID_DVT = reader.GetString(reader.GetOrdinal("ID_DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiCo"))) entity.MaLoaiCo = reader.GetString(reader.GetOrdinal("MaLoaiCo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetBoolean(reader.GetOrdinal("NoCo"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<HangChuyenTiep> SelectCollectionBy_Master_ID(long master_ID)
		{
			List<HangChuyenTiep> collection = new List<HangChuyenTiep>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_Master_ID(master_ID);
			while (reader.Read())
			{
				HangChuyenTiep entity = new HangChuyenTiep();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) entity.ID_NuocXX = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_DVT"))) entity.ID_DVT = reader.GetString(reader.GetOrdinal("ID_DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiCo"))) entity.MaLoaiCo = reader.GetString(reader.GetOrdinal("MaLoaiCo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetBoolean(reader.GetOrdinal("NoCo"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Master_ID(long master_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_SelectBy_Master_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_Master_ID(long master_ID)
		{
			const string spName = "p_KDT_GC_HangChuyenTiep_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHangChuyenTiep(long master_ID, int soThuTuHang, string maHang, string tenHang, string maHS, decimal soLuong, string iD_NuocXX, string iD_DVT, decimal donGia, decimal triGia, decimal trongLuong, decimal donGiaTT, decimal triGiaTT, decimal triGiaKB_VND, decimal thueSuatXNK, decimal thueSuatTTDB, decimal thueSuatGTGT, decimal thueXNK, decimal thueTTDB, decimal thueGTGT, decimal phuThu, decimal tyLeThuKhac, decimal triGiaThuKhac, byte mienThue, string ma_HTS, string dVT_HTS, decimal soLuong_HTS, string thueSuatXNKGiam, string thueSuatTTDBGiam, string thueSuatVATGiam, double donGiaTuyetDoi, bool fOC, bool thueTuyetDoi, string maHSMoRong, string nhanHieu, string quyCachPhamChat, string thanhPhan, string model, string tenHangSX, string maHangSX, string maLoaiCo, bool noCo)
		{
			HangChuyenTiep entity = new HangChuyenTiep();	
			entity.Master_ID = master_ID;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.SoLuong = soLuong;
			entity.ID_NuocXX = iD_NuocXX;
			entity.ID_DVT = iD_DVT;
			entity.DonGia = donGia;
			entity.TriGia = triGia;
			entity.TrongLuong = trongLuong;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaKB_VND = triGiaKB_VND;
			entity.ThueSuatXNK = thueSuatXNK;
			entity.ThueSuatTTDB = thueSuatTTDB;
			entity.ThueSuatGTGT = thueSuatGTGT;
			entity.ThueXNK = thueXNK;
			entity.ThueTTDB = thueTTDB;
			entity.ThueGTGT = thueGTGT;
			entity.PhuThu = phuThu;
			entity.TyLeThuKhac = tyLeThuKhac;
			entity.TriGiaThuKhac = triGiaThuKhac;
			entity.MienThue = mienThue;
			entity.Ma_HTS = ma_HTS;
			entity.DVT_HTS = dVT_HTS;
			entity.SoLuong_HTS = soLuong_HTS;
			entity.ThueSuatXNKGiam = thueSuatXNKGiam;
			entity.ThueSuatTTDBGiam = thueSuatTTDBGiam;
			entity.ThueSuatVATGiam = thueSuatVATGiam;
			entity.DonGiaTuyetDoi = donGiaTuyetDoi;
			entity.FOC = fOC;
			entity.ThueTuyetDoi = thueTuyetDoi;
			entity.MaHSMoRong = maHSMoRong;
			entity.NhanHieu = nhanHieu;
			entity.QuyCachPhamChat = quyCachPhamChat;
			entity.ThanhPhan = thanhPhan;
			entity.Model = model;
			entity.TenHangSX = tenHangSX;
			entity.MaHangSX = maHangSX;
			entity.MaLoaiCo = maLoaiCo;
			entity.NoCo = noCo;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NChar, MaHS);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@ID_NuocXX", SqlDbType.Char, ID_NuocXX);
			db.AddInParameter(dbCommand, "@ID_DVT", SqlDbType.Char, ID_DVT);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Money, TriGia);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
			db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
			db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
			db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
			db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
			db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
			db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
			db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
			db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
			db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
			db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, ThueSuatXNKGiam);
			db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, ThueSuatTTDBGiam);
			db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, ThueSuatVATGiam);
			db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
			db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
			db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
			db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
			db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
			db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
			db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
			db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
			db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
			db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
			db.AddInParameter(dbCommand, "@MaLoaiCo", SqlDbType.VarChar, MaLoaiCo);
			db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Bit, NoCo);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HangChuyenTiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangChuyenTiep item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHangChuyenTiep(long id, long master_ID, int soThuTuHang, string maHang, string tenHang, string maHS, decimal soLuong, string iD_NuocXX, string iD_DVT, decimal donGia, decimal triGia, decimal trongLuong, decimal donGiaTT, decimal triGiaTT, decimal triGiaKB_VND, decimal thueSuatXNK, decimal thueSuatTTDB, decimal thueSuatGTGT, decimal thueXNK, decimal thueTTDB, decimal thueGTGT, decimal phuThu, decimal tyLeThuKhac, decimal triGiaThuKhac, byte mienThue, string ma_HTS, string dVT_HTS, decimal soLuong_HTS, string thueSuatXNKGiam, string thueSuatTTDBGiam, string thueSuatVATGiam, double donGiaTuyetDoi, bool fOC, bool thueTuyetDoi, string maHSMoRong, string nhanHieu, string quyCachPhamChat, string thanhPhan, string model, string tenHangSX, string maHangSX, string maLoaiCo, bool noCo)
		{
			HangChuyenTiep entity = new HangChuyenTiep();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.SoLuong = soLuong;
			entity.ID_NuocXX = iD_NuocXX;
			entity.ID_DVT = iD_DVT;
			entity.DonGia = donGia;
			entity.TriGia = triGia;
			entity.TrongLuong = trongLuong;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaKB_VND = triGiaKB_VND;
			entity.ThueSuatXNK = thueSuatXNK;
			entity.ThueSuatTTDB = thueSuatTTDB;
			entity.ThueSuatGTGT = thueSuatGTGT;
			entity.ThueXNK = thueXNK;
			entity.ThueTTDB = thueTTDB;
			entity.ThueGTGT = thueGTGT;
			entity.PhuThu = phuThu;
			entity.TyLeThuKhac = tyLeThuKhac;
			entity.TriGiaThuKhac = triGiaThuKhac;
			entity.MienThue = mienThue;
			entity.Ma_HTS = ma_HTS;
			entity.DVT_HTS = dVT_HTS;
			entity.SoLuong_HTS = soLuong_HTS;
			entity.ThueSuatXNKGiam = thueSuatXNKGiam;
			entity.ThueSuatTTDBGiam = thueSuatTTDBGiam;
			entity.ThueSuatVATGiam = thueSuatVATGiam;
			entity.DonGiaTuyetDoi = donGiaTuyetDoi;
			entity.FOC = fOC;
			entity.ThueTuyetDoi = thueTuyetDoi;
			entity.MaHSMoRong = maHSMoRong;
			entity.NhanHieu = nhanHieu;
			entity.QuyCachPhamChat = quyCachPhamChat;
			entity.ThanhPhan = thanhPhan;
			entity.Model = model;
			entity.TenHangSX = tenHangSX;
			entity.MaHangSX = maHangSX;
			entity.MaLoaiCo = maLoaiCo;
			entity.NoCo = noCo;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_HangChuyenTiep_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NChar, MaHS);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@ID_NuocXX", SqlDbType.Char, ID_NuocXX);
			db.AddInParameter(dbCommand, "@ID_DVT", SqlDbType.Char, ID_DVT);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Money, TriGia);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
			db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
			db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
			db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
			db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
			db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
			db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
			db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
			db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
			db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
			db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, ThueSuatXNKGiam);
			db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, ThueSuatTTDBGiam);
			db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, ThueSuatVATGiam);
			db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
			db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
			db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
			db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
			db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
			db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
			db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
			db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
			db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
			db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
			db.AddInParameter(dbCommand, "@MaLoaiCo", SqlDbType.VarChar, MaLoaiCo);
			db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Bit, NoCo);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HangChuyenTiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangChuyenTiep item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHangChuyenTiep(long id, long master_ID, int soThuTuHang, string maHang, string tenHang, string maHS, decimal soLuong, string iD_NuocXX, string iD_DVT, decimal donGia, decimal triGia, decimal trongLuong, decimal donGiaTT, decimal triGiaTT, decimal triGiaKB_VND, decimal thueSuatXNK, decimal thueSuatTTDB, decimal thueSuatGTGT, decimal thueXNK, decimal thueTTDB, decimal thueGTGT, decimal phuThu, decimal tyLeThuKhac, decimal triGiaThuKhac, byte mienThue, string ma_HTS, string dVT_HTS, decimal soLuong_HTS, string thueSuatXNKGiam, string thueSuatTTDBGiam, string thueSuatVATGiam, double donGiaTuyetDoi, bool fOC, bool thueTuyetDoi, string maHSMoRong, string nhanHieu, string quyCachPhamChat, string thanhPhan, string model, string tenHangSX, string maHangSX, string maLoaiCo, bool noCo)
		{
			HangChuyenTiep entity = new HangChuyenTiep();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.SoLuong = soLuong;
			entity.ID_NuocXX = iD_NuocXX;
			entity.ID_DVT = iD_DVT;
			entity.DonGia = donGia;
			entity.TriGia = triGia;
			entity.TrongLuong = trongLuong;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaKB_VND = triGiaKB_VND;
			entity.ThueSuatXNK = thueSuatXNK;
			entity.ThueSuatTTDB = thueSuatTTDB;
			entity.ThueSuatGTGT = thueSuatGTGT;
			entity.ThueXNK = thueXNK;
			entity.ThueTTDB = thueTTDB;
			entity.ThueGTGT = thueGTGT;
			entity.PhuThu = phuThu;
			entity.TyLeThuKhac = tyLeThuKhac;
			entity.TriGiaThuKhac = triGiaThuKhac;
			entity.MienThue = mienThue;
			entity.Ma_HTS = ma_HTS;
			entity.DVT_HTS = dVT_HTS;
			entity.SoLuong_HTS = soLuong_HTS;
			entity.ThueSuatXNKGiam = thueSuatXNKGiam;
			entity.ThueSuatTTDBGiam = thueSuatTTDBGiam;
			entity.ThueSuatVATGiam = thueSuatVATGiam;
			entity.DonGiaTuyetDoi = donGiaTuyetDoi;
			entity.FOC = fOC;
			entity.ThueTuyetDoi = thueTuyetDoi;
			entity.MaHSMoRong = maHSMoRong;
			entity.NhanHieu = nhanHieu;
			entity.QuyCachPhamChat = quyCachPhamChat;
			entity.ThanhPhan = thanhPhan;
			entity.Model = model;
			entity.TenHangSX = tenHangSX;
			entity.MaHangSX = maHangSX;
			entity.MaLoaiCo = maLoaiCo;
			entity.NoCo = noCo;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NChar, MaHS);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@ID_NuocXX", SqlDbType.Char, ID_NuocXX);
			db.AddInParameter(dbCommand, "@ID_DVT", SqlDbType.Char, ID_DVT);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Money, TriGia);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
			db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
			db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
			db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
			db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
			db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
			db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
			db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
			db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
			db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
			db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, ThueSuatXNKGiam);
			db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, ThueSuatTTDBGiam);
			db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, ThueSuatVATGiam);
			db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
			db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
			db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
			db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
			db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
			db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
			db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
			db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
			db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
			db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
			db.AddInParameter(dbCommand, "@MaLoaiCo", SqlDbType.VarChar, MaLoaiCo);
			db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Bit, NoCo);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HangChuyenTiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangChuyenTiep item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHangChuyenTiep(long id)
		{
			HangChuyenTiep entity = new HangChuyenTiep();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_Master_ID(long master_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_DeleteBy_Master_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HangChuyenTiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangChuyenTiep item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}