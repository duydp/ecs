-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]
	@Master_ID bigint,
	@MaSacThueAnHan varchar(1),
	@TenSacThueAnHan nvarchar(9),
	@HanNopThueSauKhiAnHan datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
(
	[Master_ID],
	[MaSacThueAnHan],
	[TenSacThueAnHan],
	[HanNopThueSauKhiAnHan]
)
VALUES 
(
	@Master_ID,
	@MaSacThueAnHan,
	@TenSacThueAnHan,
	@HanNopThueSauKhiAnHan
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaSacThueAnHan varchar(1),
	@TenSacThueAnHan nvarchar(9),
	@HanNopThueSauKhiAnHan datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
SET
	[Master_ID] = @Master_ID,
	[MaSacThueAnHan] = @MaSacThueAnHan,
	[TenSacThueAnHan] = @TenSacThueAnHan,
	[HanNopThueSauKhiAnHan] = @HanNopThueSauKhiAnHan
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaSacThueAnHan varchar(1),
	@TenSacThueAnHan nvarchar(9),
	@HanNopThueSauKhiAnHan datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan] 
		SET
			[Master_ID] = @Master_ID,
			[MaSacThueAnHan] = @MaSacThueAnHan,
			[TenSacThueAnHan] = @TenSacThueAnHan,
			[HanNopThueSauKhiAnHan] = @HanNopThueSauKhiAnHan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
		(
			[Master_ID],
			[MaSacThueAnHan],
			[TenSacThueAnHan],
			[HanNopThueSauKhiAnHan]
		)
		VALUES 
		(
			@Master_ID,
			@MaSacThueAnHan,
			@TenSacThueAnHan,
			@HanNopThueSauKhiAnHan
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSacThueAnHan],
	[TenSacThueAnHan],
	[HanNopThueSauKhiAnHan]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaSacThueAnHan],
	[TenSacThueAnHan],
	[HanNopThueSauKhiAnHan]
FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSacThueAnHan],
	[TenSacThueAnHan],
	[HanNopThueSauKhiAnHan]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]	

GO

