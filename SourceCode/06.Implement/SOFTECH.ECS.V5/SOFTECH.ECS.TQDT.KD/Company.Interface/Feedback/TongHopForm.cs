﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Feedback.Components;

namespace Company.Interface.Feedback
{
    public partial class TongHopForm : BaseForm
    {
        private DataTable dtThuVienCauHoi = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["ThuVienCauHoi"];
       
        public TongHopForm()
        {
            InitializeComponent();

            FeedbackGlobalSettings.KhoiTao_GopYMacDinh();
        }

        private void TongHopForm_Load(object sender, EventArgs e)
        {
            if (dtThuVienCauHoi == null)
                dtThuVienCauHoi = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["ThuVienCauHoi"];

            LoadDanhSachCauHoi();
        }

        private void LoadDanhSachCauHoi()
        {
            try
            {
                cauHoiUserControl1.DataBinding(linhVucUserControl1.GetMa());
            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void linhVucUserControl1_LinhVucRowCheckStateChanged(object sender, Janus.Windows.GridEX.RowCheckStateChangeEventArgs e)
        {
            LoadDanhSachCauHoi();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }

        private void cauHoiUserControl1_CauHoiSelectionChanged(object sender, EventArgs e)
        {
            LoadNoiDungCauHoi();
        }

        private void LoadNoiDungCauHoi()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                DataRow rowSelected = cauHoiUserControl1.CurrentRow();

                if (rowSelected == null)
                {
                    txtNoiDung.Text = txtTraLoi.Text = "";
                    return;
                }

                //Delete in memory
                DataRow[] rows = dtThuVienCauHoi.Select("ThuVienID = " + rowSelected["ThuVienID"] + "");

                if (rows.Length > 0)
                {
                    txtNoiDung.TextChanged -= new EventHandler(textBox_TextChanged);
                    txtTraLoi.TextChanged -= new EventHandler(textBox_TextChanged);

                    txtNoiDung.Text = rows[0]["NoiDungCauHoi"].ToString();
                    txtTraLoi.Text = rows[0]["TraLoi"].ToString();

                    txtNoiDung.TextChanged += new EventHandler(textBox_TextChanged);
                    txtTraLoi.TextChanged += new EventHandler(textBox_TextChanged);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage(ex.Message, false);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            SetChanged(true);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            FeedbackGlobalSettings.KhoiTao_GopYMacDinh();

            LoadDanhSachCauHoi();

            SetChanged(false);
        }
    }
}
