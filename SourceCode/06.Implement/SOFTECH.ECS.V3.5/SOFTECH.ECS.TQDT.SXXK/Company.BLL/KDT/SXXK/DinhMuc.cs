﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.BLL.KDT.SXXK
{
    public partial class DinhMuc
    {
        //---------------------------------------------------------------------------------------------
        public ThuTucHQTruocDo ThuTucTruocDo { get; set; }
        public bool Load()
        {
            DinhMuc entity = null;
            string where = "MaSanPham='" + this.MaSanPham + "'";
            IDataReader reader = SelectReaderDynamic(where, "");
            if (reader.Read())
            {
                entity = new DinhMuc();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));


                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public static DataSet GetLuongNguyenPhuLieuTheoDM(string maSP, decimal soluong, int SoTPNPL)
        {
            string sql = "SELECT MaSanPham,MaNguyenPhuLieu,dinhMucChung,round((dinhMucChung * @SoLuong),@SoTPDM) as LuongCanDung FROM t_SXXK_DinhMuc where MaSanPham=@MaSanPham";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, soluong);
            db.AddInParameter(dbCommand, "@SoTPDM", SqlDbType.Int, SoTPNPL);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public static DinhMucCollection SelectCollectionBy_Master_ID(long master_ID, SqlTransaction transaction)
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);

            IDataReader reader = db.ExecuteReader(dbCommand, transaction);
            DinhMucCollection collection = new DinhMucCollection();
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));

                collection.Add(entity);
            }
            reader.Close();
            dbCommand.Connection.Close();
            return collection;

        }

        public bool CloneToDB(SqlTransaction transaction)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand("p_KDT_SXXK_DinhMuc_Copy");
                db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
                if (transaction != null)
                    db.ExecuteNonQuery(dbCommand, transaction);
                else
                    db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch
            {
                return false;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransactionBy(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_SXXK_DinhMuc_InsertUpdateBy";

            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this._GhiChu);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public static DataSet GetALLDanhSachDMLocalTQDT(string MaHaiQuan, string MaDoanhNghiep, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay)
        {
            string query = "SELECT  DM.Master_ID,  " +
                            "DM.MaSanPham AS Ma_SP,  " +
                            "DM.MaNguyenPhuLieu AS Ma_NPL,  " +
                            "DM.DinhMucSuDung AS DM_SD,  " +
                            "DM.TyLeHaoHut AS TL_HH , " +
                            "DM.GhiChu AS GHI_CHU " +
                            "FROM    dbo.t_KDT_SXXK_DinhMuc DM " +
                            "INNER JOIN dbo.t_KDT_SXXK_DinhMucDangKy DMDK ON DM.Master_ID = DMDK.ID " +
                            "WHERE   DMDK.MaHaiQuan = '" + MaHaiQuan + "' " +
                            "AND DMDK.MaDoanhNghiep = '" + MaDoanhNghiep + "' ";
            if (trangThaiDaDuyet)
                query += " AND DMDK.TrangThaiXuLy = 1";
            if (tuNgay > DateTime.MinValue)
                query += " and  DMDK.NgayTiepNhan >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND  DMDK.NgayTiepNhan <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            query += " ORDER BY  DMDK.NgayTiepNhan";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(cmd);
        }

        public static DinhMucCollection SelectDistinctCollectionDynamicSP(string WhereCondition, string OrderByExpression)
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectDynamicSP";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, WhereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, OrderByExpression);
            IDataReader reader = db.ExecuteReader(dbCommand);
            DinhMucCollection collection = new DinhMucCollection();
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDinhDanhLenhSX"))) entity.MaDinhDanhLenhSX = reader.GetString(reader.GetOrdinal("MaDinhDanhLenhSX"));
                collection.Add(entity);
            }
            reader.Close();
            dbCommand.Connection.Close();
            return collection;

        }
        public static DinhMucCollection SelectDistinctCollectionDynamicLSX(string WhereCondition, string OrderByExpression)
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectDistinctDynamicLSX";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, WhereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, OrderByExpression);
            IDataReader reader = db.ExecuteReader(dbCommand);
            DinhMucCollection collection = new DinhMucCollection();
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDinhDanhLenhSX"))) entity.MaDinhDanhLenhSX = reader.GetString(reader.GetOrdinal("MaDinhDanhLenhSX"));
                collection.Add(entity);
            }
            reader.Close();
            dbCommand.Connection.Close();
            return collection;

        }

        public static bool checkDinhMucExit(string masp, long master_ID, string mahq, string madv)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "select dm.id " +
                       "from t_KDT_SXXK_DinhMuc dm inner join t_KDT_SXXK_DinhMucDangKy dmdk on " +
                       "dm.Master_ID=dmdk.ID " +
                       "where dm.MaSanPham=@MaSP and dm.Master_ID<>@Master_ID and dmdk.TrangThaiXuLy<>1 " +
                       " and dmdk.MaHaiQuan=@MaHaiQuan and dmdk.MaDoanhNghiep=@MaDoanhNghiep";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahq);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, masp);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            return true;
        }
        public static long GetIDDinhMucExit(string masp, long master_ID, string mahq, string madv)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "select dmdk.ID " +
                       "from t_KDT_SXXK_DinhMuc dm inner join t_KDT_SXXK_DinhMucDangKy dmdk on " +
                       "dm.Master_ID=dmdk.ID " +
                       "where dm.MaSanPham=@MaSP and dm.Master_ID<>@Master_ID and dmdk.TrangThaiXuLy NOT IN (10) " +
                       " and dmdk.MaHaiQuan=@MaHaiQuan and dmdk.MaDoanhNghiep=@MaDoanhNghiep";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahq);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, masp);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return 0;
            return Convert.ToInt64(o);
        }
        public static long GetIDDinhMucExit(string masp, long master_ID, string mahq, string madv, string maLenhSanXuat)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = 
                       " select dmdk.ID " +
                       " from t_KDT_SXXK_DinhMuc dm inner join t_KDT_SXXK_DinhMucDangKy dmdk on " +
                       " dm.Master_ID=dmdk.ID " +
                       " where dm.MaSanPham=@MaSP and dm.Master_ID<>@Master_ID and dmdk.TrangThaiXuLy NOT IN (10) " +
                       " and dmdk.MaHaiQuan=@MaHaiQuan and dmdk.MaDoanhNghiep=@MaDoanhNghiep AND dm.MaDinhDanhLenhSX = '@MaDinhDanhLenhSX' ";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahq);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, masp);
            db.AddInParameter(dbCommand, "@MaDinhDanhLenhSX", SqlDbType.VarChar, maLenhSanXuat);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return 0;
            return Convert.ToInt64(o);
        }

        public static DinhMuc GetDinhMucByMaSP(string maSP, string mahq, string madv)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "select * " +
                       "from t_KDT_SXXK_DinhMuc dm inner join t_KDT_SXXK_DinhMucDangKy dmdk on " +
                       "dm.Master_ID=dmdk.ID " +
                       "where dm.MaSanPham=@MaSP " +
                       " and dmdk.MaHaiQuan=@MaHaiQuan and dmdk.MaDoanhNghiep=@MaDoanhNghiep";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahq);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, maSP);
            DinhMuc entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDinhDanhLenhSX"))) entity.MaDinhDanhLenhSX = reader.GetString(reader.GetOrdinal("MaDinhDanhLenhSX"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }
        public static DinhMuc GetDinhMucByMaSP_NPL(string maSP, string maNPL, string mahq, string madv)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "select * " +
                       "from t_KDT_SXXK_DinhMuc dm inner join t_KDT_SXXK_DinhMucDangKy dmdk on " +
                       "dm.Master_ID=dmdk.ID " +
                       "where dm.MaSanPham=@MaSP " +
                       "and dm.MaNguyenPhuLieu=@maNPL" +
                       " and dmdk.MaHaiQuan=@MaHaiQuan and dmdk.MaDoanhNghiep=@MaDoanhNghiep";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahq);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@maNPL", SqlDbType.VarChar, maNPL);
            DinhMuc entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDinhDanhLenhSX"))) entity.MaDinhDanhLenhSX = reader.GetString(reader.GetOrdinal("MaDinhDanhLenhSX"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }
    }
}
