﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Category
{
    public partial class VNACC_Category_Common : BaseForm
    {
        public Company.KDT.SHARE.VNACCS.VNACC_Category_Common common;
        public string where = "";
        public VNACC_Category_Common()
        {
            InitializeComponent();
        }

        private void VNACC_Category_Common_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = Company.KDT.SHARE.VNACCS.VNACC_Category_Common.SelectCollectionDynamic(GetSearchWhere(), null);
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ClearText()
        {
            //txtCode.Text = String.Empty;
            txtName.Text = String.Empty;
            txtName_EN.Text = String.Empty;
            txtNotes.Text = String.Empty;
            txtTableID.Text = String.Empty;
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtCode, errorProvider, "Mã danh mục ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtName, errorProvider, "Tên tiếng việt ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtName_EN, errorProvider, "Tên tiếng anh ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNotes, errorProvider, "Ghi chú ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTableID, errorProvider, "Table ID ", isOnlyWarning);
            }
            catch (Exception ex)
            {
               //Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void Get()
        {
            try
            {
                common.Code = txtCode.Text.Trim().ToUpper();
                common.Name_VN = txtName.Text.Trim().ToUpper();
                common.Name_EN = txtName_EN.Text.Trim().ToUpper();
                common.Notes = txtNotes.Text.Trim().ToUpper();
                common.ReferenceDB = txtTableID.Text.Trim().ToUpper();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void Set()
        {
            try
            {
                txtCode.Text = common.Code;
                txtName.Text = common.Name_VN;
                txtName_EN.Text = common.Name_EN;
                txtNotes.Text = common.Notes;
                txtTableID.Text = common.ReferenceDB;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtCode.Text))
                    where += " AND Code = '" + txtCode.Text.Trim() + "'";
                if (!String.IsNullOrEmpty(txtName.Text))
                    where += " AND Name_VN LIKE N'%" + txtName.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtName_EN.Text))
                    where += " AND Name_EN LIKE N'%" + txtName_EN.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtNotes.Text))
                    where += " AND Notes LIKE N'%" + txtNotes.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtTableID.Text))
                    where += " AND ReferenceDB LIKE N'%" + txtTableID.Text.Trim() + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return where;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                Get();
                //common.ReferenceDB = "E001";
                common.InsertUpdate();
                ShowMessage("Lưu thành công ", false);
                common = new Company.KDT.SHARE.VNACCS.VNACC_Category_Common();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Get();
                if (ShowMessage("Bạn có muốn xóa dòng này không ? ", true) == "Yes")
                {
                    common.Delete();
                }
                ShowMessage("Xóa thành công ", false);
                common = new Company.KDT.SHARE.VNACCS.VNACC_Category_Common();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {

                if (e.Row.RowType == RowType.Record)
                {
                    common = new Company.KDT.SHARE.VNACCS.VNACC_Category_Common();
                    common = (Company.KDT.SHARE.VNACCS.VNACC_Category_Common)e.Row.DataRow;
                    Set();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                btnSearch_Click(null,null);
            }
        }
    }
}
