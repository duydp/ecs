﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Janus.Windows.GridEX;
using DevExpress.XtraCharts;

using Company.Interface.Report;
using Company.Interface.Report.SXXK;

using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using HangChuyenTiep = Company.GC.BLL.KDT.GC.HangChuyenTiep;
using Company.KDT.SHARE.QuanLyChungTu;
namespace Company.Interface
{
    public partial class HoaDonFormGCCT : BaseForm
    {
        public ToKhaiChuyenTiep TKCT;
        public HoaDon HDonTM = new HoaDon() { TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO };
        public List<HoaDonChiTiet> listHDTMDetail = new List<HoaDonChiTiet>();
        public bool isKhaiBoSung = false;
        public HoaDonFormGCCT()
        {
            InitializeComponent();

            SetEvent_TextBox_DoiTac();
        }

        private void HoaDonThuongMaiForm_Load(object sender, EventArgs e)
        {
            //Update by KHANHHN - 03/03/2012
            // Tạo mới HDonTM với trạng thái chưa khao báo
            if (HDonTM == null)
                HDonTM = new HoaDon { TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO };
            btnKhaiBao.Enabled = false;
            btnLayPhanHoi.Enabled = false;

            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
            //cbPTTT.SelectedValue = TKCT.PTTT_ID;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.DisplayMember = cbDKGH.ValueMember = "ID";
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            //cbDKGH.SelectedValue = TKCT.DKGH_ID;

            if (HDonTM != null && HDonTM.ID <= 0)
            {
                //DATLMQ bổ sung lấy HĐơn từ TKCT 13/12/2010

                if (TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    txtMaDVMua.Text = TKCT.MaDoanhNghiep;
                    txtTenDVMua.Text = GlobalSettings.TEN_DON_VI;
                    txtTenDVBan.Text = TKCT.TenDonViDoiTac;
                }
                else
                {
                    txtTenDVMua.Text = TKCT.TenDonViDoiTac;
                    txtMaDVBan.Text = TKCT.MaDoanhNghiep;
                    txtTenDVBan.Text = GlobalSettings.TEN_DON_VI;
                }
            }
            else if (HDonTM != null && HDonTM.ID > 0)
            {
                cbDKGH.SelectedValue = HDonTM.DKGH_ID;
                txtMaDVBan.Text = HDonTM.MaDonViBan;
                txtMaDVMua.Text = HDonTM.MaDonViMua;
                ccNgayVanDon.Value = HDonTM.NgayHoaDon;
                nguyenTeControl2.Ma = HDonTM.NguyenTe_ID;
                cbPTTT.SelectedValue = HDonTM.PTTT_ID;
                txtSoVanDon.Text = HDonTM.SoHoaDon;
                txtTenDVBan.Text = HDonTM.TenDonViBan;
                txtTenDVMua.Text = HDonTM.TenDonViMua;
                txtThongTinKhac.Text = HDonTM.ThongTinKhac;
                BindData();
            }
            if (HDonTM.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                ccNgayTiepNhan.Text = HDonTM.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = HDonTM.SoTiepNhan + "";
            }
            SetButtonStateHOADON(TKCT, isKhaiBoSung, HDonTM);
        }
        private void BindData()
        {
            //Company.GC.BLL.KDT.HangMauDich hmd = new Company.GC.BLL.KDT.HangMauDich();
            //hmd.TKMD_ID = TKCT.ID;
            //DataTable dt = hmd.SelectBy_TKMD_ID().Tables[0];
            //dgList.DataSource = HDonTM.ConvertListToDataSet(dt);

            dgList.DataSource = HDonTM.ListHangMDOfHoaDon;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private bool checkSoHoaDon(string soHoaDon)
        {
            foreach (HoaDon HDTM in TKCT.HoaDonThuongMaiCollection)
            {
                if (HDTM.SoHoaDon.Trim().ToUpper() == soHoaDon.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (HDonTM.ListHangMDOfHoaDon.Count == 0)
            {
                ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                return;
            }


            TKCT.HoaDonThuongMaiCollection.Remove(HDonTM);

            if (checkSoHoaDon(txtSoVanDon.Text.Trim()))
            {
                if (HDonTM.ID > 0)
                    TKCT.HoaDonThuongMaiCollection.Add(HDonTM);
                ShowMessage("Số hóa đơn này đã tồn tại.", false);
                return;
            }

            if (!ValidateHoaDon())
                return;

            HDonTM.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
            HDonTM.MaDonViBan = txtMaDVBan.Text.Trim();
            HDonTM.MaDonViMua = txtMaDVMua.Text.Trim();
            HDonTM.NgayHoaDon = ccNgayVanDon.Value;
            HDonTM.NguyenTe_ID = nguyenTeControl2.Ma;
            HDonTM.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
            HDonTM.SoHoaDon = txtSoVanDon.Text.Trim();
            HDonTM.TenDonViBan = txtTenDVBan.Text.Trim();
            HDonTM.TenDonViMua = txtTenDVMua.Text.Trim();
            HDonTM.ThongTinKhac = txtThongTinKhac.Text.Trim();
            HDonTM.ID_TK = TKCT.ID;
            HDonTM.LoaiTK = "TKCT";
            HDonTM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            if (isKhaiBoSung)
                HDonTM.LoaiKB = 1;
            else
                HDonTM.LoaiKB = 0;

            GridEXRow[] rowCollection = dgList.GetRows();
            /*
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                                
                foreach (HoaDonChiTiet item in HDonTM.ListHangMDOfHoaDon)
                {
                    
                    if (item.HMD_ID.ToString().Trim() == rowview["HMD_ID"].ToString().Trim())
                    {
                        item.GhiChu = rowview["GhiChu"].ToString();
                        item.GiaiTriDieuChinhGiam = Convert.ToDouble(rowview["GiaiTriDieuChinhGiam"]);
                        item.GiaTriDieuChinhTang = Convert.ToDouble(rowview["GiaTriDieuChinhTang"]);

                        item.MaHS = rowview["MaHS"].ToString();
                        item.MaPhu = rowview["MaPhu"].ToString();
                        item.TenHang = rowview["TenHang"].ToString();
                        item.NuocXX_ID = rowview["NuocXX_ID"].ToString();
                        item.DVT_ID = rowview["DVT_ID"].ToString();
                        item.SoLuong = Convert.ToDecimal(rowview["SoLuong"]);
                        item.DonGiaKB = Convert.ToDouble(rowview["DonGiaKB"]);
                        item.TriGiaKB = Convert.ToDouble(rowview["TriGiaKB"]);

                        break;
                    }
                    
                }
                
            }
            */

            try
            {
                HDonTM.InsertUpdateFull();
                TKCT.HoaDonThuongMaiCollection.Add(HDonTM);
                BindData();
                ShowMessage("Lưu thành công.", false);
                //if (TKCT.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                //    btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HoaDonChiTiet> HoaDonThuongMaiDetailCollection = new List<HoaDonChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonChiTiet hdtmdtmp = new HoaDonChiTiet();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HoaDonThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HoaDonChiTiet hdtmtmp in HoaDonThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HoaDonChiTiet hdtmd in HDonTM.ListHangMDOfHoaDon)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HDonTM.ListHangMDOfHoaDon.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public List<HangChuyenTiep> ConvertListToHangMauDichCollection(List<HoaDonChiTiet> listHangHoaDon, List<HangChuyenTiep> hangCollection)
        {
            List<HangChuyenTiep> tmp = new List<HangChuyenTiep>();

            foreach (HoaDonChiTiet item in listHangHoaDon)
            {
                foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichFormGCCT f = new SelectHangMauDichFormGCCT();
            f.TKCT = TKCT;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất HMD
            //if (HDonTM.ListHangMDOfHoaDon.Count > 0)
            //{
            //    f.TKCT.HMDCollection = ConvertListToHangMauDichCollection(HDonTM.ListHangMDOfHoaDon, TKCT.HMDCollection);
            //}
            f.ShowDialog();
            if (f.HCTCollection.Count > 0)
            {
                foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HMD in f.HCTCollection)
                {
                    bool ok = false;
                    foreach (HoaDonChiTiet hangHoaDonDetail in HDonTM.ListHangMDOfHoaDon)
                    {
                        if (hangHoaDonDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HoaDonChiTiet hoaDonDetail = new HoaDonChiTiet();
                        hoaDonDetail.HMD_ID = HMD.ID;
                        hoaDonDetail.HoaDonTM_ID = HDonTM.ID;
                        hoaDonDetail.MaPhu = HMD.MaHang;
                        hoaDonDetail.MaHS = HMD.MaHS;
                        hoaDonDetail.TenHang = HMD.TenHang;
                        hoaDonDetail.DVT_ID = HMD.ID_DVT;
                        hoaDonDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hoaDonDetail.SoLuong = HMD.SoLuong;
                        hoaDonDetail.NuocXX_ID = HMD.ID_NuocXX;
                        hoaDonDetail.DonGiaKB = Convert.ToDouble(HMD.DonGia);
                        hoaDonDetail.TriGiaKB = Convert.ToDouble(HMD.TriGia);

                        HDonTM.ListHangMDOfHoaDon.Add(hoaDonDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (HDonTM.ID == 0)
            {
                ShowMessage("Chưa lưu thông tin hóa đơn", false);
                return;
            }
            if (HDonTM.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
            {
                ShowMessage("Đã khai báo đến hải quan. Nhấn nút [Lấy phản hồi] để nhận thông tin từ hải quan", false);
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
            }

            try
            {
                if ((HDonTM.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO) || (HDonTM.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET))
                    HDonTM.GuidStr = Guid.NewGuid().ToString();
                else if (HDonTM.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    return;
                }
                HDonTM.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                ObjectSend msgSend = SingleMessage.BoSungChungTuGCCT(TKCT, HDonTM, GlobalSettings.TEN_DON_VI);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);
                if (isSend && HDonTM.TrangThai != TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    HDonTM.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    sendMessageForm.Message.XmlSaveMessage(HDonTM.ID, MessageTitle.KhaiBaoBoSungHoaDonTM);
                    HDonTM.Update();
                    SetButtonStateHOADON(TKCT, isKhaiBoSung, HDonTM);
                    btnLayPhanHoi_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            ObjectSend msgSend = SingleMessage.FeedBackGCCT(TKCT, HDonTM.GuidStr);
            SendMessageForm sendMessageForm = new SendMessageForm();
            sendMessageForm.Send += SendMessage;
            sendMessageForm.DoSend(msgSend);

        }

        #region Begin Doi tac TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick, Leave cho các TextBox Mã đơn vị mua, bán.
        /// </summary>
        /// Hungtq, Update 30052010
        private void SetEvent_TextBox_DoiTac()
        {
            txtMaDVMua.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtMaDVBan.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;

            txtMaDVMua.ButtonClick += new EventHandler(txtMaDVMua_ButtonClick);

            txtMaDVBan.ButtonClick += new EventHandler(txtMaDVBan_ButtonClick);

            txtMaDVMua.Leave += new EventHandler(txtMaDVMua_Leave);

            txtMaDVBan.Leave += new EventHandler(txtMaDVBan_Leave);
        }

        private void txtMaDVMua_ButtonClick(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaDVMua.Text = objDoiTac.MaCongTy;
                txtTenDVMua.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVBan_ButtonClick(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaDVBan.Text = objDoiTac.MaCongTy;
                txtTenDVBan.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVMua_Leave(object sender, EventArgs e)
        {
            if (txtMaDVMua.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVMua.Text.Trim()) != "")
                txtTenDVMua.Text = DoiTac.GetName(txtMaDVMua.Text.Trim());
        }

        private void txtMaDVBan_Leave(object sender, EventArgs e)
        {
            if (txtMaDVBan.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVBan.Text.Trim()) != "")
                txtTenDVBan.Text = DoiTac.GetName(txtMaDVBan.Text.Trim());
        }

        #endregion End Doi tac TextBox

        #region Begin VALIDATE HOA DON

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateHoaDon()
        {
            bool isValid = true;

            //So_CT	varchar(50)
            isValid = Globals.ValidateLength(txtSoVanDon, 50, err, "Số vận đơn");

            //Ma_PTTT	varchar(10)
            isValid &= Globals.ValidateLength(cbPTTT, 10, err, "Phương thức thanh toán");

            //Ma_GH	varchar(7)
            isValid &= Globals.ValidateLength(cbDKGH, 7, err, "Điều kiện giao hàng");

            //Ma_NT	char(3)

            //Ma_DV	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVMua, 14, err, "Mã đơn vị mua");

            //Ma_DV_DT	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVBan, 14, err, "Mã đơn vị bán");

            return isValid;
        }

        #endregion End VALIDATE HOA DON

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOA DON.
        /// </summary>
        /// <param name="TKCT"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOADON(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon hoadon)
        {
            if (hoadon == null)
                return false;

            if (hoadon != null)
            {
                txtSoVanDon.Text = hoadon.SoHoaDon;
            }
            switch (hoadon.TrangThai)
            {
                case -1:
                    lbTrangThai.Text = "Chưa Khai Báo";
                    break;
                case -3:
                    lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                    break;
                case 0:
                    lbTrangThai.Text = "Chờ Duyệt";
                    break;
                case 1:
                    lbTrangThai.Text = "Đã Duyệt";
                    break;
                case 2:
                    lbTrangThai.Text = "Hải quan không phê duyệt";
                    break;

            }

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                     || TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                      || TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;

                btnChonHang.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            else if (TKCT.SoToKhai > 0 && TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {
                btnKetQuaXuLy.Enabled = true;
                //Re set button declaration LanNT


                bool khaiBaoEnable = (hoadon.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     hoadon.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnChonHang.Enabled = btnXoa.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = hoadon.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   hoadon.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   hoadon.TrangThai == TrangThaiXuLy.CHO_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }

            txtSoTiepNhan.Text = hoadon.SoTiepNhan > 0 ? hoadon.SoTiepNhan.ToString() : string.Empty;
            if (hoadon.NgayTiepNhan.Year > 1900) ccNgayTiepNhan.Value = hoadon.NgayTiepNhan;
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (HDonTM.GuidStr != null && HDonTM.GuidStr != "")
            {
                ThongDiepForm f = new ThongDiepForm();
                f.DeclarationIssuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                f.ItemID = HDonTM.ID;
                f.ShowDialog();
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }
        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            List<HoaDonChiTiet> HoaDonThuongMaiDetailCollection = new List<HoaDonChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonChiTiet hdtmdtmp = new HoaDonChiTiet();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HoaDonThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HoaDonChiTiet hdtmtmp in HoaDonThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HoaDonChiTiet hdtmd in HDonTM.ListHangMDOfHoaDon)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HDonTM.ListHangMDOfHoaDon.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
        #region  V3

        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    FeedBackContent feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(HDonTM.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                HDonTM.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                this.ShowMessageTQDT("Hải quan từ chối tiếp nhận", noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                            {
                                GlobalSettings.IsRemember = false;
                            }
                            noidung = noidung.Replace(string.Format("Message [{0}]", HDonTM.GuidStr), string.Empty);
                            this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                HDonTM.SoTiepNhan = long.Parse(vals[0].Trim());
                                HDonTM.NamTiepNhan = int.Parse(vals[1].Trim());
                                HDonTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(HDonTM.ID, MessageTitle.KhaiBaoBoSungHoaDonTMDuocChapNhan, noidung);
                                HDonTM.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                this.ShowMessageTQDT("Được cấp số tiếp nhận.\n" + HDonTM.SoTiepNhan.ToString() + "\n" + HDonTM.NgayTiepNhan.ToString(), false);
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HDonTM.ID, MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong, noidung);
                                HDonTM.TrangThai = TrangThaiXuLy.DA_DUYET;
                                ShowMessageTQDT("Chứng từ bổ sung đã được duyệt", false);

                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HDonTM.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }

                    if (isUpdate)
                    {
                        HDonTM.Update();
                        SetButtonStateHOADON(TKCT, isKhaiBoSung, HDonTM);
                    }

                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion
    }
}
