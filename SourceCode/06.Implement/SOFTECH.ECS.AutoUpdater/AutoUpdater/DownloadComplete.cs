﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml;
using ECS.AutoUpdate;

namespace ECS.AutoUpdater
{
    public partial class DownloadComplete : Form
    {
        public DownloadComplete()
        {
            InitializeComponent();
        }
        public DownloadComplete(string txt):this()
        {
            Text = "Auto Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + " - " + Config.Install.TitleUpdate;
            lbMsg.Text = txt;
            lbMsg.Visible = true;
        }
        private void DownloadComplete_Load(object sender, EventArgs e)
        {
            lbDbVersion.Text = UpdateSql.VersionDataBase();
        }
    }
}
