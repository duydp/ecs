﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_ChuyenCuaKhau_TrungChuyenForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiVanChuyen TKVC = new KDT_VNACC_ToKhaiVanChuyen();
        DataTable dt = new DataTable();
        DataSet dsMaDiaDiem = new DataSet();
        public VNACC_ChuyenCuaKhau_TrungChuyenForm()
        {
            InitializeComponent();
        }

        private void VNACC_ChuyenCuaKhau_TrungChuyenForm_Load(object sender, EventArgs e)
        {

            for (int i = 0; i < TKVC.TrungChuyenCollection.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        ctrMaTrungChuyen1.Code = TKVC.TrungChuyenCollection[0].MaDiaDiemTrungChuyen;
                        clcNgayDenDuKien1.Value = TKVC.TrungChuyenCollection[0].NgayDenDiaDiem_TC;
                        clcNgayDenThucTe1.Value = TKVC.TrungChuyenCollection[0].NgayDenThucTe;
                        clcNgayDiDuKien1.Value = TKVC.TrungChuyenCollection[0].NgayDiDiaDiem_TC;
                        clcNgayDiThucTe1.Value = TKVC.TrungChuyenCollection[0].NgayDiThucTe;
                        break;

                    case 1:
                        ctrMaTrungChuyen2.Code = TKVC.TrungChuyenCollection[1].MaDiaDiemTrungChuyen;
                        clcNgayDenDuKien2.Value = TKVC.TrungChuyenCollection[1].NgayDenDiaDiem_TC;
                        clcNgayDenThucTe2.Value = TKVC.TrungChuyenCollection[1].NgayDenThucTe;
                        clcNgayDiDuKien2.Value = TKVC.TrungChuyenCollection[1].NgayDiDiaDiem_TC;
                        clcNgayDiThucTe2.Value = TKVC.TrungChuyenCollection[1].NgayDiThucTe;
                        break;

                    case 2:
                        ctrMaTrungChuyen3.Code = TKVC.TrungChuyenCollection[2].MaDiaDiemTrungChuyen;
                        clcNgayDenDuKien3.Value = TKVC.TrungChuyenCollection[2].NgayDenDiaDiem_TC;
                        clcNgayDenThucTe3.Value = TKVC.TrungChuyenCollection[2].NgayDenThucTe;
                        clcNgayDiDuKien3.Value = TKVC.TrungChuyenCollection[2].NgayDiDiaDiem_TC;
                        clcNgayDiThucTe3.Value = TKVC.TrungChuyenCollection[2].NgayDiThucTe;
                        break;

                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn lưu thông tin trung chuyển này không?", true) == "Yes")
                {
                    TKVC.TrungChuyenCollection.Clear();
                    KDT_VNACC_TKVC_TrungChuyen.DeleteDynamic("Master_ID =" + TKVC.ID);
                    for (int i = 0; i < 3; i++)
                    {
                        KDT_VNACC_TKVC_TrungChuyen TC = new KDT_VNACC_TKVC_TrungChuyen();
                        TC.Master_ID = TKVC.ID;

                        switch (i)
                        {
                            case 0:
                                if (ctrMaTrungChuyen1.Code != "")
                                {
                                    TC.MaDiaDiemTrungChuyen = ctrMaTrungChuyen1.Code;
                                    TC.NgayDenDiaDiem_TC = clcNgayDenDuKien1.Value;
                                    TC.NgayDenThucTe = clcNgayDenThucTe1.Value;
                                    TC.NgayDiDiaDiem_TC = clcNgayDiDuKien1.Value;
                                    TC.NgayDiThucTe = clcNgayDiThucTe1.Value;
                                    TKVC.TrungChuyenCollection.Add(TC);
                                }
                                break;

                            case 1:
                                if (ctrMaTrungChuyen2.Code != "")
                                {
                                    TC.MaDiaDiemTrungChuyen = ctrMaTrungChuyen2.Code;
                                    TC.NgayDenDiaDiem_TC = clcNgayDenDuKien2.Value;
                                    TC.NgayDenThucTe = clcNgayDenThucTe2.Value;
                                    TC.NgayDiDiaDiem_TC = clcNgayDiDuKien2.Value;
                                    TC.NgayDiThucTe = clcNgayDiThucTe2.Value;
                                    TKVC.TrungChuyenCollection.Add(TC);
                                }
                                break;
                            case 2:
                                if (ctrMaTrungChuyen3.Code != "")
                                {
                                    TC.MaDiaDiemTrungChuyen = ctrMaTrungChuyen3.Code;
                                    TC.NgayDenDiaDiem_TC = clcNgayDenDuKien3.Value;
                                    TC.NgayDenThucTe = clcNgayDenThucTe3.Value;
                                    TC.NgayDiDiaDiem_TC = clcNgayDiDuKien3.Value;
                                    TC.NgayDiThucTe = clcNgayDiThucTe3.Value;
                                    TKVC.TrungChuyenCollection.Add(TC);
                                }
                                break;

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }


    }
}
