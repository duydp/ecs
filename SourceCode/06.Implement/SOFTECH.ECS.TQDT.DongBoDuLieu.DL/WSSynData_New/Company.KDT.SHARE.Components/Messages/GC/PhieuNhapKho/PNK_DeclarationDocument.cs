﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho
{
    public  class PNK_DeclarationDocument
    {
        /// <summary>
        /// Số vận đơn
        /// 
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Loại
        /// 
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
    }
}
