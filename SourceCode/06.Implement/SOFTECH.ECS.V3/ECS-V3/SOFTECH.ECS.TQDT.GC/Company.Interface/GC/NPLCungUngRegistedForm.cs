﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.GC.BLL.KDT.SXXK;
using System.Xml.Serialization;
using Company.Interface.KDT.GC;
using Company.GC.BLL.KDT.GC;
using System.Data;

namespace Company.Interface.GC
{
    public partial class NPLCungUngRegistedForm : BaseForm
    {
        public long IDHopDong = 0;
        public NPLCungUngRegistedForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------


        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
            this.Cursor = Cursors.WaitCursor;
            this.search();
            this.Cursor = Cursors.Default;
            BindHopDong();
        }
        private void BindHopDong()
        {
            DataTable dt;
            {
                string where = string.Format("madoanhnghiep='{0}'", GlobalSettings.MA_DON_VI);
                HopDong HD = new HopDong();
                dt = HopDong.SelectDynamic(where, "").Tables[0];
            }
            cbHopDong.DataSource = dt;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            BKCungUngDangKy BKCU = (BKCungUngDangKy)e.Row.DataRow;
            NPLCungUngTheoTKSendForm f = new NPLCungUngTheoTKSendForm();
            BKCU.Load();
            if (BKCU.TKMD_ID > 0)
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = BKCU.TKMD_ID;
                TKMD.Load();
                f.TKMD = TKMD;
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = BKCU.TKCT_ID;
                TKCT = ToKhaiChuyenTiep.Load(BKCU.TKCT_ID);
                f.TKCT = TKCT;
            }
            f.BKCU = BKCU;
            f.ShowDialog();
            search();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            //p_NPLTuCungUng           
            int namTN = 0;
            if (!string.IsNullOrEmpty(txtNamTiepNhan.Text))
                int.Parse(txtNamTiepNhan.Text);

            // Thực hiện tìm kiếm.            
            //this.BKCungUngCollection = new  BKCungUngDangKy().SelectCollectionDynamic(where, "");
            dgList.DataSource = NPLCungUng.GetNPLTuCungUng(GlobalSettings.MA_DON_VI, namTN, IDHopDong);
            SetCommandStatus();

            //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }
        private void SetCommandStatus()
        {

        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record && e.Row.DataRow != null)
            {
                //e.Row.Cells["MaHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                //if (e.Row.Cells["NgayTiepNhan"].Text != "")
                //{
                //    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                //    if (dt.Year <= 1900)
                //        e.Row.Cells["NgayTiepNhan"].Text = "";
                //}
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}