﻿using System.ComponentModel;
using System.Windows.Forms;

namespace eDeclaration
{
	public class LoginForm : Form
	{
		private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
		private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private Janus.Windows.EditControls.UIButton btnLogin;
		private Janus.Windows.EditControls.UICheckBox chkGhiNho;
		private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
		private Janus.Windows.GridEX.EditControls.EditBox txtMatKhau;
		private IContainer components = null;
		private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.ContainerValidator cvError;

		public bool IsLogin;
		public LoginForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkGhiNho = new Janus.Windows.EditControls.UICheckBox();
            this.txtMatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLogin = new Janus.Windows.EditControls.UIButton();
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(318, 120);
            this.uiGroupBox2.TabIndex = 22;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.chkGhiNho);
            this.uiGroupBox1.Controls.Add(this.txtMatKhau);
            this.uiGroupBox1.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.btnLogin);
            this.uiGroupBox1.Location = new System.Drawing.Point(8, 4);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(304, 104);
            this.uiGroupBox1.TabIndex = 23;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // chkGhiNho
            // 
            this.chkGhiNho.Location = new System.Drawing.Point(124, 68);
            this.chkGhiNho.Name = "chkGhiNho";
            this.chkGhiNho.Size = new System.Drawing.Size(64, 23);
            this.chkGhiNho.TabIndex = 20;
            this.chkGhiNho.Text = "Ghi nhớ";
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(124, 40);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.PasswordChar = '*';
            this.txtMatKhau.Size = new System.Drawing.Size(168, 21);
            this.txtMatKhau.TabIndex = 19;
            this.txtMatKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(124, 16);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(168, 21);
            this.txtMaDoanhNghiep.TabIndex = 18;
            this.txtMaDoanhNghiep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Mã doanh nghiệp";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Mật khẩu";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(200, 64);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(92, 24);
            this.btnLogin.TabIndex = 20;
            this.btnLogin.Text = "Đăng nhập";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaDoanhNghiep;
            this.rfvMa.ErrorMessage = "\"Mã doanh nghiệp\" bắt buộc phải nhập.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // LoginForm
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(318, 120);
            this.Controls.Add(this.uiGroupBox2);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập hệ thống";
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private void btnLogin_Click(object sender, System.EventArgs e)
		{
            //try
            //{
            //    cvError.Validate();
            //    if (!cvError.IsValid) return;
            //    this.Cursor = Cursors.WaitCursor;
            //    btnLogin.Text = "Đang xử lý ...";
            //    btnLogin.Enabled = false;
            //    ASEANService service = new ASEANService();
            //    service.Timeout = 3600000;				
            //    if (service.Login(txtMaDoanhNghiep.Text, txtMatKhau.Text))
            //    {
            //        this.IsLogin = true;
            //        this.Close();
            //    }
            //    else
            //    {
            //        MessageBox.Show("Đăng nhập không thành công.", "Thông báo",  MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        this.IsLogin = false;
            //    }
            //}
            //catch 
            //{
            //    MessageBox.Show("Có lỗi khi thực hiện. Vui lòng thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    this.IsLogin = false;
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //    btnLogin.Text = "Đăng nhập";
            //    btnLogin.Enabled = true;
            //}
		}
	}
}