using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT.SXXK;
using GemBox.Spreadsheet;
namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamReadExcelForm : BaseForm
    {
        public SanPhamCollection SPCollection = new SanPhamCollection();
        public SanPhamReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private int checkSPExit(string maSP)
        {
            for (int i = 0; i < this.SPCollection.Count; i++)
            {
                if (this.SPCollection[i].Ma == maSP) return i;
            }
            return -1;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if(!cvError.IsValid)return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                //error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                }
                else
                {
                    error.SetError(txtRow, "Begin row must be greater than zero ");
                }
                error.SetIconPadding(txtRow, -8);
                return;

            }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text,true);
            }
            catch
            {
                //ShowMessage("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", false);
                MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
               ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
            int maHangCol = ConvertCharToInt(maHangColumn);
            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
            int tenHangCol = ConvertCharToInt(tenHangColumn);
            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
            int maHSCol = ConvertCharToInt(maHSColumn);
            char dvtColumn = Convert.ToChar(txtDVTColumn.Text);
            int dvtCol = ConvertCharToInt(dvtColumn);
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        SanPham sp = new SanPham();
                        sp.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        sp.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                        sp.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                        //while (sp.MaHS.Trim().Length < 10)
                        //    sp.MaHS += "0";
                        if (sp.Ma.Trim() == "")
                        {
                            string rel = MLMessages("Mã của sản phẩm tại dòng : " + (wsr.Index + 1) + "rỗng nên được bỏ qua. Bạn có muốn tiếp tục không ?", "MSG_EXC06", "" + (wsr.Index + 1), true);
                            if (rel == "Yes")
                            {
                                continue;
                            }
                            else
                                break;
                        }
                        try
                        {
                            sp.DVT_ID = DonViTinh_GetID(Convert.ToString(wsr.Cells[dvtCol].Value).ToUpper());
                        }
                        catch
                        {
                            string rel = ShowMessage("Đơn vị tính : " + Convert.ToString(wsr.Cells[dvtCol].Value).ToUpper() + " của hàng thứ : " + (wsr.Index + 1) + " không có trong hệ thống nên sẽ được bỏ qua. Bạn có muốn tiếp tục không ?", true);
                            if (rel == "Yes")
                            {
                                continue;
                            }
                            else
                                break;
                        }
                        Company.BLL.SXXK.SanPham SPDaDuyet = new Company.BLL.SXXK.SanPham();
                        SPDaDuyet.Ma = sp.Ma;
                        SPDaDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        SPDaDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        if (SPDaDuyet.Load())
                        {
                            string rel = ShowMessage("Sản phẩm có mã  : " + SPDaDuyet.Ma + " đã có trong hệ thống nên sẽ được bỏ qua. Bạn có muốn tiếp tục không ?", true);
                            if (rel == "Yes")
                            {
                                continue;
                            }
                            else
                                break;
                        }
                        if (checkSPExit(sp.Ma)>=0)
                        {
                            //if (ShowMessage("Sản phẩm có mã \"" + sp.Ma + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế sản phẩm trên lưới bằng sản phẩm này?", true) == "Yes")
                            if (MLMessages("Sản phẩm có mã \"" + sp.Ma + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế sản phẩm trên lưới bằng sản phẩm này?", "MSG_EXC08", "", true) != "Yes")
                                this.SPCollection[checkSPExit(sp.Ma)] = sp;

                        }
                        else this.SPCollection.Add(sp);
                    }
                    catch
                    {
                      //  if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", "", true) != "Yes")
                        {
                            this.SPCollection.Clear();
                            return;
                        }
                    }
                }
            }
            this.Close();
        }

        private void SanPhamReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
            txtSheet.Focus();
        }
    }
}