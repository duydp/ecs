﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{
    // SiHV
    public partial class GiayPhep
    {
        /// <summary>
        /// ConfigGoodsItem
        /// </summary>
        /// <param name="hgpd">HangGiayPhepDetail</param>
        /// <returns>XElement</returns>
        public static XElement ConfigGoodsItem(XElement GoodsItem, HangGiayPhepDetail hgpd)
        {
            if (GoodsItem != null && GoodsItem.Elements().Count() >0 && hgpd != null)
            {
                foreach (XElement element in GoodsItem.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.sequence.ToUpper()))                  //Số thứ tự hàng
                        element.SetValue(hgpd.SoThuTuHang);         
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.statisticalValue.ToUpper()))     // Trị giá
                        element.SetValue(hgpd.TriGiaKB);    //??????????????????????????????????   
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.CurrencyExchange.ToUpper()))     // Nguyên tuệ
                    {
                        foreach (XElement child in element.Elements())
                        {
                            if (child.Name.ToString().ToUpper().Equals(NodeGiayPhep.CurrencyType.ToUpper()))
                                child.SetValue(hgpd.MaNguyenTe);
                        }
                    } 
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.Commodity.ToUpper()))            //Thông tin hàng hóa kèm theo giấy phép
                    {
                        foreach (XElement child in element.Elements())
                        {
                            if (child.Name.ToString().ToUpper().Equals(NodeGiayPhep.description.ToUpper()))         //Tên hàng hoá	1	an..256
                                child.SetValue(hgpd.TenHang);
                            else if (child.Name.ToString().ToUpper().Equals(NodeGiayPhep.identification.ToUpper())) //Mã tham chiếu hàng hoá	1	an..12
                                child.SetValue(hgpd.ID);
                            else if (child.Name.ToString().ToUpper().Equals(NodeGiayPhep.tariffClassification.ToUpper()))  //Mã HS 	0	an..12	Danh mục chuẩn     
                                child.SetValue(hgpd.MaHS);
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.GoodsMeasure.ToUpper()))         //Lượng cấp phép
                    {
                        foreach (XElement child in element.Elements())
                        {
                            if (child.Name.ToString().ToUpper().Equals(NodeGiayPhep.quantity.ToUpper()))            //Số lượng	1	n..14,3	
                                child.SetValue(hgpd.SoLuong);
                            else if (child.Name.ToString().ToUpper().Equals(NodeGiayPhep.measureUnit.ToUpper()))    //Đơn vị tính	1	an..3	Danh mục chuẩn
                                child.SetValue(hgpd.DonGiaKB);//???????????????????????????????????????????????
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.AdditionalInformation.ToUpper()))//Ghi chú khác
                    {
                        foreach (XElement child in element.Elements())
                        {
                            if (child.Name.ToString().ToUpper().Equals(NodeGiayPhep.content.ToUpper()))                
                                child.SetValue(hgpd.GhiChu);
                        }
                    }
                }           
            }
            return GoodsItem;
        }
        /// <summary>
        /// ConfigLicense (Thông tin giấy phép)
        /// </summary>
        /// <param name="gp">GiayPhep</param>
        /// <returns>XElement</returns>
        public static XElement ConfigLicense(XElement Root, GiayPhep gp) //Thông tin giấy phép
        {
            if (Root != null && Root.Elements().Count() > 0 && gp != null)
            {
                XElement GoodsItem = new XElement(NodeGiayPhep.GoodsItem);
                foreach (XElement element in Root.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.issuer.ToUpper()))                //Người cấp giấy phép
                        element.SetValue(gp.NguoiCap);
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.reference.ToUpper()))         //Số giấy phép
                        element.SetValue(gp.SoGiayPhep);
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.issue.ToUpper()))             //Ngày cấp giấy phép
                        element.SetValue(gp.NgayGiayPhep);
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.type.ToUpper()))              //Hình thức trừ lùi
                        element.SetValue("");   //?????????????????????????????????????????????????????????????????????
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.expire.ToUpper()))            //Ngày hết hạn giấy phép
                        element.SetValue(gp.NgayHetHan);
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.AdditionalInformation.ToUpper())) //Ghi chú khác
                    {
                        foreach (XElement child in element.Elements())
                        {
                            if (child.Name.ToString().ToUpper().Equals(NodeGiayPhep.content))
                                child.SetValue(gp.ThongTinKhac);
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.GoodsItem.ToUpper()))
                    {
                        GoodsItem = element;
                    }
                }
                if (GoodsItem != null)
                {
                    IList<HangGiayPhepDetail> lHGPD = HangGiayPhepDetail.SelectCollectionBy_GiayPhep_ID(gp.ID);
                    if (lHGPD != null)
                    {
                        if (lHGPD.Count > 1)
                        {
                            XElement TempGoodsItem = new XElement(GoodsItem);
                            ConfigGoodsItem(GoodsItem, lHGPD[0]);
                            for (int i = 1; i < lHGPD.Count; i++)
                            {
                                XElement NewGoodsItem = new XElement(TempGoodsItem);
                                Root.Add(ConfigGoodsItem(NewGoodsItem, lHGPD[i]));
                            }
                        }
                        else if(lHGPD.Count == 1)
                        {
                            ConfigGoodsItem(GoodsItem, lHGPD[0]);
                        }
                    }
                }
            }
            return Root;
        }
        /// <summary>
        /// ConfigGiayPhep
        /// </summary>
        /// <param name="TKMD_ID">long</param>
        /// <param name="function">int</param>
        /// <returns></returns>
        public static string ConfigGiayPhep(ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            try
            {
                IList<GiayPhep> lGP = GiayPhep.SelectCollectionBy_TKMD_ID(tkmd.ID);
                if (lGP != null && lGP.Count > 0)
                {
                    //doc = XElement.Load("../../../SOFTECH.ECS.V3.Components/XML/KD/GiayPhep.xml");
                    XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/KD/GiayPhep.xml");
                    if (tkmd != null && docXML != null)
                    {
                        XElement Root = docXML.Root;
                        if (Root != null && Root.Elements().Count() > 0)
                        {
                            XElement Licenses = Root.Element(NodeGiayPhep.Licenses);
                            XElement License = Licenses.Element(NodeGiayPhep.License);
                            // Check Upper
                            foreach (XElement element in Root.Elements())
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.Licenses.ToUpper()))
                                    Licenses = element;
                            }
                            foreach (XElement element in Licenses.Elements())
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeGiayPhep.License.ToUpper()))
                                    License = element;
                            }
                            Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                            // Add Licenses
                            if (lGP.Count == 1)
                            {
                                ConfigLicense(License, lGP[0]);
                            }
                            else if (lGP.Count > 1)
                            {
                                XElement TempLicense = new XElement(License);
                                ConfigLicense(License, lGP[0]);
                                // Add new license
                                for (int i = 1; i < lGP.Count; i++)
                                {
                                    XElement NewLicense = new XElement(TempLicense);
                                    Licenses.Add(ConfigLicense(NewLicense, lGP[i]));
                                }
                            }
                            //docXML.Save(@"D:\NewGiayPhep.xml");
                            //docXML.Save(@"../../../SOFTECH.ECS.V3.Components/XML/LoadKD/NewGiayPhep.xml");
                            return docXML.ToString();
                        }
                    }
                    return "";
                } return "";
            }
            catch (Exception ex)
            {
                string tt = ex.Message;
                return "";
            }
        }  
    }
}
