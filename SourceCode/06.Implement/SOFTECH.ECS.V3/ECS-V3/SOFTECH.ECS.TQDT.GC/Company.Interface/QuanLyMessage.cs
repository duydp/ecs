﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.SXXK;

namespace Company.Interface
{
    public partial class QuanLyMessage : BaseForm
    {
        public QuanLyMessage()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        MsgSend msg = (MsgSend)i.GetRow().DataRow;
                        try
                        {
                            msg.Delete();                       
                        }
                        catch { }
                    }
                }
                dgList.DataSource = new MsgSend().SelectCollectionAll();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }

            }
        }

        private void QuanLyMessage_Load(object sender, EventArgs e)
        {           
            dgList.DataSource = new MsgSend().SelectCollectionAll();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if(e.Row.RowType==RowType.Record)
            {
                string chucnang = e.Row.Cells["func"].Value.ToString();
                if (chucnang.Trim() == "1")
                    e.Row.Cells["func"].Text = "Khai báo";
                else if (chucnang.Trim() == "2")
                    e.Row.Cells["func"].Text = "Nhận dữ liệu";
                else
                    e.Row.Cells["func"].Text = "Hủy khai báo";
                string loaihs = e.Row.Cells["LoaiHS"].Value.ToString();
                if(loaihs.Trim().ToUpper()=="TK")
                    e.Row.Cells["LoaiHS"].Text = "Tờ khai";
                else if (loaihs.Trim().ToUpper() == "HD")
                    e.Row.Cells["LoaiHS"].Text = "Hợp đồng";
                else if (loaihs.Trim().ToUpper() == "PK")
                    e.Row.Cells["LoaiHS"].Text = "Phụ kiện";
                else if (loaihs.Trim().ToUpper() == "DMCU")
                    e.Row.Cells["LoaiHS"].Text = "Định mức cung ứng";
                else if (loaihs.Trim().ToUpper() == "TKCT")
                    e.Row.Cells["LoaiHS"].Text = "Định mức cung ứng";
                else
                    e.Row.Cells["LoaiHS"].Text = "Định mức";


            }
        }
    }
}

