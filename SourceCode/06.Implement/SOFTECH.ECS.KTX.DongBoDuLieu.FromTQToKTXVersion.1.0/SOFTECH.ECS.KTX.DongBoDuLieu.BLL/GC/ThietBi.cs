using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
namespace DongBoDuLieu.BLL.GC
{
    public class TB
    {
        public static DataSet GetDataSetThietBi(Company.GC.BLL.KDT.GC.HopDong HD,string matKhau)
        {
            
            StringBuilder queryTB = new StringBuilder();
            queryTB.Append("SELECT ");
            queryTB.Append("TB.So_HD			AS SoHopDong, ");
            queryTB.Append("TB.Ma_HQHD		AS MaHaiQuan, ");
            queryTB.Append("TB.DVGC			AS MaDoanhNghiep, ");
            queryTB.Append("TB.Ngay_Ky		AS NgayKy, ");
            queryTB.Append("TB.P_Code			AS Ma, ");
            queryTB.Append("TB.HS_Code		AS MaHS, ");
            queryTB.Append("TB.Ten_TB			AS Ten, ");
            queryTB.Append("TB.Ma_DVT			AS DVT_ID, ");
            queryTB.Append("TB.SL_DK			AS SoLuongDangKy, ");
            queryTB.Append("TB.Xuat_Xu		AS NuocXX_ID, ");
            queryTB.Append("TB.TinhTrang		AS TinhTrang, ");
            queryTB.Append("TB.DonGia			AS DonGia, ");
            queryTB.Append("TB.TriGia			AS TriGia, ");
            queryTB.Append("TB.NgTe			AS NguyenTe_ID ");
            queryTB.Append("FROM DTHIETBI TB ");
            queryTB.Append("WHERE TB.Ma_HQHD = '" + HD.MaHaiQuan + "' AND DVGC = '" + HD.MaDoanhNghiep + "' ");
            queryTB.Append(" AND So_HD='" + HD.SoHopDong + "'");
            DataSet dsTB = WebServiceDB.getDataBySQL(queryTB.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsTB;

            //TODO: ThietBi
            /*
            StringBuilder queryTB = new StringBuilder();
            queryTB.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryTB.Append("SELECT ");
            queryTB.Append("TB.So_HD			AS SoHopDong, ");
            queryTB.Append("TB.Ma_HQHD		AS MaHaiQuan, ");
            queryTB.Append("TB.DVGC			AS MaDoanhNghiep, ");
            queryTB.Append("TB.Ngay_Ky		AS NgayKy, ");
            queryTB.Append("TB.P_Code			AS Ma, ");
            queryTB.Append("TB.HS_Code		AS MaHS, ");
            queryTB.Append("TB.Ten_TB			AS Ten, ");
            queryTB.Append("TB.Ma_DVT			AS DVT_ID, ");
            queryTB.Append("TB.SL_DK			AS SoLuongDangKy, ");
            queryTB.Append("TB.Xuat_Xu		AS NuocXX_ID, ");
            queryTB.Append("TB.TinhTrang		AS TinhTrang, ");
            queryTB.Append("TB.DonGia			AS DonGia, ");
            queryTB.Append("TB.TriGia			AS TriGia, ");
            queryTB.Append("TB.NgTe			AS NguyenTe_ID ");
            queryTB.Append("FROM DTHIETBI TB ");
            queryTB.Append("WHERE TB.Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            queryTB.Append(" AND So_HD=''" + HD.SoHopDong + "''");
            queryTB.Append(" ') AS a;");
            DataSet dsTB = WebServiceDB.getDataBySQL(queryTB.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsTB;
            */
        }

        public static DataSet GetTBAtTMP(long Id_HopDong)
        {
            string sql = "select * from t_GC_ThietBi where HopDong_ID=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }

        public static DataSet GetTBAtDN(long Id_HopDong)
        {
            string sql = "select * from t_GC_ThietBi where HopDong_ID=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }
    }
}
