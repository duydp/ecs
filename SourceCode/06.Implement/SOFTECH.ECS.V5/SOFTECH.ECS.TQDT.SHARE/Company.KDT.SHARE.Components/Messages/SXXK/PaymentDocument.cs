﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class PaymentDocument : DeclarationBase
    {
        /// <summary>
        /// Trị giá thanh toán trên chứng từ thanh toán;bắt buộc;n..16,2
        /// </summary>
        [XmlElement("customsValue")]
        public string CustomsValue { get; set; }
        [XmlElement("Payment")]
        public Payment Payment { get; set; }
    }
}
