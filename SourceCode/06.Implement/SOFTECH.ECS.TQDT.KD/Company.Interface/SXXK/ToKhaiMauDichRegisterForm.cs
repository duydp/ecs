﻿

using System;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.KD.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean=Janus.Windows.UI.InheritableBoolean;


namespace Company.Interface.SXXK
{
	public partial class ToKhaiMauDichRegisterForm : BaseForm
	{
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        private ToKhaiMauDich tkMD = new ToKhaiMauDich();
       
        public string NhomLoaiHinh = "";
        public ToKhaiMauDichRegisterForm()
		{
			InitializeComponent();
		}

		//-----------------------------------------------------------------------------------------

   
		private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
		{
            txtNamTiepNhan.Text = DateTime.Now.Year.ToString();
            txtThangDK.Text = DateTime.Now.Month.ToString();
            cbbLoaiToKhai.SelectedIndex = 0;
            uiButton2.Enabled = false;
            try
			{
                txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
                this.search();
                dgList.RootTable.Columns["NGAY_THN_THX"].DefaultValue = DateTime.Today;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
            //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
            //{
            //    uiButton2.Visible = false;
            //}    
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {
            //short namdk = 0;
            //if (txtNamTiepNhan.Text.Trim().Length > 0)
            //    namdk = Convert.ToInt16(txtNamTiepNhan.Text.Trim());
            //try
            //{
                
               
            //    WSForm wsForm = new WSForm();
            //    wsForm.ShowDialog(this);
            //    if (!wsForm.IsReady) return;
            //    this.Cursor = Cursors.WaitCursor;
            //    string strSTN = "";
            //    long count=ToKhaiMauDich.DongBoDuLieuHaiQuanAll(donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI, namdk);
            //    if (count > 0)
            //    {
            //        this.Cursor = Cursors.Default;
            //        ShowMessage("Nhận dữ liệu thành công " + count + " tờ khai", false);
            //        this.search();
            //    }
            //    else
            //    {
            //        this.Cursor = Cursors.Default;
            //        ShowMessage("Không có tờ khai nào cả.", false);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    string st=ShowMessage(ex.Message+"\n Có lỗi khi thực hiện. Bạn có muốn đưa vào hàng đợi không", true);
            //    if (st == "Yes")
            //    {
            //        Company.KD.BLL.KDT.HangDoi hd = new Company.KD.BLL.KDT.HangDoi();                   
            //        hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
            //        hd.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
            //        hd.ChucNang = ChucNang.DONG_BO_DU_LIEU;
            //        hd.SoTK = 0;
            //        hd.NamDangKy = namdk;
            //        hd.MaHaiQuan = donViHaiQuanControl1.Ma;                 
            //        MainForm.AddToQueueForm(hd);
            //        MainForm.ShowQueueForm();
            //    }
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
        }
        private ToKhaiMauDich getTKMDByPrimaryKey(int sotk,string mahaiquan,string maloaihinh,int namdk)
        {
            foreach (ToKhaiMauDich tk in this.tkmdCollection)
            {
                if (tk.SoToKhai == sotk && tk.MaHaiQuan.Trim()==mahaiquan && tk.NamDangKy==namdk && tk.MaLoaiHinh==maloaihinh) 
                    return tk;
            }
            return null;
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
           
            if (e.Row.RowType == RowType.Record)
            {
                string sotk1=e.Row.Cells["SoToKhai"].Text;
                Int16  soKT = Convert.ToInt16 (e.Row.Cells["TrangThai"].Text);
                Form[] forms = this.ParentForm.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals(sotk1))
                    {
                        forms[i].Activate();
                        return;
                    }
                }              
                int sotk = Convert.ToInt32(sotk1.Substring(0,sotk1.IndexOf("/")));
                int namdk = Convert.ToDateTime(e.Row.Cells[3].Text).Year;
                string mahq = donViHaiQuanControl1.Ma;
                string maloaihinh = e.Row.Cells[2].Value.ToString();

                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                f.TKMD = this.getTKMDByPrimaryKey(sotk, mahq, maloaihinh, namdk);
                f.NhomLoaiHinh = maloaihinh.Substring(0, 3);
                f.MdiParent = this.ParentForm;
                f.Name = sotk1;
                f.Show();   
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "1 = 1";
            where += string.Format(" AND MaHaiQuan = '{0}' and MaDoanhNghiep='{1}'", donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI);
            where += " AND MaLoaiHinh LIKE '" + cbbLoaiToKhai.SelectedValue.ToString() + "%'";
            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoToKhai = " + txtSoTiepNhan.Value;
            }
                      
            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND NamDangKy = " + txtNamTiepNhan.Value;
            }
            if (Convert.ToInt16(txtThangDK.Value) > 0)
            {
                where += " AND month(NgayDangKy)=" + Convert.ToInt16(txtThangDK.Value);
            }
            if (GlobalSettings.MA_DON_VI != "0400101556")
            {
                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                        where += " AND TenChuHang LIKE '%" + txtTenChuHang.Text + "%'";
                }
                else
                {
                    where += " AND TenChuHang LIKE ''";
                }
            }
            else
            {
                label5.Text = "Tên khách hàng";
                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    where += " AND TenDonViDoiTac LIKE '%" + txtTenChuHang.Text + "%'";
                }
            }
            if (this.NhomLoaiHinh != "") where += " AND MaLoaiHinh LIKE '%" + this.NhomLoaiHinh + "%'";
            // Thực hiện tìm kiếm.            
            this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.tkmdCollection;            
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string loaiHinh = (e.Row.Cells["MaLoaiHinh"].Value.ToString());
                DateTime time = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + donViHaiQuanControl1.Ma + "/" + time.Year.ToString();
                if (loaiHinh.StartsWith("NSX"))
                {
                    switch (e.Row.Cells["ThanhLy"].Value.ToString().Trim())
                    {
                        case "":
                            e.Row.Cells["ThanhLy"].Text =setText( "Chưa thanh khoản","Not Liquidated yet");
                            break;
                        case "H":
                            e.Row.Cells["ThanhLy"].Text = setText("Đã thanh khoản hết", "Liquidated completely");
                            break;
                        case "L":
                            e.Row.Cells["ThanhLy"].Text = setText("Thanh khoản một phần", "Liquidated partly");
                            break;
                    }
                }
                else
                    e.Row.Cells["ThanhLy"].Text = "";
                //string phanluong = e.Row.Cells["PhanLuong"].Text;
                //if (phanluong == "1" || phanluong == "3" || phanluong == "5")
                //    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng xanh";
                //else if (phanluong == "2" || phanluong == "4" || phanluong == "6" || phanluong == "10")
                //{
                //    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng đỏ";
                //}
                //else if (phanluong == "8" || phanluong == "12")
                //    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng vàng";

                
                DateTime ngayTNX = Convert.ToDateTime(e.Row.Cells["NGAY_THN_THX"].Value.ToString());
                DateTime ngayDK = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value.ToString());
                DateTime today = DateTime.Today;
              
                if(loaiHinh.StartsWith("NSX"))
                {
                    string thanhly = e.Row.Cells["ThanhLy"].Value.ToString().Trim();
                    if (thanhly == "H")
                    {
                        e.Row.Cells["ChuY"].Text = setText("Tờ khai này đã thanh khoản hết", "This declaration have been completely Liquidated ");
                        return;
                    }
                    TimeSpan time1 = today.Subtract(ngayDK);
                    int ngay = 275 - time1.Days;
                    if (ngay > 0)
                        e.Row.Cells["ChuY"].Text = setText("Còn " + ngay.ToString() + " ngày phải thanh khoản", ngay.ToString() + "days to expired iquidation ");
                    else if (ngay == 0)
                        e.Row.Cells["ChuY"].Text = setText("Hết hạn thanh khoản trong ngày hôm nay", "Deadline of Liquidation is Today");                        
                    else
                        e.Row.Cells["ChuY"].Text = setText("Đã hết hạn thanh khoản","Liquidation had expired");

                }
                if (loaiHinh.StartsWith("XSX"))
                {

                    TimeSpan time1 = today.Subtract(ngayDK);
                    int ngay = 45 - time1.Days;
                    if (ngay > 0)
                        e.Row.Cells["ChuY"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                    else if (ngay == 0)
                        e.Row.Cells["ChuY"].Text = setText("Hết hạn thanh khoản trong ngày hôm nay", "Deadline of Liquidation is Today");
                    else
                        e.Row.Cells["ChuY"].Text = setText("Đã hết hạn thanh khoản", "Liquidation had expired");

                }
                if (ngayTNX.Year <= 1900)
                {                    
                    e.Row.Cells["NGAY_THN_THX"].Text = "";
                }
            }
        }

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            
        }
       
                
       
        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_DropDown(object sender, ColumnActionEventArgs e)
        {
            
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            uiButton2.Enabled = false;
            new ToKhaiMauDich().InsertUpdate(this.tkmdCollection);
            uiButton2.Enabled = true;
            this.Cursor = Cursors.Default;
            MLMessages("Cập nhật thành công","MSG_SAV02","", false);
            uiButton2.Enabled = false;
        }

        private void dgList_EditingCell(object sender, EditingCellEventArgs e)
        {
            //GridEXRow row = dgList.GetRow();
            //if (row.Cells["TrangThaiThanhKhoan"].Value.ToString() == "S")
            //{
            //    e.Cancel = true;
            //}
        }

        private void dgList_CellUpdated(object sender, ColumnActionEventArgs e)
        {
            //GridEXRow row = dgList.GetRow();
            //if (Convert.ToDateTime(row.Cells["NGAY_THN_THX"].Value).Year > 1900)
            //{
            //    dgList.GetRow().Cells["TrangThaiThanhKhoan"].Value = "K";
            //}
            uiButton2.Enabled = true;
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count > 0)
            {
                if(MLMessages("Bạn có muốn tính thuế sau kiểm hoá các tờ khai này không?","MSG_WRN38","",true) == "Yes")
                    foreach (GridEXSelectedItem item in items)
                    {
                        if (item.RowType == RowType.Record)
                        {

                            ToKhaiMauDich tkmd = (ToKhaiMauDich)item.GetRow().DataRow;
                            if (tkmd.TrangThaiThanhKhoan == "D") ShowMessage("Tờ khai số '" + item.GetRow().Cells["SoToKhai"].Text + "' chưa được kiểm hoá.", false);
                            else 
                                tkmd.TrangThaiThanhKhoan = "S";
                        }

                    }
                new ToKhaiMauDich().InsertUpdate(this.tkmdCollection);
                dgList.DataSource = this.tkmdCollection;
                dgList.Refetch();
            }
            
        }

        private void uiGroupBox2_Click(object sender, EventArgs e)
        {

        }
	
	}
}