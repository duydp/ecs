using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;

namespace Company.BLL.KDT.SXXK
{
    public partial class NguyenPhuLieu
    {
        public int DeleteTransaction(SqlTransaction transaction, string MaDN, string MaHQ)
        {
            string spName = "[dbo].p_KDT_SXXK_NguyenPhuLieu_Delete";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
            nplSXXK.Ma = this.Ma;
            nplSXXK.MaDoanhNghiep = MaDN;
            nplSXXK.MaHaiQuan = MaHQ;


            if (transaction != null)
            {
                nplSXXK.DeleteTransaction(transaction);
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                nplSXXK.Delete();
                return db.ExecuteNonQuery(dbCommand);
            }

        }
        public static int Delete(long iD, string MaDN, string MaHQ)
        {
            NguyenPhuLieu entity = NguyenPhuLieu.Load(iD);
            BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
            nplSXXK.Ma = entity.Ma;
            nplSXXK.MaDoanhNghiep = MaDN;
            nplSXXK.MaHaiQuan = MaHQ;
            nplSXXK.Delete();
            return entity.Delete(MaDN, MaHQ);
        }

        public int Delete(string MaDN, string MaHQ)
        {
            return this.DeleteTransaction(null, MaDN, MaHQ);
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(NguyenPhuLieuCollection collection, string MaDN, string MaHQ)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NguyenPhuLieu item in collection)
                    {
                        if (item.DeleteTransaction(transaction, MaDN, MaHQ) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                        BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                        nplSXXK.Ma = item.Ma;
                        nplSXXK.MaDoanhNghiep = MaDN;
                        nplSXXK.MaHaiQuan = MaHQ;
                        nplSXXK.DeleteTransaction(transaction);
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------

        public static void DeleteCollection(NguyenPhuLieuCollection collection, SqlTransaction transaction, string MaDN, string MaHQ)
        {
            foreach (NguyenPhuLieu item in collection)
            {
                item.DeleteTransaction(transaction, MaDN, MaHQ);
            }
        }
        public static NguyenPhuLieuCollection SelectCollectionBy_Master_ID(long master_ID, SqlTransaction transaction)
        {
            string spName = "[dbo].p_KDT_SXXK_NguyenPhuLieu_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);

            IDataReader reader = db.ExecuteReader(dbCommand, transaction);
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;

        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "[dbo].p_KDT_SXXK_NguyenPhuLieu_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }
        public bool CloneToDB(SqlTransaction transaction)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand("p_KDT_SXXK_NguyenPhuLieu_Copy");
                db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
                if (transaction != null)
                    db.ExecuteNonQuery(dbCommand, transaction);
                else
                    db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch
            {
                return false;
            }
        }
        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "[dbo].p_KDT_SXXK_NguyenPhuLieu_Update";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransactionBy(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_SXXK_NguyenPhuLieu_InsertUpdateBy";

            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public NguyenPhuLieuCollection LoadNPLCollection(string MaHaiQuan, string MaDoanhNghiep, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay)
        {
            string sql = "	SELECT  NPL.ID, NPL.Master_ID,  " +
                            "			NPL.Ma AS Ma,  " +
                            "			NPL.Ten AS Ten,  " +
                            "			NPL.MaHS AS MaHS,  " +
                            "			NPL.DVT_ID AS DVT_ID,  " +
                            "			NPL.STTHang AS STTHang  " +
                            "	FROM    dbo.t_KDT_SXXK_NguyenPhuLieu NPL " +
                            "			INNER JOIN dbo.t_KDT_SXXK_NguyenPhuLieuDangKy NPLDK ON NPL.Master_ID = NPLDK.ID " +
                            "	WHERE   NPLDK.MaHaiQuan = '" + MaHaiQuan + "' " +
                            "			AND NPLDK.MaDoanhNghiep = '" + MaDoanhNghiep + "' ";
            if (trangThaiDaDuyet)
                sql += "			AND npldk.TrangThaiXuLy = 1";
            if (tuNgay > DateTime.MinValue)
                sql += " and npldk.NgayTiepNhan >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND npldk.NgayTiepNhan <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            sql += " ORDER BY npldk.NgayTiepNhan";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(sql);

            IDataReader reader = db.ExecuteReader(cmd);
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
       
        public static void InsertDuLieuTQDT(string MaHaiQuan, string MaDoanhNghiep, DataSet ds)
        {
            try
            {
                Company.BLL.KDT.SXXK.NguyenPhuLieuDangKyCollection kdtNPLDKCollection = new Company.BLL.KDT.SXXK.NguyenPhuLieuDangKyCollection();

                //Create collection NguyenPhuLieuDangKy
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy nplDK = Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.Load(long.Parse(row["Master_ID"].ToString()));

                    if (nplDK != null)
                    {
                        //Load NPL cua NPL DK.
                        nplDK.LoadNPLCollection();

                        kdtNPLDKCollection.Add(nplDK);
                    }
                }

                //Insert data NguyenPhuLieuDangKy
                foreach (Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy item in kdtNPLDKCollection)
                {
                    item.InsertUpdateFull();
                }

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        public static List<MaterialsNorm> GetNPLFromDinhMuc(long IDDinhMuc , string MaSanPham, string MaDoanhNghiep, string MaHaiQuan)
        {
//            string sql = string.Format(@"SELECT     t_SXXK_NguyenPhuLieu.MaHS, 
//                                                    t_SXXK_NguyenPhuLieu.DVT_ID, 
//                                                    t_SXXK_NguyenPhuLieu.Ma, 
//                                                    t_SXXK_NguyenPhuLieu.Ten, 
//                                                    t_KDT_SXXK_DinhMuc.DinhMucSuDung, 
//                                                    t_KDT_SXXK_DinhMuc.TyLeHaoHut
//                                            FROM    t_SXXK_NguyenPhuLieu INNER JOIN
//                                                    t_KDT_SXXK_DinhMuc ON t_SXXK_NguyenPhuLieu.Ma = t_KDT_SXXK_DinhMuc.MaNguyenPhuLieu INNER JOIN
//                                                    t_KDT_SXXK_DinhMucDangKy ON t_KDT_SXXK_DinhMuc.Master_ID = t_KDT_SXXK_DinhMucDangKy.ID
//                                           WHERE    (t_KDT_SXXK_DinhMucDangKy.ID = '{0}') AND (t_KDT_SXXK_DinhMuc.MaSanPham = '{1}')", IDDinhMuc, MaSanPham);

//            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
//            DbCommand cmd = db.GetSqlStringCommand(sql);
//            IDataReader reader = db.ExecuteReader(cmd);

            string spName = "[dbo].[p_GC_NguyenPhuLieu_GetNPLDinhMuc]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDDinhMuc", SqlDbType.BigInt, IDDinhMuc);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<MaterialsNorm> NplCollection = new List<MaterialsNorm>();
            decimal _dinhMucSD = 0;
            decimal _tyLeHH = 0;
            while (reader.Read())
            {
                MaterialsNorm entity = new MaterialsNorm() { Material = new Product(){Commodity = new Commodity(),GoodsMeasure = new GoodsMeasure()} };
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.Material.Commodity.TariffClassification = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.Material.GoodsMeasure.MeasureUnit = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Material.Commodity.Identification = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Material.Commodity.Description = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) _dinhMucSD = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) _tyLeHH = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                entity.Norm = Helpers.FormatNumeric(_dinhMucSD, 6);
                entity.Loss = Helpers.FormatNumeric(_tyLeHH, 4);
                NplCollection.Add(entity);
            }
            reader.Close();
            return NplCollection;
        }
        public static List<MaterialsNorm> GetNPLFromDinhMucSUA(long IDDinhMuc, string MaSanPham)
        {
            string sql = string.Format(@"SELECT     t_SXXK_NguyenPhuLieu.MaHS, 
                                                    t_SXXK_NguyenPhuLieu.DVT_ID, 
                                                    t_SXXK_NguyenPhuLieu.Ma, 
                                                    t_SXXK_NguyenPhuLieu.Ten, 
                                                    t_KDT_SXXK_DinhMucSUA.DinhMucSuDung, 
                                                    t_KDT_SXXK_DinhMucSUA.TyLeHaoHut
                                            FROM    t_SXXK_NguyenPhuLieu INNER JOIN
                                                    t_KDT_SXXK_DinhMucSUA ON t_SXXK_NguyenPhuLieu.Ma = t_KDT_SXXK_DinhMucSUA.MaNguyenPhuLieu INNER JOIN
                                                    t_KDT_SXXK_DinhMucDangKySUA ON t_KDT_SXXK_DinhMucSUA.Master_IDSUA = t_KDT_SXXK_DinhMucDangKySUA.ID
                                           WHERE    (t_KDT_SXXK_DinhMucDangKySUA.ID = '{0}') AND (t_KDT_SXXK_DinhMucSUA.MaSanPham = '{1}')", IDDinhMuc, MaSanPham);

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(sql);
            IDataReader reader = db.ExecuteReader(cmd);
            List<MaterialsNorm> NplCollection = new List<MaterialsNorm>();
            decimal _dinhMucSD = 0;
            decimal _tyLeHH = 0;
            while (reader.Read())
            {
                MaterialsNorm entity = new MaterialsNorm() { Material = new Product() { Commodity = new Commodity(), GoodsMeasure = new GoodsMeasure() } };
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.Material.Commodity.TariffClassification = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.Material.GoodsMeasure.MeasureUnit = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Material.Commodity.Identification = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Material.Commodity.Description = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) _dinhMucSD = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) _tyLeHH = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                entity.Norm = Helpers.FormatNumeric(_dinhMucSD, 6);
                entity.Loss = Helpers.FormatNumeric(_tyLeHH, 4);
                NplCollection.Add(entity);
            }
            reader.Close();
            return NplCollection;
        }


        internal static List<MaterialsNorm> GetNPLFromDinhMucSUA(long p, string p_2, string p_3, string p_4)
        {
            throw new NotImplementedException();
        }
    }
}