

/****** Object:  Table [dbo].[t_VNACCS_CapSoToKhai]    Script Date: 05/09/2014 08:23:56 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACCS_CapSoToKhai]') AND type in (N'U'))
BEGIN 


/****** Object:  Table [dbo].[t_VNACCS_CapSoToKhai]    Script Date: 05/09/2014 08:23:56 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[t_VNACCS_CapSoToKhai](
	[SoTK] [int] NOT NULL,
	[MaLoaiHinh] [varchar](50) NOT NULL,
	[NamDangKy] [int] NOT NULL,
	[SoTKVNACCS] [decimal](15, 0) NOT NULL,
	[SoTKVNACCSFull] [decimal](12, 0) NULL,
	[SoTKDauTien] [decimal](15, 0) NULL,
	[SoNhanhTK] [int] NULL,
	[TongSoTKChiaNho] [int] NULL,
	[SoTKTNTX] [decimal](15, 0) NULL,
	[Temp1] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_VNACCS_CapSoToKhai] PRIMARY KEY CLUSTERED 
(
	[SoTK] ASC,
	[MaLoaiHinh] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_t_VNACCS_CapSoToKhai] UNIQUE NONCLUSTERED 
(
	[SoTKVNACCS] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF
END 


GO 
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Update]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Load]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Insert]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Insert]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(15, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255)
AS
INSERT INTO [dbo].[t_VNACCS_CapSoToKhai]
(
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
)
VALUES
(
	@SoTK,
	@MaLoaiHinh,
	@NamDangKy,
	@SoTKVNACCS,
	@SoTKVNACCSFull,
	@SoTKDauTien,
	@SoNhanhTK,
	@TongSoTKChiaNho,
	@SoTKTNTX,
	@Temp1
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Update]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Update]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(15, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255)
AS

UPDATE
	[dbo].[t_VNACCS_CapSoToKhai]
SET
	[NamDangKy] = @NamDangKy,
	[SoTKVNACCS] = @SoTKVNACCS,
	[SoTKVNACCSFull] = @SoTKVNACCSFull,
	[SoTKDauTien] = @SoTKDauTien,
	[SoNhanhTK] = @SoNhanhTK,
	[TongSoTKChiaNho] = @TongSoTKChiaNho,
	[SoTKTNTX] = @SoTKTNTX,
	[Temp1] = @Temp1
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(15, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255)
AS
IF EXISTS(SELECT [SoTK], [MaLoaiHinh] FROM [dbo].[t_VNACCS_CapSoToKhai] WHERE [SoTK] = @SoTK AND [MaLoaiHinh] = @MaLoaiHinh)
	BEGIN
		UPDATE
			[dbo].[t_VNACCS_CapSoToKhai] 
		SET
			[NamDangKy] = @NamDangKy,
			[SoTKVNACCS] = @SoTKVNACCS,
			[SoTKVNACCSFull] = @SoTKVNACCSFull,
			[SoTKDauTien] = @SoTKDauTien,
			[SoNhanhTK] = @SoNhanhTK,
			[TongSoTKChiaNho] = @TongSoTKChiaNho,
			[SoTKTNTX] = @SoTKTNTX,
			[Temp1] = @Temp1
		WHERE
			[SoTK] = @SoTK
			AND [MaLoaiHinh] = @MaLoaiHinh
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACCS_CapSoToKhai]
	(
			[SoTK],
			[MaLoaiHinh],
			[NamDangKy],
			[SoTKVNACCS],
			[SoTKVNACCSFull],
			[SoTKDauTien],
			[SoNhanhTK],
			[TongSoTKChiaNho],
			[SoTKTNTX],
			[Temp1]
	)
	VALUES
	(
			@SoTK,
			@MaLoaiHinh,
			@NamDangKy,
			@SoTKVNACCS,
			@SoTKVNACCSFull,
			@SoTKDauTien,
			@SoNhanhTK,
			@TongSoTKChiaNho,
			@SoTKTNTX,
			@Temp1
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Delete]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Delete]
	@SoTK int,
	@MaLoaiHinh varchar(50)
AS

DELETE FROM 
	[dbo].[t_VNACCS_CapSoToKhai]
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACCS_CapSoToKhai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Load]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Load]
	@SoTK int,
	@MaLoaiHinh varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
FROM
	[dbo].[t_VNACCS_CapSoToKhai]
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
FROM [dbo].[t_VNACCS_CapSoToKhai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
FROM
	[dbo].[t_VNACCS_CapSoToKhai]	

GO


--------------------------------------------------



DELETE FROM t_HaiQuan_LoaiHinhMauDich WHERE id LIKE '%V%'

Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVB11',N'Xuất khẩu thương mại','B11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVB12',N'Xuất khẩu hàng hoá tạm xuất khẩu','B12',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVB13',N'Xuất khẩu hàng hoá nhập khẩu (reshipment)','B13',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE42',N'Xuất khẩu các sản phẩm từ nhà máy CX','E42',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE44',N'Xuất khẩu các sản phẩm từ CX nhà máy được ủy quyền','E44',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE46',N'Mang hàng hóa từ nhà máy CX vào nội địa','E46',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE52',N'Xuất khẩu các sản phẩm từ nhà máy GC','E52',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE54',N'Xuất khẩu các sản phẩm từ GC nhà máy được ủy quyền','E54',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE56',N'Mang hàng hóa từ nhà máy GC vào nội địa','E56',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE62',N'Xuất khẩu các sản phẩm từ nhà máy sản xuất SXXK','E62',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE64',N'Xuất khẩu SP từ nhà máy SXXK có thẩm quyền','E64',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE82',N'Xuất khẩu nguyên liệu để chế biến nước ngoài','E82',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG21',N'Tái xuất hàng hoá thương mại ','G21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG22',N'Tái xuất máy móc, TB cho  các dự án giới hạn','G22',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG23',N'Tái xuất khẩu hàng hóa không chịu thuế tạm nhập','G23',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG24',N'Khác tái xuất','G24',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG61',N'Xuất khẩu tạm thời hàng hóa','G61',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVC12',N'Mang hàng ra khỏi kho ngoại quan để xuất khẩu','C12',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVC22',N'Mang hàng ra khỏi khu miễn thuế khác để xuất khẩu','C22',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVAEO',N'Xuất khẩu AEO','AEO',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVH21',N'Xuất khẩu khác','H21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA11',N'Nhập kinh doanh tiêu dùng','A11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA12',N'Nhập kinh doanh sản xuất','A12',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA21',N'Chuyển tiêu thụ nội địa từ nguồn tạm nhập','A21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA31',N'Nhập hàng XK bị trả lại','A31',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA41',N'Nhập kinh doanh của doanh nghiệp đầu tư','A41',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA42',N'Chuyển tiêu thụ nội địa khác','A42',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA43',N'Không dùng/dự phòng','A43',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA44',N'Nhập vào khu phi thuế quan từ nội địa','A44',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE11',N'Nhập nguyên liệu của doanh nghiệp chế xuất','E11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE13',N'Nhập tạo tài sản cố định của doanh nghiệp chế xuất','E13',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE15',N'Nhập NL của DNCX từ nội địa','E15',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE21',N'Nhập NL để GC cho  nước ngoài','E21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE23',N'Nhập NL GC từ hợp đồng khác chuyển sang','E23',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE25',N'Không dùng/dự phòng','E25',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE31',N'Nhập nguyên liệu sản xuất xuất khẩu','E31',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE33',N'Không dùng/dự phòng','E33',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE41',N'Nhập sản phẩm thuê gia công ở nước ngoài','E41',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG11',N'Tạm nhập hàng kinh doanh tạm nhập tái xuất','G11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG12',N'Tạm nhập máy móc, thiết bị cho dự án có thời hạn','G12',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG13',N'Tạm nhập miễn thuế','G13',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG14',N'Tạm nhập khác','G14',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG51',N'Tái nhập hàng đã tạm xuất','G51',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVC11',N'Hàng gửi kho ngoại quan','C11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVC21',N'Hàng đưa vào khu phi thuế quan','C21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVAEO',N'Loại hình dành cho doanh nghiệp ưu tiên','AEO',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVH11',N'Các loại nhập khẩu khác','H11',GetDate(),GetDate())


GO 

  UPDATE t_KDT_VNACC_ToKhaiMauDich
  SET LoaiHang = (SELECT TOP 1 
							CASE WHEN  hmd.MaQuanLy LIKE '1%' THEN 'N'
							 WHEN hmd.MaQuanLy  LIKE '2%' THEN 'S'
							 WHEN hmd.MaQuanLy LIKE '3%' THEN 'T'
							 WHEN hmd.MaQuanLy LIKE '4%' THEN 'H' END AS LoaiHangT FROM t_kdt_VNACC_HangMauDich hmd WHERE hmd.TKMD_ID = tkmd.ID )
 From  t_KDT_VNACC_ToKhaiMauDich tkmd  
							
 GO
 
 
/****** Object:  StoredProcedure [dbo].[p_NPLCungUngDaDangKy]    Script Date: 05/11/2014 16:26:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_NPLCungUngDaDangKy]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_NPLCungUngDaDangKy]
GO


/****** Object:  StoredProcedure [dbo].[p_NPLCungUngDaDangKy]    Script Date: 05/11/2014 16:26:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author,,Huynh Ngoc Khanh>  
-- Create date: <Create Date,,01/09/2012>  
-- Description: <Description,,Theo doi NPL cung ung>  
-- =============================================  
CREATE PROCEDURE [dbo].[p_NPLCungUngDaDangKy]  
@HopDongID int
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 -- p_NPLTuCungUng '0400101556',23,676  
 SET NOCOUNT ON;   
  
--Declare @MaDoanhNghiep varchar(50);  
--set @MaDoanhNghiep='0100101308'  
SELECT Ma,MAX(TenNPL)AS TenNPL,SUM(LuongCung)LuongCung,MAX(DonViTinh) AS DonViTinh,STK,MAX(TKMD_ID) AS TKMD_ID ,MAX(TKCT_ID)AS TKCT_ID
FROM
(SELECT     t_KDT_GC_NguyenPhuLieu.Ma, t_KDT_GC_NguyenPhuLieu.Ten AS TenNPL, t_KDT_GC_NguyenPhuLieu.MaHS, 
                       t_View_KDT_NPLCungUng.LuongCung,
                          (SELECT     Ten
                            FROM          t_HaiQuan_DonViTinh
                            WHERE      (ID = t_KDT_GC_NguyenPhuLieu.DVT_ID)) AS DonViTinh, 
                           CASE WHEN t_View_KDT_NPLCungUng.TKMD_ID > 0 THEN
									(SELECT   
										CASE	WHEN  t_KDT_ToKhaiMauDich.SoToKhai > 0 THEN
												CONVERT(VARCHAR(12), CASE WHEN MaLoaiHinh LIKE '%V%' THEN LoaiVanDon ELSE SoToKhai END) + '/' + MaLoaiHinh + '/' + CONVERT(VARCHAR(10), NamDK)
												ELSE
												N'Tờ khai chưa được cấp số (ID = '+CONVERT(VARCHAR(6),t_KDT_ToKhaiMauDich.ID)+')'
												END
									FROM    dbo.t_KDT_ToKhaiMauDich
									WHERE      ID = t_View_KDT_NPLCungUng.TKMD_ID) 
							     WHEN	t_View_KDT_NPLCungUng.TKMD_ID = 0 AND t_View_KDT_NPLCungUng.TKCT_ID > 0 THEN
									(SELECT     
										CASE	WHEN tkct.SoToKhai > 0 THEN
												CONVERT(VARCHAR(12), CASE WHEN MaLoaiHinh LIKE '%V%' THEN tkct.Huongdan_PL ELSE tkct.SoToKhai END ) + '/' + tkct.MaLoaiHinh + '/' + CONVERT(VARCHAR(4), tkct.NamDK)
												ELSE
												N'Tờ khai chuyển tiếp xuất chưa được cấp số (ID = '+CONVERT(VARCHAR(6),tkct.ID)+')'
												END
									FROM          dbo.t_KDT_GC_ToKhaiChuyenTiep tkct
									WHERE      tkct.ID = t_View_KDT_NPLCungUng.TKCT_ID)
                            END AS STK,
                             t_View_KDT_NPLCungUng.TKMD_ID AS TKMD_ID, t_View_KDT_NPLCungUng.TKCT_ID AS TKCT_ID
FROM         t_KDT_GC_NguyenPhuLieu INNER JOIN
                      t_View_KDT_NPLCungUng ON t_KDT_GC_NguyenPhuLieu.Ma = t_View_KDT_NPLCungUng.MaNguyenPhuLieu
WHERE     (t_KDT_GC_NguyenPhuLieu.HopDong_ID = @HopDongID )) AS t_view_NPLCungUng
GROUP BY STK,Ma
ORDER BY Ma


END  


GO



/****** Object:  View [dbo].[v_GC_PhanBo]    Script Date: 05/11/2014 16:36:31 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_GC_PhanBo]'))
DROP VIEW [dbo].[v_GC_PhanBo]
GO



/****** Object:  View [dbo].[v_GC_PhanBo]    Script Date: 05/11/2014 16:36:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_GC_PhanBo]
AS
/* To Khai Xuat GC binh thuong va  to khai NGC binh thuong*/ SELECT pbtkx_1.MaSP, pbtkx_1.SoLuongXuat, pbtkx_1.MaDoanhNghiep, pbtkn.SoToKhaiNhap, 
                      pbtkn.MaLoaiHinhNhap, pbtkn.MaHaiQuanNhap, pbtkn.NamDangKyNhap, pbtkn.DinhMucChung, pbtkn.LuongTonDau, pbtkn.LuongPhanBo, pbtkn.LuongCungUng, 
                      pbtkn.LuongTonCuoi, pbtkn.TKXuat_ID, pbtkx_1.ID_TKMD, pbtkn.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.TenHang AS TenNPL, 
                      hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, hangNhap.ThueSuatXNK AS ThueSuat, hangXuat.NgayDangKy AS NgayDangKyXuat, 
                      hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaTinhThue AS TyGiaTT, hangXuat.NgayDangKy AS NgayThucXuat, 
                      hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat
                       WHERE      (MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%')) AS pbtkx_1 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, LuongCungUng, 
                                                   LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap
                            WHERE      (MaLoaiHinhNhap LIKE 'N%')) AS pbtkn ON pbtkx_1.ID = pbtkn.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn.MaNPL = hangNhap.MaPhu INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_1.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_1.MaSP = hangXuat.MaPhu
UNION
/* To Khai xuat GC Chuyen Tiep va To khai nhap GC Binh Thuong*/ SELECT pbtkx_1_1.MaSP, pbtkx_1_1.SoLuongXuat, pbtkx_1_1.MaDoanhNghiep, pbtkn_2.SoToKhaiNhap, 
                      pbtkn_2.MaLoaiHinhNhap, pbtkn_2.MaHaiQuanNhap, pbtkn_2.NamDangKyNhap, pbtkn_2.DinhMucChung, pbtkn_2.LuongTonDau, pbtkn_2.LuongPhanBo, 
                      pbtkn_2.LuongCungUng, pbtkn_2.LuongTonCuoi, pbtkn_2.TKXuat_ID, pbtkx_1_1.ID_TKMD, pbtkn_2.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, 
                      hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, 
                      hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.TenHang AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, 
                      hangNhap.ThueSuatXNK AS ThueSuat, hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_S, 
                      hangNhap.TyGiaTinhThue AS TyGiaTT, hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_2
                       WHERE      (MaDoanhNghiep LIKE 'PH%' OR
                                              MaDoanhNghiep LIKE '%19' OR
                                              MaDoanhNghiep LIKE '%18' OR
                                              MaDoanhNghiep LIKE '%20'OR
                                              MaDoanhNghiep LIKE 'XVE54')) AS pbtkx_1_1 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, LuongCungUng, 
                                                   LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_2
                            WHERE      (MaLoaiHinhNhap LIKE 'N%')) AS pbtkn_2 ON pbtkx_1_1.ID = pbtkn_2.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn_2.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn_2.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn_2.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn_2.MaNPL = hangNhap.MaPhu INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_1_1.ID_TKMD = hangXuat.ID AND pbtkx_1_1.MaSP = hangXuat.MaHang
UNION
/* To Khai Xuat GC Binh Thuong va To Khai Nhap GC Chuyen Tiep*/ SELECT pbtkx.MaSP, pbtkx.SoLuongXuat, pbtkx.MaDoanhNghiep, pbtkn_1.SoToKhaiNhap, 
                      pbtkn_1.MaLoaiHinhNhap, pbtkn_1.MaHaiQuanNhap, pbtkn_1.NamDangKyNhap, pbtkn_1.DinhMucChung, pbtkn_1.LuongTonDau, pbtkn_1.LuongPhanBo, 
                      pbtkn_1.LuongCungUng, pbtkn_1.LuongTonCuoi, pbtkn_1.TKXuat_ID, pbtkx.ID_TKMD, pbtkn_1.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, 
                      hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, 
                      hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, hangNhap.DonGia AS DonGiaTT, 0 AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaVND AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      (MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%')) AS pbtkx INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, LuongCungUng, 
                                                   LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE 'PH%' OR
                                                   MaLoaiHinhNhap LIKE '%19' OR
                                                   MaLoaiHinhNhap LIKE '%18' OR
                                                   MaLoaiHinhNhap LIKE '%20'OR
                                              MaDoanhNghiep LIKE 'NVE23')) AS pbtkn_1 ON pbtkx.ID = pbtkn_1.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn_1.MaNPL = hangNhap.MaHang INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx.ID_TKMD = hangXuat.TKMD_ID AND pbtkx.MaSP = hangXuat.MaPhu
UNION
/* To Khai xuat GC Chuyen Tiep va to khai Nhap GC Chuyen Tiep*/ SELECT pbtkx_2.MaSP, pbtkx_2.SoLuongXuat, pbtkx_2.MaDoanhNghiep, pbtkn_1_1.SoToKhaiNhap, 
                      pbtkn_1_1.MaLoaiHinhNhap, pbtkn_1_1.MaHaiQuanNhap, pbtkn_1_1.NamDangKyNhap, pbtkn_1_1.DinhMucChung, pbtkn_1_1.LuongTonDau, 
                      pbtkn_1_1.LuongPhanBo, pbtkn_1_1.LuongCungUng, pbtkn_1_1.LuongTonCuoi, pbtkn_1_1.TKXuat_ID, pbtkx_2.ID_TKMD, pbtkn_1_1.MaNPL, 
                      hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, 
                      hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, 
                      hangNhap.DonGia AS DonGiaTT, 0 AS ThueSuat, hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_SP, 
                      hangNhap.TyGiaVND AS TyGiaTT, hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      (MaDoanhNghiep LIKE 'PH%' OR
                                              MaDoanhNghiep LIKE '%19' OR
                                              MaDoanhNghiep LIKE '%18' OR
                                              MaDoanhNghiep LIKE '%20'OR
                                              MaDoanhNghiep LIKE 'XVE54')) AS pbtkx_2 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, LuongCungUng, 
                                                   LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE 'PH%' OR
                                                   MaLoaiHinhNhap LIKE '%19' OR
                                                   MaLoaiHinhNhap LIKE '%18' OR
                                                   MaLoaiHinhNhap LIKE '%20'OR
                                              MaDoanhNghiep LIKE 'NVE23')) AS pbtkn_1_1 ON pbtkx_2.ID = pbtkn_1_1.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1_1.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn_1_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn_1_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn_1_1.MaNPL = hangNhap.MaHang INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_2.ID_TKMD = hangXuat.ID AND pbtkx_2.MaSP = hangXuat.MaHang
UNION
SELECT     pbtkx_3.MaSP, pbtkx_3.SoLuongXuat, pbtkx_3.MaDoanhNghiep, pbtkn_1_2.SoToKhaiNhap, pbtkn_1_2.MaLoaiHinhNhap, pbtkn_1_2.MaHaiQuanNhap, 
                      pbtkn_1_2.NamDangKyNhap, pbtkn_1_2.DinhMucChung, pbtkn_1_2.LuongTonDau, pbtkn_1_2.LuongPhanBo, pbtkn_1_2.LuongCungUng, pbtkn_1_2.LuongTonCuoi, 
                      pbtkn_1_2.TKXuat_ID, pbtkx_3.ID_TKMD, pbtkn_1_2.MaNPL, '01/01/1900' AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.Ten AS TenNPL, 
                      hangNhap.DVT_ID AS DVT_NPL, 0 AS DonGiaTT, 0 AS ThueSuat, hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, 
                      hangXuat.DVT_ID AS DVT_SP, 0 AS TyGiaTT, hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      (MaDoanhNghiep LIKE 'XGC%'OR MaDoanhNghiep LIKE 'XV%')) AS pbtkx_3 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, LuongCungUng, 
                                                   LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE '')) AS pbtkn_1_2 ON pbtkx_3.ID = pbtkn_1_2.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_3.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_3.MaSP = hangXuat.MaPhu INNER JOIN
                      dbo.t_GC_NguyenPhuLieu AS hangNhap ON pbtkn_1_2.MaNPL = hangNhap.Ma AND hangNhap.HopDong_ID = hangXuat.IDHopDong

GO



							 
/****** Object:  View [dbo].[v_GC_ToKhaiXuatDaPhanBo]    Script Date: 05/11/2014 16:39:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_GC_ToKhaiXuatDaPhanBo]'))
DROP VIEW [dbo].[v_GC_ToKhaiXuatDaPhanBo]
GO


/****** Object:  View [dbo].[v_GC_ToKhaiXuatDaPhanBo]    Script Date: 05/11/2014 16:39:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[v_GC_ToKhaiXuatDaPhanBo]
AS
SELECT     TOP (100) PERCENT b.ID, b.SoTiepNhan, b.NgayTiepNhan, b.MaHaiQuan, b.SoToKhai, b.MaLoaiHinh, b.NgayDangKy, b.MaDoanhNghiep, b.IDHopDong, a.STT
FROM         (SELECT     MAX(ID) AS STT, ID_TKMD
                       FROM          dbo.t_GC_PhanBoToKhaiXuat
                       WHERE      ((MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%') AND MaDoanhNghiep <> 'XGC18' AND MaDoanhNghiep <> 'XGC19' AND MaDoanhNghiep <> 'XGC20')
                       GROUP BY ID_TKMD) AS a INNER JOIN
                      dbo.t_KDT_ToKhaiMauDich AS b ON a.ID_TKMD = b.ID
UNION
SELECT     TOP (100) PERCENT b.ID, b.SoTiepNhan, b.NgayTiepNhan, b.MaHaiQuanTiepNhan AS MaHaiQuan, b.SoToKhai, b.MaLoaiHinh, b.NgayDangKy, b.MaDoanhNghiep, 
                      b.IDHopDong, a_1.STT
FROM         (SELECT     MAX(ID) AS STT, ID_TKMD
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      (MaDoanhNghiep LIKE 'PH%' OR
                                              MaDoanhNghiep = 'XGC18' OR
                                              MaDoanhNghiep = 'XGC19' OR
                                              MaDoanhNghiep = 'XGC20'OR
                                              MaDoanhNghiep = 'XVE54')
                       GROUP BY ID_TKMD) AS a_1 INNER JOIN
                      dbo.t_KDT_GC_ToKhaiChuyenTiep AS b ON a_1.ID_TKMD = b.ID
ORDER BY STT


GO



/****** Object:  View [dbo].[v_GC_PhanBo_TKMD]    Script Date: 05/11/2014 16:51:54 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_GC_PhanBo_TKMD]'))
DROP VIEW [dbo].[v_GC_PhanBo_TKMD]
GO



/****** Object:  View [dbo].[v_GC_PhanBo_TKMD]    Script Date: 05/11/2014 16:51:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[v_GC_PhanBo_TKMD]
AS
SELECT     pbtkx_1.MaSP, pbtkx_1.SoLuongXuat, pbtkx_1.MaDoanhNghiep, pbtkn.SoToKhaiNhap, pbtkn.MaLoaiHinhNhap, pbtkn.MaHaiQuanNhap, 
                      pbtkn.NamDangKyNhap, pbtkn.DinhMucChung, pbtkn.LuongTonDau, pbtkn.LuongPhanBo, pbtkn.LuongCungUng, pbtkn.LuongTonCuoi, pbtkn.TKXuat_ID, 
                      pbtkx_1.ID_TKMD, pbtkn.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, hangNhap.ThueSuatXNK AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaTinhThue AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong, hangNhap.TriGiaKB AS TriGia
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat
                       WHERE      ((MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%')  AND MaDoanhNghiep <> 'XVE54' AND MaDoanhNghiep <> 'XGC18' AND MaDoanhNghiep <> 'XGC19' AND MaDoanhNghiep <> 'XGC20')) AS pbtkx_1 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap
                            WHERE      (MaLoaiHinhNhap LIKE 'N%' AND MaLoaiHinhNhap <> 'NVE23' AND MaLoaiHinhNhap <> 'NGC18' AND MaLoaiHinhNhap <> 'NGC19' AND MaLoaiHinhNhap <> 'NGC20')) AS pbtkn ON pbtkx_1.ID = pbtkn.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn.MaNPL = hangNhap.MaPhu INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_1.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_1.MaSP = hangXuat.MaPhu
UNION
SELECT     pbtkx.MaSP, pbtkx.SoLuongXuat, pbtkx.MaDoanhNghiep, pbtkn_1.SoToKhaiNhap, pbtkn_1.MaLoaiHinhNhap, pbtkn_1.MaHaiQuanNhap, 
                      pbtkn_1.NamDangKyNhap, pbtkn_1.DinhMucChung, pbtkn_1.LuongTonDau, pbtkn_1.LuongPhanBo, pbtkn_1.LuongCungUng, pbtkn_1.LuongTonCuoi, 
                      pbtkn_1.TKXuat_ID, pbtkx.ID_TKMD, pbtkn_1.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, hangNhap.DonGia AS DonGiaTT, 0 AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaVND AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong, hangNhap.TriGia AS TriGia
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      ((MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%')  AND MaDoanhNghiep <> 'XVE54' AND MaDoanhNghiep <> 'XGC18' AND MaDoanhNghiep <> 'XGC19' AND MaDoanhNghiep <> 'XGC20')) AS pbtkx INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE 'PH%' OR MaLoaiHinhNhap = 'NVE23' OR MaLoaiHinhNhap='NGC18' OR MaLoaiHinhNhap='NGC19' OR MaLoaiHinhNhap='NGC20')) AS pbtkn_1 ON pbtkx.ID = pbtkn_1.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1.SoToKhaiNhap = hangNhap.SoToKhai AND 
                      pbtkn_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND pbtkn_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND 
                      pbtkn_1.MaNPL = hangNhap.MaHang INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx.ID_TKMD = hangXuat.TKMD_ID AND pbtkx.MaSP = hangXuat.MaPhu
UNION

SELECT     pbtkx_3.MaSP, pbtkx_3.SoLuongXuat, pbtkx_3.MaDoanhNghiep, pbtkn_1_2.SoToKhaiNhap, pbtkn_1_2.MaLoaiHinhNhap, pbtkn_1_2.MaHaiQuanNhap, 
                      pbtkn_1_2.NamDangKyNhap, pbtkn_1_2.DinhMucChung, pbtkn_1_2.LuongTonDau, pbtkn_1_2.LuongPhanBo, pbtkn_1_2.LuongCungUng, 
                      pbtkn_1_2.LuongTonCuoi, pbtkn_1_2.TKXuat_ID, pbtkx_3.ID_TKMD, pbtkn_1_2.MaNPL, '01/01/1900' AS NgayDangKyNhap, 
                      hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, 
                      hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.Ten AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, 0 AS DonGiaTT, 0 AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, 0 AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong, hangXuat.TriGiaKB AS TriGia
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      ((MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%') AND MaDoanhNghiep <> 'XVE54'  AND MaDoanhNghiep <> 'XGC18' AND MaDoanhNghiep <> 'XGC19' AND MaDoanhNghiep <> 'XGC20')) AS pbtkx_3 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE '')) AS pbtkn_1_2 ON pbtkx_3.ID = pbtkn_1_2.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_3.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_3.MaSP = hangXuat.MaPhu INNER JOIN
                      dbo.t_GC_NguyenPhuLieu AS hangNhap ON pbtkn_1_2.MaNPL = hangNhap.Ma AND hangNhap.HopDong_ID = hangXuat.IDHopDong




GO




/****** Object:  View [dbo].[v_GC_PhanBo_TKCT]    Script Date: 05/11/2014 17:00:51 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_GC_PhanBo_TKCT]'))
DROP VIEW [dbo].[v_GC_PhanBo_TKCT]
GO


/****** Object:  View [dbo].[v_GC_PhanBo_TKCT]    Script Date: 05/11/2014 17:00:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[v_GC_PhanBo_TKCT]
AS
SELECT     pbtkx_1_1.MaSP, pbtkx_1_1.SoLuongXuat, pbtkx_1_1.MaDoanhNghiep, pbtkn_2.SoToKhaiNhap, pbtkn_2.MaLoaiHinhNhap, pbtkn_2.MaHaiQuanNhap, 
                      pbtkn_2.NamDangKyNhap, pbtkn_2.DinhMucChung, pbtkn_2.LuongTonDau, pbtkn_2.LuongPhanBo, pbtkn_2.LuongCungUng, pbtkn_2.LuongTonCuoi, 
                      pbtkn_2.TKXuat_ID, pbtkx_1_1.ID_TKMD, pbtkn_2.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, hangNhap.ThueSuatXNK AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_S, hangNhap.TyGiaTinhThue AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong, hangNhap.TriGiaKB AS TriGia
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_2
                       WHERE      (MaDoanhNghiep LIKE 'PH%' OR
                                              MaDoanhNghiep LIKE '%18' OR
                                              MaDoanhNghiep LIKE '%19' OR
                                              MaDoanhNghiep LIKE '%20'OR
                                              MaDoanhNghiep LIKE 'XVE54')) AS pbtkx_1_1 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, LuongCungUng, 
                                                   LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_2
                            WHERE      (MaLoaiHinhNhap LIKE 'N%' AND  MaLoaiHinhNhap <> 'NVE23' AND MaLoaiHinhNhap <> 'NGC18' AND MaLoaiHinhNhap <> 'NGC19' AND MaLoaiHinhNhap <> 'NGC20')) AS pbtkn_2 ON 
                      pbtkx_1_1.ID = pbtkn_2.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn_2.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn_2.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn_2.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn_2.MaNPL = hangNhap.MaPhu INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_1_1.ID_TKMD = hangXuat.ID AND pbtkx_1_1.MaSP = hangXuat.MaHang
UNION
SELECT     pbtkx_2.MaSP, pbtkx_2.SoLuongXuat, pbtkx_2.MaDoanhNghiep, pbtkn_1_1.SoToKhaiNhap, pbtkn_1_1.MaLoaiHinhNhap, pbtkn_1_1.MaHaiQuanNhap, 
                      pbtkn_1_1.NamDangKyNhap, pbtkn_1_1.DinhMucChung, pbtkn_1_1.LuongTonDau, pbtkn_1_1.LuongPhanBo, pbtkn_1_1.LuongCungUng, pbtkn_1_1.LuongTonCuoi, 
                      pbtkn_1_1.TKXuat_ID, pbtkx_2.ID_TKMD, pbtkn_1_1.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, hangNhap.DonGia AS DonGiaTT, 0 AS ThueSuat, hangXuat.NgayDangKy AS NgayDangKyXuat, 
                      hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_SP, hangNhap.TyGiaVND AS TyGiaTT, hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong, 
                      hangNhap.TriGia AS TriGia
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      (MaDoanhNghiep LIKE 'PH%' OR
                                              MaDoanhNghiep LIKE '%18' OR
                                              MaDoanhNghiep LIKE '%19' OR
                                              MaDoanhNghiep LIKE '%20'OR
                                              MaDoanhNghiep LIKE 'XVE54')) AS pbtkx_2 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, LuongCungUng, 
                                                   LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE 'PH%' OR
                                                   MaLoaiHinhNhap LIKE '%18' OR
                                                   MaLoaiHinhNhap LIKE '%19' OR
                                                   MaLoaiHinhNhap LIKE '%20'OR
                                                   MaLoaiHinhNhap LIKE 'NVE23')) AS pbtkn_1_1 ON pbtkx_2.ID = pbtkn_1_1.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1_1.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn_1_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn_1_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn_1_1.MaNPL = hangNhap.MaHang INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_2.ID_TKMD = hangXuat.ID AND pbtkx_2.MaSP = hangXuat.MaHang





GO



/****** Object:  StoredProcedure [dbo].[p_XemPhanBoToKhai]    Script Date: 05/11/2014 17:21:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_XemPhanBoToKhai]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_XemPhanBoToKhai]
GO


/****** Object:  StoredProcedure [dbo].[p_XemPhanBoToKhai]    Script Date: 05/11/2014 17:21:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Huynh Ngoc Khanh
-- Create date: 01/04/2013
-- Description:	Xem bang phan bo
-- =============================================
CREATE PROCEDURE [dbo].[p_XemPhanBoToKhai]
	-- Add the parameters for the stored procedure here
	@ID_TKX  BIGINT,
	@MaNPL	VARCHAR(30),
	@MaLoaiHinhXuat VARCHAR(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	IF(@MaLoaiHinhXuat LIKE '%18' OR @MaLoaiHinhXuat LIKE '%19' OR @MaLoaiHinhXuat LIKE '%20' OR @MaLoaiHinhXuat LIKE 'PH%' OR @MaLoaiHinhXuat LIKE 'XVE54')
	BEGIN
		select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo
		FROM
		-- To Khai xuat GC Chuyen Tiep va To khai nhap GC Binh Thuong
(SELECT     pbtkx_1_1.MaSP, pbtkx_1_1.SoLuongXuat, pbtkx_1_1.MaDoanhNghiep, pbtkn_2.SoToKhaiNhap, pbtkn_2.MaLoaiHinhNhap, pbtkn_2.MaHaiQuanNhap, 
                      pbtkn_2.NamDangKyNhap, pbtkn_2.DinhMucChung, pbtkn_2.LuongTonDau, pbtkn_2.LuongPhanBo, pbtkn_2.LuongCungUng, pbtkn_2.LuongTonCuoi, 
                      pbtkn_2.TKXuat_ID, pbtkx_1_1.ID_TKMD, pbtkn_2.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, hangNhap.ThueSuatXNK AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_S, hangNhap.TyGiaTinhThue AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_2
                       WHERE    (t_GC_PhanBoToKhaiXuat_2.ID_TKMD = @ID_TKX) AND (t_GC_PhanBoToKhaiXuat_2.MaDoanhNghiep = @MaLoaiHinhXuat)) AS pbtkx_1_1 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_2
                            WHERE      (MaLoaiHinhNhap LIKE 'N%') AND t_GC_PhanBoToKhaiNhap_2.MaNPL = @MaNPL) AS pbtkn_2 ON pbtkx_1_1.ID = pbtkn_2.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn_2.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn_2.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn_2.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn_2.MaNPL = hangNhap.MaPhu INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_1_1.ID_TKMD = hangXuat.ID AND pbtkx_1_1.MaSP = hangXuat.MaHang
UNION
-- Tờ khai Xuất GC Chuyển tiếp được cung ứng từ tờ khai nhập GC chuyển tiếp 
SELECT     pbtkx_2.MaSP, pbtkx_2.SoLuongXuat, pbtkx_2.MaDoanhNghiep, pbtkn_1_1.SoToKhaiNhap, pbtkn_1_1.MaLoaiHinhNhap, pbtkn_1_1.MaHaiQuanNhap, 
                      pbtkn_1_1.NamDangKyNhap, pbtkn_1_1.DinhMucChung, pbtkn_1_1.LuongTonDau, pbtkn_1_1.LuongPhanBo, pbtkn_1_1.LuongCungUng, 
                      pbtkn_1_1.LuongTonCuoi, pbtkn_1_1.TKXuat_ID, pbtkx_2.ID_TKMD, pbtkn_1_1.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, 
                      hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, 
                      hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, hangNhap.DonGia AS DonGiaTT, 
                      0 AS ThueSuat, hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_SP, 
                      hangNhap.TyGiaVND AS TyGiaTT, hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      (t_GC_PhanBoToKhaiXuat_1.ID_TKMD = @ID_TKX) AND (t_GC_PhanBoToKhaiXuat_1.MaDoanhNghiep = @MaLoaiHinhXuat)) AS pbtkx_2 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE 'PH%' OR MaLoaiHinhNhap LIKE 'NVE23' OR MaLoaiHinhNhap LIKE '%19' OR MaLoaiHinhNhap LIKE '%18' OR MaLoaiHinhNhap LIKE '%20')AND t_GC_PhanBoToKhaiNhap_1.MaNPL = @MaNPL ) AS pbtkn_1_1 ON pbtkx_2.ID = pbtkn_1_1.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1_1.SoToKhaiNhap = hangNhap.SoToKhai AND 
                      pbtkn_1_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND pbtkn_1_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND 
                      pbtkn_1_1.MaNPL = hangNhap.MaHang INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_2.ID_TKMD = hangXuat.ID AND pbtkx_2.MaSP = hangXuat.MaHang) AS A
		GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL                   

	END
	ELSE
	BEGIN
		select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo
		FROM
		(SELECT     pbtkx_1.MaSP, pbtkx_1.SoLuongXuat, pbtkx_1.MaDoanhNghiep, pbtkn.SoToKhaiNhap, pbtkn.MaLoaiHinhNhap, pbtkn.MaHaiQuanNhap, 
                      pbtkn.NamDangKyNhap, pbtkn.DinhMucChung, pbtkn.LuongTonDau, pbtkn.LuongPhanBo, pbtkn.LuongCungUng, pbtkn.LuongTonCuoi, pbtkn.TKXuat_ID, 
                      pbtkx_1.ID_TKMD, pbtkn.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, hangNhap.ThueSuatXNK AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaTinhThue AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat
                       WHERE      ((MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%' )AND dbo.t_GC_PhanBoToKhaiXuat.ID_TKMD = @ID_TKX)) AS pbtkx_1 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap
                            WHERE      (MaLoaiHinhNhap LIKE 'N%' AND dbo.t_GC_PhanBoToKhaiNhap.MaNPL = @MaNPL)) AS pbtkn ON pbtkx_1.ID = pbtkn.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn.MaNPL = hangNhap.MaPhu INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_1.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_1.MaSP = hangXuat.MaPhu
UNION
SELECT     pbtkx.MaSP, pbtkx.SoLuongXuat, pbtkx.MaDoanhNghiep, pbtkn_1.SoToKhaiNhap, pbtkn_1.MaLoaiHinhNhap, pbtkn_1.MaHaiQuanNhap, 
                      pbtkn_1.NamDangKyNhap, pbtkn_1.DinhMucChung, pbtkn_1.LuongTonDau, pbtkn_1.LuongPhanBo, pbtkn_1.LuongCungUng, pbtkn_1.LuongTonCuoi, 
                      pbtkn_1.TKXuat_ID, pbtkx.ID_TKMD, pbtkn_1.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, hangNhap.DonGia AS DonGiaTT, 0 AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaVND AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      ((MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%' ) AND ID_TKMD = @ID_TKX))  AS pbtkx INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      ((MaLoaiHinhNhap LIKE 'PH%' OR  MaLoaiHinhNhap LIKE 'NVE23' OR MaLoaiHinhNhap LIKE '%19' oR MaLoaiHinhNhap LIKE '%18' OR MaLoaiHinhNhap LIKE '%20') AND (MaNPL = @MaNPL))) 
                            AS pbtkn_1 ON pbtkx.ID = pbtkn_1.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1.SoToKhaiNhap = hangNhap.SoToKhai AND 
                      pbtkn_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND pbtkn_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND 
                      pbtkn_1.MaNPL = hangNhap.MaHang INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx.ID_TKMD = hangXuat.TKMD_ID AND pbtkx.MaSP = hangXuat.MaPhu
UNION
SELECT     pbtkx_3.MaSP, pbtkx_3.SoLuongXuat, pbtkx_3.MaDoanhNghiep, pbtkn_1_2.SoToKhaiNhap, pbtkn_1_2.MaLoaiHinhNhap, pbtkn_1_2.MaHaiQuanNhap, 
                      pbtkn_1_2.NamDangKyNhap, pbtkn_1_2.DinhMucChung, pbtkn_1_2.LuongTonDau, pbtkn_1_2.LuongPhanBo, pbtkn_1_2.LuongCungUng, 
                      pbtkn_1_2.LuongTonCuoi, pbtkn_1_2.TKXuat_ID, pbtkx_3.ID_TKMD, pbtkn_1_2.MaNPL, '01/01/1900' AS NgayDangKyNhap, 
                      hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, 
                      hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.Ten AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, 0 AS DonGiaTT, 0 AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, 0 AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      (MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%' )) AS pbtkx_3 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE '')) AS pbtkn_1_2 ON pbtkx_3.ID = pbtkn_1_2.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_3.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_3.MaSP = hangXuat.MaPhu INNER JOIN
                      dbo.t_GC_NguyenPhuLieu AS hangNhap ON pbtkn_1_2.MaNPL = hangNhap.Ma AND hangNhap.HopDong_ID = hangXuat.IDHopDong) AS B
        GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL
		END
	
	
END


GO

/****** Object:  StoredProcedure [dbo].[p_GC_NPLNhapTonThucTe_SelectDynamic]    Script Date: 05/12/2014 16:56:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_NPLNhapTonThucTe_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_NPLNhapTonThucTe_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_GC_NPLNhapTonThucTe_SelectDynamic]    Script Date: 05/12/2014 16:56:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_NPLNhapTonThucTe_SelectDynamic]  
-- Database: ECS_GIACONG  
-- Author: Ngo Thanh Tung  
-- Time created: Wednesday, January 14, 2009  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_GC_NPLNhapTonThucTe_SelectDynamic]  
 @WhereCondition nvarchar(500),  
 @OrderByExpression nvarchar(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(3250)  
  
SET @SQL = 'SELECT  a.*, Case WHEN a.MaLoaiHinh like ''%V%'' then (Select top 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = a.SoToKhai ) else a.SoToKhai end as SoToKhaiVNACCS,  b.TenHang as TenNPL,b.MaHS,b.DVT_ID, b.TriGiaKB, a.Ton * b.TriGiaKB / a.Luong  as TonTriGiaKB, b.NuocXX_ID as XuatXu  
 FROM (SELECT * FROM [dbo].[t_GC_NPLNhapTonThucTe] WHERE MaLoaiHinh LIKE ''N%'')a  
 INNER JOIN v_GC_HangToKhai b ON a.SoToKhai = b.SoToKhai AND a.NgayDangKy = b.NgayDangKY AND a.MaLoaiHinh = b.MaLoaiHinh AND a.MaNPL = b.MaPhu  
  
 WHERE ' + @WhereCondition +   
 'UNION   
 SELECT  a.*,Case WHEN a.MaLoaiHinh like ''%V%'' then (Select top 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = a.SoToKhai ) else a.SoToKhai end as SoToKhaiVNACCS, b.TenHang as TenNPL,b.MaHS,b.ID_DVT as DVT_ID, b.TriGia, a.Ton * b.TriGia / a.Luong  as TonTriGiaKB, b.ID_NuocXX as XuatXu  
 FROM (SELECT * FROM [dbo].[t_GC_NPLNhapTonThucTe] WHERE MaLoaiHinh LIKE ''PH%'' OR MaLoaiHinh LIKE ''N%'')a  
 INNER JOIN v_GC_HangChuyenTiep b ON a.SoToKhai = b.SoToKhai AND a.NgayDangKy = b.NgayDangKY AND a.MaLoaiHinh = b.MaLoaiHinh AND a.MaNPL = b.MaHang  
 WHERE ' + @WhereCondition   
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  


GO


/****** Object:  View [dbo].[t_View_LuongNPLTrongTKXuat]    Script Date: 05/12/2014 18:59:33 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[t_View_LuongNPLTrongTKXuat]'))
DROP VIEW [dbo].[t_View_LuongNPLTrongTKXuat]
GO

/****** Object:  View [dbo].[t_View_LuongNPLTrongTKXuat]    Script Date: 05/12/2014 18:59:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[t_View_LuongNPLTrongTKXuat]
AS
SELECT     CASE WHEN t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%V%' THEN t_KDT_ToKhaiMauDich.LoaiVanDon ELSE CONVERT(VARCHAR(12), 
                      t_KDT_ToKhaiMauDich.SoToKhai) END AS SoToKhaiVNACCS, dbo.t_KDT_ToKhaiMauDich.MaHaiQuan, dbo.t_KDT_ToKhaiMauDich.SoToKhai, 
                      dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, dbo.t_KDT_HangMauDich.MaPhu AS MaSP, dbo.t_KDT_HangMauDich.TenHang, dbo.t_KDT_HangMauDich.SoLuong, 
                      dbo.t_GC_DinhMuc.MaNguyenPhuLieu, dbo.t_GC_NguyenPhuLieu.Ten, 
                      dbo.t_KDT_HangMauDich.SoLuong * (dbo.t_GC_DinhMuc.DinhMucSuDung / 100 * dbo.t_GC_DinhMuc.TyLeHaoHut + dbo.t_GC_DinhMuc.DinhMucSuDung) 
                      AS LuongNPL, dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep, dbo.t_KDT_ToKhaiMauDich.IDHopDong, dbo.t_KDT_ToKhaiMauDich.ID, 
                      dbo.t_GC_DinhMuc.DinhMucSuDung, dbo.t_GC_DinhMuc.TyLeHaoHut, YEAR(dbo.t_KDT_ToKhaiMauDich.NgayDangKy) AS NamDK
FROM         dbo.t_KDT_ToKhaiMauDich INNER JOIN
                      dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID INNER JOIN
                      dbo.t_GC_DinhMuc ON dbo.t_GC_DinhMuc.MaSanPham = dbo.t_KDT_HangMauDich.MaPhu AND 
                      dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_KDT_ToKhaiMauDich.IDHopDong INNER JOIN
                      dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID AND 
                      dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma
WHERE     (dbo.t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S')

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t_KDT_ToKhaiMauDich"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 122
               Right = 241
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t_KDT_HangMauDich"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 242
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t_GC_DinhMuc"
            Begin Extent = 
               Top = 6
               Left = 279
               Bottom = 122
               Right = 455
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t_GC_NguyenPhuLieu"
            Begin Extent = 
               Top = 126
               Left = 297
               Bottom = 242
               Right = 470
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N't_View_LuongNPLTrongTKXuat'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N't_View_LuongNPLTrongTKXuat'
GO



------------------------------------------------------- Alter store Bao cao GC---------------------------------------


/****** Object:  StoredProcedure [dbo].[p_GC_BC01HSTK_GC_TT117]    Script Date: 05/12/2014 20:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  LanNT      
-- Create date:       
-- Description: Modify 19/06/2012      
-- =============================================      
      
ALTER PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT117]       
 -- Add the parameters for the stored procedure here      
 @IDHopDong BIGINT,      
 @MaHaiQuan CHAR(6),      
 @MaDoanhNghiep VARCHAR(14)       
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',  
-- ROW_NUMBER() OVER (ORDER BY  ToKhai.MaNguyenPhuLieu) AS 'STT'

SELECT    ROW_NUMBER() OVER (ORDER BY  ToKhai.MaNguyenPhuLieu) AS 'STT',  ToKhai.IDHopDong AS ID, ToKhai.MaDoanhNghiep, ToKhai.MaHaiQuan, ToKhai.SoToKhai, ToKhai.NgayDangKy, ToKhai.TenHang AS TenNguyenPhuLieu, 
                      ToKhai.MaNguyenPhuLieu, ToKhai.SoLuong, t_HaiQuan_DonViTinh.Ten AS DVT, '-' AS TongLuong
FROM         (SELECT     CONVERT(varchar(20),
 (case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_ToKhaiMauDich.SoToKhai) else t_KDT_ToKhaiMauDich.SoToKhai end )
) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, 
                                              t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, t_KDT_ToKhaiMauDich.MaHaiQuan, 
                                              t_KDT_HangMauDich.MaPhu AS MaNguyenPhuLieu, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.DVT_ID, 
                                              t_KDT_ToKhaiMauDich.MaDoanhNghiep
                       FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN
                                              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
                       WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                              (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'N%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N')
                       UNION
                       SELECT     CONVERT(varchar(20), 
                       (case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) else t_KDT_GC_ToKhaiChuyenTiep.SoToKhai end )
                       ) + '/' + t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh AS SoToKhai, 
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, 
                                             t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, t_KDT_GC_HangChuyenTiep.MaHang AS MaNguyenPhuLieu, t_KDT_GC_HangChuyenTiep.SoLuong, 
                                             t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.ID_DVT, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                       FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                             t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                       WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                             (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh IN ('PHPLN', 'PHSPN', 'NGC18', 'NGC19', 'PHTBN','NVE23')) AND (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')) 
                      AS ToKhai INNER JOIN
                      t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID
WHERE     (ToKhai.IDHopDong = @IDHopDong) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan)
END   
GO



/****** Object:  StoredProcedure [dbo].[p_GC_BC02HSTK_GC_TT117]    Script Date: 05/12/2014 20:21:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  LanNT    
-- Create date:     
-- Description:     
-- =============================================    
    
ALTER PROCEDURE [dbo].[p_GC_BC02HSTK_GC_TT117]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 -- p_GC_BC02HSTK_GC_TT117 710, 'C34C','0400101556'    
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',    
    --ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT', 
SELECT DISTINCT 
                   ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',   
                   CONVERT(varchar(50), (case when ToKhai.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = ToKhai.SoToKhai) else ToKhai.SoToKhai end ))
                    + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai, ToKhai.NgayDangKy, v_KDT_SP_Xuat_HD.MaPhu AS MaHang, 
                      ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, '-' AS TongCong
FROM         v_KDT_SP_Xuat_HD INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
                                                   t_KDT_ToKhaiMauDich.MaDoanhNghiep
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S')
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, t_KDT_GC_HangChuyenTiep.MaHang, 
                                                  t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19'OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XVE54')) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong AND 
                      v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang INNER JOIN
                      t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID INNER JOIN
                      t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
WHERE     (v_KDT_SP_Xuat_HD.IDHopDong = @IDHopDong) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan)
ORDER BY MaHang 
    
END 

GO

/****** Object:  StoredProcedure [dbo].[p_GC_BC03HSTK_GC_TT117]    Script Date: 05/12/2014 20:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  LanNT          
-- Create date:           
-- Description:   Modify 18/06/2012    
-- =============================================          
          
ALTER PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117]           
 -- Add the parameters for the stored procedure here          
 @IDHopDong BIGINT,          
 @MaHaiQuan CHAR(6),          
 @MaDoanhNghiep VARCHAR(14)           
           
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
 -- p_GC_BC03HSTK_GC_TT117 219, 'C34C','0400101556'          
-- SET @IDHopDong=306 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',          
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);  set @IDHopDong=  306;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';          
          
    --      
     /*ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT'*/    
     -- CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + v_HaiQuan_MaLoaiHinh.Ten_VT AS SoToKhai  
SELECT    ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT', ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, '-' AS TongCong, ToKhai.IDHopDong  
FROM         (  
          
      SELECT     CONVERT(varchar(50), 
      (case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_ToKhaiMauDich.SoToKhai) else t_KDT_ToKhaiMauDich.SoToKhai end )) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai,   
              LTRIM(RTRIM(t_KDT_ToKhaiMauDich.MaLoaiHinh)) AS MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong,   
              t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.DVT_ID  
       FROM         t_KDT_ToKhaiMauDich INNER JOIN  
              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN  
              t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
       WHERE     (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND   
        (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND  
        (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%') AND   
        (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND  
        (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND   
        (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep) AND  
        (NOT (t_KDT_ToKhaiMauDich.MaLoaiHinh IN ('XGC18,XGC19,XGC20','XVE54')))  
                         
                                                                                                                        
                       UNION  
                         
       SELECT     CONVERT(varchar(50), 
       (case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) else t_KDT_GC_ToKhaiChuyenTiep.SoToKhai end )) + '/' + t_HaiQuan_LoaiPhieuChuyenTiep.ID AS SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,   
                      t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang,   
                      t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_HangChuyenTiep.ID_DVT  
FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                      t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID INNER JOIN  
                      t_HaiQuan_LoaiPhieuChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = t_HaiQuan_LoaiPhieuChuyenTiep.ID  
WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND   
                      (LTRIM(RTRIM(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh)) IN ('PHPLX', 'XGC18', 'PHSPX','PHTBX','XVE54')) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep) AND   
                      (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N')           
       )   
                      AS ToKhai INNER JOIN  
                      t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID  
WHERE     (ToKhai.IDHopDong = @IDHopDong)  
ORDER BY ToKhai.MaHang  
  
END     
GO


--------------

/****** Object:  StoredProcedure [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]    Script Date: 05/12/2014 20:38:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Huynh Ngoc Khanh    
-- Create date:     18/04/2013
-- Description:     View BC 09/HSTK-TT117
-- =============================================    
ALTER PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;   
  
 --DECLARE
 --@IDHopDong BIGINT,    
 --@MaHaiQuan CHAR(6),    
 --@MaDoanhNghiep VARCHAR(14) 
 --SET @IDHopDong = 444
 --SET @MaHaiQuan = 'C34C'
 --SET @MaDoanhNghiep = '0400101556'
 SELECT  BangKe.SoNgayToKhai, BangKe.TenHang, BangKe.SoLuong, BangKe.TriGia, t_KDT_GC_HopDong.SoHopDong + ', ' + CONVERT(NVARCHAR(20),     
                      t_KDT_GC_HopDong.NgayDangKy, 103) + ', ' + CONVERT(NVARCHAR(20), t_KDT_GC_HopDong.NgayHetHan, 103) AS HdPhuLuc, BangKe.CuaKhau,     
                      BangKe.SoNgayBL, t_KDT_GC_HopDong.ID AS IDHopDong, t_KDT_GC_HopDong.MaHaiQuan, t_KDT_GC_HopDong.MaDoanhNghiep     FROM
 (
 (SELECT  CONVERT(NVARCHAR(20), 
 (case when TKMD.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = TKMD.SoToKhai) else TKMD.SoToKhai end )) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKMD.NgayDangKy, 103) AS SoNgayToKhai, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
								  t_KDT_HangMauDich.TriGiaKB AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau,
								   CASE PTVT_ID 
								  WHEN '001' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  WHEN '005' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  ELSE CASE TKMD.SoVanDon 
								  WHEN '' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103)
								  ELSE
								   TKMD.SoVanDon + ', ' + CONVERT(NVARCHAR(20), TKMD.NgayVanDon, 103) 
								       END 
								   END AS SoNgayBL
			    FROM (SELECT ID,SoToKhai,NgayDangKy,MaLoaiHinh,CuaKhau_ID,SoVanDon,NgayVanDon,PTVT_ID
			            FROM t_KDT_ToKhaiMauDich  
				WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' 
				AND t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong AND t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep 
				AND t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AS TKMD
	   LEFT JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKMD.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKMD.MaLoaiHinh
	   INNER JOIN t_KDT_HangMauDich ON TKMD.ID = t_KDT_HangMauDich.TKMD_ID
	   LEFT  JOIN ( select * from t_GC_BC08TT74_VanDonToKhaiXuat WHERE t_GC_BC08TT74_VanDonToKhaiXuat.IDHopDong = @IDHopDong) t_GC_BC08TT74_VanDonToKhaiXuat  ON t_GC_BC08TT74_VanDonToKhaiXuat.TKMD_ID = TKMD.ID)
 UNION
 (SELECT CONVERT(NVARCHAR(20),
  (case when TKCT.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = TKCT.SoToKhai) else TKCT.SoToKhai end )) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKCT.NgayDangKy, 103) AS SoNgayToKhai,t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, 
								  t_KDT_GC_HangChuyenTiep.TriGia AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau, '' AS SoNgayBL 
  FROM (SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep 
                WHERE  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE 'X%' OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE '%X')
						AND  t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep AND t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AS TKCT
	   LEFT JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKCT.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKCT.MaLoaiHinh
	   INNER JOIN t_KDT_GC_HangChuyenTiep ON TKCT.ID = t_KDT_GC_HangChuyenTiep.Master_ID)
)	
AS BangKe    
INNER JOIN t_KDT_GC_HopDong ON t_KDT_GC_HopDong.ID = @IDHopDong
		

 
END
GO

--Select * from t_KDT_ToKhaiMauDich WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' 
--				AND t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong AND t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep 
--				AND t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan

  IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '16.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('16.3',GETDATE(), N'Tạo table t_VNACCS_CapSoTK,Cập nhật store,View,report')
END	