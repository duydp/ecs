﻿namespace Company.Interface.VNACCS
{
    partial class VNACCS_ChuyenThanhKhoan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACCS_ChuyenThanhKhoan));
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.btnThucHien = new Janus.Windows.EditControls.UIButton();
            this.btnDong = new Janus.Windows.EditControls.UIButton();
            this.uiProgressBar1 = new Janus.Windows.EditControls.UIProgressBar();
            this.lblError = new System.Windows.Forms.Label();
            this.ckbKhongHienThi = new Janus.Windows.EditControls.UICheckBox();
            this.chkIsChuaThongQuan = new Janus.Windows.EditControls.UICheckBox();
            this.chkIsTK_AMA = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.chkIsTK_AMA);
            this.grbMain.Controls.Add(this.chkIsChuaThongQuan);
            this.grbMain.Controls.Add(this.ckbKhongHienThi);
            this.grbMain.Controls.Add(this.lblError);
            this.grbMain.Controls.Add(this.uiProgressBar1);
            this.grbMain.Controls.Add(this.btnDong);
            this.grbMain.Controls.Add(this.btnThucHien);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Size = new System.Drawing.Size(780, 453);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.grList);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 3);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(777, 389);
            this.uiGroupBox3.TabIndex = 3;
            this.uiGroupBox3.Text = "Danh sách tờ chưa chuyển thanh khoản";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic;
            this.grList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Hierarchical = true;
            this.grList.Location = new System.Drawing.Point(3, 17);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(771, 369);
            this.grList.TabIndex = 0;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grList_LoadingRow);
            // 
            // btnThucHien
            // 
            this.btnThucHien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThucHien.Image = ((System.Drawing.Image)(resources.GetObject("btnThucHien.Image")));
            this.btnThucHien.Location = new System.Drawing.Point(589, 425);
            this.btnThucHien.Name = "btnThucHien";
            this.btnThucHien.Size = new System.Drawing.Size(99, 23);
            this.btnThucHien.TabIndex = 4;
            this.btnThucHien.Text = "Thực hiện";
            this.btnThucHien.VisualStyleManager = this.vsmMain;
            this.btnThucHien.Click += new System.EventHandler(this.btnThucHien_Click);
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDong.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDong.Icon")));
            this.btnDong.Location = new System.Drawing.Point(697, 425);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(80, 23);
            this.btnDong.TabIndex = 4;
            this.btnDong.Text = "Đóng";
            this.btnDong.VisualStyleManager = this.vsmMain;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // uiProgressBar1
            // 
            this.uiProgressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uiProgressBar1.Location = new System.Drawing.Point(3, 422);
            this.uiProgressBar1.Name = "uiProgressBar1";
            this.uiProgressBar1.Size = new System.Drawing.Size(256, 23);
            this.uiProgressBar1.TabIndex = 5;
            this.uiProgressBar1.VisualStyleManager = this.vsmMain;
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblError.AutoSize = true;
            this.lblError.BackColor = System.Drawing.Color.Transparent;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(265, 430);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(35, 13);
            this.lblError.TabIndex = 6;
            this.lblError.Text = "label1";
            // 
            // ckbKhongHienThi
            // 
            this.ckbKhongHienThi.BackColor = System.Drawing.Color.Transparent;
            this.ckbKhongHienThi.Location = new System.Drawing.Point(3, 394);
            this.ckbKhongHienThi.Name = "ckbKhongHienThi";
            this.ckbKhongHienThi.Size = new System.Drawing.Size(256, 23);
            this.ckbKhongHienThi.TabIndex = 7;
            this.ckbKhongHienThi.Text = "Hiển thị tờ khai đã cập nhật thanh khoản";
            this.ckbKhongHienThi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.ckbKhongHienThi.CheckedChanged += new System.EventHandler(this.ckbKhongHienThi_CheckedChanged);
            // 
            // chkIsChuaThongQuan
            // 
            this.chkIsChuaThongQuan.BackColor = System.Drawing.Color.Transparent;
            this.chkIsChuaThongQuan.Location = new System.Drawing.Point(268, 394);
            this.chkIsChuaThongQuan.Name = "chkIsChuaThongQuan";
            this.chkIsChuaThongQuan.Size = new System.Drawing.Size(256, 23);
            this.chkIsChuaThongQuan.TabIndex = 7;
            this.chkIsChuaThongQuan.Text = "Hiển thị tờ khai chưa được thông quan";
            this.chkIsChuaThongQuan.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.chkIsChuaThongQuan.CheckedChanged += new System.EventHandler(this.chkIsChuaThongQuan_CheckedChanged);
            // 
            // chkIsTK_AMA
            // 
            this.chkIsTK_AMA.BackColor = System.Drawing.Color.Transparent;
            this.chkIsTK_AMA.Location = new System.Drawing.Point(515, 394);
            this.chkIsTK_AMA.Name = "chkIsTK_AMA";
            this.chkIsTK_AMA.Size = new System.Drawing.Size(256, 23);
            this.chkIsTK_AMA.TabIndex = 7;
            this.chkIsTK_AMA.Text = "Hiển thị tờ khai được khai bổ sung";
            this.chkIsTK_AMA.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.chkIsTK_AMA.CheckedChanged += new System.EventHandler(this.chkIsTK_AMA_CheckedChanged);
            // 
            // VNACCS_ChuyenThanhKhoan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 453);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACCS_ChuyenThanhKhoan";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cập nhật chuyển thanh khoản";
            this.Load += new System.EventHandler(this.VNACCS_ChuyenThanhKhoan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.EditControls.UIButton btnThucHien;
        private Janus.Windows.EditControls.UIButton btnDong;
        private System.Windows.Forms.Label lblError;
        private Janus.Windows.EditControls.UIProgressBar uiProgressBar1;
        private Janus.Windows.EditControls.UICheckBox ckbKhongHienThi;
        private Janus.Windows.EditControls.UICheckBox chkIsChuaThongQuan;
        private Janus.Windows.EditControls.UICheckBox chkIsTK_AMA;
    }
}