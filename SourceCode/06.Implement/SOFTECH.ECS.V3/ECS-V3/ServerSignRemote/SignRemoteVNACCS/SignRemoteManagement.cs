﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SignRemote;
using ServerRemote;
using Janus.Windows.GridEX;
using System.IO;
using IWshRuntimeLibrary;

namespace SignRemoteVNACCS
{
    public partial class SignRemoteManagement : Form
    {
        public ServerLogin server;
        public static bool IsLoginSuccess = false;
        public SignRemoteManagement()
        {
            InitializeComponent();
        }
        private void BindData()
        {
            gridList.Refresh();
            gridList.DataSource = User_DaiLy.SelectAll().Tables[0];
            gridList.Refetch();
        }
        private void SignRemoteManagement_Load(object sender, EventArgs e)
        {
            Login login = new Login();
            login.ShowDialog();
            if (IsLoginSuccess)
            {
                this.Show();
                BindData();
                BindDataLogin();
                CreateShorcut();
            }
            else
            {
                Application.Exit();
            }
            //BindDataInbox();
            //BindDataOutBox();
        }
        private void CreateShorcut()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
        private void BindDataInbox()
        {
            try
            {
                gridInbox.Refresh();
                gridInbox.DataSource = INBOX.SelectAll().Tables[0];
                gridInbox.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }

        }
        private void BindDataOutBox()
        {
            try
            {
                gridOutbox.Refresh();
                gridOutbox.DataSource = OUTBOX.SelectAll().Tables[0];
                gridOutbox.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }

        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string where = "";
                if (txtUserName.Text.Length > 0)
                {
                    where += " USER_NAME LIKE '%" + txtUserName.Text.ToString()+"%'";
                }
                if (txtMaDoanhNghiep.Text.Length > 0)
                {
                    if (where.Length > 0)
                    {
                        where += " AND MaDoanhNghiep LIKE '%" + txtMaDoanhNghiep.Text.ToString() + "%'";
                    }
                    else
                    {
                        where += " MaDoanhNghiep LIKE '%" + txtMaDoanhNghiep.Text.ToString() + "%'";
                    }
                }
                if (txtTenDoanhNghiep.Text.Length > 0)
                {
                    if (where.Length > 0)
                    {
                        where += " AND HO_TEN LIKE '%" + txtTenDoanhNghiep.Text.ToString()+"%'";
                    }
                    else
                    {
                        where += " HO_TEN LIKE '%" + txtTenDoanhNghiep.Text.ToString() +"%'";
                    }
                }
                if (where.Length > 0)
                {
                    gridList.Refresh();
                    gridList.DataSource = User_DaiLy.SelectDynamic(where, null).Tables[0];
                    gridList.Refetch();
                }
                else
                {
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            UserForm f = new UserForm();
            f.ShowDialog(this);
        }

        private void gridList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                User_DaiLy user = User_DaiLy.Load(id);
                UserForm form = new UserForm();
                form.user = user;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }

        private void btnSearchInbox_Click(object sender, EventArgs e)
        {
            try
            {
                string where = "";
                if (txtFrom.Text.Length > 0)
                {
                    where += " MSG_FROM LIKE '%" + txtFrom.Text.ToString() + "%'";
                }
                if (txtTo.Text.Length > 0)
                {
                    if (where.Length > 0)
                    {
                        where += " AND MSG_TO LIKE '%" + txtTo.Text.ToString() + "%'";
                    }
                    else
                    {
                        where += " MSG_TO LIKE '%" + txtTo.Text.ToString() + "%'";
                    }
                }
                if (dtpDateFrom.Value.Year > 0)
                {
                    if (where.Length > 0)
                    {
                        where += " AND CREATE_TIME BETWEEN '" + dtpDateFrom.Value.ToString("yyyy-MM-dd hh:mm:ss") + "' AND '" + dateTo.Value.ToString("yyyy-MM-dd hh:mm:ss") + "'"; 
                    }
                    else
                    {
                        where += "  CREATE_TIME BETWEEN '" + dtpDateFrom.Value.ToString("yyyy-MM-dd hh:mm:ss") + "' AND '" + dateTo.Value.ToString("yyyy-MM-dd hh:mm:ss") + "'"; 
                    }
                }
                gridInbox.Refresh();
                gridInbox.DataSource = INBOX.SelectDynamic(where, null).Tables[0];
                gridInbox.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }

        private void btnSearchOutbox_Click(object sender, EventArgs e)
        {
            try
            {
                string where = "";
                if (txtNguoiGui.Text.Length > 0)
                {
                    where += " MSG_FROM LIKE '%" + txtNguoiGui.Text.ToString() + "%'";
                }
                if (txtNguoiNhan.Text.Length > 0)
                {
                    if (where.Length > 0)
                    {
                        where += " AND MSG_TO LIKE '%" + txtNguoiNhan.Text.ToString() + "%'";
                    }
                    else
                    {
                        where += " MSG_TO LIKE '%" + txtNguoiNhan.Text.ToString() + "%'";
                    }
                }
                if (dateTuNgay.Value.Year > 0)
                {
                    if (where.Length > 0)
                    {
                        where += " AND CREATE_TIME BETWEEN '" + dateTuNgay.Value.ToString("yyyy-MM-dd hh:mm:ss") + "' AND '" + dateDenNgay.Value.ToString("yyyy-MM-dd hh:mm:ss") + "'"; 
                    }
                    else
                    {
                        where += "  CREATE_TIME BETWEEN '" + dateTuNgay.Value.ToString("yyyy-MM-dd hh:mm:ss") + "' AND '" + dateDenNgay.Value.ToString("yyyy-MM-dd hh:mm:ss") + "'"; 
                    }
                }
                gridOutbox.Refresh();
                gridOutbox.DataSource = OUTBOX.SelectDynamic(where, null).Tables[0];
                gridOutbox.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }

        private void gridLogin_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                server = ServerLogin.Load(id);
                txtMaDN.Text = server.MaDoanhNghiep;
                dateWarning.Value = Convert.ToDateTime(server.Warrning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }

        private void BindDataLogin()
        {
            try
            {
                gridLogin.Refetch();
                gridLogin.DataSource = ServerLogin.SelectAll().Tables[0];
                gridLogin.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }
        private void btnSearchLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string where = "";
                if (txtMaDN.Text.Length > 0)
                {
                    where += " MaDoanhNghiep LIKE '%" + txtMaDN.Text.ToString() + "%'";
                }
                //if (dateWarning.DateTime.Year > 0)
                //{
                //    if (where.Length > 0)
                //    {
                //        where += " AND Warrning = '" + dateWarning.DateTime.ToString() + "'";
                //    }
                //    else
                //    {
                //        where += " Warrning = '" + dateWarning.DateTime.ToString() + "'";
                //    }
                //}
                if (where.Length > 0)
                {
                    gridLogin.Refetch();
                    gridLogin.DataSource = ServerLogin.SelectDynamic(where, null).Tables[0];
                    gridLogin.Refresh();
                }
                else
                {
                    BindDataLogin();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }

        }

        private void btnDeleteLogin_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = gridLogin.SelectedItems;
                if (items.Count > 0)
                {
                    if (MessageBoxControl.ShowMessageConfirm("Bạn có muốn xóa các tờ khai này không?") == "Yes")
                    {
                        foreach (GridEXSelectedItem i in items)
                        {
                            if (i.RowType == RowType.Record)
                            {
                                DataRowView dr = (DataRowView)i.GetRow().DataRow;
                                ServerLogin server = new ServerLogin();
                                int id = Convert.ToInt32(dr["ID"].ToString());
                                server.DeleteServerLogin(id);
                            }
                        }
                        MessageBoxControl.ShowMessage("Xóa thành công ");
                    }
                }
                BindDataLogin();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }

        }

        private void btnDeleteUser_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = gridList.SelectedItems;
                if (items.Count > 0)
                {
                    if (MessageBoxControl.ShowMessageConfirm("Bạn có muốn xóa dòng này không ?") == "Yes")
                    {
                        foreach (GridEXSelectedItem i in items)
                        {
                            if (i.RowType == RowType.Record)
                            {
                                DataRowView dr = (DataRowView)i.GetRow().DataRow;
                                User_DaiLy user = new User_DaiLy();
                                int id = Convert.ToInt32(dr["ID"].ToString());
                                user.DeleteUser_DaiLy(id);
                            }
                        }
                        MessageBoxControl.ShowMessage("Xóa thành công ");
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }

        private void btnDeleteInbox_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = gridInbox.SelectedItems;
                if (items.Count > 0)
                {
                    if (MessageBoxControl.ShowMessageConfirm("Bạn có muốn xóa dòng này không?") == "Yes")
                    {
                        foreach (GridEXSelectedItem i in items)
                        {
                            if (i.RowType == RowType.Record)
                            {
                                DataRowView dr = (DataRowView)i.GetRow().DataRow;
                                INBOX inbox = new INBOX();
                                long id = Convert.ToInt64(dr["ID"].ToString());
                                inbox.DeleteINBOX(id);
                            }
                        }
                        MessageBoxControl.ShowMessage("Xóa thành công ");
                    }
                }
                btnSearchInbox_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }

        }

        private void btnDeleteOutbox_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = gridOutbox.SelectedItems;
                if (items.Count > 0)
                {
                    if (MessageBoxControl.ShowMessageConfirm("Bạn có muốn xóa dòng này không?") == "Yes")
                    {
                        foreach (GridEXSelectedItem i in items)
                        {
                            if (i.RowType == RowType.Record)
                            {
                                DataRowView dr = (DataRowView)i.GetRow().DataRow;
                                OUTBOX outbox = new OUTBOX();
                                long id = Convert.ToInt64(dr["ID"].ToString());
                                outbox.DeleteOUTBOX(id);
                            }
                        }
                        MessageBoxControl.ShowMessage("Xóa thành công ");
                    }
                }
                btnSearchOutbox_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }

        private void btnCreateLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (server == null)
                {
                    server = new ServerLogin();
                }
                if (txtMaDN.Text.Length ==0)
                {
                    MessageBoxControl.ShowMessage(" Nhập mã doanh nghiệp ");
                    return;
                }
                server.MaDoanhNghiep = txtMaDN.Text;
                server.Warrning = dateWarning.Value.ToString("yyyy-MM-dd hh:mm:ss");
                server.InsertUpdate();
                MessageBoxControl.ShowMessage("Cập nhật thành công ");
                btnSearchLogin_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.ToString());
            }
        }
    }
}
