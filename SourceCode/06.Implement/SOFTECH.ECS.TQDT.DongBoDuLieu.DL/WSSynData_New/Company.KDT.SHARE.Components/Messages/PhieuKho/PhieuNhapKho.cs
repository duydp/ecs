﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages;

namespace Company.KDT.SHARE.Components.Messages.PhieuKho
{
    [XmlRoot("Declaration")]
    public class PhieuNhapKho : DeclarationBase
    {
        /// <summary>
        /// Loại chứng từ
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { set; get; }
        /// <summary>
        /// Loại Khai Báo
        /// </summary>
        [XmlElement("function")]
        public string Function { set; get; }
        /// <summary>
        /// GUID
        /// </summary>
        [XmlElement("reference")]
        public string Reference { set; get; }
        /// <summary>
        /// issueLocation
        /// </summary>
        [XmlElement("issue")]
        public string Issue { set; get; }
        /// <summary>
        /// issueLocation
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { set; get; }
        /// <summary>
        /// Cơ quan hq
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { set; get; }
        /// <summary>
        /// 1. Đại lý làm thủ tục Hải quan 
        /// 2. Ủy thác 
        /// 3. Người khai hải quan 
        /// 4. Người chịu trách nhiệm nộp thuế
        /// 5. Mã MID
        /// n1
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }
        /// <summary>
        /// customsReference
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { set; get; }
        /// <summary>
        /// acceptance
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { set; get; }
        /// <summary>
        /// Đại lý
        /// </summary>
        [XmlElement("Agent")]
        public List<Agent> Agents { get; set; }
        /// <summary>
        /// Người khai nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { set; get; }
        /// <summary>
        /// Người đại diện doanh nghiệp
        /// </summary>
        [XmlElement("Reference")]
        public ReferencePNK ReferencePNK { set; get; }
        
    }
}
