﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.GC.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using SOFTECH.ECS.V3.Components.SHARE;
namespace SOFTECH.ECS.V3.Components.GC
{//AiNPV
    public  class ToKhaiXuatChuyenTiep
    {
        /// <summary>
        /// Config To Khai Xuat Chuyen Tiep 
        /// </summary>
        /// <param name="tknhap">int</param>
        /// <param name="issuer">int</param>
        /// <param name="function">int</param>
        /// <param name="status">int</param>
        /// <param name="declarationOffice">int</param>
        /// <returns>string</returns>
        public static string ConfigTKXuatChuyenTiep(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkxuat, int issuer, int function, int status, string declarationOffice)
        {
            string doc = "";
            string path = "../../../SOFTECH.ECS.V3.Components/XML/GC/tkxuatChuyenTiep.xml";
            XDocument docXML = XDocument.Load(path);
            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tkxuat != null)
                {
                    XElement GoodsShipment = Root.Element(NodeTKXuatChuyenTiep.GoodsShipment);
                    XElement AdditionalDocument = Root.Element(NodeTKXuatChuyenTiep.AdditionalDocument);
                    XElement CustomsGoodsItem = GoodsShipment.Element(NodeTKXuatChuyenTiep.CustomsGoodsItem);
                    XElement tempCustomsGoodsItem = new XElement(CustomsGoodsItem);
                    bool checkCustomsGoodsItem = true;
                    IList<HangChuyenTiep> lHCTiep = HangChuyenTiep.SelectCollectionBy_Master_ID(tkxuat.ID);
                    CuaKhau cknhap = CuaKhau.Load(tkxuat.MaHaiQuanKH);

                    ToKhaiChuyenTiep.ConfigParent(Root, tkxuat, issuer, function, status, declarationOffice);

                    foreach (XElement element in Root.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.GoodsShipment.ToUpper()))
                        {
                            foreach (XElement GoodsShipmentChild in element.Elements())
                            {
                                if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.importationCountry.ToUpper()))
                                {
                                    GoodsShipmentChild.SetValue("");//>??? Not Found
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.Consignor.ToUpper()))
                                {
                                    foreach (XElement ConsignorChild in GoodsShipmentChild.Elements())
                                    {
                                        if (ConsignorChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.name.ToUpper()))
                                        {
                                            ConsignorChild.SetValue("");//>??? Not Found
                                        }
                                        else if (ConsignorChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.identity.ToUpper()))
                                        {
                                            ConsignorChild.SetValue(tkxuat.MaDoanhNghiep);
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.Consignee.ToUpper()))
                                {
                                    foreach (XElement ConsigneeChild in GoodsShipmentChild.Elements())
                                    {
                                        if (ConsigneeChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.name.ToUpper()))
                                        {
                                            ConsigneeChild.SetValue(tkxuat.TenKH);
                                        }
                                        else if (ConsigneeChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.identity.ToUpper()))
                                        {
                                            ConsigneeChild.SetValue(tkxuat.MaKhachHang);
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.NotifyParty.ToUpper()))
                                {
                                    foreach (XElement NotifyPartyChild in GoodsShipmentChild.Elements())
                                    {
                                        if (NotifyPartyChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.name.ToUpper()))
                                        {
                                            NotifyPartyChild.SetValue(tkxuat.NguoiChiDinhDV);
                                        }
                                        else if (NotifyPartyChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.identity.ToUpper()))
                                        {
                                            NotifyPartyChild.SetValue("");//>??? Not Found
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.DeliveryDestination.ToUpper()))
                                {
                                    foreach (XElement DeliveryDestinationChild in GoodsShipmentChild.Elements())
                                    {
                                        if (DeliveryDestinationChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.line.ToUpper()))
                                        {
                                            DeliveryDestinationChild.SetValue(tkxuat.DiaDiemXepHang);
                                        }
                                        else if (DeliveryDestinationChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.time.ToUpper()))
                                        {
                                            DeliveryDestinationChild.SetValue("");//>??? Not Found
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.EntryCustomsOffice.ToUpper()))
                                {
                                    foreach (XElement EntryCustomsOfficeChild in GoodsShipmentChild.Elements())
                                    {
                                        if (EntryCustomsOfficeChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.code.ToUpper()))
                                        {
                                            EntryCustomsOfficeChild.SetValue(tkxuat.MaHaiQuanKH);
                                            // MaHaiQuanKH
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.ExitCustomsOffice.ToUpper()))
                                {
                                    //MaHaiQuanTiepNhan
                                    foreach (XElement ExitCustomsOfficeChild in GoodsShipmentChild.Elements())
                                    {
                                        if (ExitCustomsOfficeChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.name.ToUpper()))
                                        {
                                            if (cknhap != null && cknhap.Ten != null)
                                            {
                                                ExitCustomsOfficeChild.SetValue(cknhap.Ten);
                                            }
                                        }
                                        else if (ExitCustomsOfficeChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.code.ToUpper()))
                                        {
                                            if (cknhap != null && cknhap.ID != null)
                                            {
                                                ExitCustomsOfficeChild.SetValue(cknhap.ID);
                                            }
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.Exporter.ToUpper()))
                                {
                                    foreach (XElement ExporterChild in GoodsShipmentChild.Elements())
                                    {
                                        if (ExporterChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.name.ToUpper()))
                                        {
                                            ExporterChild.SetValue(tkxuat.TenKH);
                                        }
                                        else if (ExporterChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.identity.ToUpper()))
                                        {
                                            ExporterChild.SetValue(tkxuat.MaKhachHang);
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.TradeTerm.ToUpper()))
                                {
                                    foreach (XElement TradeTermChild in GoodsShipmentChild.Elements())
                                    {
                                        if (TradeTermChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.condition.ToUpper()))
                                        {
                                            TradeTermChild.SetValue("");//??? Not Found
                                        }
                                    }
                                }
                                else if ((GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKXuatChuyenTiep.CustomsGoodsItem.ToUpper())) && checkCustomsGoodsItem == true)
                                {
                                    if (lHCTiep != null)
                                    {
                                        if (lHCTiep.Count == 1)
                                        {
                                            configCustomsGoodsItem(CustomsGoodsItem, lHCTiep[0]);
                                        }
                                        else if (lHCTiep.Count > 1)
                                        {
                                            configCustomsGoodsItem(CustomsGoodsItem, lHCTiep[0]);
                                            for (int i = 1; i < lHCTiep.Count; i++)
                                            {
                                                XElement newCustomsGoodsItem = new XElement(tempCustomsGoodsItem);
                                                configCustomsGoodsItem(newCustomsGoodsItem, lHCTiep[i]);
                                                GoodsShipment.Add(newCustomsGoodsItem);
                                            }
                                        }
                                    }
                                    checkCustomsGoodsItem = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    return doc; //return "";
                }
            }
            docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();
            return doc;
        }
        public static XElement configCustomsGoodsItem(XElement CustomsGoodsItem, HangChuyenTiep hct)
        {
            if (hct != null)
            {
                ToKhaiChuyenTiep tkct = ToKhaiChuyenTiep.Load(hct.Master_ID);
                NguyenTe nt = NguyenTe.Load(tkct.NguyenTe_ID);
                foreach (XElement CustomsGoodsItemChild in CustomsGoodsItem.Elements())
                {
                    if (CustomsGoodsItemChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.customsValue.ToUpper()))
                    {
                        CustomsGoodsItemChild.SetValue(hct.TriGia);
                    }
                    else if (CustomsGoodsItemChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.sequence.ToUpper()))
                    {
                        CustomsGoodsItemChild.SetValue(hct.SoThuTuHang);
                    }
                    else if (CustomsGoodsItemChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.statisticalValue.ToUpper()))
                    {
                        CustomsGoodsItemChild.SetValue(hct.TriGia);
                    }
                    else if (CustomsGoodsItemChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.unitPrice.ToUpper()))
                    {
                        CustomsGoodsItemChild.SetValue(hct.DonGia);
                    }
                    else if (CustomsGoodsItemChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.Manufacturer.ToUpper()))
                    {
                        foreach (XElement ManufacturerChild in CustomsGoodsItemChild.Elements())
                        {
                            if (ManufacturerChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.name.ToUpper()))
                            {
                                ManufacturerChild.SetValue("");//??? Not Found
                            }
                            else if (ManufacturerChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.identity.ToUpper()))
                            {
                                ManufacturerChild.SetValue("");//??? Not Found
                            }
                        }
                    }
                    else if (CustomsGoodsItemChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.Origin.ToUpper()))
                    {
                        foreach (XElement OriginChild in CustomsGoodsItemChild.Elements())
                        {
                            if (OriginChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.originCountry.ToUpper()))
                            {
                                OriginChild.SetValue(hct.ID_NuocXX);
                            }
                        }
                    }
                    else if (CustomsGoodsItemChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.Commodity.ToUpper()))
                    {
                        foreach (XElement CommodityChild in CustomsGoodsItemChild.Elements())
                        {
                            if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.description.ToUpper()))
                            {
                                CommodityChild.SetValue(hct.TenHang);
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.identification.ToUpper()))
                            {
                                CommodityChild.SetValue(hct.MaHang);
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.tariffClassification.ToUpper()))
                            {
                                CommodityChild.SetValue(hct.MaHS);
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.tariffClassificationExtension.ToUpper()))
                            {
                                CommodityChild.SetValue("");
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.brand.ToUpper()))
                            {
                                CommodityChild.SetValue("");
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.grade.ToUpper()))
                            {
                                CommodityChild.SetValue("");
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.ingredients.ToUpper()))
                            {
                                CommodityChild.SetValue("");
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.modelNumber.ToUpper()))
                            {
                                CommodityChild.SetValue("");
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.DutyTaxFee.ToUpper()))
                            {
                                foreach (XElement DutyTaxFeeChild in CommodityChild.Elements())
                                {
                                    if (DutyTaxFeeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.adValoremTaxBase.ToUpper()))
                                    {
                                        DutyTaxFeeChild.SetValue("");//??? Not Found
                                    }
                                    else if (DutyTaxFeeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.tax.ToUpper()))
                                    {
                                        DutyTaxFeeChild.SetValue("");//??? Not Found
                                    }
                                    else if (DutyTaxFeeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.type.ToUpper()))
                                    {
                                        DutyTaxFeeChild.SetValue("");//??? Not Found
                                    }
                                }
                            }
                            else if (CommodityChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.InvoiceLine.ToUpper()))
                            {
                                foreach (XElement InvoiceLineChild in CommodityChild.Elements())
                                {
                                    if (InvoiceLineChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.itemCharge.ToUpper()))
                                    {
                                        InvoiceLineChild.SetValue("");//??? Not Found
                                    }
                                    else if (InvoiceLineChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.line.ToUpper()))
                                    {
                                        InvoiceLineChild.SetValue("");//??? Not Found
                                    }
                                }
                            }
                        }
                    }
                    else if (CustomsGoodsItemChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.GoodsMeasure.ToUpper()))
                    {
                        foreach (XElement GoodsMeasureChild in CustomsGoodsItemChild.Elements())
                        {
                            if (GoodsMeasureChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.tariff.ToUpper()))
                            {
                                GoodsMeasureChild.SetValue(hct.SoLuong);// SoLuong
                            }
                            else if (GoodsMeasureChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.measureUnit.ToUpper()))
                            {
                                GoodsMeasureChild.SetValue(hct.ID_DVT);//Madonvitinh
                            }
                            else if (GoodsMeasureChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.conversionRate.ToUpper()))
                            {
                                GoodsMeasureChild.SetValue("");//
                            }
                        }
                    }
                    else if (CustomsGoodsItemChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.CustomsValuation.ToUpper()))
                    {
                        foreach (XElement CustomsValuationeChild in CustomsGoodsItemChild.Elements())
                        {
                            if (CustomsValuationeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.exitToEntryCharge.ToUpper()))
                            {
                                CustomsValuationeChild.SetValue("");//??? Not Found
                            }
                            else if (CustomsValuationeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.freightCharge.ToUpper()))
                            {
                                CustomsValuationeChild.SetValue("");//??? Not Found
                            }
                            else if (CustomsValuationeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.Method.ToUpper()))
                            {
                                CustomsValuationeChild.SetValue("");//??? Not Found
                            }
                            else if (CustomsValuationeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.otherChargeDeduction.ToUpper()))
                            {
                                CustomsValuationeChild.SetValue("");//??? Not Found
                            }
                        }
                    }
                }
            }
            return CustomsGoodsItem;
        }
    }
}
