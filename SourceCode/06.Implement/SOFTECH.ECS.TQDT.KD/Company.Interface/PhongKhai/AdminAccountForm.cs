﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Company.KD.BLL.DuLieuChuan ;
using System.Security.Cryptography;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL.KDT;


namespace Company.Interface.PhongKhai
{
    public partial class AdminAccountForm : Company.Interface.BaseForm
    {
        private HeThongPhongKhai hqch = new HeThongPhongKhai();
        HeThongPhongKhaiCollection hqpkCollection;
        private DataRowCollection dtb;
        private int tempdel = 0;
        private string _PassTemp;
        public AdminAccountForm()
        {
            InitializeComponent();
        }
        public bool IsRegister;
        private long t =0;
       // private DataRowCollection dtb;
        private HeThongPhongKhai haiquanpkch = new HeThongPhongKhai();


        private void Register(string strMa, string strTen, string strPass)
        {

            try
            {
                strMa = txtMaDoanhNghiep.Text.Trim();
                strTen = txtTenDoanhNghiep.Text.Trim();
                strPass = this.GetMD5Value(txtRePass.Text.Trim());
                //Tên doanh Nghiệp
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "CauHinh";
                haiquanpkch.Value_Config = "1";
                haiquanpkch.Role = 1; //quyền doanh nghiệp
                t = haiquanpkch.Insert();
                dgList.DataSource = haiquanpkch.SelectCollectionByRole();
                dgList.Refetch();
                tempdel = haiquanpkch.SelectCollectionByRole().Count;
            }
            catch
            {
                ShowMessage("Phát hiện lỗi trong quá trình tạo tài khoản,vui lòng kiểm tra lại", false);
            }

        }
        
        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }

        //
        private void ShowLoginForm()
        {         
            LoginForm loginForm = new LoginForm();            
            loginForm.ShowDialog();
        }
  
        private void btnCancel_Click(object sender, EventArgs e)
        {           
                this.Close();
                      
        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void DangKyForm_Load(object sender, EventArgs e)
        {
            //txtRePass.Visible = false ;            
            btnEditAccount.Enabled = false;
            btnDelAccount.Enabled = true;
            btnAddAdmin.Enabled = true;             
            try
            {
                hqpkCollection = new HeThongPhongKhai().SelectCollectionByRole();
                dgList.DataSource = hqpkCollection;
                tempdel = hqpkCollection.Count;
            }
            catch(Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
          
           
        }

        private void DangKyForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            
           // 
        }

        

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            btnAddAdmin.Enabled = false;
            btnDelAccount.Enabled = false;
            btnEditAccount.Enabled = true;
            txtMaDoanhNghiep.ReadOnly = true;
            HeThongPhongKhai hqpkcauhinh = new HeThongPhongKhai() ;
            GridEXSelectedItemCollection gridEXSelectedCollection = dgList.SelectedItems;;
            foreach ( GridEXSelectedItem gridEXSelectedItem in gridEXSelectedCollection)
            {
                if (gridEXSelectedItem.RowType == RowType.Record)
                {
                    hqpkcauhinh = (HeThongPhongKhai )dgList.GetRow().DataRow ;
                    txtMaDoanhNghiep.Text = hqpkcauhinh.MaDoanhNghiep.Trim();
                    _PassTemp = hqpkcauhinh.PassWord;
                    //txtPass.Text = hqpkcauhinh.PassWord;
                    txtTenDoanhNghiep.Text = hqpkcauhinh.TenDoanhNghiep.Trim();
                }
            }
            
        }

        private void btnEditAccount_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;

            string strPassPre = txtPass.Text.Trim();
            string strPass = this.GetMD5Value(txtPass.Text.Trim());
            string strRepass = txtRePass.Text;
            txtMaDoanhNghiep.ReadOnly = false;
            //Kiểm tra xem mật khẩu có nhập không
            if (txtPass.Text.Trim().Length > 0)
            {
                strPass = this.GetMD5Value(txtPass.Text.Trim());
                strRepass = this.GetMD5Value(txtRePass.Text.Trim());
                if (strPass != strRepass)
                {
                    //MessageBox.Show("Mật khẩu không trùng khớp, yêu cầu kiểm tra lại !");
                    ShowMessage("Mật khẩu chưa trùng khớp ", false);
                    return;
                }
            }      
            btnEditAccount.Enabled = false;           
            btnEditAccount.Enabled = true;
            btnDelAccount.Enabled = true;
            btnAddAdmin.Enabled = true;
            HeThongPhongKhai hqpk = new HeThongPhongKhai();

            try
            {
                hqpk.MaDoanhNghiep = txtMaDoanhNghiep.Text;
                hqpk.TenDoanhNghiep = txtTenDoanhNghiep.Text;
                hqpk.Key_Config = "CauHinh";
                hqpk.Value_Config = "1";
                if (txtPass.Text.Trim().Length > 0)
                    hqpk.PassWord = strPass;
                else
                    hqpk.PassWord = _PassTemp;
                hqpk.Role = 1;
                int t = hqpk.Update();
                if (t > 0)
                {
                    ShowMessage("Tài khoản đã được cập nhật!", false);
                    btnEditAccount.Enabled = false;
                }
                else
                    ShowMessage("Cập nhật thông tin tài khoản không thành công!", false);

                dgList.DataSource = haiquanpkch.SelectCollectionByRole();
                dgList.Refetch();                
            }
            catch (Exception ex1)
            {
                ShowMessage(" Lỗi : " + ex1.Message, false);
            }
            txtMaDoanhNghiep.Text = "";
            txtTenDoanhNghiep.Text = "";
            txtPass.Text = "";
            txtRePass.Text = "";
        }

        private void btnAddAdmin_Click(object sender, EventArgs e)
        {

            cvError.Validate();
            if (!cvError.IsValid) return;

            txtRePass.Visible = true;
            label1.Visible = true;
            btnAddAdmin.Enabled = false;
            //btnEditAccount.Enabled = true;
            btnDelAccount.Enabled = true;
            string strMa = txtMaDoanhNghiep.Text.Trim();
            string strTen = txtTenDoanhNghiep.Text.Trim();
            string strPass = txtPass.Text.Trim();
            string strRePass = txtRePass.Text.Trim();
            if (strPass.Length > 0)
            {
                if (strPass != strRePass)
                {
                    ShowMessage("Mật khẩu chưa trùng khớp !", false);
                    btnAddAdmin.Enabled = true;                   
                    return;
                }
            }
            else
            {
                ShowMessage("Mật khẩu không được rỗng !", false);
                btnAddAdmin.Enabled = true;               
                return;
            }
            try
            {
                dtb = hqch.SelectLogin().Tables[0].Rows;
                string strMaRow = "";
               // string strPassRow = "";
               // int intRole = 0;
                foreach (DataRow dr in dtb)
                {
                    strMaRow = dr["MadoanhNghiep"].ToString().Trim();
                    //strPassRow = dr["PassWord"].ToString().Trim();
                    if (strMa == strMaRow )
                    {
                        ShowMessage("Tài khoản Administrator này đã được đăng ký !", false);
                        btnAddAdmin.Enabled = true;
                        //btnEditAccount.Enabled = false;
                        return;

                    }
                    
                }
                               
            }
            catch
            {

                ShowMessage("lỗi!", false);
                btnAddAdmin.Enabled = true;
                //btnEditAccount.Enabled = false;
                return;
            }
            cvError.Validate();
            if (!cvError.IsValid)
            {
                btnAddAdmin.Enabled = true;
                btnEditAccount.Enabled = false;
                return;
            }
            this.Cursor = Cursors.WaitCursor;
           //btnAddAdmin.Enabled 
            this.Register(strMa, strTen, strPass);
            if (t > 0)
            {
                 ShowMessage("Tài khoản của Administrator đã được tạo!", false);
                 txtMaDoanhNghiep.Text = "";
                 txtPass.Text = "";
                 txtTenDoanhNghiep.Text = "";
                 txtRePass.Text = "";
            }
            else
            {
                dgList.Refresh();
                ShowMessage("Tạo Tài khoản không thành công !", false);
                //return;

            }
            this.Cursor = Cursors.Arrow ;
           // this.btnCreatAccount.Enabled = true ;
            btnAddAdmin.Enabled = true;
            
        
        }

        private void btnDelAccount_Click(object sender, EventArgs e)
        {
            this.DeleteAcount();

            
        }
       
        private void DeleteAcount()
        {
            if (ShowMessage("Bạn có chắc chắn là xóa tài khoản này không ?", true) == "Yes")
            {
                //count record number 
                // if record number is greater than 1 : done
                //else : none
             if (tempdel > 1)
               {    
                  tempdel--;        
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                int count = 0;
                int t = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        try
                        {
                            HeThongPhongKhai hqchpk = (HeThongPhongKhai)i.GetRow().DataRow;
                             //t = hqchpk.Delete();
                            t = hqchpk.DeleteALL();
                            count++;
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi : " + ex.Message, false);
                        }
                    }

                }
                if (t > 0)
                    ShowMessage(count.ToString() + " Tài khoản đã được xóa ", false);
                else
                    ShowMessage("Không xóa được bản ghi nào cả", false);
            }
            else
                {
                    ShowMessage("Đây là tài khoản Admin duy nhất,nếu xóa tài khoản này bạn sẽ không thể đăng nhập vào hệ thống lần sau, Bạn phải tạo thêm tài khoản Admin trước khi xóa tài khoản này. ", false);
                }
        }
        
            dgList.DataSource = haiquanpkch.SelectCollectionByRole();
            dgList.Refetch();
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            this.DeleteAcount();
        }

        private void btnDongBo_Click(object sender, EventArgs e)
        {
            //DongBoDuLieuForm dongbodulieuForm = new DongBoDuLieuForm();
            //dongbodulieuForm.ShowDialog();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            CreatAccountForm cf = new CreatAccountForm();
            cf.ShowDialog();
        }
    }
}

