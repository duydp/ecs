﻿using System;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace CA_QuanLyGCN
{
    public class NganhNgheCollections : CollectionBase
    {
        #region Constructor

        public NganhNgheCollections() { }
        #endregion Constructor

        #region Indexer Methods

        public NganhNghe this[int index]
        {
            get { return (NganhNghe)List[index]; }
            set { List[index] = value; }
        }

        #endregion Indexer Methods

        #region Private Methods

        public int Add(NganhNghe value)
        {
            return List.Add(value);
        }

        public void Insert(int index, NganhNghe value)
        {
            List.Insert(index, value);
        }

        public void Remove(NganhNghe value)
        {
            List.Remove(value);
        }

        public bool Contains(string id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                NganhNghe NganhNgheEntity = (NganhNghe)List[i];
                if (NganhNgheEntity.Id.Equals(id) && (NganhNgheEntity.EntityState != EntityState.Deleted))
                {
                    return true;
                }
            }
            return false;
        }

        public NganhNghe SearchById(int id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                NganhNghe NganhNgheEntity = (NganhNghe)List[i];
                if ((NganhNgheEntity.Id.Equals(id)) && (NganhNgheEntity.EntityState != EntityState.Deleted))
                {
                    return NganhNgheEntity;
                }
            }
            return null;
        }
/*
        public NganhNghe SearchByTen(string name)
        {
            for (int i = 0; i < List.Count; i++)
            {
                NganhNghe NganhNgheEntity = (NganhNghe)List[i];
                if ((NganhNgheEntity.Name.ToLower().Trim().Equals(name.ToLower().Trim())) && (NganhNgheEntity.EntityState != EntityState.Deleted))
                {
                    return NganhNgheEntity;
                }
            }
            return null;
        }
*/
        public void Save()
        {
            for (int i = 0; i < List.Count; i++)
            {
                NganhNghe NganhNgheEntity = (NganhNghe)List[i];
                switch (NganhNgheEntity.EntityState)
                {
                    case EntityState.Deleted:
                        NganhNgheEntity.Delete();
                        break;
                    case EntityState.Added:
                        NganhNgheEntity.Insert();
                        break;
                    case EntityState.Updated:
                        NganhNgheEntity.Update();
                        break;
                }
                NganhNgheEntity.EntityState = EntityState.Unchanged;
            }

        }

        #endregion Private Methods
						
		#region Clone Method

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public NganhNgheCollections Clone()
        {
            NganhNgheCollections _tmpCollections = new NganhNgheCollections();

            foreach (NganhNghe item in this)
            {
                _tmpCollections.Add((NganhNghe)item.Clone());
            }

            return _tmpCollections;
        }

        #endregion

    }
}

