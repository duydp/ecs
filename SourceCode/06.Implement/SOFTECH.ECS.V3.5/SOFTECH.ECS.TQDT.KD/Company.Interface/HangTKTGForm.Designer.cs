﻿namespace Company.Interface
{
    partial class HangTKTGForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HangTKTGForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDonGiaTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGiaNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtThietKe = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtCongCu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtGiamGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtVatLieu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtNguyenLieu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTienLai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTroGiup = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtChiPhiPhatSinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtChiPhiDongGoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPhiNoiDia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtChiPhiBaoBi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtChiPhiVanChuyen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTraTruoc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtHoaHong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtbanQuyen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtGiaGhiTrenHD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTienTraSuDung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtKhoanTTGianTiep = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.gbGiayPhep = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).BeginInit();
            this.gbGiayPhep.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(775, 455);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.gbGiayPhep);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(775, 455);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnClose.Location = new System.Drawing.Point(694, 423);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(613, 423);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 7;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtDonGiaTT);
            this.uiGroupBox4.Controls.Add(this.txtDonGiaNT);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.label24);
            this.uiGroupBox4.Controls.Add(this.txtThietKe);
            this.uiGroupBox4.Controls.Add(this.txtCongCu);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Controls.Add(this.txtGiamGia);
            this.uiGroupBox4.Controls.Add(this.txtVatLieu);
            this.uiGroupBox4.Controls.Add(this.txtTienThue);
            this.uiGroupBox4.Controls.Add(this.txtNguyenLieu);
            this.uiGroupBox4.Controls.Add(this.label17);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.label19);
            this.uiGroupBox4.Controls.Add(this.label20);
            this.uiGroupBox4.Controls.Add(this.txtTienLai);
            this.uiGroupBox4.Controls.Add(this.txtTroGiup);
            this.uiGroupBox4.Controls.Add(this.label15);
            this.uiGroupBox4.Controls.Add(this.label16);
            this.uiGroupBox4.Controls.Add(this.txtChiPhiPhatSinh);
            this.uiGroupBox4.Controls.Add(this.txtChiPhiDongGoi);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.txtPhiNoiDia);
            this.uiGroupBox4.Controls.Add(this.txtChiPhiBaoBi);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.txtChiPhiVanChuyen);
            this.uiGroupBox4.Controls.Add(this.txtTraTruoc);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.txtPhiBaoHiem);
            this.uiGroupBox4.Controls.Add(this.txtHoaHong);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtbanQuyen);
            this.uiGroupBox4.Controls.Add(this.txtGiaGhiTrenHD);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Controls.Add(this.txtTienTraSuDung);
            this.uiGroupBox4.Controls.Add(this.txtKhoanTTGianTiep);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 75);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(758, 342);
            this.uiGroupBox4.TabIndex = 2;
            this.uiGroupBox4.Text = "Chi tiết";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtDonGiaTT
            // 
            this.txtDonGiaTT.DecimalDigits = 0;
            this.txtDonGiaTT.Location = new System.Drawing.Point(516, 288);
            this.txtDonGiaTT.Name = "txtDonGiaTT";
            this.txtDonGiaTT.Size = new System.Drawing.Size(235, 21);
            this.txtDonGiaTT.TabIndex = 24;
            this.txtDonGiaTT.Text = "0";
            this.txtDonGiaTT.Value = 0;
            this.txtDonGiaTT.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDonGiaTT.VisualStyleManager = this.vsmMain;
            // 
            // txtDonGiaNT
            // 
            this.txtDonGiaNT.DecimalDigits = 8;
            this.txtDonGiaNT.Location = new System.Drawing.Point(516, 261);
            this.txtDonGiaNT.Name = "txtDonGiaNT";
            this.txtDonGiaNT.ReadOnly = true;
            this.txtDonGiaNT.Size = new System.Drawing.Size(235, 21);
            this.txtDonGiaNT.TabIndex = 23;
            this.txtDonGiaNT.Text = "0.00000000";
            this.txtDonGiaNT.Value = 0;
            this.txtDonGiaNT.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDonGiaNT.VisualStyleManager = this.vsmMain;
            this.txtDonGiaNT.Leave += new System.EventHandler(this.txtDonGiaNT_Leave);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(364, 292);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(134, 21);
            this.label23.TabIndex = 49;
            this.label23.Text = "24. Trị giá tính thuế VNĐ";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(364, 265);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(134, 27);
            this.label24.TabIndex = 48;
            this.label24.Text = "23. Trị giá tính hải quan";
            // 
            // txtThietKe
            // 
            this.txtThietKe.DecimalDigits = 8;
            this.txtThietKe.Location = new System.Drawing.Point(229, 288);
            this.txtThietKe.Name = "txtThietKe";
            this.txtThietKe.Size = new System.Drawing.Size(110, 21);
            this.txtThietKe.TabIndex = 13;
            this.txtThietKe.Text = "0.00000000";
            this.txtThietKe.Value = 0;
            this.txtThietKe.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtThietKe.VisualStyleManager = this.vsmMain;
            this.txtThietKe.Leave += new System.EventHandler(this.txtNguyenLieu_Leave);
            // 
            // txtCongCu
            // 
            this.txtCongCu.DecimalDigits = 8;
            this.txtCongCu.Location = new System.Drawing.Point(229, 261);
            this.txtCongCu.Name = "txtCongCu";
            this.txtCongCu.Size = new System.Drawing.Size(110, 21);
            this.txtCongCu.TabIndex = 12;
            this.txtCongCu.Text = "0.00000000";
            this.txtCongCu.Value = 0;
            this.txtCongCu.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtCongCu.VisualStyleManager = this.vsmMain;
            this.txtCongCu.Leave += new System.EventHandler(this.txtNguyenLieu_Leave);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(6, 292);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(217, 44);
            this.label21.TabIndex = 45;
            this.label21.Text = "d. Bản vẽ thiết kế/kỹ thuật/triển khai, thiết kế mỹ thuật/thi công/mẫu, sơ đồ, ph" +
                "ác thảo, sản phẩm và dịch vụ tương ứng";
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(6, 260);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(217, 27);
            this.label22.TabIndex = 44;
            this.label22.Text = "c. Công cụ, dụng cụ, khuôn rập, khuôn đúc, khuôn mẫu chi tiết tương tự";
            // 
            // txtGiamGia
            // 
            this.txtGiamGia.DecimalDigits = 8;
            this.txtGiamGia.Location = new System.Drawing.Point(637, 234);
            this.txtGiamGia.Name = "txtGiamGia";
            this.txtGiamGia.Size = new System.Drawing.Size(114, 21);
            this.txtGiamGia.TabIndex = 22;
            this.txtGiamGia.Text = "0.00000000";
            this.txtGiamGia.Value = 0;
            this.txtGiamGia.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtGiamGia.VisualStyleManager = this.vsmMain;
            this.txtGiamGia.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // txtVatLieu
            // 
            this.txtVatLieu.DecimalDigits = 8;
            this.txtVatLieu.Location = new System.Drawing.Point(229, 234);
            this.txtVatLieu.Name = "txtVatLieu";
            this.txtVatLieu.Size = new System.Drawing.Size(110, 21);
            this.txtVatLieu.TabIndex = 11;
            this.txtVatLieu.Text = "0.00000000";
            this.txtVatLieu.Value = 0;
            this.txtVatLieu.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtVatLieu.VisualStyleManager = this.vsmMain;
            this.txtVatLieu.Leave += new System.EventHandler(this.txtNguyenLieu_Leave);
            // 
            // txtTienThue
            // 
            this.txtTienThue.DecimalDigits = 8;
            this.txtTienThue.Location = new System.Drawing.Point(637, 207);
            this.txtTienThue.Name = "txtTienThue";
            this.txtTienThue.Size = new System.Drawing.Size(114, 21);
            this.txtTienThue.TabIndex = 21;
            this.txtTienThue.Text = "0.00000000";
            this.txtTienThue.Value = 0;
            this.txtTienThue.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTienThue.VisualStyleManager = this.vsmMain;
            this.txtTienThue.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // txtNguyenLieu
            // 
            this.txtNguyenLieu.DecimalDigits = 8;
            this.txtNguyenLieu.Location = new System.Drawing.Point(229, 207);
            this.txtNguyenLieu.Name = "txtNguyenLieu";
            this.txtNguyenLieu.Size = new System.Drawing.Size(110, 21);
            this.txtNguyenLieu.TabIndex = 10;
            this.txtNguyenLieu.Text = "0.00000000";
            this.txtNguyenLieu.Value = 0;
            this.txtNguyenLieu.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtNguyenLieu.VisualStyleManager = this.vsmMain;
            this.txtNguyenLieu.Leave += new System.EventHandler(this.txtNguyenLieu_Leave);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(364, 238);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(230, 24);
            this.label17.TabIndex = 39;
            this.label17.Text = "22. Khoản giảm giá";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 238);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(217, 17);
            this.label18.TabIndex = 38;
            this.label18.Text = "b. Vật liệu, nhiên liệu, năng lượng tiêu hao";
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(364, 211);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(230, 20);
            this.label19.TabIndex = 37;
            this.label19.Text = "21. Các khoản thuế, phí lệ phí phải trả";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(6, 207);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(217, 27);
            this.label20.TabIndex = 36;
            this.label20.Text = "a. Nguyên vật liệu, bộ phận cấu thành, phụ tùng, chi tiết tương tự";
            // 
            // txtTienLai
            // 
            this.txtTienLai.DecimalDigits = 8;
            this.txtTienLai.Location = new System.Drawing.Point(637, 180);
            this.txtTienLai.Name = "txtTienLai";
            this.txtTienLai.Size = new System.Drawing.Size(114, 21);
            this.txtTienLai.TabIndex = 20;
            this.txtTienLai.Text = "0.00000000";
            this.txtTienLai.Value = 0;
            this.txtTienLai.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTienLai.VisualStyleManager = this.vsmMain;
            this.txtTienLai.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // txtTroGiup
            // 
            this.txtTroGiup.DecimalDigits = 8;
            this.txtTroGiup.Location = new System.Drawing.Point(229, 180);
            this.txtTroGiup.Name = "txtTroGiup";
            this.txtTroGiup.ReadOnly = true;
            this.txtTroGiup.Size = new System.Drawing.Size(110, 21);
            this.txtTroGiup.TabIndex = 9;
            this.txtTroGiup.Text = "0.00000000";
            this.txtTroGiup.Value = 0;
            this.txtTroGiup.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTroGiup.VisualStyleManager = this.vsmMain;
            this.txtTroGiup.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(364, 184);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(263, 28);
            this.label15.TabIndex = 33;
            this.label15.Text = "20. Tiền lãi phải trả cho việc thanh toán tiền hàng";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 178);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(217, 29);
            this.label16.TabIndex = 32;
            this.label16.Text = "13. Các khoản trợ giúp người mua cung cấp miễn phí hoặc giảm giá";
            // 
            // txtChiPhiPhatSinh
            // 
            this.txtChiPhiPhatSinh.DecimalDigits = 8;
            this.txtChiPhiPhatSinh.Location = new System.Drawing.Point(637, 153);
            this.txtChiPhiPhatSinh.Name = "txtChiPhiPhatSinh";
            this.txtChiPhiPhatSinh.Size = new System.Drawing.Size(114, 21);
            this.txtChiPhiPhatSinh.TabIndex = 19;
            this.txtChiPhiPhatSinh.Text = "0.00000000";
            this.txtChiPhiPhatSinh.Value = 0;
            this.txtChiPhiPhatSinh.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtChiPhiPhatSinh.VisualStyleManager = this.vsmMain;
            this.txtChiPhiPhatSinh.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // txtChiPhiDongGoi
            // 
            this.txtChiPhiDongGoi.DecimalDigits = 8;
            this.txtChiPhiDongGoi.Location = new System.Drawing.Point(229, 153);
            this.txtChiPhiDongGoi.Name = "txtChiPhiDongGoi";
            this.txtChiPhiDongGoi.Size = new System.Drawing.Size(110, 21);
            this.txtChiPhiDongGoi.TabIndex = 8;
            this.txtChiPhiDongGoi.Text = "0.00000000";
            this.txtChiPhiDongGoi.Value = 0;
            this.txtChiPhiDongGoi.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtChiPhiDongGoi.VisualStyleManager = this.vsmMain;
            this.txtChiPhiDongGoi.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(364, 157);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(230, 20);
            this.label13.TabIndex = 29;
            this.label13.Text = "19. Chi phí phát sinh sau khi nhập khẩu";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 157);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(217, 17);
            this.label14.TabIndex = 28;
            this.label14.Text = "12. Chi phí đóng gói";
            // 
            // txtPhiNoiDia
            // 
            this.txtPhiNoiDia.DecimalDigits = 8;
            this.txtPhiNoiDia.Location = new System.Drawing.Point(637, 126);
            this.txtPhiNoiDia.Name = "txtPhiNoiDia";
            this.txtPhiNoiDia.Size = new System.Drawing.Size(114, 21);
            this.txtPhiNoiDia.TabIndex = 18;
            this.txtPhiNoiDia.Text = "0.00000000";
            this.txtPhiNoiDia.Value = 0;
            this.txtPhiNoiDia.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtPhiNoiDia.VisualStyleManager = this.vsmMain;
            this.txtPhiNoiDia.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // txtChiPhiBaoBi
            // 
            this.txtChiPhiBaoBi.DecimalDigits = 8;
            this.txtChiPhiBaoBi.Location = new System.Drawing.Point(229, 126);
            this.txtChiPhiBaoBi.Name = "txtChiPhiBaoBi";
            this.txtChiPhiBaoBi.Size = new System.Drawing.Size(110, 21);
            this.txtChiPhiBaoBi.TabIndex = 7;
            this.txtChiPhiBaoBi.Text = "0.00000000";
            this.txtChiPhiBaoBi.Value = 0;
            this.txtChiPhiBaoBi.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtChiPhiBaoBi.VisualStyleManager = this.vsmMain;
            this.txtChiPhiBaoBi.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(364, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(230, 21);
            this.label10.TabIndex = 25;
            this.label10.Text = "18. Phí BH, VT hàng hóa trong nội địa";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 130);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(217, 17);
            this.label12.TabIndex = 24;
            this.label12.Text = "11. Chi phí bao bì gắn với hàng hóa";
            // 
            // txtChiPhiVanChuyen
            // 
            this.txtChiPhiVanChuyen.DecimalDigits = 8;
            this.txtChiPhiVanChuyen.Location = new System.Drawing.Point(637, 72);
            this.txtChiPhiVanChuyen.Name = "txtChiPhiVanChuyen";
            this.txtChiPhiVanChuyen.Size = new System.Drawing.Size(114, 21);
            this.txtChiPhiVanChuyen.TabIndex = 16;
            this.txtChiPhiVanChuyen.Text = "0.00000000";
            this.txtChiPhiVanChuyen.Value = 0;
            this.txtChiPhiVanChuyen.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtChiPhiVanChuyen.VisualStyleManager = this.vsmMain;
            this.txtChiPhiVanChuyen.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // txtTraTruoc
            // 
            this.txtTraTruoc.DecimalDigits = 8;
            this.txtTraTruoc.Location = new System.Drawing.Point(229, 72);
            this.txtTraTruoc.Name = "txtTraTruoc";
            this.txtTraTruoc.Size = new System.Drawing.Size(110, 21);
            this.txtTraTruoc.TabIndex = 5;
            this.txtTraTruoc.Text = "0.00000000";
            this.txtTraTruoc.Value = 0;
            this.txtTraTruoc.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTraTruoc.VisualStyleManager = this.vsmMain;
            this.txtTraTruoc.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(364, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(230, 17);
            this.label5.TabIndex = 21;
            this.label5.Text = "16. Chi phí vận tải, bốc xếp, chuyển hàng";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(217, 21);
            this.label7.TabIndex = 4;
            this.label7.Text = "9. Khoản tiền trả trước, ứng trước, đặt cọc";
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.DecimalDigits = 8;
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(637, 98);
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(114, 21);
            this.txtPhiBaoHiem.TabIndex = 17;
            this.txtPhiBaoHiem.Text = "0.00000000";
            this.txtPhiBaoHiem.Value = 0;
            this.txtPhiBaoHiem.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtPhiBaoHiem.VisualStyleManager = this.vsmMain;
            this.txtPhiBaoHiem.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // txtHoaHong
            // 
            this.txtHoaHong.DecimalDigits = 8;
            this.txtHoaHong.Location = new System.Drawing.Point(229, 99);
            this.txtHoaHong.Name = "txtHoaHong";
            this.txtHoaHong.Size = new System.Drawing.Size(110, 21);
            this.txtHoaHong.TabIndex = 6;
            this.txtHoaHong.Text = "0.00000000";
            this.txtHoaHong.Value = 0;
            this.txtHoaHong.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtHoaHong.VisualStyleManager = this.vsmMain;
            this.txtHoaHong.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(364, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(230, 21);
            this.label8.TabIndex = 17;
            this.label8.Text = "17. Chi phí bảo hiểm hàng hóa";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(217, 22);
            this.label9.TabIndex = 16;
            this.label9.Text = "10. Chi phí hoa hồng bán hàng/phí môi giới";
            // 
            // txtbanQuyen
            // 
            this.txtbanQuyen.DecimalDigits = 8;
            this.txtbanQuyen.Location = new System.Drawing.Point(637, 18);
            this.txtbanQuyen.Name = "txtbanQuyen";
            this.txtbanQuyen.Size = new System.Drawing.Size(114, 21);
            this.txtbanQuyen.TabIndex = 14;
            this.txtbanQuyen.Text = "0.00000000";
            this.txtbanQuyen.Value = 0;
            this.txtbanQuyen.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtbanQuyen.VisualStyleManager = this.vsmMain;
            this.txtbanQuyen.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // txtGiaGhiTrenHD
            // 
            this.txtGiaGhiTrenHD.DecimalDigits = 8;
            this.txtGiaGhiTrenHD.Location = new System.Drawing.Point(229, 18);
            this.txtGiaGhiTrenHD.Name = "txtGiaGhiTrenHD";
            this.txtGiaGhiTrenHD.Size = new System.Drawing.Size(110, 21);
            this.txtGiaGhiTrenHD.TabIndex = 1;
            this.txtGiaGhiTrenHD.Text = "0.00000000";
            this.txtGiaGhiTrenHD.Value = 0;
            this.txtGiaGhiTrenHD.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtGiaGhiTrenHD.VisualStyleManager = this.vsmMain;
            this.txtGiaGhiTrenHD.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(364, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "14. Tiền bản quyền, phí giấy phép";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "7. Giá mua ghi trên hóa đơn";
            // 
            // txtTienTraSuDung
            // 
            this.txtTienTraSuDung.DecimalDigits = 8;
            this.txtTienTraSuDung.Location = new System.Drawing.Point(637, 45);
            this.txtTienTraSuDung.Name = "txtTienTraSuDung";
            this.txtTienTraSuDung.Size = new System.Drawing.Size(114, 21);
            this.txtTienTraSuDung.TabIndex = 15;
            this.txtTienTraSuDung.Text = "0.00000000";
            this.txtTienTraSuDung.Value = 0;
            this.txtTienTraSuDung.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTienTraSuDung.VisualStyleManager = this.vsmMain;
            this.txtTienTraSuDung.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // txtKhoanTTGianTiep
            // 
            this.txtKhoanTTGianTiep.DecimalDigits = 8;
            this.txtKhoanTTGianTiep.Location = new System.Drawing.Point(229, 45);
            this.txtKhoanTTGianTiep.Name = "txtKhoanTTGianTiep";
            this.txtKhoanTTGianTiep.Size = new System.Drawing.Size(110, 21);
            this.txtKhoanTTGianTiep.TabIndex = 3;
            this.txtKhoanTTGianTiep.Text = "0.00000000";
            this.txtKhoanTTGianTiep.Value = 0;
            this.txtKhoanTTGianTiep.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtKhoanTTGianTiep.VisualStyleManager = this.vsmMain;
            this.txtKhoanTTGianTiep.Leave += new System.EventHandler(this.txtGiaGhiTrenHD_Leave);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(364, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(263, 28);
            this.label3.TabIndex = 9;
            this.label3.Text = "15. Tiền thu được phải trả sau khi định đoạt, sử dụng hàng hóa";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(217, 21);
            this.label4.TabIndex = 2;
            this.label4.Text = "8. Khoản thanh toán gián tiếp";
            // 
            // gbGiayPhep
            // 
            this.gbGiayPhep.BackColor = System.Drawing.Color.Transparent;
            this.gbGiayPhep.Controls.Add(this.txtTenHang);
            this.gbGiayPhep.Controls.Add(this.label6);
            this.gbGiayPhep.Controls.Add(this.txtSTT);
            this.gbGiayPhep.Controls.Add(this.label11);
            this.gbGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGiayPhep.Location = new System.Drawing.Point(12, 12);
            this.gbGiayPhep.Name = "gbGiayPhep";
            this.gbGiayPhep.Size = new System.Drawing.Size(758, 57);
            this.gbGiayPhep.TabIndex = 0;
            this.gbGiayPhep.Text = "Thông tin chung";
            this.gbGiayPhep.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.gbGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(286, 24);
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(280, 21);
            this.txtTenHang.TabIndex = 3;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(199, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(320, 21);
            this.label6.TabIndex = 2;
            this.label6.Text = "Tên hàng";
            // 
            // txtSTT
            // 
            this.txtSTT.Location = new System.Drawing.Point(77, 24);
            this.txtSTT.Name = "txtSTT";
            this.txtSTT.Size = new System.Drawing.Size(100, 21);
            this.txtSTT.TabIndex = 1;
            this.txtSTT.Text = "0";
            this.txtSTT.Value = ((ulong)(0ul));
            this.txtSTT.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSTT.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(19, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "STT hàng";
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // HangTKTGForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(775, 455);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HangTKTGForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Trị giá tính thuế của một dòng hàng";
            this.Load += new System.EventHandler(this.HangTKTGForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).EndInit();
            this.gbGiayPhep.ResumeLayout(false);
            this.gbGiayPhep.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox gbGiayPhep;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSTT;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChiPhiVanChuyen;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTraTruoc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiBaoHiem;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtHoaHong;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtbanQuyen;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtGiaGhiTrenHD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTienTraSuDung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtKhoanTTGianTiep;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiNoiDia;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChiPhiBaoBi;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTienThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNguyenLieu;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTienLai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTroGiup;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChiPhiPhatSinh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChiPhiDongGoi;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaTT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaNT;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThietKe;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtCongCu;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtGiamGia;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtVatLieu;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;


    }
}