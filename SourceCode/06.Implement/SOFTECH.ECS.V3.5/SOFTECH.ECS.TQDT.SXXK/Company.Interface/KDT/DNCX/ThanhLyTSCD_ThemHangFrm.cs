﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;

namespace Company.Interface.KDT.DNCX
{
    public partial class ThanhLyTSCD_ThemHangFrm : BaseForm
    {
        public ThanhLyTSCD_ThemHangFrm()
        {
            InitializeComponent();
        }

        private void uiGroupBox2_Click(object sender, EventArgs e)
        {

        }
        public KDT_SXXK_TLTSCD HangHoa;
        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                Get();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                
            }
            
        }

        private void ThanhLyTSCD_ThemHangFrm_Load(object sender, EventArgs e)
        {
            txtNgayDangKyTK.Text = string.Empty;
            this._DonViTinh = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
            cmbDVT.DataSource = this._DonViTinh;
            cmbDVT.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            cmbDVT.ValueMember = "ID";
            cmbDVT.DisplayMember = "Ten";
            txtMaHQTK.ReadOnly = false;

            if(HangHoa != null)
            {
                Set();
            }
        }

        private void Get()
        {
            if (HangHoa == null) HangHoa = new KDT_SXXK_TLTSCD();
            HangHoa.MaSanPham = txtMaHangHoa.Text.Trim();
            HangHoa.TenSanPham = txtTenHangHoa.Text.Trim();
            HangHoa.SoLuongTL = Convert.ToDecimal(txtSoLuongThanhLy.Value);
            HangHoa.DVT_ID = cmbDVT.SelectedValue.ToString();
            HangHoa.SoToKhai = txtSoToKhai.Text.Trim();
            HangHoa.MaLoaiHinhTK = txtMaLoaiHinhTK.Ma;
            HangHoa.MaHaiQuanTK = txtMaHQTK.Ma;
            HangHoa.NgayDangKyTK = txtNgayDangKyTK.Value;
        }
        private void Set()
        {
            if (HangHoa != null)
            {
                txtMaHangHoa.Text = HangHoa.MaSanPham;
                txtTenHangHoa.Text = HangHoa.TenSanPham;
                txtSoLuongThanhLy.Value = HangHoa.SoLuongTL;
                cmbDVT.SelectedValue = HangHoa.DVT_ID;
                txtSoToKhai.Text = HangHoa.SoToKhai;
                txtNgayDangKyTK.Value = HangHoa.NgayDangKyTK;
                txtNgayDangKyTK.Text = HangHoa.NgayDangKyTK.Year > 1900 ? HangHoa.NgayDangKyTK.ToString() : string.Empty;
                txtMaHQTK.Ma = HangHoa.MaHaiQuanTK;
                txtMaLoaiHinhTK.Ma = HangHoa.MaLoaiHinhTK;

            }
        }
    }
}
