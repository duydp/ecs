IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Update_LenhSanXuat]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Update_LenhSanXuat]
GO
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Update]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Update_LenhSanXuat]  
 @HopDong_ID bigint,  
 @MaSanPham varchar(50),  
 @MaNguyenPhuLieu nvarchar(50),  
 @DinhMucSuDung numeric(18, 8),  
 @TyLeHaoHut numeric(18, 8),  
 @STTHang int,  
 @NPL_TuCungUng numeric(18, 6),  
 @TenSanPham nvarchar(255),  
 @TenNPL nvarchar(255),  
 @SoTiepNhan numeric(18, 0),  
 @NgayTiepNhan datetime,  
 @TrangThaiXuLy int,  
 @GuidString nvarchar(50),  
 @LenhSanXuat_ID bigint  
AS  
  
UPDATE  
 [dbo].[t_GC_DinhMuc]  
SET  
 [DinhMucSuDung] = @DinhMucSuDung,  
 [TyLeHaoHut] = @TyLeHaoHut,  
 [STTHang] = @STTHang,  
 [NPL_TuCungUng] = @NPL_TuCungUng,  
 [TenSanPham] = @TenSanPham,  
 [TenNPL] = @TenNPL,  
 [SoTiepNhan] = @SoTiepNhan,  
 [NgayTiepNhan] = @NgayTiepNhan,  
 [TrangThaiXuLy] = @TrangThaiXuLy,  
 [GuidString] = @GuidString ,
 [LenhSanXuat_ID] = @LenhSanXuat_ID  
WHERE  
 [HopDong_ID] = @HopDong_ID  
 AND [MaSanPham] = @MaSanPham  
 AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu

 GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.8',GETDATE(), N'CẬP NHẬT PROCDEDURE MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT')
END
