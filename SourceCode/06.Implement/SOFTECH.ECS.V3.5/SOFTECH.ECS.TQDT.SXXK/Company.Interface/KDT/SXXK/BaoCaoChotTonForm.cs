﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using Company.Interface.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class BaoCaoChotTonForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_TotalInventoryReport totalInventoryReport = new KDT_VNACCS_TotalInventoryReport();
        public KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReport_Detail = new KDT_VNACCS_TotalInventoryReport_Detail();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
        public bool isAdd = true;
        List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = new List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu>();
        List<Company.BLL.KDT.SXXK.SXXK_SanPham> SPCollection = new List<Company.BLL.KDT.SXXK.SXXK_SanPham>();
        IList<HangDuaVao> HangDuaVaoCollection ;
        public BaoCaoChotTonForm()
        {
            InitializeComponent();
        }
        private void SetCommandStatus()
        {
            if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdGuidString.Enabled = cmdGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnLuu.Enabled = false;
                btnXoa.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = totalInventoryReport.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnLuu.Enabled = true;
                btnXoa.Enabled = true;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdGuidString.Enabled = cmdGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTiepNhan.Value = DateTime.Now;
                lblTrangThai.Text = totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdGuidString.Enabled = cmdGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnLuu.Enabled = false;
                btnXoa.Enabled = false;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = totalInventoryReport.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnLuu.Enabled = false;
                btnXoa.Enabled = false;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdGuidString.Enabled = cmdGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnLuu.Enabled = true;
                btnXoa.Enabled = true;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdGuidString.Enabled = cmdGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;


                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = totalInventoryReport.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnLuu.Enabled = true;
                btnXoa.Enabled = true;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdGuidString.Enabled = cmdGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;


                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = totalInventoryReport.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {

                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnLuu.Enabled = false;
                btnXoa.Enabled = false;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdGuidString.Enabled = cmdGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = totalInventoryReport.NgayTiepNhan;
                lblTrangThai.Text = totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Not declared") : setText("Chờ duyệt Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }


        }
        private void BaoCaoChotTonForm_Load(object sender, EventArgs e)
        {
            if (totalInventoryReport == null || totalInventoryReport.ID == 0)
            {
                txtMaDN.Text = GlobalSettings.MA_DON_VI;
                txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                txtDiaChi.Text = GlobalSettings.DIA_CHI;
                clcNgayTiepNhan.Value = DateTime.Now;
                clcNgayChotTon.Value = DateTime.Now;
                cbbQuy.SelectedValue = "1";
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                cbbType.SelectedIndex = 0;
                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                SetCommandStatus();
            }
            else
            {
                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = totalInventoryReport.NgayTiepNhan;
                txtMaDN.Text = totalInventoryReport.MaDoanhNghiep;
                txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                txtDiaChi.Text = GlobalSettings.DIA_CHI;
                ctrCoQuanHaiQuan.Code = totalInventoryReport.MaHaiQuan;
                clcNgayChotTon.Value = totalInventoryReport.NgayChotTon;
                cbbQuy.SelectedValue = totalInventoryReport.LoaiBaoCao.ToString();
                //
                cbbType.SelectedValue = totalInventoryReport.LoaiSua.ToString();
                //
                txtGiaiTrinh.Text = totalInventoryReport.GhiChuKhac;
                totalInventoryReport.DetailCollection = KDT_VNACCS_TotalInventoryReport_Detail.SelectCollectionBy_TotalInventory_ID(totalInventoryReport.ID);
                SetCommandStatus();
                BindData();
            }
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdSend":
                    this.SendV5("Send");
                    break;
                case "cmdEdit":
                    this.SendV5("Edit");
                    break;
                case "cmdCancel":
                    if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN KHAI BÁO HỦY BÁO CÁO CHỐT TỒN NÀY ĐẾN HQ KHÔNG ? ", true) == "Yes")
                    {
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        SetCommandStatus();
                        totalInventoryReport.Update();
                        this.SendV5("Cancel");
                    }
                    break;
                case "cmdFeedback":
                    this.FeedBackV5();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdAddExcel":
                    this.AddExcel();
                    break;
                case "cmdChuyenTT":
                    ChuyenTT();
                    break;
                    
            }
        }
        private void ChuyenTT()
        {
            try
            {
                if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN CHUYỂN BÁO CÁO CHỐT TỒN NÀY SANG KHAI BÁO SỬA KHÔNG ? ", true) == "Yes")
                {
                    totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    totalInventoryReport.Update();
                    SetCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void AddExcel()
        {
            try
            {
                BaoCaoChotTonReadExcelForm form = new BaoCaoChotTonReadExcelForm();
                form.totalInventoryReport = totalInventoryReport;
                form.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_TotalInventoryReport", "", Convert.ToInt32(totalInventoryReport.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = this.totalInventoryReport.ID;
                form.DeclarationIssuer = DeclarationIssuer.TotalInventoryReport;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = totalInventoryReport.GuidString;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.TotalInventoryReport,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.TotalInventoryReport,
                };
                subjectBase.Type = DeclarationIssuer.TotalInventoryReport;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = totalInventoryReport.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(totalInventoryReport.MaHaiQuan.Trim())),
                                                  Identity = totalInventoryReport.MaHaiQuan
                                              }, subjectBase, null);
                if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(totalInventoryReport.MaHaiQuan));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                    {
                        totalInventoryReport.Update();
                        isFeedBack = true;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        totalInventoryReport.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        totalInventoryReport.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                    }
                }
            }
        }

        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.TotalInventoryReportSendHandler(totalInventoryReport, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5(string Status)
        {
            if (ShowMessage("DOANH NGHIỆP CÓ MUỐN KHAI PHỤ KIỆN NÀY ĐẾN HQ KHÔNG ?", true) == "Yes")
            {
                if (totalInventoryReport.ID == 0)
                {
                    ShowMessage("DOANH NGHIỆP HÃY LƯU THÔNG TIN TRƯỚC KHI KHAI BÁO ", false);
                    return;
                }
                else
                {
                    totalInventoryReport.DetailCollection = KDT_VNACCS_TotalInventoryReport_Detail.SelectCollectionBy_TotalInventory_ID(totalInventoryReport.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.TotalInventoryReport;
                sendXML.master_id = totalInventoryReport.ID;

                if (sendXML.Load())
                {
                    if (Status=="Send")
                    {
                        if (totalInventoryReport.TrangThaiXuLy==TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                        {
                            ShowMessage("THÔNG TIN ĐÃ ĐƯỢC GỬI ĐẾN HẢI QUAN. NHẤN [LẤY PHẢN HỒI] ĐỂ LẤY THÔNG TIN", false);
                            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            return;   
                        }
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    totalInventoryReport.GuidString = Guid.NewGuid().ToString();
                    GoodsItems_VNACCS totalInventoryReport_VNACCS = new GoodsItems_VNACCS();
                    totalInventoryReport_VNACCS = Mapper_V4.ToDataTransferTotalInventoryReport(totalInventoryReport,totalInventoryReport_Detail, GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI,Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = totalInventoryReport.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(totalInventoryReport.MaHaiQuan)),
                              Identity = totalInventoryReport.MaHaiQuan
                          },
                          new SubjectBase()
                          {

                              Type = totalInventoryReport_VNACCS.Issuer,
                              //Function = DeclarationFunction.SUA,
                              Reference = totalInventoryReport.GuidString,
                          },
                          totalInventoryReport_VNACCS
                        );
                    if (Status=="Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if ((Status == "Edit"))
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET; 
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.RegisterTotalInventoryReport);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.TotalInventoryReport;
                        sendXML.master_id = totalInventoryReport.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            totalInventoryReport.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                            FeedBackV5();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            SetCommandStatus();
                            FeedBackV5();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            totalInventoryReport.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            ShowMessageTQDT(msgInfor, false);
                            FeedBackV5();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.RegisterTotalInventoryReport);
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateChoose(cbbMaHangHoa, errorProvider, "MÃ HÀNG HÓA ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, "TÊN HÀNG HÓA ", isOnlyWarning);
                ctrDVT.SetValidate = !isOnlyWarning; ctrDVT.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrDVT.IsValidate;
                isValid &= ValidateControl.ValidateNull(txtLuongSoSach, errorProvider, "SỐ LƯỢNG SỔ SÁCH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtLuongThucTe, errorProvider, "SỐ LƯỢNG THỰC TẾ", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(cbbLoaiHangHoa, errorProvider, "Loại hàng hóa", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void BindData()
        {
            try
            {
                dgList2.Refetch();
                dgList2.DataSource = totalInventoryReport.DetailCollection;
                dgList2.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Save()
        {
            try
            {
                totalInventoryReport.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text);
                totalInventoryReport.NgayTiepNhan = clcNgayTiepNhan.Value;
                totalInventoryReport.MaHaiQuan = ctrCoQuanHaiQuan.Code;
                totalInventoryReport.MaDoanhNghiep = txtMaDN.Text;
                if (totalInventoryReport==null || totalInventoryReport.ID==0)
                    totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                totalInventoryReport.NgayChotTon = clcNgayChotTon.Value;
                totalInventoryReport.LoaiBaoCao = Convert.ToDecimal(cbbQuy.SelectedValue.ToString());
                totalInventoryReport.LoaiSua = Convert.ToDecimal(cbbType.SelectedValue.ToString());
                totalInventoryReport.GhiChuKhac = txtGiaiTrinh.Text;
                totalInventoryReport.InsertUpdateFull();
                ShowMessage("LƯU THÀNH CÔNG ", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (totalInventoryReport == null)
                {
                    totalInventoryReport = new KDT_VNACCS_TotalInventoryReport();
                }
                totalInventoryReport_Detail.TenHangHoa = txtTenNPL.Text;
                totalInventoryReport_Detail.MaHangHoa = cbbMaHangHoa.Value.ToString();
                totalInventoryReport_Detail.DVT = ctrDVT.Code;                
                totalInventoryReport_Detail.SoLuongTonKhoSoSach = Convert.ToDecimal(txtLuongSoSach.Text);
                totalInventoryReport_Detail.SoLuongTonKhoThucTe = Convert.ToDecimal(txtLuongThucTe.Text);
                if(isAdd)
                    totalInventoryReport.DetailCollection.Add(totalInventoryReport_Detail);
                isAdd = true;
                BindData();
                totalInventoryReport_Detail = new KDT_VNACCS_TotalInventoryReport_Detail();
                txtTenNPL.Text = String.Empty;
                cbbMaHangHoa.Text = String.Empty;
                txtLuongThucTe.Text = String.Empty;
                txtLuongSoSach.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {

                GridEXSelectedItemCollection items = dgList2.SelectedItems;
                List<KDT_VNACCS_TotalInventoryReport_Detail> ItemColl = new List<KDT_VNACCS_TotalInventoryReport_Detail>();
                if (dgList2.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_TotalInventoryReport_Detail)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_TotalInventoryReport_Detail item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        totalInventoryReport.DetailCollection.Remove(item);
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            //try
            //{
            //    string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;
            //    if (cbbLoaiHang.SelectedValue.ToString() == "1" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            //    {

            //        if (this.NPLRegistedForm_CX == null)
            //            this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
            //        this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
            //        this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        this.NPLRegistedForm_CX.LoaiNPL = "3";
            //        this.NPLRegistedForm_CX.ShowDialog(this);
            //        if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
            //        {
            //            txtMaNPL.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
            //            txtTenNPL.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
            //            txtMaHS.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
            //            ctrDVT.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

            //        }

            //    }
            //    else if (cbbLoaiHang.SelectedValue.ToString() == "1")
            //    {
            //        if (this.NPLRegistedForm == null)
            //            this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
            //        this.NPLRegistedForm.CalledForm = "HangMauDichForm";
            //        this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        this.NPLRegistedForm.isBrowser = true;
            //        this.NPLRegistedForm.ShowDialog(this);
            //        if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
            //        {
            //            txtMaNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
            //            txtTenNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
            //            txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
            //            ctrDVT.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

            //        }
            //    }
            //    else if (cbbLoaiHang.SelectedValue.ToString() == "2")
            //    {
            //        this.SPRegistedForm = new SanPhamRegistedForm();
            //        this.SPRegistedForm.CalledForm = "HangMauDichForm";
            //        this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        this.SPRegistedForm.ShowDialog();
            //        if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
            //        {
            //            txtMaNPL.Text = this.SPRegistedForm.SanPhamSelected.Ma;
            //            txtTenNPL.Text = this.SPRegistedForm.SanPhamSelected.Ten;
            //            txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
            //            ctrDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
            //        }
            //    }
            //    else
            //    {
            //        ShowMessage("DOANH NGHIỆP CHƯA CHỌN LOẠI LOẠI HÀNG HÓA ", false);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            //try
            //{
            //    if (cbbLoaiHang.SelectedValue.ToString() == "1" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            //    {
            //        IList<HangDuaVao> listTB = HangDuaVao.SelectCollectionDynamic(string.Format("Ma = '{0}' and LoaiNPL = '3'", txtMaNPL.Text.Trim()), null);
            //        if (listTB != null && listTB.Count > 0)
            //        {
            //            HangDuaVao tb = listTB[0];
            //            txtTenNPL.Text = tb.Ten;
            //            txtMaHS.Text = tb.MaHS;
            //            txtMaNPL.Text = tb.Ma;
            //            ctrDVT.Code = DVT_VNACC(tb.DVT_ID.PadRight(3));
            //        }
            //        else
            //        {
            //        }
            //    }
            //    else if (cbbLoaiHang.SelectedValue.ToString() == "1")
            //    {
            //        Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
            //        npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //        npl.Ma = txtMaNPL.Text;
            //        if (npl.Load())
            //        {
            //            txtMaNPL.Text = npl.Ma;
            //            ctrDVT.Code = DVT_VNACC(npl.DVT_ID.PadRight(3));
            //            txtTenNPL.Text = npl.Ten;
            //            txtMaHS.Text = npl.MaHS;

            //        }
            //    }
            //    else if (cbbLoaiHang.SelectedValue.ToString() == "2")
            //    {
            //        Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
            //        sp.Ma = txtMaNPL.Text;
            //        sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //        if (sp.Load())
            //        {
            //            txtMaNPL.Text = sp.Ma;
            //            ctrDVT.Code = DVT_VNACC(sp.DVT_ID.PadRight(3));
            //            txtTenNPL.Text = sp.Ten;
            //            txtMaHS.Text = sp.MaHS;
            //        }
            //    }
            //    else
            //    {
            //        //ShowMessage("Bạn chưa chọn loại hàng hóa", false);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //ShowMessage("Bạn chưa chọn loại hàng hóa", false);
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                totalInventoryReport_Detail = (KDT_VNACCS_TotalInventoryReport_Detail)dgList2.GetRow().DataRow;
                cbbMaHangHoa.Value = totalInventoryReport_Detail.MaHangHoa;
                txtTenNPL.Text = totalInventoryReport_Detail.TenHangHoa;
                ctrDVT.Code = totalInventoryReport_Detail.DVT;
                txtLuongSoSach.Text = totalInventoryReport_Detail.SoLuongTonKhoSoSach.ToString();
                txtLuongThucTe.Text = totalInventoryReport_Detail.SoLuongTonKhoThucTe.ToString();
                isAdd = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý :\r\n" + ex.Message, false);
            }
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void dgList2_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList2.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            totalInventoryReport_Detail = (KDT_VNACCS_TotalInventoryReport_Detail)i.GetRow().DataRow;
                            cbbMaHangHoa.Value = totalInventoryReport_Detail.MaHangHoa;
                            txtTenNPL.Text = totalInventoryReport_Detail.TenHangHoa;
                            ctrDVT.Code = totalInventoryReport_Detail.DVT;
                            txtLuongSoSach.Text = totalInventoryReport_Detail.SoLuongTonKhoSoSach.ToString();
                            txtLuongThucTe.Text = totalInventoryReport_Detail.SoLuongTonKhoThucTe.ToString();
                            isAdd = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH HÀNG HÓA BÁO CÁO CHỐT TỒN.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList2;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLoaiHang_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                string LoaiHang = cbbLoaiHang.SelectedValue.ToString();
                switch (LoaiHang)
                {
                    case "1" :
                        if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                        {
                            HangDuaVaoCollection = HangDuaVao.SelectCollectionAll();
                            cbbMaHangHoa.DataSource = HangDuaVaoCollection;
                            cbbMaHangHoa.DisplayMember = "Ma";
                            cbbMaHangHoa.ValueMember = "Ma";
                        }
                        else
                        {
                            NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                            cbbMaHangHoa.DataSource = NPLCollection;
                            cbbMaHangHoa.DisplayMember = "Ma";
                            cbbMaHangHoa.ValueMember = "Ma";
                        }
                        break;
                    case "2":
                        SPCollection = Company.BLL.KDT.SXXK.SXXK_SanPham.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                        cbbMaHangHoa.DataSource = SPCollection;
                        cbbMaHangHoa.DisplayMember = "Ma";
                        cbbMaHangHoa.ValueMember = "Ma";
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaHangHoa_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaHangHoa.Value != null)
                {
                    switch (cbbLoaiHang.SelectedValue.ToString())
                    {
                        case "1" :
                            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                            {
                                foreach (HangDuaVao item in HangDuaVaoCollection)
                                {
                                    if (item.Ma == cbbMaHangHoa.Value.ToString())
                                    {
                                        ctrDVT.Code = DVT_VNACC(item.DVT_ID.PadRight(3));
                                        txtTenNPL.Text = item.Ten;
                                        txtMaHS.Text = item.MaHS;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu item in NPLCollection)
                                {
                                    if (item.Ma == cbbMaHangHoa.Value.ToString())
                                    {
                                        ctrDVT.Code = DVT_VNACC(item.DVT_ID.PadRight(3));
                                        txtTenNPL.Text = item.Ten;
                                        txtMaHS.Text = item.MaHS;
                                        break;
                                    }
                                }
                            }
                            break;
                        case "2":
                            foreach (Company.BLL.KDT.SXXK.SXXK_SanPham item in SPCollection)
                            {
                                if (item.Ma == cbbMaHangHoa.Value.ToString())
                                {
                                    ctrDVT.Code = DVT_VNACC(item.DVT_ID.PadRight(3));
                                    txtTenNPL.Text = item.Ten;
                                    txtMaHS.Text = item.MaHS;
                                    break;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList2.RootTable.Columns["MaHangHoa"], ConditionOperator.Contains, txtMaNPL.Text);
                dgList2.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
