﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.KD
{
    public class NodeName
    {
        /// <summary>
        /// Khai bao.
        /// </summary>
        public const string Declaration = "Declaration";
        /// <summary>
        /// Mo ta issue
        /// </summary>
        public const string issuer = "issuer";
        public const string reference = "reference";
        public const string issue = "issue";
        public const string function = "function";
        public const string issueLocation = "issueLocation";
        public const string status = "status";
        public const string customsReference = "customsReference";
        public const string acceptance = "acceptance";
        public const string declarationOffice = "declarationOffice";
        public const string Agent = "Agent";
        public const string name = "name";
        public const string identity = "identity";
        public const string Importer = "Importer";
        public const string importer = "importer";
        public const string Identity = "Identity";
        public const string DeclarationDocument = "DeclarationDocument";
        public const string natureOfTransaction = "natureOfTransaction";
        public const string Licenses = "Licenses";
        public const string License = "License";
        public const string type = "type";
        public const string expire = "expire";
        public const string AdditionalInformation = "AdditionalInformation";
        public const string content = "content";
        public const string GoodsItem = "GoodsItem";
        /// <summary>
        /// Ghi chu mo ta seqquen
        /// </summary>
        public const string sequence = "sequence";
        public const string statisticalValue = "statisticalValue";
        public const string CurrencyExchange = "CurrencyExchange";
        public const string Commodity = "Commodity";
        public const string description = "description";
        public const string identification = "identification";
        public const string tariffClassification = "tariffClassification";
        public const string GoodsMeasure = "GoodsMeasure";
        public const string quantity = "quantity";
        public const string measureUnit = "measureUnit";
        public const string CertificateOfOrigins = "CertificateOfOrigins";
        public const string CertificateOfOrigin = "CertificateOfOrigin";
        public const string representative = "representative";
        public const string exporter = "exporter";
        public const string exportationCountry = "exportationCountry";
        public const string code = "code";
        public const string importationCountry = "importationCountry";
        public const string LoadingLocation = "LoadingLocation";
        public const string loading = "loading";
        public const string UnLoadingLocation = "UnLoadingLocation";
        public const string CurrencyType = "CurrencyType";
        public const string ConsignmentItemPackaging = "ConsignmentItemPackaging";
        public const string markNumber = "markNumber";
        public const string cargoDescription = "cargoDescription";
        public const string grossMass = "grossMass";
        public const string Origin = "Origin";
        public const string originCountry = "originCountry";
        public const string Invoice = "Invoice";
        public const string rate = "rate";
        public const string DeclarationPackaging = "DeclarationPackaging";
        public const string tariff = "tariff";
        public const string CommercialInvoices = "CommercialInvoices";
        public const string CommercialInvoice = "CommercialInvoice";
        public const string Seller = "Seller";
        public const string Buyer = "Buyer";
        public const string AdditionalDocument = "AdditionalDocument";
        public const string Payment = "Payment";
        public const string method = "method";
        public const string currencyType = "currencyType";
        public const string TradeTerm = "TradeTerm";
        public const string condition = "condition";
        public const string CommercialInvoiceItem = "CommercialInvoiceItem";
        public const string unitPrice = "unitPrice";
        public const string ValuationAdjustment = "ValuationAdjustment";
        public const string addition = "addition";
        public const string deduction = "deduction";
        public const string totalValue = "totalValue";
        public const string DeliveryDestination = "DeliveryDestination";
        public const string line = "line";
        public const string RepresentativePerson = "RepresentativePerson";
        public const string ContractDocuments = "ContractDocuments";
        public const string ContractDocument = "ContractDocument";
        public const string ContractItem = "ContractItem";
        public const string GoodsShipment = "GoodsShipment";
        public const string Consignor = "Consignor";
        public const string Consignee = "Consignee";
        public const string NotifyParty = "NotifyParty";
        public const string EntryCustomsOffice = "EntryCustomsOffice";
        public const string ExitCustomsOffice = "ExitCustomsOffice";
        public const string Exporter = "Exporter";
        public const string CustomsGoodsItem = "CustomsGoodsItem";
        public const string statement = "statement";
        public const string CustomsOfficeChangedRequest = "CustomsOfficeChangedRequest";
        public const string examinationPlace = "examinationPlace";
        public const string time = "time";
        public const string route = "route";
        public const string customsValue = "customsValue";
        public const string Manufacturer = "Manufacturer";
        public const string CustomsValuation = "CustomsValuation";
        public const string SpecializedManagement = "SpecializedManagement";
        public const string tariffClassificationExtension = "tariffClassificationExtension";
        public const string brand = "brand";
        public const string grade = "grade";
        public const string ingredients = "ingredients";
        public const string modelNumber = "modelNumber";
        public const string DutyTaxFee = "DutyTaxFee";
        public const string InvoiceLine = "InvoiceLine";
        public const string adValoremTaxBase = "adValoremTaxBase";
        public const string dutyRegime = "dutyRegime";
        public const string specificTaxBase = "specificTaxBase";
        public const string tax = "tax";
        public const string itemCharge = "itemCharge";
        public const string statementDescription = "statementDescription";
        public const string exitToEntryCharge = "exitToEntryCharge";
        public const string freightCharge = "freightCharge";
        public const string otherChargeDeduction = "otherChargeDeduction";
        public const string percentage = "percentage";
        public const string amount = "amount";
        public const string AttachDocuments = "AttachDocuments";
        public const string AttachDocumentItem = "AttachDocumentItem";
        public const string AttachedFiles = "AttachedFiles";
        public const string AttachedFile = "AttachedFile";
        public const string fileName = "fileName";
        public const string PreviousCustomsprocedure = "PreviousCustomsprocedure";
        public const string CustomsProcedure = "CustomsProcedure";
        public const string current = "current";
        public const string previous = "previous";
    }
}
