﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.GC.SHARE
{
    public class Message_PhuKienGC
    {
        public const string PhuKienGiaHanHopDong = "PhuKienGiaHanHopDong";          // Phụ kiện Gia hạn hợp đồng
        public const string PhuLienHuyHopDong = "PhuLienHuyHopDong";                // Phụ kiện Hủy hợp đồng
        public const string PhuKienBoSungNguyenLieu = "PhuKienBoSungNguyenLieu";    // Phụ kiện bổ sung nguyên liệu
        public const string PhuKienBoSungSanPham = "PhuKienBoSungSanPham";          // Phụ kiện bổ sung sản phẩm
        public const string PhuKienBoSungThietBi = "PhuKienBoSungThietBi";          // Phụ kiện bổ sung thiết bị
        public const string PhuKienBoSungHangMau = "PhuKienBoSungHangMau";          // Phụ kiện bổ sung hàng mẫu
        public const string PhuKienSuaNguyenPhuLieu = "PhuKienSuaNguyenPhuLieu";    // Phụ kiện sửa nguyên phụ liệu
        public const string PhuKienSuaSanPham = "PhuKienSuaSanPham";                // Phụ kiện sửa sản phẩm 
        public const string PhuKienSuaDoiThietBi= "PhuKienSuaDoiThietBi";           // Phụ kiện sửa đổi thiết bị
        public const string PhuKienSuaDoiHangMau = "PhuKienSuaDoiHangMau";          // Phụ kiện sửa đổi hàng mẫu
        public const string PhuKienHuyDangKyNguyenLieu = "PhuKienHuyDangKyNguyenLieu";// Phụ kiện hủy đăng ký nguyên liệu
        public const string PhuKienHuyDangKySanPham = "PhuKienHuyDangKySanPham";    // Phụ kiện hủy đăng ký sản phẩm
        public const string PhuKienHuyDangKyThietBi = "PhuKienHuyDangKyThietBi";    // Phụ kiện hủy đăng ký thiết bị
        public const string PhuKienHuyDangKyHangMau = "PhuKienHuyDangKyHangMau";    // Phụ kiện hủy đăng ký hàng mẫu
        public const string PhuKienSuaThongTinChungHopDong = "PhuKienSuaThongTinChungHopDong";// Phụ kiện sửa thông tin chung hợp đồng

    }
}
