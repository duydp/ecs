﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.SXXK;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
using System.Collections.Generic;

namespace Company.Interface.Report.SXXK
{
    public partial class ReportNPL : DevExpress.XtraReports.UI.XtraReport
    {
        public List<NguyenPhuLieu> NPLCollection = new List<NguyenPhuLieu>();
        private int STT = 0;
        public ReportNPL()
        {
            InitializeComponent();
        }
        public void BindReportDinhMucDaDangKy()
        {

            this.DataSource = this.NPLCollection;
            lblMaSP.DataBindings.Add("Text", this.DataSource, "Ma");
            lblTenSP.DataBindings.Add("Text", this.DataSource, "Ten");
            lblMaHS.DataBindings.Add("Text", this.DataSource, "MaHS");
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDVT.Text = DonViTinh.GetName(GetCurrentColumnValue("DVT_ID"));
        }

    }
}
