﻿namespace Company.Interface
{
    partial class ContainerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grdSoContainer_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TKMD_BaoLanhVaTraThueThay));
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.editBox2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.editBox3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.editBox4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.editBox5 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.editBox6 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.editBox7 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grdSoContainer = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSoContainer)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(647, 267);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 325;
            this.label5.Text = "Mã";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 325;
            this.label6.Text = "Tên ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 325;
            this.label8.Text = "Địa chỉ";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 235);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(647, 32);
            this.uiGroupBox1.TabIndex = 326;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.editBox7);
            this.uiGroupBox2.Controls.Add(this.editBox6);
            this.uiGroupBox2.Controls.Add(this.editBox5);
            this.uiGroupBox2.Controls.Add(this.editBox4);
            this.uiGroupBox2.Controls.Add(this.editBox3);
            this.uiGroupBox2.Controls.Add(this.editBox2);
            this.uiGroupBox2.Controls.Add(this.editBox1);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(351, 235);
            this.uiGroupBox2.TabIndex = 327;
            this.uiGroupBox2.Text = "Địa điểm xếp hàng lên xe chở hàng";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(560, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(485, 3);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(69, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.grdSoContainer);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(351, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(296, 235);
            this.uiGroupBox3.TabIndex = 328;
            this.uiGroupBox3.Text = "Số Container";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // editBox1
            // 
            this.editBox1.Location = new System.Drawing.Point(234, 19);
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(44, 21);
            this.editBox1.TabIndex = 3;
            this.editBox1.VisualStyleManager = this.vsmMain;
            // 
            // editBox2
            // 
            this.editBox2.Location = new System.Drawing.Point(174, 19);
            this.editBox2.Name = "editBox2";
            this.editBox2.Size = new System.Drawing.Size(44, 21);
            this.editBox2.TabIndex = 2;
            this.editBox2.VisualStyleManager = this.vsmMain;
            // 
            // editBox3
            // 
            this.editBox3.Location = new System.Drawing.Point(114, 19);
            this.editBox3.Name = "editBox3";
            this.editBox3.Size = new System.Drawing.Size(44, 21);
            this.editBox3.TabIndex = 1;
            this.editBox3.VisualStyleManager = this.vsmMain;
            // 
            // editBox4
            // 
            this.editBox4.Location = new System.Drawing.Point(54, 19);
            this.editBox4.Name = "editBox4";
            this.editBox4.Size = new System.Drawing.Size(44, 21);
            this.editBox4.TabIndex = 0;
            this.editBox4.VisualStyleManager = this.vsmMain;
            // 
            // editBox5
            // 
            this.editBox5.Location = new System.Drawing.Point(294, 19);
            this.editBox5.Name = "editBox5";
            this.editBox5.Size = new System.Drawing.Size(44, 21);
            this.editBox5.TabIndex = 4;
            this.editBox5.VisualStyleManager = this.vsmMain;
            // 
            // editBox6
            // 
            this.editBox6.Location = new System.Drawing.Point(54, 51);
            this.editBox6.Name = "editBox6";
            this.editBox6.Size = new System.Drawing.Size(284, 21);
            this.editBox6.TabIndex = 5;
            this.editBox6.VisualStyleManager = this.vsmMain;
            // 
            // editBox7
            // 
            this.editBox7.Location = new System.Drawing.Point(54, 86);
            this.editBox7.Multiline = true;
            this.editBox7.Name = "editBox7";
            this.editBox7.Size = new System.Drawing.Size(284, 126);
            this.editBox7.TabIndex = 6;
            this.editBox7.VisualStyleManager = this.vsmMain;
            // 
            // grdSoContainer
            // 
            this.grdSoContainer.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdSoContainer.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdSoContainer.ColumnAutoResize = true;
            grdSoContainer_DesignTimeLayout.LayoutString = resources.GetString("grdSoContainer_DesignTimeLayout.LayoutString");
            this.grdSoContainer.DesignTimeLayout = grdSoContainer_DesignTimeLayout;
            this.grdSoContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdSoContainer.EmptyRows = true;
            this.grdSoContainer.GridLineStyle = Janus.Windows.GridEX.GridLineStyle.Solid;
            this.grdSoContainer.GroupByBoxVisible = false;
            this.grdSoContainer.Location = new System.Drawing.Point(3, 17);
            this.grdSoContainer.Name = "grdSoContainer";
            this.grdSoContainer.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdSoContainer.Size = new System.Drawing.Size(290, 215);
            this.grdSoContainer.TabIndex = 0;
            this.grdSoContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdSoContainer.VisualStyleManager = this.vsmMain;
            // 
            // TKMD_BaoLanhVaTraThueThay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 267);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "TKMD_BaoLanhVaTraThueThay";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin Container";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdSoContainer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.GridEX.EditControls.EditBox editBox7;
        private Janus.Windows.GridEX.EditControls.EditBox editBox6;
        private Janus.Windows.GridEX.EditControls.EditBox editBox5;
        private Janus.Windows.GridEX.EditControls.EditBox editBox4;
        private Janus.Windows.GridEX.EditControls.EditBox editBox3;
        private Janus.Windows.GridEX.EditControls.EditBox editBox2;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private Janus.Windows.GridEX.GridEX grdSoContainer;
    }
}