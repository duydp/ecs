﻿using System;
using Company.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamDangKyDetailForm_CX : BaseForm
    {
        public SanPhamDangKy SPDangKy = new SanPhamDangKy();

        //-----------------------------------------------------------------------------------------
        public SanPhamDangKyDetailForm_CX()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        private void SanPhamDangKyDetailForm_Load(object sender, EventArgs e)
        {
            this.SPDangKy.LoadSPCollection();
            dgList.DataSource = this.SPDangKy.SPCollection;
            switch (this.SPDangKy.TrangThaiXuLy)
            {
                case -1:
                    // lblTrangThai.Text = "Chưa gửi thông tin";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa gửi thông tin";
                    }
                    else
                    {
                        lblTrangThai.Text = "Information has not yet sent";
                    }
                    break;
                case 0:
                    //lblTrangThai.Text = "Chờ duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait to approval";
                    }
                    break;
                case 1:
                    //lblTrangThai.Text = "Đã duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Approved";
                    }
                    break;
                case 2:
                    //lblTrangThai.Text = "Không phê duyệt";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Không phê duyệt";
                    }
                    else
                    {
                        lblTrangThai.Text = "Not Approved";
                    }
                    break;
                case 11:
                    //lblTrangThai.Text = "Chờ Hủy";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ Hủy";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait to Canceled";
                    }
                    break;
                case 10:
                    //lblTrangThai.Text = "Đã Hủy";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã hủy";
                    }
                    else
                    {
                        lblTrangThai.Text = "Canceled";
                    }
                    break;

            }

            txtSoTiepNhan.Text = this.SPDangKy.SoTiepNhan.ToString();
            txtMaHaiQuan.Text = this.SPDangKy.MaHaiQuan;
            txtTenHaiQuan.Text = DonViHaiQuan.GetName(this.SPDangKy.MaHaiQuan);
            if (this.OpenType == OpenFormType.View)
            {
                TopRebar1.Visible = false;
                dgList.AllowDelete = InheritableBoolean.False;
            }
            else
            {
                TopRebar1.Visible = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            //MsgSend msg = new MsgSend();
            //msg.master_id = this.SPDangKy.ID;
            //msg.LoaiHS = "SP";
            //if (msg.Load())
            //{
            //    //lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
            //    if (GlobalSettings.NGON_NGU == "0")
            //    {
            //        lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
            //    }
            //    else
            //    {
            //        lblTrangThai.Text = "Not Confirm information to Customs";
            //    }
            //    TopRebar1.Visible = false;
            //    dgList.AllowDelete = InheritableBoolean.False;
            //}
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.KhaiDienTu)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    TopRebar1.Visible = false;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới sản phẩm.
        /// </summary>
        private void add()
        {
            SanPhamEditForm f = new SanPhamEditForm();
            f.OpenType = OpenFormType.Insert;
            f.MaHaiQuan = txtMaHaiQuan.Text;
            f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            f.SPCollection = this.SPDangKy.SPCollection;
            f.spDangKy = SPDangKy;
            f.ShowDialog(this);

            if (f.SPDetail != null)
            {
                SanPham sp = f.SPDetail;
                sp.Master_ID = this.SPDangKy.ID;
                this.SPDangKy.SPCollection.Add(sp);
                sp.Insert();

                BLL.SXXK.SanPham spSXXK = new BLL.SXXK.SanPham();
                spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                spSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN.Trim();
                spSXXK.Ma = sp.Ma;
                spSXXK.MaHS = sp.MaHS;
                spSXXK.Ten = sp.Ten;
                spSXXK.DVT_ID = sp.DVT_ID;
                spSXXK.Insert();
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
        }

        #endregion

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (this.OpenType != OpenFormType.View)
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPhamEditForm f = new SanPhamEditForm();
                        f.MaHaiQuan = txtMaHaiQuan.Text;
                        f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        f.SPDetail = (SanPham)e.Row.DataRow;
                        f.SPCollection = this.SPDangKy.SPCollection;
                        f.SPCollection.RemoveAt(i.Position);
                        f.OpenType = OpenFormType.Edit;
                        f.spDangKy = SPDangKy;
                        f.ShowDialog(this);
                        if (f.SPDetail != null)
                        {
                            this.SPDangKy.SPCollection.Insert(i.Position, f.SPDetail);
                        }
                    }
                    dgList.Refetch();
                    break;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdDelete":
                    dgList_DeletingRecords(null, null);
                    break;
                case "cmdRefresh":
                    RefeshItem();
                    break;

            }
        }

        private void RefeshItem()
        {
            try
            {
                this.SPDangKy.LoadSPCollection();
                dgList.DataSource = this.SPDangKy.SPCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (ShowMessage("Bạn có muốn xóa sản phẩm này không?", true) == "Yes")
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            if (MLMessages("Bạn có muốn xóa sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
            {

                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPham sp = (SanPham)i.GetRow().DataRow;
                        if (sp.ID > 0)
                        {
                            sp.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                        }
                    }
                }
                RefeshItem();
            }
        }
    }
}