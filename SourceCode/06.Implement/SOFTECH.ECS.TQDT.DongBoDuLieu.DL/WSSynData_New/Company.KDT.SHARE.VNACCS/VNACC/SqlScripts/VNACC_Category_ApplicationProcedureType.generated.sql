-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ApplicationProcedureType varchar(5),
	@ApplicationProcedureName nvarchar(250),
	@Section int,
	@IndicationOfGeneralDepartmentOfCustoms int,
	@IndicationOfDisapproval int,
	@IndicationOfCancellation int,
	@IndicationOfCompletionOfExamination int,
	@NumberOfYearsForSavingTheOriginalDocumentData int,
	@ExpiryDate datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_ApplicationProcedureType]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ApplicationProcedureType],
	[ApplicationProcedureName],
	[Section],
	[IndicationOfGeneralDepartmentOfCustoms],
	[IndicationOfDisapproval],
	[IndicationOfCancellation],
	[IndicationOfCompletionOfExamination],
	[NumberOfYearsForSavingTheOriginalDocumentData],
	[ExpiryDate],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@ApplicationProcedureType,
	@ApplicationProcedureName,
	@Section,
	@IndicationOfGeneralDepartmentOfCustoms,
	@IndicationOfDisapproval,
	@IndicationOfCancellation,
	@IndicationOfCompletionOfExamination,
	@NumberOfYearsForSavingTheOriginalDocumentData,
	@ExpiryDate,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ApplicationProcedureType varchar(5),
	@ApplicationProcedureName nvarchar(250),
	@Section int,
	@IndicationOfGeneralDepartmentOfCustoms int,
	@IndicationOfDisapproval int,
	@IndicationOfCancellation int,
	@IndicationOfCompletionOfExamination int,
	@NumberOfYearsForSavingTheOriginalDocumentData int,
	@ExpiryDate datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_ApplicationProcedureType]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[ApplicationProcedureName] = @ApplicationProcedureName,
	[Section] = @Section,
	[IndicationOfGeneralDepartmentOfCustoms] = @IndicationOfGeneralDepartmentOfCustoms,
	[IndicationOfDisapproval] = @IndicationOfDisapproval,
	[IndicationOfCancellation] = @IndicationOfCancellation,
	[IndicationOfCompletionOfExamination] = @IndicationOfCompletionOfExamination,
	[NumberOfYearsForSavingTheOriginalDocumentData] = @NumberOfYearsForSavingTheOriginalDocumentData,
	[ExpiryDate] = @ExpiryDate,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ApplicationProcedureType] = @ApplicationProcedureType

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ApplicationProcedureType varchar(5),
	@ApplicationProcedureName nvarchar(250),
	@Section int,
	@IndicationOfGeneralDepartmentOfCustoms int,
	@IndicationOfDisapproval int,
	@IndicationOfCancellation int,
	@IndicationOfCompletionOfExamination int,
	@NumberOfYearsForSavingTheOriginalDocumentData int,
	@ExpiryDate datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ApplicationProcedureType] FROM [dbo].[t_VNACC_Category_ApplicationProcedureType] WHERE [ApplicationProcedureType] = @ApplicationProcedureType)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_ApplicationProcedureType] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[ApplicationProcedureName] = @ApplicationProcedureName,
			[Section] = @Section,
			[IndicationOfGeneralDepartmentOfCustoms] = @IndicationOfGeneralDepartmentOfCustoms,
			[IndicationOfDisapproval] = @IndicationOfDisapproval,
			[IndicationOfCancellation] = @IndicationOfCancellation,
			[IndicationOfCompletionOfExamination] = @IndicationOfCompletionOfExamination,
			[NumberOfYearsForSavingTheOriginalDocumentData] = @NumberOfYearsForSavingTheOriginalDocumentData,
			[ExpiryDate] = @ExpiryDate,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ApplicationProcedureType] = @ApplicationProcedureType
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_ApplicationProcedureType]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[ApplicationProcedureType],
			[ApplicationProcedureName],
			[Section],
			[IndicationOfGeneralDepartmentOfCustoms],
			[IndicationOfDisapproval],
			[IndicationOfCancellation],
			[IndicationOfCompletionOfExamination],
			[NumberOfYearsForSavingTheOriginalDocumentData],
			[ExpiryDate],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@ApplicationProcedureType,
			@ApplicationProcedureName,
			@Section,
			@IndicationOfGeneralDepartmentOfCustoms,
			@IndicationOfDisapproval,
			@IndicationOfCancellation,
			@IndicationOfCompletionOfExamination,
			@NumberOfYearsForSavingTheOriginalDocumentData,
			@ExpiryDate,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]
	@ApplicationProcedureType varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_ApplicationProcedureType]
WHERE
	[ApplicationProcedureType] = @ApplicationProcedureType

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_ApplicationProcedureType] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Load]
	@ApplicationProcedureType varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ApplicationProcedureType],
	[ApplicationProcedureName],
	[Section],
	[IndicationOfGeneralDepartmentOfCustoms],
	[IndicationOfDisapproval],
	[IndicationOfCancellation],
	[IndicationOfCompletionOfExamination],
	[NumberOfYearsForSavingTheOriginalDocumentData],
	[ExpiryDate],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_ApplicationProcedureType]
WHERE
	[ApplicationProcedureType] = @ApplicationProcedureType
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ApplicationProcedureType],
	[ApplicationProcedureName],
	[Section],
	[IndicationOfGeneralDepartmentOfCustoms],
	[IndicationOfDisapproval],
	[IndicationOfCancellation],
	[IndicationOfCompletionOfExamination],
	[NumberOfYearsForSavingTheOriginalDocumentData],
	[ExpiryDate],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_ApplicationProcedureType] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ApplicationProcedureType],
	[ApplicationProcedureName],
	[Section],
	[IndicationOfGeneralDepartmentOfCustoms],
	[IndicationOfDisapproval],
	[IndicationOfCancellation],
	[IndicationOfCompletionOfExamination],
	[NumberOfYearsForSavingTheOriginalDocumentData],
	[ExpiryDate],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_ApplicationProcedureType]	

GO

