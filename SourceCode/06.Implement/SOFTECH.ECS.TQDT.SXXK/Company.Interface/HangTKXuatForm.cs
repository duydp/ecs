﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ThanhKhoan;
using Company.BLL.SXXK.ToKhai;
using Company.BLL;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;
using Company.QuanTri;
using Company.Interface.SXXK;
using System.IO;
using System.Diagnostics;
namespace Company.Interface
{
    public partial class HangTKXuatForm : BaseForm
    {
        public HangTKXuatForm()
        {
            InitializeComponent();
        }
        public Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection tkmaudichColl = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
        public Company.BLL.SXXK.ToKhai.HangMauDichCollection hangmaudichColl = new Company.BLL.SXXK.ToKhai.HangMauDichCollection();
        private Company.BLL.SXXK.SanPham sanpham = new Company.BLL.SXXK.SanPham();
        public Company.BLL.SXXK.ToKhai.HangMauDich hangmaudich = new Company.BLL.SXXK.ToKhai.HangMauDich();

        private void HangTon_Load(object sender, EventArgs e)
        {
            dgList.ColumnAutoResize = false;
            dgList.RootTable.HeaderLines = 2;

            cbTrangThai.SelectedIndex = 0;
            nuocHControl1.Enabled = false;
            //dgList.Tables[0].Columns["Luong"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            // dgList.Tables[0].Columns["Ton"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            //  dgList.Tables[0].Columns["Luong"].TotalFormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            //  dgList.Tables[0].Columns["Ton"].TotalFormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = new Company.BLL.SXXK.SanPham().SelectDynamic("MaDoanhNghiep = '" + MaDoanhNghiep + "'", "").Tables[0];
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["Ma"].ToString());
            txtMaNPL.AutoCompleteCustomSource = col;
            //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
            //{
            //    uiButton2.Visible = false;
            //}
            //if (!((SiteIdentity)MainForm.EcsQuanTri.Identity).user.isAdmin)
            //{
            //    uiButton4.Visible = false;
            //}
            txtNamTiepNhan.Text = DateTime.Today.Year.ToString();

            //search();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            ToKhaiMauDich tk = new ToKhaiMauDich();
            if (dgList.GetRow().RowType == RowType.Record)
            {
                GridEXRow dr = dgList.GetRow();
                Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)dr.DataRow;
                tk.MaHaiQuan = npl.MaHaiQuan;
                tk.SoToKhai = npl.SoToKhai;
                tk.MaLoaiHinh = npl.MaLoaiHinh;
                tk.NamDangKy = npl.NamDangKy;

            }
            else if (dgList.GetRow().RowType == RowType.GroupHeader)
            {
                object[] obj = (object[])dgList.GetRow().GroupValue;
                string a = obj[0].ToString();
                tk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                tk.SoToKhai = (int)obj[0];
                tk.MaLoaiHinh = obj[1].ToString();
                tk.NamDangKy = (short)Convert.ToDateTime(obj[2].ToString()).Year;
            }

            tk.Load();

            ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
            f.TKMD = tk;
            f.NhomLoaiHinh = tk.MaLoaiHinh.Substring(0, 3);
            f.MdiParent = this.ParentForm;
            f.Name = "Tờ khai xuất khẩu số " + tk.SoToKhai + "/" + tk.NamDangKy;
            f.Show();
        }

        //DATLMQ bổ sung tìm kiếm theo NPL tờ khai xuất ngày 10/06/2011
        private DataTable dtNPL;
        private DataTable dtTongLuongNPL;
        private void searchNPL()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (chkOnlyNPL.Checked == false)
                {
                    if (dtNPL == null || dtNPL.Rows.Count == 0)
                        if (txtNamTiepNhan.Text.Trim().Length > 0)
                            dtNPL = new HangMauDich().GetNPLFromTKXuat(int.Parse(txtNamTiepNhan.Text.Trim())).Tables[0];
                        else
                            dtNPL = new HangMauDich().GetNPLFromTKXuat().Tables[0];
                    dgList.DataSource = dtNPL;
                    if (chkTimChinhXacNPL.Checked)
                        dtNPL.DefaultView.RowFilter = "MaNguyenPhuLieu = '" + txtMaNguyenPhuLieu.Text.Trim() + "'";
                    else
                        dtNPL.DefaultView.RowFilter = "MaNguyenPhuLieu LIKE '%" + txtMaNguyenPhuLieu.Text.Trim() + "%'";

                    if (dgList.RootTable.Columns.Contains("MaPhu"))
                        dgList.RootTable.Columns["MaPhu"].Visible = true;
                    if (dgList.RootTable.Columns.Contains("TenHang"))
                        dgList.RootTable.Columns["TenHang"].Visible = true;
                    if (dgList.RootTable.Columns.Contains("SoLuong"))
                        dgList.RootTable.Columns["SoLuong"].Visible = true;
                    if (dgList.RootTable.Columns.Contains("DonGiaKB"))
                        dgList.RootTable.Columns["DonGiaKB"].Visible = true;
                    if (dgList.RootTable.Columns.Contains("TriGiaKB"))
                        dgList.RootTable.Columns["TriGiaKB"].Visible = true;
                    if (dgList.RootTable.Columns.Contains("ThueXNK"))
                        dgList.RootTable.Columns["ThueXNK"].Visible = true;

                    dgList.RootTable.Columns["MaNguyenPhuLieu"].Visible = true;
                    dgList.RootTable.Columns["MaNguyenPhuLieu"].Caption = "Mã Nguyên phụ liệu";
                    dgList.RootTable.Columns["LuongNPL"].Visible = true;
                    dgList.RootTable.Columns["LuongNPL"].Caption = "Lượng Nguyên phụ liệu";
                    dgList.RootTable.Columns["MaDoanhNghiep"].Visible = true;
                    dgList.RootTable.Columns["MaDoanhNghiep"].Caption = "Mã doanh nghiệp";
                    if (dgList.RootTable.Columns.Contains("DinhMucSuDung"))
                    {
                        dgList.RootTable.Columns["DinhMucSuDung"].Visible = true;
                        dgList.RootTable.Columns["DinhMucSuDung"].Caption = "Định mức sử dụng";
                    }
                    if (dgList.RootTable.Columns.Contains("TyLeHaoHut"))
                    {
                        dgList.RootTable.Columns["TyLeHaoHut"].Visible = true;
                        dgList.RootTable.Columns["TyLeHaoHut"].Caption = "Tỷ lệ hao hụt";
                    }
                }
                else
                {
                    ChiHienThiNPL();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình tìm kiếm dữ liệu.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ChiHienThiNPL()
        {
            if (txtNamTiepNhan.Text.Trim().Length > 0)
                dtTongLuongNPL = new HangMauDich().GetTongLuongNPLFromTKXuat(int.Parse(txtNamTiepNhan.Text.Trim())).Tables[0];
            else
                dtTongLuongNPL = new HangMauDich().GetTongLuongNPLFromTKXuat().Tables[0];

            dgList.DataSource = dtTongLuongNPL;
            if (chkTimChinhXacNPL.Checked)
                dtTongLuongNPL.DefaultView.RowFilter = "MaNguyenPhuLieu = '" + txtMaNguyenPhuLieu.Text.Trim() + "'";
            else
                dtTongLuongNPL.DefaultView.RowFilter = "MaNguyenPhuLieu LIKE '%" + txtMaNguyenPhuLieu.Text.Trim() + "%'";

            if (dgList.RootTable.Columns.Contains("MaPhu"))
                dgList.RootTable.Columns["MaPhu"].Visible = false;
            if (dgList.RootTable.Columns.Contains("TenHang"))
                dgList.RootTable.Columns["TenHang"].Visible = false;
            if (dgList.RootTable.Columns.Contains("SoLuong"))
                dgList.RootTable.Columns["SoLuong"].Visible = false;
            if (dgList.RootTable.Columns.Contains("DonGiaKB"))
                dgList.RootTable.Columns["DonGiaKB"].Visible = false;
            if (dgList.RootTable.Columns.Contains("TriGiaKB"))
                dgList.RootTable.Columns["TriGiaKB"].Visible = false;
            if (dgList.RootTable.Columns.Contains("ThueXNK"))
                dgList.RootTable.Columns["ThueXNK"].Visible = false;


            dgList.RootTable.Columns["MaNguyenPhuLieu"].Visible = true;
            dgList.RootTable.Columns["MaNguyenPhuLieu"].Caption = "Mã Nguyên phụ liệu";
            dgList.RootTable.Columns["LuongNPL"].Visible = true;
            dgList.RootTable.Columns["LuongNPL"].Caption = "Lượng Nguyên phụ liệu";
            dgList.RootTable.Columns["MaDoanhNghiep"].Visible = false;
            if (dgList.RootTable.Columns.Contains("DinhMucSuDung"))
                dgList.RootTable.Columns["DinhMucSuDung"].Visible = false;
            if (dgList.RootTable.Columns.Contains("TyLeHaoHut"))
                dgList.RootTable.Columns["TyLeHaoHut"].Visible = false;
        }

        private void search()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                // Xây dựng điều kiện tìm kiếm.
                //string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "'";
                string where = "";
                where += string.Format(" MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoToKhai = " + txtSoTiepNhan.Value;
                }

                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND NamDangKy = " + txtNamTiepNhan.Value;
                }
                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND MaPhu = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND MaPhu LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }
                string temp = "";
                if (cbTrangThai.SelectedIndex > 0)
                {
                    temp += " AND ThanhLy = '" + cbTrangThai.SelectedValue.ToString() + "'";
                }

                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                    {
                        temp += " AND TenChuHang LIKE '%" + txtTenChuHang.Text.Trim() + "%'";
                    }
                }
                else
                {
                    temp += " AND TenChuHang LIKE ''";

                }
                string temp2 = "";
                if (nuocHControl1.Enabled == true)
                {
                    temp2 = " And NuocNK_ID='" + nuocHControl1.Ma + "'";
                }

                where += " AND Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan IN " +
               "(SELECT Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan FROM dbo.t_SXXK_ToKhaiMauDich WHERE MaLoaiHinh LIKE 'X%'" + temp + " And MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'" + temp2 + ")";

                // Thực hiện tìm kiếm.     
                //hangmaudichColl = hangmaudich.SelectCollectionDynamic(where, "");           
                //dgList.DataSource = tkmaudichColl;
                DataTable dt = hangmaudich.SelectDynamic(where, "").Tables[0];
                dgList.DataSource = dt;

                if (dgList.RootTable.Columns.Contains("MaPhu"))
                    dgList.RootTable.Columns["MaPhu"].Visible = true;
                if (dgList.RootTable.Columns.Contains("TenHang"))
                    dgList.RootTable.Columns["TenHang"].Visible = true;
                if (dgList.RootTable.Columns.Contains("SoLuong"))
                    dgList.RootTable.Columns["SoLuong"].Visible = true;
                if (dgList.RootTable.Columns.Contains("DonGiaKB"))
                    dgList.RootTable.Columns["DonGiaKB"].Visible = true;
                if (dgList.RootTable.Columns.Contains("TriGiaKB"))
                    dgList.RootTable.Columns["TriGiaKB"].Visible = true;
                if (dgList.RootTable.Columns.Contains("ThueXNK"))
                    dgList.RootTable.Columns["ThueXNK"].Visible = true;

                if (dgList.RootTable.Columns.Contains("MaNguyenPhuLieu"))
                    dgList.RootTable.Columns["MaNguyenPhuLieu"].Visible = false;
                if (dgList.RootTable.Columns.Contains("LuongNPL"))
                    dgList.RootTable.Columns["LuongNPL"].Visible = false;
                if (dgList.RootTable.Columns.Contains("MaDoanhNghiep"))
                    dgList.RootTable.Columns["MaDoanhNghiep"].Visible = false;
                if (dgList.RootTable.Columns.Contains("DinhMucSuDung"))
                    dgList.RootTable.Columns["DinhMucSuDung"].Visible = false;
                if (dgList.RootTable.Columns.Contains("TyLeHaoHut"))
                    dgList.RootTable.Columns["TyLeHaoHut"].Visible = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình tìm kiếm dữ liệu.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtMaNguyenPhuLieu.TextLength > 0 || chkOnlyNPL.Checked == true)
            {
                this.searchNPL();
                //mnuGrid.Visible = false;
            }
            else
                this.search();
        }


        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                gridEXPrintDocument1.Print();
            }
            catch { ShowMessage(" Lỗi máy in, vui lòng kiểm tra lại ", false); }
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            try
            {
                new Company.BLL.KDT.SXXK.HoSoThanhLyDangKy().FixOldData(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                //ShowMessage("Điều chỉnh thành công.", false);
                MLMessages("Điều chỉnh thành công.", "MSG_THK99", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
        }

        private void cbTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTrangThai.SelectedIndex != 0)
                this.search();
        }

        private void mnuGrid_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "toolStripThanhkhoan":
                    this.doThanhKhoan();
                    break;
            }
        }

        private void doThanhKhoan()
        {
            try
            {
                if (dgList.GetRow().RowType == RowType.Record)
                {
                    GridEXRow dr = dgList.GetRow();
                    Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)dr.DataRow;
                    TheoDoiThanhKhoanNPLToKhaiForm f = new TheoDoiThanhKhoanNPLToKhaiForm();
                    f.NPL = npl;
                    f.ShowDialog();

                }
                else if (dgList.GetRow().RowType == RowType.GroupHeader)
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    object[] obj = (object[])dgList.GetRow().GroupValue;
                    string a = obj[0].ToString();
                    tk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    tk.SoToKhai = (int)obj[0];
                    tk.MaLoaiHinh = obj[1].ToString();
                    tk.NamDangKy = (short)Convert.ToDateTime(obj[2]).Year;
                    tk.Load();
                    TheoDoiThanhKhoanToKhaiForm f = new TheoDoiThanhKhoanToKhaiForm();
                    f.TKMD = tk;
                    f.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi trong quá trình xem thông tin.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }
        private DateTime getMaxDateBKTKX(HoSoThanhLyDangKy HSTL)
        {
            int i = HSTL.getBKToKhaiXuat();
            HSTL.BKCollection[i].LoadChiTietBangKe();
            BKToKhaiXuatCollection bkTKXCollection = HSTL.BKCollection[i].bkTKXColletion;
            DateTime dt = bkTKXCollection[0].NgayDangKy;
            for (int j = 1; j < bkTKXCollection.Count; j++)
            {
                if (dt < bkTKXCollection[j].NgayDangKy) dt = bkTKXCollection[j].NgayDangKy;
            }
            return dt;
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TenHang"].Text == "")
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }
            }
        }

        private void dgList_EditingCell(object sender, EditingCellEventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)row.DataRow;
            if (npl.CheckMaNPLDaThanhKhoan())
            {
                ShowMessage("Nguyên phụ liệu '" + npl.MaNPL + "' của tờ khai " + npl.SoToKhai + "/" + npl.NamDangKy + " đã thanh khoản máy, không thể sửa.", false);
                e.Cancel = true;
            }
        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {

            dgList.Tables[0].Columns["SoToKhai"].Visible = true;
            dgList.Tables[0].Columns["NgayDangKy"].Visible = true;
            dgList.Tables[0].Columns["MaLoaiHinh"].Visible = true;
            GridEXGroup customG = dgList.Tables[0].Groups[0].Clone();
            dgList.Tables[0].Groups.Clear();
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog();
            if (sfNPL.FileName != "")
            {
                Stream str = sfNPL.OpenFile();
                gridEXExporter1.Export(str);
                str.Close();
                dgList.Tables[0].Groups.Add(customG);
                dgList.Tables[0].Columns["SoToKhai"].Visible = false;
                dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
                dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;

                if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
            else
            {
                dgList.Tables[0].Columns["SoToKhai"].Visible = false;
                dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
                dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;
                dgList.Tables[0].Groups.Add(customG);
            }


        }

        private void uiButton5_Click(object sender, EventArgs e)
        {
            dgList.ShowFieldChooser();
            dgList.SaveSettings = true;
        }

        private void chkNuoc_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNuoc.Checked)
            {
                nuocHControl1.Enabled = true;
            }
            else
            {
                nuocHControl1.Enabled = false;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }



    }
}