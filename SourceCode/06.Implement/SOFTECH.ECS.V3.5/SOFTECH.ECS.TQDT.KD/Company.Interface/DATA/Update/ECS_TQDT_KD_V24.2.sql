GO
ALTER TABLE dbo.t_KDT_ContainerDangKy
ALTER COLUMN Note NVARCHAR(500)
GO
ALTER TABLE t_KDT_ContainerDangKy 
ALTER COLUMN WeightUnitCode NVARCHAR(100)
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ThoiGianXuatDL datetime,
	@LuongTK int,
	@TrangThaiToKhai int,
	@CustomsStatus nvarchar(4),
	@Note nvarchar(500),
	@CargoPiece varchar(100),
	@PieceUnitCode varchar(100),
	@CargoWeight varchar(100),
	@WeightUnitCode nvarchar(100),
	@LocationControl nvarchar(7),
	@CargoCtrlNo nvarchar(255),
	@TransportationCode int,
	@ArrivalDeparture datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ContainerDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai],
	[CustomsStatus],
	[Note],
	[CargoPiece],
	[PieceUnitCode],
	[CargoWeight],
	[WeightUnitCode],
	[LocationControl],
	[CargoCtrlNo],
	[TransportationCode],
	[ArrivalDeparture]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@TKMD_ID,
	@TrangThaiXuLy,
	@MaHQ,
	@MaDoanhNghiep,
	@GuidStr,
	@DeXuatKhac,
	@LyDoSua,
	@ThoiGianXuatDL,
	@LuongTK,
	@TrangThaiToKhai,
	@CustomsStatus,
	@Note,
	@CargoPiece,
	@PieceUnitCode,
	@CargoWeight,
	@WeightUnitCode,
	@LocationControl,
	@CargoCtrlNo,
	@TransportationCode,
	@ArrivalDeparture
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ThoiGianXuatDL datetime,
	@LuongTK int,
	@TrangThaiToKhai int,
	@CustomsStatus nvarchar(4),
	@Note nvarchar(500),
	@CargoPiece varchar(100),
	@PieceUnitCode varchar(100),
	@CargoWeight varchar(100),
	@WeightUnitCode nvarchar(100),
	@LocationControl nvarchar(7),
	@CargoCtrlNo nvarchar(255),
	@TransportationCode int,
	@ArrivalDeparture datetime
AS

UPDATE
	[dbo].[t_KDT_ContainerDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TKMD_ID] = @TKMD_ID,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[GuidStr] = @GuidStr,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ThoiGianXuatDL] = @ThoiGianXuatDL,
	[LuongTK] = @LuongTK,
	[TrangThaiToKhai] = @TrangThaiToKhai,
	[CustomsStatus] = @CustomsStatus,
	[Note] = @Note,
	[CargoPiece] = @CargoPiece,
	[PieceUnitCode] = @PieceUnitCode,
	[CargoWeight] = @CargoWeight,
	[WeightUnitCode] = @WeightUnitCode,
	[LocationControl] = @LocationControl,
	[CargoCtrlNo] = @CargoCtrlNo,
	[TransportationCode] = @TransportationCode,
	[ArrivalDeparture] = @ArrivalDeparture
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ThoiGianXuatDL datetime,
	@LuongTK int,
	@TrangThaiToKhai int,
	@CustomsStatus nvarchar(4),
	@Note nvarchar(500),
	@CargoPiece varchar(100),
	@PieceUnitCode varchar(100),
	@CargoWeight varchar(100),
	@WeightUnitCode nvarchar(100),
	@LocationControl nvarchar(7),
	@CargoCtrlNo nvarchar(255),
	@TransportationCode int,
	@ArrivalDeparture datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ContainerDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ContainerDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TKMD_ID] = @TKMD_ID,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[GuidStr] = @GuidStr,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ThoiGianXuatDL] = @ThoiGianXuatDL,
			[LuongTK] = @LuongTK,
			[TrangThaiToKhai] = @TrangThaiToKhai,
			[CustomsStatus] = @CustomsStatus,
			[Note] = @Note,
			[CargoPiece] = @CargoPiece,
			[PieceUnitCode] = @PieceUnitCode,
			[CargoWeight] = @CargoWeight,
			[WeightUnitCode] = @WeightUnitCode,
			[LocationControl] = @LocationControl,
			[CargoCtrlNo] = @CargoCtrlNo,
			[TransportationCode] = @TransportationCode,
			[ArrivalDeparture] = @ArrivalDeparture
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ContainerDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[TKMD_ID],
			[TrangThaiXuLy],
			[MaHQ],
			[MaDoanhNghiep],
			[GuidStr],
			[DeXuatKhac],
			[LyDoSua],
			[ThoiGianXuatDL],
			[LuongTK],
			[TrangThaiToKhai],
			[CustomsStatus],
			[Note],
			[CargoPiece],
			[PieceUnitCode],
			[CargoWeight],
			[WeightUnitCode],
			[LocationControl],
			[CargoCtrlNo],
			[TransportationCode],
			[ArrivalDeparture]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@TKMD_ID,
			@TrangThaiXuLy,
			@MaHQ,
			@MaDoanhNghiep,
			@GuidStr,
			@DeXuatKhac,
			@LyDoSua,
			@ThoiGianXuatDL,
			@LuongTK,
			@TrangThaiToKhai,
			@CustomsStatus,
			@Note,
			@CargoPiece,
			@PieceUnitCode,
			@CargoWeight,
			@WeightUnitCode,
			@LocationControl,
			@CargoCtrlNo,
			@TransportationCode,
			@ArrivalDeparture
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ContainerDangKy]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ContainerDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai],
	[CustomsStatus],
	[Note],
	[CargoPiece],
	[PieceUnitCode],
	[CargoWeight],
	[WeightUnitCode],
	[LocationControl],
	[CargoCtrlNo],
	[TransportationCode],
	[ArrivalDeparture]
FROM
	[dbo].[t_KDT_ContainerDangKy]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai],
	[CustomsStatus],
	[Note],
	[CargoPiece],
	[PieceUnitCode],
	[CargoWeight],
	[WeightUnitCode],
	[LocationControl],
	[CargoCtrlNo],
	[TransportationCode],
	[ArrivalDeparture]
FROM [dbo].[t_KDT_ContainerDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectAll]























AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai],
	[CustomsStatus],
	[Note],
	[CargoPiece],
	[PieceUnitCode],
	[CargoWeight],
	[WeightUnitCode],
	[LocationControl],
	[CargoCtrlNo],
	[TransportationCode],
	[ArrivalDeparture]
FROM
	[dbo].[t_KDT_ContainerDangKy]	

GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '24.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('24.2',GETDATE(), N'CẬP NHẬT PROCDEDURE CONTAINER')
END