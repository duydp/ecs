﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Components
{
	public partial class KDT_VNACC_HopDongDangKy : ICloneable
	{
        private List<KDT_VNACC_HopDongDangKy_ChiTiet> _listHopDongDangKyChiTiet = new List<KDT_VNACC_HopDongDangKy_ChiTiet>();

        public List<KDT_VNACC_HopDongDangKy_ChiTiet> hopDongDangKyChiTietCollection
        {
            set { this.hopDongDangKyChiTietCollection = value; }
            get { return this.hopDongDangKyChiTietCollection; }
        }

        public void LoadListHopDongDangKy_ChiTiet()
        {
            _listHopDongDangKyChiTiet = KDT_VNACC_HopDongDangKy_ChiTiet.SelectCollectionBy_HopDongID(this.ID);
        }
		#region Properties.
		
		public int ID { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public string LoaiChungTu { set; get; }
		public string SoThamChieuCT { set; get; }
		public DateTime NgayGuiCT { set; get; }
		public decimal ChucNangCuaCT { set; get; }
		public string NoiKhaiBao { set; get; }
		public string TrangThaiCT { set; get; }
		public string SoDangKyCT { set; get; }
		public DateTime NgayDangKyCT { set; get; }
		public string HaiQuanTiepNhan { set; get; }
		public string TenNguoiKhaiHQ { set; get; }
		public string MaNguoiKhaiHQ { set; get; }
		public decimal TrangThaiDaiLy { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string MaDoanhNghiep { set; get; }
		public decimal SoToKhai { set; get; }
		public DateTime NgayDangKyTK { set; get; }
		public string MaLoaiHinh { set; get; }
		public string MaHaiQuan { set; get; }
		public string SoHopDong { set; get; }
		public DateTime NgayHopDong { set; get; }
		public DateTime ThoiHanThanhToan { set; get; }
		public decimal TongTriGia { set; get; }
		public string GhiChuKhac { set; get; }
        public string TrangThaiXuLy { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HopDongDangKy> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HopDongDangKy> collection = new List<KDT_VNACC_HopDongDangKy>();
			while (reader.Read())
			{
				KDT_VNACC_HopDongDangKy entity = new KDT_VNACC_HopDongDangKy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetString(reader.GetOrdinal("LoaiChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThamChieuCT"))) entity.SoThamChieuCT = reader.GetString(reader.GetOrdinal("SoThamChieuCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGuiCT"))) entity.NgayGuiCT = reader.GetDateTime(reader.GetOrdinal("NgayGuiCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChucNangCuaCT"))) entity.ChucNangCuaCT = reader.GetDecimal(reader.GetOrdinal("ChucNangCuaCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiKhaiBao"))) entity.NoiKhaiBao = reader.GetString(reader.GetOrdinal("NoiKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiCT"))) entity.TrangThaiCT = reader.GetString(reader.GetOrdinal("TrangThaiCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDangKyCT"))) entity.SoDangKyCT = reader.GetString(reader.GetOrdinal("SoDangKyCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyCT"))) entity.NgayDangKyCT = reader.GetDateTime(reader.GetOrdinal("NgayDangKyCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("HaiQuanTiepNhan"))) entity.HaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("HaiQuanTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhaiHQ"))) entity.TenNguoiKhaiHQ = reader.GetString(reader.GetOrdinal("TenNguoiKhaiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhaiHQ"))) entity.MaNguoiKhaiHQ = reader.GetString(reader.GetOrdinal("MaNguoiKhaiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiDaiLy"))) entity.TrangThaiDaiLy = reader.GetDecimal(reader.GetOrdinal("TrangThaiDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyTK"))) entity.NgayDangKyTK = reader.GetDateTime(reader.GetOrdinal("NgayDangKyTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanThanhToan"))) entity.ThoiHanThanhToan = reader.GetDateTime(reader.GetOrdinal("ThoiHanThanhToan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGia"))) entity.TongTriGia = reader.GetDecimal(reader.GetOrdinal("TongTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuKhac"))) entity.GhiChuKhac = reader.GetString(reader.GetOrdinal("GhiChuKhac"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HopDongDangKy> collection, int id)
        {
            foreach (KDT_VNACC_HopDongDangKy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HopDongDangKy VALUES(@InputMessageID, @MessageTag, @IndexTag, @LoaiChungTu, @SoThamChieuCT, @NgayGuiCT, @ChucNangCuaCT, @NoiKhaiBao, @TrangThaiCT, @SoDangKyCT, @NgayDangKyCT, @HaiQuanTiepNhan, @TenNguoiKhaiHQ, @MaNguoiKhaiHQ, @TrangThaiDaiLy, @TenDoanhNghiep, @MaDoanhNghiep, @SoToKhai, @NgayDangKyTK, @MaLoaiHinh, @MaHaiQuan, @SoHopDong, @NgayHopDong, @ThoiHanThanhToan, @TongTriGia, @GhiChuKhac)";
            string update = "UPDATE t_KDT_VNACC_HopDongDangKy SET InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, LoaiChungTu = @LoaiChungTu, SoThamChieuCT = @SoThamChieuCT, NgayGuiCT = @NgayGuiCT, ChucNangCuaCT = @ChucNangCuaCT, NoiKhaiBao = @NoiKhaiBao, TrangThaiCT = @TrangThaiCT, SoDangKyCT = @SoDangKyCT, NgayDangKyCT = @NgayDangKyCT, HaiQuanTiepNhan = @HaiQuanTiepNhan, TenNguoiKhaiHQ = @TenNguoiKhaiHQ, MaNguoiKhaiHQ = @MaNguoiKhaiHQ, TrangThaiDaiLy = @TrangThaiDaiLy, TenDoanhNghiep = @TenDoanhNghiep, MaDoanhNghiep = @MaDoanhNghiep, SoToKhai = @SoToKhai, NgayDangKyTK = @NgayDangKyTK, MaLoaiHinh = @MaLoaiHinh, MaHaiQuan = @MaHaiQuan, SoHopDong = @SoHopDong, NgayHopDong = @NgayHopDong, ThoiHanThanhToan = @ThoiHanThanhToan, TongTriGia = @TongTriGia, GhiChuKhac = @GhiChuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HopDongDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.NVarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThamChieuCT", SqlDbType.NVarChar, "SoThamChieuCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGuiCT", SqlDbType.DateTime, "NgayGuiCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangCuaCT", SqlDbType.Decimal, "ChucNangCuaCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiKhaiBao", SqlDbType.NVarChar, "NoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiCT", SqlDbType.NVarChar, "TrangThaiCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyCT", SqlDbType.NVarChar, "SoDangKyCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyCT", SqlDbType.DateTime, "NgayDangKyCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HaiQuanTiepNhan", SqlDbType.NVarChar, "HaiQuanTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiDaiLy", SqlDbType.Decimal, "TrangThaiDaiLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyTK", SqlDbType.NVarChar, "NgayDangKyTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.NVarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDong", SqlDbType.NVarChar, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanThanhToan", SqlDbType.NVarChar, "ThoiHanThanhToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGia", SqlDbType.Decimal, "TongTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.NVarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThamChieuCT", SqlDbType.NVarChar, "SoThamChieuCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGuiCT", SqlDbType.DateTime, "NgayGuiCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangCuaCT", SqlDbType.Decimal, "ChucNangCuaCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiKhaiBao", SqlDbType.NVarChar, "NoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiCT", SqlDbType.NVarChar, "TrangThaiCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyCT", SqlDbType.NVarChar, "SoDangKyCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyCT", SqlDbType.DateTime, "NgayDangKyCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HaiQuanTiepNhan", SqlDbType.NVarChar, "HaiQuanTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiDaiLy", SqlDbType.Decimal, "TrangThaiDaiLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyTK", SqlDbType.NVarChar, "NgayDangKyTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.NVarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDong", SqlDbType.NVarChar, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanThanhToan", SqlDbType.NVarChar, "ThoiHanThanhToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGia", SqlDbType.Decimal, "TongTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HopDongDangKy VALUES(@InputMessageID, @MessageTag, @IndexTag, @LoaiChungTu, @SoThamChieuCT, @NgayGuiCT, @ChucNangCuaCT, @NoiKhaiBao, @TrangThaiCT, @SoDangKyCT, @NgayDangKyCT, @HaiQuanTiepNhan, @TenNguoiKhaiHQ, @MaNguoiKhaiHQ, @TrangThaiDaiLy, @TenDoanhNghiep, @MaDoanhNghiep, @SoToKhai, @NgayDangKyTK, @MaLoaiHinh, @MaHaiQuan, @SoHopDong, @NgayHopDong, @ThoiHanThanhToan, @TongTriGia, @GhiChuKhac)";
            string update = "UPDATE t_KDT_VNACC_HopDongDangKy SET InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, LoaiChungTu = @LoaiChungTu, SoThamChieuCT = @SoThamChieuCT, NgayGuiCT = @NgayGuiCT, ChucNangCuaCT = @ChucNangCuaCT, NoiKhaiBao = @NoiKhaiBao, TrangThaiCT = @TrangThaiCT, SoDangKyCT = @SoDangKyCT, NgayDangKyCT = @NgayDangKyCT, HaiQuanTiepNhan = @HaiQuanTiepNhan, TenNguoiKhaiHQ = @TenNguoiKhaiHQ, MaNguoiKhaiHQ = @MaNguoiKhaiHQ, TrangThaiDaiLy = @TrangThaiDaiLy, TenDoanhNghiep = @TenDoanhNghiep, MaDoanhNghiep = @MaDoanhNghiep, SoToKhai = @SoToKhai, NgayDangKyTK = @NgayDangKyTK, MaLoaiHinh = @MaLoaiHinh, MaHaiQuan = @MaHaiQuan, SoHopDong = @SoHopDong, NgayHopDong = @NgayHopDong, ThoiHanThanhToan = @ThoiHanThanhToan, TongTriGia = @TongTriGia, GhiChuKhac = @GhiChuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HopDongDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.NVarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThamChieuCT", SqlDbType.NVarChar, "SoThamChieuCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGuiCT", SqlDbType.DateTime, "NgayGuiCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangCuaCT", SqlDbType.Decimal, "ChucNangCuaCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiKhaiBao", SqlDbType.NVarChar, "NoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiCT", SqlDbType.NVarChar, "TrangThaiCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyCT", SqlDbType.NVarChar, "SoDangKyCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyCT", SqlDbType.DateTime, "NgayDangKyCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HaiQuanTiepNhan", SqlDbType.NVarChar, "HaiQuanTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiDaiLy", SqlDbType.Decimal, "TrangThaiDaiLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyTK", SqlDbType.NVarChar, "NgayDangKyTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.NVarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDong", SqlDbType.NVarChar, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanThanhToan", SqlDbType.NVarChar, "ThoiHanThanhToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGia", SqlDbType.Decimal, "TongTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.NVarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThamChieuCT", SqlDbType.NVarChar, "SoThamChieuCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGuiCT", SqlDbType.DateTime, "NgayGuiCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangCuaCT", SqlDbType.Decimal, "ChucNangCuaCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiKhaiBao", SqlDbType.NVarChar, "NoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiCT", SqlDbType.NVarChar, "TrangThaiCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyCT", SqlDbType.NVarChar, "SoDangKyCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyCT", SqlDbType.DateTime, "NgayDangKyCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HaiQuanTiepNhan", SqlDbType.NVarChar, "HaiQuanTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiDaiLy", SqlDbType.Decimal, "TrangThaiDaiLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyTK", SqlDbType.NVarChar, "NgayDangKyTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.NVarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDong", SqlDbType.NVarChar, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanThanhToan", SqlDbType.NVarChar, "ThoiHanThanhToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGia", SqlDbType.Decimal, "TongTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HopDongDangKy Load(int id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HopDongDangKy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertKDT_VNACC_HopDongDangKy(string inputMessageID, string messageTag, string indexTag, string loaiChungTu, string soThamChieuCT, DateTime ngayGuiCT, decimal chucNangCuaCT, string noiKhaiBao, string trangThaiCT, string soDangKyCT, DateTime ngayDangKyCT, string haiQuanTiepNhan, string tenNguoiKhaiHQ, string maNguoiKhaiHQ, decimal trangThaiDaiLy, string tenDoanhNghiep, string maDoanhNghiep, decimal soToKhai, DateTime ngayDangKyTK, string maLoaiHinh, string maHaiQuan, string soHopDong, DateTime ngayHopDong, DateTime thoiHanThanhToan, decimal tongTriGia, string ghiChuKhac)
		{
			KDT_VNACC_HopDongDangKy entity = new KDT_VNACC_HopDongDangKy();	
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.LoaiChungTu = loaiChungTu;
			entity.SoThamChieuCT = soThamChieuCT;
			entity.NgayGuiCT = ngayGuiCT;
			entity.ChucNangCuaCT = chucNangCuaCT;
			entity.NoiKhaiBao = noiKhaiBao;
			entity.TrangThaiCT = trangThaiCT;
			entity.SoDangKyCT = soDangKyCT;
			entity.NgayDangKyCT = ngayDangKyCT;
			entity.HaiQuanTiepNhan = haiQuanTiepNhan;
			entity.TenNguoiKhaiHQ = tenNguoiKhaiHQ;
			entity.MaNguoiKhaiHQ = maNguoiKhaiHQ;
			entity.TrangThaiDaiLy = trangThaiDaiLy;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKyTK = ngayDangKyTK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.ThoiHanThanhToan = thoiHanThanhToan;
			entity.TongTriGia = tongTriGia;
			entity.GhiChuKhac = ghiChuKhac;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.NVarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@SoThamChieuCT", SqlDbType.NVarChar, SoThamChieuCT);
			db.AddInParameter(dbCommand, "@NgayGuiCT", SqlDbType.DateTime, NgayGuiCT.Year <= 1753 ? DBNull.Value : (object) NgayGuiCT);
			db.AddInParameter(dbCommand, "@ChucNangCuaCT", SqlDbType.Decimal, ChucNangCuaCT);
			db.AddInParameter(dbCommand, "@NoiKhaiBao", SqlDbType.NVarChar, NoiKhaiBao);
			db.AddInParameter(dbCommand, "@TrangThaiCT", SqlDbType.NVarChar, TrangThaiCT);
			db.AddInParameter(dbCommand, "@SoDangKyCT", SqlDbType.NVarChar, SoDangKyCT);
			db.AddInParameter(dbCommand, "@NgayDangKyCT", SqlDbType.DateTime, NgayDangKyCT.Year <= 1753 ? DBNull.Value : (object) NgayDangKyCT);
			db.AddInParameter(dbCommand, "@HaiQuanTiepNhan", SqlDbType.NVarChar, HaiQuanTiepNhan);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, TenNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, MaNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@TrangThaiDaiLy", SqlDbType.Decimal, TrangThaiDaiLy);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKyTK", SqlDbType.NVarChar, NgayDangKyTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.NVarChar, NgayHopDong);
			db.AddInParameter(dbCommand, "@ThoiHanThanhToan", SqlDbType.NVarChar, ThoiHanThanhToan);
			db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Decimal, TongTriGia);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HopDongDangKy(int id, string inputMessageID, string messageTag, string indexTag, string loaiChungTu, string soThamChieuCT, DateTime ngayGuiCT, decimal chucNangCuaCT, string noiKhaiBao, string trangThaiCT, string soDangKyCT, DateTime ngayDangKyCT, string haiQuanTiepNhan, string tenNguoiKhaiHQ, string maNguoiKhaiHQ, decimal trangThaiDaiLy, string tenDoanhNghiep, string maDoanhNghiep, decimal soToKhai, DateTime ngayDangKyTK, string maLoaiHinh, string maHaiQuan, string soHopDong, DateTime ngayHopDong, DateTime thoiHanThanhToan, decimal tongTriGia, string ghiChuKhac)
		{
			KDT_VNACC_HopDongDangKy entity = new KDT_VNACC_HopDongDangKy();			
			entity.ID = id;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.LoaiChungTu = loaiChungTu;
			entity.SoThamChieuCT = soThamChieuCT;
			entity.NgayGuiCT = ngayGuiCT;
			entity.ChucNangCuaCT = chucNangCuaCT;
			entity.NoiKhaiBao = noiKhaiBao;
			entity.TrangThaiCT = trangThaiCT;
			entity.SoDangKyCT = soDangKyCT;
			entity.NgayDangKyCT = ngayDangKyCT;
			entity.HaiQuanTiepNhan = haiQuanTiepNhan;
			entity.TenNguoiKhaiHQ = tenNguoiKhaiHQ;
			entity.MaNguoiKhaiHQ = maNguoiKhaiHQ;
			entity.TrangThaiDaiLy = trangThaiDaiLy;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKyTK = ngayDangKyTK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.ThoiHanThanhToan = thoiHanThanhToan;
			entity.TongTriGia = tongTriGia;
			entity.GhiChuKhac = ghiChuKhac;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HopDongDangKy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.NVarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@SoThamChieuCT", SqlDbType.NVarChar, SoThamChieuCT);
			db.AddInParameter(dbCommand, "@NgayGuiCT", SqlDbType.DateTime, NgayGuiCT.Year <= 1753 ? DBNull.Value : (object) NgayGuiCT);
			db.AddInParameter(dbCommand, "@ChucNangCuaCT", SqlDbType.Decimal, ChucNangCuaCT);
			db.AddInParameter(dbCommand, "@NoiKhaiBao", SqlDbType.NVarChar, NoiKhaiBao);
			db.AddInParameter(dbCommand, "@TrangThaiCT", SqlDbType.NVarChar, TrangThaiCT);
			db.AddInParameter(dbCommand, "@SoDangKyCT", SqlDbType.NVarChar, SoDangKyCT);
			db.AddInParameter(dbCommand, "@NgayDangKyCT", SqlDbType.DateTime, NgayDangKyCT.Year <= 1753 ? DBNull.Value : (object) NgayDangKyCT);
			db.AddInParameter(dbCommand, "@HaiQuanTiepNhan", SqlDbType.NVarChar, HaiQuanTiepNhan);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, TenNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, MaNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@TrangThaiDaiLy", SqlDbType.Decimal, TrangThaiDaiLy);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKyTK", SqlDbType.NVarChar, NgayDangKyTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.NVarChar, NgayHopDong);
			db.AddInParameter(dbCommand, "@ThoiHanThanhToan", SqlDbType.NVarChar, ThoiHanThanhToan);
			db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Decimal, TongTriGia);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HopDongDangKy(int id, string inputMessageID, string messageTag, string indexTag, string loaiChungTu, string soThamChieuCT, DateTime ngayGuiCT, decimal chucNangCuaCT, string noiKhaiBao, string trangThaiCT, string soDangKyCT, DateTime ngayDangKyCT, string haiQuanTiepNhan, string tenNguoiKhaiHQ, string maNguoiKhaiHQ, decimal trangThaiDaiLy, string tenDoanhNghiep, string maDoanhNghiep, decimal soToKhai, DateTime ngayDangKyTK, string maLoaiHinh, string maHaiQuan, string soHopDong, DateTime ngayHopDong, DateTime thoiHanThanhToan, decimal tongTriGia, string ghiChuKhac)
		{
			KDT_VNACC_HopDongDangKy entity = new KDT_VNACC_HopDongDangKy();			
			entity.ID = id;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.LoaiChungTu = loaiChungTu;
			entity.SoThamChieuCT = soThamChieuCT;
			entity.NgayGuiCT = ngayGuiCT;
			entity.ChucNangCuaCT = chucNangCuaCT;
			entity.NoiKhaiBao = noiKhaiBao;
			entity.TrangThaiCT = trangThaiCT;
			entity.SoDangKyCT = soDangKyCT;
			entity.NgayDangKyCT = ngayDangKyCT;
			entity.HaiQuanTiepNhan = haiQuanTiepNhan;
			entity.TenNguoiKhaiHQ = tenNguoiKhaiHQ;
			entity.MaNguoiKhaiHQ = maNguoiKhaiHQ;
			entity.TrangThaiDaiLy = trangThaiDaiLy;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKyTK = ngayDangKyTK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.ThoiHanThanhToan = thoiHanThanhToan;
			entity.TongTriGia = tongTriGia;
			entity.GhiChuKhac = ghiChuKhac;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.NVarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@SoThamChieuCT", SqlDbType.NVarChar, SoThamChieuCT);
			db.AddInParameter(dbCommand, "@NgayGuiCT", SqlDbType.DateTime, NgayGuiCT.Year <= 1753 ? DBNull.Value : (object) NgayGuiCT);
			db.AddInParameter(dbCommand, "@ChucNangCuaCT", SqlDbType.Decimal, ChucNangCuaCT);
			db.AddInParameter(dbCommand, "@NoiKhaiBao", SqlDbType.NVarChar, NoiKhaiBao);
			db.AddInParameter(dbCommand, "@TrangThaiCT", SqlDbType.NVarChar, TrangThaiCT);
			db.AddInParameter(dbCommand, "@SoDangKyCT", SqlDbType.NVarChar, SoDangKyCT);
			db.AddInParameter(dbCommand, "@NgayDangKyCT", SqlDbType.DateTime, NgayDangKyCT.Year <= 1753 ? DBNull.Value : (object) NgayDangKyCT);
			db.AddInParameter(dbCommand, "@HaiQuanTiepNhan", SqlDbType.NVarChar, HaiQuanTiepNhan);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, TenNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, MaNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@TrangThaiDaiLy", SqlDbType.Decimal, TrangThaiDaiLy);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKyTK", SqlDbType.NVarChar, NgayDangKyTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.NVarChar, NgayHopDong);
			db.AddInParameter(dbCommand, "@ThoiHanThanhToan", SqlDbType.NVarChar, ThoiHanThanhToan);
			db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Decimal, TongTriGia);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HopDongDangKy(int id)
		{
			KDT_VNACC_HopDongDangKy entity = new KDT_VNACC_HopDongDangKy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}