﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;
using Company.GC.BLL.GC;
using System.IO;
namespace Company.GC.BLL.KDT.GC
{
    public partial class DinhMucDangKy
    {
        public DinhMucCollection DMCollection = new DinhMucCollection();
        public string SoHopDong = "";
        public static string strTuChoi = "";
        public void LoadCollection()
        {
            DinhMuc dm = new DinhMuc();
            dm.Master_ID = this.ID;
            DMCollection = dm.SelectCollectionBy_Master_ID();
        }

        //public long SoTiepNhanMax()
        //{
        //    string spName = "Select Max(SoTiepNhan) from t_KDT_GC_DinhMucDangKy";
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
        //    object obj = db.ExecuteScalar(dbCommand);
        //    if (obj == null || string.IsNullOrEmpty(obj.ToString()))
        //        return 0;
        //    else
        //        return (Int64)obj;
        //}
        public void LoadCollection(string dbName)
        {
            DinhMuc dm = new DinhMuc();
            SetDabaseMoi(dbName);
            dm.Master_ID = this.ID;
            DMCollection = dm.SelectCollectionBy_Master_ID();
        }
        protected static List<DinhMucDangKy> ConvertToCollection(IDataReader reader)
        {
            List<DinhMucDangKy> collection = new List<DinhMucDangKy>();
            while (reader.Read())
            {
                DinhMucDangKy entity = new DinhMucDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public static DinhMucDangKy Load(long id)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<DinhMucDangKy> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }
        public void DeleteFull()
        {
            if (this.DMCollection.Count == 0)
                this.LoadCollection();

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {

                    foreach (DinhMuc dm in this.DMCollection)
                    {
                        dm.DeleteTransaction(transaction);
                    }
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateFull()
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.ID == 0)
                        InsertTransaction(transaction);
                    else
                        UpdateTransaction(transaction);
                    int i = 1;
                    foreach (DinhMuc dm in this.DMCollection)
                    {
                        dm.Master_ID = this.ID;
                        dm.STTHang = i++;
                        if (dm.ID > 0)
                            dm.UpdateTransaction(transaction);
                        else
                            dm.InsertTransaction(transaction);
                        #region Đơn vị tính quy đổi và thủ tục HQ trước đó
                        if (dm.DVT_QuyDoi != null)
                        {
                            if (dm.DVT_QuyDoi.IsUse)
                            {
                                dm.DVT_QuyDoi.Master_ID = dm.ID;
                                dm.DVT_QuyDoi.Type = "DMGC";
                                dm.DVT_QuyDoi.InsertUpdate();
                            }
                            else
                            {
                                if (dm.DVT_QuyDoi.ID > 0)
                                    dm.DVT_QuyDoi.Delete();
                            }
                        }
                        if (dm.ThuTucHQTruocDo != null)
                        {

                            dm.ThuTucHQTruocDo.Type = "DMGC";
                            dm.ThuTucHQTruocDo.Master_ID = dm.ID;
                            dm.ThuTucHQTruocDo.InsertUpdateFull();
                        }
                            
                        #endregion

                    }
                    this.DeleteDinhMucGCTransaction(transaction);
                    //this.TransferGCTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public DataSet SelectDynamic(string whereCondition, string orderByExpression, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_DinhMucDangKy_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }
        public static DataSet SelectDynamicFull(string whereCondition, string orderByExpression)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string spName = "p_KDT_GC_DinhMucDangKy_SelectDynamicFull";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
        public DinhMucDangKyCollection SelectCollectionBy_ID_HopDong(string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDongBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);

            DinhMucDangKyCollection collection = new DinhMucDangKyCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                DinhMucDangKy entity = new DinhMucDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //Phiph -----Tim kiem dinh muc kem ma san phan va nguyen phu lieu
        public IDataReader SelectReader_SP_NPL(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_GC_DinhMucDangKy_SelectDynamic_SP_NPL";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }
        public DinhMucDangKyCollection SelectCollection_SP_NPL(string whereCondition, string orderByExpression)
        {
            DinhMucDangKyCollection collection = new DinhMucDangKyCollection();

            IDataReader reader = this.SelectReader_SP_NPL(whereCondition, orderByExpression);
            while (reader.Read())
            {
                DinhMucDangKy entity = new DinhMucDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public DinhMucDangKyCollection SelectCollectionBy_ID_HopDong_ByKTX(string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDongByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);

            DinhMucDangKyCollection collection = new DinhMucDangKyCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                DinhMucDangKy entity = new DinhMucDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public bool Load(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_DinhMucDangKy_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) this._ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) this._NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) this._Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public int InsertUpdate(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_DinhMucDangKy_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.NVarChar, this._NamTN);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public void InsertUpdateTQDT(string dbName)
        {
            SetDabaseMoi(dbName);

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    InsertUpdate(transaction, dbName);

                    DinhMucDangKy dmdkTemp = new DinhMucDangKy();
                    dmdkTemp.SoTiepNhan = this.SoTiepNhan;
                    dmdkTemp.NgayTiepNhan = this.NgayTiepNhan;
                    dmdkTemp.ID_HopDong = this.ID_HopDong;
                    dmdkTemp.MaHaiQuan = this.MaHaiQuan;
                    dmdkTemp.MaDoanhNghiep = this.MaDoanhNghiep;
                    dmdkTemp.Load(transaction, dbName);

                    DataSet ds = dmdkTemp.SelectDynamic(string.Format("SoTiepNhan = {0} AND NgayTiepNhan = '{1}' AND ID_HopDong = {2} AND MaHaiQuan = '{3}' AND MaDoanhNghiep = '{4}'", this.SoTiepNhan, this.NgayTiepNhan, this.ID_HopDong, this.MaHaiQuan, this.MaDoanhNghiep), "", dbName);

                    int i = 1;
                    foreach (DinhMuc dm in this.DMCollection)
                    {
                        dm.Master_ID = dmdkTemp.ID;
                        dm.STTHang = i++;
                        dm.InsertUpdateTransaction(transaction, dbName);
                    }

                    //this.DeleteDinhMucGCTransaction(transaction, dbName);

                    this.TransferGCTransaction(transaction, dbName);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool InsertUpdateDongBoDuLieuTQDT(DinhMucDangKyCollection collection, HopDong HD, string dbName)
        {
            SetDabaseMoi(dbName);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    DinhMucDangKy dmdk = new DinhMucDangKy();
                    dmdk.ID_HopDong = HD.ID;
                    dmdk.DeleteBy_ID_HopDong(transaction, dbName);

                    foreach (DinhMucDangKy item in collection)
                    {
                        item.ID_HopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.InsertTransaction(transaction, dbName);
                        else
                            item.UpdateTransaction(transaction, dbName);

                        foreach (DinhMuc dm in item.DMCollection)
                        {
                            dm.Master_ID = item.ID;
                            dm.InsertUpdateTransaction(transaction, dbName);
                        }

                        this.TransferGCTransaction(transaction, dbName);

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int DeleteBy_ID_HopDong(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_DinhMucDangKy_DeleteBy_ID_HopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        public long InsertTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_DinhMucDangKy_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.NVarChar, this._NamTN);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public int UpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_DinhMucDangKy_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.NVarChar, this._NamTN);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public bool checkExitDinhMuc(string manpl, string masp)
        {
            string sql = "select dm.id " +
                        "from t_KDT_GC_DinhMuc dm " +
                        "inner join t_KDT_GC_DinhMucDangKy dmdk on dmdk.ID=dm.Master_ID " +
                        "where dmdk.id <> @id and ID_HopDong=@ID_HopDong";
            DbCommand com = db.GetSqlStringCommand(sql);
            db.AddInParameter(com, "@id", SqlDbType.BigInt, this.ID);
            db.AddInParameter(com, "@ID_HopDong", SqlDbType.BigInt, this.ID_HopDong);
            IDataReader reader = db.ExecuteReader(com);
            bool exit = false;
            if (reader.Read())
            {
                exit = true;
            }
            reader.Close();
            return exit;
        }

        #region WS
        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            HopDong HD = new HopDong();
            HD.ID = this.ID_HopDong;
            HD = HopDong.Load(HD.ID);
            nodeName.InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = HD.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];

            /* LanNT Comment khong cho phep them moi guidstr
             * trong qua trinh khai bao-lay phan hoi
             * dat la gia tri duy nhat trong suot qua trinh xu ly
            //DATLMQ bổ sung kiểm tra GUIDSTR Định mức 10/01/2011
            if (this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                nodeReference.InnerText = this.GUIDSTR.ToUpper();
            else if (this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                //DATLMQ bổ sung reference cho Định mức 07/01/2011
                this.GUIDSTR = System.Guid.NewGuid().ToString();
                nodeReference.InnerText = this.GUIDSTR.ToUpper();
            }
            */

            nodeReference.InnerText = GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            nodeFrom.ChildNodes[1].InnerText = HD.MaDoanhNghiep;
            //DATLMQ bổ sung lưu GUIDSTR vào cho Định mức 07/01/2011
            this.Update();
            return doc.InnerXml;
        }

        private string ConfigPhongBiPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            HopDong HD = new HopDong();
            HD.ID = this.ID_HopDong;
            HD = HopDong.Load(HD.ID);
            nodeName.InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = HD.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            this.GUIDSTR = (System.Guid.NewGuid().ToString().ToUpper());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = HD.MaDoanhNghiep.Trim();
            nodeFrom.ChildNodes[0].InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));

            return doc.InnerXml;
        }
        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            //int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            //doc.GetElementsByTagName("function")[0].InnerText = MessageFunctions.LayPhanHoi.ToString();
            doc.GetElementsByTagName("function")[0].InnerText = MessageFunctions.LayPhanHoi.ToString();
            doc.GetElementsByTagName("type")[0].InnerText = MessgaseType.ThongTin.ToString();

            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);

            XmlNode Result = null;
            string kq = "";
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                string msgError = string.Empty;
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    if (Result.Attributes["Err"].Value == "no")
                    {
                        if (Result.Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        msgError = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty)
                    {
                        kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                        throw ex;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(kq))
                            Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    }
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");

                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            bool ok = false;
            /* LanNT không cần thiết đây là method lấy phản hồi vì vậy func luôn luôn là 5
            if (function == MessageFunctions.KhaiBao)
            {
                this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SoTN"].Value);
                this.NgayTiepNhan = DateTime.Today;
                this.TrangThaiXuLy = 0;
                //TransferGC();
                ok = true;
            }
            else if (function == MessageFunctions.HuyKhaiBao)
            {
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.TrangThaiXuLy = -1;
                //DeleteDinhMucGC();
                ok = true;
            }
            else*/
            try
            {
                if (function == (int)MessageFunctions.LayPhanHoi)
                {
                    #region old code
                    //if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").InnerText == "2")
                    //{
                    //    this.TrangThaiXuLy = 1;
                    //    //TransferGC();
                    //    ok = false;
                    //    this.Update();
                    //    try
                    //    {
                    //        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                    //        if (nodeMess != null)
                    //        {
                    //            return nodeMess.OuterXml;
                    //        }
                    //    }
                    //    catch { }
                    //}
                    //else
                    //{
                    //    try
                    //    {
                    //        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                    //        if (nodeMess != null)
                    //        {
                    //            return nodeMess.OuterXml;
                    //        }
                    //    }
                    //    catch { }
                    //}

                    //if (Result.Attributes["TrangThai"].Value == "yes")
                    //{
                    //    try
                    //    {
                    //        XmlNode nodephanluong = docNPL.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG");
                    //        if (nodephanluong != null)
                    //        {
                    //            this.PhanLuong = docNPL.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG").Attributes["MALUONG"].Value.Trim();
                    //            this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG").Attributes["HUONGDAN"].Value);
                    //            this.ActionStatus = 1;
                    //            this.TrangThaiXuLy = 1;
                    //            this.Update();
                    //        }
                    //    }
                    //    catch { }


                    //    ok = false;

                    //    try
                    //    {
                    //        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                    //        if (nodeMess != null)
                    //        {
                    //            return nodeMess.OuterXml;
                    //        }
                    //    }
                    //    catch { }
                    //}
                    #endregion
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        XmlNode nodeTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TuChoi"];
                        if (nodeTuChoi != null)
                        {
                            if (nodeTuChoi.InnerText == "yes")
                            {
                                this.SoTiepNhan = 0;
                                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                strTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText;
                                ok = true;
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQTuChoiDinhMuc, FontConverter.TCVN2Unicode(strTuChoi));
                            }
                            else
                            {
                                this.TrangThaiXuLy =Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                ok = true;
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQDuyetDinhMuc);

                            }

                        }
                        else
                        {
                            XmlNode nodeSoTN = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SOTN"];
                            if (nodeSoTN != null && this.SoTiepNhan == 0)
                            {
                                this.SoTiepNhan = Convert.ToInt64(nodeSoTN.Value);
                                //this.NgayTiepNhan = DateTime.Now;
                                try
                                {
                                    this.NgayTiepNhan = DateTime.Parse(docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["NgayTN"].Value);
                                    NamTN = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["NAMTN"].Value;
                                }
                                catch
                                {
                                    this.NgayTiepNhan = DateTime.Now;
                                }
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                                ok = true;
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoLayPhanHoiDinhMuc, string.Format("Số tiếp nhận ={0},Ngày tiếp nhận={1},Năm tiếp nhận={2}", SoTiepNhan, NgayTiepNhan, NamTN));
                            }
                            //DATLMQ bổ sung nhận Định mức đã duyệt 09/01/2011
                            else if (nodeSoTN != null && this.SoTiepNhan > 0)
                            {
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                ok = true;
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQDuyetDinhMuc);

                            }

                            else if (nodeSoTN == null && docNPL.SelectSingleNode("Envelope/Header/Subject/function").InnerText == "2")
                            {
                                this.SoTiepNhan = 0;
                                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                                this.TrangThaiXuLy = -1;
                                ok = true;
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQHuyDinhMuc);

                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                            if (nodeMess != null)
                            {
                                return nodeMess.OuterXml;
                            }
                        }
                        catch { }
                    }
                }

                if (ok)
                    this.Update();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            return "";

        }
        public void TransferGC()
        {
            if (this.DMCollection.Count == 0)
                this.LoadCollection();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    foreach (DinhMuc dmInfo in this.DMCollection)
                    {
                        Company.GC.BLL.GC.GC_DinhMuc dmDuyet = new Company.GC.BLL.GC.GC_DinhMuc();
                        dmDuyet.SoTiepNhan = this.SoTiepNhan;
                        dmDuyet.NgayTiepNhan = this.NgayTiepNhan;                         
                        dmDuyet.LenhSanXuat_ID = Company.GC.BLL.GC.KDT_LenhSanXuat.GetID(dmInfo.MaDDSX.ToString(),this.ID_HopDong);
                        dmDuyet.GuidString = this.GUIDSTR;
                        dmDuyet.TrangThaiXuLy = this.TrangThaiXuLy;
                        dmDuyet.DinhMucSuDung = dmInfo.DinhMucSuDung;
                        dmDuyet.MaNguyenPhuLieu = dmInfo.MaNguyenPhuLieu;
                        dmDuyet.MaSanPham = dmInfo.MaSanPham;
                        dmDuyet.NPL_TuCungUng = dmInfo.NPL_TuCungUng;
                        dmDuyet.TyLeHaoHut = dmInfo.TyLeHaoHut;
                        dmDuyet.TenNPL = dmInfo.TenNPL;
                        dmDuyet.TenSanPham = dmInfo.TenSanPham;
                        dmDuyet.HopDong_ID = this.ID_HopDong;
                        #region Ghi Log
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine("Số tiếp nhận : " + dmDuyet.SoTiepNhan);
                        write.WriteLine("Ngày tiếp nhận : " + dmDuyet.NgayTiepNhan);
                        write.WriteLine("Lệnh sản xuất : " + dmDuyet.LenhSanXuat_ID);
                        write.WriteLine("GuidString : " + dmDuyet.GuidString);
                        write.WriteLine("Trạng thái xử lý : " + dmDuyet.TrangThaiXuLy);
                        write.WriteLine("Định mức SD : " + dmDuyet.DinhMucSuDung);
                        write.WriteLine("Mã NPL : " + dmDuyet.MaNguyenPhuLieu);
                        write.WriteLine("Mã SP : " + dmDuyet.MaSanPham);
                        write.WriteLine("NPL CU : " + dmDuyet.NPL_TuCungUng);
                        write.WriteLine("TLHH : " + dmDuyet.TyLeHaoHut);
                        write.WriteLine("Tên NPL : " + dmDuyet.TenNPL);
                        write.WriteLine("Tên SP : " + dmDuyet.TenSanPham);
                        write.WriteLine("Hợp đồng ID : " + dmDuyet.HopDong_ID);  
                        write.Flush();
                        write.Close();
                        #endregion
                        dmDuyet.InsertUpdate(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("Lỗi cập nhật định mức ");
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void TransferGCTransaction(SqlTransaction transaction)
        {
            if (this.DMCollection.Count == 0)
                this.LoadCollection();
            this.UpdateTransaction(transaction);
            foreach (DinhMuc dmInfo in this.DMCollection)
            {
                Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
                dmDuyet.DinhMucSuDung = dmInfo.DinhMucSuDung;
                dmDuyet.MaNguyenPhuLieu = dmInfo.MaNguyenPhuLieu;
                dmDuyet.MaSanPham = dmInfo.MaSanPham;
                dmDuyet.NPL_TuCungUng = dmInfo.NPL_TuCungUng;
                dmDuyet.TyLeHaoHut = dmInfo.TyLeHaoHut;
                dmDuyet.TenNPL = dmInfo.TenNPL;
                dmDuyet.TenSanPham = dmInfo.TenSanPham;
                dmDuyet.HopDong_ID = this.ID_HopDong;
                dmDuyet.InsertUpdateTransaction(transaction);
            }

        }
        public void TransferGCTransaction(SqlTransaction transaction, string dbName)
        {
            if (this.DMCollection.Count == 0)
                this.LoadCollection(dbName);

            foreach (DinhMuc dmInfo in this.DMCollection)
            {
                Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
                dmDuyet.DinhMucSuDung = dmInfo.DinhMucSuDung;
                dmDuyet.MaNguyenPhuLieu = dmInfo.MaNguyenPhuLieu;
                dmDuyet.MaSanPham = dmInfo.MaSanPham;
                dmDuyet.NPL_TuCungUng = dmInfo.NPL_TuCungUng;
                dmDuyet.TyLeHaoHut = dmInfo.TyLeHaoHut;
                dmDuyet.TenNPL = dmInfo.TenNPL;
                dmDuyet.TenSanPham = dmInfo.TenSanPham;
                dmDuyet.HopDong_ID = this.ID_HopDong;
                dmDuyet.InsertUpdateTransaction(transaction, dbName);
            }

        }
        public void DeleteDinhMucGCTransaction(SqlTransaction transaction)
        {
            if (this.DMCollection.Count == 0)
                this.LoadCollection();
            this.UpdateTransaction(transaction);
            foreach (DinhMuc dmInfo in this.DMCollection)
            {
                Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
                dmDuyet.MaNguyenPhuLieu = dmInfo.MaNguyenPhuLieu;
                dmDuyet.MaSanPham = dmInfo.MaSanPham;
                dmDuyet.HopDong_ID = this.ID_HopDong;
                dmDuyet.DeleletDM(this.ID_HopDong, dmDuyet.MaSanPham, dmDuyet.MaNguyenPhuLieu, transaction);
            }


        }
        public void DeleteDinhMucGCTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            if (this.DMCollection.Count == 0)
                this.LoadCollection();

            foreach (DinhMuc dmInfo in this.DMCollection)
            {
                Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
                dmDuyet.MaNguyenPhuLieu = dmInfo.MaNguyenPhuLieu;
                dmDuyet.MaSanPham = dmInfo.MaSanPham;
                dmDuyet.HopDong_ID = this.ID_HopDong;
                dmDuyet.DeleletDM(this.ID_HopDong, dmDuyet.MaSanPham, dmDuyet.MaNguyenPhuLieu, transaction, dbName);
            }


        }
        public void DeleteDinhMucGC()
        {
            if (this.DMCollection.Count == 0)
                this.LoadCollection();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    foreach (DinhMuc dmInfo in this.DMCollection)
                    {
                        Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
                        dmDuyet.MaNguyenPhuLieu = dmInfo.MaNguyenPhuLieu;
                        dmDuyet.MaSanPham = dmInfo.MaSanPham;
                        dmDuyet.HopDong_ID = this.ID_HopDong;
                        dmDuyet.LenhSanXuat_ID = String.IsNullOrEmpty(dmInfo.MaDDSX) ? 0 : KDT_LenhSanXuat.GetID(dmInfo.MaDDSX.ToString(),this.ID_HopDong);
                        dmDuyet.DeleletDinhMuc(this.ID_HopDong, dmDuyet.MaSanPham,dmDuyet.MaNguyenPhuLieu ,dmDuyet.LenhSanXuat_ID, transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public string WSSend(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.DinhMuc, (int)MessageFunctions.KhaiBao));
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docHopDong.Load(path + "\\B03GiaCong\\KhaiBaoDinhMuc.xml");

            //thong tin hop dong
            docHopDong.SelectSingleNode(@"Root/DinhMuc/HD").Attributes["ID"].Value = this.ID_HopDong.ToString();

            HopDong HD = new HopDong();
            HD.ID = this.ID_HopDong;
            HD = HopDong.Load(HD.ID);
            XmlNode NodeSoHD = docHopDong.SelectSingleNode(@"Root/DinhMuc/HD/So_HD");
            NodeSoHD.InnerText = HD.SoHopDong;
            XmlNode NodeMaHaiQuan = docHopDong.SelectSingleNode(@"Root/DinhMuc/HD/Ma_HQHD");
            NodeMaHaiQuan.InnerText = HD.MaHaiQuan;
            XmlNode NodeMaDoanhNghiep = docHopDong.SelectSingleNode(@"Root/DinhMuc/HD/DVGC");
            NodeMaDoanhNghiep.InnerText = HD.MaDoanhNghiep;
            XmlNode NodeNgayKy = docHopDong.SelectSingleNode(@"Root/DinhMuc/HD/Ngay_Ky");
            NodeNgayKy.InnerText = HD.NgayKy.ToString("MM/dd/yyyy");

            CultureInfo culture = new CultureInfo("vi-VN");
            NumberFormatInfo f = new NumberFormatInfo();
            if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            {
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";
            }
            #region By XML định mức
            XmlNode NodeDM = docHopDong.SelectSingleNode(@"Root/DinhMuc/DDMucs");
            foreach (DinhMuc dm in this.DMCollection)
            {
                XmlElement NodeDMChild = docHopDong.CreateElement("DDMuc");
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.Ma = dm.MaSanPham;
                sp.HopDong_ID = this.ID_HopDong;
                sp.Load();

                XmlNode NodeMaSP = docHopDong.CreateElement("SPP_Code");
                NodeMaSP.InnerText = "S" + dm.MaSanPham;

                XmlNode NodeMaHSSP = docHopDong.CreateElement("SPHS_Code");
                NodeMaHSSP.InnerText = sp.MaHS;

                XmlNode NodeMaNPL = docHopDong.CreateElement("NPLP_Code");
                NodeMaNPL.InnerText = "N" + dm.MaNguyenPhuLieu;

                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.Ma = dm.MaNguyenPhuLieu;
                npl.HopDong_ID = this.ID_HopDong;
                npl.Load();

                XmlNode NodeMaHSNPL = docHopDong.CreateElement("NPLHS_Code");
                NodeMaHSNPL.InnerText = npl.MaHS;

                XmlNode NodeTenNPL = docHopDong.CreateElement("Ten_NPL");
                NodeTenNPL.InnerText = FontConverter.Unicode2TCVN(npl.Ten);
                XmlNode NodeTyLeHH = docHopDong.CreateElement("Tieu_Hao");
                NodeTyLeHH.InnerText = dm.TyLeHaoHut.ToString(f);

                XmlNode NodeDMGC = docHopDong.CreateElement("DMGC");
                NodeDMGC.InnerText = dm.DinhMucSuDung.ToString(f);

                XmlNode NodeMuaVN = docHopDong.CreateElement("Mua_VN");
                NodeMuaVN.InnerText = dm.NPL_TuCungUng.ToString(f);

                XmlNode NodeDVT = docHopDong.CreateElement("Ma_DVT");
                NodeDVT.InnerText = npl.DVT_ID;


                XmlNode NodeDVTQuyDoi = docHopDong.CreateElement("DVT_QuyDoi");
                NodeDVTQuyDoi.InnerText = npl.DVT_ID;
                XmlNode NodeGhiChu = docHopDong.CreateElement("Ghi_Chu");
                NodeGhiChu.InnerText = FontConverter.Unicode2TCVN(dm.GhiChu);


                XmlNode NodeSTT = docHopDong.CreateElement("Index");
                NodeSTT.InnerText = dm.STTHang.ToString();


                NodeDMChild.AppendChild(NodeMaSP);
                NodeDMChild.AppendChild(NodeMaHSSP);
                NodeDMChild.AppendChild(NodeMaNPL);
                NodeDMChild.AppendChild(NodeMaHSNPL);
                NodeDMChild.AppendChild(NodeTenNPL);
                NodeDMChild.AppendChild(NodeTyLeHH);
                NodeDMChild.AppendChild(NodeDMGC);
                NodeDMChild.AppendChild(NodeMuaVN);
                NodeDMChild.AppendChild(NodeDVT);
                NodeDMChild.AppendChild(NodeDVTQuyDoi);
                NodeDMChild.AppendChild(NodeGhiChu);
                NodeDMChild.AppendChild(NodeSTT);
                NodeDM.AppendChild(NodeDMChild);

            }
            #endregion By XML định mức

            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);


            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            string msgError = string.Empty;
            try
            {

                kq = kdt.Send(doc.InnerXml, pass);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoDinhMuc);

                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        //XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                        //XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                        //nodeRoot.RemoveChild(node);
                        return doc.InnerXml;
                    }
                }
                else
                {
                    msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    throw new Exception(msgError);

                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                    throw ex;
                }
                else
                    if (kq != "")
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));

                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            return "";
        }

        public string WSCancel(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.DinhMuc, (int)MessageFunctions.HuyKhaiBao));
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docHopDong.Load(path + "\\B03GiaCong\\HuyKhaiBaoDinhMuc.xml");
            docHopDong.SelectSingleNode("Root").InnerText = this.SoTiepNhan.ToString();
            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            string msgError = string.Empty;

            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHuyDinhMuc);
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        return doc.InnerXml;
                    }
                }
                else
                {
                    msgError = " Hải quan trả lỗi ->" + FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    throw ex;
                else
                {
                    if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }
            return "";
        }
        public string WSDownLoad(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiPhanHoi((int)MessgaseType.DinhMuc, (int)MessageFunctions.HoiTrangThai));
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docHopDong.Load(path + "\\B03GiaCong\\LayPhanHoiDinhMuc.XML");
            docHopDong.SelectSingleNode("Root").InnerText = this.SoTiepNhan.ToString();

            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);



            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }


        #endregion WS


        //kiem tra san pham nay da khai dm chua
        //0 chua khai bao
        //>0 da khai bao trong mot danh sach dinh muc nhung danh sach do chua duyet
        //<0 da duoc duyet roi
        public long GetIDDinhMucExit(string MaSanPham, long dmDangKy_ID, long HopDong_ID , string LenhSanXuat)
        {
            string sql = "select dm.Master_ID,dmdk.TrangThaiXuLy " +
                       "from t_KDT_GC_DinhMuc dm " +
                       "inner join t_KDT_GC_DinhMucDangKy dmdk on dmdk.ID=dm.Master_ID  " +
                       "where dmdk.id <> @id and ID_HopDong=@ID_HopDong and MaSanPham=@MaSanPham AND MaDDSX=@MaDDSX AND dmdk.TrangThaiXuLy NOT IN (10)";
            DbCommand com = db.GetSqlStringCommand(sql);
            db.AddInParameter(com, "@id", SqlDbType.BigInt, dmDangKy_ID);
            db.AddInParameter(com, "@ID_HopDong", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(com, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            db.AddInParameter(com, "@MaDDSX", SqlDbType.VarChar, LenhSanXuat);
            IDataReader reader = db.ExecuteReader(com);
            long id = 0;
            if (reader.Read())
            {
                if (reader.GetInt32(1) == 1)
                    id = -1;
                else
                {
                    id = reader.GetInt64(0);
                }
            }
            reader.Close();
            return id;
        }

        public DataTable getDinhMucChuaDuyetByMaSPAndIDHopDong(long idHopDong, string maSP, decimal SoLuong)
        {
            string sql = "select *, (DinhMucChung * @SoLuong) as NhuCau from t_View_DinhMucChuaDangKy where ID_HopDong=@ID_HopDong and MaSanPham=@MaSanPham";
            DbCommand com = db.GetSqlStringCommand(sql);
            db.AddInParameter(com, "@ID_HopDong", SqlDbType.BigInt, idHopDong);
            db.AddInParameter(com, "@MaSanPham", SqlDbType.VarChar, maSP);
            db.AddInParameter(com, "@SoLuong", SqlDbType.Decimal, SoLuong);
            return db.ExecuteDataSet(com).Tables[0];
        }

        #region DongBoDuLieuPhongKhai
        public static void DongBoDuLieuKhaiDTByIDDinhMucDK(DinhMucDangKy dmdk, HopDong HD, long IDDMOfHQ, string nameConnectKDT)
        {
            //nguyen phu lieu
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select * from DDMuc where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD and DMID=@DMID";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            db.AddInParameter(dbCommand, "@DMID", SqlDbType.Decimal, IDDMOfHQ);
            DataSet dsDM = db.ExecuteDataSet(dbCommand);
            dmdk.DMCollection = new DinhMucCollection();
            foreach (DataRow row in dsDM.Tables[0].Rows)
            {
                DinhMuc dm = new DinhMuc();
                dm.DinhMucSuDung = Convert.ToDecimal(row["DMGC"]);
                dm.GhiChu = row["Ghi_Chu"].ToString();
                dm.MaNguyenPhuLieu = row["NPLP_Code"].ToString().Substring(1).Trim();
                dm.MaSanPham = row["SPP_Code"].ToString().Substring(1).Trim();
                dm.NPL_TuCungUng = Convert.ToDecimal(row["Mua_VN"].ToString());
                dm.TyLeHaoHut = Convert.ToDecimal(row["TLHH"].ToString());
                dmdk.DMCollection.Add(dm);
            }
            dmdk.TrangThaiXuLy = 0;
            try
            {
                dmdk.InsertUpdateFull();
            }
            catch { }



        }
        public static void DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlDelete = "delete t_KDT_GC_DinhMucDangKy where MaDoanhNghiep=@DVGC and TrangThaiXuLy!=-1 and MaHaiQuan=@Ma_HQHD";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);

            db.ExecuteNonQuery(dbCommandDelete);
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select DISTINCT DinhMuc.*,DDMuc.So_HD,DDMuc.NgayNhap,DDMuc.Ngay_Ky from DinhMuc inner join DDMuc on DinhMuc.DMID=DDMuc.DMID where DVGC=@DVGC and TrangThai<>2 and Ma_HQHD=@Ma_HQHD";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string sohopdong = row["So_HD"].ToString();
                DinhMucDangKy dmdk = new DinhMucDangKy();
                dmdk.MaDoanhNghiep = MaDoanhNghiep;
                dmdk.MaHaiQuan = MaHaiQuan;
                dmdk.SoTiepNhan = Convert.ToInt64(row["DMID"]);
                dmdk.NgayTiepNhan = Convert.ToDateTime(row["NgayNhap"]);
                dmdk.TrangThaiXuLy = 0;
                HopDong hd = new HopDong();
                hd.ID = (new HopDong()).GetIDHopDongExit(sohopdong, MaHaiQuan, MaDoanhNghiep, Convert.ToDateTime(row["Ngay_Ky"]));
                hd = HopDong.Load(hd.ID);
                
                dmdk.ID_HopDong = hd.ID;
                if (dmdk.ID_HopDong > 0)
                    DongBoDuLieuKhaiDTByIDDinhMucDK(dmdk, hd, dmdk.SoTiepNhan, nameConnectKDT);
            }

        }

        #endregion DongBoDuLieuPhongKhai

        #region TQDT :
        public string QDTWSTLayPhanHoi(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.DinhMuc, (int)MessageFunctions.HoiTrangThai));
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docHopDong.Load(path + "\\B03GiaCong\\LayPhanHoiDaDuyet.xml");
            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }
        public string TQDTLayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            doc.GetElementsByTagName("function")[0].InnerText = MessageFunctions.LayPhanHoi.ToString();
            doc.GetElementsByTagName("type")[0].InnerText = MessgaseType.ThongTin.ToString();

            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            //doc.GetElementsByTagName("function")[0].InnerText = MessageFunctions.LayPhanHoi.ToString();

            XmlNode Result = null;
            string kq = "";
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                string msgError = string.Empty;
                try
                {

                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    if (Result.Attributes["Err"].Value == "no")
                    {
                        if (Result.Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        msgError = "Hải quan trả về ->" + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty) throw ex;
                    if (!string.IsNullOrEmpty(kq))
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            bool ok = false;
            try
            {
                if (function == (int)MessageFunctions.LayPhanHoi)
                {

                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        XmlNode nodeTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TuChoi"];
                        if (nodeTuChoi != null)
                        {
                            if (nodeTuChoi.InnerText == "yes")
                            {
                                this.SoTiepNhan = 0;
                                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                strTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText;
                                ok = true;
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQTuChoiDinhMuc, FontConverter.TCVN2Unicode(strTuChoi));
                            }
                            else
                            {
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                ok = true;
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQDuyetDinhMuc);

                            }

                        }
                        else
                        {
                            XmlNode nodeSoTN = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SOTN"];
                            if (nodeSoTN != null && this.SoTiepNhan == 0)
                            {
                                this.SoTiepNhan = Convert.ToInt64(nodeSoTN.Value);
                                //this.NgayTiepNhan = DateTime.Now;
                                try
                                {
                                    this.NgayTiepNhan = DateTime.Parse(docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["NgayTN"].Value);
                                    NamTN = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["NAMTN"].Value;
                                }
                                catch
                                {
                                    this.NgayTiepNhan = DateTime.Now;
                                }
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                                ok = true;
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoLayPhanHoiDinhMuc, string.Format("Số tiếp nhận ={0},Ngày tiếp nhận={1},Năm tiếp nhận={2}", SoTiepNhan, NgayTiepNhan, NamTN));
                            }
                            //DATLMQ bổ sung nhận Định mức đã duyệt 09/01/2011
                            else if (nodeSoTN != null && this.SoTiepNhan > 0)
                            {
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                ok = true;
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQDuyetDinhMuc);

                            }
                        }
                    }
                    else
                    {
                        //try
                        //{
                        //    XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                        //    if (nodeMess != null)
                        //    {
                        //        return nodeMess.OuterXml;
                        //    }
                        //}
                        //catch { }
                    }
                }

                if (ok)
                    this.Update();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            return string.Empty;
        }

        #endregion
        #region Đồng bộ dữ liệu V3
        public string InsertFullFromISyncDaTa()
        {
            string error = string.Empty;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    List<HopDong> hdco = HopDong.SelectCollectionDynamic("SoHopDong = '" + this.SoHopDong.Trim()+"'", null);
                    if (hdco == null || hdco.Count == 0)
                    {
                        transaction.Rollback();
                        this.ID = 0;
                        connection.Close();
                        return "Không tồn tại hợp đồng của tờ khai";
                    }
                    else
                    {
                        this.ID_HopDong = hdco[0].ID;
                    }
                    this.ID = 0;
                    this.ID = this.InsertTransaction(transaction);
                    foreach (DinhMuc dm in this.DMCollection)
                    {
                        dm.Master_ID = this.ID;
                        dm.ID = 0;
                        dm.InsertTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    error = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            return error;
        }
        #endregion
    }
}
