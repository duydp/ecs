using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Licenses_v2.Module;
using ActivationInfo;

namespace Licenses_v2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        public static string LoginID = "";
        public static Boolean isLoginSuccess = false;
        [STAThread]
        static void Main()
        {
            //Helpers.Demo();
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.UserSkins.OfficeSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (doLogin())
            {
                Application.Run(new FrmMain());
            }
        }

        private static Boolean doLogin()
        {
            FrmLogin obj = new FrmLogin();
            obj.FormBorderStyle = FormBorderStyle.None;
            obj.StartPosition = FormStartPosition.CenterParent;
            obj.ShowDialog();
            return isLoginSuccess;
        }
    }
}