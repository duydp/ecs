﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_HoaDonManageForm : BaseForm
    {

        public List<KDT_VNACC_HoaDon> listHoaDon = new List<KDT_VNACC_HoaDon>();
        public List<KDT_VNACC_HangHoaDon> listHangHD = new List<KDT_VNACC_HangHoaDon>();
        public List<KDT_VNACC_PhanLoaiHoaDon> listPhanLoaiHD = new List<KDT_VNACC_PhanLoaiHoaDon>();
        private DateTime dayMin = new DateTime(1900, 1, 1);
        public VNACC_HoaDonManageForm()
        {
            InitializeComponent();
        }

        private void VNACC_HoaDonManageForm_Load(object sender, EventArgs e)
        {
            cbbPhanLoaiXuatNhap.SelectedValue = "0";
            cbbTrangThaiXuLy.SelectedValue = "0";
            //grdList.DataSource = KDT_VNACC_HoaDon.SelectCollectionAll();

        }

        private void grdList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                VNACC_HoaDonForm f = new VNACC_HoaDonForm();
                f.HoaDon = new KDT_VNACC_HoaDon();
                f.HoaDon = getHoaDonID(id);
                f.ShowDialog();

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
           
            this.btnTimKiem_Click(null, null);

        }
        private KDT_VNACC_HoaDon getHoaDonID(long id)
        {

            listHoaDon = KDT_VNACC_HoaDon.SelectCollectionAll();
            foreach (KDT_VNACC_HoaDon hd in listHoaDon)
            {
                if (hd.ID == id)
                {
                    string where = "HoaDon_ID=" + hd.ID;
                    listHangHD = new List<KDT_VNACC_HangHoaDon>();
                    listHangHD = KDT_VNACC_HangHoaDon.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_HangHoaDon hang in listHangHD)
                    {
                        hd.HangCollection.Add(hang);
                    }
                    listPhanLoaiHD = new List<KDT_VNACC_PhanLoaiHoaDon>();
                    listPhanLoaiHD = KDT_VNACC_PhanLoaiHoaDon.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_PhanLoaiHoaDon phanloai in listPhanLoaiHD)
                    {
                        hd.PhanLoaiCollection.Add(phanloai);
                    }
                    return hd;
                }
            }
            return null;
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soTN = txtSoTiepNhan.Text.Trim();
                string soHD = txtSoHoaDon.Text.Trim();
                string loaiHinh = (cbbPhanLoaiXuatNhap.SelectedValue.ToString() == "0") ? "" : cbbPhanLoaiXuatNhap.SelectedValue.ToString();
                string trangThai = (cbbTrangThaiXuLy.SelectedValue == null) ? "0" : cbbTrangThaiXuLy.SelectedValue.ToString();
                string where = String.Empty;
                if (soTN != "")
                    where = "SoTiepNhan='" + soTN + "' and ";
                if (clcNgayTiepNhan.Text != "" && clcNgayTiepNhan.Value > dayMin)
                    where = where + "NgayTiepNhan='" + String.Format("{0:yyyy/MM/dd}",clcNgayTiepNhan.Value) + "' and ";
                if (soHD != "")
                    where = where + "SoHoaDon='" + soHD + "' and ";
                if (loaiHinh != "")
                    where = where + "PhanLoaiXuatNhap='" + loaiHinh + "' and ";
                if (trangThai != "")
                    where = where + "TrangThaiXuLy='" + trangThai + "'";
                listHoaDon.Clear();
                listHoaDon = KDT_VNACC_HoaDon.SelectCollectionDynamic(where, "ID");

                grdList.DataSource = listHoaDon;
                grdList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void grdList_DeletingRecord(object sender, Janus.Windows.GridEX.RowActionCancelEventArgs e)
        {
            try
            {
                if (grdList.CurrentRow.RowType == RowType.Record)
                {
                    KDT_VNACC_HoaDon hoadon = new KDT_VNACC_HoaDon();
                    this.Cursor = Cursors.WaitCursor;
                    int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                    hoadon = getHoaDonID(id);
                    if (ShowMessage("Bạn có muốn xóa hóa đơn này không?", true) == "Yes")
                    {
                        foreach (KDT_VNACC_HangHoaDon hang in hoadon.HangCollection)
                        {
                            hang.Delete();
                        }
                        foreach (KDT_VNACC_PhanLoaiHoaDon pl in hoadon.PhanLoaiCollection)
                        {
                            pl.Delete();
                        }
                        hoadon.Delete();
                    }
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void cbbTrangThaiXuLy_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }

      

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                if (grdList.CurrentRow.RowType == RowType.Record)
                {
                    int id = System.Convert.ToInt32(grdList.CurrentRow.Cells["ID"].Value.ToString());
                    int TrangThai = System.Convert.ToInt32(grdList.CurrentRow.Cells["TrangThaiXuLy"].Value.ToString());
                    if (TrangThai == 0)
                    {
                        if (ShowMessage("Bạn có muốn xóa hóa đơn này không?", true) == "Yes")
                        {

                            KDT_VNACC_HoaDon hoadon = new KDT_VNACC_HoaDon();
                            hoadon.DeleteFull(id);
                            ShowMessage("Xóa hóa đơn thành công.", false);
                        }
                    }
                    else
                        ShowMessage("Không thể xóa hóa đơn đã duyệt.", false);

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
            btnTimKiem_Click(null, null);
        }

        private void grdList_LoadingRow(object sender, RowLoadEventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                if (e.Row.RowType == RowType.Record)
                {
                    string maLH = e.Row.Cells["PhanLoaiXuatNhap"].Value.ToString().Trim();
                    if (maLH == "I")
                        e.Row.Cells["PhanLoaiXuatNhap"].Text = maLH + " - Nhập khẩu";
                    else
                        e.Row.Cells["PhanLoaiXuatNhap"].Text = maLH + " - Xuất khẩu";
                   
                    if (e.Row.Cells["NgayTiepNhan"].Text != "")
                    {
                        DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                        if (dt.Year <= 1900)
                            e.Row.Cells["NgayTiepNhan"].Text = "";
                    }
                    if (e.Row.Cells["NgayLapHoaDon"].Text != "")
                    {
                        DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayLapHoaDon"].Text);
                        e.Row.Cells["NgayLapHoaDon"].Text = dt.ToShortDateString();
                        
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void copyHóaĐơnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (grdList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(grdList.SelectedItems[0].GetRow().Cells["ID"].Value);
                    KDT_VNACC_HoaDon TKMD = new KDT_VNACC_HoaDon();
                    
                    CopyToKhai(getHoaDonID(id), false);

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }
        }
        private void CopyToKhai(KDT_VNACC_HoaDon tkmd, bool isCopyHangHoa)
        {
            KDT_VNACC_HoaDon tkmdcopy = new KDT_VNACC_HoaDon();
            HelperVNACCS.UpdateObject<KDT_VNACC_HoaDon>(tkmdcopy, tkmd, false);
            tkmdcopy.NgayTiepNhan = new DateTime(1900, 1, 1);
            tkmdcopy.SoTiepNhan = 0;
            //tkmdcopy.NgayTiepNhan = new DateTime(1900, 1, 1);
            tkmdcopy.TrangThaiXuLy = "0";
            tkmdcopy.InputMessageID = string.Empty;
            tkmdcopy.MessageTag = string.Empty;
            tkmdcopy.IndexTag = string.Empty;
            if (tkmdcopy != null)
            {
                VNACC_HoaDonForm f = new VNACC_HoaDonForm();
                f.HoaDon = tkmdcopy;
                f.ShowDialog(this);
            }

        }

    }
}
