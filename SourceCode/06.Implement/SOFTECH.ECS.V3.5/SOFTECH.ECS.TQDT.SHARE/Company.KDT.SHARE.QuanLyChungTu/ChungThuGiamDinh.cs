﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class ChungThuGiamDinh
    {
        List<HangGiamDinh> _listHang = new List<HangGiamDinh>();
        public List<HangGiamDinh> ListHang { get { return _listHang; } set { _listHang = value; } }

        public void LoadHang()
        {
            if (this.ID > 0)
                _listHang = (List<HangGiamDinh>)HangGiamDinh.SelectCollectionBy_ChungThu_ID(this.ID);
        }

        public DataTable ConvertListToDataSet(DataTable dsHMD)
        {
            if (_listHang != null && _listHang.Count == 0)
            {
                LoadHang();
            }

            DataTable tmp = dsHMD.Copy();
            tmp.Rows.Clear();
            tmp.Columns.Add("GhiChu");
            dsHMD.Columns.Add("GhiChu");
            dsHMD.Columns.Add("HMD_ID");
            tmp.Columns["ID"].ColumnName = "HMD_ID";
            tmp.Columns["HMD_ID"].Caption = "HMD_ID";
            tmp.Columns.Add("ID");
            tmp.Columns.Add("SoVanTaiDon");
            tmp.Columns.Add("NgayVanDon", typeof(DateTime));
            tmp.Columns.Add("SoHieuContainer");
            tmp.Columns.Add("TinhTrangContainer", typeof(bool));

            dsHMD.Columns.Add("SoVanTaiDon");
            dsHMD.Columns.Add("NgayVanDon", typeof(DateTime));
            dsHMD.Columns.Add("SoHieuContainer");
            dsHMD.Columns.Add("TinhTrangContainer", typeof(bool));
            foreach (HangGiamDinh item in _listHang)
            {
                DataRow[] row = dsHMD.Select("ID=" + item.HMD_ID);
                if (row != null && row.Length != 0)
                {
                    row[0]["GhiChu"] = item.GhiChu;
                    row[0]["HMD_ID"] = row[0]["ID"];
                    row[0]["ID"] = item.ID.ToString();

                    row[0]["MaHS"] = item.MaHS;
                    row[0]["MaPhu"] = item.MaPhu;
                    row[0]["TenHang"] = item.TenHang;
                    row[0]["NuocXX_ID"] = item.NuocXX_ID;
                    row[0]["DVT_ID"] = item.DVT_ID;
                    row[0]["SoLuong"] = item.SoLuong;
                    row[0]["SoVanTaiDon"] = item.SoVanTaiDon;
                    row[0]["NgayVanDon"] = item.NgayVanDon;
                    row[0]["SoHieuContainer"] = item.SoHieuContainer;
                    row[0]["TinhTrangContainer"] = item.TinhTrangContainer;

                    tmp.ImportRow(row[0]);
                }
            }
            return tmp;

        }

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.ID = this.Insert(transaction);
                        else
                            this.Update(transaction);
                        foreach (HangGiamDinh hang in this._listHang)
                        {
                            hang.ChungThu_ID = this.ID;
                            if (hang.ID == 0)
                                hang.Insert(transaction);
                            else
                                hang.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (System.Exception ex)
                    {
                        this.ID = id;
                        transaction.Rollback();
                        Logger.LocalLogger.Instance().WriteMessage(ex);

                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }
        }

        public void DeleteAll()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (HangGiamDinh item in _listHang)
                        {
                            item.Delete(transaction);
                        }
                        this.Delete(transaction);
                        transaction.Commit();
                    }
                    catch (System.Exception ex)
                    {
                        transaction.Rollback();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    finally
                    {
                        connection.Close();
                    }


                }
            }
        }

        public static bool KiemTraChungTuCoBoSung(ChungThuGiamDinh item)
        {
            if (item.LoaiKB == 1)
            {
                return true;
            }

            return false;
        }
    }
}
