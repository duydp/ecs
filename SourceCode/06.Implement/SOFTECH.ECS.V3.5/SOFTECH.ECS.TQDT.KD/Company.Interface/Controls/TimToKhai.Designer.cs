﻿namespace Company.Interface.Controls
{
    partial class TimToKhai
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimToKhai));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cbStatus = new Janus.Windows.EditControls.UIComboBox();
            this.cboPhanLuong = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.borderLabel = new System.Windows.Forms.Label();
            this.buttonGo = new Janus.Windows.EditControls.UIButton();
            this.buttonDropDown = new Janus.Windows.EditControls.UIButton();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Search_48px.png");
            this.imageList1.Images.SetKeyName(1, "Downloads_48px.png");
            // 
            // cbStatus
            // 
            this.cbStatus.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Chưa khai báo";
            uiComboBoxItem1.Value = -1;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.FormatStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Chờ duyệt";
            uiComboBoxItem2.Value = 0;
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Đã duyệt";
            uiComboBoxItem3.Value = 1;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Không phê duyệt";
            uiComboBoxItem4.Value = "2";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Sửa tờ khai";
            uiComboBoxItem5.Value = "5";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Đã hủy";
            uiComboBoxItem6.Value = "10";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Chờ hủy";
            uiComboBoxItem7.Value = "11";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "VNACCS";
            uiComboBoxItem8.Value = "5000";
            this.cbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbStatus.Location = new System.Drawing.Point(243, 1);
            this.cbStatus.MaxDropDownItems = 10;
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(105, 21);
            this.cbStatus.TabIndex = 2;
            this.cbStatus.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbStatus.SelectedIndexChanged += new System.EventHandler(this.ToKhaiTimKiem);
            // 
            // cboPhanLuong
            // 
            this.cboPhanLuong.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboPhanLuong.DropListFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPhanLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPhanLuong.Location = new System.Drawing.Point(422, 1);
            this.cboPhanLuong.Name = "cboPhanLuong";
            this.cboPhanLuong.Size = new System.Drawing.Size(105, 21);
            this.cboPhanLuong.TabIndex = 3;
            this.cboPhanLuong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cboPhanLuong.SelectedIndexChanged += new System.EventHandler(this.ToKhaiTimKiem);
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "#####";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(80, 1);
            this.txtSoTiepNhan.MaxLength = 5;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(90, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Số tiếp nhận";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(176, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Trạng thái";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(351, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Phân luồng";
            // 
            // borderLabel
            // 
            this.borderLabel.BackColor = System.Drawing.SystemColors.Window;
            this.borderLabel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.borderLabel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.borderLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.borderLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.borderLabel.Location = new System.Drawing.Point(528, 0);
            this.borderLabel.Name = "borderLabel";
            this.borderLabel.Size = new System.Drawing.Size(103, 23);
            this.borderLabel.TabIndex = 19;
            this.borderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.borderLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.borderLabel_MouseDown);
            // 
            // buttonGo
            // 
            this.buttonGo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo.Image = ((System.Drawing.Image)(resources.GetObject("buttonGo.Image")));
            this.buttonGo.Location = new System.Drawing.Point(635, 0);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(75, 23);
            this.buttonGo.TabIndex = 20;
            this.buttonGo.Text = "Tìm kiếm";
            this.buttonGo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.buttonGo.Click += new System.EventHandler(this.ToKhaiTimKiem);
            // 
            // buttonDropDown
            // 
            this.buttonDropDown.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDropDown.Image = ((System.Drawing.Image)(resources.GetObject("buttonDropDown.Image")));
            this.buttonDropDown.Location = new System.Drawing.Point(716, 0);
            this.buttonDropDown.Name = "buttonDropDown";
            this.buttonDropDown.Size = new System.Drawing.Size(75, 23);
            this.buttonDropDown.TabIndex = 20;
            this.buttonDropDown.Text = "Nâng cao";
            this.buttonDropDown.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.buttonDropDown.Click += new System.EventHandler(this.buttonDropDown_Click);
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.ButtonFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(529, 1);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(100, 20);
            this.txtSoToKhai.TabIndex = 21;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoToKhai.Leave += new System.EventHandler(this.textBox_Leave);
            this.txtSoToKhai.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.txtSoToKhai.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // TimToKhai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.txtSoToKhai);
            this.Controls.Add(this.buttonDropDown);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.borderLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtSoTiepNhan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboPhanLuong);
            this.Controls.Add(this.cbStatus);
            this.Name = "TimToKhai";
            this.Size = new System.Drawing.Size(792, 23);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Janus.Windows.EditControls.UIComboBox cbStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label borderLabel;
        private System.Windows.Forms.ImageList imageList1;
        public Janus.Windows.EditControls.UIComboBox cboPhanLuong;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private Janus.Windows.EditControls.UIButton buttonGo;
        private Janus.Windows.EditControls.UIButton buttonDropDown;
        public Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;

    }
}
