﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class IBA : BasicVNACC
    {
        public PropertiesAttribute BRC { get; set; } //Mã ngân hàng trả thay
        public PropertiesAttribute UBP { get; set; } //Mã đơn vị sử dụng hạn mức
        public PropertiesAttribute RYA { get; set; } //Năm phát hành
        public PropertiesAttribute BPS { get; set; } //Kí hiệu chứng từ phát hành hạn mức
        public PropertiesAttribute BPN { get; set; } //Số hiệu phát hành hạn mức
        public PropertiesAttribute CCC { get; set; } //Mã tiền tệ

        public IBA()
        {
            BRC = new PropertiesAttribute(11, typeof(string)); //Mã ngân hàng trả thay
            UBP = new PropertiesAttribute(13, typeof(string)); //Mã đơn vị sử dụng hạn mức
            RYA = new PropertiesAttribute(4, typeof(int)); //Năm phát hành
            BPS = new PropertiesAttribute(10, typeof(string)); //Kí hiệu chứng từ phát hành hạn mức
            BPN = new PropertiesAttribute(10, typeof(string)); //Số hiệu phát hành hạn mức
            CCC = new PropertiesAttribute(3, typeof(string)); //Mã tiền tệ
        }
    }

    public partial class EnumGroupID { }

}
