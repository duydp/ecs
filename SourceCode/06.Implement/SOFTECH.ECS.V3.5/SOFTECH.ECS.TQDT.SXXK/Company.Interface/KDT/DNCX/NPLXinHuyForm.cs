using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.Interface.SXXK.BangKeThanhLy;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.KDT.SXXK
{
    public partial class NPLXinHuyForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public KDT_CX_NPLXinHuyDangKy TaiXuatDK = new KDT_CX_NPLXinHuyDangKy();
        private KDT_CX_NPLXinHuy TaiXuat = new KDT_CX_NPLXinHuy();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;

        public NPLXinHuyForm()
        {
            InitializeComponent();
        }
        bool isnew = true;
        private void cmdAdd_Click(object sender, EventArgs e)
        {
            if (txtSoToKhai.Text == string.Empty || txtMaNPL.Text == string.Empty)
                ShowMessage("Chưa chọn số tờ khai", false);
            else
            {
                cvError.Validate();
                if (TaiXuat == null)
                    TaiXuat = new KDT_CX_NPLXinHuy();
                getTX();
                if (isnew)
                    TaiXuatDK.XinHuyList.Add(TaiXuat);
                grdList.DataSource = TaiXuatDK.XinHuyList;
                grdList.Refetch();
                TaiXuat = new KDT_CX_NPLXinHuy();
                txtSoToKhai.Text = "";
                isnew = true;
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                TaiXuatDK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                TaiXuatDK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                TaiXuatDK.LanThanhLy = HSTL.LanThanhLy;
                TaiXuatDK.NgayThongBaoHuy = clcNgayThongBao.Value;
                TaiXuatDK.InsertUpdatFull();
                LoadData();
                ShowMessage("Lưu bảng kê thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi lưu bảng kê. " + ex, false);
            }
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdKhaiBao":
                    SendV5();
                    break;
                case "cmdNhanDulieu":
                    FeedBackV5();
                    break;
                case "cmdKetQua":
                    KetQuaXuLyTX();
                    break;
                case "cmdHuyBK":
                    SendHuyV5();
                    break;
            }
        }

        private void txtSoToKhai_ButtonClick(object sender, EventArgs e)
        {
            int id = this.HSTL.getBKToKhaiNhap();//getBKToKhaiXuat();
            DataSet ds = new BKToKhaiNhap().getBKToKhaiNhap(this.HSTL.BKCollection[id].ID);//BKToKhaiXuat().getBKToKhaiXuatNPL(this.HSTL.BKCollection[id].ID);
            DSBangKeToKhaiNhapForm f = new DSBangKeToKhaiNhapForm();
            f.BangKeHSTL_ID = this.HSTL.BKCollection[id].ID;
            f.ShowDialog(this);

            if (TaiXuat == null)
                TaiXuat = new KDT_CX_NPLXinHuy();
            if (f.SoToKhai > 0)
            {
                //TaiXuat.TKMD_ID = f.TKMD_ID;
                TaiXuat.SoToKhai = f.SoToKhaiVNACCS;
                TaiXuat.MaLoaiHinh = f.MaLoaiHinh;
                TaiXuat.NgayDangKy = f.NgayDangKy;
                TaiXuat.MaHaiQuan = f.MaHaiQuan;
                TaiXuat.MaHang = f.MaNPL;
                TaiXuat.TenHang = f.TenNPL;
                TaiXuat.DVT = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(f.DVT_ID.Trim());
                TaiXuat.LoaiHang = 1;
                TaiXuat.LuongTieuHuy = f.SoLuong;
                setTX();
            }


        }

        private void BKTaiXuatForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            if (TaiXuatDK.ID != 0)
            {
                txtSoTiepNhan.Text = TaiXuatDK.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = TaiXuatDK.NgayTiepNhan;
                clcNgayThongBao.Value = TaiXuatDK.NgayThongBaoHuy;
                switch (TaiXuatDK.TrangThaiXuLy)
                {
                    case 1:
                        lblTrangThaiXuLy.Text = "Đã duyệt";
                        break;
                    case 2:
                        lblTrangThaiXuLy.Text = "Không phê duyệt";
                        break;
                    case 0:
                        lblTrangThaiXuLy.Text = "Chờ duyệt";
                        break;
                    case -1:
                        lblTrangThaiXuLy.Text = "Chưa khai báo";
                        break;
                }
                grdList.DataSource = TaiXuatDK.XinHuyList;
                grdList.Refetch();
            }
            else
            {
                TaiXuatDK.TrangThaiXuLy = -1;
            }
        }
        private void getTX()
        {
            TaiXuat.SoToKhai = Convert.ToDecimal(txtSoToKhai.Text);
            TaiXuat.NgayDangKy = clcNgayDangKy.Value;
            TaiXuat.MaLoaiHinh = txtLoaiHinhXuat.Text;
            TaiXuat.MaHang = txtMaNPL.Text;
            TaiXuat.TenHang = txtTenNPL.Text;
            TaiXuat.DVT = ctrDVT.Code;
            TaiXuat.LoaiHang = 1;
            TaiXuat.LuongTieuHuy = Convert.ToDecimal(txtSoLuongXuat.Value);
            TaiXuat.Temp = txtGhichu.Text;
        }
        private void setTX()
        {
            txtSoToKhai.Text = TaiXuat.SoToKhai.ToString();
            txtLoaiHinhXuat.Text = TaiXuat.MaLoaiHinh;
            clcNgayDangKy.Value = TaiXuat.NgayDangKy;
            txtMaNPL.Text = TaiXuat.MaHang;
            txtTenNPL.Text = TaiXuat.TenHang;
            txtSoLuongXuat.Value = TaiXuat.LuongTieuHuy;
            ctrDVT.Code = TaiXuat.DVT;
            txtGhichu.Text = TaiXuat.Temp;
        }

        private void grdList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                TaiXuat = new KDT_CX_NPLXinHuy();
                TaiXuat = (KDT_CX_NPLXinHuy)grdList.CurrentRow.DataRow;
                setTX();
                isnew = false;
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {

        }


        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.TieuHuySendHandler(TaiXuatDK, ref msgInfor, e);
        }

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void SendV5()
        {
            if (ShowMessage("Bạn có muốn khai báo bảng kê tiêu hủy này đến Hải quan?", true) == "Yes")
            {
                bool isKhaiSua = false;
                if (TaiXuatDK.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    TaiXuatDK = KDT_CX_NPLXinHuyDangKy.LoadFull(TaiXuatDK.ID);
                    if (TaiXuatDK.TrangThaiXuLy == 5)
                        isKhaiSua = true;
                    else
                        isKhaiSua = false;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.TieuHuy;
                sendXML.master_id = TaiXuatDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdAdd.Enabled = cmdSave.Enabled = false;
                    cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanDulieu.Enabled = cmdNhanDulieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    return;
                }
                try
                {
                    if (this.TaiXuatDK.XinHuyList.Count > 0)
                    {
                        string returnMessage = string.Empty;
                        TaiXuatDK.GUIDSTR = Guid.NewGuid().ToString();
                        SXXK_HosoThanhKhoan TaiXuat_Object = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferBKTieuHuy(TaiXuatDK, HSTL.SoQuyetDinh, GlobalSettings.TEN_DON_VI, isKhaiSua);
                        //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(TaiXuatDK, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = TaiXuatDK.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TaiXuatDK.MaHaiQuan)),
                                  Identity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TaiXuatDK.MaHaiQuan).Trim()
                                  //Identity = TaiXuatDK.MaHaiQuan
                              },
                              new SubjectBase()
                              {

                                  Type = DeclarationIssuer.BKTieuHuyKCX,
                                  Function = isKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                                  Reference = TaiXuatDK.GUIDSTR,
                              },
                              TaiXuat_Object
                            );
                        //TaiXuatDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(TaiXuatDK.ID, isKhaiSua ? MessageTitle.KhaiBaoSuaTieuHuy : MessageTitle.KhaiBaoTieuHuy);
                            cmdNhanDulieu.Enabled = cmdNhanDulieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = cmdSave.Enabled = false;
                            cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.TieuHuy;
                            sendXML.master_id = TaiXuatDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            TaiXuatDK.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            //MsgSend sendXML = new MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.TaiXuat;
            //sendXML.master_id = TaiXuatDK.ID;
            //if (!sendXML.Load())
            //{
            //    ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
            //    cmdNhanDulieu.Enabled = cmdNhanDulieu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdAdd.Enabled = cmdSave.Enabled = false;
            //    cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    return;
            //}
            while (isFeedBack)
            {
                string reference = TaiXuatDK.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.BKTieuHuyKCX,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.BKTieuHuyKCX,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = TaiXuatDK.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TaiXuatDK.MaHaiQuan.Trim())),
                                                  Identity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TaiXuatDK.MaHaiQuan).Trim()
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }

            }
        }

        private void KetQuaXuLyTX()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.TaiXuatDK.ID;
            form.DeclarationIssuer = DeclarationIssuer.BKTieuHuyKCX;
            form.ShowDialog(this);
        }

        private void SendHuyV5()
        {
            if (ShowMessage("Bạn có muốn khai báo hủy bảng kê tiêu hủy này không ?", true) == "Yes")
            {

                if (TaiXuatDK.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    TaiXuatDK = KDT_CX_NPLXinHuyDangKy.LoadFull(TaiXuatDK.ID);
                    TaiXuatDK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.HuyTieuHuy;
                sendXML.master_id = TaiXuatDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdAdd.Enabled = cmdSave.Enabled = false;
                    cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanDulieu.Enabled = cmdNhanDulieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    if (this.TaiXuatDK.XinHuyList.Count > 0)
                    {
                        string returnMessage = string.Empty;
                        TaiXuatDK.GUIDSTR = Guid.NewGuid().ToString();
                        
                        ObjectSend msgSend = CancelMessageV5(TaiXuatDK, HSTL.SoQuyetDinh);
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(TaiXuatDK.ID, "Hhủy bảng kê tiêu hủy");
                            cmdNhanDulieu.Enabled = cmdNhanDulieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = cmdSave.Enabled = false;
                            cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            sendXML.LoaiHS = LoaiKhaiBao.HuyTieuHuy;
                            sendXML.master_id = TaiXuatDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();
                            TaiXuatDK.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }

        }

        public static ObjectSend CancelMessageV5(KDT_CX_NPLXinHuyDangKy BK, string soquyetDinh)
        {

            string returnMessage = string.Empty;
            BK.GUIDSTR = Guid.NewGuid().ToString();
            SXXK_HosoThanhKhoan HuyTiuHuy = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferBKTieuHuy(BK, soquyetDinh, GlobalSettings.TEN_DON_VI, false);
            HuyTiuHuy.Function = DeclarationFunction.HUY;
            HuyTiuHuy.CustomsReference = BK.SoTiepNhan.ToString();
            HuyTiuHuy.Acceptance = BK.NgayTiepNhan.ToString("yyyy-MM-dd");
            ObjectSend msgSend = new ObjectSend(
                 new NameBase()
                 {
                     Name = GlobalSettings.TEN_DON_VI,
                     Identity = BK.MaDoanhNghiep
                 },
                  new NameBase()
                  {
                      Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BK.MaHaiQuan)),
                      Identity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BK.MaHaiQuan)
                  },
                  new SubjectBase()
                  {
                      Type = DeclarationIssuer.BKTieuHuyKCX,
                      Function = DeclarationFunction.HUY,
                      Reference = BK.GUIDSTR,
                  },
                  HuyTiuHuy
                );
            return msgSend;
        }
    }
}