﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Helper.Controls
{
    public partial class MessageBoxControlV : Form
    {
        public MessageBoxControlV()
        {
            InitializeComponent();
        }

        public DialogResult ReturnValue = DialogResult.Cancel;
        public bool ShowYesNoButton
        {
            set
            {
                this.btnNo.Visible = this.btnYes.Visible = value;
                this.btnClose.Visible = !value;
                if (!value) this.CancelButton = btnClose;
                else this.CancelButton = btnNo;
            }
        }

        public string MessageString
        {
            set { this.txtMessage.Text = value; }
        }
        public string TitleMessageString
        {
            set { this.txtTitle.Text = value; }
        }

        public void dispEnglish()
        {
            this.Text = "Announcement";
            this.btnYes.Text = "&Yes";
            this.btnNo.Text = "&No";
            this.btnClose.Text = "&Close";
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.ReturnValue = DialogResult.Yes;
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.ReturnValue = DialogResult.No;
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Thong bao

        protected static MessageBoxControlV _MsgBoxV;

        public static DialogResult ShowMessage(string message, bool showYesNoButton)
        {
            _MsgBoxV = new MessageBoxControlV();
            _MsgBoxV.TopMost = false;
            _MsgBoxV.ShowYesNoButton = showYesNoButton;
            _MsgBoxV.TitleMessageString = "Thông báo lỗi của chương trình";
            _MsgBoxV.MessageString = message;
            _MsgBoxV.ShowDialog();
            DialogResult st = _MsgBoxV.ReturnValue;
            _MsgBoxV.Dispose();
            return st;
        }

        public static DialogResult ShowMessage(string message, bool showYesNoButton, bool isTopMost)
        {
            _MsgBoxV = new MessageBoxControlV();
            _MsgBoxV.TopMost = isTopMost;
            _MsgBoxV.ShowYesNoButton = showYesNoButton;
            _MsgBoxV.TitleMessageString = "Thông báo lỗi của chương trình";
            _MsgBoxV.MessageString = message;
            _MsgBoxV.ShowDialog();
            DialogResult st = _MsgBoxV.ReturnValue;
            _MsgBoxV.Dispose();
            return st;
        }

        public static DialogResult ShowMessage(string title, string message, bool showYesNoButton)
        {
            _MsgBoxV = new MessageBoxControlV();
            _MsgBoxV.TopMost = false;
            _MsgBoxV.ShowYesNoButton = showYesNoButton;
            _MsgBoxV.TitleMessageString = title;
            _MsgBoxV.MessageString = message;
            _MsgBoxV.ShowDialog();
            DialogResult st = _MsgBoxV.ReturnValue;
            _MsgBoxV.Dispose();
            return st;
        }

        public static DialogResult ShowMessage(string title, string message, bool showYesNoButton, bool isTopMost)
        {
            _MsgBoxV = new MessageBoxControlV();
            _MsgBoxV.TopMost = isTopMost;
            _MsgBoxV.ShowYesNoButton = showYesNoButton;
            _MsgBoxV.TitleMessageString = title;
            _MsgBoxV.MessageString = message;
            _MsgBoxV.ShowDialog();
            DialogResult st = _MsgBoxV.ReturnValue;
            _MsgBoxV.Dispose();
            return st;
        }

        #endregion


    }
}
