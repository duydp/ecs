Option Explicit On
Public Class frmMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdWrite As System.Windows.Forms.Button
    Friend WithEvents cmdTransform As System.Windows.Forms.Button
    Friend WithEvents cmdRead As System.Windows.Forms.Button
    Friend WithEvents cmdEncrypt As System.Windows.Forms.Button
    Friend WithEvents cmdDecrypt As System.Windows.Forms.Button
    Friend WithEvents cmdWriteMap As System.Windows.Forms.Button
    Friend WithEvents cmdReadMap As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lstOutput = New System.Windows.Forms.ListBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmdWrite = New System.Windows.Forms.Button
        Me.cmdTransform = New System.Windows.Forms.Button
        Me.cmdRead = New System.Windows.Forms.Button
        Me.cmdEncrypt = New System.Windows.Forms.Button
        Me.cmdDecrypt = New System.Windows.Forms.Button
        Me.cmdWriteMap = New System.Windows.Forms.Button
        Me.cmdReadMap = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lstOutput
        '
        Me.lstOutput.Location = New System.Drawing.Point(16, 56)
        Me.lstOutput.Name = "lstOutput"
        Me.lstOutput.Size = New System.Drawing.Size(376, 264)
        Me.lstOutput.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(520, 32)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "SecureDongle X VB.net 2003 Sample"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmdWrite
        '
        Me.cmdWrite.Location = New System.Drawing.Point(408, 56)
        Me.cmdWrite.Name = "cmdWrite"
        Me.cmdWrite.Size = New System.Drawing.Size(112, 32)
        Me.cmdWrite.TabIndex = 3
        Me.cmdWrite.Text = "Write Data"
        '
        'cmdTransform
        '
        Me.cmdTransform.Location = New System.Drawing.Point(408, 96)
        Me.cmdTransform.Name = "cmdTransform"
        Me.cmdTransform.Size = New System.Drawing.Size(112, 32)
        Me.cmdTransform.TabIndex = 4
        Me.cmdTransform.Text = "Transform Data"
        '
        'cmdRead
        '
        Me.cmdRead.Location = New System.Drawing.Point(408, 136)
        Me.cmdRead.Name = "cmdRead"
        Me.cmdRead.Size = New System.Drawing.Size(112, 32)
        Me.cmdRead.TabIndex = 5
        Me.cmdRead.Text = "Read Data"
        '
        'cmdEncrypt
        '
        Me.cmdEncrypt.Location = New System.Drawing.Point(408, 176)
        Me.cmdEncrypt.Name = "cmdEncrypt"
        Me.cmdEncrypt.Size = New System.Drawing.Size(112, 32)
        Me.cmdEncrypt.TabIndex = 6
        Me.cmdEncrypt.Text = "Encrypt Data"
        '
        'cmdDecrypt
        '
        Me.cmdDecrypt.Location = New System.Drawing.Point(408, 216)
        Me.cmdDecrypt.Name = "cmdDecrypt"
        Me.cmdDecrypt.Size = New System.Drawing.Size(112, 32)
        Me.cmdDecrypt.TabIndex = 7
        Me.cmdDecrypt.Text = "Decrypt Data"
        '
        'cmdWriteMap
        '
        Me.cmdWriteMap.Location = New System.Drawing.Point(408, 256)
        Me.cmdWriteMap.Name = "cmdWriteMap"
        Me.cmdWriteMap.Size = New System.Drawing.Size(112, 32)
        Me.cmdWriteMap.TabIndex = 8
        Me.cmdWriteMap.Text = "Write with Map"
        '
        'cmdReadMap
        '
        Me.cmdReadMap.Location = New System.Drawing.Point(408, 296)
        Me.cmdReadMap.Name = "cmdReadMap"
        Me.cmdReadMap.Size = New System.Drawing.Size(112, 32)
        Me.cmdReadMap.TabIndex = 9
        Me.cmdReadMap.Text = "Read with Map"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(536, 344)
        Me.Controls.Add(Me.cmdReadMap)
        Me.Controls.Add(Me.cmdWriteMap)
        Me.Controls.Add(Me.cmdDecrypt)
        Me.Controls.Add(Me.cmdEncrypt)
        Me.Controls.Add(Me.cmdRead)
        Me.Controls.Add(Me.cmdTransform)
        Me.Controls.Add(Me.cmdWrite)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lstOutput)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "SecureDongle X VB.net 2003 Sample"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim retcode, handl As Integer
    Dim block_index As Short
    Dim uid, hid As Integer
    Dim buffer As String
    Dim seed As String
    Dim stri As String
    Dim key512(512) As Char
    Dim tempKey As String
    Dim plainText As String
    Dim data As String
    Dim bufferLength As Object
    Friend WithEvents lstOutput As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Dim startIndex As Short

    Private Sub cmdRead_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRead.Click
        ' Read SecureDongle X with specified UID
        retcode = SDX_Find()
        If (retcode < 0) Then
            lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
        End If

        If (retcode = 0) Then
            lstOutput.Items.Add("No SecureDongle X plugged")
            Exit Sub
        Else
            lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
        End If

        stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
        uid = Val(stri)

        retcode = SDX_Open(1, uid, hid)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        Else
            lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
        End If

        handl = retcode
        seed = InputBox("Please input Read block index (0-4)", "Input")
        block_index = Val(seed)
        buffer = New String(Chr(0), 512)

        retcode = SDX_Read(handl, block_index, buffer)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        End If
        lstOutput.Items.Add("Read: " & (buffer))

        SDX_Close(handl)
    End Sub

    Private Sub cmdTransform_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdTransform.Click
        ' Transform data using specified SecureDongle X
        retcode = SDX_Find()
        If (retcode < 0) Then
            lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
        End If

        If (retcode = 0) Then
            lstOutput.Items.Add("No SecureDongle X plugged")
            Exit Sub
        Else
            lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
        End If

        stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
        uid = Val(stri)

        retcode = SDX_Open(1, uid, hid)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        Else
            lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
        End If

        handl = retcode
        stri = InputBox("Please input data (i.e. helloworld)", "Input", "SDXTransform Test")

        retcode = SDX_Transform(handl, Len(stri), stri)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        End If
        lstOutput.Items.Add("Transform result:  " & (stri))

        SDX_Close(handl)
    End Sub

    Private Sub cmdWrite_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWrite.Click
        ' Write to SecureDongle X with specified UID
        retcode = SDX_Find()
        If (retcode < 0) Then
            lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
        End If

        If (retcode = 0) Then
            lstOutput.Items.Add("No SecureDongle X plugged")
            Exit Sub
        Else
            lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
        End If

        stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
        uid = Val(stri)

        retcode = SDX_Open(1, uid, hid)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        Else
            lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
        End If

        handl = retcode
        seed = InputBox("Please input write block index (0-4)", "Input", "0")
        block_index = Val(seed)
        stri = InputBox("Please input data to be written (i.e. helloworld)", "Input", "SDX - Sample Data")
        retcode = SDX_Write(handl, block_index, stri)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        End If
        lstOutput.Items.Add("Write: " & (stri))

        SDX_Close(handl)
    End Sub

    Private Sub cmdEncrypt_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdEncrypt.Click
        ' Encrypt using RSA and store to SecureDongle X with specified UID
        retcode = SDX_Find()
        If (retcode < 0) Then
            lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
        End If

        If (retcode = 0) Then
            lstOutput.Items.Add("No SecureDongle X plugged")
            Exit Sub
        Else
            lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
        End If

        stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
        uid = Val(stri)

        retcode = SDX_Open(1, uid, hid)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        Else
            lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
        End If

        handl = retcode
        seed = InputBox("Please enter start index (0-2559)", "Input", "0")
        startIndex = Val(seed)
        data = InputBox("Please enter data to encrypt", "Input", "SDX Test RSA Encrypt")
        bufferLength = Len(data)
        tempKey = New String(Chr(0), 512)
        retcode = SDX_RSAEncrypt(handl, startIndex, data, bufferLength, tempKey)
        key512 = tempKey.ToCharArray
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        End If
        lstOutput.Items.Add("Write success, size: " & bufferLength & ". Key to decrypt is stored inside key512 variable.")
        lstOutput.Items.Add("You can try reading the contents of the SDX")

        SDX_Close(handl)
    End Sub

    Private Sub cmdDecrypt_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDecrypt.Click
        ' Read from SecureDongle X with specified UID and decrypt using RSA
        retcode = SDX_Find()
        If (retcode < 0) Then
            lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
        End If

        If (retcode = 0) Then
            lstOutput.Items.Add("No SecureDongle X plugged")
            Exit Sub
        Else
            lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
        End If

        stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
        uid = Val(stri)

        retcode = SDX_Open(1, uid, hid)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        Else
            lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
        End If

        handl = retcode
        seed = InputBox("Please enter start index (0-2559)", "Input", "0")
        startIndex = Val(seed)

        stri = InputBox("Please enter length of data to decrypt", "Input", bufferLength)
        bufferLength = Val(stri)

        plainText = New String(Chr(0), 512)
        retcode = SDX_RSADecrypt(handl, startIndex, plainText, bufferLength, key512) '.Value)

        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        End If
        lstOutput.Items.Add("Decrypted Data: " & plainText)

        SDX_Close(handl)
    End Sub

    Private Sub cmdWriteMap_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWriteMap.Click
        ' Write to SecureDongle X with specified UID
        ' This is a sample of simple Data Mapping to put plain text data not in order
        ' For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
        retcode = SDX_Find()
        If (retcode < 0) Then
            lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
        End If

        If (retcode = 0) Then
            lstOutput.Items.Add("No SecureDongle X plugged")
            Exit Sub
        Else
            lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
        End If

        stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
        uid = Val(stri)

        retcode = SDX_Open(1, uid, hid)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        Else
            lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
        End If

        handl = retcode
        seed = InputBox("Please input write block index(0-4)", "Input", "0")
        block_index = Val(seed)
        stri = InputBox("Please input data to be written (i.e. helloworld)", "Input", "SDX - Sample Map Encryption")

        lstOutput.Items.Add("Original data:  " & (stri))
        lstOutput.Items.Add("Length =  " & Len(stri))

        '****************** ENCRYPTION SAMPLE *******************
        Dim temp(512) As Byte
        Dim sze, i, indx As Short
        If Len(stri) > 512 Then
            lstOutput.Items.Add("Error: Data size max is 512")
            Exit Sub
        End If

        Randomize()
        'Create a 512-byte Full Map with randomized content
        For i = 0 To 512
            If CInt(Int((2 * Rnd()) + 1)) = 1 Then
                temp(i) = CInt(Int((26 * Rnd()) + 65))
            Else
                temp(i) = CInt(Int((26 * Rnd()) + 97))
            End If
        Next i

        'Calculate where to put the data, and put it there
        For i = 0 To Len(stri) - 1
            sze = i * (Len(stri) - 1)
            indx = sze Mod 512
            temp(indx) = Asc(Mid(stri, i + 1, 1))
        Next i
        stri = ""
        For i = 0 To 512
            stri = stri & Chr(temp(i))
        Next i
        '********************************************************

        'Write the Mapped data
        retcode = SDX_Write(handl, block_index, stri)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        End If
        lstOutput.Items.Add("Data Written:  " & (stri))

        SDX_Close(handl)
    End Sub

    Private Sub cmdReadMap_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdReadMap.Click
        ' Read from SecureDongle X with specified UID
        ' This is the sample to read the Mapped data
        retcode = SDX_Find()
        If (retcode < 0) Then
            lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
        End If

        If (retcode = 0) Then
            lstOutput.Items.Add("No SecureDongle X plugged")
            Exit Sub
        Else
            lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
        End If

        stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
        uid = Val(stri)

        retcode = SDX_Open(1, uid, hid)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        Else
            lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
        End If

        Dim datasize As Short
        handl = retcode
        seed = InputBox("Please input Read block index (0-4)", "Input")
        block_index = Val(seed)
        seed = InputBox("Please enter size of the data to be read (1-512)", "Input")
        datasize = Val(seed)
        If datasize < 1 Or datasize > 512 Then
            MsgBox("Data size wrong")
            Exit Sub
        End If

        buffer = New String(Chr(0), 512)
        retcode = SDX_Read(handl, block_index, buffer)
        If (retcode < 0) Then
            lstOutput.Items.Add("Error: " & Hex(retcode))
            Exit Sub
        End If

        '******************* DECRYPTION **********************
        Dim buff As String
        Dim sze, i, indx As Short
        buff = ""
        For i = 0 To datasize - 1
            sze = i * (datasize - 1)
            indx = sze Mod 512
            buff = buff & Mid(buffer, indx + 1, 1)
        Next i
        '*****************************************************

        lstOutput.Items.Add("Read:  " & (buff))

        SDX_Close(handl)
    End Sub
End Class