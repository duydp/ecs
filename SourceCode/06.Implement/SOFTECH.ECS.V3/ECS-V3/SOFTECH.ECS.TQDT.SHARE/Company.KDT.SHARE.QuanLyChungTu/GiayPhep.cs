﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class GiayPhep
    {
        public List<HangGiayPhepDetail> ListHMDofGiayPhep = new List<HangGiayPhepDetail>();

        /// <summary>
        /// Lay du lieu hang hoa nam trong giay phep
        /// </summary>
        /// <param name="GiayPhep_TKMD_ID">tuong duong voi TKMD_ID va GiayPhep_ID</param>
        public void LoadListHMDofGiayPhep()
        {
            ListHMDofGiayPhep = HangGiayPhepDetail.SelectCollectionBy_GiayPhep_ID(this.ID);
        }

        public DataTable ConvertListToDataSet(DataTable dsHMD)
        {
            DataTable tmp = dsHMD.Copy();
            tmp.Rows.Clear();
            tmp.Columns.Add("GhiChu");
            dsHMD.Columns.Add("GhiChu");
            dsHMD.Columns.Add("HMD_ID");
            dsHMD.Columns.Add("MaChuyenNganh");
            dsHMD.Columns.Add("MaNguyenTe");
            tmp.Columns["ID"].ColumnName = "HMD_ID";
            tmp.Columns["HMD_ID"].Caption = "HMD_ID";
            tmp.Columns.Add("ID");
            tmp.Columns.Add("MaChuyenNganh");
            tmp.Columns.Add("MaNguyenTe");

            foreach (HangGiayPhepDetail item in ListHMDofGiayPhep)
            {
                DataRow[] row = dsHMD.Select("ID=" + item.HMD_ID);
                if (row != null && row.Length != 0)
                {
                    row[0]["HMD_ID"] = row[0]["ID"];
                    row[0]["MaChuyenNganh"] = item.MaChuyenNganh;
                    row[0]["MaNguyenTe"] = item.MaNguyenTe;
                    row[0]["ID"] = item.ID.ToString();

                    row[0]["SoThuTuHang"] = item.SoThuTuHang;
                    row[0]["MaHS"] = item.MaHS;
                    row[0]["MaPhu"] = item.MaPhu;
                    row[0]["TenHang"] = item.TenHang;
                    row[0]["NuocXX_ID"] = item.NuocXX_ID;
                    row[0]["DVT_ID"] = item.DVT_ID;
                    row[0]["SoLuong"] = item.SoLuong;
                    row[0]["DonGiaKB"] = item.DonGiaKB;
                    row[0]["TriGiaKB"] = item.TriGiaKB;

                    tmp.ImportRow(row[0]);
                }
            }
            return tmp;

        }

        public List<HangMauDich> ConvertListToHangMauDichCollection(List<HangMauDich> dsHMD)
        {
            List<HangMauDich> tmp = new List<HangMauDich>();

            foreach (HangGiayPhepDetail item in ListHMDofGiayPhep)
            {
                foreach (HangMauDich HMDTMP in dsHMD)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }

        public static bool checkSoGiayPhepExit(string SoGiayPhep, string MaDoanhNghiep, long TKMD_ID, long giayPhepID)
        {
            string sql = "select SoCO from t_KDT_GiayPhep where SoGiayPhep=@SoGiayPhep and MaDoanhNghiep=@MaDoanhNghiep and TKMD_ID=" + TKMD_ID + " and ID!=" + giayPhepID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbcommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbcommand, "@SoGiayPhep", SqlDbType.VarChar, SoGiayPhep);
            db.AddInParameter(dbcommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            object o = db.ExecuteScalar(dbcommand);
            return o != null;
        }

        public static GiayPhep Load(string maDoanhNghiep, string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHan, long idTKMD, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_LoadBy]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.VarChar, soGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, ngayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, ngayHetHan);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, idTKMD);

            GiayPhep entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new GiayPhep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiCap"))) entity.NguoiCap = reader.GetString(reader.GetOrdinal("NguoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiCap"))) entity.NoiCap = reader.GetString(reader.GetOrdinal("NoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViDuocCap"))) entity.MaDonViDuocCap = reader.GetString(reader.GetOrdinal("MaDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDuocCap"))) entity.TenDonViDuocCap = reader.GetString(reader.GetOrdinal("TenDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanCap"))) entity.MaCoQuanCap = reader.GetString(reader.GetOrdinal("MaCoQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenQuanCap"))) entity.TenQuanCap = reader.GetString(reader.GetOrdinal("TenQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.VarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year <= 1900 ? DBNull.Value : (object)NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1900 ? DBNull.Value : (object)NgayHetHan);
            db.AddInParameter(dbCommand, "@NguoiCap", SqlDbType.NVarChar, NguoiCap);
            db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
            db.AddInParameter(dbCommand, "@MaDonViDuocCap", SqlDbType.VarChar, MaDonViDuocCap);
            db.AddInParameter(dbCommand, "@TenDonViDuocCap", SqlDbType.NVarChar, TenDonViDuocCap);
            db.AddInParameter(dbCommand, "@MaCoQuanCap", SqlDbType.VarChar, MaCoQuanCap);
            db.AddInParameter(dbCommand, "@TenQuanCap", SqlDbType.NVarChar, TenQuanCap);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_GiayPhep_Update]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.VarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year == 1900 ? DBNull.Value : (object)NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1900 ? DBNull.Value : (object)NgayHetHan);
            db.AddInParameter(dbCommand, "@NguoiCap", SqlDbType.NVarChar, NguoiCap);
            db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
            db.AddInParameter(dbCommand, "@MaDonViDuocCap", SqlDbType.VarChar, MaDonViDuocCap);
            db.AddInParameter(dbCommand, "@TenDonViDuocCap", SqlDbType.NVarChar, TenDonViDuocCap);
            db.AddInParameter(dbCommand, "@MaCoQuanCap", SqlDbType.VarChar, MaCoQuanCap);
            db.AddInParameter(dbCommand, "@TenQuanCap", SqlDbType.NVarChar, TenQuanCap);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);


                        foreach (HangGiayPhepDetail hangGP in this.ListHMDofGiayPhep)
                        {
                            hangGP.GiayPhep_ID = this.ID;
                            if (id == 0)
                                hangGP.ID = 0;

                            if (hangGP.ID == 0)
                                hangGP.Insert(transaction);
                            else
                                hangGP.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void InsertUpdateFull(SqlTransaction transaction, SqlDatabase db)
        {
            try
            {
                //Kiem tra ton tai To khai tren Database Target
                GiayPhep gpTemp = GiayPhep.Load(this._MaDoanhNghiep, this._SoGiayPhep, this._NgayGiayPhep, this._NgayHetHan, this._TKMD_ID, db);

                if (gpTemp == null || gpTemp.ID == 0)
                    this.ID = this.InsertTransaction(transaction, db);
                else
                {
                    this.ID = gpTemp.ID;
                    this.UpdateTransaction(transaction, db);
                }

                foreach (HangGiayPhepDetail hangGP in this.ListHMDofGiayPhep)
                {
                    hangGP.GiayPhep_ID = this.ID;

                    HangGiayPhepDetail.DeleteBy_GiayPhep_ID(hangGP.GiayPhep_ID, transaction, db);

                    hangGP.InsertUpdate(transaction, db);
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

            }
        }

        /// <summary>
        /// Kiem tra giay phep nay dc su dung cho may to khai
        /// </summary>

        public static List<GiayPhep> SelectListGiayPhepByMaDanhNghiepAndKhacTKMD(long TKMD_ID, string MaDoanhNghiep)
        {
            List<GiayPhep> collection = new List<GiayPhep>();
            string sql = "select * from t_KDT_GiayPhep where TKMD_ID<>@TKMD_ID and MaDoanhNghiep=@MaDoanhNghiep";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbcommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbcommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbcommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbcommand);
            while (reader.Read())
            {
                GiayPhep entity = new GiayPhep();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiCap"))) entity.NguoiCap = reader.GetString(reader.GetOrdinal("NguoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiCap"))) entity.NoiCap = reader.GetString(reader.GetOrdinal("NoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViDuocCap"))) entity.MaDonViDuocCap = reader.GetString(reader.GetOrdinal("MaDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDuocCap"))) entity.TenDonViDuocCap = reader.GetString(reader.GetOrdinal("TenDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanCap"))) entity.MaCoQuanCap = reader.GetString(reader.GetOrdinal("MaCoQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenQuanCap"))) entity.TenQuanCap = reader.GetString(reader.GetOrdinal("TenQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        public bool DeleteFull()
        {
            bool ret = true;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        HangGiayPhepDetail.DeleteBy_GiayPhep_ID(this.ID, transaction);
                        this.Delete(transaction);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        public static XmlNode ConvertCollectionGiayPhepToXML(XmlDocument doc, List<GiayPhep> GiayPhepCollection, long TKMD_ID, List<HangMauDich> listHang)
        {
            try
            {
                NumberFormatInfo f = new NumberFormatInfo();
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";
                if (GiayPhepCollection == null || GiayPhepCollection.Count == 0)
                {
                    GiayPhepCollection = GiayPhep.SelectCollectionBy_TKMD_ID(TKMD_ID);
                }
                XmlElement CHUNG_TU_GIAYPHEP = doc.CreateElement("CHUNG_TU_GIAYPHEP");
                foreach (GiayPhep giayphep in GiayPhepCollection)
                {
                    XmlElement giayphepItem = doc.CreateElement("CHUNG_TU_GIAYPHEP.ITEM");
                    CHUNG_TU_GIAYPHEP.AppendChild(giayphepItem);

                    XmlAttribute SOGP = doc.CreateAttribute("SOGP");
                    SOGP.Value = FontConverter.Unicode2TCVN(giayphep.SoGiayPhep.Trim());
                    giayphepItem.Attributes.Append(SOGP);

                    XmlAttribute NGAYGP = doc.CreateAttribute("NGAYGP");
                    NGAYGP.Value = giayphep.NgayGiayPhep.ToString("MM/dd/yyyy");
                    giayphepItem.Attributes.Append(NGAYGP);

                    XmlAttribute NGAYHHGP = doc.CreateAttribute("NGAYHHGP");
                    NGAYHHGP.Value = giayphep.NgayHetHan.ToString("MM/dd/yyyy");
                    giayphepItem.Attributes.Append(NGAYHHGP);

                    XmlAttribute NOICAP = doc.CreateAttribute("NOICAP");
                    NOICAP.Value = FontConverter.Unicode2TCVN(giayphep.NoiCap.Trim());
                    giayphepItem.Attributes.Append(NOICAP);

                    XmlAttribute NGUOI_CAP = doc.CreateAttribute("NGUOI_CAP");
                    NGUOI_CAP.Value = FontConverter.Unicode2TCVN(giayphep.NguoiCap.Trim());
                    giayphepItem.Attributes.Append(NGUOI_CAP);

                    XmlAttribute MA_NGUOI_DUOC_CAP = doc.CreateAttribute("MA_NGUOI_DUOC_CAP");
                    MA_NGUOI_DUOC_CAP.Value = giayphep.MaDonViDuocCap.Trim();
                    giayphepItem.Attributes.Append(MA_NGUOI_DUOC_CAP);

                    XmlAttribute TEN_NGUOI_DUOC_CAP = doc.CreateAttribute("TEN_NGUOI_DUOC_CAP");
                    TEN_NGUOI_DUOC_CAP.Value = FontConverter.Unicode2TCVN(giayphep.TenDonViDuocCap.Trim());
                    giayphepItem.Attributes.Append(TEN_NGUOI_DUOC_CAP);

                    XmlAttribute MA_CQC = doc.CreateAttribute("MA_CQC");
                    MA_CQC.Value = giayphep.MaCoQuanCap.Trim();
                    giayphepItem.Attributes.Append(MA_CQC);

                    XmlAttribute TEN_CQC = doc.CreateAttribute("TEN_CQC");
                    TEN_CQC.Value = FontConverter.Unicode2TCVN(giayphep.TenQuanCap.Trim());
                    giayphepItem.Attributes.Append(TEN_CQC);

                    XmlAttribute HINH_THUC_TL = doc.CreateAttribute("HINH_THUC_TL");
                    HINH_THUC_TL.Value = "";
                    giayphepItem.Attributes.Append(HINH_THUC_TL);

                    XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                    GHI_CHU.Value = "";
                    giayphepItem.Attributes.Append(GHI_CHU);

                    if (giayphep.ListHMDofGiayPhep == null || giayphep.ListHMDofGiayPhep.Count == 0)
                    {
                        giayphep.LoadListHMDofGiayPhep();
                    }

                    foreach (HangGiayPhepDetail hanggiayPhep in giayphep.ListHMDofGiayPhep)
                    {

                        HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hanggiayPhep.HMD_ID, listHang);

                        //Cap nhat thong tin hang thay doi cua rieng giay phep.
                        if (hmd != null)
                        {
                            hmd.MaHS = hanggiayPhep.MaHS;
                            hmd.MaPhu = hanggiayPhep.MaPhu;
                            hmd.TenHang = hanggiayPhep.TenHang;
                            hmd.NuocXX_ID = hanggiayPhep.NuocXX_ID;
                            hmd.DVT_ID = hanggiayPhep.DVT_ID;
                            hmd.SoLuong = hanggiayPhep.SoLuong;
                            hmd.DonGiaKB = hanggiayPhep.DonGiaKB;
                            hmd.TriGiaKB = hanggiayPhep.TriGiaKB;
                        }

                        XmlElement hangItem = doc.CreateElement("CHUNG_TU_GIAYPHEP.HANG");
                        giayphepItem.AppendChild(hangItem);

                        XmlAttribute MA_NPL_SP = doc.CreateAttribute("MA_NPL_SP");
                        MA_NPL_SP.Value = hmd.MaPhu.Trim();
                        hangItem.Attributes.Append(MA_NPL_SP);

                        XmlAttribute TEN_HANG = doc.CreateAttribute("TEN_HANG");
                        TEN_HANG.Value = FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                        hangItem.Attributes.Append(TEN_HANG);

                        XmlAttribute MA_HANGKB = doc.CreateAttribute("MA_HANGKB");
                        MA_HANGKB.Value = hmd.MaHS.Trim();
                        hangItem.Attributes.Append(MA_HANGKB);

                        XmlAttribute MA_DVT = doc.CreateAttribute("MA_DVT");
                        MA_DVT.Value = hmd.DVT_ID.Trim();
                        hangItem.Attributes.Append(MA_DVT);

                        XmlAttribute LUONG = doc.CreateAttribute("LUONG");
                        LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                        hangItem.Attributes.Append(LUONG);

                        XmlAttribute TRIGIA_KB = doc.CreateAttribute("TRIGIA_KB");
                        TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                        hangItem.Attributes.Append(TRIGIA_KB);


                        //chua co phai bo sung
                        XmlAttribute MA_NT = doc.CreateAttribute("MA_NT");
                        MA_NT.Value = hanggiayPhep.MaNguyenTe;
                        hangItem.Attributes.Append(MA_NT);

                        if (hanggiayPhep.MaChuyenNganh != null)
                        {
                            XmlAttribute MA_CN = doc.CreateAttribute("MA_CN");
                            MA_CN.Value = hanggiayPhep.MaChuyenNganh.Trim();
                            hangItem.Attributes.Append(MA_CN);
                        }
                    }
                }


                if (CHUNG_TU_GIAYPHEP.ChildNodes.Count == 0)
                    return null;
                return CHUNG_TU_GIAYPHEP;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return null; }
        }

        public string send(string password, string maHQ, long soTK, string maLH, int namDK, long TKMD_ID, List<HangMauDich> listHang)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];
            nodeTO_KHAI.Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<GiayPhep> listGiayPhep = new List<GiayPhep>();
            listGiayPhep.Add(this);
            nodeDulieu.AppendChild(GiayPhep.ConvertCollectionGiayPhepToXML(doc, listGiayPhep, TKMD_ID, listHang));


            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public bool WSKhaiBaoGP(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, List<GiayPhep> list, MessageTypes messageType, MessageFunctions messageFunction, List<HangMauDich> listHang)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GuidStr;
            this.Update();


            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<GiayPhep> listGiayPhep = new List<GiayPhep>();
            listGiayPhep.Add(this);
            xmlNodeDulieu.AppendChild(GiayPhep.ConvertCollectionGiayPhepToXML(xmlDocument, listGiayPhep, TKMD_ID, listHang));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            string msgError = string.Empty;
            try
            {

                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
                xmlDocument.InnerXml.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungGiayPhep);
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                        XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                        XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                        return true;
                    }
                }
                else
                {
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL";
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty) throw ex;
                if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(new Exception(xmlDocument.InnerXml));
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            return false;

        }

        public string LayPhanHoi(string password, string maHQ)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\LayPhanHoiDaDuyet.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            //if (GuidStr == "")
            //{
            //    GuidStr = (System.Guid.NewGuid().ToString()); ;
            //    nodeReference.InnerText = this.GuidStr;
            //    this.Update();
            //}
            nodeReference.InnerText = this.GuidStr;

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;




            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            nodeDulieu.Attributes[0].Value = this.GuidStr;

            KDTService kdt = WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                string msgError = string.Empty;
                try
                {

                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, password);
                    doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoBoSungLayPhanhoiGiayPhep);


                    docNPL.LoadXml(kq);
                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    if (Result.Attributes["Err"].Value == "no")
                    {
                        if (Result.Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                        {
                            msgError = "Thông báo từ hải quan :" + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                            throw new Exception(msgError);
                        }
                        else
                        {
                            msgError = "Thông báo từ hải quan :" + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                            throw new Exception(msgError);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty) throw ex;
                    if (!string.IsNullOrEmpty(kq))
                    {
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                    }
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }

            return "";
        }

        public string GenerateToXmlEnvelope(MessageTypes messageType, MessageFunctions messageFunction, Guid referenceId, string path, string maHQ, string maDN)
        {
            XmlDocument doc = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");
                else
                    doc.Load(path + @"\TemplateXML\PhongBi.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = Globals.trimMaHaiQuan(maHQ);

            // Subject.
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)messageType).ToString();

            // Function.
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference ID.
            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = referenceId.ToString();

            // Message ID.
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = System.Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = maDN;
            nodeFrom.ChildNodes[0].InnerText = maDN;

            this.GuidStr = referenceId.ToString();
            this.Update();
            return doc.InnerXml;
        }

        public bool WSLaySoTiepNhan(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            string msgError = string.Empty;
            XmlNode xmlNodeResult = null;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);


                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                 xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = "Thông báo từ hải quan :" + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan :" + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }
                }
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    msgError = "Thông báo từ hải quan :" + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                    throw new Exception(msgError);
                }
            }
            catch(Exception ex)
            {
                if (msgError != string.Empty) throw ex;
                if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(new Exception(xmlDocument.InnerXml));
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            try
            {
                
                if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();
                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungGiayPhepThanhCong, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
            return false;
        }

        public bool WSLayPhanHoi(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }
            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);                
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }
                }

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                    throw new Exception(msgError);
                }
                if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
                {

                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungGiayPhepTuChoiChapNhan,
                        string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), xmlDocumentResult.InnerText));
                    // Cập lại số tiếp nhận
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.Update();

                    return true;
                }
                else if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();

                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungGiayPhepThanhCong, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                    return true;
                }
                else if (xmlNodeResult.Attributes["SOTN"] != null)
                {

                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungGiayPhepThanhCong, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));
                    this.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                    this.Update();
                    return true;
                }

            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID,MessageTitle.Error,ex.Message);
                }
                Logger.LocalLogger.Instance().WriteMessage(new Exception(xmlDocument.InnerXml));                

            }
            return false;
        }

    }
}