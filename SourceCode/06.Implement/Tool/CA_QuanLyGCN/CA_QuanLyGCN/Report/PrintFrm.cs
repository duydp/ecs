﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraPrinting;

namespace CA_QuanLyGCN.Report
{
    public partial class PrintFrm : Form
    {
        public ChungNhan chungNhan { get; set; }
       
        PrintingSystem printingSystem1 = new PrintingSystem();
        public PrintFrm()
        {
            InitializeComponent();
        }

        private void PrintFrm_Load(object sender, EventArgs e)
        {
            //printControl1.PrintingSystem = rp.PrintingSystem;
            printControl1.PrintingSystem = new PrintingSystem();
            printControl1.PrintingSystem.StartPrint += new DevExpress.XtraPrinting.PrintDocumentEventHandler(PrintingSystem_StartPrint);
            printControl1.PrintingSystem.EndPrint += new EventHandler(PrintingSystem_EndPrint);
            printControl1.PrintingSystem.PrintProgress += new PrintProgressEventHandler(PrintingSystem_PrintProgress);
            printControl1.PrintingSystem.ShowMarginsWarning = false;
            LoadReport(true);
        }

        void PrintingSystem_PrintProgress(object sender, PrintProgressEventArgs e)
        {
            
        }

        void PrintingSystem_EndPrint(object sender, EventArgs e)
        {
            LoadReport(true);
        }

        void PrintingSystem_StartPrint(object sender, DevExpress.XtraPrinting.PrintDocumentEventArgs e)
        {
            LoadReport(false);
//             CA_QuanLyGCN.Report.GiayChungNhanReport rp2 = new CA_QuanLyGCN.Report.GiayChungNhanReport();
//             rp2.gcn = chungNhan;
//             rp2.printImage = false;
//             //rp2.PrintingSystem = printControl1.PrintingSystem;
//             
//             rp2.BindReport();    
//             rp2.CreateDocument();
//             rp2.Print();
//             return;
        }
        private void LoadReport(bool HaveImage)
        {
            printControl1.PrintingSystem.Pages.Clear();
            CA_QuanLyGCN.Report.GiayChungNhanReport rp = new CA_QuanLyGCN.Report.GiayChungNhanReport();
            rp.gcn = chungNhan;
            rp.printImage = HaveImage;
            rp.BindReport();
            rp.CreateDocument();
            printControl1.PrintingSystem.Pages.AddRange(rp.Pages);
            
        }
    }
}
