﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.BLL.Utils;
using System.Threading;
using Company.KDT.SHARE.Components.Utils;
namespace Company.BLL.KDT.SXXK
{
    public partial class DinhMucDangKySUA
    {
        public static DataSet getDinhMucSUADaDK_OfSanPham(long masterID, string mahaiquan, string madv)
        {
            string sql = "SELECT dm.ID as ID, MaSanPham, MaNguyenPhuLieu, DinhMucSuDung,TyLeHaoHut,(DinhMucSuDung+DinhMucSuDung*TyLeHaoHut/100)as DinhMucChung " +
                        ", npl.Ten as TenNPL, npl.DVT_ID as DVT_ID " +
                       "FROM [dbo].[t_KDT_SXXK_DinhMucSUA] dm " +
                       "inner join t_SXXK_NguyenPhuLieu npl " +
                       "on dm.MaNguyenPhuLieu=npl.Ma " + " and npl.MaHaiQuan = @MaHaiQuan "+
                //"inner join t_HaiQuan_DonViTinh dvt on dvt.id=npl.DVT_ID " +[MaSanPham]=@MaSanPham and
                        "WHERE  dm.[Master_IDSUA] = @Master_ID and MaHaiQuan = @MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.VarChar, masterID);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahaiquan);

            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);

            dbCommand.Connection.Close();
            return ds;
        }

        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan.Trim());
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            nodeFrom.ChildNodes[0].InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DoiTac.GetName(this.MaDoanhNghiep);
            this.Update();
            return doc.InnerXml;
        }

        public string WSDownLoad(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiPhanHoi(MessgaseType.DinhMuc, MessgaseFunction.LayPhanHoi));
            
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan.Trim());

            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);


            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string WSSendXMLSuaDM(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DinhMuc, MessgaseFunction.KhaiBao));
            XmlDocument docSP = new XmlDocument();
            docSP.LoadXml(ConvertCollectionToXML());


            //luu vao string
            XmlNode root = doc.ImportNode(docSP.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";
        }

        private string ConvertCollectionToXML()
        {
            //load du lieu
            XmlDocument docDM = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docDM.Load(path + "\\TemplateXML\\KhaiBaoDinhMucSUA.xml");
            XmlNode DinhMucNode = docDM.GetElementsByTagName("DINH_MUC")[0];

            XmlNode nodeHQNhan = docDM.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan.Trim());

            XmlNode nodeDN = docDM.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DoiTac.GetName(this.MaDoanhNghiep);

            string st = "";
            XmlNode nodeSP = null;
            CultureInfo culture = new CultureInfo("vi-VN");
            NumberFormatInfo f = new NumberFormatInfo();
            if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            {
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";
            }
            //CultureInfo culture = new CultureInfo("en-US");
            //NumberFormatInfo f = new NumberFormatInfo(); BaseClass.Round(this.TyGiaTinhThue, 9);

            List<DinhMucSUA> listDM = new List<DinhMucSUA>();
            listDM = (List<DinhMucSUA>)DinhMucSUA.SelectCollectionBy_Master_IDSUA(this.ID);
            foreach (DinhMucSUA dm in listDM)
            {
                if (dm.MaSanPham != st)
                {
                    st = dm.MaSanPham;
                    nodeSP = docDM.CreateElement("DINH_MUC.SP");
                    XmlAttribute attMASP = docDM.CreateAttribute("MA_SP");
                    attMASP.Value = dm.MaSanPham;
                    nodeSP.Attributes.Append(attMASP);
                    DinhMucNode.AppendChild(nodeSP);
                }
                XmlNode nodeNPL = docDM.CreateElement("DINH_MUC.SP.ITEM");

                XmlAttribute sttAtt = docDM.CreateAttribute("STT");
                sttAtt.Value = dm.STTHang.ToString();
                nodeNPL.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docDM.CreateAttribute("MA_NPL");
                maAtt.Value = dm.MaNguyenPhuLieu;
                nodeNPL.Attributes.Append(maAtt);

                XmlAttribute DinhMucSDAtt = docDM.CreateAttribute("DINH_MUC_SD");
                DinhMucSDAtt.Value = dm.DinhMucSuDung.ToString(f);
                nodeNPL.Attributes.Append(DinhMucSDAtt);

                XmlAttribute TyLeHHAtt = docDM.CreateAttribute("TY_LE_HH");
                TyLeHHAtt.Value = dm.TyLeHaoHut.ToString(f);
                nodeNPL.Attributes.Append(TyLeHHAtt);
                nodeSP.AppendChild(nodeNPL);
            }
            return docDM.InnerXml;
        }

        #region Lấy phản hồi 

        private string ConfigPhongBiPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + @"\TemplateXML\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan.Trim());
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            nodeFrom.ChildNodes[0].InnerText = this.MaDoanhNghiep;//TenDoanhNghiep

            return doc.InnerXml;
        }

        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.GetElementsByTagName("function")[0].InnerText = MessgaseFunction.LayPhanHoi.ToString();
            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);

            string kq = "";
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);
                XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (nodeRoot.Attributes["Err"].Value == "no")
                {
                    if (nodeRoot.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else if (FontConverter.TCVN2Unicode(nodeRoot.InnerText).Equals("Chứng từ không xử lý"))
                {
                    return doc.InnerXml;
                }
                else
                {
                    throw new Exception(FontConverter.TCVN2Unicode(nodeRoot.InnerText) + "|" + "Web Service");
                }
            }

            if (i > 1)
                return doc.InnerXml;

            /*XU LY THONG TIN MESSAGE PHAN HOI TU HAI QUAN*/
            XmlNode nodeRoot1 = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
            if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
            {
                XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == "SUA_DM")
                {
                    #region Lấy số tiếp nhận của danh sách Định mức

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                        this.NgayTiepNhan = DateTime.Today;
                        this.NamDK = Convert.ToInt16(nodeDuLieu.Attributes["NAM_TN"].Value);
                        this.TrangThaiXuLy = 0;
                        this.Update();
                        //MsgSend sendXML = new MsgSend();
                        //sendXML.LoaiHS = "DM_SUA";
                        //sendXML.master_id = this.ID;
                        //sendXML.Load();
                        //sendXML.msg = kq;
                        //sendXML.InsertUpdate();
                    }
                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = "DM";
                    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                    kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                    #endregion Lấy số tiếp nhận của danh sách Định mức
                }
            }
            else if (nodeRoot1 != null && nodeRoot1.Attributes.GetNamedItem("TuChoi") != null
                && nodeRoot1.Attributes["TuChoi"].Value == "yes")// Từ chối
            {
                #region Từ chối định mức 
                
                this.HUONGDAN = FontConverter.TCVN2Unicode(nodeRoot1.InnerText);
                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                this.Update();

                KetQuaXuLy kqxl = new KetQuaXuLy();
                kqxl.ItemID = this.ID;
                kqxl.ReferenceID = new Guid(this.GUIDSTR);
                kqxl.LoaiChungTu = "DM";
                kqxl.LoaiThongDiep = "Định mức sửa bị từ chối";
                kqxl.NoiDung = FontConverter.TCVN2Unicode(nodeRoot1.InnerText);
                kqxl.Ngay = DateTime.Now;
                kqxl.Insert();

                #endregion
            }
            else if (nodeRoot1 != null && nodeRoot1.SelectSingleNode("PHAN_LUONG") != null)
            {
                #region Phân luồng
                
                XmlNode nodePhanLuong = docNPL.SelectSingleNode("Envelope/Body/Content/Root").SelectSingleNode("PHAN_LUONG");
                this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                this.HUONGDAN = nodePhanLuong.Attributes["HUONGDAN"].Value;
                this.Update();
                string tenluong = "Xanh";
                if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                    tenluong = "Vàng";
                else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                    tenluong = "Đỏ";
                KetQuaXuLy kqxl = new KetQuaXuLy();
                kqxl.ItemID = this.ID;
                kqxl.ReferenceID = new Guid(this.GUIDSTR);
                kqxl.LoaiChungTu = "DM";
                kqxl.LoaiThongDiep = "Định mức sửa được phân luồng";
                kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nHải quan: {2}\r\nPhân luồng: {3}\r\nHướng dẫn: {4}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), this.MaHaiQuan.Trim().Trim(), tenluong, this.HUONGDAN);
                kqxl.Ngay = DateTime.Now;
                kqxl.Insert();

                #endregion
            }
            else if (nodeRoot1 != null && nodeRoot1.Attributes.GetNamedItem("TrangThai") != null && nodeRoot1.Attributes["TrangThai"].Value == "yes"
                && nodeRoot1.Attributes["SOTN"].Value != "")
            {
                #region Duyệt định mức 
                
                KetQuaXuLy kqxl = new KetQuaXuLy();
                kqxl.ItemID = this.ID;
                kqxl.ReferenceID = new Guid(this.GUIDSTR);
                kqxl.LoaiChungTu = "DM";
                kqxl.LoaiThongDiep = "Định mức sửa được duyệt";
                kqxl.NoiDung = string.Format("Định mức sửa được duyệt: Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nHải quan: {2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), this.MaHaiQuan.Trim().Trim());
                kqxl.Ngay = DateTime.Now;
                kqxl.Insert();

                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                this.Update();
                //cập nhật lại Định mức gốc
                List<DinhMucSUA> dmSUAList = (List<DinhMucSUA>)DinhMucSUA.SelectCollectionBy_Master_IDSUA(this.ID);
                DinhMuc dmGoc = new DinhMuc();
                DinhMuc dmTemp = new DinhMuc();
                long masterID = 0;
                foreach (DinhMucSUA dmSUA in dmSUAList)
                {
                    //Cập nhật vào bảng t_KDT_SXXK_DinhMuc
                    dmGoc = DinhMuc.GetDinhMucByMaSP_NPL(dmSUA.MaSanPham, dmSUA.MaNguyenPhuLieu, this.MaHaiQuan.Trim(), this.MaDoanhNghiep);
                    dmTemp = DinhMuc.GetDinhMucByMaSP(dmSUA.MaSanPham, this.MaHaiQuan.Trim(), this.MaDoanhNghiep);
                    masterID = dmTemp.Master_ID;
                    if (dmGoc == null)
                    {
                        dmGoc = new DinhMuc();
                        dmGoc.ID = 0;
                        dmGoc.Master_ID = dmTemp.Master_ID;
                    }
                    dmGoc.MaSanPham = dmSUA.MaSanPham;
                    dmGoc.MaNguyenPhuLieu = dmSUA.MaNguyenPhuLieu;
                    dmGoc.DinhMucSuDung = dmSUA.DinhMucSuDung;
                    dmGoc.TyLeHaoHut = dmSUA.TyLeHaoHut;
                    dmGoc.DVT_ID = dmSUA.DVT_ID;
                    if (dmGoc.ID == 0)
                        dmGoc.Insert();
                    else
                        dmGoc.Update();
                    // Cập nhật vào bảng t_SXXK_DinhMuc
                    BLL.SXXK.DinhMuc dmSXXK = BLL.SXXK.DinhMuc.getDinhMuc(this.MaHaiQuan.Trim(), this.MaDoanhNghiep, dmGoc.MaSanPham, dmGoc.MaNguyenPhuLieu);
                    if (dmSXXK == null)
                    {
                        dmSXXK = new BLL.SXXK.DinhMuc();
                        dmSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                        dmSXXK.MaHaiQuan = this.MaHaiQuan.Trim();
                        dmSXXK.MaSanPHam = dmGoc.MaSanPham;
                        dmSXXK.MaNguyenPhuLieu = dmGoc.MaNguyenPhuLieu;
                        dmSXXK.DinhMucSuDung = dmSUA.DinhMucSuDung;
                        dmSXXK.TyLeHaoHut = dmSUA.TyLeHaoHut;
                        dmSXXK.DinhMucChung = Globals.tinhDinhMucChung(dmSUA.DinhMucSuDung, dmSUA.TyLeHaoHut);
                        dmSXXK.Insert();
                    }
                    else
                    {
                        dmSXXK.DinhMucSuDung = dmSUA.DinhMucSuDung;
                        dmSXXK.TyLeHaoHut = dmSUA.TyLeHaoHut;
                        dmSXXK.DinhMucChung = Globals.tinhDinhMucChung(dmSUA.DinhMucSuDung, dmSUA.TyLeHaoHut);
                        dmSXXK.Update();
                    }
                    // Cập nhật vào bảng t_SXXK_ThongTinDinhMuc
                }
                //Xóa Định mức không tồn tại trong danh sách khai báo sửa
                DinhMucCollection dmList = DinhMuc.SelectCollectionBy_Master_ID(masterID);
                foreach (DinhMuc dm in dmList)
                {
                    //Nếu không có trong danh sách khai báo sửa thì xóa trong danh sách ĐM cũ
                    DinhMucSUA obj = DinhMucSUA.SearchBy(dmSUAList, dm.MaSanPham, dm.MaNguyenPhuLieu);
                    if (obj == null)
                    {
                        BLL.SXXK.DinhMuc.getDinhMuc(this.MaHaiQuan.Trim(), this.MaDoanhNghiep, dm.MaSanPham, dm.MaNguyenPhuLieu).Delete();
                        dm.Delete();
                    }
                }
                #endregion
            }
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
            {
                #region Lỗi

                XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                string errorSt = "";
                if (stMucLoi == "XML_LEVEL")
                    errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                else if (stMucLoi == "DATA_LEVEL")
                    errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                else if (stMucLoi == "SERVICE_LEVEL")
                    errorSt = "Lỗi do Web service trả về ";
                else if (stMucLoi == "DOTNET_LEVEL")
                    errorSt = "Lỗi do hệ thống của hải quan ";
                throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);

                #endregion
            }
            return "";

        }

        private void TransgferDataToSXXK(XmlNode nodeDM)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.Update(transaction);
                    //cap nhat vao nbang thong tin dinh muc
                    foreach (XmlNode node in nodeDM.ChildNodes)
                    {
                        BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                        ttdm.MaDoanhNghiep = this.MaDoanhNghiep;
                        ttdm.MaHaiQuan = this.MaHaiQuan.Trim();
                        ttdm.MaSanPham = node.Attributes["MA_SP"].Value;
                        ttdm.NgayDangKy = Convert.ToDateTime(node.Attributes["NGAY_DK"].Value);
                        ttdm.SoDinhMuc = Convert.ToInt32(node.Attributes["SO_DM"].Value);
                        ttdm.ThanhLy = 0;
                        ttdm.InsertUpdateTransaction(transaction);
                    }
                    //cp nhat vao bang dinh muc

                    //foreach (DinhMuc dm in listDM)
                    //{
                    //    BLL.SXXK.DinhMuc dmSXXK = new Company.BLL.SXXK.DinhMuc();
                    //    dmSXXK.DinhMucSuDung = dm.DinhMucSuDung;
                    //    dmSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                    //    dmSXXK.MaHaiQuan = this.MaHaiQuan.Trim();
                    //    dmSXXK.MaNguyenPhuLieu = dm.MaNguyenPhuLieu;
                    //    dmSXXK.MaSanPHam = dm.MaSanPham;
                    //    dmSXXK.TyLeHaoHut = dm.TyLeHaoHut;
                    //    dmSXXK.DinhMucChung = dm.DinhMucSuDung * (dm.TyLeHaoHut / 100 + 1);
                    //    dmSXXK.GhiChu = "";
                    //    dmSXXK.InsertUpdateTransaction(transaction);
                    //}
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
    }
}
