using System;
using System.Data;
using System.Windows.Forms;
//using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace SaoChepChuyenDaDuyetMaHQKhac
{
    public partial class DonViHaiQuanControl : UserControl
    {
        public DonViHaiQuanControl()
        {
            this.InitializeComponent();
        }

        public string Ma
        {
            set
            {
                this.txtMa.Text = value;
                this.cbTen.Value = this.txtMa.Text.PadRight(6);
            }
            get { return this.txtMa.Text; }
        }
        public string Ten
        {
            get { return this.cbTen.Text; }
        }
        public bool ReadOnly
        {
            set
            {
                this.cbTen.ReadOnly = value;
                this.txtMa.ReadOnly = value;
            }
            get { return this.cbTen.ReadOnly; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.cbTen.VisualStyleManager = value;
                this.txtMa.VisualStyleManager = value;
            }
            get
            {
                return this.txtMa.VisualStyleManager;
            }
        }
        private string _MaCuc = string.Empty;
        public string MaCuc
        {
            set { this._MaCuc = value; }
            get { return this._MaCuc; }
        }

        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);

        public void loadData()
        {
            try
            {
                if (this._MaCuc.Trim().Length == 0) this._MaCuc = GlobalSettings.MA_CUC_HAI_QUAN;
                if (this.txtMa.Text.Trim().Length == 0) this.txtMa.Text = GlobalSettings.MA_HAI_QUAN;

                DataTable dt;
                dt = SelectAll().Tables[0];
                dtDonViHaiQuan.Rows.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    this.dtDonViHaiQuan.ImportRow(row);
                }

                this.cbTen.Value = this.txtMa.Text.PadRight(6);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw ex; }
        }

        private void DonViHaiQuanControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                //this.cbTen.ReadOnly = true;
                //this.txtMa.ReadOnly = true;
                this.loadData();
            }
        }

        private void cbTen_ValueChanged(object sender, EventArgs e)
        {
            this.txtMa.Text = this.cbTen.Value.ToString().Trim();
            if (this.ValueChanged != null) this.ValueChanged(sender, e);
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {
            this.txtMa.Text = this.txtMa.Text.Trim();
            if (this.dtDonViHaiQuan.Select("ID='" + txtMa.Text.Trim() + "'").Length == 0)
            {
                this.txtMa.Text = GlobalSettings.MA_HAI_QUAN;
            }
            this.cbTen.Value = this.txtMa.Text.PadRight(6);
        }

        public static DataSet SelectAll()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = "SELECT * FROM t_HaiQuan_DonViHaiQuan ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }
    }
}