﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KeySecurity;

namespace LicenseCode
{
    public partial class frmUser : Form
    {
        public frmUser()
        {
            InitializeComponent();
        }

        public KeyServer.User.User User = new KeyServer.User.User();
        public string UserName = string.Empty;
        public string Pass = string.Empty;
        public bool isEdit = false;
        public bool isSuccess = false;
        private ProductKey.ProductKey productServices = new ProductKey.ProductKey();


        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                
                Get();
                string result = string.Empty;
                string _user = KeyCode.Serializer(User);
                _user = KeyCode.ConvertToBase64(_user);
                if (isEdit)
                    result = productServices.UpdatePermission(UserName, Pass, _user);
                else
                    result = productServices.CreateUser(UserName, Pass, _user);
                if (!string.IsNullOrEmpty(result))
                    MessageBoxControl.ShowMessage("Lỗi khi cập nhật User: " + result);
                else
                {
                    if (!isEdit)
                        frmMain.listUser.Add(User);
                    isSuccess = true;
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi: " + ex.Message);
            }
        }
        private void Set()
        {
            if (isEdit)
            {
                txtUserName.Text = User.UserName;
                txtPass.Text = User.PassWord;
                txtHo.Text = User.FirstName;
                txtTen.Text = User.LastName;
                txtPhongBan.Text = User.Department;
                txtChucVu.Text = User.position;
                txtEmail.Text = User.Email;
                txtThongTinKhac.Text = User.Title;
                trvPermission.Nodes[0].Checked = User.Permission.Admin;
                trvPermission.Nodes[1].Checked = User.Permission.GenKey;
                trvPermission.Nodes[2].Checked = User.Permission.GetLicense;
                trvPermission.Nodes[3].Checked = User.Permission.GetTrial;
                trvPermission.Nodes[4].Checked = User.Permission.UpdateNotices;
            }
        }
        private void Get()
        {
            User.UserName = txtUserName.Text;
            string _pass = txtPass.Text;
            if (!string.IsNullOrEmpty(_pass))
            {
                _pass = KeyCode.HashMD5(KeyCode.Encrypt(_pass));
                User.PassWord = _pass;
            }
            User.FirstName = txtHo.Text;
            User.LastName = txtTen.Text;
            User.Department = txtPhongBan.Text;
            User.position = txtChucVu.Text;
            User.Email = txtEmail.Text;
            User.Title = txtThongTinKhac.Text;
            if (User.Permission == null)
                User.Permission = new KeyServer.User.Permission();
            User.Permission.Admin = trvPermission.Nodes[0].Checked;
            User.Permission.GenKey = trvPermission.Nodes[1].Checked;
            User.Permission.GetLicense = trvPermission.Nodes[2].Checked;
            User.Permission.GetTrial = trvPermission.Nodes[3].Checked;
            User.Permission.UpdateNotices = trvPermission.Nodes[4].Checked;
        }
        private void KhoiTaoGiaDien()
        {
            if (isEdit)
            {
                txtUserName.Enabled = txtPass.Enabled = false;
                txtUserName.BackColor = txtPass.BackColor = Color.LightGoldenrodYellow;
            }
        }

        private void btnResetPass_Click(object sender, EventArgs e)
        {
            txtPass.Enabled = true;
            txtPass.BackColor = Color.White;
        }
        private  string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "abcdefghijklmnopqrstuvwxyz0123456789";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }
        private bool CheckEmail(string str)
        {
            System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (re.IsMatch(str))
                return true;
            else
                return false;
        }

        private void frmUser_Load(object sender, EventArgs e)
        {
            Set();
            KhoiTaoGiaDien();
            trvPermission.ExpandAll();
        }
    }
}
