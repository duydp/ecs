﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
  public  class MemberCompany
    {
        /// <summary>
        /// Tên doanh nghiệp
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Mã doanh nghiệp
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; }

        /// <summary>
        /// Địa chỉ chi nhánh
        /// </summary>
        [XmlElement("address")]
        public string Address { get; set; }
    }
}
