﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public partial class NhomCuaKhau
    {
        public static DataSet GetCuaKhau(string cuc_ID)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetStoredProcCommand("p_HaiQuan_NhomCuaKhauGetCuaKhau");
            db.AddInParameter(dbCommand, "@Cuc_ID", SqlDbType.VarChar, cuc_ID);            
            return db.ExecuteDataSet(dbCommand);
        }
        public static string GetDonVi(string CuaKhau_ID)
        {            
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetStoredProcCommand("p_HaiQuan_NhomCuaKhauGetDonVi");
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.VarChar, CuaKhau_ID);
            object ret = db.ExecuteScalar(dbCommand);
            return ret != null ? ret.ToString() : string.Empty;
        }
        public static bool DeleteCollectionItem(IList<NhomCuaKhau> collection)
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (NhomCuaKhau item in collection)
                        {
                            item.Delete(transaction);                            
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);                        
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
                ret = true;
            }
            return ret;
        }
    }
}
