using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using System.Data;

namespace Company.Interface.Report.WareHouse
{
    public partial class ReportViewPhieuXuatKho : DevExpress.XtraReports.UI.XtraReport
    {
        public T_KHOKETOAN_PHIEUXNKHO PhieuXNKho;
        private decimal TongThanhTien = 0;
        public ReportViewPhieuXuatKho()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            lblTenDoanhNghiep.Text = PhieuXNKho.TENDN.ToString();
            txtBoPhan.Text = "Kế toán";
            lblNgay.Text = PhieuXNKho.NGAYCT.Day.ToString();
            lblThang.Text = PhieuXNKho.NGAYCT.Month.ToString();
            lblNam.Text = PhieuXNKho.NGAYCT.Year.ToString();

            //lblTKCo.Text = PhieuXNKho.TKKHO.ToString();
            //lblTKNo.Text = PhieuXNKho.TKKHO.ToString();
            lblSoCT.Text = PhieuXNKho.SOCT.ToString();

            lblNguoiGiao.Text = PhieuXNKho.NGUOIVANCHUYEN.ToString();
            lblHoaDon.Text = PhieuXNKho.SOHOADON.ToString();

            lblNgayHD.Text = PhieuXNKho.NGAYHOADON.Day.ToString();
            lblThangHD.Text = PhieuXNKho.NGAYHOADON.Month.ToString();
            lblNamHD.Text = PhieuXNKho.NGAYHOADON.Year.ToString();

            lblTenKho.Text = PhieuXNKho.TENKHO.ToString();
            lblDiaChi.Text = PhieuXNKho.DIACHIDN.ToString();
            lblSoCTGoc.Text = PhieuXNKho.SOCTGOC.ToString();
            DataTable dt = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectBy_PHIEUXNKHO_ID(PhieuXNKho.ID).Tables[0];
            dt.TableName = "PHIEUXNKHO_HANGHOA";
            this.DataSource = dt;

            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblTenHangHoa.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TENHANG");
            lblMaHangHoa.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MAHANG");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblTheoCT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".GHICHU");
            lblSoLuong.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SOLUONGQD", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblDonGia.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DONGIANT", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblThanhTien.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TRIGIANTSAUPB", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
        }

        private void lblTongThanhTien_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {            
            lblTongThanhTien.Text = TongThanhTien.ToString("N5");
        }

        private void lblThanhTien_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            TongThanhTien += Convert.ToDecimal(lblThanhTien.Text.ToString());
            lblTongTien.Text = ChuyenSo(TongThanhTien.ToString());
            lblTongTien.Text = UppercaseFirst(lblTongTien.Text);
        }
        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
        public static string ChuyenSo(string number)
        {
            string[] strTachPhanSauDauPhay;
            if (number.Contains(".") || number.Contains(","))
            {
                strTachPhanSauDauPhay = number.Split(',', '.');
                return (ChuyenSo(strTachPhanSauDauPhay[0]) + "phẩy " + ChuyenSo(strTachPhanSauDauPhay[1]) + "đồng");
            }

            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "linh ";
                                    ddv = 0;
                                }
                                break;
                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;
                            case '5':
                                if ((i + j == len - 1) || (i + j + 3 == len - 1))
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;
                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += ((n - j) != 1) ? dv[n - j - 1] + " " : dv[n - j - 1];
                        }
                    }
                }


                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48];

            return doc;
        }
    }
}
