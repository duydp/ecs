﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public abstract class ApproverMessageInfo
    {
        public delegate bool MessageHandler(object sender, MessageEventArgs e);
        public MessageHandler RaiseMessageHandler;
        public abstract bool ProcessMessageHandler(object sender, MessageEventArgs e);

        public ApproverMessageInfo()
        {
            RaiseMessageHandler += new MessageHandler(ProcessMessageHandler);
        }
        public bool ProcessRequest(MessageInfo messageInfo)
        {
            MessageEventArgs e = new MessageEventArgs { MessageInfo = messageInfo };            
            return OnMessage(e);
        }
        public virtual bool OnMessage(MessageEventArgs e)
        {
            if (RaiseMessageHandler != null)
                return RaiseMessageHandler(this, e);
            else
                return false;
        }
        public ApproverMessageInfo Successor { get; set; }
    }   

}
