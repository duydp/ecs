﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Janus.Windows.CalendarCombo;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using CuaKhauControl = Company.Interface.Controls.CuaKhauControl;
using DonViHaiQuanControl = Company.Interface.Controls.DonViHaiQuanControl;
using LoaiHinhMauDichControl01 = Company.Interface.Controls.LoaiHinhMauDichVControl;
using NguyenTeControl = Company.Interface.Controls.NguyenTeControl;
using NuocControl = Company.Interface.Controls.NuocControl;

//namespace Company.KDT.SHARE.Components
namespace Company.Interface.VNACCS.PhieuKho
{
    partial class PhieuKho
    {
        private Label label9;
        private Label label30;
        private NumericEditBox txtTyGiaTinhThue;
        private NumericEditBox txtTyGiaUSD;
        private UIGroupBox uiGroupBox2;
        private UIGroupBox uiGroupBox1;
        private UICommandManager cmMain;
        private UICommand cmdSave;
        private UIRebar TopRebar1;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private IContainer components;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhieuKho));
            this.txtTyGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTyGiaUSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayTN = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.linkHDSD = new System.Windows.Forms.LinkLabel();
            this.cbbMaKhoCFS = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.cbbCoQuanHQ = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbToolBar = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSendV51 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendV5");
            this.cmdCancel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdAdd1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAdd");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdTruyenDuLieuTuXa = new Janus.Windows.UI.CommandBars.UICommand("cmdTruyenDuLieuTuXa");
            this.cmdSendTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendV5");
            this.NhanDuLieu2 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.XacNhan2 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSendV5 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendV5");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdAdd = new Janus.Windows.UI.CommandBars.UICommand("cmdAdd");
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdReadExcel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.ChungTuKemTheo2 = new Janus.Windows.UI.CommandBars.UICommand("ChungTuKemTheo");
            this.InToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("InToKhai");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdThemHang2 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 539), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 29);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 539);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 515);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 515);
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(203, 29);
            this.grbMain.Size = new System.Drawing.Size(872, 539);
            // 
            // txtTyGiaTinhThue
            // 
            this.txtTyGiaTinhThue.DecimalDigits = 3;
            this.txtTyGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaTinhThue.Location = new System.Drawing.Point(80, 47);
            this.txtTyGiaTinhThue.Name = "txtTyGiaTinhThue";
            this.txtTyGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaTinhThue.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaTinhThue.TabIndex = 2;
            this.txtTyGiaTinhThue.Tag = "TyGiaTinhThue";
            this.txtTyGiaTinhThue.Text = "0.000";
            this.txtTyGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTyGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyGiaTinhThue.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(4, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 3;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(4, 55);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 13);
            this.label30.TabIndex = 1;
            // 
            // txtTyGiaUSD
            // 
            this.txtTyGiaUSD.DecimalDigits = 0;
            this.txtTyGiaUSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaUSD.Location = new System.Drawing.Point(80, 74);
            this.txtTyGiaUSD.Name = "txtTyGiaUSD";
            this.txtTyGiaUSD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaUSD.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaUSD.TabIndex = 4;
            this.txtTyGiaUSD.Text = "0";
            this.txtTyGiaUSD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaUSD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTyGiaUSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyGiaUSD.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(754, 139);
            this.uiGroupBox2.TabIndex = 258;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.grList);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(203, 29);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(872, 539);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grList.ColumnAutoResize = true;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Hierarchical = true;
            this.grList.Location = new System.Drawing.Point(0, 106);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(872, 388);
            this.grList.TabIndex = 81;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnXoa);
            this.uiGroupBox4.Controls.Add(this.btnClose);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 494);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(872, 45);
            this.uiGroupBox4.TabIndex = 80;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(702, 14);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(74, 23);
            this.btnXoa.TabIndex = 9;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(782, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.uiGroupBox3.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox3.Controls.Add(this.clcNgayTN);
            this.uiGroupBox3.Controls.Add(this.linkHDSD);
            this.uiGroupBox3.Controls.Add(this.cbbMaKhoCFS);
            this.uiGroupBox3.Controls.Add(this.cbbCoQuanHQ);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.label27);
            this.uiGroupBox3.Controls.Add(this.lblTrangThai);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(872, 106);
            this.uiGroupBox3.TabIndex = 78;
            this.uiGroupBox3.Text = "Thông tin khai báo";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayTN
            // 
            this.clcNgayTN.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTN.Appearance.Options.UseFont = true;
            this.clcNgayTN.Location = new System.Drawing.Point(606, 19);
            this.clcNgayTN.Name = "clcNgayTN";
            this.clcNgayTN.ReadOnly = false;
            this.clcNgayTN.Size = new System.Drawing.Size(132, 23);
            this.clcNgayTN.TabIndex = 332;
            this.clcNgayTN.TagName = "";
            this.clcNgayTN.Value = new System.DateTime(2020, 8, 4, 13, 53, 57, 0);
            this.clcNgayTN.WhereCondition = "";
            // 
            // linkHDSD
            // 
            this.linkHDSD.AutoSize = true;
            this.linkHDSD.BackColor = System.Drawing.Color.Transparent;
            this.linkHDSD.Location = new System.Drawing.Point(502, 82);
            this.linkHDSD.Name = "linkHDSD";
            this.linkHDSD.Size = new System.Drawing.Size(193, 13);
            this.linkHDSD.TabIndex = 331;
            this.linkHDSD.TabStop = true;
            this.linkHDSD.Text = "Hướng dẫn khai báo hàng vào kho CFS";
            this.linkHDSD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHDSD_LinkClicked);
            // 
            // cbbMaKhoCFS
            // 
            this.cbbMaKhoCFS.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cbbMaKhoCFS.Appearance.Options.UseBackColor = true;
            this.cbbMaKhoCFS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A204;
            this.cbbMaKhoCFS.Code = "";
            this.cbbMaKhoCFS.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbMaKhoCFS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.cbbMaKhoCFS.IsOnlyWarning = false;
            this.cbbMaKhoCFS.IsValidate = true;
            this.cbbMaKhoCFS.Location = new System.Drawing.Point(128, 55);
            this.cbbMaKhoCFS.Name = "cbbMaKhoCFS";
            this.cbbMaKhoCFS.Name_VN = "";
            this.cbbMaKhoCFS.SetOnlyWarning = false;
            this.cbbMaKhoCFS.SetValidate = false;
            this.cbbMaKhoCFS.ShowColumnCode = true;
            this.cbbMaKhoCFS.ShowColumnName = true;
            this.cbbMaKhoCFS.Size = new System.Drawing.Size(349, 21);
            this.cbbMaKhoCFS.TabIndex = 330;
            this.cbbMaKhoCFS.TagCode = "";
            this.cbbMaKhoCFS.TagName = "";
            this.cbbMaKhoCFS.Where = null;
            this.cbbMaKhoCFS.WhereCondition = "";
            this.cbbMaKhoCFS.Leave += new System.EventHandler(this.cbbMaKhoCFS_Leave);
            // 
            // cbbCoQuanHQ
            // 
            this.cbbCoQuanHQ.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cbbCoQuanHQ.Appearance.Options.UseBackColor = true;
            this.cbbCoQuanHQ.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.cbbCoQuanHQ.Code = "";
            this.cbbCoQuanHQ.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbCoQuanHQ.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.cbbCoQuanHQ.IsOnlyWarning = false;
            this.cbbCoQuanHQ.IsValidate = true;
            this.cbbCoQuanHQ.Location = new System.Drawing.Point(128, 20);
            this.cbbCoQuanHQ.Name = "cbbCoQuanHQ";
            this.cbbCoQuanHQ.Name_VN = "";
            this.cbbCoQuanHQ.SetOnlyWarning = false;
            this.cbbCoQuanHQ.SetValidate = false;
            this.cbbCoQuanHQ.ShowColumnCode = true;
            this.cbbCoQuanHQ.ShowColumnName = true;
            this.cbbCoQuanHQ.Size = new System.Drawing.Size(349, 21);
            this.cbbCoQuanHQ.TabIndex = 328;
            this.cbbCoQuanHQ.TagCode = "";
            this.cbbCoQuanHQ.TagName = "";
            this.cbbCoQuanHQ.Where = null;
            this.cbbCoQuanHQ.WhereCondition = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(497, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 107;
            this.label1.Text = "Trạng thái :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(497, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 107;
            this.label3.Text = "Ngày tiếp nhận :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 93;
            this.label4.Text = "Mã Kho CFS :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(12, 24);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(107, 13);
            this.label27.TabIndex = 88;
            this.label27.Text = "Hải quan tiếp nhận : ";
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(603, 55);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(87, 13);
            this.lblTrangThai.TabIndex = 85;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdTruyenDuLieuTuXa,
            this.cmdSendV5,
            this.cmdCancel,
            this.cmdUpdateGuidString,
            this.cmdAdd});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbToolBar_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbToolBar
            // 
            this.cmbToolBar.CommandManager = this.cmMain;
            this.cmbToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSendV51,
            this.cmdCancel1,
            this.cmdAdd1,
            this.cmdSave1,
            this.cmdFeedback1,
            this.cmdResult1,
            this.cmdUpdateGuidString1});
            this.cmbToolBar.FullRow = true;
            this.cmbToolBar.Key = "cmdToolBar";
            this.cmbToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmbToolBar.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbToolBar.MergeRowOrder = 1;
            this.cmbToolBar.Name = "cmbToolBar";
            this.cmbToolBar.RowIndex = 0;
            this.cmbToolBar.Size = new System.Drawing.Size(1078, 26);
            this.cmbToolBar.Text = "cmdToolBar";
            this.cmbToolBar.View = Janus.Windows.UI.CommandBars.View.SmallIcons;
            this.cmbToolBar.Wrappable = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // cmdSendV51
            // 
            this.cmdSendV51.Key = "cmdSendV5";
            this.cmdSendV51.Name = "cmdSendV51";
            // 
            // cmdCancel1
            // 
            this.cmdCancel1.Key = "cmdCancel";
            this.cmdCancel1.Name = "cmdCancel1";
            // 
            // cmdAdd1
            // 
            this.cmdAdd1.Key = "cmdAdd";
            this.cmdAdd1.Name = "cmdAdd1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave1.Image")));
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback1.Image")));
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult1.Image")));
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            this.cmdResult1.Text = "Kết quả xử lý";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            // 
            // cmdSave
            // 
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.MergeOrder = 3;
            this.cmdSave.MergeType = Janus.Windows.UI.CommandBars.CommandMergeType.MergeItems;
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSave.Text = "Lưu thông tin";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "Nhận dữ liệu";
            // 
            // cmdResult
            // 
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Xác nhận thông tin";
            // 
            // cmdTruyenDuLieuTuXa
            // 
            this.cmdTruyenDuLieuTuXa.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSendTK1,
            this.NhanDuLieu2,
            this.XacNhan2,
            this.Separator3,
            this.Separator4});
            this.cmdTruyenDuLieuTuXa.IsEditableControl = Janus.Windows.UI.InheritableBoolean.True;
            this.cmdTruyenDuLieuTuXa.Key = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.MdiList = Janus.Windows.UI.InheritableBoolean.True;
            this.cmdTruyenDuLieuTuXa.Name = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.Text = "Truyền dữ liệu từ xa";
            // 
            // cmdSendTK1
            // 
            this.cmdSendTK1.Key = "cmdSendV5";
            this.cmdSendTK1.Name = "cmdSendTK1";
            // 
            // NhanDuLieu2
            // 
            this.NhanDuLieu2.Key = "cmdFeedback";
            this.NhanDuLieu2.Name = "NhanDuLieu2";
            // 
            // XacNhan2
            // 
            this.XacNhan2.Key = "cmdResult";
            this.XacNhan2.Name = "XacNhan2";
            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdSendV5
            // 
            this.cmdSendV5.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendV5.Image")));
            this.cmdSendV5.Key = "cmdSendV5";
            this.cmdSendV5.Name = "cmdSendV5";
            this.cmdSendV5.Text = "Khai báo hàng vào kho CFS";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Image = ((System.Drawing.Image)(resources.GetObject("cmdCancel.Image")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Text = "Khai báo hủy hàng vào kho CFS";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Text = "Cập nhật chuỗi phản hồi";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdd.Image")));
            this.cmdAdd.Key = "cmdAdd";
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Text = "Thêm tờ khai";
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "attached.ico");
            this.ImageList1.Images.SetKeyName(5, "internet.ico");
            this.ImageList1.Images.SetKeyName(6, "explorerBarItem25.Icon.ico");
            this.ImageList1.Images.SetKeyName(7, "page_edit.png");
            this.ImageList1.Images.SetKeyName(8, "page_red.png");
            this.ImageList1.Images.SetKeyName(9, "printer.png");
            this.ImageList1.Images.SetKeyName(10, "export.png");
            this.ImageList1.Images.SetKeyName(11, "Yes.png");
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbToolBar);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1078, 26);
            // 
            // cmdReadExcel2
            // 
            this.cmdReadExcel2.Key = "cmdReadExcel";
            this.cmdReadExcel2.Name = "cmdReadExcel2";
            // 
            // ChungTuKemTheo2
            // 
            this.ChungTuKemTheo2.Key = "ChungTuKemTheo";
            this.ChungTuKemTheo2.Name = "ChungTuKemTheo2";
            // 
            // InToKhai1
            // 
            this.InToKhai1.Key = "InToKhai";
            this.InToKhai1.Name = "InToKhai1";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // cmdThemHang2
            // 
            this.cmdThemHang2.Key = "cmdThemHang";
            this.cmdThemHang2.Name = "cmdThemHang2";
            // 
            // PhieuKho
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1078, 571);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "PhieuKho";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Phiếu nhập kho";
            this.Load += new System.EventHandler(this.PhieuKho_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PhieuKho_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UICommand cmdFeedback;
        private UICommand cmdReadExcel2;
        private UICommand ChungTuKemTheo2;
        private UICommand cmdResult;
        private UICommand InToKhai1;
        private UICommand InPhieuTN1;
        private UICommand cmdTruyenDuLieuTuXa;
        private UICommand cmdThemHang2;
        private UICommand NhanDuLieu2;
        private UICommand XacNhan2;
        private UICommand Separator3;
        private UICommand Separator4;
        private UICommand cmdSendTK1;
        private UICommand cmdSendV5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDangKy;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaLoaiHinh;
        private UICommandBar cmbToolBar;
        private ImageList ImageList1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDiaDiemDichVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayKhoiHanhVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTrongLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTHoaDon;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTSoLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDDLuuKho;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayTiepNhan;
        private UIGroupBox uiGroupBox3;
        private Label label3;
        private Label label27;
        private Label lblTrangThai;
        private Label label4;
        private Label label1;
        private UICommand cmdCancel;
        private UICommand cmdSave1;
        private UICommand cmdFeedback1;
        private UICommand cmdResult1;
        private UICommand cmdUpdateGuidString;
        private UICommand cmdUpdateGuidString1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty cbbCoQuanHQ;
        private UICommand cmdAdd;
        private UICommand cmdAdd1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty cbbMaKhoCFS;
        private LinkLabel linkHDSD;
        private UICommand cmdSendV51;
        private UICommand cmdCancel1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayTN;
        private UIGroupBox uiGroupBox4;
        private UIButton btnXoa;
        private UIButton btnClose;
        private GridEX grList;
    }
}
