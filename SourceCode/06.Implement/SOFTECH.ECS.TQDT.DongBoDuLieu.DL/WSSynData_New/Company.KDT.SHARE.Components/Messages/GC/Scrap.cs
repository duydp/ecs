﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class Scrap
    {
        /// <summary>
        /// Thứ tự hàng
        /// </summary>
        [XmlElement("sequence")]
        public string sequence { get; set; }

        /// <summary>
        /// Chi tiết hàng
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }

        /// <summary>
        /// Số lượng và đơn vị tính
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public SXXK_GoodsMeasure GoodsMeasure { get; set; }


        ///// <summary>
        ///// Mã hàng
        ///// </summary>
        //[XmlElement("identification")]
        //public string Identification { get; set; }
        ///// <summary>
        ///// Tên hàng
        ///// </summary>
        //[XmlElement("name")]
        //public string Name { get; set; }
        ///// <summary>
        ///// Mã HS
        ///// </summary>
        //[XmlElement("tariffClassification")]
        //public string TariffClassification { get; set; }
        ///// <summary>
        ///// Loại hàng hóa Nguyên phụ liệu(1), sản phẩm(2), Thiết bị(3), Hàng mẫu(4)
        ///// </summary>
        //[XmlElement("commodityType")]
        //public string CommodityType { get; set; }
        ///// <summary>
        /////Số Lượng n..14,3
        ///// </summary>
        //[XmlElement("count")]
        //public string Count { get; set; }
        ///// <summary>
        ///// Mã đơn vị tính
        ///// </summary>
        //[XmlElement("measureUnit")]
        //public string MeasureUnit { get; set; }
        ///// <summary>
        ///// Ghi chú
        ///// </summary>
        //[XmlElement("description")]
        //public string Description { get; set; }



    }
}
