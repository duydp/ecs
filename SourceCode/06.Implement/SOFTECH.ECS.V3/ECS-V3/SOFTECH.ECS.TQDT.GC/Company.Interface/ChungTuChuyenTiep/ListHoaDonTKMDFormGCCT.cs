﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListHoaDonTKMDFormGCCT : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public bool IsBrowseForm = false;
        public HoaDon HoaDonThuongMaiSelected = new HoaDon();
        public ToKhaiChuyenTiep TKCT;
        //-----------------------------------------------------------------------------------------

        public ListHoaDonTKMDFormGCCT()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NguyenTe_ID"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value.ToString());
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            TKCT.LoadChungTuKem();
            dgList.DataSource = TKCT.HoaDonThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            
            //HUNGTQ, Uopdate 07/06/2010
            SetButtonStateHOADON(TKCT, isKhaiBoSung);
            
        }      

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HoaDon> listHoaDon = new List<HoaDon>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDon hd = (HoaDon)i.GetRow().DataRow;
                        listHoaDon.Add(hd);
                    }
                }
            }

            foreach (HoaDon hd in listHoaDon)
            {
                hd.Delete();
                TKCT.HoaDonThuongMaiCollection.Remove(hd);
            }

            dgList.DataSource = TKCT.HoaDonThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
              
        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            HoaDonFormGCCT f = new HoaDonFormGCCT();
            f.TKCT = TKCT;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKCT.HoaDonThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (!IsBrowseForm)
            {
                HoaDonFormGCCT f = new HoaDonFormGCCT();
                f.TKCT = TKCT;
                f.HDonTM = (HoaDon)e.Row.DataRow;
                f.isKhaiBoSung = this.isKhaiBoSung;
                f.ShowDialog();
                dgList.DataSource = TKCT.HoaDonThuongMaiCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
            else { 
                HoaDonThuongMaiSelected = (HoaDon)e.Row.DataRow;
                this.Close();
            }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form.
        /// </summary>
        /// <param name="tkct"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOADON(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, bool isKhaiBoSung)
        {
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    );

                btnXoa.Enabled = status;
                btnTaoMoi.Enabled = status;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkct.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    status = true;

                    btnXoa.Enabled = status;
                    btnTaoMoi.Enabled = status;
                }
            }

            return true;
        }

        #endregion

    }
}
