﻿namespace Company.Interface.KDT.GC
{
    partial class HistoryDetaiHopDongForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgNhomSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgNhomSanPham_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HistoryDetaiHopDongForm));
            Janus.Windows.GridEX.GridEXLayout dgNguyenPhuLieu_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgNguyenPhuLieu_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgSanPham_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgThietBi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgThietBi_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgHangMau_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgHangMau_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tabHopDong = new Janus.Windows.UI.Tab.UITab();
            this.tpNhomSanPham = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNhomSanPham = new Janus.Windows.GridEX.GridEX();
            this.tpNguyenPhuLieu = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNguyenPhuLieu = new Janus.Windows.GridEX.GridEX();
            this.tpSanPham = new Janus.Windows.UI.Tab.UITabPage();
            this.dgSanPham = new Janus.Windows.GridEX.GridEX();
            this.tpThietBi = new Janus.Windows.UI.Tab.UITabPage();
            this.dgThietBi = new Janus.Windows.GridEX.GridEX();
            this.tpHangMau = new Janus.Windows.UI.Tab.UITabPage();
            this.dgHangMau = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).BeginInit();
            this.tabHopDong.SuspendLayout();
            this.tpNhomSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNhomSanPham)).BeginInit();
            this.tpNguyenPhuLieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).BeginInit();
            this.tpSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).BeginInit();
            this.tpThietBi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).BeginInit();
            this.tpHangMau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHangMau)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.tabHopDong);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Size = new System.Drawing.Size(979, 495);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.btnExportExcel);
            this.uiGroupBox5.Controls.Add(this.btnClose);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 454);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(979, 41);
            this.uiGroupBox5.TabIndex = 345;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(12, 12);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(90, 23);
            this.btnExportExcel.TabIndex = 70;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(895, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnSearch);
            this.uiGroupBox6.Controls.Add(this.label6);
            this.uiGroupBox6.Controls.Add(this.txtMaHangHoa);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(979, 50);
            this.uiGroupBox6.TabIndex = 348;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(399, 16);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(109, 23);
            this.btnSearch.TabIndex = 87;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 86;
            this.label6.Text = "Mã hàng hoá";
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHangHoa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHangHoa.BackColor = System.Drawing.Color.White;
            this.txtMaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangHoa.Location = new System.Drawing.Point(104, 17);
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.Size = new System.Drawing.Size(280, 21);
            this.txtMaHangHoa.TabIndex = 85;
            this.txtMaHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHangHoa.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // tabHopDong
            // 
            this.tabHopDong.BackColor = System.Drawing.Color.Transparent;
            this.tabHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabHopDong.Location = new System.Drawing.Point(0, 50);
            this.tabHopDong.Name = "tabHopDong";
            this.tabHopDong.Size = new System.Drawing.Size(979, 404);
            this.tabHopDong.TabIndex = 349;
            this.tabHopDong.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tpNhomSanPham,
            this.tpNguyenPhuLieu,
            this.tpSanPham,
            this.tpThietBi,
            this.tpHangMau});
            this.tabHopDong.TabsStateStyles.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.tabHopDong.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // tpNhomSanPham
            // 
            this.tpNhomSanPham.AutoScroll = true;
            this.tpNhomSanPham.Controls.Add(this.dgNhomSanPham);
            this.tpNhomSanPham.Location = new System.Drawing.Point(1, 21);
            this.tpNhomSanPham.Name = "tpNhomSanPham";
            this.tpNhomSanPham.Size = new System.Drawing.Size(977, 382);
            this.tpNhomSanPham.TabStop = true;
            this.tpNhomSanPham.Text = "Loại Sản phẩm gia công";
            // 
            // dgNhomSanPham
            // 
            this.dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNhomSanPham.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNhomSanPham.AlternatingColors = true;
            this.dgNhomSanPham.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNhomSanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNhomSanPham.ColumnAutoResize = true;
            dgNhomSanPham_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgNhomSanPham_DesignTimeLayout_Reference_0.Instance")));
            dgNhomSanPham_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgNhomSanPham_DesignTimeLayout_Reference_0});
            dgNhomSanPham_DesignTimeLayout.LayoutString = resources.GetString("dgNhomSanPham_DesignTimeLayout.LayoutString");
            this.dgNhomSanPham.DesignTimeLayout = dgNhomSanPham_DesignTimeLayout;
            this.dgNhomSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNhomSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNhomSanPham.GroupByBoxVisible = false;
            this.dgNhomSanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNhomSanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNhomSanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNhomSanPham.Location = new System.Drawing.Point(0, 0);
            this.dgNhomSanPham.Name = "dgNhomSanPham";
            this.dgNhomSanPham.RecordNavigator = true;
            this.dgNhomSanPham.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNhomSanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNhomSanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNhomSanPham.Size = new System.Drawing.Size(977, 382);
            this.dgNhomSanPham.TabIndex = 0;
            this.dgNhomSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNhomSanPham.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgNhomSanPham_LoadingRow);
            // 
            // tpNguyenPhuLieu
            // 
            this.tpNguyenPhuLieu.AutoScroll = true;
            this.tpNguyenPhuLieu.Controls.Add(this.dgNguyenPhuLieu);
            this.tpNguyenPhuLieu.Location = new System.Drawing.Point(1, 21);
            this.tpNguyenPhuLieu.Name = "tpNguyenPhuLieu";
            this.tpNguyenPhuLieu.Size = new System.Drawing.Size(977, 382);
            this.tpNguyenPhuLieu.TabStop = true;
            this.tpNguyenPhuLieu.Text = "Nguyên phụ liệu";
            // 
            // dgNguyenPhuLieu
            // 
            this.dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNguyenPhuLieu.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNguyenPhuLieu.AlternatingColors = true;
            this.dgNguyenPhuLieu.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNguyenPhuLieu.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNguyenPhuLieu.ColumnAutoResize = true;
            dgNguyenPhuLieu_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgNguyenPhuLieu_DesignTimeLayout_Reference_0.Instance")));
            dgNguyenPhuLieu_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgNguyenPhuLieu_DesignTimeLayout_Reference_0});
            dgNguyenPhuLieu_DesignTimeLayout.LayoutString = resources.GetString("dgNguyenPhuLieu_DesignTimeLayout.LayoutString");
            this.dgNguyenPhuLieu.DesignTimeLayout = dgNguyenPhuLieu_DesignTimeLayout;
            this.dgNguyenPhuLieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNguyenPhuLieu.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNguyenPhuLieu.GroupByBoxVisible = false;
            this.dgNguyenPhuLieu.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNguyenPhuLieu.Location = new System.Drawing.Point(0, 0);
            this.dgNguyenPhuLieu.Name = "dgNguyenPhuLieu";
            this.dgNguyenPhuLieu.RecordNavigator = true;
            this.dgNguyenPhuLieu.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNguyenPhuLieu.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNguyenPhuLieu.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNguyenPhuLieu.Size = new System.Drawing.Size(977, 382);
            this.dgNguyenPhuLieu.TabIndex = 1;
            this.dgNguyenPhuLieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNguyenPhuLieu.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgNguyenPhuLieu_LoadingRow);
            // 
            // tpSanPham
            // 
            this.tpSanPham.AutoScroll = true;
            this.tpSanPham.Controls.Add(this.dgSanPham);
            this.tpSanPham.Location = new System.Drawing.Point(1, 21);
            this.tpSanPham.Name = "tpSanPham";
            this.tpSanPham.Size = new System.Drawing.Size(977, 382);
            this.tpSanPham.TabStop = true;
            this.tpSanPham.Text = "Sản phẩm";
            // 
            // dgSanPham
            // 
            this.dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgSanPham.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgSanPham.AlternatingColors = true;
            this.dgSanPham.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgSanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgSanPham.ColumnAutoResize = true;
            dgSanPham_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgSanPham_DesignTimeLayout_Reference_0.Instance")));
            dgSanPham_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgSanPham_DesignTimeLayout_Reference_0});
            dgSanPham_DesignTimeLayout.LayoutString = resources.GetString("dgSanPham_DesignTimeLayout.LayoutString");
            this.dgSanPham.DesignTimeLayout = dgSanPham_DesignTimeLayout;
            this.dgSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgSanPham.GroupByBoxVisible = false;
            this.dgSanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgSanPham.Location = new System.Drawing.Point(0, 0);
            this.dgSanPham.Name = "dgSanPham";
            this.dgSanPham.RecordNavigator = true;
            this.dgSanPham.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgSanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgSanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgSanPham.Size = new System.Drawing.Size(977, 382);
            this.dgSanPham.TabIndex = 1;
            this.dgSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgSanPham.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgSanPham_LoadingRow);
            // 
            // tpThietBi
            // 
            this.tpThietBi.AutoScroll = true;
            this.tpThietBi.Controls.Add(this.dgThietBi);
            this.tpThietBi.Location = new System.Drawing.Point(1, 21);
            this.tpThietBi.Name = "tpThietBi";
            this.tpThietBi.Size = new System.Drawing.Size(977, 382);
            this.tpThietBi.TabStop = true;
            this.tpThietBi.Text = "Thiết bị";
            // 
            // dgThietBi
            // 
            this.dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThietBi.AlternatingColors = true;
            this.dgThietBi.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThietBi.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThietBi.ColumnAutoResize = true;
            dgThietBi_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgThietBi_DesignTimeLayout_Reference_0.Instance")));
            dgThietBi_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgThietBi_DesignTimeLayout_Reference_0});
            dgThietBi_DesignTimeLayout.LayoutString = resources.GetString("dgThietBi_DesignTimeLayout.LayoutString");
            this.dgThietBi.DesignTimeLayout = dgThietBi_DesignTimeLayout;
            this.dgThietBi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThietBi.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThietBi.GroupByBoxVisible = false;
            this.dgThietBi.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThietBi.Location = new System.Drawing.Point(0, 0);
            this.dgThietBi.Name = "dgThietBi";
            this.dgThietBi.RecordNavigator = true;
            this.dgThietBi.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgThietBi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgThietBi.Size = new System.Drawing.Size(977, 382);
            this.dgThietBi.TabIndex = 5;
            this.dgThietBi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThietBi.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgThietBi_LoadingRow);
            // 
            // tpHangMau
            // 
            this.tpHangMau.AutoScroll = true;
            this.tpHangMau.Controls.Add(this.dgHangMau);
            this.tpHangMau.Location = new System.Drawing.Point(1, 21);
            this.tpHangMau.Name = "tpHangMau";
            this.tpHangMau.Size = new System.Drawing.Size(977, 382);
            this.tpHangMau.TabStop = true;
            this.tpHangMau.Text = "Hàng mẫu";
            // 
            // dgHangMau
            // 
            this.dgHangMau.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgHangMau.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgHangMau.AlternatingColors = true;
            this.dgHangMau.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgHangMau.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgHangMau.ColumnAutoResize = true;
            dgHangMau_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgHangMau_DesignTimeLayout_Reference_0.Instance")));
            dgHangMau_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgHangMau_DesignTimeLayout_Reference_0});
            dgHangMau_DesignTimeLayout.LayoutString = resources.GetString("dgHangMau_DesignTimeLayout.LayoutString");
            this.dgHangMau.DesignTimeLayout = dgHangMau_DesignTimeLayout;
            this.dgHangMau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgHangMau.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgHangMau.GroupByBoxVisible = false;
            this.dgHangMau.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHangMau.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHangMau.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgHangMau.Hierarchical = true;
            this.dgHangMau.Location = new System.Drawing.Point(0, 0);
            this.dgHangMau.Name = "dgHangMau";
            this.dgHangMau.RecordNavigator = true;
            this.dgHangMau.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgHangMau.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgHangMau.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgHangMau.Size = new System.Drawing.Size(977, 382);
            this.dgHangMau.TabIndex = 13;
            this.dgHangMau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgHangMau.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgHangMau_LoadingRow);
            // 
            // HistoryDetaiHopDongForm
            // 
            this.ClientSize = new System.Drawing.Size(979, 495);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HistoryDetaiHopDongForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chi tiết lịch sử";
            this.Load += new System.EventHandler(this.HistoryDetaiHopDongForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).EndInit();
            this.tabHopDong.ResumeLayout(false);
            this.tpNhomSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNhomSanPham)).EndInit();
            this.tpNguyenPhuLieu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).EndInit();
            this.tpSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).EndInit();
            this.tpThietBi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).EndInit();
            this.tpHangMau.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgHangMau)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.UI.Tab.UITab tabHopDong;
        private Janus.Windows.UI.Tab.UITabPage tpNhomSanPham;
        private Janus.Windows.GridEX.GridEX dgNhomSanPham;
        private Janus.Windows.UI.Tab.UITabPage tpNguyenPhuLieu;
        private Janus.Windows.GridEX.GridEX dgNguyenPhuLieu;
        private Janus.Windows.UI.Tab.UITabPage tpSanPham;
        private Janus.Windows.GridEX.GridEX dgSanPham;
        private Janus.Windows.UI.Tab.UITabPage tpThietBi;
        private Janus.Windows.GridEX.GridEX dgThietBi;
        private Janus.Windows.UI.Tab.UITabPage tpHangMau;
        private Janus.Windows.GridEX.GridEX dgHangMau;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangHoa;
    }
}
