﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CA_QuanLyGCN
{
    public partial class DanhMucNganhNgheFrm : Form
    {
        public DanhMucNganhNgheFrm()
        {
            InitializeComponent();
        }
        private NganhNghe nganhNghe;
        private void uiButton1_Click(object sender, EventArgs e)
        {

        }
        public void SetForm()
        {
            if(nganhNghe.Id > 0)
            {
                txtMaNganhNghe.Text = nganhNghe.MaNganhNghe;
                txtGhichu.Text = nganhNghe.GhiChu;
                txtTenNganhNghe.Text = nganhNghe.TenNganhNghe;
            }
        }
        public void GetForm()
        {
            nganhNghe.MaNganhNghe = txtMaNganhNghe.Text.Trim();
            nganhNghe.TenNganhNghe = txtTenNganhNghe.Text.Trim();
            nganhNghe.GhiChu = txtGhichu.Text.Trim();

        }
        private void bindingData()
        {
            try
            {
                gridEX1.DataSource = NganhNghe.ListAllDataSet().Tables[0];
                gridEX1.Refetch();

            }
            catch (System.Exception ex)
            {
                gridEX1.Refetch();
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }


        }

        private void DanhMucNganhNgheFrm_Load(object sender, EventArgs e)
        {
            bindingData();
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (nganhNghe == null) nganhNghe = new NganhNghe();
                GetForm();
                ErrorProvider error = new ErrorProvider();
                if(string.IsNullOrEmpty(nganhNghe.TenNganhNghe))
                {
                    error.SetError(txtTenNganhNghe, "Không được để trống");
                    return;
                }
                if (nganhNghe.Id > 0)
                    nganhNghe.Update();
                else
                    nganhNghe.Insert();
                Reset();
                bindingData();
                MessageBox.Show("Lưu thành công");
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Reset()
        {
            txtMaNganhNghe.Text = string.Empty;
            txtGhichu.Text = string.Empty;
            txtTenNganhNghe.Text = string.Empty;
            nganhNghe = new NganhNghe();
        }
        private void gridEX1_RecordsDeleted(object sender, EventArgs e)
        {
           

        }

        private void gridEX1_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa dòng danh mục này", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Janus.Windows.GridEX.GridEXSelectedItemCollection list = gridEX1.SelectedItems;
                if (list.Count > 0)
                {
                    foreach (Janus.Windows.GridEX.GridEXSelectedItem item in list)
                    {
                        if (item.RowType == Janus.Windows.GridEX.RowType.Record)
                        {
                            //DataTable dt = gridEX1.DataSource as DataTable;
                            DataRowView dr = (DataRowView)item.GetRow().DataRow;
                            Int32 id = System.Convert.ToInt32(dr["ID"]);
                            NganhNghe nn = new NganhNghe();
                            nn.Id = id;
                            nn.Delete();
                        }
                    }
                    bindingData();
                }
            }
        }

        private void gridEX1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
            {
                DataRowView dr = (DataRowView)e.Row.DataRow;
                if (nganhNghe == null) nganhNghe = new NganhNghe();
                nganhNghe.GhiChu = dr["GhiChu"].ToString();
                nganhNghe.Id = Convert.ToInt32(dr["ID"]);
                nganhNghe.TenNganhNghe = dr["TenNganhNghe"].ToString();
                nganhNghe.MaNganhNghe = dr["MaNganhNghe"].ToString();
                SetForm();
            }
        }

    }
}
