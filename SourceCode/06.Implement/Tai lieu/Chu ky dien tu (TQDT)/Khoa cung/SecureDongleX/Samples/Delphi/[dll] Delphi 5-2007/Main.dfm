object frmMain: TfrmMain
  Left = 206
  Top = 116
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'SecureDongle X Delphi Sample'
  ClientHeight = 248
  ClientWidth = 577
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FromClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lstOutput: TListBox
    Left = 13
    Top = 18
    Width = 444
    Height = 215
    ExtendedSelect = False
    ItemHeight = 13
    TabOrder = 0
  end
  object btnWrite: TButton
    Left = 472
    Top = 16
    Width = 91
    Height = 25
    Caption = 'Write'
    TabOrder = 1
    OnClick = btnWriteClick
  end
  object btnRead: TButton
    Left = 472
    Top = 48
    Width = 91
    Height = 25
    Caption = 'Read'
    TabOrder = 2
    OnClick = btnReadClick
  end
  object btnTransform: TButton
    Left = 472
    Top = 80
    Width = 89
    Height = 25
    Caption = 'Transform'
    TabOrder = 3
    OnClick = btnTransformClick
  end
  object btnEncrypt: TButton
    Left = 472
    Top = 112
    Width = 89
    Height = 25
    Caption = 'Encrypt'
    TabOrder = 4
    OnClick = btnEncryptClick
  end
  object btnDecrypt: TButton
    Left = 472
    Top = 144
    Width = 89
    Height = 25
    Caption = 'Decrypt'
    TabOrder = 5
    OnClick = btnDecryptClick
  end
  object btnWriteMap: TButton
    Left = 472
    Top = 176
    Width = 89
    Height = 25
    Caption = 'Write with Map'
    TabOrder = 6
    OnClick = btnWriteMapClick
  end
  object btnReadMap: TButton
    Left = 472
    Top = 208
    Width = 89
    Height = 25
    Caption = 'Read with Map'
    TabOrder = 7
    OnClick = btnReadMapClick
  end
end
