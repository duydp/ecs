          
----------------------------------------------------------------------------------------------------            
            
Alter PROCEDURE [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]            
    
 @MaDoanhNghiep NVarchar(14),          
 @MaHaiQuan varchar (6),            
 @LanThanhLy int ,           
@BangKeHoSoThanhLy_ID  int   
as   
 --set   @MaDoanhNghiep='0400604366'  
 --set @MaHaiQuan='C34C'  
 --set @LanThanhLy =16  
 --set @BangKeHoSoThanhLy_ID = 304  
         
Create table #BangKeNPLTon           
      ( MaNPL nvarchar(80),          
    TenNPL nvarchar(255),          
    LuongTonDau numeric(24,8),          
    LuongNhap numeric(24,8),          
   LuongNPLSuDung numeric(24,8),          
   LuongNPLTaiXuat numeric(24,8),          
   LuongNopThue numeric(24,8),       
      LuongTieuHuy numeric(24,8),       
   LuongXuatTheoHD numeric(24,8),          
   LuongTonCuoi numeric(24,8),          
   DVT_ID varchar(10)          
   )          
          
insert into #BangKeNPLTon (MaNPL ,          
    TenNPL ,          
    LuongTonDau ,          
    LuongNhap ,          
   LuongNPLSuDung ,          
   LuongNPLTaiXuat ,          
   LuongNopThue,    
   LuongTieuHuy,        
   LuongXuatTheoHD ,          
   LuongTonCuoi,          
   DVT_ID )          
          
Select             
 A.MaNPL,            
 MAX(B.Ten) as TenNPL,             
 SUM(A.LuongTonDau)  as LuongTonDau,            
 Sum(LuongNhap) as LuongNhap,            
 Sum(LuongNPLSuDung) as LuongNPLSuDung,            
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,            
 Sum(LuongNopThue) as LuongNopThue,            
 Sum (LuongTieuHuy) as LuongTieuHuy,  
 Sum(LuongXuatTheoHD) as LuongXuatTheoHD,            
 Sum(LuongTonCuoi) as LuongTonCuoi,            
 B.DVT_ID --MAX(ID_DVT_NPL) as ID_DVT_NPL,            
          
 From             
(          
 SELECT            
    SoToKhaiNhap,            
    NgayDangKyNhap,            
    MaNPL,            
    TenNPL,     
    --Tinh luong ton dau gom (to khai tk do dang và to khai da dưa vao bang ke o những lan thanh ly truoc)            
    CASE when     
    (select COUNT(*) from t_KDT_SXXK_BKToKhaiNhap where BangKeHoSoThanhLy_ID <@BangKeHoSoThanhLy_ID and SoToKhai=SoToKhaiNhap and MaLoaiHinh= MaLoaiHinhNhap  and YEAR(NgayDangKy)= YEAR(NgayDangKyNhap))>0     
    then LuongTonDau     
    else 0 end as LuongTonDau,            
    -- tinh luong nhap(neu to khai da có trong bang ke truoc do thi tinh luong nhap = 0)    
    CASE when      
    (select COUNT(*) from t_KDT_SXXK_BKToKhaiNhap where BangKeHoSoThanhLy_ID <@BangKeHoSoThanhLy_ID and SoToKhai=SoToKhaiNhap and MaLoaiHinh= MaLoaiHinhNhap and YEAR(NgayDangKy)= YEAR(NgayDangKyNhap) )>0     
    then 0     
    else (CASE when     
       LuongNhap = LuongTonDau      
      then (Case when  MaLoaiHinhNhap not like '%V%' then 0 else LuongNhap end)     
      else 0     
      end)     
    end as LuongNhap,             
    --Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung,             
    Sum( Case when LuongTonCuoi<0 then (case when LuongNPLSuDung + LuongTonCuoi >LuongTonDau then LuongTonDau else  LuongNPLSuDung + LuongTonCuoi end) else (Case when      
      ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau       
      then LuongTonDau- LuongTonCuoi      
      else LuongNPLSuDung end) end      
    )  as LuongNPLSuDung,       
    Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,             
    0 as LuongNopThue,       
    0 as LuongTieuHuy,       
    0 as LuongXuatTheoHD,            
    Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,            
    (select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL            
             
                 
   FROM            
    [dbo].[t_KDT_SXXK_BCXuatNhapTon]            
   WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy       
   GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTondau,LuongNhap            
   ) as A inner join t_SXXK_NguyenPhuLieu B            
on A.MaNPL = B.Ma  AND B.MaDoanhNghiep = @MaDoanhNghiep And B.MaHaiQuan= @MaHaiQuan      
--inner join    t_KDT_SXXK_BKToKhaiNhap as D on a.SoToKhaiNhap= D.SoToKhai and  D.BangKeHoSoThanhLy_ID =302    
    
Group by A.MaNPL,B.DVT_ID     
order by A.MaNPL            
          
 if(exists( select * from t_KDT_SXXK_BangKeHoSoThanhLy where MaterID = (select top 1 ID from  t_KDT_SXXK_HoSoThanhLyDangKy where LanThanhLy = @LanThanhLy AND MaBangKe = 'DTLNPLCHUATL')))          
 begin           
  update #BangKeNPLTon set LuongTonCuoi = LuongTonCuoi + b.Luong --, LuongNPLSuDung = LuongNPLSuDung- b.Luong          
  from #BangKeNPLTon a inner join (select sum(Luong)as Luong,MaNPL,DVT_ID  from t_KDT_SXXK_BKNPLChuaThanhLy where BangKeHoSoThanhLy_ID = (select top 1 ID from t_KDT_SXXK_BangKeHoSoThanhLy where MaterID = (select top 1 ID from  t_KDT_SXXK_HoSoThanhLyDangKy
    
 where LanThanhLy = @LanThanhLY AND MaBangKe = 'DTLNPLCHUATL')) group by MaNPL,DVT_ID) b          
  on a.MaNPL = b.MaNPL and a.DVT_ID = b.DVT_ID          
 end    
   
if(exists( select * from t_KDT_CX_NPLXinHuy where Master_id = (select top 1 id from t_KDT_CX_NPLXinHuyDangKy)))  
begin   
 update #BangKeNPLTon set LuongTonCuoi= e.LuongTonCuoi- d.LuongTieuHuy, LuongTieuHuy=e.LuongTieuHuy+d.LuongTieuHuy  
 from #BangKeNPLTon e   
 Inner Join (select sum(LuongTieuHuy)as LuongTieuHuy,MaHang,DVT from t_KDT_CX_NPLXinHuy where Master_id = (select top 1 id from t_KDT_CX_NPLXinHuyDangKy where LanThanhLy= @LanThanhLy) group by MaHang,DVT) d on d.MaHang=e.MaNPL  
end    
        
          
select LuongNPLTaiXuat+LuongTieuHuy as LuongTaiXuatVaTieuHuy,* from #BangKeNPLTon          
drop table #BangKeNPLTon          

Go

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKNPLXinHuy_SelectBy_BangKeHoSoThanhLy_ID]    Script Date: 01/05/2015 10:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLXinHuy_SelectBy_BangKeHoSoThanhLy_ID]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 17, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_BKNPLXinHuy_SelectBy_BangKeHoSoThanhLy_ID]
	@BangKeHoSoThanhLy_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BangKeHoSoThanhLy_ID],
	[SoToKhai],
	CASE WHEN MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=SoToKhai)ELSE SoToKhai END AS SoToKhaiVNACCS,
	[MaLoaiHinh],
	[NamDangKy],
	a.MaHaiQuan,
	[NgayDangKy],
	[MaNPL],
	[TenNPL],
	[BienBanHuy],
	[NgayHuy],
	[LuongHuy],
	a.DVT_ID,
	[TenDVT],
	[STTHang],
	b.MaHS
FROM
	[dbo].[t_KDT_SXXK_BKNPLXinHuy] as a
	inner join t_SXXK_NguyenPhuLieu as b on a.MaNPL = b.Ma and a.MaHaiQuan= b.MaHaiQuan
	and a.BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID


Go

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.2',GETDATE(), N' Cap nhat báo cáo NPL cho Che xuat')
END  
  