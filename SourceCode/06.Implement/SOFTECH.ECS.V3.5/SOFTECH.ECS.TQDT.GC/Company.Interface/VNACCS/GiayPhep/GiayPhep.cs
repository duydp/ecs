﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.Interface.Report;
using System.IO;
//using Company.BLL;
#if SXXK_V4
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;
#endif
#if GC_V4
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.SXXK;
#endif
using DuLieuChuan = Company.KDT.SHARE.Components.DuLieuChuan;
//using Company.BLL.KDT.SXXK;
using System.Net.Mail;
using Company.Interface.DanhMucChuan;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
using System.Xml;
using System.Globalization;
using Infragistics.Excel;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.KDT.SXXK;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.VNACC.PhieuKho;
//using Company.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.PhieuKho;
using System.Data;


namespace Company.Interface.VNACCS.GiayPhep
{
    public partial class GiayPhep : BaseForm
    {

        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        private static string sfmtYear = "yyyy";

        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        KDT_VNACC_ToKhaiMauDich TKMD;
        //List
        List<KDT_VNACC_ToKhaiMauDich> listTKMD = new List<KDT_VNACC_ToKhaiMauDich>();
        KDT_KhoCFS_DangKy khoCFS = new KDT_KhoCFS_DangKy();
        List<KDT_KhoCFS_Detail> listTK = new List<KDT_KhoCFS_Detail>();
        
        public long TKMD_ID = 0;
        public long khoCFS_ID = 0;
        public GiayPhep()
        {
            InitializeComponent();
            
        }

        private DataTable makeTB() 
        {
            DataTable tb = new DataTable("KHO");
            DataColumn[] col1 = new DataColumn[3];
            col1[0] = new DataColumn("TKMD_ID", Type.GetType("System.String"));
            col1[1] = new DataColumn("SoToKhai", Type.GetType("System.String"));
            col1[2] = new DataColumn("MaLoaiHinh", Type.GetType("System.String"));
            tb.Columns.AddRange(col1);
            return tb;

        }

        private void loadTK(long id) 
        {
            listTK = KDT_KhoCFS_Detail.SelectCollectionAll().FindAll(delegate(KDT_KhoCFS_Detail tk) 
            {
                return tk.Master_ID == id;
            });
            
        }
        
        private void PhieuKho_Load(object sender, EventArgs e)
        {
            txtMaHQ.ReadOnly = false;
            if (khoCFS_ID != 0)
            {
                khoCFS = KDT_KhoCFS_DangKy.LoadFull(khoCFS_ID);
                ReLoad();
                if (khoCFS.TrangThaiXuLy == 1) 
                {
                    lblTrangThai.Text = "Đã duyệt";
                    txtMaLoaiHinh.Text = khoCFS.NgayTiepNhan.ToString();
                }
                else if (khoCFS.TrangThaiXuLy == -1) 
                {
                    lblTrangThai.Text = "Không phê duyệt";
                }else
                    lblTrangThai.Text = "Chưa khai báo";
            }
            else 
            {
                    cbxMaKhoCFS.SelectedIndex = 0;
                lblTrangThai.Text = "Chưa khai báo";
            }
            
        }

        
        private void cmbToolBar_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key) 
            {
                // Khai báo PK.
                case "cmdSend":
                    this.SendV5();
                    break;
                // Khai báo PK.
                case "cmdSave":
                    this.Save(true);
                    break;
                case "NhanDuLieu":
                    this.FeedBackV5();
                    break;
                case "XacNhan":
                    this.KetQuaXuLy();
                    break;
            }
        }

        private void KetQuaXuLy()
        {
            ThongDiepForm form = new ThongDiepForm();
            //form.ItemID = this.phieukhoDK.ID;
            form.ItemID = this.khoCFS.ID;
            form.KhoCFS = true;
            form.DeclarationIssuer = DeclarationIssuer.TTHangGuiKho;
            form.ShowDialog(this);
        
        }

        
        private void Save(bool saveList)
        {

            try
            {
                
                khoCFS.MaKhoCFS = cbxMaKhoCFS.SelectedItem.ToString();
                khoCFS.MaHQ = cbxMaKhoCFS.SelectedItem.ToString().Substring(0,4);
                khoCFS.InsertUpdateFull();
                khoCFS_ID = khoCFS.ID;
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SendV5()
        {
            if (ShowMessage("Bạn có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {

                //if (phieukhoDK.ID == 0)
                if(khoCFS_ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    //phieukhoDK = KDT_PhieuKhoDangKy.Load(phieukhoDK.ID);
                    
                    khoCFS = KDT_KhoCFS_DangKy.LoadFull(khoCFS.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.PhieuNhapKho;
                //sendXML.master_id = phieukhoDK.ID;
                sendXML.master_id = khoCFS.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    //if (this.phieukhoDK.ID!=0)
                    if (this.khoCFS.ID != 0)
                    {

                        string returnMessage = string.Empty;
                        //phieukhoDK.GuidStr = Guid.NewGuid().ToString();
                        khoCFS.GuidStr = Guid.NewGuid().ToString();
                        PhieuNhapKho pnk = new PhieuNhapKho();
                        //pnk = Mapper_V4.ToDaTaTransferPhieuNhapKho(TKMD,phieukhoDK);
                        pnk = ToDaTaTransferPhieuKhoCFS(khoCFS, GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = new ObjectSend(
                            new MessageHeader()
                            {
                                ProcedureType = "2",
                                Reference = new Reference()
                                {
                                    Version = "3.00",
                                    MessageID = khoCFS.GuidStr,
                                },
                                SendApplication = new SendApplication()
                                {
                                    Name = "Softech_ECS",
                                    Version = "5.00",
                                    CompanyName = "Công ty cổ phần SOFTECH Đà Nẵng",
                                    CompanyIdentity = "0401430088",
                                    CreateMessageIssue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                },
                                From = new NameBase()
                                {
                                    Name = GlobalSettings.TEN_DON_VI,
                                    Identity = GlobalSettings.MA_DON_VI,
                                },
                                To = new NameBase()
                                {
                                    //Name = TKMD.TenCoQuanHaiQuan,
                                    //Identity = TKMD.CoQuanHaiQuan,
                                    Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0,4))),
                                    Identity = khoCFS.MaKhoCFS.Substring(0,4)
                                },
                                Subject = new Subject()
                                {
                                    Type = DeclarationIssuer.TTHangGuiKho,
                                    Function = DeclarationFunction.KHAIBAO_HANGGUIKHO,
                                    Reference = khoCFS.GuidStr,
                                    SendApplication = "SOFTECH_ECS",
                                    ReceiveApplication = "CFS_Service",
                                }
                            },
                            new NameBase()
                            {
                                Name = GlobalSettings.TEN_DON_VI,
                                Identity = GlobalSettings.MA_DON_VI,
                            },
                            new NameBase()
                            {
                                    //Name = TKMD.TenCoQuanHaiQuan,
                                    //Identity = TKMD.CoQuanHaiQuan,
                                Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0, 4))),
                                Identity = khoCFS.MaKhoCFS.Substring(0, 4)
                            },
                            new Subject()
                            {
                                    Type = DeclarationIssuer.TTHangGuiKho,
                                    Function = DeclarationFunction.KHAIBAO_HANGGUIKHO,
                                    //Reference = phieukhoDK.GuidStr,
                                    Reference = khoCFS.GuidStr,
                                    SendApplication = "SOFTECH_ECS",
                                    ReceiveApplication = "CFS_Service",
                            },
                            pnk
                            );
                        //phieukhoDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        khoCFS.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend, 1);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            //sendForm.Message.XmlSaveMessage(phieukhoDK.ID, MessageTitle.PhieuNhapKho);
                            sendForm.Message.XmlSaveMessage(khoCFS.ID, MessageTitle.PhieuNhapKho);
                            NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            
                            sendXML.LoaiHS = LoaiKhaiBao.PhieuNhapKho;
                            
                            sendXML.master_id = khoCFS.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            //phieukhoDK.Update();
                            khoCFS.Update();
                            
                            //FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                    PhieuKho_Load(null,null);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            //feedbackContent = SingleMessage.PhieuKhoSendHandler(phieukhoDK, ref msgInfor, e);
            feedbackContent = SingleMessage.PhieuKhoSendHandler(khoCFS, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                //string reference = phieukhoDK.GuidStr;
                string reference = khoCFS.GuidStr;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.LayTTHangGuiKho,
                    Reference = reference,
                    Function = DeclarationFunction.LAY_TT_HANGGUIKHO,
                    Type = DeclarationIssuer.LayTTHangGuiKho,

                };
                
                //subjectBase.Type = DeclarationIssuer.PhieuNhapKho;
                

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = GlobalSettings.MA_DON_VI
                                            },
                                              new NameBase()
                                              {
                                                  //Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(phieukhoDK.MaHQ.Trim())),
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0,4))),
                                                  //Identity = phieukhoDK.MaHQ
                                                  Identity = khoCFS.MaKhoCFS.Substring(0, 4)
                                              }, subjectBase, null);
                //if (phieukhoDK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                if (khoCFS.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    //msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    //msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0, 4)));
                    msgSend.To.Identity = khoCFS.MaKhoCFS.Substring(0, 4);
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend,1);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }

            }
            PhieuKho_Load(null,null);
        }
        private bool CheckTK(long tkmd_id, KDT_KhoCFS_DangKy khoCFS) 
        {
            foreach (var item in khoCFS.ListTK)
            {
                if (tkmd_id == item.TKMD_ID)
                    return true;
            }
            return false;
        }
        private void btnThemToKhai_Click(object sender, EventArgs e)
        {

            if (cbxMaKhoCFS.SelectedItem != null)
            {
                VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
                f.IsShowChonToKhai = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    TKMD = f.TKMDDuocChon;
                    KDT_KhoCFS_Detail tk = new KDT_KhoCFS_Detail();
                    if (!CheckTK(TKMD.ID, khoCFS)) 
                    {
                        tk.TKMD_ID = TKMD.ID;
                        tk.SoToKhai = TKMD.SoToKhai;
                        tk.NgayDangKy = TKMD.NgayDangKy;
                        tk.MaLoaiHinh = TKMD.MaLoaiHinh;
                        listTKMD.Add(TKMD);
                        khoCFS.ListTK.Add(tk);
                    }
                }
                ReLoad();
            }
            else 
            {
                MessageBox.Show("Chọn mã kho", "Thông Báo", MessageBoxButtons.OK);
            }
            
        }

        private void ReLoad() 
        {
            if (khoCFS.ID != 0) 
            {
                cbxMaKhoCFS.SelectedItem = khoCFS.MaKhoCFS;
                txtMaHQ.Text = khoCFS.MaKhoCFS.Substring(0, 4);
            }
            try
            {
                DataTable dtb = this.makeTB();
                for (int i = 0; i < khoCFS.ListTK.Count; i++)
                {
                    DataRow dr = dtb.NewRow();
                    dr["TKMD_ID"] = khoCFS.ListTK[i].TKMD_ID;
                    dr["SoToKhai"] = khoCFS.ListTK[i].SoToKhai;
                    dr["MaLoaiHinh"] = khoCFS.ListTK[i].MaLoaiHinh;
                    dtb.Rows.Add(dr);
                }

                
                grList.DataSource = dtb;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                //throw;
            }
            
        }

        private void grList_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void grList_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void cbxMaKhoCFS_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtMaHQ.Text = cbxMaKhoCFS.SelectedItem.ToString().Substring(0, 4);
        }

        public static PhieuNhapKho ToDaTaTransferPhieuKhoCFS(KDT_KhoCFS_DangKy khoCFS, string maDV, string tenVD)
        {

            PhieuNhapKho pnk = new PhieuNhapKho()
            {
                Issuer = DeclarationIssuer.TTHangGuiKho,
                Function = DeclarationFunction.KHAIBAO_HANGGUIKHO,
                Reference = khoCFS.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = khoCFS.MaHQ,
                Status = "1",
                CustomsReference = "",
                Acceptance = DateTime.Now.ToString(sfmtDate),
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Name = tenVD,
                    Identity = maDV
                },
                ReferencePNK = new ReferencePNK()
                {
                    Indentity = maDV,
                    DeclarationOffice = khoCFS.MaKhoCFS.Substring(0, 4),
                    Warehouse = khoCFS.MaKhoCFS,
                    //Test mã kho
                    //Warehouse = "43ND001",
                    DeclarationList = new List<DeclarationList>()
                }
            };
            foreach (KDT_KhoCFS_Detail tk in khoCFS.ListTK)
            {
                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(tk.TKMD_ID);
                KDT_VNACC_TK_PhanHoi_TyGia tygia = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionAll().Find(delegate(KDT_VNACC_TK_PhanHoi_TyGia t)
                {
                    return t.Master_ID == TKMD.ID;
                });
                KDT_VNACC_HangMauDich hmd = KDT_VNACC_HangMauDich.SelectCollectionAll().Find(delegate(KDT_VNACC_HangMauDich h)
                {
                    return h.TKMD_ID == TKMD.ID;
                });
                pnk.ReferencePNK.DeclarationList.Add(new DeclarationList()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Acceptance = TKMD.NgayDangKy.ToString(sfmtDate),
                    //Acceptance = DateTime.Now.ToString(sfmtDate),
                    DeclarationOffice = TKMD.CoQuanHaiQuan,
                    DeclarationHeadOffice = "",
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    Identification = hmd.MaSoHang.Substring(0, 4),
                    Identificationname = hmd.TenHang,
                    PackagingQuanlity = float.Parse(TKMD.SoLuong.ToString()).ToString(),
                    PackagingUnit = TKMD.MaDVTSoLuong,
                    TotalGrossMass = TKMD.TrongLuong.ToString(),
                    TotalGrossMassUnit = TKMD.MaDVTTrongLuong,
                    TotalcustomsValue = TKMD.TongTriGiaHD.ToString(),
                    //TotalcustomsValue = "8.08",
                    CurrencyType = TKMD.MaTTHoaDon,
                    RateVND = tygia.TyGiaTinhThue.ToString(),
                    RateUSD = "",
                    QueueDestination = TKMD.MaDiaDiemXepHang,
                    DeliveryDestination = khoCFS.MaKhoCFS,
                    DeliveryDestinationDate = TKMD.NgayDen.ToString(sfmtDate),
                    //DeliveryDestinationDate = DateTime.Now.ToString(sfmtDate),
                    PluongInformation = TKMD.MaPhanLoaiKiemTra,
                });
            }

            pnk.Agents.Add(new Agent()
            {
                Name = tenVD,
                Identity = maDV,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            return pnk;
        }
     }
}

