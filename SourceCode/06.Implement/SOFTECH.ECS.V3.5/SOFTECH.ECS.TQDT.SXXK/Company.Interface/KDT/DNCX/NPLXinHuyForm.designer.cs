namespace Company.Interface.KDT.SXXK
{
    partial class NPLXinHuyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NPLXinHuyForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.grdList = new Janus.Windows.GridEX.GridEX();
            this.dgList = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDVT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.clcNgayDangKy = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLoaiHinhXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cmdAdd = new Janus.Windows.EditControls.UIButton();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoLuongXuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtGhichu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvToKhaiXuat = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cmdSave = new Janus.Windows.EditControls.UIButton();
            this.cvSoLuong = new Company.Controls.CustomValidation.CompareValidator();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdNhanDulieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanDulieu");
            this.cmdKetQua1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQua");
            this.cmdHuyBK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyBK");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdNhanDulieu = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanDulieu");
            this.cmdKetQua = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQua");
            this.cmdHuyBK = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyBK");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayThongBao = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayTiepNhan = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.lblTrangThaiXuLy = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.dgList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(804, 477);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.grdList);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 132);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(798, 260);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // grdList
            // 
            this.grdList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList.AlternatingColors = true;
            this.grdList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdList.AutomaticSort = false;
            this.grdList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.grdList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.grdList.ColumnAutoResize = true;
            grdList_DesignTimeLayout.LayoutString = resources.GetString("grdList_DesignTimeLayout.LayoutString");
            this.grdList.DesignTimeLayout = grdList_DesignTimeLayout;
            this.grdList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grdList.GroupByBoxVisible = false;
            this.grdList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdList.ImageList = this.ImageList1;
            this.grdList.Location = new System.Drawing.Point(2, 11);
            this.grdList.Name = "grdList";
            this.grdList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.grdList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.grdList.Size = new System.Drawing.Size(795, 247);
            this.grdList.TabIndex = 0;
            this.grdList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grdList.VisualStyleManager = this.vsmMain;
            this.grdList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdList_RowDoubleClick);
            // 
            // dgList
            // 
            this.dgList.BackColor = System.Drawing.Color.Transparent;
            this.dgList.Controls.Add(this.uiGroupBox3);
            this.dgList.Controls.Add(this.uiGroupBox4);
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgList.Location = new System.Drawing.Point(0, 44);
            this.dgList.Name = "dgList";
            this.dgList.Size = new System.Drawing.Size(804, 395);
            this.dgList.TabIndex = 0;
            this.dgList.Text = "Thông tin tờ khai";
            this.dgList.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.ctrDVT);
            this.uiGroupBox4.Controls.Add(this.clcNgayDangKy);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.txtLoaiHinhXuat);
            this.uiGroupBox4.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox4.Controls.Add(this.txtMaNPL);
            this.uiGroupBox4.Controls.Add(this.cmdAdd);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtSoLuongXuat);
            this.uiGroupBox4.Controls.Add(this.txtGhichu);
            this.uiGroupBox4.Controls.Add(this.txtTenNPL);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(798, 115);
            this.uiGroupBox4.TabIndex = 20;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // ctrDVT
            // 
            this.ctrDVT.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDVT.Appearance.Options.UseBackColor = true;
            this.ctrDVT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVT.Code = "";
            this.ctrDVT.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVT.IsOnlyWarning = false;
            this.ctrDVT.IsValidate = true;
            this.ctrDVT.Location = new System.Drawing.Point(372, 59);
            this.ctrDVT.Name = "ctrDVT";
            this.ctrDVT.Name_VN = "";
            this.ctrDVT.SetOnlyWarning = false;
            this.ctrDVT.SetValidate = false;
            this.ctrDVT.ShowColumnCode = true;
            this.ctrDVT.ShowColumnName = true;
            this.ctrDVT.Size = new System.Drawing.Size(100, 21);
            this.ctrDVT.TabIndex = 20;
            this.ctrDVT.TagName = "";
            this.ctrDVT.Where = null;
            this.ctrDVT.WhereCondition = "";
            // 
            // clcNgayDangKy
            // 
            this.clcNgayDangKy.Location = new System.Drawing.Point(567, 6);
            this.clcNgayDangKy.Name = "clcNgayDangKy";
            this.clcNgayDangKy.ReadOnly = false;
            this.clcNgayDangKy.Size = new System.Drawing.Size(130, 21);
            this.clcNgayDangKy.TabIndex = 19;
            this.clcNgayDangKy.TagName = "";
            this.clcNgayDangKy.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDangKy.WhereCondition = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số tờ khai";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Mã hàng";
            // 
            // txtLoaiHinhXuat
            // 
            this.txtLoaiHinhXuat.Location = new System.Drawing.Point(372, 6);
            this.txtLoaiHinhXuat.Name = "txtLoaiHinhXuat";
            this.txtLoaiHinhXuat.ReadOnly = true;
            this.txtLoaiHinhXuat.Size = new System.Drawing.Size(100, 21);
            this.txtLoaiHinhXuat.TabIndex = 3;
            this.txtLoaiHinhXuat.VisualStyleManager = this.vsmMain;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoToKhai.Location = new System.Drawing.Point(99, 6);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.ReadOnly = true;
            this.txtSoToKhai.Size = new System.Drawing.Size(182, 21);
            this.txtSoToKhai.TabIndex = 1;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            this.txtSoToKhai.ButtonClick += new System.EventHandler(this.txtSoToKhai_ButtonClick);
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaNPL.Location = new System.Drawing.Point(99, 33);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.ReadOnly = true;
            this.txtMaNPL.Size = new System.Drawing.Size(182, 21);
            this.txtMaNPL.TabIndex = 7;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            this.txtMaNPL.ButtonClick += new System.EventHandler(this.txtMaNPL_ButtonClick);
            // 
            // cmdAdd
            // 
            this.cmdAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAdd.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAdd.Icon")));
            this.cmdAdd.Location = new System.Drawing.Point(616, 85);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(81, 23);
            this.cmdAdd.TabIndex = 18;
            this.cmdAdd.Text = "Thêm";
            this.cmdAdd.VisualStyleManager = this.vsmMain;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(314, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Loại hình";
            // 
            // txtSoLuongXuat
            // 
            this.txtSoLuongXuat.DecimalDigits = 6;
            this.txtSoLuongXuat.Location = new System.Drawing.Point(99, 59);
            this.txtSoLuongXuat.Name = "txtSoLuongXuat";
            this.txtSoLuongXuat.Size = new System.Drawing.Size(182, 21);
            this.txtSoLuongXuat.TabIndex = 17;
            this.txtSoLuongXuat.Text = "0.000000";
            this.txtSoLuongXuat.Value = new decimal(new int[] {
            0,
            0,
            0,
            393216});
            this.txtSoLuongXuat.VisualStyleManager = this.vsmMain;
            // 
            // txtGhichu
            // 
            this.txtGhichu.Location = new System.Drawing.Point(99, 86);
            this.txtGhichu.Name = "txtGhichu";
            this.txtGhichu.Size = new System.Drawing.Size(499, 21);
            this.txtGhichu.TabIndex = 9;
            this.txtGhichu.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.Location = new System.Drawing.Point(372, 33);
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.ReadOnly = true;
            this.txtTenNPL.Size = new System.Drawing.Size(325, 21);
            this.txtTenNPL.TabIndex = 9;
            this.txtTenNPL.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(314, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "ĐVT";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Ghi chú";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 63);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Lượng tiêu hủy";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(314, 37);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Tên NPL";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(478, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Ngày đăng ký";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this.dgList;
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvToKhaiXuat
            // 
            this.rfvToKhaiXuat.ControlToValidate = this.txtSoToKhai;
            this.rfvToKhaiXuat.ErrorMessage = "\"Số tờ khai \" không được để trống.";
            this.rfvToKhaiXuat.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvToKhaiXuat.Icon")));
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(716, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave.Icon")));
            this.cmdSave.Location = new System.Drawing.Point(594, 9);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(116, 23);
            this.cmdSave.TabIndex = 3;
            this.cmdSave.Text = "Lưu bảng kê";
            this.cmdSave.VisualStyleManager = this.vsmMain;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cvSoLuong
            // 
            this.cvSoLuong.ControlToValidate = this.txtSoLuongXuat;
            this.cvSoLuong.ErrorMessage = "\"Lượng tái xuất\" không hợp lệ.";
            this.cvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuong.Icon")));
            this.cvSoLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuong.ValueToCompare = "0";
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao,
            this.cmdNhanDulieu,
            this.cmdKetQua,
            this.cmdHuyBK});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("bae53847-5f57-45ca-8edd-f925998b729e");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 505);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(804, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao1,
            this.cmdNhanDulieu1,
            this.cmdKetQua1,
            this.cmdHuyBK1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(370, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdNhanDulieu1
            // 
            this.cmdNhanDulieu1.Key = "cmdNhanDulieu";
            this.cmdNhanDulieu1.Name = "cmdNhanDulieu1";
            // 
            // cmdKetQua1
            // 
            this.cmdKetQua1.Key = "cmdKetQua";
            this.cmdKetQua1.Name = "cmdKetQua1";
            // 
            // cmdHuyBK1
            // 
            this.cmdHuyBK1.Key = "cmdHuyBK";
            this.cmdHuyBK1.Name = "cmdHuyBK1";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdNhanDulieu
            // 
            this.cmdNhanDulieu.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhanDulieu.Image")));
            this.cmdNhanDulieu.Key = "cmdNhanDulieu";
            this.cmdNhanDulieu.Name = "cmdNhanDulieu";
            this.cmdNhanDulieu.Text = "Nhận phản hồi";
            // 
            // cmdKetQua
            // 
            this.cmdKetQua.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQua.Image")));
            this.cmdKetQua.Key = "cmdKetQua";
            this.cmdKetQua.Name = "cmdKetQua";
            this.cmdKetQua.Text = "Kết quả xử lý";
            // 
            // cmdHuyBK
            // 
            this.cmdHuyBK.Key = "cmdHuyBK";
            this.cmdHuyBK.Name = "cmdHuyBK";
            this.cmdHuyBK.Text = "Huỷ bảng kê";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 477);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(804, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 477);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(804, 28);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.clcNgayThongBao);
            this.uiGroupBox1.Controls.Add(this.clcNgayTiepNhan);
            this.uiGroupBox1.Controls.Add(this.lblTrangThaiXuLy);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(804, 44);
            this.uiGroupBox1.TabIndex = 5;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayThongBao
            // 
            this.clcNgayThongBao.Location = new System.Drawing.Point(122, 15);
            this.clcNgayThongBao.Name = "clcNgayThongBao";
            this.clcNgayThongBao.ReadOnly = false;
            this.clcNgayThongBao.Size = new System.Drawing.Size(102, 21);
            this.clcNgayThongBao.TabIndex = 19;
            this.clcNgayThongBao.TagName = "";
            this.clcNgayThongBao.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayThongBao.WhereCondition = "";
            // 
            // clcNgayTiepNhan
            // 
            this.clcNgayTiepNhan.Location = new System.Drawing.Point(514, 15);
            this.clcNgayTiepNhan.Name = "clcNgayTiepNhan";
            this.clcNgayTiepNhan.ReadOnly = false;
            this.clcNgayTiepNhan.Size = new System.Drawing.Size(95, 21);
            this.clcNgayTiepNhan.TabIndex = 19;
            this.clcNgayTiepNhan.TagName = "";
            this.clcNgayTiepNhan.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayTiepNhan.WhereCondition = "";
            // 
            // lblTrangThaiXuLy
            // 
            this.lblTrangThaiXuLy.AutoSize = true;
            this.lblTrangThaiXuLy.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThaiXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThaiXuLy.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThaiXuLy.Location = new System.Drawing.Point(709, 19);
            this.lblTrangThaiXuLy.Name = "lblTrangThaiXuLy";
            this.lblTrangThaiXuLy.Size = new System.Drawing.Size(87, 13);
            this.lblTrangThaiXuLy.TabIndex = 4;
            this.lblTrangThaiXuLy.Text = "Chưa khai báo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(608, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Trạng thái xử lý: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(425, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ngày tiếp nhận";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Ngày thông báo hủy";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(230, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Số tiếp nhận";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Location = new System.Drawing.Point(319, 15);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(100, 21);
            this.txtSoTiepNhan.TabIndex = 3;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.cmdSave);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 439);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(804, 38);
            this.uiGroupBox2.TabIndex = 5;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // NPLXinHuyForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(804, 505);
            this.Controls.Add(this.LeftRebar1);
            this.Controls.Add(this.RightRebar1);
            this.Controls.Add(this.TopRebar1);
            this.Controls.Add(this.BottomRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "NPLXinHuyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Bảng kê NPL tiêu hủy";
            this.Load += new System.EventHandler(this.BKTaiXuatForm_Load);
            this.Controls.SetChildIndex(this.BottomRebar1, 0);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.RightRebar1, 0);
            this.Controls.SetChildIndex(this.LeftRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.dgList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX grdList;
        private Janus.Windows.EditControls.UIGroupBox dgList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiHinhXuat;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongXuat;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIButton cmdAdd;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvToKhaiXuat;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton cmdSave;
        private Company.Controls.CustomValidation.CompareValidator cvSoLuong;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanDulieu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQua1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanDulieu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQua;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDangKy;
        private System.Windows.Forms.Label label1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayTiepNhan;
        private System.Windows.Forms.Label lblTrangThaiXuLy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhichu;
        private System.Windows.Forms.Label label7;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayThongBao;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyBK;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyBK1;
    }
}