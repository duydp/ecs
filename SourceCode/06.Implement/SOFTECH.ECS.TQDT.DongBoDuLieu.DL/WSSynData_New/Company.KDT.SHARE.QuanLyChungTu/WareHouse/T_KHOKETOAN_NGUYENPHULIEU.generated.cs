﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.WareHouse
{
	public partial class T_KHOKETOAN_NGUYENPHULIEU : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MANPL { set; get; }
		public string TENNPL { set; get; }
		public string MAHS { set; get; }
		public string DVT { set; get; }
		public string TENTIENGANH { set; get; }
		public string GHICHU { set; get; }
		public string MAKHO { set; get; }
		public string TAIKHOAN { set; get; }
		public string DONHANG { set; get; }
		public string NGUOIBAN { set; get; }
		public string KICHTHUOC { set; get; }
		public string MAUSAC { set; get; }
		public long HOPDONG_ID { set; get; }
        public List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLMapCollection = new List<T_KHOKETOAN_NGUYENPHULIEU_MAP>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KHOKETOAN_NGUYENPHULIEU> ConvertToCollection(IDataReader reader)
		{
			List<T_KHOKETOAN_NGUYENPHULIEU> collection = new List<T_KHOKETOAN_NGUYENPHULIEU>();
			while (reader.Read())
			{
				T_KHOKETOAN_NGUYENPHULIEU entity = new T_KHOKETOAN_NGUYENPHULIEU();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MANPL"))) entity.MANPL = reader.GetString(reader.GetOrdinal("MANPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENNPL"))) entity.TENNPL = reader.GetString(reader.GetOrdinal("TENNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAHS"))) entity.MAHS = reader.GetString(reader.GetOrdinal("MAHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENTIENGANH"))) entity.TENTIENGANH = reader.GetString(reader.GetOrdinal("TENTIENGANH"));
				if (!reader.IsDBNull(reader.GetOrdinal("GHICHU"))) entity.GHICHU = reader.GetString(reader.GetOrdinal("GHICHU"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAKHO"))) entity.MAKHO = reader.GetString(reader.GetOrdinal("MAKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TAIKHOAN"))) entity.TAIKHOAN = reader.GetString(reader.GetOrdinal("TAIKHOAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("DONHANG"))) entity.DONHANG = reader.GetString(reader.GetOrdinal("DONHANG"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGUOIBAN"))) entity.NGUOIBAN = reader.GetString(reader.GetOrdinal("NGUOIBAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("KICHTHUOC"))) entity.KICHTHUOC = reader.GetString(reader.GetOrdinal("KICHTHUOC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAUSAC"))) entity.MAUSAC = reader.GetString(reader.GetOrdinal("MAUSAC"));
				if (!reader.IsDBNull(reader.GetOrdinal("HOPDONG_ID"))) entity.HOPDONG_ID = reader.GetInt64(reader.GetOrdinal("HOPDONG_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KHOKETOAN_NGUYENPHULIEU> collection, long id)
        {
            foreach (T_KHOKETOAN_NGUYENPHULIEU item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KHOKETOAN_NGUYENPHULIEU VALUES(@MANPL, @TENNPL, @MAHS, @DVT, @TENTIENGANH, @GHICHU, @MAKHO, @TAIKHOAN, @DONHANG, @NGUOIBAN, @KICHTHUOC, @MAUSAC, @HOPDONG_ID)";
            string update = "UPDATE T_KHOKETOAN_NGUYENPHULIEU SET MANPL = @MANPL, TENNPL = @TENNPL, MAHS = @MAHS, DVT = @DVT, TENTIENGANH = @TENTIENGANH, GHICHU = @GHICHU, MAKHO = @MAKHO, TAIKHOAN = @TAIKHOAN, DONHANG = @DONHANG, NGUOIBAN = @NGUOIBAN, KICHTHUOC = @KICHTHUOC, MAUSAC = @MAUSAC, HOPDONG_ID = @HOPDONG_ID WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_NGUYENPHULIEU WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHS", SqlDbType.VarChar, "MAHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENTIENGANH", SqlDbType.VarChar, "TENTIENGANH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.VarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOAN", SqlDbType.VarChar, "TAIKHOAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONHANG", SqlDbType.NVarChar, "DONHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGUOIBAN", SqlDbType.NVarChar, "NGUOIBAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KICHTHUOC", SqlDbType.NVarChar, "KICHTHUOC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAUSAC", SqlDbType.NVarChar, "MAUSAC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHS", SqlDbType.VarChar, "MAHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENTIENGANH", SqlDbType.VarChar, "TENTIENGANH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.VarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOAN", SqlDbType.VarChar, "TAIKHOAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONHANG", SqlDbType.NVarChar, "DONHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGUOIBAN", SqlDbType.NVarChar, "NGUOIBAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KICHTHUOC", SqlDbType.NVarChar, "KICHTHUOC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAUSAC", SqlDbType.NVarChar, "MAUSAC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KHOKETOAN_NGUYENPHULIEU VALUES(@MANPL, @TENNPL, @MAHS, @DVT, @TENTIENGANH, @GHICHU, @MAKHO, @TAIKHOAN, @DONHANG, @NGUOIBAN, @KICHTHUOC, @MAUSAC, @HOPDONG_ID)";
            string update = "UPDATE T_KHOKETOAN_NGUYENPHULIEU SET MANPL = @MANPL, TENNPL = @TENNPL, MAHS = @MAHS, DVT = @DVT, TENTIENGANH = @TENTIENGANH, GHICHU = @GHICHU, MAKHO = @MAKHO, TAIKHOAN = @TAIKHOAN, DONHANG = @DONHANG, NGUOIBAN = @NGUOIBAN, KICHTHUOC = @KICHTHUOC, MAUSAC = @MAUSAC, HOPDONG_ID = @HOPDONG_ID WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_NGUYENPHULIEU WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHS", SqlDbType.VarChar, "MAHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENTIENGANH", SqlDbType.VarChar, "TENTIENGANH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.VarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOAN", SqlDbType.VarChar, "TAIKHOAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONHANG", SqlDbType.NVarChar, "DONHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGUOIBAN", SqlDbType.NVarChar, "NGUOIBAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KICHTHUOC", SqlDbType.NVarChar, "KICHTHUOC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAUSAC", SqlDbType.NVarChar, "MAUSAC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.NVarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHS", SqlDbType.VarChar, "MAHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENTIENGANH", SqlDbType.VarChar, "TENTIENGANH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.VarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOAN", SqlDbType.VarChar, "TAIKHOAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONHANG", SqlDbType.NVarChar, "DONHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGUOIBAN", SqlDbType.NVarChar, "NGUOIBAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KICHTHUOC", SqlDbType.NVarChar, "KICHTHUOC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAUSAC", SqlDbType.NVarChar, "MAUSAC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KHOKETOAN_NGUYENPHULIEU Load(long id)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KHOKETOAN_NGUYENPHULIEU> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KHOKETOAN_NGUYENPHULIEU> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KHOKETOAN_NGUYENPHULIEU> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KHOKETOAN_NGUYENPHULIEU(string mANPL, string tENNPL, string mAHS, string dVT, string tENTIENGANH, string gHICHU, string mAKHO, string tAIKHOAN, string dONHANG, string nGUOIBAN, string kICHTHUOC, string mAUSAC, long hOPDONG_ID)
		{
			T_KHOKETOAN_NGUYENPHULIEU entity = new T_KHOKETOAN_NGUYENPHULIEU();	
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.MAHS = mAHS;
			entity.DVT = dVT;
			entity.TENTIENGANH = tENTIENGANH;
			entity.GHICHU = gHICHU;
			entity.MAKHO = mAKHO;
			entity.TAIKHOAN = tAIKHOAN;
			entity.DONHANG = dONHANG;
			entity.NGUOIBAN = nGUOIBAN;
			entity.KICHTHUOC = kICHTHUOC;
			entity.MAUSAC = mAUSAC;
			entity.HOPDONG_ID = hOPDONG_ID;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.NVarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@MAHS", SqlDbType.VarChar, MAHS);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@TENTIENGANH", SqlDbType.VarChar, TENTIENGANH);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.VarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TAIKHOAN", SqlDbType.VarChar, TAIKHOAN);
			db.AddInParameter(dbCommand, "@DONHANG", SqlDbType.NVarChar, DONHANG);
			db.AddInParameter(dbCommand, "@NGUOIBAN", SqlDbType.NVarChar, NGUOIBAN);
			db.AddInParameter(dbCommand, "@KICHTHUOC", SqlDbType.NVarChar, KICHTHUOC);
			db.AddInParameter(dbCommand, "@MAUSAC", SqlDbType.NVarChar, MAUSAC);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KHOKETOAN_NGUYENPHULIEU> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_NGUYENPHULIEU item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KHOKETOAN_NGUYENPHULIEU(long id, string mANPL, string tENNPL, string mAHS, string dVT, string tENTIENGANH, string gHICHU, string mAKHO, string tAIKHOAN, string dONHANG, string nGUOIBAN, string kICHTHUOC, string mAUSAC, long hOPDONG_ID)
		{
			T_KHOKETOAN_NGUYENPHULIEU entity = new T_KHOKETOAN_NGUYENPHULIEU();			
			entity.ID = id;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.MAHS = mAHS;
			entity.DVT = dVT;
			entity.TENTIENGANH = tENTIENGANH;
			entity.GHICHU = gHICHU;
			entity.MAKHO = mAKHO;
			entity.TAIKHOAN = tAIKHOAN;
			entity.DONHANG = dONHANG;
			entity.NGUOIBAN = nGUOIBAN;
			entity.KICHTHUOC = kICHTHUOC;
			entity.MAUSAC = mAUSAC;
			entity.HOPDONG_ID = hOPDONG_ID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KHOKETOAN_NGUYENPHULIEU_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.NVarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@MAHS", SqlDbType.VarChar, MAHS);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@TENTIENGANH", SqlDbType.VarChar, TENTIENGANH);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.VarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TAIKHOAN", SqlDbType.VarChar, TAIKHOAN);
			db.AddInParameter(dbCommand, "@DONHANG", SqlDbType.NVarChar, DONHANG);
			db.AddInParameter(dbCommand, "@NGUOIBAN", SqlDbType.NVarChar, NGUOIBAN);
			db.AddInParameter(dbCommand, "@KICHTHUOC", SqlDbType.NVarChar, KICHTHUOC);
			db.AddInParameter(dbCommand, "@MAUSAC", SqlDbType.NVarChar, MAUSAC);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KHOKETOAN_NGUYENPHULIEU> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_NGUYENPHULIEU item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KHOKETOAN_NGUYENPHULIEU(long id, string mANPL, string tENNPL, string mAHS, string dVT, string tENTIENGANH, string gHICHU, string mAKHO, string tAIKHOAN, string dONHANG, string nGUOIBAN, string kICHTHUOC, string mAUSAC, long hOPDONG_ID)
		{
			T_KHOKETOAN_NGUYENPHULIEU entity = new T_KHOKETOAN_NGUYENPHULIEU();			
			entity.ID = id;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.MAHS = mAHS;
			entity.DVT = dVT;
			entity.TENTIENGANH = tENTIENGANH;
			entity.GHICHU = gHICHU;
			entity.MAKHO = mAKHO;
			entity.TAIKHOAN = tAIKHOAN;
			entity.DONHANG = dONHANG;
			entity.NGUOIBAN = nGUOIBAN;
			entity.KICHTHUOC = kICHTHUOC;
			entity.MAUSAC = mAUSAC;
			entity.HOPDONG_ID = hOPDONG_ID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.NVarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@MAHS", SqlDbType.VarChar, MAHS);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@TENTIENGANH", SqlDbType.VarChar, TENTIENGANH);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.VarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TAIKHOAN", SqlDbType.VarChar, TAIKHOAN);
			db.AddInParameter(dbCommand, "@DONHANG", SqlDbType.NVarChar, DONHANG);
			db.AddInParameter(dbCommand, "@NGUOIBAN", SqlDbType.NVarChar, NGUOIBAN);
			db.AddInParameter(dbCommand, "@KICHTHUOC", SqlDbType.NVarChar, KICHTHUOC);
			db.AddInParameter(dbCommand, "@MAUSAC", SqlDbType.NVarChar, MAUSAC);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KHOKETOAN_NGUYENPHULIEU> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_NGUYENPHULIEU item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KHOKETOAN_NGUYENPHULIEU(long id)
		{
			T_KHOKETOAN_NGUYENPHULIEU entity = new T_KHOKETOAN_NGUYENPHULIEU();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KHOKETOAN_NGUYENPHULIEU> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_NGUYENPHULIEU item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPLMapCollection)
                    {
                        if (item.KHONPL_ID == 0)
                        {
                            item.KHONPL_ID = this.ID;
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public void DeleteFull()
        {
            try
            {
                foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPLMapCollection)
                {
                    item.KHONPL_ID = this.ID;
                    item.Delete();
                }          
                this.Delete();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }
	}	
}