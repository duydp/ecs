using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class OLA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute OLT { get; set; }
public PropertiesAttribute IEI { get; set; }
public PropertiesAttribute CHC { get; set; }
public PropertiesAttribute TRC { get; set; }
public PropertiesAttribute TRN { get; set; }
public PropertiesAttribute TRA { get; set; }
public PropertiesAttribute TCN { get; set; }
public PropertiesAttribute TCD { get; set; }
public PropertiesAttribute EDC { get; set; }
public PropertiesAttribute BYA { get; set; }
public PropertiesAttribute OBJ { get; set; }
public PropertiesAttribute USS { get; set; }
public PropertiesAttribute SDT { get; set; }
public PropertiesAttribute SDH { get; set; }
public PropertiesAttribute EDT { get; set; }
public PropertiesAttribute EDH { get; set; }
public PropertiesAttribute DPR { get; set; }
public PropertiesAttribute BRT { get; set; }
public PropertiesAttribute PRT { get; set; }
public PropertiesAttribute DPN { get; set; }
public PropertiesAttribute ARR { get; set; }
public PropertiesAttribute ARB { get; set; }
public PropertiesAttribute ARP { get; set; }
public PropertiesAttribute ARN { get; set; }
public PropertiesAttribute ROU { get; set; }
public PropertiesAttribute TOS { get; set; }
public PropertiesAttribute SBC { get; set; }
public PropertiesAttribute RYA { get; set; }
public PropertiesAttribute SCM { get; set; }
public PropertiesAttribute SCN { get; set; }
public PropertiesAttribute SEP { get; set; }
public PropertiesAttribute NTA { get; set; }
public GroupAttribute CG_ { get; set; }
public GroupAttribute E__ { get; set; }

public OLA()
        {
OLT = new PropertiesAttribute(12, typeof(decimal));
IEI = new PropertiesAttribute(1, typeof(string));
CHC = new PropertiesAttribute(6, typeof(string));
TRC = new PropertiesAttribute(13, typeof(string));
TRN = new PropertiesAttribute(300, typeof(string));
TRA = new PropertiesAttribute(300, typeof(string));
TCN = new PropertiesAttribute(11, typeof(string));
TCD = new PropertiesAttribute(8, typeof(DateTime));
EDC = new PropertiesAttribute(8, typeof(DateTime));
BYA = new PropertiesAttribute(2, typeof(string));
OBJ = new PropertiesAttribute(3, typeof(string));
USS = new PropertiesAttribute(2, typeof(string));
SDT = new PropertiesAttribute(8, typeof(DateTime));
SDH = new PropertiesAttribute(2, typeof(string));
EDT = new PropertiesAttribute(8, typeof(DateTime));
EDH = new PropertiesAttribute(2, typeof(string));
DPR = new PropertiesAttribute(7, typeof(string));
BRT = new PropertiesAttribute(6, typeof(string));
PRT = new PropertiesAttribute(6, typeof(string));
DPN = new PropertiesAttribute(105, typeof(string));
ARR = new PropertiesAttribute(7, typeof(string));
ARB = new PropertiesAttribute(6, typeof(string));
ARP = new PropertiesAttribute(6, typeof(string));
ARN = new PropertiesAttribute(105, typeof(string));
ROU = new PropertiesAttribute(35, typeof(string));
TOS = new PropertiesAttribute(1, typeof(string));
SBC = new PropertiesAttribute(11, typeof(string));
RYA = new PropertiesAttribute(4, typeof(int));
SCM = new PropertiesAttribute(10, typeof(string));
SCN = new PropertiesAttribute(10, typeof(string));
SEP = new PropertiesAttribute(11, typeof(int));
NTA = new PropertiesAttribute(765, typeof(string));
#region CG_
List<PropertiesAttribute> listCG_ = new List<PropertiesAttribute>();
listCG_.Add(new PropertiesAttribute(35, 5, EnumGroupID.OLA_CG_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.OLA_DB_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(210, 5, EnumGroupID.OLA_CM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(4, 5, EnumGroupID.OLA_HS_, typeof(string)));
listCG_.Add(new PropertiesAttribute(140, 5, EnumGroupID.OLA_MR_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.OLA_IS_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(1, 5, EnumGroupID.OLA_RM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.OLA_OR_, typeof(string)));
listCG_.Add(new PropertiesAttribute(6, 5, EnumGroupID.OLA_LP_, typeof(string)));
listCG_.Add(new PropertiesAttribute(6, 5, EnumGroupID.OLA_PA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(1, 5, EnumGroupID.OLA_TM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(38, 5, EnumGroupID.OLA_TE_, typeof(string)));
listCG_.Add(new PropertiesAttribute(35, 5, EnumGroupID.OLA_SN_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.OLA_DD_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(13, 5, EnumGroupID.OLA_IM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.OLA_IN_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.OLA_IA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(13, 5, EnumGroupID.OLA_EM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.OLA_EN_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.OLA_EA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(13, 5, EnumGroupID.OLA_KM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.OLA_KN_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.OLA_KA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.OLA_OA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.OLA_OB_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.OLA_OC_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.OLA_OD_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.OLA_OE_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.OLA_PC_, typeof(int)));
listCG_.Add(new PropertiesAttribute(3, 5, EnumGroupID.OLA_PU_, typeof(string)));
listCG_.Add(new PropertiesAttribute(10, 5, EnumGroupID.OLA_GW_, typeof(int)));
listCG_.Add(new PropertiesAttribute(3, 5, EnumGroupID.OLA_WU_, typeof(string)));
listCG_.Add(new PropertiesAttribute(10, 5, EnumGroupID.OLA_VO_, typeof(int)));
listCG_.Add(new PropertiesAttribute(3, 5, EnumGroupID.OLA_VU_, typeof(string)));
listCG_.Add(new PropertiesAttribute(20, 5, EnumGroupID.OLA_PR_, typeof(int)));
listCG_.Add(new PropertiesAttribute(3, 5, EnumGroupID.OLA_RT_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.OLA_RA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.OLA_RB_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.OLA_RC_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.OLA_RD_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.OLA_RE, typeof(string)));
listCG_.Add(new PropertiesAttribute(11, 5, EnumGroupID.OLA_PM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.OLA_PD_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.OLA_EP_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(765, 5, EnumGroupID.OLA_NT_, typeof(string)));
CG_ = new GroupAttribute("CG_", 5, listCG_);
#endregion CG_
#region E__
List<PropertiesAttribute> listE__ = new List<PropertiesAttribute>();
listE__.Add(new PropertiesAttribute(12, 50, EnumGroupID.OLA_E__, typeof(int)));
E__ = new GroupAttribute("E__", 50, listE__);
#endregion E__
TongSoByte = 19120;
}

}public partial class EnumGroupID
    {
public static readonly string OLA_CG_ = "OLA_CG_";
public static readonly string OLA_DB_ = "OLA_DB_";
public static readonly string OLA_CM_ = "OLA_CM_";
public static readonly string OLA_HS_ = "OLA_HS_";
public static readonly string OLA_MR_ = "OLA_MR_";
public static readonly string OLA_IS_ = "OLA_IS_";
public static readonly string OLA_RM_ = "OLA_RM_";
public static readonly string OLA_OR_ = "OLA_OR_";
public static readonly string OLA_LP_ = "OLA_LP_";
public static readonly string OLA_PA_ = "OLA_PA_";
public static readonly string OLA_TM_ = "OLA_TM_";
public static readonly string OLA_TE_ = "OLA_TE_";
public static readonly string OLA_SN_ = "OLA_SN_";
public static readonly string OLA_DD_ = "OLA_DD_";
public static readonly string OLA_IM_ = "OLA_IM_";
public static readonly string OLA_IN_ = "OLA_IN_";
public static readonly string OLA_IA_ = "OLA_IA_";
public static readonly string OLA_EM_ = "OLA_EM_";
public static readonly string OLA_EN_ = "OLA_EN_";
public static readonly string OLA_EA_ = "OLA_EA_";
public static readonly string OLA_KM_ = "OLA_KM_";
public static readonly string OLA_KN_ = "OLA_KN_";
public static readonly string OLA_KA_ = "OLA_KA_";
public static readonly string OLA_OA_ = "OLA_OA_";
public static readonly string OLA_OB_ = "OLA_OB_";
public static readonly string OLA_OC_ = "OLA_OC_";
public static readonly string OLA_OD_ = "OLA_OD_";
public static readonly string OLA_OE_ = "OLA_OE_";
public static readonly string OLA_PC_ = "OLA_PC_";
public static readonly string OLA_PU_ = "OLA_PU_";
public static readonly string OLA_GW_ = "OLA_GW_";
public static readonly string OLA_WU_ = "OLA_WU_";
public static readonly string OLA_VO_ = "OLA_VO_";
public static readonly string OLA_VU_ = "OLA_VU_";
public static readonly string OLA_PR_ = "OLA_PR_";
public static readonly string OLA_RT_ = "OLA_RT_";
public static readonly string OLA_RA_ = "OLA_RA_";
public static readonly string OLA_RB_ = "OLA_RB_";
public static readonly string OLA_RC_ = "OLA_RC_";
public static readonly string OLA_RD_ = "OLA_RD_";
public static readonly string OLA_RE = "OLA_RE";
public static readonly string OLA_PM_ = "OLA_PM_";
public static readonly string OLA_PD_ = "OLA_PD_";
public static readonly string OLA_EP_ = "OLA_EP_";
public static readonly string OLA_NT_ = "OLA_NT_";
public static readonly string OLA_E__ = "OLA_E__";
}

}
