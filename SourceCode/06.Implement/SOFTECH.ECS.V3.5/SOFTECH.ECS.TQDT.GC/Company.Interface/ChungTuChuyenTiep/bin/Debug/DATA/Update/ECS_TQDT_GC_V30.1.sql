
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TotalInventoryReport]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_TotalInventoryReport]
GO

CREATE TABLE [dbo].[t_KDT_VNACCS_TotalInventoryReport]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[SoTiepNhan] [bigint] NOT NULL,
[NgayTiepNhan] [datetime] NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrangThaiXuLy] [int] NOT NULL,
[GuidString] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayChotTon] [datetime] NOT NULL,
[LoaiBaoCao] [numeric] (5, 0) NOT NULL,
[LoaiSua] [numeric] (1, 0) NULL,
[GhiChuKhac] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[TotalInventory_ID] [bigint] NOT NULL FOREIGN KEY REFERENCES [dbo].[t_KDT_VNACCS_TotalInventoryReport](ID) ,
[SoHopDong] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayHopDong] [datetime] NOT NULL,
[MaHQ] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayHetHan] [datetime] NOT NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[ContractReference_ID] [bigint] NOT NULL FOREIGN KEY REFERENCES [dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference](ID),
[TenHangHoa] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHangHoa] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongTonKhoSoSach] [numeric] (18, 8) NOT NULL,
[SoLuongTonKhoThucTe] [numeric] (18, 4) NOT NULL,
[DVT] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(17),
	@TrangThaiXuLy int,
	@GuidString nvarchar(500),
	@NgayChotTon datetime,
	@LoaiBaoCao numeric(5, 0),
	@LoaiSua numeric(1, 0),
	@GhiChuKhac nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString],
	[NgayChotTon],
	[LoaiBaoCao],
	[LoaiSua],
	[GhiChuKhac]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@TrangThaiXuLy,
	@GuidString,
	@NgayChotTon,
	@LoaiBaoCao,
	@LoaiSua,
	@GhiChuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(17),
	@TrangThaiXuLy int,
	@GuidString nvarchar(500),
	@NgayChotTon datetime,
	@LoaiBaoCao numeric(5, 0),
	@LoaiSua numeric(1, 0),
	@GhiChuKhac nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TotalInventoryReport]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString,
	[NgayChotTon] = @NgayChotTon,
	[LoaiBaoCao] = @LoaiBaoCao,
	[LoaiSua] = @LoaiSua,
	[GhiChuKhac] = @GhiChuKhac
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(17),
	@TrangThaiXuLy int,
	@GuidString nvarchar(500),
	@NgayChotTon datetime,
	@LoaiBaoCao numeric(5, 0),
	@LoaiSua numeric(1, 0),
	@GhiChuKhac nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TotalInventoryReport] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString,
			[NgayChotTon] = @NgayChotTon,
			[LoaiBaoCao] = @LoaiBaoCao,
			[LoaiSua] = @LoaiSua,
			[GhiChuKhac] = @GhiChuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[TrangThaiXuLy],
			[GuidString],
			[NgayChotTon],
			[LoaiBaoCao],
			[LoaiSua],
			[GhiChuKhac]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@TrangThaiXuLy,
			@GuidString,
			@NgayChotTon,
			@LoaiBaoCao,
			@LoaiSua,
			@GhiChuKhac
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TotalInventoryReport]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString],
	[NgayChotTon],
	[LoaiBaoCao],
	[LoaiSua],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString],
	[NgayChotTon],
	[LoaiBaoCao],
	[LoaiSua],
	[GhiChuKhac]
FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString],
	[NgayChotTon],
	[LoaiBaoCao],
	[LoaiSua],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectBy_TotalInventory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectBy_TotalInventory_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteBy_TotalInventory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteBy_TotalInventory_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Insert]
	@TotalInventory_ID bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]
(
	[TotalInventory_ID],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan]
)
VALUES 
(
	@TotalInventory_ID,
	@SoHopDong,
	@NgayHopDong,
	@MaHQ,
	@NgayHetHan
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Update]
	@ID bigint,
	@TotalInventory_ID bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]
SET
	[TotalInventory_ID] = @TotalInventory_ID,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[MaHQ] = @MaHQ,
	[NgayHetHan] = @NgayHetHan
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_InsertUpdate]
	@ID bigint,
	@TotalInventory_ID bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference] 
		SET
			[TotalInventory_ID] = @TotalInventory_ID,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[MaHQ] = @MaHQ,
			[NgayHetHan] = @NgayHetHan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]
		(
			[TotalInventory_ID],
			[SoHopDong],
			[NgayHopDong],
			[MaHQ],
			[NgayHetHan]
		)
		VALUES 
		(
			@TotalInventory_ID,
			@SoHopDong,
			@NgayHopDong,
			@MaHQ,
			@NgayHetHan
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteBy_TotalInventory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteBy_TotalInventory_ID]
	@TotalInventory_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]
WHERE
	[TotalInventory_ID] = @TotalInventory_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TotalInventory_ID],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectBy_TotalInventory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectBy_TotalInventory_ID]
	@TotalInventory_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TotalInventory_ID],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]
WHERE
	[TotalInventory_ID] = @TotalInventory_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TotalInventory_ID],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan]
FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TotalInventory_ID],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_ContractReference]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_ContractReference_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_ContractReference_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteBy_ContractReference_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteBy_ContractReference_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Insert]
	@ContractReference_ID bigint,
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@SoLuongTonKhoSoSach numeric(18, 8),
	@SoLuongTonKhoThucTe numeric(18, 4),
	@DVT nvarchar(4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
(
	[ContractReference_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
)
VALUES 
(
	@ContractReference_ID,
	@TenHangHoa,
	@MaHangHoa,
	@SoLuongTonKhoSoSach,
	@SoLuongTonKhoThucTe,
	@DVT
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Update]
	@ID bigint,
	@ContractReference_ID bigint,
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@SoLuongTonKhoSoSach numeric(18, 8),
	@SoLuongTonKhoThucTe numeric(18, 4),
	@DVT nvarchar(4)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
SET
	[ContractReference_ID] = @ContractReference_ID,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[SoLuongTonKhoSoSach] = @SoLuongTonKhoSoSach,
	[SoLuongTonKhoThucTe] = @SoLuongTonKhoThucTe,
	[DVT] = @DVT
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_InsertUpdate]
	@ID bigint,
	@ContractReference_ID bigint,
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@SoLuongTonKhoSoSach numeric(18, 8),
	@SoLuongTonKhoThucTe numeric(18, 4),
	@DVT nvarchar(4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details] 
		SET
			[ContractReference_ID] = @ContractReference_ID,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[SoLuongTonKhoSoSach] = @SoLuongTonKhoSoSach,
			[SoLuongTonKhoThucTe] = @SoLuongTonKhoThucTe,
			[DVT] = @DVT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
		(
			[ContractReference_ID],
			[TenHangHoa],
			[MaHangHoa],
			[SoLuongTonKhoSoSach],
			[SoLuongTonKhoThucTe],
			[DVT]
		)
		VALUES 
		(
			@ContractReference_ID,
			@TenHangHoa,
			@MaHangHoa,
			@SoLuongTonKhoSoSach,
			@SoLuongTonKhoThucTe,
			@DVT
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteBy_ContractReference_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteBy_ContractReference_ID]
	@ContractReference_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
WHERE
	[ContractReference_ID] = @ContractReference_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ContractReference_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_ContractReference_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_ContractReference_ID]
	@ContractReference_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ContractReference_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
WHERE
	[ContractReference_ID] = @ContractReference_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ContractReference_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ContractReference_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]	

GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.1',GETDATE(), N'CẬP NHẬT TABLE BÁO CÁO CHỐT TỒN')
END