
ALTER TABLE [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra] DROP CONSTRAINT [FK_t_KDT_GiayKiemTra_HangGiayKiemTra_t_KDT_GiayKiemTra]
GO

/****** Object:  Table [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]    Script Date: 1/13/2013 7:37:12 AM ******/
DROP TABLE [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]
GO

/****** Object:  Table [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]    Script Date: 1/13/2013 7:37:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GiayKiemTra_ID] [bigint] NOT NULL,
	[HMD_ID] [bigint] NOT NULL,
	[MaHang] [varchar](50) NULL,
	[TenHang] [nvarchar](255) NULL,
	[MaHS] [varchar](12) NULL,
	[NuocXX_ID] [varchar](2) NULL,
	[SoLuong] [decimal](18, 4) NULL,
	[DVT_ID] [varchar](3) NULL,
	[GhiChu] [nvarchar](2000) NULL,
	[LoaiChungTu] [nvarchar](50) NULL,
	[SoChungTu] [nvarchar](50) NULL,
	[TenChungTu] [nvarchar](255) NULL,
	[NgayPhatHanh] [datetime] NULL,
	[DiaDiemKiemTra] [nvarchar](255) NULL,
	[MaCoQuanKT] [nvarchar](50) NULL,
	[TenCoQuanKT] [nvarchar](255) NULL,
	[KetQuaKT] [nchar](2000) NULL,
 CONSTRAINT [PK_t_KDT_GiayKiemTra_HangGiayKiemTra] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_GiayKiemTra_HangGiayKiemTra_t_KDT_GiayKiemTra] FOREIGN KEY([GiayKiemTra_ID])
REFERENCES [dbo].[t_KDT_GiayKiemTra] ([ID])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra] CHECK CONSTRAINT [FK_t_KDT_GiayKiemTra_HangGiayKiemTra_t_KDT_GiayKiemTra]
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectBy_GiayKiemTra_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectBy_GiayKiemTra_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteBy_GiayKiemTra_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteBy_GiayKiemTra_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Insert]
	@GiayKiemTra_ID bigint,
	@HMD_ID bigint,
	@MaHang varchar(50),
	@TenHang nvarchar(255),
	@MaHS varchar(12),
	@NuocXX_ID varchar(2),
	@SoLuong decimal(18, 4),
	@DVT_ID varchar(3),
	@GhiChu nvarchar(2000),
	@LoaiChungTu nvarchar(50),
	@SoChungTu nvarchar(50),
	@TenChungTu nvarchar(255),
	@NgayPhatHanh datetime,
	@DiaDiemKiemTra nvarchar(255),
	@MaCoQuanKT nvarchar(50),
	@TenCoQuanKT nvarchar(255),
	@KetQuaKT nchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]
(
	[GiayKiemTra_ID],
	[HMD_ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LoaiChungTu],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[DiaDiemKiemTra],
	[MaCoQuanKT],
	[TenCoQuanKT],
	[KetQuaKT]
)
VALUES 
(
	@GiayKiemTra_ID,
	@HMD_ID,
	@MaHang,
	@TenHang,
	@MaHS,
	@NuocXX_ID,
	@SoLuong,
	@DVT_ID,
	@GhiChu,
	@LoaiChungTu,
	@SoChungTu,
	@TenChungTu,
	@NgayPhatHanh,
	@DiaDiemKiemTra,
	@MaCoQuanKT,
	@TenCoQuanKT,
	@KetQuaKT
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Update]
	@ID bigint,
	@GiayKiemTra_ID bigint,
	@HMD_ID bigint,
	@MaHang varchar(50),
	@TenHang nvarchar(255),
	@MaHS varchar(12),
	@NuocXX_ID varchar(2),
	@SoLuong decimal(18, 4),
	@DVT_ID varchar(3),
	@GhiChu nvarchar(2000),
	@LoaiChungTu nvarchar(50),
	@SoChungTu nvarchar(50),
	@TenChungTu nvarchar(255),
	@NgayPhatHanh datetime,
	@DiaDiemKiemTra nvarchar(255),
	@MaCoQuanKT nvarchar(50),
	@TenCoQuanKT nvarchar(255),
	@KetQuaKT nchar(2000)
AS

UPDATE
	[dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]
SET
	[GiayKiemTra_ID] = @GiayKiemTra_ID,
	[HMD_ID] = @HMD_ID,
	[MaHang] = @MaHang,
	[TenHang] = @TenHang,
	[MaHS] = @MaHS,
	[NuocXX_ID] = @NuocXX_ID,
	[SoLuong] = @SoLuong,
	[DVT_ID] = @DVT_ID,
	[GhiChu] = @GhiChu,
	[LoaiChungTu] = @LoaiChungTu,
	[SoChungTu] = @SoChungTu,
	[TenChungTu] = @TenChungTu,
	[NgayPhatHanh] = @NgayPhatHanh,
	[DiaDiemKiemTra] = @DiaDiemKiemTra,
	[MaCoQuanKT] = @MaCoQuanKT,
	[TenCoQuanKT] = @TenCoQuanKT,
	[KetQuaKT] = @KetQuaKT
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_InsertUpdate]
	@ID bigint,
	@GiayKiemTra_ID bigint,
	@HMD_ID bigint,
	@MaHang varchar(50),
	@TenHang nvarchar(255),
	@MaHS varchar(12),
	@NuocXX_ID varchar(2),
	@SoLuong decimal(18, 4),
	@DVT_ID varchar(3),
	@GhiChu nvarchar(2000),
	@LoaiChungTu nvarchar(50),
	@SoChungTu nvarchar(50),
	@TenChungTu nvarchar(255),
	@NgayPhatHanh datetime,
	@DiaDiemKiemTra nvarchar(255),
	@MaCoQuanKT nvarchar(50),
	@TenCoQuanKT nvarchar(255),
	@KetQuaKT nchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra] 
		SET
			[GiayKiemTra_ID] = @GiayKiemTra_ID,
			[HMD_ID] = @HMD_ID,
			[MaHang] = @MaHang,
			[TenHang] = @TenHang,
			[MaHS] = @MaHS,
			[NuocXX_ID] = @NuocXX_ID,
			[SoLuong] = @SoLuong,
			[DVT_ID] = @DVT_ID,
			[GhiChu] = @GhiChu,
			[LoaiChungTu] = @LoaiChungTu,
			[SoChungTu] = @SoChungTu,
			[TenChungTu] = @TenChungTu,
			[NgayPhatHanh] = @NgayPhatHanh,
			[DiaDiemKiemTra] = @DiaDiemKiemTra,
			[MaCoQuanKT] = @MaCoQuanKT,
			[TenCoQuanKT] = @TenCoQuanKT,
			[KetQuaKT] = @KetQuaKT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]
		(
			[GiayKiemTra_ID],
			[HMD_ID],
			[MaHang],
			[TenHang],
			[MaHS],
			[NuocXX_ID],
			[SoLuong],
			[DVT_ID],
			[GhiChu],
			[LoaiChungTu],
			[SoChungTu],
			[TenChungTu],
			[NgayPhatHanh],
			[DiaDiemKiemTra],
			[MaCoQuanKT],
			[TenCoQuanKT],
			[KetQuaKT]
		)
		VALUES 
		(
			@GiayKiemTra_ID,
			@HMD_ID,
			@MaHang,
			@TenHang,
			@MaHS,
			@NuocXX_ID,
			@SoLuong,
			@DVT_ID,
			@GhiChu,
			@LoaiChungTu,
			@SoChungTu,
			@TenChungTu,
			@NgayPhatHanh,
			@DiaDiemKiemTra,
			@MaCoQuanKT,
			@TenCoQuanKT,
			@KetQuaKT
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteBy_GiayKiemTra_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteBy_GiayKiemTra_ID]
	@GiayKiemTra_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]
WHERE
	[GiayKiemTra_ID] = @GiayKiemTra_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayKiemTra_ID],
	[HMD_ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LoaiChungTu],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[DiaDiemKiemTra],
	[MaCoQuanKT],
	[TenCoQuanKT],
	[KetQuaKT]
FROM
	[dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectBy_GiayKiemTra_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectBy_GiayKiemTra_ID]
	@GiayKiemTra_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayKiemTra_ID],
	[HMD_ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LoaiChungTu],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[DiaDiemKiemTra],
	[MaCoQuanKT],
	[TenCoQuanKT],
	[KetQuaKT]
FROM
	[dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]
WHERE
	[GiayKiemTra_ID] = @GiayKiemTra_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayKiemTra_ID],
	[HMD_ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LoaiChungTu],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[DiaDiemKiemTra],
	[MaCoQuanKT],
	[TenCoQuanKT],
	[KetQuaKT]
FROM [dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Sunday, January 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayKiemTra_ID],
	[HMD_ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LoaiChungTu],
	[SoChungTu],
	[TenChungTu],
	[NgayPhatHanh],
	[DiaDiemKiemTra],
	[MaCoQuanKT],
	[TenCoQuanKT],
	[KetQuaKT]
FROM
	[dbo].[t_KDT_GiayKiemTra_HangGiayKiemTra]	

GO

UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '6.2'
      ,[Date] = getdate()
      ,[Notes] = ''
 GO