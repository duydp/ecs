/****** Object:  Table [dbo].[t_KDT_HangVanDonDetail]    Script Date: 04/09/2013 10:52:08 ******/
IF (SELECT COUNT(*) --column_name 'Column Name', data_type 'Data Type', CHARacter_maximum_length 'Maximum Length' 
	FROM information_schema.columns
	WHERE table_name = 't_KDT_HangVanDonDetail' AND COLUMN_NAME = 'KichThuocHoacTheTich') = 0
BEGIN

ALTER TABLE [dbo].[t_KDT_HangVanDonDetail]
ADD [KichThuocHoacTheTich] [nvarchar](50) NULL

END

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_SelectAll]    Script Date: 04/09/2013 10:55:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_SelectDynamic]    Script Date: 04/09/2013 10:55:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]    Script Date: 04/09/2013 10:55:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_Load]    Script Date: 04/09/2013 10:55:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_Delete]    Script Date: 04/09/2013 10:55:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]    Script Date: 04/09/2013 10:55:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_Update]    Script Date: 04/09/2013 10:55:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_Insert]    Script Date: 04/09/2013 10:55:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Insert]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_SelectAll]    Script Date: 04/09/2013 10:55:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB],
	[LoaiKien],
	[SoHieuContainer],
	[SoHieuKien],
	[TrongLuong],
	[SoKien],
	[TrongLuongTinh],
	[KichThuocHoacTheTich]
FROM
	[dbo].[t_KDT_HangVanDonDetail]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_SelectDynamic]    Script Date: 04/09/2013 10:55:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[VanDon_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB],
	[LoaiKien],
	[SoHieuContainer],
	[SoHieuKien],
	[TrongLuong],
	[SoKien],
	[TrongLuongTinh],
	[KichThuocHoacTheTich]
FROM [dbo].[t_KDT_HangVanDonDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]    Script Date: 04/09/2013 10:55:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HangVanDonDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_Load]    Script Date: 04/09/2013 10:55:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB],
	[LoaiKien],
	[SoHieuContainer],
	[SoHieuKien],
	[TrongLuong],
	[SoKien],
	[TrongLuongTinh],
	[KichThuocHoacTheTich]
FROM
	[dbo].[t_KDT_HangVanDonDetail]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_Delete]    Script Date: 04/09/2013 10:55:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HangVanDonDetail]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]    Script Date: 04/09/2013 10:55:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]
	@ID bigint,
	@VanDon_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@TriGiaKB float,
	@LoaiKien nvarchar(10),
	@SoHieuContainer nvarchar(50),
	@SoHieuKien nvarchar(50),
	@TrongLuong float,
	@SoKien decimal(8, 0),
	@TrongLuongTinh float,
	@KichThuocHoacTheTich nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangVanDonDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangVanDonDetail] 
		SET
			[VanDon_ID] = @VanDon_ID,
			[MaNguyenTe] = @MaNguyenTe,
			[MaChuyenNganh] = @MaChuyenNganh,
			[GhiChu] = @GhiChu,
			[HMD_ID] = @HMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB,
			[LoaiKien] = @LoaiKien,
			[SoHieuContainer] = @SoHieuContainer,
			[SoHieuKien] = @SoHieuKien,
			[TrongLuong] = @TrongLuong,
			[SoKien] = @SoKien,
			[TrongLuongTinh] = @TrongLuongTinh,
			[KichThuocHoacTheTich] = @KichThuocHoacTheTich
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangVanDonDetail]
		(
			[VanDon_ID],
			[MaNguyenTe],
			[MaChuyenNganh],
			[GhiChu],
			[HMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB],
			[LoaiKien],
			[SoHieuContainer],
			[SoHieuKien],
			[TrongLuong],
			[SoKien],
			[TrongLuongTinh],
			[KichThuocHoacTheTich]
		)
		VALUES 
		(
			@VanDon_ID,
			@MaNguyenTe,
			@MaChuyenNganh,
			@GhiChu,
			@HMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB,
			@LoaiKien,
			@SoHieuContainer,
			@SoHieuKien,
			@TrongLuong,
			@SoKien,
			@TrongLuongTinh,
			@KichThuocHoacTheTich
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_Update]    Script Date: 04/09/2013 10:55:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Update]
	@ID bigint,
	@VanDon_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@TriGiaKB float,
	@LoaiKien nvarchar(10),
	@SoHieuContainer nvarchar(50),
	@SoHieuKien nvarchar(50),
	@TrongLuong float,
	@SoKien decimal(8, 0),
	@TrongLuongTinh float,
	@KichThuocHoacTheTich nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_HangVanDonDetail]
SET
	[VanDon_ID] = @VanDon_ID,
	[MaNguyenTe] = @MaNguyenTe,
	[MaChuyenNganh] = @MaChuyenNganh,
	[GhiChu] = @GhiChu,
	[HMD_ID] = @HMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB,
	[LoaiKien] = @LoaiKien,
	[SoHieuContainer] = @SoHieuContainer,
	[SoHieuKien] = @SoHieuKien,
	[TrongLuong] = @TrongLuong,
	[SoKien] = @SoKien,
	[TrongLuongTinh] = @TrongLuongTinh,
	[KichThuocHoacTheTich] = @KichThuocHoacTheTich
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_HangVanDonDetail_Insert]    Script Date: 04/09/2013 10:55:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Insert]
	@VanDon_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@TriGiaKB float,
	@LoaiKien nvarchar(10),
	@SoHieuContainer nvarchar(50),
	@SoHieuKien nvarchar(50),
	@TrongLuong float,
	@SoKien decimal(8, 0),
	@TrongLuongTinh float,
	@KichThuocHoacTheTich nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_HangVanDonDetail]
(
	[VanDon_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB],
	[LoaiKien],
	[SoHieuContainer],
	[SoHieuKien],
	[TrongLuong],
	[SoKien],
	[TrongLuongTinh],
	[KichThuocHoacTheTich]
)
VALUES 
(
	@VanDon_ID,
	@MaNguyenTe,
	@MaChuyenNganh,
	@GhiChu,
	@HMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB,
	@LoaiKien,
	@SoHieuContainer,
	@SoHieuKien,
	@TrongLuong,
	@SoKien,
	@TrongLuongTinh,
	@KichThuocHoacTheTich
)

SET @ID = SCOPE_IDENTITY()


GO

update t_haiquan_version  set version='7.9', notes=N'Cập nhật vận đơn'
