﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;


namespace Company.KDT.SHARE.VNACCS
{
     public partial class KDT_VNACCS_TEA
    {
         List<KDT_VNACCS_TEADieuChinh> _ListDieuChinh = new List<KDT_VNACCS_TEADieuChinh>();
         List<KDT_VNACCS_TEANguoiXNK> _ListNguoiXNK = new List<KDT_VNACCS_TEANguoiXNK>();
         List<KDT_VNACCS_TEA_HangHoa> _ListHang = new List<KDT_VNACCS_TEA_HangHoa>();

         public List<KDT_VNACCS_TEA_HangHoa> HangCollection
        {
            set { this._ListHang = value; }
            get { return this._ListHang; }
        }
         public List<KDT_VNACCS_TEANguoiXNK> NguoiXNKCollection
         {
             set { this._ListNguoiXNK = value; }
             get { return this._ListNguoiXNK; }
         }
         public List<KDT_VNACCS_TEADieuChinh> DieuChinhCollection
         {
             set { this._ListDieuChinh = value; }
             get { return this._ListDieuChinh; }
         }
         public bool InsertUpdateFul()
         {
             bool ret;
             SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
             using (SqlConnection connection = (SqlConnection)db.CreateConnection())
             {
                 connection.Open();
                 SqlTransaction transaction = connection.BeginTransaction();
                 try
                 {
                     if (this.ID == 0)
                     {
                        // this.TrangThaiXuLy = "1";
                         this.ID = this.Insert();
                     }
                     else
                         this.Update();

                     // luu hang to khai
                     foreach (KDT_VNACCS_TEA_HangHoa item in this.HangCollection)
                     {
                         if (item.ID == 0)
                         {
                             item.Master_ID = this.ID;
                             item.ID = item.Insert();
                         }
                         else
                         {
                             item.Update();
                         }
                     }
                     foreach (KDT_VNACCS_TEADieuChinh item in this.DieuChinhCollection)
                     {
                         if (item.ID == 0)
                         {
                             item.Master_ID = this.ID;
                             item.ID = item.Insert();
                         }
                         else
                         {
                             item.Update();
                         }
                     }
                     foreach (KDT_VNACCS_TEANguoiXNK item in this.NguoiXNKCollection)
                     {
                         if (item.ID == 0)
                         {
                             item.Master_ID = this.ID;
                             item.ID = item.Insert();
                         }
                         else
                         {
                             item.Update();
                         }
                     }
                   

                  
                     ret = true;
                 }
                 catch (Exception ex)
                 {
                     transaction.Rollback();
                     this.ID = 0;
                     throw new Exception(ex.Message);
                 }
                 finally
                 {
                     connection.Close();
                 }
             }
             return ret;
         }
    }
}
