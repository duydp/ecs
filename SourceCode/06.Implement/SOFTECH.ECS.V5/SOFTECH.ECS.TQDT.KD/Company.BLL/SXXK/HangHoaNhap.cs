using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KD.BLL.SXXK
{
    public partial class HangHoaNhap
    {
        public DataSet SearchBy_MaHaiQuan(string maHaiQuan)
        {
            string where = string.Format("MaHaiQuan = '{0}'", maHaiQuan.PadRight(6));
            return this.SelectDynamic(where, "Ma");
        }

        //public DataSet WS_GetDanhSachDaDangKy(string maHaiQuan, string maDoanhNghiep)
        //{
        //    SXXKService service = new SXXKService();
        //    return service.NguyenPhuLieu_GetDanhSach(maHaiQuan, maDoanhNghiep);
        //}

        public bool UpdateRegistedToDatabase(string maHaiQuan, string maDoanhNghiep, DataSet ds)
        {
            HangHoaNhapCollection collection = new HangHoaNhapCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HangHoaNhap npl = new HangHoaNhap();
                npl.MaHaiQuan = maHaiQuan;
                npl.MaDoanhNghiep = maDoanhNghiep;
                npl.Ma = row["Ma"].ToString();
                npl.Ten = row["Ten"].ToString();
                npl.MaHS = row["MaHS"].ToString().Trim();
                int k = npl.MaHS.Length;
                if (npl.MaHS.Length < 10)
                {
                    for (int i = 1; i <= 10 - k; ++i)
                        npl.MaHS += "0";
                }
                npl.DVT_ID = row["DVT_ID"].ToString();
                collection.Add(npl);
            }
            HangHoaNhap NPL = new HangHoaNhap();
            return NPL.InsertUpdate(collection);
        }

        public bool UpdateRegistedToDatabaseMoi(string maHaiQuan, string maDoanhNghiep, DataSet ds)
        {
            //NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HangHoaNhap npl = new HangHoaNhap();
                npl.MaHaiQuan = maHaiQuan;
                npl.MaDoanhNghiep = maDoanhNghiep;
                npl.Ma = row["Ma"].ToString();
                npl.Ten = row["Ten"].ToString();
                npl.MaHS = row["MaHS"].ToString().Trim();
                int k = npl.MaHS.Length;
                if (npl.MaHS.Length < 10)
                {
                    for (int i = 1; i <= 10 - k; ++i)
                        npl.MaHS += "0";
                }
                npl.DVT_ID = row["DVT_ID"].ToString();
                try
                {
                    npl.Insert();
                }
                catch { }
            }
            return true;
        }

        public void InsertUpdateFull(string MaCu)
        {
             SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
             using (SqlConnection connection = (SqlConnection)db.CreateConnection())
             {
                 connection.Open();
                 SqlTransaction transaction = connection.BeginTransaction();
                 try
                 {
                     if (MaCu != "")
                     {
                         HangHoaNhap hang = new HangHoaNhap();
                         hang.Ma = MaCu;
                         hang.MaDoanhNghiep = this.MaDoanhNghiep;
                         hang.DeleteTransaction(transaction);
                     }
                     this.InsertTransaction(transaction);
                     if (MaCu != "")
                     {
                         string sql = "update t_KDT_HangMauDich set MaPhu=@MaPhu where MaPhu=@MaCu and MaPhu in (select MaPhu from t_KDT_HangMauDich hang inner join t_KDT_ToKhaiMauDich tk on tk.id=hang.TKMD_ID where MaDoanhNghiep=@MaDoanhNghiep)";
                         DbCommand command = db.GetSqlStringCommand(sql);
                         db.AddInParameter(command, "@MaPhu", SqlDbType.VarChar, this.Ma);
                         db.AddInParameter(command, "@MaCu", SqlDbType.VarChar, MaCu);
                         db.AddInParameter(command, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                         db.ExecuteNonQuery(command, transaction);

                         sql = "update t_SXXK_HangMauDich set MaPhu=@MaPhu where MaPhu=@MaCu and MaPhu in (select MaPhu from t_SXXK_HangMauDich hang inner join t_SXXK_ToKhaiMauDich tk on tk.MaHaiQuan=hang.MaHaiQuan and  tk.SoToKhai=hang.SoToKhai and  tk.MaLoaiHinh=hang.MaLoaiHinh and  tk.NamDangKy=hang.NamDangKy where MaDoanhNghiep=@MaDoanhNghiep)";
                         command = db.GetSqlStringCommand(sql);
                         db.AddInParameter(command, "@MaPhu", SqlDbType.VarChar, this.Ma);
                         db.AddInParameter(command, "@MaCu", SqlDbType.VarChar, MaCu);
                         db.AddInParameter(command, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                         db.ExecuteNonQuery(command, transaction);
                     }
                     transaction.Commit();
                 }
                 catch (Exception ex)
                 {
                     transaction.Rollback();
                     throw ex;
                 }
                 finally
                 {
                     connection.Close();
                 }
             }
        }

        public bool CheckTKMDUserMa(string MaDoanhNghiep)
        {

            string sql = "select * from  t_KDT_HangMauDich where MaPhu=@MaPhu and MaPhu in (select MaPhu from t_KDT_HangMauDich hang inner join t_KDT_ToKhaiMauDich tk on tk.id=hang.TKMD_ID where MaDoanhNghiep=@MaDoanhNghiep)";
            DbCommand command = db.GetSqlStringCommand(sql);
            db.AddInParameter(command, "@MaPhu", SqlDbType.VarChar, this.Ma);
            db.AddInParameter(command, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            object o = db.ExecuteScalar(command);
            if (o != null)
                return true;

            sql = "select * from  t_SXXK_HangMauDich where MaPhu=@MaPhu and MaPhu in (select MaPhu from t_SXXK_HangMauDich hang inner join t_SXXK_ToKhaiMauDich tk on tk.MaHaiQuan=hang.MaHaiQuan and  tk.SoToKhai=hang.SoToKhai and  tk.MaLoaiHinh=hang.MaLoaiHinh and  tk.NamDangKy=hang.NamDangKy where MaDoanhNghiep=@MaDoanhNghiep)";
            command = db.GetSqlStringCommand(sql);
            db.AddInParameter(command, "@MaPhu", SqlDbType.VarChar, this.Ma);
            db.AddInParameter(command, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            o = db.ExecuteScalar(command);
            if (o != null)
                return true;
            return false;
        }
    }
}