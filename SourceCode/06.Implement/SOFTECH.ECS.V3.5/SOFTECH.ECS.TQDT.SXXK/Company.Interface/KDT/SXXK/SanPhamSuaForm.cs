﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL;
using Company.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;


namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamSuaForm : BaseForm
    {
        private SanPhamCollection spCollection = new SanPhamCollection();
        public SanPhamDangKy SPDangKy = new SanPhamDangKy();
        public SanPhamDangKySUA spdkSUA = new SanPhamDangKySUA();
        public SanPhamSUA spSUA = new SanPhamSUA();
        public long masterId;
        public long masterIdSUA;
        public List<SanPhamSUA> listSpSUA = new List<SanPhamSUA>();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;

        public SanPhamSuaForm()
        {
            InitializeComponent();
        }

        private void SanPhamSuaForm_Load(object sender, EventArgs e)
        {
            BindData();
            //Dữ liệu HS và DVT
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dtHS = MaHS.SelectAll();
            GridEXColumn columnHS = dgList.RootTable.Columns["MaHS"];
            GridEXValueListItemCollection valueListHS = columnHS.ValueList;
            valueListHS.PopulateValueList(dtHS.DefaultView, "HS10So", "HS10So");
            columnHS.CompareTarget = Janus.Windows.GridEX.ColumnCompareTarget.Text;

            //Load dữ liệu DVT
            //cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            GridEXColumn column = dgList.RootTable.Columns["DVT_ID"];
            GridEXValueListItemCollection valueList = column.ValueList;
            valueList.PopulateValueList(_DonViTinh.DefaultView, "ID", "Ten");
            column.CompareTarget = Janus.Windows.GridEX.ColumnCompareTarget.Text;

            if (spdkSUA.SoTiepNhan > 0)
            {
                txtSoTiepNhan.Text = this.spdkSUA.SoTiepNhan.ToString("N0");
                ccNgayTiepNhan.Value = this.spdkSUA.NgayTiepNhan;
                ccNgayTiepNhan.Text = this.spdkSUA.NgayTiepNhan.ToShortDateString();
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                btnGhi.Enabled = false;
                btnAddNew.Enabled = false;
            }
            this.setCommandStatus();
        }

        public void BindData()
        {
            listSpSUA = (List<SanPhamSUA>)SanPhamSUA.SelectCollectionBy_Master_IDSUA(masterIdSUA);
            dgList.DataSource = listSpSUA;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            SelectSanPhamSUAForm f = new SelectSanPhamSUAForm();
            f.SPDangKy = SPDangKy;
            f.masterId = SPDangKy.ID;
            f.ShowDialog(this);
            if (f.listSpSUA.Count > 0)
            {
                foreach (SanPhamSUA sp in f.listSpSUA)
                {
                    bool ok = false;
                    foreach (SanPhamSUA sp_ in this.listSpSUA)
                    {
                        if (sp_.Ma == sp.Ma)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                        this.listSpSUA.Add(sp);
                }
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (this.listSpSUA.Count == 0)
            {
                ShowMessage("Chưa chọn sản phẩm", false);
                return;
            }
            save();

            //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
            try
            {
                string where = "1 = 1";
                where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", spdkSUA.ID, LoaiKhaiBao.SanPham);
                List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                if (listLog.Count > 0)
                {
                    long idLog = listLog[0].IDLog;
                    string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                    long idDK = listLog[0].ID_DK;
                    string guidstr = spdkSUA.GUIDSTR;
                    string userKhaiBao = listLog[0].UserNameKhaiBao;
                    DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                    string userSuaDoi = GlobalSettings.UserLog;
                    DateTime ngaySuaDoi = DateTime.Now;
                    string ghiChu = listLog[0].GhiChu;
                    bool isDelete = listLog[0].IsDelete;
                    Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }
        private void save()
        {

            spdkSUA.SoTiepNhan = SPDangKy.SoTiepNhan;
            spdkSUA.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            spdkSUA.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            spdkSUA.TrangThaiXuLy = -1;
            spdkSUA.ActionStatus = -1;
            spdkSUA.NgayTiepNhan = DateTime.Parse("01/01/1900");
            spdkSUA.IDSPDK = SPDangKy.ID;
            try
            {
                long id = 0;
                if (spdkSUA.ID == 0)
                    id = spdkSUA.Insert();
                else
                    spdkSUA.InsertUpdate();
                id = id != 0 ? id : spdkSUA.ID;
                foreach (SanPhamSUA sp in this.listSpSUA)
                {
                    sp.Master_IDSUA = id;
                    sp.InsertUpdate();
                }
                this.listSpSUA = (List<SanPhamSUA>)SanPhamSUA.SelectCollectionBy_Master_IDSUA(id);
                ShowMessage("Lưu thành công.", false);

            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
                return;
            }
        }
        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (spdkSUA.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                this.SendV3();
            else
                send();
        }

        private void send()
        {
            MsgSend sendXML = new MsgSend();
            string password = "";
            sendXML.LoaiHS = "SP_SUA";
            sendXML.master_id = spdkSUA.ID;
            if (sendXML.Load())
            {
                MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (SanPhamSUA.SelectCollectionBy_Master_IDSUA(spdkSUA.ID).Count == 0)
                {
                    MLMessages("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }

                //string[] danhsachDaDangKy = new string[0];


                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                xmlCurrent = spdkSUA.WSSendXMLSuaSP(password);
                this.Cursor = Cursors.Default;

                sendXML = new MsgSend();
                sendXML.LoaiHS = "SP_SUA";
                sendXML.master_id = spdkSUA.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true);
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                #region Ghi lỗi
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo sửa SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                #endregion

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "SP_SUA";
                sendXML.master_id = spdkSUA.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = spdkSUA.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        LayPhanHoi(pass);
                    }
                    return;
                }

                if (sendXML.func == 1)
                {
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + spdkSUA.SoTiepNhan, "MSG_SEN05", "" + spdkSUA.SoTiepNhan, false);
                    //this.search();
                    txtSoTiepNhan.Text = this.spdkSUA.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.spdkSUA.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.spdkSUA.NgayTiepNhan.ToShortDateString();
                    btnKhaiBao.Enabled = false;
                }
                else if (sendXML.func == 2)
                {
                    if (spdkSUA.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN18", "", false);
                        lblTrangThai.Text = "Đã duyệt";
                        //xoa thông tin msg nay trong database
                        sendXML.Delete();
                    }
                    else if (spdkSUA.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        lblTrangThai.Text = "Chờ duyệt";
                        if (spdkSUA.PhanLuong != "")
                        {
                            string tenluong = "Xanh";
                            if (spdkSUA.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                                tenluong = "Vàng";
                            else if (spdkSUA.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                                tenluong = "Đỏ";
                            MLMessages("Sản phẩm đã được phân luồng: " + tenluong + "\n" + spdkSUA.HUONGDAN, "MSG_SEN08", "", false);
                        }
                        else
                            MLMessages("Hải quan chưa xử lý!", "MSG_SEN08", "", false);
                    }
                    else if (spdkSUA.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        lblTrangThai.Text = "Không phê duyệt";
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN09", "", false);
                        sendXML.Delete();
                    }
                }

                //xoa thông tin msg nay trong database
                //sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\n", "MSG_SEN19", "", true);

                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                    }
                    #endregion FPTService
                }
                #region Ghi lỗi
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo sửa SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                #endregion
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
            this.FeedBackV3();
            //else
            //{
            //    string password = "";
            //    WSForm wsForm = new WSForm();
            //    try
            //    {
            //        if (GlobalSettings.PassWordDT == "")
            //        {
            //            wsForm.ShowDialog(this);
            //            if (!wsForm.IsReady) return;
            //        }
            //        this.Cursor = Cursors.WaitCursor;
            //        password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

            //        if (this.spdkSUA.SoTiepNhan > 0)
            //        {
            //            string xmlCurrent = "";
            //            MsgSend sendXML = new MsgSend();
            //            sendXML.LoaiHS = "SP_SUA";
            //            sendXML.master_id = spdkSUA.ID;
            //            if (!sendXML.Load())
            //            {
            //                xmlCurrent = spdkSUA.WSDownLoad(password);
            //                sendXML.msg = xmlCurrent;
            //                xmlCurrent = "";
            //                sendXML.LoaiHS = "SP_SUA";
            //                sendXML.master_id = spdkSUA.ID;
            //            }
            //            sendXML.func = 2;
            //            sendXML.InsertUpdate();

            //            this.Cursor = Cursors.Default;
            //        }
            //        LayPhanHoi(password);
            //    }
            //    catch (Exception ex)
            //    {
            //        this.Cursor = Cursors.Default;
            //        {
            //            #region FPTService
            //            string[] msg = ex.Message.Split('|');
            //            if (msg.Length == 2)
            //            {
            //                if (msg[1] == "DOTNET_LEVEL")
            //                {
            //                    // if (ShowMessage("Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
            //                    MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true);
            //                }
            //                else
            //                {
            //                    // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
            //                    MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
            //                    if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
            //                    {
            //                        GlobalSettings.PassWordDT = "";
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                //ShowMessage("Xảy ra lỗi không xác định.", false);
            //                MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
            //                GlobalSettings.PassWordDT = "";
            //            }
            //            #endregion FPTService
            //        }
            //        #region Ghi lỗi
            //        StreamWriter write = File.AppendText("Error.txt");
            //        write.WriteLine("--------------------------------");
            //        write.WriteLine("Lỗi khi lấy phản hồi sửa SP. Thời gian thực hiện : " + DateTime.Now.ToString());
            //        write.WriteLine(ex.StackTrace);
            //        write.WriteLine("Lỗi là : ");
            //        write.WriteLine(ex.Message);
            //        write.WriteLine("--------------------------------");
            //        write.Flush();
            //        write.Close();
            //        #endregion

            //    }
            //    finally
            //    {
            //        this.Cursor = Cursors.Default;
            //    }
            //}
        }

        private void btnKetQuaXyLy_Click(object sender, EventArgs e)
        {
            if (spdkSUA.ID != 0)
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = spdkSUA.ID;
                form.DeclarationIssuer = DeclarationIssuer.SXXK_SP;
                form.ShowDialog(this);
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void dgList_EditingCell(object sender, EditingCellEventArgs e)
        {

            //     If you want to implement auto Complete function, you can try something as following:
            // // add a datagridview textbox column to datagridview
            // private void Form2_Load(object sender, EventArgs e)
            // {
            //     dataGridView1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(dataGridView1_EditingControlShowing);

            // }
            // void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
            // {
            //     TextBox txt = e.Control as TextBox;
            //     if (dataGridView1.CurrentCell.ColumnIndex == 0 && txt != null)
            //     {
            //         txt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //         txt.AutoCompleteSource = AutoCompleteSource.CustomSource;
            //         txt.AutoCompleteCustomSource.AddRange(new string[] { "aa", "a1", "cc", "ac", "ca" });
            //     }

            //     else if (dataGridView1.CurrentCell.ColumnIndex != 0 && txt != null)
            //     {
            //         txt.AutoCompleteMode = AutoCompleteMode.None;
            //     }
            //}

            //try
            //{
            //    //TextBox txt = (TextBox)sender;
            //    GridEXRow row =  dgList.SelectedItems[0].GetRow();
            //    GridEXCellCollection cells = row.Cells;
            //    GridEXCell cell = cells[3];
            //    object tmpCell = new object();
            //    tmpCell = cell;
            //    TextBox txt = (TextBox)tmpCell;

            //    MessageBox.Show("1........" + cells[3].Text);
            //}catch(Exception ex){
            //    MessageBox.Show(ex.ToString());
            //}
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //TextBox txt = (TextBox)e.Row.Cells["MaHS"];

        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count > 0)
            {
                if (MLMessages("Bạn có muốn xóa các sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            if (this.spdkSUA.TrangThaiXuLy.ToString() == "-1")//Chưa khai báo
                            {
                                SanPhamSUA spSUA = (SanPhamSUA)i.GetRow().DataRow;
                                this.listSpSUA.Remove(spSUA);
                            }
                            else
                                MLMessages("Đã gửi thông tin tới hải quan, không thể xóa được!", "MSG_NPL03", "" + i.Position + "", false);
                        }
                    }
                    try { dgList.Refetch(); }
                    catch { dgList.Refresh(); }
                }
            }
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }
        //---------------------------------------------------------------------------------
        #region Send V3 Create by LANNT
        private void SendV3()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.SanPham;
            sendXML.master_id = spdkSUA.ID;
            if (sendXML.Load())
            {
                MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                return;
            }
            try
            {
                if (SanPhamSUA.SelectCollectionBy_Master_IDSUA(spdkSUA.ID).Count == 0)
                {
                    MLMessages("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }
                else
                {
                    spdkSUA.GUIDSTR = Guid.NewGuid().ToString();
                    Company.KDT.SHARE.Components.SXXK_SanPham sanpham = Company.BLL.DataTransferObjectMapper.Mapper.ToDaTaTransferObject_SXXK_SP_SUA(spdkSUA, listSpSUA);
                    ObjectSend msgSend = new ObjectSend(
                                   new NameBase()
                                   {
                                       Name = GlobalSettings.TEN_DON_VI,
                                       Identity = spdkSUA.MaDoanhNghiep,
                                   }
                                     , new NameBase()
                                     {
                                         Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spdkSUA.MaHaiQuan),
                                         Identity = spdkSUA.MaHaiQuan,
                                     }
                                  ,
                                    new SubjectBase()
                                    {
                                        Type = DeclarationIssuer.SXXK_SP,
                                        Function = DeclarationFunction.SUA,
                                        Reference = spdkSUA.GUIDSTR,
                                    }
                                    ,
                                    sanpham);
                    spdkSUA.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend)
                    {
                        sendForm.Message.XmlSaveMessage(spdkSUA.ID, MessageTitle.KhaiBaoSanPham);
                        sendXML.func = 1;
                        //  sendXML.msg = msgSend;
                        sendXML.InsertUpdate();
                        #region cập nhật lại sản phẩm
                        //cập nhật lại sản phẩm gốc
                        List<SanPhamSUA> spSUAList = (List<SanPhamSUA>)SanPhamSUA.SelectCollectionBy_Master_IDSUA(spdkSUA.ID);
                        SanPham spGoc = new SanPham();
                        foreach (SanPhamSUA spSUA in spSUAList)
                        {
                            //Cập nhật vào bảng t_KDT_SXXK_SanPham
                            spGoc = SanPham.Load(spSUA.IDSP);
                            spGoc.Ten = spSUA.Ten;
                            spGoc.MaHS = spSUA.MaHS;
                            spGoc.DVT_ID = spSUA.DVT_ID;
                            spGoc.Update();
                            // Cập nhật vào bảng t_SXXK_SanPham
                            BLL.SXXK.SanPham spSXXK = BLL.SXXK.SanPham.getSanPham(spdkSUA.MaHaiQuan, spdkSUA.MaDoanhNghiep, spGoc.Ma);
                            if (spSXXK != null)
                            {
                                spSXXK.Ten = spSUA.Ten;
                                spSXXK.MaHS = spSUA.MaHS;
                                spSXXK.DVT_ID = spSUA.DVT_ID;
                                spSXXK.Update();
                            }
                        }
                        #endregion
                        FeedBackV3();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
            {
                spdkSUA.Update();
                setCommandStatus();
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = SingleMessage.GetErrorContent(feedbackContent);
                            spdkSUA.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(spdkSUA.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case DeclarationFunction.CHUA_XU_LY:
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = feedbackContent.AdditionalInformations[0].Content.Text.Split('/');
                            noidung = "\r\nSố Tiếp Nhận: " + ketqua[0] + "\r\nNăm Ðăng Ký: " + ketqua[1] + "\r\nNgày tiếp nhận: " + feedbackContent.Issue;
                            noidung = "Cấp số tiếp nhận khai báo sửa\r\n" + noidung;

                            spdkSUA.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;

                            spdkSUA.SoTiepNhan = long.Parse(ketqua[0].Trim());
                            spdkSUA.NamDK = short.Parse(ketqua[1].Trim());
                            spdkSUA.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);
                            e.FeedBackMessage.XmlSaveMessage(spdkSUA.ID, MessageTitle.KhaiBaoLayPhanHoiSanPham, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            e.FeedBackMessage.XmlSaveMessage(spdkSUA.ID, MessageTitle.KhaiBaoHQDuyet, noidung);
                            spdkSUA.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(spdkSUA.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", spdkSUA.MaHaiQuan, e);
                                break;
                            }


                    }
                    if (isDeleteMsg)
                        SingleMessage.DeleteMsgSend(LoaiKhaiBao.NguyenPhuLieu, spdkSUA.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        spdkSUA.Update();
                    msgInfor = noidung;
                }
                else
                {
                    SingleMessage.DeleteMsgSend(LoaiKhaiBao.SanPham, spdkSUA.ID);
                    SingleMessage.SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", spdkSUA.MaHaiQuan, e);
                }
            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SingleMessage.SendMail(msgTitle, spdkSUA.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
        }
        private void FeedBackV3()
        {
            string reference = spdkSUA.GUIDSTR;
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_SP,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_SP,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = spdkSUA.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spdkSUA.MaHaiQuan.Trim()),
                                                  Identity = spdkSUA.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (spdkSUA.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (spdkSUA.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) setCommandStatus();
                }
            }
        }
        private void setCommandStatus()
        {
            if (spdkSUA.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || spdkSUA.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                btnAddNew.Enabled = btnGhi.Enabled = btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = false;
            }
            else if (spdkSUA.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
            {
                btnAddNew.Enabled = btnGhi.Enabled = btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
            }
            else if (spdkSUA.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnAddNew.Enabled = btnGhi.Enabled = btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                txtSoTiepNhan.Text = spdkSUA.SoTiepNhan.ToString();
                ccNgayTiepNhan.Value = spdkSUA.NgayTiepNhan;
            }
            else
            {
                btnAddNew.Enabled = btnGhi.Enabled = btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            this.SetLbStatus();
        }
        #endregion
        //Label Trạng thái
        private void SetLbStatus()
        {
            switch (spdkSUA.TrangThaiXuLy)
            {
                case 0:
                    lblTrangThai.Text = "Chờ duyệt";
                    break;
                case 1:
                    lblTrangThai.Text = "Đã duyệt";
                    break;
                case -1:
                    lblTrangThai.Text = "Chưa khai báo";
                    break;
                case 2:
                    lblTrangThai.Text = "Không phê duyệt";
                    break;
                case -3:
                    lblTrangThai.Text = "Đã khai báo nhưng chưa có phản hồi";
                    break;
            }
        }
    }
}
