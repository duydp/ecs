﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class VanDon
    {
        public static string ConfigVanDon( ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            string doc = "";
            string pathTKMD = "../../../SOFTECH.ECS.V3.Components/XML/KD/VanDon.xml";
            XDocument docXML = XDocument.Load(pathTKMD);
            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tkmd != null)
                {
                    IList<VanDon> lvandon = VanDon.SelectCollectionBy_TKMD_ID(tkmd.ID);
                    if (lvandon != null && lvandon.Count > 0)
                    {
                        Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                        XElement BillOfLadings = Root.Element(NodeVanDon.BillOfLadings);
                        XElement BillOfLading = BillOfLadings.Element(NodeVanDon.BillOfLading);
                        if (lvandon.Count == 1)
                        {
                            BillOfLading = Config_BillOfLading(BillOfLading, lvandon[0]);
                        }
                        else if (lvandon.Count > 1)
                        {
                            XElement tempBillOfLading = new XElement(BillOfLading);
                            BillOfLading = Config_BillOfLading(BillOfLading, lvandon[0]);
                            for (int i = 1; i < lvandon.Count; i++)
                            {
                                XElement newBillOfLading = new XElement(tempBillOfLading);
                                BillOfLadings.Add(Config_BillOfLading(newBillOfLading, lvandon[i]));
                            }
                        }
                    }
                    else
                    {
                        return doc; //return "";
                    }
                }
                else
                {
                    return doc; //return "";
                }
            }
            //docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();
            return doc;
        }
        public static XElement Config_BillOfLading(XElement BillOfLading, VanDon vandon)
        {
            if (vandon != null)
            {
                XElement TransportEquipment = BillOfLading.Element(NodeVanDon.Consignment).Element(NodeVanDon.TransportEquipment);
                XElement ConsignmentItem = BillOfLading.Element(NodeVanDon.Consignment).Element(NodeVanDon.ConsignmentItem);
                IList<Container> lContainer = Container.SelectCollectionBy_VanDon_ID(vandon.ID);
                XElement TempConsignmentItem = new XElement(ConsignmentItem);
                XElement TempTransportEquipment = new XElement(TransportEquipment);
                ToKhaiMauDich tk = ToKhaiMauDich.Load(vandon.TKMD_ID);
                foreach (XElement element in BillOfLading.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeVanDon.reference.ToUpper()))
                        element.SetValue(vandon.SoVanDon);
                    else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.issue.ToUpper()))
                        element.SetValue(vandon.NgayVanDon);
                    else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.issueLocation.ToUpper()))
                    {
                        if (tk != null)
                        {
                            IList<CO> co = CO.SelectCollectionBy_TKMD_ID(tk.ID);
                            if (co != null)
                            {
                                element.SetValue(co[0].NuocCapCO);
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.BorderTransportMeans.ToUpper()))
                    {
                        foreach (XElement BorderTransportMeans_Child in element.Elements())
                        {
                            if (BorderTransportMeans_Child.Name.ToString().ToUpper().Equals(NodeVanDon.identity.ToUpper()))
                                BorderTransportMeans_Child.SetValue(vandon.SoHieuPTVT);
                            else if (BorderTransportMeans_Child.Name.ToString().ToUpper().Equals(NodeVanDon.identification.ToUpper()))
                                BorderTransportMeans_Child.SetValue(vandon.TenPTVT);
                            else if (BorderTransportMeans_Child.Name.ToString().ToUpper().Equals(NodeVanDon.journey.ToUpper()))
                                BorderTransportMeans_Child.SetValue(vandon.SoHieuChuyenDi);
                            else if (BorderTransportMeans_Child.Name.ToString().ToUpper().Equals(NodeVanDon.modeAndType.ToUpper()))
                            {
                                if (tk != null)
                                {
                                    PhuongThucVanTai pt = PhuongThucVanTai.Load(tk.PTVT_ID);
                                    if (pt != null && pt.Ten!="")
                                    {
                                        BorderTransportMeans_Child.SetValue(pt.Ten);
                                    }
                                }
                            }
                            else if (BorderTransportMeans_Child.Name.ToString().ToUpper().Equals(NodeVanDon.departure.ToUpper()))
                                BorderTransportMeans_Child.SetValue(vandon.NgayKhoiHanh);
                            else if (BorderTransportMeans_Child.Name.ToString().ToUpper().Equals(NodeVanDon.registrationNationality.ToUpper()))
                                BorderTransportMeans_Child.SetValue(vandon.QuocTichPTVT);
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.Carrier.ToUpper()))
                    {
                        foreach (XElement Carrier_Child in element.Elements())
                        {
                            if (Carrier_Child.Name.ToString().ToUpper().Equals(NodeVanDon.name.ToUpper()))
                                Carrier_Child.SetValue(vandon.TenHangVT);
                            else if (Carrier_Child.Name.ToString().ToUpper().Equals(NodeVanDon.identity.ToUpper()))
                                Carrier_Child.SetValue(vandon.MaHangVT);
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.Consignment.ToUpper()))
                    {
                        XElement Consignment = element;
                        bool checkTransportEquipment = false;
                        bool checkConsignmentItem = false;
                        foreach (XElement Consignment_Child in element.Elements())
                        {
                            if (Consignment_Child.Name.ToString().ToUpper().Equals(NodeVanDon.Consignor.ToUpper()))
                            {
                                foreach (XElement Consignor_Child in Consignment_Child.Elements())
                                {
                                    if (Consignor_Child.Name.ToString().ToUpper().Equals(NodeVanDon.name.ToUpper()))
                                        Consignor_Child.SetValue(vandon.TenNguoiGiaoHang);
                                    else if (Consignor_Child.Name.ToString().ToUpper().Equals(NodeVanDon.identity.ToUpper()))
                                        Consignor_Child.SetValue(vandon.MaNguoiGiaoHang);
                                }
                            }
                            else if (Consignment_Child.Name.ToString().ToUpper().Equals(NodeVanDon.Consignee.ToUpper()))
                            {
                                foreach (XElement Consignee_Child in Consignment_Child.Elements())
                                {
                                    if (Consignee_Child.Name.ToString().ToUpper().Equals(NodeVanDon.name.ToUpper()))
                                        Consignee_Child.SetValue(vandon.TenNguoiNhanHang);
                                    else if (Consignee_Child.Name.ToString().ToUpper().Equals(NodeVanDon.identity.ToUpper()))
                                        Consignee_Child.SetValue(vandon.MaNguoiNhanHang);
                                }
                            }
                            else if (Consignment_Child.Name.ToString().ToUpper().Equals(NodeVanDon.NotifyParty.ToUpper()))
                            {
                                foreach (XElement NotifyParty_Child in Consignment_Child.Elements())
                                {
                                    if (NotifyParty_Child.Name.ToString().ToUpper().Equals(NodeVanDon.name.ToUpper()))
                                        NotifyParty_Child.SetValue("");// ?? Not Found
                                    else if (NotifyParty_Child.Name.ToString().ToUpper().Equals(NodeVanDon.identity.ToUpper()))
                                        NotifyParty_Child.SetValue("");// ?? Not Found
                                }
                            }
                            else if (Consignment_Child.Name.ToString().ToUpper().Equals(NodeVanDon.LoadingLocation.ToUpper()))
                            {
                                foreach (XElement LoadingLocation_Child in Consignment_Child.Elements())
                                {
                                    if (LoadingLocation_Child.Name.ToString().ToUpper().Equals(NodeVanDon.name.ToUpper()))
                                        LoadingLocation_Child.SetValue(vandon.TenCangXepHang);
                                    else if (LoadingLocation_Child.Name.ToString().ToUpper().Equals(NodeVanDon.code.ToUpper()))
                                        LoadingLocation_Child.SetValue("");
                                    else if (LoadingLocation_Child.Name.ToString().ToUpper().Equals(NodeVanDon.loading.ToUpper()))
                                        LoadingLocation_Child.SetValue(vandon.NgayKhoiHanh);
                                }
                            }
                            else if (Consignment_Child.Name.ToString().ToUpper().Equals(NodeVanDon.UnloadingLocation.ToUpper()))
                            {
                                foreach (XElement UnloadingLocation_Child in Consignment_Child.Elements())
                                {
                                    if (UnloadingLocation_Child.Name.ToString().ToUpper().Equals(NodeVanDon.name.ToUpper()))
                                        UnloadingLocation_Child.SetValue(vandon.TenCangDoHang);
                                    else if (UnloadingLocation_Child.Name.ToString().ToUpper().Equals(NodeVanDon.code.ToUpper()))
                                        UnloadingLocation_Child.SetValue("");
                                    else if (UnloadingLocation_Child.Name.ToString().ToUpper().Equals(NodeVanDon.arrival.ToUpper()))
                                        UnloadingLocation_Child.SetValue(vandon.NgayDenPTVT);
                                }
                            }
                            else if (Consignment_Child.Name.ToString().ToUpper().Equals(NodeVanDon.DeliveryDestination.ToUpper()))
                            {
                                foreach (XElement DeliveryDestination_Child in Consignment_Child.Elements())
                                {
                                    if (DeliveryDestination_Child.Name.ToString().ToUpper().Equals(NodeVanDon.line.ToUpper()))
                                        DeliveryDestination_Child.SetValue(vandon.DiaDiemGiaoHang);//??? Cảng giao hàng/Cảng đích
                                }
                            }
                            else if (Consignment_Child.Name.ToString().ToUpper().Equals(NodeVanDon.ConsignmentItemPackaging.ToUpper()))
                            {
                                foreach (XElement ConsignmentItemPackaging_Child in Consignment_Child.Elements())
                                {
                                    if (ConsignmentItemPackaging_Child.Name.ToString().ToUpper().Equals(NodeVanDon.quantity.ToUpper()))
                                        ConsignmentItemPackaging_Child.SetValue("");//??? Not Found
                                    else if (ConsignmentItemPackaging_Child.Name.ToString().ToUpper().Equals(NodeVanDon.type.ToUpper()))
                                        ConsignmentItemPackaging_Child.SetValue("");//??? Not Found
                                }
                            }
                            else if (Consignment_Child.Name.ToString().ToUpper().Equals(NodeVanDon.TransportEquipment.ToUpper()))
                            {
                                if (checkTransportEquipment == false)
                                {
                                    TransportEquipment = Consignment_Child;
                                    if (lContainer != null && lContainer.Count > 0)
                                    {
                                        if (TransportEquipment != null)
                                        {
                                            if (lContainer.Count == 1)
                                            { //1 container co 1 hang mau dich
                                                TransportEquipment = config_TransportEquipment(TransportEquipment, lContainer[0]);
                                            }
                                            else if (lContainer.Count > 1)
                                            {
                                                TransportEquipment = config_TransportEquipment(TransportEquipment, lContainer[0]);
                                                for (int i = 1; i < lContainer.Count; i++)
                                                {
                                                    XElement newTransportEquipment = new XElement(TempTransportEquipment);
                                                    config_TransportEquipment(newTransportEquipment, lContainer[i]);
                                                    //TransportEquipment.Add(newTransportEquipment);
                                                    Consignment.Element("TransportEquipment").AddAfterSelf(newTransportEquipment);
                                                }
                                            }
                                        }
                                    }
                                    checkTransportEquipment = true;
                                }
                            }
                            else if (Consignment_Child.Name.ToString().ToUpper().Equals(NodeVanDon.ConsignmentItem.ToUpper()))
                            {
                                if (checkConsignmentItem == false)
                                {
                                    ConsignmentItem = Consignment_Child;
                                    if (lContainer != null && lContainer.Count > 0)
                                    {
                                        if (ConsignmentItem != null)
                                        {
                                            if (lContainer.Count == 1)
                                            { //1 container co 1 hang mau dich
                                                ConsignmentItem = config_ConsignmentItem(ConsignmentItem, lContainer[0]);
                                            }
                                            else if (lContainer.Count > 1)
                                            {
                                                ConsignmentItem = config_ConsignmentItem(ConsignmentItem, lContainer[0]);
                                                for (int i = 1; i < lContainer.Count; i++)
                                                {
                                                    XElement newConsignmentItem = new XElement(TempConsignmentItem);
                                                    config_ConsignmentItem(newConsignmentItem, lContainer[i]);
                                                    Consignment.Element("ConsignmentItem").AddAfterSelf(newConsignmentItem);
                                                }
                                            }
                                        }
                                    }
                                    checkConsignmentItem = true;
                                }
                            }
                        }
                    }
                }
            }
            return BillOfLading;
        }
        public static XElement config_TransportEquipment(XElement TransportEquipment, Container container)
        {
            if (container != null)
            {
                foreach (XElement element in TransportEquipment.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeVanDon.characteristic.ToUpper()))
                        element.SetValue("");// ?? Not Found
                    else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.fullness.ToUpper()))
                        element.SetValue("");// ?? Not Found
                    else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.seal.ToUpper()))
                        element.SetValue(container.Seal_No);
                    else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.EquipmentIdentification.ToUpper()))
                    {
                        foreach (XElement EquipmentIdentification_Child in element.Elements())
                        {
                            if (EquipmentIdentification_Child.Name.ToString().ToUpper().Equals(NodeVanDon.identification.ToUpper()))
                                EquipmentIdentification_Child.SetValue(container.SoHieu);
                        }
                    }
                }
            }
            return TransportEquipment;
        }
        public static XElement config_ConsignmentItem(XElement ConsignmentItem, Container container)
        {
            if (container != null)
            {
                IList<HangMauDich_Container> lhmd_C = HangMauDich_Container.SelectCollectionBy_Container_ID(container.ID);
                if (lhmd_C != null && lhmd_C.Count > 0)
                {
                    HangMauDich hmd = HangMauDich.Load(lhmd_C[0].HangMauDich_ID);
                    foreach (XElement element in ConsignmentItem.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeVanDon.sequence.ToUpper()))
                            element.SetValue("");// ?? Not Found
                        else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.ConsignmentItemPackaging.ToUpper()))
                        {
                            foreach (XElement ConsignmentItemPackaging_Child in element.Elements())
                            {
                                if (ConsignmentItemPackaging_Child.Name.ToString().ToUpper().Equals(NodeVanDon.quantity.ToUpper()))
                                    ConsignmentItemPackaging_Child.SetValue("");//?? Not Found
                                else if (ConsignmentItemPackaging_Child.Name.ToString().ToUpper().Equals(NodeVanDon.type.ToUpper()))
                                    ConsignmentItemPackaging_Child.SetValue("");//?? Not Found
                                else if (ConsignmentItemPackaging_Child.Name.ToString().ToUpper().Equals(NodeVanDon.markNumber.ToUpper()))
                                    ConsignmentItemPackaging_Child.SetValue("");//?? Not Found
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.Commodity.ToUpper()))
                        {
                                if (hmd != null)
                                {
                                    foreach (XElement Commodity_Child in element.Elements())
                                    {
                                        if (Commodity_Child.Name.ToString().ToUpper().Equals(NodeVanDon.description.ToUpper()))
                                            Commodity_Child.SetValue(hmd.TenHang);
                                        else if (Commodity_Child.Name.ToString().ToUpper().Equals(NodeVanDon.identification.ToUpper()))
                                            Commodity_Child.SetValue(hmd.MaPhu);
                                        else if (Commodity_Child.Name.ToString().ToUpper().Equals(NodeVanDon.tariffClassification.ToUpper()))
                                            Commodity_Child.SetValue(hmd.MaHS);
                                    }
                                }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.GoodMeasure.ToUpper()))
                        {
                                if (hmd != null)
                                {
                                    foreach (XElement GoodMeasure_Child in element.Elements())
                                    {
                                        if (GoodMeasure_Child.Name.ToString().ToUpper().Equals(NodeVanDon.grossMass.ToUpper()))
                                            GoodMeasure_Child.SetValue("");// ?? Not Found
                                        if (GoodMeasure_Child.Name.ToString().ToUpper().Equals(NodeVanDon.measureUnit.ToUpper()))
                                            GoodMeasure_Child.SetValue(hmd.DVT_ID);
                                    }
                                }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeVanDon.EquipmentIdentification.ToUpper()))
                        {
                            foreach (XElement EquipmentIdentification_Child in element.Elements())
                            {
                                if (EquipmentIdentification_Child.Name.ToString().ToUpper().Equals(NodeVanDon.identification.ToUpper()))
                                    EquipmentIdentification_Child.SetValue(container.SoHieu);
                            }
                        }
                    }
                }
            }
            return ConsignmentItem;
        }
    }
}
