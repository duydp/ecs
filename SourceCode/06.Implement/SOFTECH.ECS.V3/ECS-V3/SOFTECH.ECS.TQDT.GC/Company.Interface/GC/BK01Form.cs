﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.Interface.Report.GC;
using Company.Interface.Report;

namespace Company.Interface.GC
{
    public partial class BK01Form : BaseForm
    {
        
        public BK01Form()
        {
            InitializeComponent();            
        }
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private ThanhKhoanHDGCCollection thanhkhoanColl = new ThanhKhoanHDGCCollection();
        private ThanhKhoanHDGC thkhoan = new ThanhKhoanHDGC();
        public DataSet dsBK = new DataSet();              
        private void LoadData()
        {                         
            dsBK = new ThanhKhoanHDGC().SelectDynamic("OldHD_ID =" + this.HD.ID , "STT");
            ////Cảnh báo khi chênh lệch vượt quá 3% :
            //// ct = (tongxuat )*3 /100 ;
            dsBK.Tables[0].Columns.Add("Temp", typeof(string));
            dsBK.Tables[0].Columns.Add("Temp2", typeof(decimal));
            int count = 0;
            int i = 0;
             foreach (DataRow dr in dsBK.Tables[0].Rows)
             {

                 decimal declallow = (Convert.ToDecimal(dr["TongLuongNK"])) * 3 / 100;
                 decimal declreal = Convert.ToDecimal(dr["TongLuongXK"]) - (Convert.ToDecimal(dr["TongLuongNK"]) + Convert.ToDecimal(dr["TongLuongCU"]));
                 decimal deCU = 0;
                 if (declreal > declallow)
                 {
                     deCU = declreal - declallow;
                     dsBK.Tables[0].Rows[i]["Temp"] = deCU + "";
                     dsBK.Tables[0].Rows[i]["Temp2"] = 0;
                     count++;
                 }
                 else
                 {
                     dsBK.Tables[0].Rows[i]["Temp"] = 0 + "";
                     dsBK.Tables[0].Rows[i]["Temp2"] = 1 + "";
                 }
                 i++;
             }
             if (count > 0)
             {
                 showMsg("MSG_WRN14", count);
                 //ShowMessage("Có " + count + " nguyên phụ liệu đã xuất vượt quá 3% cho phép", false);
                 count = 0;
             }
            dgList.DataSource = dsBK.Tables[0];
            
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
 
        }

        private bool KiemTraChenhLech()
            {
                return false;
            }
        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void lblViewRP_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;           
            ReportViewBC06Form f = new ReportViewBC06Form();
            f.HD = this.HD;
            f.dsBK = this.dsBK;
            f.Show();
            this.Cursor = Cursors.Default;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
           
                if (thkhoan.UpdateDataSetFromGrid(dsBK))
                      {
                          //ShowMessage("Cập nhật thành công", false);
                          //Message("MSG_SAV02", "", false);
                          showMsg("MSG_2702001");
                      }
                else
                     {
                           //ShowMessage("Cập nhật không thành công", false);
                         //Message("MSG_SAV01", "", false);
                         showMsg("MSG_2702002");
                     }

                    LoadData();
                    
          
        }

        private void KB06Form_Load(object sender, EventArgs e)
        {
            for (int i = 4; i <= 7; i++)
                dgList.Tables[0].Columns[i].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns[11].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            LoadData();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {


            //decimal declallow = (Convert.ToDecimal(e.Row.Cells["TongLuongNK"].Value)) * 3 / 100;
            //decimal declreal = Convert.ToDecimal(e.Row.Cells["TongLuongXK"].Value) - Convert.ToDecimal(e.Row.Cells["TongLuongNK"].Value);
            //if (declreal > declallow)
            //{
            //    e.Row.Cells["Temp"].Text   = "1";
            //    //e.Row.Cells["Temp2"].Text = "1";
                

            //}
            //else
            //{
            //    e.Row.Cells["Temp"].Text  = "0";
            //    //e.Row.Cells["Temp2"].Text = "c";

            //}
            //dgList.Refresh();
                

        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {
            //dgList.Refresh();
        }

        

      
        
       
     

         
 
    }
}