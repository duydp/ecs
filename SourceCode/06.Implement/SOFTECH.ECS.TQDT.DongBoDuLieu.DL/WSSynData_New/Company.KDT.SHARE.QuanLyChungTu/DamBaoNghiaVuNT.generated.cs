using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class DamBaoNghiaVuNT : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public bool IsDamBao { set; get; }
		public string HinhThuc { set; get; }
		public decimal TriGiaDB { set; get; }
		public DateTime NgayBatDau { set; get; }
		public DateTime NgayKetThuc { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<DamBaoNghiaVuNT> ConvertToCollection(IDataReader reader)
		{
			IList<DamBaoNghiaVuNT> collection = new List<DamBaoNghiaVuNT>();
			while (reader.Read())
			{
				DamBaoNghiaVuNT entity = new DamBaoNghiaVuNT();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsDamBao"))) entity.IsDamBao = reader.GetBoolean(reader.GetOrdinal("IsDamBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThuc"))) entity.HinhThuc = reader.GetString(reader.GetOrdinal("HinhThuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaDB"))) entity.TriGiaDB = reader.GetDecimal(reader.GetOrdinal("TriGiaDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) entity.NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) entity.NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<DamBaoNghiaVuNT> collection, long id)
        {
            foreach (DamBaoNghiaVuNT item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_DamBaoNghiaVuNT VALUES(@TKMD_ID, @IsDamBao, @HinhThuc, @TriGiaDB, @NgayBatDau, @NgayKetThuc)";
            string update = "UPDATE t_KDT_DamBaoNghiaVuNT SET TKMD_ID = @TKMD_ID, IsDamBao = @IsDamBao, HinhThuc = @HinhThuc, TriGiaDB = @TriGiaDB, NgayBatDau = @NgayBatDau, NgayKetThuc = @NgayKetThuc WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_DamBaoNghiaVuNT WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsDamBao", SqlDbType.Bit, "IsDamBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HinhThuc", SqlDbType.NVarChar, "HinhThuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaDB", SqlDbType.Decimal, "TriGiaDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThuc", SqlDbType.DateTime, "NgayKetThuc", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsDamBao", SqlDbType.Bit, "IsDamBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HinhThuc", SqlDbType.NVarChar, "HinhThuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaDB", SqlDbType.Decimal, "TriGiaDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThuc", SqlDbType.DateTime, "NgayKetThuc", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_DamBaoNghiaVuNT VALUES(@TKMD_ID, @IsDamBao, @HinhThuc, @TriGiaDB, @NgayBatDau, @NgayKetThuc)";
            string update = "UPDATE t_KDT_DamBaoNghiaVuNT SET TKMD_ID = @TKMD_ID, IsDamBao = @IsDamBao, HinhThuc = @HinhThuc, TriGiaDB = @TriGiaDB, NgayBatDau = @NgayBatDau, NgayKetThuc = @NgayKetThuc WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_DamBaoNghiaVuNT WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsDamBao", SqlDbType.Bit, "IsDamBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HinhThuc", SqlDbType.NVarChar, "HinhThuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaDB", SqlDbType.Decimal, "TriGiaDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThuc", SqlDbType.DateTime, "NgayKetThuc", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsDamBao", SqlDbType.Bit, "IsDamBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HinhThuc", SqlDbType.NVarChar, "HinhThuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaDB", SqlDbType.Decimal, "TriGiaDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThuc", SqlDbType.DateTime, "NgayKetThuc", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static DamBaoNghiaVuNT Load(long id)
		{
			const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<DamBaoNghiaVuNT> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<DamBaoNghiaVuNT> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<DamBaoNghiaVuNT> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<DamBaoNghiaVuNT> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_KDT_DamBaoNghiaVuNT_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertDamBaoNghiaVuNT(long tKMD_ID, bool isDamBao, string hinhThuc, decimal triGiaDB, DateTime ngayBatDau, DateTime ngayKetThuc)
		{
			DamBaoNghiaVuNT entity = new DamBaoNghiaVuNT();	
			entity.TKMD_ID = tKMD_ID;
			entity.IsDamBao = isDamBao;
			entity.HinhThuc = hinhThuc;
			entity.TriGiaDB = triGiaDB;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayKetThuc = ngayKetThuc;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@IsDamBao", SqlDbType.Bit, IsDamBao);
			db.AddInParameter(dbCommand, "@HinhThuc", SqlDbType.NVarChar, HinhThuc);
			db.AddInParameter(dbCommand, "@TriGiaDB", SqlDbType.Decimal, TriGiaDB);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, NgayKetThuc.Year <= 1753 ? DBNull.Value : (object) NgayKetThuc);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<DamBaoNghiaVuNT> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DamBaoNghiaVuNT item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateDamBaoNghiaVuNT(long id, long tKMD_ID, bool isDamBao, string hinhThuc, decimal triGiaDB, DateTime ngayBatDau, DateTime ngayKetThuc)
		{
			DamBaoNghiaVuNT entity = new DamBaoNghiaVuNT();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.IsDamBao = isDamBao;
			entity.HinhThuc = hinhThuc;
			entity.TriGiaDB = triGiaDB;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayKetThuc = ngayKetThuc;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_DamBaoNghiaVuNT_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@IsDamBao", SqlDbType.Bit, IsDamBao);
			db.AddInParameter(dbCommand, "@HinhThuc", SqlDbType.NVarChar, HinhThuc);
			db.AddInParameter(dbCommand, "@TriGiaDB", SqlDbType.Decimal, TriGiaDB);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, NgayKetThuc.Year <= 1753 ? DBNull.Value : (object) NgayKetThuc);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<DamBaoNghiaVuNT> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DamBaoNghiaVuNT item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateDamBaoNghiaVuNT(long id, long tKMD_ID, bool isDamBao, string hinhThuc, decimal triGiaDB, DateTime ngayBatDau, DateTime ngayKetThuc)
		{
			DamBaoNghiaVuNT entity = new DamBaoNghiaVuNT();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.IsDamBao = isDamBao;
			entity.HinhThuc = hinhThuc;
			entity.TriGiaDB = triGiaDB;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayKetThuc = ngayKetThuc;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@IsDamBao", SqlDbType.Bit, IsDamBao);
			db.AddInParameter(dbCommand, "@HinhThuc", SqlDbType.NVarChar, HinhThuc);
			db.AddInParameter(dbCommand, "@TriGiaDB", SqlDbType.Decimal, TriGiaDB);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, NgayKetThuc.Year <= 1753 ? DBNull.Value : (object) NgayKetThuc);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<DamBaoNghiaVuNT> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DamBaoNghiaVuNT item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteDamBaoNghiaVuNT(long id)
		{
			DamBaoNghiaVuNT entity = new DamBaoNghiaVuNT();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_DamBaoNghiaVuNT_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<DamBaoNghiaVuNT> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DamBaoNghiaVuNT item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}