﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using Company.KDT.SHARE.Components;
using System.Data;
using Company.Interface.SXXK;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.QuanTri;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuSendForm : BaseFormHaveGuidPanel
    {
        public  NguyenPhuLieuDangKy nplDangKy = new NguyenPhuLieuDangKy();
        public NguyenPhuLieu NPL = new NguyenPhuLieu();
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        private string xmlCurrent = "";
        public bool isAdd = true;
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();

        NguyenPhuLieuRegistedForm NPLRegistedForm = new NguyenPhuLieuRegistedForm();
        public bool isEdit = false;
        public bool isCancel = false;
        public LogHistory Log = new LogHistory();
        //-----------------------------------------------------------------------------------------
        public NguyenPhuLieuSendForm()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            try
            {
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = MaHS.SelectAll();
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaHS.AutoCompleteCustomSource = col;
                this._DonViTinh = Company.KDT.SHARE.Components.Globals.GlobalDanhMucChuanHQ.Tables["DonViTinh"];
                cbDonViTinh.DataSource = this._DonViTinh;
                cbDonViTinh.DisplayMember = "Ten";
                cbDonViTinh.ValueMember = "ID";
                cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
                NPLCollection = NguyenPhuLieu.SelectCollectionAll();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void NguyenPhuLieuSendForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.khoitao_DuLieuChuan();
                if (isEdit)
                {
                    nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                }
                else if (isCancel)
                {
                    nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                }
                else
                {
                    if (nplDangKy.ID == 0)
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;   
                    }
                }
                SetCommandNew();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = this.nplDangKy.NPLCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetCommandNew()
        {
            if (nplDangKy.TrangThaiXuLy==TrangThaiXuLy.DA_DUYET)
	        {
                btnAdd.Enabled = false;
                btnAddExcel.Enabled = false;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = nplDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = true;
                btnAddExcel.Enabled = true;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                dtpNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                btnAdd.Enabled = false;
                btnAddExcel.Enabled = false;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = nplDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                btnAdd.Enabled = false;
                btnAddExcel.Enabled = false;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (nplDangKy.TrangThaiXuLy ==TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = true;
                btnAddExcel.Enabled = true;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = nplDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = true;
                btnAddExcel.Enabled = true;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = nplDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if(nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnAdd.Enabled = false;
                btnAddExcel.Enabled = false;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Chờ duyệt", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            

        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới nguyên phụ liệu.
        /// </summary>
        private void Add()
        {
            try
            {
                NguyenPhuLieuEditForm f = new NguyenPhuLieuEditForm();
                f.OpenType = OpenFormType.Insert;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                f.NPLCollection = nplDangKy.NPLCollection;
                f.nplDangKy = nplDangKy;
                f.ShowDialog(this);

                if (f.NPLDetail != null)
                {
                    this.nplDangKy.NPLCollection.Add(f.NPLDetail);
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void Save()
        {
            try
            {
                if (this.nplDangKy.NPLCollection.Count == 0)
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "DOANH NGHIỆP CHƯA NHẬP THÔNG TIN NGUYÊN PHỤ LIỆU .", false);
                    return;
                }
                this.nplDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (this.nplDangKy.ID == 0)
                {
                    this.nplDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    this.nplDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    if (isEdit)
                    {
                        this.nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else if (isCancel)
                    {
                        this.nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    else
                    {
                        this.nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    this.nplDangKy.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text.ToString());
                    this.nplDangKy.NgayTiepNhan = DateTime.Now;
                }
                else
                {
                    nplDangKy.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text.ToString());
                    nplDangKy.NgayTiepNhan = dtpNgayTN.Value;
                }
                int sttHang = 1;
                foreach (NguyenPhuLieu nplD in this.nplDangKy.NPLCollection)
                {
                    nplD.STTHang = sttHang++;
                }
                nplDangKy.InsertUpdateFull();
                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "LƯU THÀNH CÔNG!", false);
                Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.LuuThongTin, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }

        //-----------------------------------------------------------------------------------------
        private void updateRowOnGrid(string maNPL)
        {
            try
            {
                GridEXRow[] jrows = dgList.GetRows();
                foreach (GridEXRow row in jrows)
                {
                    if (row.Cells["Ma"].Value.ToString().Equals(maNPL))
                    {
                        row.BeginEdit();
                        row.Cells["IsExistOnServer"].Value = 1;
                        row.EndEdit();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        //-----------------------------------------------------------------------------------------
        private void updateRowsOnGrid(IEnumerable<string> collection)
        {
            try
            {
                GridEXRow[] jrows = dgList.GetRows();
                foreach (GridEXRow row in jrows)
                {
                    row.BeginEdit();
                    row.Cells["IsExistOnServer"].Value = 0;
                    row.EndEdit();
                }

                foreach (string s in collection)
                {
                    this.updateRowOnGrid(s);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        #endregion

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    NPL = new NguyenPhuLieu();
                    NPL = (NguyenPhuLieu)e.Row.DataRow;
                    SetData();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdSend":
                     this.SendV5(false);
                    break;
                case "cmdCancel":
                    if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN KHAI BÁO HỦY NGUYÊN PHỤ LIỆU NÀY ĐẾN HQ KHÔNG ? ", true) == "Yes")
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        nplDangKy.Update();
                        SetCommandNew();
                        this.SendV5(true);
                    }
                    break;
                case "cmdEdit":
                     this.SendV5(false);
                    break;
                case "cmdAddExcel":
                    this.AddNguyenPhuLieuFromExcel();
                    break;
                case "XacNhan":
                    this.FeedBackVNACCS();
                    break;
                case "InPhieuTN":
                    this.Print();
                    break;
                case "cmdSuaNPL":
                    this.Edit();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;

            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = nplDangKy.ID;
                f.loaiKhaiBao = LoaiKhaiBao.NguyenPhuLieu;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", nplDangKy.ID, LoaiKhaiBao.NguyenPhuLieu), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageNPL(nplDangKy);
                    nplDangKy.InsertUpdateFull();
                    if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                    {
                        nplDangKy.TransgferDataToSXXK();
                    }
                    Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.CapNhatKetQuaXuLy, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    ShowMessage("Cập nhật thông tin thành công .", false);
                    SetCommandNew();
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_SXXK_NguyenPhuLieuDangKy", "", Convert.ToInt32(nplDangKy.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        // duydp
        private void Edit() 
        {
            try
            {
                string msg = "";
                msg += "---------THÔNG TIN NGUYÊN PHỤ LIỆU ĐÃ KHAI BÁO ĐẾN HQ---------";
                msg += "\nSỐ TIẾP NHẬN : " + nplDangKy.SoTiepNhan.ToString();
                msg += "\nNGÀY TIẾP NHẬN : " + nplDangKy.NgayTiepNhan.ToString("dd/MM/yy hh:mm:ss");
                msg += "\nHẢI QUAN TIẾP NHẬN : " + nplDangKy.MaHaiQuan.ToString();
                msg += "\n--------------------THÔNG TIN XÁC NHẬN--------------------";
                msg += "\nDOANH NGHIỆP CÓ MUỐN CHUYỂN SANG KHAI BÁO SỬA KHÔNG ?";
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", msg, true) == "Yes")
                {
                    nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    nplDangKy.InsertUpdate();
                    nplDangKy.DeleteFull(nplDangKy.MaDoanhNghiep, nplDangKy.MaHaiQuan);
                    SetCommandNew();
                    Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.ChuyenKhaiBaoSua, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Print()
        {
            try
            {
                if (this.nplDangKy.SoTiepNhan == 0) return;
                Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
                phieuTN.phieu = "NGUYÊN PHỤ LIỆU";
                phieuTN.soTN = this.nplDangKy.SoTiepNhan.ToString();
                phieuTN.ngayTN = this.nplDangKy.NgayTiepNhan.ToString("dd/MM/yyyy HH:mm:ss");
                phieuTN.maHaiQuan = nplDangKy.MaHaiQuan;
                phieuTN.BindReport();
                phieuTN.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void AddNguyenPhuLieuFromExcel()
        {
            try
            {
                NguyenPhuLieuReadExcelForm f = new NguyenPhuLieuReadExcelForm();
                f.NPLCollection = this.nplDangKy.NPLCollection;
                f.nplDangKy = nplDangKy;
                f.isEdit = isEdit;
                f.isCancel = isCancel;
                f.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            btnDelete_Click(null,null);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void XoaNguyenPhuLieu(GridEXSelectedItemCollection items, NguyenPhuLieuCollection nguyenphulieus)
        {
            try
            {
                if (items.Count == 0) return;
                string msgWarning = string.Empty;
                List<NguyenPhuLieu> npls = new List<NguyenPhuLieu>();
                List<GridEXSelectedItem> itemRemove = new List<GridEXSelectedItem>();
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieu npl = (NguyenPhuLieu)i.GetRow().DataRow;
                        npls.Add(npl);
                    }
                }
                if (ShowMessage(" DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    foreach (NguyenPhuLieu item in npls)
                    {
                        if (item.ID > 0)
                        {
                            try
                            {
                                // XÓA NPL ĐÃ ĐĂNG KÝ
                                Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                NPL.Ma = item.Ma;
                                NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                NPL.Delete();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                            item.Delete();
                        }
                        nguyenphulieus.Remove(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Delete()
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                XoaNguyenPhuLieu(items, nplDangKy.NPLCollection);
                BindData();
                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG","XÓA DỮ LIỆU THÀNH CÔNG !", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                NguyenPhuLieuCollection ItemCollDelete = new NguyenPhuLieuCollection();
                List<NguyenPhuLieu> ItemColl = new List<NguyenPhuLieu>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "DOANH NGHIỆP MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((NguyenPhuLieu)i.GetRow().DataRow);
                        }

                    }
                    foreach (NguyenPhuLieu item in ItemColl)
                    {
                        if (item.ID > 0)
                        {
                            try
                            {
                                // XÓA NPL ĐÃ ĐĂNG KÝ
                                Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                NPL.Ma = item.Ma;
                                NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                NPL.Delete();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                            item.Delete();
                            ItemCollDelete.Add(item);
                        }
                        nplDangKy.NPLCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (NguyenPhuLieu item in nplDangKy.NPLCollection)
                    {
                        item.STTHang = k;
                        k++;
                    }
                    BindData();
                    Log.LogNguyenPhuLieu(nplDangKy, ItemCollDelete, MessageTitle.XoaNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        #region EDIT DUYDP
        private void SendV5(bool IsCancel)
        {

            if (nplDangKy.ID == 0)
            {
                this.ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "DOANH NGHIỆP HÃY LƯU THÔNG TIN TRƯỚC KHI KHAI BÁO", false);
                return;
            }
            try
            {
                if (nplDangKy.NPLCollection.Count == 0)
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG","DOANH NGHIỆP CHƯA NHẬP THÔNG TIN DANH SÁCH NGUYÊN PHỤ LIỆU .", false);
                    this.Cursor = Cursors.Default;
                    return;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
                sendXML.master_id = nplDangKy.ID;
                if (sendXML.Load())
                {
                    if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG","THÔNG TIN ĐÃ ĐƯỢC GỬI ĐẾN HẢI QUAN. NHẤN [LẤY PHẢN HỒI] ĐỂ LẤY THÔNG TIN", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;   
                    }
                }
                nplDangKy.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu npl = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_NPL(nplDangKy, IsCancel, GlobalSettings.TEN_DON_VI);
                     ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = nplDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(nplDangKy.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(nplDangKy.MaHaiQuan).Trim() : nplDangKy.MaHaiQuan
                                 }
                              ,

                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_NPL,
                                    Function = npl.Function,
                                    Reference = nplDangKy.GUIDSTR,
                                }
                                ,
                                npl);
                     switch (nplDangKy.TrangThaiXuLy)
                     {
                         case -1:
                             Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.KhaiBaoNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                             break;
                         case -5:
                             Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.KhaiBaoHuyNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                             break;
                         case 5:
                             Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.KhaiBaoSuaNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                             break;
                         default:
                             break;
                     }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoNguyenPhuLieu);
                    XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdAdd.Enabled = cmdSave.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    btnDelete.Enabled = false;
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
                    sendXML.master_id = nplDangKy.ID;
                    sendXML.func = 1;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.KhaiBaoHuyNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            nplDangKy.DeleteFull(nplDangKy.MaDoanhNghiep, nplDangKy.MaHaiQuan);
                        }
                        else
                        {
                            Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.KhaiBaoHQDuyetNguyennPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            nplDangKy.TransgferDataToSXXK();
                        }
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandNew();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        FeedBackVNACCS();
                        SetCommandNew();
                    }
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                    Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.KhaiBaoHQKhongDuyetNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    sendForm.Message.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoNguyenPhuLieu);
                    ShowMessageTQDT(msgInfor, false);
                    SetCommandNew();
                }

            }


            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.NguyenPhuLieuSendHandler(nplDangKy, ref msgInfor, e);
        }
        private void FeedBackVNACCS()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
            sendXML.master_id = nplDangKy.ID;
            if (!sendXML.Load())
            {
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN HOẶC HỆ THỐNG HQ BỊ QUÁ TẢI KHÔNG NHẬN ĐƯỢC PHẢN HỒI . NẾU TRƯỜNG HỢP HỆ THỐNG HQ BỊ QUÁ TẢI BẠN CHỌN YES ĐỂ TẠO MESSAGE NHẬN PHẢN HỒI . CHỌN NO ĐỂ BỎ QUA", true) == "Yes")
                {
                    sendXML.msg = String.Empty;
                    sendXML.func = Convert.ToInt32(DeclarationFunction.KHAI_BAO);
                    sendXML.InsertUpdate();
                }
                else
                {
                    return;
                }
            }
            while (isFeedBack)
            {
                string reference = nplDangKy.GUIDSTR;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_NPL,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_NPL,
                };
                subjectBase.Type = DeclarationIssuer.SXXK_NPL;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = nplDangKy.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(nplDangKy.MaHaiQuan.Trim())),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(nplDangKy.MaHaiQuan).Trim() : nplDangKy.MaHaiQuan
                                              }, subjectBase, null);
                if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(nplDangKy.MaHaiQuan));
                    msgSend.To.Identity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(nplDangKy.MaHaiQuan).Trim();
                }
                while (isFeedBack)
                {
                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;
                    isFeedBack = dlgSendForm.DoSend(msgSend);
                    if (isFeedBack)
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                        {
                            if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.KhaiBaoHuyNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                                nplDangKy.DeleteFull(nplDangKy.MaDoanhNghiep,nplDangKy.MaHaiQuan);
                            }
                            else
                            {
                                Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.KhaiBaoHQDuyetNguyennPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                                nplDangKy.TransgferDataToSXXK();
                            }
                            nplDangKy.Update();
                            isFeedBack = false;
                            SetCommandNew();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            Log.LogNguyenPhuLieu(nplDangKy, MessageTitle.KhaiBaoHQKhongDuyetNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            nplDangKy.Update();
                            isFeedBack = false;
                            SetCommandNew();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG?", true) == "Yes";
                        }
                    }
                }
            }
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = nplDangKy.ID;
                form.DeclarationIssuer = DeclarationIssuer.SXXK_NPL;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        #endregion
        private void GetData()
        {
            try
            {
                if (NPL == null)
                    NPL = new NguyenPhuLieu();
                NPL.Ma = txtMa.Text.ToString().Trim();
                NPL.Ten = txtTen.Text.ToString().Trim();
                NPL.MaHS = txtMaHS.Text.ToString();
                NPL.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetData()
        {
            try
            {
                txtMa.Text = NPL.Ma;
                txtTen.Text = NPL.Ten;
                txtMaHS.Text = NPL.MaHS;
                cbDonViTinh.SelectedValue = NPL.DVT_ID;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, "Mã Nguyên phụ liệu", isOnlyWarning);
                isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã Nguyên phụ liệu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, "Tên Nguyên phụ liệu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHS, errorProvider, "Mã số hàng hóa", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbDonViTinh, errorProvider, "Đơn vị tính", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetData();
                if (isAdd)
                {
                    bool isExits = false;
                    // KIỂM TRA NPL ĐÃ NHẬP TRƯỚC ĐÓ
                    foreach (NguyenPhuLieu item in nplDangKy.NPLCollection)
                    {
                        if (item.Ma==NPL.Ma)
                        {
                            errorProvider.SetError(txtMa, "NGUYÊN PHỤ LIỆU NÀY ĐÃ ĐƯỢC NHẬP Ở DANH SÁCH PHÍA DƯỚI .");
                            isExits = true;
                            return;                            
                        }
                    }
                    if (!isEdit || !isCancel)
                    {
                        // KIỂM TRA NPL ĐÃ ĐĂNG KÝ TRƯỚC ĐÓ
                        foreach (NguyenPhuLieu item in NPLCollection)
                        {
                            if (item.Ma == NPL.Ma && item.Master_ID != nplDangKy.ID)
                            {
                                NguyenPhuLieuDangKy NPLExits = new NguyenPhuLieuDangKy();
                                NPLExits.ID = item.Master_ID;
                                NPLExits.Load();
                                string Error = "";
                                Error += "\nID : " + NPLExits.ID;
                                Error += "\nSỐ TIẾP NHẬN : " + NPLExits.SoTiepNhan.ToString();
                                Error += "\nNGÀY TIẾP NHẬN : " + NPLExits.NgayTiepNhan.ToString("dd/MM/yyyy");
                                switch (NPLExits.TrangThaiXuLy)
                                {
                                    case -4:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ KHAI BÁO HỦY";
                                        break;
                                    case -3:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ KHAI BÁO";
                                        break;
                                    case -2:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : CHỜ DUYỆT KHAI BÁO SỬA";
                                        break;
                                    case -1:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : CHƯA KHAI BÁO";
                                        break;
                                    case 0:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : CHỜ DUYỆT";
                                        break;
                                    case 1:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ DUYỆT";
                                        break;
                                    case 2:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : KHÔNG PHÊ DUYỆT";
                                        break;
                                    case 4:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ KHAI BÁO SỬA";
                                        break;
                                    case 5:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐANG SỬA";
                                        break;
                                    case 10:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ  HỦY";
                                        break;
                                    case 11:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : CHỜ HỦY";
                                        break;
                                    default:
                                        break;
                                }
                                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "NGUYÊN PHỤ LIỆU NÀY ĐÃ ĐƯỢC ĐĂNG KÝ TRƯỚC ĐÓ VỚI THÔNG TIN " + Error, false);
                                //errorProvider.SetError(txtMa, "Nguyên phụ liệu này đã được đăng ký\n" + Error);
                                isExits = true;
                                return;
                            }
                        }
                        if (!isExits)
                        {
                            nplDangKy.NPLCollection.Add(NPL);
                        }
                    }
                    else
                    {
                        isExits = false;
                        foreach (NguyenPhuLieu item in NPLCollection)
                        {
                            if (item.Ma == NPL.Ma)
                            {
                                errorProvider.SetError(txtMa, "NPL NÀY CHƯA ĐƯỢC ĐĂNG KÝ CHỈ NHỮNG NPL ĐÃ ĐĂNG KÝ MỚI ĐƯỢC PHÉP KHAI BÁO SỬA HOẶC HỦY .");
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            nplDangKy.NPLCollection.Add(NPL);    
                        }                        
                    }
                }
                BindData();
                NPL = new NguyenPhuLieu();
                txtMa.Text = String.Empty;
                txtTen.Text = String.Empty;
                txtMaHS.Text = String.Empty;
                isAdd = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAddExcel_Click(object sender, EventArgs e)
        {
            try
            {
                NguyenPhuLieuReadExcelForm f = new NguyenPhuLieuReadExcelForm();
                f.NPLCollection = this.nplDangKy.NPLCollection;
                f.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            NPL = new NguyenPhuLieu();
                            NPL = (NguyenPhuLieu)i.GetRow().DataRow;
                            SetData();
                            isAdd = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH NGUYÊN PHỤ LIỆU KHAI BÁO.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm.isBrowser = true;
                this.NPLRegistedForm.ShowDialog(this);
                bool isExits = false;
                if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                {
                    if (nplDangKy.NPLCollection.Count >=1)
                    {
                        isExits = false;
                        foreach (NguyenPhuLieu item in nplDangKy.NPLCollection)
                        {
                            if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma == item.Ma)
                            {
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            txtMa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                            txtTen.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);    
                        }
                    }
                    else
                    {
                        txtMa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTen.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                }
                else if (this.NPLRegistedForm.NPLCollectionSelected.Count >=1)
                {
                    NguyenPhuLieuCollection NPLSelectCollection = new NguyenPhuLieuCollection();
                    isExits = false;
                    foreach (Company.BLL.SXXK.NguyenPhuLieu item in NPLRegistedForm.NPLCollectionSelected)
                    {
                        if (nplDangKy.NPLCollection.Count >=1)
                        {
                            isExits = false;
                            foreach (NguyenPhuLieu ite in nplDangKy.NPLCollection)
                            {
                                if (item.Ma == ite.Ma)
                                {
                                    isExits = true;
                                    break;
                                }   
                            }
                            if (!isExits)
                            {
                                NPL = new NguyenPhuLieu();
                                NPL.Ma = item.Ma;
                                NPL.Ten = item.Ten;
                                NPL.MaHS = item.MaHS;
                                NPL.DVT_ID = item.DVT_ID;
                                NPLSelectCollection.Add(NPL);
                            }
                        }
                        else
                        {
                            NPL = new NguyenPhuLieu();
                            NPL.Ma = item.Ma;
                            NPL.Ten = item.Ten;
                            NPL.MaHS = item.MaHS;
                            NPL.DVT_ID = item.DVT_ID;
                            nplDangKy.NPLCollection.Add(NPL);
                        }
                    }
                    foreach (NguyenPhuLieu item in NPLSelectCollection)
                    {
                        nplDangKy.NPLCollection.Add(item);
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                //npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                //npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //npl.Ma = txtMa.Text;
                //if (npl.Load())
                //{
                //    txtMa.Text = npl.Ma;
                //    cbDonViTinh.SelectedValue = npl.DVT_ID.PadRight(3);
                //    txtTen.Text = npl.Ten;
                //    txtMaHS.Text = npl.MaHS;
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {
            try
            {
                //Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                //npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                //npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //npl.Ma = txtMa.Text;
                //if (npl.Load())
                //{
                //    txtMa.Text = npl.Ma;
                //    cbDonViTinh.SelectedValue = npl.DVT_ID.PadRight(3);
                //    txtTen.Text = npl.Ten;
                //    txtMaHS.Text = npl.MaHS;
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaNPL.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
