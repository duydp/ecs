﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
     public class Item
    {
         /// <summary>
         /// Mã sản phẩm
         /// </summary>
         [XmlElement("identity")]
         public string Identity { get; set; }
         /// <summary>
         /// Tên sản phẩm
         /// </summary>
         [XmlElement("name")]
         public string Name { get; set; }
         /// <summary>
         /// Số lượng
         /// </summary>
         [XmlElement("quantity")]
         public string Quantity { get; set; }
         /// <summary>
         /// Trị giá sản phẩm
         /// </summary>
         [XmlElement("productValue")]
         public string ProductValue { get; set; }
         /// <summary>
         /// Trị giá tiền gia công
         /// </summary>
         [XmlElement("paymentValue")]
         public string PaymentValue { get; set; }
    }
}
