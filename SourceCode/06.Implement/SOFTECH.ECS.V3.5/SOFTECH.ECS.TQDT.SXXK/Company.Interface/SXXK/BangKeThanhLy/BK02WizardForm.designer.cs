namespace Company.Interface.SXXK.BangKe
{
    partial class BK02WizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKXTL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK02WizardForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.ccToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.ccFromDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.btnNext = new Janus.Windows.EditControls.UIButton();
            this.btnFinish = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.btnBack = new Janus.Windows.EditControls.UIButton();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgTKX = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnExportTKX = new Janus.Windows.EditControls.UIButton();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnGoTo = new Janus.Windows.EditControls.UIButton();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnExportSelect = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoToKhaiTL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnSearchSelect = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgTKXTL = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKXTL)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox8);
            this.grbMain.Controls.Add(this.uiGroupBox9);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(1151, 678);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(6, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(266, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "Danh sách tờ khai xuất chưa thanh khoản";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(579, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(286, 14);
            this.label5.TabIndex = 5;
            this.label5.Text = "Danh sách tờ khai xuất đưa vào thanh khoản";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.ccToDate);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.ccFromDate);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1151, 54);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Tìm kiếm";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(432, 20);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 23);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ccToDate
            // 
            this.ccToDate.BackColor = System.Drawing.Color.White;
            this.ccToDate.CustomFormat = "dd/MM/yyyy";
            this.ccToDate.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccToDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccToDate.DropDownCalendar.Name = "";
            this.ccToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccToDate.Location = new System.Drawing.Point(319, 20);
            this.ccToDate.Name = "ccToDate";
            this.ccToDate.Nullable = true;
            this.ccToDate.NullButtonText = "Xóa";
            this.ccToDate.ShowNullButton = true;
            this.ccToDate.Size = new System.Drawing.Size(107, 21);
            this.ccToDate.TabIndex = 3;
            this.ccToDate.TodayButtonText = "Hôm nay";
            this.ccToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccToDate.VisualStyleManager = this.vsmMain;
            this.ccToDate.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(264, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "đến ngày";
            // 
            // ccFromDate
            // 
            this.ccFromDate.BackColor = System.Drawing.Color.White;
            this.ccFromDate.CustomFormat = "dd/MM/yyyy";
            this.ccFromDate.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccFromDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccFromDate.DropDownCalendar.Name = "";
            this.ccFromDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccFromDate.Location = new System.Drawing.Point(149, 20);
            this.ccFromDate.Name = "ccFromDate";
            this.ccFromDate.Nullable = true;
            this.ccFromDate.NullButtonText = "Xóa";
            this.ccFromDate.ShowNullButton = true;
            this.ccFromDate.Size = new System.Drawing.Size(109, 21);
            this.ccFromDate.TabIndex = 1;
            this.ccFromDate.TodayButtonText = "Hôm nay";
            this.ccFromDate.Value = new System.DateTime(2016, 10, 18, 0, 0, 0, 0);
            this.ccFromDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFromDate.VisualStyleManager = this.vsmMain;
            this.ccFromDate.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Chọn tờ khai từ ngày";
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageSize = new System.Drawing.Size(25, 25);
            this.btnNext.Location = new System.Drawing.Point(841, 15);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(105, 23);
            this.btnNext.TabIndex = 9;
            this.btnNext.Text = "Tiếp tục";
            this.btnNext.VisualStyleManager = this.vsmMain;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Enabled = false;
            this.btnFinish.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinish.Image = ((System.Drawing.Image)(resources.GetObject("btnFinish.Image")));
            this.btnFinish.Location = new System.Drawing.Point(952, 15);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(105, 23);
            this.btnFinish.TabIndex = 10;
            this.btnFinish.Text = "Hoàn thành";
            this.btnFinish.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(1063, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageSize = new System.Drawing.Size(25, 25);
            this.btnDelete.Location = new System.Drawing.Point(6, 222);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(26, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageSize = new System.Drawing.Size(25, 25);
            this.btnAdd.Location = new System.Drawing.Point(6, 193);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(26, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.VisualStyleManager = this.vsmMain;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.Enabled = false;
            this.btnBack.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.ImageSize = new System.Drawing.Size(25, 25);
            this.btnBack.Location = new System.Drawing.Point(730, 15);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(105, 23);
            this.btnBack.TabIndex = 8;
            this.btnBack.Text = "Trở lại";
            this.btnBack.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(3, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(346, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ghi chú: Kích đôi vào tờ khai để xem thông tin chi tiết tờ khai";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.label4);
            this.uiGroupBox5.Controls.Add(this.label5);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 54);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1151, 33);
            this.uiGroupBox5.TabIndex = 13;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.btnClose);
            this.uiGroupBox7.Controls.Add(this.btnNext);
            this.uiGroupBox7.Controls.Add(this.btnFinish);
            this.uiGroupBox7.Controls.Add(this.btnBack);
            this.uiGroupBox7.Controls.Add(this.label3);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 631);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1151, 47);
            this.uiGroupBox7.TabIndex = 15;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox6.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 87);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(543, 544);
            this.uiGroupBox6.TabIndex = 17;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgTKX);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 57);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(537, 484);
            this.uiGroupBox2.TabIndex = 13;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // dgTKX
            // 
            this.dgTKX.AutoEdit = true;
            this.dgTKX.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKX.ColumnAutoResize = true;
            dgTKX_DesignTimeLayout.LayoutString = resources.GetString("dgTKX_DesignTimeLayout.LayoutString");
            this.dgTKX.DesignTimeLayout = dgTKX_DesignTimeLayout;
            this.dgTKX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKX.GroupByBoxVisible = false;
            this.dgTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKX.ImageList = this.ImageList1;
            this.dgTKX.Location = new System.Drawing.Point(3, 8);
            this.dgTKX.Name = "dgTKX";
            this.dgTKX.RecordNavigator = true;
            this.dgTKX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKX.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKX.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKX.Size = new System.Drawing.Size(531, 473);
            this.dgTKX.TabIndex = 3;
            this.dgTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTKX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKX_RowDoubleClick);
            this.dgTKX.RecordUpdated += new System.EventHandler(this.dgTKX_RecordUpdated);
            this.dgTKX.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKX_LoadingRow);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnExportTKX);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Controls.Add(this.btnGoTo);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(537, 49);
            this.uiGroupBox1.TabIndex = 12;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnExportTKX
            // 
            this.btnExportTKX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportTKX.Image = ((System.Drawing.Image)(resources.GetObject("btnExportTKX.Image")));
            this.btnExportTKX.Location = new System.Drawing.Point(367, 17);
            this.btnExportTKX.Name = "btnExportTKX";
            this.btnExportTKX.Size = new System.Drawing.Size(83, 23);
            this.btnExportTKX.TabIndex = 4;
            this.btnExportTKX.Text = "Xuất Excel";
            this.btnExportTKX.VisualStyleManager = this.vsmMain;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(128, 17);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(141, 21);
            this.txtSoToKhai.TabIndex = 1;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.Value = 0;
            this.txtSoToKhai.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            this.txtSoToKhai.TextChanged += new System.EventHandler(this.btnGoTo_Click);
            // 
            // btnGoTo
            // 
            this.btnGoTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoTo.Image = ((System.Drawing.Image)(resources.GetObject("btnGoTo.Image")));
            this.btnGoTo.Location = new System.Drawing.Point(275, 16);
            this.btnGoTo.Name = "btnGoTo";
            this.btnGoTo.Size = new System.Drawing.Size(89, 23);
            this.btnGoTo.TabIndex = 2;
            this.btnGoTo.Text = "Tìm";
            this.btnGoTo.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(6, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tìm đến tờ khai số:";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.btnDelete);
            this.uiGroupBox9.Controls.Add(this.btnAdd);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.Location = new System.Drawing.Point(543, 87);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(38, 544);
            this.uiGroupBox9.TabIndex = 18;
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.AutoScroll = true;
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.btnExportSelect);
            this.uiGroupBox10.Controls.Add(this.label1);
            this.uiGroupBox10.Controls.Add(this.txtSoToKhaiTL);
            this.uiGroupBox10.Controls.Add(this.btnSearchSelect);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox10.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(564, 49);
            this.uiGroupBox10.TabIndex = 20;
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // btnExportSelect
            // 
            this.btnExportSelect.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnExportSelect.Image")));
            this.btnExportSelect.Location = new System.Drawing.Point(373, 17);
            this.btnExportSelect.Name = "btnExportSelect";
            this.btnExportSelect.Size = new System.Drawing.Size(80, 23);
            this.btnExportSelect.TabIndex = 6;
            this.btnExportSelect.Text = "Xuất Excel";
            this.btnExportSelect.VisualStyleManager = this.vsmMain;
            this.btnExportSelect.Click += new System.EventHandler(this.btnExportSelect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tìm đến tờ khai số:";
            // 
            // txtSoToKhaiTL
            // 
            this.txtSoToKhaiTL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiTL.Location = new System.Drawing.Point(128, 18);
            this.txtSoToKhaiTL.Name = "txtSoToKhaiTL";
            this.txtSoToKhaiTL.Size = new System.Drawing.Size(144, 21);
            this.txtSoToKhaiTL.TabIndex = 4;
            this.txtSoToKhaiTL.Text = "0";
            this.txtSoToKhaiTL.Value = 0;
            this.txtSoToKhaiTL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoToKhaiTL.VisualStyleManager = this.vsmMain;
            this.txtSoToKhaiTL.TextChanged += new System.EventHandler(this.btnSearchSelect_Click);
            // 
            // btnSearchSelect
            // 
            this.btnSearchSelect.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchSelect.Image")));
            this.btnSearchSelect.Location = new System.Drawing.Point(278, 17);
            this.btnSearchSelect.Name = "btnSearchSelect";
            this.btnSearchSelect.Size = new System.Drawing.Size(89, 23);
            this.btnSearchSelect.TabIndex = 5;
            this.btnSearchSelect.Text = "Tìm";
            this.btnSearchSelect.VisualStyleManager = this.vsmMain;
            this.btnSearchSelect.Click += new System.EventHandler(this.btnSearchSelect_Click);
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.Location = new System.Drawing.Point(581, 87);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(570, 544);
            this.uiGroupBox8.TabIndex = 19;
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgTKXTL);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 57);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(564, 484);
            this.uiGroupBox3.TabIndex = 21;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgTKXTL
            // 
            this.dgTKXTL.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKXTL.AlternatingColors = true;
            this.dgTKXTL.AutoEdit = true;
            this.dgTKXTL.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKXTL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKXTL.ColumnAutoResize = true;
            dgTKXTL_DesignTimeLayout.LayoutString = resources.GetString("dgTKXTL_DesignTimeLayout.LayoutString");
            this.dgTKXTL.DesignTimeLayout = dgTKXTL_DesignTimeLayout;
            this.dgTKXTL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKXTL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKXTL.GroupByBoxVisible = false;
            this.dgTKXTL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXTL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXTL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKXTL.ImageList = this.ImageList1;
            this.dgTKXTL.Location = new System.Drawing.Point(3, 8);
            this.dgTKXTL.Name = "dgTKXTL";
            this.dgTKXTL.RecordNavigator = true;
            this.dgTKXTL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKXTL.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKXTL.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXTL.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKXTL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKXTL.Size = new System.Drawing.Size(558, 473);
            this.dgTKXTL.TabIndex = 1;
            this.dgTKXTL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTKXTL.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgTKXTL_DeletingRecord);
            this.dgTKXTL.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKXTL_RowDoubleClick);
            this.dgTKXTL.RecordUpdated += new System.EventHandler(this.dgTKXTL_RecordUpdated);
            this.dgTKXTL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKXTL_LoadingRow);
            // 
            // BK02WizardForm
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1151, 678);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK02WizardForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tạo bảng kê tờ khai xuất khẩu thanh lý";
            this.Load += new System.EventHandler(this.BK02WizardForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKXTL)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.CalendarCombo.CalendarCombo ccToDate;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFromDate;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIButton btnNext;
        private Janus.Windows.EditControls.UIButton btnFinish;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIButton btnBack;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgTKX;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnExportTKX;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.EditControls.UIButton btnGoTo;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgTKXTL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.EditControls.UIButton btnExportSelect;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiTL;
        private Janus.Windows.EditControls.UIButton btnSearchSelect;
    }
}