﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC10Form : BaseForm
    {
        private BangKe10_HQGC BC10 = new BangKe10_HQGC();
        public HopDong HD = new HopDong();
        public DataSet dsBK = new DataSet();
        public ReportViewBC10Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            Company.GC.BLL.GC.SanPhamCollection SPCollection = this.HD.GetSP();
            foreach(Company.GC.BLL.GC.SanPham SP in SPCollection)
            {
                cbPage.Items.Add(SP.Ten, SP.Ma);
            }
            cbPage.SelectedIndex = 0;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BC10.HD = this.HD;
            this.BC10.dsBK = this.dsBK;
            this.BC10.BindReport(cbPage.SelectedValue.ToString());
            
            printControl1.PrintingSystem = this.BC10.PrintingSystem;
            this.BC10.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }   
        }

   
    }
}