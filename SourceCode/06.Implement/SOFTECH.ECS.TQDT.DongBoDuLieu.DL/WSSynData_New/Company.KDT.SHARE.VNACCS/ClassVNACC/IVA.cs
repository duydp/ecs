﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    public class IVA : BasicVNACC
    {
        [AttributeIndex(STT = 1)]
        public PropertiesAttribute soTiepNhan { get; set; }
        [AttributeIndex(STT = 2)]
        public PropertiesAttribute PhanLoaiXuatKhau { get; set; }
        [AttributeIndex(STT = 3)]
        public PropertiesAttribute MaDaiLyHQ { get; set; }
        [AttributeIndex(STT = 4)]
        public PropertiesAttribute SoHoaDon { get; set; }
        [AttributeIndex(STT = 5)]
        public PropertiesAttribute NgayLapHoaDon { get; set; }
        [AttributeIndex(STT = 6)]
        public PropertiesAttribute DiaDiemLapHoaDon { get; set; }
        [AttributeIndex(STT = 7)]
        public GroupAttribute ChungTuHoaDon { get; set; }
        [AttributeIndex(STT = 8)]
        public PropertiesAttribute PTTT { get; set; }
        [AttributeIndex(STT = 9)]
        public PropertiesAttribute PhanLoaiBenLienQuan { get; set; }
        [AttributeIndex(STT = 10)]
        public PropertiesAttribute NguoiXNK_Ma { get; set; }
        [AttributeIndex(STT = 11)]
        public PropertiesAttribute NguoiXNK_Ten { get; set; }
        [AttributeIndex(STT = 12)]
        public PropertiesAttribute NguoiXNK_MaBuuChinh { get; set; }
        [AttributeIndex(STT = 13)]
        public PropertiesAttribute NguoiXNK_DiaChi { get; set; }
        [AttributeIndex(STT = 14)]
        public PropertiesAttribute NguoiXNK_DienThoai { get; set; }
        [AttributeIndex(STT = 15)]
        public PropertiesAttribute NguoiLapHoaDon { get; set; }
        [AttributeIndex(STT = 16)]
        public PropertiesAttribute NguoiGuiNhan_Ma { get; set; }
        [AttributeIndex(STT = 17)]
        public PropertiesAttribute NguoiGuiNhan_Ten { get; set; }
        [AttributeIndex(STT = 18)]
        public PropertiesAttribute NguoiGuiNhan_MaBuuChinh { get; set; }
        [AttributeIndex(STT = 19)]
        public PropertiesAttribute NguoiGuiNhan_DiaChi1 { get; set; }
        [AttributeIndex(STT = 20)]
        public PropertiesAttribute NguoiGuiNhan_DiaChi2 { get; set; }
        [AttributeIndex(STT = 21)]
        public PropertiesAttribute NguoiGuiNhan_DiaChi3 { get; set; }
        [AttributeIndex(STT = 22)]
        public PropertiesAttribute NguoiGuiNhan_DiaChi4 { get; set; }
        [AttributeIndex(STT = 23)]
        public PropertiesAttribute NguoiGuiNhan_SoDienThoai { get; set; }
        [AttributeIndex(STT = 24)]
        public PropertiesAttribute NguoiGuiNhan_Nuoc { get; set; }
        [AttributeIndex(STT = 25)]
        public PropertiesAttribute MaKyHieu { get; set; }
        [AttributeIndex(STT = 26)]
        public PropertiesAttribute PhanLoaiVC { get; set; }
        [AttributeIndex(STT = 27)]
        public PropertiesAttribute TenPhuongTienVC { get; set; }
        [AttributeIndex(STT = 28)]
        public PropertiesAttribute SoHieuChuyenDi { get; set; }
        [AttributeIndex(STT = 29)]
        public PropertiesAttribute DiaDiemXepHang_Ma { get; set; }
        [AttributeIndex(STT = 30)]
        public PropertiesAttribute DiaDiemXepHang_Ten { get; set; }
        [AttributeIndex(STT = 31)]
        public PropertiesAttribute DiaDiemXepHang_NgayXepHang { get; set; }
        [AttributeIndex(STT = 32)]
        public PropertiesAttribute DiaDiemDohang_Ma { get; set; }
        [AttributeIndex(STT = 33)]
        public PropertiesAttribute DiaDiemDohang_Ten { get; set; }
        [AttributeIndex(STT = 34)]
        public PropertiesAttribute DiaDiemTrungChuyen_Ma { get; set; }
        [AttributeIndex(STT = 35)]
        public PropertiesAttribute DiaDiemTrungChuyen_Ten { get; set; }
        [AttributeIndex(STT = 36)]
        public PropertiesAttribute TongTrongLuong { get; set; }
        [AttributeIndex(STT = 37)]
        public PropertiesAttribute TongTrongLuong_DVT { get; set; }
        [AttributeIndex(STT = 38)]
        public PropertiesAttribute TrongLuongTinh { get; set; }
        [AttributeIndex(STT = 39)]
        public PropertiesAttribute TrongLuongTinh_DVT { get; set; }
        [AttributeIndex(STT = 40)]
        public PropertiesAttribute TheTich { get; set; }
        [AttributeIndex(STT = 41)]
        public PropertiesAttribute TheTich_DVT { get; set; }
        [AttributeIndex(STT = 42)]
        public PropertiesAttribute TongSoKien { get; set; }
        [AttributeIndex(STT = 43)]
        public PropertiesAttribute TongSoKien_DVT { get; set; }
        [AttributeIndex(STT = 44)]
        public PropertiesAttribute GhiChu { get; set; }
        [AttributeIndex(STT = 45)]
        public PropertiesAttribute SoPL { get; set; }
        [AttributeIndex(STT = 46)]
        public PropertiesAttribute NganHangLC { get; set; }
        [AttributeIndex(STT = 47)]
        public PropertiesAttribute TriGiaFOB { get; set; }
        [AttributeIndex(STT = 48)]
        public PropertiesAttribute TriGiaFOB_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 49)]
        public PropertiesAttribute TriGiaFOB_TienDieuChinh { get; set; }
        [AttributeIndex(STT = 50)]
        public PropertiesAttribute TriGiaFOB_TienDieuChinh_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 51)]
        public PropertiesAttribute PhiVanChuyen { get; set; }
        [AttributeIndex(STT = 52)]
        public PropertiesAttribute PhiVanChuyen_MaTienTe { get; set; }
        [AttributeIndex(STT = 53)]
        public PropertiesAttribute PhiVanChuyen_NoiThanhToan { get; set; }
        [AttributeIndex(STT = 54)]
        public PropertiesAttribute ChiPhiXepHang1 { get; set; }
        [AttributeIndex(STT = 55)]
        public PropertiesAttribute ChiPhiXepHang1_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 56)]
        public PropertiesAttribute ChiPhiXepHang1_LoaiPhi { get; set; }
        [AttributeIndex(STT = 57)]
        public PropertiesAttribute ChiPhiXepHang2 { get; set; }
        [AttributeIndex(STT = 58)]
        public PropertiesAttribute ChiPhiXepHang2_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 59)]
        public PropertiesAttribute ChiPhiXepHang2_LoaiPhi { get; set; }
        [AttributeIndex(STT = 60)]
        public PropertiesAttribute PhiVanChuyenDuongBo { get; set; }
        [AttributeIndex(STT = 61)]
        public PropertiesAttribute PhiVanChuyenDuongBo_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 62)]
        public PropertiesAttribute PhiBaoHiem { get; set; }
        [AttributeIndex(STT = 63)]
        public PropertiesAttribute PhiBaoHiem_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 64)]
        public PropertiesAttribute PhiBaoHiem_SoTienDieuChinh { get; set; }
        [AttributeIndex(STT = 65)]
        public PropertiesAttribute PhiBaoHiem_SoTienDieuChinh_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 66)]
        public PropertiesAttribute SoTienKhauTru { get; set; }
        [AttributeIndex(STT = 67)]
        public PropertiesAttribute SoTienKhauTru_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 68)]
        public PropertiesAttribute SoTienKhauTru_LoaiKT { get; set; }
        [AttributeIndex(STT = 69)]
        public PropertiesAttribute SoTienDieuChinhKhac { get; set; }
        [AttributeIndex(STT = 70)]
        public PropertiesAttribute SoTienDieuChinhKhac_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 71)]
        public PropertiesAttribute SoTienDieuChinhKhac_Loai { get; set; }
        [AttributeIndex(STT = 72)]
        public PropertiesAttribute TongTriGiaHoaDon { get; set; }
        [AttributeIndex(STT = 73)]
        public PropertiesAttribute TongTriGiaHoaDon_MaNguyenTe { get; set; }
        [AttributeIndex(STT = 74)]
        public PropertiesAttribute DieuKienGiaHD { get; set; }
        [AttributeIndex(STT = 75)]
        public PropertiesAttribute DiaDiemGiaoHang { get; set; }
        [AttributeIndex(STT = 76)]
        public PropertiesAttribute GhiChuDacBiet { get; set; }
        [AttributeIndex(STT = 77)]
        public PropertiesAttribute TongSoDongHang { get; set; }
        [AttributeIndex(STT = 78)]
        public List<IVA_HangHoa> HangHoaTrongHoaDon { get; set; }

        public IVA()
        {

            #region Thông tin chung
            soTiepNhan = new PropertiesAttribute(12, typeof(int));
            PhanLoaiXuatKhau = new PropertiesAttribute(1, typeof(string));
            MaDaiLyHQ = new PropertiesAttribute(5, typeof(string));
            SoHoaDon = new PropertiesAttribute(35, typeof(string));
            NgayLapHoaDon = new PropertiesAttribute(8, typeof(DateTime));
            DiaDiemLapHoaDon = new PropertiesAttribute(35, typeof(string));
            PTTT = new PropertiesAttribute(30, typeof(string));
            #endregion

            #region Chưng từ trên hóa đơn
            List<PropertiesAttribute> listChungTu = new List<PropertiesAttribute>();
            listChungTu.Add(new PropertiesAttribute(3, 11, EnumGroupID.IVA_ChungTuHoaDon_MaPhanLoai, typeof(string)));
            listChungTu.Add(new PropertiesAttribute(35, 11, EnumGroupID.IVA_ChungTuHoaDon_SoChungTu, typeof(string)));
            listChungTu.Add(new PropertiesAttribute(8, 11, EnumGroupID.IVA_ChungTuHoaDon_NgayHoaDon, typeof(DateTime)));
            ChungTuHoaDon = new GroupAttribute("ChungTuHD", 11, listChungTu);
            #endregion

            #region Người xuất nhập khẩu
            NguoiXNK_Ma = new PropertiesAttribute(13, typeof(string));
            NguoiXNK_Ten = new PropertiesAttribute(100, typeof(string));
            NguoiXNK_MaBuuChinh = new PropertiesAttribute(7, typeof(string));
            NguoiXNK_DiaChi = new PropertiesAttribute(100, typeof(string));
            NguoiXNK_DienThoai = new PropertiesAttribute(20, typeof(string));
            #endregion

            NguoiLapHoaDon = new PropertiesAttribute(60, typeof(string));

            #region Người gửi / nhận
            NguoiGuiNhan_Ma = new PropertiesAttribute(13, typeof(string));
            NguoiGuiNhan_Ten = new PropertiesAttribute(70, typeof(string));
            NguoiGuiNhan_MaBuuChinh = new PropertiesAttribute(9, typeof(string));
            NguoiGuiNhan_DiaChi1 = new PropertiesAttribute(35, typeof(string));
            NguoiGuiNhan_DiaChi2 = new PropertiesAttribute(35, typeof(string));
            NguoiGuiNhan_DiaChi3 = new PropertiesAttribute(35, typeof(string));
            NguoiGuiNhan_DiaChi4 = new PropertiesAttribute(35, typeof(string));
            NguoiGuiNhan_SoDienThoai = new PropertiesAttribute(12, typeof(string));
            NguoiGuiNhan_Nuoc = new PropertiesAttribute(2, typeof(string));
            #endregion

            #region Phương tiện vận chuyển
            MaKyHieu = new PropertiesAttribute(140);
            PhanLoaiVC = new PropertiesAttribute(4);
            TenPhuongTienVC = new PropertiesAttribute(35);
            SoHieuChuyenDi = new PropertiesAttribute(10);
            #endregion

            #region Địa điểm xếp hàng, dở hàng, trung chuyển
            DiaDiemXepHang_Ma = new PropertiesAttribute(5);
            DiaDiemXepHang_Ten = new PropertiesAttribute(20);
            DiaDiemXepHang_NgayXepHang = new PropertiesAttribute(8, typeof(DateTime));

            DiaDiemDohang_Ma = new PropertiesAttribute(5);
            DiaDiemDohang_Ten = new PropertiesAttribute(20);
            DiaDiemTrungChuyen_Ma = new PropertiesAttribute(5);
            DiaDiemTrungChuyen_Ten = new PropertiesAttribute(30);
            #endregion

            #region Trọng lượng, số lượng
            TongTrongLuong = new PropertiesAttribute(10, typeof(double));
            TongTrongLuong_DVT = new PropertiesAttribute(3);

            TrongLuongTinh = new PropertiesAttribute(10, typeof(double));
            TrongLuongTinh_DVT = new PropertiesAttribute(3);

            TheTich = new PropertiesAttribute(10, typeof(double));
            TheTich_DVT = new PropertiesAttribute(3);

            TongSoKien = new PropertiesAttribute(8, typeof(int));
            TongSoKien_DVT = new PropertiesAttribute(3);
            #endregion

            GhiChu = new PropertiesAttribute(70);

            SoPL = new PropertiesAttribute(10);
            NganHangLC = new PropertiesAttribute(80);

            #region Giá FOB
            TriGiaFOB = new PropertiesAttribute(20, typeof(decimal));
            TriGiaFOB_MaNguyenTe = new PropertiesAttribute(3);
            TriGiaFOB_TienDieuChinh = new PropertiesAttribute(13, typeof(decimal));
            TriGiaFOB_TienDieuChinh_MaNguyenTe = new PropertiesAttribute(3);
            #endregion

            #region Phí
            PhiVanChuyen = new PropertiesAttribute(18, typeof(decimal));
            PhiVanChuyen_MaTienTe = new PropertiesAttribute(3);
            PhiVanChuyen_NoiThanhToan = new PropertiesAttribute(20);

            ChiPhiXepHang1 = new PropertiesAttribute(9, typeof(decimal));
            ChiPhiXepHang1_MaNguyenTe = new PropertiesAttribute(3);
            ChiPhiXepHang1_LoaiPhi = new PropertiesAttribute(20);

            ChiPhiXepHang2 = new PropertiesAttribute(9, typeof(decimal));
            ChiPhiXepHang2_MaNguyenTe = new PropertiesAttribute(3);
            ChiPhiXepHang2_LoaiPhi = new PropertiesAttribute(20);

            PhiVanChuyenDuongBo = new PropertiesAttribute(9, typeof(decimal));
            PhiVanChuyenDuongBo_MaNguyenTe = new PropertiesAttribute(3);

            PhiBaoHiem = new PropertiesAttribute(16, typeof(decimal));
            PhiBaoHiem_MaNguyenTe = new PropertiesAttribute(3);
            PhiBaoHiem_SoTienDieuChinh = new PropertiesAttribute(9, typeof(decimal));
            PhiBaoHiem_SoTienDieuChinh_MaNguyenTe = new PropertiesAttribute(3);
            #endregion

            #region Khấu trừ - điều chỉnh
            SoTienKhauTru = new PropertiesAttribute(9, typeof(decimal));
            SoTienKhauTru_MaNguyenTe = new PropertiesAttribute(3);
            SoTienKhauTru_LoaiKT = new PropertiesAttribute(20);

            SoTienDieuChinhKhac = new PropertiesAttribute(9, typeof(decimal));
            SoTienDieuChinhKhac_MaNguyenTe = new PropertiesAttribute(3);
            SoTienDieuChinhKhac_Loai = new PropertiesAttribute(20);
            #endregion

            TongTriGiaHoaDon = new PropertiesAttribute(20, typeof(decimal));
            TongTriGiaHoaDon_MaNguyenTe = new PropertiesAttribute(3);
            DieuKienGiaHD = new PropertiesAttribute(20);

            DiaDiemGiaoHang = new PropertiesAttribute(30);
            GhiChuDacBiet = new PropertiesAttribute(100);
            TongSoDongHang = new PropertiesAttribute(4, typeof(int));

            HangHoaTrongHoaDon = new List<IVA_HangHoa>();
        }
        public StringBuilder BuilEdiMessagesIVA( StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<IVA>(StrBuild,true,GlobalVNACC.PathConfig,"IVA");
            foreach (IVA_HangHoa item in HangHoaTrongHoaDon)
            {
                item.BuildEdiMessages<IVA_HangHoa>(StrBuild, true, GlobalVNACC.PathConfig, "IVA_HangHoa");
            }
            return str;
        }
        

    }
}
