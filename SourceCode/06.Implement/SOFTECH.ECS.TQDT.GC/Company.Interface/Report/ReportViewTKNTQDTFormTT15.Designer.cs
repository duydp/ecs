﻿namespace Company.Interface.Report
{
    partial class ReportViewTKNTQDTFormTT15
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnPrintPDF = new Janus.Windows.EditControls.UIButton();
            this.chkMienThueGTGT = new Janus.Windows.EditControls.UICheckBox();
            this.chkMienThueNK = new Janus.Windows.EditControls.UICheckBox();
            this.chkInBanLuuHaiQuan = new Janus.Windows.EditControls.UICheckBox();
            this.chkInMaHang = new Janus.Windows.EditControls.UICheckBox();
            this.btnPrint = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.cboToKhai = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDeXuatKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblDeXuatKhac = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnApDung = new Janus.Windows.EditControls.UIButton();
            this.txtTenNhomHang = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.printControl1);
            this.grbMain.Size = new System.Drawing.Size(1024, 428);
            // 
            // printControl1
            // 
            this.printControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.printControl1.BackColor = System.Drawing.Color.Empty;
            this.printControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.printControl1.ForeColor = System.Drawing.Color.Empty;
            this.printControl1.IsMetric = false;
            this.printControl1.Location = new System.Drawing.Point(1, 73);
            this.printControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.printControl1.Name = "printControl1";
            this.printControl1.PrintingSystem = this.printingSystem1;
            this.printControl1.Size = new System.Drawing.Size(1021, 353);
            this.printControl1.TabIndex = 1;
            this.printControl1.TooltipFont = new System.Drawing.Font("Tahoma", 8.25F);
            // 
            // printingSystem1
            // 
            this.printingSystem1.ShowMarginsWarning = false;
            this.printingSystem1.ShowPrintStatusDialog = false;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnPrintPDF);
            this.uiGroupBox1.Controls.Add(this.chkMienThueGTGT);
            this.uiGroupBox1.Controls.Add(this.chkMienThueNK);
            this.uiGroupBox1.Controls.Add(this.chkInBanLuuHaiQuan);
            this.uiGroupBox1.Controls.Add(this.chkInMaHang);
            this.uiGroupBox1.Controls.Add(this.btnPrint);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.cboToKhai);
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 3);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(540, 64);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnPrintPDF
            // 
            this.btnPrintPDF.Location = new System.Drawing.Point(408, 18);
            this.btnPrintPDF.Name = "btnPrintPDF";
            this.btnPrintPDF.Size = new System.Drawing.Size(51, 23);
            this.btnPrintPDF.TabIndex = 3;
            this.btnPrintPDF.Text = "In pdf";
            this.btnPrintPDF.VisualStyleManager = this.vsmMain;
            this.btnPrintPDF.Click += new System.EventHandler(this.btnPrintPDF_Click);
            // 
            // chkMienThueGTGT
            // 
            this.chkMienThueGTGT.Location = new System.Drawing.Point(408, 41);
            this.chkMienThueGTGT.Name = "chkMienThueGTGT";
            this.chkMienThueGTGT.Size = new System.Drawing.Size(110, 23);
            this.chkMienThueGTGT.TabIndex = 7;
            this.chkMienThueGTGT.Text = "Miễn thuế GTGT";
            this.chkMienThueGTGT.VisualStyleManager = this.vsmMain;
            // 
            // chkMienThueNK
            // 
            this.chkMienThueNK.Location = new System.Drawing.Point(323, 41);
            this.chkMienThueNK.Name = "chkMienThueNK";
            this.chkMienThueNK.Size = new System.Drawing.Size(79, 23);
            this.chkMienThueNK.TabIndex = 6;
            this.chkMienThueNK.Text = "Miễn thuế NK";
            this.chkMienThueNK.VisualStyleManager = this.vsmMain;
            // 
            // chkInBanLuuHaiQuan
            // 
            this.chkInBanLuuHaiQuan.Location = new System.Drawing.Point(201, 41);
            this.chkInBanLuuHaiQuan.Name = "chkInBanLuuHaiQuan";
            this.chkInBanLuuHaiQuan.Size = new System.Drawing.Size(116, 23);
            this.chkInBanLuuHaiQuan.TabIndex = 5;
            this.chkInBanLuuHaiQuan.Text = "In bản lưu Hải quan";
            this.chkInBanLuuHaiQuan.VisualStyleManager = this.vsmMain;
            this.chkInBanLuuHaiQuan.CheckedChanged += new System.EventHandler(this.chkInBanLuuHaiQuan_CheckedChanged);
            // 
            // chkInMaHang
            // 
            this.chkInMaHang.Location = new System.Drawing.Point(116, 41);
            this.chkInMaHang.Name = "chkInMaHang";
            this.chkInMaHang.Size = new System.Drawing.Size(79, 23);
            this.chkInMaHang.TabIndex = 4;
            this.chkInMaHang.Text = "In mã hàng";
            this.chkInMaHang.VisualStyleManager = this.vsmMain;
            this.chkInMaHang.CheckedChanged += new System.EventHandler(this.chkInMaHang_CheckedChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(364, 18);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(38, 23);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "In";
            this.btnPrint.VisualStyleManager = this.vsmMain;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chọn tờ khai chính / phụ lục";
            // 
            // cboToKhai
            // 
            this.cboToKhai.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Tờ khai chính";
            uiComboBoxItem1.Value = 0;
            this.cboToKhai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1});
            this.cboToKhai.Location = new System.Drawing.Point(200, 19);
            this.cboToKhai.Name = "cboToKhai";
            this.cboToKhai.Size = new System.Drawing.Size(158, 21);
            this.cboToKhai.TabIndex = 1;
            this.cboToKhai.VisualStyleManager = this.vsmMain;
            this.cboToKhai.SelectedIndexChanged += new System.EventHandler(this.cboToKhai_SelectedIndexChanged);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtDeXuatKhac);
            this.uiGroupBox2.Controls.Add(this.lblDeXuatKhac);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.btnApDung);
            this.uiGroupBox2.Controls.Add(this.txtTenNhomHang);
            this.uiGroupBox2.Location = new System.Drawing.Point(546, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(475, 64);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtDeXuatKhac
            // 
            this.txtDeXuatKhac.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeXuatKhac.Location = new System.Drawing.Point(113, 41);
            this.txtDeXuatKhac.Name = "txtDeXuatKhac";
            this.txtDeXuatKhac.Size = new System.Drawing.Size(275, 21);
            this.txtDeXuatKhac.TabIndex = 1;
            this.txtDeXuatKhac.VisualStyleManager = this.vsmMain;
            // 
            // lblDeXuatKhac
            // 
            this.lblDeXuatKhac.AutoSize = true;
            this.lblDeXuatKhac.Location = new System.Drawing.Point(17, 46);
            this.lblDeXuatKhac.Name = "lblDeXuatKhac";
            this.lblDeXuatKhac.Size = new System.Drawing.Size(71, 13);
            this.lblDeXuatKhac.TabIndex = 3;
            this.lblDeXuatKhac.Text = "Đề xuất khác";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên hàng 1";
            // 
            // btnApDung
            // 
            this.btnApDung.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApDung.Location = new System.Drawing.Point(394, 17);
            this.btnApDung.Name = "btnApDung";
            this.btnApDung.Size = new System.Drawing.Size(75, 23);
            this.btnApDung.TabIndex = 2;
            this.btnApDung.Text = "Áp dụng";
            this.btnApDung.VisualStyleManager = this.vsmMain;
            this.btnApDung.Click += new System.EventHandler(this.btnApDung_Click);
            // 
            // txtTenNhomHang
            // 
            this.txtTenNhomHang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenNhomHang.Location = new System.Drawing.Point(113, 19);
            this.txtTenNhomHang.Name = "txtTenNhomHang";
            this.txtTenNhomHang.Size = new System.Drawing.Size(275, 21);
            this.txtTenNhomHang.TabIndex = 0;
            this.txtTenNhomHang.VisualStyleManager = this.vsmMain;
            // 
            // ReportViewTKNTQDTFormTT15
            // 
            this.AcceptButton = this.btnPrint;
            this.ClientSize = new System.Drawing.Size(1024, 428);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ReportViewTKNTQDTFormTT15";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "In tờ khai nhập giấy A4";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReportViewTKXTQDTFormTT15_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cboToKhai;
        private Janus.Windows.EditControls.UIButton btnPrint;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnApDung;
        public System.Windows.Forms.Label label3;
        public Janus.Windows.GridEX.EditControls.EditBox txtTenNhomHang;
        private Janus.Windows.EditControls.UICheckBox chkInMaHang;
        private Janus.Windows.EditControls.UICheckBox chkInBanLuuHaiQuan;
        private Janus.Windows.EditControls.UICheckBox chkMienThueGTGT;
        private Janus.Windows.EditControls.UICheckBox chkMienThueNK;
        public Janus.Windows.GridEX.EditControls.EditBox txtDeXuatKhac;
        public System.Windows.Forms.Label lblDeXuatKhac;
        private Janus.Windows.EditControls.UIButton btnPrintPDF;
    }
}