using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_CungUng_ChungTu : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string SoChungTu { set; get; }
		public DateTime NgayChungTu { set; get; }
		public int LoaiChungTu { set; get; }
		public string NoiCap { set; get; }
		public decimal SoLuong { set; get; }
        public decimal DonGia { set; get; }
        public decimal TriGia { set; get; }
        public decimal TyGia { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_CungUng_ChungTu> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_CungUng_ChungTu> collection = new List<KDT_GC_CungUng_ChungTu>();
			while (reader.Read())
			{
				KDT_GC_CungUng_ChungTu entity = new KDT_GC_CungUng_ChungTu();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetInt32(reader.GetOrdinal("LoaiChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiCap"))) entity.NoiCap = reader.GetString(reader.GetOrdinal("NoiCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGia"))) entity.TyGia = reader.GetDecimal(reader.GetOrdinal("TyGia"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_CungUng_ChungTu> collection, long id)
        {
            foreach (KDT_GC_CungUng_ChungTu item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_CungUng_ChungTu VALUES(@Master_ID, @SoChungTu, @NgayChungTu, @LoaiChungTu, @NoiCap, @SoLuong,@DonGia,@TriGia,@TyGia)";
            string update = "UPDATE t_KDT_GC_CungUng_ChungTu SET Master_ID = @Master_ID, SoChungTu = @SoChungTu, NgayChungTu = @NgayChungTu, LoaiChungTu = @LoaiChungTu, NoiCap = @NoiCap, SoLuong = @SoLuong,DonGia = @DonGia,TriGia = @TriGia, TyGia = @TyGia WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_CungUng_ChungTu WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.VarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.Int, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCap", SqlDbType.NVarChar, "NoiCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TyGia", SqlDbType.Decimal, "TyGia", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.VarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.Int, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCap", SqlDbType.NVarChar, "NoiCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TyGia", SqlDbType.Decimal, "TyGia", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_CungUng_ChungTu VALUES(@Master_ID, @SoChungTu, @NgayChungTu, @LoaiChungTu, @NoiCap, @SoLuong,@DonGia,@TriGia,@TyGia)";
            string update = "UPDATE t_KDT_GC_CungUng_ChungTu SET Master_ID = @Master_ID, SoChungTu = @SoChungTu, NgayChungTu = @NgayChungTu, LoaiChungTu = @LoaiChungTu, NoiCap = @NoiCap, SoLuong = @SoLuong,DonGia = @DonGia,TriGia = @TriGia, TyGia = @TyGia WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_CungUng_ChungTu WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.VarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.Int, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCap", SqlDbType.NVarChar, "NoiCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TyGia", SqlDbType.Decimal, "TyGia", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.VarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.Int, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCap", SqlDbType.NVarChar, "NoiCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TyGia", SqlDbType.Decimal, "TyGia", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_CungUng_ChungTu Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_CungUng_ChungTu_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_CungUng_ChungTu> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_CungUng_ChungTu> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_CungUng_ChungTu> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_CungUng_ChungTu_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_CungUng_ChungTu_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_CungUng_ChungTu_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_CungUng_ChungTu_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_CungUng_ChungTu(long master_ID, string soChungTu, DateTime ngayChungTu, int loaiChungTu, string noiCap, decimal soLuong,decimal donGia,decimal triGia,decimal tyGia)
		{
			KDT_GC_CungUng_ChungTu entity = new KDT_GC_CungUng_ChungTu();	
			entity.Master_ID = master_ID;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.LoaiChungTu = loaiChungTu;
			entity.NoiCap = noiCap;
			entity.SoLuong = soLuong;
            entity.DonGia = donGia;
            entity.TriGia = triGia;
            entity.TyGia = tyGia;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_CungUng_ChungTu_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.VarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.Int, LoaiChungTu);
			db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
            db.AddInParameter(dbCommand, "@TyGia", SqlDbType.Decimal, TyGia);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_CungUng_ChungTu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_CungUng_ChungTu item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_CungUng_ChungTu(long id, long master_ID, string soChungTu, DateTime ngayChungTu, int loaiChungTu, string noiCap, decimal soLuong,decimal donGia,decimal triGia,decimal tyGia)
		{
			KDT_GC_CungUng_ChungTu entity = new KDT_GC_CungUng_ChungTu();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.LoaiChungTu = loaiChungTu;
			entity.NoiCap = noiCap;
			entity.SoLuong = soLuong;
            entity.DonGia = donGia;
            entity.TriGia = triGia;
            entity.TyGia = tyGia;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_CungUng_ChungTu_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.VarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.Int, LoaiChungTu);
			db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
            db.AddInParameter(dbCommand, "@TyGia", SqlDbType.Decimal, TyGia);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_CungUng_ChungTu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_CungUng_ChungTu item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_CungUng_ChungTu(long id, long master_ID, string soChungTu, DateTime ngayChungTu, int loaiChungTu, string noiCap, decimal soLuong,decimal donGia,decimal triGia, decimal tyGia)
		{
			KDT_GC_CungUng_ChungTu entity = new KDT_GC_CungUng_ChungTu();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.LoaiChungTu = loaiChungTu;
			entity.NoiCap = noiCap;
			entity.SoLuong = soLuong;
            entity.DonGia = donGia;
            entity.TriGia = triGia;
            entity.TyGia = tyGia;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_CungUng_ChungTu_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.VarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.Int, LoaiChungTu);
			db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
            db.AddInParameter(dbCommand, "@TyGia", SqlDbType.Decimal, TyGia);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_CungUng_ChungTu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_CungUng_ChungTu item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_CungUng_ChungTu(long id)
		{
			KDT_GC_CungUng_ChungTu entity = new KDT_GC_CungUng_ChungTu();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_CungUng_ChungTu_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_CungUng_ChungTu_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_CungUng_ChungTu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_CungUng_ChungTu item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}