﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class EquipmentIdentification
    {
        [XmlElement("identification")]
        public string identification { get; set; }
    }
}
