/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V4_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 04/17/2013 2:29:50 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ADD
[IsHangDongBo] [bit] NULL,
[CheDoUuDai] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_LoadBy2]'
GO
    
    
    
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Load]    
-- Database: HaiQuan    
-- Author: Ngo Thanh Tung    
-- Time created: Tuesday, October 28, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_LoadBy2]  
    @TKMD_ID BIGINT ,  
    @MaHS VARCHAR(12) ,  
    @MaPhu VARCHAR(30) ,  
    @TenHang NVARCHAR(80) ,  
    @NuocXX_ID CHAR(3) ,  
    @DVT_ID CHAR(3)
AS   
    SET NOCOUNT ON    
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
    SELECT  		[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[ThueSuatGiam],
	[FOC],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueBVMT],
	[BieuThueGTGT],
	[BieuThueCBPG],
	[ThongTinKhac],
	[IsHangDongBo],
	[CheDoUuDai]
    FROM    [dbo].[t_KDT_HangMauDich]  
    WHERE   TKMD_ID = @TKMD_ID  
            AND MaHS = @MaHS  
            AND MaPhu = @MaPhu  
            AND TenHang = @TenHang  
            AND NuocXX_ID = @NuocXX_ID  
            AND DVT_ID = @DVT_ID   
    
    
    
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Insert]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Insert]
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@TrongLuong numeric(18, 3),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@ThueSuatGiam varchar(50),
	@FOC bit,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam float,
	@ThueSuatTTDBGiam float,
	@ThueSuatVATGiam float,
	@ThueBVMT money,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia money,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@isHangCu bit,
	@BieuThueXNK nvarchar(255),
	@BieuThueTTDB nvarchar(255),
	@BieuThueBVMT nvarchar(255),
	@BieuThueGTGT nvarchar(255),
	@BieuThueCBPG nvarchar(255),
	@ThongTinKhac nvarchar(2000),
	@IsHangDongBo bit,
	@CheDoUuDai nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_HangMauDich]
(
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[ThueSuatGiam],
	[FOC],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueBVMT],
	[BieuThueGTGT],
	[BieuThueCBPG],
	[ThongTinKhac],
	[IsHangDongBo],
	[CheDoUuDai]
)
VALUES 
(
	@TKMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@TrongLuong,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaKB,
	@TriGiaTT,
	@TriGiaKB_VND,
	@ThueSuatXNK,
	@ThueSuatTTDB,
	@ThueSuatGTGT,
	@ThueXNK,
	@ThueTTDB,
	@ThueGTGT,
	@PhuThu,
	@TyLeThuKhac,
	@TriGiaThuKhac,
	@MienThue,
	@Ma_HTS,
	@DVT_HTS,
	@SoLuong_HTS,
	@ThueSuatGiam,
	@FOC,
	@DonGiaTuyetDoi,
	@MaHSMoRong,
	@NhanHieu,
	@QuyCachPhamChat,
	@ThanhPhan,
	@Model,
	@MaHangSX,
	@TenHangSX,
	@ThueTuyetDoi,
	@ThueSuatXNKGiam,
	@ThueSuatTTDBGiam,
	@ThueSuatVATGiam,
	@ThueBVMT,
	@ThueSuatBVMT,
	@ThueSuatBVMTGiam,
	@ThueChongPhaGia,
	@ThueSuatChongPhaGia,
	@ThueSuatChongPhaGiaGiam,
	@isHangCu,
	@BieuThueXNK,
	@BieuThueTTDB,
	@BieuThueBVMT,
	@BieuThueGTGT,
	@BieuThueCBPG,
	@ThongTinKhac,
	@IsHangDongBo,
	@CheDoUuDai
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Update]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@TrongLuong numeric(18, 3),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@ThueSuatGiam varchar(50),
	@FOC bit,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam float,
	@ThueSuatTTDBGiam float,
	@ThueSuatVATGiam float,
	@ThueBVMT money,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia money,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@isHangCu bit,
	@BieuThueXNK nvarchar(255),
	@BieuThueTTDB nvarchar(255),
	@BieuThueBVMT nvarchar(255),
	@BieuThueGTGT nvarchar(255),
	@BieuThueCBPG nvarchar(255),
	@ThongTinKhac nvarchar(2000),
	@IsHangDongBo bit,
	@CheDoUuDai nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[TrongLuong] = @TrongLuong,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaKB] = @TriGiaKB,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaKB_VND] = @TriGiaKB_VND,
	[ThueSuatXNK] = @ThueSuatXNK,
	[ThueSuatTTDB] = @ThueSuatTTDB,
	[ThueSuatGTGT] = @ThueSuatGTGT,
	[ThueXNK] = @ThueXNK,
	[ThueTTDB] = @ThueTTDB,
	[ThueGTGT] = @ThueGTGT,
	[PhuThu] = @PhuThu,
	[TyLeThuKhac] = @TyLeThuKhac,
	[TriGiaThuKhac] = @TriGiaThuKhac,
	[MienThue] = @MienThue,
	[Ma_HTS] = @Ma_HTS,
	[DVT_HTS] = @DVT_HTS,
	[SoLuong_HTS] = @SoLuong_HTS,
	[ThueSuatGiam] = @ThueSuatGiam,
	[FOC] = @FOC,
	[DonGiaTuyetDoi] = @DonGiaTuyetDoi,
	[MaHSMoRong] = @MaHSMoRong,
	[NhanHieu] = @NhanHieu,
	[QuyCachPhamChat] = @QuyCachPhamChat,
	[ThanhPhan] = @ThanhPhan,
	[Model] = @Model,
	[MaHangSX] = @MaHangSX,
	[TenHangSX] = @TenHangSX,
	[ThueTuyetDoi] = @ThueTuyetDoi,
	[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
	[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
	[ThueSuatVATGiam] = @ThueSuatVATGiam,
	[ThueBVMT] = @ThueBVMT,
	[ThueSuatBVMT] = @ThueSuatBVMT,
	[ThueSuatBVMTGiam] = @ThueSuatBVMTGiam,
	[ThueChongPhaGia] = @ThueChongPhaGia,
	[ThueSuatChongPhaGia] = @ThueSuatChongPhaGia,
	[ThueSuatChongPhaGiaGiam] = @ThueSuatChongPhaGiaGiam,
	[isHangCu] = @isHangCu,
	[BieuThueXNK] = @BieuThueXNK,
	[BieuThueTTDB] = @BieuThueTTDB,
	[BieuThueBVMT] = @BieuThueBVMT,
	[BieuThueGTGT] = @BieuThueGTGT,
	[BieuThueCBPG] = @BieuThueCBPG,
	[ThongTinKhac] = @ThongTinKhac,
	[IsHangDongBo] = @IsHangDongBo,
	[CheDoUuDai] = @CheDoUuDai
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@TrongLuong numeric(18, 3),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@ThueSuatGiam varchar(50),
	@FOC bit,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam float,
	@ThueSuatTTDBGiam float,
	@ThueSuatVATGiam float,
	@ThueBVMT money,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia money,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@isHangCu bit,
	@BieuThueXNK nvarchar(255),
	@BieuThueTTDB nvarchar(255),
	@BieuThueBVMT nvarchar(255),
	@BieuThueGTGT nvarchar(255),
	@BieuThueCBPG nvarchar(255),
	@ThongTinKhac nvarchar(2000),
	@IsHangDongBo bit,
	@CheDoUuDai nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue,
			[Ma_HTS] = @Ma_HTS,
			[DVT_HTS] = @DVT_HTS,
			[SoLuong_HTS] = @SoLuong_HTS,
			[ThueSuatGiam] = @ThueSuatGiam,
			[FOC] = @FOC,
			[DonGiaTuyetDoi] = @DonGiaTuyetDoi,
			[MaHSMoRong] = @MaHSMoRong,
			[NhanHieu] = @NhanHieu,
			[QuyCachPhamChat] = @QuyCachPhamChat,
			[ThanhPhan] = @ThanhPhan,
			[Model] = @Model,
			[MaHangSX] = @MaHangSX,
			[TenHangSX] = @TenHangSX,
			[ThueTuyetDoi] = @ThueTuyetDoi,
			[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
			[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
			[ThueSuatVATGiam] = @ThueSuatVATGiam,
			[ThueBVMT] = @ThueBVMT,
			[ThueSuatBVMT] = @ThueSuatBVMT,
			[ThueSuatBVMTGiam] = @ThueSuatBVMTGiam,
			[ThueChongPhaGia] = @ThueChongPhaGia,
			[ThueSuatChongPhaGia] = @ThueSuatChongPhaGia,
			[ThueSuatChongPhaGiaGiam] = @ThueSuatChongPhaGiaGiam,
			[isHangCu] = @isHangCu,
			[BieuThueXNK] = @BieuThueXNK,
			[BieuThueTTDB] = @BieuThueTTDB,
			[BieuThueBVMT] = @BieuThueBVMT,
			[BieuThueGTGT] = @BieuThueGTGT,
			[BieuThueCBPG] = @BieuThueCBPG,
			[ThongTinKhac] = @ThongTinKhac,
			[IsHangDongBo] = @IsHangDongBo,
			[CheDoUuDai] = @CheDoUuDai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangMauDich]
		(
			[TKMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[TrongLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue],
			[Ma_HTS],
			[DVT_HTS],
			[SoLuong_HTS],
			[ThueSuatGiam],
			[FOC],
			[DonGiaTuyetDoi],
			[MaHSMoRong],
			[NhanHieu],
			[QuyCachPhamChat],
			[ThanhPhan],
			[Model],
			[MaHangSX],
			[TenHangSX],
			[ThueTuyetDoi],
			[ThueSuatXNKGiam],
			[ThueSuatTTDBGiam],
			[ThueSuatVATGiam],
			[ThueBVMT],
			[ThueSuatBVMT],
			[ThueSuatBVMTGiam],
			[ThueChongPhaGia],
			[ThueSuatChongPhaGia],
			[ThueSuatChongPhaGiaGiam],
			[isHangCu],
			[BieuThueXNK],
			[BieuThueTTDB],
			[BieuThueBVMT],
			[BieuThueGTGT],
			[BieuThueCBPG],
			[ThongTinKhac],
			[IsHangDongBo],
			[CheDoUuDai]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@TrongLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue,
			@Ma_HTS,
			@DVT_HTS,
			@SoLuong_HTS,
			@ThueSuatGiam,
			@FOC,
			@DonGiaTuyetDoi,
			@MaHSMoRong,
			@NhanHieu,
			@QuyCachPhamChat,
			@ThanhPhan,
			@Model,
			@MaHangSX,
			@TenHangSX,
			@ThueTuyetDoi,
			@ThueSuatXNKGiam,
			@ThueSuatTTDBGiam,
			@ThueSuatVATGiam,
			@ThueBVMT,
			@ThueSuatBVMT,
			@ThueSuatBVMTGiam,
			@ThueChongPhaGia,
			@ThueSuatChongPhaGia,
			@ThueSuatChongPhaGiaGiam,
			@isHangCu,
			@BieuThueXNK,
			@BieuThueTTDB,
			@BieuThueBVMT,
			@BieuThueGTGT,
			@BieuThueCBPG,
			@ThongTinKhac,
			@IsHangDongBo,
			@CheDoUuDai
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Delete]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HangMauDich]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HangMauDich]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Load]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[ThueSuatGiam],
	[FOC],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueBVMT],
	[BieuThueGTGT],
	[BieuThueCBPG],
	[ThongTinKhac],
	[IsHangDongBo],
	[CheDoUuDai]
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[ThueSuatGiam],
	[FOC],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueBVMT],
	[BieuThueGTGT],
	[BieuThueCBPG],
	[ThongTinKhac],
	[IsHangDongBo],
	[CheDoUuDai]
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[ThueSuatGiam],
	[FOC],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueBVMT],
	[BieuThueGTGT],
	[BieuThueCBPG],
	[ThongTinKhac],
	[IsHangDongBo],
	[CheDoUuDai]
FROM
	[dbo].[t_KDT_HangMauDich]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_LoadBy]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Load]
-- Database: HaiQuan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 28, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_LoadBy]
	@TKMD_ID bigint,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB numeric(38, 15),
	@ThueSuatXNK numeric(5, 2)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
		[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[ThueSuatGiam],
	[FOC],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueBVMT],
	[BieuThueGTGT],
	[BieuThueCBPG],
	[ThongTinKhac],
	[IsHangDongBo],
	[CheDoUuDai]
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	TKMD_ID = @TKMD_ID AND
	MaHS = @MaHS AND
	MaPhu = @MaPhu AND
	TenHang = @TenHang AND
	NuocXX_ID = @NuocXX_ID AND
	DVT_ID = @DVT_ID AND
	SoLuong = @SoLuong AND
	DonGiaKB = @DonGiaKB AND
	ThueSuatXNK = @ThueSuatXNK




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_InsertUpdateBy]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]
-- Database: HaiQuan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 28, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdateBy]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@TrongLuong numeric(18, 3),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@ThueSuatGiam varchar(50),
	@FOC bit,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam float,
	@ThueSuatTTDBGiam float,
	@ThueSuatVATGiam float,
	@ThueBVMT money,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia money,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@isHangCu bit,
	@BieuThueXNK nvarchar(255),
	@BieuThueTTDB nvarchar(255),
	@BieuThueBVMT nvarchar(255),
	@BieuThueGTGT nvarchar(255),
	@BieuThueCBPG nvarchar(255),
	@ThongTinKhac nvarchar(2000),
	@IsHangDongBo bit,
	@CheDoUuDai nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] 
		WHERE 	
			TKMD_ID = @TKMD_ID AND
			MaHS = @MaHS AND
			MaPhu = @MaPhu AND
			TenHang = @TenHang AND
			NuocXX_ID = @NuocXX_ID AND
			DVT_ID = @DVT_ID AND
			SoLuong = @SoLuong
)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue,
			[Ma_HTS] = @Ma_HTS,
			[DVT_HTS] = @DVT_HTS,
			[SoLuong_HTS] = @SoLuong_HTS,
			[ThueSuatGiam] = @ThueSuatGiam,
			[FOC] = @FOC,
			[DonGiaTuyetDoi] = @DonGiaTuyetDoi,
			[MaHSMoRong] = @MaHSMoRong,
			[NhanHieu] = @NhanHieu,
			[QuyCachPhamChat] = @QuyCachPhamChat,
			[ThanhPhan] = @ThanhPhan,
			[Model] = @Model,
			[MaHangSX] = @MaHangSX,
			[TenHangSX] = @TenHangSX,
			[ThueTuyetDoi] = @ThueTuyetDoi,
			[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
			[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
			[ThueSuatVATGiam] = @ThueSuatVATGiam,
			[ThueBVMT] = @ThueBVMT,
			[ThueSuatBVMT] = @ThueSuatBVMT,
			[ThueSuatBVMTGiam] = @ThueSuatBVMTGiam,
			[ThueChongPhaGia] = @ThueChongPhaGia,
			[ThueSuatChongPhaGia] = @ThueSuatChongPhaGia,
			[ThueSuatChongPhaGiaGiam] = @ThueSuatChongPhaGiaGiam,
			[isHangCu] = @isHangCu,
			[BieuThueXNK] = @BieuThueXNK,
			[BieuThueTTDB] = @BieuThueTTDB,
			[BieuThueBVMT] = @BieuThueBVMT,
			[BieuThueGTGT] = @BieuThueGTGT,
			[BieuThueCBPG] = @BieuThueCBPG,
			[ThongTinKhac] = @ThongTinKhac,
			[IsHangDongBo] = @IsHangDongBo,
			[CheDoUuDai] = @CheDoUuDai
		WHERE
			TKMD_ID = @TKMD_ID AND
			MaHS = @MaHS AND
			MaPhu = @MaPhu AND
			TenHang = @TenHang AND
			NuocXX_ID = @NuocXX_ID AND
			DVT_ID = @DVT_ID AND
			SoLuong = @SoLuong
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangMauDich]
		(
			[TKMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[TrongLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue],
			[Ma_HTS],
			[DVT_HTS],
			[SoLuong_HTS],
			[ThueSuatGiam],
			[FOC],
			[DonGiaTuyetDoi],
			[MaHSMoRong],
			[NhanHieu],
			[QuyCachPhamChat],
			[ThanhPhan],
			[Model],
			[MaHangSX],
			[TenHangSX],
			[ThueTuyetDoi],
			[ThueSuatXNKGiam],
			[ThueSuatTTDBGiam],
			[ThueSuatVATGiam],
			[ThueBVMT],
			[ThueSuatBVMT],
			[ThueSuatBVMTGiam],
			[ThueChongPhaGia],
			[ThueSuatChongPhaGia],
			[ThueSuatChongPhaGiaGiam],
			[isHangCu],
			[BieuThueXNK],
			[BieuThueTTDB],
			[BieuThueBVMT],
			[BieuThueGTGT],
			[BieuThueCBPG],
			[ThongTinKhac],
			[IsHangDongBo],
			[CheDoUuDai]
		)
		VALUES 
		(
		@TKMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@TrongLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue,
			@Ma_HTS,
			@DVT_HTS,
			@SoLuong_HTS,
			@ThueSuatGiam,
			@FOC,
			@DonGiaTuyetDoi,
			@MaHSMoRong,
			@NhanHieu,
			@QuyCachPhamChat,
			@ThanhPhan,
			@Model,
			@MaHangSX,
			@TenHangSX,
			@ThueTuyetDoi,
			@ThueSuatXNKGiam,
			@ThueSuatTTDBGiam,
			@ThueSuatVATGiam,
			@ThueBVMT,
			@ThueSuatBVMT,
			@ThueSuatBVMTGiam,
			@ThueChongPhaGia,
			@ThueSuatChongPhaGia,
			@ThueSuatChongPhaGiaGiam,
			@isHangCu,
			@BieuThueXNK,
			@BieuThueTTDB,
			@BieuThueBVMT,
			@BieuThueGTGT,
			@BieuThueCBPG,
			@ThongTinKhac,
			@IsHangDongBo,
			@CheDoUuDai
		)		
	END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HangMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 17, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[ThueSuatGiam],
	[FOC],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueBVMT],
	[BieuThueGTGT],
	[BieuThueCBPG],
	[ThongTinKhac],
	[IsHangDongBo],
	[CheDoUuDai]
FROM [dbo].[t_KDT_HangMauDich] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '8.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('8.5',GETDATE(), N'Cap nhat che do uu dai')
END