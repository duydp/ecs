
/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKToKhaiXuat_CheckDuplicateTK]    Script Date: 05/10/2013 10:09:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKToKhaiXuat_CheckDuplicateTK]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiXuat_CheckDuplicateTK]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKToKhaiNhap_CheckDuplicateTK]    Script Date: 05/10/2013 10:09:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKToKhaiNhap_CheckDuplicateTK]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiNhap_CheckDuplicateTK]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKToKhaiXuat_CheckDuplicateTK]    Script Date: 05/10/2013 10:09:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiXuat_CheckDuplicateTK]
	@BangKeHoSoThanhLy_ID bigint
AS

SELECT [SoToKhai], [MaLoaiHinh], [NamDangKy],	[MaHaiQuan], [NgayDangKy]
FROM
	[dbo].[t_KDT_SXXK_BKToKhaiXuat]
WHERE
	[BangKeHoSoThanhLy_ID] = @BangKeHoSoThanhLy_ID
GROUP BY [SoToKhai], [MaLoaiHinh], [NamDangKy],	[MaHaiQuan], [NgayDangKy]
HAVING COUNT(*) > 1	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKToKhaiNhap_CheckDuplicateTK]    Script Date: 05/10/2013 10:09:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiNhap_CheckDuplicateTK]
	@BangKeHoSoThanhLy_ID bigint
AS

SELECT	[SoToKhai], [MaLoaiHinh], [NamDangKy], [MaHaiQuan], [NgayDangKy]
FROM
	[dbo].[t_KDT_SXXK_BKToKhaiNhap]
WHERE
	[BangKeHoSoThanhLy_ID] = @BangKeHoSoThanhLy_ID
GROUP BY [SoToKhai], [MaLoaiHinh], [NamDangKy],	[MaHaiQuan], [NgayDangKy]	
HAVING COUNT(*) > 1




GO



/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKToKhaiNhap_KiemTraChenhLechSoLuongHMD]    Script Date: 05/11/2013 11:16:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKToKhaiNhap_KiemTraChenhLechSoLuongHMD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiNhap_KiemTraChenhLechSoLuongHMD]
GO



/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKToKhaiNhap_KiemTraChenhLechSoLuongHMD]    Script Date: 05/11/2013 11:16:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		KhanhHN	
-- Create date: 11/05/2013
-- Description:	Kiem tra du lieu truoc khi thanh khoan
-- =============================================
CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiNhap_KiemTraChenhLechSoLuongHMD] 
	-- Add the parameters for the stored procedure here
	@LanThanhLy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     HangNhap.SoToKhai, HangNhap.NamDangKy, HangNhap.MaLoaiHinh, HangNhap.MaPhu AS MaNPL, HangNhap.SoLuong, KDT.SoLuongKB, HangNhap.TenHang
FROM         t_SXXK_HangMauDich AS HangNhap INNER JOIN
                          (SELECT     KDT_HMD.MaPhu, KDT_HMD.DVT_ID, KDT_HMD.TKMD_ID, SUM(KDT_HMD.SoLuong) AS SoLuongKB, KDT_Tokhai.SoToKhai, KDT_Tokhai.MaHaiQuan, 
                                                   KDT_Tokhai.MaLoaiHinh, KDT_Tokhai.NamDangKy
                            FROM          t_KDT_HangMauDich AS KDT_HMD INNER JOIN
                                                       (SELECT     SoToKhai, MaLoaiHinh, MaHaiQuan, YEAR(NgayDangKy) AS NamDangKy, ID
                                                         FROM          t_KDT_ToKhaiMauDich
                                                         WHERE      (ID IN
                                                                                    (SELECT     KDT.ID
                                                                                      FROM          (SELECT     SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan
                                                                                                              FROM          t_KDT_SXXK_BKToKhaiNhap
                                                                                                              WHERE      (BangKeHoSoThanhLy_ID =
                                                                                                                                         (SELECT     TOP (1) ID
                                                                                                                                           FROM          t_KDT_SXXK_BangKeHoSoThanhLy
                                                                                                                                           WHERE      (MaBangKe = 'DTLTKN') AND (MaterID =
                                                                                                                                                                      (SELECT     TOP (1) ID
                                                                                                                                                                        FROM          t_KDT_SXXK_HoSoThanhLyDangKy
                                                                                                                                                                        WHERE      (LanThanhLy = @LanThanhLy)))))) AS ThanhKhoan INNER JOIN
                                                                                                             t_KDT_ToKhaiMauDich AS KDT ON KDT.SoToKhai = ThanhKhoan.SoToKhai AND 
                                                                                                             KDT.MaLoaiHinh = ThanhKhoan.MaLoaiHinh AND YEAR(KDT.NgayDangKy) = ThanhKhoan.NamDangKy AND 
                                                                                                             KDT.MaHaiQuan = ThanhKhoan.MaHaiQuan))) AS KDT_Tokhai ON KDT_HMD.TKMD_ID = KDT_Tokhai.ID
                            GROUP BY KDT_HMD.TKMD_ID, KDT_Tokhai.SoToKhai, KDT_Tokhai.MaLoaiHinh, KDT_Tokhai.MaHaiQuan, KDT_Tokhai.NamDangKy, KDT_HMD.MaPhu, 
                                                   KDT_HMD.DVT_ID) AS KDT ON KDT.SoToKhai = HangNhap.SoToKhai AND KDT.MaPhu = HangNhap.MaPhu AND 
                      KDT.MaHaiQuan = HangNhap.MaHaiQuan AND KDT.MaLoaiHinh = HangNhap.MaLoaiHinh AND KDT.NamDangKy = HangNhap.NamDangKy AND 
                      HangNhap.SoLuong <> KDT.SoLuongKB

END

GO





IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '8.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('8.7',GETDATE(), N'Cap nhat kiem tra to khai trung Bang ke to khai nhap, xuat truoc thanh khoan')
END