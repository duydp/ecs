using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.BLL.SXXK.ThanhKhoan
{
    public partial class NPLNhapTon : EntityBase
    {

        protected decimal _TriGiaKB;
        protected string _NuocXX_ID = String.Empty;
        protected string _NguyenTe_ID = String.Empty;
        private int _SoLanThanhLy;
        private int _LanThanhLy;
        private int _TrangThaiThanhKhoan;
        private int _SaiSoLuong;
        private decimal _SoLuongDangKy;
        private string _TienTrinhChayThanhLy = String.Empty;
        public decimal SoToKhaiVNACCS { get; set; }
        public int SoLanThanhLy
        {
            get { return _SoLanThanhLy; }
            set { _SoLanThanhLy = value; }
        }

        public int LanThanhLy
        {
            get { return _LanThanhLy; }
            set { _LanThanhLy = value; }
        }

        public int TrangThaiThanhKhoan
        {
            get { return _TrangThaiThanhKhoan; }
            set { _TrangThaiThanhKhoan = value; }
        }

        public int SaiSoLuong
        {
            get { return _SaiSoLuong; }
            set { _SaiSoLuong = value; }
        }

        public decimal SoLuongDangKy
        {
            get { return _SoLuongDangKy; }
            set { _SoLuongDangKy = value; }
        }

        public string TienTrinhChayThanhLy
        {
            get { return _TienTrinhChayThanhLy; }
            set { _TienTrinhChayThanhLy = value; }
        }

        public string NguyenTe_ID
        {
            set { this._NguyenTe_ID = value; }
            get { return this._NguyenTe_ID; }
        }
        public string NuocXX_ID
        {
            set { this._NuocXX_ID = value; }
            get { return this._NuocXX_ID; }
        }
        public decimal TriGiaKB
        {
            set { this._TriGiaKB = value; }
            get { return this._TriGiaKB; }
        }

        public DataSet SelectThueToKhaiNK(DateTime fromDate, DateTime toDate, string tenChuHang, string maDN)
        {
            string spName = "p_SXXK_SelectThueToKhaiNK";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, fromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, toDate);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, "%" + tenChuHang + "%");
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDN);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public DataSet SelectTriGiaHangToKhaiXK(DateTime fromDate, DateTime toDate, string tenChuHang, string maDN)
        {
            string spName = "p_SXXK_SelectTriGiaHangToKhaiXK";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, fromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, toDate);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, "%" + tenChuHang + "%");
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDN);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public DataSet SelectTriGiaHangToKhaiNK(DateTime fromDate, DateTime toDate, string tenChuHang, string maDN)
        {
            string sql = "SELECT CASE WHEN MaLoaiHinh LIKE '%V%' THEN(Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK =Sotokhai) ELSE SoToKhai END as SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, NgayDangKy,NguyenTe_ID,SoHopDong,NuocXK_ID,TenDonViDoiTac,Sum(TriGiaKB)as TongTriGia FROM( " +
                            "SELECT a.SoToKhai, a.MaLoaiHinh, a.NamDangKy,b.NgayDangKy,b.MaDoanhNghiep,b.SoHopDong,b.NuocXK_ID,b.TenDonViDoiTac, a.MaHaiQuan, a.TriGiaKB,b.NguyenTe_ID FROM dbo.t_SXXK_HangMauDich a " +
                            "INNER JOIN dbo.t_SXXK_ToKhaiMauDich b " +
                            "ON a.SoToKhai = b.SoToKhai " +
                            "AND a.MaLoaiHinh = b.MaLoaiHinh " +
                            "AND a.NamDangKy = b.NamDangKy " +
                            "AND a.MaHaiQuan = b.MaHaiQuan " +
                            "WHERE a.MaLoaiHinh LIKE 'N%' AND TenChuHang LIKE @TenChuHang)c " +
                            "WHERE NgayDangKy BETWEEN @FromDate AND @ToDate AND MaDoanhNghiep = @MaDoanhNghiep " +
                            "GROUP BY SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, NgayDangKy, NguyenTe_ID, SoHopDong,NuocXK_ID,TenDonViDoiTac ";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, fromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, toDate);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, "%" + tenChuHang + "%");
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDN);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public NPLNhapTonCollection SelectCollectionBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            NPLNhapTonCollection collection = new NPLNhapTonCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand, transaction);
            while (reader.Read())
            {
                NPLNhapTon entity = new NPLNhapTon();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public int DeleteTransactionByToKhai(SqlTransaction transaction)
        {
            string spName = "DELETE FROM t_SXXK_ThanhLy_NPLNhapTon WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND NamDangKy = @NamDangKy AND MaHaiQuan = @MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        public bool CheckMaNPLDaThanhKhoan()
        {
            //string sql = "SELECT count(*) FROM t_KDT_SXXK_NPLNhapTon WHERE MaNPL = @MaNPL AND SoToKhai = @SoToKhai AND" +
            //             " NamDangKy = @NamDangKy AND MaLoaiHinh = @MaLoaiHinh AND TonDau > TonCuoi AND LanThanhLy IN " +
            //             " (SELECT LanThanhLy FROM T_KDT_SXXK_HoSoThanhLyDangKy WHERE TrangThaiThanhKhoan = 401) ";
            //SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            //this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this.MaNPL);
            //this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
            //this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
            //this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, this.NamDangKy);
            //return Convert.ToInt32(db.ExecuteScalar(dbCommand)) > 0;
            return false;
        }

        public static decimal getSumTon(string MaNPL)
        {
            SqlDatabase sdb = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT SUM(Ton)" +
                        "FROM " +
                            "t_SXXK_ThanhLy_NPLNhapTon " +
                        "WHERE " +
                            "MaNPL = @MaNPL ";

            DbCommand cmd = sdb.GetSqlStringCommand(sql);
            sdb.AddInParameter(cmd, "@MaNPL", SqlDbType.VarChar, MaNPL);
            object obj = sdb.ExecuteScalar(cmd);
            if (obj.GetType().Name != "DBNull")
            {
                return Convert.ToDecimal(sdb.ExecuteScalar(cmd));
            }
            else return 0;
        }
        public static DataSet SelectTongNPLbyTenChuHang(string maHaiQuan, string MaDoanhNghiep, string tenChuHang)
        {
            string spName = "SelectTongNPLbyTenChuHang";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.VarChar, tenChuHang);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, this._Ton);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, this._ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, this._ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, this._ThueVAT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, this._PhuThu);
            db.AddInParameter(dbCommand, "@ThueCLGia", SqlDbType.Float, this._ThueCLGia);
            db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, Math.Round((double)this._Ton * this._ThueXNK / (double)this._Luong, 0));
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public NPLNhapTonCollection SelectCollection_ByYear(string whereCondition, string orderByExpression)
        {
            NPLNhapTonCollection collection = new NPLNhapTonCollection();

            string spName = "p_KDT_SXXK_NPLNhapTon_SelectAll_ByYear";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                
                    NPLNhapTon entity = new NPLNhapTon();
                    Company.BLL.SXXK.ToKhai.HangMauDich HMD = new Company.BLL.SXXK.ToKhai.HangMauDich();
                    if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));

                    if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                    
                    if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                    if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt16(reader.GetOrdinal("SoThuTuHang"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                    if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                    if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ThueCLGia"))) entity.ThueCLGia = reader.GetDouble(reader.GetOrdinal("ThueCLGia"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));

                    if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) entity.TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoLanThanhLy"))) entity.SoLanThanhLy = reader.GetInt32(reader.GetOrdinal("SoLanThanhLy"));
                    if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SaiSoLuong"))) entity.SaiSoLuong = reader.GetInt32(reader.GetOrdinal("SaiSoLuong"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TienTrinhChayThanhLy"))) entity.TienTrinhChayThanhLy = reader.GetString(reader.GetOrdinal("TienTrinhChayThanhLy"));
                    try
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("SoHopDong"));
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        //throw;
                    }                    
                    collection.Add(entity);
              
            }
            reader.Close();
            return collection;
        }

        public DataSet SelectAll_ByYear(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_NPLNhapTon_SelectAll_ByYear";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            DataSet ds = this.db.ExecuteDataSet(dbCommand);

            return ds;
        }

    }
}