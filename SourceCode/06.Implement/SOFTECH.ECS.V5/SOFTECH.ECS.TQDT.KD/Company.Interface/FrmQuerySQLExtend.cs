﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface
{
    public partial class FrmQuerySQLExtend : Form
    {
        private DataSet _data = new DataSet();

        public FrmQuerySQLExtend()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            Query();

        }

        private void QueryTable()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {

                    string sql = "SELECT  o.object_id AS id,o.name,u.name AS owner,o.type,CAST(p.[value] AS varchar(8000)) AS cmt "
                               + "FROM sys.all_objects o INNER JOIN sys.schemas u ON u.schema_id = o.schema_id "
                                   + "LEFT OUTER JOIN sys.extended_properties p ON p.major_id = o.object_id AND p.minor_id = 0 AND p.name = 'MS_Description' "
                               + "WHERE o.type in ('U')  ORDER BY o.name, u.name";

                    SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                    _data = db.ExecuteDataSet(dbCommand);
                    int cont = _data.Tables[0].Rows.Count;
                    for (int i = 0; i < cont; i++)
                    {
                        // string a = _data.Tables[0].Rows[i][1].ToString();
                        comboBoxTable.Items.Add(_data.Tables[0].Rows[i][1].ToString());

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi " + ex);
                }
            }
        }

        private void Query()
        {
            Cursor = Cursors.WaitCursor;

            //Clear content.
            txtMessage.Clear();
            _data.Clear();

            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {

                    string sql = GetQueryString();

                    SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

                    if (sql.Trim().ToUpper().StartsWith("SELECT"))
                    {
                        _data = db.ExecuteDataSet(dbCommand);
                        grdResult.DataSource = _data;
                        grdResult.DataMember = "Table";

                        txtMessage.Text = grdResult.RowCount + " record(s).";
                        //lblRecord.Text = grdResult.RowCount + " record(s).";
                    }
                    else if (sql.Trim().ToUpper().StartsWith("INSERT") ||
                        sql.Trim().ToUpper().StartsWith("UPDATE") ||
                        sql.Trim().ToUpper().StartsWith("DELETE"))
                    {
                        txtMessage.Text = db.ExecuteNonQuery(dbCommand) + " record(s).";
                    }

                    //Active Tab Grid after have result.
                    uiTabPage4.Selected = true;

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    txtMessage.Text = ex.Message + "\r\n\n" + ex.StackTrace;
                    //uiTabPage2.Selected = true;
                }
                finally
                {
                    connection.Close();

                    Cursor = Cursors.Default;
                }
            }
        }

        private string GetQueryString()
        {
            Control query = tabQuery.SelectedTab.Controls[0];
            TextBox txtbox = query as TextBox;
            string[] lines = txtbox.Lines;
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < lines.Length; i++)
            {
                if (!lines[i].StartsWith("--"))
                {
                    sb.AppendLine(lines[i]);
                }
            }

            return sb.ToString();
        }

        private void FrmQuerySQL_Load(object sender, EventArgs e)
        {

            QueryTable();
            tabQuery.SelectedTab.Controls.Add(new TextBox() { Dock = DockStyle.Fill, Multiline = true, ScrollBars = ScrollBars.Vertical });
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            string table = comboBoxTable.Text;
            string value = txtID.Text.Trim();
            string colum = "ID";
            if (rbtnID.Checked == true)
            {
                colum = "ID";
            }
            else if (rbtnSoTN.Checked == true)
            {
                colum = "Sotiepnhan";
            }
            else if (rbtnSoTK.Checked == true)
            {
                colum = "SOTOKHAI";
            }

            if (table != "" && value != "")
            {
                try
                {

                    string str = "SELECT * from " + table + " Where " + colum + "=" + value;
                    tabQuery.SelectedTab.Controls[0].Text = str;
                    Query();

                    txtSTN.Text = grdResult.Rows[0].Cells["sotiepnhan"].Value.ToString();
                    txtNgayTiepNhan.Text = grdResult.Rows[0].Cells["NgayTiepNhan"].Value.ToString();
                    txtTT_XuLy.Text = grdResult.Rows[0].Cells["Trangthaixuly"].Value.ToString();
                    txtReferenceid.Text = grdResult.Rows[0].Cells["Guidstr"].Value.ToString();
                    if (table == "t_KDT_ToKhaiMauDich" || table == "t_KDT_GC_ToKhaiChuyenTiep")
                    {
                        txtNgayDangKy.Text = grdResult.Rows[0].Cells["Ngaydangky"].Value.ToString();
                        txtSoToKhai.Text = grdResult.Rows[0].Cells["SoToKhai"].Value.ToString();
                    }
                    tabQuery.SelectedTab.Controls[0].Text = str;
                }
                catch (Exception exx)
                {
                    MessageBox.Show(colum + " không tồn tại");
                }
            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string table = comboBoxTable.Text;
            string value = txtID.Text;
            string colum = "";
            if (rbtnID.Checked == true)
            {
                colum = "ID";
            }
            else if (rbtnSoTN.Checked == true)
            {
                colum = "Sotiepnhan";
            }
            else if (rbtnSoTK.Checked == true)
            {
                colum = "ID";
            }
            try
            {
                if (table != "" && value != "")
                {

                    DialogResult myDialogResult;
                    myDialogResult = MessageBox.Show("Bạn muốn cập nhật cho bảng " + table + " với " + colum + " = " + value, "Cập nhật thông tin tờ khai", MessageBoxButtons.YesNo);

                    if (myDialogResult == DialogResult.Yes)
                    {
                        string str = "";
                        if (table == "t_KDT_ToKhaiMauDich" || table == "t_KDT_GC_ToKhaiChuyenTiep")
                        {
                            str = "UPDATE " + table + " set sotiepnhan=" + txtSTN.Text + ", ngaytiepnhan='" + txtNgayTiepNhan.Text + "',trangthaixuly=" + txtTT_XuLy.Text + ",Guidstr='" + txtReferenceid.Text + "', ngaydangky='" + txtNgayDangKy.Text + "', SoToKhai='" + txtSoToKhai.Text + "' where " + colum + "=" + value + "";
                        }
                        else
                        {
                            str = "UPDATE " + table + " set sotiepnhan=" + txtSTN.Text + ", ngaytiepnhan='" + txtNgayTiepNhan.Text + "',trangthaixuly=" + txtTT_XuLy.Text + ",Guidstr='" + txtReferenceid.Text + "' where " + colum + "=" + value + "";
                        }
                        tabQuery.SelectedTab.Controls[0].Text = str;
                        Query();
                        MessageBox.Show("Cập nhật thành công.", "Thông báo");
                        btnSelect_Click(null, null);
                    }
                }
                else
                {
                    MessageBox.Show("Chọn điều kiện để cập nhật", "Cảnh báo");
                    txtID.Focus();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("" + exc);
            }
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            string id = txtID.Text.Trim();
            if (id == "")
            {
                btnSelect.Enabled = false;
            }
            btnSelect.Enabled = true;
        }

        private void txtID_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                btnSelect_Click(null, null);
            }
        }

        private void btnCmd_Select_Click(object sender, EventArgs e)
        {
            // txtQuery.Text = txtQuery.Text + "\r\n\n" + "SELECT * from " + comboBoxTable.Text + " WHERE id = ";
            Control query = tabQuery.SelectedTab.Controls[0];
            query.Text = query.Text + "\r\n" + "SELECT * from " + comboBoxTable.Text + " WHERE id = ";
        }

        private void bntCmd_Update_Click(object sender, EventArgs e)
        {
            Control query = tabQuery.SelectedTab.Controls[0];
            query.Text = query.Text + "\r\n" + "UPDATE " + comboBoxTable.Text + " SET  WHERE id = ";
        }

        private void btnCmd_Insert_Click(object sender, EventArgs e)
        {
            Control query = tabQuery.SelectedTab.Controls[0];
            query.Text = query.Text + "\r\n" + "INSERT INTO " + comboBoxTable.Text + " (,,,) VALUES (,,,)";
        }

        private void btnCmd_Delete_Click(object sender, EventArgs e)
        {
            Control query = tabQuery.SelectedTab.Controls[0];
            query.Text = query.Text + "\r\n" + "DELETE " + comboBoxTable.Text + "  WHERE id = ";
        }

        private void btnTabNew_Click(object sender, EventArgs e)
        {
            string title = "Cmd_SQL " + (tabQuery.TabCount + 1).ToString();
            TabPage myTabPage = new TabPage(title);
            TextBox txtQuery = new TextBox();
            myTabPage.Controls.Add(txtQuery);
            txtQuery.Dock = DockStyle.Fill;
            txtQuery.Multiline = true;
            tabQuery.TabPages.Add(myTabPage);
        }

        private void btnTabDelete_Click(object sender, EventArgs e)
        {
            tabQuery.TabPages.Remove(tabQuery.SelectedTab);
        }

        private void tabQuery_KeyUp(object sender, KeyEventArgs e)
        {
            string str = tabQuery.SelectedTab.Controls[0].Text;
            if (e.KeyData == Keys.F5)
            {
                Query();
            }
            else if (e.Control && e.KeyCode == Keys.A)
            {
                Control query = tabQuery.SelectedTab.Controls[0];
                TextBox txtbox = query as TextBox;
                txtbox.SelectAll();
            }
        }
















    }
}
