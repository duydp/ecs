﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface
{
    public partial class GiayNopTienChitietForm : BaseForm
    {
        public GiayNopTienChiTiet ChiTiet = new GiayNopTienChiTiet();
        bool _isEdit = false;
        public GiayNopTienChitietForm(bool isEdit)
        {
            InitializeComponent();
            _isEdit = isEdit;
        }
        private void Set()
        {
            txtSoTien.Value = ChiTiet.SoTien;
            txtDieuChinhGiam.Value = ChiTiet.DieuChinhGiam;
            cbSacThue.SelectedValue = ChiTiet.SacThue;
            txtChuong.Text = ChiTiet.MaChuong;
            txtLoai.Text = ChiTiet.Loai;
            txtKhoan.Text = ChiTiet.Khoan;
            txtMuc.Text = ChiTiet.Muc;
            txtTieuMuc.Text = ChiTiet.TieuMuc;
            txtGhiChu.Text = ChiTiet.GhiChu;
            if (ChiTiet.ID == 0)
                cbSacThue.SelectedIndex = 0;
        }
        private void Get()
        {
            ChiTiet.SoTien = Convert.ToDouble(txtSoTien.Value);
            ChiTiet.DieuChinhGiam = Convert.ToDouble(txtDieuChinhGiam.Value);
            ChiTiet.SacThue = Convert.ToInt32(cbSacThue.SelectedValue);
            ChiTiet.MaChuong = txtChuong.Text;
            ChiTiet.Loai = txtLoai.Text;
            ChiTiet.Khoan = txtKhoan.Text;
            ChiTiet.Muc = txtMuc.Text;
            ChiTiet.TieuMuc = txtTieuMuc.Text;
            ChiTiet.GhiChu = txtGhiChu.Text;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                bool isValidate = true;
                isValidate &= Globals.ValidateNull(txtChuong, error, "Chương thuế");
                isValidate &= Globals.ValidateNull(txtLoai, error, "Loại thuế");
                isValidate &= Globals.ValidateNull(txtKhoan, error, "Điều khoản");
                isValidate &= Globals.ValidateNull(txtMuc, error, "Mục thuế");
                isValidate &= Globals.ValidateNull(txtTieuMuc, error, "Tiểu mục");
                //&& Globals.ValidateNull(txtMaDVNhan, error, "Mã đơn vị nhận")
                //&& Globals.ValidateNull(txtSoTKNhan, error, "Tài khoản nhận");

                if (!isValidate)
                {
                    return;
                }

                Get();
                DialogResult = DialogResult.OK;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void GiayNopTienChitietForm_Load(object sender, EventArgs e)
        {
            Set();
            btnGhi.Enabled = btnXoa.Enabled = _isEdit;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (ChiTiet.ID > 0 && ShowMessage("Bạn có thật sự muốn xóa thông tin này không?", true) != "Yes")
                return;

            try
            {
                ChiTiet.Delete();
                DialogResult = DialogResult.No;
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new SendEventArgs(ex));
            }
        }

    }
}
