﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.VNACCS
{
    public partial class VNACC_ListThueVaThuKhac : BaseForm
    {
        public VNACC_ListThueVaThuKhac()
        {
            InitializeComponent();
        }
        public KDT_VNACC_HangMauDich HMD;
        public KDT_VNACC_HangMauDich_ThueThuKhac thuethukhac;
        DataTable dtThuKhac = new DataTable();
        private DataTable NewdtThuKhac()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("MaLoaiThue", typeof(string));
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("LoaiThue", typeof(string));
            dt.Columns.Add("MaTSThueThuKhac", typeof(string));
            dt.Columns.Add("MaMGThueThuKhac", typeof(string));
            dt.Columns.Add("SoTienGiamThueThuKhac", typeof(decimal));
            dt.Columns.Add("TenKhoanMucThueVaThuKhac", typeof(string));
            dt.Columns.Add("TriGiaTinhThueVaThuKhac", typeof(decimal));
            dt.Columns.Add("SoLuongTinhThueVaThuKhac", typeof(decimal));
            dt.Columns.Add("MaDVTDanhThueVaThuKhac", typeof(string));
            dt.Columns.Add("ThueSuatThueVaThuKhac", typeof(string));
            dt.Columns.Add("SoTienThueVaThuKhac", typeof(string));
            dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac", typeof(string));

            for (int i = 0; i < 5; i++)
            {
                DataRow dr = dt.NewRow();
                switch (i)
                {
                    case 0:
                        dr["MaLoaiThue"] = "VAT";
                        dr["LoaiThue"] = "THUẾ GIÁ TRỊ GIA TĂNG";
                        break;
                    case 1:
                        dr["MaLoaiThue"] = "TTDB";
                        dr["LoaiThue"] = "THUẾ TIÊU THỤ ĐẶC BIỆT";
                        break;
                    case 2:
                        dr["MaLoaiThue"] = "BVMT";
                        dr["LoaiThue"] = "THUẾ BẢO VỆ MÔI TRƯỜNG";
                        break;
                    case 3:
                        dr["MaLoaiThue"] = "TUVE";
                        dr["LoaiThue"] = "THUẾ TỰ VỆ/CHỐNG PHÁ GIÁ";
                        break;
                    case 4:
                        dr["MaLoaiThue"] = "KHAC";
                        dr["LoaiThue"] = "THUẾ KHÁC";
                        break;
                }
                dr["STT"] = i;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        private void setdtThuKhac(List<KDT_VNACC_HangMauDich_ThueThuKhac> listThuKhac)
        {
            try
            {
                dtThuKhac = NewdtThuKhac();
                foreach (KDT_VNACC_HangMauDich_ThueThuKhac thukhac in listThuKhac)
                {
                    DataRow dr = dtThuKhac.Rows[4];
                    if (thukhac.MaTSThueThuKhac.Contains("VB"))
                    {
                        dr = dtThuKhac.Rows[0];
                    }
                    else if (thukhac.MaTSThueThuKhac.Contains("TB"))
                    {
                        dr = dtThuKhac.Rows[1];
                    }
                    else if (thukhac.MaTSThueThuKhac.Contains("MB"))
                    {
                        dr = dtThuKhac.Rows[2];
                    }
                    else if (thukhac.MaTSThueThuKhac.Contains("BB") || thukhac.MaTSThueThuKhac.Contains("GB") || thukhac.MaTSThueThuKhac.Contains("CB") || thukhac.MaTSThueThuKhac.Contains("PB") || thukhac.MaTSThueThuKhac.Contains("DB") || thukhac.MaTSThueThuKhac.Contains("EB"))
                    {
                        dr = dtThuKhac.Rows[3];
                    }

                    dr["ID"] = thukhac.ID;
                    dr["MaTSThueThuKhac"] = thukhac.MaTSThueThuKhac;
                    dr["MaMGThueThuKhac"] = thukhac.MaMGThueThuKhac;
                    dr["SoTienGiamThueThuKhac"] = thukhac.SoTienGiamThueThuKhac;
                    dr["TenKhoanMucThueVaThuKhac"] = thukhac.TenKhoanMucThueVaThuKhac;
                    dr["TriGiaTinhThueVaThuKhac"] = thukhac.TriGiaTinhThueVaThuKhac;
                    dr["SoLuongTinhThueVaThuKhac"] = thukhac.SoLuongTinhThueVaThuKhac;
                    dr["MaDVTDanhThueVaThuKhac"] = thukhac.MaDVTDanhThueVaThuKhac;
                    dr["ThueSuatThueVaThuKhac"] = thukhac.ThueSuatThueVaThuKhac;
                    dr["SoTienThueVaThuKhac"] = thukhac.SoTienThueVaThuKhac;
                    dr["DieuKhoanMienGiamThueVaThuKhac"] = thukhac.DieuKhoanMienGiamThueVaThuKhac;
                }
                grListThueThuKhac.DataSource = dtThuKhac;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_ListThueVaThuKhac_Load(object sender, EventArgs e)
        {
            try
            {
                if (HMD != null)
                {
                    HMD.loadThueVaThuKhac();
                    if (HMD.ThueThuKhacCollection != null && HMD.ThueThuKhacCollection.Count > 0)
                        setdtThuKhac(HMD.ThueThuKhacCollection);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void grListThueThuKhac_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    DataRowView dr = (DataRowView)e.Row.DataRow;
                    thuethukhac = HMD.ThueThuKhacCollection.Find(x => x.MaTSThueThuKhac == dr["MaTSThueThuKhac"].ToString().Trim());
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
