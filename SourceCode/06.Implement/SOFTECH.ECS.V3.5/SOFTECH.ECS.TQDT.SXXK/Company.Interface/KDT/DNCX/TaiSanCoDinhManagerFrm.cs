﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class TaiSanCoDinhManagerFrm : BaseForm
    {
        KDT_SXXK_TLTSCDDangKy TLTSCD = new KDT_SXXK_TLTSCDDangKy();
        
        public TaiSanCoDinhManagerFrm()
        {
            InitializeComponent();
            dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(dgList_RowDoubleClick);
        }

        void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            KDT_SXXK_TLTSCDDangKy tscd = (KDT_SXXK_TLTSCDDangKy)e.Row.DataRow;
            tscd.LoadHangCollection();
            TaiSanCoDinhEditFrm f = new TaiSanCoDinhEditFrm();
            f.TLTSCD = tscd;
            f.ShowDialog(this);
            btnSearch_Click(null, null);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TaiSanCoDinhManagerFrm_Load(object sender, EventArgs e)
        {
            ctrMaHaiQuan.Ma = GlobalSettings.MA_HAI_QUAN;
            cbStatus.SelectedIndex = 0;
            btnSearch_Click(null, null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string maHQ = ctrMaHaiQuan.Ma;
                string soTN = txtSoTiepNhan.Text.Trim();
                string namTN = txtNamTiepNhan.Text.Trim();
                string trangThai = (cbStatus.SelectedValue == null) ? "-1" : cbStatus.SelectedValue.ToString();
                string where = "(1 = 1) ";
               
                if (trangThai != "")
                    where = where + " AND TrangThaiXuLy=" + trangThai + "";
                if (soTN != "")
                    where += " and SoTiepNhan=" + soTN + " ";
                if (namTN != "")
                    where += " and year(NgayTiepNhan)=" + namTN + " ";
                if (maHQ != "")
                    where += " and MaHaiQuan='" + maHQ + "'";

                
                dgList.DataSource = KDT_SXXK_TLTSCDDangKy.SelectCollectionDynamic(where, "ID");//TLTSCD.HangThanhLyCollection;
                dgList.Refetch();
                            
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }

    }
}
