﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Management;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;
using System.Data;
using System.Xml;

namespace TestActivateService
{
    static class Program
    {
        public static bool isActivated = false;
        public static License lic = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                try
                {
                    lic = new License();
                    loadLicense();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + "\nChương trình có lỗi. Vui lòng cài đặt lại.");
                    Application.Exit();
                    return;
                }

                isActivated = checkRegistered();
                //isActivated = true;

                string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
                ApplicationSettings settings = new ApplicationSettings();
                settings.ReadXml(settingFileName);
                //GlobalSettings.RefreshKey();
                CultureInfo culture = null;
                //DevExpress.UserSkins.OfficeSkins.Register();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //if (Properties.Settings.Default.NgonNgu == "0")
                //    culture = new CultureInfo("vi-VN");
                //else
                //    culture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("vi-VN");

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmRegister());

            }
            catch (Exception exx)
            {
                Logger.LocalLogger.Instance().WriteMessage(exx);
                MessageBox.Show(" Lỗi : " + exx.Message);
            }
        }

        private static void loadLicense()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                int numInConf = int.Parse(lic.DecryptString(config.AppSettings.Settings["DateTr"].Value));
                if (!lic.checkExistsLicense())
                {
                    lic.generTrial();
                    lic.Load();
                    if (numInConf < int.Parse(lic.dateTrial))
                    {
                        lic.dateTrial = numInConf.ToString();
                        lic.Save();
                    }
                }
                else lic.Load();
                if (numInConf > int.Parse(lic.dateTrial))
                {
                    config.AppSettings.Settings.Remove("DateTr");
                    config.AppSettings.Settings.Add("DateTr", lic.EncryptString(lic.dateTrial));
                    config.Save(ConfigurationSaveMode.Full);
                    ConfigurationManager.RefreshSection("appSettings");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static bool checkRegistered()
        {
            bool value = false;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (lic.codeActivate == getCodeToActivate())
            {
                CultureInfo infoVN = new CultureInfo("vi-VN");
                if (DateTime.Parse(lic.dayExpires, infoVN.DateTimeFormat).Date > DateTime.Now.Date)
                {
                    value = true;
                }
            }
            return value;
        }

        public static string getProcessorID()
        {
            string CPUID = "BoardID:";
            ManagementObjectSearcher srch = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
            foreach (ManagementObject obj in srch.Get())
            {
                CPUID = obj.Properties["SerialNumber"].Value.ToString();
            }
            return License.md5String(CPUID);
        }

        public static string getCodeToActivate()
        {
            return License.md5String(getProcessorID() + "SOFTECH.ECS.KD");
        }

        public static int getOSArchitecture()
        {
            string pa = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
            return ((String.IsNullOrEmpty(pa) || String.Compare(pa, 0, "x86", 0, 3, true) == 0) ? 32 : 64);
        }

        public static string getOSInfo()
        {
            //Get Operating system information.
            OperatingSystem os = Environment.OSVersion;
            //Get version information about the os.
            Version vs = os.Version;

            //Variable to hold our return value
            string operatingSystem = "";

            if (os.Platform == PlatformID.Win32Windows)
            {
                //This is a pre-NT version of Windows
                switch (vs.Minor)
                {
                    case 0:
                        operatingSystem = "95";
                        break;
                    case 10:
                        if (vs.Revision.ToString() == "2222A")
                            operatingSystem = "98SE";
                        else
                            operatingSystem = "98";
                        break;
                    case 90:
                        operatingSystem = "Me";
                        break;
                    default:
                        break;
                }
            }
            else if (os.Platform == PlatformID.Win32NT)
            {
                switch (vs.Major)
                {
                    case 3:
                        operatingSystem = "NT 3.51";
                        break;
                    case 4:
                        operatingSystem = "NT 4.0";
                        break;
                    case 5:
                        if (vs.Minor == 0)
                            operatingSystem = "2000";
                        else
                            operatingSystem = "XP";
                        break;
                    case 6:
                        if (vs.Minor == 0)
                            operatingSystem = "Vista";
                        else
                            operatingSystem = "7";
                        break;
                    default:
                        break;
                }
            }
            //Make sure we actually got something in our OS check
            //We don't want to just return " Service Pack 2" or " 32-bit"
            //That information is useless without the OS version.
            if (operatingSystem != "")
            {
                //Got something.  Let's prepend "Windows" and get more info.
                operatingSystem = "Windows " + operatingSystem;
                //See if there's a service pack installed.
                if (os.ServicePack != "")
                {
                    //Append it to the OS name.  i.e. "Windows XP Service Pack 3"
                    operatingSystem += " " + os.ServicePack;
                }
                //Append the OS architecture.  i.e. "Windows XP Service Pack 3 32-bit"
                operatingSystem += " " + getOSArchitecture().ToString() + "-bit";
            }
            //Return the information we've gathered.
            return operatingSystem;
        }
    }
}
