using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components.AnDinhThue
{
	public partial class AnDinhThue
	{
		#region Properties.
		
		public int ID { set; get; }
		public long TKMD_ID { set; get; }
		public string TKMD_Ref { set; get; }
		public string SoQuyetDinh { set; get; }
		public DateTime NgayQuyetDinh { set; get; }
		public DateTime NgayHetHan { set; get; }
		public string TaiKhoanKhoBac { set; get; }
		public string TenKhoBac { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static AnDinhThue Load(int id)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
			AnDinhThue entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new AnDinhThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_Ref"))) entity.TKMD_Ref = reader.GetString(reader.GetOrdinal("TKMD_Ref"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) entity.SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) entity.NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TaiKhoanKhoBac"))) entity.TaiKhoanKhoBac = reader.GetString(reader.GetOrdinal("TaiKhoanKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKhoBac"))) entity.TenKhoBac = reader.GetString(reader.GetOrdinal("TenKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<AnDinhThue> SelectCollectionAll()
		{
			List<AnDinhThue> collection = new List<AnDinhThue>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				AnDinhThue entity = new AnDinhThue();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_Ref"))) entity.TKMD_Ref = reader.GetString(reader.GetOrdinal("TKMD_Ref"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) entity.SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) entity.NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TaiKhoanKhoBac"))) entity.TaiKhoanKhoBac = reader.GetString(reader.GetOrdinal("TaiKhoanKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKhoBac"))) entity.TenKhoBac = reader.GetString(reader.GetOrdinal("TenKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<AnDinhThue> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<AnDinhThue> collection = new List<AnDinhThue>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				AnDinhThue entity = new AnDinhThue();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_Ref"))) entity.TKMD_Ref = reader.GetString(reader.GetOrdinal("TKMD_Ref"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) entity.SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) entity.NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TaiKhoanKhoBac"))) entity.TaiKhoanKhoBac = reader.GetString(reader.GetOrdinal("TaiKhoanKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKhoBac"))) entity.TenKhoBac = reader.GetString(reader.GetOrdinal("TenKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<AnDinhThue> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
			List<AnDinhThue> collection = new List<AnDinhThue>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_TKMD_ID(tKMD_ID);
			while (reader.Read())
			{
				AnDinhThue entity = new AnDinhThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_Ref"))) entity.TKMD_Ref = reader.GetString(reader.GetOrdinal("TKMD_Ref"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) entity.SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) entity.NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TaiKhoanKhoBac"))) entity.TaiKhoanKhoBac = reader.GetString(reader.GetOrdinal("TaiKhoanKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKhoBac"))) entity.TenKhoBac = reader.GetString(reader.GetOrdinal("TenKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_AnDinhThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_AnDinhThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_AnDinhThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_AnDinhThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_KDT_AnDinhThue_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertAnDinhThue(long tKMD_ID, string tKMD_Ref, string soQuyetDinh, DateTime ngayQuyetDinh, DateTime ngayHetHan, string taiKhoanKhoBac, string tenKhoBac, string ghiChu)
		{
			AnDinhThue entity = new AnDinhThue();	
			entity.TKMD_ID = tKMD_ID;
			entity.TKMD_Ref = tKMD_Ref;
			entity.SoQuyetDinh = soQuyetDinh;
			entity.NgayQuyetDinh = ngayQuyetDinh;
			entity.NgayHetHan = ngayHetHan;
			entity.TaiKhoanKhoBac = taiKhoanKhoBac;
			entity.TenKhoBac = tenKhoBac;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_AnDinhThue_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@TKMD_Ref", SqlDbType.NVarChar, TKMD_Ref);
			db.AddInParameter(dbCommand, "@SoQuyetDinh", SqlDbType.NVarChar, SoQuyetDinh);
			db.AddInParameter(dbCommand, "@NgayQuyetDinh", SqlDbType.DateTime, NgayQuyetDinh.Year <= 1753 ? DBNull.Value : (object) NgayQuyetDinh);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@TaiKhoanKhoBac", SqlDbType.VarChar, TaiKhoanKhoBac);
			db.AddInParameter(dbCommand, "@TenKhoBac", SqlDbType.NVarChar, TenKhoBac);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<AnDinhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (AnDinhThue item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateAnDinhThue(int id, long tKMD_ID, string tKMD_Ref, string soQuyetDinh, DateTime ngayQuyetDinh, DateTime ngayHetHan, string taiKhoanKhoBac, string tenKhoBac, string ghiChu)
		{
			AnDinhThue entity = new AnDinhThue();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.TKMD_Ref = tKMD_Ref;
			entity.SoQuyetDinh = soQuyetDinh;
			entity.NgayQuyetDinh = ngayQuyetDinh;
			entity.NgayHetHan = ngayHetHan;
			entity.TaiKhoanKhoBac = taiKhoanKhoBac;
			entity.TenKhoBac = tenKhoBac;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_AnDinhThue_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@TKMD_Ref", SqlDbType.NVarChar, TKMD_Ref);
			db.AddInParameter(dbCommand, "@SoQuyetDinh", SqlDbType.NVarChar, SoQuyetDinh);
			db.AddInParameter(dbCommand, "@NgayQuyetDinh", SqlDbType.DateTime, NgayQuyetDinh.Year == 1753 ? DBNull.Value : (object) NgayQuyetDinh);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@TaiKhoanKhoBac", SqlDbType.VarChar, TaiKhoanKhoBac);
			db.AddInParameter(dbCommand, "@TenKhoBac", SqlDbType.NVarChar, TenKhoBac);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<AnDinhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (AnDinhThue item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateAnDinhThue(int id, long tKMD_ID, string tKMD_Ref, string soQuyetDinh, DateTime ngayQuyetDinh, DateTime ngayHetHan, string taiKhoanKhoBac, string tenKhoBac, string ghiChu)
		{
			AnDinhThue entity = new AnDinhThue();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.TKMD_Ref = tKMD_Ref;
			entity.SoQuyetDinh = soQuyetDinh;
			entity.NgayQuyetDinh = ngayQuyetDinh;
			entity.NgayHetHan = ngayHetHan;
			entity.TaiKhoanKhoBac = taiKhoanKhoBac;
			entity.TenKhoBac = tenKhoBac;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@TKMD_Ref", SqlDbType.NVarChar, TKMD_Ref);
			db.AddInParameter(dbCommand, "@SoQuyetDinh", SqlDbType.NVarChar, SoQuyetDinh);
			db.AddInParameter(dbCommand, "@NgayQuyetDinh", SqlDbType.DateTime, NgayQuyetDinh.Year == 1753 ? DBNull.Value : (object) NgayQuyetDinh);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@TaiKhoanKhoBac", SqlDbType.VarChar, TaiKhoanKhoBac);
			db.AddInParameter(dbCommand, "@TenKhoBac", SqlDbType.NVarChar, TenKhoBac);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<AnDinhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (AnDinhThue item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteAnDinhThue(int id)
		{
			AnDinhThue entity = new AnDinhThue();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_AnDinhThue_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<AnDinhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (AnDinhThue item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}