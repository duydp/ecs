﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.Interface.DanhMucChuan;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT.SXXK;
namespace Company.Interface
{
    public class SingleMessage
    {
        public static string SendMessageV3(ToKhaiMauDich TKMD)
        {
            TKMD.GUIDSTR = Guid.NewGuid().ToString();
            KD_ToKhaiMauDich kd_tkmd = Company.KD.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject(TKMD);
            bool isToKhaiSua = TKMD.SoToKhai != 0 && TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            string msgSend = Helpers.BuildSend(
                           new NameBase()
                           {
                               Name = TKMD.TenDoanhNghiep,
                               Identity = TKMD.MaDoanhNghiep
                           }
                             , new NameBase()
                             {
                                 Name = Company.KD.BLL.DuLieuChuan.DonViHaiQuan.GetName(TKMD.MaHaiQuan),
                                 Identity = TKMD.MaHaiQuan
                             }
                          ,
                            new SubjectBase()
                            {
                                Type = TKMD.MaLoaiHinh.StartsWith("N") ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT,
                                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                                Reference = TKMD.GUIDSTR
                            }
                            ,
                            kd_tkmd, null);
            return msgSend;
        }
        public static string CancelMessageV3(ToKhaiMauDich TKMD)
        {
            bool IsToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            TKMD.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
            bool IsHuyToKhai = (TKMD.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = IsToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT,
                Reference = TKMD.GUIDSTR,
                Issue = DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss"),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(TKMD.SoTiepNhan, 0),
                Acceptance = TKMD.NgayTiepNhan.ToString("yyyy-MM-dd"),
                DeclarationOffice = TKMD.MaHaiQuan.Trim(),
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                }
            };
            
            if (IsHuyToKhai)
            {
                List<HuyToKhai> cancelContent = HuyToKhai.SelectCollectionBy_TKMD_ID(TKMD.ID);
                if (cancelContent.Count > 0)
                    declaredCancel.AdditionalInformation.Content.Text = cancelContent[0].LyDoHuy;
            }
            string msgSend = Helpers.BuildSend(
                                  new NameBase()
                                  {
                                      Name = TKMD.TenDoanhNghiep,
                                      Identity = TKMD.MaDoanhNghiep
                                  }
                                    , new NameBase()
                                    {
                                        Name = Company.KD.BLL.DuLieuChuan.DonViHaiQuan.GetName(TKMD.MaHaiQuan),
                                        Identity = TKMD.MaHaiQuan
                                    }
                                 ,
                                   new SubjectBase()
                                   {
                                       Type = IsToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT,
                                       Function = declaredCancel.Function,
                                       Reference = TKMD.GUIDSTR
                                   }
                                   ,
                                   declaredCancel, null, true);
            return msgSend;
        }
        public static string FeedBackMessageV3(ToKhaiMauDich TKMD)
        {
            string reference = TKMD.GUIDSTR;
            bool IsToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = IsToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT,
                Reference = reference,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = IsToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT

            };

            string msgSend = Company.KDT.SHARE.Components.Helpers.BuildFeedBack(
                                        new NameBase()
                                        {
                                            Name = TKMD.TenDoanhNghiep,
                                            Identity = TKMD.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KD.BLL.DuLieuChuan.DonViHaiQuan.GetName(TKMD.MaHaiQuan.Trim()),
                                              Identity = TKMD.MaHaiQuan
                                          }, subjectBase, null);
            return msgSend;
        }
    }
}
