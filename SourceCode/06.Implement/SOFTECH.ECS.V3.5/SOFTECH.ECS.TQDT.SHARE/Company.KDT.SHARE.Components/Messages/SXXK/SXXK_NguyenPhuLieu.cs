﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class SXXK_NguyenPhuLieu : DeclarationBase
    {
        /// <summary>
        /// Nguyên phụ liệu
        /// </summary>
        [XmlElement("Material")]
        public List<Product> Product { get; set; }


    }
}
