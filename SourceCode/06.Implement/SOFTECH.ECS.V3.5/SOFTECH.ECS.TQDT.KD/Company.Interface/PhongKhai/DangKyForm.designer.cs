﻿namespace Company.Interface.PhongKhai
{
    partial class DangKyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DangKyForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.btnCancel = new Janus.Windows.EditControls.UIButton();
            this.btnLogin = new Janus.Windows.EditControls.UIButton();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbTuDongTinhThue = new Janus.Windows.EditControls.UIComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMaMid = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDiaChiDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.diaDiemBocHangControl1 = new Company.Interface.Controls.DiaDiemBocHangControl();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nuocHControl1 = new Company.Interface.Controls.NuocHControl();
            this.nguyenTeControl1 = new Company.Interface.Controls.NguyenTeControl();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.cbPTVT = new Janus.Windows.EditControls.UIComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtRePass = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblHopDong = new System.Windows.Forms.Label();
            this.txtPass = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox11);
            this.grbMain.Controls.Add(this.btnCancel);
            this.grbMain.Controls.Add(this.btnLogin);
            this.grbMain.Size = new System.Drawing.Size(623, 352);
            this.grbMain.Click += new System.EventHandler(this.grbMain_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCancel.Icon")));
            this.btnCancel.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnCancel.Location = new System.Drawing.Point(301, 322);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(92, 23);
            this.btnCancel.TabIndex = 203;
            this.btnCancel.Text = "Thoát";
            this.btnCancel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCancel.VisualStyleManager = this.vsmMain;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogin.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLogin.Icon")));
            this.btnLogin.Location = new System.Drawing.Point(205, 322);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(90, 23);
            this.btnLogin.TabIndex = 198;
            this.btnLogin.Text = "Thiết lập";
            this.btnLogin.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnLogin.VisualStyleManager = this.vsmMain;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTenDoanhNghiep;
            this.rfvTen.ErrorMessage = "\"Tên Doanh Nghiệp\" bắt buộc phải nhập.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(9, 75);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(261, 21);
            this.txtTenDoanhNghiep.TabIndex = 206;
            this.txtTenDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.cbTuDongTinhThue);
            this.uiGroupBox11.Controls.Add(this.label13);
            this.uiGroupBox11.Controls.Add(this.label7);
            this.uiGroupBox11.Controls.Add(this.txtMaMid);
            this.uiGroupBox11.Controls.Add(this.label10);
            this.uiGroupBox11.Controls.Add(this.txtDiaChiDN);
            this.uiGroupBox11.Controls.Add(this.diaDiemBocHangControl1);
            this.uiGroupBox11.Controls.Add(this.label9);
            this.uiGroupBox11.Controls.Add(this.label6);
            this.uiGroupBox11.Controls.Add(this.label5);
            this.uiGroupBox11.Controls.Add(this.label4);
            this.uiGroupBox11.Controls.Add(this.label3);
            this.uiGroupBox11.Controls.Add(this.nuocHControl1);
            this.uiGroupBox11.Controls.Add(this.nguyenTeControl1);
            this.uiGroupBox11.Controls.Add(this.cbPTTT);
            this.uiGroupBox11.Controls.Add(this.cbDKGH);
            this.uiGroupBox11.Controls.Add(this.cbPTVT);
            this.uiGroupBox11.Controls.Add(this.label2);
            this.uiGroupBox11.Controls.Add(this.label17);
            this.uiGroupBox11.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox11.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox11.Controls.Add(this.label1);
            this.uiGroupBox11.Controls.Add(this.label27);
            this.uiGroupBox11.Controls.Add(this.txtRePass);
            this.uiGroupBox11.Controls.Add(this.lblHopDong);
            this.uiGroupBox11.Controls.Add(this.txtPass);
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.Location = new System.Drawing.Point(12, 3);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(600, 313);
            this.uiGroupBox11.TabIndex = 209;
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // cbTuDongTinhThue
            // 
            this.cbTuDongTinhThue.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbTuDongTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Có tự động tính thuế";
            uiComboBoxItem1.Value = "1";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Không tự động tính thuế";
            uiComboBoxItem2.Value = "0";
            this.cbTuDongTinhThue.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbTuDongTinhThue.Location = new System.Drawing.Point(9, 283);
            this.cbTuDongTinhThue.Name = "cbTuDongTinhThue";
            this.cbTuDongTinhThue.Size = new System.Drawing.Size(261, 21);
            this.cbTuDongTinhThue.TabIndex = 232;
            this.cbTuDongTinhThue.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbTuDongTinhThue.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 267);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 13);
            this.label13.TabIndex = 231;
            this.label13.Text = "Tự động tính thuế";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 13);
            this.label7.TabIndex = 230;
            this.label7.Text = "Mã MID Doanh Nghiệp";
            // 
            // txtMaMid
            // 
            this.txtMaMid.Location = new System.Drawing.Point(9, 156);
            this.txtMaMid.Name = "txtMaMid";
            this.txtMaMid.Size = new System.Drawing.Size(261, 21);
            this.txtMaMid.TabIndex = 229;
            this.txtMaMid.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 13);
            this.label10.TabIndex = 223;
            this.label10.Text = "Địa chỉ Doanh Nghiệp";
            // 
            // txtDiaChiDN
            // 
            this.txtDiaChiDN.Location = new System.Drawing.Point(9, 116);
            this.txtDiaChiDN.Name = "txtDiaChiDN";
            this.txtDiaChiDN.Size = new System.Drawing.Size(261, 21);
            this.txtDiaChiDN.TabIndex = 222;
            this.txtDiaChiDN.VisualStyleManager = this.vsmMain;
            // 
            // diaDiemBocHangControl1
            // 
            this.diaDiemBocHangControl1.BackColor = System.Drawing.Color.Transparent;
            this.diaDiemBocHangControl1.ErrorMessage = "\"Đơn vị Hải quan\" không được bỏ trống.";
            this.diaDiemBocHangControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diaDiemBocHangControl1.Location = new System.Drawing.Point(319, 36);
            this.diaDiemBocHangControl1.Ma = "";
            this.diaDiemBocHangControl1.Name = "diaDiemBocHangControl1";
            this.diaDiemBocHangControl1.ReadOnly = false;
            this.diaDiemBocHangControl1.Size = new System.Drawing.Size(273, 22);
            this.diaDiemBocHangControl1.TabIndex = 221;
            this.diaDiemBocHangControl1.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(316, 182);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 220;
            this.label9.Text = "Phương tiện vận tải";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(316, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 13);
            this.label6.TabIndex = 217;
            this.label6.Text = "Cảng, địa điểm dỡ hàng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(316, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 216;
            this.label5.Text = "Đồng tiền thanh toán";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(316, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 215;
            this.label4.Text = "Phương thức thanh toán";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(316, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 214;
            this.label3.Text = "Nước sản xuất";
            // 
            // nuocHControl1
            // 
            this.nuocHControl1.BackColor = System.Drawing.Color.Transparent;
            this.nuocHControl1.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.nuocHControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nuocHControl1.Location = new System.Drawing.Point(319, 114);
            this.nuocHControl1.Ma = "";
            this.nuocHControl1.Name = "nuocHControl1";
            this.nuocHControl1.ReadOnly = false;
            this.nuocHControl1.Size = new System.Drawing.Size(273, 22);
            this.nuocHControl1.TabIndex = 213;
            this.nuocHControl1.VisualStyleManager = this.vsmMain;
            // 
            // nguyenTeControl1
            // 
            this.nguyenTeControl1.BackColor = System.Drawing.Color.Transparent;
            this.nguyenTeControl1.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.nguyenTeControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nguyenTeControl1.Location = new System.Drawing.Point(319, 74);
            this.nguyenTeControl1.Ma = "";
            this.nguyenTeControl1.Name = "nguyenTeControl1";
            this.nguyenTeControl1.ReadOnly = false;
            this.nguyenTeControl1.Size = new System.Drawing.Size(273, 22);
            this.nguyenTeControl1.TabIndex = 212;
            this.nguyenTeControl1.VisualStyleManager = this.vsmMain;
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(319, 239);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(259, 21);
            this.cbPTTT.TabIndex = 209;
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbPTTT.VisualStyleManager = this.vsmMain;
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "ID";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(319, 158);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(259, 21);
            this.cbDKGH.TabIndex = 208;
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbDKGH.VisualStyleManager = this.vsmMain;
            // 
            // cbPTVT
            // 
            this.cbPTVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTVT.DisplayMember = "Ten";
            this.cbPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTVT.Location = new System.Drawing.Point(319, 196);
            this.cbPTVT.Name = "cbPTVT";
            this.cbPTVT.Size = new System.Drawing.Size(259, 21);
            this.cbPTVT.TabIndex = 1;
            this.cbPTVT.ValueMember = "ID";
            this.cbPTVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 207;
            this.label2.Text = "Tên Doanh Nghiệp";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(316, 142);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Điều kiện giao hàng";
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(9, 36);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.ReadOnly = true;
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(261, 21);
            this.txtMaDoanhNghiep.TabIndex = 201;
            this.txtMaDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 224);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 205;
            this.label1.Text = "Khẳng định Mật Khẩu";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 20);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(91, 13);
            this.label27.TabIndex = 199;
            this.label27.Text = "Mã Doanh Nghiệp";
            // 
            // txtRePass
            // 
            this.txtRePass.Location = new System.Drawing.Point(9, 240);
            this.txtRePass.Name = "txtRePass";
            this.txtRePass.PasswordChar = '*';
            this.txtRePass.Size = new System.Drawing.Size(261, 21);
            this.txtRePass.TabIndex = 204;
            this.txtRePass.VisualStyleManager = this.vsmMain;
            // 
            // lblHopDong
            // 
            this.lblHopDong.AutoSize = true;
            this.lblHopDong.BackColor = System.Drawing.Color.Transparent;
            this.lblHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHopDong.Location = new System.Drawing.Point(6, 182);
            this.lblHopDong.Name = "lblHopDong";
            this.lblHopDong.Size = new System.Drawing.Size(52, 13);
            this.lblHopDong.TabIndex = 200;
            this.lblHopDong.Text = "Mật Khẩu";
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(9, 196);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(261, 21);
            this.txtPass.TabIndex = 202;
            this.txtPass.VisualStyleManager = this.vsmMain;
            // 
            // DangKyForm
            // 
            this.ClientSize = new System.Drawing.Size(623, 352);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DangKyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đăng Ký Thông tin Doanh Nghiệp";
            this.Load += new System.EventHandler(this.DangKyForm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DangKyForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnCancel;
        private Janus.Windows.EditControls.UIButton btnLogin;
        private System.Windows.Forms.ErrorProvider epError;
        private System.Windows.Forms.ToolTip toolTip1;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.EditControls.UIComboBox cbTuDongTinhThue;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMid;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDN;
        private Company.Interface.Controls.DiaDiemBocHangControl diaDiemBocHangControl1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Company.Interface.Controls.NuocHControl nuocHControl1;
        private Company.Interface.Controls.NguyenTeControl nguyenTeControl1;
        private Janus.Windows.EditControls.UIComboBox cbPTTT;
        private Janus.Windows.EditControls.UIComboBox cbDKGH;
        private Janus.Windows.EditControls.UIComboBox cbPTVT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtRePass;
        private System.Windows.Forms.Label lblHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtPass;
    }
}
