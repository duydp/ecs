-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]
	@HoaDon_ID bigint,
	@MaPhaLoai varchar(3),
	@So varchar(35),
	@NgayThangNam datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
(
	[HoaDon_ID],
	[MaPhaLoai],
	[So],
	[NgayThangNam],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@HoaDon_ID,
	@MaPhaLoai,
	@So,
	@NgayThangNam,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]
	@ID bigint,
	@HoaDon_ID bigint,
	@MaPhaLoai varchar(3),
	@So varchar(35),
	@NgayThangNam datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
SET
	[HoaDon_ID] = @HoaDon_ID,
	[MaPhaLoai] = @MaPhaLoai,
	[So] = @So,
	[NgayThangNam] = @NgayThangNam,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]
	@ID bigint,
	@HoaDon_ID bigint,
	@MaPhaLoai varchar(3),
	@So varchar(35),
	@NgayThangNam datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_PhanLoaiHoaDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_PhanLoaiHoaDon] 
		SET
			[HoaDon_ID] = @HoaDon_ID,
			[MaPhaLoai] = @MaPhaLoai,
			[So] = @So,
			[NgayThangNam] = @NgayThangNam,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
		(
			[HoaDon_ID],
			[MaPhaLoai],
			[So],
			[NgayThangNam],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@HoaDon_ID,
			@MaPhaLoai,
			@So,
			@NgayThangNam,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_PhanLoaiHoaDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoaDon_ID],
	[MaPhaLoai],
	[So],
	[NgayThangNam],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HoaDon_ID],
	[MaPhaLoai],
	[So],
	[NgayThangNam],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_PhanLoaiHoaDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoaDon_ID],
	[MaPhaLoai],
	[So],
	[NgayThangNam],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_PhanLoaiHoaDon]	

GO

