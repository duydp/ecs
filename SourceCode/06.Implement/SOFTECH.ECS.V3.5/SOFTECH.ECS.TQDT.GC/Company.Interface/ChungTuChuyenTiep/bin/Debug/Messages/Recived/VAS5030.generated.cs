using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAS5030 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute AB { get; set; }
public PropertiesAttribute AC { get; set; }
public PropertiesAttribute AD { get; set; }
public PropertiesAttribute AE { get; set; }
public PropertiesAttribute SCG { get; set; }
public PropertiesAttribute ED { get; set; }
public PropertiesAttribute AF { get; set; }
public PropertiesAttribute AG { get; set; }
public PropertiesAttribute AH { get; set; }
public PropertiesAttribute AI { get; set; }
public PropertiesAttribute AL { get; set; }
public PropertiesAttribute AM { get; set; }
public PropertiesAttribute AN { get; set; }
public PropertiesAttribute AO { get; set; }
public PropertiesAttribute AP { get; set; }
public PropertiesAttribute AQ { get; set; }
public PropertiesAttribute AR { get; set; }
public PropertiesAttribute AS { get; set; }
public PropertiesAttribute AT { get; set; }
public PropertiesAttribute AU { get; set; }
public PropertiesAttribute AV { get; set; }
public PropertiesAttribute AW { get; set; }
public PropertiesAttribute AX { get; set; }
public PropertiesAttribute AY { get; set; }
public PropertiesAttribute AZ { get; set; }
public PropertiesAttribute BA { get; set; }
public PropertiesAttribute ZJ { get; set; }
public PropertiesAttribute BC { get; set; }
public PropertiesAttribute BD { get; set; }
public PropertiesAttribute BE { get; set; }
public PropertiesAttribute BF { get; set; }
public PropertiesAttribute BG { get; set; }
public PropertiesAttribute BH { get; set; }
public PropertiesAttribute FA { get; set; }
public PropertiesAttribute FB { get; set; }
public PropertiesAttribute BI { get; set; }
public PropertiesAttribute BJ { get; set; }
public PropertiesAttribute BK { get; set; }
public GroupAttribute DG { get; set; }
public PropertiesAttribute CW { get; set; }
public PropertiesAttribute CX { get; set; }
public PropertiesAttribute CY { get; set; }
public PropertiesAttribute CZ { get; set; }
public GroupAttribute ZO1 { get; set; }

public VAS5030()
        {
AB = new PropertiesAttribute(144, typeof(string));
AC = new PropertiesAttribute(2, typeof(string));
AD = new PropertiesAttribute(10, typeof(string));
AE = new PropertiesAttribute(12, typeof(string));
SCG = new PropertiesAttribute(1, typeof(string));
ED = new PropertiesAttribute(1, typeof(string));
AF = new PropertiesAttribute(8, typeof(DateTime));
AG = new PropertiesAttribute(5, typeof(string));
AH = new PropertiesAttribute(50, typeof(string));
AI = new PropertiesAttribute(300, typeof(string));
AL = new PropertiesAttribute(13, typeof(string));
AM = new PropertiesAttribute(300, typeof(string));
AN = new PropertiesAttribute(300, typeof(string));
AO = new PropertiesAttribute(11, typeof(string));
AP = new PropertiesAttribute(8, typeof(DateTime));
AQ = new PropertiesAttribute(8, typeof(DateTime));
AR = new PropertiesAttribute(2, typeof(string));
AS = new PropertiesAttribute(12, typeof(string));
AT = new PropertiesAttribute(3, typeof(string));
AU = new PropertiesAttribute(210, typeof(string));
AV = new PropertiesAttribute(2, typeof(string));
AW = new PropertiesAttribute(210, typeof(string));
AX = new PropertiesAttribute(7, typeof(string));
AY = new PropertiesAttribute(6, typeof(string));
AZ = new PropertiesAttribute(6, typeof(string));
BA = new PropertiesAttribute(1, typeof(string));
ZJ = new PropertiesAttribute(105, typeof(string));
BC = new PropertiesAttribute(7, typeof(string));
BD = new PropertiesAttribute(6, typeof(string));
BE = new PropertiesAttribute(6, typeof(string));
BF = new PropertiesAttribute(1, typeof(string));
BG = new PropertiesAttribute(105, typeof(string));
BH = new PropertiesAttribute(35, typeof(string));
FA = new PropertiesAttribute(1, typeof(string));
FB = new PropertiesAttribute(11, typeof(int));
BI = new PropertiesAttribute(1, typeof(int));
BJ = new PropertiesAttribute(3, typeof(int));
BK = new PropertiesAttribute(765, typeof(string));
CW = new PropertiesAttribute(8, typeof(DateTime));
CX = new PropertiesAttribute(2, typeof(string));
CY = new PropertiesAttribute(8, typeof(DateTime));
CZ = new PropertiesAttribute(2, typeof(string));
#region DG
List<PropertiesAttribute> listDG = new List<PropertiesAttribute>();
listDG.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAS5030_DG1, typeof(string)));
listDG.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAS5030_BP1, typeof(string)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS5030_BQ1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(210, 5, EnumGroupID.VAS5030_BT1, typeof(string)));
listDG.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAS5030_BV1, typeof(string)));
listDG.Add(new PropertiesAttribute(140, 5, EnumGroupID.VAS5030_BU1, typeof(string)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS5030_BR1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAS5030_BS1, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS5030_BW1, typeof(string)));
listDG.Add(new PropertiesAttribute(7, 5, EnumGroupID.VAS5030_BX1, typeof(string)));
listDG.Add(new PropertiesAttribute(6, 5, EnumGroupID.VAS5030_BY1, typeof(string)));
listDG.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAS5030_BZ1, typeof(string)));
listDG.Add(new PropertiesAttribute(6, 5, EnumGroupID.VAS5030_CA1, typeof(string)));
listDG.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAS5030_CB1, typeof(string)));
listDG.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAS5030_CC1, typeof(string)));
listDG.Add(new PropertiesAttribute(38, 5, EnumGroupID.VAS5030_CD1, typeof(string)));
listDG.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAS5030_CE1, typeof(string)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS5030_CF1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(13, 5, EnumGroupID.VAS5030_YA1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS5030_YB1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS5030_YC1, typeof(string)));
listDG.Add(new PropertiesAttribute(13, 5, EnumGroupID.VAS5030_WA1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS5030_WB1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS5030_WC1, typeof(string)));
listDG.Add(new PropertiesAttribute(13, 5, EnumGroupID.VAS5030_WD1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS5030_WE1, typeof(string)));
listDG.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAS5030_WF1, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS5030_OA1, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS5030_OA2, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS5030_OA3, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS5030_OA4, typeof(string)));
listDG.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAS5030_OA5, typeof(string)));
listDG.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAS5030_CM1, typeof(string)));
listDG.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAS5030_CN1, typeof(int)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS5030_PC1, typeof(int)));
listDG.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAS5030_CP1, typeof(string)));
listDG.Add(new PropertiesAttribute(10, 5, EnumGroupID.VAS5030_CQ1, typeof(int)));
listDG.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAS5030_CR1, typeof(string)));
listDG.Add(new PropertiesAttribute(10, 5, EnumGroupID.VAS5030_CS1, typeof(int)));
listDG.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAS5030_CT1, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS5030_RA1, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS5030_RA2, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS5030_RA3, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS5030_RA4, typeof(string)));
listDG.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAS5030_RA5, typeof(string)));
listDG.Add(new PropertiesAttribute(11, 5, EnumGroupID.VAS5030_BM1, typeof(string)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS5030_BN1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAS5030_BO1, typeof(DateTime)));
listDG.Add(new PropertiesAttribute(765, 5, EnumGroupID.VAS5030_CV1, typeof(string)));
DG = new GroupAttribute("DG", 5, listDG);
#endregion DG
#region ZO1
List<PropertiesAttribute> listZO1 = new List<PropertiesAttribute>();
listZO1.Add(new PropertiesAttribute(12, 50, EnumGroupID.VAS5030_ZO1, typeof(string)));
ZO1 = new GroupAttribute("ZO1", 50, listZO1);
#endregion ZO1
TongSoByte = 20502;
}

}public partial class EnumGroupID
    {
public static readonly string VAS5030_DG1 = "VAS5030_DG1";
public static readonly string VAS5030_BP1 = "VAS5030_BP1";
public static readonly string VAS5030_BQ1 = "VAS5030_BQ1";
public static readonly string VAS5030_BT1 = "VAS5030_BT1";
public static readonly string VAS5030_BV1 = "VAS5030_BV1";
public static readonly string VAS5030_BU1 = "VAS5030_BU1";
public static readonly string VAS5030_BR1 = "VAS5030_BR1";
public static readonly string VAS5030_BS1 = "VAS5030_BS1";
public static readonly string VAS5030_BW1 = "VAS5030_BW1";
public static readonly string VAS5030_BX1 = "VAS5030_BX1";
public static readonly string VAS5030_BY1 = "VAS5030_BY1";
public static readonly string VAS5030_BZ1 = "VAS5030_BZ1";
public static readonly string VAS5030_CA1 = "VAS5030_CA1";
public static readonly string VAS5030_CB1 = "VAS5030_CB1";
public static readonly string VAS5030_CC1 = "VAS5030_CC1";
public static readonly string VAS5030_CD1 = "VAS5030_CD1";
public static readonly string VAS5030_CE1 = "VAS5030_CE1";
public static readonly string VAS5030_CF1 = "VAS5030_CF1";
public static readonly string VAS5030_YA1 = "VAS5030_YA1";
public static readonly string VAS5030_YB1 = "VAS5030_YB1";
public static readonly string VAS5030_YC1 = "VAS5030_YC1";
public static readonly string VAS5030_WA1 = "VAS5030_WA1";
public static readonly string VAS5030_WB1 = "VAS5030_WB1";
public static readonly string VAS5030_WC1 = "VAS5030_WC1";
public static readonly string VAS5030_WD1 = "VAS5030_WD1";
public static readonly string VAS5030_WE1 = "VAS5030_WE1";
public static readonly string VAS5030_WF1 = "VAS5030_WF1";
public static readonly string VAS5030_OA1 = "VAS5030_OA1";
public static readonly string VAS5030_OA2 = "VAS5030_OA2";
public static readonly string VAS5030_OA3 = "VAS5030_OA3";
public static readonly string VAS5030_OA4 = "VAS5030_OA4";
public static readonly string VAS5030_OA5 = "VAS5030_OA5";
public static readonly string VAS5030_CM1 = "VAS5030_CM1";
public static readonly string VAS5030_CN1 = "VAS5030_CN1";
public static readonly string VAS5030_PC1 = "VAS5030_PC1";
public static readonly string VAS5030_CP1 = "VAS5030_CP1";
public static readonly string VAS5030_CQ1 = "VAS5030_CQ1";
public static readonly string VAS5030_CR1 = "VAS5030_CR1";
public static readonly string VAS5030_CS1 = "VAS5030_CS1";
public static readonly string VAS5030_CT1 = "VAS5030_CT1";
public static readonly string VAS5030_RA1 = "VAS5030_RA1";
public static readonly string VAS5030_RA2 = "VAS5030_RA2";
public static readonly string VAS5030_RA3 = "VAS5030_RA3";
public static readonly string VAS5030_RA4 = "VAS5030_RA4";
public static readonly string VAS5030_RA5 = "VAS5030_RA5";
public static readonly string VAS5030_BM1 = "VAS5030_BM1";
public static readonly string VAS5030_BN1 = "VAS5030_BN1";
public static readonly string VAS5030_BO1 = "VAS5030_BO1";
public static readonly string VAS5030_CV1 = "VAS5030_CV1";
public static readonly string VAS5030_ZO1 = "VAS5030_ZO1";
}

}
