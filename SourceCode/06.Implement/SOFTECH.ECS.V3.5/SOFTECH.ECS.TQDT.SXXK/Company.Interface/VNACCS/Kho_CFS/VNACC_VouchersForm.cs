﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
using System.IO;
using Janus.Windows.GridEX;
using Company.KD.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class VNACC_VouchersForm : BaseForm
    {
        public KDT_VNACCS_License license = new KDT_VNACCS_License();
        public KDT_VNACCS_License_Detail licenseDetail = new KDT_VNACCS_License_Detail();
        public KDT_VNACCS_ContractDocument contractDocument = new KDT_VNACCS_ContractDocument();
        public KDT_VNACCS_ContractDocument_Detail contractDocumentDetail = new KDT_VNACCS_ContractDocument_Detail();
        public KDT_VNACCS_CommercialInvoice commercialInvoice = new KDT_VNACCS_CommercialInvoice();
        public KDT_VNACCS_CommercialInvoice_Detail commercialInvoiceDetail = new KDT_VNACCS_CommercialInvoice_Detail();
        public KDT_VNACCS_CertificateOfOrigin certificateOfOrigin = new KDT_VNACCS_CertificateOfOrigin();
        public KDT_VNACCS_CertificateOfOrigin_Detail certificateOfOriginDetail = new KDT_VNACCS_CertificateOfOrigin_Detail();
        public KDT_VNACCS_BillOfLading billOfLading = new KDT_VNACCS_BillOfLading();
        public KDT_VNACCS_BillOfLading_Detail billOfLadingDetail = new KDT_VNACCS_BillOfLading_Detail();
        public KDT_VNACCS_Container_Detail container = new KDT_VNACCS_Container_Detail();
        public KDT_VNACCS_AdditionalDocument additionalDocument = new KDT_VNACCS_AdditionalDocument();
        public KDT_VNACCS_AdditionalDocument_Detail additionalDocumentDetail = new KDT_VNACCS_AdditionalDocument_Detail();
        public KDT_VNACCS_OverTime overTime = new KDT_VNACCS_OverTime();
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public System.IO.FileInfo fin;
        public System.IO.FileStream fs;
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public long size;
        public byte[] data;
        public VNACC_VouchersForm()
        {
            InitializeComponent();
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend" :
                    SendVNACCSAll();
                    break;
                case "cmdSave":
                    SaveAll();
                    break;
                case "cmdFeedback":
                    FeedbackAll();
                    break;
                case "cmdResult":
                    ResultAll();
                    break;
                case "cmdEdit":
                    SendVNACCS_OverTime("Edit");
                    break;
                case "cmdCancel":
                    SendVNACCS_OverTime("Cancel");
                    break;
                default:
                    break;
            }
        }
        //Giấy phép
        public void BindDataLicense()
        {
            try
            {
                grListGiayPhep.DataSource = KDT_VNACCS_License.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
                grListGiayPhep.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        //Hợp đồng
        public void BindDataContractDocument()
        {
            try
            {

            grListHopDong.DataSource = KDT_VNACCS_ContractDocument.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
            grListHopDong.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        //Hóa đơn
        public void BindDataCommercialInvoice()
        {
            try
            {
            grListHoaDon.DataSource = KDT_VNACCS_CommercialInvoice.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
            grListHoaDon.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        // CO
        public void BindDataCertificateOfOrigin()
        {
            try
            {
            grListCO.DataSource = KDT_VNACCS_CertificateOfOrigin.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
            grListCO.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        // Vận đơn
        public void BindDataBillOfLading()
        {
            try
            {
            grListVanDon.DataSource = KDT_VNACCS_BillOfLading.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
            grListVanDon.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                                
            }
        }
        // Container
        public void BindDataContainer()
        {
            try
            {
            txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            dtpNgayDangKyToKhai.Value = TKMD.NgayDangKy;
            ctrCoQuanHaiQuan.Code = TKMD.CoQuanHaiQuan;
            ctrMaLoaiHinh.Code = TKMD.MaLoaiHinh;
            grListContainer.DataSource = KDT_VNACCS_Container_Detail.SelectDynamic("TKMD_ID =" + TKMD.ID, "").Tables[0];
            grListContainer.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                   
            }
        }
        // Chứng từ khác
        public void BindDataAdditionalDocument()
        {
            try
            {
            grListCTKhac.DataSource = KDT_VNACCS_AdditionalDocument.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
            grListCTKhac.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        // Đăng ký làm ngoài giờ
        public void BindDataOvertime()
        {
            try
            {
            grListDKLNG.DataSource = KDT_VNACCS_OverTime.SelectAll().Tables[0];
            grListDKLNG.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void SendVNACCSAll()
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep" :
                    SendVNACCS_License();
                    break;
                case "tpHopDong":
                    SendVNACCS_ContractDocument();
                    break;
                case "tpHoaDon":
                    SendVNACCS_CommercialInvoice();
                    break;
                case "tpCO":
                    SendVNACCS_CertificateOfOrigin();
                    break;
                case "tpVanDon":
                    SendVNACCS_BillOfLading();
                    break;
                case "tpContainer":
                    SendVNACCS_Container();
                    break;
                case "tpCTKhac":
                    SendVNACCS_AdditionalDocumentn();
                    break;
                case "tpLamNgoaiGio":
                    string Status="Send";
                    SendVNACCS_OverTime(Status);
                    break;
                default:
                    break;
            }
        }
        public void SendVNACCS_License()
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (license.ID < 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    license = KDT_VNACCS_License.Load(license.ID);
                    license.LicenseCollection = KDT_VNACCS_License_Detail.SelectCollectionBy_License_ID(license.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.License;
                sendXML.master_id = license.ID;

                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    license.GuidStr = Guid.NewGuid().ToString();
                    Licenses_VNACCS licenses_VNACCS = new Licenses_VNACCS();
                    licenses_VNACCS = Mapper_V4.ToDataTransferLicense(license, licenseDetail, TKMD, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = license.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(license.MaHQ)),
                              Identity = license.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = licenses_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = license.GuidStr,
                          },
                          licenses_VNACCS
                        );
                    license.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(license.ID, MessageTitle.RegisterLicense);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.License;
                        sendXML.master_id = license.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        license.Update();
                        FeedbackAll();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        public void SendVNACCS_ContractDocument()
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (contractDocument.ID < 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    contractDocument = KDT_VNACCS_ContractDocument.Load(contractDocument.ID);
                    contractDocument.ContractDocumentCollection = KDT_VNACCS_ContractDocument_Detail.SelectCollectionBy_ContractDocument_ID(contractDocument.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ContractDocument;
                sendXML.master_id = contractDocument.ID;

                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    contractDocument.GuidStr = Guid.NewGuid().ToString();
                    Contract_VNACCS contract_VNACCS = new Contract_VNACCS();
                    contract_VNACCS = Mapper_V4.ToDataTransferContract(contractDocument, contractDocumentDetail, TKMD, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = contractDocument.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(contractDocument.MaHQ)),
                              Identity = contractDocument.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = contract_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = contractDocument.GuidStr,
                          },
                          contract_VNACCS
                        );
                    contractDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(contractDocument.ID, MessageTitle.RegisterContractDocument);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.ContractDocument;
                        sendXML.master_id = contractDocument.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        contractDocument.Update();
                        FeedbackAll();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        public void SendVNACCS_CommercialInvoice()
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (commercialInvoice.ID < 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    commercialInvoice = KDT_VNACCS_CommercialInvoice.Load(commercialInvoice.ID);
                    commercialInvoice.CommercialInvoiceCollection = KDT_VNACCS_CommercialInvoice_Detail.SelectCollectionBy_CommercialInvoice_ID(commercialInvoice.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.CommercialInvoice;
                sendXML.master_id = commercialInvoice.ID;

                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    commercialInvoice.GuidStr = Guid.NewGuid().ToString();
                    CommercialInvoice_VNACCS commercialInvoice_VNACCS = new CommercialInvoice_VNACCS();
                    commercialInvoice_VNACCS = Mapper_V4.ToDataTransferCommercialInvoice(commercialInvoice, commercialInvoiceDetail, TKMD, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = commercialInvoice.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(commercialInvoice.MaHQ)),
                              Identity = commercialInvoice.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = commercialInvoice_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = commercialInvoice.GuidStr,
                          },
                          commercialInvoice_VNACCS
                        );
                    commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(commercialInvoice.ID, MessageTitle.RegisterCommercialInvoicer);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.CommercialInvoice;
                        sendXML.master_id = commercialInvoice.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        commercialInvoice.Update();
                        FeedbackAll();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        public void SendVNACCS_CertificateOfOrigin()
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (certificateOfOrigin.ID < 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    certificateOfOrigin = KDT_VNACCS_CertificateOfOrigin.Load(certificateOfOrigin.ID);
                    certificateOfOrigin.CertificateOfOriginCollection = KDT_VNACCS_CertificateOfOrigin_Detail.SelectCollectionBy_CertificateOfOrigin_ID(certificateOfOrigin.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.CertificateOfOrigin;
                sendXML.master_id = certificateOfOrigin.ID;

                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    certificateOfOrigin.GuidStr = Guid.NewGuid().ToString();
                    CertificateOfOrigins_VNACCS certificateOfOrigins_VNACCS = new CertificateOfOrigins_VNACCS();
                    certificateOfOrigins_VNACCS = Mapper_V4.ToDataTransferCertificateOfOrigin(certificateOfOrigin, certificateOfOriginDetail, TKMD, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = certificateOfOrigin.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(certificateOfOrigin.MaHQ)),
                              Identity = certificateOfOrigin.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = certificateOfOrigins_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = certificateOfOrigin.GuidStr,
                          },
                          certificateOfOrigins_VNACCS
                        );
                    certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(certificateOfOrigin.ID, MessageTitle.RegisterCertificateOfOrigin);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.CertificateOfOrigin;
                        sendXML.master_id = certificateOfOrigin.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        certificateOfOrigin.Update();
                        FeedbackAll();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        public void SendVNACCS_BillOfLading()
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (billOfLading.ID < 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    billOfLading = KDT_VNACCS_BillOfLading.Load(billOfLading.ID);
                    billOfLading.BillOfLadingCollection = KDT_VNACCS_BillOfLading_Detail.SelectCollectionBy_BillOfLading_ID(billOfLading.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.BillOfLading;
                sendXML.master_id = billOfLading.ID;

                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    billOfLading.GuidStr = Guid.NewGuid().ToString();
                    BillOfLading_VNACCS billOfLading_VNACCS = new BillOfLading_VNACCS();
                    billOfLading_VNACCS = Mapper_V4.ToDataTransferBillOfLading(billOfLading, billOfLadingDetail, TKMD, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = billOfLading.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(billOfLading.MaHQ)),
                              Identity = billOfLading.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = billOfLading_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = billOfLading.GuidStr,
                          },
                          billOfLading_VNACCS
                        );
                    billOfLading.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(billOfLading.ID, MessageTitle.RegisterBillOfLading);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.BillOfLading;
                        sendXML.master_id = billOfLading.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        billOfLading.Update();
                        FeedbackAll();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        public void SendVNACCS_Container()
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (container.ID < 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    container = KDT_VNACCS_Container_Detail.Load(container.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.Containers;
                sendXML.master_id = container.ID;

                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    container.GuidStr = Guid.NewGuid().ToString();
                    Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS container_VNACCS = new Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS();
                    container_VNACCS = Mapper_V4.ToDataTransferContainer(container, TKMD, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = container.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(container.MaHQ)),
                              Identity = container.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = container_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = container.GuidStr,
                          },
                          container_VNACCS
                        );
                    container.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(container.ID, MessageTitle.RegisterContainer);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.Containers;
                        sendXML.master_id = container.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        container.Update();
                        FeedbackAll();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        public void SendVNACCS_AdditionalDocumentn()
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (additionalDocument.ID < 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    additionalDocument = KDT_VNACCS_AdditionalDocument.Load(additionalDocument.ID);
                    additionalDocument.AdditionalDocumentCollection = KDT_VNACCS_AdditionalDocument_Detail.SelectCollectionBy_AdditionalDocument_ID(additionalDocument.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.AdditionalDocument;
                sendXML.master_id = additionalDocument.ID;

                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    additionalDocument.GuidStr = Guid.NewGuid().ToString();
                    AdditionalDocument_VNACCS additionalDocument_VNACCS = new AdditionalDocument_VNACCS();
                    additionalDocument_VNACCS = Mapper_V4.ToDataTransferAdditionalDocument(additionalDocument, additionalDocumentDetail, TKMD, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = additionalDocument.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(additionalDocument.MaHQ)),
                              Identity = additionalDocument.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = additionalDocument_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = additionalDocument.GuidStr,
                          },
                          additionalDocument_VNACCS
                        );
                    additionalDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(additionalDocument.ID, MessageTitle.RegisterAdditionalDocumentr);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.AdditionalDocument;
                        sendXML.master_id = additionalDocument.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        additionalDocument.Update();
                        FeedbackAll();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        public void SendVNACCS_OverTime(string Status)
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (overTime.ID < 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    overTime = KDT_VNACCS_OverTime.Load(overTime.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.OverTime;
                sendXML.master_id = overTime.ID;

                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    overTime.GuidStr = Guid.NewGuid().ToString();
                    Overtime_VNACCS overtime_VNACCS = new Overtime_VNACCS();
                    overtime_VNACCS = Mapper_V4.ToDataTransferOvertime(overTime, TKMD, GlobalSettings.TEN_DON_VI, Status);
                    ObjectSend msgSend = new ObjectSend(

                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = overTime.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(overTime.MaHQ)),
                              Identity = overTime.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = overtime_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = overTime.GuidStr,
                          },
                          overtime_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        overTime.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        overTime.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        overTime.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;                        
                    }

                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(overTime.ID, MessageTitle.RegisterOverTime);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.OverTime;
                        sendXML.master_id = overTime.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        overTime.Update();
                        FeedbackAll();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }        
        }
        public void Feedback_License()
        { 
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = license.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.License,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.License,
                };
                subjectBase.Type = DeclarationIssuer.License;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = license.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(license.MaHQ.Trim())),
                                                  Identity = license.MaHQ
                                              }, subjectBase, null);
                if (license.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
        
        }
        public void Feedback_ContractDocument()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = contractDocument.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.ContractDocument,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.ContractDocument,
                };
                subjectBase.Type = DeclarationIssuer.ContractDocument;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = contractDocument.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(contractDocument.MaHQ.Trim())),
                                                  Identity = contractDocument.MaHQ
                                              }, subjectBase, null);
                if (contractDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }

        }
        public void Feedback_CommercialInvoice()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = commercialInvoice.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.CommercialInvoice,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.CommercialInvoice,
                };
                subjectBase.Type = DeclarationIssuer.CommercialInvoice;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = commercialInvoice.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(commercialInvoice.MaHQ.Trim())),
                                                  Identity = commercialInvoice.MaHQ
                                              }, subjectBase, null);
                if (commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }

        }
        public void Feedback_CertificateOfOrigin()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = certificateOfOrigin.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.CertificateOfOrigin,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.CertificateOfOrigin,
                };
                subjectBase.Type = DeclarationIssuer.CertificateOfOrigin;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = certificateOfOrigin.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(certificateOfOrigin.MaHQ.Trim())),
                                                  Identity = certificateOfOrigin.MaHQ
                                              }, subjectBase, null);
                if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }

        }
        public void Feedback_BillOfLading()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = billOfLading.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.BillOfLading,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.BillOfLading,
                };
                subjectBase.Type = DeclarationIssuer.BillOfLading;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = billOfLading.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(billOfLading.MaHQ.Trim())),
                                                  Identity = billOfLading.MaHQ
                                              }, subjectBase, null);
                if (billOfLading.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }

        }
        public void Feedback_Container()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = certificateOfOrigin.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.Containers,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.Containers,
                };
                subjectBase.Type = DeclarationIssuer.Containers;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = container.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(container.MaHQ.Trim())),
                                                  Identity = container.MaHQ
                                              }, subjectBase, null);
                if (container.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }

        }
        public void Feedback_AdditionalDocumentn()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = additionalDocument.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.AdditionalDocument,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.AdditionalDocument,
                };
                subjectBase.Type = DeclarationIssuer.AdditionalDocument;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = additionalDocument.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(additionalDocument.MaHQ.Trim())),
                                                  Identity = additionalDocument.MaHQ
                                              }, subjectBase, null);
                if (additionalDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }

        }
        private void Feedback_OverTime()
        { 
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = overTime.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.OverTime,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.OverTime,
                };
                subjectBase.Type = DeclarationIssuer.OverTime;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = overTime.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(overTime.MaHQ.Trim())),
                                                  Identity = overTime.MaHQ
                                              }, subjectBase, null);
                if (overTime.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
        }
        private void FeedbackAll()
        {
                switch (tbCtrVouchers.SelectedTab.Name)
                {
                    case "tpGiayPhep":
                        Feedback_License(); 
                        break;
                    case "tpHopDong":
                        Feedback_ContractDocument();
                        break;
                    case "tpHoaDon":
                        Feedback_CommercialInvoice();
                        break;
                    case "tpCO":
                        Feedback_CertificateOfOrigin();
                        break;
                    case "tpVanDon":
                        Feedback_BillOfLading();
                        break;
                    case "tpContainer":
                        Feedback_Container();
                        break;
                    case "tpCTKhac":
                        Feedback_AdditionalDocumentn();
                        break;
                    case "tpLamNgoaiGio":
                        Feedback_OverTime();
                        break;
                    default:
                        break;
                }
        }
        public void SaveAll()
        {
            try
            {
                switch (tbCtrVouchers.SelectedTab.Name)
                {
                    case "tpGiayPhep":
                        if (String.IsNullOrEmpty(lblFileNameGiayPhep.Text))
                        {
                            ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("License");
                        license.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataLicense();
                        break;
                    case "tpHopDong":
                        if (String.IsNullOrEmpty(lblFileNameHopDong.Text))
                        {
                            ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("ContractDocument");
                        contractDocument.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataContractDocument();
                        break;
                    case "tpHoaDon":
                        if (String.IsNullOrEmpty(lblFileNameHoaDon.Text))
                        {
                            ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("CommercialInvoice");
                        commercialInvoice.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataCommercialInvoice();
                        break;
                    case "tpCO":
                        if (String.IsNullOrEmpty(lblFileNameCO.Text))
                        {
                            ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("CertificateOfOrigin");
                        certificateOfOrigin.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        BindDataCertificateOfOrigin();
                        break;
                    case "tpVanDon":
                        if (String.IsNullOrEmpty(lblFileNameVanDon.Text))
                        {
                            ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("BillOfLading");
                        billOfLading.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataBillOfLading();
                        break;
                    case "tpContainer":
                        if (String.IsNullOrEmpty(lblFileNamContainer.Text))
                        {
                            ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                            return;
                        }
                        GetVouchers("Container");
                        container.InsertUpdate();
                        ShowMessage("Lưu thành công", false);
                        BindDataContainer();
                        break;
                    case "tpCTKhac":
                        if (String.IsNullOrEmpty(lblFileNameCTKhac.Text))
                        {
                            ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("AdditionalDocument");
                        additionalDocument.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataAdditionalDocument();
                        break;
                    case "tpLamNgoaiGio":
                        GetVouchers("OverTime");
                        overTime.InsertUpdate();
                        ShowMessage("Lưu thành công", false);
                        BindDataOvertime();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);               
            }
            
        }
        public void GetVouchers(string Vouchers)
        {
            switch (Vouchers)
            {
                case "License":
                    license.TKMD_ID = TKMD.ID;
                    license.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    license.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                    license.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    license.GhiChuKhac = txtGhiChuGiayPhep.Text.ToString();
                    license.FileName = lblFileNameGiayPhep.Text;

                    license.LicenseCollection.Clear();
                    licenseDetail.NguoiCapGP = txtNguoiCapGiayPhep.Text.ToString();
                    licenseDetail.SoGP = txtSoGiayPhep.Text.ToString();
                    licenseDetail.NgayCapGP = dtpNgayCapGiayPhep.Value;
                    licenseDetail.NoiCapGP = txtNoiCapGiayPhep.Text.ToString();
                    licenseDetail.LoaiGP = txtLoaiGiayPhep.Text.ToString();
                    licenseDetail.NgayHetHanGP = dtpNgayHetHanGiayPhep.Value;
                    license.LicenseCollection.Add(licenseDetail);
                    break;
                case "ContractDocument":
                    contractDocument.TKMD_ID = TKMD.ID;
                    contractDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    contractDocument.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                    contractDocument.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    contractDocument.GhiChuKhac = txtGhiChuHopDong.Text.ToString();
                    contractDocument.FileName = lblFileNameHopDong.Text;

                    contractDocument.ContractDocumentCollection.Clear();
                    contractDocumentDetail.SoHopDong = txtSoHopDong.Text.ToString();
                    contractDocumentDetail.NgayHopDong = dtpNgayHopDong.Value;
                    contractDocumentDetail.ThoiHanThanhToan = dtpThoiHanThanhToan.Value;
                    contractDocumentDetail.TongTriGia = Convert.ToDecimal(txtTongTriGiaHopDong.Text.ToString());
                    contractDocument.ContractDocumentCollection.Add(contractDocumentDetail);
                    break;
                case "CommercialInvoice":
                    commercialInvoice.TKMD_ID = TKMD.ID;
                    commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    commercialInvoice.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                    commercialInvoice.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    commercialInvoice.GhiChuKhac = txtGhiChuHoaDon.Text.ToString();
                    commercialInvoice.FileName = lblFileNameHoaDon.Text;

                    commercialInvoice.CommercialInvoiceCollection.Clear();
                    commercialInvoiceDetail.SoHoaDonTM = txtSoHoaDonTM.Text.ToString();
                    commercialInvoiceDetail.NgayPhatHanhHDTM = dtpNgayPhatHanhHoaDonTM.Value;
                    commercialInvoice.CommercialInvoiceCollection.Add(commercialInvoiceDetail);
                    break;
                case "CertificateOfOrigin":
                    certificateOfOrigin.TKMD_ID = TKMD.ID;
                    certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    certificateOfOrigin.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                    certificateOfOrigin.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    certificateOfOrigin.GhiChuKhac = txtGhiChuCO.Text.ToString();
                    certificateOfOrigin.FileName = lblFileNameCO.Text;

                    certificateOfOrigin.CertificateOfOriginCollection.Clear();
                    certificateOfOriginDetail.SoCO = txtSoCO.Text.ToString();
                    certificateOfOriginDetail.LoaiCO = cbbLoaiCO.SelectedValue.ToString();
                    certificateOfOriginDetail.NgayCapCO = dateNgayCapCO.Value;
                    certificateOfOriginDetail.NuocCapCO = txtNuocCapCO.Ma.ToString();
                    certificateOfOriginDetail.ToChucCapCO = txtToChucCapCO.Text.ToString();
                    certificateOfOriginDetail.NguoiCapCO = txtNguoiCapCO.Text.ToString();
                    certificateOfOrigin.CertificateOfOriginCollection.Add(certificateOfOriginDetail);
                    break;
                case "BillOfLading":
                    billOfLading.TKMD_ID = TKMD.ID;
                    billOfLading.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    billOfLading.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                    billOfLading.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    billOfLading.GhiChuKhac = txtGhiChuVanDon.Text.ToString();
                    billOfLading.FileName = lblFileNameVanDon.Text;

                    billOfLading.BillOfLadingCollection.Clear();                   
                    billOfLadingDetail.SoVanDon = txtSoVanDon.Text.ToString();
                    billOfLadingDetail.NgayVanDon = dtpNgayVanDon.Value;
                    billOfLadingDetail.NuocPhatHanh = ctrNuocPHVanDon.Ma.ToString();
                    billOfLadingDetail.DiaDiemCTQC = txtDiaDiemCTQC.Text.ToString();
                    billOfLadingDetail.LoaiVanDon = Convert.ToDecimal(cbbLoaiVanDon.SelectedValue.ToString());
                    billOfLading.BillOfLadingCollection.Add(billOfLadingDetail);
                    break;
                case "Container":
                    container.TKMD_ID = TKMD.ID;
                    container.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    container.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                    container.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    container.GhiChuKhac = txtGhiChuContainer.Text.ToString();
                    container.FileName = lblFileNamContainer.Text;
                    break;
                case "AdditionalDocument":
                    additionalDocument.TKMD_ID = TKMD.ID;
                    additionalDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    additionalDocument.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                    additionalDocument.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    additionalDocument.GhiChuKhac = txtGhiChuCTKhac.Text.ToString();
                    additionalDocument.FileName = lblFileNameCTKhac.Text;

                    additionalDocument.AdditionalDocumentCollection.Clear();
                    additionalDocumentDetail.SoChungTu = txtSoChungTuKhac.Text.ToString();
                    additionalDocumentDetail.TenChungTu = txtTenChungTuKhac.Text.ToString();
                    additionalDocumentDetail.NgayPhatHanh = dtpNgayChungTuKhac.Value;
                    additionalDocumentDetail.NoiPhatHanh = txtNoiPhatHanhCTKhac.Text.ToString();
                    additionalDocument.AdditionalDocumentCollection.Add(additionalDocumentDetail);
                    break;
                case "OverTime":
                    overTime.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    overTime.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                    overTime.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    overTime.NgayDangKy = dtpGioLamTT.Value;
                    overTime.GioLamThuTuc = dtpGioLamTT.Value.Hour + ":" + dtpGioLamTT.Value.Minute;
                    overTime.NoiDung = txtNoiDung.Text.ToString();
                    break;
                default:
                    break;
            }
        }
        public void ResultAll()
        {
            ThongDiepForm form = new ThongDiepForm();
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    form.ItemID = license.ID;
                    form.DeclarationIssuer = DeclarationIssuer.License;
                    form.ShowDialog(this);
                    break;
                case "tpHopDong":
                    form.ItemID = contractDocument.ID;
                    form.DeclarationIssuer = DeclarationIssuer.ContractDocument;
                    form.ShowDialog(this);
                    break;
                case "tpHoaDon":
                    form.ItemID = commercialInvoice.ID;
                    form.DeclarationIssuer = DeclarationIssuer.CommercialInvoice;
                    form.ShowDialog(this);
                    break;
                case "tpCO":
                    form.ItemID = certificateOfOrigin.ID;
                    form.DeclarationIssuer = DeclarationIssuer.CertificateOfOrigin;
                    form.ShowDialog(this);
                    break;
                case "tpVanDon":
                    form.ItemID = billOfLading.ID;
                    form.DeclarationIssuer = DeclarationIssuer.BillOfLading;
                    form.ShowDialog(this);
                    break;
                case "tpContainer":
                    form.ItemID = container.ID;
                    form.DeclarationIssuer = DeclarationIssuer.Containers;
                    form.ShowDialog(this);
                    break;
                case "tpCTKhac":
                    form.ItemID = additionalDocument.ID;
                    form.DeclarationIssuer = DeclarationIssuer.AdditionalDocument;
                    form.ShowDialog(this);
                    break;
                case "tpLamNgoaiGio":
                    form.ItemID = overTime.ID;
                    form.DeclarationIssuer = DeclarationIssuer.OverTime;
                    form.ShowDialog(this);
                    break;
                default:
                    break;
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                switch (tbCtrVouchers.SelectedTab.Name)
                {
                    case "tpGiayPhep":
                        isValid &= ValidateControl.ValidateNull(txtNguoiCapGiayPhep, errorProvider, "Người cấp giấy phép", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtSoGiayPhep, errorProvider, "Số giấy phép", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(dtpNgayCapGiayPhep, errorProvider, "Ngày cấp giấy phép", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtNoiCapGiayPhep, errorProvider, "Nơi cấp giấy phép", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtLoaiGiayPhep, errorProvider, "Loại giấy phép ", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(dtpNgayHetHanGiayPhep, errorProvider, "Ngày hết hạn giấy phép ", isOnlyWarning);
                        break;
                    case "tpHopDong":
                        isValid &= ValidateControl.ValidateNull(txtSoHopDong, errorProvider, "Số hợp đồng", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(dtpNgayHopDong, errorProvider, "Ngày hợp đồng", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(dtpThoiHanThanhToan, errorProvider, "Thời hạn thanh toán", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtTongTriGiaHopDong, errorProvider, "Tổng trị giá ", isOnlyWarning);
                        break;
                    case "tpHoaDon":
                        isValid &= ValidateControl.ValidateNull(txtSoHoaDonTM, errorProvider, "Số hóa đơn thương mại", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(dtpSoPhatHanhHoaDonTM, errorProvider, "Ngày phát hành hóa đơn", isOnlyWarning);
                        break;
                    case "tpCO":
                        isValid &= ValidateControl.ValidateNull(txtSoCO, errorProvider, "Số CO", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(cbbLoaiCO, errorProvider, "Loại CO", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtToChucCapCO, errorProvider, "Tổ chức cấp", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtNguoiCapCO, errorProvider, "Người cấp", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtGhiChuCO, errorProvider, "Ghi chú khác ", isOnlyWarning);
                        break;
                    case "tpVanDon":
                        isValid &= ValidateControl.ValidateNull(txtSoVanDon, errorProvider, "Số vận đơn", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(dtpNgayVanDon, errorProvider, "Ngày vận đơn", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateChoose(ctrNuocPHVanDon, errorProvider, "Nước phát hành", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtDiaDiemCTQC, errorProvider, "Địa điểm chuyển tải quá cảnh", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(cbbLoaiVanDon, errorProvider, "Loại vận đơn ", isOnlyWarning);
                        break;
                    case "tpCTKhac":
                        isValid &= ValidateControl.ValidateNull(txtSoChungTuKhac, errorProvider, "Số chứng từ", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtTenChungTuKhac, errorProvider, "Tên chứng từ", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateDate(dtpNgayChungTuKhac, errorProvider, "Ngày chứng từ", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtNoiPhatHanhCTKhac, errorProvider, "Nơi phát hành", isOnlyWarning);                        
                        break;
                    case "tpLamNgoaiGio":
                        isValid &= ValidateControl.ValidateNull(txtNoiDung, errorProvider, "Nội dung làm ngoài giờ", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(txtTenChungTuKhac, errorProvider, "Tên chứng từ", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(dtpNgayChungTuKhac, errorProvider, "Ngày chứng từ", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(txtNoiPhatHanhCTKhac, errorProvider, "Nơi phát hành", isOnlyWarning);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void tbCtrVouchers_Selected(object sender, TabControlEventArgs e)
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    BindDataLicense();
                    break;
                case "tpHopDong":
                    BindDataContractDocument();
                    break;
                case "tpHoaDon":
                    BindDataCommercialInvoice();
                    break;
                case "tpCO":
                    BindDataCertificateOfOrigin();
                    break;
                case "tpVanDon":
                    BindDataBillOfLading();
                    break;
                case "tpContainer":
                    BindDataContainer();
                    break;
                case "tpCTKhac":
                    BindDataAdditionalDocument();
                    break;
                case "tpLamNgoaiGio":
                    BindDataOvertime();
                    break;
                default:
                    break;
            }
        }

        public void AttachFile(string Vouchers)
        {
            OpenFileDialog OpenFileDialog = new OpenFileDialog();

            OpenFileDialog.FileName = "";
            OpenFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                    + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                    + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

            OpenFileDialog.Multiselect = false;
            if (OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.FileInfo fin = new System.IO.FileInfo(OpenFileDialog.FileName);
                if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                  && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                  && fin.Extension.ToUpper() != ".gif".ToUpper()
                  && fin.Extension.ToUpper() != ".tiff".ToUpper()
                  && fin.Extension.ToUpper() != ".txt".ToUpper()
                  && fin.Extension.ToUpper() != ".xml".ToUpper()
                  && fin.Extension.ToUpper() != ".xsl".ToUpper()
                  && fin.Extension.ToUpper() != ".csv".ToUpper()
                  && fin.Extension.ToUpper() != ".doc".ToUpper()
                  && fin.Extension.ToUpper() != ".mdb".ToUpper()
                  && fin.Extension.ToUpper() != ".pdf".ToUpper()
                  && fin.Extension.ToUpper() != ".ppt".ToUpper()
                  && fin.Extension.ToUpper() != ".xls".ToUpper())
                {
                    ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                }
                else
                {
                    System.IO.FileStream fs = new System.IO.FileStream(OpenFileDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    size = 0;
                    size = fs.Length;
                    data = new byte[fs.Length];
                    fs.Read(data, 0, data.Length);
                    filebase64 = System.Convert.ToBase64String(data);
                    switch (tbCtrVouchers.SelectedTab.Name)
                    {
                        case "tpGiayPhep":
                            lblFileNameGiayPhep.Text = fin.Name;
                            license.Content = filebase64;
                            break;
                        case "tpHopDong":
                            lblFileNameHopDong.Text = fin.Name;
                            contractDocument.Content = filebase64;
                            break;
                        case "tpHoaDon":
                            lblFileNameHoaDon.Text = fin.Name;
                            commercialInvoice.Content = filebase64;
                            break;
                        case "tpCO":
                            lblFileNameCO.Text = fin.Name;
                            certificateOfOrigin.Content = filebase64;
                            break;
                        case "tpVanDon":
                            lblFileNameVanDon.Text = fin.Name;
                            billOfLading.Content = filebase64;
                            break;
                        case "tpContainer":
                            lblFileNamContainer.Text = fin.Name;
                            container.Content = filebase64;
                            break;
                        case "tpCTKhac":
                            lblFileNameCTKhac.Text = fin.Name;
                            additionalDocument.Content = filebase64;
                            break;
                        default:
                            break;
                    }
                }

            }

        }
        public void View(string Vouchers)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string path = Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                string fileName = "";
                switch (Vouchers)
                {
                    case "License":
                        if (String.IsNullOrEmpty(license.FileName))
                        {
                            ShowMessage("Bạn chưa chọn File để xem.", false);
                            return;
                        }
                        fileName = path + "\\" + license.FileName;
                        break;
                    case "ContractDocument":
                        if (String.IsNullOrEmpty(contractDocument.FileName))
                        {
                            ShowMessage("Bạn chưa chọn File để xem.", false);
                            return;
                        }
                        fileName = path + "\\" + contractDocument.FileName;
                        break;
                    case "CommercialInvoice":
                        if (String.IsNullOrEmpty(commercialInvoice.FileName))
                        {
                            ShowMessage("Bạn chưa chọn File để xem.", false);
                            return;
                        }
                        fileName = path + "\\" + commercialInvoice.FileName;
                        break;
                    case "CertificateOfOrigin":
                        certificateOfOrigin = KDT_VNACCS_CertificateOfOrigin.Load(certificateOfOrigin.ID);
                        if (String.IsNullOrEmpty(certificateOfOrigin.FileName))
                        {
                            ShowMessage("Bạn chưa chọn File để xem.", false);
                            return;
                        }
                        fileName = path + "\\" + certificateOfOrigin.FileName;
                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }
                        System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite);
                        byte[] bytes = new byte[fs.Length];
                        bytes = Convert.FromBase64String(certificateOfOrigin.Content);
                        fs.Write(bytes, 0, bytes.Length);
                        System.Diagnostics.Process.Start(fileName);
                        break;
                    case "BillOfLading":
                        if (String.IsNullOrEmpty(billOfLading.FileName))
                        {
                            ShowMessage("Bạn chưa chọn File để xem.", false);
                            return;
                        }
                        fileName = path + "\\" + billOfLading.FileName;
                        break;
                    case "Container":
                        if (String.IsNullOrEmpty(container.FileName))
                        {
                            ShowMessage("Bạn chưa chọn File để xem.", false);
                            return;
                        }
                        fileName = path + "\\" + container.FileName;
                        break;
                    case "AdditionalDocument":
                        if (String.IsNullOrEmpty(additionalDocument.FileName))
                        {
                            ShowMessage("Bạn chưa chọn File để xem.", false);
                            return;
                        }
                        fileName = path + "\\" + additionalDocument.FileName;
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                Cursor = Cursors.Default;
            }        
        
        }
        private void btnAddCO_Click(object sender, EventArgs e)
        {
            AttachFile("CertificateOfOrigin");
        }
        private void btnAddVanDon_Click(object sender, EventArgs e)
        {
            AttachFile("BillOfLading");
        }

        private void btnAddHopDong_Click(object sender, EventArgs e)
        {
            AttachFile("ContractDocument");
        }

        private void btnAddHoaDon_Click(object sender, EventArgs e)
        {
            AttachFile("CommercialInvoice");
        }

        private void btnAddGiayPhep_Click(object sender, EventArgs e)
        {
            AttachFile("License");
        }

        private void btnAddContainer_Click(object sender, EventArgs e)
        {
            AttachFile("Container");
        }

        private void btnAddCTKhac_Click(object sender, EventArgs e)
        {
            AttachFile("AdditionalDocument");
        }

        private void btnViewCO_Click(object sender, EventArgs e)
        {
            View("CertificateOfOrigin");
        }

        private void btnViewVanDon_Click(object sender, EventArgs e)
        {
            View("BillOfLading");
        }

        private void btnViewHopDong_Click(object sender, EventArgs e)
        {
            View("ContractDocument");
        }

        private void btnViewHoaDon_Click(object sender, EventArgs e)
        {
            View("CommercialInvoice");
        }

        private void btnViewGiayPhep_Click(object sender, EventArgs e)
        {
            View("License");
        }

        private void btnViewContainer_Click(object sender, EventArgs e)
        {
            View("Container");
        }

        private void btnViewCTKhac_Click(object sender, EventArgs e)
        {
            View("AdditionalDocument");
        }

        private void btnDeleteCO_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListCO.SelectedItems;
                if (grListCO.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessage("Bạn có muốn xóa CO này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        certificateOfOrigin.ID = Convert.ToInt64(grListCO.GetRow().Cells["ID"].Value.ToString());
                        if (certificateOfOrigin.ID == 0)
                        {
                            continue;
                        }
                        if (certificateOfOrigin.ID > 0)
                        {
                            certificateOfOrigin.CertificateOfOriginCollection = KDT_VNACCS_CertificateOfOrigin_Detail.SelectCollectionBy_CertificateOfOrigin_ID(certificateOfOrigin.ID);
                            certificateOfOrigin.DeleteFull();
                        }
                    }
                    certificateOfOrigin.ID = 0;
                    certificateOfOriginDetail.ID = 0;
                    lblFileNameCO.Text = String.Empty;
                }
                BindDataCertificateOfOrigin();
            }
            catch (Exception ex) 
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }

        }

        private void btnDeleteVanDon_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListVanDon.SelectedItems;
                if (grListVanDon.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessage("Bạn có muốn xóa Vận đơn này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        billOfLading.ID = Convert.ToInt64(grListVanDon.GetRow().Cells["ID"].Value.ToString());
                        if (billOfLading.ID == 0)
                        {
                            continue;
                        }
                        if (billOfLading.ID > 0)
                        {
                            billOfLading.BillOfLadingCollection = KDT_VNACCS_BillOfLading_Detail.SelectCollectionBy_BillOfLading_ID(billOfLading.ID);
                            billOfLading.DeleteFull();
                        }
                    }
                    billOfLading.ID = 0;
                    billOfLadingDetail.BillOfLading_ID = 0;
                    lblFileNameVanDon.Text = String.Empty;
                }
                BindDataBillOfLading();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }

        }

        private void btnDeleteHopDong_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListHopDong.SelectedItems;
                if (grListHopDong.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessage("Bạn có muốn Hợp đồng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        contractDocument.ID = Convert.ToInt64(grListHopDong.GetRow().Cells["ID"].Value.ToString());
                        if (contractDocument.ID == 0)
                        {
                            continue;
                        }
                        if (contractDocument.ID > 0)
                        {
                            contractDocument.ContractDocumentCollection = KDT_VNACCS_ContractDocument_Detail.SelectCollectionBy_ContractDocument_ID(contractDocument.ID);
                            contractDocument.DeleteFull();
                        }
                    }
                    contractDocument.ID = 0;
                    contractDocumentDetail.ID = 0;
                    lblFileNameHopDong.Text = String.Empty;
                }
                BindDataContractDocument();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }

        }

        private void btnDeleteHoaDon_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListHoaDon.SelectedItems;
                if (grListHoaDon.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessage("Bạn có muốn xóa Hóa đơn này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        commercialInvoice.ID = Convert.ToInt64(grListHoaDon.GetRow().Cells["ID"].Value.ToString());
                        if (commercialInvoice.ID == 0)
                        {
                            continue;
                        }
                        if (commercialInvoice.ID > 0)
                        {
                            commercialInvoice.CommercialInvoiceCollection = KDT_VNACCS_CommercialInvoice_Detail.SelectCollectionBy_CommercialInvoice_ID(commercialInvoice.ID);
                            commercialInvoice.DeleteFull();
                        }
                    }
                    commercialInvoice.ID = 0;
                    commercialInvoiceDetail.ID = 0;
                    lblFileNameHoaDon.Text = String.Empty;
                }
                BindDataCommercialInvoice();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }

        }

        private void btnDeleteGiayPhep_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListGiayPhep.SelectedItems;
                if (grListGiayPhep.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessage("Bạn có muốn xóa Giấy phép này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        license.ID = Convert.ToInt64(grListGiayPhep.GetRow().Cells["ID"].Value.ToString());
                        if (license.ID == 0)
                        {
                            continue;
                        }
                        if (license.ID > 0)
                        {
                            license.LicenseCollection = KDT_VNACCS_License_Detail.SelectCollectionBy_License_ID(license.ID);
                            license.DeleteFull();
                        }
                    }
                    license.ID = 0;
                    licenseDetail.ID = 0;
                    lblFileNameGiayPhep.Text = String.Empty;
                }
                BindDataLicense();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }

        }

        private void btnDeleteContainer_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListContainer.SelectedItems;
                if (grListContainer.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessage("Bạn có muốn xóa Container này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        container.ID = Convert.ToInt64(grListContainer.GetRow().Cells["ID"].Value.ToString());
                        if (container.ID == 0)
                        {
                            continue;
                        }
                        if (container.ID > 0)
                        {
                            container.Delete();
                        }
                    }
                    container.ID = 0;
                }
                BindDataContainer();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }

        }

        private void btnDeleteCTKhac_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListCTKhac.SelectedItems;
                if (grListCTKhac.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessage("Bạn có muốn xóa Chứng từ này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        additionalDocument.ID = Convert.ToInt64(grListCTKhac.GetRow().Cells["ID"].Value.ToString());
                        if (additionalDocument.ID == 0)
                        {
                            continue;
                        }
                        if (additionalDocument.ID > 0)
                        {
                            additionalDocument.AdditionalDocumentCollection = KDT_VNACCS_AdditionalDocument_Detail.SelectCollectionBy_AdditionalDocument_ID(additionalDocument.ID);
                            additionalDocument.DeleteFull();
                        }
                    }
                    additionalDocument.ID = 0;
                    additionalDocumentDetail.ID = 0;
                    lblFileNameCTKhac.Text = String.Empty;
                }
                BindDataAdditionalDocument();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }

        }

        private void btnDeleteOverTime_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListDKLNG.SelectedItems;
                if (grListDKLNG.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessage("Bạn có muốn xóa Đăng ký làm ngoài giờ này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        overTime.ID = Convert.ToInt64(grListDKLNG.GetRow().Cells["ID"].Value.ToString());
                        if (overTime.ID == 0)
                        {
                            continue;
                        }
                        if (overTime.ID > 0)
                        {
                            overTime.Delete();
                        }
                    }
                    overTime.ID = 0;
                }
                BindDataOvertime();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void grListCO_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    certificateOfOrigin.ID = Convert.ToInt64(grListCO.GetRow().Cells["ID"].Value.ToString());
                    certificateOfOrigin = KDT_VNACCS_CertificateOfOrigin.Load(certificateOfOrigin.ID);
                    txtSoCO.Text = grListCO.GetRow().Cells["SoCO"].Value.ToString();
                    cbbLoaiCO.SelectedValue = grListCO.GetRow().Cells["LoaiCO"].Value.ToString();
                    txtToChucCapCO.Text = grListCO.GetRow().Cells["ToChucCapCO"].Value.ToString();
                    dateNgayCapCO.Value = Convert.ToDateTime(grListCO.GetRow().Cells["NgayCapCO"].Value.ToString());
                    txtNuocCapCO.Text = grListCO.GetRow().Cells["NuocCapCO"].Value.ToString();
                    txtNguoiCapCO.Text = grListCO.GetRow().Cells["NguoiCapCO"].Value.ToString();
                    txtGhiChuCO.Text = grListCO.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameCO.Text = grListCO.GetRow().Cells["FileName"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }


        }
        private void grListVanDon_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    billOfLading.ID = Convert.ToInt64(grListVanDon.GetRow().Cells["ID"].Value.ToString());
                    billOfLading = KDT_VNACCS_BillOfLading.Load(billOfLading.ID);
                    txtSoVanDon.Text = grListVanDon.GetRow().Cells["SoVanDon"].Value.ToString();
                    dtpNgayVanDon.Value = Convert.ToDateTime(grListVanDon.GetRow().Cells["NgayVanDon"].Value.ToString());
                    ctrNuocPHVanDon.Text = grListVanDon.GetRow().Cells["NuocPhatHanh"].Value.ToString();
                    txtDiaDiemCTQC.Text = grListVanDon.GetRow().Cells["DiaDiemCTQC"].Value.ToString();
                    cbbLoaiVanDon.SelectedValue = grListVanDon.GetRow().Cells["LoaiVanDon"].Value.ToString();
                    txtGhiChuVanDon.Text = grListVanDon.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameVanDon.Text = grListVanDon.GetRow().Cells["FileName"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }

        }

        private void grListHopDong_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    contractDocument.ID = Convert.ToInt64(grListHopDong.GetRow().Cells["ID"].Value.ToString());
                    contractDocument = KDT_VNACCS_ContractDocument.Load(contractDocument.ID);
                    txtSoHopDong.Text = grListHopDong.GetRow().Cells["SoHopDong"].Value.ToString();
                    dtpNgayHopDong.Value = Convert.ToDateTime(grListHopDong.GetRow().Cells["NgayHopDong"].Value.ToString());
                    dtpThoiHanThanhToan.Value = Convert.ToDateTime(grListHopDong.GetRow().Cells["ThoiHanThanhToan"].Value.ToString());
                    txtTongTriGiaHopDong.Text = grListHopDong.GetRow().Cells["TongTriGia"].Value.ToString();
                    txtGhiChuHopDong.Text = grListHopDong.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameHopDong.Text = grListHopDong.GetRow().Cells["FileName"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }

        private void grListHoaDon_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    commercialInvoice.ID = Convert.ToInt64(grListHoaDon.GetRow().Cells["ID"].Value.ToString());
                    commercialInvoice = KDT_VNACCS_CommercialInvoice.Load(commercialInvoice.ID);
                    txtSoHoaDonTM.Text = grListHoaDon.GetRow().Cells["SoHoaDonTM"].Value.ToString();
                    dtpNgayPhatHanhHoaDonTM.Value = Convert.ToDateTime(grListHoaDon.GetRow().Cells["NgayPhatHanhHDTM"].Value.ToString());
                    txtGhiChuHoaDon.Text = grListHoaDon.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameHoaDon.Text = grListHoaDon.GetRow().Cells["FileName"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }

        private void grListGiayPhep_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    license.ID = Convert.ToInt64(grListGiayPhep.GetRow().Cells["ID"].Value.ToString());
                    license = KDT_VNACCS_License.Load(license.ID);
                    txtNguoiCapGiayPhep.Text = grListGiayPhep.GetRow().Cells["NguoiCapGP"].Value.ToString();
                    txtSoGiayPhep.Text = grListGiayPhep.GetRow().Cells["SoGP"].Value.ToString();
                    dtpNgayCapGiayPhep.Value = Convert.ToDateTime(grListGiayPhep.GetRow().Cells["NgayCapGP"].Value.ToString());
                    txtNoiCapGiayPhep.Text = grListGiayPhep.GetRow().Cells["NoiCapGP"].Value.ToString();
                    txtLoaiGiayPhep.Text = grListGiayPhep.GetRow().Cells["LoaiGP"].Value.ToString();
                    dtpNgayHetHanGiayPhep.Value = Convert.ToDateTime(grListGiayPhep.GetRow().Cells["NgayHetHanGP"].Value.ToString());
                    txtGhiChuGiayPhep.Text = grListGiayPhep.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameGiayPhep.Text = grListGiayPhep.GetRow().Cells["FileName"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }

        private void grListContainer_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    container.ID = Convert.ToInt64(grListContainer.GetRow().Cells["ID"].Value.ToString());
                    container = KDT_VNACCS_Container_Detail.Load(container.ID);
                    txtGhiChuContainer.Text = grListContainer.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNamContainer.Text = grListContainer.GetRow().Cells["FileName"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }

        private void grListCTKhac_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    additionalDocument.ID = Convert.ToInt64(grListCTKhac.GetRow().Cells["ID"].Value.ToString());
                    additionalDocument = KDT_VNACCS_AdditionalDocument.Load(additionalDocument.ID);
                    txtSoChungTuKhac.Text = grListCTKhac.GetRow().Cells["SoChungTu"].Value.ToString();
                    txtTenChungTuKhac.Text = grListCTKhac.GetRow().Cells["TenChungTu"].Value.ToString();
                    dtpNgayChungTuKhac.Value = Convert.ToDateTime(grListCTKhac.GetRow().Cells["NgayPhatHanh"].Value.ToString());
                    txtNoiPhatHanhCTKhac.Text = grListCTKhac.GetRow().Cells["NoiPhatHanh"].Value.ToString();
                    txtGhiChuCTKhac.Text = grListCTKhac.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameCTKhac.Text = additionalDocument.FileName;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }

        private void grListDKLNG_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    overTime.ID = Convert.ToInt64(grListDKLNG.GetRow().Cells["ID"].Value.ToString());
                    overTime = KDT_VNACCS_OverTime.Load(overTime.ID);
                    txtNoiDung.Text = grListDKLNG.GetRow().Cells["NoiDung"].Value.ToString();
                    dtpNgayDangKyThuTuc.Value = Convert.ToDateTime(grListDKLNG.GetRow().Cells["NgayDangKy"].Value.ToString());
                    dtpGioLamTT.Value = Convert.ToDateTime(grListDKLNG.GetRow().Cells["GioLamThuTuc"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    feedbackContent = SingleMessage.LicenseSendHandler(license, ref msgInfor, e);
                    break;
                case "tpHopDong":
                    feedbackContent = SingleMessage.ContractDocumentSendHandler(contractDocument, ref msgInfor, e);
                    break;
                case "tpHoaDon":
                    feedbackContent = SingleMessage.CommercialInvoiceSendHandler(commercialInvoice, ref msgInfor, e);
                    break;
                case "tpCO":
                    feedbackContent = SingleMessage.CertificateOfOriginSendHandler(certificateOfOrigin, ref msgInfor, e);
                    break;
                case "tpVanDon":
                    feedbackContent = SingleMessage.BillOfLadingSendHandler(billOfLading, ref msgInfor, e);
                    break;
                case "tpContainer":
                    feedbackContent = SingleMessage.ContainersSendHandler(container, ref msgInfor, e);
                    break;
                case "tpCTKhac":
                    feedbackContent = SingleMessage.AdditionalDocumentSendHandler(additionalDocument, ref msgInfor, e);
                    break;
                case "tpLamNgoaiGio":
                    feedbackContent = SingleMessage.OverTimeSendHandler(overTime, ref msgInfor, e);
                    break;
                default:
                    break;
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void grListCO_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }

            }
        }

        private void grListVanDon_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }

            }
        }

        private void grListHopDong_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                }

            }
        }

        private void grListHoaDon_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }

            }
        }

        private void grListGiayPhep_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }

            }
        }

        private void grListContainer_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }

            }
        }

        private void grListCTKhac_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }

            }
        }

        private void grListDKLNG_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                    case "-4":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Hủy khai báo";
                        break;

                }

            }
        }
    }
}
