﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
namespace SignRemote
{
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Services.WebServiceBinding(ConformsTo = (System.Web.Services.WsiProfiles)1)]
    [System.Web.Services.WebService(Namespace = "http://softech.vn/", Description = "Ký dữ liệu từ xa V4 và VNACCS")]
    public class SignMessage : System.Web.Services.WebService
    {

        private object GetItem(string sqlQuery)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sqlQuery);
            return db.ExecuteScalar(dbCommand);
        }
        private object CheckItem(string sqlQuery)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sqlQuery);
            return db.ExecuteNonQuery(dbCommand);
        }
        private DataTable GetData(string sqlQuery)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sqlQuery);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        /// <summary>
        /// Kiểm tra tài khoản
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [WebMethod]
        public bool IsExist(string username, string password)
        {
            try
            {
                string sqlCmd = string.Format("Select count(id) from t_User_DaiLy where USER_NAME = '{0}' AND PASSWORD = '{1}'", username, password);
                object obj = GetItem(sqlCmd);
                if (obj != null)
                    return Convert.ToInt32(obj) > 0;
                else return false;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Lấy thông tin các đại lý khai báo cho doanh nghiệp
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [WebMethod]
        public DataTable GetDaiLy(string username, string password)
        {
            try
            {
                if (IsExist(username, password))
                {

                    string sqlCmd = string.Format(@"Select * from t_User_DaiLy where isAdmin = 0 AND
                                    MaDoanhNghiep in (SELECT MaDoanhNghiep FROM t_User_DaiLy WHERE [USER_NAME] = '{0}' and isAdmin = 1)", username);
                    DataTable data = GetData(sqlCmd);
                    if (data != null)
                        return data;
                    else return null;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Lấy thông tin tất cả các doanh nghiệp khai báo chữ ký số
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [WebMethod]
        public DataTable GetAllCustommer(string username, string password)
        {
            try
            {
                if (IsExist(username, password))
                {

                    string sqlCmd = "Select * from t_User_DaiLy";
                    DataTable data = GetData(sqlCmd);
                    if (data != null)
                        return data;
                    else return null;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Tìm kiếm thông tin doanh nghiệp
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [WebMethod]
        public DataTable SearchCustommer(string where,string username, string password)
        {
            try
            {
                if (IsExist(username, password))
                {

                    string sqlCmd = "Select * from t_User_DaiLy WHERE " + where;
                    DataTable data = GetData(sqlCmd);
                    if (data != null)
                        return data;
                    else return null;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }
        [WebMethod]
        public string ChangePass(string username, string password, string accountDL, string NewPass, bool ChangePassAdmin)
        {
            try
            {
                if (IsExist(username, password))
                {
                    string sqlCmd = string.Empty;
                    if (ChangePassAdmin)
                    {
                        sqlCmd = string.Format(@"update [t_User_DaiLy]
                                                set [PASSWORD] = '{0}'
                                                WHERE  [PASSWORD] = '{1}' AND USER_NAME = '{2}' AND isAdmin = 1 ", NewPass, password, username);
                    }
                    else
                        sqlCmd = string.Format(@" update [t_User_DaiLy]
                                                     set [PASSWORD] = '{2}'
                                                        where [USER_NAME] = (select [USER_NAME] FROM [t_User_DaiLy] 
                                                                               WHERE  [USER_NAME] = '{0}' AND
                                                                          [MaDoanhNghiep] = (Select [MaDoanhNghiep] FROM [t_User_DaiLy] WHERE [USER_NAME] = '{1}' AND isAdmin = 1 ))", accountDL, username,NewPass);
                    object obj = CheckItem(sqlCmd);
                    if (obj != null && Convert.ToInt32(obj) > 0)
                        return string.Empty;
                    else return "Account đại lý không tồn tại";
                }
                return "Sai user hoặc mật khẩu doanh nghiệp";
            }
            catch (System.Exception ex)
            {
                return "Lỗi khi thực hiện: " + ex.Message;
            }
        }

        [WebMethod]
        /// <summary>
        /// Nội dung dành cho phía client
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string ClientSend(string message)
        {
            MessageSend msg;
            ServiceAPI api = new ServiceAPI();

            try
            {
                msg = api.Deserialize<MessageSend>(message);
                bool isValid = IsExist(msg.From, msg.Password);
                if (!isValid)
                {
                    throw new Exception(string.Format("Doanh nghiệp {0} chưa được sử dụng hệ thống ký từ xa", msg.From));
                }
                string sql = "Select user_name from t_User_DaiLy where isAdmin=1 and MaDoanhNghiep='" + msg.To + "'";
                object obj = GetItem(sql);
                if (obj == null)
                {
                    throw new Exception("Doanh nghiệp '" + msg.To + "' chưa được đăng ký khai từ xa");
                }
                msg.To = obj.ToString();
                List<ServerLogin> serverLogins = ServerLogin.SelectCollectionDynamic("MaDoanhNghiep='" + obj.ToString() + "'", "");
                if (serverLogins.Count == 0)
                {
                    try
                    {
                        ServerLogin serverLogin = new ServerLogin()
                        {
                            MaDoanhNghiep = msg.From,
                            Warrning = msg.Warrning
                        };
                        serverLogin.Insert();
                    }
                    catch (Exception)
                    {
                        throw new Exception("Máy chủ chưa khởi động hệ thống ký từ xa");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageSend msgError = new MessageSend()
                {
                    SendStatus = SendStatusInfo.Error,
                    Warrning = ex.Message
                };
                return api.Serializer(msgError, Encoding.UTF8, true, true);
            }

            return api.Client(msg);
        }
        [WebMethod]
        /// <summary>
        /// Nội dung dành cho phía máy server
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string ServerSend(string message)
        {
            MessageSend msg;
            ServiceAPI api = new ServiceAPI();
            try
            {
                msg = api.Deserialize<MessageSend>(message);
                bool isValid = IsExist(msg.From, msg.Password);
                if (!isValid)
                {
                    throw new Exception(string.Format("Doanh nghiệp '{0}' chưa được phép sử dụng hệ thống ký từ xa", msg.From));
                }
                if (msg.SendStatus == SendStatusInfo.ServerLogin)
                {
                    List<ServerLogin> serverLogins = ServerLogin.SelectCollectionDynamic("MaDoanhNghiep='" + msg.From + "'", "");
                    if (serverLogins.Count == 0)
                    {
                        ServerLogin serverLogin = new ServerLogin()
                        {
                            MaDoanhNghiep = msg.From,
                            Warrning = msg.Warrning
                        };
                        serverLogin.Insert();
                    }
                }
                else if (msg.SendStatus == SendStatusInfo.ServerLogout)
                {
                    List<ServerLogin> serverLogins = ServerLogin.SelectCollectionDynamic("MaDoanhNghiep='" + msg.From + "'", "");
                    ServerLogin.DeleteCollection(serverLogins);
                }


            }
            catch (Exception ex)
            {
                MessageSend msgError = new MessageSend()
                {
                    SendStatus = SendStatusInfo.Error,
                    Warrning = ex.Message
                };
                return api.Serializer(msgError, Encoding.UTF8, true, true);
            }

            return api.Server(msg);
        }


        [WebMethod]
        /// <summary>
        /// Nội dung dành cho phía client
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string ClientSendFile(string message)
        {
            MessageSendFile msg;
            ServiceAPI api = new ServiceAPI();

            try
            {
                msg = api.Deserialize<MessageSendFile>(message);
                bool isValid = IsExist(msg.From, msg.Password);
                if (!isValid)
                {
                    throw new Exception(string.Format("Doanh nghiệp {0} chưa được sử dụng hệ thống ký từ xa", msg.From));
                }
                string sql = "Select user_name from t_User_DaiLy where isAdmin=1 and MaDoanhNghiep='" + msg.To + "'";
                object obj = GetItem(sql);
                if (obj == null)
                {
                    throw new Exception("Doanh nghiệp '" + msg.To + "' chưa được đăng ký khai từ xa");
                }
                msg.To = obj.ToString();
                List<ServerLogin> serverLogins = ServerLogin.SelectCollectionDynamic("MaDoanhNghiep='" + obj.ToString() + "'", "");
                if (serverLogins.Count == 0)
                {
                    try
                    {
                        ServerLogin serverLogin = new ServerLogin()
                        {
                            MaDoanhNghiep = msg.From,
                            Warrning = msg.Warrning
                        };
                        serverLogin.Insert();
                    }
                    catch (Exception)
                    {
                        throw new Exception("Máy chủ chưa khởi động hệ thống ký từ xa");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageSend msgError = new MessageSend()
                {
                    SendStatus = SendStatusInfo.Error,
                    Warrning = ex.Message
                };
                return api.Serializer(msgError, Encoding.UTF8, true, true);
            }

            return api.FileClient(msg);
        }
        [WebMethod]
        /// <summary>
        /// Nội dung dành cho phía máy server
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string ServerSendFile(string message)
        {
            MessageSendFile msg;
            ServiceAPI api = new ServiceAPI();
            try
            {
                msg = api.Deserialize<MessageSendFile>(message);
                bool isValid = IsExist(msg.From, msg.Password);
                if (!isValid)
                {
                    throw new Exception(string.Format("Doanh nghiệp '{0}' chưa được phép sử dụng hệ thống ký từ xa", msg.From));
                }
                if (msg.SendStatus == SendStatusInfo.ServerLogin)
                {
                    List<ServerLogin> serverLogins = ServerLogin.SelectCollectionDynamic("MaDoanhNghiep='" + msg.From + "'", "");
                    if (serverLogins.Count == 0)
                    {
                        ServerLogin serverLogin = new ServerLogin()
                        {
                            MaDoanhNghiep = msg.From,
                            Warrning = msg.Warrning
                        };
                        serverLogin.Insert();
                    }
                }
                else if (msg.SendStatus == SendStatusInfo.ServerLogout)
                {
                    List<ServerLogin> serverLogins = ServerLogin.SelectCollectionDynamic("MaDoanhNghiep='" + msg.From + "'", "");
                    ServerLogin.DeleteCollection(serverLogins);
                }


            }
            catch (Exception ex)
            {
                MessageSendFile msgError = new MessageSendFile()
                {
                    SendStatus = SendStatusInfo.Error,
                    Warrning = ex.Message
                };
                return api.Serializer(msgError, Encoding.UTF8, true, true);
            }

            return api.FileServer(msg);
        }

        [WebMethod]
        /// <summary>
        /// Nội dung dành cho phía máy server
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public DataTable GetNewFileClientSend(string From)
        {
            try
            {
                string sfmtWhere = string.Format("MSG_FROM='{0}' and ([STATUS]={1} OR [STATUS] = {2}) ", From, SendStatusInfo.NoSign, SendStatusInfo.NoSignVNACCS);
                DataTable items = MSG_FILE_INBOX.SelectDynamic(sfmtWhere, "").Tables[0];
                return items;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        [WebMethod]
        /// <summary>
        /// Nội dung dành cho phía máy server
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public DataTable GetNewMessageClientSend(string From)
        {
            try
            {
                string sfmtWhere = string.Format("MSG_FROM='{0}' and ([STATUS]={1} OR [STATUS] = {2}) ", From, SendStatusInfo.NoSign, SendStatusInfo.NoSignVNACCS);
                DataTable items = INBOX.SelectDynamic(sfmtWhere, "").Tables[0];
                return items;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
