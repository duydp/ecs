﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.GC;
using Company.GC.BLL.KDT.SXXK;
using DevExpress.XtraPrinting;
using Company.GC.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using Infragistics.Excel;
using System.Threading;
using System.Diagnostics;

namespace Company.Interface.Report
{
    public partial class ViewNPLCungUngTKCTForm : BaseForm
    {
        BangNPLCungUngTKCTReport Report = new BangNPLCungUngTKCTReport();
        public ToKhaiChuyenTiep TKCT;
        public HopDong HD;
        public ViewNPLCungUngTKCTForm()
        {
            InitializeComponent();
        }

        private void explorerBar1_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            this.Text += " của tờ khai xuất số " + this.TKCT.SoToKhai + "-" + this.TKCT.NgayDangKy.ToShortDateString();
            this.TKCT.LoadHCTCollection();
            for (int i = 0; i < this.TKCT.HCTCollection.Count; i++)
            {
                if (PhanBoToKhaiNhap.CheckSanPhamCungUngTKCT(TKCT.ID, TKCT.IDHopDong, this.TKCT.HCTCollection[i].MaHang))
                    cbPage.Items.Add(this.TKCT.HCTCollection[i].TenHang + "/" + this.TKCT.HCTCollection[i].MaHang, this.TKCT.HCTCollection[i].MaHang);
            }
            if (cbPage.Items.Count > 0)
            {
                cbPage.SelectedIndex = 0;
                this.ViewPhanBo();
            }
            else
            {
                ShowMessage("Tờ khai này không có nguyên phụ liệu cung ứng", false);
                this.Close();
            }
        }
        private void ViewPhanBo()
        {
            this.Report.HD = this.HD;
            this.Report.TKCT = this.TKCT;
            this.Report.BindReport(cbPage.SelectedValue.ToString());
            printControl1.PrintingSystem = this.Report.PrintingSystem;
            this.Report.CreateDocument();
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            } 
        }

       
      

        

        private void cbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewPhanBo();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            } 

        }

        

    }
}