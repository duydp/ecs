﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.Feedback
{
    public partial class PhanLoaiUserControl : UserControl
    {
        public PhanLoaiUserControl()
        {
            InitializeComponent();
        }

        //public PhanLoaiUserControl(string ma)
        //{
        //    InitializeComponent();

        //    if (ma != "")
        //    {
        //        switch (ma)
        //        {
        //            case "ThacMac":
        //                radThacMac.Checked = true;
        //                break;
        //            case "GopY":
        //                radGopY.Checked = true;
        //                break;
        //            case "Loi":
        //                radLoi.Checked = true;
        //                break;
        //            default:
        //                radThacMac.Checked = true;
        //                break;
        //        }
        //    }
        //    else
        //        radThacMac.Checked = true;
        //}

        public bool ThacMacChecked
        {
            get { return radThacMac.Checked; }
            set { radThacMac.Checked = value; }
        }

        public bool GopYChecked
        {
            get { return radGopY.Checked; }
            set { radGopY.Checked = value; }
        }

        public bool LoiChecked
        {
            get { return radLoi.Checked; }
            set { radLoi.Checked = value; }
        }

        public String GetItemChecked()
        {
            if (radLoi.Checked)
            {
                return radLoi.CheckedValue.ToString();
            }
            else if (radThacMac.Checked)
            {
                return radThacMac.CheckedValue.ToString();
            }
            else if (radGopY.Checked)
            {
                return radGopY.CheckedValue.ToString();
            }
            else
                return radThacMac.CheckedValue.ToString();
        }
    }
}
