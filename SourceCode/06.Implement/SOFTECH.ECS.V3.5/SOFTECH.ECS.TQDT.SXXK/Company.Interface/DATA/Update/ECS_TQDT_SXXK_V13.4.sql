

-- Cập nhật danh mục Miễn/Giảm/Không chịu thuế------
Delete t_VNACC_Category_Common where ReferenceDB='A521'

INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNK10',N'Hàng vận chuyển quá cảnh, chuyển khẩu','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNK20',N'Hàng viện trợ nhân đạo, viện trợ không hoàn lại','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNK31',N'Hàng từ khu PTQ XK ra nước ngoài','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNK32',N'Hàng NK từ nước ngoài vào khu PTQ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNK33',N'Hàng từ khu PTQ này sang khu PTQ khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNK40',N'Hàng XK là phần dầu khí thuộc thuế tài nguyên của NN','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNG81',N'Hàng NK để gia công cho nước ngoài (đối tượng miễn thuế NK)','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNG82',N'Sản phẩm gia công xuất trả cho phía nước ngoài (đối tượng miễn thuế XK)','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNG83',N'Hàng XK để gia công cho Việt Nam (đối tượng miễn thuế XK)','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNG84',N'Sản phẩm gia công nhập trả Việt Nam (đối tượng miễn thuế NK)','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XNK90',N'Hàng hóa khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK010',N'SP trồng trọt, chăn nuôi, thủy sản nuôi trồng, đánh bắt','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK020',N'Giống vật nuôi, giống cây trồng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK030',N'Muối ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK040',N'Báo, tạp chí, bản tin, sách, tranh, ảnh, áp phích','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK050',N'Hàng NK phục vụ nghiên cứu khoa học, phát triển công nghệ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK060',N'Hàng NK phục vụ tìm kiếm, thăm dò, phát triển mỏ dầu, khí đốt','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK070',N'Tàu bay, dàn khoan, tàu thủy','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK080',N'Vũ khí, khí tài chuyên dùng phục vụ quốc phòng an ninh','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK091',N'Hàng viện trợ nhân đạo, viện trợ không hoàn lại','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK092',N'Quà biếu, quà tặng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK093',N'Đồ dùng theo tiêu chuẩn miễn trừ ngoại giao','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK100',N'Hàng chuyển khẩu, quá cảnh','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK110',N'Hàng tạm nhập tái xuất, tạm xuất tái nhập','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK120',N'Nguyên liệu NK để SX, gia công hàng XK','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK130',N'Hàng mua bán giữa nước ngoài với khu PTQ, giữa các khu PTQ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK140',N'Vàng ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK150',N'SP nhân tạo thay thế bộ phận cơ thể người bệnh','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK160',N'Dụng cụ chuyên dùng cho người tàn tật','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK170',N'Hàng gửi qua dịch vụ chuyển phát nhanh','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK180',N'Hàng hóa bán tại cửa hàng miễn thuế','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK190',N'Hàng nông sản do VN hỗ trợ đầu tư, trồng tại Campuchia','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','VK900',N'Hàng hóa khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','MK010',N'Hàng vận chuyển quá cảnh, chuyển khẩu','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','MK020',N'Hàng tạm nhập tái xuất','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','MK030',N'Bao bì đóng gói sẵn hàng hóa','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','MK040',N'Túi ni lông thân thiện với môi trường','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','MK100',N'Hàng hóa khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK010',N'Hàng viện trợ nhân đạo, viện trợ không hoàn lại','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK020',N'Hàng quà biếu, tặng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK030',N'Hàng quá cảnh, mượn đường, chuyển khẩu','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK040',N'Hàng tạm nhập tái xuất, tạm xuất tái nhập','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK050',N'Hàng theo tiêu chuẩn miễn trừ ngoại giao','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK060',N'Hàng NK để bán tại cửa hàng miễn thuế','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK070',N'Hàng đưa vào khu phi thuế quan','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK080',N'Tàu bay, du thuyền ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK090',N'Xe ô tô ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK100',N'Nap-ta, condensate, chế phẩm tái hợp... ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK110',N'Điều hòa nhiệt độ không chịu thuế TTĐB','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','TK300',N'Hàng hóa khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN011',N'Hàng TNTX, TXTN tham dự hội chợ triển lãm','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN012',N'Hàng TNTX, TXTN phục vụ công việc trong thời hạn nhất định','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN020',N'Tài sản di chuyển','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN030',N'Hàng của đối tượng được ưu đãi miễn trừ ngoại giao','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN050',N'Hàng gửi qua dịch vụ chuyển phát nhanh','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN061',N'Hàng NK tạo TSCĐ của dự án đầu tư vào lĩnh vực được ưu đãi','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN062',N'Hàng NK tạo TSCĐ của dự án đầu tư vào địa bàn được ưu đãi','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN063',N'Hàng NK tạo TSCĐ của dự án đầu tư bằng nguồn vốn ODA','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN070',N'Giống cây trồng, vật nuôi cho dự án nông, lâm, ngư nghiệp.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN081',N'Hàng NK cho dự án đầu tư mở rộng thuộc lĩnh vực ưu đãi','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN082',N'Hàng NK cho dự án đầu tư mở rộng thuộc địa bàn ưu đãi','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN083',N'Hàng NK tạo TSCĐ của dự án ODA mở rộng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN084',N'Hàng NK cho dự án nông, lâm, ngư nghiệp mở rộng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN090',N'Hàng NK được miễn thuế lần đầu','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN100',N'Hàng NK phục vụ hoạt động dầu khí','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN111',N'Hàng NK của cơ sở đóng tàu','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN112',N'Sản phẩm tàu biển XK của cơ sở đóng tàu','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN120',N'Hàng NK cho hoạt động sản xuất  phần mềm','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN130',N'Hàng NK cho hoạt động NCKH và phát triển công nghệ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN140',N'Hàng NK để sản xuất của dự án đầu tư được miễn thuế 05 năm','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN150',N'Hàng NK được sản xuất, gia công, tái chế, lắp ráp tại khu PTQ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN160',N'Hàng TNTX để thực hiện dự án ODA','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN170',N'Hàng NK để sản xuất của dự án đầu tư theo QĐ 33/2009/QĐ-TTg','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN180',N'Hàng NK để bán tại cửa hàng miễn thuế','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN500',N'Hàng miễn thuế theo K20, Đ12, NĐ 87/2010/NĐ-CP','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN600',N'Vật liệu xây dựng đưa vào khu PTQ theo TT 11/2012/TT-BTC ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN710',N'Hàng nông sản NK do VN hỗ trợ đầu tư, trồng tại Campuchia','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN720',N'Hàng NK để SX, lắp ráp xe buýt theo TT 85/2012/TT-BTC','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN900',N'Hàng NK, XK khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A521','XN910',N'Hàng NK, XK khác','','','','','')




-- Cập nhật Mã biểu thuế áp dụng mức thuế
Delete t_VNACC_Category_Common where ReferenceDB='A522'

INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB015',N'Nước sạch phục vụ sản xuất và sinh hoạt','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB025',N'Phân bón; quặng để sản xuất phân bón','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB035',N'Thuốc phòng trừ sâu bệnh  và chất kích thích tăng trưởng vật nuôi, cây trồng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB045',N'Thức ăn gia súc, gia cầm và thức ăn cho vật nuôi khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB055',N'Mủ cao su sơ chế','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB065',N'Nhựa thông sơ chế','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB075',N'Lưới, dây giềng và sợi để đan lưới đánh cá','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB085',N'Thực phẩm tươi sống; lâm sản chưa qua chế biến, trừ gỗ, măng và sản phẩm quy định tại khoản 1 Điều 5 Luật thuế GTGT số 13/2008/QH12','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB095',N'Đường; phụ phẩm trong sản xuất đường, bao gồm gỉ đường, bã mía, bã bùn','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB105',N'Sản phẩm bằng đay, cói, tre, nứa, lá, rơm, vỏ dừa, sọ dừa, bèo tây và các sản phẩm thủ công khác sản xuất bằng nguyên liệu tận dụng từ nông nghiệp ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB115',N'Bông sơ chế','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB125',N'Giấy in báo','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB135',N'Máy móc, thiết bị chuyên dùng phục vụ cho sản xuất nông nghiệp, bao gồm máy cày, máy bừa, máy cấy, máy gieo hạt, máy tuốt lúa, máy gặt, máy gặt đập liên hợp, máy thu hoạch sản phẩm nông nghiệp, máy hoặc bình bơm thuốc trừ sâu','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB145',N'Thiết bị, dụng cụ y tế; bông, băng vệ sinh y tế','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB155',N'Thuốc phòng bệnh, chữa bệnh; sản phẩm hóa dược, dược liệu là nguyên liệu sản xuất thuốc chữa bệnh, thuốc phòng bệnh','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB165',N'Giáo cụ dùng để giảng dạy và học tập, bao gồm các loại mô hình, hình vẽ, bảng, phấn, thước kẻ, com-pa và các loại thiết bị, dụng cụ chuyên dùng cho giảng dạy, nghiên cứu, thí nghiệm khoa học','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB175',N'Phim nhập khẩu','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB185',N'Đồ chơi cho trẻ em; sách các loại, trừ sách quy định tại khoản 15 Điều 5 Luật thuế GTGT số 13/2008/QH12','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','VB901',N'Hàng hóa thuộc đối tượng chịu thuế GTGT với mức thuế suất 10%','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB010',N'Xăng, trừ etanol','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB020',N'Nhiên liệu bay','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB030',N'Dầu diezel','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB040',N'Dầu hỏa','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB050',N'Dầu mazut','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB060',N'Dầu nhờn','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB070',N'Mỡ nhờn','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB110',N'Than nâu','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB120',N'Than an - tra - xít (antraxit)','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB130',N'Than mỡ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB140',N'Than đá khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB200',N'III. Dung dịch Hydro-chloro-fluoro-carbon (HCFC)','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB300',N'IV. Túi ni lông thuộc diện chịu thuế BVMT','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB400',N'V. Thuốc diệt cỏ thuộc loại hạn chế sử dụng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB500',N'VI. Thuốc trừ mối thuộc loại hạn chế sử dụng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB600',N'VII. Thuốc bảo quản lâm sản thuộc loại hạn chế sử dụng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','MB700',N'VIII. Thuốc khử trùng kho thuộc loại hạn chế sử dụng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB010',N'Thuốc lá điếu, xì gà và các chế phẩm khác từ cây thuốc lá','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB020',N'Rượu từ 20 độ trở lên, từ ngày 01 tháng 01 năm 2013','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB030',N'Rượu dưới 20 độ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB040',N'Bia, từ ngày 01 tháng 01 năm 2013','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB050',N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 2.000 cm3 trở xuống ; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB060',N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 3.000 cm3 ; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB070',N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3 ; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB080',N'Xe ô tô chở người từ 10 đến dưới 16 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB090',N'Xe ô tô chở người từ 16 đến dưới 24 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB100',N'Xe ô tô vừa chở người, vừa chở hàng; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 7 Luật thuế tiêu thụ đặc biệt số 27/2008/QH12','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB110',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 2.000 cm3 trở xuống.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB120',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 3.000 cm3 .','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB130',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3 .','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB140',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB150',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB160',N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô vừa chở người, vừa chở hàng.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB170',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 2.000 cm3 trở xuống.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB180',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 3.000 cm3 .','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB190',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3 .','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB200',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB210',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB220',N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô vừa chở người, vừa chở hàng.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB230',N'Xe ô tô chạy bằng điện, loại chở người từ 9 chỗ trở xuống','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB240',N'Xe ô tô chạy bằng điện, loại chở người từ 10 đến dưới 16 chỗ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB250',N'Xe ô tô chạy bằng điện, loại chở người từ 16 đến dưới 24 chỗ ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB260',N'Xe ô tô chạy bằng điện, loại thiết kế vừa chở người, vừa chở hàng ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB270',N'Xe mô tô hai bánh, xe mô tô ba bánh có dung tích xi lanh trên 125cm3','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB280',N'Tàu bay','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB290',N'Du thuyền','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB300',N'Xăng các loại, nap-ta, chế phẩm tái hợp và các chế phẩm khác để pha chế xăng','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB310',N'Điều hoà nhiệt độ công suất từ 90.000 BTU trở xuống','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB320',N'Bài lá','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','TB330',N'Vàng mã, hàng mã.','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','BB001',N'Hàng hóa bị áp dụng thuế tự vệ ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','BB002',N'Hàng hóa bị áp dụng thuế tự vệ ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','BB003',N'Hàng hóa bị áp dụng thuế tự vệ ','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','GB100',N'Hàng hóa bị áp dụng thuế chống bán phá giá','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','GB101',N'Hàng hóa bị áp dụng thuế chống bán phá giá','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','GB102',N'Hàng hóa bị áp dụng thuế chống bán phá giá','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','CB200',N'Hàng hóa bị áp dụng thuế chống trợ cấp','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','CB201',N'Hàng hóa bị áp dụng thuế chống trợ cấp','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','CB202',N'Hàng hóa bị áp dụng thuế chống trợ cấp','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','PB300',N'Hàng hóa bị áp dụng thuế chống phân biệt đối xử','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','PB301',N'Hàng hóa bị áp dụng thuế chống phân biệt đối xử','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','PB302',N'Hàng hóa bị áp dụng thuế chống phân biệt đối xử','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','DB400',N'Hàng hóa bị áp dụng biện pháp về thuế nhập khẩu khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','DB401',N'Hàng hóa bị áp dụng biện pháp về thuế nhập khẩu khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','DB402',N'Hàng hóa bị áp dụng biện pháp về thuế nhập khẩu khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','EB500',N'Hàng hóa bị áp dụng biện pháp về thuế nhập khẩu khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','EB501',N'Hàng hóa bị áp dụng biện pháp về thuế nhập khẩu khác','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A522','EB502',N'Hàng hóa bị áp dụng biện pháp về thuế nhập khẩu khác','','','','','')

--- Cập nhật mã danh mục A202----
delete t_VNACC_Category_Cargo where BondedAreaCode in('47CIC03','10BBD01')
INSERT INTO t_VNACC_Category_Cargo ([ResultCode] ,[PGNumber],[TableID],[ProcessClassification],[CreatorClassification],[NumberOfKeyItems],[BondedAreaCode],[UserCode],[BondedAreaName],[BondedAreaDemarcation],[NecessityIndicationOfBondedAreaName],[CustomsOfficeCodeForImportDeclaration],[CustomsOfficeCodeForExportDeclaration],[CustomsOfficeCodeForDeclarationOnTransportation],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES('','','A202A','A','1','01','47CIC03',' ','THUOC LA BAT VINA','C','0','47CI','47CI','47CI','','','','')
INSERT INTO t_VNACC_Category_Cargo ([ResultCode] ,[PGNumber],[TableID],[ProcessClassification],[CreatorClassification],[NumberOfKeyItems],[BondedAreaCode],[UserCode],[BondedAreaName],[BondedAreaDemarcation],[NecessityIndicationOfBondedAreaName],[CustomsOfficeCodeForImportDeclaration],[CustomsOfficeCodeForExportDeclaration],[CustomsOfficeCodeForDeclarationOnTransportation],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES('','','A202A','A','1','01','10BBD01',' ','CTY LD MINH PHONG','D','0','10BB','10BB','10BB','','','','')

-- Cập nhật Danh mục A528---
Delete t_VNACC_Category_Common where Code in('HL02','BF02','XE02')

INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A528','HL02',N'HL02','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A528','BF02',N'BF02','','','','','')
INSERT INTO t_VNACC_Category_Common ([ReferenceDB],[Code],[Name_VN],[Name_EN],[Notes],[InputMessageID],[MessageTag],[IndexTag]) VALUES ('A528','XE02',N'XE02','','','','','')

-- Cập nhật Danh muc Tiền tệ ---

update t_VNACC_Category_CurrencyExchange set CurrencyName='DOLA MY' where CurrencyCode='USD'
update t_VNACC_Category_CurrencyExchange set CurrencyName='EURO' where CurrencyCode='EUR'
update t_VNACC_Category_CurrencyExchange set CurrencyName='YEN NHAT' where CurrencyCode='JPY'
update t_VNACC_Category_CurrencyExchange set CurrencyName='BANG ANH' where CurrencyCode='GBP'
update t_VNACC_Category_CurrencyExchange set CurrencyName='FRANC THUYSY' where CurrencyCode='CHF'
update t_VNACC_Category_CurrencyExchange set CurrencyName='DOLA UC' where CurrencyCode='AUD'
update t_VNACC_Category_CurrencyExchange set CurrencyName='DOLA CANADA' where CurrencyCode='CAD'
update t_VNACC_Category_CurrencyExchange set CurrencyName='CURON THUYDIEN' where CurrencyCode='SEK'
update t_VNACC_Category_CurrencyExchange set CurrencyName='CURON NAUY' where CurrencyCode='NOK'
update t_VNACC_Category_CurrencyExchange set CurrencyName='CURON DANMACH' where CurrencyCode='DKK'
update t_VNACC_Category_CurrencyExchange set CurrencyName='RUP NGA' where CurrencyCode='RUB'
update t_VNACC_Category_CurrencyExchange set CurrencyName='DOLA NEWZEALAND' where CurrencyCode='NZD'
update t_VNACC_Category_CurrencyExchange set CurrencyName='DOLA HONG KONG' where CurrencyCode='HKD'
update t_VNACC_Category_CurrencyExchange set CurrencyName='DOLA SINGAPORE' where CurrencyCode='SGD'
update t_VNACC_Category_CurrencyExchange set CurrencyName='RINGIT MALAYSIA' where CurrencyCode='MYR'
update t_VNACC_Category_CurrencyExchange set CurrencyName='BATH THAI LAN' where CurrencyCode='THB'
update t_VNACC_Category_CurrencyExchange set CurrencyName='RUP INDONESIA' where CurrencyCode='IDR'
update t_VNACC_Category_CurrencyExchange set CurrencyName='WON HANQUOC' where CurrencyCode='KRW'
update t_VNACC_Category_CurrencyExchange set CurrencyName='RUP ANDO' where CurrencyCode='INR'
update t_VNACC_Category_CurrencyExchange set CurrencyName='DOLA DAI LOAN' where CurrencyCode='TWD'
update t_VNACC_Category_CurrencyExchange set CurrencyName='NHAN DAN TE' where CurrencyCode='CNY'
update t_VNACC_Category_CurrencyExchange set CurrencyName='RIEL CAMPUCHIA' where CurrencyCode='KHR'
update t_VNACC_Category_CurrencyExchange set CurrencyName='KIP LAO' where CurrencyCode='LAK'
update t_VNACC_Category_CurrencyExchange set CurrencyName='PATACA MACAO' where CurrencyCode='MOP'

delete t_VNACC_Category_CurrencyExchange where CurrencyCode='VND'

INSERT INTO t_VNACC_Category_CurrencyExchange
           ([ErrorCode]
           ,[PGNumber]
           ,[TableID]
           ,[ProcessClassification]
           ,[MakerClassification]
           ,[NumberOfKeyItems]
           ,[CurrencyCode]
           ,[CurrencyName]
           ,[GenerationManagementIndication]
           ,[DateOfMaintenanceUpdated]
           ,[EndDate]
           ,[StartDate_1]
           ,[ExchangeRate_1]
           ,[StartDate_2]
           ,[ExchangeRate_2]
           ,[StartDate_3]
           ,[ExchangeRate_3]
           ,[StartDate_4]
           ,[ExchangeRate_4]
           ,[StartDate_5]
           ,[ExchangeRate_5]
           ,[StartDate_6]
           ,[ExchangeRate_6]
           ,[StartDate_7]
           ,[ExchangeRate_7]
           ,[StartDate_8]
           ,[ExchangeRate_8]
           ,[StartDate_9]
           ,[ExchangeRate_9]
           ,[StartDate_10]
           ,[ExchangeRate_10]
           ,[Notes]
           ,[InputMessageID]
           ,[MessageTag]
           ,[IndexTag])
     VALUES
           ('','','A527','','','','VND','VIET NAM DONG','','2013/12/16','','2013/12/16',1,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
           
           --Cập nhật table t_VNACC_Category_Common
			ALTER TABLE t_VNACC_Category_Common
			ALTER COLUMN Code varchar (10)
           -- Câp nhật địa chỉ khai báo chữ ký số
          Update t_Haiquan_cuc set IPServiceCKS='203.210.197.144:82', ServicePathCKS='WSchukyso/service.asmx' where id=29
		  Update t_Haiquan_cuc set IPServiceCKS='118.69.226.38' where id=02
           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '13.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('13.4', GETDATE(), N'Cap nhat Danh muc (A202,A527,A528,A522,A521).Cap nhat dia chi chu ky so')
END	
