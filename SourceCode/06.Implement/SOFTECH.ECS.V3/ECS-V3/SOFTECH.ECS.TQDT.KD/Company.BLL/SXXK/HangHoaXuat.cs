using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;

namespace Company.KD.BLL.SXXK
{
    public partial class HangHoaXuat
    {
        public DataSet SearchBy_MaHaiQuan(string maHaiQuan,string maDoanhNghiep)
        {
            string where = string.Format("MaHaiQuan = '{0}' and maDoanhNghiep={1}", maHaiQuan.PadRight(6),maDoanhNghiep);
            return this.SelectDynamic(where, "Ma");
        }
        public HangHoaXuatCollection getSanPhamCoDinhMuc(string maHaiQuan,string maDoanhNghiep)
        {
            string sql = "SELECT * from t_SXXK_SanPham " +
                        " where ma in (select masanpham from t_SXXK_ThongTinDinhMuc) and MaHaiQuan='" + maHaiQuan + "'";
            DbCommand dbc = db.GetSqlStringCommand(sql);
            HangHoaXuatCollection collection = new HangHoaXuatCollection();

            IDataReader reader = db.ExecuteReader(dbc);
            while (reader.Read())
            {
                HangHoaXuat entity = new HangHoaXuat();
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                collection.Add(entity);
            }
            return collection;
        }
        //public DataSet WS_GetDanhSachDaDangKy(string maHaiQuan, string maDoanhNghiep)
        //{
        //    SXXKService service = new SXXKService();
        //    return service.SanPham_GetDanhSach(maHaiQuan, maDoanhNghiep);
        //}

        public bool UpdateRegistedToDatabase(string maHaiQuan, string maDoanhNghiep, DataSet ds)
        {
            //HangHoaXuatCollection collection = new HangHoaXuatCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HangHoaXuat sp = new HangHoaXuat();
                sp.MaHaiQuan = maHaiQuan;
                sp.MaDoanhNghiep = maDoanhNghiep;
                sp.Ma = row["Ma"].ToString();
                sp.Ten = row["Ten"].ToString();
                sp.MaHS = row["MaHS"].ToString().Trim();
                int k=sp.MaHS.Length;
                if (sp.MaHS.Length < 10)
                {
                    for (int i = 1; i <= 10 -k ; ++i)
                        sp.MaHS += "0";
                }
                sp.DVT_ID = row["DVT_ID"].ToString();
                try
                {
                    sp.Insert();
                }
                catch { }
               
            }
            return true;
            //return this.InsertUpdate(collection);
        }
        public void InsertUpdateFull(string MaCu)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (MaCu != "")
                    {
                        HangHoaXuat hang = new HangHoaXuat();
                        hang.Ma = MaCu;
                        hang.MaDoanhNghiep = this.MaDoanhNghiep;
                        hang.MaHaiQuan = ""; 
                        hang.DeleteTransaction(transaction);
                    }
                    this.InsertTransaction(transaction);
                    string sql = "update t_KDT_HangMauDich set MaPhu=@MaPhu where MaPhu=@MaCu and MaPhu in (select MaPhu from t_KDT_HangMauDich hang inner join t_KDT_ToKhaiMauDich tk on tk.id=hang.TKMD_ID where MaDoanhNghiep=@MaDoanhNghiep)";
                    DbCommand command = db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@MaPhu", SqlDbType.VarChar, this.Ma);
                    db.AddInParameter(command, "@MaCu", SqlDbType.VarChar, MaCu);
                    db.AddInParameter(command, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "update t_SXXK_HangMauDich set MaPhu=@MaPhu where MaPhu=@MaCu and MaPhu in (select MaPhu from t_SXXK_HangMauDich hang inner join t_SXXK_ToKhaiMauDich tk on tk.MaHaiQuan=hang.MaHaiQuan and  tk.SoToKhai=hang.SoToKhai and  tk.MaLoaiHinh=hang.MaLoaiHinh and  tk.NamDangKy=hang.NamDangKy where MaDoanhNghiep=@MaDoanhNghiep)";
                    command = db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@MaPhu", SqlDbType.VarChar, this.Ma);
                    db.AddInParameter(command, "@MaCu", SqlDbType.VarChar, MaCu);
                    db.AddInParameter(command, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                    db.ExecuteNonQuery(command, transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }


        public bool CheckTKMDUserMa(string MaDoanhNghiep)
        {

            string sql = "select * from  t_KDT_HangMauDich where MaPhu=@MaPhu and MaPhu in (select MaPhu from t_KDT_HangMauDich hang inner join t_KDT_ToKhaiMauDich tk on tk.id=hang.TKMD_ID where MaDoanhNghiep=@MaDoanhNghiep)";
            DbCommand command = db.GetSqlStringCommand(sql);
            db.AddInParameter(command, "@MaPhu", SqlDbType.VarChar, this.Ma);
            db.AddInParameter(command, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            object o = db.ExecuteScalar(command);
            if (o != null)
                return true;

            sql = "select * from  t_SXXK_HangMauDich where MaPhu=@MaPhu and MaPhu in (select MaPhu from t_SXXK_HangMauDich hang inner join t_SXXK_ToKhaiMauDich tk on tk.MaHaiQuan=hang.MaHaiQuan and  tk.SoToKhai=hang.SoToKhai and  tk.MaLoaiHinh=hang.MaLoaiHinh and  tk.NamDangKy=hang.NamDangKy where MaDoanhNghiep=@MaDoanhNghiep)";
            command = db.GetSqlStringCommand(sql);
            db.AddInParameter(command, "@MaPhu", SqlDbType.VarChar, this.Ma);
            db.AddInParameter(command, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            o = db.ExecuteScalar(command);
            if (o != null)
                return true;
            return false;
        }
    }
}