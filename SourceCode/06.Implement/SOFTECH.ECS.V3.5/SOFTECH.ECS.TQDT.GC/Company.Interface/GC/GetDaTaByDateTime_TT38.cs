﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.Interface.SXXK;
using System.IO;
using System.Diagnostics;

using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.Interface.Report.GC;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.SXXK;
namespace Company.Interface
{
    public partial class GetDaTaByDateTime_TT38 : BaseForm
    {
        public HopDong HD = new HopDong();
        public string bc = "";
        public GetDaTaByDateTime_TT38()
        {
            InitializeComponent();

        }

        private void XuatNhapTonForm_Load(object sender, EventArgs e)
        {

            if (bc == "BC02")
                this.Text = "Xử lý báo cáo 02";
            dtpDenNgay.Value = DateTime.Now;
            dtpTuNgay.Value = HD.NgayKy;
            
        }

        

        private void btnInBaoCao_Click(object sender, EventArgs e)
        {
            
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    if (bc == "BC01") 
                    {
                        BangKe01_HSTK_TT38 xnt = new BangKe01_HSTK_TT38();
                        xnt.HD = this.HD;
                        xnt.BindReport(dtpTuNgay.Value, dtpDenNgay.Value);
                        xnt.ShowPreview();
                    }
                    else if (bc == "BC02") 
                    {
                        BangKe02_HSTK_TT38 xnt = new BangKe02_HSTK_TT38();
                        xnt.HD = this.HD;
                        xnt.BindReport(dtpTuNgay.Value, dtpDenNgay.Value);
                        xnt.ShowPreview();
                    }
                    
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

    }
}