﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
    public partial class DinhMuc
    {
        public bool CheckExitsDinhMucSanPham()
        {
            string spName = "select * from t_GC_DinhMuc where MaSanPham=@MaSanPham and HopDong_ID=@HopDong_ID AND MaSanPham NOT IN " +
                "(SELECT DISTINCT MaSanPham FROM t_KDT_GC_DinhMuc WHERE Master_ID IN (SELECT ID FROM t_KDT_GC_DinhMucDangKy WHERE TrangThaiXuLy <0 AND ID_HopDong = @HopDong_ID ))";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool CheckExitsDinhMucSanPhamDaDangKy()
        {
            string spName = "select * from t_GC_DinhMuc where MaSanPham=@MaSanPham and HopDong_ID=@HopDong_ID AND MaSanPham NOT IN " +
                "(SELECT DISTINCT MaSanPham FROM t_KDT_GC_DinhMuc WHERE Master_ID IN (SELECT ID FROM t_KDT_GC_DinhMucDangKy WHERE TrangThaiXuLy <1 AND ID_HopDong = @HopDong_ID ))";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public decimal GetTLHHOfHopDong(long idHopDong)
        {
            string spName = "select MAX(TyLeHaoHut) from t_GC_DinhMuc where HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, idHopDong);
            decimal TLHH = 0;
            try
            {
                TLHH = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            {
                TLHH = 0;
            }
            return TLHH;
        }
        public decimal GetTLHHOfHopDong(long idHopDong, string maNPL)
        {
            string spName = "select MAX(TyLeHaoHut) from t_GC_DinhMuc where HopDong_ID=@HopDong_ID AND MaNguyenPhuLieu = @MaNPL";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, idHopDong);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, maNPL);
            decimal TLHH = 0;
            try
            {
                TLHH = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            {
                TLHH = 0;
            }
            return TLHH;
        }
        public bool CheckExitsDinhMucSanPhamNguyenPhuLieu()
        {
            string spName = "select * from t_GC_DinhMuc where MaSanPham=@MaSanPham and HopDong_ID=@HopDong_ID and MaNguyenPhuLieu=@MaNguyenPhuLieu";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        //cap nhat lai ma san pham sau khi thay doi
        public int UpdateMaDinhMucTransaction(SqlTransaction transaction, string MaSanPhamCu)
        {
            string spName = "update t_GC_DinhMuc set MaSanPham=@MaSanPhamMoi where HopDong_ID=@HopDong_ID and MaSanPham=@MaSanPhamCu";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPhamMoi", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaSanPhamCu", SqlDbType.VarChar, MaSanPhamCu);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public DataSet getDinhMuc(long HopDong_ID)
        {
            string sql = " SELECT  "
                            + " * "
                            + " FROM t_View_DinhMuc where HopDong_ID= " + HopDong_ID;
            // + " And MaSanPham ='" + maSP + "'";  
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(cmd);
        }
        public decimal GetDinhMucChungOfMaSPAndMaNPL(string MaSP, string MaNPL, long ID_HD)
        {
            string spName = "select DinhMucChung from t_View_DinhMuc where MaSanPham=@MaSanPham and HopDong_ID=@HopDong_ID and MaNguyenPhuLieu=@MaNguyenPhuLieu";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, ID_HD);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSP);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNPL);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            decimal DinhMucChung = 0;
            if (reader.Read())
            {
                DinhMucChung = reader.GetDecimal(0);
            }
            reader.Close();
            return DinhMucChung;
        }
        public decimal GetDinhMucChungOfMaSPAndMaNPL(string MaSP, string MaNPL, long ID_HD, SqlTransaction transaction)
        {
            string spName = "select DinhMucChung from t_View_DinhMuc where MaSanPham=@MaSanPham and HopDong_ID=@HopDong_ID and MaNguyenPhuLieu=@MaNguyenPhuLieu";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, ID_HD);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSP);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNPL);

            IDataReader reader = this.db.ExecuteReader(dbCommand, transaction);
            decimal DinhMucChung = 0;
            if (reader.Read())
            {
                DinhMucChung = reader.GetDecimal(0);
            }
            reader.Close();
            return DinhMucChung;
        }

        public DataSet getDinhMucSanPham(long HopDong_ID, string MaSP)
        {
            string sql = " SELECT  "
                            + " * "
                            + " FROM t_GC_DinhMuc where HopDong_ID= " + HopDong_ID + " and MaSanPham=@MaSanPham"; ;
            DbCommand cmd = db.GetSqlStringCommand(sql);
            this.db.AddInParameter(cmd, "@MaSanPham", SqlDbType.VarChar, MaSP);
            return db.ExecuteDataSet(cmd);
        }

        public DataTable getDinhMuc(long HopDong_ID, string MaSP)
        {
            string sql = " SELECT  "
                            + " * "
                            + " FROM t_View_DinhMuc where HopDong_ID= " + HopDong_ID + " and MaSanPham=@MaSanPham"; ;
            DbCommand cmd = db.GetSqlStringCommand(sql);
            this.db.AddInParameter(cmd, "@MaSanPham", SqlDbType.VarChar, MaSP);
            return db.ExecuteDataSet(cmd).Tables[0];
        }
        //public static DataTable getDinhMuc(long HopDong_ID, string MaSP, decimal SoLuong, int SoThapPhanNPL, SqlTransaction transaction)
        //{
        //    string sql = " SELECT  "
        //                    + " *, Round((DinhMucChung * @SoLuong),@SoThapPhanNPL) as NhuCau "
        //                    + " FROM t_View_DinhMuc where HopDong_ID= " + HopDong_ID + " and MaSanPham=@MaSanPham";
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    DbCommand cmd = (DbCommand)db.GetSqlStringCommand(sql);
        //    db.AddInParameter(cmd, "@MaSanPham", SqlDbType.VarChar, MaSP);
        //    db.AddInParameter(cmd, "@SoLuong", SqlDbType.Decimal, SoLuong);
        //    db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
        //    return db.ExecuteDataSet(cmd,transaction).Tables[0];
        //}
        public static DataTable getDinhMuc(long HopDong_ID, string MaSP, decimal SoLuong, int SoThapPhanNPL, int SoThapPhanDM)
        {
            string sql = " SELECT HopDong_ID, MaSanPham, MaNguyenPhuLieu, STTHang, TenNPL, DVT_ID, TenSP, DinhMucSuDung, TyLeHaoHut, DinhMucChung "
                            + " , Round((Round(DinhMucSuDung,@SoThapPhanDM)* (1 + TyLeHaoHut/100) * @SoLuong),@SoThapPhanNPL) as NhuCau "
                            + " FROM t_View_DinhMuc where HopDong_ID= " + HopDong_ID + " and MaSanPham=@MaSanPham AND DinhMucChung > 0";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = (DbCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@MaSanPham", SqlDbType.VarChar, MaSP);
            db.AddInParameter(cmd, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(cmd, "@SoThapPhanDM", SqlDbType.Int, SoThapPhanDM);
            return db.ExecuteDataSet(cmd).Tables[0];
        }
        public DataSet GetThongTinDinhMuc(long IDHopDong, string maNPL, string maSP)
        {
            string sql = " SELECT * FROM t_GC_DinhMuc "
                       + " WHERE HopDong_ID = @HopDong_ID "
                       + " AND MaNguyenPhuLieu =@MaNPL "
                       + " AND MaSanPham =@MaSP ";
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbcommand = db1.GetSqlStringCommand(sql);
            db1.AddInParameter(dbcommand, "@HopDong_ID", DbType.Int64, IDHopDong);
            db1.AddInParameter(dbcommand, "@MaNPL", DbType.String, maNPL);
            db1.AddInParameter(dbcommand, "@MaSP", DbType.String, maSP);
            return db1.ExecuteDataSet(dbcommand);
        }

        //DATLMQ bổ sung lấy thông tin của Định mức của Người khai báo tạo ra 01/07/2011
        public DinhMucCollection GetDMFromUserName(string userName, string maSanPham, long idHD)
        {
            string query = "SELECT dm.HopDong_ID, dm.MaSanPham, dm.MaNguyenPhuLieu, dm.DinhMucSuDung, dm.TyLeHaoHut, " +
                           "dm.STTHang, dm.NPL_TuCungUng, dm.TenSanPham, dm.TenNPL FROM dbo.t_GC_DinhMuc dm LEFT JOIN dbo.t_GC_SanPham sp " +
                           "ON sp.Ma = dm.MaSanPham WHERE dm.MaSanPham IN (SELECT MaSanPham FROM dbo.t_KDT_GC_DinhMuc WHERE Master_ID IN " +
                           "(SELECT id_dk FROM dbo.t_KDT_SXXK_LogKhaiBao WHERE LoaiKhaiBao = 'DM' AND UserNameKhaiBao = '" + userName + "')) " +
                           "AND dm.MaSanPham LIKE '%" + maSanPham + "%' AND dm.HopDong_ID = " + idHD + "ORDER BY dm.MaSanPham";
            DbCommand dbc1 = db.GetSqlStringCommand(query);
            DinhMucCollection collection = new DinhMucCollection();

            IDataReader reader = db.ExecuteReader(dbc1);
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) entity.NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        
        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_DinhMuc_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
            this.db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
            this.db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, this._NPL_TuCungUng);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public bool InsertUpdate(DinhMucCollection collection, Company.GC.BLL.KDT.GC.HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    DinhMuc DMdelete = new DinhMuc();
                    DMdelete.HopDong_ID = HD.ID;
                    DMdelete.DeleteBy_HopDong_ID(transaction);
                    foreach (DinhMuc item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //Update by HUNGTQ, 06/05/2011.
        public bool InsertUpdate(DinhMucCollection collection, Company.GC.BLL.KDT.GC.HopDong HD, string databaseName)
        {
            SetDabaseMoi(databaseName);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //DinhMuc DMdelete = new DinhMuc();
                    //DMdelete.HopDong_ID = HD.ID;
                    //DMdelete.DeleteBy_HopDong_ID(transaction, databaseName);

                    foreach (DinhMuc item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction, databaseName) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdate(DinhMucCollection collection, string MaDoanhNghiep)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    DinhMuc DMdelete = new DinhMuc();
                    DMdelete.DeleteBy_MaDoanhNghiep(MaDoanhNghiep, transaction);
                    foreach (DinhMuc item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateMoi(DinhMucCollection collection)
        {
            bool ret;
            DinhMucCollection dmTMP = new DinhMucCollection();
            foreach (DinhMuc item in collection)
            {
                if (item.CheckExitsDinhMucSanPham())
                    continue;
                dmTMP.Add(item);
            }
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    string masp = "";
                    foreach (DinhMuc item in dmTMP)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateGrid(DinhMucCollection collection, long idHopDong)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int DeleletDM(long idHopDong, string maSP, string maNPL)
        {
            string SQL = "Delete from t_GC_DinhMuc where HopDong_ID =@HopDong_ID  "
                + " And  MaSanPham =@MaSanPham "
                + " AND MaNguyenPhuLieu = @MaNguyenPhuLieu ";
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbcommand = db1.GetSqlStringCommand(SQL);
            db1.AddInParameter(dbcommand, "@HopDong_ID", DbType.Int64, idHopDong);
            db1.AddInParameter(dbcommand, "@MaSanPham", DbType.String, maSP);
            db1.AddInParameter(dbcommand, "@MaNguyenPhuLieu", DbType.String, maNPL);
            return db1.ExecuteNonQuery(dbcommand);

        }
        public int DeleletDM(long idHopDong, string maSP, string maNPL, SqlTransaction transaction)
        {
            string SQL = "Delete from t_GC_DinhMuc where HopDong_ID =@HopDong_ID  "
                + " And  MaSanPham =@MaSanPham "
                + " AND MaNguyenPhuLieu = @MaNguyenPhuLieu ";
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbcommand = db1.GetSqlStringCommand(SQL);
            db1.AddInParameter(dbcommand, "@HopDong_ID", DbType.Int64, idHopDong);
            db1.AddInParameter(dbcommand, "@MaSanPham", DbType.String, maSP);
            db1.AddInParameter(dbcommand, "@MaNguyenPhuLieu", DbType.String, maNPL);
            return db1.ExecuteNonQuery(dbcommand, transaction);

        }
        public int DeleletDM(long idHopDong, string maSP, string maNPL, SqlTransaction transaction, string dbName)
        {
            string SQL = "Delete from t_GC_DinhMuc where HopDong_ID =@HopDong_ID  "
                + " And  MaSanPham =@MaSanPham "
                + " AND MaNguyenPhuLieu = @MaNguyenPhuLieu ";
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase(dbName);
            DbCommand dbcommand = db1.GetSqlStringCommand(SQL);
            db1.AddInParameter(dbcommand, "@HopDong_ID", DbType.Int64, idHopDong);
            db1.AddInParameter(dbcommand, "@MaSanPham", DbType.String, maSP);
            db1.AddInParameter(dbcommand, "@MaNguyenPhuLieu", DbType.String, maNPL);
            return db1.ExecuteNonQuery(dbcommand, transaction);

        }

        public static DataSet GetSanPhamCoDinhMuc(long id_HopDong)
        {
            string sql = "select * from t_gc_SanPham where HopDong_ID=@HopDong_ID and ma in " +
                        "(select distinct MaSanPham from t_GC_DinhMuc where HopDong_ID=@HopDong_ID)";
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbcommand = db1.GetSqlStringCommand(sql);
            db1.AddInParameter(dbcommand, "@HopDong_ID", DbType.Int64, id_HopDong);
            return db1.ExecuteDataSet(dbcommand);
        }
        public int DeleteBy_MaDoanhNghiep(string MaDoanhNghiep, SqlTransaction transaction)
        {
            string spName = "DELETE  FROM t_GC_DinhMuc " +
                            "WHERE HopDong_ID IN ( " +
                            "SELECT tkghd.ID FROM t_KDT_GC_HopDong tkghd WHERE tkghd.MaDoanhNghiep='" + MaDoanhNghiep + "')";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);


            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        //Update by HUNGTQ, 06/05/2011.
        public bool Load(string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_DinhMuc_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) this._MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) this._MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) this._DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) this._TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) this._NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) this._TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression, string databaseName)
        {
            string spName = "p_GC_DinhMuc_SelectDynamic";

            //Updated by HUNGTQ, 06/05/2011.
            SetDabaseMoi(databaseName);

            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        public int DeleteBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_DinhMuc_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_DinhMuc_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
            this.db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
            this.db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, this._NPL_TuCungUng);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public DinhMucCollection SelectCollectionBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 28/06/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_GC_DinhMuc_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            DinhMucCollection collection = new DinhMucCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) entity.NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }


    }
}