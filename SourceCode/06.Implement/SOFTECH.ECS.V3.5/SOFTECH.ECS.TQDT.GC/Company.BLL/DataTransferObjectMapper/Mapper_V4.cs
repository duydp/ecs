﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.Globalization;
using System.Threading;
using Company.KD.BLL.KDT;
using GlobalsShare = Company.KDT.SHARE.Components.Globals;
using Company.KDT.SHARE.QuanLyChungTu;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using DuLieuChuan = Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.Components.Messages.GoodsItem;
using Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho;
#if KD_V4
using Company.KDT.SHARE.Components.Messages.CO;
#endif
namespace Company.GC.BLL.DataTransferObjectMapper
{
    /// <summary>
    /// Pattern transfer 2 object 
    /// http://msdn.microsoft.com/en-us/library/ff649585.aspx
    /// </summary>
    public class Mapper_V4
    {
        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        private static string sfmtDateNoTime = "yyyy-MM-dd 00:00:00";
        /// <summary>
        /// HopDong to GC_HopDong
        /// </summary>
        /// <param name="hopdong"></param>
        /// <param name="diaChiDoanhNghiep"></param>
        /// <returns></returns>
        public static GC_HopDong ToDataTransferObject_GC_HopDong(KDT.GC.HopDong hopdong, string diaChiDoanhNghiep)
        {

            bool isSua = hopdong.ActionStatus == (short)ActionStatus.HopDongSua;
            if (isSua)
                hopdong.SoTiepNhan = KhaiBaoSua.SoTNSua(hopdong.SoTiepNhan, hopdong.NamTN, string.Empty,
                            hopdong.MaHaiQuan, hopdong.MaDoanhNghiep, LoaiKhaiBao.HopDong);
            GC_HopDong hd = new GC_HopDong()
            {
                Issuer = DeclarationIssuer.HOP_DONG_GIA_CONG,
                Function = isSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                Reference = hopdong.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                DeclarationOffice = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hopdong.MaHaiQuan) : hopdong.MaHaiQuan,
                //DeclarationOffice = hopdong.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isSua ? hopdong.SoTiepNhan.ToString() : string.Empty,
                Acceptance = isSua ? hopdong.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = hopdong.TenDoanhNghiep, Identity = hopdong.MaDoanhNghiep },
                ContractDocument = new Company.KDT.SHARE.Components.ContractDocument
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate),
                    //isInverseProcedure = "0",
                    Payment = new Payment { Method = hopdong.PTTT_ID },
                    CurrencyExchange = new CurrencyExchange { CurrencyType = hopdong.NguyenTe_ID },
                    Importer = new NameBase { Identity = hopdong.MaDoanhNghiep, Name = hopdong.TenDoanhNghiep, Address = diaChiDoanhNghiep },
                    Exporter = new NameBase { Identity = hopdong.DonViDoiTac, Name = hopdong.TenDonViDoiTac, Address = hopdong.DiaChiDoiTac },
                    CustomsValue = new CustomsValue { TotalPaymentValue = Helpers.FormatNumeric(hopdong.TongTriGiaTienCong, GlobalsShare.TriGiaNT), TotalProductValue = Helpers.FormatNumeric(hopdong.TongTriGiaSP, GlobalsShare.TriGiaNT) },
                    ImportationCountry = "VN",
                    ExportationCountry = hopdong.NuocThue_ID.Trim(),
                    AdditionalInformations = new List<AdditionalInformation>()
                },
            };
            hd.ContractDocument.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = hopdong.GhiChu } });
            #region add Agent
            hd.Agents.Add(new Agent
                         {
                             Identity = hopdong.MaDoanhNghiep,
                             Name = hopdong.TenDoanhNghiep,
                             Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                         });
            hd.Agents.Add(new Agent
                        {
                            Identity = hopdong.MaDoanhNghiep,
                            Name = hopdong.TenDoanhNghiep,
                            Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
                        });
            #endregion

            #region add ContractItem - Loại mặt hàng
            if (hopdong.NhomSPCollection != null && hopdong.NhomSPCollection.Count > 0)
            {
                hd.ContractDocument.Items = new List<Item>();
                foreach (KDT.GC.NhomSanPham item in hopdong.NhomSPCollection)
                    hd.ContractDocument.Items.Add(new Item
                    {
                        Identity = item.MaSanPham,
                        Name = item.TenSanPham,
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 0),
                        ProductValue = Helpers.FormatNumeric(item.TriGia, 0),
                        PaymentValue = Helpers.FormatNumeric(item.GiaGiaCong, 0),
                    });
            }
            #endregion

            #region add Product - sản phẩm
            if (hopdong.SPCollection != null && hopdong.SPCollection.Count > 0)
            {
                hd.ContractDocument.Products = new List<Product>();
                foreach (KDT.GC.SanPham item in hopdong.SPCollection)
                    hd.ContractDocument.Products.Add(new Product
                    {
                        Commodity = new Commodity
                        {
                            Identification = item.Ma,
                            Description = item.Ten,
                            TariffClassification = item.MaHS.Trim(),
                            ProductGroup = item.NhomSanPham_ID
                        },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.DVT_ID) : item.DVT_ID }
                        //GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID }
                    });
            }
            #endregion

            #region add Materials - Nguyên Phụ Liệu
            if (hopdong.NPLCollection != null && hopdong.NPLCollection.Count > 0)
            {
                hd.ContractDocument.Materials = new List<Product>();
                foreach (KDT.GC.NguyenPhuLieu item in hopdong.NPLCollection)
                    hd.ContractDocument.Materials.Add(new Product
                    {
                        Commodity = new Commodity
                        {
                            Identification = item.Ma,
                            Description = item.Ten,
                            TariffClassification = item.MaHS.Trim(),
                        },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.DVT_ID) : item.DVT_ID }
                        // GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID }
                    });
            }
            #endregion

            #region add Equipments - Thiết bị
            if (hopdong.TBCollection != null && hopdong.TBCollection.Count > 0)
            {
                hd.ContractDocument.Equipments = new List<Equipment>();
                foreach (KDT.GC.ThietBi item in hopdong.TBCollection)
                    hd.ContractDocument.Equipments.Add(new Equipment
                    {
                        Commodity = new Commodity
                        {
                            Identification = item.Ma,
                            Description = item.Ten,
                            TariffClassification = item.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.DVT_ID) : item.DVT_ID, Quantity = Helpers.FormatNumeric(item.SoLuongDangKy, 0) },
                        //GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID, Quantity = Helpers.FormatNumeric(item.SoLuongDangKy, 0) },
                        CurrencyExchange = new CurrencyExchange { CurrencyType = item.NguyenTe_ID },
                        Origin = new Origin { OriginCountry = item.NuocXX_ID.Trim() },
                        CustomsValue = new CustomsValue { unitPrice = Helpers.FormatNumeric(item.DonGia, GlobalsShare.DonGiaNT) },
                        Status = item.TinhTrang
                    });
            }
            #endregion add Hàng Mẫu

            #region  add Sample product - Hàng mẫu
            if (hopdong.SPCollection != null && hopdong.HangMauCollection.Count > 0)
            {
                hd.ContractDocument.SampleProducts = new List<Product>();
                foreach (KDT.GC.HangMau item in hopdong.HangMauCollection)
                    hd.ContractDocument.SampleProducts.Add(new Product
                    {
                        Commodity = new Commodity
                        {
                            Identification = item.Ma,
                            Description = item.Ten,
                            TariffClassification = item.MaHS.Trim(),
                        },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(item.DVT_ID) : item.DVT_ID, Quantity = Helpers.FormatNumeric(item.SoLuongDangKy, GlobalsShare.TriGiaNT) }
                        //GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID, Quantity = Helpers.FormatNumeric(item.SoLuongDangKy, GlobalsShare.TriGiaNT) }
                    });
            }
            #endregion
            return hd;
        }
        public static DeclarationBase HuyKhaiBao(string loaiToKhai, string reference, long soTiepNhan, string MaHaiQuan, DateTime ngayTiepNhan)
        {
            DeclarationBase dec = new DeclarationBase()
            {
                Issuer = loaiToKhai,
                Reference = reference,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(soTiepNhan),
                Acceptance = ngayTiepNhan.ToString(sfmtDateTime),
                DeclarationOffice = MaHaiQuan.Trim(),
                AdditionalInformations = new List<AdditionalInformation>()
            };
            dec.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                });
            return dec;
        }

        public static GC_DinhMuc ToDataTransferDinhMuc(DinhMucDangKy dmdk, HopDong hopdong, string tenDN)
        {

            bool isKhaiBaoSua = dmdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isKhaiBaoSua)
                dmdk.SoTiepNhan = KhaiBaoSua.SoTNSua(dmdk.SoTiepNhan, Convert.ToInt32(dmdk.NamTN), string.Empty, dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, LoaiKhaiBao.DinhMuc);

            GC_DinhMuc dinhmuc = new GC_DinhMuc()
            {
                Issuer = DeclarationIssuer.GC_DINH_MUC,
                Function = dmdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? DeclarationFunction.KHAI_BAO : DeclarationFunction.SUA,
                Reference = dmdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                DeclarationOffice = dmdk.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? Helpers.FormatNumeric(dmdk.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiBaoSua ? dmdk.NgayTiepNhan.ToString(sfmtDate) : string.Empty,
                Agents = new List<Agent>(),
                ContractReference = new Company.KDT.SHARE.Components.ContractDocument()
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    DeclarationOffice = hopdong.MaHaiQuan.Trim(),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate)
                }

            };
            dinhmuc.Agents.Add(new Agent()
            {

                Name = hopdong.TenDoanhNghiep,
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });


            dinhmuc.Importer = new NameBase()
            {
                Identity = dmdk.MaDoanhNghiep,
                Name = hopdong.TenDoanhNghiep
            };
            if (hopdong.TenDoanhNghiep == null || hopdong.TenDoanhNghiep.Trim() == "")
            {
                dinhmuc.Agents[0].Name = tenDN;
                dinhmuc.Importer.Name = tenDN;
            }
            dinhmuc.ProductionNorms = new List<ProductionNorm>();
            List<ProductionNorm> products = dinhmuc.ProductionNorms;


            foreach (DinhMuc dmSP in dmdk.DMCollection.ToArray().Distinct(new DistinctMaSP()))
            {
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.Ma = dmSP.MaSanPham;
                sp.HopDong_ID = hopdong.ID;
                sp.Load();
                ProductionNorm p = new ProductionNorm()
                {
                    Product = new Product()
                    {
                        Commodity = new Commodity()
                        {
                            Identification = sp.Ma,
                            Description = sp.Ten,
                            TariffClassification = sp.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure()
                        {
                            MeasureUnit = sp.DVT_ID
                        }
                    }
                };

                //IEnumerable<DinhMuc> dmNPLs = (from npl in dmdk.DMCollection.ToArray()
                //                               where npl.MaSanPham == dmSP.MaSanPham
                //                               select npl);

                //if (dmNPLs != null)
                //    foreach (DinhMuc dmNpl in dmNPLs)
                //    {
                //        p.MaterialsNorms = new List<MaterialsNorm>();
                //        NguyenPhuLieu npl = new NguyenPhuLieu();
                //        npl.Ma = dmNpl.MaNguyenPhuLieu;
                //        npl.HopDong_ID = hopdong.ID;
                //        npl = NguyenPhuLieu.Load(hopdong.ID, npl.Ma);

                //        MaterialsNorm mtn = new MaterialsNorm()
                //        {
                //            Material = new Product()
                //            {
                //                Commodity = new Commodity()
                //                {
                //                    Identification = npl.Ma,
                //                    Description = npl.Ten,
                //                    TariffClassification = npl.MaHS.Trim()
                //                },
                //                GoodsMeasure = new GoodsMeasure()
                //                {
                //                    MeasureUnit = npl.DVT_ID
                //                }
                //            },
                //            Loss = Helpers.FormatNumeric(dmNpl.TyLeHaoHut, 1),
                //            Norm = Helpers.FormatNumeric(dmNpl.DinhMucSuDung),
                //            RateExchange = "1"
                //        };
                //        p.MaterialsNorms.Add(mtn);
                //    }
                p.MaterialsNorms = NguyenPhuLieu.GetNPLFromDinhMuc(dmdk.ID, dmSP.MaSanPham, hopdong.ID);
                if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS)
                {
                    foreach (MaterialsNorm item in p.MaterialsNorms)
                    {
                        item.Material.GoodsMeasure.MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.Material.GoodsMeasure.MeasureUnit.Trim());

                    }
                }

                products.Add(p);
            }

            return dinhmuc;
        }

        public static GC_PhuKien ToDataTransferPhuKien(PhuKienDangKy pkdk, HopDong hopdong, string tenDN)
        {
            bool isKhaiSua = false;
            GC_PhuKien phukien = new GC_PhuKien()
            {
                Issuer = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                Function = DeclarationFunction.KHAI_BAO,
                Reference = pkdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                DeclarationOffice = pkdk.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isKhaiSua ? Helpers.FormatNumeric(pkdk.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiSua ? pkdk.NgayTiepNhan.ToString(
                ) : string.Empty,
                Agents = new List<Agent>(),
                ContractReference = new Company.KDT.SHARE.Components.ContractDocument()
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    DeclarationOffice = hopdong.MaHaiQuan.Trim(),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate),
                    CustomsReference = Helpers.FormatNumeric(hopdong.SoTiepNhan)
                }

            };
            // Người Khai Hải Quan
            phukien.Agents.Add(new Agent
            {
                Name = hopdong.TenDoanhNghiep,
                Identity = hopdong.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            phukien.Agents.Add(new Agent
            {
                Name = hopdong.TenDoanhNghiep,
                Identity = hopdong.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            phukien.Importer = new NameBase()
            {
                Identity = hopdong.MaDoanhNghiep,
                Name = hopdong.TenDoanhNghiep
            };
            if (hopdong.TenDoanhNghiep.Trim() == "")
            {
                foreach (Agent item in phukien.Agents)
                {
                    item.Name = tenDN;
                }
                phukien.Importer.Name = tenDN;
            }
            phukien.SubContract = new Subcontract()
            {
                Reference = pkdk.SoPhuKien,
                Decription = pkdk.GhiChu,
                Issue = pkdk.NgayPhuKien.ToString(sfmtDate)
            };
            phukien.AdditionalInformations = new List<AdditionalInformation>();
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien loaiPK in pkdk.PKCollection)
            {
                if (loaiPK.HPKCollection == null || loaiPK.HPKCollection.Count == 0)
                    loaiPK.LoadCollection();
                AdditionalInformation additionalInformation = new AdditionalInformation()
                {
                    Statement = loaiPK.MaPhuKien.Trim()
                };
                switch (loaiPK.MaPhuKien.Trim())
                {
                    #region Hủy Hợp đồng
                    case "101":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    additionalInformation = "Lý do hủy phụ kiện " + loaiPK.NoiDung,
                                    //AdditionalInformation = new AdditionalInformation()
                                    //{
                                    //    ContentPK = new Content() { Text = pkdk.GhiChu }
                                    //}
                                }
                            };
                            break;
                        }
                    #endregion

                    #region Hủy đăng ký Sản phẩm
                    case "102":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Products = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Product p = new Product()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Products.Add(p);
                            }
                            break;
                        }
                    #endregion

                    #region Hủy đăng ký Nguyên liệu
                    case "103":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Materials = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Product p = new Product()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Materials.Add(p);
                            };
                            break;
                        }
                    #endregion

                    #region Hủy đăng ký Thiết bị
                    case "104":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Equipments = new List<Equipment>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Equipment eq = new Equipment()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Equipments.Add(eq);
                            }
                            break;
                        }
                    #endregion

                    #region Hủy đăng ký Hàng mẫu
                    case "105":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    SampleProducts = new List<Equipment>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Equipment eq = new Equipment()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.SampleProducts.Add(eq);
                            }
                            break;
                        }
                    #endregion

                    #region Gia hạn Hợp đồng
                    case "201":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    OldExpire = DateTime.ParseExact(loaiPK.ThongTinCu, "dd/MM/yyyy", null).ToString(sfmtDate),
                                    NewExpire = DateTime.ParseExact(loaiPK.ThongTinMoi, "dd/MM/yyyy", null).ToString(sfmtDate)
                                }
                            };
                            break;
                        }
                    #endregion

                    #region Sửa thông tin chung Hợp đồng
                    case "501":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Payment = new Payment { Method = hopdong.PTTT_ID.Trim() },
                                    CurrencyExchange = new CurrencyExchange
                                    {
                                        CurrencyType = hopdong.NguyenTe_ID
                                    },
                                    Importer = new NameBase
                                    {
                                        Name = hopdong.TenDoanhNghiep,
                                        Identity = hopdong.MaDoanhNghiep,
                                        Address = hopdong.DiaChiDoanhNghiep
                                    },
                                    Exporter = new NameBase
                                    {
                                        Name = hopdong.TenDonViDoiTac,
                                        Identity = hopdong.DonViDoiTac,
                                        Address = hopdong.DiaChiDoiTac
                                    },
                                    CustomsValue = new CustomsValue
                                    {
                                        TotalPaymentValue = Helpers.FormatNumeric(hopdong.TongTriGiaTienCong, GlobalsShare.TriGiaNT),
                                        TotalProductValue = Helpers.FormatNumeric(hopdong.TongTriGiaSP, GlobalsShare.TriGiaNT)
                                    },
                                    ImportationCountry = "VN",
                                    ExportationCountry = hopdong.NuocThue_ID.Trim(),
                                    AdditionalInformation = new AdditionalInformation_GhiChu
                                    {
                                        Content = hopdong.GhiChu
                                    }
                                }
                            };

                            break;
                        }
                    #endregion

                    #region Sửa Sản phẩm
                    case "502":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Products = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Product p = new Product()
                                {
                                    PreIdentification = hangPK.MaHang,

                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS.Trim(),
                                        ProductGroup = hangPK.NhomSP
                                    },
                                    GoodsMeasure = new GoodsMeasure()
                                    {
                                        MeasureUnit = hangPK.DVT_ID.Substring(0, 3)
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Products.Add(p);
                            }
                            break;
                        }
                    #endregion

                    #region Sửa Nguyên liệu
                    case "503":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Materials = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.Materials.Add(new Product
                                {
                                    PreIdentification = hangPK.ThongTinCu,
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS.Trim()
                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        Quantity = Helpers.FormatNumeric(hangPK.SoLuong, 0),
                                        MeasureUnit = hangPK.DVT_ID.Substring(0, 3)
                                    }
                                });
                            break;
                        }
                    #endregion

                    #region Sửa Thiết bị
                    case "504":
                        {

                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Equipments = new List<Equipment>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.Equipments.Add(new Equipment
                                {
                                    PreIdentification = hangPK.ThongTinCu,
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS.Trim()
                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        Quantity = Helpers.FormatNumeric(hangPK.SoLuong, 0),
                                        MeasureUnit = hangPK.DVT_ID.Substring(0, 3)
                                    },
                                    CurrencyExchange = new CurrencyExchange
                                    {
                                        CurrencyType = hangPK.NguyenTe_ID.Trim()
                                    },
                                    Origin = new Origin
                                    {
                                        OriginCountry = hangPK.NuocXX_ID.Trim()
                                    },
                                    CustomsValue = new CustomsValue
                                    {
                                        unitPrice = Helpers.FormatNumeric(hangPK.DonGia, GlobalsShare.DonGiaNT)
                                    },
                                    Status = hangPK.TinhTrang,
                                });
                            break;
                        }
                    #endregion

                    #region Sửa Hàng mẫu
                    case "505":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    SampleProducts = new List<Equipment>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.SampleProducts.Add(new Equipment
                            {
                                PreIdentification = hangPK.ThongTinCu,
                                Commodity = new Commodity
                                {
                                    Identification = hangPK.MaHang,
                                    Description = hangPK.TenHang,
                                    TariffClassification = hangPK.MaHS.Trim()
                                },
                                GoodsMeasure = new GoodsMeasure
                                {
                                    Quantity = Helpers.FormatNumeric(hangPK.SoLuong, GlobalsShare.TriGiaNT),
                                    MeasureUnit = Helpers.FormatNumeric(hangPK.DonGia, GlobalsShare.DonGiaNT)
                                }

                            });
                            break;
                        }
                    #endregion

                    #region Bổ sung Sản phẩm
                    case "802":
                        {
                            additionalInformation.ContentPK = new Content()
                            {
                                Declaration = new DeclarationPhuKien()
                                {
                                    Products = new List<Product>()
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                Product p = new Product()
                                {
                                    Commodity = new Commodity()
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS.Trim(),
                                        ProductGroup = hangPK.NhomSP
                                    }
                                    ,
                                    GoodsMeasure = new GoodsMeasure()
                                    {
                                        MeasureUnit = hangPK.DVT_ID
                                    }
                                };
                                additionalInformation.ContentPK.Declaration.Products.Add(p);
                            }
                            break;
                        }
                    #endregion

                    #region Bổ sung Nguyên liệu
                    case "803":
                        {
                            additionalInformation.ContentPK = new Content
                            {
                                Declaration = new DeclarationPhuKien
                                {
                                    Materials = new List<Product>(),
                                }
                            };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.Materials.Add(new Product
                                {
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS.Trim(),
                                        Origin = hangPK.TinhTrang

                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        MeasureUnit = hangPK.DVT_ID.Substring(0, 3)
                                    }
                                });


                            break;
                        }
                    #endregion

                    #region Bổ sung Thiết bị
                    case "804":
                        {
                            additionalInformation.ContentPK = new Content
                           {
                               Declaration = new DeclarationPhuKien
                               {
                                   Equipments = new List<Equipment>()
                               }
                           };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                                additionalInformation.ContentPK.Declaration.Equipments.Add(new Equipment
                                {
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS.Trim()
                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        MeasureUnit = hangPK.DVT_ID.Substring(0, 3),
                                        Quantity = Helpers.FormatNumeric(hangPK.SoLuong, GlobalsShare.TriGiaNT)
                                    },
                                    CurrencyExchange = new CurrencyExchange
                                    {
                                        CurrencyType = hangPK.NguyenTe_ID.Trim()
                                    },
                                    Origin = new Origin
                                    {
                                        OriginCountry = hangPK.NuocXX_ID.Trim()
                                    },
                                    CustomsValue = new CustomsValue
                                    {
                                        unitPrice = Helpers.FormatNumeric(hangPK.DonGia, GlobalsShare.DonGiaNT)
                                    },
                                    Status = hangPK.TinhTrang,
                                });


                            break;
                        }
                    #endregion

                    #region Bổ sung Hàng mẫu
                    case "805":
                        {
                            additionalInformation.ContentPK = new Content
                           {
                               Declaration = new DeclarationPhuKien
                               {
                                   SampleProducts = new List<Equipment>()
                               }
                           };
                            foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                            {
                                additionalInformation.ContentPK.Declaration.SampleProducts.Add(new Equipment
                                {
                                    Commodity = new Commodity
                                    {
                                        Identification = hangPK.MaHang,
                                        Description = hangPK.TenHang,
                                        TariffClassification = hangPK.MaHS.Trim()
                                    },
                                    GoodsMeasure = new GoodsMeasure
                                    {
                                        Quantity = Helpers.FormatNumeric(hangPK.SoLuong, 0),
                                        MeasureUnit = hangPK.DVT_ID.Substring(0, 3)
                                    }
                                });
                            }
                            break;
                        }
                    #endregion

                    default:
                        break;
                }
                phukien.AdditionalInformations.Add(additionalInformation);
            }
            return phukien;
        }

        public static GC_DNGiamSatTieuHuy ToDataTransferGSTieuHuy(GiamSatTieuHuy gsTieuHuy, HopDong hopdong)
        {
            bool isKhaiBaoSua = gsTieuHuy.ActionStatus == (int)ActionStatus.DN_GiamSatSua;
            GC_DNGiamSatTieuHuy dnGsTieuHuy = new GC_DNGiamSatTieuHuy()
            {
                Issuer = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                Function = isKhaiBaoSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                Reference = gsTieuHuy.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                DeclarationOffice = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(gsTieuHuy.MaHaiQuan.Trim()) : gsTieuHuy.MaHaiQuan.Trim(),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? Helpers.FormatNumeric(gsTieuHuy.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiBaoSua ? gsTieuHuy.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                Agents = new List<Agent>(),
                Importer = new NameBase() { Identity = gsTieuHuy.MaDoanhNghiep, Name = hopdong.TenDoanhNghiep },
                SubContructReference = new List<Company.KDT.SHARE.Components.ContractDocument>(),
                //{
                //    Reference = hopdong.SoHopDong,
                //    Issue = hopdong.NgayKy.ToString(sfmtDate),
                //    CustomsReference = hopdong.SoHopDong,
                //},
                ContractReference = new Company.KDT.SHARE.Components.ContractDocument()
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    DeclarationOffice = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hopdong.MaHaiQuan.Trim()) : hopdong.MaHaiQuan.Trim(),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate),
                    CustomsReference = hopdong.SoHopDong
                },
                License = new Company.KDT.SHARE.Components.License()
                {
                    NumberLicense = gsTieuHuy.SoGiayPhep,
                    DateLicense = gsTieuHuy.NgayGiayPhep.ToString(sfmtDate),
                    ExpireDate = gsTieuHuy.NgayHetHan.ToString(sfmtDate),
                    AdminitrativeOrgan = gsTieuHuy.ToChucCap,
                },
                UserAttends = gsTieuHuy.CacBenThamGia,
                Time = gsTieuHuy.ThoiGianTieuHuy.ToString(sfmtDate),
                Location = gsTieuHuy.DiaDiemTieuHuy,
                AdditionalInformationNew = new AdditionalDocument() 
                {
                    Content = gsTieuHuy.GhiChuKhac
                },
                //AdditionalInformations = new List<AdditionalInformation>(),

            };
            PhuKienDangKyCollection pkHD = new PhuKienDangKyCollection();
            pkHD = hopdong.GetPK();
            foreach (PhuKienDangKy item in pkHD)
            {
                dnGsTieuHuy.SubContructReference.Add(new Company.KDT.SHARE.Components.ContractDocument
                {
                    Reference = item.SoPhuKien,
                    Issue = item.NgayPhuKien.ToString(sfmtDate),
                    CustomsReference = item.SoTiepNhan.ToString(),
                });

            }
            //if (pkHD.Count == 0)
            //{
            //    dnGsTieuHuy.SubContructReference.Add(new Company.KDT.SHARE.Components.ContractDocument
            //    {
            //        Reference = "1",
            //        Issue = "2014-09-29",
            //        CustomsReference = "1",
            //    });
            //}

            //dnGsTieuHuy.AdditionalInformations.Add(new AdditionalInformation() { Statement = "001", Content = new Content() { Text = gsTieuHuy.GhiChuKhac } });
            dnGsTieuHuy.Agents.Add(new Agent
             {
                 Name = hopdong.TenDoanhNghiep,
                 Identity = hopdong.MaDoanhNghiep,
                 Status = AgentsStatus.NGUOIKHAI_HAIQUAN
             });
            dnGsTieuHuy.Agents.Add(new Agent
                                 {
                                     Name = hopdong.TenDoanhNghiep,
                                     Identity = hopdong.MaDoanhNghiep,
                                     Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                                 });
            if (gsTieuHuy.HangGSTieuHuys != null && gsTieuHuy.HangGSTieuHuys.Count > 0)
            {
                dnGsTieuHuy.Scraps = new List<Company.KDT.SHARE.Components.Scrap>();
                foreach (HangGSTieuHuy item in gsTieuHuy.HangGSTieuHuys)
                {
                    Company.KDT.SHARE.Components.Scrap scrap = new Company.KDT.SHARE.Components.Scrap()
                    {
                        sequence = item.STTHang.ToString(),
                        Commodity = new Commodity
                        {
                            Identification = item.MaHang,
                            Type = Helpers.FormatNumeric(item.LoaiHang),
                            TariffClassification = item.MaHS.Trim(),
                            Description = item.TenHang,
                        },
                        GoodsMeasure = new SXXK_GoodsMeasure
                        {
                            Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                            //ConversionRate = "1",
                            //MeasureUnits = new List<string>(),
                            RegisteredMeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim()
                        }
                    };
                    //scrap.GoodsMeasure.MeasureUnits.Add(GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim());
                    //scrap.GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());
                    dnGsTieuHuy.Scraps.Add(scrap);
                }

            }
            return dnGsTieuHuy;
        }
        public static GC_ThanhKhoan ToDataTransferThanhKhoan(ThanhKhoan ThanhKhoan, HopDong hopdong, int SoThapPhanHang)
        {
            bool isKhaiBaoSua = ThanhKhoan.ActionStatus == (int)ActionStatus.ThanhKhoanSua;
            GC_ThanhKhoan ThanhKhoanHD = new GC_ThanhKhoan()
            {
                Issuer = DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,
                Function = isKhaiBaoSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                Reference = ThanhKhoan.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                DeclarationOffice = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(ThanhKhoan.MaHaiQuan.Trim()) : ThanhKhoan.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? Helpers.FormatNumeric(ThanhKhoan.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiBaoSua ? ThanhKhoan.NgayTiepNhan.ToString(sfmtDate) : string.Empty,
                Agents = new List<Agent>(),
                Importer = new NameBase() { Identity = ThanhKhoan.MaDoanhNghiep, Name = hopdong.TenDoanhNghiep },
                ContractReference = new Company.KDT.SHARE.Components.ContractDocument()
                {
                    Reference = hopdong.SoHopDong,
                    Issue = hopdong.NgayKy.ToString(sfmtDate),
                    DeclarationOffice = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(hopdong.MaHaiQuan.Trim()) : hopdong.MaHaiQuan.Trim(),
                    Expire = hopdong.NgayHetHan.ToString(sfmtDate),
                    CustomsReference = hopdong.SoHopDong
                },
                AdditionalInformations = new List<AdditionalInformation>(),
                //GoodsItems = new List<Product>(),
                AdditionalDocuments = new List<AdditionalDocument>()
            };
            AdditionalDocument additional = new AdditionalDocument()
            {
                DocumentType = ThanhKhoan.LoaiChungTu,
                DocumentReference = ThanhKhoan.ThamChieuCT,
                Description = ThanhKhoan.MoTaCT,

            };
            ThanhKhoanHD.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = ThanhKhoan.GhiChu }, Statement = "001" });
            ThanhKhoanHD.Suggest = ThanhKhoan.GhiChu;
            ThanhKhoanHD.Agents.Add(new Agent
            {
                Name = hopdong.TenDoanhNghiep,
                Identity = hopdong.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ThanhKhoanHD.Agents.Add(new Agent
            {
                Name = hopdong.TenDoanhNghiep,
                Identity = hopdong.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            //additional.GoodItems = new List<GoodsItem>();
            ThanhKhoanHD.GoodsItems = new List<GoodsItem>();
            foreach (HangThanhKhoan item in ThanhKhoan.HangCollection)
            {
                //                 additional.DocumentType = item.LoaiChungTu;
                //                 additional.DocumentReference = item.ThamChieuCT;
                //                 additional.Description = item.MoTaCT;

                ThanhKhoanHD.GoodsItems.Add(new GoodsItem
                {
                    Commodity = new Commodity
                    {
                        Description = item.TenHang,
                        Identification = item.MaHang,
                        TariffClassification = item.MaHS.Trim(),
                        Type = item.LoaiHang.ToString(),
                        Suggest = item.GhiChu
                    },
                    GoodsMeasure = new GoodsMeasure
                    {
                        quantityImport = Helpers.FormatNumeric(item.SoLuong, SoThapPhanHang),
                        quantityExport = Helpers.FormatNumeric(item.LuongXuat, SoThapPhanHang),
                        quantitySupply = Helpers.FormatNumeric(item.LuongCU, SoThapPhanHang),
                        quantityExcess = Helpers.FormatNumeric(item.LuongTon, SoThapPhanHang),
                        Quantity = Helpers.FormatNumeric(item.SoLuong, SoThapPhanHang),
                        MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID.Trim(),
                        //QuantityReExport = Helpers.FormatNumeric(item.LuongTaiXuat, SoThapPhanHang)
                    }

                });
            }
            ThanhKhoanHD.AdditionalDocuments.Add(additional);
            return ThanhKhoanHD;
        }

        #region TransferOb tờ khai
        public static ToKhai ToDataTransferObject(ToKhaiMauDich tkmd)
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");
            bool isToKhaiSua = tkmd.SoToKhai != 0 && tkmd.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isToKhaiSua)
                tkmd.SoTiepNhan = KhaiBaoSua.SoTNSua(tkmd.SoTiepNhan, tkmd.SoToKhai, tkmd.NgayDangKy.Year, tkmd.MaLoaiHinh,
                        tkmd.MaHaiQuan.Trim(), tkmd.MaDoanhNghiep, LoaiKhaiBao.ToKhai);
            string Issuer = string.Empty;
            if (tkmd.MaLoaiHinh.Contains("NTA"))
            {
                if (isToKhaiNhap)
                    Issuer = DeclarationIssuer.KD_TOKHAI_NHAP;
                else
                    Issuer = DeclarationIssuer.KD_TOKHAI_XUAT;
            }
            else
            {
                if (isToKhaiNhap)
                    Issuer = DeclarationIssuer.GC_TOKHAI_NHAP;
                else
                    Issuer = DeclarationIssuer.GC_TOKHAI_XUAT;
            }
            #region Header
            ToKhai tokhai = new ToKhai()
            {
                Issuer = Issuer, //isToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_NHAP : DeclarationIssuer.GC_TOKHAI_XUAT ,
                Reference = tkmd.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                // Số tiếp nhận
                CustomsReference = isToKhaiSua ? Helpers.FormatNumeric(tkmd.SoTiepNhan) : string.Empty,
                //Ngày dang ký chứng thư

                Acceptance = isToKhaiSua ? tkmd.NgayDangKy.ToString(sfmtDateTime) : string.Empty,

                // Ðon vị hải quan khai báo
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkmd.MaHaiQuan.Trim()).Trim() : tkmd.MaHaiQuan.Trim(),//tkmd.MaHaiQuan.Trim(),
                // Số hàng
                GoodsItem = Helpers.FormatNumeric(tkmd.SoHang),
                LoadingList = Helpers.FormatNumeric(tkmd.SoLuongPLTK),

                // Khối luợng và khối luợng tịnh
                TotalGrossMass = Helpers.FormatNumeric(tkmd.TrongLuong, 3),
                TotalNetGrossMass = Helpers.FormatNumeric(tkmd.TrongLuongNet, 3),
                // Mã Loại Hình

                NatureOfTransaction = tkmd.MaLoaiHinh,
                // Phuong thức thanh toán
                PaymentMethod = tkmd.PTTT_ID,



                #region Install element
                Agents = new List<Agent>(),

                //Nguyên tệ
                CurrencyExchange = new CurrencyExchange { CurrencyType = tkmd.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkmd.TyGiaTinhThue, 3) },

                //Số kiện
                DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkmd.SoKien) },
                //Neu có day du 1 trong 3 chứng từ :  Giấy phép, Hợp đồng, Vận đơn
                /* AdditionalDocuments = new List<AdditionalDocument>(),*/

                // Hóa Ðon thuong mại
                Invoice = new Invoice
                {
                    Issue = tkmd.NgayHoaDonThuongMai.Year <= 1900 ? string.Empty : tkmd.NgayHoaDonThuongMai.ToString(sfmtDate),
                    Reference = tkmd.SoHoaDonThuongMai,
                    Type = AdditionalDocumentType.HOA_DON_THUONG_MAI
                },

                //datcv 01/10/2015 Neu la to khai nhap thi Exporter (nguoi xuat khau) phai la doi tac va nguoc lai
                // Doanh Nghiệp Xuất khẩu
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = "." } :
                new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() },

                //Doanh nghiệp nhập khẩu
                Importer = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = "." } :
                new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() },

                // Người đại diện doanh nghiệp | Tên chủ hàng
                RepresentativePerson = new RepresentativePerson { Name = tkmd.TenChuHang, ContactFunction = tkmd.ChucVu },

                // Đề xuất khác | Mã loại thông tin - Ghi chú khác
                AdditionalInformations = new List<AdditionalInformation>(),

                // GoodsShipmet Thông tin hàng hóa
                GoodsShipment = new GoodsShipment(),

                /*  License = new List<License>(),
                  ContractDocument = new List<ContractDocument>(),
                  CommercialInvoices = new List<CommercialInvoice>(),
                  CertificateOfOrigins = new List<CertificateOfOrigin>(),
                  CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                  AttachDocumentItem = new List<AttachDocumentItem>(),
                  AdditionalDocumentEx = new List<AdditionalDocument>(),
                 */

                #region Ân hạng thuế và Đảm bảo nghĩa vụ nộp thuế
                TaxGrace = tkmd.AnHanThue.IsAnHan ? new Company.KDT.SHARE.Components.Messages.TaxGrace()
                {
                    IsGrace = "1",
                    Reason = tkmd.AnHanThue.LyDoAnHan,
                    Value = Helpers.FormatNumeric(tkmd.AnHanThue.SoNgay, 0)
                } : null,

                TaxGuarantee = tkmd.DamBaoNghiaVuNopThue.IsDamBao ? new Company.KDT.SHARE.Components.Messages.TaxGuarantee()
                {
                    IsGuarantee = "1",
                    Type = tkmd.DamBaoNghiaVuNopThue.HinhThuc,
                    Value = Helpers.FormatNumeric(tkmd.DamBaoNghiaVuNopThue.TriGiaDB, Company.KDT.SHARE.Components.Globals.TriGiaNT),
                    Issue = tkmd.DamBaoNghiaVuNopThue.NgayBatDau.ToString(sfmtDate),
                    Expire = tkmd.DamBaoNghiaVuNopThue.NgayKetThuc.ToString(sfmtDate)
                } : null,
                #endregion

                #region  Số container của tờ khai
                TransportEquipment = new QuantityContainer
                {
                    Quantity20 = "0",
                    Quantity40 = "0",
                    Quantity45 = "0",
                    QuantityOthers = "0"
                },


                #endregion

                //TemporaryImportExpire = null,
                #endregion Nrr

            };
            #region Neu khai to khai Thu cong va dua vao TK
            if (Company.KDT.SHARE.Components.Globals.IsKTX)
            {
                if (Company.KDT.SHARE.Components.Globals.IsKhaiTK)
                {
                    if (tkmd.MaLoaiHinh.Contains("V"))
                        tokhai.NatureOfTransaction = tkmd.MaLoaiHinh.Substring(2, 3);
                    tokhai.CustomsReference = tkmd.LoaiVanDon;
                    tokhai.DeclarationOfficeLiquidity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkmd.MaHaiQuan.Trim());
                    tokhai.Clearance = tkmd.Ngay_THN_THX.ToString(sfmtDate);
                    tokhai.Channel = tkmd.PhanLuong;
                    tokhai.Acceptance = tkmd.NgayDangKy.ToString(sfmtDateTime);
                    if (tkmd.MaLoaiHinh.Contains("G23"))
                    {
                        if (!isToKhaiNhap)
                        {
                            tokhai.Importer.Name = tkmd.TenDonViDoiTac;
                            tokhai.Importer.Identity = tkmd.MaDoanhNghiep;
                        }
                    }
                }
                else
                {
                    tokhai.CustomsReferenceManual = tkmd.ChiTietDonViDoiTac;
                    tokhai.Clearance = tkmd.Ngay_THN_THX.ToString(sfmtDate);
                    tokhai.Channel = tkmd.PhanLuong;
                    tokhai.Acceptance = tkmd.NgayDangKy.ToString(sfmtDateTime);
                }
                if (tkmd.MaLoaiHinh.Contains("V"))
                    tokhai.NatureOfTransaction = tkmd.MaLoaiHinh.Substring(2, 3);
                if (tkmd.MaLoaiHinh.Contains("G23"))
                {
                    if (!isToKhaiNhap)
                    {
                        tokhai.Importer.Name = tkmd.TenDonViDoiTac;
                        tokhai.Importer.Identity = tkmd.MaDoanhNghiep;
                    }
                    else
                    {

                    }
                }
            }
            #endregion
            #region fill  Số container của tờ khai (nếu có)
            //if (tkmd.VanTaiDon != null && tkmd.VanTaiDon.ContainerCollection != null && tkmd.VanTaiDon.ContainerCollection.Count > 0)
            //{
            //    int cont20 = 0;
            //    int cont40 = 0;
            //    int cont45 = 0;
            //    int contOther = 0;
            //    foreach (Container cont in tkmd.VanTaiDon.ContainerCollection)
            //    {
            //        switch (cont.LoaiContainer)
            //        {
            //            case "2" :
            //                cont20++;
            //                break;
            //            case "4":
            //                cont40++;
            //                break;
            //            case "45":
            //                cont45++;
            //                break;
            //            case "0":
            //                contOther++;
            //                break;
            //            default:
            //                break;
            //        }
            //    }
            if (tkmd.SoLuongContainer != null)
            {
                tokhai.TransportEquipment.Quantity20 = Helpers.FormatNumeric(tkmd.SoLuongContainer.Cont20, 0);
                tokhai.TransportEquipment.Quantity40 = Helpers.FormatNumeric(tkmd.SoLuongContainer.Cont40, 0);
                tokhai.TransportEquipment.Quantity45 = Helpers.FormatNumeric(tkmd.SoLuongContainer.Cont45, 0);
                tokhai.TransportEquipment.QuantityOthers = Helpers.FormatNumeric(tkmd.SoLuongContainer.ContKhac, 0);
            }



            #endregion

            tokhai.AdditionalInformations.Add(new AdditionalInformation
            {
                Statement = "001",
                Content = new Content() { Text = tkmd.DeXuatKhac }
            });
            if (isToKhaiSua)
                tokhai.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Statement = "005",
                    Content = new Content() { Text = tkmd.LyDoSua }
                });
            #endregion Header

            #region Agents Đại lý khai
            tokhai.Agents = AgentsFrom(tkmd);
            #endregion

            #region AdditionalDocument Hợp đồng - Giấy phép - Vận đơn ngoài tờ khai
            bool isDocument = !string.IsNullOrEmpty(tkmd.SoHopDong) ||
                !string.IsNullOrEmpty(tkmd.SoVanDon) || !string.IsNullOrEmpty(tkmd.SoGiayPhep);
            if (isDocument) tokhai.AdditionalDocuments = new List<AdditionalDocument>();
            // Add AdditionalDocument (thêm hợp đồng)
            if (!string.IsNullOrEmpty(tkmd.SoHopDong))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayHopDong.Year > 1900 ? tkmd.NgayHopDong.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoHopDong,
                    Type = AdditionalDocumentType.HOP_DONG,
                    Name = "Hop dong",
                    Expire = tkmd.NgayHetHanHopDong.Year > 1900 ? tkmd.NgayHetHanHopDong.ToString(sfmtDate) : string.Empty
                });
            // Thêm vận đơn
            if (isToKhaiNhap && !string.IsNullOrEmpty(tkmd.SoVanDon))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayVanDon.Year > 1900 ? tkmd.NgayVanDon.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoVanDon,
                    Type = AdditionalDocumentType.BILL_OF_LADING_ORIGIN,
                    Name = "Bill of lading"
                });
            // Thêm giấy phép
            if (!string.IsNullOrEmpty(tkmd.SoGiayPhep))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayGiayPhep.Year > 1900 ? tkmd.NgayGiayPhep.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoGiayPhep,
                    Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                    Name = "Giay phep",
                    Expire = tkmd.NgayHetHanGiayPhep.Year > 1900 ? tkmd.NgayHetHanGiayPhep.ToString(sfmtDate) : string.Empty
                });

            #endregion

            #region GoodsShipment thông tin về hàng hóa
            tokhai.GoodsShipment = new GoodsShipment()
            {
                ImportationCountry = isToKhaiNhap ? null : tkmd.NuocNK_ID.Substring(0, 2),
                ExportationCountry = isToKhaiNhap ? tkmd.NuocXK_ID.Substring(0, 2) : null,
                Consignor = new NameBase { Identity = string.Empty, Name = string.Empty },
                Consignee = new NameBase { Identity = string.Empty, Name = string.Empty },
                NotifyParty = new NameBase { Identity = string.Empty, Name = string.Empty },
                DeliveryDestination = new DeliveryDestination { Line = string.Empty },
                EntryCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? tkmd.CuaKhau_ID : string.Empty, Name = isToKhaiNhap ? DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) : string.Empty },
                ExitCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? string.Empty : tkmd.CuaKhau_ID, Name = isToKhaiNhap ? string.Empty : DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) },
                Importer = isToKhaiNhap ? null : new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty },
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty } : null,
                TradeTerm = new TradeTerm { Condition = tkmd.DKGH_ID },
                //CustomsGoodsItems = new List<CustomsGoodsItem>()
            };
            #endregion GoodsShipment

            #region CustomGoodsItem Danh sách hàng khai báo

            //Add CustomGoodsItem
            int soluonghang = 0;
            if (tkmd.HMDCollection != null)
            {
                soluonghang = tkmd.HMDCollection.Count;
                tokhai.GoodsShipment.CustomsGoodsItems = new List<CustomsGoodsItem>();
            }
            for (int i = 0; i < soluonghang; i++)
            {
                HangMauDich hmd = tkmd.HMDCollection[i];
                #region CustomsGoodsItem
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem
                {

                    CustomsValue = Helpers.FormatNumeric(hmd.TriGiaKB, GlobalsShare.TriGiaNT),
                    Sequence = Helpers.FormatNumeric(hmd.SoThuTuHang),
                    StatisticalValue = Company.KDT.SHARE.Components.Globals.IsTinhThue ? Helpers.FormatNumeric(hmd.TriGiaTT, GlobalsShare.TriGiaNT) : Helpers.FormatNumeric(hmd.TriGiaKB_VND, GlobalsShare.DonGiaNT),
                    UnitPrice = Helpers.FormatNumeric(hmd.DonGiaKB, GlobalsShare.DonGiaNT),
                    StatisticalUnitPrice = Helpers.FormatNumeric(hmd.DonGiaTT, GlobalsShare.DonGiaNT),
                    Manufacturer = new NameBase { Name = hmd.TenHangSX/*Tên hãng sx*/, Identity = hmd.MaHangSX/*Mã hãng sx*/ },
                    Origin = new Origin { OriginCountry = hmd.NuocXX_ID.Substring(0, 2) },
                    GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hmd.SoLuong, 6), MeasureUnit = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hmd.DVT_ID) : hmd.DVT_ID }

                };

                //Trị giá trên mỗi dòng hàng (Thường thì chia đều cho tất cả dòng hàng) nếu có 1 dòng hàng thì tính tổng
                //Nếu có nhiều hơn 1 dòng hàng thì chia đều ra cho mỗi dòng hàng
                // ExitToEntryCharge Tổng chi phí bằng (tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem)/số kiện hàng
                //FreightCharge chi phí vận tải cũng chia đôi ra
                //Metho = số thứ tự kiện hàng.  
                decimal dTongPhi = tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem;
                decimal dPhiVanChuyen = tkmd.PhiVanChuyen;
                customsGoodsItem.CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = Helpers.FormatNumeric(dTongPhi / soluonghang, 4),
                    FreightCharge = Helpers.FormatNumeric(dPhiVanChuyen / soluonghang, 4),
                    Method = string.Empty,
                    OtherChargeDeduction = "0"
                };

                #endregion

                #region Commodity Hàng trong tờ khai
                Commodity commodity = new Commodity
                {
                    Description = hmd.TenHang,
                    Identification = hmd.MaPhu,
                    TariffClassification = hmd.MaHS.Trim(),
                    IsNew = (hmd.isHangCu != null && hmd.isHangCu) ? "0" : "1",
                    TariffClassificationExtension = hmd.MaHSMoRong,
                    Brand = hmd.NhanHieu,
                    Grade = hmd.QuyCachPhamChat,
                    Ingredients = hmd.ThanhPhan,
                    ModelNumber = hmd.Model,
                    isIntegrate = (hmd.ThongTinKhac != null && hmd.ThongTinKhac.Contains("isHangDongBo")) ? "1" : "0",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    //Hệ thống hải quan chưa sử dụng
                    InvoiceLine = new InvoiceLine { ItemCharge = string.Empty, Line = string.Empty }
                };
                if (tkmd.LoaiHangHoa == "N")
                    commodity.Type = "1";
                else if (tkmd.LoaiHangHoa == "S")
                    commodity.Type = "2";
                else if (tkmd.LoaiHangHoa == "T")
                    commodity.Type = "3";
                else if (tkmd.LoaiHangHoa == "H")
                    commodity.Type = "4";

                #region DutyTaxFee tiền thuế của hàng
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 2),
                    DutyRegime = hmd.BieuThueXNK,
                    SpecificTaxBase = string.Empty,
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatXNK, 1),
                    AbsoluteTax = tkmd.HMDCollection[i].ThueTuyetDoi ? Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 4) : string.Empty,
                    Type = DutyTaxFeeType.THUE_XNK
                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueGTGT, 2),
                    DutyRegime = hmd.BieuThueGTGT,
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatGTGT, 1),
                    Type = DutyTaxFeeType.THUE_VAT,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueTTDB, 2),
                    DutyRegime = hmd.BieuThueTTDB,
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatTTDB, 1),
                    Type = DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].TriGiaThuKhac, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].TyLeThuKhac, 1),
                    Type = DutyTaxFeeType.THUE_KHAC,


                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].PhuThu, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].TyLeThuKhac, 1),
                    Type = DutyTaxFeeType.THUE_CHENH_LECH_GIA,

                });
                #region Thuế bảo vệ môi trường và thuế chống bán phá giá
                if (tkmd.HMDCollection[i].ThueBVMT > 0)
                {

                    commodity.DutyTaxFee.Add(new DutyTaxFee
                    {
                        AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueBVMT, 2),
                        DutyRegime = hmd.BieuThueBVMT,
                        SpecificTaxBase = "",
                        Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatBVMT, 4),
                        Type = DutyTaxFeeType.THUE_BAO_VE_MOI_TRUONG,

                    });
                }
                if (tkmd.HMDCollection[i].ThueChongPhaGia > 0)
                {
                    commodity.DutyTaxFee.Add(new DutyTaxFee
                    {
                        AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueChongPhaGia, 2),
                        DutyRegime = hmd.BieuThueCBPG,
                        SpecificTaxBase = "",
                        Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatChongPhaGia, 4),
                        Type = DutyTaxFeeType.THUE_CHONG_BAN_PHA_GIA,

                    });
                }
                #endregion
                #region Phí hải quan(nếu có)
                tkmd.LoadListLePhiHQ();
                foreach (LePhiHQ lephi in tkmd.LePhiHQCollection)
                {
                    commodity.DutyTaxFee.Add(new DutyTaxFee
                    {
                        AdValoremTaxBase = Helpers.FormatNumeric(lephi.SoTienLePhi, 2),
                        DutyRegime = "",
                        SpecificTaxBase = "",
                        Tax = Helpers.FormatNumeric(0, 4),
                        Type = lephi.MaLePhi,

                    });
                }
                #endregion
                #endregion DutyTaxFee thuế

                customsGoodsItem.Commodity = commodity;
                #endregion Hàng chính

                #region AdditionalDocument if Search(Giấy phép có hàng trong tờ khai)
                //Lấy thông tin giấy phép đầu tiên
                if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = tkmd.GiayPhepCollection[0];
                    customsGoodsItem.AdditionalDocument = new AdditionalDocument()
                    {
                        Issue = giayphep.NgayGiayPhep.ToString(sfmtDate), /*Ngày cấp giấy phép yyyy-MM-dd*/
                        Issuer = giayphep.NguoiCap/*Người cấp*/,
                        IssueLocation = giayphep.NoiCap/*Nơi cấp*/,
                        Reference = giayphep.SoGiayPhep/*Số giấy phép*/,
                        Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                        Name = "GP"/* Tên giấy phép*/,
                        Expire = giayphep.NgayHetHan.ToString(sfmtDate)
                    };
                }
                #endregion

                #region CertificateOfOrigin if Search(Co có hàng trong tờ khai)

                if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.CO co = tkmd.COCollection[tkmd.COCollection.Count - 1];
                    customsGoodsItem.CertificateOfOrigin = new Company.KDT.SHARE.Components.CertificateOfOrigin()
                    {
                        Issue = co.NgayCO.ToString(sfmtDate),
                        Issuer = co.ToChucCap,
                        IssueLocation = co.NuocCapCO,
                        Reference = co.SoCO,
                        Type = co.LoaiCO,
                        Name = "CO",
                        Expire = co.NgayHetHan.ToString(sfmtDate),
                        Exporter = co.TenDiaChiNguoiXK,
                        Importer = co.TenDiaChiNguoiNK,
                        ExportationCountry = co.MaNuocXKTrenCO,
                        ImportationCountry = co.MaNuocNKTrenCO,
                        Content = co.ThongTinMoTaChiTiet,
                        IsDebt = Helpers.FormatNumeric(co.NoCo, 0),
                        Submit = co.ThoiHanNop.ToString(sfmtDate)
                    };
                }
                #endregion
                #region AdditionalInformations - ValuationAdjustments Thêm hàng từ tờ khai trị giá 1,2,3
                if (isToKhaiNhap)
                {
                    //Thông tin tờ khai trị giá.
                    #region Tờ khai trị giá PP1
                    if (tkmd.TKTGCollection != null)
                        if (tkmd.TKTGCollection.Count > 0)
                        {
                            ToKhaiTriGia tktg = tkmd.TKTGCollection[0];
                            foreach (HangTriGia hangtrigia in tktg.HTGCollection)
                                if (hangtrigia.TenHang.Trim() == hmd.TenHang.Trim())
                                {
                                    customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
                                    customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();
                                    /* Fill dữ liệu hàng  trong tờ khai trị giá PP1*/
                                    //Nếu có tờ khai trị giá thì method = 1 
                                    customsGoodsItem.CustomsValuation.Method = "1";
                                    #region AdditionalInformations Nội dung tờ khai trị giá
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TO_SO",
                                        Statement = AdditionalInformationStatement.TO_SO,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.ToSo) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "NGAY_XK",
                                        Statement = AdditionalInformationStatement.NGAY_XK,
                                        Content = new Content() { Text = tktg.NgayXuatKhau.ToString(sfmtDate) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "QUYEN_SD",
                                        Statement = AdditionalInformationStatement.QUYEN_SD,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.QuyenSuDung) }
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "KHONG_XD",
                                        Statement = AdditionalInformationStatement.KHONG_XD,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.KhongXacDinh) }
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TRA_THEM",
                                        Statement = AdditionalInformationStatement.TRA_THEM,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.TraThem) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TIEN_TRA_16",
                                        Statement = AdditionalInformationStatement.TIEN_TRA_16,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.TienTra) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "CO_QHDB",
                                        Statement = AdditionalInformationStatement.CO_QHDB,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.CoQuanHeDacBiet) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "KIEU_QHDB",
                                        Statement = AdditionalInformationStatement.KIEU_QHDB,
                                        Content = new Content() { Text = tktg.KieuQuanHe }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "ANH_HUONG_QH",
                                        Statement = AdditionalInformationStatement.ANH_HUONG_QH,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.AnhHuongQuanHe) }
                                    });
                                    #endregion Nội dung tờ khai trị giá

                                    #region ValuationAdjustments Chi tiết hàng tờ khai trị giá pp1
                                    if (tktg.HTGCollection != null && tktg.HTGCollection.Count > 0)
                                    {
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Gia_hoa_don,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.GiaTrenHoaDon, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Thanh_toan_gian_tiep,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.KhoanThanhToanGianTiep, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_tra_truoc,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TraTruoc, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_hoa_hong,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.HoaHong, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_bao_bi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiBaoBi, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_dong_goi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiDongGoi, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Khoan_tro_giup,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TroGiup, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_NVL,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.NguyenLieu, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_NL,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.VatLieu, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_cong_cu,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.CongCu, GlobalsShare.TriGiaNT)
                                        });
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_thiet_ke,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ThietKe, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_ban_quyen,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.BanQuyen, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_phai_tra,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienTraSuDung, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_van_tai,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiVanChuyen, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_bao_hiem,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.PhiBaoHiem, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_VT_BH_noi_dia,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiNoiDia, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_phat_sinh,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiPhatSinh, GlobalsShare.TriGiaNT)
                                        });


                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_lai,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienLai, GlobalsShare.TriGiaNT)
                                        });


                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Thue_phi_le_phi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienThue, GlobalsShare.TriGiaNT)
                                        });
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Khoan_Giam_Gia,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.GiamGia, GlobalsShare.TriGiaNT)
                                        });

                                    }
                                }


                                    #endregion Chi tiết hàng tờ khai trị giá

                        }
                    #endregion Tờ khai trị giá PP1

                    if (tkmd.TKTGPP23Collection != null)
                        if (tkmd.TKTGPP23Collection.Count > 0)
                        {
                            foreach (ToKhaiTriGiaPP23 tkpgP23 in tkmd.TKTGPP23Collection)
                            {
                                if (tkpgP23.TenHang.ToUpper().Trim() == hmd.TenHang.ToUpper().Trim())
                                {
                                    customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
                                    customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();
                                    string maTktg = tkpgP23.MaToKhaiTriGia.ToString();
                                    customsGoodsItem.CustomsValuation.Method = maTktg;
                                    #region AdditionalInformations Nội dung tờ khai trị giá PP23

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.LyDo },
                                        Statement = maTktg + AdditionalInformationStatement.LYDO_KAD_PP1,
                                        StatementDescription = "LYDO_KAD_PP1"

                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayXuat.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_XK23,
                                        StatementDescription = "NGAY_XK"

                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.STTHangTT) },
                                        Statement = maTktg + AdditionalInformationStatement.STTHANG_TT,
                                        StatementDescription = "STTHANG_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.SoTKHangTT) },
                                        Statement = maTktg + AdditionalInformationStatement.SOTK_TT,
                                        StatementDescription = "SOTK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayDangKyHangTT.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_NK_TT,
                                        StatementDescription = "NGAY_NK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.MaHaiQuanHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.MA_HQ_TT,
                                        StatementDescription = "MA_HQ_TT"
                                    });


                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayXuatTT.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_XK_TT,
                                        StatementDescription = "NGAY_XK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.GiaiTrinh },
                                        Statement = maTktg + AdditionalInformationStatement.GIAI_TRINH,
                                        StatementDescription = "GIAI_TRINH"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.MaLoaiHinhHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.Ma_LH,
                                        StatementDescription = "Ma_LH"
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.TenHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.HANG_TUONG_TU,
                                        StatementDescription = "Ma_LH"
                                    });
                                    #endregion Nội dung tờ khai trị giá PP23

                                    #region ValuationAdjustments Chi tiết nội dung trong tờ khai tri giá PP23

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.Tri_gia_hang_TT,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.TriGiaNguyenTeHangTT, GlobalsShare.TriGiaNT)
                                    });

                                    // Cộng ghi số dương trừ ghi số âm(+/-)
                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_cap_do_TM,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongThuongMai != 0 ? tkpgP23.DieuChinhCongThuongMai : -tkpgP23.DieuChinhTruCapDoThuongMai, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_so_luong,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongSoLuong != 0 ? tkpgP23.DieuChinhCongSoLuong : -tkpgP23.DieuChinhTruSoLuong, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_khoan_khac,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongKhoanGiamGiaKhac != 0 ? tkpgP23.DieuChinhCongKhoanGiamGiaKhac : -tkpgP23.DieuChinhTruKhoanGiamGiaKhac, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_van_tai,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiVanTai != 0 ? tkpgP23.DieuChinhCongChiPhiVanTai : -tkpgP23.DieuChinhTruChiPhiVanTai, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_bao_hiem,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiBaoHiem != 0 ? tkpgP23.DieuChinhCongChiPhiBaoHiem : -tkpgP23.DieuChinhTruChiPhiBaoHiem, GlobalsShare.TriGiaNT)
                                    });

                                    #endregion Chi tiết nội dung trong tờ khai tri giá PP23

                                    /* Fill dữ liệu hàng trong tờ khai trị giá PP23*/

                                }
                            }
                        }


                }
                else
                {
                    /*
                    customsGoodsItem.SpecializedManagement = new SpecializedManagement()
                    {
                        GrossMass = "",
                        Identification = "",
                        MeasureUnit = "",
                        Quantity = "",
                        Type = "",
                        UnitPrice = ""
                    };*/
                }
                #endregion thêm hàng từ tờ khai trị giá 1,2,3

                #region Văn bản miễn thuế
                if (hmd.MienGiamThueCollection != null && hmd.MienGiamThueCollection.Count > 0)
                {
                    ReduceTax miengiam = new ReduceTax
                    {
                        Reference = hmd.MienGiamThueCollection[0].SoVanBanMienGiam,
                        tax = Helpers.FormatNumeric(hmd.MienGiamThueCollection[0].ThueSuatTruocGiam, 4),
                        redureValue = Helpers.FormatNumeric(hmd.MienGiamThueCollection[0].TyLeMienGiam, 4)
                    };
                    customsGoodsItem.ReduceTax = new List<ReduceTax>();
                    customsGoodsItem.ReduceTax.Add(miengiam);
                }
                #endregion
                tokhai.GoodsShipment.CustomsGoodsItems.Add(customsGoodsItem);
            }
            #endregion CustomGoodsItem Danh sách hàng khai báo

            #region Supply Nguyên phụ liệu cung ứng cho tờ khai GC XK
            if (!isToKhaiNhap && tkmd.NPLCungUngs != null && tkmd.NPLCungUngs.Count > 0)
            {

                tokhai.GoodsShipment.Supplys = new List<Supply>();
                foreach (NPLCungUng nplCungUng in tkmd.NPLCungUngs)
                {
                    Supply supply = new Supply()
                    {
                        Material = new Product()
                        {
                            Commodity = new Commodity()
                            {
                                Identification = nplCungUng.MaHang,
                                Description = nplCungUng.TenHang,
                                TariffClassification = nplCungUng.MaHS.Trim()
                            }
                        }
                    };
                    if (nplCungUng.NPLCungUngDetails != null && nplCungUng.NPLCungUngDetails.Count > 0)
                    {
                        supply.SupplyItems = new List<SupplyItem>();
                        foreach (NPLCungUngDetail nplCungUngDetail in nplCungUng.NPLCungUngDetails)
                        {
                            SupplyItem supplyItem = new SupplyItem()
                            {
                                Invoice = new Invoice()
                                {
                                    Type = Helpers.FormatNumeric(nplCungUngDetail.HinhThuc),
                                    Reference = nplCungUngDetail.SoChungTu,
                                    Issue = nplCungUngDetail.NgayChungTu.ToString(sfmtDate),
                                    Issuer = nplCungUngDetail.NoiPhatHanh,
                                    NatureOfTransaction = nplCungUngDetail.MaLoaiHinh.Trim(),
                                    DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(nplCungUngDetail.MaHaiQuan.Trim()) : nplCungUngDetail.MaHaiQuan.Trim()// nplCungUngDetail.MaHaiQuan.Trim()
                                },
                                InvoiceLine = new InvoiceLine()
                                {
                                    Line = Helpers.FormatNumeric(nplCungUngDetail.DongHangTrenCT)
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    Quantity = Helpers.FormatNumeric(nplCungUngDetail.SoLuong, 3),
                                    MeasureUnit = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(nplCungUngDetail.DVT_ID) : nplCungUngDetail.DVT_ID,//nplCungUngDetail.DVT_ID.Trim(),
                                    ConversionRate = Helpers.FormatNumeric(nplCungUngDetail.TyLeQuyDoi, 3)
                                }
                            };
                            supply.SupplyItems.Add(supplyItem);
                        }
                    }
                    tokhai.GoodsShipment.Supplys.Add(supply);
                }


            }
            #endregion

            #region Danh sách các chứng từ đính kèm

            if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
            #region License Giấy phép
            {
                tokhai.License = new List<Company.KDT.SHARE.Components.License>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep in tkmd.GiayPhepCollection)
                {
                    //Company.KDT.SHARE.Components.License lic = LicenseFrom(giayPhep);
                    //tokhai.License.Add(lic);
                }
            }
            #endregion Giấy phép

            if (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0)
            #region ContractDocument  Hợp đồng thương mại
            {
                tokhai.ContractDocument = new List<Company.KDT.SHARE.Components.ContractDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai in tkmd.HopDongThuongMaiCollection)
                {
                    //Company.KDT.SHARE.Components.ContractDocument contractDocument = ContractFrom(hdThuongMai);
                    //tokhai.ContractDocument.Add(contractDocument);
                }
            }
            #endregion Hợp đồng thương mại

            if (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0)
            #region CommercialInvoice Hóa đơn thương mại
            {
                tokhai.CommercialInvoices = new List<Company.KDT.SHARE.Components.CommercialInvoice>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai in tkmd.HoaDonThuongMaiCollection)
                {
                    //Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoaDonThuongMai);
                    //tokhai.CommercialInvoices.Add(commercialInvoice);
                }
            }
            #endregion Hóa đơn thương mại

            if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
            #region CertificateOfOrigin Thêm CO
            {
                tokhai.CertificateOfOrigins = new List<Company.KDT.SHARE.Components.CertificateOfOrigin>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.CO co in tkmd.COCollection)
                {
                    //tokhai.CertificateOfOrigins.Add(CertificateOfOriginFrom(co, tkmd, isToKhaiNhap));
                }
            }
            #endregion CO


            if (tkmd.VanTaiDon != null)
            #region BillOfLadings Vận đơn
            {

                tokhai.BillOfLadings = new List<Company.KDT.SHARE.Components.BillOfLading>();
                Company.KDT.SHARE.QuanLyChungTu.VanDon vandon = tkmd.VanTaiDon;
                Company.KDT.SHARE.Components.BillOfLading billOfLading;
                if (isToKhaiNhap)
                {
                    billOfLading = new Company.KDT.SHARE.Components.BillOfLading()
                    {
                        Reference = vandon.SoVanDon,
                        Issue = vandon.NgayVanDon.ToString(sfmtDate),
                        IssueLocation = vandon.ID_NuocPhatHanh,
                        IsContainer = vandon.HangRoi ? "0" : "1",
                        TransitLocation = vandon.DiaDiemChuyenTai,
                        Category = vandon.LoaiVanDon,
                    };
                    billOfLading.BorderTransportMeans = new BorderTransportMeans()
                    {
                        Identity = vandon.SoHieuPTVT,
                        Identification = vandon.TenPTVT,
                        Journey = (vandon.LoaiVanDon == LoaiVanDon.DUONG_KHONG || vandon.LoaiVanDon == LoaiVanDon.DUONG_BIEN) ? vandon.SoHieuChuyenDi : null,
                        ModeAndType = tkmd.PTVT_ID,
                        Departure = vandon.NgayKhoiHanh.ToString(sfmtDate),
                        RegistrationNationality = (vandon.LoaiVanDon == LoaiVanDon.DUONG_KHONG && string.IsNullOrEmpty(vandon.QuocTichPTVT)) ? vandon.QuocTichPTVT.Substring(0, 2) : string.Empty
                    };
                    billOfLading.Carrier = (vandon.LoaiVanDon == LoaiVanDon.DUONG_KHONG || vandon.LoaiVanDon == LoaiVanDon.DUONG_BIEN) ? new NameBase()
                    {
                        Name = vandon.TenHangVT,
                        Identity = vandon.MaHangVT
                    } : null;
                    billOfLading.Consignment = new Consignment()
                    {
                        Consignor = new NameBase()
                        {
                            Name = vandon.TenNguoiGiaoHang,
                            Identity = vandon.MaNguoiGiaoHang
                        },
                        Consignee = new NameBase()
                        {
                            Name = vandon.TenNguoiNhanHang,
                            Identity = vandon.MaNguoiNhanHang
                        },
                        NotifyParty = (vandon.LoaiVanDon == LoaiVanDon.DUONG_BO) ? null : new NameBase()
                        {
                            Name = vandon.TenNguoiNhanHangTrungGian,
                            Identity = vandon.MaNguoiNhanHangTrungGian
                        },
                        LoadingLocation = new Company.KDT.SHARE.Components.LoadingLocation()
                        {
                            Name = vandon.TenCangXepHang,
                            Code = vandon.MaCangXepHang,
                            Loading = (vandon.LoaiVanDon == LoaiVanDon.DUONG_BO) ? string.Empty : vandon.NgayKhoiHanh.ToString(sfmtDate)
                        },
                        UnloadingLocation = new UnloadingLocation()
                        {
                            Name = vandon.TenCangDoHang,
                            Code = vandon.MaCangDoHang,
                            Arrival = vandon.NgayDenPTVT.ToString(sfmtDate)
                        },
                        DeliveryDestination = new DeliveryDestination() { Line = vandon.DiaDiemGiaoHang },
                        ConsignmentItemPackaging = new Packaging()
                        {
                            Quantity = Helpers.FormatNumeric(vandon.TongSoKien),
                            Type = vandon.LoaiKien,
                            MarkNumber = string.Empty
                        }

                    };
                    //if( vandon.ListHangOfVanDon != null && vandon.ListHangOfVanDon.Count > 0)
                    //{
                    //    billOfLading.Consignment.ConsignmentItems = new List<ConsignmentItem>();
                    //    foreach (HangVanDonDetail item in vandon.ListHangOfVanDon)
                    //    {
                    //        ConsignmentItem consigItem = new ConsignmentItem
                    //        {
                    //            Commodity = new Commodity
                    //            {
                    //                Description = item.TenHang,
                    //                TariffClassification = item.MaHS

                    //            },
                    //            GoodsMeasure = new GoodsMeasure()
                    //            {
                    //                GrossMass =  Helpers.FormatNumeric(item.TrongLuong),
                    //                NetMass = Helpers.FormatNumeric(item.TrongLuongTinh),
                    //                Quantity = Helpers.FormatNumeric(item.SoLuong),
                    //                MeasureUnit = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) : item.DVT_ID
                    //            }
                    //        };
                    //        billOfLading.Consignment.ConsignmentItems.Add(consigItem);
                    //    }
                    //}
                }
                else
                {
                    billOfLading = new Company.KDT.SHARE.Components.BillOfLading()
                    {
                        Reference = string.Empty,
                        Issue = string.Empty,
                        IssueLocation = string.Empty,
                        IsContainer = vandon.HangRoi ? "0" : "1",
                        TransitLocation = string.Empty,
                        Category = vandon.LoaiVanDon,
                    };
                    billOfLading.BorderTransportMeans = new BorderTransportMeans()
                    {
                        Identity = string.Empty,
                        Identification = string.Empty,
                        Journey = string.Empty,
                        ModeAndType = string.Empty,
                        Departure = string.Empty,
                        RegistrationNationality = string.Empty
                    };
                    billOfLading.Carrier = new NameBase()
                    {
                        Name = string.Empty,
                        Identity = string.Empty
                    };
                    billOfLading.Consignment = new Consignment()
                    {
                        Consignor = new NameBase()
                        {
                            Name = string.Empty,
                            Identity = string.Empty
                        },
                        Consignee = new NameBase()
                        {
                            Name = string.Empty,
                            Identity = string.Empty
                        },
                        NotifyParty = (vandon.LoaiVanDon != LoaiVanDon.DUONG_KHONG) ? null : new NameBase()
                        {
                            Name = string.Empty,
                            Identity = string.Empty
                        },
                        LoadingLocation = new Company.KDT.SHARE.Components.LoadingLocation()
                        {
                            Name = string.Empty,
                            Code = string.Empty,
                            Loading = string.Empty
                        },
                        UnloadingLocation = new UnloadingLocation()
                        {
                            Name = string.Empty,
                            Code = string.Empty,
                            Arrival = string.Empty
                        },
                        DeliveryDestination = new DeliveryDestination() { Line = vandon.DiaDiemGiaoHang },
                        ConsignmentItemPackaging = new Packaging()
                        {
                            Quantity = string.Empty,
                            Type = string.Empty,
                            MarkNumber = string.Empty
                        }
                    };
                }
                #region Đổ dữ liệu bên ngoài GoodsShipment
                tokhai.GoodsShipment.Consignor = billOfLading.Consignment.Consignor;
                tokhai.GoodsShipment.Consignee = billOfLading.Consignment.Consignee;
                tokhai.GoodsShipment.NotifyParty = billOfLading.Consignment.NotifyParty;
                tokhai.GoodsShipment.DeliveryDestination = billOfLading.Consignment.DeliveryDestination;
                tokhai.GoodsShipment.ExitCustomsOffice.Name = vandon.CuaKhauXuat;
                #endregion
                if (vandon.ContainerCollection != null && vandon.ContainerCollection.Count > 0)
                    billOfLading.Consignment.TransportEquipments = new List<TransportEquipment>();

                foreach (Company.KDT.SHARE.QuanLyChungTu.Container container in vandon.ContainerCollection)
                {

                    TransportEquipment transportEquipment = new TransportEquipment()
                    {
                        Characteristic = container.LoaiContainer,
                        EquipmentIdentifications = new EquipmentIdentification()
                        {
                            identification = container.SoHieu
                        },
                        Fullness = Helpers.FormatNumeric(container.Trang_thai, 0),
                        Seal = container.Seal_No,
                        GrossMass = Helpers.FormatNumeric(container.TrongLuong, 4),
                        NetMass = Helpers.FormatNumeric(container.TrongLuongNet, 4),
                        Quantity = Helpers.FormatNumeric(container.SoKien, 2),
                        PackagingLocation = container.DiaDiemDongHang
                    };

                    billOfLading.Consignment.TransportEquipments.Add(transportEquipment);
                }

                #region Hàng  trong vận đơn
                if (vandon.ListHangOfVanDon != null && vandon.ListHangOfVanDon.Count > 0)
                {
                    List<ConsignmentItem> hangsVanDon = new List<ConsignmentItem>();

                    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail hangVanDon in vandon.ListHangOfVanDon)
                    {
                        HangMauDich hangToKhai = tkmd.HMDCollection.Find(hang => hang.ID == hangVanDon.HMD_ID);
                        if (hangToKhai != null)
                            hangsVanDon.Add(new ConsignmentItem()
                            {
                                Sequence = Helpers.FormatNumeric(hangVanDon.SoThuTuHang),
                                ConsignmentItemPackaging = new Packaging()
                                {
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong),
                                    Type = hangVanDon.LoaiKien,
                                    //MarkNumber = hangVanDon.SoHieuKien,
                                },
                                Commodity = new Commodity()
                                {
                                    Description = hangVanDon.TenHang,
                                    Identification = hangToKhai.Ma,//Mã hàng
                                    TariffClassification = hangVanDon.MaHS.Trim()
                                    //TariffClassificationExtension = hangVanDon.MaHS.Trim()
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    GrossMass = Helpers.FormatNumeric(hangVanDon.TrongLuong, 4),
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong, 4),
                                    MeasureUnit = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangVanDon.DVT_ID) : hangVanDon.DVT_ID,//hangVanDon.DVT_ID.Substring(0, 3),
                                    NetMass = Helpers.FormatNumeric(hangVanDon.TrongLuongTinh, 4),
                                    CustomsValue = Helpers.FormatNumeric(hangVanDon.TriGiaKB, 4),

                                },
                                //                                 EquipmentIdentification = new EquipmentIdentification()
                                //                                 {
                                //                                     identification = hangVanDon.SoHieuContainer
                                //                                 }

                            });

                    }
                    billOfLading.Consignment.ConsignmentItems = hangsVanDon;
                }
                #endregion Hàng và Container trong vận đơn

                tokhai.BillOfLadings.Add(billOfLading);

            }

            #endregion BillOfLadings  Vận đơn

            if (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0)
            #region CustomsOfficeChangedRequest Đề nghị chuyển cửa khẩu
            {
                tokhai.CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau in tkmd.listChuyenCuaKhau)
                {
                    CustomsOfficeChangedRequest customsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCuaKhau);
                    tokhai.CustomsOfficeChangedRequest.Add(customsOfficeChangedRequest);
                };
            }
            #endregion Đề nghị chuyển cửa khẩu

            if (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0)
            #region AttachDocumentItem Chứng từ đính kèm
            {
                tokhai.AttachDocumentItem = new List<AttachDocumentItem>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem in tkmd.ChungTuKemCollection)
                {
                    AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, fileinChungtuDinhKem);
                    tokhai.AttachDocumentItem.Add(attachDocumentItem);
                };
            }
            #endregion Chứng từ đính kèm

            if (tkmd.GiayKiemTraCollection != null && tkmd.GiayKiemTraCollection.Count > 0)
            {
                #region Giấy đăng ký kiểm tra / giấy kết quả kiểm tra

                foreach (GiayKiemTra gkt in tkmd.GiayKiemTraCollection)
                {
                    if (gkt.LoaiGiay == LoaiGiayKiemTra.DANG_KY_KIEM_TRA)
                    {
                        gkt.LoadFull();
                        tokhai.ExaminationRegistration = ExamRegistration(gkt);
                    }
                    else if (gkt.LoaiGiay == LoaiGiayKiemTra.KET_QUA_KIEM_TRA)
                    {
                        gkt.LoadFull();
                        tokhai.ExaminationResult = ExaminationResult(gkt);
                    }


                }
                #endregion
            }

            if (tkmd.ChungThuGD != null && tkmd.ChungThuGD.LoaiKB == 0 && tkmd.ChungThuGD.ListHang != null && tkmd.ChungThuGD.ListHang.Count > 0)
            {
                #region Chứng thư giám định
                tkmd.ChungThuGD.LoadHang();
                CertificateOfInspection chungthu = CertificateOfInspectionFrom(tkmd.ChungThuGD);
                tokhai.CertificateOfInspection = chungthu;

                #endregion
            }


            #endregion Danh sách giấy phép XNK đi kèm

            #region AdditionalDocumentNos Thêm chứng từ nợ

            if (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0)
            {
                tokhai.AdditionalDocumentNos = new List<AdditionalDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuNo ctn in tkmd.ChungTuNoCollection)
                {
                    tokhai.AdditionalDocumentNos.Add(new AdditionalDocument()
                    {
                        Type = ctn.MA_LOAI_CT,
                        Reference = ctn.SO_CT,
                        Issue = ctn.NGAY_CT.ToString(sfmtDate),
                        IssueLocation = ctn.NOI_CAP,
                        Issuer = ctn.TO_CHUC_CAP,
                        Expire = ctn.NgayHetHan.ToString(sfmtDate),
                        IsDebt = Helpers.FormatNumeric(ctn.IsNoChungTu),
                        Submit = ctn.ThoiHanNop.ToString(sfmtDate),
                        AdditionalInformation = new AdditionalInformation()
                        {
                            Content = new Content() { Text = ctn.DIENGIAI }
                        }

                    });
                }

            }
            #endregion
            return tokhai;
        }
        #endregion

        #region Data Mapper
        private static List<Agent> AgentsFrom(ToKhaiMauDich tkmd)
        {
            List<Agent> Agents = new List<Agent>();
            // Edit by Khanhhn - 12/06/2012
            #region Agents Đại lý khai
            // Người khai hải quan
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Đại lý khai Hải Quan
            if (!string.IsNullOrEmpty(tkmd.TenDaiLyTTHQ))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDaiLyTTHQ,
                    Identity = tkmd.MaDaiLyTTHQ,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN
                });
            // ủy Thác
            if (!string.IsNullOrEmpty(tkmd.MaDonViUT))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDonViUT,
                    Identity = tkmd.MaDonViUT,
                    Status = AgentsStatus.UYTHAC,
                });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            #endregion
            return Agents;

        }
        private static Company.KDT.SHARE.Components.CertificateOfOrigin CertificateOfOriginFrom(Company.KDT.SHARE.QuanLyChungTu.CO co, ToKhaiMauDich tkmd, bool isToKhaiNhap)
        {
            #region Điền thông tin CO
            Company.KDT.SHARE.Components.CertificateOfOrigin certificateOfOrigin = new Company.KDT.SHARE.Components.CertificateOfOrigin
            {


                Reference = co.SoCO,
                Type = co.LoaiCO,
                Issuer = co.ToChucCap,
                Issue = co.NgayCO.ToString(sfmtDate),
                IssueLocation = co.NuocCapCO,
                Representative = co.NguoiKy,

                ExporterEx = isToKhaiNhap ? new NameBase() { Name = co.TenDiaChiNguoiXK, Identity = string.Empty } : new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep },
                ExportationCountryEx = new LocationNameBase { Code = co.MaNuocXKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocXKTrenCO) },

                ImporterEx = isToKhaiNhap ? new NameBase() { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep } :
                            new NameBase { Name = co.TenDiaChiNguoiNK, Identity = string.Empty },
                ImportationCountryEx = new LocationNameBase { Code = co.MaNuocNKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocNKTrenCO) },

                //LoadingLocation = new LoadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangXepHang.Trim()), Code = co.CangXepHang.Trim(), Loading = co.NgayKhoiHanh.ToString(sfmtDate) },
                //LoadingLocation = new LoadingLocation
                //{
                //    //Neu la TK Nhap, chi can lay ten Cang xep hang, khong lay thong tin tu ma
                //    Name = tkmd.MaLoaiHinh.Substring(0, 1) == "N" ? co.CangXepHang.Trim() : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangXepHang.Trim()),
                //    Code = co.CangXepHang.Trim(),
                //    Loading = co.NgayKhoiHanh.ToString(sfmtDate)
                //},

                //UnloadingLocation = new UnloadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangDoHang.Trim()), Code = co.CangDoHang.Trim() },
                //UnloadingLocation = new UnloadingLocation
                //{
                //    //Neu la TK Xuat, chi can lay ten Cang do hang, khong lay thong tin tu ma
                //    Name = tkmd.MaLoaiHinh.Substring(0, 1) == "X" ? co.TenCangDoHang.Trim() : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangDoHang.Trim()),
                //    Code = co.CangDoHang.Trim()
                //},

                IsDebt = Helpers.FormatNumeric(co.NoCo),

                Submit = co.ThoiHanNop.ToString(sfmtDate),

                Description = co.MoTaHangHoa,
                PercentOrigin = Helpers.FormatNumeric(co.HamLuongXuatXu, 4),

                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = co.ThongTinMoTaChiTiet } },
                //Hang in Co
                GoodsItems = new List<GoodsItem>(),
            };
            #endregion CO
            #region Điền hàng trong Co

            //             foreach (Company.KDT.SHARE.QuanLyChungTu.HangCoDetail hangCo in co.ListHMDofCo)
            //             {
            //                 GoodsItem goodsItem = new GoodsItem();
            //                 goodsItem.Sequence = Helpers.FormatNumeric(hangCo.SoThuTuHang);
            //                 goodsItem.StatisticalValue = Helpers.FormatNumeric(hangCo.TriGiaKB, GlobalsShare.TriGiaNT);
            //                 goodsItem.CurrencyExchange = new CurrencyExchange()
            //                 {
            //                     CurrencyType = hangCo.MaNguyenTe
            //                 };
            //                 goodsItem.ConsignmentItemPackaging = new Packaging()
            //                 {
            //                     Quantity = Helpers.FormatNumeric(hangCo.SoLuong),
            //                     Type = hangCo.LoaiKien,
            //                     MarkNumber = hangCo.SoHieuKien
            //                 };
            //                 goodsItem.Commodity = new Commodity()
            //                 {
            //                     Description = hangCo.TenHang,
            //                     Identification = hangCo.MaPhu,
            //                     TariffClassification = hangCo.MaHS.Trim()
            //                 };
            //                 goodsItem.GoodsMeasure = new GoodsMeasure()
            //                 {
            //                     GrossMass = Helpers.FormatNumeric(hangCo.TrongLuong, 3),
            //                     MeasureUnit = hangCo.DVT_ID
            //                 };
            //                 goodsItem.Origin = new Origin()
            //                 {
            //                     OriginCountry = hangCo.NuocXX_ID.Trim()
            //                 };
            //                 goodsItem.Invoice = new Invoice()
            //                 {
            //                     Reference = hangCo.SoHoaDon,
            //                     Issue = hangCo.NgayHoaDon.ToString(sfmtDate)
            //                 };
            //                 certificateOfOrigin.GoodsItems.Add(goodsItem);
            //             }
            #endregion Thêm hàng
            return certificateOfOrigin;
        }
        private static AttachDocumentItem AttachDocumentItemFrom(ToKhaiMauDich tkmd, Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem)
        {
            AttachDocumentItem attachDocumentItem = new AttachDocumentItem()
            {
                Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH,
                Sequence = Helpers.FormatNumeric(tkmd.ChungTuKemCollection.IndexOf(chungtukem) + 1, 0),
                Issue = chungtukem.NGAY_CT.ToString(sfmtDate),
                Reference = chungtukem.SO_CT,
                Description = chungtukem.DIENGIAI,
                AttachedFiles = new List<Company.KDT.SHARE.Components.AttachedFile>(),
            };

            if (chungtukem.listCTChiTiet != null && chungtukem.listCTChiTiet.Count > 0)
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet fileDetail in chungtukem.listCTChiTiet)
                {

                    attachDocumentItem.AttachedFiles.Add(new Company.KDT.SHARE.Components.AttachedFile
                    {
                        FileName = fileDetail.FileName,
                        //Content = new Content { Text = System.Convert.ToBase64String(fileDetail.NoiDung, Base64FormattingOptions.None), Base64 = "bin.base64" },
                    });
                };
            return attachDocumentItem;
        }
        private static Company.KDT.SHARE.Components.License LicenseFrom(Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep)
        {
            Company.KDT.SHARE.Components.License lic = new Company.KDT.SHARE.Components.License
            {
                Issuer = giayPhep.NguoiCap,
                Reference = giayPhep.SoGiayPhep,
                Issue = giayPhep.NgayGiayPhep.ToString(sfmtDate),
                IssueLocation = giayPhep.NoiCap,
                Type = "",
                Expire = giayPhep.NgayHetHan.ToString(sfmtDate),
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = giayPhep.ThongTinKhac } },
                GoodItems = new List<GoodsItem>()
            };

            if (giayPhep.ListHMDofGiayPhep != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail hangInGiayPhep in giayPhep.ListHMDofGiayPhep)
                {
                    lic.GoodItems.Add(new GoodsItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInGiayPhep.SoThuTuHang, 0),
                        StatisticalValue = Helpers.FormatNumeric(hangInGiayPhep.TriGiaKB, GlobalsShare.TriGiaNT),
                        CurrencyExchange = new CurrencyExchange { CurrencyType = hangInGiayPhep.MaNguyenTe },
                        Commodity = new Commodity
                        {
                            Description = hangInGiayPhep.TenHang,
                            Identification = string.Empty,
                            TariffClassification = hangInGiayPhep.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInGiayPhep.SoLuong, 3), MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangInGiayPhep.DVT_ID.Trim()) : hangInGiayPhep.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInGiayPhep.GhiChu } }

                    });
                }
            return lic;
        }
        private static Company.KDT.SHARE.Components.ContractDocument ContractFrom(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai)
        {
            Company.KDT.SHARE.Components.ContractDocument contractDocument = new Company.KDT.SHARE.Components.ContractDocument
            {
                Reference = hdThuongMai.SoHopDongTM,
                Issue = hdThuongMai.NgayHopDongTM.ToString(sfmtDate),
                Expire = hdThuongMai.ThoiHanThanhToan.ToString(sfmtDate),
                Payment = new Payment { Method = hdThuongMai.PTTT_ID },
                TradeTerm = new TradeTerm { Condition = hdThuongMai.DKGH_ID },
                DeliveryDestination = new DeliveryDestination { Line = hdThuongMai.DiaDiemGiaoHang },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hdThuongMai.NguyenTe_ID },
                TotalValue = Helpers.FormatNumeric(hdThuongMai.TongTriGia, 2),
                Buyer = new NameBase { Name = hdThuongMai.TenDonViMua, Identity = hdThuongMai.MaDonViMua },
                Seller = new NameBase { Name = hdThuongMai.TenDonViBan, Identity = hdThuongMai.MaDonViBan },
                AdditionalInformations = new List<AdditionalInformation>(),
                ContractItems = new List<ContractItem>()
            };
            contractDocument.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = hdThuongMai.ThongTinKhac } });
            if (hdThuongMai.ListHangMDOfHopDong != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail hangInHdThuongMai in hdThuongMai.ListHangMDOfHopDong)
                    contractDocument.ContractItems.Add(new ContractItem
                    {
                        unitPrice = Helpers.FormatNumeric(hangInHdThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        statisticalValue = Helpers.FormatNumeric(hangInHdThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Commodity = new Commodity
                        {
                            Description = hangInHdThuongMai.TenHang,
                            Identification = hangInHdThuongMai.MaPhu,
                            TariffClassification = hangInHdThuongMai.MaHS.Trim(),
                        },
                        Origin = new Origin { OriginCountry = hangInHdThuongMai.NuocXX_ID.Substring(0, 2) },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHdThuongMai.SoLuong, 4), MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangInHdThuongMai.DVT_ID.Trim()) : hangInHdThuongMai.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHdThuongMai.GhiChu } }
                    });
            return contractDocument;
        }
        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>DeclarationBase</returns>
        private static DeclarationBase DeclarationDocument(ToKhaiMauDich tkmd)
        {
            return new DeclarationBase()
            {
                Reference = Helpers.FormatNumeric(tkmd.SoToKhai),
                Issue = tkmd.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = tkmd.MaLoaiHinh,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };

        }
        /// <summary>
        /// Đề nghị chuyển cửa khẩu
        /// </summary>
        /// <param name="dnChuyenCuaKhau"></param>
        /// <returns></returns>
        private static CustomsOfficeChangedRequest CustomsOfficeChangedRequestFrom(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau)
        {
            CustomsOfficeChangedRequest customsOfficeChangedRequest = new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new AdditionalDocument { Reference = dnChuyenCuaKhau.SoVanDon, Issue = dnChuyenCuaKhau.NgayVanDon.ToString(sfmtDate) },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content { Text = dnChuyenCuaKhau.ThongTinKhac },
                    ExaminationPlace = dnChuyenCuaKhau.DiaDiemKiemTra.Trim().Length > 0 ? dnChuyenCuaKhau.DiaDiemKiemTra : dnChuyenCuaKhau.DiaDiemKiemTraCucHQThanhPho,
                    Time = dnChuyenCuaKhau.ThoiGianDen.ToString(sfmtDate),
                    Route = dnChuyenCuaKhau.TuyenDuong
                },
                BorderTransportMeans = new BorderTransportMeans { ModeAndType = dnChuyenCuaKhau.PTVT_ID },
            };
            return customsOfficeChangedRequest;
        }
        private static Company.KDT.SHARE.Components.CommercialInvoice CommercialInvoiceFrom(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai)
        {
            Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = new Company.KDT.SHARE.Components.CommercialInvoice
            {
                Reference = hoaDonThuongMai.SoHoaDon,
                Issue = hoaDonThuongMai.NgayHoaDon.ToString(sfmtDate),
                Seller = new NameBase { Name = hoaDonThuongMai.TenDonViBan, Identity = hoaDonThuongMai.MaDonViBan },
                Buyer = new NameBase { Name = hoaDonThuongMai.TenDonViMua, Identity = hoaDonThuongMai.MaDonViMua },
                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = "" },
                Payment = new Payment { Method = hoaDonThuongMai.PTTT_ID },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hoaDonThuongMai.NguyenTe_ID },
                TradeTerm = new TradeTerm { Condition = hoaDonThuongMai.DKGH_ID.Trim() },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()
            };

            if (hoaDonThuongMai.ListHangMDOfHoaDon != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail hangInHoaDonThuongMai in hoaDonThuongMai.ListHangMDOfHoaDon)
                    commercialInvoice.CommercialInvoiceItems.Add(new CommercialInvoiceItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoThuTuHang, 0),
                        UnitPrice = Helpers.FormatNumeric(hangInHoaDonThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        StatisticalValue = Helpers.FormatNumeric(hangInHoaDonThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Origin = new Origin { OriginCountry = hangInHoaDonThuongMai.NuocXX_ID.Substring(0, 2) },
                        Commodity = new Commodity
                        {
                            Description = hangInHoaDonThuongMai.TenHang,
                            Identification = hangInHoaDonThuongMai.MaPhu,
                            TariffClassification = hangInHoaDonThuongMai.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoLuong, 4), MeasureUnit = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hangInHoaDonThuongMai.DVT_ID.Trim()) : hangInHoaDonThuongMai.DVT_ID },
                        ValuationAdjustment = new ValuationAdjustment
                        {
                            Addition = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaTriDieuChinhTang, GlobalsShare.TriGiaNT),
                            Deduction = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaiTriDieuChinhGiam, GlobalsShare.TriGiaNT),
                        },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHoaDonThuongMai.GhiChu } },
                    });
            return commercialInvoice;
        }
        #endregion

        #region Bổ sung chứng từ
        public static BoSungChungTu ToDataTransferBoSung(ToKhaiMauDich tkmd, object NoiDungBoSung
            /*Company.KDT.SHARE.QuanLyChungTu.CO co,
            Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem,
            Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep,
            Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong,
            Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon,
            Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK*/
            )
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");

            BoSungChungTu boSungChungTuDto = new BoSungChungTu()
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                Function = DeclarationFunction.KHAI_BAO,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };
            boSungChungTuDto.Agents = AgentsFrom(tkmd);
            boSungChungTuDto.Importer = new NameBase()
            {
                Identity = tkmd.MaDoanhNghiep,
                Name = tkmd.TenDoanhNghiep
            };
            boSungChungTuDto.DeclarationDocument = DeclarationDocument(tkmd);

            if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.CO))
            #region CO
            {
                Company.KDT.SHARE.QuanLyChungTu.CO co = (Company.KDT.SHARE.QuanLyChungTu.CO)NoiDungBoSung;
                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = co.MaDoanhNghiep,
                    Name = co.TenDiaChiNguoiNK
                };
                boSungChungTuDto.Issuer = AdditionalDocumentType.EXPORT_CO_FORM_D;
                boSungChungTuDto.Reference = co.GuidStr;

                boSungChungTuDto.CertificateOfOrigins = new List<Company.KDT.SHARE.Components.CertificateOfOrigin>();
                //Company.KDT.SHARE.Components.CertificateOfOrigin certificateOfOrigin = CertificateOfOriginFrom(co, tkmd, isToKhaiNhap);
                //boSungChungTuDto.CertificateOfOrigins.Add(certificateOfOrigin);
            }
            #endregion

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GiayPhep))
            #region Giấy phép
            {

                Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = (Company.KDT.SHARE.QuanLyChungTu.GiayPhep)NoiDungBoSung;
                boSungChungTuDto.Issuer = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                boSungChungTuDto.Reference = giayphep.GuidStr;

                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = giayphep.MaDonViDuocCap,
                    Name = giayphep.TenDonViDuocCap
                };
                boSungChungTuDto.Licenses = new List<Company.KDT.SHARE.Components.License>();
                //Company.KDT.SHARE.Components.License license = LicenseFrom(giayphep);
                //boSungChungTuDto.Licenses.Add(license);
            }
            #endregion Giấy phép
            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai))
            #region Hợp đồng thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong = (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOP_DONG;
                boSungChungTuDto.Reference = hopdong.GuidStr;

                //boSungChungTuDto.ContractDocuments = new List<Company.KDT.SHARE.Components.ContractDocument>();
                //Company.KDT.SHARE.Components.ContractDocument contract = ContractFrom(hopdong);
                //boSungChungTuDto.ContractDocuments.Add(contract);
            }
            #endregion Hợp đồng thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai))
            #region Hóa đơn thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon = (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                boSungChungTuDto.Reference = hoadon.GuidStr;
                boSungChungTuDto.CommercialInvoices = new List<Company.KDT.SHARE.Components.CommercialInvoice>();
                //CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoadon);
                //boSungChungTuDto.CommercialInvoices.Add(commercialInvoice);
            }
            #endregion Hóa đơn thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau))
            #region Đề nghị chuyển cửa khẩu
            {
                Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK = (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                boSungChungTuDto.Reference = dnChuyenCK.GuidStr;
                boSungChungTuDto.NatureOfTransaction = tkmd.MaLoaiHinh.Trim();
                boSungChungTuDto.CustomsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCK);
                boSungChungTuDto.CustomsReference = tkmd.SoToKhai.ToString();
                boSungChungTuDto.Acceptance = tkmd.NgayTiepNhan.ToString(sfmtDate);
                boSungChungTuDto.DeclarationDocument = null;
                boSungChungTuDto.Agents.RemoveAt(1);
            }
            #endregion Đề nghị chuyển cửa khẩu

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.ChungTuKem))
            #region Chứng từ kèm
            {
                Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem = (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                boSungChungTuDto.Reference = chungtukem.GUIDSTR;

                boSungChungTuDto.AttachDocuments = new List<AttachDocumentItem>();
                AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, chungtukem);
                boSungChungTuDto.AttachDocuments.Add(attachDocumentItem);
            }
            #endregion Chứng từ kèm
            else if (NoiDungBoSung.GetType() == typeof(GiayNopTien))
            #region Giấy nộp tiền
            {
                Company.KDT.SHARE.QuanLyChungTu.GiayNopTien giayNopTien = (GiayNopTien)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_NOP_TIEN;
                boSungChungTuDto.Reference = giayNopTien.GuidStr;
                Receipt receipt = ReceiptFrom(giayNopTien);
                boSungChungTuDto.Receipt = receipt;
            }
            #endregion

            else if (NoiDungBoSung.GetType() == typeof(GiayKiemTra))
            {
                #region Giấy kiểm tra
                GiayKiemTra giayKT = (GiayKiemTra)NoiDungBoSung;
                boSungChungTuDto.Reference = giayKT.GuidStr;
                if (giayKT.LoaiGiay == LoaiGiayKiemTra.DANG_KY_KIEM_TRA)
                {
                    boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_DANG_KY_KT;
                    boSungChungTuDto.ExaminationRegistration = ExamRegistration(giayKT);
                }
                else if (giayKT.LoaiGiay == LoaiGiayKiemTra.KET_QUA_KIEM_TRA)
                {
                    boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_KET_QUA_KT;
                    boSungChungTuDto.ExaminationResult = ExaminationResult(giayKT);
                }
                #endregion
            }
            else if (NoiDungBoSung.GetType() == typeof(ChungThuGiamDinh))
            {
                #region Chứng thư giám định
                ChungThuGiamDinh chungthuGD = (ChungThuGiamDinh)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.CHUNG_THU_GIAM_DINH;
                boSungChungTuDto.Reference = chungthuGD.GuidStr;
                boSungChungTuDto.NatureOfTransaction = tkmd.MaLoaiHinh.Trim();
                boSungChungTuDto.CertificateOfInspection = CertificateOfInspectionFrom(chungthuGD);
                boSungChungTuDto.CustomsReference = tkmd.SoToKhai.ToString();
                boSungChungTuDto.Acceptance = tkmd.NgayTiepNhan.ToString(sfmtDate);
                #endregion
            }
            return boSungChungTuDto;
        }


        #endregion
        private static Receipt ReceiptFrom(GiayNopTien giayNopTien)
        {
            Receipt receipt = new Receipt()
            {
                Reference = giayNopTien.SoLenh,
                Issue = giayNopTien.NgayPhatLenh.ToString(sfmtDate),
                Payer = new Payer
                {
                    Name = giayNopTien.TenNguoiNop,
                    Identity = giayNopTien.SoCMNDNguoiNop,
                    AddressGNT = new DeliveryDestination { Line = giayNopTien.DiaChi }
                },
                TaxPayer = new Payer
                {
                    Name = giayNopTien.TenDonViNop,
                    Identity = giayNopTien.MaDonViNop,
                    Account = new Account
                    {
                        Number = giayNopTien.SoTKNop,
                        Bank = new NameBase { Name = giayNopTien.TenNganHangNop, Identity = giayNopTien.MaNganHangNop }
                    }
                },
                Payee = new Payer
                {
                    Name = giayNopTien.TenDonViNhan,
                    Identity = giayNopTien.MaDonViNhan,
                    Account = new Account
                    {
                        Number = giayNopTien.SoTKNhan,
                        Bank = new NameBase { Name = giayNopTien.TenNganHangNhan, Identity = giayNopTien.MaNganHangNhan }
                    }
                },
                DutyTaxFee = new List<DutyTaxFee>(),
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = giayNopTien.GhiChu } },
                AdditionalDocument = new List<AdditionalDocument>(),
            };
            foreach (GiayNopTienChiTiet chitiet in giayNopTien.ChiTietCollection)
            {
                receipt.DutyTaxFee.Add(DutyTaxFeeForGiayNopTien(chitiet));
            }
            foreach (GiayNopTienChungTu chungtu in giayNopTien.ChungTuCollection)
            {
                AdditionalDocument add = new AdditionalDocument
                {
                    Type = chungtu.LoaiChungTu,
                    Reference = chungtu.SoChungTu,
                    Name = chungtu.TenChungTu,
                    Issue = chungtu.NgayPhatHanh.ToString(sfmtDate),
                };
            }
            return receipt;
        }
        private static DutyTaxFee DutyTaxFeeForGiayNopTien(GiayNopTienChiTiet chiTietGiayNopTien)
        {
            DutyTaxFee tax = new DutyTaxFee
            {
                AdValoremTaxBase = Helpers.FormatNumeric(chiTietGiayNopTien.SoTien, 4),
                Deduct = Helpers.FormatNumeric(chiTietGiayNopTien.DieuChinhGiam, 4),
                Type = Helpers.FormatNumeric(chiTietGiayNopTien.SacThue),
                AdditionalInformations = new List<AdditionalInformation>(),
            };
            for (int i = 1; i < 6; i++)
            {
                AdditionalInformation add = new AdditionalInformation
                {
                    Content = new Content()
                };
                switch (i)
                {
                    case 1:
                        add.Statement = "211";
                        add.StatementDescription = "Chuong";
                        add.Content.Text = chiTietGiayNopTien.MaChuong;
                        break;
                    case 2:
                        add.Statement = "212";
                        add.StatementDescription = "Loai";
                        add.Content.Text = chiTietGiayNopTien.Loai;
                        break;
                    case 3:
                        add.Statement = "213";
                        add.StatementDescription = "Khoan";
                        add.Content.Text = chiTietGiayNopTien.Khoan;
                        break;
                    case 4:
                        add.Statement = "214";
                        add.StatementDescription = "Muc";
                        add.Content.Text = chiTietGiayNopTien.Muc;
                        break;
                    case 5:
                        add.Statement = "215";
                        add.StatementDescription = "Tieu Muc";
                        add.Content.Text = chiTietGiayNopTien.TieuMuc;
                        break;
                }
                tax.AdditionalInformations.Add(add);
            }

            return tax;
        }

        public static BoSungChungTu ToDataTransferBoSungChuyenTiep(ToKhaiChuyenTiep tkct, object NoiDungBoSung, string tenDoanhNghiep)
        /*
       Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem,
       Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep,
       Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong,
       Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon,
       Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK*/
        {
            bool isTokhaiCTiepNhap = tkct.MaLoaiHinh.Substring(0, 1).Equals("N");
            BoSungChungTu BoSungChungTuCT = new BoSungChungTu()
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                DeclarationOffice = tkct.MaHaiQuanTiepNhan.Trim(),



            };

            BoSungChungTuCT.Agents = AgentsFromCT(tkct, tenDoanhNghiep);
            BoSungChungTuCT.Importer = new NameBase() { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep };
            BoSungChungTuCT.DeclarationDocument = DeclarationDocumentCT(tkct);
            if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep))
            {
                #region Giấy phép tờ khai chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep giayphep = (Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep)NoiDungBoSung;
                BoSungChungTuCT.Issuer = isTokhaiCTiepNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                BoSungChungTuCT.Reference = giayphep.GuidStr;

                BoSungChungTuCT.Importer = new NameBase()
                {
                    Identity = giayphep.MaDonViDuocCap,
                    Name = giayphep.TenDonViDuocCap
                };
                BoSungChungTuCT.Licenses = new List<Company.KDT.SHARE.Components.License>();
                //License license = LicenseFromCT(giayphep);
                //BoSungChungTuCT.Licenses.Add(license);
                #endregion
            }

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong))
            {
                #region Hop Dong Thương mại tờ khai chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hopdong = (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong)NoiDungBoSung;
                BoSungChungTuCT.Issuer = AdditionalDocumentType.HOP_DONG;
                BoSungChungTuCT.Reference = hopdong.GuidStr;

                BoSungChungTuCT.ContractDocuments = new List<Company.KDT.SHARE.Components.ContractDocument>();
                //ContractDocument contract = ContractFromCT(hopdong);
                //BoSungChungTuCT.ContractDocuments.Add(contract);
                #endregion
            }
            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon))
            {
                #region Hóa đơn thương mại Tờ khai Chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon hoadon = (Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon)NoiDungBoSung;
                BoSungChungTuCT.Issuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                BoSungChungTuCT.Reference = hoadon.GuidStr;
                BoSungChungTuCT.CommercialInvoices = new List<Company.KDT.SHARE.Components.CommercialInvoice>();
                //CommercialInvoice commercialInvoice = CommercialInvoiceFromCT(hoadon);
                //BoSungChungTuCT.CommercialInvoices.Add(commercialInvoice);
                #endregion
            }
            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau))
            {
                #region Đê nghị chuyển cửa khẩu Tờ khai Chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau dnChuyenCK = (Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau)NoiDungBoSung;
                BoSungChungTuCT.Issuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                BoSungChungTuCT.Reference = dnChuyenCK.GuidStr;
                BoSungChungTuCT.NatureOfTransaction = tkct.MaLoaiHinh.Trim();
                BoSungChungTuCT.CustomsOfficeChangedRequest = CustomsOfficeChangedRequestFromCT(dnChuyenCK);
                BoSungChungTuCT.CustomsReference = tkct.SoToKhai.ToString();
                BoSungChungTuCT.Acceptance = tkct.NgayTiepNhan.ToString(sfmtDate);
                BoSungChungTuCT.DeclarationDocument = null;
                BoSungChungTuCT.Agents.RemoveAt(1);
                #endregion
            }
            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh))
            {
                #region Chứng từ kèm tờ khai chuyển tiếp
                Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh chungtukem = (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh)NoiDungBoSung;
                BoSungChungTuCT.Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                BoSungChungTuCT.Reference = chungtukem.GUIDSTR;

                BoSungChungTuCT.AttachDocuments = new List<AttachDocumentItem>();
                AttachDocumentItem attachDocumentItem = AttachDocumentItemFromCT(tkct, chungtukem);
                BoSungChungTuCT.AttachDocuments.Add(attachDocumentItem);
                #endregion
            }
            return BoSungChungTuCT;
        }

        private class DistinctMaSP : IEqualityComparer<DinhMuc>
        {
            public bool Equals(DinhMuc x, DinhMuc y)
            {
                return x.MaSanPham == y.MaSanPham;
            }

            public int GetHashCode(DinhMuc obj)
            {
                return obj.MaSanPham.GetHashCode();
            }
        }
        /// <summary>
        /// AgentsFromCT
        /// </summary>
        /// <param name="tkct"></param>
        /// <returns></returns>
        private static List<Agent> AgentsFromCT(ToKhaiChuyenTiep tkct, string tenDoanhNghiep)
        {
            List<Agent> Agents = new List<Agent>();
            // Edit by Khanhhn - 12/06/2012
            #region Agents Đại lý khai
            // Người khai hải quan
            Agents.Add(new Agent
            {
                Name = tenDoanhNghiep,
                Identity = tkct.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Đại lý khai Hải Quan
            if (!string.IsNullOrEmpty(tkct.TenDaiLyTTHQ))
                Agents.Add(new Agent
                {
                    Name = tkct.TenDaiLyTTHQ,
                    Identity = tkct.MaDaiLy,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN
                });
            // ủy Thác
            if (!string.IsNullOrEmpty(tkct.MaDonViUT))
                Agents.Add(new Agent
                {
                    Name = tkct.TenDonViUT,
                    Identity = tkct.MaDonViUT,
                    Status = AgentsStatus.UYTHAC,
                });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            Agents.Add(new Agent
            {
                Name = tenDoanhNghiep,
                Identity = tkct.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            #endregion
            return Agents;
        }
        /// <summary>
        /// DeclarationDocumentCT
        /// </summary>
        /// <param name="tkct"></param>
        /// <returns></returns>
        private static DeclarationBase DeclarationDocumentCT(ToKhaiChuyenTiep tkct)
        {
            DeclarationBase DeclarationDoc = new DeclarationBase()
            {
                Reference = Helpers.FormatNumeric(tkct.SoToKhai),
                Issue = tkct.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = tkct.MaLoaiHinh.Trim(),
                DeclarationOffice = tkct.MaHaiQuanTiepNhan.Trim()
            };
            return DeclarationDoc;
        }
        /// <summary>
        /// LicenseCT
        /// </summary>
        /// <param name="giayPhep"></param>
        /// <returns></returns>
        private static Company.KDT.SHARE.Components.License LicenseFromCT(Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep giayPhep)
        {
            Company.KDT.SHARE.Components.License lic = new Company.KDT.SHARE.Components.License
            {
                Issuer = giayPhep.NguoiCap,
                Reference = giayPhep.SoGiayPhep,
                Issue = giayPhep.NgayGiayPhep.ToString(sfmtDate),
                IssueLocation = giayPhep.NoiCap,
                Type = "",
                Expire = giayPhep.NgayHetHan.ToString(sfmtDate),
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = giayPhep.ThongTinKhac } },
                GoodItems = new List<GoodsItem>()
            };

            if (giayPhep.ListHMDofGiayPhep != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhepChiTiet hangInGiayPhep in giayPhep.ListHMDofGiayPhep)
                {
                    lic.GoodItems.Add(new GoodsItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInGiayPhep.SoThuTuHang, 0),
                        StatisticalValue = Helpers.FormatNumeric(hangInGiayPhep.TriGiaKB, GlobalsShare.TriGiaNT),
                        CurrencyExchange = new CurrencyExchange { CurrencyType = hangInGiayPhep.MaNguyenTe },
                        Commodity = new Commodity
                        {
                            Description = hangInGiayPhep.TenHang,
                            Identification = string.Empty,
                            TariffClassification = hangInGiayPhep.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInGiayPhep.SoLuong, 3), MeasureUnit = hangInGiayPhep.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInGiayPhep.GhiChu } }

                    });
                }
            return lic;
        }
        /// <summary>
        /// CertificateOfOriginCT
        /// </summary>
        /// <param name="co"></param>
        /// <param name="tkmd"></param>
        /// <param name="isTokhaiCTiepNhap"></param>
        /// <returns></returns>
        private static AttachDocumentItem AttachDocumentItemFromCT(ToKhaiChuyenTiep tkct, Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh chungtukem)
        {
            AttachDocumentItem attachDocumentItem = new AttachDocumentItem()
            {
                Issuer = "200",
                Sequence = Helpers.FormatNumeric(tkct.AnhCollection.IndexOf(chungtukem) + 1, 0),
                Issue = chungtukem.NGAY_CT.ToString(sfmtDate),
                Reference = chungtukem.SO_CT,
                Description = chungtukem.DIENGIAI,
                AttachedFiles = new List<Company.KDT.SHARE.Components.AttachedFile>(),
            };

            if (chungtukem.ListChungTuKemAnhChiTiet != null && chungtukem.ListChungTuKemAnhChiTiet.Count > 0)
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnhChiTiet fileDetail in chungtukem.ListChungTuKemAnhChiTiet)
                {

                    attachDocumentItem.AttachedFiles.Add(new Company.KDT.SHARE.Components.AttachedFile
                    {
                        FileName = fileDetail.FileName,
                        //Content = new Content { Text = System.Convert.ToBase64String(fileDetail.NoiDung, Base64FormattingOptions.None), Base64 = "bin.base64" },
                    });
                };
            return attachDocumentItem;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hdThuongMai"></param>
        /// <returns></returns>
        private static Company.KDT.SHARE.Components.ContractDocument ContractFromCT(Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hdThuongMai)
        {
            Company.KDT.SHARE.Components.ContractDocument contractDocument = new Company.KDT.SHARE.Components.ContractDocument
            {
                Reference = hdThuongMai.SoHopDongTM,
                Issue = hdThuongMai.NgayHopDongTM.ToString(sfmtDate),
                Expire = hdThuongMai.ThoiHanThanhToan.ToString(sfmtDate),
                Payment = new Payment { Method = hdThuongMai.PTTT_ID },
                TradeTerm = new TradeTerm { Condition = hdThuongMai.DKGH_ID },
                DeliveryDestination = new DeliveryDestination { Line = hdThuongMai.DiaDiemGiaoHang },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hdThuongMai.NguyenTe_ID },
                TotalValue = Helpers.FormatNumeric(hdThuongMai.TongTriGia, 2),
                Buyer = new NameBase { Name = hdThuongMai.TenDonViMua, Identity = hdThuongMai.MaDonViMua },
                Seller = new NameBase { Name = hdThuongMai.TenDonViBan, Identity = hdThuongMai.MaDonViBan },
                AdditionalInformations = new List<AdditionalInformation>(),
                ContractItems = new List<ContractItem>()
            };
            contractDocument.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = hdThuongMai.ThongTinKhac } });
            if (hdThuongMai.ListHangMDOfHopDong != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDongChiTiet hangInHdThuongMai in hdThuongMai.ListHangMDOfHopDong)
                    contractDocument.ContractItems.Add(new ContractItem
                    {
                        unitPrice = Helpers.FormatNumeric(hangInHdThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        statisticalValue = Helpers.FormatNumeric(hangInHdThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Commodity = new Commodity
                        {
                            Description = hangInHdThuongMai.TenHang,
                            Identification = hangInHdThuongMai.MaPhu,
                            TariffClassification = hangInHdThuongMai.MaHS.Trim(),
                        },
                        Origin = new Origin { OriginCountry = hangInHdThuongMai.NuocXX_ID.Substring(0, 2) },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHdThuongMai.SoLuong, 3), MeasureUnit = hangInHdThuongMai.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHdThuongMai.GhiChu } }
                    });
            return contractDocument;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hoaDonThuongMai"></param>
        /// <returns></returns>
        private static Company.KDT.SHARE.Components.CommercialInvoice CommercialInvoiceFromCT(Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon hoaDonThuongMai)
        {
            Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = new Company.KDT.SHARE.Components.CommercialInvoice
            {
                Reference = hoaDonThuongMai.SoHoaDon,
                Issue = hoaDonThuongMai.NgayHoaDon.ToString(sfmtDate),
                Seller = new NameBase { Name = hoaDonThuongMai.TenDonViBan, Identity = hoaDonThuongMai.MaDonViBan },
                Buyer = new NameBase { Name = hoaDonThuongMai.TenDonViMua, Identity = hoaDonThuongMai.MaDonViMua },
                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = "" },
                Payment = new Payment { Method = hoaDonThuongMai.PTTT_ID },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hoaDonThuongMai.NguyenTe_ID },
                TradeTerm = new TradeTerm { Condition = hoaDonThuongMai.DKGH_ID },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()
            };

            if (hoaDonThuongMai.ListHangMDOfHoaDon != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDonChiTiet hangInHoaDonThuongMai in hoaDonThuongMai.ListHangMDOfHoaDon)
                    commercialInvoice.CommercialInvoiceItems.Add(new CommercialInvoiceItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoThuTuHang, 0),
                        UnitPrice = Helpers.FormatNumeric(hangInHoaDonThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        StatisticalValue = Helpers.FormatNumeric(hangInHoaDonThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Origin = new Origin { OriginCountry = hangInHoaDonThuongMai.NuocXX_ID.Substring(0, 2) },
                        Commodity = new Commodity
                        {
                            Description = hangInHoaDonThuongMai.TenHang,
                            Identification = hangInHoaDonThuongMai.MaPhu,
                            TariffClassification = hangInHoaDonThuongMai.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoLuong, 3), MeasureUnit = hangInHoaDonThuongMai.DVT_ID },
                        ValuationAdjustment = new ValuationAdjustment
                        {
                            Addition = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaTriDieuChinhTang, 4),
                            Deduction = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaiTriDieuChinhGiam, 8),
                        },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHoaDonThuongMai.GhiChu } },
                    });
            return commercialInvoice;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dnChuyenCuaKhau"></param>
        /// <returns></returns>
        private static CustomsOfficeChangedRequest CustomsOfficeChangedRequestFromCT(Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau dnChuyenCuaKhau)
        {
            CustomsOfficeChangedRequest customsOfficeChangedRequest = new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new AdditionalDocument { Reference = dnChuyenCuaKhau.SoVanDon, Issue = dnChuyenCuaKhau.NgayVanDon.ToString(sfmtDate) },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content { Text = dnChuyenCuaKhau.ThongTinKhac },
                    ExaminationPlace = dnChuyenCuaKhau.DiaDiemKiemTra,
                    Time = dnChuyenCuaKhau.ThoiGianDen.ToString(sfmtDate),
                    Route = dnChuyenCuaKhau.TuyenDuong
                }
            };
            return customsOfficeChangedRequest;
        }

        private static ExaminationResult ExaminationResult(GiayKiemTra giaykt)
        {
            ExaminationResult Result = new ExaminationResult()
            {
                Reference = giaykt.SoGiayKiemTra,
                Issue = giaykt.NgayDangKy.ToString(sfmtDate),
                Register = new NameBase() { Identity = giaykt.MaNguoiDuocCap, Name = giaykt.TenNguoiDuocCap },
                //GoodsItem = new List<GoodsItem>()
            };
            foreach (HangGiayKiemTra item in giaykt.HangCollection)
            {

                GoodsItem itemGiayKT = new GoodsItem()
                {
                    Commodity = new Commodity()
                    {
                        Identification = item.MaHang,
                        Description = item.TenHang,
                        TariffClassification = item.MaHS.Trim()
                    },
                    Origin = new Origin { OriginCountry = item.NuocXX_ID },
                    GoodsMeasure = new GoodsMeasure()
                    {
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                        MeasureUnit = item.DVT_ID
                    },
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content { Text = item.GhiChu }
                    },
                    AdditionalDocument = (!string.IsNullOrEmpty(item.LoaiChungTu) && !string.IsNullOrEmpty(item.TenChungTu)) ? new AdditionalDocument()
                    {
                        Type = item.LoaiChungTu,
                        Reference = item.SoChungTu,
                        Name = item.TenChungTu,
                        Issue = item.NgayPhatHanh.ToString(sfmtDate)
                    } : null,
                    Examination = new Examination()
                    {
                        Place = item.DiaDiemKiemTra
                    },
                    Examiner = new NameBase()
                    {
                        Name = item.TenCoQuanKT,
                        Identity = item.MaCoQuanKT,
                        Result = item.KetQuaKT
                    }

                };
                //Result.GoodsItem.Add(itemGiayKT);
            }
            return Result;
        }

        private static ExaminationRegistration ExamRegistration(GiayKiemTra giaykt)
        {
            giaykt.LoadFull();
            ExaminationRegistration registration = new ExaminationRegistration()
            {
                Reference = giaykt.SoGiayKiemTra,
                Issue = giaykt.NgayDangKy.ToString(sfmtDate),
                Register = new NameBase() { Identity = giaykt.MaNguoiDuocCap, Name = giaykt.TenNguoiDuocCap },
            };
            foreach (HangGiayKiemTra item in giaykt.HangCollection)
            {

                GoodsItem itemGiayKT = new GoodsItem()
                {
                    Commodity = new Commodity()
                    {
                        Identification = item.MaHang,
                        Description = item.TenHang,
                        TariffClassification = item.MaHS.Trim()
                    },
                    Origin = new Origin { OriginCountry = item.NuocXX_ID },
                    GoodsMeasure = new GoodsMeasure()
                    {
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                        MeasureUnit = item.DVT_ID
                    },
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content { Text = item.GhiChu }
                    },
                    AdditionalDocument = (!string.IsNullOrEmpty(item.LoaiChungTu) && !string.IsNullOrEmpty(item.TenChungTu)) ? new AdditionalDocument()
                    {
                        Type = item.LoaiChungTu,
                        Reference = item.SoChungTu,
                        Name = item.TenChungTu,
                        Issue = item.NgayPhatHanh.ToString(sfmtDate)
                    } : null,
                    Examination = new Examination()
                    {
                        Place = item.DiaDiemKiemTra
                    },
                    Examiner = new NameBase()
                    {
                        Name = item.TenCoQuanKT,
                        Identity = item.MaCoQuanKT,
                        //Result = item.KetQuaKT
                    }

                };
                //registration.GoodsItem.Add(itemGiayKT);
            }
            return registration;
        }

        private static CertificateOfInspection CertificateOfInspectionFrom(ChungThuGiamDinh GiamDinh)
        {
            GiamDinh.LoadHang();
            CertificateOfInspection CTGiamDinh = new CertificateOfInspection()
            {
                Examination = new Examination()
                {
                    Place = GiamDinh.DiaDiem,
                    Examiner = new NameBase { Name = GiamDinh.TenCoQuanGD, Identity = GiamDinh.MaCoQuanGD },
                    ExaminePerson = new NameBase { Name = GiamDinh.CanBoGD },
                    Result = new Result { content = GiamDinh.NoiDung, resultOfExam = GiamDinh.KetQua },

                },
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = GiamDinh.ThongTinKhac } },
                //GoodsItem = new List<GoodsItem>()
            };
            foreach (HangGiamDinh item in GiamDinh.ListHang)
            {
                GoodsItem hang = new GoodsItem
                {
                    Commodity = new Commodity
                    {
                        Description = item.TenHang,
                        Identification = item.MaPhu,
                        TariffClassification = item.MaHS.Trim()
                    },
                    Origin = new Origin { OriginCountry = item.NuocXX_ID },
                    GoodsMeasure = new GoodsMeasure
                    {
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                        MeasureUnit = item.DVT_ID
                    },
                    BillOfLading = new Company.KDT.SHARE.Components.BillOfLading
                    {
                        Reference = item.SoVanTaiDon,
                        Issue = item.NgayVanDon.ToString(sfmtDate)
                    },
                    EquipmentIdentification = new EquipmentIdentification
                    {
                        identification = item.SoHieuContainer,
                        description = item.TinhTrangContainer ? "1" : "0"
                    },
                    AdditionalInformation = new AdditionalInformation
                    {
                        Content = new Content { Text = item.GhiChu }
                    }

                };
                //CTGiamDinh.GoodsItem.Add(hang);
            }
            return CTGiamDinh;
        }


        public static ToKhai ToDataTransferToKhaiCT(ToKhaiChuyenTiep tkct, string tenDoanhNghiep)
        {
            bool isToKhaiNhap = tkct.MaLoaiHinh.Substring(0, 1).Equals("N");
            bool isToKhaiSua = tkct.SoToKhai != 0 && tkct.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isToKhaiSua)
                tkct.SoTiepNhan = KhaiBaoSua.SoTNSua(
                    tkct.SoTiepNhan,
                    tkct.SoToKhai,
                    tkct.NgayDangKy.Year,
                    tkct.MaLoaiHinh,
                    GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkct.MaHaiQuanTiepNhan) : tkct.MaHaiQuanTiepNhan,
                    tkct.MaDoanhNghiep,
                    LoaiKhaiBao.ToKhaiCT);

            #region Header
            ToKhai tokhai = new ToKhai()
            {
                Issuer = isToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT,
                Reference = tkct.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                // Số tiếp nhận
                CustomsReference = isToKhaiSua ? Helpers.FormatNumeric(tkct.SoTiepNhan) : string.Empty,
                //Ngày dang ký chứng thư
                Acceptance = isToKhaiSua ? tkct.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                // Ðon vị hải quan khai báo
                DeclarationOffice = GlobalsShare.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkct.MaHaiQuanTiepNhan.Trim()) : tkct.MaHaiQuanTiepNhan.Trim(),
                // Số hàng
                GoodsItem = Helpers.FormatNumeric(tkct.HCTCollection.Count),
                LoadingList = Helpers.FormatNumeric(Helpers.GetLoading(tkct.HCTCollection.Count)),

                // Khối luợng và khối luợng tịnh - Bỏ theo nghị định 87
                //TotalGrossMass = Helpers.FormatNumeric(tkct.TrongLuong, 3),
                //TotalNetGrossMass = Helpers.FormatNumeric(tkct.TrongLuongNet, 3),
                // Mã Loại Hình
                NatureOfTransaction = tkct.MaLoaiHinh.Trim(),
                // Phuong thức thanh toán - Bỏ theo nghị định 87
                PaymentMethod = tkct.PTTT_ID,



                #region Install element
                Agents = new List<Agent>(),

                //Nguyên tệ
                CurrencyExchange = new CurrencyExchange { CurrencyType = tkct.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkct.TyGiaVND, 3) },

                //Số kiện - Bỏ theo nghị định 87
                //DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkct.SoKien) },

                // Doanh Nghiệp Xuất khẩu
                // Exporter = isToKhaiNhap ? new NameBase { Name = tkct.TenDonViDoiTac, Identity = tkct.MaKhachHang } :
                //new NameBase { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep.Trim() },
                Exporter = isToKhaiNhap ? null : new NameBase { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep },

                //Doanh nghiệp nhập khẩu
                //Importer = isToKhaiNhap ? new NameBase { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep } :
                //new NameBase { Name = tkct.TenDonViDoiTac, Identity = ""},
                Importer = isToKhaiNhap ? new NameBase { Name = tenDoanhNghiep, Identity = tkct.MaDoanhNghiep } : null,

                // Người đại diện doanh nghiệp | Tên chủ hàng
                RepresentativePerson = new RepresentativePerson { Name = tkct.DaiDienDoanhNghiep, ContactFunction = tkct.CanBoDangKy },

                // Đề xuất khác | Mã loại thông tin - Ghi chú khác
                AdditionalInformations = new List<AdditionalInformation>(),

                // GoodsShipmet Thông tin hàng hóa
                GoodsShipment = new GoodsShipment(),

                /*  License = new List<License>(),
                  ContractDocument = new List<ContractDocument>(),
                  CommercialInvoices = new List<CommercialInvoice>(),
                  CertificateOfOrigins = new List<CertificateOfOrigin>(),
                  CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                  AttachDocumentItem = new List<AttachDocumentItem>(),
                  AdditionalDocumentEx = new List<AdditionalDocument>(),
                 */
                #endregion Nrr

            };
            tokhai.AdditionalInformations.Add(new AdditionalInformation
            {
                Statement = "001",
                Content = new Content() { Text = tkct.DeXuatKhac }
            });
            if (isToKhaiSua)
                tokhai.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Statement = "005",
                    Content = new Content() { Text = tkct.LyDoSua }
                });
            #endregion Header
            #region Neu khai to khai Thu cong va dua vao TK
            if (Company.KDT.SHARE.Components.Globals.IsKTX)
            {
                if (Company.KDT.SHARE.Components.Globals.IsKhaiTK)
                {
                    if (tkct.MaLoaiHinh.Contains("V"))
                        tokhai.NatureOfTransaction = tkct.MaLoaiHinh.Substring(2, 3);
                    tokhai.CustomsReference = tkct.Huongdan_PL;
                    tokhai.DeclarationOfficeLiquidity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkct.MaHaiQuanKH.Trim());
                    tokhai.Clearance = tkct.NgayBienLai.ToString(sfmtDate);// lấy ngay biên lai khong dung để dung cho ngay dua vao thanh khoản
                    tokhai.Channel = tkct.PhanLuong;
                    tokhai.Acceptance = tkct.NgayDangKy.ToString(sfmtDateTime);
                }
                else
                {
                    //tokhai.CustomsReferenceManual = tkct.ChiTietDonViDoiTac;
                    //tokhai.Clearance = tkct.Ngay_THN_THX.ToString(sfmtDate);
                    //tokhai.Channel = tkct.PhanLuong;
                    //tokhai.Acceptance = tkct.NgayDangKy.ToString(sfmtDateTime);
                }
            }
            #endregion
            #region Agents Đại lý khai
            tokhai.Agents = AgentsFromCT(tkct, tenDoanhNghiep);
            #endregion
            #region AdditionalDocument Hợp đồng
            tokhai.AdditionalDocuments = new List<AdditionalDocument>();

            // Add AdditionalDocument (thêm hợp đồng) nhận          
            tokhai.AdditionalDocuments.Add(new AdditionalDocument
            {
                Issue = isToKhaiNhap ? tkct.NgayHDDV.ToString(sfmtDate) : tkct.NgayHDKH.ToString(sfmtDate),
                Reference = isToKhaiNhap ? tkct.SoHopDongDV : tkct.SoHDKH,
                Type = AdditionalDocumentType.HOP_DONG_NHAN,
                Name = "Hop dong",
                Expire = isToKhaiNhap ? tkct.NgayHetHanHDDV.ToString(sfmtDate) : tkct.NgayHetHanHDKH.ToString(sfmtDate)
            });
            // Add AdditionalDocument (thêm hợp đồng) giao          
            tokhai.AdditionalDocuments.Add(new AdditionalDocument
            {
                Issue = isToKhaiNhap ? tkct.NgayHDKH.ToString(sfmtDate) : tkct.NgayHDDV.ToString(sfmtDate),
                Reference = isToKhaiNhap ? tkct.SoHDKH : tkct.SoHopDongDV,
                Type = AdditionalDocumentType.HOP_DONG_GIAO,
                Name = "Hop dong",
                Expire = isToKhaiNhap ? tkct.NgayHetHanHDKH.ToString(sfmtDate) : tkct.NgayHetHanHDDV.ToString(sfmtDate)
            });
            #endregion
            #region GoodsShipment thông tin về hàng hóa
            tokhai.GoodsShipment = new GoodsShipment()
            {
                //- Bỏ theo nghị định 87
                //ImportationCountry = isToKhaiNhap ? "VN" : tkct.NuocNK_ID.Substring(0, 2),
                //ExportationCountry = isToKhaiNhap ? tkct.NuocXK_ID.Substring(0, 2) : "VN",
                Consignor = isToKhaiNhap ? new NameBase { Identity = tkct.MaKhachHang, Name = tkct.NguoiChiDinhKH } : new NameBase { Identity = tkct.MaDoanhNghiep, Name = tenDoanhNghiep },
                Consignee = isToKhaiNhap ? new NameBase { Identity = tkct.MaDoanhNghiep, Name = tenDoanhNghiep } : new NameBase { Identity = tkct.MaKhachHang, Name = tkct.NguoiChiDinhKH },
                NotifyParty = new NameBase { Identity = tkct.NguoiChiDinhDV, Name = tkct.NguoiChiDinhKH },
                DeliveryDestination = new DeliveryDestination { Line = tkct.DiaDiemXepHang, Time = tkct.ThoiGianGiaoHang.ToString(sfmtDate) },
                //- Bỏ theo nghị định 87
                //EntryCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? tkct.CuaKhau_ID : string.Empty, Name = isToKhaiNhap ? DuLieuChuan.CuaKhau.GetName(tkct.CuaKhau_ID.Trim()) : string.Empty },
                //ExitCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? string.Empty : tkct.CuaKhau_ID, Name = isToKhaiNhap ? string.Empty : DuLieuChuan.CuaKhau.GetName(tkct.CuaKhau_ID.Trim()) },
                Importer = isToKhaiNhap ? null : new NameBase { Name = tkct.TenDonViDoiTac, Identity = string.Empty },
                Exporter = isToKhaiNhap ? new NameBase { Name = tkct.TenDonViDoiTac, Identity = string.Empty } : null,
                //Exporter = new NameBase { Name = tkct.TenDonViDoiTac, Identity = tkct.MaKhachHang },
                //Importer = new NameBase { Name = tkct.TenDonViDoiTac, Identity = tkct.MaKhachHang },
                TradeTerm = new TradeTerm { Condition = tkct.DKGH_ID },//- Bỏ theo nghị định 87
                //CustomsGoodsItems = new List<CustomsGoodsItem>()
            };
            if (tkct.PhuLucHDCollection == null)
                tkct.LoadPhuLucHD();
            if (tkct.PhuLucHDCollection != null && tkct.PhuLucHDCollection.Count > 0)
                tokhai.GoodsShipment.SubContructReference = new SubContructReference()
                {
                    Reference = tkct.PhuLucHDCollection[0].SoPhuLuc,
                    Issue = tkct.PhuLucHDCollection[0].NgayPhuLuc.ToString(sfmtDate),
                    Expire = tkct.PhuLucHDCollection[0].NgayHetHan.ToString(sfmtDate)
                };
            #endregion GoodsShipment
            #region CustomGoodsItem Danh sách hàng khai báo

            //Add CustomGoodsItem
            int soluonghang = 0;
            if (tkct.HCTCollection != null)
            {
                soluonghang = tkct.HCTCollection.Count;
                tokhai.GoodsShipment.CustomsGoodsItems = new List<CustomsGoodsItem>();
            }
            for (int i = 0; i < soluonghang; i++)
            {
                HangChuyenTiep hct = tkct.HCTCollection[i];
                #region CustomsGoodsItem
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem
                {

                    CustomsValue = Helpers.FormatNumeric(hct.TriGia, GlobalsShare.TriGiaNT),
                    Sequence = Helpers.FormatNumeric(hct.SoThuTuHang),
                    StatisticalValue = Helpers.FormatNumeric(hct.TriGiaKB_VND, GlobalsShare.DonGiaNT),
                    UnitPrice = Helpers.FormatNumeric(hct.DonGia, GlobalsShare.DonGiaNT),
                    StatisticalUnitPrice = Helpers.FormatNumeric(hct.DonGiaTT, GlobalsShare.DonGiaNT),
                    Manufacturer = new NameBase { Name = hct.TenHangSX/*Tên hãng sx*/, Identity = hct.MaHangSX/*Mã hãng sx*/ },
                    Origin = new Origin { OriginCountry = hct.ID_NuocXX.Substring(0, 2) },
                    GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hct.SoLuong, 6), MeasureUnit = hct.ID_DVT, ConversionRate = "1" }

                };


                customsGoodsItem.CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = Helpers.FormatNumeric(0, 6),
                    FreightCharge = Helpers.FormatNumeric(0, 6),
                    Method = string.Empty,
                    OtherChargeDeduction = Helpers.FormatNumeric(0, 6)
                };

                #endregion

                #region Commodity Hàng trong tờ khai
                Commodity commodity = new Commodity
                {
                    Description = hct.TenHang,
                    Identification = hct.MaHang,
                    TariffClassification = hct.MaHS.Trim(),
                    TariffClassificationExtension = hct.MaHSMoRong,
                    Brand = hct.NhanHieu,
                    Grade = hct.QuyCachPhamChat,
                    Ingredients = hct.ThanhPhan,
                    ModelNumber = hct.Model,
                    DutyTaxFee = new List<DutyTaxFee>(),
                    //Type = isToKhaiNhap ? "1" : "2",
                    //Hệ thống hải quan chưa sử dụng
                    InvoiceLine = new InvoiceLine { ItemCharge = string.Empty, Line = string.Empty }
                };
                if (tkct.LoaiHangHoa == "N")
                    commodity.Type = "1";
                else if (tkct.LoaiHangHoa == "S")
                    commodity.Type = "2";
                else if (tkct.LoaiHangHoa == "T")
                    commodity.Type = "3";
                else if (tkct.LoaiHangHoa == "H")
                    commodity.Type = "4";

                #region DutyTaxFee tiền thuế của hàng
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueXNK, 2),
                    DutyRegime = string.Empty,
                    SpecificTaxBase = string.Empty,
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueSuatXNK, 1),
                    Type = DutyTaxFeeType.THUE_XNK
                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueGTGT, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueSuatGTGT, 1),
                    Type = DutyTaxFeeType.THUE_VAT,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueTTDB, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueSuatTTDB, 1),
                    Type = DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueXNK, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].ThueSuatXNK, 1),
                    Type = DutyTaxFeeType.THUE_KHAC,


                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkct.HCTCollection[i].PhuThu, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkct.HCTCollection[i].TyLeThuKhac, 1),
                    Type = DutyTaxFeeType.THUE_CHENH_LECH_GIA,

                });
                #endregion DutyTaxFee thuế

                customsGoodsItem.Commodity = commodity;
                #endregion Hàng chính

                #region AdditionalDocument if Search(Giấy phép có hàng trong tờ khai)
                //Lấy thông tin giấy phép đầu tiên
                if (tkct.GiayPhepCollection != null && tkct.GiayPhepCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep giayphep = tkct.GiayPhepCollection[0];
                    customsGoodsItem.AdditionalDocument = new AdditionalDocument()
                    {
                        Issue = giayphep.NgayGiayPhep.ToString(sfmtDate), /*Ngày cấp giấy phép yyyy-MM-dd*/
                        Issuer = giayphep.NguoiCap/*Người cấp*/,
                        IssueLocation = giayphep.NoiCap/*Nơi cấp*/,
                        Reference = giayphep.SoGiayPhep/*Số giấy phép*/,
                        Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                        Name = "GP"/* Tên giấy phép*/,
                        Expire = giayphep.NgayHetHan.ToString(sfmtDate)
                    };
                }
                #endregion

                tokhai.GoodsShipment.CustomsGoodsItems.Add(customsGoodsItem);
            }
            #endregion CustomGoodsItem Danh sách hàng khai báo
            #region Licenses Danh sách giấy phép XNK đi kèm

            if (tkct.GiayPhepCollection != null && tkct.GiayPhepCollection.Count > 0)
            #region License Giấy phép
            {
                tokhai.License = new List<Company.KDT.SHARE.Components.License>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep giayPhep in tkct.GiayPhepCollection)
                {
                    Company.KDT.SHARE.Components.License lic = LicenseFromCT(giayPhep);
                    tokhai.License.Add(lic);
                }
            }
            #endregion Giấy phép

            if (tkct.HopDongThuongMaiCollection != null && tkct.HopDongThuongMaiCollection.Count > 0)
            #region ContractDocument  Hợp đồng thương mại
            {
                tokhai.ContractDocument = new List<Company.KDT.SHARE.Components.ContractDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hdThuongMai in tkct.HopDongThuongMaiCollection)
                {
                    Company.KDT.SHARE.Components.ContractDocument contractDocument = ContractFromCT(hdThuongMai);
                    tokhai.ContractDocument.Add(contractDocument);
                }
            }
            #endregion Hợp đồng thương mại

            if (tkct.HoaDonThuongMaiCollection != null && tkct.HoaDonThuongMaiCollection.Count > 0)
            #region CommercialInvoice Hóa đơn thương mại
            {
                tokhai.CommercialInvoices = new List<Company.KDT.SHARE.Components.CommercialInvoice>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon hoaDonThuongMai in tkct.HoaDonThuongMaiCollection)
                {
                    Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = CommercialInvoiceFromCT(hoaDonThuongMai);
                    tokhai.CommercialInvoices.Add(commercialInvoice);
                }
            }
            #endregion Hóa đơn thương mại

            if (tkct.DeNghiChuyenCuaKhau != null && tkct.DeNghiChuyenCuaKhau.Count > 0)
            #region CustomsOfficeChangedRequest Đề nghị chuyển cửa khẩu
            {
                tokhai.CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau dnChuyenCuaKhau in tkct.DeNghiChuyenCuaKhau)
                {
                    CustomsOfficeChangedRequest customsOfficeChangedRequest = CustomsOfficeChangedRequestFromCT(dnChuyenCuaKhau);
                    tokhai.CustomsOfficeChangedRequest.Add(customsOfficeChangedRequest);
                };
            }
            #endregion Đề nghị chuyển cửa khẩu

            if (tkct.AnhCollection != null && tkct.AnhCollection.Count > 0)
            #region AttachDocumentItem Chứng từ đính kèm
            {
                tokhai.AttachDocumentItem = new List<AttachDocumentItem>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh fileinChungtuDinhKem in tkct.AnhCollection)
                {
                    AttachDocumentItem attachDocumentItem = AttachDocumentItemFromCT(tkct, fileinChungtuDinhKem);
                    tokhai.AttachDocumentItem.Add(attachDocumentItem);
                };
            }
            #endregion Chứng từ đính kèm

            #endregion Danh sách giấy phép XNK đi kèm
            #region Supply Nguyên phụ liệu cung ứng cho tờ khai GC chuyển tiếp XK
            if (!isToKhaiNhap && tkct.NPLCungUngs != null && tkct.NPLCungUngs.Count > 0)
            {

                tokhai.GoodsShipment.Supplys = new List<Supply>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng nplCungUng in tkct.NPLCungUngs)
                {
                    Supply supply = new Supply()
                    {
                        Material = new Product()
                        {
                            Commodity = new Commodity()
                            {
                                Identification = nplCungUng.MaHang,
                                Description = nplCungUng.TenHang,
                                TariffClassification = nplCungUng.MaHS.Trim()
                            }
                        }
                    };
                    if (nplCungUng.NPLCungUngDetails != null && nplCungUng.NPLCungUngDetails.Count > 0)
                    {
                        supply.SupplyItems = new List<SupplyItem>();
                        foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail nplCungUngDetail in nplCungUng.NPLCungUngDetails)
                        {
                            SupplyItem supplyItem = new SupplyItem()
                            {
                                Invoice = new Invoice()
                                {
                                    Type = Helpers.FormatNumeric(nplCungUngDetail),
                                    Reference = nplCungUngDetail.SoChungTu,
                                    Issue = nplCungUngDetail.NgayChungTu.ToString(sfmtDate),
                                    Issuer = nplCungUngDetail.NoiPhatHanh,
                                    NatureOfTransaction = nplCungUngDetail.MaLoaiHinh.Trim(),
                                    DeclarationOffice = nplCungUngDetail.MaHaiQuan.Trim()
                                },
                                InvoiceLine = new InvoiceLine()
                                {
                                    Line = Helpers.FormatNumeric(nplCungUngDetail.DongHangTrenCT)
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    Quantity = Helpers.FormatNumeric(nplCungUngDetail.SoLuong, 3),
                                    MeasureUnit = nplCungUngDetail.DVT_ID.Trim(),
                                    ConversionRate = Helpers.FormatNumeric(nplCungUngDetail.TyLeQuyDoi, 3)
                                }
                            };
                            supply.SupplyItems.Add(supplyItem);
                        }
                    }
                    tokhai.GoodsShipment.Supplys.Add(supply);
                }


            }
            #endregion
            #region AdditionalDocumentNos Thêm chứng từ nợ

            if (tkct.ChungTuNoCollection != null && tkct.ChungTuNoCollection.Count > 0)
            {
                tokhai.AdditionalDocumentNos = new List<AdditionalDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuNo ctn in tkct.ChungTuNoCollection)
                {
                    tokhai.AdditionalDocumentNos.Add(new AdditionalDocument()
                    {
                        Type = ctn.MA_LOAI_CT,
                        Reference = ctn.SO_CT,
                        Issue = ctn.NGAY_CT.ToString(sfmtDate),
                        IssueLocation = ctn.NOI_CAP,
                        Issuer = ctn.TO_CHUC_CAP,
                        Expire = ctn.NgayHetHan.ToString(sfmtDate),
                        IsDebt = Helpers.FormatNumeric(ctn.IsNoChungTu),
                        Submit = ctn.ThoiHanNop.ToString(sfmtDate),
                        AdditionalInformation = new AdditionalInformation()
                        {
                            Content = new Content() { Text = ctn.DIENGIAI }
                        }

                    });
                }

            }
            #endregion
            return tokhai;
        }

        public static GC_GiaHanThanhKhoan ToDataTransferGiaHanThanhKhoan(GiaHanThanhKhoan giaHanThanhKhoan, HopDong HD)
        {
            bool isKhaiBaoSua = giaHanThanhKhoan.ActionStatus == (int)ActionStatus.GiaHanSua;
            GC_GiaHanThanhKhoan gc_GiaHanHD = new GC_GiaHanThanhKhoan()
            {
                Issuer = DeclarationIssuer.GC_GIA_HAN_THANH_KHOAN,
                Function = isKhaiBaoSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                Reference = giaHanThanhKhoan.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                DeclarationOffice = giaHanThanhKhoan.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = isKhaiBaoSua ? Helpers.FormatNumeric(giaHanThanhKhoan.SoTiepNhan) : string.Empty,
                Acceptance = isKhaiBaoSua ? giaHanThanhKhoan.NgayTiepNhan.ToString(sfmtDate) : string.Empty,
                Agents = new List<Agent>(),
                Importer = new NameBase() { Identity = giaHanThanhKhoan.MaDoanhNghiep, Name = HD.TenDoanhNghiep },
                ContractReference = new Company.KDT.SHARE.Components.ContractDocument()
                {
                    Reference = HD.SoHopDong,
                    Issue = HD.NgayTiepNhan.ToString(sfmtDate),
                    DeclarationOffice = HD.MaHaiQuan.Trim(),
                    Expire = HD.NgayHetHan.ToString(sfmtDate),
                    CustomsReference = HD.SoHopDong
                },

                AdditionalInformations = new List<AdditionalInformation>(),

            };
            gc_GiaHanHD.Agents.Add(new Agent
            {
                Name = HD.TenDoanhNghiep,
                Identity = HD.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            gc_GiaHanHD.Agents.Add(new Agent
            {
                Name = HD.TenDoanhNghiep,
                Identity = HD.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            gc_GiaHanHD.AdditionalInformations.Add(new AdditionalInformation()
            {
                ExtensionNumberDate = Helpers.FormatNumeric(giaHanThanhKhoan.SoNgayGiaHan),
                Content = new Content() { Text = giaHanThanhKhoan.LyDo }
            });
            return gc_GiaHanHD;
        }
        public static PhuKienToKhai ToDataTransferPhuKienToKhai(KDT_SXXK_PhuKienDangKy pkdk, string TenDoanhNghiep)
        {
            PhuKienToKhai phukien = new PhuKienToKhai()
            {
                Issuer = DeclarationIssuer.PHUKIEN_TOKHAI,
                Function = DeclarationFunction.KHAI_BAO,
                Reference = pkdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                DeclarationOffice = pkdk.MaHaiQuan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = pkdk.SoToKhai, //isKhaiSua ? Helpers.FormatNumeric(pkdk.SoTiepNhan) : string.Empty,
                Acceptance = pkdk.NgayDangKy.ToString(sfmtDate), //isKhaiSua ? pkdk.NgayTiepNhan.ToString(                ) : string.Empty,
                NatureOfTransaction = pkdk.MaLoaiHinh,
                Importer = new NameBase() { Identity = pkdk.MaDoanhNghiep, Name = TenDoanhNghiep },
                Agents = new List<Agent>(),

            };
            // Người Khai Hải Quan
            phukien.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = pkdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            //             phukien.Agents.Add(new Agent
            //             {
            //                 Name = hopdong.TenDoanhNghiep,
            //                 Identity = hopdong.MaDoanhNghiep,
            //                 Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            //             });
            //             phukien.Importer = new NameBase()
            //             {
            //                 Identity = hopdong.MaDoanhNghiep,
            //                 Name = hopdong.TenDoanhNghiep
            //             };
            phukien.SubContract = new Subcontract()
            {
                Reference = pkdk.SoPhuKien,
                Decription = pkdk.GhiChu,
                Issue = pkdk.NgayPhuKien.ToString(sfmtDate)
            };
            phukien.AdditionalInformations = new List<PhuKienToKhai_AdditionalInformation>();
            foreach (KDT_SXXK_LoaiPhuKien loaiPK in pkdk.ListPhuKien)
            {
                //if (loaiPK. == null || loaiPK.HPKCollection.Count == 0)
                //    loaiPK.LoadCollection();
                PhuKienToKhai_AdditionalInformation additionalInformation = new PhuKienToKhai_AdditionalInformation()
                {
                    Statement = loaiPK.MaPhuKien.Trim()
                };
                switch (loaiPK.MaPhuKien.Trim())
                {
                    #region Phụ kiện sửa thông tin ngày đưa vào thanh khoản (501)
                    case "501":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                            {
                                Declaration = new DeclarationPhuKienToKhai()
                                {
                                    NewClearance = loaiPK.ListPhuKienDeTail[0].NgayTKSuaDoi.ToString(sfmtDate)
                                }
                            };
                            if (loaiPK.ListPhuKienDeTail[0].NgayTKHeThong.Year > 2000)
                            {
                                additionalInformation.Content.Declaration.OldClearance = loaiPK.ListPhuKienDeTail[0].NgayTKHeThong.ToString(sfmtDate);
                            }
                            break;
                        }
                    #endregion

                    #region Phụ kiện sửa thông tin hợp đồng của tờ khai(502)
                    case "502":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                            {
                                Declaration = new DeclarationPhuKienToKhai()
                                {
                                    //minhnd
                                    OldContractDocument = new DeclarationContract()
                                    {
                                        OldIssue = loaiPK.ListPhuKienDeTail[0].NgayHopDongCu.ToString(sfmtDate),
                                        OldReference = loaiPK.ListPhuKienDeTail[0].SoHopDongCu
                                    },
                                    //minhnd

                                    NewContractDocument = new DeclarationContract()
                                    {
                                        NewIssue = loaiPK.ListPhuKienDeTail[0].NgayHopDongMoi.ToString(sfmtDate),
                                        NewReference = loaiPK.ListPhuKienDeTail[0].SoHopDongMoi
                                    }
                                    
                                }
                            };
                            if (!string.IsNullOrEmpty(loaiPK.ListPhuKienDeTail[0].SoHopDongCu))
                            {
                                additionalInformation.Content.Declaration.OldContractDocument = new DeclarationContract()
                                {
                                    OldReference = loaiPK.ListPhuKienDeTail[0].SoHopDongCu,
                                    OldIssue = loaiPK.ListPhuKienDeTail[0].NgayHopDongCu.ToString(sfmtDate),
                                    
                                };
                                //minhnd Thêm hợp đồng mới
                                additionalInformation.Content.Declaration.NewContractDocument = new DeclarationContract()
                                {
                                    NewReference = loaiPK.ListPhuKienDeTail[0].SoHopDongMoi,
                                    NewIssue = loaiPK.ListPhuKienDeTail[0].NgayHopDongMoi.ToString(sfmtDate),

                                };
                                //minhnd Thêm hợp đồng mới

                            }
                            break;
                        }
                    #endregion

                    #region Phụ kiện sửa thông tin mã hàng của tờ khai(503)
                    case "503":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                            {
                                Declaration = new DeclarationPhuKienToKhai()
                                {
                                    GoodsShipment = new PhuKienGoodsShipment() { CustomsGoodsItem = new List<PhuKienCustomsGoodsItem>() }
                                }
                            };
                            foreach (KDT_SXXK_PhuKienDetail item in loaiPK.ListPhuKienDeTail)
                            {
                                PhuKienCustomsGoodsItem pkdetail = new PhuKienCustomsGoodsItem
                                {
                                    sequence = item.STTHang.ToString(),
                                    NewCommodity = new PhuKien_NewCommodity()
                                    {
                                        NewIdentification = item.MaHangMoi.Trim(),
                                        NewRegisterCustoms = item.MaHQDangKyMoi.Trim(),
                                        NewType = item.LoaiHangMoi.Trim()
                                    }
                                };
                                if (!string.IsNullOrEmpty(item.MaHangCu.Trim()))
                                {
                                    pkdetail.OldCommodity = new PhuKien_OldCommodity
                                    {
                                        OldIdentification = item.MaHangCu.Trim(),
                                        OldRegisterCustoms = item.MaHQDangKyCu.Trim(),
                                        //OldRegisterCustoms = item.MaHQDangKyCu,
                                        OldType = item.LoaiHangCu.Trim()
                                    };
                                }

                                additionalInformation.Content.Declaration.GoodsShipment.CustomsGoodsItem.Add(pkdetail);
                            }

                            break;
                        }
                    #endregion

                    #region Phụ kiện sửa thông tin số lượng hàng của tờ khai(504)
                    case "504":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                            {
                                Declaration = new DeclarationPhuKienToKhai()
                                {
                                    GoodsShipment = new PhuKienGoodsShipment() { CustomsGoodsItem = new List<PhuKienCustomsGoodsItem>() }
                                }
                            };
                            foreach (KDT_SXXK_PhuKienDetail item in loaiPK.ListPhuKienDeTail)
                            {
                                PhuKienCustomsGoodsItem pkdetail = new PhuKienCustomsGoodsItem
                                {
                                    sequence = item.STTHang.ToString(),
                                    Commodity = new PhuKien_Commodity()
                                    {
                                        Identification = item.MaHangCu.Trim(),

                                    },
                                    GoodsMeasure = new PhuKien_GoodsMeasure()
                                    {
                                        OldQuantity = Helpers.FormatNumeric(item.SoLuongCu, 4),
                                        NewQuantity = Helpers.FormatNumeric(item.SoLuongMoi, 4)
                                    }
                                };
                                additionalInformation.Content.Declaration.GoodsShipment.CustomsGoodsItem.Add(pkdetail);
                            }

                            break;
                        }
                    #endregion

                    #region Phụ kiện sửa thông tin số lượng hàng của tờ khai(505)
                    case "505":
                        {
                            additionalInformation.Content = new PhuKienToKhai_Content
                            {
                                Declaration = new DeclarationPhuKienToKhai()
                                {
                                    GoodsShipment = new PhuKienGoodsShipment() { CustomsGoodsItem = new List<PhuKienCustomsGoodsItem>() }
                                }
                            };
                            foreach (KDT_SXXK_PhuKienDetail item in loaiPK.ListPhuKienDeTail)
                            {
                                PhuKienCustomsGoodsItem pkdetail = new PhuKienCustomsGoodsItem
                                {
                                    sequence = item.STTHang.ToString(),
                                    Commodity = new PhuKien_Commodity()
                                    {
                                        Identification = item.MaHangCu.Trim(),

                                    },
                                    GoodsMeasure = new PhuKien_GoodsMeasure()
                                    {
                                        OldMeasureUnit = item.DVTCu.Trim(),
                                        NewMeasureUnit = item.DVTMoi.Trim()
                                    }
                                };
                                additionalInformation.Content.Declaration.GoodsShipment.CustomsGoodsItem.Add(pkdetail);
                            }

                            break;
                        }
                    #endregion
                    default:
                        break;
                }
                phukien.AdditionalInformations.Add(additionalInformation);
            }
            return phukien;
        }
        public static GC_NPLCungUng ToDataTransferTuCungUng(KDT_GC_CungUngDangKy cudk, string TenDoanhNghiep)
        {
            GC_NPLCungUng tuCungUng = new GC_NPLCungUng()
            {

                Issuer = DeclarationIssuer.GC_TUCUNGUNG,
                Function = DeclarationFunction.KHAI_BAO,
                Reference = cudk.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                DeclarationOffice = cudk.MaHQ.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                // NatureOfTransaction = cudk.MaLoaiHinh,
                Importer = new NameBase()
                {
                    Identity = cudk.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
                ContractDocument = new Company.KDT.SHARE.Components.ContractDocument()
                {
                    Reference = cudk.SoHopDong,
                    Issue = cudk.NgayHopDong.ToString(sfmtDate),
                    Expire = cudk.NgayHetHan.ToString(sfmtDate),
                },

            };
            // Người Khai Hải Quan
            tuCungUng.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = cudk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            GC_Materials materials = new GC_Materials() { Material = new List<GC_Material>() };
            foreach (KDT_GC_CungUng cungung in cudk.NPLCungUng_List)
            {
                GC_Material material = new GC_Material()
                {

                    Commodity = new Commodity()
                    {
                        Description = cungung.TenNPL,
                        Identification = cungung.MaNPL,
                        TariffClassification = cungung.MaHS
                    },
                    GoodsMeasure = new GoodsMeasure()
                    {
                        MeasureUnit = cungung.DVT_ID
                    }

                };
                GC_DeclarationDocuments declarationDocuments = new GC_DeclarationDocuments() { DeclarationDocument = new List<GC_DeclarationDocument>() };

                foreach (KDT_GC_CungUng_Detail cungungdetail in cungung.CungUngDetail_List)
                {

                    bool isTrungToKhai = false;
                    if (declarationDocuments.DeclarationDocument != null || declarationDocuments.DeclarationDocument.Count > 0)
                    {
                        foreach (GC_DeclarationDocument tk in declarationDocuments.DeclarationDocument)
                        {
                            if (tk.CustomsReference.Trim() == cungungdetail.SoToKhaiVNACCS.ToString().Trim())
                            {
                                string tempMaLoaiHinh = cungungdetail.MaLoaiHinh.Contains("V") ? cungungdetail.MaLoaiHinh.Trim().Substring(2) : cungungdetail.MaLoaiHinh.Trim();
                                if (tk.NatureOfTransaction.Trim() == tempMaLoaiHinh.Trim())
                                {
                                    isTrungToKhai = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (isTrungToKhai)
                        continue;
                    List<KDT_GC_CungUng_Detail> listSp = new List<KDT_GC_CungUng_Detail>();
                    //if(item.MaLoaiHinh.Contains("V"))
                    listSp = cungung.CungUngDetail_List.FindAll(x => x.SoToKhaiXuat == cungungdetail.SoToKhaiXuat && x.MaLoaiHinh.Trim() == cungungdetail.MaLoaiHinh.Trim() && x.Master_ID == cungungdetail.Master_ID);

                    GC_DeclarationDocument declarationDocument = new GC_DeclarationDocument() { Product = new List<GC_Product>() };

                    declarationDocument.CustomsReference = cungungdetail.SoToKhaiVNACCS;
                    declarationDocument.NatureOfTransaction = cungungdetail.MaLoaiHinh.Contains("V") ? cungungdetail.MaLoaiHinh.Trim().Substring(2) : cungungdetail.MaLoaiHinh.Trim();
                    declarationDocument.Acceptance = cungungdetail.NgayDangKy.ToString(sfmtDateTime);
                    declarationDocument.DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(cungungdetail.MaHaiQuan);

                    foreach (KDT_GC_CungUng_Detail item in listSp)
                    {
                        GC_Product product = new GC_Product() { PaymentDocuments = new List<GC_PaymentDocuments>() };
                        product.Commodity = new Commodity()
                            {
                                Description = item.TenSanPham,
                                Identification = item.MaSanPham
                            };
                        product.GoodsMeasure = new GoodsMeasure() { MeasureUnit = item.DVT_ID };
                        foreach (KDT_GC_CungUng_ChungTu chungtu in item.CungUngCT_List)
                        {
                            GC_PaymentDocuments paymentDocuments = new GC_PaymentDocuments() { PaymentDocument = new GC_PaymentDocument() };
                            paymentDocuments.PaymentDocument.Reference = chungtu.SoChungTu;
                            paymentDocuments.PaymentDocument.Issue = chungtu.NgayChungTu.ToString(sfmtDate);
                            paymentDocuments.PaymentDocument.Type = chungtu.LoaiChungTu.ToString();
                            paymentDocuments.PaymentDocument.IssueLocation = chungtu.NoiCap;
                            paymentDocuments.PaymentDocument.Quantity = Helpers.FormatNumeric(chungtu.SoLuong, 4);
                            product.PaymentDocuments.Add(paymentDocuments);
                        }
                        declarationDocument.Product.Add(product);
                    }

                    declarationDocuments.DeclarationDocument.Add(declarationDocument);
                }
                material.DeclarationDocuments = declarationDocuments;
                materials.Material.Add(material);
            }
            tuCungUng.Materials = materials;

            return tuCungUng;
        }
        public static GC_TKTaiXuat ToDataTransferTaiXuat(KDT_GC_TaiXuatDangKy txdk, string TenDoanhNghiep)
        {
            GC_TKTaiXuat taiXuat = new GC_TKTaiXuat()
            {
                Issuer = DeclarationIssuer.GC_TaiXuat,
                Function = DeclarationFunction.KHAI_BAO,
                Reference = txdk.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                DeclarationOffice = txdk.MaHQ.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                // NatureOfTransaction = cudk.MaLoaiHinh,
                Importer = new NameBase()
                {
                    Identity = txdk.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
                contractReference = new Company.KDT.SHARE.Components.ContractDocument()
                {
                    Reference = txdk.SoHopDong,
                    Issue = txdk.NgayHopDong.ToString(sfmtDate),
                    Expire = txdk.NgayHetHan.ToString(sfmtDate),
                },
                materialReExport = new GC_DeclarationDocument(),
            };
            // Người Khai Hải Quan
            taiXuat.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = txdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            taiXuat.materialReExport = new GC_DeclarationDocument();
            GC_DeclarationDocument materialReExport = new GC_DeclarationDocument() { exportDeclarationDocuments = new List<ExportDeclarationDocument>() };
            foreach (KDT_GC_TaiXuat tx_detail in txdk.TaiXuat_List)
            {
                string tempMaLoaiHinh = tx_detail.MaLoaiHinh.Contains("V") ? tx_detail.MaLoaiHinh.Trim().Substring(2) : tx_detail.MaLoaiHinh.Trim();
                ExportDeclarationDocument exportDeclarationDocument = new ExportDeclarationDocument() { Materials = new List<GC_Material>() };
                bool isTrungToKhai = false;
                foreach (ExportDeclarationDocument tk in materialReExport.exportDeclarationDocuments)
                {
                    if (tk.CustomsReference.Trim() == tx_detail.SoToKhaiVNACCS.ToString().Trim())
                    {

                        if (tk.NatureOfTransaction.Trim() == tempMaLoaiHinh.Trim())
                        {
                            isTrungToKhai = true;
                            break;
                        }
                    }

                }
                if (isTrungToKhai)
                    continue;
                List<KDT_GC_TaiXuat> listNPL = new List<KDT_GC_TaiXuat>();
                //if(item.MaLoaiHinh.Contains("V"))
                listNPL = txdk.TaiXuat_List.FindAll(x => x.SoToKhaiVNACCS == tx_detail.SoToKhaiVNACCS && x.MaLoaiHinh.Trim() == tx_detail.MaLoaiHinh.Trim());

                exportDeclarationDocument.CustomsReference = tx_detail.SoToKhaiVNACCS;
                exportDeclarationDocument.NatureOfTransaction = tempMaLoaiHinh;
                exportDeclarationDocument.Acceptance = tx_detail.NgayDangKy.ToString(sfmtDateTime);
                exportDeclarationDocument.DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tx_detail.MaHaiQuan);
                exportDeclarationDocument.Materials = new List<GC_Material>();
                foreach (KDT_GC_TaiXuat item in listNPL)
                {
                    GC_Material material = new GC_Material();
                    material.Commodity = new Commodity()
                    {
                        Identification = item.MaNPL,
                        Description = item.TenNPL,
                        Type = item.LoaiHang.ToString()
                    };
                    material.GoodsMeasure = new GoodsMeasure()
                    {
                        Quantity = Helpers.FormatNumeric(item.LuongTaiXuat, 4),
                        MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID),
                        Content = item.GhiChu
                    };
                    exportDeclarationDocument.Materials.Add(material);
                }
                materialReExport.exportDeclarationDocuments.Add(exportDeclarationDocument);

            }
            taiXuat.materialReExport = materialReExport;
            return taiXuat;
        }
        
        public static Company.KDT.SHARE.Components.Container_VNACCS ToDataTransferContainer(KDT_ContainerDangKy ContDK, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep, bool KVGS)
        {
            Company.KDT.SHARE.Components.Container_VNACCS Cont = new Company.KDT.SHARE.Components.Container_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = ContDK.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = ContDK.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Cont.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = ContDK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            if (KVGS)
            {
                Cont.Issuer = DeclarationIssuer.Container_KVGS;
                Cont.CustomsReference = TKMD.SoToKhai.ToString();
                Cont.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
                Cont.DeclarationOffice = TKMD.CoQuanHaiQuan.Trim();
                Cont.NatureOfTransaction = TKMD.MaLoaiHinh;
            }
            else
            {
                Cont.Issuer = DeclarationIssuer.Container;
                Cont.DeclarationOffice = ContDK.MaHQ.Trim();
                Cont.CustomsReference = string.Empty;
                Cont.Acceptance = string.Empty;

                ////Phiph
                //Cont.DeclarationDocument = new DeclarationDocument()
                //{
                //    Reference = TKMD.SoToKhai.ToString(),
                //    Issue = TKMD.NgayDangKy.ToString(sfmtDate),
                //    NatureOfTransaction = TKMD.MaLoaiHinh,
                //    DeclarationOffice = TKMD.CoQuanHaiQuan
                //};
                //TransportEquipments transportEquipments = new TransportEquipments()
                //{
                //    TransportEquipment = new List<TransportEquipment>()
                //};
                //foreach (KDT_ContainerBS item in ContDK.ListCont)
                //{
                //    transportEquipments.TransportEquipment.Add(new TransportEquipment
                //    {
                //        BillOfLading = item.SoVanDon,
                //        Container = item.SoContainer,
                //        Seal = item.SoSeal,
                //        Content = item.GhiChu
                //    });
                //}
                //Cont.TransportEquipments = transportEquipments;
                ////Phiph

            }
            //minhnd 21/03/2015
            Cont.DeclarationDocument = new Company.KDT.SHARE.Components.Messages.SXXK.Container_DeclarationDocument()
            {
                Reference = TKMD.SoToKhai.ToString(),
                Issue = TKMD.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = TKMD.MaLoaiHinh,
                DeclarationOffice = TKMD.CoQuanHaiQuan
            };
              //Cont.DeclarationDocument.Reference = TKMD.SoToKhai.ToString();
              //Cont.DeclarationDocument.Issue = TKMD.NgayDangKy.ToString(sfmtDate);
              //Cont.DeclarationDocument.NatureOfTransaction = TKMD.MaLoaiHinh;
              //Cont.DeclarationDocument.DeclarationOffice = TKMD.CoQuanHaiQuan;
            TransportEquipments transportEquipments = new TransportEquipments()
            {
                TransportEquipment = new List<TransportEquipment>()
            };
            foreach (KDT_ContainerBS item in ContDK.ListCont)
            {
                transportEquipments.TransportEquipment.Add(new TransportEquipment
                {
                    BillOfLading = item.SoVanDon,
                    Container = item.SoContainer,
                    Seal = item.SoSeal,
                    Content = item.GhiChu
                });
            }
            Cont.TransportEquipments = transportEquipments;
            //minhnd 21/03/2015
            return Cont;
        }

        public static CertificateOfOrigins_VNACCS ToDataTransferCertificateOfOrigin(KDT_VNACCS_CertificateOfOrigin certificateOfOrigin, KDT_VNACCS_CertificateOfOrigin_Detail certificateOfOriginDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send"; 

            CertificateOfOrigins_VNACCS certificateOfOrigins_VNACCS = new CertificateOfOrigins_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = certificateOfOrigin.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = certificateOfOrigin.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            certificateOfOrigins_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = certificateOfOrigin.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                certificateOfOrigins_VNACCS.Function = DeclarationFunction.SUA;
                certificateOfOrigins_VNACCS.CustomsReference = certificateOfOrigin.SoTN.ToString();
                certificateOfOrigins_VNACCS.Acceptance = certificateOfOrigin.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                certificateOfOrigins_VNACCS.Function = DeclarationFunction.HUY;
                certificateOfOrigins_VNACCS.CustomsReference = certificateOfOrigin.SoTN.ToString();
                certificateOfOrigins_VNACCS.Acceptance = certificateOfOrigin.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                certificateOfOrigins_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                certificateOfOrigins_VNACCS.CustomsReference = string.Empty;
                certificateOfOrigins_VNACCS.Acceptance = string.Empty;
            }
            certificateOfOrigins_VNACCS.Issuer = DeclarationIssuer.CertificateOfOrigin;
            certificateOfOrigins_VNACCS.DeclarationOffice = certificateOfOrigin.MaHQ.Trim();
            //certificateOfOrigins_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //certificateOfOrigins_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);

            if (FormType == "TKMD")
            {
                certificateOfOrigins_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                certificateOfOrigins_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            CertificateOfOriginsNew certificateOfOrigins = new CertificateOfOriginsNew()
            {
                CertificateOfOriginNew = new List<CertificateOfOriginNew>()
            };
            foreach (KDT_VNACCS_CertificateOfOrigin_Detail item in certificateOfOrigin.CertificateOfOriginCollection)
            {
                certificateOfOrigins.CertificateOfOriginNew.Add(new CertificateOfOriginNew
                {
                     Reference= item.SoCO,
                     Type= item.LoaiCO,
                     Issuer = item.ToChucCapCO,
                     Issue = item.NgayCapCO.ToString(sfmtDate),
                     IssueLocation = item.NuocCapCO,
                     Representative = item.NguoiCapCO,
                AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                {
                    FileName = certificateOfOrigin.FileName,
                    Content = certificateOfOrigin.Content
                }
                });
            }
            certificateOfOrigins_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument()
            {
                Content = certificateOfOrigin.GhiChuKhac
            };
            certificateOfOrigins_VNACCS.CertificateOfOriginsNew = certificateOfOrigins;
            return certificateOfOrigins_VNACCS;
        }
        public static CapSoDinhDanh_VNACCS ToDataTransferCapSoDinhDanh(KDT_VNACCS_CapSoDinhDanh capSoDinhDanh, string TenDoanhNghiep)
        {
            CapSoDinhDanh_VNACCS CapSoDinhDanh_VNACCS = new CapSoDinhDanh_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = capSoDinhDanh.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = capSoDinhDanh.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
                RequestType = new IssueBase()
                {
                    Type = capSoDinhDanh.LoaiDoiTuong.ToString(),
                },
                TransportEquipmentType = new IssueBase()
                {
                    Type = capSoDinhDanh.LoaiTTHH.ToString(),
                },
                CustomsImporter = new NameBase() 
                {
                    Identity = capSoDinhDanh.MaDoanhNghiep,
                }
            };
            // Người Khai Hải Quan
            CapSoDinhDanh_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = capSoDinhDanh.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            CapSoDinhDanh_VNACCS.Issuer = DeclarationIssuer.DinhDanh;
            CapSoDinhDanh_VNACCS.DeclarationOffice = capSoDinhDanh.MaHQ.Trim();
            CapSoDinhDanh_VNACCS.CustomsReference = capSoDinhDanh.SoTiepNhan.ToString();
            CapSoDinhDanh_VNACCS.Acceptance = capSoDinhDanh.NgayTiepNhan.ToString(sfmtDateTime);

            return CapSoDinhDanh_VNACCS;
        }

        public static Licenses_VNACCS ToDataTransferLicense(KDT_VNACCS_License license, KDT_VNACCS_License_Detail licenseDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status ,string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send"; 

            Licenses_VNACCS Licenses_VNACCS = new Licenses_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = license.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = license.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Licenses_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = license.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                Licenses_VNACCS.Function = DeclarationFunction.SUA;
                Licenses_VNACCS.CustomsReference = license.SoTN.ToString();
                Licenses_VNACCS.Acceptance = license.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                Licenses_VNACCS.Function = DeclarationFunction.HUY;
                Licenses_VNACCS.CustomsReference = license.SoTN.ToString();
                Licenses_VNACCS.Acceptance = license.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                Licenses_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                Licenses_VNACCS.CustomsReference = license.SoTN.ToString();
                Licenses_VNACCS.Acceptance = license.NgayTN.ToString(sfmtDateTime);
            }
            Licenses_VNACCS.Issuer = DeclarationIssuer.License;
            Licenses_VNACCS.DeclarationOffice = license.MaHQ.Trim();
            //Licenses_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //Licenses_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType=="TKMD")
            {
                Licenses_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                Licenses_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }

            Licenses licenses = new Licenses()
            {
                License = new List<Company.KDT.SHARE.Components.Messages.Vouchers.License>()
            };
            foreach (KDT_VNACCS_License_Detail item in license.LicenseCollection)
            {
                licenses.License.Add(new Company.KDT.SHARE.Components.Messages.Vouchers.License
                {
                    Issuer = item.NguoiCapGP,
                    Reference = item.SoGP,
                    Issue = item.NgayCapGP.ToString(sfmtDate),
                    IssueLocation = item.NoiCapGP,
                    Category = item.LoaiGP,
                    Expire = item.NgayHetHanGP.ToString(sfmtDate),
                AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                {
                    FileName = license.FileName,
                    Content = license.Content
                }
                });
            }
            Licenses_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = license.GhiChuKhac
            };

            Licenses_VNACCS.Licenses = licenses;
            return Licenses_VNACCS;
        }
        public static Contract_VNACCS ToDataTransferContract(KDT_VNACCS_ContractDocument contractDocument, KDT_VNACCS_ContractDocument_Detail contractDocumentDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status ,string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send"; 

            Contract_VNACCS Contract_VNACCS = new Contract_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = contractDocument.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = contractDocument.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Contract_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = contractDocument.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                Contract_VNACCS.Function = DeclarationFunction.SUA;
                Contract_VNACCS.CustomsReference = contractDocument.SoTN.ToString();
                Contract_VNACCS.Acceptance = contractDocument.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                Contract_VNACCS.Function = DeclarationFunction.HUY;
                Contract_VNACCS.CustomsReference = contractDocument.SoTN.ToString();
                Contract_VNACCS.Acceptance = contractDocument.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                Contract_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                Contract_VNACCS.CustomsReference = string.Empty;
                Contract_VNACCS.Acceptance = string.Empty;
            }
            Contract_VNACCS.Issuer = DeclarationIssuer.ContractDocument;
            Contract_VNACCS.DeclarationOffice = contractDocument.MaHQ.Trim();
            //Contract_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //Contract_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType=="TKMD")
            {
                Contract_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                Contract_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            Contract_VNACCS.DeclarationDocument = new DeclarationDocument()
            {
                Reference = TKMD.SoToKhai.ToString(),
                Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = TKMD.MaLoaiHinh,
                DeclarationOffice = TKMD.CoQuanHaiQuan
            };
            ContractDocuments contractDocuments = new ContractDocuments()
            {
                ContractDocument = new List<Company.KDT.SHARE.Components.Messages.Vouchers.ContractDocument>()
            };

            foreach (KDT_VNACCS_ContractDocument_Detail item in contractDocument.ContractDocumentCollection)
            {
                contractDocuments.ContractDocument.Add(new Company.KDT.SHARE.Components.Messages.Vouchers.ContractDocument
                {
                    Reference = item.SoHopDong,
                    Issue = item.NgayHopDong.ToString(sfmtDate),
                    Expire = item.ThoiHanThanhToan.ToString(sfmtDate),
                    TotalValue = item.TongTriGia,
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                {
                    FileName = contractDocument.FileName,
                    Content = contractDocument.Content
                }
                });
            }
            Contract_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = contractDocument.GhiChuKhac
            };

            Contract_VNACCS.ContractDocuments = contractDocuments;
            return Contract_VNACCS;
        }
        public static CommercialInvoice_VNACCS ToDataTransferCommercialInvoice(KDT_VNACCS_CommercialInvoice commercialInvoice, KDT_VNACCS_CommercialInvoice_Detail commercialInvoiceDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status,string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send"; 

            CommercialInvoice_VNACCS CommercialInvoice_VNACCS = new CommercialInvoice_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = commercialInvoice.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = commercialInvoice.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            CommercialInvoice_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = commercialInvoice.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                CommercialInvoice_VNACCS.Function = DeclarationFunction.SUA;
                CommercialInvoice_VNACCS.CustomsReference = commercialInvoice.SoTN.ToString();
                CommercialInvoice_VNACCS.Acceptance = commercialInvoice.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                CommercialInvoice_VNACCS.Function = DeclarationFunction.HUY;
                CommercialInvoice_VNACCS.CustomsReference = commercialInvoice.SoTN.ToString();
                CommercialInvoice_VNACCS.Acceptance = commercialInvoice.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                CommercialInvoice_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                CommercialInvoice_VNACCS.CustomsReference = string.Empty;
                CommercialInvoice_VNACCS.Acceptance = string.Empty;
            }
            CommercialInvoice_VNACCS.Issuer = DeclarationIssuer.CommercialInvoice;
            CommercialInvoice_VNACCS.DeclarationOffice = commercialInvoice.MaHQ.Trim();
            //CommercialInvoice_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //CommercialInvoice_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                CommercialInvoice_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                CommercialInvoice_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            CommercialInvoices commercialInvoices = new CommercialInvoices()
            {
                CommercialInvoice = new List<Company.KDT.SHARE.Components.Messages.Vouchers.CommercialInvoice>()
            };
            foreach (KDT_VNACCS_CommercialInvoice_Detail item in commercialInvoice.CommercialInvoiceCollection)
            {
                commercialInvoices.CommercialInvoice.Add(new Company.KDT.SHARE.Components.Messages.Vouchers.CommercialInvoice
                {
                    Reference = item.SoHoaDonTM,
                    Issue = item.NgayPhatHanhHDTM.ToString(sfmtDate),
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                {
                    FileName = commercialInvoice.FileName,
                    Content = commercialInvoice.Content
                }                    
                });
            }
            CommercialInvoice_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = commercialInvoice.GhiChuKhac
            };
 
            CommercialInvoice_VNACCS.CommercialInvoices = commercialInvoices;
            return CommercialInvoice_VNACCS;
        }
        public static BillOfLading_VNACCS ToDataTransferBillOfLading(KDT_VNACCS_BillOfLading billOfLading, KDT_VNACCS_BillOfLading_Detail billOfLadingDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status,string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send"; 

            BillOfLading_VNACCS BillOfLading_VNACCS = new BillOfLading_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = billOfLading.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = billOfLading.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            BillOfLading_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = billOfLading.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                BillOfLading_VNACCS.Function = DeclarationFunction.SUA;
                BillOfLading_VNACCS.CustomsReference = billOfLading.SoTN.ToString();
                BillOfLading_VNACCS.Acceptance = billOfLading.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                BillOfLading_VNACCS.Function = DeclarationFunction.HUY;
                BillOfLading_VNACCS.CustomsReference = billOfLading.SoTN.ToString();
                BillOfLading_VNACCS.Acceptance = billOfLading.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                BillOfLading_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                BillOfLading_VNACCS.CustomsReference = string.Empty;
                BillOfLading_VNACCS.Acceptance = string.Empty;
            }
            BillOfLading_VNACCS.Issuer = DeclarationIssuer.BillOfLading;
            BillOfLading_VNACCS.DeclarationOffice = billOfLading.MaHQ.Trim();
            //BillOfLading_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //BillOfLading_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                BillOfLading_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                BillOfLading_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            BillOfLadings billOfLadings = new BillOfLadings()
            {
                BillOfLading = new List<Company.KDT.SHARE.Components.BillOfLading>()
            };
            foreach (KDT_VNACCS_BillOfLading_Detail item in billOfLading.BillOfLadingCollection)
            {
                billOfLadings.BillOfLading.Add(new Company.KDT.SHARE.Components.BillOfLading
                {
                    Reference = item.SoVanDon,
                    Issue = item.NgayVanDon.ToString(sfmtDate),
                    IssueLocation = item.NuocPhatHanh,
                    TransitLocation = item.DiaDiemCTQC,
                    Category = item.LoaiVanDon.ToString(),
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                {
                    FileName = billOfLading.FileName,
                    Content = billOfLading.Content
                }
                });
            }
            BillOfLading_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = billOfLading.GhiChuKhac
            };
            BillOfLading_VNACCS.BillOfLadings = billOfLadings;
            return BillOfLading_VNACCS;
        }
        public static Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS ToDataTransferContainer(KDT_VNACCS_Container_Detail container, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send"; 

            Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS Container_VNACCS = new Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = container.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = container.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Container_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = container.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                Container_VNACCS.Function = DeclarationFunction.SUA;
                Container_VNACCS.CustomsReference = container.SoTN.ToString();
                Container_VNACCS.Acceptance = container.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                Container_VNACCS.Function = DeclarationFunction.HUY;
                Container_VNACCS.CustomsReference = container.SoTN.ToString();
                Container_VNACCS.Acceptance = container.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                Container_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                Container_VNACCS.CustomsReference = string.Empty;
                Container_VNACCS.Acceptance = string.Empty;
            }
            Container_VNACCS.Issuer = DeclarationIssuer.Containers;
            Container_VNACCS.DeclarationOffice = container.MaHQ.Trim();
            //Container_VNACCS.CustomsReference = string.Empty;
            //Container_VNACCS.Acceptance = DateTime.Now.ToString(sfmtDateTime);

            if (FormType == "TKMD")
            {
                Container_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                Container_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            Container_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = container.GhiChuKhac
            };
            Container_VNACCS.AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
            {
                FileName = container.FileName,
                Content = container.Content
            };
            return Container_VNACCS;
        }
        public static AdditionalDocument_VNACCS ToDataTransferAdditionalDocument(KDT_VNACCS_AdditionalDocument additionalDocument, KDT_VNACCS_AdditionalDocument_Detail additionalDocument_Detail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status =="Edit" : Status=="Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            AdditionalDocument_VNACCS AdditionalDocument_VNACCS = new AdditionalDocument_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = additionalDocument.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = additionalDocument.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            AdditionalDocument_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = additionalDocument.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.SUA;
                AdditionalDocument_VNACCS.CustomsReference = additionalDocument.SoTN.ToString();
                AdditionalDocument_VNACCS.Acceptance = additionalDocument.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.HUY;
                AdditionalDocument_VNACCS.CustomsReference = additionalDocument.SoTN.ToString();
                AdditionalDocument_VNACCS.Acceptance = additionalDocument.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                AdditionalDocument_VNACCS.CustomsReference = string.Empty;
                AdditionalDocument_VNACCS.Acceptance = string.Empty;
            }
            AdditionalDocument_VNACCS.Issuer = DeclarationIssuer.AdditionalDocument;
            AdditionalDocument_VNACCS.DeclarationOffice = additionalDocument.MaHQ.Trim();
            if (FormType == "TKMD")
            {
                AdditionalDocument_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                AdditionalDocument_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }

            AdditionalDocuments additionalDocuments = new AdditionalDocuments()
            {
                AdditionalDocument = new List<AdditionalDocument>()
            };
            foreach (KDT_VNACCS_AdditionalDocument_Detail item in additionalDocument.AdditionalDocumentCollection)
            {
                additionalDocuments.AdditionalDocument.Add(new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Reference = item.SoChungTu,
                    Description = item.TenChungTu,
                    Issue = item.NgayPhatHanh.ToString(sfmtDate),
                    IssueLocation = item.NoiPhatHanh,
                    Type = additionalDocument.LoaiChungTu.ToString(),
                    AdditionalDocumentSub = new AdditionalDocument()
                    {
                        Content = additionalDocument.GhiChuKhac
                    },
                AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                {
                    FileName = additionalDocument.FileName,
                    Content = additionalDocument.Content
                }
                });
            }
            //AdditionalDocument_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            //{
            //    Content = additionalDocument.GhiChuKhac
            //};
            AdditionalDocument_VNACCS.AdditionalDocuments = additionalDocuments;
            return AdditionalDocument_VNACCS;
        }
        public static Overtime_VNACCS ToDataTransferOvertime(KDT_VNACCS_OverTime overTime, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep, string Status)
        {
            if (Status =="Send")
            {
                Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
                {
                    Function = DeclarationFunction.KHAI_BAO,
                    Reference = overTime.GuidStr,
                    Issue = DateTime.Now.ToString(sfmtDateTime),
                    IssueLocation = string.Empty,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                    Importer = new NameBase()
                    {
                        Identity = overTime.MaDoanhNghiep,
                        Name = TenDoanhNghiep
                    },
                    Agents = new List<Agent>(),
                };
                // Người Khai Hải Quan
                Overtime_VNACCS.Agents.Add(new Agent
                {
                    Name = TenDoanhNghiep,
                    Identity = overTime.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                });
                    Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
                    Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
                    //Overtime_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
                    //Overtime_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = overTime.NgayDangKy.ToString(sfmtDate),
                    Hour = overTime.GioLamThuTuc.ToString(),
                    Content = overTime.NoiDung,
                };
                return Overtime_VNACCS;
            }
            else if (Status == "Edit")
            {
                Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
                {
                    Function = DeclarationFunction.SUA,
                    Reference = overTime.GuidStr,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    IssueLocation = string.Empty,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                    Importer = new NameBase()
                    {
                        Identity = overTime.MaDoanhNghiep,
                        Name = TenDoanhNghiep
                    },
                    Agents = new List<Agent>(),
                };
                // Người Khai Hải Quan
                Overtime_VNACCS.Agents.Add(new Agent
                {
                    Name = TenDoanhNghiep,
                    Identity = overTime.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                });
                    Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
                    Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
                    Overtime_VNACCS.CustomsReference = overTime.SoTN.ToString();
                    Overtime_VNACCS.Acceptance = overTime.NgayTN.ToString(sfmtDateTime);

                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = overTime.NgayDangKy.ToString(sfmtDate),
                    Hour = overTime.GioLamThuTuc.ToString(),
                    Content = overTime.NoiDung,
                };
                return Overtime_VNACCS;
            }
            else
            {
                Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
                {
                    Function = DeclarationFunction.HUY,
                    Reference = overTime.GuidStr,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    IssueLocation = string.Empty,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                    Importer = new NameBase()
                    {
                        Identity = overTime.MaDoanhNghiep,
                        Name = TenDoanhNghiep
                    },
                    Agents = new List<Agent>(),
                };
                // Người Khai Hải Quan
                Overtime_VNACCS.Agents.Add(new Agent
                {
                    Name = TenDoanhNghiep,
                    Identity = overTime.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                });
                Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
                Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
                Overtime_VNACCS.CustomsReference = overTime.SoTN.ToString();
                Overtime_VNACCS.Acceptance = overTime.NgayTN.ToString(sfmtDateTime);
                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Content = overTime.NoiDung,
                };
                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = overTime.NgayDangKy.ToString(sfmtDate),
                    Hour = overTime.GioLamThuTuc.ToString(),
                    Content = overTime.NoiDung,
                };
                return Overtime_VNACCS;           
            }
        }

        public static GoodsItems_VNACCS ToDataTransferGoodsItem(KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodsItem, KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail goodsItem_Detail, string DiaChi, string TenDoanhNghiep,string Status)
        {
            GoodsItems_VNACCS GoodsItems_VNACCS = new GoodsItems_VNACCS()
            {

                Reference = goodsItem.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                //Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                FinalYear = goodsItem.NamQuyetToan.ToString(),
                Type = goodsItem.LoaiHinhBaoCao.ToString(),
                Unit = goodsItem.DVTBaoCao.ToString(),
                Importer = new NameBase()
                {
                    Identity = goodsItem.MaDoanhNghiep,
                    Name = TenDoanhNghiep ,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            if (Status == "Send")
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.SUA;
            }
            // Người Khai Hải Quan
            GoodsItems_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = goodsItem.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            GoodsItems_VNACCS.Issuer = DeclarationIssuer.GoodsItem;
            GoodsItems_VNACCS.DeclarationOffice = goodsItem.MaHQ.Trim();
            GoodsItems_VNACCS.CustomsReference = goodsItem.SoTN.ToString();
            GoodsItems_VNACCS.Acceptance = goodsItem.NgayTN.ToString(sfmtDateTime);

            GoodsItems goodsItems = new GoodsItems()
            {
                GoodsItem = new List<GoodsItem>()
            };
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in goodsItem.GoodItemsCollection)
            {
                goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                {
                    Sequence = item.STT.ToString(),
                    Type = item.LoaiHangHoa.ToString(),
                    Account = item.TaiKhoan.ToString(),
                    Description = item.TenHangHoa,
                    Identification = item.MaHangHoa,
                    MeasureUnit = item.DVT,
                    QuantityBegin = item.TonDauKy,
                    QuantityImport = item.NhapTrongKy,
                    QuantityExport = item.XuatTrongKy,
                    QuantityExcess = item.TonCuoiKy,
                    Content = item.GhiChu,
                });
            }

            GoodsItems_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = goodsItem.GhiChuKhac
            };
            GoodsItems_VNACCS.AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
            {
                FileName = goodsItem.FileName,
                Content = goodsItem.Content
            };           
            GoodsItems_VNACCS.GoodsItems = goodsItems;
            return GoodsItems_VNACCS;
        }
        public static GoodsItems_VNACCS ToDataTransferGoodsItem_New(KDT_VNACCS_BaoCaoQuyetToan_TT39 goodsItem, string DiaChi, string TenDoanhNghiep, string Status, string LISTHOPDONG_ID)
        {
            GoodsItems_VNACCS GoodsItems_VNACCS = new GoodsItems_VNACCS()
            {

                Reference = goodsItem.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                //Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                StartDate = goodsItem.NgayBatDauBC.ToString(sfmtDate),
                FinishDate = goodsItem.NgayKetThucBC.ToString(sfmtDate),
                //FinalYear = goodsItem.NamQuyetToan.ToString(),
                Type = goodsItem.LoaiBC.ToString(),
                UpdateType = goodsItem.LoaiSua.ToString(),
                //Unit = goodsItem.DVTBaoCao.ToString(),
                Importer = new NameBase()
                {
                    Identity = goodsItem.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            if (Status == "Send")
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else if (Status == "Edit")
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.SUA;
            }
            else
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.HUY;
            }
            // Người Khai Hải Quan
            GoodsItems_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = goodsItem.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            GoodsItems_VNACCS.Issuer = DeclarationIssuer.GoodsItemNew;
            GoodsItems_VNACCS.DeclarationOffice = goodsItem.MaHQ.Trim();
            GoodsItems_VNACCS.CustomsReference = goodsItem.SoTN.ToString();
            GoodsItems_VNACCS.Acceptance = goodsItem.NgayTN.ToString(sfmtDateTime);

            ContractReferences contractReferences = new ContractReferences()
            {
                ContractReference = new List<ContractReference>()
            };
            foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HD in goodsItem.HopDongCollection)
            {
                GoodsItems goodsItems = new GoodsItems()
                {
                    GoodsItem = new List<GoodsItem>()
                };
                foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in HD.HangHoaCollection)
                {
                    goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                    {
                        Sequence = item.STT.ToString(),
                        DescriptionMaterial = item.TenHangHoa,
                        IdentificationMaterial = item.MaHangHoa,
                        MeasureUnitMaterial = item.DVT,
                        QuantityBeginMaterial = item.LuongTonDauKy,
                        QuantityImportMaterial = item.LuongNhapTrongKy,
                        QuantityReExportMaterial = item.LuongTaiXuat,
                        QuantityRePurposeMaterial = item.LuongChuyenMucDichSD,
                        QuantityExportProduct = item.LuongXuatKhau,
                        QuantityExportOther = item.LuongXuatKhac,
                        QuantityExcessMaterial = item.LuongTonCuoiKy,
                        Content = item.GhiChu,
                    }); 
                }
                contractReferences.ContractReference.Add(new ContractReference
                {
                    Sequence = HD.STT,
                    Reference = HD.SoHopDong,
                    Issue = HD.NgayHopDong.ToString(sfmtDate),
                    DeclarationOffice = HD.MaHQ,
                    Expire = HD.NgayHetHan.ToString(sfmtDate),
                    AdditionalInformation = new AdditionalDocument()
                    {
                        Content = HD.GhiChuKhac,
                    },
                    GoodsItems = goodsItems,
                });
            }
            #region Comment Code
            ////foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New items in goodsItem.GoodItemsCollection)
            ////{
            //    LISTHOPDONG_ID = "11";
            //    List<HopDong> HDCollection = HopDong.SelectCollectionDynamic("ID IN (" + LISTHOPDONG_ID + ")", "ID");
            //    foreach (HopDong HD in HDCollection)
            //    {
            //        int i = 0;
            //        foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in goodsItem.GoodItemsCollection)
            //        {
            //            //if (HD.ID ==item.STT)
            //            //{
            //                goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
            //                {
            //                    Sequence = item.STT.ToString(),
            //                    DescriptionMaterial = item.TenHangHoa,
            //                    IdentificationMaterial = item.MaHangHoa,
            //                    MeasureUnitMaterial = item.DVT,
            //                    QuantityBeginMaterial = item.TonDauKy,
            //                    QuantityImportMaterial = item.NhapTrongKy,
            //                    QuantityReExportMaterial = item.TaiXuat,
            //                    QuantityRePurposeMaterial = item.ChuyenMDSD,
            //                    QuantityExportProduct = item.XuatTrongKy,
            //                    QuantityExportOther = item.XuatKhac,
            //                    QuantityExcessMaterial = item.TonCuoiKy,
            //                    Content = item.GhiChu,
            //                }); 
            //            //}
            //        }  
            //        //HopDong HD = HopDong.Load(Convert.ToInt64(item.STT));
            //        contractReferences.ContractReference.Add(new ContractReference
            //        {
            //            Sequence = i++,
            //            Reference = HD.SoHopDong,
            //            Issue = HD.NgayDangKy.ToString(sfmtDate),
            //            DeclarationOffice = HD.MaHaiQuan,
            //            Expire = HD.NgayHetHan.ToString(sfmtDate),
            //            AdditionalInformation = new AdditionalDocument()
            //            {
            //                Content ="GHI CHU",
            //            },
            //            GoodsItems = goodsItems,
            //        });
            //    }
            ////}
            #endregion
            GoodsItems_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = goodsItem.GhiChuKhac
            };
            GoodsItems_VNACCS.ContractReferences = contractReferences;
            return GoodsItems_VNACCS;
        }
        public static GC_ImportWareHouse ToDataTransferExportWareHouseNew(T_KDT_VNACCS_WarehouseExport warehouseExport, T_KDT_VNACCS_WarehouseExport_Detail warehouseExport_Detail, T_KDT_VNACCS_WarehouseExport_GoodsDetail warehouseExport_GoodsDetail, string TenDoanhNghiep,String Status)
        {
            //bool Status;
            GC_ImportWareHouse ExportWareHouse = new GC_ImportWareHouse()
            {

                Issuer = DeclarationIssuer.EXportWareHouse,
                Reference = warehouseExport.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                //Function = DeclarationFunction.SUA,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = warehouseExport.SoTN.ToString(),
                Acceptance = warehouseExport.NgayTN.ToString(sfmtDate),
                DeclarationOffice = warehouseExport.MaHQ,
                StartDate = warehouseExport.NgayBatDauBC.ToString(sfmtDate),
                FinishDate = warehouseExport.NgayKetthucBC.ToString(sfmtDate),
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Identity = warehouseExport.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Warehouse = new NameBase()
                {
                    Name = warehouseExport.TenKho,
                    Identity = warehouseExport.MaKho
                },
            };
            if (Status == "Send")
            {
                ExportWareHouse.Function = DeclarationFunction.KHAI_BAO;
            }
            else if (Status == "Edit")
            {
                ExportWareHouse.Function = DeclarationFunction.SUA;
            }
            else
            {
                ExportWareHouse.Function = DeclarationFunction.HUY;
            }
            // Người Khai Hải Quan
            ExportWareHouse.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = warehouseExport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            PNK_AdditionalDocuments additionalDocuments = new PNK_AdditionalDocuments()
            {
                AdditionalDocument = new List<PNK_AdditionalDocument>(),
            };

            foreach (T_KDT_VNACCS_WarehouseExport_Detail item in warehouseExport.WarehouseExportCollection)
            {
                int i = 1;
                additionalDocuments.AdditionalDocument.Add(new PNK_AdditionalDocument
                {
                    Sequence = i++.ToString(),
                    Identification = item.SoPhieuXuat,
                    Issue = item.NgayPhieuXuat.ToString(sfmtDate),
                    NameConsignee = item.TenNguoiNhanHang,
                    IdentityConsignee = item.MaNguoiNhanHang,
                    CustomsGoodsItem = T_KDT_VNACCS_WarehouseExport_GoodsDetail.SelectCollectionBy_WarehouseExport_Details_ID_Update(item.ID,warehouseExport),
                    AdditionalInformation = new PNK_AdditionalInformation()
                    {
                        Content = warehouseExport.GhiChuKhac,
                    }
                });
            }
            ExportWareHouse.AdditionalDocuments = additionalDocuments;
            //ImportWareHouse.ContractReference = new PNK_ContractReference()
            //{
            //    Reference = "SOFTECH NEW",
            //    Issue = "2018-03-16",
            //    DeclarationOffice = "01TE",
            //    Expire = "2019-03-16",

            //};
            return ExportWareHouse;
        }

        public static Company.KDT.SHARE.Components.Messages.CSSX.StorageAreasProduction_VNACCS ToDataTransferStorageAreasProduction(KDT_VNACCS_StorageAreasProduction storageAreasProduction, string DiaChi, string TenDoanhNghiep, string Status)
        {
            Company.KDT.SHARE.Components.Messages.CSSX.StorageAreasProduction_VNACCS StorageAreasProduction_VNACCS = new Company.KDT.SHARE.Components.Messages.CSSX.StorageAreasProduction_VNACCS()
            {

                Reference = storageAreasProduction.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Name = TenDoanhNghiep,
                    Identity = storageAreasProduction.MaDoanhNghiep,
                },
                ImporterDetail = new Company.KDT.SHARE.Components.Messages.CSSX.ImporterDetail()
                {
                    Address = DiaChi,
                    AddressType = storageAreasProduction.LoaiDiaChiTruSoChinh.ToString(),
                    InvestmentCountry = storageAreasProduction.NuocDauTu,
                    IndustryProduction = storageAreasProduction.NganhNgheSanXuat,
                    DateFinanceYear = storageAreasProduction.NgayKetThucNamTC.ToString("dd-MM"),
                    TypeOfBusiness = storageAreasProduction.LoaiHinhDN.ToString(),
                    OldImporter = new Company.KDT.SHARE.Components.Messages.CSSX.OldImporter()
                    {
                        Name = storageAreasProduction.TenDoanhNghiepTKTD,
                        Identity = storageAreasProduction.MaDoanhNghiepTKTD,
                        Reason = storageAreasProduction.LyDoChuyenDoi,
                    },
                    ChairmanImporter = new Company.KDT.SHARE.Components.Messages.CSSX.ChairmanImporter()
                    {
                        Identity = storageAreasProduction.SoCMNDCT,
                        Issue = storageAreasProduction.NgayCapGiayPhepCT.ToString(sfmtDate),
                        IssueLocation = storageAreasProduction.NoiCapGiayPhepCT,
                        PermanentResidence = storageAreasProduction.NoiDangKyHKTTCT,
                        PhoneNumbers = storageAreasProduction.SoDienThoaiCT,
                    },
                    GeneralDirector = new Company.KDT.SHARE.Components.Messages.CSSX.GeneralDirector()
                    {
                        Identity = storageAreasProduction.SoCMNDGD,
                        Issue = storageAreasProduction.NgayCapGiayPhepGD.ToString(sfmtDate),
                        IssueLocation = storageAreasProduction.NoiCapGiayPhepGD,
                        PermanentResidence = storageAreasProduction.NoiDangKyHKTTGD,
                        PhoneNumbers = storageAreasProduction.SoDienThoaiGD,
                    },
                    //StorageOfGoods = new StorageOfGoods()
                    //{
                    //    StorageOfGood = new List<StorageOfGood>(),
                    //},                    
                    ProductionInspectionHis = new Company.KDT.SHARE.Components.Messages.CSSX.ProductionInspectionHis()
                    {
                        IsInspection = storageAreasProduction.DaDuocCQHQKT.ToString(),
                        //ContentInspections = new Company.KDT.SHARE.Components.Messages.CSSX.ContentInspections() 
                        //{
                        //    ContentInspection = new List<Company.KDT.SHARE.Components.Messages.CSSX.ContentInspection>()
                        //},
                    },
                }
            };

            Company.KDT.SHARE.Components.Messages.CSSX.StorageOfGoods storageOfGoods = new Company.KDT.SHARE.Components.Messages.CSSX.StorageOfGoods()
            {
                StorageOfGood = new List<Company.KDT.SHARE.Components.Messages.CSSX.StorageOfGood>()
            };

            foreach (KDT_VNACCS_StorageOfGood item in storageAreasProduction.StorageOfGoodCollection)
            {
                storageOfGoods.StorageOfGood.Add(new Company.KDT.SHARE.Components.Messages.CSSX.StorageOfGood
                {
                    Name = item.Ten,
                    Identity = item.Ma
                });
            }
            StorageAreasProduction_VNACCS.ImporterDetail.StorageOfGoods = storageOfGoods;
            Company.KDT.SHARE.Components.Messages.CSSX.ContentInspections contentInspections = new Company.KDT.SHARE.Components.Messages.CSSX.ContentInspections()
            {
                ContentInspection = new List<Company.KDT.SHARE.Components.Messages.CSSX.ContentInspection>()
            };
            foreach (KDT_VNACCS_ContentInspection item in storageAreasProduction.ContentInspectionCollection)
            {
                contentInspections.ContentInspection.Add(new Company.KDT.SHARE.Components.Messages.CSSX.ContentInspection
                {
                    InspectionNumbers = item.SoBienBanKiemTra,
                    ConclusionNumbers =item.SoKetLuanKiemTra,
                    InspectionDate =item.NgayKiemTra.ToString(sfmtDate),
                });
            }
            StorageAreasProduction_VNACCS.ImporterDetail.ProductionInspectionHis.ContentInspections = contentInspections;

            if (Status == "Send")
            {
                StorageAreasProduction_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else
            {
                StorageAreasProduction_VNACCS.Function = DeclarationFunction.SUA;
            }
            // Người Khai Hải Quan
            StorageAreasProduction_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = storageAreasProduction.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            StorageAreasProduction_VNACCS.Issuer = DeclarationIssuer.StorageAreasProduction;
            StorageAreasProduction_VNACCS.DeclarationOffice = storageAreasProduction.MaHQ.Trim();
            StorageAreasProduction_VNACCS.CustomsReference = storageAreasProduction.SoTN.ToString();
            StorageAreasProduction_VNACCS.Acceptance = storageAreasProduction.NgayTN.ToString(sfmtDateTime);
            StorageAreasProduction_VNACCS.UpdateType = storageAreasProduction.LoaiSua.ToString();

            Company.KDT.SHARE.Components.Messages.CSSX.ManufactureFactories manufactureFactories = new Company.KDT.SHARE.Components.Messages.CSSX.ManufactureFactories()
            {
                ManufactureFactory = new List<Company.KDT.SHARE.Components.Messages.CSSX.ManufactureFactory>()
            };
            foreach (KDT_VNACCS_ManufactureFactory item in storageAreasProduction.ManufactureFactoryCollection)
	        {
                Company.KDT.SHARE.Components.Messages.CSSX.Careers carrery = new Company.KDT.SHARE.Components.Messages.CSSX.Careers()
                {
                    Career = new List<Company.KDT.SHARE.Components.Messages.CSSX.Career>()
                };
                List<KDT_VNACCS_Careery> CareeryCollection = KDT_VNACCS_Careery.SelectCollectionBy_ManufactureFactory_ID(item.ID);
                foreach (KDT_VNACCS_Careery careery in CareeryCollection)
                {
                    List<KDT_VNACCS_Careeries_Product> ProductCollection = KDT_VNACCS_Careeries_Product.SelectCollectionBy_Careeries_ID(careery.ID);
                    Company.KDT.SHARE.Components.Messages.CSSX.Products products = new Company.KDT.SHARE.Components.Messages.CSSX.Products()
                    {
                        Product = new List<Company.KDT.SHARE.Components.Messages.CSSX.Product>(),
                    };
                    foreach (KDT_VNACCS_Careeries_Product product in ProductCollection)
                    {
                        products.Product.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Product
                        {
                            Identification = product.MaSP,
                            TariffClassification = product.MaHS,
                            Period = product.ChuKySXTG.ToString(),
                            MeasureUnit =product.ChuKySXDVT.ToString(),
                        });
                    }
                    List<KDT_VNACCS_ProductionCapacity_Product> ProductionCapacityCollection = KDT_VNACCS_ProductionCapacity_Product.SelectCollectionBy_Careeries_ID(careery.ID);

                    Company.KDT.SHARE.Components.Messages.CSSX.Products productionCapacities = new Company.KDT.SHARE.Components.Messages.CSSX.Products()
                    {
                        Product = new List<Company.KDT.SHARE.Components.Messages.CSSX.Product>(),
                    };
                    foreach (KDT_VNACCS_ProductionCapacity_Product product in ProductionCapacityCollection)
                    {
                        productionCapacities.Product.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Product
                        {
                            Time = product.ThoiGianSXTG.ToString(),
                            MeasureUnitTime = product.ThoiGianSXDVT.ToString(),
                            Identification = product.MaSP,
                            TariffClassification = product.MaHS,       
                            MeasureUnit = product.DVT.ToString(),
                            Quantity = product.SoLuong.ToString(),
                        });
                    }
                    carrery.Career.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Career
                        {
                            Type = careery.LoaiNganhNghe.ToString(),
                            Period = products,
                            ProductionCapacity = productionCapacities,
                        });
                }
                manufactureFactories.ManufactureFactory.Add(new Company.KDT.SHARE.Components.Messages.CSSX.ManufactureFactory
                {
                    Type = item.LoaiCSSX.ToString(),
                    Address = item.DiaChiCSSX,
                    AddressType = item.LoaiDiaChiCSSX.ToString(),
                    Square = item.DienTichNhaXuong.ToString().Replace(",","."),
                    WorkerQuantity = item.SoLuongCongNhan.ToString(),
                    Machine = new Company.KDT.SHARE.Components.Messages.CSSX.Machine()
                    {
                        OwnedQuantity = item.SoLuongSoHuu,
                        RentQuantity = item.SoLuongDiThue,
                        OtherQuantity = item.SoLuongKhac,
                        TotalQuantity = item.TongSoLuong,
                        ProductionCapacity = item.NangLucSanXuat,
                    },
                    Careers = carrery,
                });
		 
	        }
            StorageAreasProduction_VNACCS.ManufactureFactories = manufactureFactories;
            Company.KDT.SHARE.Components.Messages.CSSX.Careers career = new Company.KDT.SHARE.Components.Messages.CSSX.Careers()
            {
                Career = new List<Company.KDT.SHARE.Components.Messages.CSSX.Career>()
            };
            foreach (KDT_VNACCS_Career item in storageAreasProduction.CareerCollection)
            {
                career.Career.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Career
                    {
                        Type = item.LoaiNganhNghe.ToString(),
                    });
            }
            StorageAreasProduction_VNACCS.Careers = career;

            StorageAreasProduction_VNACCS.Staff = new Company.KDT.SHARE.Components.Messages.CSSX.Staff
            {
                ManageQuantity = storageAreasProduction.BoPhanQuanLy.ToString(),
                WorkerQuantity = storageAreasProduction.SoLuongCongNhan.ToString(),
            };

            StorageAreasProduction_VNACCS.Machine = new Company.KDT.SHARE.Components.Messages.CSSX.Machine
            {
                OwnedQuantity = storageAreasProduction.SoLuongSoHuu,
                RentQuantity = storageAreasProduction.SoLuongDiThue,
                OtherQuantity = storageAreasProduction.SoLuongKhac,
                TotalQuantity = storageAreasProduction.TongSoLuong,
            };

            if (storageAreasProduction.HoldingCompanyCollection.Count >= 1)
            {
                Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompanies holdingCompanies = new Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompanies()
                {
                    Quantity = storageAreasProduction.SoLuongThanhVienCTM.ToString(),
                    HoldingCompany = new List<Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompany>()
                };
                foreach (KDT_VNACCS_HoldingCompany item in storageAreasProduction.HoldingCompanyCollection)
                {
                    holdingCompanies.HoldingCompany.Add(new Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompany
                    {
                        Name = item.TenDoanhNghiep,
                        Identity = item.MaDoanhNghiep,
                        Address = item.DiaChiCSSX,
                    });
                }
                StorageAreasProduction_VNACCS.HoldingCompanies = holdingCompanies;
            }
            else
            {
                Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompanies holdingCompanies = new Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompanies()
                {
                    HoldingCompany = new List<Company.KDT.SHARE.Components.Messages.CSSX.HoldingCompany>()
                };
            }
            if (storageAreasProduction.AffiliatedMemberCompanyCollection.Count >= 1)
            {
                Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompanies affiliatedMemberCompanies = new Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompanies()
                {
                    Name = storageAreasProduction.TenCongTyMe,
                    Identity = storageAreasProduction.MaCongTyMe,
                    Quantity = storageAreasProduction.SoLuongThanhVien.ToString(),
                    AffiliatedMemberCompany = new List<Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompany>()
                };
                foreach (KDT_VNACCS_AffiliatedMemberCompany item in storageAreasProduction.AffiliatedMemberCompanyCollection)
                {
                    affiliatedMemberCompanies.AffiliatedMemberCompany.Add(new Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompany
                    {
                        Name = item.TenDoanhNghiep,
                        Identity = item.MaDoanhNghiep,
                        Address = item.DiaChiCSSX,
                    });
                }
                StorageAreasProduction_VNACCS.AffiliatedMemberCompanies = affiliatedMemberCompanies;
            }
            else
            {
                Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompanies affiliatedMemberCompanies = new Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompanies()
                {
                    AffiliatedMemberCompany = new List<Company.KDT.SHARE.Components.Messages.CSSX.AffiliatedMemberCompany>()
                };
            }

            if (storageAreasProduction.MemberCompanyCollection.Count >= 1)
            {
                Company.KDT.SHARE.Components.Messages.CSSX.MemberCompanies memberCompanies = new Company.KDT.SHARE.Components.Messages.CSSX.MemberCompanies()
                {
                    Quantity = storageAreasProduction.SoLuongChiNhanh.ToString(),
                    MemberCompany = new List<Company.KDT.SHARE.Components.Messages.CSSX.MemberCompany>()
                };
                foreach (KDT_VNACCS_MemberCompany item in storageAreasProduction.MemberCompanyCollection)
                {
                    memberCompanies.MemberCompany.Add(new Company.KDT.SHARE.Components.Messages.CSSX.MemberCompany
                    {
                        Name = item.TenDoanhNghiep,
                        Identity = item.MaDoanhNghiep,
                        Address = item.DiaChiChiNhanh,
                    });
                }
                StorageAreasProduction_VNACCS.MemberCompanies = memberCompanies;
            }
            else
            {
                Company.KDT.SHARE.Components.Messages.CSSX.MemberCompanies memberCompanies = new Company.KDT.SHARE.Components.Messages.CSSX.MemberCompanies()
                {
                    MemberCompany = new List<Company.KDT.SHARE.Components.Messages.CSSX.MemberCompany>()
                };
            }

            StorageAreasProduction_VNACCS.ComplianceWithLaws = new Company.KDT.SHARE.Components.Messages.CSSX.ComplianceWithLaws
            {
                Smuggling = storageAreasProduction.BiXuPhatVeBuonLau.ToString(),
                TaxEvasion = storageAreasProduction.BiXuPhatVeTronThue.ToString(),
                HandlingViolations = storageAreasProduction.BiXuPhatVeKeToan.ToString(),
            };

            if (storageAreasProduction.OutsourcingManufactureFactoryCollection.Count >= 1)
            {
                Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactories outsourcingManufactureFactories = new Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactories()
                {
                    OutsourcingManufactureFactory = new List<Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactory>()
                };

                foreach (KDT_VNACCS_OutsourcingManufactureFactory item in storageAreasProduction.OutsourcingManufactureFactoryCollection)
                {
                    Company.KDT.SHARE.Components.Messages.CSSX.ContractDocuments contractDocuments = new Company.KDT.SHARE.Components.Messages.CSSX.ContractDocuments()
                    {
                        ContractDocument = new List<Company.KDT.SHARE.Components.Messages.CSSX.ContractDocument>(),
                    };

                    List<KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument> ContractDocumentCollection = KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument.SelectCollectionBy_OutsourcingManufactureFactory_ID(item.ID);

                    foreach (KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument contractDocument in ContractDocumentCollection)
                    {
                        contractDocuments.ContractDocument.Add(new Company.KDT.SHARE.Components.Messages.CSSX.ContractDocument
                        {
                            Reference = contractDocument.SoHopDong.ToString(),
                            Issue = contractDocument.NgayHopDong.ToString(sfmtDate),
                            Expire = contractDocument.NgayHetHan.ToString(sfmtDate)
                        });
                    }

                    List<KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory> OurManufactureFactoryCollection = KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory.SelectCollectionBy_OutsourcingManufactureFactory_ID(item.ID);
                    List<Company.KDT.SHARE.Components.Messages.CSSX.OurManufactureFactory> ourManufactureFactories = new List<Company.KDT.SHARE.Components.Messages.CSSX.OurManufactureFactory>();
                    foreach (KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory ourManufactureFactory in OurManufactureFactoryCollection)
                    {
                        List<KDT_VNACCS_OutsourcingManufactureFactory_Product> ProductCollection = KDT_VNACCS_OutsourcingManufactureFactory_Product.SelectCollectionBy_OutsourcingManufactureFactory_ManufactureFactory_ID(ourManufactureFactory.ID);
                        Company.KDT.SHARE.Components.Messages.CSSX.Products products = new Company.KDT.SHARE.Components.Messages.CSSX.Products()
                        {
                            Product = new List<Company.KDT.SHARE.Components.Messages.CSSX.Product>(),
                        };
                        foreach (KDT_VNACCS_OutsourcingManufactureFactory_Product product in ProductCollection)
                        {
                            products.Product.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Product
                            {
                                Identification = product.MaSP,
                                TariffClassification = product.MaHS,
                                Period = product.ChuKySXTG.ToString(),
                                MeasureUnit = product.ChuKySXDVT.ToString(),
                            });
                        }

                        List<KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product> ProductionCapacityCollection = KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product.SelectCollectionBy_OutsourcingManufactureFactory_ManufactureFactory_ID(ourManufactureFactory.ID);

                        Company.KDT.SHARE.Components.Messages.CSSX.Products productionCapacities = new Company.KDT.SHARE.Components.Messages.CSSX.Products()
                        {
                            Product = new List<Company.KDT.SHARE.Components.Messages.CSSX.Product>(),
                        };
                        foreach (KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product product in ProductionCapacityCollection)
                        {
                            productionCapacities.Product.Add(new Company.KDT.SHARE.Components.Messages.CSSX.Product
                            {
                                Time = product.ThoiGianSXTG.ToString(),
                                MeasureUnitTime = product.ThoiGianSXDVT.ToString(),
                                Identification = product.MaSP,
                                TariffClassification = product.MaHS,
                                MeasureUnit = product.DVT.ToString(),
                                Quantity = product.SoLuong.ToString(),
                            });
                        }
                        Company.KDT.SHARE.Components.Messages.CSSX.OurManufactureFactory outsourcingManufactureFactory = new Company.KDT.SHARE.Components.Messages.CSSX.OurManufactureFactory()
                        {
                            Address = ourManufactureFactory.DiaChiCSSX,
                            Square = ourManufactureFactory.DienTichNX.ToString().Replace(",", "."),
                            WorkerQuantity = ourManufactureFactory.SoLuongCongNhan.ToString(),
                            Period = products,
                            ProductionCapacity = productionCapacities,
                        };
                        ourManufactureFactories.Add(outsourcingManufactureFactory);
                        outsourcingManufactureFactory.Machine = new Company.KDT.SHARE.Components.Messages.CSSX.Machine()
                        {
                            OwnedQuantity = ourManufactureFactory.SoLuongSoHuu,
                            RentQuantity = ourManufactureFactory.SoLuongDiThue,
                            OtherQuantity = ourManufactureFactory.SoLuongKhac,
                            TotalQuantity = ourManufactureFactory.TongSoLuong,
                            ProductionCapacity = ourManufactureFactory.NangLucSX.ToString()
                        };
                    }
                    outsourcingManufactureFactories.OutsourcingManufactureFactory.Add(new Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactory
                    {
                        Name = item.TenDoiTac,
                        Identity = item.MaDoiTac,
                        Address = item.DiaChiDoiTac,
                        ContractDocuments = contractDocuments,
                        OutManufactureFactory = ourManufactureFactories
                    });
                    StorageAreasProduction_VNACCS.OutsourcingManufactureFactories = outsourcingManufactureFactories;
                }
            }
            else
            {
                Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactories outsourcingManufactureFactories = new Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactories()
                {
                    OutsourcingManufactureFactory = new List<Company.KDT.SHARE.Components.Messages.CSSX.OutsourcingManufactureFactory>()
                };
            }
            Company.KDT.SHARE.Components.Messages.CSSX.FileAttacheds attachedFiles = new Company.KDT.SHARE.Components.Messages.CSSX.FileAttacheds()
            {
                File = new List<Company.KDT.SHARE.Components.Messages.CSSX.FileAttached>(),
            };
            foreach (KDT_VNACCS_StorageAreasProduction_AttachedFile attachedFile in storageAreasProduction.AttachedFileGoodCollection)
	        {
                attachedFiles.File.Add(new Company.KDT.SHARE.Components.Messages.CSSX.FileAttached
                {
                    FileName = attachedFile.FileName,
                    Content =attachedFile.Content,
                });
	        }
            StorageAreasProduction_VNACCS.AttachedFiles = attachedFiles;
            StorageAreasProduction_VNACCS.Information = new Company.KDT.SHARE.Components.Messages.CSSX.Information
            {
                Content = storageAreasProduction.GhiChuKhac
            };
            return StorageAreasProduction_VNACCS;
        }

        public static ContractReference_VNACCS ToDataTransferContractReferences(KDT_VNACCS_BaoCaoQuyetToan_MMTB goodsItem, KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail goodsItem_Detail, string DiaChi, string TenDoanhNghiep, string Status)
        {
            ContractReference_VNACCS ContractReference_VNACCS = new ContractReference_VNACCS()
            {

                Reference = goodsItem.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                //Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = goodsItem.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            if (Status == "Send")
            {
                ContractReference_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else
            {
                ContractReference_VNACCS.Function = DeclarationFunction.SUA;
            }
            // Người Khai Hải Quan
            ContractReference_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = goodsItem.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ContractReference_VNACCS.Issuer =DeclarationIssuer.ContractReference;
            ContractReference_VNACCS.DeclarationOffice = goodsItem.MaHQ.Trim();
            ContractReference_VNACCS.CustomsReference = goodsItem.SoTN.ToString();
            ContractReference_VNACCS.Acceptance = goodsItem.NgayTN.Year == 1 ? DateTime.Now.ToString(sfmtDateTime) : goodsItem.NgayTN.ToString(sfmtDateTime);

            ContractReferences contractReferences = new ContractReferences()
            {
                ContractReference = new List<ContractReference>()
            };
            foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodsItem.GoodItemCollection)
	        {
                GoodsItems goodsItems = new GoodsItems()
                {
                    GoodsItem = new List<GoodsItem>()
                };
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail items in item.GoodItemDetailCollection)
                {
                    goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                    {
                        Commodity = new Commodity()
                        {
                            Sequence = items.STT.ToString(),
                            Description = items.TenHangHoa,
                            Identification = items.MaHangHoa,
                            TariffClassification = items.MaHS.ToString(),
                            Content = items.GhiChu,
                        },
                        GoodsMeasure = new GoodsMeasure()
                        {
                            QuantityTempImport = items.LuongTamNhap.ToString().Replace(",","."),
                            QuantityReExport = items.LuongTaiXuat.ToString().Replace(",", "."),
                            QuantityForward = new QuantityForward()
                            {
                                Quantity = items.LuongChuyenTiep,
                                Reference = items.SoHopDong,
                                Issue = items.NgayHopDong.ToString(sfmtDate),
                                DeclarationOffice = item.MaHQ,
                                Expire = items.NgayHetHan.ToString(sfmtDate),
                            },
                            quantityExcess = items.LuongConLai.ToString().Replace(",", "."),
                            MeasureUnit = items.DVT,
                        },
                    });
                }
                contractReferences.ContractReference.Add(new ContractReference
                    {
                        Sequence = item.STT,
                        Reference = item.SoHopDong,
                        Issue = item.NgayHopDong.ToString(sfmtDate),
                        DeclarationOffice = item.MaHQ,
                        Expire = item.NgayHetHan.ToString(sfmtDate),
                        AdditionalInformation = new AdditionalDocument()
                        {
                            Content = item.GhiChuKhac,
                        },
                        GoodsItems = goodsItems,
                    });
	        }
            ContractReference_VNACCS.ContractReferences = contractReferences;
            return ContractReference_VNACCS;
        }


        public static ContractReference_VNACCS ToDataTransferContractReferencesNew(KDT_VNACCS_BaoCaoQuyetToan_MMTB goodsItem, KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail goodsItem_Detail, string DiaChi, string TenDoanhNghiep, string Status)
        {
            ContractReference_VNACCS ContractReference_VNACCS = new ContractReference_VNACCS()
            {

                Reference = goodsItem.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                //Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = goodsItem.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            if (Status == "Send")
            {
                ContractReference_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else
            {
                ContractReference_VNACCS.Function = DeclarationFunction.SUA;
            }
            // Người Khai Hải Quan
            ContractReference_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = goodsItem.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ContractReference_VNACCS.Issuer = DeclarationIssuer.ContractReference;
            ContractReference_VNACCS.DeclarationOffice = goodsItem.MaHQ.Trim();
            ContractReference_VNACCS.CustomsReference = goodsItem.SoTN.ToString();
            ContractReference_VNACCS.Acceptance = DateTime.Now.ToString(sfmtDateTime);

            ContractReferences contractReferences = new ContractReferences()
            {
                ContractReference = new List<ContractReference>()
            };
            foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodsItem.GoodItemCollection)
            {
                goodsItem.GoodItemDetailCollection.Clear();
                goodsItem.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionDynamic("Contract_ID = " + item.ID, " ");
                GoodsItems goodsItems = new GoodsItems()
                {
                    GoodsItem = new List<GoodsItem>()
                };
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail items in goodsItem.GoodItemDetailCollection)
                {
                    goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                    {
                        Commodity = new Commodity()
                        {
                            Sequence = items.STT.ToString(),
                            Description = items.TenHangHoa,
                            Identification = items.MaHangHoa,
                            TariffClassification = items.MaHS.ToString(),
                            Content = items.GhiChu,
                        },
                        GoodsMeasure = new GoodsMeasure()
                        {
                            QuantityTempImport = items.LuongTamNhap.ToString().Replace(",","."),
                            QuantityReExport = items.LuongTaiXuat.ToString().Replace(",", "."),
                            QuantityForward = new QuantityForward()
                            {
                                Quantity = items.LuongChuyenTiep,
                                Reference = items.SoHopDong,
                                Issue = items.NgayHopDong.ToString(sfmtDate),
                                DeclarationOffice = item.MaHQ,
                                Expire = items.NgayHetHan.ToString(sfmtDate),
                            },
                            quantityExcess = items.LuongConLai.ToString().Replace(",","."),
                            MeasureUnit = items.DVT,
                        },
                    });
                }
                contractReferences.ContractReference.Add(new ContractReference
                {
                    Sequence = item.STT,
                    Reference = item.SoHopDong,
                    Issue = item.NgayHopDong.ToString(sfmtDate),
                    DeclarationOffice = item.MaHQ,
                    Expire = item.NgayHetHan.ToString(sfmtDate),
                    AdditionalInformation = new AdditionalDocument()
                    {
                        Content = item.GhiChuKhac,
                    },
                    GoodsItems = goodsItems,
                });
            }
            ContractReference_VNACCS.ContractReferences = contractReferences;
            return ContractReference_VNACCS;
        }

        public static ContractReference_VNACCS ToDataTransferContractReferencesForeignTraders(KDT_VNACCS_BaoCaoQuyetToan_MMTB goodsItem, KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail goodsItem_Detail, string DiaChi, string TenDoanhNghiep, string Status)
        {
            ContractReference_VNACCS ContractReference_VNACCS = new ContractReference_VNACCS()
            {

                Reference = goodsItem.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                //Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = goodsItem.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            if (Status == "Send")
            {
                ContractReference_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else
            {
                ContractReference_VNACCS.Function = DeclarationFunction.SUA;
            }
            // Người Khai Hải Quan
            ContractReference_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = goodsItem.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ContractReference_VNACCS.Issuer = DeclarationIssuer.ContractReference;
            ContractReference_VNACCS.DeclarationOffice = goodsItem.MaHQ.Trim();
            ContractReference_VNACCS.CustomsReference = goodsItem.SoTN.ToString();
            ContractReference_VNACCS.Acceptance = goodsItem.NgayTN.ToString(sfmtDateTime);
            ContractReference_VNACCS.StartDate = goodsItem.NgayTN.ToString(sfmtDate);
            ContractReference_VNACCS.FinishDate = goodsItem.NgayTN.ToString(sfmtDate);
            ContractReference_VNACCS.Type = "1";

            ContractReferences contractReferences = new ContractReferences()
            {
                ContractReference = new List<ContractReference>()
            };
            foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodsItem.GoodItemCollection)
            {
                goodsItem.GoodItemDetailCollection.Clear();
                goodsItem.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionDynamic("Contract_ID = " + item.ID, " ");
                GoodsItems goodsItems = new GoodsItems()
                {
                    GoodsItem = new List<GoodsItem>()
                };
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail items in goodsItem.GoodItemDetailCollection)
                {
                    goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                    {
                        Sequence = items.STT.ToString(),
                        DescriptionMaterial = items.TenHangHoa.ToString(),
                        IdentificationMaterial = items.MaHangHoa,
                        MeasureUnitMaterial = items.DVT,
                        QuantityBeginMaterial = items.LuongChuyenTiep,
                        QuantityImportMaterial = items.LuongChuyenTiep,
                        QuantityReExportMaterial = items.LuongChuyenTiep,
                        QuantityRePurposeMaterial = items.LuongChuyenTiep,
                        QuantityExportProduct = items.LuongChuyenTiep,
                        QuantityExportOther = items.LuongChuyenTiep,
                        QuantityExcessMaterial =items.LuongChuyenTiep,
                        //Commodity = new Commodity()
                        //{
                        //    Sequence = items.STT.ToString(),
                        //    Description = items.TenHangHoa,
                        //    Identification = items.MaHangHoa,
                        //    TariffClassification = items.MaHS.ToString(),
                        //    Content = items.GhiChu,
                        //},
                        //GoodsMeasure = new GoodsMeasure()
                        //{
                        //    QuantityTempImport = items.LuongTamNhap.ToString(),
                        //    QuantityReExport = items.LuongTaiXuat.ToString(),
                        //},
                        //QuantityForward = new QuantityForward()
                        //{
                        //    Quantity = items.LuongChuyenTiep,
                        //    Reference = items.SoHopDong,
                        //    Issue = items.NgayHopDong.ToString(sfmtDate),
                        //    DeclarationOffice = items.MaHQ,
                        //    Expire = items.NgayHetHan.ToString(sfmtDate),
                        //    QuantityExcess = items.LuongConLai,
                        //    MeasureUnit = items.DVT,
                        //},
                    });
                }
                contractReferences.ContractReference.Add(new ContractReference
                {
                    Sequence = item.STT,
                    Reference = item.SoHopDong,
                    Issue = item.NgayHopDong.ToString(sfmtDate),
                    DeclarationOffice = item.MaHQ,
                    Expire = item.NgayHetHan.ToString(sfmtDate),
                    AdditionalInformation = new AdditionalDocument()
                    {
                        Content = item.GhiChuKhac,
                    },
                    GoodsItems = goodsItems,
                });
            }
            ContractReference_VNACCS.ContractReferences = contractReferences;
            return ContractReference_VNACCS;
        }

        public static GoodsItems_VNACCS ToDataTransferGoodsItems(KDT_VNACCS_BaoCaoQuyetToan_NLVTTP goodsItem, KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail goodsItem_Detail, string DiaChi, string TenDoanhNghiep, string Status)
        {
            GoodsItems_VNACCS GoodsItems_VNACCS = new GoodsItems_VNACCS()
            {

                Reference = goodsItem.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                //Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                FinalYear = goodsItem.NamQuyetToan.ToString(),
                Importer = new NameBase()
                {
                    Identity = goodsItem.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            if (Status == "Send")
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            }
            else
            {
                GoodsItems_VNACCS.Function = DeclarationFunction.SUA;
            }
            // Người Khai Hải Quan
            GoodsItems_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = goodsItem.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            GoodsItems_VNACCS.Issuer = DeclarationIssuer.GoodsItems;
            GoodsItems_VNACCS.DeclarationOffice = goodsItem.MaHQ.Trim();
            GoodsItems_VNACCS.CustomsReference = goodsItem.SoTN.ToString();
            GoodsItems_VNACCS.Acceptance = goodsItem.NgayTN.ToString(sfmtDateTime);

            GoodsItems goodsItems = new GoodsItems()
            {
                GoodsItem = new List<GoodsItem>()
            };
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail item in goodsItem.GoodItemsCollection)
            {
                goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                {
                    Sequence = item.STT.ToString(),
                    Type = item.LoaiHangHoa.ToString(),
                    Account = item.TaiKhoan.ToString(),
                    Description = item.TenHangHoa,
                    Identification = item.MaHangHoa,
                    MeasureUnit = item.DVT,
                    QuantityMaterialExport = item.LuongNLVTXuatKhau,
                    QuantityMaterialOutsourcing = item.LuongNLVTTon,
                    QuantityProductImport = item.LuongSPGCNhapKhau,
                    QuantityProductExport = item.LuongSPGCBan,
                    QuantityExportLiquidate = new QuantityExportLiquidate()
                    {
                        QuantityDestruction = item.LuongTieuHuy,
                        QuantitySell = item.LuongBan,
                    },
                });
            }
            GoodsItems_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = goodsItem.GhiChuKhac
            };
            GoodsItems_VNACCS.AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
            {
                FileName = goodsItem.FileName,
                Content = goodsItem.Content
            };
            GoodsItems_VNACCS.GoodsItems = goodsItems;
            return GoodsItems_VNACCS;
        }

        public static TransportEquipment_VNACCS ToDataTransferTransportEquipment(KDT_VNACCS_TransportEquipment transportEquipment, KDT_VNACCS_TransportEquipment_Detail transportEquipmentDetail, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep)
        {
            TransportEquipment_VNACCS TransportEquipment_VNACCS = new TransportEquipment_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = transportEquipment.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = transportEquipment.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            TransportEquipment_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = transportEquipment.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            TransportEquipment_VNACCS.Issuer = DeclarationIssuer.TransportEquipment;
            TransportEquipment_VNACCS.DeclarationOffice = transportEquipment.MaHQ.Trim();
            TransportEquipment_VNACCS.CustomsReference = transportEquipment.SoTN.ToString();
            TransportEquipment_VNACCS.Acceptance = transportEquipment.NgayTN.ToString(sfmtDateTime);
            TransportEquipment_VNACCS.DeclarationDocument = new DeclarationDocument()
            {
                Reference = TKMD.SoToKhai.ToString(),
                IsExportImport = transportEquipment.CoBaoXNK,
                Issue = TKMD.NgayDangKy.ToString(sfmtDateTime),
                Clearance = TKMD.NgayCapPhep.ToString(sfmtDateTime),
                NatureOfTransaction = transportEquipment.MaLoaiHinhVanChuyen,
                DeclarationOffice = TKMD.CoQuanHaiQuan,
                DeclarationOfficeControl = transportEquipment.MaHQGiamSat,
                Transportation = transportEquipment.MaPTVC,
                PurposesTransport = transportEquipment.MaMucDichVC,
                StartDateOfTransport = transportEquipment.NgayBatDauVC.ToString(sfmtDateTime),
                ArrivalDateOfTransport = transportEquipment.NgayKetThucVC.ToString(sfmtDateTime),
            };
            TransportEquipment_VNACCS.ContractDocument = new Company.KDT.SHARE.Components.Messages.Vouchers.ContractDocument()
            {
                Issue = transportEquipment.NgayHopDongVC.ToString(sfmtDate),
                Reference = transportEquipment.SoHopDongVC,
                Expire = transportEquipment.NgayHetHanHDVC.ToString(sfmtDate),
            };
            TransportEquipment_VNACCS.LoadingLocation = new UnloadingLocation()
            {
                CustomsCode = transportEquipment.MaDiaDiemXepHang,
                CustomsName = transportEquipment.TenDiaDiemXepHang,
                Code = transportEquipment.MaViTriXepHang,
                PortCode = transportEquipment.MaCangCuaKhauGaXepHang,
            };
            TransportEquipment_VNACCS.UnloadingLocation = new UnloadingLocation()
            {
                CustomsCode = transportEquipment.MaDiaDiemDoHang,
                CustomsName = transportEquipment.TenDiaDiemDoHang,
                Code = transportEquipment.MaViTriDoHang,
                PortCode = transportEquipment.MaCangCuaKhauGaDoHang,
            };
            TransportEquipment_VNACCS.AdditionalInformation = new AdditionalDocument()
            {
                Content = transportEquipment.GhiChuKhac,
                Route = transportEquipment.TuyenDuong,
                Tel = transportEquipment.SoDienThoaiHQ,
                Fax = transportEquipment.SoFaxHQ,
                Name = transportEquipment.TenDaiDienDN,
                Time = transportEquipment.ThoiGianVC,
                Km = transportEquipment.SoKMVC.ToString(),
            };
            TransportEquipments transportEquipments = new TransportEquipments()
            {
                TransportEquipment = new List<TransportEquipment>()
            };
            foreach (KDT_VNACCS_TransportEquipment_Detail item in transportEquipment.TransportEquipmentCollection)
            {
                transportEquipments.TransportEquipment.Add(new TransportEquipment
                {
                    Container = item.SoContainer,
                    BillOfLading = item.SoVanDon,
                    Seal = item.SoSeal,
                    CustomsSeal = item.SoSealHQ,
                    Type = item.LoaiContainer,
                });
            }
            TransportEquipment_VNACCS.TransportEquipments = transportEquipments;
            return TransportEquipment_VNACCS;
        }

        public static ImportWareHouse ToDataTransferImportWareHouse(T_KDT_VNACCS_WarehouseImport warehouseImport, T_KDT_VNACCS_WarehouseImport_Detail warehouseImport_Detail, T_KDT_VNACCS_WarehouseImport_GoodsDetail warehouseImport_GoodsDetail, string TenDoanhNghiep)
        {
            ImportWareHouse ImportWareHouse = new ImportWareHouse()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = warehouseImport.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,                
                Importer = new NameBase()
                {
                    Identity = warehouseImport.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Warehouse = new NameBase()
                {
                    Name = warehouseImport.TenKho,
                    Identity = warehouseImport.MaKho
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            ImportWareHouse.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = warehouseImport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ImportWareHouse.Issuer = DeclarationIssuer.ImportWareHouse;
            ImportWareHouse.DeclarationOffice = "01TE";//warehouseImport.MaHQ.Trim();
            ImportWareHouse.CustomsReference = warehouseImport.SoTN.ToString();
            ImportWareHouse.Acceptance = string.Empty;//; warehouseImport.NgayTN.ToString(sfmtDate);
            ImportWareHouse.StartDate = warehouseImport.NgayBatDauBC.ToString(sfmtDate);
            ImportWareHouse.FinishDate = warehouseImport.NgayKetthucBC.ToString(sfmtDate);
            AdditionalDocuments additionalDocuments = new AdditionalDocuments()
            {
                AdditionalDocument = new List<AdditionalDocument>(),
            };
            foreach (T_KDT_VNACCS_WarehouseImport_Detail item in warehouseImport.WarehouseImportDetailsCollection)
            {
                additionalDocuments.AdditionalDocument.Add(new AdditionalDocument
                {
                    Sequence = item.STT.ToString(),
                    Identification = item.SoPhieuNhap,
                    Issue = item.NgayPhieuNhap.ToString(sfmtDate),
                    NameConsignor = item.TenNguoiGiaoHang,
                    IdentityConsignor = item.MaNguoiGiaoHang,
                    CustomsGoodsItem = T_KDT_VNACCS_WarehouseImport_GoodsDetail.SelectCollectionBy_WarehouseImport_Details_ID_New(item.ID),
                    //AdditionalInformation = new AdditionalInformation()
                    //{
                    //    Content = new Content() 
                    //    {
                    //        Text = warehouseImport.GhiChuKhac,
                    //    },
                    //},
                });
            }
            ImportWareHouse.AdditionalDocumentNew = additionalDocuments;
            ImportWareHouse.DeclarationDocument = new DeclarationDocument()
            {
                Reference = warehouseImport.SoTKChungTu.ToString(),
                Type = warehouseImport.Loai.ToString()
            };
            ImportWareHouse.ContractReference = new ContractReference()
            {
                Reference= "SOFTECH NEW",
                Issue="2018-03-16",
                DeclarationOffice="01TE",
                Expire = "2019-03-16",
                
            };
            //ImportWareHouse.AdditionalInformations = new AdditionalInformation()
            //{
            //    Content = new Content
            //    {
            //        Text = warehouseImport.GhiChuKhac,
            //    }

            //};
            return ImportWareHouse;
        }
        public static GC_ImportWareHouse ToDataTransferImportWareHouseNew(T_KDT_VNACCS_WarehouseImport warehouseImport, T_KDT_VNACCS_WarehouseImport_Detail warehouseImport_Detail, T_KDT_VNACCS_WarehouseImport_GoodsDetail warehouseImport_GoodsDetail, string TenDoanhNghiep,string Status)
        {
            //bool Status;
            GC_ImportWareHouse ImportWareHouse = new GC_ImportWareHouse()
            {

                Issuer = DeclarationIssuer.ImportWareHouse,
                Reference = warehouseImport.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                //Function = DeclarationFunction.HUY,
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = warehouseImport.SoTN.ToString(),
                Acceptance =  warehouseImport.NgayTN.ToString(sfmtDate),
                DeclarationOffice = warehouseImport.MaHQ,
                StartDate = warehouseImport.NgayBatDauBC.ToString(sfmtDate),
                FinishDate = warehouseImport.NgayKetthucBC.ToString(sfmtDate),
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Identity = warehouseImport.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Warehouse = new NameBase()
                {
                    Name = warehouseImport.TenKho,
                    Identity = warehouseImport.MaKho
                },
            };
            if (Status == "Send")
            {
                ImportWareHouse.Function = DeclarationFunction.KHAI_BAO;
            }
            else if (Status == "Edit")
            {
                ImportWareHouse.Function = DeclarationFunction.SUA;
            }
            else
            {
                ImportWareHouse.Function = DeclarationFunction.HUY;
            }
            // Người Khai Hải Quan
            ImportWareHouse.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = warehouseImport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            PNK_AdditionalDocuments additionalDocuments = new PNK_AdditionalDocuments()
            {
                AdditionalDocument = new List<PNK_AdditionalDocument>(),
            };
            
            foreach (T_KDT_VNACCS_WarehouseImport_Detail item in warehouseImport.WarehouseImportDetailsCollection)
            {
                int i = 1;
                additionalDocuments.AdditionalDocument.Add(new PNK_AdditionalDocument
                {
                    Sequence = i++.ToString(),
                    Identification = item.SoPhieuNhap,
                    Issue = item.NgayPhieuNhap.ToString(sfmtDate),
                    NameConsignor = item.TenNguoiGiaoHang,
                    IdentityConsignor = item.MaNguoiGiaoHang,
                    CustomsGoodsItem = T_KDT_VNACCS_WarehouseImport_GoodsDetail.SelectCollectionBy_WarehouseImport_Details_ID_Update(item.ID,warehouseImport),
                    AdditionalInformation = new PNK_AdditionalInformation() 
                    {
                        Content = warehouseImport.GhiChuKhac,
                    }
                });
            }
            ImportWareHouse.AdditionalDocuments = additionalDocuments;
            //ImportWareHouse.ContractReference = new PNK_ContractReference()
            //{
            //    Reference = "SOFTECH NEW",
            //    Issue = "2018-03-16",
            //    DeclarationOffice = "01TE",
            //    Expire = "2019-03-16",

            //};
            return ImportWareHouse;
        }

        public static ExportWareHouse ToDataTransferExportWareHouse(T_KDT_VNACCS_WarehouseExport warehouseExport, T_KDT_VNACCS_WarehouseExport_Detail warehouseExport_Detail, T_KDT_VNACCS_WarehouseExport_GoodsDetail warehouseExport_GoodsDetail, string TenDoanhNghiep)
        {
            ExportWareHouse ExportWareHouse = new ExportWareHouse()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = warehouseExport.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = warehouseExport.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Warehouse = new NameBase()
                {
                    Name = warehouseExport.TenKho,
                    Identity = warehouseExport.MaKho
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            ExportWareHouse.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = warehouseExport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ExportWareHouse.Issuer = DeclarationIssuer.ImportWareHouse;
            ExportWareHouse.DeclarationOffice = warehouseExport.MaHQ.Trim();
            ExportWareHouse.CustomsReference = warehouseExport.SoTN.ToString();
            ExportWareHouse.Acceptance = warehouseExport.NgayTN.ToString(sfmtDateTime);
            ExportWareHouse.StartDate = warehouseExport.NgayBatDauBC.ToString(sfmtDate);
            ExportWareHouse.FinishDate = warehouseExport.NgayKetthucBC.ToString(sfmtDate);
            AdditionalDocuments additionalDocuments = new AdditionalDocuments()
            {
                AdditionalDocument = new List<AdditionalDocument>(),
            };

            foreach (T_KDT_VNACCS_WarehouseExport_Detail item in warehouseExport.WarehouseExportCollection)
            {
                additionalDocuments.AdditionalDocument.Add (new AdditionalDocument
                {
                    Sequence = item.STT.ToString(),
                    Identification = item.SoPhieuXuat,
                    Issue = item.NgayPhieuXuat.ToString(sfmtDate),
                    NameConsignee = item.TenNguoiNhanHang,
                    IdentityConsignee = item.MaNguoiNhanHang,
                    CustomsGoodsItem =T_KDT_VNACCS_WarehouseExport_GoodsDetail.SelectCollectionBy_WarehouseExport_Details_ID_New(item.ID),
                });               
            }
            ExportWareHouse.AdditionalDocumentNew = additionalDocuments;
            ExportWareHouse.DeclarationDocument = new DeclarationDocument()
            {
                Reference = warehouseExport.SoTKChungTu.ToString(),
                Type = warehouseExport.Loai.ToString()
            };
            ExportWareHouse.ContractReference = new ContractReference()
            {
                Reference = "",
                Issue = "",
                DeclarationOffice = "",
                Expire = "",

            };
            ExportWareHouse.AdditionalInformationNew = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = warehouseExport.GhiChuKhac,
            };
            return ExportWareHouse;
        }
        public static GoodsItems_VNACCS ToDataTransferTotalInventoryReport(KDT_VNACCS_TotalInventoryReport totalInventoryReport, KDT_VNACCS_TotalInventoryReport_Detail totalInventoryRepor_Detail, string TenDoanhNghiep, string DiaChi)
        {
            GoodsItems_VNACCS GoodsItems_VNACCS = new GoodsItems_VNACCS()
            {

                Function = DeclarationFunction.SUA,
                Reference = totalInventoryReport.GuidString,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = totalInventoryReport.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            GoodsItems_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = totalInventoryReport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            GoodsItems_VNACCS.FinishDate = totalInventoryReport.NgayChotTon.ToString(sfmtDate);
            GoodsItems_VNACCS.UpdateType = "1";
            GoodsItems_VNACCS.Type = totalInventoryReport.LoaiBaoCao.ToString();
            GoodsItems_VNACCS.Issuer = DeclarationIssuer.TotalInventoryReport_GC;
            GoodsItems_VNACCS.DeclarationOffice = totalInventoryReport.MaHaiQuan.Trim();
            GoodsItems_VNACCS.CustomsReference = totalInventoryReport.SoTiepNhan.ToString();
            GoodsItems_VNACCS.Acceptance = totalInventoryReport.NgayTiepNhan.ToString(sfmtDateTime);
            ContractReferences contractReferences = new ContractReferences
            {
                ContractReference = new List<ContractReference>()
            };
            GoodsItems goodsItems = new GoodsItems
            {
                GoodsItem = new List<GoodsItem>()
            };
            foreach (KDT_VNACCS_TotalInventoryReport_Detail item in totalInventoryReport.DetailCollection)
            {
                goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                {
                    Description = item.TenHangHoa,
                    Identification = item.MaHangHoa,
                    Quantity1 = Decimal.Round(item.SoLuongTonKhoSoSach, 4, MidpointRounding.AwayFromZero),
                    Quantity2 = Decimal.Round(item.SoLuongTonKhoThucTe, 4, MidpointRounding.AwayFromZero),
                    MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT),
                }); 
            }
            contractReferences.ContractReference.Add(new ContractReference
            {
                Reference = "SOFTECH NEW",
                Issue = "2018-03-16",
                DeclarationOffice = "01TE",
                Expire = "2019-03-16",
                GoodsItems = goodsItems,
                AdditionalInformation = new AdditionalDocument()
                {
                    Content = totalInventoryReport.GhiChuKhac,
                }
            });
            //GoodsItems_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            //{
            //    Content = totalInventoryReport.GhiChuKhac
            //};
            GoodsItems_VNACCS.ContractReferences = contractReferences;
            //GoodsItems_VNACCS.GoodsItems = goodsItems;
            return GoodsItems_VNACCS;
        }

        public static GoodsItems_VNACCS ToDataTransferTotalInventoryReportNew(Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport totalInventoryReport, Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail totalInventoryRepor_Detail, string TenDoanhNghiep, string DiaChi)
        {
            GoodsItems_VNACCS GoodsItems_VNACCS = new GoodsItems_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = totalInventoryReport.GuidString,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = totalInventoryReport.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = DiaChi,
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            GoodsItems_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = totalInventoryReport.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            GoodsItems_VNACCS.FinishDate = totalInventoryReport.NgayChotTon.ToString(sfmtDate);
            GoodsItems_VNACCS.UpdateType = totalInventoryReport.LoaiSua.ToString();
            GoodsItems_VNACCS.Type = totalInventoryReport.LoaiBaoCao.ToString();
            GoodsItems_VNACCS.Issuer = DeclarationIssuer.TotalInventoryReport_GC;
            GoodsItems_VNACCS.DeclarationOffice = totalInventoryReport.MaHaiQuan.Trim();
            GoodsItems_VNACCS.CustomsReference = totalInventoryReport.SoTiepNhan.ToString();
            GoodsItems_VNACCS.Acceptance = totalInventoryReport.NgayTiepNhan.ToString(sfmtDateTime);
            ContractReferences contractReferences = new ContractReferences
            {
                ContractReference = new List<ContractReference>()
            };
            foreach (Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_ContractReference contractReference in totalInventoryReport.ContractCollection)
            {
                GoodsItems goodsItems = new GoodsItems
                {
                    GoodsItem = new List<GoodsItem>()
                };
                foreach (Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail item in contractReference.DetailCollection)
                {
                    goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
                    {
                        Description = item.TenHangHoa,
                        Identification = item.MaHangHoa,
                        Quantity1 = Decimal.Round(item.SoLuongTonKhoSoSach, 4, MidpointRounding.AwayFromZero),
                        Quantity2 = Decimal.Round(item.SoLuongTonKhoThucTe, 4, MidpointRounding.AwayFromZero),
                        MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT),
                    });
                }
                contractReferences.ContractReference.Add(new ContractReference
                {        
                    Reference = contractReference.SoHopDong.ToString(),
                    Issue = contractReference.NgayHopDong.ToString(sfmtDate),
                    DeclarationOffice = totalInventoryReport.MaHaiQuan,
                    Expire = contractReference.NgayHetHan.ToString(sfmtDate),
                    GoodsItems = goodsItems,
                    AdditionalInformation = new AdditionalDocument()
                    {
                        Content = totalInventoryReport.GhiChuKhac,
                    }
                });
            }
            //GoodsItems_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            //{
            //    Content = totalInventoryReport.GhiChuKhac
            //};
            GoodsItems_VNACCS.ContractReferences = contractReferences;
            return GoodsItems_VNACCS;
        }
        public static BillOfLadingNew_VNACCS ToDataTransferBillOfLadingNew(KDT_VNACCS_BillOfLadingNew billOfLading)
        {

            BillOfLadingNew_VNACCS BillOfLading_VNACCS = new BillOfLadingNew_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = billOfLading.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = billOfLading.MaDoanhNghiep,
                    Name = billOfLading.TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            BillOfLading_VNACCS.Agents.Add(new Agent
            {
                Name = billOfLading.TenDoanhNghiep,
                Identity = billOfLading.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            BillOfLading_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            BillOfLading_VNACCS.CustomsReference = string.Empty;
            BillOfLading_VNACCS.Acceptance = string.Empty;
            BillOfLading_VNACCS.Issuer = DeclarationIssuer.BillOfLadingNew;
            BillOfLading_VNACCS.DeclarationOffice = billOfLading.MaHQ.Trim();

            BillOfLadingNews billOfLadings = new BillOfLadingNews()
            {
                BillOfLading = new List<BillOfLadingNew>(),
            };

            foreach (KDT_VNACCS_BillOfLadings_Detail item in billOfLading.Collection)
            {
                BranchDetails branchDetails = new BranchDetails()
                {
                    BranchDetail = new List<BranchDetail>(),
                };
                List<KDT_VNACCS_BranchDetail> collection = KDT_VNACCS_BranchDetail.SelectCollectionBy_BillOfLadings_Details_ID(item.ID);
                foreach (KDT_VNACCS_BranchDetail branchDetail in collection)
                {
                    BranchDetail_Containers transportEquipments = new BranchDetail_Containers()
                    {
                        TransportEquipment = new List<BranchDetail_Container>()
                    };
                    List<KDT_VNACCS_BranchDetail_TransportEquipment> TransportEquipmentCollction = KDT_VNACCS_BranchDetail_TransportEquipment.SelectCollectionBy_BranchDetail_ID(branchDetail.ID);
                    foreach (KDT_VNACCS_BranchDetail_TransportEquipment transportEquipment in TransportEquipmentCollction)
                    {
                        transportEquipments.TransportEquipment.Add(new BranchDetail_Container
                        {
                            Container = transportEquipment.SoContainer,
                            Seal = transportEquipment.SoSeal
                        });
                    }
                    branchDetails.BranchDetail.Add(new BranchDetail
                    {
                        Sequence = branchDetail.STT.ToString(),
                        Reference = branchDetail.SoVanDon,
                        ShipperName = branchDetail.TenNguoiGuiHang,
                        ShipperAddress = branchDetail.DiaChiNguoiGuiHang,
                        ConsigneeName = branchDetail.TenNguoiNhanHang,
                        ConsigneeAddress = branchDetail.DiaChiNguoiNhanHang,
                        NumberOfCont = branchDetail.TongSoLuongContainer.ToString(),
                        CargoPiece = branchDetail.SoLuongHang.ToString(),
                        PieceUnitCode = branchDetail.DVTSoLuong,
                        CargoWeight = branchDetail.TongTrongLuongHang.ToString().Replace(",","."),
                        WeightUnitCode = branchDetail.DVTTrongLuong,
                        TransportEquipments = transportEquipments
                    });
                }
                billOfLadings.BillOfLading.Add(new BillOfLadingNew
                {
                    Reference = item.SoVanDonGoc,
                    Issue = item.NgayVanDonGoc.ToString(sfmtDate),
                    Issuer = item.MaNguoiPhatHanh,
                    Number = item.SoLuongVDN.ToString(),
                    Type = item.PhanLoaiTachVD.ToString(),
                    IsContainer = item.LoaiHang.ToString(),
                    BranchDetails = branchDetails
                });
            }
            BillOfLading_VNACCS.BillOfLadings = billOfLadings;
            return BillOfLading_VNACCS;
        }

        #region Khai báo Thu phí cảng HP
        public static Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information ToDataTransferRegisterInformation(T_KDT_THUPHI_DOANHNGHIEP customer, string TenDoanhNghiep)
        {
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information Register_Information = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information()
            {
                Issuer = DeclarationIssuer.RegisterInformation,
                Reference = customer.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                Company = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Company()
                {
                    Name = customer.TenDoanhNghiep,
                    Identity = customer.MaDoanhNghiep,
                    Address =  customer.DiaChi,
                    Contact = customer.NguoiLienHe,
                    Email = customer.Email,
                    Phone = customer.SoDienThoai,
                },
                SignDigital = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.SignDigital()
                {
                    Serial =  customer.Serial,
                    CertString = customer.CertString,
                    Subject = customer.Subject,
                    Issuer = customer.Issuer,
                    ValidFrom = customer.ValidFrom.ToString(sfmtDate),
                    ValidTo = customer.ValidTo.ToString(sfmtDate),
                }
            };
            return Register_Information;
        }

        public static Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_HangContainer ToDataTransferRegisterTK(T_KDT_THUPHI_TOKHAI TK, string TenDoanhNghiep)
        {
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_HangContainer Register_HangContainer = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_HangContainer()
            {
                Issuer = TK.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi,
                Reference = TK.GuidStr.ToUpper(),
                Issue = DateTime.Now.ToString(sfmtDateNoTime),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = String.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = "",
                Acceptance = "",
                DeclarationOffice = TK.MaDiaDiemLuuKho,
                Agent = new Agent()
                {
                    Name = TK.TenDoanhNghiep,
                    Identity = TK.MaDoanhNghiep,
                },
                Importer = new NameBase()
                {
                    Name = TK.TenDoanhNghiep,
                    Identity = TK.MaDoanhNghiep,
                },                
                NoticeOfPayment = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.NoticeOfPayment()
                {
                    Notice = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Notice()
                    {
                        NoticeNo = TK.SoTKNP,
                        NoticeDate = TK.NgayTKNP.ToString(sfmtDate),
                        PaymentMethod = TK.HinhThucTT,
                    },
                    CustomsReference = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.CustomsReference()
                    {
                        CustomsDeclare = TK.SoTK,
                        CustomsDeclareDate = TK.NgayTK.ToString(sfmtDate),
                        CustomsDeclareType = TK.MaLoaiHinh,
                        CustomsCode = TK.MaHQ,
                        TariffTypeCode = TK.NhomLoaiHinh,
                        TransportCode = TK.MaPTVC.ToString(),
                        DestinationCode = TK.MaDiaDiemLuuKho,
                    },
                    AdditionalInformation = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_AdditionalInformation() 
                    {
                        Content =  String.IsNullOrEmpty(TK.GhiChu) ? "" : TK.GhiChu,
                    }                    
                }
            };

            Register_HangContainer.Issuer = TK.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi;

            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipments transportEquipments = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipments()
            {
                TransportEquipment = new List<Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipment>()
            };
            if (TK.LoaiHangHoa == 100)
            {
                foreach (T_KDT_THUPHI_TOKHAI_DSCONTAINER item in TK.ContainerCollection)
                {
                    transportEquipments.TransportEquipment.Add(new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipment
                    {
                        BillOfLading = item.SoVanDon,
                        Container = item.SoContainer,
                        Seal = item.SoSeal,
                        ContainerType = item.Loai.ToString(),
                        ContainerKind = item.TinhChat.ToString(),
                        Quantity = item.SoLuong.ToString(),
                        JournalMemo = item.GhiChu,
                    });
                }
            }
            else
            {
                foreach (T_KDT_THUPHI_TOKHAI_DSCONTAINER item in TK.ContainerCollection)
                {
                    transportEquipments.TransportEquipment.Add(new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipment
                    {
                        BillOfLading = item.SoVanDon,
                        GrossMass = item.TongTrongLuong,
                        MeasureUnit = item.DVT,
                        JournalMemo = item.GhiChu,
                    });
                }
            }
            Register_HangContainer.NoticeOfPayment.TransportEquipments = transportEquipments;

            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.FileAttachs fileAttachs = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.FileAttachs()
            {
                File = new List<Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.File>()
            };
            foreach (T_KDT_THUPHI_TOKHAI_FILE item in TK.FileCollection)
            {
                fileAttachs.File.Add(new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.File
                {
                    Type = item.Loai,
                    Name = item.TenFile,
                    Content = item.Content,
                });
            }
            Register_HangContainer.NoticeOfPayment.FileAttachs = fileAttachs;
            return Register_HangContainer;
        }

        public static Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_InfomationSearch ToDataTransferSearchInformation(T_KDT_THUPHI_BIENLAI BL, string TenDoanhNghiep, string LoaiHangHoa, string SoBienLai, string SoToKhai, string SoVanDon, string SoContainer)
        {
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_InfomationSearch InfomationSearch = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_InfomationSearch()
            {
                Issuer = DeclarationIssuer.SearchInformation,
                Reference = BL.GuidString,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = "",
                Acceptance = "",
                DeclarationOffice = "03CC",
                Agent = new Agent()
                {
                    Name = "Chi cục HQ CK cảng Hải Phòng KV I",
                    Identity ="03CC",
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                },
                InfomationSearch = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.InfomationSearch() 
                {
                    GoodItemType= LoaiHangHoa,
                    ReceiptNo = SoBienLai,
                    CustomsReference = SoToKhai,
                    BillOfLading = SoVanDon,
                    ContainerNo = SoContainer,
                },
            };
            return InfomationSearch;
        }
        #endregion
    }

}
