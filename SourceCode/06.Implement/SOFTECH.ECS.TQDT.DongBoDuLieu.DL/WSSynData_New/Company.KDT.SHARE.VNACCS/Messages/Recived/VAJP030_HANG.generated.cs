using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAJP030_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute C1 { get; set; }
public PropertiesAttribute C2 { get; set; }
public PropertiesAttribute C3 { get; set; }
public PropertiesAttribute C4 { get; set; }
public PropertiesAttribute C5 { get; set; }
public PropertiesAttribute PMD { get; set; }
public PropertiesAttribute C7 { get; set; }
public PropertiesAttribute C8 { get; set; }
public PropertiesAttribute C9 { get; set; }
public PropertiesAttribute C10 { get; set; }
public PropertiesAttribute C11 { get; set; }
public PropertiesAttribute C12 { get; set; }
public PropertiesAttribute C13 { get; set; }
public PropertiesAttribute C14 { get; set; }
public PropertiesAttribute C15 { get; set; }
public PropertiesAttribute C6 { get; set; }
public PropertiesAttribute C16 { get; set; }

public VAJP030_HANG()
        {
C1 = new PropertiesAttribute(768, typeof(string));
C2 = new PropertiesAttribute(12, typeof(string));
C3 = new PropertiesAttribute(1, typeof(string));
C4 = new PropertiesAttribute(3, typeof(int));
C5 = new PropertiesAttribute(150, typeof(string));
PMD = new PropertiesAttribute(50, typeof(string));
C7 = new PropertiesAttribute(8, typeof(int));
C8 = new PropertiesAttribute(3, typeof(string));
C9 = new PropertiesAttribute(10, typeof(string));
C10 = new PropertiesAttribute(10, typeof(int));
C11 = new PropertiesAttribute(3, typeof(string));
C12 = new PropertiesAttribute(10, typeof(int));
C13 = new PropertiesAttribute(3, typeof(string));
C14 = new PropertiesAttribute(8, typeof(int));
C15 = new PropertiesAttribute(3, typeof(string));
C6 = new PropertiesAttribute(100, typeof(string));
C16 = new PropertiesAttribute(300, typeof(string));
TongSoByte = 1476;
}

}public partial class EnumGroupID
    {
}

}
