using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Logger;

namespace Company.Interface.Report.SXXK
{
    public partial class BaoCaoTonSauThanhKhoan : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        private int STTBanLuu = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        private int SoToKhaiNhap = 0;
        private int SoToKhaiXuat = 0;
        private decimal temp = 0;

        private string MaNPLTemp = "";
        private string MaNPLBanLuu = "";

        private string MaSPTemp = "";
        private string MaSPBanLuu = "";

        private decimal _TongLuongNhap = 0;
        private decimal _TongLuongTonDau = 0;
        private decimal _TongLuongNPLSuDung = 0;
        private decimal _TongLuongTonCuoi = 0;
        private decimal _TongLuongNPLTaiXuat = 0;
        private float fontReport = 6;
        private string DATETIME_FORMAT = "dd/MM/yyyy";

        public int NamThanhLy;
        public string MaDoanhNghiep;
        DataTable dataSource = new DataTable();

        public BaoCaoTonSauThanhKhoan()
        {
            InitializeComponent();

            fontReport = GlobalSettings.FontBCXNT;
        }
        public void BindingReport()
        {
            try
            {
                DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontBCXNT);

                lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
                lblDiaChi.Text = GlobalSettings.DIA_CHI;
                //lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
                //if (!string.IsNullOrEmpty(GlobalSettings.NoiDungSoHoSoThanhKhoan))
                //{
                //    lblSHSTK.Text = lblSHSTK.Text + GlobalSettings.NoiDungSoHoSoThanhKhoan;
                //}
                lblSHSTK.Width = 300;

                dataSource = GetDataSource();
                decimal TongThueTon = 0;
                foreach (DataRow dr in dataSource.Rows)
                {
                    if (dr["ThueTon"] != null)
                        TongThueTon += System.Convert.ToDecimal(dr["ThueTon"]);
                }


                DetailReport.DataSource = dataSource;
                lblSoTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                lblDonGia.DataBindings.Add("Text", DetailReport.DataSource, "DonGia", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan, true));
                lblDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
                lblSoToKhaiNhapKhau.DataBindings.Add("Text", DetailReport.DataSource, "SoToKhai");
                lblTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
                lblNgayDangKy.DataBindings.Add("Text", DetailReport.DataSource, "NgayDangKy");
                lblMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MaNPL");
                lblLuongTonCuoi.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan,true));
                lblThueTon.DataBindings.Add("Text", DetailReport.DataSource, "ThueTon",Company.KDT.SHARE.Components.Globals.FormatNumber(0));
                //HQ
                lblNgayThangHQ.Text = Properties.Settings.Default.TieudeNgay;
                //DN
                lblNgayThangDN.Text = GlobalSettings.TieudeNgay;
                lblTongThue.Text = TongThueTon.ToString("N0");
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
            
        }
        private DataTable GetDataSource()
        {
            try
            {
                string spName = "[p_SXXK_BCTonSauThanhKhoan]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.BigInt, LanThanhLy);
                db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, NamThanhLy);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
      
    }
}

     