﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using System.Threading;
using System.Collections;
using System.Text;
using Company.GC.BLL.KDT.SXXK;
using System.Data.Common;
using System.Collections.Generic;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Utils;
using ToKhaiTriGiaPP23 = Company.KD.BLL.KDT.ToKhaiTriGiaPP23;
using ToKhaiTriGia = Company.KD.BLL.KDT.ToKhaiTriGia;

namespace Company.GC.BLL.KDT
{
    public partial class ToKhaiMauDich
    {
        private List<HangMauDich> _HMDCollection = new List<HangMauDich>();
        private ChungTuCollection _chungtuCollection = new ChungTuCollection();
        private List<PhanBoToKhaiXuat> _PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
        public List<ChungTuKem> listCTDK = new List<ChungTuKem>();
        public List<ChungTuNo> ChungTuNoCollection = new List<ChungTuNo>();
        private Company.KD.BLL.KDT.ToKhaiTriGiaCollection _TKTGCollection = new Company.KD.BLL.KDT.ToKhaiTriGiaCollection();
        private Company.KD.BLL.KDT.ToKhaiTriGiaPP23Collection _TKTGPP23Collection = new Company.KD.BLL.KDT.ToKhaiTriGiaPP23Collection();
        public Company.KDT.SHARE.VNACCS.CapSoToKhai TrangThaiVNACCS { get; set; }
        // Tờ khai được bảo lãnh thuế -- thông tin
        public BaoLanhThue BaoLanhThue { get; set; }

        //Thong Bao Thue By DATLMQ 24/01/2011
        public bool IsThongBaoThue = false;
        public string SoQD = "";
        public DateTime NgayQD = new DateTime();
        public DateTime NgayHetHan = new DateTime();
        public string TaiKhoanKhoBac = "";
        public string TenKhoBac = "";
        //Thue XNK
        public string ChuongThueXNK = "";
        public string LoaiThueXNK = "";
        public string KhoanThueXNK = "";
        public string MucThueXNK = "";
        public string TieuMucThueXNK = "";
        public double SoTienThueXNK = 0;
        //Thue VAT
        public string ChuongThueVAT = "";
        public string LoaiThueVAT = "";
        public string KhoanThueVAT = "";
        public string MucThueVAT = "";
        public string TieuMucThueVAT = "";
        public double SoTienThueVAT = 0;
        //Thue TTDB
        public string ChuongThueTTDB = "";
        public string LoaiThueTTDB = "";
        public string KhoanThueTTDB = "";
        public string MucThueTTDB = "";
        public string TieuMucThueTTDB = "";
        public double SoTienThueTTDB = 0;
        //Thue TVCBPG
        public string ChuongThueTVCBPG = "";
        public string LoaiThueTVCBPG = "";
        public string KhoanThueTVCBPG = "";
        public string MucThueTVCBPG = "";
        public string TieuMucThueTVCBPG = "";
        public double SoTienThueTVCBPG = 0;
        List<GiayNopTien> _giayNopTiens = new List<GiayNopTien>();
        public List<GiayNopTien> GiayNopTiens { get { return _giayNopTiens; } set { _giayNopTiens = value; } }
        public void LoadListGiayNopTiens()
        {
            _giayNopTiens = GiayNopTien.SelectCollectionDynamic("TKMD_ID=" + ID, "");
        }


        #region Lệ phí hải quan
        private List<LePhiHQ> _LePhiHQCollection = new List<LePhiHQ>();
        public List<LePhiHQ> LePhiHQCollection { get { return _LePhiHQCollection; } set { _LePhiHQCollection = value; } }
        public void LoadListLePhiHQ()
        {
            _LePhiHQCollection = LePhiHQ.SelectCollectionDynamic("TKMD_ID=" + ID, "");
        }
        #endregion


        #region Ân hạng thuế

        AnHanThue _AnHanThue = new AnHanThue();
        public AnHanThue AnHanThue { get { return _AnHanThue; } set { _AnHanThue = value; } }
        public void LoadAnHanThue()
        {
            this._AnHanThue = new AnHanThue();
            List<AnHanThue> listAnHanThue = (List<AnHanThue>)AnHanThue.SelectCollectionBy_TKMD_ID(this.ID);
            if (listAnHanThue != null && listAnHanThue.Count > 0)
                this._AnHanThue = listAnHanThue[0];
        }

        #endregion


        #region Đảm bảo nghĩa vụ nộp thuế

        DamBaoNghiaVuNT _damBaoNghiaVuNopThue = new DamBaoNghiaVuNT();
        public DamBaoNghiaVuNT DamBaoNghiaVuNopThue { get { return _damBaoNghiaVuNopThue; } set { _damBaoNghiaVuNopThue = value; } }

        public void LoadDamBaoNghiaVuNT()
        {
            this._damBaoNghiaVuNopThue = new DamBaoNghiaVuNT();
            List<DamBaoNghiaVuNT> listDamBaoNghiaVuNopThue = (List<DamBaoNghiaVuNT>)DamBaoNghiaVuNT.SelectCollectionBy_TKMD_ID(this.ID);
            if (listDamBaoNghiaVuNopThue != null && listDamBaoNghiaVuNopThue.Count > 0)
                this._damBaoNghiaVuNopThue = listDamBaoNghiaVuNopThue[0];
        }

        #endregion


        #region Chứng thư giám định
        ChungThuGiamDinh _ChungThuGD = new ChungThuGiamDinh();
        public ChungThuGiamDinh ChungThuGD { get { return _ChungThuGD; } set { this._ChungThuGD = value; } }
        public void LoadChungThuGD()
        {
            IList<ChungThuGiamDinh> listChungThuGD = ChungThuGiamDinh.SelectCollectionBy_TKMD_ID(this.ID);
            if (listChungThuGD != null && listChungThuGD.Count > 0)
            {
                _ChungThuGD = listChungThuGD[0];
                _ChungThuGD.LoadHang();
            }

        }
        #endregion


        #region Giấy kiểm tra

        public List<GiayKiemTra> GiayKiemTraCollection = new List<GiayKiemTra>();
        public void LoadGiayKiemTra()
        {
            GiayKiemTraCollection = new List<GiayKiemTra>();
            if (ID > 0)
            {
                GiayKiemTraCollection = (List<GiayKiemTra>)GiayKiemTra.SelectCollectionBy_TKMD_ID(this.ID);
                foreach (GiayKiemTra gkt in GiayKiemTraCollection)
                {
                    gkt.LoadFull();
                }
            }
        }
        #endregion

        #region Số container
        public SoContainer SoLuongContainer { get; set; }
        public void LoadSoCont()
        {
            SoLuongContainer = new SoContainer();
            if (this.ID > 0)
            {
                List<SoContainer> socont = (List<SoContainer>)SoContainer.SelectCollectionBy_TKMD_ID(this.ID);
                if (socont != null && socont.Count > 0)
                    SoLuongContainer = socont[0];
                else
                {
                    VanDon vd = new VanDon();
                    List<VanDon> listvd = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
                    if (listvd != null && listvd.Count > 0)
                    {
                        vd = listvd[0];
                        SoLuongContainer.LoadSoContFromVanDon(vd);
                    }
                    else
                        SoLuongContainer = new SoContainer { Cont20 = Convert.ToInt32(SoContainer20), Cont40 = Convert.ToInt32(SoContainer40), Cont45 = 0, ContKhac = 0, MoTaKhac = string.Empty };
                }
            }

        }

        #endregion

        #region Bao lanh thue
        public void LoadBaoLanhThue()
        {
            try
            {
                if (this.ID > 0)
                {
                    IList<Company.KDT.SHARE.QuanLyChungTu.BaoLanhThue> listBLT = Company.KDT.SHARE.QuanLyChungTu.BaoLanhThue.SelectCollectionDynamic("TKMD_ID = " + this.ID, null);
                    if (listBLT != null && listBLT.Count > 0)
                    {
                        BaoLanhThue = listBLT[listBLT.Count - 1];
                    }
                    else
                        BaoLanhThue = null;
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        #endregion

        private List<CO> _ListCO = new List<CO>();
        public List<Company.GC.BLL.KDT.GC.NPLCungUng> NPLCungUngs = new List<Company.GC.BLL.KDT.GC.NPLCungUng>();

        public List<HangMauDich> HMDCollection
        {
            set { this._HMDCollection = value; }
            get { return this._HMDCollection; }
        }
        public ChungTuCollection ChungTuTKCollection
        {
            set { this._chungtuCollection = value; }
            get { return this._chungtuCollection; }
        }
        public List<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection
        {
            set { this._PhanBoToKhaiXuatCollection = value; }
            get { return this._PhanBoToKhaiXuatCollection; }
        }
        public Company.KD.BLL.KDT.ToKhaiTriGiaCollection TKTGCollection
        {
            set { this._TKTGCollection = value; }
            get { return this._TKTGCollection; }
        }
        public void LoadTKTGCollection()
        {
            Company.KD.BLL.KDT.ToKhaiTriGia tktg = new ToKhaiTriGia();
            tktg.TKMD_ID = this.ID;
            _TKTGCollection = tktg.SelectCollectionBy_TKMD_ID();
            foreach (ToKhaiTriGia item in _TKTGCollection)
            {
                item.LoadHTGCollection();
            }
        }
        public Company.KD.BLL.KDT.ToKhaiTriGiaPP23Collection TKTGPP23Collection
        {
            set { this._TKTGPP23Collection = value; }
            get { return this._TKTGPP23Collection; }
        }
        public void LoadToKhaiTriGiaPP23()
        {
            ToKhaiTriGiaPP23 TKTGPP23 = new ToKhaiTriGiaPP23();
            TKTGPP23.TKMD_ID = this.ID;
            this.TKTGPP23Collection = TKTGPP23.SelectCollectionBy_TKMD_ID();
        }
        public void LoadChungTuKem()
        {
            this.listCTDK = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
        }

        // Add extra :
        public List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> ConvertHMDKDToHangMauDich()
        {
            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            foreach (Company.GC.BLL.KDT.HangMauDich hmdKD in this.HMDCollection)
            {
                Company.KDT.SHARE.QuanLyChungTu.HangMauDich hang = new Company.KDT.SHARE.QuanLyChungTu.HangMauDich();
                hang.ID = hmdKD.ID;
                hang.DonGiaKB = (double)hmdKD.DonGiaKB;
                hang.DVT_ID = hmdKD.DVT_ID;
                hang.MaHS = hmdKD.MaHS;
                hang.MaPhu = hmdKD.MaPhu;
                hang.NuocXX_ID = hmdKD.NuocXX_ID;
                hang.SoLuong = hmdKD.SoLuong;
                hang.SoThuTuHang = hmdKD.SoThuTuHang;
                hang.TenHang = hmdKD.TenHang;
                hang.TriGiaKB = (double)hmdKD.TriGiaKB;
                hang.TriGiaKB_VND = (double)hmdKD.TriGiaKB_VND;
                listHang.Add(hang);
            }
            return listHang;
        }

        public List<ChungTuKem> ChungTuKemList
        {
            set { this.ChungTuKemCollection = value; }
            get { return this.ChungTuKemCollection; }
        }
        public List<CO> ListCO
        {
            set { this._ListCO = value; }
            get { return this._ListCO; }
        }
        public void LoadCO()
        {
            // CO co = new CO();
            //co.TKMD_ID = this.ID;
            ListCO = (List<CO>)CO.SelectCollectionBy_TKMD_ID(this.ID);
            //this.HMDCollection = HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);

            foreach (CO item in ListCO)
            {
                Company.KDT.SHARE.Components.Common.COBoSung coBS = CO.LoadCoBoSung(item.ID);
                if (coBS != null)
                {
                    item.TenCangDoHang = coBS.TenCangDoHang;
                    item.TenCangXepHang = coBS.TenCangXepHang;
                }
            }
        }

        #region ChungTuHaiQuan

        public List<GiayPhep> GiayPhepCollection = new List<GiayPhep>();
        public List<HoaDonThuongMai> HoaDonThuongMaiCollection = new List<HoaDonThuongMai>();
        public List<HopDongThuongMai> HopDongThuongMaiCollection = new List<HopDongThuongMai>();
        public VanDon VanTaiDon;
        public List<CO> COCollection = new List<CO>();
        public List<DeNghiChuyenCuaKhau> listChuyenCuaKhau = new List<DeNghiChuyenCuaKhau>();
        public List<ChungTuKem> ChungTuKemCollection = new List<ChungTuKem>();
        public List<NoiDungDieuChinhTK> NoiDungDieuChinhTKCollection = new List<NoiDungDieuChinhTK>();

        public void LoadChungTuHaiQuan()
        {
            //Xoa du lieu truoc khi load lai tranh truong hop trung du lieu.
            COCollection.Clear();
            HoaDonThuongMaiCollection.Clear();
            HopDongThuongMaiCollection.Clear();
            GiayPhepCollection.Clear();
            listChuyenCuaKhau.Clear();
            ChungTuKemCollection.Clear();
            NoiDungDieuChinhTKCollection.Clear();

            LoadChungThuGD();
            LoadAnHanThue();
            LoadGiayKiemTra();
            LoadDamBaoNghiaVuNT();
            LoadListLePhiHQ();
            LoadSoCont();

            COCollection = (List<CO>)CO.SelectCollectionBy_TKMD_ID(this.ID);
            foreach (CO item in COCollection)
            {
                Company.KDT.SHARE.Components.Common.COBoSung coBS = CO.LoadCoBoSung(item.ID);
                if (coBS != null)
                {
                    item.TenCangDoHang = coBS.TenCangDoHang;
                    item.TenCangXepHang = coBS.TenCangXepHang;
                }
            }

            HoaDonThuongMaiCollection = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            for (int j = 0; j < HoaDonThuongMaiCollection.Count; j++)
            {
                HoaDonThuongMaiCollection[j].LoadListHangDMOfHoaDon();
            }

            HopDongThuongMaiCollection = (List<HopDongThuongMai>)HopDongThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            for (int i = 0; i < HopDongThuongMaiCollection.Count; i++)
            {
                HopDongThuongMaiCollection[i].LoadListHangDMOfHopDong();
            }

            List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
            if (VanDonCollection != null && VanDonCollection.Count > 0)
            {
                VanTaiDon = VanDonCollection[0];
                VanTaiDon.LoadContainerCollection();
                VanTaiDon.LoadListHangOfVanDon();
            }

            GiayPhepCollection = (List<GiayPhep>)GiayPhep.SelectCollectionBy_TKMD_ID(this.ID);
            for (int m = 0; m < GiayPhepCollection.Count; m++)
            {
                GiayPhepCollection[m].LoadListHMDofGiayPhep();
            }

            listChuyenCuaKhau = (List<DeNghiChuyenCuaKhau>)DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(this.ID);

            ChungTuKemCollection = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
            for (int k = 0; k < ChungTuKemCollection.Count; k++)
            {
                ChungTuKemCollection[k].LoadListCTChiTiet();
            }

            NoiDungDieuChinhTKCollection = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(this.ID);
            for (int k = 0; k < NoiDungDieuChinhTKCollection.Count; k++)
            {
                NoiDungDieuChinhTKCollection[k].LoadListNDDCTK();
            }
            ChungTuNoCollection = ChungTuNo.SelectCollectionDynamic("TKMDID=" + this.ID, "");
            NPLCungUngs = Company.GC.BLL.KDT.GC.NPLCungUng.SelectCollectionDynamic("TKMDID=" + this.ID, "");
            foreach (Company.GC.BLL.KDT.GC.NPLCungUng item in NPLCungUngs)
            {
                item.NPLCungUngDetails = Company.GC.BLL.KDT.GC.NPLCungUngDetail.SelectCollectionDynamic("Master_ID = " + item.ID, "");
            }
            LoadBaoLanhThue();
        }

        public void LoadChungTuHaiQuan(string dbname)
        {
            SetDabaseMoi(dbname);

            //Xoa du lieu truoc khi load lai tranh truong hop trung du lieu.
            COCollection.Clear();
            HoaDonThuongMaiCollection.Clear();
            HopDongThuongMaiCollection.Clear();
            GiayPhepCollection.Clear();
            listChuyenCuaKhau.Clear();
            ChungTuKemCollection.Clear();
            NoiDungDieuChinhTKCollection.Clear();

            COCollection = (List<CO>)CO.SelectCollectionBy_TKMD_ID(this.ID);

            HoaDonThuongMaiCollection = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            for (int j = 0; j < HoaDonThuongMaiCollection.Count; j++)
            {
                HoaDonThuongMaiCollection[j].LoadListHangDMOfHoaDon();
            }

            HopDongThuongMaiCollection = (List<HopDongThuongMai>)HopDongThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            for (int i = 0; i < HopDongThuongMaiCollection.Count; i++)
            {
                HopDongThuongMaiCollection[i].LoadListHangDMOfHopDong();
            }

            List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
            if (VanDonCollection != null && VanDonCollection.Count > 0)
            {
                VanTaiDon = VanDonCollection[0];
                VanTaiDon.LoadContainerCollection();
            }

            GiayPhepCollection = (List<GiayPhep>)GiayPhep.SelectCollectionBy_TKMD_ID(this.ID);
            for (int m = 0; m < GiayPhepCollection.Count; m++)
            {
                GiayPhepCollection[m].LoadListHMDofGiayPhep();
            }

            listChuyenCuaKhau = (List<DeNghiChuyenCuaKhau>)DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(this.ID);

            ChungTuKemCollection = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
            for (int k = 0; k < ChungTuKemCollection.Count; k++)
            {
                ChungTuKemCollection[k].LoadListCTChiTiet();
            }

            NoiDungDieuChinhTKCollection = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(this.ID);
            for (int k = 0; k < NoiDungDieuChinhTKCollection.Count; k++)
            {
                NoiDungDieuChinhTKCollection[k].LoadListNDDCTK();
            }
        }

        #endregion ChungTuHaiQuan

        // Cau hinh phong bi lay phan hoi :
        public string ConfigPhongBiLayPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + @"\B03GiaCong\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR.ToUpper();

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            nodeFrom.ChildNodes[0].InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            // this.GuidReference = this.GUIDSTR;
            this.Update();
            return doc.InnerXml;

        }

        #region Lấy phản hồi thông quan điện tử
        public static bool tuChoi = false;
        public string LayPhanHoiTQDTKhaiBao(string pass, string xml)
        {
            //-------------begin----------------
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiLayPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi));
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep.Trim();
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDoanhNghiep);

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan));
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim().ToUpper();
            //-----------end new --------------
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();



            int function = (int)MessageFunctions.KhaiBao;
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            string msgError = string.Empty;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);

                    //Lay funtion tu ket qua tra ve
                    function = Convert.ToInt32(docNPL.GetElementsByTagName("function")[0].InnerText);

                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    if (Result.Attributes["Err"].Value == "no")
                    {
                        if (Result.Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan: " + Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                        kq.XmlSaveMessage(ID, MessageTitle.TuChoiTiepNhan, msgError);
                        throw new Exception(msgError);
                    }
                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty)
                        throw ex;
                    if (!string.IsNullOrEmpty(kq))
                    {
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                    }
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));

                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;

            string errorSt = ""; // Luu thong tin loi
            try
            {
                #region XỬ LÝ MESSAGE
                XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (nodeRoot.Attributes.GetNamedItem("TuChoi") != null)
                {
                    #region Từ chối
                    if (nodeRoot.Attributes["TuChoi"].Value == "yes")
                    {

                        this.HUONGDAN = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeRoot.InnerText);
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;//Vẫn để trạng thái Khongpheduyet, chỉ thay đổi ở form ToKhaiMauDichForm
                        this.Update();

                        kq.XmlSaveMessage(ID, MessageTitle.TuChoiTiepNhan, this.HUONGDAN);
                    }
                    #endregion
                }
                else if (nodeRoot != null && nodeRoot.Attributes["TrangThai"].Value == "yes")
                {
                    #region Thành công
                    if (nodeRoot.Attributes["TRA_LOI"] != null && (nodeRoot.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.TOKHAINHAP || nodeRoot.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.TOKHAIXUAT
                        || nodeRoot.Attributes["TRA_LOI"].Value == "HUY_TK"))
                    {
                        #region Hủy khai báo
                        if (nodeRoot.Attributes["TrangThai"].Value == "THANH CONG" || nodeRoot.Attributes["TrangThai"].Value == "yes")
                        {
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.ActionStatus = -1;
                            this.SoTiepNhan = 0;
                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                            this.Update();

                            kq.XmlSaveMessage(ID, MessageTitle.HQHuyKhaiBaoToKhai, this.HUONGDAN);
                        }

                        #endregion
                    }
                    else if (nodeRoot.Attributes["Ok"] != null && nodeRoot.Attributes["Ok"].Value == "yes")
                    {
                        #region Lấy số tiếp nhận

                        if (nodeRoot.Attributes["TrangThai"].Value == "yes")
                        {
                            if (this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && this.SoToKhai > 0)
                            {
                                this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                                this.NamDK = Convert.ToInt32(nodeRoot.Attributes["NAMTN"].Value);
                                this.NgayTiepNhan = Convert.ToDateTime(nodeRoot.Attributes["NgayTN"].Value);
                                if (this.TrangThaiXuLy != (int)Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                                    this.ActionStatus = 0;
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                                this.Update();
                                kq.XmlSaveMessage(ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToString()));

                            }
                            else if (this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy)
                            {


                                List<HuyToKhai> huyTk = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_TKMD_ID(this.ID);
                                if (huyTk[0].SoTiepNhan != 0)
                                {
                                    huyTk[0].TrangThai = 10;
                                    huyTk[0].Update();
                                    this.TrangThaiXuLy = 10;
                                    Update();
                                    kq.XmlSaveMessage(this.ID, Company.KDT.SHARE.Components.MessageTitle.HuyToKhaiThanhCong);
                                }
                                else
                                {

                                    huyTk[0].SoTiepNhan = long.Parse(nodeRoot.Attributes["SOTN"].Value);
                                    huyTk[0].NgayTiepNhan = Convert.ToDateTime(nodeRoot.Attributes["Ngay_TN"].Value);
                                    huyTk[0].NamTiepNhan = Convert.ToInt16(nodeRoot.Attributes["NAM_TN"].Value);
                                    huyTk[0].Update();

                                    this.TrangThaiXuLy = 11;
                                    this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                                    this.Update();
                                    kq.XmlSaveMessage(this.ID, Company.KDT.SHARE.Components.MessageTitle.HuyToKhai);
                                }
                            }
                            else
                            {
                                XmlAttribute xmlAttr = nodeRoot.Attributes["SOTN"];
                                if (xmlAttr != null)
                                {
                                    this.SoTiepNhan = Convert.ToInt64(xmlAttr.Value);
                                }
                                else
                                {
                                    xmlAttr = nodeRoot.Attributes["SO_TN"];
                                    if (xmlAttr != null)
                                        this.SoTiepNhan = Convert.ToInt64(xmlAttr.Value);
                                }

                                xmlAttr = nodeRoot.Attributes["NAMTN"];
                                if (xmlAttr != null)
                                    this.NamDK = Convert.ToInt32(xmlAttr.Value);
                                xmlAttr = nodeRoot.Attributes["NgayTN"];
                                if (xmlAttr == null) xmlAttr = nodeRoot.Attributes["Ngay_TN"];
                                if (xmlAttr != null)
                                    this.NgayTiepNhan = Convert.ToDateTime(xmlAttr.Value);
                                if (this.TrangThaiXuLy != (int)Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                                    this.ActionStatus = 0;
                                if (this.TrangThaiXuLy != 11)
                                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                                this.Update();
                                kq.XmlSaveMessage(ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToString()));
                            }

                        }

                        #endregion
                    }
                    else if (nodeRoot.Attributes.GetNamedItem("SOTK") != null
                        && nodeRoot.Attributes.GetNamedItem("SOTK").Value == this.SoToKhai.ToString()
                        && this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET
                        && this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua
                        && nodeRoot.SelectSingleNode("PHAN_LUONG") == null)
                    {
                        #region Kiem tra duyệt sửa tờ khai

                        this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        this.Update();
                        kq.XmlSaveMessage(ID, MessageTitle.ToKhaiDuocCapSo,
                            string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim()));
                        #endregion

                    }/*Kiem tra lấy số tờ khai*/
                    else if (nodeRoot.Attributes.GetNamedItem("SOTK") != null && this.SoToKhai == 0)
                    {
                        #region Lấy số TK

                        this.SoToKhai = Convert.ToInt32(nodeRoot.Attributes["SOTK"].Value);
                        if (nodeRoot.Attributes.GetNamedItem("NGAYDK") != null)
                            this.NgayDangKy = Convert.ToDateTime(nodeRoot.Attributes["NGAYDK"].Value);
                        this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMDK"].Value);

                        this.ActionStatus = 1;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        this.Update();
                        kq.XmlSaveMessage(ID, MessageTitle.ToKhaiDuocCapSo,
                            string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim()));

                        #endregion
                    }
                    else if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                    {
                        #region Lấy thông tin phân luồng

                        XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");
                        this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                        this.HUONGDAN = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);
                        this.Update();

                        string tenluong = "Xanh";
                        if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                            tenluong = "Vàng";
                        else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                            tenluong = "Đỏ";

                        kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQDuyet, string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN));

                        //AN DINH THUE
                        AnDinhThue(docNPL, kq);

                        #endregion
                    }
                    #endregion
                }
                else if (nodeRoot != null && nodeRoot.Attributes["TrangThai"].Value == "LOI")
                {
                    #region Xử lý lỗi
                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DULIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DULIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;

                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";


                    errorSt = errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi;
                    throw new Exception(errorSt);
                    #endregion
                }
                else
                    kq.XmlSaveMessage(this.ID, Company.KDT.SHARE.Components.MessageTitle.None);


                #endregion
            }

            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(kq))
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);
                else
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return "";
        }
        #endregion

        /// <summary>
        /// Ấn định Thuế
        /// </summary>
        /// <param name="docNPL"></param>
        /// <param name="kq"></param>
        private void AnDinhThue(XmlDocument docNPL, string kq)
        {
            try
            {
                #region BEGIN AN DINH THUE
                //DATLMQ bổ sung In ra thông báo thuế 06/08/2011
                XmlNode xmlNodeAnDinhThue = docNPL.SelectSingleNode("Envelope/Body/Content/Root/AN_DINH_THUE");
                if (xmlNodeAnDinhThue != null)
                {
                    IsThongBaoThue = true;
                    SoQD = xmlNodeAnDinhThue.Attributes["SOQD"].Value;
                    NgayQD = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYQD"].Value);
                    NgayHetHan = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYHH"].Value);
                    TaiKhoanKhoBac = xmlNodeAnDinhThue.Attributes["TKKB"].Value;
                    TenKhoBac = xmlNodeAnDinhThue.Attributes["KHOBAC"].Value;

                    //Kiem tra thong tin an dinh thue cau to khai
                    AnDinhThue anDinhThueObj = new AnDinhThue();
                    List<AnDinhThue> dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(this.ID);
                    if (dsADT.Count > 0)
                    {
                        anDinhThueObj.ID = dsADT[0].ID;
                    }

                    //Luu an dinh thue
                    anDinhThueObj.SoQuyetDinh = SoQD;
                    anDinhThueObj.NgayQuyetDinh = NgayQD;
                    anDinhThueObj.NgayHetHan = NgayHetHan;
                    anDinhThueObj.TaiKhoanKhoBac = TaiKhoanKhoBac;
                    anDinhThueObj.TenKhoBac = TenKhoBac;
                    anDinhThueObj.GhiChu = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), Company.KDT.SHARE.Components.Globals.GetPhanLuong(this.PhanLuong), this.HUONGDAN);
                    anDinhThueObj.TKMD_ID = ID;
                    anDinhThueObj.TKMD_Ref = GUIDSTR;
                    if (anDinhThueObj.ID == 0)
                        anDinhThueObj.Insert();
                    else
                        anDinhThueObj.InsertUpdate();

                    #region Begin Danh sach thue chi tiet
                    ChiTiet thueObj = new ChiTiet();

                    XmlNodeList xmlNodeThue = docNPL.SelectNodes("Envelope/Body/Content/Root/AN_DINH_THUE/THUE");
                    foreach (XmlNode node in xmlNodeThue)
                    {
                        if (node.Attributes["SACTHUE"].Value.Equals("XNK"))
                        {
                            ChuongThueXNK = node.Attributes["CHUONG"].Value;
                            SoTienThueXNK = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueXNK = node.Attributes["LOAI"].Value;
                            KhoanThueXNK = node.Attributes["KHOAN"].Value;
                            MucThueXNK = node.Attributes["MUC"].Value;
                            TieuMucThueXNK = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue XNK
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "XNK";
                            thueObj.Chuong = ChuongThueXNK;
                            thueObj.TienThue = (decimal)SoTienThueXNK;
                            thueObj.Loai = int.Parse(LoaiThueXNK);
                            thueObj.Khoan = int.Parse(KhoanThueXNK);
                            thueObj.Muc = int.Parse(MucThueXNK);
                            thueObj.TieuMuc = int.Parse(TieuMucThueXNK);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                        if (node.Attributes["SACTHUE"].Value.Equals("VAT"))
                        {
                            ChuongThueVAT = node.Attributes["CHUONG"].Value;
                            SoTienThueVAT = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueVAT = node.Attributes["LOAI"].Value;
                            KhoanThueVAT = node.Attributes["KHOAN"].Value;
                            MucThueVAT = node.Attributes["MUC"].Value;
                            TieuMucThueVAT = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue VAT
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "VAT";
                            thueObj.Chuong = ChuongThueVAT;
                            thueObj.TienThue = (decimal)SoTienThueVAT;
                            thueObj.Loai = int.Parse(LoaiThueVAT);
                            thueObj.Khoan = int.Parse(KhoanThueVAT);
                            thueObj.Muc = int.Parse(MucThueVAT);
                            thueObj.TieuMuc = int.Parse(TieuMucThueVAT);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                        if (node.Attributes["SACTHUE"].Value.Equals("TTDB"))
                        {
                            ChuongThueTTDB = node.Attributes["CHUONG"].Value;
                            SoTienThueTTDB = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueTTDB = node.Attributes["LOAI"].Value;
                            KhoanThueTTDB = node.Attributes["KHOAN"].Value;
                            MucThueTTDB = node.Attributes["MUC"].Value;
                            TieuMucThueTTDB = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue Tieu thu dac biet
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "TTDB";
                            thueObj.Chuong = ChuongThueTTDB;
                            thueObj.TienThue = (decimal)SoTienThueTTDB;
                            thueObj.Loai = int.Parse(LoaiThueTTDB);
                            thueObj.Khoan = int.Parse(KhoanThueTTDB);
                            thueObj.Muc = int.Parse(MucThueTTDB);
                            thueObj.TieuMuc = int.Parse(TieuMucThueTTDB);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                        if (node.Attributes["SACTHUE"].Value.Equals("TVCBPG"))
                        {
                            ChuongThueTVCBPG = node.Attributes["CHUONG"].Value;
                            SoTienThueTVCBPG = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueTVCBPG = node.Attributes["LOAI"].Value;
                            KhoanThueTVCBPG = node.Attributes["KHOAN"].Value;
                            MucThueTVCBPG = node.Attributes["MUC"].Value;
                            TieuMucThueTVCBPG = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue Thu chenh lech gia
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "TVCBPG";
                            thueObj.Chuong = ChuongThueTVCBPG;
                            thueObj.TienThue = (decimal)SoTienThueTVCBPG;
                            thueObj.Loai = int.Parse(LoaiThueTVCBPG);
                            thueObj.Khoan = int.Parse(KhoanThueTVCBPG);
                            thueObj.Muc = int.Parse(MucThueTVCBPG);
                            thueObj.TieuMuc = int.Parse(TieuMucThueTVCBPG);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                    }


                    #endregion End Danh sach thue chi tiet

                    Company.KDT.SHARE.Components.KetQuaXuLy kqxlAnDinhThue = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    kqxlAnDinhThue.ItemID = this.ID;
                    kqxlAnDinhThue.ReferenceID = new Guid(this.GUIDSTR);
                    kqxlAnDinhThue.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxlAnDinhThue.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_AnDinhThueToKhai;
                    kqxlAnDinhThue.NoiDung = string.Format("Ấn định thuế:\r\nSố quyết định: {0}\r\nNgày quyết định: {1}\r\nNgày hết hạn: {2}\r\nTài khoản kho bạc: {3}\r\nKho bạc: {4}", SoQD, NgayQD.ToString(), NgayHetHan.ToString(), TaiKhoanKhoBac, TenKhoBac);
                    kqxlAnDinhThue.Ngay = DateTime.Now;
                    kqxlAnDinhThue.Insert();

                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiAnDinhThue, kqxlAnDinhThue.NoiDung);
                }
                #endregion END AN DINH THUE
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(string.Format("Ấn định thuế tờ khai số: {0}, ngày đăng ký: {1}, mã loại hình: {2}, ID: {3}", this.SoToKhai, NgayDangKy.ToString(), MaLoaiHinh, GUIDSTR), ex); }
        }

        public string WSRequestPhanLuong(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiLayPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi));
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.TenDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            //this.GUIDSTR = (System.Guid.NewGuid().ToString()); ;
            //this.Update();
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    //if (i > 1)
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không thể kết nối tới hệ thống hải quan!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);

                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                    else
                    {
                        if (this.TrangThaiXuLy != (int)Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                            this.ActionStatus = -1;
                    }
                }
                else
                {
                    if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                    {
                        throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                    }
                    else
                        throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                }
            }

            if (i > 1)
                return doc.InnerXml;

            string errorSt = ""; // Luu thong tin loi
            try
            {
                /*HUNGTQ, Update 19/05/2010, XU LY THONG BAO TRA VE*/
                //Kiem tra thong tin tu choi TK?
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("TuChoi") != null)
                {
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TuChoi"].Value == "yes")
                    {
                        //nodeTuChoi.InnerText;
                        this.HUONGDAN = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                        Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                        kqxl.NoiDung = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                        this.Update();
                    }
                }
                /*Kiem tra duyệt sửa tờ khai*/
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTK").Value == this.SoToKhai.ToString() && this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET
                    && this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                {
                    this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                    this.Update();

                    Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_ToKhaiSuaDuocDuyet;
                    kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                }
                /*Kiem tra PHAN LUONG*/
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTN") != null)
                {
                    #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                    XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    //linhhtn cập nhật lấy phản hồi dữ liệu từng loại một: 1/ lấy số TN 2/ lấy số TK 3/ Lấy phân luồng

                    /** Lấy STN trong trường hợp chưa lấy được STN ở hàm LayPhanHoiTQDTKhaiBao() **/
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("Ok").Value == "yes")
                    //if (nodeRoot.SelectSingleNode("PHAN_LUONG") == null) //Neu message chua phan luong -> cap nhat SO TN.
                    {
                        this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                        this.NgayTiepNhan = DateTime.Today;
                        this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);
                        if (this.TrangThaiXuLy != (int)Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                            this.ActionStatus = 0;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;

                        if (SoTiepNhan == 0)
                        {
                            Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToString());
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();
                        }
                    }
                    //else if(nodeRoot.Attributes.GetNamedItem("SOTK") != null && nodeRoot.Attributes.GetNamedItem("SOTN")>0)
                    /*Kiem tra co So to khai*/
                    else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTK") != null)
                    {
                        this.SoToKhai = Convert.ToInt32(nodeRoot.Attributes["SOTK"].Value);
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("NGAYDK") != null)
                            this.NgayDangKy = Convert.ToDateTime(nodeRoot.Attributes["NGAYDK"].Value);
                        this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMDK"].Value);

                        this.ActionStatus = 1;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                        if (SoToKhai == 0)
                        {
                            Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                            kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();
                        }
                    }
                    //else if(nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                    /*Lay thong tin phan luong*/
                    else if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                    {
                        XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                        this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                        this.HUONGDAN = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                        this.ActionStatus = 1;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;


                        Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;

                        string tenluong = "Xanh";

                        if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                            tenluong = "Vàng";
                        else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                            tenluong = "Đỏ";

                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();

                        //Tuch, Update 03/06/2010
                        //Bổ sung nội dung phân luồng trả về: Ấn định thuế


                    }

                    this.Update();

                    #endregion Lấy số tiếp nhận của danh sách NPL
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception(errorSt);
            }

            return "";

        }

        public string WSRequestDaDuyet(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.StartsWith("N"))
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi));
            else
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi));
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = this.MaHaiQuan;

            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "no")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }
        // End add new
        public static long SelectCountSoTK(string madoanhnghiep)
        {
            string sql = "select count(*) from t_KDT_ToKhaiMauDich where MaDoanhNghiep='" + madoanhnghiep + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return 0;
            else
                return Convert.ToInt64(o);
        }
        public void LayDuLieuCungUng()
        {

            string sql = "SELECT ID, SoToKhai, MaLoaiHinh, year(NgayDangKy)as NamDangKy FROM t_KDT_ToKhaiMauDich  WHERE Cast(SoToKhai as varchar(6)) + MaLoaiHinh + Cast(year(NgayDangKy)AS varchar(4)) IN " +
                        "(SELECT Cast(SoToKhai as varchar(6)) + MaLoaiHinh + Cast(NamDangKy AS varchar(4)) FROM t_GC_NPLNhapTonThucTe WHERE MaLoaiHinh LIKE 'NSX%' AND MaNPL NOT LIKE 'RKV%')";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataTable dtToKhai = db.ExecuteDataSet(dbCommand).Tables[0];

            //using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            //{
            try
            {
                SqlDatabase dbSXXK = (SqlDatabase)DatabaseFactory.CreateDatabase("SXXK");
                dbCommand = null;

                foreach (DataRow dr in dtToKhai.Rows)
                {
                    sql = "SELECT * FROM t_SXXK_HangMauDich WHERE SoToKhai =" + dr["SoToKhai"] + " AND MaLoaiHinh = '" + dr["MaLoaiHinh"] + "' AND NamDangKy =" + dr["NamDangKy"];
                    dbCommand = (SqlCommand)dbSXXK.GetSqlStringCommand(sql);
                    DataTable dtHang = dbSXXK.ExecuteDataSet(dbCommand).Tables[0];
                    foreach (DataRow drHang in dtHang.Rows)
                    {
                        HangMauDich HMD = new HangMauDich();
                        HMD.ID = 0;
                        HMD.TKMD_ID = Convert.ToInt64(dr["ID"]);
                        HMD.SoThuTuHang = 0;
                        HMD.MaHS = drHang["MaHS"].ToString();
                        HMD.MaPhu = drHang["MaPhu"].ToString();
                        HMD.TenHang = drHang["TenHang"].ToString();
                        HMD.NuocXX_ID = drHang["NuocXX_ID"].ToString();
                        HMD.DVT_ID = drHang["DVT_ID"].ToString();
                        HMD.SoLuong = -1;
                        HMD.DonGiaKB = Convert.ToDecimal(drHang["DonGiaKB"]);
                        HMD.DonGiaTT = Convert.ToDecimal(drHang["DonGiaTT"]);
                        HMD.TriGiaKB = Convert.ToDecimal(drHang["TriGiaKB"]);
                        HMD.TriGiaTT = Convert.ToDecimal(drHang["TriGiaTT"]);
                        HMD.TriGiaKB_VND = Convert.ToDecimal(drHang["TriGiaKB_VND"]);
                        HMD.ThueSuatXNK = Convert.ToDecimal(drHang["ThueSuatXNK"]);
                        HMD.ThueXNK = Convert.ToDecimal(drHang["ThueXNK"]);
                        if (!HMD.CheckMaHangToKhai())
                            HMD.Insert();
                    }
                }
                //scope.Complete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //}

        }
        public void LoadPhanBoToKhaiXuatCollection()
        {
            string sql = "select * from t_GC_PhanBoToKhaiXuat where SoToKhaiXuat=@SoToKhaiXuat and MaLoaiHinhXuat=@MaLoaiHinhXuat and MaHaiQuanXuat=@MaHaiQuanXuat and NamDangKyXuat=@NamDangKyXuat";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.BigInt, this.SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.VarChar, this.MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this.MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, (short)this.NgayDangKy.Year);

            IDataReader reader = db.ExecuteReader(dbCommand);
            _PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            while (reader.Read())
            {
                PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_TKMD"))) entity.ID_TKMD = reader.GetInt16(reader.GetOrdinal("ID_TKMD"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                entity.LoadPhanBoToKhaiNhapCollection();
                _PhanBoToKhaiXuatCollection.Add(entity);
            }

            reader.Close();

        }
        public IDataReader SelectReaderDynamic(string sql, DateTime from, DateTime to)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@From", SqlDbType.DateTime, from);
            db.AddInParameter(dbCommand, "@To", SqlDbType.DateTime, to);

            return db.ExecuteReader(dbCommand);
        }
        public bool Load(SqlTransaction trans)
        {
            string spName = "p_KDT_ToKhaiMauDich_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) this._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) this._IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) this._MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) this._Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool Load(SqlTransaction trans, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);

            IDataReader reader = null;
            if (trans != null)
                reader = this.db.ExecuteReader(dbCommand, trans);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) this._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) this._IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) this._MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) this._Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) this._TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) this._NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool LoadKTX(SqlTransaction trans, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_LoadByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);

            IDataReader reader = null;
            if (trans != null)
                reader = this.db.ExecuteReader(dbCommand, trans);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) this._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) this._IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) this._MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) this._Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) this._TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //Hungtq, Update 31052010
        public DataTable GetContainerInfo(long tkmd)
        {
            string sql = " SELECT  distinct   dbo.t_KDT_Container.SoHieu, dbo.t_KDT_Container.LoaiContainer, dbo.t_KDT_Container.Seal_No, dbo.t_KDT_ToKhaiMauDich.NgayDangKy, "
                     + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep, dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
                     + " dbo.t_KDT_VanDon.NgayVanDon, dbo.t_KDT_VanDon.SoHieuChuyenDi "
                     + " FROM         dbo.t_KDT_Container INNER JOIN "
                     + " dbo.t_KDT_VanDon ON dbo.t_KDT_Container.VanDon_ID = dbo.t_KDT_VanDon.ID inner join dbo.t_KDT_ToKhaiMauDich ON "
                     + " dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_VanDon.TKMD_ID "
                     + " where dbo.t_KDT_VanDon.TKMD_ID = " + tkmd;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public bool isHaveWith4Para()
        {
            string whereCond = "MaHaiQuan ='" + this.MaHaiQuan + "'";
            whereCond += " and MaLoaiHinh ='" + this.MaLoaiHinh + "'";
            whereCond += " and SoToKhai = " + this.SoToKhai + "";
            whereCond += " and year(NgayDangKy) = " + this.NgayDangKy.Year + "";
            string order = "ID";
            DataSet ds = this.SelectDynamic(whereCond, order);
            //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //{
            //    //DateTime NgayDK = (DateTime)ds.Tables[0].Rows[i]["NgayDangKy"];
            //    //if (NgayDK.Year == this.NgayDangKy.Year)
            //    //{
            //        return true;
            //    //}
            //}
            if (ds.Tables[0].Rows.Count > 0) return true;
            return false;
        }

        public static long GetIDToKhaiMauDichBySoToKhaiAndNamDangKy(string MaHQ, string MaLH, long SoToKhai, int NamDK)
        {
            string sql = "select ID from t_KDT_ToKhaiMauDich where SoToKhai=" + SoToKhai + " and year(NgayDangKy)=" + NamDK + " and MaHaiQuan='" + MaHQ + "' and MaLoaiHinh='" + MaLH + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object id = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(id);
        }
        public static long GetIDToKhaiMauDichBySoToKhaiAndNamDangKy(string MaHQ, string MaLH, long SoToKhai, int NamDK, string name)
        {
            string sql = "select ID from t_KDT_ToKhaiMauDich where SoToKhai=" + SoToKhai + " and year(NgayDangKy)=" + NamDK + " and MaHaiQuan='" + MaHQ + "' and MaLoaiHinh='" + MaLH + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(name);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object id = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(id);
        }
        //-----------------------------------------------------------------------------------------
        public ToKhaiMauDichCollection SelectCollectionDynamic(string sql, DateTime from, DateTime to)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDynamic(sql, from, to);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //-----------------------------------------------------------------------------------------
        public ToKhaiMauDichCollection searchTKMDByUserName(string userName, string where)
        {
            string query = "SELECT * FROM dbo.t_KDT_ToKhaiMauDich tkmd INNER JOIN dbo.t_KDT_SXXK_LogKhaiBao lg ON lg.ID_DK = tkmd.ID " +
                           "WHERE lg.UserNameKhaiBao = '" + userName + "' AND LoaiKhaiBao = 'TK' AND ";
            if (!where.Equals(""))
                query += where;
            System.Data.Common.DbCommand dbCommand = (System.Data.Common.DbCommand)this.db.GetSqlStringCommand(query);
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) this._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) this._IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) this._MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) this._Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) this._TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) this._NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //-----------------------------------------------------------------------------------------

        public void InsertUpdateToKhaiXuLyDuLieu(SqlTransaction transaction)
        {
            try
            {
                int i = 1;
                foreach (HangMauDich hmd in this.HMDCollection)
                {

                    hmd.SoThuTuHang = i++;
                    if (this.MaLoaiHinh.StartsWith("NGC"))
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Nhap NPL
                            NguyenPhuLieu npl = new NguyenPhuLieu();
                            npl.HopDong_ID = this.IDHopDong;
                            npl.Ma = hmd.MaPhu;
                            npl.Load(transaction);
                            npl.SoLuongDaNhap += hmd.SoLuong;
                            npl.UpdateTransaction(transaction);
                            #endregion Nhap NPL
                        }
                        else
                        {
                            #region Nhap Thiet BI
                            ThietBi tb = new ThietBi();
                            tb.HopDong_ID = this.IDHopDong;
                            tb.Ma = hmd.MaPhu;
                            tb.Load(transaction);
                            tb.SoLuongDaNhap += hmd.SoLuong;
                            tb.UpdateTransaction(transaction);
                            #endregion Nhap Thiet BI
                        }
                    }
                    else
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Xuat NPL
                            NguyenPhuLieu npl = new NguyenPhuLieu();
                            npl.HopDong_ID = this.IDHopDong;
                            npl.Ma = hmd.MaPhu;
                            npl.Load(transaction);
                            npl.SoLuongDaNhap -= hmd.SoLuong;
                            npl.UpdateTransaction(transaction);
                            #endregion Xuat NPL
                        }
                        else if (LoaiHangHoa == "T")
                        {
                            #region Xuat TB
                            ThietBi tb = new ThietBi();
                            tb.HopDong_ID = this.IDHopDong;
                            tb.Ma = hmd.MaPhu;
                            tb.Load(transaction);
                            tb.SoLuongDaNhap -= hmd.SoLuong;
                            tb.UpdateTransaction(transaction);
                            #endregion Xuat Thiet bi
                        }
                        else
                        {
                            SanPham sp = new SanPham();
                            sp.HopDong_ID = this.IDHopDong;
                            sp.Ma = hmd.MaPhu;
                            sp.Load(transaction);
                            sp.SoLuongDaXuat += hmd.SoLuong;
                            sp.UpdateTransaction(transaction);
                            DataSet dsLuongNPL = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(sp.Ma, hmd.SoLuong, this.IDHopDong, transaction);
                            foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                            {
                                NguyenPhuLieu npl = new NguyenPhuLieu();
                                npl.HopDong_ID = this.IDHopDong;
                                npl.Ma = row["MaNguyenPhuLieu"].ToString();
                                npl.Load(transaction);
                                npl.SoLuongDaDung += Convert.ToDecimal(row["LuongCanDung"]);
                                npl.UpdateTransaction(transaction);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool InsertUpdateDongBoDuLieu(ToKhaiMauDichCollection collection, Company.GC.BLL.KDT.GC.HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    ToKhaiMauDich TKMDDelete = new ToKhaiMauDich();
                    TKMDDelete.IDHopDong = HD.ID;
                    TKMDDelete.DeleteBy_IDHopDong(transaction);
                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.InsertTransaction(transaction);
                        else
                            item.UpdateTransaction(transaction);
                        //int i = 1;
                        foreach (HangMauDich hmd in item.HMDCollection)
                        {
                            hmd.TKMD_ID = item.ID;
                            hmd.ID = hmd.Insert(transaction);
                        }

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieu(ToKhaiMauDichCollection collection, Company.GC.BLL.KDT.GC.HopDong HD, string dbname)
        {
            SetDabaseMoi(dbname);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //ToKhaiMauDich TKMDDelete = new ToKhaiMauDich();
                    //TKMDDelete.IDHopDong = HD.ID;
                    //TKMDDelete.DeleteBy_IDHopDong(transaction, dbname);

                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        item.InsertUpdateTransaction(transaction, dbname);

                        ToKhaiMauDich itemTemp = new ToKhaiMauDich();
                        itemTemp.IDHopDong = HD.ID;
                        itemTemp.SoHopDong = item.SoHopDong;
                        itemTemp.SoToKhai = item.SoToKhai;
                        itemTemp.NgayDangKy = item.NgayDangKy;
                        itemTemp.MaLoaiHinh = item.MaLoaiHinh;
                        itemTemp.MaHaiQuan = item.MaHaiQuan;
                        itemTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                        itemTemp.Load(transaction, dbname);

                        //int i = 1;
                        foreach (HangMauDich hmd in item.HMDCollection)
                        {
                            hmd.TKMD_ID = itemTemp.ID;
                            hmd.ID = hmd.InsertUpdateTransaction(transaction, dbname);
                        }

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieuKTX(ToKhaiMauDichCollection collection, Company.GC.BLL.KDT.GC.HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    ToKhaiMauDich TKMDDelete = new ToKhaiMauDich();
                    TKMDDelete.IDHopDong = HD.ID;
                    TKMDDelete.DeleteBy_IDHopDong(transaction);
                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.InsertTransactionKTX(transaction);
                        else
                            item.UpdateTransactionKTX(transaction);
                        //int i = 1;
                        foreach (HangMauDich hmd in item.HMDCollection)
                        {
                            hmd.TKMD_ID = item.ID;
                            hmd.ID = hmd.Insert(transaction);
                        }

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieuKTX(ToKhaiMauDichCollection collection, Company.GC.BLL.KDT.GC.HopDong HD, string dbname)
        {
            SetDabaseMoi(dbname);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //ToKhaiMauDich TKMDDelete = new ToKhaiMauDich();
                    //TKMDDelete.IDHopDong = HD.ID;
                    //TKMDDelete.DeleteBy_IDHopDong(transaction, dbname);

                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.ID = 0;
                        item.IDHopDong = HD.ID;
                        item.InsertUpdateTransactionKTX(transaction, dbname);

                        ToKhaiMauDich itemTemp = new ToKhaiMauDich();
                        itemTemp.IDHopDong = item.IDHopDong;
                        itemTemp.SoHopDong = item.SoHopDong;
                        itemTemp.SoToKhai = item.SoToKhai;
                        itemTemp.NgayDangKy = item.NgayDangKy;
                        itemTemp.MaLoaiHinh = item.MaLoaiHinh;
                        itemTemp.MaHaiQuan = item.MaHaiQuan;
                        itemTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                        itemTemp.LoadKTX(transaction, dbname);

                        foreach (HangMauDich hmd in item.HMDCollection)
                        {
                            hmd.TKMD_ID = itemTemp.ID;
                            hmd.ID = hmd.InsertUpdateTransactionKTX(transaction, dbname);
                        }
                    }

                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieu(ToKhaiMauDichCollection collection, string MaDoanhNghiep)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    ToKhaiMauDich TKMDDelete = new ToKhaiMauDich();
                    TKMDDelete.MaDoanhNghiep = MaDoanhNghiep;
                    TKMDDelete.DeleteBy_MaDoanhNghiep(transaction);
                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.InsertTransaction(transaction);
                        //int i = 1;
                        foreach (HangMauDich hmd in item.HMDCollection)
                        {
                            hmd.TKMD_ID = item.ID;
                            hmd.ID = hmd.Insert(transaction);
                        }

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieuMoi(ToKhaiMauDichCollection collection, Company.GC.BLL.KDT.GC.HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;

                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.InsertTransaction(transaction);
                        else
                            item.UpdateTransaction(transaction);
                        //int i = 1;
                        foreach (HangMauDich hmd in item.HMDCollection)
                        {
                            hmd.TKMD_ID = item.ID;
                            hmd.ID = hmd.Insert(transaction);
                        }

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieuMoiKTX(ToKhaiMauDichCollection collection, Company.GC.BLL.KDT.GC.HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;

                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.InsertTransactionKTX(transaction);
                        else
                            item.UpdateTransactionKTX(transaction);
                        //int i = 1;
                        foreach (HangMauDich hmd in item.HMDCollection)
                        {
                            hmd.TKMD_ID = item.ID;
                            hmd.ID = hmd.Insert(transaction);
                        }

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieuMoiKTX(ToKhaiMauDichCollection collection, Company.GC.BLL.KDT.GC.HopDong HD, string databaseName)
        {
            SetDabaseMoi(databaseName);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;

                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.InsertTransactionKTX(transaction, databaseName);
                        else
                            item.UpdateTransactionKTX(transaction, databaseName);

                        //int i = 1;
                        foreach (HangMauDich hmd in item.HMDCollection)
                        {
                            hmd.TKMD_ID = item.ID;
                            hmd.ID = hmd.InsertUpdateTransaction(transaction, databaseName);
                        }

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, this._ChucVu);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);
            this.db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, this._Ngay_THN_THX);
            this.db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, this._TrangThaiPhanBo);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        public int InsertUpdateTransactionKTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_InsertUpdateByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, this._ChucVu);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);
            this.db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, this._Ngay_THN_THX);
            this.db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, this._TrangThaiPhanBo);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, this._ChucVu);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);
            this.db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, this._Ngay_THN_THX);
            this.db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, this._TrangThaiPhanBo);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_ToKhaiMauDich_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, this._ChucVu);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);
            this.db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, this._Ngay_THN_THX);
            this.db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, this._TrangThaiPhanBo);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_ToKhaiMauDich_Insert_ByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, this._ChucVu);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);
            this.db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, this._Ngay_THN_THX);
            this.db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, this._TrangThaiPhanBo);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, this._ChucVu);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);
            this.db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, this._Ngay_THN_THX);
            this.db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, this._TrangThaiPhanBo);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_ToKhaiMauDich_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, this._ChucVu);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);
            this.db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, this._Ngay_THN_THX);
            this.db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, this._TrangThaiPhanBo);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_ToKhaiMauDich_Update_ByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, this._ChucVu);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);
            this.db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, this._Ngay_THN_THX);
            this.db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, this._TrangThaiPhanBo);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiMauDichCollection SelectCollectionBy_IDHopDong(string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);

            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) entity.Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) entity.TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public ToKhaiMauDichCollection SelectCollectionBy_IDHopDong(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);

            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) entity.Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) entity.TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public ToKhaiMauDichCollection SelectCollectionBy_IDHopDong_KTX()
        {
            string spName = "p_KDT_ToKhaiMauDich_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);

            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) entity.Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) entity.TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public ToKhaiMauDichCollection SelectCollectionBy_IDHopDong_KTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);

            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) entity.Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) entity.TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public ToKhaiMauDichCollection SelectCollectionBy_IDHopDong_KTX_KhongLayChuaKhaiBao(SqlTransaction transaction, string dbName)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDynamic("IDHopDong = " + this._IDHopDong + " and TrangThaiXuLy != -1", "", null, dbName);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) entity.Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) entity.TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            return reader;
        }

        public bool InsertUpdateDongBoDuLieuMoi(ToKhaiMauDichCollection collection)
        {
            bool ret;
            ToKhaiMauDichCollection TKMDTMP = new ToKhaiMauDichCollection();
            foreach (ToKhaiMauDich tk in collection)
            {
                if (tk.CheckExistToKhaiMauDich(tk.MaHaiQuan, tk.MaLoaiHinh, tk.SoToKhai, tk.NgayDangKy))
                    continue;
                TKMDTMP.Add(tk);
            }
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;

                    foreach (ToKhaiMauDich item in TKMDTMP)
                    {

                        item.InsertTransaction(transaction);
                        //int i = 1;
                        foreach (HangMauDich hmd in item.HMDCollection)
                        {
                            hmd.TKMD_ID = item.ID;
                            hmd.ID = hmd.Insert(transaction);
                        }

                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieu()
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    //int i = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        if (id == 0)
                            hmd.ID = 0;
                        hmd.TKMD_ID = this.ID;
                        hmd.ID = hmd.Insert(transaction);
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this.ID == 0)
                this.ID = this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);

            foreach (HangMauDich hmdDetail in this.HMDCollection)
            {
                if (hmdDetail.ID == 0)
                {
                    hmdDetail.TKMD_ID = this.ID;
                    hmdDetail.ID = hmdDetail.Insert(transaction);
                }
                else
                {
                    hmdDetail.Update(transaction);
                }
            }
            foreach (ChungTu ct in this.ChungTuTKCollection)
            {
                ct.InsertUpdateTransaction(transaction);
            }
        }
        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            long id = this.ID;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    int i = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {

                        hmd.SoThuTuHang = i++;
                        if (this.ID == 0)
                            hmd.ID = 0;
                        HangMauDich HDMCu = new HangMauDich();
                        HDMCu.ID = hmd.ID;
                        if (HDMCu.ID > 0)
                        {
                            HDMCu.Load(transaction);

                        }
                        if (hmd.ID == 0)
                        {
                            hmd.TKMD_ID = this.ID;
                            hmd.ID = hmd.Insert(transaction);
                            foreach (MienGiamThue item in hmd.MienGiamThueCollection)
                            {
                                item.HMD_ID = hmd.ID;
                                item.Insert(transaction);
                            }

                        }
                        else
                        {
                            hmd.Update(transaction);
                            foreach (MienGiamThue item in hmd.MienGiamThueCollection)
                            {
                                item.HMD_ID = hmd.ID;
                                item.InsertUpdate(transaction);
                            }
                        }
                        if (this.MaLoaiHinh.StartsWith("NGC"))
                        {
                            if (LoaiHangHoa == "N")
                            {
                                #region Nhap NPL
                                NguyenPhuLieu npl = new NguyenPhuLieu();
                                npl.HopDong_ID = this.IDHopDong;
                                npl.Ma = hmd.MaPhu;
                                npl.Load(transaction);
                                npl.SoLuongDaNhap += hmd.SoLuong;

                                if (HDMCu.ID > 0)
                                {
                                    if (HDMCu.MaPhu == hmd.MaPhu)
                                    {
                                        //ko doi ma chi can cap nhat lai luong                                        
                                        npl.SoLuongDaNhap -= HDMCu.SoLuong;
                                    }
                                    else
                                    {
                                        //cap nhat thong tin so luong cho ma cu                                      
                                        NguyenPhuLieu nplCu = new NguyenPhuLieu();
                                        nplCu.HopDong_ID = this.IDHopDong;
                                        nplCu.Ma = HDMCu.MaPhu;
                                        nplCu.Load(transaction);
                                        nplCu.SoLuongDaNhap -= HDMCu.SoLuong;
                                        nplCu.UpdateTransaction(transaction);
                                    }
                                }
                                npl.UpdateTransaction(transaction);
                                #endregion Nhap NPL
                            }
                            else
                            {
                                #region Nhap Thiet BI
                                ThietBi tb = new ThietBi();
                                tb.HopDong_ID = this.IDHopDong;
                                tb.Ma = hmd.MaPhu;
                                tb.Load(transaction);
                                tb.SoLuongDaNhap += hmd.SoLuong;
                                if (HDMCu.ID > 0)
                                {
                                    if (HDMCu.MaPhu == hmd.MaPhu)
                                    {
                                        tb.SoLuongDaNhap -= HDMCu.SoLuong;
                                    }
                                    else
                                    {
                                        ThietBi tbCu = new ThietBi();
                                        tbCu.HopDong_ID = this.IDHopDong;
                                        tbCu.Ma = HDMCu.MaPhu;
                                        tbCu.Load(transaction);
                                        tbCu.SoLuongDaNhap -= HDMCu.SoLuong;
                                        tbCu.UpdateTransaction(transaction);
                                    }
                                }
                                tb.UpdateTransaction(transaction);
                                #endregion Nhap Thiet BI
                            }
                        }
                        else
                        {
                            if (LoaiHangHoa == "N")
                            {
                                #region Xuat NPL
                                NguyenPhuLieu npl = new NguyenPhuLieu();
                                npl.HopDong_ID = this.IDHopDong;
                                npl.Ma = hmd.MaPhu;
                                npl.Load(transaction);
                                npl.SoLuongDaDung += hmd.SoLuong;
                                if (HDMCu.ID > 0)
                                {
                                    if (HDMCu.MaPhu == hmd.MaPhu)
                                    {
                                        npl.SoLuongDaDung -= HDMCu.SoLuong;
                                    }
                                    else
                                    {
                                        NguyenPhuLieu nplCu = new NguyenPhuLieu();
                                        nplCu.HopDong_ID = this.IDHopDong;
                                        nplCu.Ma = HDMCu.MaPhu;
                                        nplCu.Load(transaction);
                                        nplCu.SoLuongDaDung -= HDMCu.SoLuong;
                                        nplCu.UpdateTransaction(transaction);
                                    }
                                }
                                npl.UpdateTransaction(transaction);
                                #endregion Xuat NPL
                            }
                            else if (LoaiHangHoa == "T")
                            {
                                #region Xuat TB
                                ThietBi tb = new ThietBi();
                                tb.HopDong_ID = this.IDHopDong;
                                tb.Ma = hmd.MaPhu;
                                tb.Load(transaction);
                                tb.SoLuongDaNhap -= hmd.SoLuong;
                                if (HDMCu.ID > 0)
                                {
                                    if (HDMCu.MaPhu == hmd.MaPhu)
                                    {
                                        tb.SoLuongDaNhap += HDMCu.SoLuong;
                                    }
                                    else
                                    {
                                        ThietBi tbCu = new ThietBi();
                                        tbCu.HopDong_ID = this.IDHopDong;
                                        tbCu.Ma = HDMCu.MaPhu;
                                        tbCu.Load(transaction);
                                        tbCu.SoLuongDaNhap += HDMCu.SoLuong;
                                        tbCu.UpdateTransaction(transaction);
                                    }
                                }
                                tb.UpdateTransaction(transaction);
                                #endregion Xuat Thiet bi
                            }
                            else
                            {
                                SanPham sp = new SanPham();
                                sp.HopDong_ID = this.IDHopDong;
                                sp.Ma = hmd.MaPhu;
                                sp.Load(transaction);
                                sp.SoLuongDaXuat += hmd.SoLuong;
                                if (HDMCu.ID > 0)
                                {
                                    if (HDMCu.MaPhu == hmd.MaPhu)
                                    {
                                        sp.SoLuongDaXuat -= HDMCu.SoLuong;
                                    }
                                    else
                                    {
                                        SanPham spCu = new SanPham();
                                        spCu.HopDong_ID = this.IDHopDong;
                                        spCu.Ma = HDMCu.MaPhu;
                                        spCu.Load(transaction);
                                        spCu.SoLuongDaXuat -= HDMCu.SoLuong;
                                        spCu.UpdateTransaction(transaction);
                                    }
                                }
                                sp.UpdateTransaction(transaction);
                                if (HDMCu.ID > 0)
                                {
                                    DataSet dsLuongNPLCu = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HDMCu.MaPhu, HDMCu.SoLuong, this.IDHopDong, transaction);
                                    foreach (DataRow row in dsLuongNPLCu.Tables[0].Rows)
                                    {
                                        NguyenPhuLieu npl = new NguyenPhuLieu();
                                        npl.HopDong_ID = this.IDHopDong;
                                        npl.Ma = row["MaNguyenPhuLieu"].ToString();
                                        npl.Load(transaction);
                                        npl.SoLuongDaDung -= Convert.ToDecimal(row["LuongCanDung"]);
                                        npl.UpdateTransaction(transaction);
                                    }
                                }
                                DataSet dsLuongNPL = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(sp.Ma, hmd.SoLuong, this.IDHopDong, transaction);
                                foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                {
                                    NguyenPhuLieu npl = new NguyenPhuLieu();
                                    npl.HopDong_ID = this.IDHopDong;
                                    npl.Ma = row["MaNguyenPhuLieu"].ToString();
                                    npl.Load(transaction);
                                    npl.SoLuongDaDung += Convert.ToDecimal(row["LuongCanDung"]);
                                    npl.UpdateTransaction(transaction);
                                }
                            }
                        }
                        #region Thủ tục HQ trước đó
                        if (hmd.ThuTucHQTruocDo != null)
                        {
                            hmd.ThuTucHQTruocDo.Master_ID = hmd.ID;
                            long IdThuTuc;
                            if (hmd.ThuTucHQTruocDo.ID == 0)
                                IdThuTuc = hmd.ThuTucHQTruocDo.Insert(transaction);
                            else
                            {
                                hmd.ThuTucHQTruocDo.Update();
                                IdThuTuc = hmd.ThuTucHQTruocDo.ID;
                            }
                            foreach (ChungTuHQTruocDo item in hmd.ThuTucHQTruocDo.ListChungTu)
                            {
                                item.Master_ID = IdThuTuc;
                                item.InsertUpdate();
                            }
                        }
                        #endregion
                        #region Đơn vị tính quy đổi
                        if (hmd.DVT_QuyDoi != null && hmd.DVT_QuyDoi.IsUse)
                        {
                            hmd.DVT_QuyDoi.Master_ID = hmd.ID;
                            hmd.DVT_QuyDoi.Type = "HMD";
                            hmd.DVT_QuyDoi.InsertUpdate();
                        }
                        else if (hmd.DVT_QuyDoi != null && hmd.DVT_QuyDoi.ID > 0)
                        {
                            hmd.DVT_QuyDoi.Delete();
                        }

                        #endregion

                    }
                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;
                        if (ct.ID == 0)
                        {
                            ct.Master_ID = this.ID;
                            ct.ID = ct.InsertTransaction(transaction);
                        }
                        else
                        {
                            ct.UpdateTransaction(transaction);
                        }
                    }
                    #region ân hạn thuế / Được đảm bảo nghĩa vụ nộp thuế
                    this.AnHanThue.TKMD_ID = this.ID;
                    this.AnHanThue.InsertUpdate(transaction);
                    this.DamBaoNghiaVuNopThue.TKMD_ID = this.ID;
                    this.DamBaoNghiaVuNopThue.InsertUpdate(transaction);
                    #endregion

                    #region Van Tai Don
                    if (VanTaiDon != null)
                    {
                        if (VanTaiDon.ID == 0)
                        {
                            VanTaiDon.TKMD_ID = this.ID;
                            VanTaiDon.ID = VanTaiDon.Insert(transaction);
                        }
                        else
                        {
                            VanTaiDon.Update(transaction);
                        }
                        foreach (Container container in VanTaiDon.ContainerCollection)
                        {
                            container.VanDon_ID = VanTaiDon.ID;
                            if (container.ID == 0)
                                container.Insert(transaction);
                            else
                                container.Update(transaction);
                        }
                        foreach (HangVanDonDetail item in VanTaiDon.ListHangOfVanDon)
                        {
                            item.VanDon_ID = VanTaiDon.ID;
                            if (item.ID == 0)
                                item.Insert(transaction);
                            else
                                item.Update(transaction);
                        }
                    }
                    #endregion Van Tai Don

                    #region Chứng từ nợ
                    if (ChungTuNoCollection != null)
                    {
                        foreach (ChungTuNo item in ChungTuNoCollection)
                        {
                            item.TKMDID = this.ID;
                            if (item.ID == 0)
                                item.Insert(transaction);
                            else item.Update(transaction);
                        }
                    }
                    #endregion

                    #region Nguyên phụ liệu cung ứng
                    foreach (Company.GC.BLL.KDT.GC.NPLCungUng nplCungUng in NPLCungUngs)
                    {
                        nplCungUng.TKMDID = this.ID;
                        if (nplCungUng.ID == 0)
                            nplCungUng.Insert(transaction);
                        else nplCungUng.Update(transaction);
                        foreach (Company.GC.BLL.KDT.GC.NPLCungUngDetail nplCungUngDetail in nplCungUng.NPLCungUngDetails)
                        {
                            nplCungUngDetail.Master_ID = nplCungUng.ID;
                            if (nplCungUngDetail.ID == 0)
                                nplCungUngDetail.Insert(transaction);
                            else nplCungUngDetail.Update(transaction);
                        }
                    }
                    #endregion

                    #region Số lượng cont
                    if (SoLuongContainer == null)
                        SoLuongContainer = new SoContainer();
                    if (SoLuongContainer.ID > 0)
                    {
                        SoLuongContainer.TKMD_ID = this.ID;
                        SoLuongContainer.Update(transaction);
                    }
                    else
                    {
                        SoLuongContainer.TKMD_ID = this.ID;
                        SoLuongContainer.Insert(transaction);
                    }
                    #endregion
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public void InsertUpdateFullNoneTransaction()
        {
            try
            {
                if (this.ID == 0)
                    this.ID = this.InsertTransaction(null);
                else
                    this.UpdateTransaction(null);
                int i = 1;
                foreach (HangMauDich hmd in this.HMDCollection)
                {

                    hmd.SoThuTuHang = i++;
                    if (this.ID == 0)
                        hmd.ID = 0;
                    HangMauDich HDMCu = new HangMauDich();
                    HDMCu.ID = hmd.ID;
                    if (HDMCu.ID > 0)
                        HDMCu.Load();
                    if (hmd.ID == 0)
                    {
                        hmd.TKMD_ID = this.ID;
                        hmd.ID = hmd.Insert(null);
                    }
                    else
                    {
                        hmd.Update(null);
                    }
                    if (this.MaLoaiHinh.StartsWith("NGC"))
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Nhap NPL
                            NguyenPhuLieu npl = new NguyenPhuLieu();
                            npl.HopDong_ID = this.IDHopDong;
                            npl.Ma = hmd.MaPhu;
                            npl.Load();
                            npl.SoLuongDaNhap += hmd.SoLuong;

                            if (HDMCu.ID > 0)
                            {
                                if (HDMCu.MaPhu == hmd.MaPhu)
                                {
                                    //ko doi ma chi can cap nhat lai luong                                        
                                    npl.SoLuongDaNhap -= HDMCu.SoLuong;
                                }
                                else
                                {
                                    //cap nhat thong tin so luong cho ma cu                                      
                                    NguyenPhuLieu nplCu = new NguyenPhuLieu();
                                    nplCu.HopDong_ID = this.IDHopDong;
                                    nplCu.Ma = HDMCu.MaPhu;
                                    nplCu.Load();
                                    nplCu.SoLuongDaNhap -= HDMCu.SoLuong;
                                    nplCu.UpdateTransaction(null);
                                }
                            }
                            npl.UpdateTransaction(null);
                            #endregion Nhap NPL
                        }
                        else
                        {
                            #region Nhap Thiet BI
                            ThietBi tb = new ThietBi();
                            tb.HopDong_ID = this.IDHopDong;
                            tb.Ma = hmd.MaPhu;
                            tb.Load();
                            tb.SoLuongDaNhap += hmd.SoLuong;
                            if (HDMCu.ID > 0)
                            {
                                if (HDMCu.MaPhu == hmd.MaPhu)
                                {
                                    tb.SoLuongDaNhap -= HDMCu.SoLuong;
                                }
                                else
                                {
                                    ThietBi tbCu = new ThietBi();
                                    tbCu.HopDong_ID = this.IDHopDong;
                                    tbCu.Ma = HDMCu.MaPhu;
                                    tbCu.Load();
                                    tbCu.SoLuongDaNhap -= HDMCu.SoLuong;
                                    tbCu.UpdateTransaction(null);
                                }
                            }
                            tb.UpdateTransaction(null);
                            #endregion Nhap Thiet BI
                        }
                    }
                    else
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Xuat NPL
                            NguyenPhuLieu npl = new NguyenPhuLieu();
                            npl.HopDong_ID = this.IDHopDong;
                            npl.Ma = hmd.MaPhu;
                            npl.Load();
                            npl.SoLuongDaDung += hmd.SoLuong;
                            if (HDMCu.ID > 0)
                            {
                                if (HDMCu.MaPhu == hmd.MaPhu)
                                {
                                    npl.SoLuongDaDung -= HDMCu.SoLuong;
                                }
                                else
                                {
                                    NguyenPhuLieu nplCu = new NguyenPhuLieu();
                                    nplCu.HopDong_ID = this.IDHopDong;
                                    nplCu.Ma = HDMCu.MaPhu;
                                    nplCu.Load();
                                    nplCu.SoLuongDaDung -= HDMCu.SoLuong;
                                    nplCu.UpdateTransaction(null);
                                }
                            }
                            npl.UpdateTransaction(null);
                            #endregion Xuat NPL
                        }
                        else if (LoaiHangHoa == "T")
                        {
                            #region Xuat TB
                            ThietBi tb = new ThietBi();
                            tb.HopDong_ID = this.IDHopDong;
                            tb.Ma = hmd.MaPhu;
                            tb.Load();
                            tb.SoLuongDaNhap -= hmd.SoLuong;
                            if (HDMCu.ID > 0)
                            {
                                if (HDMCu.MaPhu == hmd.MaPhu)
                                {
                                    tb.SoLuongDaNhap += HDMCu.SoLuong;
                                }
                                else
                                {
                                    ThietBi tbCu = new ThietBi();
                                    tbCu.HopDong_ID = this.IDHopDong;
                                    tbCu.Ma = HDMCu.MaPhu;
                                    tbCu.Load();
                                    tbCu.SoLuongDaNhap += HDMCu.SoLuong;
                                    tbCu.UpdateTransaction(null);
                                }
                            }
                            tb.UpdateTransaction(null);
                            #endregion Xuat Thiet bi
                        }
                        else
                        {
                            SanPham sp = new SanPham();
                            sp.HopDong_ID = this.IDHopDong;
                            sp.Ma = hmd.MaPhu;
                            sp.Load();
                            sp.SoLuongDaXuat += hmd.SoLuong;
                            if (HDMCu.ID > 0)
                            {
                                if (HDMCu.MaPhu == hmd.MaPhu)
                                {
                                    sp.SoLuongDaXuat -= HDMCu.SoLuong;
                                }
                                else
                                {
                                    SanPham spCu = new SanPham();
                                    spCu.HopDong_ID = this.IDHopDong;
                                    spCu.Ma = HDMCu.MaPhu;
                                    spCu.Load();
                                    spCu.SoLuongDaXuat -= HDMCu.SoLuong;
                                    spCu.UpdateTransaction(null);
                                }
                            }
                            sp.UpdateTransaction(null);
                            if (HDMCu.ID > 0)
                            {
                                DataSet dsLuongNPLCu = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HDMCu.MaPhu, HDMCu.SoLuong, this.IDHopDong);
                                foreach (DataRow row in dsLuongNPLCu.Tables[0].Rows)
                                {
                                    NguyenPhuLieu npl = new NguyenPhuLieu();
                                    npl.HopDong_ID = this.IDHopDong;
                                    npl.Ma = row["MaNguyenPhuLieu"].ToString();
                                    npl.Load();
                                    npl.SoLuongDaDung -= Convert.ToDecimal(row["LuongCanDung"]);
                                    npl.UpdateTransaction(null);
                                }
                            }
                            DataSet dsLuongNPL = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(sp.Ma, hmd.SoLuong, this.IDHopDong);
                            foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                            {
                                NguyenPhuLieu npl = new NguyenPhuLieu();
                                npl.HopDong_ID = this.IDHopDong;
                                npl.Ma = row["MaNguyenPhuLieu"].ToString();
                                npl.Load();
                                npl.SoLuongDaDung += Convert.ToDecimal(row["LuongCanDung"]);
                                npl.Update();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void LoadHMDCollection()
        {
            //             HangMauDich hmd = new HangMauDich();
            //             hmd.TKMD_ID = this.ID;
            if (this.ID > 0)
            {
                TrangThaiVNACCS = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetFromTKMD(this.SoToKhai, this.MaLoaiHinh);
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
                foreach (HangMauDich item in this.HMDCollection)
                {
                    item.LoadMienGiamThue();
                }
            }
        }
        public void LoadHMDCollection(SqlTransaction tran, string dbName)
        {
            HangMauDich hmd = new HangMauDich();
            hmd.TKMD_ID = this.ID;
            if (this.ID > 0)
            {
                TrangThaiVNACCS = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetFromTKMD(this.SoToKhai, this.MaLoaiHinh, tran, db);
                this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID(tran, dbName);
                foreach (HangMauDich item in this.HMDCollection)
                {
                    item.LoadMienGiamThue();
                }
            }
        }
        public void LoadHMDCollection(SqlTransaction tran)
        {
            HangMauDich hmd = new HangMauDich();
            hmd.TKMD_ID = this.ID;
            if (this.ID > 0)
            {
                TrangThaiVNACCS = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetFromTKMD(this.SoToKhai, this.MaLoaiHinh, tran, null);
                this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID(tran);
                foreach (HangMauDich item in this.HMDCollection)
                {
                    item.LoadMienGiamThue();
                }
            }
        }
        public void LoadHMDCollection(string dbName)
        {
            HangMauDich hmd = new HangMauDich();
            hmd.TKMD_ID = this.ID;
            if (this.ID > 0)
            {
                TrangThaiVNACCS = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetFromTKMD(this.SoToKhai, this.MaLoaiHinh, null, db);
                this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID(dbName);
                foreach (HangMauDich item in this.HMDCollection)
                {
                    item.LoadMienGiamThue();
                }
            }
        }

        public void LoadChungTuTKCollection()
        {
            ChungTu ct = new ChungTu();
            ct.Master_ID = this.ID;
            this.ChungTuTKCollection = ct.SelectCollectionBy_Master_ID();
        }
        //-----------------------------------------------------------------------------------------

        public bool Delete()
        {
            bool ret;
            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            long id = this.ID;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (HangMauDich HMD in this.HMDCollection)
                    {
                        HMD.Delete(this.LoaiHangHoa, this.MaLoaiHinh, this.IDHopDong, transaction);
                    }
                    if (this.MaLoaiHinh.Substring(0, 1) == "N" && this.LoaiHangHoa == "N")
                        NPLNhapTonThucTe.DeleteNPLTonByToKhai(this.SoToKhai, (short)this.NgayDangKy.Year, this.MaLoaiHinh, this.MaHaiQuan, transaction);
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool CheckExistToKhaiMauDich(string MaHQ, string MaLH, int SoToKhai, DateTime NamDK)
        {
            string sql = "select * from t_KDT_ToKhaiMauDich where SoToKhai=" + SoToKhai + " and year(NgayDangKy) =@NamDK and MaHaiQuan='" + MaHQ + "' and MaLoaiHinh='" + MaLH + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK.Year);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            else return true;
        }
        public bool InsertDongBoDuLieu()
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    long ID = this.InsertTransaction(transaction);
                    int i = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;
                        hmd.TKMD_ID = ID;
                        hmd.ID = hmd.Insert(transaction);
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #region Report

        public DataSet GetToKhaiMauDichCoCungUng(long IDHopDong)
        {
            string sql = " SELECT * From v_ToKhaiMauDichCoCungUng Where IDHopDong = @IDHopDong AND MaLoaiHinh Like 'X%'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetHangCungUng(long TKMD_ID)
        {
            string sql = " SELECT * From v_HangCungUng Where TKMD_ID = @TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet SelectToKhaiMauDichNhap()
        {
            string sql = " SELECT  "
                            + " row_number() over ( order by A.ID ) as STT, "
                            + " A.SoToKhai , "
                            + " A.SoHopDong , "
                            + " A.MaLoaiHinh , "
                            + " A.NgayDangKy, "
                            + " A.MaHaiQuan, "
                            + " A.MaDoanhNghiep , "
                            + " B.Ten , "
                            + " A.Canbodangky ,"
                            + " A.LoaiHangHoa "
                            + " FROM t_KDT_ToKhaiMauDich A , t_HaiQuan_CuaKhau B"
                            + " Where MaLoaiHinh like 'N%' "
                            + " AND A.CuaKhau_ID = B.ID ";
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(cmd);
        }

        public DataSet SelectToKhaiMauDichXuat()
        {
            string sql = " SELECT  "
                            + " row_number() over ( order by A.ID ) as STT, "
                            + " A.SoToKhai , "
                            + " A.SoHopDong , "
                            + " A.MaLoaiHinh , "
                            + " A.NgayDangKy, "
                            + " A.MaHaiQuan, "
                            + " A.MaDoanhNghiep , "
                            + " B.Ten , "
                            + " A.Canbodangky ,"
                            + " A.LoaiHangHoa "
                            + " FROM t_KDT_ToKhaiMauDich A , t_HaiQuan_CuaKhau B"
                            + " Where MaLoaiHinh like 'X%' "
                            + " AND A.CuaKhau_ID = B.ID ";
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(cmd);
        }

        //Get tokhai :
        public DataSet GetToKhaiNhapHaveThanhKhoan() // get all tokhai in database
        {
            string sql = " SELECT * " +
                         " from   t_KDT_ToKhaiMauDich " +
                         " where  IDHopDong = @IDHopDong AND MaLoaiHinh like 'N%' AND TrangThaiXuLy =1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            //db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;

        }

        public DataSet GetSoToKhaiNhapTheoHopDong(long IDHopDong)
        {
            string sql = " SELECT ID,SoToKhai, MaLoaiHinh, NgayDangKy, MaHaiQuan " +
                       " from   t_KDT_ToKhaiMauDich " +
                       " where  IDHopDong = @IDHopDong AND  TrangThaiXuLy =1 AND MaLoaiHinh like 'N%' AND LoaiHangHoa= 'N'";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetToKhaiMauDichNhapTheoHopDong(long IDHopDong)
        {
            string sql = " SELECT * " +
                       " from   t_KDT_ToKhaiMauDich " +
                       " where  IDHopDong = @IDHopDong AND MaLoaiHinh like 'N%' AND LoaiHangHoa= 'N'";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetSoToKhaiNhapTheoHopDongUnion(long IDHopDong)
        {
            string sql = " SELECT ID,SoToKhai, MaLoaiHinh, NgayDangKy, MaHaiQuan "
                       + " from   t_KDT_ToKhaiMauDich "
                       + " where  IDHopDong = @IDHopDong "
                       + " AND ( TrangThaiXuLy =1 OR TrangThaiXuLy =0 ) "
                       + " AND MaLoaiHinh like 'NGC%' "
                       + " AND LoaiHangHoa= 'N'"
                       + " UNION "
                       + " SELECT ID,SoToKhai, MaLoaiHinh, NgayDangKy, MaHaiQuanTiepNhan as [MaHaiQuan] "
                       + " from   t_KDT_GC_ToKhaiChuyenTiep "
                       + " where  IDHopDong = @IDHopDong  "
                       + " AND ( TrangThaiXuLy =1 OR TrangThaiXuLy =0) "
                       + " AND ( MaLoaiHinh='PHPLN' OR MaLoaiHinh='NGC18')";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetSoToKhaiNhapTheoHopDongT(long IDHopDong)
        {
            string sql = " SELECT ID,SoToKhai, MaLoaiHinh, NgayDangKy, MaHaiQuan "
                      + " from   t_KDT_ToKhaiMauDich "
                      + " where  IDHopDong = @IDHopDong  "
                      + " AND  ( TrangThaiXuLy =1 OR TrangThaiXuLy =0 )"
                      + " AND MaLoaiHinh like 'NGC%' AND LoaiHangHoa ='T'"
                      + " UNION "
                      + " SELECT ID,SoToKhai, MaLoaiHinh, NgayDangKy, MaHaiQuanTiepNhan as [MaHaiQuan] "
                      + " from   t_KDT_GC_ToKhaiChuyenTiep "
                      + " where  IDHopDong = @IDHopDong  "
                      + " AND ( TrangThaiXuLy =1 OR TrangThaiXuLy =0 ) "
                      + " AND ( MaLoaiHinh='PHTBN' OR MaLoaiHinh='NGC20')";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetSoToKhaiXuatTheoHopDong(long IDHopDong)
        {
            string sql = " SELECT ID,SoToKhai, MaLoaiHinh, NgayDangKy, MaHaiQuan "
                      + " from   t_KDT_ToKhaiMauDich "
                      + " where  IDHopDong = @IDHopDong  "
                      + " AND ( TrangThaiXuLy =1 OR TrangThaiXuLy =0 ) "
                      + " AND MaLoaiHinh like 'XGC%' AND LoaiHangHoa='S' "
                      + " UNION "
                      + " SELECT ID,SoToKhai, MaLoaiHinh, NgayDangKy, MaHaiQuanTiepNhan as [MaHaiQuan] "
                      + " from   t_KDT_GC_ToKhaiChuyenTiep "
                      + " where  IDHopDong = @IDHopDong  "
                      + " AND ( TrangThaiXuLy =1 OR TrangThaiXuLy =0 ) "
                      + " AND ( MaLoaiHinh='PHSPX' OR MaLoaiHinh='XGC19')";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public static DataSet GetToKhaiNK(long IDHopDong)
        {
            string sql = "SELECT * " +
                         "from   t_KDT_ToKhaiMauDich " +
                         "where  IDHopDong = @IDHopDong AND MaLoaiHinh like 'N%' AND TrangThaiXuLy =1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetHangMauDich(long IDHopDong)
        {
            string sql = "SELECT T.ID, T.SoTiepNhan, T.NgayTiepNhan, T.MaHaiQuan, T.SoToKhai, T.MaLoaiHinh, T.NgayDangKy, H.MaPhu, H.TenHang, H.SoLuong, H.DVT_ID" +
                         " From t_KDT_ToKhaiMauDich as T inner join t_KDT_HangMauDich as H" +
                         " On T.ID = H.TKMD_ID" +
                         " where  T.IDHopDong = @IDHopDong AND T.MaLoaiHinh like 'NGC%' AND T.TrangThaiXuLy =1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }


        public DataSet GetHangMauDichXuatNPL(long IDHopDong)
        {
            string sql = "SELECT T.ID, T.SoTiepNhan, T.NgayTiepNhan, T.MaHaiQuan, T.SoToKhai, T.MaLoaiHinh, T.NgayDangKy, H.MaPhu, H.TenHang, H.SoLuong, H.DVT_ID" +
                         " From t_KDT_ToKhaiMauDich as T inner join t_KDT_HangMauDich as H" +
                         " On T.ID = H.TKMD_ID" +
                         " where  T.IDHopDong = @IDHopDong AND T.MaLoaiHinh like 'XGC%' AND T.TrangThaiXuLy =1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet GetToKhaiXK(long IDHopDong)
        {
            string sql = "SELECT * " +
                         "from   t_KDT_ToKhaiMauDich " +
                         "where IDHopDong = @IDHopDong AND MaLoaiHinh like 'X%' AND TrangThaiXuLy =1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetNPLXuatToKhai()
        {
            string sql = "SELECT e.MaPhu, e.TenHang, e.DVT_ID, Sum(e.SoLuong) as SoLuong FROM (" +
                         "SELECT b.MaNguyenPhuLieu as MaPhu, c.Ten as TenHang,c.DVT_ID, Round(b.DinhMucSuDung *(100 + b.TyLeHaoHut)/100 * a.SoLuong,2) as SoLuong FROM t_KDT_HangMauDich a INNER JOIN t_GC_DinhMuc b ON a.MaPhu = b.MaSanPham INNER JOIN t_GC_NguyenPhuLieu c ON b.MaNguyenPhuLieu = c.Ma " +
                         "WHERE a.TKMD_ID = " + this.ID + " AND b.HopDong_ID =" + this.IDHopDong + " AND c.HopDong_ID =" + this.IDHopDong + ")e " +
                         "GROUP BY e.MaPhu, e.TenHang, e.DVT_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            return db.ExecuteDataSet(dbCommand);
        }
        public DataSet GetNPLCungUngTK(long TKMD_ID)
        {
            string sql = "SELECT * FROM t_View_KDT_NPLCungUng WHERE TKMD_ID = " + TKMD_ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            return db.ExecuteDataSet(dbCommand);
        }



        public DataSet GetHangMauDichNPLXuat(long IDHopDong)
        {
            string sql = "Select * from t_View_KDT_GC_HMD where IDHopDong = @IDHopDong AND MaLoaiHinh like 'XGC%' AND TrangThaiXuLy = 1 AND LoaiHangHoa = 'N'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetHangMauDichSPXuat(long IDHopDong)
        {
            string sql = "Select DISTINCT MaPhu,DVT_ID from t_View_KDT_GC_HMD_HD where IDHopDong = @IDHopDong AND MaLoaiHinh like 'XGC%' AND TrangThaiXuLy = 1 AND LoaiHangHoa = 'S'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }


        public DataSet GetHangMauDichNPLNhap(long IDHopDong)
        {
            string sql = "Select DISTINCT MaPhu,DVT_ID from t_View_KDT_GC_HMD_HD where IDHopDong = @IDHopDong AND MaLoaiHinh like 'NGC%' AND TrangThaiXuLy = 1 AND LoaiHangHoa = 'N'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }


        public DataSet GetHangMauDichNPLNhapAll(long IDHopDong)
        {
            string sql = "Select DISTINCT MaPhu from v_KDT_NPL_Nhap_HD where IDHopDong = @IDHopDong";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetSPXuatAll(long IDHopDong)
        {
            string sql = "Select DISTINCT MaPhu from v_KDT_SP_Xuat_HD where IDHopDong = @IDHopDong";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        //ToKhai
        #region ThongTinToKhai
        public DataSet GetToKhaiNhapInfo(long IDHopDong)
        {
            //string sql = " SELECT  dbo.t_KDT_HangMauDich.MaPhu AS [MaHang], dbo.t_KDT_HangMauDich.TenHang, dbo.t_KDT_HangMauDich.SoLuong, dbo.t_KDT_ToKhaiMauDich.SoToKhai, " 
            //          + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, dbo.t_KDT_ToKhaiMauDich.NgayDangKy,t_HaiQuan_DonViTinh.ten DVT "
            //          + " FROM   dbo.t_KDT_HangMauDich INNER JOIN "
            //          + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
            //          + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_HangMauDich.DVT_ID "
            //          + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
            //          + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
            //          + " AND MaLoaiHinh like 'N%' AND LoaiHangHoa= 'N'";

            string sql = "SELECT "
            + " dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
            + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, "
            + " dbo.t_KDT_ToKhaiMauDich.NgayDangKy,"
            + " dbo.t_KDT_HangMauDich.MaPhu as [MaHang],"
            + " dbo.t_KDT_HangMauDich.TenHang,"
            + " dbo.t_KDT_HangMauDich.SoLuong, "
            + " t_HaiQuan_DonViTinh.ten as [DVT],"
            + " t_KDT_ToKhaiMauDich.IDHopDong"
            + " FROM   dbo.t_KDT_HangMauDich INNER JOIN "
            + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
            + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_HangMauDich.DVT_ID "
            + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
            + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
            + " AND t_KDT_ToKhaiMauDich.MaLoaiHinh like 'N%' AND t_KDT_ToKhaiMauDich.LoaiHangHoa= 'N' "
            + " UNION "
            + " SELECT "
            + " dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai,"
            + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,"
            + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, "
            + " dbo.t_KDT_GC_HangChuyenTiep.MaHang, "
            + " dbo.t_KDT_GC_HangChuyenTiep.TenHang, "
            + " dbo.t_KDT_GC_HangChuyenTiep.SoLuong, "
            + " t_HaiQuan_DonViTinh.ten  as [DVT],"
            + " dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong"
            + " FROM   dbo.t_KDT_GC_HangChuyenTiep INNER JOIN"
            + " dbo.t_KDT_GC_ToKhaiChuyenTiep ON dbo.t_KDT_GC_HangChuyenTiep.Master_ID = dbo.t_KDT_GC_ToKhaiChuyenTiep.ID"
            + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_GC_HangChuyenTiep.ID_DVT "
            + " WHERE t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 "
            + " AND t_KDT_GC_ToKhaiChuyenTiep.IDHopDong =@IDHopDong"
            + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,3,2) = 'PL' "
            + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,5,1) = 'N' ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetToKhaiNhapThietBi(long HopDongID)
        {
            string sql = "Select ID,SoToKhai,MaLoaiHinh,NgayDangKy from t_KDT_ToKhaiMauDich Where IDHopDong = @IDHopDong AND MaLoaiHinh LIKE 'NGC%' AND TrangThaiXuLy = 1 AND LoaiHangHoa = 'T'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, HopDongID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetToKhaiXuatThietBi(long HopDongID)
        {
            string sql = "Select ID,SoToKhai,MaLoaiHinh,NgayDangKy from t_KDT_ToKhaiMauDich Where IDHopDong = @IDHopDong AND MaLoaiHinh LIKE 'XGC%' AND TrangThaiXuLy = 1 AND LoaiHangHoa = 'T'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, HopDongID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetToKhaiXuatThietBiAll(long IDHopDong)
        {
            string sql = "SELECT "
           + " dbo.t_KDT_ToKhaiMauDich.ID, "
           + " dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
           + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, "
           + " dbo.t_KDT_ToKhaiMauDich.NgayDangKy"
           + " FROM   dbo.t_KDT_HangMauDich INNER JOIN "
           + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
           + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_HangMauDich.DVT_ID "
           + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
           + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
           + " AND t_KDT_ToKhaiMauDich.MaLoaiHinh like 'XGC%' AND t_KDT_ToKhaiMauDich.LoaiHangHoa= 'T' "
           + " UNION "
           + " SELECT "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.ID,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy "
           + " FROM   dbo.t_KDT_GC_HangChuyenTiep INNER JOIN"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep ON dbo.t_KDT_GC_HangChuyenTiep.Master_ID = dbo.t_KDT_GC_ToKhaiChuyenTiep.ID"
           + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_GC_HangChuyenTiep.ID_DVT "
           + " WHERE t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 "
           + " AND t_KDT_GC_ToKhaiChuyenTiep.IDHopDong =@IDHopDong"
           + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,3,2) = 'TB'"
           + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,5,1) = 'X'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetToKhaiVaThietBiNhap(long IDHopDong)
        {
            string sql = "SELECT "
           + " dbo.t_KDT_ToKhaiMauDich.ID, "
           + " dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
           + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, "
           + " dbo.t_KDT_ToKhaiMauDich.NgayDangKy,"
           + " dbo.t_KDT_HangMauDich.MaPhu as [MaHang],"
           + " dbo.t_KDT_HangMauDich.TenHang,"
           + " dbo.t_KDT_HangMauDich.SoLuong, "
           + " t_HaiQuan_DonViTinh.ten as [DVT],"
           + " t_KDT_ToKhaiMauDich.IDHopDong"
           + " FROM   dbo.t_KDT_HangMauDich INNER JOIN "
           + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
           + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_HangMauDich.DVT_ID "
           + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
           + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
           + " AND t_KDT_ToKhaiMauDich.MaLoaiHinh like 'NGC%' AND t_KDT_ToKhaiMauDich.LoaiHangHoa= 'T' ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetToKhaiVaThietBiXuatAll(long IDHopDong)
        {
            string sql = "SELECT "
           + " dbo.t_KDT_ToKhaiMauDich.ID, "
           + " dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
           + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, "
           + " dbo.t_KDT_ToKhaiMauDich.NgayDangKy,"
           + " dbo.t_KDT_HangMauDich.MaPhu as [MaHang],"
           + " dbo.t_KDT_HangMauDich.TenHang,"
           + " dbo.t_KDT_HangMauDich.SoLuong, "
           + " t_HaiQuan_DonViTinh.ten as [DVT],"
           + " t_KDT_ToKhaiMauDich.IDHopDong"
           + " FROM   dbo.t_KDT_HangMauDich INNER JOIN "
           + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
           + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_HangMauDich.DVT_ID "
           + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
           + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
           + " AND t_KDT_ToKhaiMauDich.MaLoaiHinh like 'XGC%' AND t_KDT_ToKhaiMauDich.LoaiHangHoa= 'T' "
           + " UNION "
           + " SELECT "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.ID,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, "
           + " dbo.t_KDT_GC_HangChuyenTiep.MaHang, "
           + " dbo.t_KDT_GC_HangChuyenTiep.TenHang, "
           + " dbo.t_KDT_GC_HangChuyenTiep.SoLuong, "
           + " t_HaiQuan_DonViTinh.ten  as [DVT],"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong"
           + " FROM   dbo.t_KDT_GC_HangChuyenTiep INNER JOIN"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep ON dbo.t_KDT_GC_HangChuyenTiep.Master_ID = dbo.t_KDT_GC_ToKhaiChuyenTiep.ID"
           + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_GC_HangChuyenTiep.ID_DVT "
           + " WHERE t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 "
           + " AND t_KDT_GC_ToKhaiChuyenTiep.IDHopDong =@IDHopDong"
           + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,3,2) = 'TB'"
           + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,5,1) = 'X'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }



        public DataSet GetToKhaiXuatInfo(long IDHopDong)
        {
            //string sql = " SELECT  dbo.t_KDT_HangMauDich.MaPhu AS [MaHang], dbo.t_KDT_HangMauDich.TenHang, dbo.t_KDT_HangMauDich.SoLuong, dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
            //          + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, dbo.t_KDT_ToKhaiMauDich.NgayDangKy,t_HaiQuan_DonViTinh.ten DVT "
            //          + " FROM   dbo.t_KDT_HangMauDich INNER JOIN "
            //          + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
            //          + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_HangMauDich.DVT_ID "
            //          + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
            //          + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
            //          + " AND MaLoaiHinh like 'X%' AND LoaiHangHoa= 'S' ";

            string sql = "SELECT "
           + " dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
           + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, "
           + " dbo.t_KDT_ToKhaiMauDich.NgayDangKy,"
           + " dbo.t_KDT_HangMauDich.MaPhu as [MaHang],"
           + " dbo.t_KDT_HangMauDich.TenHang,"
           + " dbo.t_KDT_HangMauDich.SoLuong, "
           + " t_HaiQuan_DonViTinh.ten as [DVT],"
           + " t_KDT_ToKhaiMauDich.IDHopDong"
           + " FROM   dbo.t_KDT_HangMauDich INNER JOIN "
           + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
           + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_HangMauDich.DVT_ID "
           + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
           + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
           + " AND t_KDT_ToKhaiMauDich.MaLoaiHinh like 'X%' AND t_KDT_ToKhaiMauDich.LoaiHangHoa= 'S' "
           + " UNION "
           + " SELECT "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, "
           + " dbo.t_KDT_GC_HangChuyenTiep.MaHang, "
           + " dbo.t_KDT_GC_HangChuyenTiep.TenHang, "
           + " dbo.t_KDT_GC_HangChuyenTiep.SoLuong, "
           + " t_HaiQuan_DonViTinh.ten  as [DVT],"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong"
           + " FROM   dbo.t_KDT_GC_HangChuyenTiep INNER JOIN"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep ON dbo.t_KDT_GC_HangChuyenTiep.Master_ID = dbo.t_KDT_GC_ToKhaiChuyenTiep.ID"
           + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_GC_HangChuyenTiep.ID_DVT "
           + " WHERE t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 "
           + " AND t_KDT_GC_ToKhaiChuyenTiep.IDHopDong =@IDHopDong"
           + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,3,2) = 'SP' "
           + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,5,1) = 'X' ";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetToKhaiXuatThietBiInfo(long IDHopDong)
        {
            string sql = "SELECT "
           + " dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
           + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, "
           + " dbo.t_KDT_ToKhaiMauDich.NgayDangKy,"
           + " dbo.t_KDT_HangMauDich.MaPhu as [MaHang],"
           + " dbo.t_KDT_HangMauDich.TenHang,"
           + " dbo.t_KDT_HangMauDich.SoLuong, "
           + " t_HaiQuan_DonViTinh.ten as [DVT],"
           + " t_KDT_ToKhaiMauDich.IDHopDong"
           + " FROM   dbo.t_KDT_HangMauDich INNER JOIN "
           + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
           + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_HangMauDich.DVT_ID "
           + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
           + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
           + " AND t_KDT_ToKhaiMauDich.MaLoaiHinh like 'N%' AND t_KDT_ToKhaiMauDich.LoaiHangHoa= 'T' "
           + " UNION "
           + " SELECT "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, "
           + " dbo.t_KDT_GC_HangChuyenTiep.MaHang, "
           + " dbo.t_KDT_GC_HangChuyenTiep.TenHang, "
           + " dbo.t_KDT_GC_HangChuyenTiep.SoLuong, "
           + " t_HaiQuan_DonViTinh.ten  as [DVT],"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong"
           + " FROM   dbo.t_KDT_GC_HangChuyenTiep INNER JOIN"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep ON dbo.t_KDT_GC_HangChuyenTiep.Master_ID = dbo.t_KDT_GC_ToKhaiChuyenTiep.ID"
           + " Inner join dbo.t_HaiQuan_DonViTinh on t_HaiQuan_DonViTinh.ID =t_KDT_GC_HangChuyenTiep.ID_DVT "
           + " WHERE t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 "
           + " AND t_KDT_GC_ToKhaiChuyenTiep.IDHopDong =@IDHopDong"
           + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,3,2) = 'TB'"
           + " AND  Substring(t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh,5,1) = 'N'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        #endregion ThongTinToKhai

        #region Theo Thông tư 74

        public DataSet GetToKhaiForMau03TT74(long IDHopDong)
        {
            string sql = "SELECT "
           + " dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
           + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, "
           + " dbo.t_KDT_ToKhaiMauDich.NgayDangKy,"
           + " t_KDT_ToKhaiMauDich.IDHopDong,"
           + " t_KDT_ToKhaiMauDich.MaHaiQuan"
           + " FROM t_KDT_ToKhaiMauDich"
           + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
           + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
           + " AND t_KDT_ToKhaiMauDich.MaLoaiHinh like 'XGC%'AND t_KDT_ToKhaiMauDich.LoaiHangHoa= 'N' "
           + " UNION "
           + " SELECT "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan"
           + " FROM t_KDT_GC_ToKhaiChuyenTiep"
           + " WHERE t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 "
           + " AND t_KDT_GC_ToKhaiChuyenTiep.IDHopDong =@IDHopDong"
           + " AND  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHPLX' OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC18')";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetToKhaiForMau02TT74(long IDHopDong)
        {
            string sql = "SELECT "
           + " dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
           + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, "
           + " dbo.t_KDT_ToKhaiMauDich.NgayDangKy,"
           + " t_KDT_ToKhaiMauDich.IDHopDong,"
           + " t_KDT_ToKhaiMauDich.MaHaiQuan"
           + " FROM t_KDT_ToKhaiMauDich"
           + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
           + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
           + " AND t_KDT_ToKhaiMauDich.MaLoaiHinh Like 'XGC%' AND t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' "
           + " UNION "
           + " SELECT "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan"
           + " FROM  t_KDT_GC_ToKhaiChuyenTiep "
           + " WHERE t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 "
           + " AND t_KDT_GC_ToKhaiChuyenTiep.IDHopDong =@IDHopDong"
           + " AND  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX' OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19')";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetToKhaiForMau01TT74(long IDHopDong)
        {
            string sql = "SELECT "
           + " dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
           + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, "
           + " dbo.t_KDT_ToKhaiMauDich.NgayDangKy,"
           + " t_KDT_ToKhaiMauDich.IDHopDong,"
           + " t_KDT_ToKhaiMauDich.MaHaiQuan"
           + " FROM t_KDT_ToKhaiMauDich"
           + " WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 "
           + " AND t_KDT_ToKhaiMauDich.IDHopDong =@IDHopDong "
           + " AND t_KDT_ToKhaiMauDich.MaLoaiHinh Like 'NGC%' AND t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N' "
           + " UNION "
           + " SELECT "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, "
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong,"
           + " dbo.t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan"
           + " FROM  t_KDT_GC_ToKhaiChuyenTiep "
           + " WHERE t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 "
           + " AND t_KDT_GC_ToKhaiChuyenTiep.IDHopDong =@IDHopDong"
           + " AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHPLN' OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPN' "
           + " OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'NGC18' OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'NGC19') ";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        #endregion

        #endregion Report

        #region Webservice của hải quan

        public string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            //DATLMQ bổ sung kiểm tra GUIDSTR Tờ khai 10/01/2011
            //if (this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            //    nodeReference.InnerText = this.GUIDSTR.ToUpper();
            //else
            //{
            //    this.GUIDSTR = (System.Guid.NewGuid().ToString().ToUpper());
            //    nodeReference.InnerText = this.GUIDSTR;
            //}
            nodeReference.InnerText = this.GUIDSTR;
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep.Trim();
            //this.Update();
            return doc.InnerXml;
        }

        public string ConfigPhongBiSua(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR.ToUpper();

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep.Trim();
            this.Update();
            return doc.InnerXml;
        }

        public void TransgferDataToNPLTonThucTe(DateTime NgayHangVeKho)
        {
            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    if (this.MaLoaiHinh.Substring(0, 1) == "N" && this.LoaiHangHoa == "N")
                    {
                        foreach (HangMauDich hmd in this.HMDCollection)
                        {
                            //CAP NHAT VAO LUONG TON THUC TE
                            NPLNhapTonThucTe nplNhapTonThucTe = new NPLNhapTonThucTe();
                            nplNhapTonThucTe.SoToKhai = this.SoToKhai;
                            nplNhapTonThucTe.MaLoaiHinh = this.MaLoaiHinh;
                            nplNhapTonThucTe.NamDangKy = (short)this.NgayDangKy.Year;
                            nplNhapTonThucTe.MaHaiQuan = this.MaHaiQuan;
                            nplNhapTonThucTe.MaNPL = hmd.MaPhu;
                            nplNhapTonThucTe.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplNhapTonThucTe.Luong = hmd.SoLuong;
                            nplNhapTonThucTe.Ton = hmd.SoLuong;
                            nplNhapTonThucTe.NgayDangKy = this.NgayDangKy;
                            nplNhapTonThucTe.NgayHangVeKho = NgayHangVeKho;
                            nplNhapTonThucTe.ID_HopDong = this.IDHopDong;
                            nplNhapTonThucTe.MuaVN = 0;
                            nplNhapTonThucTe.Insert(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void TransgferDataToNPLTonThucTe(string dbName)
        {
            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(dbName);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction, dbName);

                    if (this.MaLoaiHinh.StartsWith("NGC") && this.LoaiHangHoa == "N")
                    {
                        foreach (HangMauDich hmd in this.HMDCollection)
                        {

                            //CAP NHAT VAO LUONG TON THUC TE
                            NPLNhapTonThucTe nplNhapTonThucTe = new NPLNhapTonThucTe();
                            nplNhapTonThucTe.SoToKhai = this.SoToKhai;
                            nplNhapTonThucTe.MaLoaiHinh = this.MaLoaiHinh;
                            nplNhapTonThucTe.NamDangKy = (short)this.NgayDangKy.Year;
                            nplNhapTonThucTe.MaHaiQuan = this.MaHaiQuan;
                            nplNhapTonThucTe.MaNPL = hmd.MaPhu;
                            nplNhapTonThucTe.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplNhapTonThucTe.Luong = hmd.SoLuong;
                            nplNhapTonThucTe.Ton = hmd.SoLuong;
                            nplNhapTonThucTe.NgayDangKy = this.NgayDangKy;
                            nplNhapTonThucTe.ID_HopDong = this.IDHopDong;
                            nplNhapTonThucTe.Insert(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        #region Khai báo tờ khai nhập

        public string WSSendXMLNHAP(string pass)
        {
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiNhap, (int)MessageFunctions.KhaiBao));
            //XmlDocument docNPL = new XmlDocument();
            //docNPL.LoadXml(ConvertCollectionToXMLNHAPGCALL());

            ////luu vao string
            //XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            //XmlNode Content = doc.GetElementsByTagName("Content")[0];
            //Content.AppendChild(root);

            //XmlNode DU_LEU = doc.GetElementsByTagName("Root")[0];
            //XmlNode node = CO.ConvertCollectionCOToXML(doc, this.COCollection, this.ID);

            ////Hang hoa
            //List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            //listHang = ConvertHMDKDToHangMauDich();
            //if (node != null)
            //    DU_LEU.AppendChild(node);

            ////Giay phep
            //node = GiayPhep.ConvertCollectionGiayPhepToXML(doc, this.GiayPhepCollection, this.ID, listHang);
            //if (node != null)
            //    DU_LEU.AppendChild(node);

            ////Hoa don thuong mai
            //node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKN(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
            //if (node != null)
            //    DU_LEU.AppendChild(node);

            ////Hop dong thuong mai
            //node = HopDongThuongMai.ConvertCollectionHopDongToXML_TKN(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
            //if (node != null)
            //    DU_LEU.AppendChild(node);

            ////Van tai don
            //if (VanTaiDon == null)
            //{
            //    List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
            //    if (VanDonCollection != null && VanDonCollection.Count > 0)
            //    {
            //        VanTaiDon = VanDonCollection[0];
            //        VanTaiDon.LoadContainerCollection();
            //    }
            //}
            //if (VanTaiDon != null)
            //{
            //    node = this.VanTaiDon.ConvertVanDonToXML(doc);
            //    if (node != null)
            //        DU_LEU.AppendChild(node);
            //}

            ////Chuyen cua khau
            //node = DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, this.listChuyenCuaKhau, this.ID);
            //if (node != null)
            //    DU_LEU.AppendChild(node);

            ////Ctkem
            //ChungTuKem ctct = new ChungTuKem();

            //List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet> listCTCT = new List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet>();
            //listCTCT = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctct.LoadCT(this.ID));
            //if (listCTCT != null && listCTCT.Count != 0)
            //{
            //    node = ChungTuKem.ConvertCollectionCTDinhKemToXML(doc, ChungTuKemCollection, this.ID, listCTCT);
            //    if (node != null)
            //        DU_LEU.AppendChild(node);
            //}

            ////WebService
            //Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            //string kq = "";
            //string msgError = string.Empty;
            //try
            //{
            //    //Khai báo
            //    kq = kdt.Send(doc.InnerXml, pass);
            //    doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoToKhai);
            //    XmlDocument docResult = new XmlDocument();
            //    docResult.LoadXml(kq);
            //    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            //    {
            //        if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
            //        {
            //            return doc.InnerXml;
            //        }
            //    }
            //    else
            //    {
            //        msgError = "Thông báo từ hải quan :" + Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

            //        throw new Exception(msgError);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (msgError != string.Empty)
            //    {
            //        kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
            //        throw ex;
            //    }
            //    else
            //        if (!string.IsNullOrEmpty(kq))
            //            Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
            //    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
            //    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            //}
            return "";
        }

        private string ConvertCollectionToXMLNHAPGCALL()
        {
            //load du lieu
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                HangMauDich hmd = new HangMauDich();
                hmd.TKMD_ID = this.ID;
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }

            //load du lieu
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();


            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\khai bao to khai nhap.xml");
            //docNPL.Load(path + "\\B03GiaCong\\KhaiBaoToKhaiNhaptq.xml");
            //
            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/tokhai/MA_HQ");
            nodeHQNhan.InnerText = this.MaHaiQuan;

            XmlNode nodeSoTK = docNPL.SelectSingleNode("Root/tokhai/SoTK");
            nodeSoTK.InnerText = this.ID.ToString();

            XmlNode nodeMaLH = docNPL.SelectSingleNode("Root/tokhai/Ma_LH");
            nodeMaLH.InnerText = this.MaLoaiHinh;

            XmlNode nodeNamDK = docNPL.SelectSingleNode("Root/tokhai/NamDK");
            nodeNamDK.InnerText = DateTime.Today.Year.ToString();

            XmlNode nodeNgayDK = docNPL.SelectSingleNode("Root/tokhai/Ngay_DK");
            nodeNgayDK.InnerText = "";

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/tokhai/Ma_DV");
            nodeDN.InnerText = this.MaDoanhNghiep;

            XmlNode nodeDVUT = docNPL.SelectSingleNode("Root/tokhai/Ma_DVUT");
            nodeDVUT.InnerText = this.MaDonViUT;

            XmlNode nodeDVDoiTac = docNPL.SelectSingleNode("Root/tokhai/DV_TD");
            if (this.TenDonViDoiTac.Length <= 40)
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            else
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 40));
            XmlNode nodePTVT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTVT");
            nodePTVT.InnerText = this.PTVT_ID;

            XmlNode nodeSoHieuPTVT = docNPL.SelectSingleNode("Root/tokhai/Ten_PTVT");
            if (this.SoHieuPTVT.Length <= 20)
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT);
            else
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT.Substring(0, 20));
            XmlNode nodeNgayDen = docNPL.SelectSingleNode("Root/tokhai/NgayDen");
            if (this.NgayDenPTVT.Year > 1900)
                nodeNgayDen.InnerText = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeNgayDen.InnerText = "";

            XmlNode nodeVanDon = docNPL.SelectSingleNode("Root/tokhai/Van_Don");
            if (this.SoVanDon.Length <= 20)
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));

            XmlNode nodeCuaKhau = docNPL.SelectSingleNode("Root/tokhai/Ma_CK");
            nodeCuaKhau.InnerText = this.CuaKhau_ID;

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/tokhai/CangNN");
            if (this.DiaDiemXepHang.Length <= 40)
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));
            XmlNode nodeSoGiayPhep = docNPL.SelectSingleNode("Root/tokhai/So_GP");
            if (this.SoGiayPhep.Length <= 80)
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep);
            else
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Substring(0, 80));
            XmlNode nodeNgayGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_GP");
            if (NgayGiayPhep.Year > 1900)
                nodeNgayGiayPhep.InnerText = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayGiayPhep.InnerText = "";

            XmlNode nodeNgayHHGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHGP");
            if (NgayHetHanGiayPhep.Year > 1900)
                nodeNgayHHGiayPhep.InnerText = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayHHGiayPhep.InnerText = "";

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/tokhai/So_HD");
            nodeSoHopDong.InnerText = this.SoHopDong;

            XmlNode nodeTyGiaUSD = docNPL.SelectSingleNode("Root/tokhai/TyGia_USD");
            nodeTyGiaUSD.InnerText = BaseClass.Round(this.TyGiaUSD, 9);

            XmlNode nodeNgayHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HD");
            if (NgayHopDong.Year > 1900)
                nodeNgayHopDong.InnerText = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHopDong.InnerText = "";

            XmlNode nodeNgayHHHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHHD");
            if (NgayHetHanHopDong.Year > 1900)
                nodeNgayHHHopDong.InnerText = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHHHopDong.InnerText = "";

            XmlNode nodeNuocXK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_XK");
            nodeNuocXK.InnerText = this.NuocXK_ID;

            XmlNode nodeNuocNK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_NK");
            nodeNuocNK.InnerText = "VN";

            XmlNode nodeDieuKienGiaoHang = docNPL.SelectSingleNode("Root/tokhai/MA_GH");
            nodeDieuKienGiaoHang.InnerText = this.DKGH_ID;

            XmlNode nodeSoHang = docNPL.SelectSingleNode("Root/tokhai/SoHang");
            nodeSoHang.InnerText = this.HMDCollection.Count.ToString();

            XmlNode nodePTTT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTTT");
            nodePTTT.InnerText = this.PTTT_ID;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/tokhai/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;

            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/tokhai/TyGia_VND");

            nodeTyGiaVND.InnerText = BaseClass.Round(this.TyGiaTinhThue, 9);

            XmlNode nodeGiayTo = docNPL.SelectSingleNode("Root/tokhai/GiayTo");
            if (GiayTo.Length > 40)
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo);

            XmlNode nodeSoKien = docNPL.SelectSingleNode("Root/tokhai/So_Kien");
            nodeSoKien.InnerText = Convert.ToUInt32(this.SoKien).ToString();

            XmlNode nodeSoContainer20 = docNPL.SelectSingleNode("Root/tokhai/So_Container");
            nodeSoContainer20.InnerText = Convert.ToUInt64(this.SoContainer20).ToString();

            XmlNode nodeHDTM = docNPL.SelectSingleNode("Root/tokhai/So_HDTM");
            if (SoHoaDonThuongMai.Length <= 30)
                nodeHDTM.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai);
            else
                nodeHDTM.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai.Substring(0, 30));

            XmlNode nodeNgayHDTM = docNPL.SelectSingleNode("Root/tokhai/Ngay_HDTM");
            if (NgayHoaDonThuongMai.Year > 1900)
                nodeNgayHDTM.InnerText = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeNgayHDTM.InnerText = "";

            XmlNode nodeSoPLTK = docNPL.SelectSingleNode("Root/tokhai/So_PLTK");
            nodeSoPLTK.InnerText = this.SoLuongPLTK.ToString();

            XmlNode nodeChuHang = docNPL.SelectSingleNode("Root/tokhai/ChuHang");
            if (TenChuHang.Length > 30)
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang);
            XmlNode nodeNgayVanDon = docNPL.SelectSingleNode("Root/tokhai/Ngay_VanDon");
            if (NgayVanDon.Year > 1900)
                nodeNgayVanDon.InnerText = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeNgayVanDon.InnerText = "";

            XmlNode nodeTrongLuong = docNPL.SelectSingleNode("Root/tokhai/TrangLuong");
            nodeTrongLuong.InnerText = BaseClass.Round(this.TrongLuong, 9);

            XmlNode nodeDeXuatKhac = docNPL.SelectSingleNode("Root/tokhai/DE_XUAT_KHAC");
            if (nodeDeXuatKhac != null)
                nodeDeXuatKhac.InnerText = this.DeXuatKhac;

            XmlNode nodeSoContainer40 = docNPL.SelectSingleNode("Root/tokhai/SoContainer40");
            nodeSoContainer40.InnerText = Convert.ToUInt64(this.SoContainer40).ToString();

            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/tokhai/CTKT");
            nodeCTKT.InnerText = "";

            XmlNode nodeMaMid = docNPL.SelectSingleNode("Root/tokhai/MA_MID");
            nodeMaMid.InnerText = this.MaMid;

            //XmlNode nodePHIBH = docNPL.SelectSingleNode("Root/tokhai/PHI_BH");
            //nodePHIBH.InnerText = BaseClass.Round(this.PhiBaoHiem,9);

            //XmlNode nodePHIVC = docNPL.SelectSingleNode("Root/tokhai/PHI_VC");
            //nodePHIVC.InnerText = BaseClass.Round(this.PhiVanChuyen, 9);

            //XmlNode nodeLEPHIHQ = docNPL.SelectSingleNode("Root/tokhai/LE_PHI_HQ");
            //nodeLEPHIHQ.InnerText = BaseClass.Round(this.LePhiHaiQuan, 9);


            //Hang mau dich
            XmlNode nodeHang = docNPL.SelectSingleNode("Root/hangTKs");
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("hangTK");

                XmlNode HangDetails = docNPL.CreateElement("HangDetails");
                HangDetails.InnerText = "";
                node.AppendChild(HangDetails);

                XmlNode MaHSAtt = docNPL.CreateElement("MaHS");
                MaHSAtt.InnerText = hmd.MaHS;
                node.AppendChild(MaHSAtt);

                XmlNode maAtt = docNPL.CreateElement("Ma_Hang");
                maAtt.InnerText = this.LoaiHangHoa + hmd.MaPhu;
                node.AppendChild(maAtt);

                XmlNode TenAtt = docNPL.CreateElement("Ten_Hang");
                TenAtt.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang);
                node.AppendChild(TenAtt);

                XmlNode NuocAtt = docNPL.CreateElement("Nuoc_XX");
                NuocAtt.InnerText = hmd.NuocXX_ID;
                node.AppendChild(NuocAtt);

                XmlNode Ma_DVTAtt = docNPL.CreateElement("Ma_DVT");
                Ma_DVTAtt.InnerText = hmd.DVT_ID;
                node.AppendChild(Ma_DVTAtt);

                XmlNode SoLuongAtt = docNPL.CreateElement("Luong");
                SoLuongAtt.InnerText = BaseClass.Round(hmd.SoLuong, 9);
                node.AppendChild(SoLuongAtt);

                XmlNode DonGiaAtt = docNPL.CreateElement("DGia_KB");
                DonGiaAtt.InnerText = BaseClass.Round(hmd.DonGiaKB, 9);
                node.AppendChild(DonGiaAtt);

                XmlNode TriGiaAtt = docNPL.CreateElement("TriGia_KB");
                TriGiaAtt.InnerText = BaseClass.Round(hmd.TriGiaKB, 9);
                node.AppendChild(TriGiaAtt);

                XmlNode TriGiaVNDAtt = docNPL.CreateElement("TGTTVND");
                TriGiaVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaTT, 9);
                node.AppendChild(TriGiaVNDAtt);

                XmlNode TGKBVNDVNDAtt = docNPL.CreateElement("TGKBVND");
                TGKBVNDVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaKB_VND, 9);
                node.AppendChild(TGKBVNDVNDAtt);

                XmlNode MienThue = docNPL.CreateElement("MienThue");
                MienThue.InnerText = BaseClass.Round((decimal)hmd.MienThue, 9);
                node.AppendChild(MienThue);

                XmlNode TS_XNKAtt = docNPL.CreateElement("TS_XNK");
                TS_XNKAtt.InnerText = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.AppendChild(TS_XNKAtt);

                XmlNode ThueXNKAtt = docNPL.CreateElement("Thue_XNK");
                ThueXNKAtt.InnerText = BaseClass.Round(hmd.ThueXNK, 9);
                node.AppendChild(ThueXNKAtt);

                XmlNode TS_VSTAtt = docNPL.CreateElement("TS_VAT");
                TS_VSTAtt.InnerText = BaseClass.Round(hmd.ThueSuatGTGT, 9);
                node.AppendChild(TS_VSTAtt);

                XmlNode ThueVATAtt = docNPL.CreateElement("Thue_VAT");
                ThueVATAtt.InnerText = BaseClass.Round(hmd.ThueGTGT, 9);
                node.AppendChild(ThueVATAtt);

                XmlNode TL_PhuThuAtt = docNPL.CreateElement("TyLe_ThuKhac");
                TL_PhuThuAtt.InnerText = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.AppendChild(TL_PhuThuAtt);

                XmlNode TriGiaTK = docNPL.CreateElement("TriGia_ThuKhac");
                TriGiaTK.InnerText = BaseClass.Round(hmd.TriGiaThuKhac, 9);
                node.AppendChild(TriGiaTK);

                XmlNode sttAtt = docNPL.CreateElement("Index");
                sttAtt.InnerText = hmd.SoThuTuHang.ToString();
                node.AppendChild(sttAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        private string ConvertCollectionToXMLNHAPGC()
        {
            //load du lieu
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                HangMauDich hmd = new HangMauDich();
                hmd.TKMD_ID = this.ID;
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }

            //NumberFormatInfo f = new NumberFormatInfo();
            //CultureInfo culture = new CultureInfo("vi-VN");
            //if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            //{
            //    f.NumberDecimalSeparator = ".";
            //    f.NumberGroupSeparator = ",";
            //}
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\khai bao to khai nhap.xml");
            //docNPL.Load(path + "\\B03GiaCong\\KhaiBaoToKhaiNhap.xml");
            //
            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/tokhai/MA_HQ");
            nodeHQNhan.InnerText = this.MaHaiQuan;

            XmlNode nodeSoTK = docNPL.SelectSingleNode("Root/tokhai/SoTK");
            nodeSoTK.InnerText = this.ID.ToString();

            XmlNode nodeMaLH = docNPL.SelectSingleNode("Root/tokhai/Ma_LH");
            nodeMaLH.InnerText = this.MaLoaiHinh;

            XmlNode nodeNamDK = docNPL.SelectSingleNode("Root/tokhai/NamDK");
            nodeNamDK.InnerText = DateTime.Today.Year.ToString();

            XmlNode nodeNgayDK = docNPL.SelectSingleNode("Root/tokhai/Ngay_DK");
            nodeNgayDK.InnerText = "";

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/tokhai/Ma_DV");
            nodeDN.InnerText = this.MaDoanhNghiep;

            XmlNode nodeDVUT = docNPL.SelectSingleNode("Root/tokhai/Ma_DVUT");
            nodeDVUT.InnerText = this.MaDonViUT;

            XmlNode nodeDVDoiTac = docNPL.SelectSingleNode("Root/tokhai/DV_TD");
            if (this.TenDonViDoiTac.Length <= 40)
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            else
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 40));
            XmlNode nodePTVT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTVT");
            nodePTVT.InnerText = this.PTVT_ID;

            XmlNode nodeSoHieuPTVT = docNPL.SelectSingleNode("Root/tokhai/Ten_PTVT");
            if (this.SoHieuPTVT.Length <= 20)
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT);
            else
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT.Substring(0, 20));
            XmlNode nodeNgayDen = docNPL.SelectSingleNode("Root/tokhai/NgayDen");
            if (this.NgayDenPTVT.Year > 1900)
                nodeNgayDen.InnerText = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeNgayDen.InnerText = "";

            XmlNode nodeVanDon = docNPL.SelectSingleNode("Root/tokhai/Van_Don");
            if (this.SoVanDon.Length <= 20)
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));

            XmlNode nodeCuaKhau = docNPL.SelectSingleNode("Root/tokhai/Ma_CK");
            nodeCuaKhau.InnerText = this.CuaKhau_ID;

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/tokhai/CangNN");
            if (this.DiaDiemXepHang.Length <= 40)
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));
            XmlNode nodeSoGiayPhep = docNPL.SelectSingleNode("Root/tokhai/So_GP");
            if (this.SoGiayPhep.Length <= 80)
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep);
            else
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Substring(0, 80));
            XmlNode nodeNgayGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_GP");
            if (NgayGiayPhep.Year > 1900)
                nodeNgayGiayPhep.InnerText = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayGiayPhep.InnerText = "";

            XmlNode nodeNgayHHGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHGP");
            if (NgayHetHanGiayPhep.Year > 1900)
                nodeNgayHHGiayPhep.InnerText = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayHHGiayPhep.InnerText = "";

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/tokhai/So_HD");
            nodeSoHopDong.InnerText = this.SoHopDong;

            XmlNode nodeTyGiaUSD = docNPL.SelectSingleNode("Root/tokhai/TyGia_USD");
            nodeTyGiaUSD.InnerText = BaseClass.Round(this.TyGiaUSD, 9);

            XmlNode nodeNgayHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HD");
            if (NgayHopDong.Year > 1900)
                nodeNgayHopDong.InnerText = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHopDong.InnerText = "";

            XmlNode nodeNgayHHHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHHD");
            if (NgayHetHanHopDong.Year > 1900)
                nodeNgayHHHopDong.InnerText = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHHHopDong.InnerText = "";

            XmlNode nodeNuocXK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_XK");
            nodeNuocXK.InnerText = this.NuocXK_ID;

            XmlNode nodeNuocNK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_NK");
            nodeNuocNK.InnerText = "VN";

            XmlNode nodeDieuKienGiaoHang = docNPL.SelectSingleNode("Root/tokhai/MA_GH");
            nodeDieuKienGiaoHang.InnerText = this.DKGH_ID;

            XmlNode nodeSoHang = docNPL.SelectSingleNode("Root/tokhai/SoHang");
            nodeSoHang.InnerText = this.HMDCollection.Count.ToString();

            XmlNode nodePTTT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTTT");
            nodePTTT.InnerText = this.PTTT_ID;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/tokhai/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;

            //XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/tokhai/TyGia_VND");
            //nodeTyGiaVND.InnerText = this.TyGiaTinhThue, 9);
            string strFront = GetFrontTGVND(this.TyGiaTinhThue.ToString(f));
            string strEnd = GetEndTGVND(this.TyGiaTinhThue.ToString(f));

            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/tokhai/TyGia_VND");
            if (strEnd != "")
                nodeTyGiaVND.InnerText = strFront.Trim() + '.' + strEnd.Trim();
            else
                nodeTyGiaVND.InnerText = strFront.Trim();

            // nodeTyGiaVND.InnerText = this.TyGiaTinhThue, 9);

            XmlNode nodeGiayTo = docNPL.SelectSingleNode("Root/tokhai/GiayTo");
            if (GiayTo.Length > 40)
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo);

            XmlNode nodeSoKien = docNPL.SelectSingleNode("Root/tokhai/So_Kien");
            nodeSoKien.InnerText = Convert.ToUInt32(this.SoKien).ToString();

            XmlNode nodeSoContainer20 = docNPL.SelectSingleNode("Root/tokhai/So_Container");
            nodeSoContainer20.InnerText = Convert.ToUInt64(this.SoContainer20).ToString();

            XmlNode nodeHDTM = docNPL.SelectSingleNode("Root/tokhai/So_HDTM");
            if (SoHoaDonThuongMai.Length <= 30)
                nodeHDTM.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai);
            else
                nodeHDTM.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai.Substring(0, 30));

            XmlNode nodeNgayHDTM = docNPL.SelectSingleNode("Root/tokhai/Ngay_HDTM");
            if (NgayHoaDonThuongMai.Year > 1900)
                nodeNgayHDTM.InnerText = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeNgayHDTM.InnerText = "";

            XmlNode nodeSoPLTK = docNPL.SelectSingleNode("Root/tokhai/So_PLTK");
            nodeSoPLTK.InnerText = this.SoLuongPLTK.ToString();

            XmlNode nodeChuHang = docNPL.SelectSingleNode("Root/tokhai/ChuHang");
            if (TenChuHang.Length > 30)
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang);
            XmlNode nodeNgayVanDon = docNPL.SelectSingleNode("Root/tokhai/Ngay_VanDon");
            if (NgayVanDon.Year > 1900)
                nodeNgayVanDon.InnerText = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeNgayVanDon.InnerText = "";

            XmlNode nodeTrongLuong = docNPL.SelectSingleNode("Root/tokhai/TrangLuong");
            nodeTrongLuong.InnerText = BaseClass.Round(this.TrongLuong, 9);

            XmlNode nodeSoContainer40 = docNPL.SelectSingleNode("Root/tokhai/SoContainer40");
            nodeSoContainer40.InnerText = Convert.ToUInt64(this.SoContainer40).ToString();

            //chung tu dinh kem :
            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/tokhai/CTKT");
            nodeCTKT.InnerText = "";

            XmlNode nodeMaMid = docNPL.SelectSingleNode("Root/tokhai/MA_MID");
            nodeMaMid.InnerText = this.MaMid;

            XmlNode nodeHang = docNPL.SelectSingleNode("Root/hangTKs");
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("hangTK");

                XmlNode HangDetails = docNPL.CreateElement("HangDetails");
                HangDetails.InnerText = "";
                node.AppendChild(HangDetails);

                XmlNode MaHSAtt = docNPL.CreateElement("MaHS");
                MaHSAtt.InnerText = hmd.MaHS;
                node.AppendChild(MaHSAtt);

                XmlNode maAtt = docNPL.CreateElement("Ma_Hang");
                maAtt.InnerText = this.LoaiHangHoa + hmd.MaPhu;
                node.AppendChild(maAtt);

                XmlNode TenAtt = docNPL.CreateElement("Ten_Hang");
                TenAtt.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang);
                node.AppendChild(TenAtt);

                XmlNode NuocAtt = docNPL.CreateElement("Nuoc_XX");
                NuocAtt.InnerText = hmd.NuocXX_ID;
                node.AppendChild(NuocAtt);

                XmlNode Ma_DVTAtt = docNPL.CreateElement("Ma_DVT");
                Ma_DVTAtt.InnerText = hmd.DVT_ID;
                node.AppendChild(Ma_DVTAtt);

                XmlNode SoLuongAtt = docNPL.CreateElement("Luong");
                SoLuongAtt.InnerText = BaseClass.Round(hmd.SoLuong, 9);
                node.AppendChild(SoLuongAtt);

                XmlNode DonGiaAtt = docNPL.CreateElement("DGia_KB");
                DonGiaAtt.InnerText = BaseClass.Round(hmd.DonGiaKB, 9);
                node.AppendChild(DonGiaAtt);

                XmlNode TriGiaAtt = docNPL.CreateElement("TriGia_KB");
                TriGiaAtt.InnerText = BaseClass.Round(hmd.TriGiaKB, 9); ;
                node.AppendChild(TriGiaAtt);

                XmlNode TriGiaVNDAtt = docNPL.CreateElement("TGTTVND");
                TriGiaVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaTT, 9); ;
                node.AppendChild(TriGiaVNDAtt);

                XmlNode TGKBVNDVNDAtt = docNPL.CreateElement("TGKBVND");
                TGKBVNDVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaKB_VND, 9); ;
                node.AppendChild(TGKBVNDVNDAtt);

                XmlNode MienThue = docNPL.CreateElement("MienThue");
                MienThue.InnerText = BaseClass.Round((decimal)hmd.MienThue, 9);
                node.AppendChild(MienThue);

                XmlNode TS_XNKAtt = docNPL.CreateElement("TS_XNK");
                TS_XNKAtt.InnerText = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.AppendChild(TS_XNKAtt);

                XmlNode ThueXNKAtt = docNPL.CreateElement("Thue_XNK");
                ThueXNKAtt.InnerText = BaseClass.Round(hmd.ThueXNK, 9);
                node.AppendChild(ThueXNKAtt);

                XmlNode TS_VSTAtt = docNPL.CreateElement("TS_VAT");
                TS_VSTAtt.InnerText = BaseClass.Round(hmd.ThueSuatGTGT, 9);
                node.AppendChild(TS_VSTAtt);

                XmlNode ThueVATAtt = docNPL.CreateElement("Thue_VAT");
                ThueVATAtt.InnerText = BaseClass.Round(hmd.ThueGTGT, 9);
                node.AppendChild(ThueVATAtt);

                XmlNode TL_PhuThuAtt = docNPL.CreateElement("TyLe_ThuKhac");
                TL_PhuThuAtt.InnerText = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.AppendChild(TL_PhuThuAtt);

                XmlNode TriGiaTK = docNPL.CreateElement("TriGia_ThuKhac");
                TriGiaTK.InnerText = BaseClass.Round(hmd.TriGiaThuKhac, 9);
                node.AppendChild(TriGiaTK);

                XmlNode sttAtt = docNPL.CreateElement("Index");
                sttAtt.InnerText = hmd.SoThuTuHang.ToString();
                node.AppendChild(sttAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        #endregion Khai báo tờ khai nhập

        #region Khai báo tờ khai xuất

        public string WSSendXMLXuat(string pass, string mamid)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiXuat, (int)MessageFunctions.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXMLXUATGCALL(mamid));
            docNPL.InnerXml = docNPL.InnerXml.Replace("dt=\"bin.base64\"", "dt:dt=\"bin.base64\"");
            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            string msgError = string.Empty;
            try
            {
                //Khai báo
                kq = kdt.Send(doc.InnerXml, pass);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoToKhai);
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                        XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                        //nodeRoot.RemoveChild(node);
                        return doc.InnerXml;
                    }
                }
                else
                {
                    msgError = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                    throw ex;
                }
                else
                    if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            return "";
        }

        private string ConvertCollectionToXMLXUATGC()
        {
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                HangMauDich hmd = new HangMauDich();
                hmd.TKMD_ID = this.ID;
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }

            //NumberFormatInfo f = new NumberFormatInfo();
            //CultureInfo culture = new CultureInfo("vi-VN");
            //if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            //{
            //    f.NumberDecimalSeparator = ".";
            //    f.NumberGroupSeparator = ",";
            //}
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\khai bao to khai xuat.xml");

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/tokhai/MA_HQ");
            nodeHQNhan.InnerText = this.MaHaiQuan;

            XmlNode nodeSoTK = docNPL.SelectSingleNode("Root/tokhai/SoTK");
            nodeSoTK.InnerText = this.ID.ToString();

            XmlNode nodeMaLH = docNPL.SelectSingleNode("Root/tokhai/Ma_LH");
            nodeMaLH.InnerText = this.MaLoaiHinh;

            XmlNode nodeNamDK = docNPL.SelectSingleNode("Root/tokhai/NamDK");
            nodeNamDK.InnerText = DateTime.Today.Year.ToString();

            XmlNode nodeNgayDK = docNPL.SelectSingleNode("Root/tokhai/Ngay_DK");
            nodeNgayDK.InnerText = "";

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/tokhai/Ma_DV");
            nodeDN.InnerText = this.MaDoanhNghiep;

            XmlNode nodeDVUT = docNPL.SelectSingleNode("Root/tokhai/Ma_DVUT");
            nodeDVUT.InnerText = this.MaDonViUT;

            XmlNode nodeDVDoiTac = docNPL.SelectSingleNode("Root/tokhai/DV_TD");
            if (this.TenDonViDoiTac.Length <= 40)
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            else
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 40));
            XmlNode nodePTVT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTVT");
            nodePTVT.InnerText = this.PTVT_ID;

            XmlNode nodeSoHieuPTVT = docNPL.SelectSingleNode("Root/tokhai/Ten_PTVT");
            if (this.SoHieuPTVT.Length <= 20)
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT);
            else
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT.Substring(0, 20));

            XmlNode nodeNgayDen = docNPL.SelectSingleNode("Root/tokhai/NgayDen");
            if (this.NgayDenPTVT.Year > 1900)
                nodeNgayDen.InnerText = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeNgayDen.InnerText = "";

            XmlNode nodeVanDon = docNPL.SelectSingleNode("Root/tokhai/Van_Don");
            if (this.SoVanDon.Length <= 20)
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));


            XmlNode nodeCuaKhau = docNPL.SelectSingleNode("Root/tokhai/Ma_CK");
            nodeCuaKhau.InnerText = this.CuaKhau_ID;

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/tokhai/CangNN");
            if (this.DiaDiemXepHang.Length <= 40)
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));

            XmlNode nodeSoGiayPhep = docNPL.SelectSingleNode("Root/tokhai/So_GP");
            if (this.SoGiayPhep.Length <= 80)
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep);
            else
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Substring(0, 80));

            XmlNode nodeNgayGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_GP");
            if (NgayGiayPhep.Year > 1900)
                nodeNgayGiayPhep.InnerText = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayGiayPhep.InnerText = "";

            XmlNode nodeNgayHHGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHGP");
            if (NgayHetHanGiayPhep.Year > 1900)
                nodeNgayHHGiayPhep.InnerText = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayHHGiayPhep.InnerText = "";

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/tokhai/So_HD");
            nodeSoHopDong.InnerText = this.SoHopDong;

            XmlNode nodeTyGiaUSD = docNPL.SelectSingleNode("Root/tokhai/TyGia_USD");
            nodeTyGiaUSD.InnerText = BaseClass.Round(this.TyGiaUSD, 9);

            XmlNode nodeNgayHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HD");
            if (NgayHopDong.Year > 1900)
                nodeNgayHopDong.InnerText = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHopDong.InnerText = "";

            XmlNode nodeNgayHHHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHHD");
            if (NgayHetHanHopDong.Year > 1900)
                nodeNgayHHHopDong.InnerText = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHHHopDong.InnerText = "";

            XmlNode nodeNuocXK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_XK");
            nodeNuocXK.InnerText = this.NuocXK_ID;

            XmlNode nodeNuocNK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_NK");
            nodeNuocNK.InnerText = "VN";

            XmlNode nodeDieuKienGiaoHang = docNPL.SelectSingleNode("Root/tokhai/MA_GH");
            nodeDieuKienGiaoHang.InnerText = this.DKGH_ID;

            XmlNode nodeSoHang = docNPL.SelectSingleNode("Root/tokhai/SoHang");
            nodeSoHang.InnerText = this.HMDCollection.Count.ToString();

            XmlNode nodePTTT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTTT");
            nodePTTT.InnerText = this.PTTT_ID;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/tokhai/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;

            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/tokhai/TyGia_VND");
            nodeTyGiaVND.InnerText = BaseClass.Round(this.TyGiaTinhThue, 9);

            XmlNode nodeGiayTo = docNPL.SelectSingleNode("Root/tokhai/GiayTo");
            if (GiayTo.Length > 40)
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo);
            XmlNode nodeTenChuHang = docNPL.SelectSingleNode("Root/tokhai/TenCH");
            nodeTenChuHang.InnerText = "";
            XmlNode nodeSoKien = docNPL.SelectSingleNode("Root/tokhai/So_Kien");
            nodeSoKien.InnerText = Convert.ToUInt32(this.SoKien).ToString();

            XmlNode nodeSoContainer20 = docNPL.SelectSingleNode("Root/tokhai/So_Container");
            nodeSoContainer20.InnerText = Convert.ToUInt64(this.SoContainer20).ToString();

            XmlNode nodeHDTM = docNPL.SelectSingleNode("Root/tokhai/So_HDTM");
            if (SoHoaDonThuongMai.Length <= 30)
                nodeHDTM.InnerText = this.SoHoaDonThuongMai;
            else
                nodeHDTM.InnerText = this.SoHoaDonThuongMai.Substring(0, 30);


            XmlNode nodeNgayHDTM = docNPL.SelectSingleNode("Root/tokhai/Ngay_HDTM");
            if (NgayHoaDonThuongMai.Year > 1900)
                nodeNgayHDTM.InnerText = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeNgayHDTM.InnerText = "";

            XmlNode nodeSoPLTK = docNPL.SelectSingleNode("Root/tokhai/So_PLTK");
            nodeSoPLTK.InnerText = this.SoLuongPLTK.ToString();

            XmlNode nodeChuHang = docNPL.SelectSingleNode("Root/tokhai/ChuHang");
            if (TenChuHang.Length > 30)
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang);
            XmlNode nodeNgayVanDon = docNPL.SelectSingleNode("Root/tokhai/Ngay_VanDon");
            if (NgayVanDon.Year > 1900)
                nodeNgayVanDon.InnerText = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeNgayVanDon.InnerText = "";

            XmlNode nodeTrongLuong = docNPL.SelectSingleNode("Root/tokhai/TrangLuong");
            nodeTrongLuong.InnerText = BaseClass.Round(this.TrongLuong, 9);

            XmlNode nodeSoContainer40 = docNPL.SelectSingleNode("Root/tokhai/SoContainer40");
            nodeSoContainer40.InnerText = Convert.ToUInt64(this.SoContainer40).ToString();

            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/tokhai/CTKT");
            nodeCTKT.InnerText = "";

            //Chung Tu dinh kem :

            //

            XmlNode nodeHang = docNPL.SelectSingleNode("Root/hangTKs");
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("hangTK");

                XmlNode HangDetails = docNPL.CreateElement("HangDetails");
                HangDetails.InnerText = "";
                node.AppendChild(HangDetails);

                XmlNode MaHSAtt = docNPL.CreateElement("MaHS");
                MaHSAtt.InnerText = hmd.MaHS;
                node.AppendChild(MaHSAtt);

                XmlNode maAtt = docNPL.CreateElement("Ma_Hang");
                maAtt.InnerText = this.LoaiHangHoa + hmd.MaPhu;
                node.AppendChild(maAtt);

                XmlNode TenAtt = docNPL.CreateElement("Ten_Hang");
                TenAtt.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang);
                node.AppendChild(TenAtt);

                XmlNode NuocAtt = docNPL.CreateElement("Nuoc_XX");
                NuocAtt.InnerText = hmd.NuocXX_ID;
                node.AppendChild(NuocAtt);

                XmlNode Ma_DVTAtt = docNPL.CreateElement("Ma_DVT");
                Ma_DVTAtt.InnerText = hmd.DVT_ID;
                node.AppendChild(Ma_DVTAtt);

                XmlNode SoLuongAtt = docNPL.CreateElement("Luong");
                SoLuongAtt.InnerText = BaseClass.Round(hmd.SoLuong, 9);
                node.AppendChild(SoLuongAtt);

                XmlNode DonGiaAtt = docNPL.CreateElement("DGia_KB");
                DonGiaAtt.InnerText = BaseClass.Round(hmd.DonGiaKB, 9);
                node.AppendChild(DonGiaAtt);

                XmlNode TriGiaAtt = docNPL.CreateElement("TriGia_KB");
                TriGiaAtt.InnerText = BaseClass.Round(hmd.TriGiaKB, 9); ;
                node.AppendChild(TriGiaAtt);

                XmlNode TriGiaVNDAtt = docNPL.CreateElement("TGTTVND");
                TriGiaVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaTT, 9); ;
                node.AppendChild(TriGiaVNDAtt);

                XmlNode DonGiaVNDAtt = docNPL.CreateElement("TGKBVND");
                DonGiaVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaKB_VND, 9); ;
                node.AppendChild(DonGiaVNDAtt);

                XmlNode MienThue = docNPL.CreateElement("MienThue");
                MienThue.InnerText = BaseClass.Round((decimal)hmd.MienThue, 9);
                node.AppendChild(MienThue);

                XmlNode TS_XNKAtt = docNPL.CreateElement("TS_XNK");
                TS_XNKAtt.InnerText = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.AppendChild(TS_XNKAtt);

                XmlNode ThueXNKAtt = docNPL.CreateElement("Thue_XNK");
                ThueXNKAtt.InnerText = BaseClass.Round(hmd.ThueXNK, 9);
                node.AppendChild(ThueXNKAtt);

                XmlNode TS_VSTAtt = docNPL.CreateElement("TS_VAT");
                TS_VSTAtt.InnerText = BaseClass.Round(hmd.ThueGTGT, 9);
                node.AppendChild(TS_VSTAtt);

                XmlNode ThueVATAtt = docNPL.CreateElement("Thue_VAT");
                ThueVATAtt.InnerText = "0";// hmd.ThueGTGT, 9);
                node.AppendChild(ThueVATAtt);

                XmlNode TL_PhuThuAtt = docNPL.CreateElement("TyLe_ThuKhac");
                TL_PhuThuAtt.InnerText = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.AppendChild(TL_PhuThuAtt);

                XmlNode TriGiaTK = docNPL.CreateElement("TriGia_ThuKhac");
                TriGiaTK.InnerText = BaseClass.Round(hmd.TriGiaThuKhac, 9);
                node.AppendChild(TriGiaTK);

                XmlNode sttAtt = docNPL.CreateElement("Index");
                sttAtt.InnerText = hmd.SoThuTuHang.ToString();
                node.AppendChild(sttAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        //
        private string ConvertCollectionToXMLXUATGCALL(string maMid)
        {
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                HangMauDich hmd = new HangMauDich();
                hmd.TKMD_ID = this.ID;
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }

            //NumberFormatInfo f = new NumberFormatInfo();
            //CultureInfo culture = new CultureInfo("vi-VN");
            //if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            //{
            //    f.NumberDecimalSeparator = ".";
            //    f.NumberGroupSeparator = ",";
            //}
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\khai bao to khai xuat.xml");

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/tokhai/MA_HQ");
            nodeHQNhan.InnerText = this.MaHaiQuan;

            XmlNode nodeSoTK = docNPL.SelectSingleNode("Root/tokhai/SoTK");
            nodeSoTK.InnerText = this.ID.ToString();

            XmlNode nodeMaLH = docNPL.SelectSingleNode("Root/tokhai/Ma_LH");
            nodeMaLH.InnerText = this.MaLoaiHinh;

            XmlNode nodeNamDK = docNPL.SelectSingleNode("Root/tokhai/NamDK");
            nodeNamDK.InnerText = DateTime.Today.Year.ToString();

            XmlNode nodeNgayDK = docNPL.SelectSingleNode("Root/tokhai/Ngay_DK");
            nodeNgayDK.InnerText = "";

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/tokhai/Ma_DV");
            nodeDN.InnerText = this.MaDoanhNghiep;

            XmlNode nodeDVUT = docNPL.SelectSingleNode("Root/tokhai/Ma_DVUT");
            nodeDVUT.InnerText = this.MaDonViUT;

            XmlNode nodeDVDoiTac = docNPL.SelectSingleNode("Root/tokhai/DV_TD");
            if (this.TenDonViDoiTac.Length <= 40)
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            else
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 40));
            XmlNode nodePTVT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTVT");
            nodePTVT.InnerText = this.PTVT_ID;

            XmlNode nodeSoHieuPTVT = docNPL.SelectSingleNode("Root/tokhai/Ten_PTVT");
            if (this.SoHieuPTVT.Length <= 20)
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT);
            else
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT.Substring(0, 20));

            if (this.VanTaiDon != null)
            {
                XmlNode nodeNgayKhoiHanh = docNPL.SelectSingleNode("Root/tokhai/NgayKH");
                if (this.VanTaiDon.NgayKhoiHanh.Year > 1900)
                    nodeNgayKhoiHanh.InnerText = this.VanTaiDon.NgayKhoiHanh.ToString("MM/dd/yyyy");
                else
                    nodeNgayKhoiHanh.InnerText = "";
            }

            XmlNode nodeNgayDen = docNPL.SelectSingleNode("Root/tokhai/NgayDen");
            if (this.NgayDenPTVT.Year > 1900)
                nodeNgayDen.InnerText = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeNgayDen.InnerText = "";

            XmlNode nodeVanDon = docNPL.SelectSingleNode("Root/tokhai/Van_Don");
            if (this.SoVanDon.Length <= 20)
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));


            XmlNode nodeCuaKhau = docNPL.SelectSingleNode("Root/tokhai/Ma_CK");
            nodeCuaKhau.InnerText = this.CuaKhau_ID;

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/tokhai/CangNN");
            if (this.DiaDiemXepHang.Length <= 40)
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));

            XmlNode nodeSoGiayPhep = docNPL.SelectSingleNode("Root/tokhai/So_GP");
            if (this.SoGiayPhep.Length <= 80)
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep);
            else
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Substring(0, 80));

            XmlNode nodeNgayGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_GP");
            if (NgayGiayPhep.Year > 1900)
                nodeNgayGiayPhep.InnerText = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayGiayPhep.InnerText = "";

            XmlNode nodeNgayHHGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHGP");
            if (NgayHetHanGiayPhep.Year > 1900)
                nodeNgayHHGiayPhep.InnerText = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayHHGiayPhep.InnerText = "";

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/tokhai/So_HD");
            nodeSoHopDong.InnerText = this.SoHopDong;

            XmlNode nodeTyGiaUSD = docNPL.SelectSingleNode("Root/tokhai/TyGia_USD");
            nodeTyGiaUSD.InnerText = BaseClass.Round(this.TyGiaUSD, 9);

            XmlNode nodeNgayHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HD");
            if (NgayHopDong.Year > 1900)
                nodeNgayHopDong.InnerText = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHopDong.InnerText = "";

            XmlNode nodeNgayHHHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHHD");
            if (NgayHetHanHopDong.Year > 1900)
                nodeNgayHHHopDong.InnerText = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHHHopDong.InnerText = "";

            XmlNode nodeNuocXK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_XK");
            nodeNuocXK.InnerText = this.NuocXK_ID;

            XmlNode nodeNuocNK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_NK");
            nodeNuocNK.InnerText = this.NuocNK_ID;//"VN";

            XmlNode nodeDieuKienGiaoHang = docNPL.SelectSingleNode("Root/tokhai/MA_GH");
            nodeDieuKienGiaoHang.InnerText = this.DKGH_ID;

            XmlNode nodeSoHang = docNPL.SelectSingleNode("Root/tokhai/SoHang");
            nodeSoHang.InnerText = this.HMDCollection.Count.ToString();

            XmlNode nodePTTT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTTT");
            nodePTTT.InnerText = this.PTTT_ID;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/tokhai/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;


            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/tokhai/TyGia_VND");

            nodeTyGiaVND.InnerText = BaseClass.Round(this.TyGiaTinhThue, 9);// this.TyGiaTinhThue.ToString("0");

            XmlNode nodeGiayTo = docNPL.SelectSingleNode("Root/tokhai/GiayTo");
            if (GiayTo.Length > 40)
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo);
            XmlNode nodeTenChuHang = docNPL.SelectSingleNode("Root/tokhai/TenCH");
            nodeTenChuHang.InnerText = "";
            XmlNode nodeSoKien = docNPL.SelectSingleNode("Root/tokhai/So_Kien");
            nodeSoKien.InnerText = Convert.ToUInt32(this.SoKien).ToString();

            XmlNode nodeSoContainer20 = docNPL.SelectSingleNode("Root/tokhai/So_Container");
            nodeSoContainer20.InnerText = Convert.ToUInt64(this.SoContainer20).ToString();

            XmlNode nodeHDTM = docNPL.SelectSingleNode("Root/tokhai/So_HDTM");
            if (SoHoaDonThuongMai.Length <= 30)
                nodeHDTM.InnerText = this.SoHoaDonThuongMai;
            else
                nodeHDTM.InnerText = this.SoHoaDonThuongMai.Substring(0, 30);


            XmlNode nodeNgayHDTM = docNPL.SelectSingleNode("Root/tokhai/Ngay_HDTM");
            if (NgayHoaDonThuongMai.Year > 1900)
                nodeNgayHDTM.InnerText = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeNgayHDTM.InnerText = "";

            XmlNode nodeSoPLTK = docNPL.SelectSingleNode("Root/tokhai/So_PLTK");
            nodeSoPLTK.InnerText = this.SoLuongPLTK.ToString();

            XmlNode nodeChuHang = docNPL.SelectSingleNode("Root/tokhai/ChuHang");
            if (TenChuHang.Length > 30)
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang);
            XmlNode nodeNgayVanDon = docNPL.SelectSingleNode("Root/tokhai/Ngay_VanDon");
            if (NgayVanDon.Year > 1900)
                nodeNgayVanDon.InnerText = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeNgayVanDon.InnerText = "";

            XmlNode nodeTrongLuong = docNPL.SelectSingleNode("Root/tokhai/TrangLuong");
            nodeTrongLuong.InnerText = BaseClass.Round(this.TrongLuong, 9);
            XmlNode nodeDeXuatKhac = docNPL.SelectSingleNode("Root/tokhai/DE_XUAT_KHAC");
            if (nodeDeXuatKhac != null)
                nodeDeXuatKhac.InnerText = this.DeXuatKhac;

            XmlNode nodeSoContainer40 = docNPL.SelectSingleNode("Root/tokhai/SoContainer40");
            nodeSoContainer40.InnerText = Convert.ToUInt64(this.SoContainer40).ToString();

            //LINHHTN
            XmlNode nodeMaMID = docNPL.SelectSingleNode("Root/tokhai/MA_MID");
            nodeMaMID.InnerText = maMid;

            //XmlNode nodePHIBH = docNPL.SelectSingleNode("Root/tokhai/PHI_BH");
            //nodePHIBH.InnerText = BaseClass.Round(this.PhiBaoHiem, 9);

            //XmlNode nodePHIVC = docNPL.SelectSingleNode("Root/tokhai/PHI_VC");
            //nodePHIVC.InnerText = BaseClass.Round(this.PhiVanChuyen, 9);

            //XmlNode nodeLEPHIHQ = docNPL.SelectSingleNode("Root/tokhai/LE_PHI_HQ");
            //nodeLEPHIHQ.InnerText = BaseClass.Round(this.LePhiHaiQuan, 9);

            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/tokhai/CTKT");
            nodeCTKT.InnerText = "";

            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            listHang = ConvertHMDKDToHangMauDich();
            #region Chứng từ đính kèm

            #region Vận tải đơn
            // VAN TAI DON :
            List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
            if (VanTaiDon == null)
            {

                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }
            }
            XmlElement ChungTuVanDon = docNPL.CreateElement("CHUNG_TU_VAN_DON");
            if (VanTaiDon != null)
            {

                foreach (VanDon vd in VanDonCollection)
                {

                    XmlAttribute SO_CHUNG_TU = docNPL.CreateAttribute("SO_CHUNG_TU");
                    SO_CHUNG_TU.Value = SoVanDon.Trim();
                    ChungTuVanDon.Attributes.Append(SO_CHUNG_TU);

                    XmlAttribute NGAY_CHUNG_TU = docNPL.CreateAttribute("NGAY_CHUNG_TU");
                    //NGAY_CHUNG_TU.Value = NgayVanDon.ToString("MM/dd/yyyy");
                    NGAY_CHUNG_TU.Value = NgayVanDon.ToString("yyyy-MM-dd");
                    ChungTuVanDon.Attributes.Append(NGAY_CHUNG_TU);

                    XmlAttribute NGAY_HH = docNPL.CreateAttribute("NGAY_HH");
                    NGAY_HH.Value = "";
                    ChungTuVanDon.Attributes.Append(NGAY_HH);

                    XmlAttribute TEN_NGUOI_NHAN_HANG = docNPL.CreateAttribute("TEN_NGUOI_NHAN_HANG");
                    TEN_NGUOI_NHAN_HANG.Value = vd.TenNguoiNhanHang.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_NGUOI_NHAN_HANG);

                    XmlAttribute MA_NGUOI_NHAN_HANG = docNPL.CreateAttribute("MA_NGUOI_NHAN_HANG");
                    MA_NGUOI_NHAN_HANG.Value = vd.MaNguoiNhanHang.Trim();
                    ChungTuVanDon.Attributes.Append(MA_NGUOI_NHAN_HANG);

                    XmlAttribute TEN_NGUOI_GIAO_HANG = docNPL.CreateAttribute("TEN_NGUOI_GIAO_HANG");
                    TEN_NGUOI_GIAO_HANG.Value = vd.TenNguoiGiaoHang.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_NGUOI_GIAO_HANG);

                    XmlAttribute MA_NGUOI_GIAO_HANG = docNPL.CreateAttribute("MA_NGUOI_GIAO_HANG");
                    MA_NGUOI_GIAO_HANG.Value = vd.MaNguoiGiaoHang.Trim();
                    ChungTuVanDon.Attributes.Append(MA_NGUOI_GIAO_HANG);

                    XmlAttribute CUA_KHAU_NHAP = docNPL.CreateAttribute("CUA_KHAU_NHAP");
                    CUA_KHAU_NHAP.Value = vd.CuaKhauNhap_ID.Trim();
                    ChungTuVanDon.Attributes.Append(CUA_KHAU_NHAP);

                    XmlAttribute CUA_KHAU_XUAT = docNPL.CreateAttribute("CUA_KHAU_XUAT");
                    CUA_KHAU_XUAT.Value = vd.CuaKhauXuat.Trim();
                    ChungTuVanDon.Attributes.Append(CUA_KHAU_XUAT);

                    XmlAttribute MA_DKGH = docNPL.CreateAttribute("MA_DKGH");
                    MA_DKGH.Value = vd.DKGH_ID.Trim();
                    ChungTuVanDon.Attributes.Append(MA_DKGH);

                    XmlAttribute TEN_NGUOI_NHAN_HANG_TG = docNPL.CreateAttribute("TEN_NGUOI_NHAN_HANG_TG");
                    TEN_NGUOI_NHAN_HANG_TG.Value = vd.DKGH_ID.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_NGUOI_NHAN_HANG_TG);

                    XmlAttribute MA_NGUOI_NHAN_HANG_TG = docNPL.CreateAttribute("MA_NGUOI_NHAN_HANG_TG");
                    MA_NGUOI_NHAN_HANG_TG.Value = vd.MaNguoiNhanHangTrungGian.Trim();
                    ChungTuVanDon.Attributes.Append(MA_NGUOI_NHAN_HANG_TG);

                    XmlAttribute DIA_DIEM_GIAO_HANG = docNPL.CreateAttribute("DIA_DIEM_GIAO_HANG");
                    DIA_DIEM_GIAO_HANG.Value = vd.DiaDiemGiaoHang.Trim();
                    ChungTuVanDon.Attributes.Append(DIA_DIEM_GIAO_HANG);

                    XmlAttribute CONTAINER = docNPL.CreateAttribute("CONTAINER");
                    CONTAINER.Value = "0";//kiem tra lai  TS DE =0
                    ChungTuVanDon.Attributes.Append(CONTAINER);

                    XmlAttribute SO_HIEU_PTVT = docNPL.CreateAttribute("SO_HIEU_PTVT");
                    SO_HIEU_PTVT.Value = vd.SoHieuPTVT.Trim();
                    ChungTuVanDon.Attributes.Append(SO_HIEU_PTVT);

                    XmlAttribute TEN_PTVT = docNPL.CreateAttribute("TEN_PTVT");
                    TEN_PTVT.Value = vd.TenPTVT.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_PTVT);

                    XmlAttribute SO_HIEU_CHUYEN_DI = docNPL.CreateAttribute("SO_HIEU_CHUYEN_DI");
                    SO_HIEU_CHUYEN_DI.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(vd.SoHieuChuyenDi);
                    ChungTuVanDon.Attributes.Append(SO_HIEU_CHUYEN_DI);

                    XmlAttribute QUOC_TICH_PTVT = docNPL.CreateAttribute("QUOC_TICH_PTVT");
                    QUOC_TICH_PTVT.Value = vd.QuocTichPTVT.Trim();
                    ChungTuVanDon.Attributes.Append(QUOC_TICH_PTVT);

                    XmlAttribute TEN_CANG_DO_HANG = docNPL.CreateAttribute("TEN_CANG_DO_HANG");
                    TEN_CANG_DO_HANG.Value = vd.TenCangDoHang.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_CANG_DO_HANG);

                    XmlAttribute MA_CANG_DO_HANG = docNPL.CreateAttribute("MA_CANG_DO_HANG");
                    MA_CANG_DO_HANG.Value = vd.MaCangDoHang.Trim();
                    ChungTuVanDon.Attributes.Append(MA_CANG_DO_HANG);

                    XmlAttribute NGAY_DEN_PTVT = docNPL.CreateAttribute("NGAY_DEN_PTVT");
                    //NGAY_DEN_PTVT.Value = vd.NgayDenPTVT.ToString("MM/dd/yyyy");
                    NGAY_DEN_PTVT.Value = vd.NgayDenPTVT.ToString("yyyy-MM-dd");
                    ChungTuVanDon.Attributes.Append(NGAY_DEN_PTVT);

                    XmlAttribute TEN_CANG_XEP_HANG = docNPL.CreateAttribute("TEN_CANG_XEP_HANG");
                    TEN_CANG_XEP_HANG.Value = vd.TenCangXepHang.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_CANG_XEP_HANG);

                    XmlAttribute MA_CANG_XEP_HANG = docNPL.CreateAttribute("MA_CANG_XEP_HANG");
                    MA_CANG_XEP_HANG.Value = vd.MaCangXepHang.Trim();
                    ChungTuVanDon.Attributes.Append(MA_CANG_XEP_HANG);

                    XmlAttribute TEN_HANG_VAN_TAI = docNPL.CreateAttribute("TEN_HANG_VAN_TAI");
                    TEN_HANG_VAN_TAI.Value = vd.TenHangVT.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_HANG_VAN_TAI);

                    XmlAttribute MA_HANG_VAN_TAI = docNPL.CreateAttribute("MA_HANG_VAN_TAI");
                    MA_HANG_VAN_TAI.Value = vd.MaHangVT.Trim();
                    ChungTuVanDon.Attributes.Append(MA_HANG_VAN_TAI);

                    XmlAttribute NUOC_XN = docNPL.CreateAttribute("NUOC_XN");
                    NUOC_XN.Value = vd.NuocXuat_ID.Trim();
                    ChungTuVanDon.Attributes.Append(NUOC_XN);

                    XmlAttribute NOI_PHAT_HANH = docNPL.CreateAttribute("NOI_PHAT_HANH");
                    NOI_PHAT_HANH.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(vd.NoiDi);
                    ChungTuVanDon.Attributes.Append(NOI_PHAT_HANH);

                    XmlAttribute NGAY_KHOI_HANH = docNPL.CreateAttribute("NGAY_KHOI_HANH");
                    //NGAY_KHOI_HANH.Value = vd.NgayKhoiHanh.ToString("MM/dd/yyyy");
                    NGAY_KHOI_HANH.Value = vd.NgayKhoiHanh.ToString("yyyy-MM-dd");
                    ChungTuVanDon.Attributes.Append(NGAY_KHOI_HANH);

                    XmlAttribute TONG_SO_KIEN = docNPL.CreateAttribute("TONG_SO_KIEN");
                    TONG_SO_KIEN.Value = "";
                    ChungTuVanDon.Attributes.Append(TONG_SO_KIEN);

                    XmlAttribute KIEU_DONG_GOI = docNPL.CreateAttribute("KIEU_DONG_GOI");
                    KIEU_DONG_GOI.Value = "";
                    ChungTuVanDon.Attributes.Append(KIEU_DONG_GOI);

                    XmlAttribute MA_PTVT = docNPL.CreateAttribute("MA_PTVT");
                    MA_PTVT.Value = "";
                    ChungTuVanDon.Attributes.Append(MA_PTVT);

                    List<Container> ContainerCollection = new List<Container>();
                    if (ContainerCollection.Count == null || ContainerCollection.Count == 0)
                        ContainerCollection = (List<Container>)Container.SelectCollectionBy_VanDon_ID(vd.ID);
                    foreach (Container c in ContainerCollection)
                    {
                        XmlElement CONT = docNPL.CreateElement("CHUNG_TU_VAN_DON.CONT");
                        ChungTuVanDon.AppendChild(CONT);

                        XmlAttribute CONTAINER_NO = docNPL.CreateAttribute("CONTAINER_NO");
                        CONTAINER_NO.Value = c.SoHieu;
                        CONT.Attributes.Append(CONTAINER_NO);

                        XmlAttribute CONTAINER_TYPE = docNPL.CreateAttribute("CONTAINER_TYPE");
                        CONTAINER_TYPE.Value = c.LoaiContainer;
                        CONT.Attributes.Append(CONTAINER_TYPE);

                        XmlAttribute SEAL_NO = docNPL.CreateAttribute("SEAL_NO");
                        SEAL_NO.Value = c.Seal_No;
                        CONT.Attributes.Append(SEAL_NO);

                        XmlAttribute TRANG_THAI = docNPL.CreateAttribute("TRANGTHAI");
                        TRANG_THAI.Value = c.Trang_thai.ToString();
                        CONT.Attributes.Append(TRANG_THAI);
                    }
                }
            }
            #endregion

            #region CO

            if (COCollection == null || COCollection.Count == 0)
            {
                COCollection = (List<CO>)CO.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlElement CHUNG_TU_CO = docNPL.CreateElement("CHUNG_TU_CO");
            foreach (CO co in COCollection)
            {
                XmlElement CoItem = docNPL.CreateElement("CHUNG_TU_CO.ITEM");
                CHUNG_TU_CO.AppendChild(CoItem);

                XmlAttribute SO_CO = docNPL.CreateAttribute("SO_CO");
                SO_CO.Value = co.SoCO;
                CoItem.Attributes.Append(SO_CO);

                XmlAttribute NUOC_CO = docNPL.CreateAttribute("NUOC_CO");
                NUOC_CO.Value = co.NuocCapCO;
                CoItem.Attributes.Append(NUOC_CO);

                XmlAttribute NGAY_CAP_CO = docNPL.CreateAttribute("NGAY_CAP_CO");
                NGAY_CAP_CO.Value = co.NgayCO.ToString("MM/dd/yyyy");
                CoItem.Attributes.Append(NGAY_CAP_CO);

                //CHUA CO PHAI BO SUNG VA KO DC TRONG
                XmlAttribute NGUOI_KY = docNPL.CreateAttribute("NGUOI_KY");
                NGUOI_KY.Value = co.NguoiKy;
                CoItem.Attributes.Append(NGUOI_KY);

                //NOI PHAT HANH LA TO CHUC CAP
                XmlAttribute NOI_PHAT_HANH = docNPL.CreateAttribute("NOI_PHAT_HANH");
                NOI_PHAT_HANH.Value = co.ToChucCap;
                CoItem.Attributes.Append(NOI_PHAT_HANH);

                XmlAttribute MA_LOAI_CO = docNPL.CreateAttribute("MA_LOAI_CO");
                MA_LOAI_CO.Value = co.LoaiCO;
                CoItem.Attributes.Append(MA_LOAI_CO);

                XmlAttribute NGUOI_XUAT = docNPL.CreateAttribute("NGUOI_XUAT");
                NGUOI_XUAT.Value = co.TenDiaChiNguoiXK.Trim(); ;
                CoItem.Attributes.Append(NGUOI_XUAT);

                XmlAttribute MA_NUOC_XUAT = docNPL.CreateAttribute("MA_NUOC_XUAT");
                MA_NUOC_XUAT.Value = co.MaNuocXKTrenCO.Trim(); ;
                CoItem.Attributes.Append(MA_NUOC_XUAT);

                XmlAttribute NGUOI_NHAP = docNPL.CreateAttribute("NGUOI_NHAP");
                NGUOI_NHAP.Value = co.TenDiaChiNguoiNK.Trim(); ;
                CoItem.Attributes.Append(NGUOI_NHAP);

                XmlAttribute MA_NUOC_NHAP = docNPL.CreateAttribute("MA_NUOC_NHAP");
                MA_NUOC_NHAP.Value = co.MaNuocNKTrenCO.Trim(); ;
                CoItem.Attributes.Append(MA_NUOC_NHAP);

                XmlAttribute NOI_DUNG = docNPL.CreateAttribute("NOI_DUNG");
                NOI_DUNG.Value = co.ThongTinMoTaChiTiet.Trim(); ;
                CoItem.Attributes.Append(NOI_DUNG);

                XmlAttribute NGAY_KHOI_HANH = docNPL.CreateAttribute("NGAY_KHOI_HANH");
                NGAY_KHOI_HANH.Value = "";
                CoItem.Attributes.Append(NGAY_KHOI_HANH);

                XmlAttribute GHI_CHU = docNPL.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = "";
                CoItem.Attributes.Append(GHI_CHU);

                // CHUA CO PHAI BO SUNG
                XmlAttribute NOP_SAU = docNPL.CreateAttribute("NOP_SAU");
                NOP_SAU.Value = co.NoCo.ToString();
                CoItem.Attributes.Append(NOP_SAU);

                // CHUA CO PHAI BO SUNG
                //XmlAttribute THOI_HAN_NOP = doc.CreateAttribute("THOI_HAN_NOP");
                //if (co.NoCo == 1)
                //    NOP_SAU.Value = co.ThoiHanNop.ToString("MM/dd/yyyy");
                //else
                //    NOP_SAU.Value = "";
                //CoItem.Attributes.Append(THOI_HAN_NOP);
                //thoilv edit :
                XmlAttribute THOI_HAN_NOP = docNPL.CreateAttribute("THOI_HAN_NOP");
                if (co.NoCo == 1)
                    THOI_HAN_NOP.Value = co.ThoiHanNop.ToString("MM/dd/yyyy");
                else
                    THOI_HAN_NOP.Value = "";
                CoItem.Attributes.Append(THOI_HAN_NOP);
            }
            #endregion

            #region Hợp đồng thương mại

            if (HopDongThuongMaiCollection == null || HopDongThuongMaiCollection.Count == 0)
            {
                HopDongThuongMaiCollection = (List<HopDongThuongMai>)HopDongThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlElement CHUNG_TU_HOPDONG = docNPL.CreateElement("CHUNG_TU_HOPDONG");
            foreach (HopDongThuongMai hopdongTM in HopDongThuongMaiCollection)
            {
                XmlElement hopdongTMItem = docNPL.CreateElement("CHUNG_TU_HOPDONG.ITEM");
                CHUNG_TU_HOPDONG.AppendChild(hopdongTMItem);


                XmlAttribute SO_CT = docNPL.CreateAttribute("SO_CT");
                SO_CT.Value = hopdongTM.SoHopDongTM.Trim();
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = docNPL.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = hopdongTM.NgayHopDongTM.ToString("MM/dd/yyyy");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute THOI_HAN_TT = docNPL.CreateAttribute("THOI_HAN_TT");
                THOI_HAN_TT.Value = hopdongTM.ThoiHanThanhToan.ToString("MM/dd/yyyy");
                hopdongTMItem.Attributes.Append(THOI_HAN_TT);

                XmlAttribute MA_GH = docNPL.CreateAttribute("MA_GH");
                MA_GH.Value = hopdongTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute DIADIEM_GH = docNPL.CreateAttribute("DIADIEM_GH");
                DIADIEM_GH.Value = hopdongTM.DiaDiemGiaoHang;
                hopdongTMItem.Attributes.Append(DIADIEM_GH);

                XmlAttribute MA_PTTT = docNPL.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = hopdongTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = docNPL.CreateAttribute("MA_NT");
                MA_NT.Value = hopdongTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute TONGTRIGIA = docNPL.CreateAttribute("TONGTRIGIA");
                TONGTRIGIA.Value = BaseClass.Round(hopdongTM.TongTriGia, 9);
                hopdongTMItem.Attributes.Append(TONGTRIGIA);

                XmlAttribute MA_DV = docNPL.CreateAttribute("MA_DV");
                MA_DV.Value = hopdongTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = docNPL.CreateAttribute("TEN_DV");
                TEN_DV.Value = hopdongTM.TenDonViMua.Trim();
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute MA_DV_DT = docNPL.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = hopdongTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = docNPL.CreateAttribute("TEN_DV_DT");
                //TEN_DV_DT.Value = hopdongTM.TenDonViMua.Trim();                
                TEN_DV_DT.Value = hopdongTM.TenDonViBan.Trim();

                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute GHI_CHU = docNPL.CreateAttribute("GHI_CHU");
                if (hopdongTM.ThongTinKhac == null)
                    hopdongTM.ThongTinKhac = "";
                GHI_CHU.Value = hopdongTM.ThongTinKhac.Trim();
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (hopdongTM.ListHangMDOfHopDong == null || hopdongTM.ListHangMDOfHopDong.Count == 0)
                {
                    hopdongTM.LoadListHangDMOfHopDong();
                }

                foreach (HopDongThuongMaiDetail hangHopDong in hopdongTM.ListHangMDOfHopDong)
                {

                    Company.KDT.SHARE.QuanLyChungTu.HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hangHopDong.HMD_ID, listHang);

                    XmlElement hangItem = docNPL.CreateElement("CHUNG_TU_HOPDONG.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = docNPL.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = docNPL.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = docNPL.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute MA_DVT = docNPL.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = docNPL.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = docNPL.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = docNPL.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GHI_CHUhang = docNPL.CreateAttribute("GHI_CHU");
                    if (hangHopDong.GhiChu == null)
                        hangHopDong.GhiChu = "";
                    GHI_CHUhang.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hangHopDong.GhiChu.Trim());
                    hangItem.Attributes.Append(GHI_CHUhang);

                    XmlAttribute NUOC_XXHang = docNPL.CreateAttribute("NUOC_XX");
                    NUOC_XXHang.Value = hmd.NuocXX_ID;
                    hangItem.Attributes.Append(NUOC_XXHang);
                }
            }
            #endregion

            #region Hóa đơn thương mại

            if (HoaDonThuongMaiCollection == null || HoaDonThuongMaiCollection.Count == 0)
            {
                HoaDonThuongMaiCollection = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlElement CHUNG_TU_HOADON = docNPL.CreateElement("CHUNG_TU_HOADON");
            foreach (HoaDonThuongMai HoaDonTM in HoaDonThuongMaiCollection)
            {
                XmlElement hopdongTMItem = docNPL.CreateElement("CHUNG_TU_HOADON.ITEM");
                CHUNG_TU_HOADON.AppendChild(hopdongTMItem);


                XmlAttribute SO_CT = docNPL.CreateAttribute("SO_CT");
                SO_CT.Value = HoaDonTM.SoHoaDon.Trim();
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = docNPL.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = HoaDonTM.NgayHoaDon.ToString("MM/dd/yyyy");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute MA_GH = docNPL.CreateAttribute("MA_GH");
                MA_GH.Value = HoaDonTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute MA_PTTT = docNPL.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = HoaDonTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = docNPL.CreateAttribute("MA_NT");
                MA_NT.Value = HoaDonTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);


                XmlAttribute MA_DV = docNPL.CreateAttribute("MA_DV");
                MA_DV.Value = HoaDonTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = docNPL.CreateAttribute("TEN_DV");
                TEN_DV.Value = HoaDonTM.TenDonViMua.Trim();
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute MA_DV_DT = docNPL.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = HoaDonTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = docNPL.CreateAttribute("TEN_DV_DT");
                //TEN_DV_DT.Value = HoaDonTM.TenDonViMua.Trim();
                TEN_DV_DT.Value = HoaDonTM.TenDonViBan.Trim();
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute GHI_CHU = docNPL.CreateAttribute("GHI_CHU");
                if (HoaDonTM.ThongTinKhac == null)
                    HoaDonTM.ThongTinKhac = "";
                GHI_CHU.Value = HoaDonTM.ThongTinKhac.Trim();
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (HoaDonTM.ListHangMDOfHoaDon == null || HoaDonTM.ListHangMDOfHoaDon.Count == 0)
                {
                    HoaDonTM.LoadListHangDMOfHoaDon();
                }

                foreach (HoaDonThuongMaiDetail hangHoaDon in HoaDonTM.ListHangMDOfHoaDon)
                {

                    Company.KDT.SHARE.QuanLyChungTu.HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hangHoaDon.HMD_ID, listHang);

                    XmlElement hangItem = docNPL.CreateElement("CHUNG_TU_HOADON.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = docNPL.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = docNPL.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = docNPL.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute NUOC_XX = docNPL.CreateAttribute("NUOC_XX");
                    NUOC_XX.Value = hmd.NuocXX_ID;
                    hangItem.Attributes.Append(NUOC_XX);

                    XmlAttribute MA_DVT = docNPL.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = docNPL.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = docNPL.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = docNPL.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GIATRITANG = docNPL.CreateAttribute("GIATRITANG");
                    GIATRITANG.Value = BaseClass.Round(hangHoaDon.GiaTriDieuChinhTang, 9);
                    hangItem.Attributes.Append(GIATRITANG);

                    XmlAttribute GIATRIGIAM = docNPL.CreateAttribute("GIATRIGIAM");
                    GIATRIGIAM.Value = BaseClass.Round(hangHoaDon.GiaiTriDieuChinhGiam, 9);
                    hangItem.Attributes.Append(GIATRIGIAM);

                    XmlAttribute GHI_CHUhang = docNPL.CreateAttribute("GHI_CHU");
                    if (hangHoaDon.GhiChu == null)
                        hangHoaDon.GhiChu = "";
                    GHI_CHUhang.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hangHoaDon.GhiChu.Trim());
                    hangItem.Attributes.Append(GHI_CHUhang);
                }
            }
            #endregion

            #region Chuyển cửa khẩu
            //Chuyen cua khau :
            if (listChuyenCuaKhau == null || listChuyenCuaKhau.Count == 0)
            {
                listChuyenCuaKhau = (List<DeNghiChuyenCuaKhau>)DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlElement CHUNG_TU_DENGHICHUYENCK = docNPL.CreateElement("CHUNG_TU_DENGHICHUYENCK");
            foreach (DeNghiChuyenCuaKhau deNghichuyen in listChuyenCuaKhau)
            {
                XmlElement CHUNG_TU_DENGHICHUYENCKItem = docNPL.CreateElement("CHUNG_TU_DENGHICHUYENCK.ITEM");
                CHUNG_TU_DENGHICHUYENCK.AppendChild(CHUNG_TU_DENGHICHUYENCKItem);

                XmlElement DoanhNghiep = docNPL.CreateElement("DoanhNghiep");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(DoanhNghiep);

                XmlAttribute MaDN = docNPL.CreateAttribute("MaDN");
                MaDN.Value = deNghichuyen.MaDoanhNghiep;
                DoanhNghiep.Attributes.Append(MaDN);

                XmlAttribute TenDN = docNPL.CreateAttribute("TenDN");
                TenDN.Value = deNghichuyen.MaDoanhNghiep;
                DoanhNghiep.Attributes.Append(TenDN);

                XmlElement VanDoncck = docNPL.CreateElement("VanDon");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(VanDoncck);

                XmlAttribute SoVanDoncck = docNPL.CreateAttribute("SoVanDon");
                SoVanDoncck.Value = deNghichuyen.SoVanDon;
                VanDoncck.Attributes.Append(SoVanDoncck);

                XmlAttribute NgayVanDoncck = docNPL.CreateAttribute("NgayVanDon");
                NgayVanDoncck.Value = deNghichuyen.NgayVanDon.ToString("MM/dd/yyyy");
                VanDoncck.Attributes.Append(NgayVanDoncck);

                XmlElement NoiDung = docNPL.CreateElement("NoiDung");
                NoiDung.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(deNghichuyen.ThongTinKhac);
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(NoiDung);

                XmlElement DiaDiemKiemTra = docNPL.CreateElement("DiaDiemKiemTra");
                DiaDiemKiemTra.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(deNghichuyen.DiaDiemKiemTra);
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(DiaDiemKiemTra);

                XmlElement ThoiGianDen = docNPL.CreateElement("ThoiGianDen");
                ThoiGianDen.InnerText = deNghichuyen.ThoiGianDen.ToString("MM/dd/yyyy");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(ThoiGianDen);

                XmlElement TuyenDuong = docNPL.CreateElement("TuyenDuong");
                TuyenDuong.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(deNghichuyen.TuyenDuong);
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(TuyenDuong);

            }
            #endregion

            #region  Giấy phép

            XmlElement CHUNG_TU_GIAYPHEP = docNPL.CreateElement("CHUNG_TU_GIAYPHEP");
            foreach (GiayPhep giayphep in GiayPhepCollection)
            {
                XmlElement giayphepItem = docNPL.CreateElement("CHUNG_TU_GIAYPHEP.ITEM");
                CHUNG_TU_GIAYPHEP.AppendChild(giayphepItem);

                XmlAttribute SOGP = docNPL.CreateAttribute("SOGP");
                SOGP.Value = giayphep.SoGiayPhep.Trim();
                giayphepItem.Attributes.Append(SOGP);

                XmlAttribute NGAYGP = docNPL.CreateAttribute("NGAYGP");
                NGAYGP.Value = giayphep.NgayGiayPhep.ToString("MM/dd/yyyy");
                giayphepItem.Attributes.Append(NGAYGP);

                XmlAttribute NGAYHHGP = docNPL.CreateAttribute("NGAYHHGP");
                NGAYHHGP.Value = giayphep.NgayHetHan.ToString("MM/dd/yyyy");
                giayphepItem.Attributes.Append(NGAYHHGP);

                XmlAttribute NOICAP = docNPL.CreateAttribute("NOICAP");
                NOICAP.Value = giayphep.NoiCap.Trim();
                giayphepItem.Attributes.Append(NOICAP);

                XmlAttribute NGUOI_CAP = docNPL.CreateAttribute("NGUOI_CAP");
                NGUOI_CAP.Value = giayphep.NguoiCap.Trim();
                giayphepItem.Attributes.Append(NGUOI_CAP);

                XmlAttribute MA_NGUOI_DUOC_CAP = docNPL.CreateAttribute("MA_NGUOI_DUOC_CAP");
                MA_NGUOI_DUOC_CAP.Value = giayphep.MaDonViDuocCap.Trim();
                giayphepItem.Attributes.Append(MA_NGUOI_DUOC_CAP);

                XmlAttribute TEN_NGUOI_DUOC_CAP = docNPL.CreateAttribute("TEN_NGUOI_DUOC_CAP");
                TEN_NGUOI_DUOC_CAP.Value = giayphep.TenDonViDuocCap.Trim();
                giayphepItem.Attributes.Append(TEN_NGUOI_DUOC_CAP);

                XmlAttribute MA_CQC = docNPL.CreateAttribute("MA_CQC");
                MA_CQC.Value = giayphep.MaCoQuanCap.Trim();
                giayphepItem.Attributes.Append(MA_CQC);

                XmlAttribute TEN_CQC = docNPL.CreateAttribute("TEN_CQC");
                TEN_CQC.Value = giayphep.TenQuanCap.Trim();
                giayphepItem.Attributes.Append(TEN_CQC);

                XmlAttribute HINH_THUC_TL = docNPL.CreateAttribute("HINH_THUC_TL");
                HINH_THUC_TL.Value = "";
                giayphepItem.Attributes.Append(HINH_THUC_TL);

                XmlAttribute GHI_CHU = docNPL.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = "";
                giayphepItem.Attributes.Append(GHI_CHU);

                if (giayphep.ListHMDofGiayPhep == null || giayphep.ListHMDofGiayPhep.Count == 0)
                {

                    giayphep.LoadListHMDofGiayPhep();
                }

                foreach (HangGiayPhepDetail hanggiayPhep in giayphep.ListHMDofGiayPhep)
                {

                    Company.KDT.SHARE.QuanLyChungTu.HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hanggiayPhep.HMD_ID, listHang);

                    XmlElement hangItem = docNPL.CreateElement("CHUNG_TU_GIAYPHEP.HANG");
                    giayphepItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = docNPL.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = docNPL.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = docNPL.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute MA_DVT = docNPL.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = docNPL.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute TRIGIA_KB = docNPL.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);


                    //chua co phai bo sung
                    XmlAttribute MA_NT = docNPL.CreateAttribute("MA_NT");
                    MA_NT.Value = hanggiayPhep.MaNguyenTe;
                    hangItem.Attributes.Append(MA_NT);

                    XmlAttribute MA_CN = docNPL.CreateAttribute("MA_CN");
                    if (hanggiayPhep.MaChuyenNganh == null)
                        hanggiayPhep.MaChuyenNganh = "";
                    MA_CN.Value = hanggiayPhep.MaChuyenNganh.Trim();
                    hangItem.Attributes.Append(MA_CN);
                }
            }
            #endregion

            #region Chứng từ đính kèm

            if (ChungTuKemCollection == null || COCollection.Count == 0)
            {
                ChungTuKemCollection = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
            }
            XmlNode CHUNG_TU_KEM = docNPL.CreateElement("CHUNG_TU_KEM");
            XmlNode nodeHang1 = docNPL.SelectSingleNode("Root");

            foreach (ChungTuKem co in ChungTuKemCollection)
            {
                XmlElement CoItem = docNPL.CreateElement("CHUNG_TU_KEM.ITEM");
                CHUNG_TU_KEM.AppendChild(CoItem);

                XmlElement SO_CT = docNPL.CreateElement("SO_CT");
                SO_CT.InnerText = co.SO_CT;
                CoItem.AppendChild(SO_CT);

                XmlElement NGAY_CAP_CO = docNPL.CreateElement("NGAY_CT");
                NGAY_CAP_CO.InnerText = co.NGAY_CT.ToString("yyyy-MM-dd");
                CoItem.AppendChild(NGAY_CAP_CO);

                //CHUA CO PHAI BO SUNG VA KO DC TRONG
                XmlElement NGUOI_KY = docNPL.CreateElement("MA_LOAI_CT");
                NGUOI_KY.InnerText = co.MA_LOAI_CT;
                CoItem.AppendChild(NGUOI_KY);

                //NOI PHAT HANH LA TO CHUC CAP
                XmlElement NOI_PHAT_HANH = docNPL.CreateElement("DIENGIAI");
                NOI_PHAT_HANH.InnerText = co.DIENGIAI;
                CoItem.AppendChild(NOI_PHAT_HANH);

                XmlElement DINH_KEM = docNPL.CreateElement("DINH_KEM");
                CoItem.AppendChild(DINH_KEM);

                ChungTuKem ctct1 = new ChungTuKem();
                List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet> listCTCT = new List<ChungTuKemChiTiet>();
                listCTCT = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctct1.LoadCT(this.ID));
                foreach (ChungTuKemChiTiet ctct in listCTCT)
                {

                    XmlElement DINHKEM_ITEM = docNPL.CreateElement("DINH_KEM.ITEM");
                    DINH_KEM.AppendChild(DINHKEM_ITEM);

                    XmlElement TenFile = docNPL.CreateElement("TENFILE");
                    TenFile.InnerText = ctct.FileName.Trim();
                    DINHKEM_ITEM.AppendChild(TenFile);

                    XmlElement NoiDung = docNPL.CreateElement("NOIDUNG");
                    NoiDung.InnerText = System.Convert.ToBase64String(ctct.NoiDung, Base64FormattingOptions.InsertLineBreaks);
                    DINHKEM_ITEM.AppendChild(NoiDung);
                    XmlAttribute AttNoidung1 = docNPL.CreateAttribute("xmlns:dt");
                    AttNoidung1.Value = "urn:schemas-microsoft-com:datatypes";
                    NoiDung.Attributes.Append(AttNoidung1);
                    // NoiDung.SetAttributeNode(AttNoidung1.LocalName, "");

                    XmlAttribute AttNoidung2 = docNPL.CreateAttribute(@"dt:dt");
                    AttNoidung2.Value = "bin.base64";
                    //AttNoidung2.InnerXml.Replace("dt", "dt:dt");
                    NoiDung.Attributes.Append(AttNoidung2);

                }

            }
            try
            {


                if (VanDonCollection != null && VanDonCollection.Count > 0)
                    nodeHang1.AppendChild(ChungTuVanDon);
                if (COCollection != null && COCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_CO);
                //LanNT Gi sai dieu kien chung tu hop dong.
                //if (HoaDonThuongMaiCollection != null && HoaDonThuongMaiCollection.Count > 0)
                //    nodeHang1.AppendChild(CHUNG_TU_HOPDONG);
                //Fix
                if (HopDongThuongMaiCollection != null && HopDongThuongMaiCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_HOPDONG);
                if (HoaDonThuongMaiCollection != null && HoaDonThuongMaiCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_HOADON);
                if (listChuyenCuaKhau != null && listChuyenCuaKhau.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_DENGHICHUYENCK);
                if (ChungTuKemCollection != null && ChungTuKemCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_KEM);
                if (GiayPhepCollection != null && GiayPhepCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_GIAYPHEP);
            }
            catch { }
            #endregion

            #region Hàng mậu dịch tờ khai

            XmlNode nodeHang = docNPL.SelectSingleNode("Root/hangTKs");
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("hangTK");

                XmlNode HangDetails = docNPL.CreateElement("HangDetails");
                HangDetails.InnerText = "";
                node.AppendChild(HangDetails);

                XmlNode MaHSAtt = docNPL.CreateElement("MaHS");
                MaHSAtt.InnerText = hmd.MaHS;
                node.AppendChild(MaHSAtt);

                XmlNode maAtt = docNPL.CreateElement("Ma_Hang");
                maAtt.InnerText = this.LoaiHangHoa + hmd.MaPhu;
                node.AppendChild(maAtt);

                XmlNode TenAtt = docNPL.CreateElement("Ten_Hang");
                TenAtt.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang);
                node.AppendChild(TenAtt);

                XmlNode NuocAtt = docNPL.CreateElement("Nuoc_XX");
                NuocAtt.InnerText = hmd.NuocXX_ID;
                node.AppendChild(NuocAtt);

                XmlNode Ma_DVTAtt = docNPL.CreateElement("Ma_DVT");
                Ma_DVTAtt.InnerText = hmd.DVT_ID;
                node.AppendChild(Ma_DVTAtt);

                XmlNode SoLuongAtt = docNPL.CreateElement("Luong");
                SoLuongAtt.InnerText = BaseClass.Round(hmd.SoLuong, 9);
                node.AppendChild(SoLuongAtt);

                XmlNode DonGiaAtt = docNPL.CreateElement("DGia_KB");
                DonGiaAtt.InnerText = BaseClass.Round(hmd.DonGiaKB, 9);
                node.AppendChild(DonGiaAtt);

                XmlNode TriGiaAtt = docNPL.CreateElement("TriGia_KB");
                TriGiaAtt.InnerText = BaseClass.Round(hmd.TriGiaKB, 9); ;
                node.AppendChild(TriGiaAtt);

                XmlNode TriGiaVNDAtt = docNPL.CreateElement("TGTTVND");
                TriGiaVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaTT, 9); ;
                node.AppendChild(TriGiaVNDAtt);

                XmlNode DonGiaVNDAtt = docNPL.CreateElement("TGKBVND");
                DonGiaVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaKB_VND, 9); ;
                node.AppendChild(DonGiaVNDAtt);

                XmlNode MienThue = docNPL.CreateElement("MienThue");
                MienThue.InnerText = BaseClass.Round((decimal)hmd.MienThue, 9);
                node.AppendChild(MienThue);

                XmlNode TS_XNKAtt = docNPL.CreateElement("TS_XNK");
                TS_XNKAtt.InnerText = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.AppendChild(TS_XNKAtt);

                XmlNode ThueXNKAtt = docNPL.CreateElement("Thue_XNK");
                ThueXNKAtt.InnerText = BaseClass.Round(hmd.ThueXNK, 9);
                node.AppendChild(ThueXNKAtt);

                XmlNode TS_VSTAtt = docNPL.CreateElement("TS_VAT");
                TS_VSTAtt.InnerText = BaseClass.Round(hmd.ThueGTGT, 9);
                node.AppendChild(TS_VSTAtt);

                XmlNode ThueVATAtt = docNPL.CreateElement("Thue_VAT");
                ThueVATAtt.InnerText = "0";
                node.AppendChild(ThueVATAtt);
                // Linhhtn bổ sung 20100629
                XmlNode MA_HTS = docNPL.CreateElement("MA_HTS");
                MA_HTS.InnerText = hmd.Ma_HTS;
                node.AppendChild(MA_HTS);

                XmlNode MA_DVT_HTS = docNPL.CreateElement("MA_DVT_HTS");
                MA_DVT_HTS.InnerText = hmd.DVT_HTS;
                node.AppendChild(MA_DVT_HTS);

                XmlNode LUONG_HTS = docNPL.CreateElement("LUONG_HTS");
                LUONG_HTS.InnerText = BaseClass.Round(hmd.SoLuong_HTS, 9);
                node.AppendChild(LUONG_HTS);

                XmlNode DGIA_KB_HTS = docNPL.CreateElement("DGIA_KB_HTS");
                decimal dongiaHTS = 0;
                try
                {
                    dongiaHTS = Convert.ToDecimal(hmd.TriGiaKB / hmd.SoLuong_HTS);
                }
                catch { }
                DGIA_KB_HTS.InnerText = BaseClass.Round(dongiaHTS, 9);
                node.AppendChild(DGIA_KB_HTS);
                // end Linhhtn bổ sung 20100629
                XmlNode TL_PhuThuAtt = docNPL.CreateElement("TyLe_ThuKhac");
                TL_PhuThuAtt.InnerText = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.AppendChild(TL_PhuThuAtt);

                XmlNode TriGiaTK = docNPL.CreateElement("TriGia_ThuKhac");
                TriGiaTK.InnerText = BaseClass.Round(hmd.TriGiaThuKhac, 9);
                node.AppendChild(TriGiaTK);

                XmlNode sttAtt = docNPL.CreateElement("Index");
                sttAtt.InnerText = hmd.SoThuTuHang.ToString();
                node.AppendChild(sttAtt);

                nodeHang.AppendChild(node);
            #endregion

            #endregion
            }
            return docNPL.InnerXml;
        }

        #endregion Khai báo tờ khai xuất

        #region Huy khai báo tờ khai

        public string WSCancelXMLGC(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.Substring(0, 1).Equals("N"))
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiNhap, (int)MessageFunctions.HuyKhaiBao));
            else
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiXuat, (int)MessageFunctions.HuyKhaiBao));
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\HuyKhaiBaoToKhai.XML");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.HuyToKhai);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        this.ActionStatus = 1;
                        return doc.InnerXml;
                    }
                    //add new
                    else
                        this.ActionStatus = -1;
                    this.Update();
                }
                else
                {
                    msgError = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                    throw ex;
                }
                else if (!string.IsNullOrEmpty(kq))
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            return "";

        }



        #endregion Huy khai báo tờ khai

        #region Lấy phản hồi trạng thái

        public string WSRequestXMLGC(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.StartsWith("N"))
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiNhap, (int)MessageFunctions.HoiTrangThai));
            else
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiXuat, (int)MessageFunctions.HoiTrangThai));
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\LayPhanHoiToKkhai.XML");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";

        }

        #endregion Lấy phản hồi

        public string LayPhanHoiGC(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            doc.GetElementsByTagName("function")[0].InnerText = "5";
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);

                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(Result.InnerText + "|" + ""));
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(Result.InnerText + "|" + ""));
            }
            bool ok = false;
            if (function == (int)MessageFunctions.KhaiBao)
            {
                if (this.MaLoaiHinh.IndexOf("GC") > 0)
                {
                    this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SoTN"].Value);
                    this.NgayTiepNhan = Convert.ToDateTime(Result.Attributes["NgayTN"].Value);//DateTime.Today;
                }
                else
                {
                    this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan = DateTime.Today;
                }
                this.TrangThaiXuLy = 0;
                ok = true;
            }
            else if (function == (int)MessageFunctions.HuyKhaiBao)
            {
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.TrangThaiXuLy = -1;
                ok = true;
            }
            else if (function == (int)MessageFunctions.HoiTrangThai)
            {
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").InnerText == "2")
                {
                    this.TrangThaiXuLy = 1;
                    this.SoToKhai = Convert.ToInt32(docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").Attributes["SoTK"].Value);
                    CultureInfo culture = new CultureInfo("vi-VN");
                    this.NgayDangKy = Convert.ToDateTime(docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").Attributes["NgayDK"].Value, culture.DateTimeFormat);
                    ok = false;
                    this.TransferToNPLTon();
                    try
                    {
                        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                        if (nodeMess != null)
                        {
                            return nodeMess.OuterXml;
                        }
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                        if (nodeMess != null)
                        {
                            return nodeMess.OuterXml;
                        }
                    }
                    catch { }
                }
            }
            if (ok)
                this.Update();
            return "";

        }
        #endregion Webservice của hải quan

        #region DongBoDuLieuPhongKhai
        public void DongBoDuLieuKhaiDTByIDTKDK(ToKhaiMauDich TKMD, long IDTKOfHQ, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select * from HQQLHGCTEMPT.dbo.DHangMDDK where  SoTK=@SoTK and Ma_LH=@Ma_LH and Ma_HQ=@Ma_HQ and NamDK=@NamDK";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, TKMD.SoTiepNhan);
            db.AddInParameter(dbCommand, "@Ma_LH", SqlDbType.VarChar, TKMD.MaLoaiHinh);
            db.AddInParameter(dbCommand, "@Ma_HQ", SqlDbType.VarChar, TKMD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, TKMD.NgayDangKy.Year);

            DataSet dsHang = db.ExecuteDataSet(dbCommand);
            TKMD.HMDCollection = new List<HangMauDich>();
            foreach (DataRow row in dsHang.Tables[0].Rows)
            {
                HangMauDich hang = new HangMauDich();
                if (row["DGia_KB"].ToString() != "")
                    hang.DonGiaKB = Convert.ToDecimal(row["DGia_KB"].ToString());
                if (row["DGia_TT"].ToString() != "")
                    hang.DonGiaTT = Convert.ToDecimal(row["DGia_TT"].ToString());
                hang.DVT_ID = row["Ma_DVT"].ToString();
                hang.MaHS = row["Ma_HangKB"].ToString().Trim();
                hang.MaPhu = row["Ma_Phu"].ToString().Substring(1).Trim();
                TKMD.LoaiHangHoa = row["Ma_Phu"].ToString().Substring(0, 1);
                if (row["MienThue"].ToString() != "")
                    hang.MienThue = Convert.ToByte(row["MienThue"].ToString());
                hang.NuocXX_ID = row["Nuoc_XX"].ToString();
                if (row["Phu_Thu"].ToString() != "")
                    hang.PhuThu = Convert.ToDecimal(row["Phu_Thu"].ToString());
                if (row["Luong"].ToString() != "")
                    hang.SoLuong = Convert.ToDecimal(row["Luong"].ToString());
                hang.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten_Hang"].ToString());
                if (row["Thue_VAT"].ToString() != "")
                    hang.ThueGTGT = Convert.ToDecimal(row["Thue_VAT"].ToString());
                if (row["TS_VAT"].ToString() != "")
                    hang.ThueSuatGTGT = Convert.ToDecimal(row["TS_VAT"].ToString());
                if (row["TS_TTDB"].ToString() != "")
                    hang.ThueSuatTTDB = Convert.ToDecimal(row["TS_TTDB"].ToString());
                if (row["TS_XNK"].ToString() != "")
                    hang.ThueSuatXNK = Convert.ToDecimal(row["TS_XNK"].ToString());
                if (row["Thue_TTDB"].ToString() != "")
                    hang.ThueTTDB = Convert.ToDecimal(row["Thue_TTDB"].ToString());
                if (row["Thue_XNK"].ToString() != "")
                    hang.ThueXNK = Convert.ToDecimal(row["Thue_XNK"].ToString());
                if (row["TriGia_KB"].ToString() != "")
                    hang.TriGiaKB = Convert.ToDecimal(row["TriGia_KB"].ToString());
                if (row["TGKB_VND"].ToString() != "")
                    hang.TriGiaKB_VND = Convert.ToDecimal(row["TGKB_VND"].ToString());
                if (row["TriGia_ThuKhac"].ToString() != "")
                    hang.TriGiaThuKhac = Convert.ToDecimal(row["TriGia_ThuKhac"].ToString());
                if (row["TriGia_TT"].ToString() != "")
                    hang.TriGiaTT = Convert.ToDecimal(row["TriGia_TT"].ToString());
                if (row["TyLe_ThuKhac"].ToString() != "")
                    hang.TyLeThuKhac = Convert.ToDecimal(row["TyLe_ThuKhac"].ToString());
                TKMD.HMDCollection.Add(hang);
            }




        }
        public static void DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlDelete = "delete t_KDT_ToKhaiMauDichCheXuat where MaDoanhNghiep=@DVGC and TrangThaiXuLy!=-1 and MaHaiQuan=@Ma_HQHD";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.ExecuteNonQuery(dbCommandDelete);

            //sqlDelete = "delete t_KDT_ToKhaiMauDich where MaDoanhNghiep=@DVGC and TrangThaiXuLy=0 and MaHaiQuan=@Ma_HQHD";
            //dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            //db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            //db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            //db.ExecuteNonQuery(dbCommandDelete);

            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select HQQLHGCTEMPT.dbo.DToKhaiMD.* from DToKhaiMD where Ma_DV=@DVGC and Ma_HQ=@Ma_HQHD and NamDK=year(getdate())";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.CuaKhau_ID = row["Ma_CK"].ToString().Trim();
                    TKMD.DiaDiemXepHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["CangNN"].ToString());
                    TKMD.DKGH_ID = row["Ma_GH"].ToString();
                    TKMD.GiayTo = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["GiayTo"].ToString());
                    TKMD.SoVanDon = row["Van_Don"].ToString();
                    TKMD.MaDoanhNghiep = MaDoanhNghiep;
                    TKMD.MaHaiQuan = MaHaiQuan;
                    TKMD.MaLoaiHinh = row["Ma_LH"].ToString();
                    TKMD.NgayDangKy = Convert.ToDateTime(row["NGAYDK"].ToString());
                    if (row["NgayDen"].ToString() != "")
                        TKMD.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                    if (row["Ngay_GP"].ToString() != "")
                        TKMD.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                    if (row["Ngay_HHGP"].ToString() != "")
                        TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                    if (row["Ngay_HHHD"].ToString() != "")
                        TKMD.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                    if (row["Ngay_HDTM"].ToString() != "")
                        TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM"].ToString());
                    if (row["Ngay_HD"].ToString() != "")
                        TKMD.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                    TKMD.NgayTiepNhan = Convert.ToDateTime(row["NGAYDK"].ToString());
                    if (row["Ngay_VanDon"].ToString() != "")
                        TKMD.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                    TKMD.NguyenTe_ID = row["Ma_NT"].ToString();
                    if (TKMD.MaLoaiHinh.IndexOf("N") == 0)
                    {
                        TKMD.NuocNK_ID = row["Nuoc_NK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_XK"].ToString();
                    }
                    else
                    {
                        TKMD.NuocNK_ID = row["Nuoc_XK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_NK"].ToString();
                    }
                    if (row["Phi_BH"].ToString() != "")
                        TKMD.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                    if (row["Phi_VC"].ToString() != "")
                        TKMD.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                    TKMD.PTTT_ID = row["Ma_PTTT"].ToString();
                    TKMD.PTVT_ID = row["Ma_PTVT"].ToString();
                    if (row["So_Container"].ToString() != "")
                        TKMD.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                    if (row["So_container40"].ToString() != "")
                        TKMD.SoContainer40 = Convert.ToDecimal(row["So_container40"].ToString());
                    TKMD.SoGiayPhep = row["So_GP"].ToString();
                    TKMD.SoHieuPTVT = (row["Ten_PTVT"].ToString());
                    TKMD.SoHoaDonThuongMai = row["So_HDTM"].ToString();
                    TKMD.SoHopDong = row["So_HD"].ToString();
                    if (row["So_Kien"].ToString() != "")
                        TKMD.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                    if (row["So_PLTK"].ToString() != "")
                        TKMD.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                    TKMD.SoTiepNhan = Convert.ToInt64(row["SoTK"].ToString());
                    TKMD.TenChuHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["TenCH"].ToString());
                    if (row["SoTKChinhThuc"].ToString() != "")
                        TKMD.SoToKhai = Convert.ToInt32(row["SoTKChinhThuc"].ToString());
                    if (row["TrangThai"].ToString() != "2")
                        TKMD.TrangThaiXuLy = 0;
                    else
                        TKMD.TrangThaiXuLy = 1;
                    if (row["Tr_Luong"].ToString() != "")
                        TKMD.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                    if (row["TyGia_VND"].ToString() != "")
                        TKMD.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                    if (row["TyGia_USD"].ToString() != "")
                        TKMD.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                    TKMD.TenDonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DV_DT"].ToString());
                    long idTK = Convert.ToInt64(row["TKID"].ToString());
                    TKMD.DongBoDuLieuKhaiDTByIDTKDK(TKMD, idTK, nameConnectKDT);
                    TKMD.IDHopDong = (new Company.GC.BLL.KDT.GC.HopDong()).GetIDHopDongExit(TKMD.SoHopDong, MaHaiQuan, MaDoanhNghiep, TKMD.NgayHopDong);
                    try
                    {
                        if (TKMD.IDHopDong > 0)
                        {
                            TKMD.ID = TKMD.GetIDToKhaiBySoTiepNhanAndNamTiepNhanAndMaHaiQuan(TKMD.SoTiepNhan, (short)TKMD.NgayTiepNhan.Year, TKMD.MaHaiQuan);
                            if (TKMD.TrangThaiXuLy == 0)
                            {
                                TKMD.DeleteHangCollection(null);
                                TKMD.InsertUpdateFull();
                            }
                            else if (TKMD.ID == 0)
                                TKMD.InsertUpdateFull();
                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == 2)
                                continue;
                        }
                    }
                    catch { }
                }
            }

        }

        public static ToKhaiMauDich DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT, long SoTiepNhan, string MaLH, short NamDK)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlGet = "select id from  t_KDT_ToKhaiMauDich where MaDoanhNghiep=@DVGC and MaHaiQuan=@Ma_HQHD and SoTiepNhan=@SoTiepNhan and MaLoaiHinh=@MaLoaiHinh and year(NgayTiepNhan)=@NamDK";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlGet);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommandDelete, "@SoTiepNhan", SqlDbType.Int, SoTiepNhan);
            db.AddInParameter(dbCommandDelete, "@MaLoaiHinh", SqlDbType.VarChar, MaLH);
            db.AddInParameter(dbCommandDelete, "@NamDK", SqlDbType.SmallInt, NamDK);
            object o = db.ExecuteScalar(dbCommandDelete);
            if (o != null)
            {
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.ID = Convert.ToInt64(o);
                tkmd.Load();
                return tkmd;
            }

            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select HQQLHGCTEMPT.dbo.DToKhaiMD.* from DToKhaiMD where Ma_DV=@DVGC and Ma_HQ=@Ma_HQHD and  SoTK=@SoTK and Ma_LH=@Ma_LH and NamDK=@NamDK";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTiepNhan);
            db.AddInParameter(dbCommand, "@Ma_LH", SqlDbType.VarChar, MaLH);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.CuaKhau_ID = row["Ma_CK"].ToString().Trim();
                    TKMD.DiaDiemXepHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["CangNN"].ToString());
                    TKMD.DKGH_ID = row["Ma_GH"].ToString();
                    TKMD.GiayTo = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["GiayTo"].ToString());
                    TKMD.SoVanDon = row["Van_Don"].ToString();
                    TKMD.MaDoanhNghiep = MaDoanhNghiep;
                    TKMD.MaHaiQuan = MaHaiQuan;
                    TKMD.MaLoaiHinh = row["Ma_LH"].ToString();
                    TKMD.NgayDangKy = Convert.ToDateTime(row["NGAYDK"].ToString());
                    if (row["NgayDen"].ToString() != "")
                        TKMD.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                    if (row["Ngay_GP"].ToString() != "")
                        TKMD.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                    if (row["Ngay_HHGP"].ToString() != "")
                        TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                    if (row["Ngay_HHHD"].ToString() != "")
                        TKMD.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                    if (row["Ngay_HDTM"].ToString() != "")
                        TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM"].ToString());
                    if (row["Ngay_HD"].ToString() != "")
                        TKMD.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                    TKMD.NgayTiepNhan = Convert.ToDateTime(row["NGAYDK"].ToString());
                    if (row["Ngay_VanDon"].ToString() != "")
                        TKMD.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                    TKMD.NguyenTe_ID = row["Ma_NT"].ToString();
                    if (TKMD.MaLoaiHinh.IndexOf("N") == 0)
                    {
                        TKMD.NuocNK_ID = row["Nuoc_NK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_XK"].ToString();
                    }
                    else
                    {
                        TKMD.NuocNK_ID = row["Nuoc_XK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_NK"].ToString();
                    }
                    if (row["Phi_BH"].ToString() != "")
                        TKMD.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                    if (row["Phi_VC"].ToString() != "")
                        TKMD.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                    TKMD.PTTT_ID = row["Ma_PTTT"].ToString();
                    TKMD.PTVT_ID = row["Ma_PTVT"].ToString();
                    if (row["So_Container"].ToString() != "")
                        TKMD.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                    if (row["So_container40"].ToString() != "")
                        TKMD.SoContainer40 = Convert.ToDecimal(row["So_container40"].ToString());
                    TKMD.SoGiayPhep = row["So_GP"].ToString();
                    TKMD.SoHieuPTVT = (row["Ten_PTVT"].ToString());
                    TKMD.SoHoaDonThuongMai = row["So_HDTM"].ToString();
                    TKMD.SoHopDong = row["So_HD"].ToString();
                    if (row["So_Kien"].ToString() != "")
                        TKMD.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                    if (row["So_PLTK"].ToString() != "")
                        TKMD.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                    TKMD.SoTiepNhan = Convert.ToInt64(row["SoTK"].ToString());
                    if (row["SoTKChinhThuc"].ToString() != "")
                        TKMD.SoToKhai = Convert.ToInt32(row["SoTKChinhThuc"].ToString());
                    TKMD.TenChuHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["TenCH"].ToString());
                    TKMD.TrangThaiXuLy = 0;
                    if (row["Tr_Luong"].ToString() != "")
                        TKMD.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                    if (row["TyGia_VND"].ToString() != "")
                        TKMD.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                    if (row["TyGia_USD"].ToString() != "")
                        TKMD.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                    TKMD.TenDonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DV_DT"].ToString());
                    long idTK = Convert.ToInt64(row["TKID"].ToString());
                    TKMD.DongBoDuLieuKhaiDTByIDTKDK(TKMD, idTK, nameConnectKDT);
                    return TKMD;
                }
            }
            return null;

        }
        public long GetIDToKhaiBySoTiepNhanAndNamTiepNhanAndMaHaiQuan(long SoTiepNhan, short NamTN, string MaHQ)
        {
            string sql = "select ID from t_KDT_ToKhaiMauDich where SoTiepNhan=@SoTiepNhan and MaHaiQuan=@MaHaiQuan and year(NgayTiepNhan)=@NamTN";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHQ);
            db.AddInParameter(dbCommand, "@NamTN", SqlDbType.SmallInt, NamTN);

            object o = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);
        }
        public void DeleteHangCollection(SqlTransaction transaction)
        {
            string sql = "delete from t_KDT_HangMauDich where TKMD_ID=@ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                db.ExecuteNonQuery(dbCommand, transaction);
            else
                db.ExecuteNonQuery(dbCommand);
        }
        #endregion DongBoDuLieuPhongKhai

        public int DeleteBy_MaDoanhNghiep(SqlTransaction transaction)
        {
            string spName = "delete from t_KDT_ToKhaiMauDich where maDoanhNghiep='" + MaDoanhNghiep + "'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);


            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        public bool LoadBySoToKhai(int NamDangKy)
        {
            string spName = "SELECT * FROM t_KDT_ToKhaiMauDich WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND Year(NgayDangKy) = @NamDangKy AND MaHaiQuan = @MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuan);
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) this._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) this._IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) this._MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) this._Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) this._TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool LoadBySoToKhai(SqlTransaction transaction, int NamDangKy, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "SELECT * FROM t_KDT_ToKhaiMauDich WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND Year(NgayDangKy) = @NamDangKy AND MaHaiQuan = @MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuan);

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) this._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) this._IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) this._MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) this._Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) this._TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        private string GetFrontTGVND(string str)
        {
            string[] strtt = str.Split('.');
            string d1 = strtt[0].ToString();
            return d1;
        }
        private string GetEndTGVND(string str)
        {
            string[] strtt = str.Split('.');
            string d1 = strtt[1].Substring(0, 2);
            decimal dec = Convert.ToDecimal(d1);
            if (dec <= 0)
                return "";
            return d1;

        }

        #region SỬA TỜ KHAI ĐÃ DUYỆT

        public string WSKhaiBaoToKhaiSua(string password, string maMid)
        {
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(ConfigPhongBiSua(this.MaLoaiHinh.Substring(0, 1).StartsWith("N") ? (int)MessgaseType.ToKhaiNhap : (int)MessgaseType.ToKhaiXuat, (int)MessageFunctions.KhaiBao));
            //XmlDocument docNPL = new XmlDocument();
            //docNPL.LoadXml(this.MaLoaiHinh.Substring(0, 1).StartsWith("N") ? ConvertCollectionToXML_SUA_TKN_GC() : ConvertCollectionToXML_SUA_TKX_GC(maMid));

            ////luu vao string
            //XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            //XmlNode Content = doc.GetElementsByTagName("Content")[0];
            //Content.AppendChild(root);
            //if (this.MaLoaiHinh.Substring(0, 1).StartsWith("N"))
            //{
            //    XmlNode DU_LEU = doc.GetElementsByTagName("Root")[0];
            //    XmlNode node = CO.ConvertCollectionCOToXML(doc, this.COCollection, this.ID);

            //    //Hang hoa
            //    List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            //    listHang = ConvertHMDKDToHangMauDich();
            //    if (node != null)
            //        DU_LEU.AppendChild(node);

            //    //Giay phep
            //    node = GiayPhep.ConvertCollectionGiayPhepToXML(doc, this.GiayPhepCollection, this.ID, listHang);
            //    if (node != null)
            //        DU_LEU.AppendChild(node);

            //    //Hoa don thuong mai
            //    node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKN(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
            //    if (node != null)
            //        DU_LEU.AppendChild(node);

            //    //Hop dong thuong mai
            //    node = HopDongThuongMai.ConvertCollectionHopDongToXML_TKN(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
            //    if (node != null)
            //        DU_LEU.AppendChild(node);

            //    //Van tai don
            //    if (VanTaiDon == null)
            //    {
            //        List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
            //        if (VanDonCollection != null && VanDonCollection.Count > 0)
            //        {
            //            VanTaiDon = VanDonCollection[0];
            //            VanTaiDon.LoadContainerCollection();
            //        }
            //    }
            //    if (VanTaiDon != null)
            //    {
            //        node = this.VanTaiDon.ConvertVanDonToXML(doc);
            //        if (node != null)
            //            DU_LEU.AppendChild(node);
            //    }

            //    //Chuyen cua khau
            //    node = DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, this.listChuyenCuaKhau, this.ID);
            //    if (node != null)
            //        DU_LEU.AppendChild(node);

            //    //Ctkem
            //    ChungTuKem ctct = new ChungTuKem();

            //    List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet> listCTCT = new List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet>();
            //    listCTCT = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctct.LoadCT(this.ID));
            //    if (listCTCT != null && listCTCT.Count != 0)
            //    {
            //        node = ChungTuKem.ConvertCollectionCTDinhKemToXML(doc, ChungTuKemCollection, this.ID, listCTCT);
            //        if (node != null)
            //            DU_LEU.AppendChild(node);
            //    }
            //}
            ////WebService
            //Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            //string kq = "";
            //string msgError = string.Empty;
            //try
            //{

            //    //Khai báo
            //    kq = kdt.Send(doc.InnerXml, password);
            //    doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoToKhaiSua);


            //    XmlDocument docResult = new XmlDocument();
            //    docResult.LoadXml(kq);
            //    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            //    {
            //        if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
            //        {
            //            return doc.InnerXml;
            //        }
            //    }
            //    else
            //    {
            //        msgError = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
            //        throw new Exception(msgError);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (msgError != string.Empty)
            //    {
            //        kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
            //        throw ex;
            //    }
            //    else
            //        if (!string.IsNullOrEmpty(kq))
            //            Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
            //    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
            //    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            //}
            return "";
        }

        private string ConvertCollectionToXML_SUA_TKN_GC()
        {
            //load du lieu
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                HangMauDich hmd = new HangMauDich();
                hmd.TKMD_ID = this.ID;
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }

            //load du lieu
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\SuaTKN_GC.xml");
            //thông tin sửa tờ khai
            XmlNode nodeSOTK = docNPL.SelectSingleNode("Root/tokhai/SOTK");
            nodeSOTK.InnerText = this.SoToKhai.ToString();

            XmlNode nodeNGAY_DK = docNPL.SelectSingleNode("Root/tokhai/NGAY_DK");
            nodeNGAY_DK.InnerText = this.NgayDangKy.ToString("yyyy-MM-dd");

            XmlNode nodeLY_DO_SUA = docNPL.SelectSingleNode("Root/tokhai/LY_DO_SUA");
            nodeLY_DO_SUA.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.LyDoSua);

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/tokhai/MA_HQ");
            nodeHQNhan.InnerText = this.MaHaiQuan;

            XmlNode nodeSoTK = docNPL.SelectSingleNode("Root/tokhai/SoTK");
            nodeSoTK.InnerText = this.ID.ToString();

            XmlNode nodeMaLH = docNPL.SelectSingleNode("Root/tokhai/Ma_LH");
            nodeMaLH.InnerText = this.MaLoaiHinh;

            XmlNode nodeNamDK = docNPL.SelectSingleNode("Root/tokhai/NamDK");
            nodeNamDK.InnerText = DateTime.Today.Year.ToString();

            XmlNode nodeNgayDK = docNPL.SelectSingleNode("Root/tokhai/Ngay_DK");
            nodeNgayDK.InnerText = "";

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/tokhai/Ma_DV");
            nodeDN.InnerText = this.MaDoanhNghiep;

            XmlNode nodeDVUT = docNPL.SelectSingleNode("Root/tokhai/Ma_DVUT");
            nodeDVUT.InnerText = this.MaDonViUT;

            XmlNode nodeDVDoiTac = docNPL.SelectSingleNode("Root/tokhai/DV_TD");
            if (this.TenDonViDoiTac.Length <= 40)
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            else
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 40));
            XmlNode nodePTVT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTVT");
            nodePTVT.InnerText = this.PTVT_ID;

            XmlNode nodeSoHieuPTVT = docNPL.SelectSingleNode("Root/tokhai/Ten_PTVT");
            if (this.SoHieuPTVT.Length <= 20)
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT);
            else
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT.Substring(0, 20));
            XmlNode nodeNgayDen = docNPL.SelectSingleNode("Root/tokhai/NgayDen");
            if (this.NgayDenPTVT.Year > 1900)
                nodeNgayDen.InnerText = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeNgayDen.InnerText = "";

            XmlNode nodeVanDon = docNPL.SelectSingleNode("Root/tokhai/Van_Don");
            if (this.SoVanDon.Length <= 20)
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));

            XmlNode nodeCuaKhau = docNPL.SelectSingleNode("Root/tokhai/Ma_CK");
            nodeCuaKhau.InnerText = this.CuaKhau_ID;

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/tokhai/CangNN");
            if (this.DiaDiemXepHang.Length <= 40)
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));
            XmlNode nodeSoGiayPhep = docNPL.SelectSingleNode("Root/tokhai/So_GP");
            if (this.SoGiayPhep.Length <= 80)
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep);
            else
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Substring(0, 80));
            XmlNode nodeNgayGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_GP");
            if (NgayGiayPhep.Year > 1900)
                nodeNgayGiayPhep.InnerText = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayGiayPhep.InnerText = "";

            XmlNode nodeNgayHHGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHGP");
            if (NgayHetHanGiayPhep.Year > 1900)
                nodeNgayHHGiayPhep.InnerText = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayHHGiayPhep.InnerText = "";

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/tokhai/So_HD");
            nodeSoHopDong.InnerText = this.SoHopDong;

            XmlNode nodeTyGiaUSD = docNPL.SelectSingleNode("Root/tokhai/TyGia_USD");
            nodeTyGiaUSD.InnerText = BaseClass.Round(this.TyGiaUSD, 9);

            XmlNode nodeNgayHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HD");
            if (NgayHopDong.Year > 1900)
                nodeNgayHopDong.InnerText = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHopDong.InnerText = "";

            XmlNode nodeNgayHHHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHHD");
            if (NgayHetHanHopDong.Year > 1900)
                nodeNgayHHHopDong.InnerText = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHHHopDong.InnerText = "";

            XmlNode nodeNuocXK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_XK");
            nodeNuocXK.InnerText = this.NuocXK_ID;

            XmlNode nodeNuocNK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_NK");
            nodeNuocNK.InnerText = "VN";

            XmlNode nodeDieuKienGiaoHang = docNPL.SelectSingleNode("Root/tokhai/MA_GH");
            nodeDieuKienGiaoHang.InnerText = this.DKGH_ID;

            XmlNode nodeSoHang = docNPL.SelectSingleNode("Root/tokhai/SoHang");
            nodeSoHang.InnerText = this.HMDCollection.Count.ToString();

            XmlNode nodePTTT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTTT");
            nodePTTT.InnerText = this.PTTT_ID;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/tokhai/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;

            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/tokhai/TyGia_VND");

            nodeTyGiaVND.InnerText = BaseClass.Round(this.TyGiaTinhThue, 9);

            XmlNode nodeGiayTo = docNPL.SelectSingleNode("Root/tokhai/GiayTo");
            if (GiayTo.Length > 40)
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo);

            XmlNode nodeSoKien = docNPL.SelectSingleNode("Root/tokhai/So_Kien");
            nodeSoKien.InnerText = Convert.ToUInt32(this.SoKien).ToString();

            XmlNode nodeSoContainer20 = docNPL.SelectSingleNode("Root/tokhai/So_Container");
            nodeSoContainer20.InnerText = Convert.ToUInt64(this.SoContainer20).ToString();

            XmlNode nodeHDTM = docNPL.SelectSingleNode("Root/tokhai/So_HDTM");
            if (SoHoaDonThuongMai.Length <= 30)
                nodeHDTM.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai);
            else
                nodeHDTM.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai.Substring(0, 30));

            XmlNode nodeNgayHDTM = docNPL.SelectSingleNode("Root/tokhai/Ngay_HDTM");
            if (NgayHoaDonThuongMai.Year > 1900)
                nodeNgayHDTM.InnerText = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeNgayHDTM.InnerText = "";

            XmlNode nodeSoPLTK = docNPL.SelectSingleNode("Root/tokhai/So_PLTK");
            nodeSoPLTK.InnerText = this.SoLuongPLTK.ToString();

            XmlNode nodeChuHang = docNPL.SelectSingleNode("Root/tokhai/ChuHang");
            if (TenChuHang.Length > 30)
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang);
            XmlNode nodeNgayVanDon = docNPL.SelectSingleNode("Root/tokhai/Ngay_VanDon");
            if (NgayVanDon.Year > 1900)
                nodeNgayVanDon.InnerText = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeNgayVanDon.InnerText = "";

            XmlNode nodeTrongLuong = docNPL.SelectSingleNode("Root/tokhai/TrangLuong");
            nodeTrongLuong.InnerText = BaseClass.Round(this.TrongLuong, 9);

            XmlNode nodeSoContainer40 = docNPL.SelectSingleNode("Root/tokhai/SoContainer40");
            nodeSoContainer40.InnerText = Convert.ToUInt64(this.SoContainer40).ToString();

            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/tokhai/CTKT");
            nodeCTKT.InnerText = "";

            XmlNode nodeMaMid = docNPL.SelectSingleNode("Root/tokhai/MA_MID");
            nodeMaMid.InnerText = this.MaMid;

            //Hang mau dich
            XmlNode nodeHang = docNPL.SelectSingleNode("Root/hangTKs");
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("hangTK");

                XmlNode HangDetails = docNPL.CreateElement("HangDetails");
                HangDetails.InnerText = "";
                node.AppendChild(HangDetails);

                XmlNode MaHSAtt = docNPL.CreateElement("MaHS");
                MaHSAtt.InnerText = hmd.MaHS;
                node.AppendChild(MaHSAtt);

                XmlNode maAtt = docNPL.CreateElement("Ma_Hang");
                maAtt.InnerText = this.LoaiHangHoa + hmd.MaPhu;
                node.AppendChild(maAtt);

                XmlNode TenAtt = docNPL.CreateElement("Ten_Hang");
                TenAtt.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang);
                node.AppendChild(TenAtt);

                XmlNode NuocAtt = docNPL.CreateElement("Nuoc_XX");
                NuocAtt.InnerText = hmd.NuocXX_ID;
                node.AppendChild(NuocAtt);

                XmlNode Ma_DVTAtt = docNPL.CreateElement("Ma_DVT");
                Ma_DVTAtt.InnerText = hmd.DVT_ID;
                node.AppendChild(Ma_DVTAtt);

                XmlNode SoLuongAtt = docNPL.CreateElement("Luong");
                SoLuongAtt.InnerText = BaseClass.Round(hmd.SoLuong, 9);
                node.AppendChild(SoLuongAtt);

                XmlNode DonGiaAtt = docNPL.CreateElement("DGia_KB");
                DonGiaAtt.InnerText = BaseClass.Round(hmd.DonGiaKB, 9);
                node.AppendChild(DonGiaAtt);

                XmlNode TriGiaAtt = docNPL.CreateElement("TriGia_KB");
                TriGiaAtt.InnerText = BaseClass.Round(hmd.TriGiaKB, 9);
                node.AppendChild(TriGiaAtt);

                XmlNode TriGiaVNDAtt = docNPL.CreateElement("TGTTVND");
                TriGiaVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaTT, 9);
                node.AppendChild(TriGiaVNDAtt);

                XmlNode TGKBVNDVNDAtt = docNPL.CreateElement("TGKBVND");
                TGKBVNDVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaKB_VND, 9);
                node.AppendChild(TGKBVNDVNDAtt);

                XmlNode MienThue = docNPL.CreateElement("MienThue");
                MienThue.InnerText = BaseClass.Round((decimal)hmd.MienThue, 9);
                node.AppendChild(MienThue);

                XmlNode TS_XNKAtt = docNPL.CreateElement("TS_XNK");
                TS_XNKAtt.InnerText = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.AppendChild(TS_XNKAtt);

                XmlNode ThueXNKAtt = docNPL.CreateElement("Thue_XNK");
                ThueXNKAtt.InnerText = BaseClass.Round(hmd.ThueXNK, 9);
                node.AppendChild(ThueXNKAtt);

                XmlNode TS_VSTAtt = docNPL.CreateElement("TS_VAT");
                TS_VSTAtt.InnerText = BaseClass.Round(hmd.ThueSuatGTGT, 9);
                node.AppendChild(TS_VSTAtt);

                XmlNode ThueVATAtt = docNPL.CreateElement("Thue_VAT");
                ThueVATAtt.InnerText = BaseClass.Round(hmd.ThueGTGT, 9);
                node.AppendChild(ThueVATAtt);

                XmlNode TL_PhuThuAtt = docNPL.CreateElement("TyLe_ThuKhac");
                TL_PhuThuAtt.InnerText = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.AppendChild(TL_PhuThuAtt);

                XmlNode TriGiaTK = docNPL.CreateElement("TriGia_ThuKhac");
                TriGiaTK.InnerText = BaseClass.Round(hmd.TriGiaThuKhac, 9);
                node.AppendChild(TriGiaTK);

                XmlNode sttAtt = docNPL.CreateElement("Index");
                sttAtt.InnerText = hmd.SoThuTuHang.ToString();
                node.AppendChild(sttAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        private string ConvertCollectionToXML_SUA_TKX_GC(string maMid)
        {
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                HangMauDich hmd = new HangMauDich();
                hmd.TKMD_ID = this.ID;
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }

            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\SuaTKX_GC.xml");

            //thông tin sửa tờ khai
            XmlNode nodeSOTK = docNPL.SelectSingleNode("Root/tokhai/SOTK");
            nodeSOTK.InnerText = this.SoToKhai.ToString();

            XmlNode nodeNGAY_DK = docNPL.SelectSingleNode("Root/tokhai/NGAY_DK");
            nodeNGAY_DK.InnerText = this.NgayDangKy.ToString("yyyy-MM-dd");

            XmlNode nodeLY_DO_SUA = docNPL.SelectSingleNode("Root/tokhai/LY_DO_SUA");
            nodeLY_DO_SUA.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.LyDoSua);

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/tokhai/MA_HQ");
            nodeHQNhan.InnerText = this.MaHaiQuan;

            XmlNode nodeSoTK = docNPL.SelectSingleNode("Root/tokhai/SoTK");
            nodeSoTK.InnerText = this.ID.ToString();

            XmlNode nodeMaLH = docNPL.SelectSingleNode("Root/tokhai/Ma_LH");
            nodeMaLH.InnerText = this.MaLoaiHinh;

            XmlNode nodeNamDK = docNPL.SelectSingleNode("Root/tokhai/NamDK");
            nodeNamDK.InnerText = DateTime.Today.Year.ToString();

            XmlNode nodeNgayDK = docNPL.SelectSingleNode("Root/tokhai/Ngay_DK");
            nodeNgayDK.InnerText = "";

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/tokhai/Ma_DV");
            nodeDN.InnerText = this.MaDoanhNghiep;

            XmlNode nodeDVUT = docNPL.SelectSingleNode("Root/tokhai/Ma_DVUT");
            nodeDVUT.InnerText = this.MaDonViUT;

            XmlNode nodeDVDoiTac = docNPL.SelectSingleNode("Root/tokhai/DV_TD");
            if (this.TenDonViDoiTac.Length <= 40)
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            else
                nodeDVDoiTac.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 40));
            XmlNode nodePTVT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTVT");
            nodePTVT.InnerText = this.PTVT_ID;

            XmlNode nodeSoHieuPTVT = docNPL.SelectSingleNode("Root/tokhai/Ten_PTVT");
            if (this.SoHieuPTVT.Length <= 20)
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT);
            else
                nodeSoHieuPTVT.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT.Substring(0, 20));

            XmlNode nodeNgayDen = docNPL.SelectSingleNode("Root/tokhai/NgayDen");
            if (this.NgayDenPTVT.Year > 1900)
                nodeNgayDen.InnerText = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeNgayDen.InnerText = "";

            XmlNode nodeVanDon = docNPL.SelectSingleNode("Root/tokhai/Van_Don");
            if (this.SoVanDon.Length <= 20)
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeVanDon.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));


            XmlNode nodeCuaKhau = docNPL.SelectSingleNode("Root/tokhai/Ma_CK");
            nodeCuaKhau.InnerText = this.CuaKhau_ID;

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/tokhai/CangNN");
            if (this.DiaDiemXepHang.Length <= 40)
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeDiaDiemXepHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));

            XmlNode nodeSoGiayPhep = docNPL.SelectSingleNode("Root/tokhai/So_GP");
            if (this.SoGiayPhep.Length <= 80)
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep);
            else
                nodeSoGiayPhep.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Substring(0, 80));

            XmlNode nodeNgayGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_GP");
            if (NgayGiayPhep.Year > 1900)
                nodeNgayGiayPhep.InnerText = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayGiayPhep.InnerText = "";

            XmlNode nodeNgayHHGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHGP");
            if (NgayHetHanGiayPhep.Year > 1900)
                nodeNgayHHGiayPhep.InnerText = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayHHGiayPhep.InnerText = "";

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/tokhai/So_HD");
            nodeSoHopDong.InnerText = this.SoHopDong;

            XmlNode nodeTyGiaUSD = docNPL.SelectSingleNode("Root/tokhai/TyGia_USD");
            nodeTyGiaUSD.InnerText = BaseClass.Round(this.TyGiaUSD, 9);

            XmlNode nodeNgayHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HD");
            if (NgayHopDong.Year > 1900)
                nodeNgayHopDong.InnerText = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHopDong.InnerText = "";

            XmlNode nodeNgayHHHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHHD");
            if (NgayHetHanHopDong.Year > 1900)
                nodeNgayHHHopDong.InnerText = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHHHopDong.InnerText = "";

            XmlNode nodeNuocXK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_XK");
            nodeNuocXK.InnerText = this.NuocXK_ID;

            XmlNode nodeNuocNK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_NK");
            nodeNuocNK.InnerText = this.NuocNK_ID;//"VN";

            XmlNode nodeDieuKienGiaoHang = docNPL.SelectSingleNode("Root/tokhai/MA_GH");
            nodeDieuKienGiaoHang.InnerText = this.DKGH_ID;

            XmlNode nodeSoHang = docNPL.SelectSingleNode("Root/tokhai/SoHang");
            nodeSoHang.InnerText = this.HMDCollection.Count.ToString();

            XmlNode nodePTTT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTTT");
            nodePTTT.InnerText = this.PTTT_ID;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/tokhai/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;


            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/tokhai/TyGia_VND");

            nodeTyGiaVND.InnerText = BaseClass.Round(this.TyGiaTinhThue, 9);// this.TyGiaTinhThue.ToString("0");

            XmlNode nodeGiayTo = docNPL.SelectSingleNode("Root/tokhai/GiayTo");
            if (GiayTo.Length > 40)
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeGiayTo.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo);
            XmlNode nodeTenChuHang = docNPL.SelectSingleNode("Root/tokhai/TenCH");
            nodeTenChuHang.InnerText = "";
            XmlNode nodeSoKien = docNPL.SelectSingleNode("Root/tokhai/So_Kien");
            nodeSoKien.InnerText = Convert.ToUInt32(this.SoKien).ToString();

            XmlNode nodeSoContainer20 = docNPL.SelectSingleNode("Root/tokhai/So_Container");
            nodeSoContainer20.InnerText = Convert.ToUInt64(this.SoContainer20).ToString();

            XmlNode nodeHDTM = docNPL.SelectSingleNode("Root/tokhai/So_HDTM");
            if (SoHoaDonThuongMai.Length <= 30)
                nodeHDTM.InnerText = this.SoHoaDonThuongMai;
            else
                nodeHDTM.InnerText = this.SoHoaDonThuongMai.Substring(0, 30);


            XmlNode nodeNgayHDTM = docNPL.SelectSingleNode("Root/tokhai/Ngay_HDTM");
            if (NgayHoaDonThuongMai.Year > 1900)
                nodeNgayHDTM.InnerText = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeNgayHDTM.InnerText = "";

            XmlNode nodeSoPLTK = docNPL.SelectSingleNode("Root/tokhai/So_PLTK");
            nodeSoPLTK.InnerText = this.SoLuongPLTK.ToString();

            XmlNode nodeChuHang = docNPL.SelectSingleNode("Root/tokhai/ChuHang");
            if (TenChuHang.Length > 30)
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeChuHang.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang);
            XmlNode nodeNgayVanDon = docNPL.SelectSingleNode("Root/tokhai/Ngay_VanDon");
            if (NgayVanDon.Year > 1900)
                nodeNgayVanDon.InnerText = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeNgayVanDon.InnerText = "";

            XmlNode nodeTrongLuong = docNPL.SelectSingleNode("Root/tokhai/TrangLuong");
            nodeTrongLuong.InnerText = BaseClass.Round(this.TrongLuong, 9);

            XmlNode nodeSoContainer40 = docNPL.SelectSingleNode("Root/tokhai/SoContainer40");
            nodeSoContainer40.InnerText = Convert.ToUInt64(this.SoContainer40).ToString();

            //LINHHTN
            XmlNode nodeMaMID = docNPL.SelectSingleNode("Root/tokhai/MA_MID");
            nodeMaMID.InnerText = maMid;

            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/tokhai/CTKT");
            nodeCTKT.InnerText = "";

            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            listHang = ConvertHMDKDToHangMauDich();
            #region Chứng từ đính kèm

            #region Vận tải đơn
            // VAN TAI DON :
            List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
            if (VanTaiDon == null)
            {

                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }
            }
            XmlElement ChungTuVanDon = docNPL.CreateElement("CHUNG_TU_VAN_DON");
            if (VanTaiDon != null)
            {

                foreach (VanDon vd in VanDonCollection)
                {

                    XmlAttribute SO_CHUNG_TU = docNPL.CreateAttribute("SO_CHUNG_TU");
                    SO_CHUNG_TU.Value = SoVanDon.Trim();
                    ChungTuVanDon.Attributes.Append(SO_CHUNG_TU);

                    XmlAttribute NGAY_CHUNG_TU = docNPL.CreateAttribute("NGAY_CHUNG_TU");
                    NGAY_CHUNG_TU.Value = NgayVanDon.ToString("MM/dd/yyyy");
                    ChungTuVanDon.Attributes.Append(NGAY_CHUNG_TU);

                    XmlAttribute NGAY_HH = docNPL.CreateAttribute("NGAY_HH");
                    NGAY_HH.Value = "";
                    ChungTuVanDon.Attributes.Append(NGAY_HH);

                    XmlAttribute TEN_NGUOI_NHAN_HANG = docNPL.CreateAttribute("TEN_NGUOI_NHAN_HANG");
                    TEN_NGUOI_NHAN_HANG.Value = vd.TenNguoiNhanHang.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_NGUOI_NHAN_HANG);

                    XmlAttribute MA_NGUOI_NHAN_HANG = docNPL.CreateAttribute("MA_NGUOI_NHAN_HANG");
                    MA_NGUOI_NHAN_HANG.Value = vd.MaNguoiNhanHang.Trim();
                    ChungTuVanDon.Attributes.Append(MA_NGUOI_NHAN_HANG);

                    XmlAttribute TEN_NGUOI_GIAO_HANG = docNPL.CreateAttribute("TEN_NGUOI_GIAO_HANG");
                    TEN_NGUOI_GIAO_HANG.Value = vd.TenNguoiGiaoHang.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_NGUOI_GIAO_HANG);

                    XmlAttribute MA_NGUOI_GIAO_HANG = docNPL.CreateAttribute("MA_NGUOI_GIAO_HANG");
                    MA_NGUOI_GIAO_HANG.Value = vd.MaNguoiGiaoHang.Trim();
                    ChungTuVanDon.Attributes.Append(MA_NGUOI_GIAO_HANG);

                    XmlAttribute CUA_KHAU_NHAP = docNPL.CreateAttribute("CUA_KHAU_NHAP");
                    CUA_KHAU_NHAP.Value = vd.CuaKhauNhap_ID.Trim();
                    ChungTuVanDon.Attributes.Append(CUA_KHAU_NHAP);

                    XmlAttribute CUA_KHAU_XUAT = docNPL.CreateAttribute("CUA_KHAU_XUAT");
                    CUA_KHAU_XUAT.Value = vd.CuaKhauXuat.Trim();
                    ChungTuVanDon.Attributes.Append(CUA_KHAU_XUAT);

                    XmlAttribute MA_DKGH = docNPL.CreateAttribute("MA_DKGH");
                    MA_DKGH.Value = vd.DKGH_ID.Trim();
                    ChungTuVanDon.Attributes.Append(MA_DKGH);

                    XmlAttribute TEN_NGUOI_NHAN_HANG_TG = docNPL.CreateAttribute("TEN_NGUOI_NHAN_HANG_TG");
                    TEN_NGUOI_NHAN_HANG_TG.Value = vd.DKGH_ID.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_NGUOI_NHAN_HANG_TG);

                    XmlAttribute MA_NGUOI_NHAN_HANG_TG = docNPL.CreateAttribute("MA_NGUOI_NHAN_HANG_TG");
                    MA_NGUOI_NHAN_HANG_TG.Value = vd.MaNguoiNhanHangTrungGian.Trim();
                    ChungTuVanDon.Attributes.Append(MA_NGUOI_NHAN_HANG_TG);

                    XmlAttribute DIA_DIEM_GIAO_HANG = docNPL.CreateAttribute("DIA_DIEM_GIAO_HANG");
                    DIA_DIEM_GIAO_HANG.Value = vd.DiaDiemGiaoHang.Trim();
                    ChungTuVanDon.Attributes.Append(DIA_DIEM_GIAO_HANG);

                    XmlAttribute CONTAINER = docNPL.CreateAttribute("CONTAINER");
                    CONTAINER.Value = "0";//kiem tra lai  TS DE =0
                    ChungTuVanDon.Attributes.Append(CONTAINER);

                    XmlAttribute SO_HIEU_PTVT = docNPL.CreateAttribute("SO_HIEU_PTVT");
                    SO_HIEU_PTVT.Value = vd.SoHieuPTVT.Trim();
                    ChungTuVanDon.Attributes.Append(SO_HIEU_PTVT);

                    XmlAttribute TEN_PTVT = docNPL.CreateAttribute("TEN_PTVT");
                    TEN_PTVT.Value = vd.TenPTVT.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_PTVT);

                    XmlAttribute SO_HIEU_CHUYEN_DI = docNPL.CreateAttribute("SO_HIEU_CHUYEN_DI");
                    SO_HIEU_CHUYEN_DI.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(vd.SoHieuChuyenDi);
                    ChungTuVanDon.Attributes.Append(SO_HIEU_CHUYEN_DI);

                    XmlAttribute QUOC_TICH_PTVT = docNPL.CreateAttribute("QUOC_TICH_PTVT");
                    QUOC_TICH_PTVT.Value = vd.QuocTichPTVT.Trim();
                    ChungTuVanDon.Attributes.Append(QUOC_TICH_PTVT);

                    XmlAttribute TEN_CANG_DO_HANG = docNPL.CreateAttribute("TEN_CANG_DO_HANG");
                    TEN_CANG_DO_HANG.Value = vd.TenCangDoHang.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_CANG_DO_HANG);

                    XmlAttribute MA_CANG_DO_HANG = docNPL.CreateAttribute("MA_CANG_DO_HANG");
                    MA_CANG_DO_HANG.Value = vd.MaCangDoHang.Trim();
                    ChungTuVanDon.Attributes.Append(MA_CANG_DO_HANG);

                    XmlAttribute NGAY_DEN_PTVT = docNPL.CreateAttribute("NGAY_DEN_PTVT");
                    NGAY_DEN_PTVT.Value = vd.NgayDenPTVT.ToString("MM/dd/yyyy");
                    ChungTuVanDon.Attributes.Append(NGAY_DEN_PTVT);

                    XmlAttribute TEN_CANG_XEP_HANG = docNPL.CreateAttribute("TEN_CANG_XEP_HANG");
                    TEN_CANG_XEP_HANG.Value = vd.TenCangXepHang.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_CANG_XEP_HANG);

                    XmlAttribute MA_CANG_XEP_HANG = docNPL.CreateAttribute("MA_CANG_XEP_HANG");
                    MA_CANG_XEP_HANG.Value = vd.MaCangXepHang.Trim();
                    ChungTuVanDon.Attributes.Append(MA_CANG_XEP_HANG);

                    XmlAttribute TEN_HANG_VAN_TAI = docNPL.CreateAttribute("TEN_HANG_VAN_TAI");
                    TEN_HANG_VAN_TAI.Value = vd.TenHangVT.Trim();
                    ChungTuVanDon.Attributes.Append(TEN_HANG_VAN_TAI);

                    XmlAttribute MA_HANG_VAN_TAI = docNPL.CreateAttribute("MA_HANG_VAN_TAI");
                    MA_HANG_VAN_TAI.Value = vd.MaHangVT.Trim();
                    ChungTuVanDon.Attributes.Append(MA_HANG_VAN_TAI);

                    XmlAttribute NUOC_XN = docNPL.CreateAttribute("NUOC_XN");
                    NUOC_XN.Value = vd.NuocXuat_ID.Trim();
                    ChungTuVanDon.Attributes.Append(NUOC_XN);

                    XmlAttribute NOI_PHAT_HANH = docNPL.CreateAttribute("NOI_PHAT_HANH");
                    NOI_PHAT_HANH.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(vd.NoiDi);
                    ChungTuVanDon.Attributes.Append(NOI_PHAT_HANH);

                    XmlAttribute NGAY_KHOI_HANH = docNPL.CreateAttribute("NGAY_KHOI_HANH");
                    NGAY_KHOI_HANH.Value = vd.NgayKhoiHanh.ToString("MM/dd/yyyy");
                    ChungTuVanDon.Attributes.Append(NGAY_KHOI_HANH);

                    XmlAttribute TONG_SO_KIEN = docNPL.CreateAttribute("TONG_SO_KIEN");
                    TONG_SO_KIEN.Value = "";
                    ChungTuVanDon.Attributes.Append(TONG_SO_KIEN);

                    XmlAttribute KIEU_DONG_GOI = docNPL.CreateAttribute("KIEU_DONG_GOI");
                    KIEU_DONG_GOI.Value = "";
                    ChungTuVanDon.Attributes.Append(KIEU_DONG_GOI);

                    XmlAttribute MA_PTVT = docNPL.CreateAttribute("MA_PTVT");
                    MA_PTVT.Value = "";
                    ChungTuVanDon.Attributes.Append(MA_PTVT);

                    List<Container> ContainerCollection = new List<Container>();
                    if (ContainerCollection.Count == null || ContainerCollection.Count == 0)
                        ContainerCollection = (List<Container>)Container.SelectCollectionBy_VanDon_ID(vd.ID);
                    foreach (Container c in ContainerCollection)
                    {
                        XmlElement CONT = docNPL.CreateElement("CHUNG_TU_VAN_DON.CONT");
                        ChungTuVanDon.AppendChild(CONT);

                        XmlAttribute CONTAINER_NO = docNPL.CreateAttribute("CONTAINER_NO");
                        CONTAINER_NO.Value = c.SoHieu;
                        CONT.Attributes.Append(CONTAINER_NO);

                        XmlAttribute CONTAINER_TYPE = docNPL.CreateAttribute("CONTAINER_TYPE");
                        CONTAINER_TYPE.Value = c.LoaiContainer;
                        CONT.Attributes.Append(CONTAINER_TYPE);

                        XmlAttribute SEAL_NO = docNPL.CreateAttribute("SEAL_NO");
                        SEAL_NO.Value = c.Seal_No;
                        CONT.Attributes.Append(SEAL_NO);

                        XmlAttribute TRANG_THAI = docNPL.CreateAttribute("TRANGTHAI");
                        TRANG_THAI.Value = c.Trang_thai.ToString();
                        CONT.Attributes.Append(TRANG_THAI);
                    }
                }
            }
            #endregion

            #region CO

            if (COCollection == null || COCollection.Count == 0)
            {
                COCollection = (List<CO>)CO.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlElement CHUNG_TU_CO = docNPL.CreateElement("CHUNG_TU_CO");
            foreach (CO co in COCollection)
            {
                XmlElement CoItem = docNPL.CreateElement("CHUNG_TU_CO.ITEM");
                CHUNG_TU_CO.AppendChild(CoItem);

                XmlAttribute SO_CO = docNPL.CreateAttribute("SO_CO");
                SO_CO.Value = co.SoCO;
                CoItem.Attributes.Append(SO_CO);

                XmlAttribute NUOC_CO = docNPL.CreateAttribute("NUOC_CO");
                NUOC_CO.Value = co.NuocCapCO;
                CoItem.Attributes.Append(NUOC_CO);

                XmlAttribute NGAY_CAP_CO = docNPL.CreateAttribute("NGAY_CAP_CO");
                NGAY_CAP_CO.Value = co.NgayCO.ToString("MM/dd/yyyy");
                CoItem.Attributes.Append(NGAY_CAP_CO);

                //CHUA CO PHAI BO SUNG VA KO DC TRONG
                XmlAttribute NGUOI_KY = docNPL.CreateAttribute("NGUOI_KY");
                NGUOI_KY.Value = co.NguoiKy;
                CoItem.Attributes.Append(NGUOI_KY);

                //NOI PHAT HANH LA TO CHUC CAP
                XmlAttribute NOI_PHAT_HANH = docNPL.CreateAttribute("NOI_PHAT_HANH");
                NOI_PHAT_HANH.Value = co.ToChucCap;
                CoItem.Attributes.Append(NOI_PHAT_HANH);

                XmlAttribute MA_LOAI_CO = docNPL.CreateAttribute("MA_LOAI_CO");
                MA_LOAI_CO.Value = co.LoaiCO;
                CoItem.Attributes.Append(MA_LOAI_CO);

                XmlAttribute NGUOI_XUAT = docNPL.CreateAttribute("NGUOI_XUAT");
                NGUOI_XUAT.Value = co.TenDiaChiNguoiXK.Trim(); ;
                CoItem.Attributes.Append(NGUOI_XUAT);

                XmlAttribute MA_NUOC_XUAT = docNPL.CreateAttribute("MA_NUOC_XUAT");
                MA_NUOC_XUAT.Value = co.MaNuocXKTrenCO.Trim(); ;
                CoItem.Attributes.Append(MA_NUOC_XUAT);

                XmlAttribute NGUOI_NHAP = docNPL.CreateAttribute("NGUOI_NHAP");
                NGUOI_NHAP.Value = co.TenDiaChiNguoiNK.Trim(); ;
                CoItem.Attributes.Append(NGUOI_NHAP);

                XmlAttribute MA_NUOC_NHAP = docNPL.CreateAttribute("MA_NUOC_NHAP");
                MA_NUOC_NHAP.Value = co.MaNuocNKTrenCO.Trim(); ;
                CoItem.Attributes.Append(MA_NUOC_NHAP);

                XmlAttribute NOI_DUNG = docNPL.CreateAttribute("NOI_DUNG");
                NOI_DUNG.Value = co.ThongTinMoTaChiTiet.Trim(); ;
                CoItem.Attributes.Append(NOI_DUNG);

                XmlAttribute NGAY_KHOI_HANH = docNPL.CreateAttribute("NGAY_KHOI_HANH");
                NGAY_KHOI_HANH.Value = "";
                CoItem.Attributes.Append(NGAY_KHOI_HANH);

                XmlAttribute GHI_CHU = docNPL.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = "";
                CoItem.Attributes.Append(GHI_CHU);

                // CHUA CO PHAI BO SUNG
                XmlAttribute NOP_SAU = docNPL.CreateAttribute("NOP_SAU");
                NOP_SAU.Value = co.NoCo.ToString();
                CoItem.Attributes.Append(NOP_SAU);

                // CHUA CO PHAI BO SUNG
                //XmlAttribute THOI_HAN_NOP = doc.CreateAttribute("THOI_HAN_NOP");
                //if (co.NoCo == 1)
                //    NOP_SAU.Value = co.ThoiHanNop.ToString("MM/dd/yyyy");
                //else
                //    NOP_SAU.Value = "";
                //CoItem.Attributes.Append(THOI_HAN_NOP);
                //thoilv edit :
                XmlAttribute THOI_HAN_NOP = docNPL.CreateAttribute("THOI_HAN_NOP");
                if (co.NoCo == 1)
                    THOI_HAN_NOP.Value = co.ThoiHanNop.ToString("MM/dd/yyyy");
                else
                    THOI_HAN_NOP.Value = "";
                CoItem.Attributes.Append(THOI_HAN_NOP);
            }
            #endregion

            #region Hợp đồng thương mại

            if (HopDongThuongMaiCollection == null || HopDongThuongMaiCollection.Count == 0)
            {
                HopDongThuongMaiCollection = (List<HopDongThuongMai>)HopDongThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlElement CHUNG_TU_HOPDONG = docNPL.CreateElement("CHUNG_TU_HOPDONG");
            foreach (HopDongThuongMai hopdongTM in HopDongThuongMaiCollection)
            {
                XmlElement hopdongTMItem = docNPL.CreateElement("CHUNG_TU_HOPDONG.ITEM");
                CHUNG_TU_HOPDONG.AppendChild(hopdongTMItem);


                XmlAttribute SO_CT = docNPL.CreateAttribute("SO_CT");
                SO_CT.Value = hopdongTM.SoHopDongTM.Trim();
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = docNPL.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = hopdongTM.NgayHopDongTM.ToString("MM/dd/yyyy");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute THOI_HAN_TT = docNPL.CreateAttribute("THOI_HAN_TT");
                THOI_HAN_TT.Value = hopdongTM.ThoiHanThanhToan.ToString("MM/dd/yyyy");
                hopdongTMItem.Attributes.Append(THOI_HAN_TT);

                XmlAttribute MA_GH = docNPL.CreateAttribute("MA_GH");
                MA_GH.Value = hopdongTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute DIADIEM_GH = docNPL.CreateAttribute("DIADIEM_GH");
                DIADIEM_GH.Value = hopdongTM.DiaDiemGiaoHang;
                hopdongTMItem.Attributes.Append(DIADIEM_GH);

                XmlAttribute MA_PTTT = docNPL.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = hopdongTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = docNPL.CreateAttribute("MA_NT");
                MA_NT.Value = hopdongTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute TONGTRIGIA = docNPL.CreateAttribute("TONGTRIGIA");
                TONGTRIGIA.Value = BaseClass.Round(hopdongTM.TongTriGia, 9);
                hopdongTMItem.Attributes.Append(TONGTRIGIA);

                XmlAttribute MA_DV = docNPL.CreateAttribute("MA_DV");
                MA_DV.Value = hopdongTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = docNPL.CreateAttribute("TEN_DV");
                TEN_DV.Value = hopdongTM.TenDonViMua.Trim();
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute MA_DV_DT = docNPL.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = hopdongTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = docNPL.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = hopdongTM.TenDonViMua.Trim();
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute GHI_CHU = docNPL.CreateAttribute("GHI_CHU");
                if (hopdongTM.ThongTinKhac == null)
                    hopdongTM.ThongTinKhac = "";
                GHI_CHU.Value = hopdongTM.ThongTinKhac.Trim();
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (hopdongTM.ListHangMDOfHopDong == null || hopdongTM.ListHangMDOfHopDong.Count == 0)
                {
                    hopdongTM.LoadListHangDMOfHopDong();
                }

                foreach (HopDongThuongMaiDetail hangHopDong in hopdongTM.ListHangMDOfHopDong)
                {

                    Company.KDT.SHARE.QuanLyChungTu.HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hangHopDong.HMD_ID, listHang);

                    XmlElement hangItem = docNPL.CreateElement("CHUNG_TU_HOPDONG.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = docNPL.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = docNPL.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = docNPL.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute MA_DVT = docNPL.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = docNPL.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = docNPL.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = docNPL.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GHI_CHUhang = docNPL.CreateAttribute("GHI_CHU");
                    if (hangHopDong.GhiChu == null)
                        hangHopDong.GhiChu = "";
                    GHI_CHUhang.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hangHopDong.GhiChu.Trim());
                    hangItem.Attributes.Append(GHI_CHUhang);

                    XmlAttribute NUOC_XXHang = docNPL.CreateAttribute("NUOC_XX");
                    NUOC_XXHang.Value = hmd.NuocXX_ID;
                    hangItem.Attributes.Append(NUOC_XXHang);
                }
            }
            #endregion

            #region Hóa đơn thương mại

            if (HoaDonThuongMaiCollection == null || HoaDonThuongMaiCollection.Count == 0)
            {
                HoaDonThuongMaiCollection = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlElement CHUNG_TU_HOADON = docNPL.CreateElement("CHUNG_TU_HOADON");
            foreach (HoaDonThuongMai HoaDonTM in HoaDonThuongMaiCollection)
            {
                XmlElement hopdongTMItem = docNPL.CreateElement("CHUNG_TU_HOADON.ITEM");
                CHUNG_TU_HOADON.AppendChild(hopdongTMItem);


                XmlAttribute SO_CT = docNPL.CreateAttribute("SO_CT");
                SO_CT.Value = HoaDonTM.SoHoaDon.Trim();
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = docNPL.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = HoaDonTM.NgayHoaDon.ToString("MM/dd/yyyy");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute MA_GH = docNPL.CreateAttribute("MA_GH");
                MA_GH.Value = HoaDonTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute MA_PTTT = docNPL.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = HoaDonTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = docNPL.CreateAttribute("MA_NT");
                MA_NT.Value = HoaDonTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);


                XmlAttribute MA_DV = docNPL.CreateAttribute("MA_DV");
                MA_DV.Value = HoaDonTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = docNPL.CreateAttribute("TEN_DV");
                TEN_DV.Value = HoaDonTM.TenDonViMua.Trim();
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute MA_DV_DT = docNPL.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = HoaDonTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = docNPL.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = HoaDonTM.TenDonViMua.Trim();
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute GHI_CHU = docNPL.CreateAttribute("GHI_CHU");
                if (HoaDonTM.ThongTinKhac == null)
                    HoaDonTM.ThongTinKhac = "";
                GHI_CHU.Value = HoaDonTM.ThongTinKhac.Trim();
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (HoaDonTM.ListHangMDOfHoaDon == null || HoaDonTM.ListHangMDOfHoaDon.Count == 0)
                {
                    HoaDonTM.LoadListHangDMOfHoaDon();
                }

                foreach (HoaDonThuongMaiDetail hangHoaDon in HoaDonTM.ListHangMDOfHoaDon)
                {

                    Company.KDT.SHARE.QuanLyChungTu.HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hangHoaDon.HMD_ID, listHang);

                    XmlElement hangItem = docNPL.CreateElement("CHUNG_TU_HOPDONG.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = docNPL.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = docNPL.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = docNPL.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute NUOC_XX = docNPL.CreateAttribute("NUOC_XX");
                    NUOC_XX.Value = hmd.NuocXX_ID;
                    hangItem.Attributes.Append(NUOC_XX);

                    XmlAttribute MA_DVT = docNPL.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = docNPL.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = docNPL.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = docNPL.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GIATRITANG = docNPL.CreateAttribute("GIATRITANG");
                    GIATRITANG.Value = BaseClass.Round(hangHoaDon.GiaTriDieuChinhTang, 9);
                    hangItem.Attributes.Append(GIATRITANG);

                    XmlAttribute GIATRIGIAM = docNPL.CreateAttribute("GIATRIGIAM");
                    GIATRIGIAM.Value = BaseClass.Round(hangHoaDon.GiaiTriDieuChinhGiam, 9);
                    hangItem.Attributes.Append(GIATRIGIAM);

                    XmlAttribute GHI_CHUhang = docNPL.CreateAttribute("GHI_CHU");
                    if (hangHoaDon.GhiChu == null)
                        hangHoaDon.GhiChu = "";
                    GHI_CHUhang.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hangHoaDon.GhiChu.Trim());
                    hangItem.Attributes.Append(GHI_CHUhang);
                }
            }
            #endregion

            #region Chuyển cửa khẩu
            //Chuyen cua khau :
            if (listChuyenCuaKhau == null || listChuyenCuaKhau.Count == 0)
            {
                listChuyenCuaKhau = (List<DeNghiChuyenCuaKhau>)DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlElement CHUNG_TU_DENGHICHUYENCK = docNPL.CreateElement("CHUNG_TU_DENGHICHUYENCK");
            foreach (DeNghiChuyenCuaKhau deNghichuyen in listChuyenCuaKhau)
            {
                XmlElement CHUNG_TU_DENGHICHUYENCKItem = docNPL.CreateElement("CHUNG_TU_DENGHICHUYENCK.ITEM");
                CHUNG_TU_DENGHICHUYENCK.AppendChild(CHUNG_TU_DENGHICHUYENCKItem);

                XmlElement DoanhNghiep = docNPL.CreateElement("DoanhNghiep");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(DoanhNghiep);

                XmlAttribute MaDN = docNPL.CreateAttribute("MaDN");
                MaDN.Value = deNghichuyen.MaDoanhNghiep;
                DoanhNghiep.Attributes.Append(MaDN);

                XmlAttribute TenDN = docNPL.CreateAttribute("TenDN");
                TenDN.Value = deNghichuyen.MaDoanhNghiep;
                DoanhNghiep.Attributes.Append(TenDN);

                XmlElement VanDoncck = docNPL.CreateElement("VanDon");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(VanDoncck);

                XmlAttribute SoVanDoncck = docNPL.CreateAttribute("SoVanDon");
                SoVanDoncck.Value = deNghichuyen.SoVanDon;
                VanDoncck.Attributes.Append(SoVanDoncck);

                XmlAttribute NgayVanDoncck = docNPL.CreateAttribute("NgayVanDon");
                NgayVanDoncck.Value = deNghichuyen.NgayVanDon.ToString("MM/dd/yyyy");
                VanDoncck.Attributes.Append(NgayVanDoncck);

                XmlElement NoiDung = docNPL.CreateElement("NoiDung");
                NoiDung.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(deNghichuyen.ThongTinKhac);
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(NoiDung);

                XmlElement DiaDiemKiemTra = docNPL.CreateElement("DiaDiemKiemTra");
                DiaDiemKiemTra.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(deNghichuyen.DiaDiemKiemTra);
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(DiaDiemKiemTra);

                XmlElement ThoiGianDen = docNPL.CreateElement("ThoiGianDen");
                ThoiGianDen.InnerText = deNghichuyen.ThoiGianDen.ToString("MM/dd/yyyy");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(ThoiGianDen);

                XmlElement TuyenDuong = docNPL.CreateElement("TuyenDuong");
                TuyenDuong.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(deNghichuyen.TuyenDuong);
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(TuyenDuong);

            }
            #endregion

            #region  Giấy phép

            XmlElement CHUNG_TU_GIAYPHEP = docNPL.CreateElement("CHUNG_TU_GIAYPHEP");
            foreach (GiayPhep giayphep in GiayPhepCollection)
            {
                XmlElement giayphepItem = docNPL.CreateElement("CHUNG_TU_GIAYPHEP.ITEM");
                CHUNG_TU_GIAYPHEP.AppendChild(giayphepItem);

                XmlAttribute SOGP = docNPL.CreateAttribute("SOGP");
                SOGP.Value = giayphep.SoGiayPhep.Trim();
                giayphepItem.Attributes.Append(SOGP);

                XmlAttribute NGAYGP = docNPL.CreateAttribute("NGAYGP");
                NGAYGP.Value = giayphep.NgayGiayPhep.ToString("MM/dd/yyyy");
                giayphepItem.Attributes.Append(NGAYGP);

                XmlAttribute NGAYHHGP = docNPL.CreateAttribute("NGAYHHGP");
                NGAYHHGP.Value = giayphep.NgayHetHan.ToString("MM/dd/yyyy");
                giayphepItem.Attributes.Append(NGAYHHGP);

                XmlAttribute NOICAP = docNPL.CreateAttribute("NOICAP");
                NOICAP.Value = giayphep.NoiCap.Trim();
                giayphepItem.Attributes.Append(NOICAP);

                XmlAttribute NGUOI_CAP = docNPL.CreateAttribute("NGUOI_CAP");
                NGUOI_CAP.Value = giayphep.NguoiCap.Trim();
                giayphepItem.Attributes.Append(NGUOI_CAP);

                XmlAttribute MA_NGUOI_DUOC_CAP = docNPL.CreateAttribute("MA_NGUOI_DUOC_CAP");
                MA_NGUOI_DUOC_CAP.Value = giayphep.MaDonViDuocCap.Trim();
                giayphepItem.Attributes.Append(MA_NGUOI_DUOC_CAP);

                XmlAttribute TEN_NGUOI_DUOC_CAP = docNPL.CreateAttribute("TEN_NGUOI_DUOC_CAP");
                TEN_NGUOI_DUOC_CAP.Value = giayphep.TenDonViDuocCap.Trim();
                giayphepItem.Attributes.Append(TEN_NGUOI_DUOC_CAP);

                XmlAttribute MA_CQC = docNPL.CreateAttribute("MA_CQC");
                MA_CQC.Value = giayphep.MaCoQuanCap.Trim();
                giayphepItem.Attributes.Append(MA_CQC);

                XmlAttribute TEN_CQC = docNPL.CreateAttribute("TEN_CQC");
                TEN_CQC.Value = giayphep.TenQuanCap.Trim();
                giayphepItem.Attributes.Append(TEN_CQC);

                XmlAttribute HINH_THUC_TL = docNPL.CreateAttribute("HINH_THUC_TL");
                HINH_THUC_TL.Value = "";
                giayphepItem.Attributes.Append(HINH_THUC_TL);

                XmlAttribute GHI_CHU = docNPL.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = "";
                giayphepItem.Attributes.Append(GHI_CHU);

                if (giayphep.ListHMDofGiayPhep == null || giayphep.ListHMDofGiayPhep.Count == 0)
                {

                    giayphep.LoadListHMDofGiayPhep();
                }

                foreach (HangGiayPhepDetail hanggiayPhep in giayphep.ListHMDofGiayPhep)
                {

                    Company.KDT.SHARE.QuanLyChungTu.HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hanggiayPhep.HMD_ID, listHang);

                    XmlElement hangItem = docNPL.CreateElement("CHUNG_TU_GIAYPHEP.HANG");
                    giayphepItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = docNPL.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = docNPL.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = docNPL.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute MA_DVT = docNPL.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = docNPL.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute TRIGIA_KB = docNPL.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);


                    //chua co phai bo sung
                    XmlAttribute MA_NT = docNPL.CreateAttribute("MA_NT");
                    MA_NT.Value = hanggiayPhep.MaNguyenTe;
                    hangItem.Attributes.Append(MA_NT);

                    XmlAttribute MA_CN = docNPL.CreateAttribute("MA_CN");
                    MA_CN.Value = hanggiayPhep.MaChuyenNganh.Trim();
                    hangItem.Attributes.Append(MA_CN);
                }
            }
            #endregion

            #region Chứng từ đính kèm

            if (ChungTuKemCollection == null || COCollection.Count == 0)
            {
                ChungTuKemCollection = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
            }
            XmlNode CHUNG_TU_KEM = docNPL.CreateElement("CHUNG_TU_KEM");
            XmlNode nodeHang1 = docNPL.SelectSingleNode("Root");

            foreach (ChungTuKem co in ChungTuKemCollection)
            {
                XmlElement CoItem = docNPL.CreateElement("CHUNG_TU_KEM.ITEM");
                CHUNG_TU_KEM.AppendChild(CoItem);

                XmlElement SO_CT = docNPL.CreateElement("SO_CT");
                SO_CT.InnerText = co.SO_CT;
                CoItem.AppendChild(SO_CT);

                XmlElement NGAY_CAP_CO = docNPL.CreateElement("NGAY_CT");
                NGAY_CAP_CO.InnerText = co.NGAY_CT.ToString("yyyy-MM-dd");
                CoItem.AppendChild(NGAY_CAP_CO);

                //CHUA CO PHAI BO SUNG VA KO DC TRONG
                XmlElement NGUOI_KY = docNPL.CreateElement("MA_LOAI_CT");
                NGUOI_KY.InnerText = co.MA_LOAI_CT;
                CoItem.AppendChild(NGUOI_KY);

                //NOI PHAT HANH LA TO CHUC CAP
                XmlElement NOI_PHAT_HANH = docNPL.CreateElement("DIENGIAI");
                NOI_PHAT_HANH.InnerText = co.DIENGIAI;
                CoItem.AppendChild(NOI_PHAT_HANH);

                XmlElement DINH_KEM = docNPL.CreateElement("DINH_KEM");
                CoItem.AppendChild(DINH_KEM);

                ChungTuKem ctct1 = new ChungTuKem();
                List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet> listCTCT = new List<ChungTuKemChiTiet>();
                listCTCT = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctct1.LoadCT(this.ID));
                foreach (ChungTuKemChiTiet ctct in listCTCT)
                {

                    XmlElement DINHKEM_ITEM = docNPL.CreateElement("DINH_KEM.ITEM");
                    DINH_KEM.AppendChild(DINHKEM_ITEM);

                    XmlElement TenFile = docNPL.CreateElement("TENFILE");
                    TenFile.InnerText = ctct.FileName.Trim();
                    DINHKEM_ITEM.AppendChild(TenFile);

                    XmlElement NoiDung = docNPL.CreateElement("NOIDUNG");
                    NoiDung.InnerText = System.Convert.ToBase64String(ctct.NoiDung, Base64FormattingOptions.None);
                    DINHKEM_ITEM.AppendChild(NoiDung);

                    XmlAttribute AttNoidung1 = docNPL.CreateAttribute("xmlns:dt");
                    AttNoidung1.Value = "urn:schemas-microsoft-com:datatypes";
                    NoiDung.Attributes.Append(AttNoidung1);

                    XmlAttribute AttNoidung2 = docNPL.CreateAttribute(@"dt:dt");
                    AttNoidung2.Value = "bin.base64";
                    NoiDung.Attributes.Append(AttNoidung2);

                }
                // Lưu ý: ĐATLMQ bổ sung nếu không có dòng này sẽ bị lỗi 08/03/2011
                DINH_KEM.InnerXml = DINH_KEM.InnerXml.Replace("dt=\"bin.base64\"", "dt:dt=\"bin.base64\"");

            }
            try
            {
                if (VanDonCollection != null && VanDonCollection.Count > 0)
                    nodeHang1.AppendChild(ChungTuVanDon);
                if (COCollection != null && COCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_CO);
                if (HoaDonThuongMaiCollection != null && HoaDonThuongMaiCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_HOPDONG);
                if (HoaDonThuongMaiCollection != null && HoaDonThuongMaiCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_HOADON);
                if (listChuyenCuaKhau != null && listChuyenCuaKhau.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_DENGHICHUYENCK);
                if (ChungTuKemCollection != null && ChungTuKemCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_KEM);
                if (GiayPhepCollection != null && GiayPhepCollection.Count > 0)
                    nodeHang1.AppendChild(CHUNG_TU_GIAYPHEP);
            }
            catch { }
            #endregion

            #region Hàng mậu dịch tờ khai

            XmlNode nodeHang = docNPL.SelectSingleNode("Root/hangTKs");
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("hangTK");

                XmlNode HangDetails = docNPL.CreateElement("HangDetails");
                HangDetails.InnerText = "";
                node.AppendChild(HangDetails);

                XmlNode MaHSAtt = docNPL.CreateElement("MaHS");
                MaHSAtt.InnerText = hmd.MaHS;
                node.AppendChild(MaHSAtt);

                XmlNode maAtt = docNPL.CreateElement("Ma_Hang");
                maAtt.InnerText = this.LoaiHangHoa + hmd.MaPhu;
                node.AppendChild(maAtt);

                XmlNode TenAtt = docNPL.CreateElement("Ten_Hang");
                TenAtt.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang);
                node.AppendChild(TenAtt);

                XmlNode NuocAtt = docNPL.CreateElement("Nuoc_XX");
                NuocAtt.InnerText = hmd.NuocXX_ID;
                node.AppendChild(NuocAtt);

                XmlNode Ma_DVTAtt = docNPL.CreateElement("Ma_DVT");
                Ma_DVTAtt.InnerText = hmd.DVT_ID;
                node.AppendChild(Ma_DVTAtt);

                XmlNode SoLuongAtt = docNPL.CreateElement("Luong");
                SoLuongAtt.InnerText = BaseClass.Round(hmd.SoLuong, 9);
                node.AppendChild(SoLuongAtt);

                XmlNode DonGiaAtt = docNPL.CreateElement("DGia_KB");
                DonGiaAtt.InnerText = BaseClass.Round(hmd.DonGiaKB, 9);
                node.AppendChild(DonGiaAtt);

                XmlNode TriGiaAtt = docNPL.CreateElement("TriGia_KB");
                TriGiaAtt.InnerText = BaseClass.Round(hmd.TriGiaKB, 9); ;
                node.AppendChild(TriGiaAtt);

                XmlNode TriGiaVNDAtt = docNPL.CreateElement("TGTTVND");
                TriGiaVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaTT, 9); ;
                node.AppendChild(TriGiaVNDAtt);

                XmlNode DonGiaVNDAtt = docNPL.CreateElement("TGKBVND");
                DonGiaVNDAtt.InnerText = BaseClass.Round(hmd.TriGiaKB_VND, 9); ;
                node.AppendChild(DonGiaVNDAtt);

                XmlNode MienThue = docNPL.CreateElement("MienThue");
                MienThue.InnerText = BaseClass.Round((decimal)hmd.MienThue, 9);
                node.AppendChild(MienThue);

                XmlNode TS_XNKAtt = docNPL.CreateElement("TS_XNK");
                TS_XNKAtt.InnerText = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.AppendChild(TS_XNKAtt);

                XmlNode ThueXNKAtt = docNPL.CreateElement("Thue_XNK");
                ThueXNKAtt.InnerText = BaseClass.Round(hmd.ThueXNK, 9);
                node.AppendChild(ThueXNKAtt);

                XmlNode TS_VSTAtt = docNPL.CreateElement("TS_VAT");
                TS_VSTAtt.InnerText = BaseClass.Round(hmd.ThueGTGT, 9);
                node.AppendChild(TS_VSTAtt);

                XmlNode ThueVATAtt = docNPL.CreateElement("Thue_VAT");
                ThueVATAtt.InnerText = "0";
                node.AppendChild(ThueVATAtt);
                // Linhhtn bổ sung 20100629
                XmlNode MA_HTS = docNPL.CreateElement("MA_HTS");
                MA_HTS.InnerText = hmd.Ma_HTS;
                node.AppendChild(MA_HTS);

                XmlNode MA_DVT_HTS = docNPL.CreateElement("MA_DVT_HTS");
                MA_DVT_HTS.InnerText = hmd.DVT_HTS;
                node.AppendChild(MA_DVT_HTS);

                XmlNode LUONG_HTS = docNPL.CreateElement("LUONG_HTS");
                LUONG_HTS.InnerText = BaseClass.Round(hmd.SoLuong_HTS, 9);
                node.AppendChild(LUONG_HTS);

                XmlNode DGIA_KB_HTS = docNPL.CreateElement("DGIA_KB_HTS");
                decimal dongiaHTS = 0;
                try
                {
                    dongiaHTS = Convert.ToDecimal(hmd.TriGiaKB / hmd.SoLuong_HTS);
                }
                catch { }
                DGIA_KB_HTS.InnerText = BaseClass.Round(dongiaHTS, 9);
                node.AppendChild(DGIA_KB_HTS);
                // end Linhhtn bổ sung 20100629
                XmlNode TL_PhuThuAtt = docNPL.CreateElement("TyLe_ThuKhac");
                TL_PhuThuAtt.InnerText = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.AppendChild(TL_PhuThuAtt);

                XmlNode TriGiaTK = docNPL.CreateElement("TriGia_ThuKhac");
                TriGiaTK.InnerText = BaseClass.Round(hmd.TriGiaThuKhac, 9);
                node.AppendChild(TriGiaTK);

                XmlNode sttAtt = docNPL.CreateElement("Index");
                sttAtt.InnerText = hmd.SoThuTuHang.ToString();
                node.AppendChild(sttAtt);

                nodeHang.AppendChild(node);
            #endregion

            #endregion
            }
            return docNPL.InnerXml;
        }

        public string LayPhanHoi_KhaibaoSuaToKhai(string pass, string xml)
        {
            //-------------begin----------------
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiLayPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.TenDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();
            //-----------end new --------------
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            int function = (int)MessageFunctions.KhaiBao;
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    //if (i > 1)
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);

                //Lay funtion tu ket qua tra ve
                function = Convert.ToInt32(docNPL.GetElementsByTagName("function")[0].InnerText);

                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {

                    Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = "Lỗi khai báo";
                    kqxl.NoiDung = string.Format("Lỗi khai báo sửa tờ khai: \r\nID: {0} \r\nLoại hình: {1}", this.ID, this.MaLoaiHinh)
                        + "\r\nNội dung: " + Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                }
            }

            if (i > 1)
                return doc.InnerXml;

            string errorSt = ""; // Luu thong tin loi
            try
            {
                #region XU LY MESSAGE

                /*Kiem tra lấy số tiếp nhận*/
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTN") != null)
                {
                    XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SOTN"].Value);
                    this.NamDK = Convert.ToInt32(nodeDuLieu.Attributes["NAMTN"].Value);
                    this.NgayTiepNhan = DateTime.Today;
                    this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                    this.Update();

                    Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                    kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToString());
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                }
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "LOI")
                {
                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DULIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DULIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;

                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";

                    Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai + " " + this.MaLoaiHinh;
                    kqxl.LoaiThongDiep = errorSt; //Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                    kqxl.NoiDung = docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText; // Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    errorSt = errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi;

                    throw new Exception(errorSt);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception(errorSt);
            }
            return "";

        }

        #endregion

        #region HỦY TỜ KHAI ĐÃ DUYỆT
        public string WSHuyToKhai(string password)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiSua(this.MaLoaiHinh.Substring(0, 1).StartsWith("N") ? (int)MessgaseType.ToKhaiNhap : (int)MessgaseType.ToKhaiXuat, (int)MessageFunctions.KhaiBao));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\XMLChungTu\\HuyTKDaDuyet.XML");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.Attributes["SOTN"].Value = this.SoTiepNhan.ToString();
            root.Attributes["NAMTN"].Value = this.NamDK.ToString();

            root.SelectSingleNode("ToKhai").Attributes["MaLH"].Value = this.MaLoaiHinh;
            root.SelectSingleNode("ToKhai").Attributes["MaHQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("ToKhai").Attributes["SoTK"].Value = this.SoToKhai.ToString();
            root.SelectSingleNode("ToKhai").Attributes["NgayDK"].Value = this.NgayDangKy.ToString("yyyy-MM-dd");
            root.SelectSingleNode("ToKhai").Attributes["NamDK"].Value = this.NamDK.ToString();

            root.SelectSingleNode("DoanhNghiep").Attributes["MaDN"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("DoanhNghiep").Attributes["TenDN"].Value = this.TenDoanhNghiep;

            List<HuyToKhai> huyTk = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_TKMD_ID(this.ID);

            root.SelectSingleNode("LyDo").InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(huyTk[0].LyDoHuy);

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(doc.InnerXml, password);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.HuyToKhai);

            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);

                if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {

                        this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                        this.Update();
                        huyTk[0].TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                        huyTk[0].Update();
                        return doc.InnerXml;
                    }
                }
                else
                {
                    msgError = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL";
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                if (!string.IsNullOrEmpty(kq))
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return string.Empty;
        }
        #endregion

        #region ĐỒNG BỘ DỮ LIỆU
        public string InsertFullFromISyncDaTa()
        {
            string error = string.Empty;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    List<Company.GC.BLL.KDT.GC.HopDong> hdco = Company.GC.BLL.KDT.GC.HopDong.SelectCollectionDynamic("SoHopDong = '" + this.SoHopDong.Trim() + "'", null);
                    if (hdco == null || hdco.Count == 0)
                    {
                        transaction.Rollback();
                        this.ID = 0;
                        connection.Close();
                        return "Không tồn tại hợp đồng của tờ khai";
                    }
                    else
                    {
                        this.IDHopDong = hdco[0].ID;
                    }
                    this.ID = 0;
                    this.ID = this.InsertTransaction(transaction);
                    int i = 1;


                    #region hàng mậu dịch
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;
                        hmd.TKMD_ID = this.ID;
                        hmd.ID = hmd.Insert(transaction);
                    }
                    #endregion
                    #region chứng từ hải quan
                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;
                        ct.ID = 0;
                        ct.Master_ID = this.ID;
                        ct.ID = ct.InsertTransaction(transaction);

                    }
                    foreach (ToKhaiTriGia tktg in this.TKTGCollection)
                    {
                        tktg.TKMD_ID = this.ID;
                        tktg.ID = 0;
                        tktg.ID = tktg.InsertTransaction(transaction);
                        foreach (Company.KD.BLL.KDT.HangTriGia htg in tktg.HTGCollection)
                        {
                            htg.ID = 0;
                            htg.TKTG_ID = tktg.ID;
                            htg.InsertTransaction(transaction);
                        }
                    }
                    foreach (ToKhaiTriGiaPP23 tktg23 in TKTGPP23Collection)
                    {
                        tktg23.TKMD_ID = this.ID;
                        tktg23.ID = 0;
                        tktg23.ID = tktg23.InsertTransaction(transaction);
                    }
                    i = 1;
                    foreach (CO co in this.ListCO)
                    {
                        co.TKMD_ID = this.ID;
                        co.ID = 0;
                        co.ID = co.InsertTransaction(transaction, db);
                        foreach (HangCoDetail item in co.ListHMDofCo)
                        {
                            item.Co_ID = co.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (ChungTuNo chungtuno in this.ChungTuNoCollection)
                    {
                        chungtuno.ID = 0;
                        chungtuno.TKMDID = this.ID;
                        chungtuno.InsertTransaction(transaction, db);
                    }
                    foreach (ChungTuKem ctk in this.ChungTuKemCollection)
                    {
                        ctk.ID = 0;
                        ctk.TKMDID = this.ID;
                        ctk.ID = ctk.InsertTransaction(transaction, db);
                        foreach (ChungTuKemChiTiet item in ctk.listCTChiTiet)
                        {
                            item.ID = 0;
                            item.ChungTuKemID = ctk.ID;
                            item.InsertTransaction(transaction, db);
                        }
                    }
                    foreach (GiayPhep gp in this.GiayPhepCollection)
                    {
                        gp.ID = 0;
                        gp.TKMD_ID = this.ID;
                        gp.ID = gp.InsertTransaction(transaction, db);
                        foreach (HangGiayPhepDetail item in gp.ListHMDofGiayPhep)
                        {
                            item.ID = 0;
                            item.GiayPhep_ID = gp.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (HoaDonThuongMai hdtm in this.HoaDonThuongMaiCollection)
                    {
                        hdtm.ID = 0;
                        hdtm.TKMD_ID = this.ID;
                        hdtm.ID = hdtm.InsertTransaction(transaction, db);
                        foreach (HoaDonThuongMaiDetail item in hdtm.ListHangMDOfHoaDon)
                        {
                            item.ID = 0;
                            item.HoaDonTM_ID = hdtm.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (HopDongThuongMai hopdong in this.HopDongThuongMaiCollection)
                    {
                        hopdong.ID = 0;
                        hopdong.TKMD_ID = this.ID;
                        hopdong.ID = hopdong.InsertTransaction(transaction, db);
                        foreach (HopDongThuongMaiDetail item in hopdong.ListHangMDOfHopDong)
                        {
                            item.ID = 0;
                            item.HopDongTM_ID = hopdong.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (DeNghiChuyenCuaKhau denghi in this.listChuyenCuaKhau)
                    {
                        denghi.ID = 0;
                        denghi.TKMD_ID = this.ID;
                        denghi.InsertTransaction(transaction, db);
                    }
                    foreach (NoiDungDieuChinhTK noidungsua in this.NoiDungDieuChinhTKCollection)
                    {
                        noidungsua.ID = 0;
                        noidungsua.TKMD_ID = this.ID;
                        noidungsua.Insert(transaction);
                    }
                    if (VanTaiDon != null)
                    {
                        VanTaiDon.ID = 0;
                        VanTaiDon.TKMD_ID = this.ID;
                        VanTaiDon.Insert(transaction);
                    }
                    foreach (Company.GC.BLL.KDT.GC.NPLCungUng item in this.NPLCungUngs)
                    {
                        item.ID = 0;
                        item.TKMDID = this.ID;
                        item.ID = item.Insert(transaction);
                        foreach (Company.GC.BLL.KDT.GC.NPLCungUngDetail nplcu in item.NPLCungUngDetails)
                        {
                            nplcu.ID = 0;
                            nplcu.Master_ID = item.ID;
                            nplcu.ID = nplcu.Insert(transaction);
                        }
                    }

                    #endregion

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    error = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            return error;
        }
        /// <summary>
        /// Đếm số tờ khai đã duyệt
        /// </summary>
        /// <param name="madoanhnghiep"></param>
        /// <returns></returns>
        public long SelectCountSoTKThongQuan(string madoanhnghiep)
        {
            string where = "MaDoanhNghiep='" + madoanhnghiep + "' AND TrangThaiXuLy = 1 AND HUONGDAN<>'' AND MSG_STATUS IS NULL";
            ToKhaiMauDichCollection tkmdco = new ToKhaiMauDichCollection();
            tkmdco = SelectCollectionDynamicDongBoDuLieu(where, null);
            if (tkmdco == null || tkmdco.Count == 0)
            {
                return 0;
            }
            else
                return tkmdco.Count;
        }

        public IDataReader SelectDynamicDongBoDuLieu(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectDynamicDongBoDuLieu]";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }
        public int TrangThaiDongBo;
        public ToKhaiMauDichCollection SelectCollectionDynamicDongBoDuLieu(string whereCondition, string orderByExpression)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = this.SelectDynamicDongBoDuLieu(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_STATUS"))) entity.TrangThaiDongBo = reader.GetInt32(reader.GetOrdinal("MSG_STATUS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) entity.Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) entity.TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }


        #endregion

        public static ToKhaiMauDich LoadToKhaiDaChuyenDoiVNACC(decimal sotk)
        {
            //Company.GC.BLL.KDT.ToKhaiMauDichCollection listTKMD = new Company.GC.BLL.KDT.ToKhaiMauDich().SelectCollectionDynamic(" SoToKhai in (Select SoTK From t_VNACCS_CapSoToKhai where SoTKVNACCSFull = " + sotk + ") AND MaLoaiHinh  = (SELECT ID FROM t_haiquan_loaihinhmaudich WHERE ten_Vt COLLATE DATABASE_DEFAULT =  (select top 1 MaLoaiHinh from t_VNACCS_CapSoToKhai where SoTKVNACCSFull = " + sotk + ") COLLATE DATABASE_DEFAULT)", null);
            Company.GC.BLL.KDT.ToKhaiMauDichCollection listTKMD = new Company.GC.BLL.KDT.ToKhaiMauDich().SelectCollectionDynamic(" LoaiVanDon like '"+sotk+"%'", null);
            if (listTKMD != null && listTKMD.Count > 0)
                return listTKMD[0];
            else
                return null;
        }
        internal void CapNhatThongTinHangToKhaiSua()
        {
            CapNhatThongTinHangToKhaiSua(true);
        }
        internal void CapNhatThongTinHangToKhaiSua(bool ChuyenTon)
        {
            if (this.MaLoaiHinh.Substring(0, 1).ToUpper() == "X")
                return;

            if (Company.GC.BLL.GC.PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap((int)this.SoToKhai, this.MaLoaiHinh, this.MaHaiQuan, (short)this.NgayDangKy.Year, this.IDHopDong))
            {
                Logger.LocalLogger.Instance().WriteMessage(new Exception("Tờ khai chuyển tiếp có id = " + this.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được."));
                return;
            }

            Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase db = (Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase)Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
            using (System.Data.SqlClient.SqlConnection connection = (System.Data.SqlClient.SqlConnection)db.CreateConnection())
            {
                connection.Open();
                System.Data.SqlClient.SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.MaLoaiHinh.Substring(0, 1) == "N" && this.LoaiHangHoa == "N")
                    {
                        Company.GC.BLL.GC.NPLNhapTonThucTe.DeleteNPLTonByToKhai(
                            this.SoToKhai,
                            (short)this.NgayDangKy.Year,
                            this.MaLoaiHinh,
                            this.MaHaiQuan, transaction);

                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            //Load hang to khai
            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();
            if (ChuyenTon)
                NPLNhapTonThucTe.UpdateNguyenPhuLieuTonThucTeByToKhaiAndHMD(this);
        }

        //TODO: Hungtq, 20/10/2014. Bo sung moi
        public static void CapNhatNamDangKyToKhaiDaDuyet()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "UPDATE dbo.t_KDT_ToKhaiMauDich SET NamDK = YEAR(NgayDangKy) WHERE TrangThaiXuLy = 1 AND NamDK = 0";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.ExecuteNonQuery(dbCommand);
        }

        public static DataSet CheckDuplicateTK(long HopDong_ID)
        {
            try
            {
                string spName = "p_KDT_ToKhaiMauDich_CheckDuplicateTK";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

                DataSet ds = db.ExecuteDataSet(dbCommand);

                return ds;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
    }
}
