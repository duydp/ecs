﻿using System;
using Company.KD.BLL;
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using System.Threading;
using System.Globalization;
using System.Resources;
using Company.Controls.CustomValidation;

namespace Company.Interface.SXXK
{
    public partial class NguyenPhuLieuEditForm : BaseForm
    {
        public Company.KD.BLL.SXXK.HangHoaNhap HangNhap;
        public NguyenPhuLieuEditForm()
        {
            InitializeComponent();
            //this.InitValidator += new ValidatorDelegate(SetInitCuleInfo);
        }
       
        private void btnClose_Click(object sender, EventArgs e)
        {          
           this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            // Kiểm tra tính hợp lệ của mã HS.
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //    error.SetIconPadding(txtMaHS, -8);
            //    error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
            //    return;
            //}

            if (!cvError.IsValid) return;
                  
            Company.KD.BLL.SXXK.HangHoaNhap nplSXXK = new Company.KD.BLL.SXXK.HangHoaNhap();
            nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            nplSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            nplSXXK.Ma = txtMa.Text.Trim();
            if (nplSXXK.Load())
            {
                MLMessages("Hàng nhập này đã được đăng ký.","MSG_STN09","", false);
                return;
            }
        
            nplSXXK.Ten = txtTen.Text.Trim();
            nplSXXK.MaHS = txtMaHS.Text;
            nplSXXK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            if (HangNhap != null)
                nplSXXK.InsertUpdateFull(HangNhap.Ma);
            else
                nplSXXK.InsertUpdateFull("");
            this.Close();
        }
      
        private void NguyenPhuLieuEditForm_Load(object sender, EventArgs e)
        {
            revMaHS.ValidationExpression = @"\d{8,10}";

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            if (HangNhap != null)
            {
                txtMa.Text = HangNhap.Ma;
                txtMaHS.Text = HangNhap.MaHS;
                txtTen.Text = HangNhap.Ten;
                cbDonViTinh.SelectedValue = HangNhap.DVT_ID;
            }
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //    error.SetIconPadding(txtMaHS, -8);
            //    error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
            //}
            //else
            //{
            //    error.SetError(txtMaHS, string.Empty);
            //}            
        }

        private void txtMaHS_TextChanged(object sender, EventArgs e)
        {

        }
    }
}