﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface.KDT.GC
{
    partial class PhuLucHopDongTKCT
    {
        private UIGroupBox uiGroupBox1;
        private UIButton btnSave;
        private Label label3;
        private Label label4;
        private UIGroupBox uiGroupBox2;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhuLucHopDongTKCT));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHetHan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayPhuLuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoPhuLuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvSoHieu = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvNgayPL = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNgayHetHan = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHetHan)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(391, 156);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.btnSave);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(391, 156);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(311, 121);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ccNgayHetHan);
            this.uiGroupBox2.Controls.Add(this.ccNgayPhuLuc);
            this.uiGroupBox2.Controls.Add(this.txtSoPhuLuc);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(1, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(385, 107);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayHetHan
            // 
            // 
            // 
            // 
            this.ccNgayHetHan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHetHan.DropDownCalendar.Name = "";
            this.ccNgayHetHan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHetHan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHetHan.IsNullDate = true;
            this.ccNgayHetHan.Location = new System.Drawing.Point(161, 74);
            this.ccNgayHetHan.Name = "ccNgayHetHan";
            this.ccNgayHetHan.Nullable = true;
            this.ccNgayHetHan.NullButtonText = "Xóa";
            this.ccNgayHetHan.ShowNullButton = true;
            this.ccNgayHetHan.Size = new System.Drawing.Size(143, 21);
            this.ccNgayHetHan.TabIndex = 2;
            this.ccNgayHetHan.TodayButtonText = "Hôm nay";
            this.ccNgayHetHan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            // 
            // ccNgayPhuLuc
            // 
            // 
            // 
            // 
            this.ccNgayPhuLuc.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayPhuLuc.DropDownCalendar.Name = "";
            this.ccNgayPhuLuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayPhuLuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayPhuLuc.IsNullDate = true;
            this.ccNgayPhuLuc.Location = new System.Drawing.Point(161, 47);
            this.ccNgayPhuLuc.Name = "ccNgayPhuLuc";
            this.ccNgayPhuLuc.Nullable = true;
            this.ccNgayPhuLuc.NullButtonText = "Xóa";
            this.ccNgayPhuLuc.ShowNullButton = true;
            this.ccNgayPhuLuc.Size = new System.Drawing.Size(143, 21);
            this.ccNgayPhuLuc.TabIndex = 1;
            this.ccNgayPhuLuc.TodayButtonText = "Hôm nay";
            this.ccNgayPhuLuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayPhuLuc.VisualStyleManager = this.vsmMain;
            // 
            // txtSoPhuLuc
            // 
            this.txtSoPhuLuc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoPhuLuc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoPhuLuc.Location = new System.Drawing.Point(161, 20);
            this.txtSoPhuLuc.Name = "txtSoPhuLuc";
            this.txtSoPhuLuc.Size = new System.Drawing.Size(215, 21);
            this.txtSoPhuLuc.TabIndex = 0;
            this.txtSoPhuLuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoPhuLuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoPhuLuc.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ngày hết hạn";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Số phụ lục hợp đồng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ngày phụ lục hợp đồng";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(225, 121);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvSoHieu
            // 
            this.rfvSoHieu.ControlToValidate = this.txtSoPhuLuc;
            this.rfvSoHieu.ErrorMessage = "\"Số phụ lục\" bắt buộc phải nhập.";
            this.rfvSoHieu.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHieu.Icon")));
            this.rfvSoHieu.Tag = "rfvSoHieu";
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvNgayPL
            // 
            this.rfvNgayPL.ControlToValidate = this.ccNgayPhuLuc;
            this.rfvNgayPL.ErrorMessage = "\"Ngày phụ lục\" bắt buộc phải nhập.";
            this.rfvNgayPL.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayPL.Icon")));
            this.rfvNgayPL.Tag = "rfvSoHieu";
            // 
            // rfvNgayHetHan
            // 
            this.rfvNgayHetHan.ControlToValidate = this.ccNgayHetHan;
            this.rfvNgayHetHan.ErrorMessage = "\"Ngày hết hạn\" bắt buộc phải nhập.";
            this.rfvNgayHetHan.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayHetHan.Icon")));
            this.rfvNgayHetHan.Tag = "rfvSoHieu";
            // 
            // PhuLucHopDongTKCT
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(391, 156);
            this.Controls.Add(this.uiGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(399, 190);
            this.Name = "PhuLucHopDongTKCT";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin chứng từ";
            this.Load += new System.EventHandler(this.PhuLucHopDongTKCT_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHetHan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoHieu;
        private UIButton btnClose;
        private EditBox txtSoPhuLuc;
        private IContainer components;
        private ErrorProvider error;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayPhuLuc;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayHetHan;
        private Label label1;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayPL;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayHetHan;
    }
}
