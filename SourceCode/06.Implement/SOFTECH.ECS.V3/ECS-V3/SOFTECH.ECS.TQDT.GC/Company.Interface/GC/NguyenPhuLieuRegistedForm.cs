using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Company.Interface.GC
{
    public partial class NguyenPhuLieuRegistedForm : BaseForm
    {
        public NguyenPhuLieu NguyenPhuLieuSelected = new NguyenPhuLieu();
        public List<NguyenPhuLieu> NPLThanhKhoan = new List<NguyenPhuLieu>();
        public bool isBrower = false;
        public bool isChonNhieu = false;
        //2 hien da duyet nhung chua dieu chinh
        public int isDisplayAll = 0;//0 hien tat ca . 1 hien cac npl da duyet nhhung chua dieu chinh va bo sung .
        public bool khaiPKN06 = false;

        public NguyenPhuLieuRegistedForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            if (isDisplayAll == 0)
                dgList.DataSource = NguyenPhuLieuSelected.SelectCollectionBy_HopDong_ID();
            else if (isDisplayAll==1)
                dgList.DataSource = NguyenPhuLieuSelected.SelectCollectionDaDuyetAndBoSungBy_HopDong_ID();
            else if (isDisplayAll == 2)
                dgList.DataSource = NguyenPhuLieuSelected.SelectCollectionDaDuyetBy_HopDong_ID();
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];



            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void NguyenPhuLieuRegistedForm_Load(object sender, EventArgs e)
        {
            if (isChonNhieu)
            {
                btnChonNhieuHang.Visible = true;
            }
            else dgList.RootTable.Columns["Check"].Visible = false;
            this.khoitao_DuLieuChuan();
            if (khaiPKN06)
            {
                dgList.DataSource = NguyenPhuLieuSelected.DisplayNPLNotExistInTK(NguyenPhuLieuSelected.HopDong_ID);
            }
            else
                BindData();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------


        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    NguyenPhuLieuSelected = (NguyenPhuLieu)e.Row.DataRow;
                    this.Close();
                }
            }
            else
            {
                ;
            }
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChonNhieuHang_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            if (dgList.GetRows().Length < 0) return;
            if (items.Length <= 0) return;
            {
                foreach (GridEXRow i in items)
                {
                    if (i.IsChecked)
                    {
                        NguyenPhuLieu npl = (NguyenPhuLieu)i.DataRow;

                        NPLThanhKhoan.Add(npl);
                    }
                }
            }
            this.Close();
        }

        //-----------------------------------------------------------------------------------------
    }
}