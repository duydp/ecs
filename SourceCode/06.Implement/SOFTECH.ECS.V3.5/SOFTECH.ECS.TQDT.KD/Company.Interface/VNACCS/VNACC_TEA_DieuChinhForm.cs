﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TEA_DieuChinhForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_TEA TEA = new KDT_VNACCS_TEA();
        private KDT_VNACCS_TEADieuChinh DC = null;
        public VNACC_TEA_DieuChinhForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.TEA.ToString());
        }

        private void GetDieuChinh()
        {
            try
            {
                DC.LanDieuChinhGP_GCN = Convert.ToInt32(txtLanDieuChinhGP_GCN.Value);
                DC.ChungNhanDieuChinhSo = txtChungNhanDieuChinhSo.Text;
                DC.NgayChungNhanDieuChinh = clcNgayChungNhanDieuChinh.Value;
                DC.DieuChinhBoi = txtDieuChinhBoi.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void SetDieuChinh()
        {
            try
            {
                txtLanDieuChinhGP_GCN.Text = DC.LanDieuChinhGP_GCN.ToString();
                txtChungNhanDieuChinhSo.Text = DC.ChungNhanDieuChinhSo;
                clcNgayChungNhanDieuChinh.Value = DC.NgayChungNhanDieuChinh;
                txtDieuChinhBoi.Text = DC.DieuChinhBoi;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                bool isAddNew = false;
                if (DC == null)
                {
                    DC = new KDT_VNACCS_TEADieuChinh();
                    isAddNew = true;
                }
                GetDieuChinh();
                if (isAddNew)
                    TEA.DieuChinhCollection.Add(DC);
                grList.DataSource = TEA.DieuChinhCollection;
                grList.Refetch();
                DC = new KDT_VNACCS_TEADieuChinh();
                SetDieuChinh();
                DC = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void VNACC_TEA_DieuChinhForm_Load(object sender, EventArgs e)
        {
            try
            {
                SetIDControl();
                grList.DataSource = TEA.DieuChinhCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACCS_TEADieuChinh> ItemColl = new List<KDT_VNACCS_TEADieuChinh>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa lần điều chỉnh này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_TEADieuChinh)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_TEADieuChinh item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TEA.DieuChinhCollection.Remove(item);
                    }

                    grList.DataSource = TEA.DieuChinhCollection;
                    grList.Refetch();
                }
            }
            catch (Exception ex)
            {
                grList.Refresh();
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                txtLanDieuChinhGP_GCN.Tag = "IC_"; //Giấy phép đầu tư hoặc Giấy chứng nhận đầu tư điều chỉnh lần
                txtChungNhanDieuChinhSo.Tag = "CA_"; //Chứng nhận điều chỉnh số
                clcNgayChungNhanDieuChinh.TagName = "DA_"; //Ngày chứng nhận điều chỉnh
                txtDieuChinhBoi.Tag = "AI_"; //Điều chỉnh bởi


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

    }
}
