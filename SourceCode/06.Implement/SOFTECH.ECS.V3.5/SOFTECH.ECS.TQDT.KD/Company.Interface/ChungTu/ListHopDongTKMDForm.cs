﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif


namespace Company.Interface
{
    public partial class ListHopDongTKMDForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListHopDongTKMDForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NguyenTe_ID"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value.ToString());
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            if (TKMD.ID > 0)
            {
                string query;

                if (isKhaiBoSung)
                    query = string.Format("TKMD_ID = {0} AND LOAIKB = '1'", TKMD.ID);
                else
                    query = string.Format("TKMD_ID = {0} AND LOAIKB = '0'", TKMD.ID);

                dgList.DataSource = HopDongThuongMai.SelectCollectionDynamic(query, null);
            }
            else
            {
                dgList.DataSource = TKMD.HopDongThuongMaiCollection;
            }
            if (!isKhaiBoSung)
            {
                if (TKMD.TrangThaiXuLy == -1 || TKMD.TrangThaiXuLy == 5)
                    btnXoa.Enabled = true;
                else
                    btnXoa.Enabled = false;
            }
            else
                btnXoa.Enabled = true;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                //btnTaoMoi.Enabled = false;
                //btnXoa.Enabled = false;
            }
        }




        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HopDongThuongMai> listHopDong = new List<HopDongThuongMai>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongThuongMai hd = (HopDongThuongMai)i.GetRow().DataRow;
                        if (hd.TrangThai != 1 || hd.TrangThai != 0)
                            listHopDong.Add(hd);
                    }
                }
            }

            foreach (HopDongThuongMai hd in listHopDong)
            {
                //if (hd.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || hd.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                if (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai.KiemTraChungTuCoBoSung(listHopDong) == false) //Update by Hungtq 06/04/2013
                {
                    hd.DeleteAll();
                    TKMD.HopDongThuongMaiCollection.Remove(hd);
                }
                else
                {
                    this.ShowMessage(string.Format("Hợp đồng thương mại bổ sung số : {0} đã được gửi đến hải quan. Không thể xóa", hd.SoHopDongTM), false);
                }

            }

            dgList.DataSource = TKMD.HopDongThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }


        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            HopDongForm f = new HopDongForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog(this);
            SelectHangTriGiaForm_Load(null, null);

//            dgList.DataSource = TKMD.HopDongThuongMaiCollection;
  //          try { dgList.Refetch(); }
    //        catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            HopDongForm f = new HopDongForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.HopDongTM = (HopDongThuongMai)e.Row.DataRow;
            f.ShowDialog(this);
            SelectHangTriGiaForm_Load(null, null);
            //dgList.DataSource = TKMD.HopDongThuongMaiCollection;
            //try { dgList.Refetch(); }
            //catch { dgList.Refresh(); }
        }




    }
}
