﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Janus.Windows.CalendarCombo;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX.EditControls;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;

namespace Company.Interface.GC
{
    partial class HopDongRegistedDetailForm
    {
        private UIGroupBox uiGroupBox1;
        private ImageList ImageList1;
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HopDongRegistedDetailForm));
            Janus.Windows.GridEX.GridEXLayout dgThongTinHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgNguyenPhuLieu_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgThietBi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgPhuKien_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKCT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKCTXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgNPLCungUng_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgThanhKhoanMoi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgThanhKhoan_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnTinhLuongTon = new Janus.Windows.EditControls.UIButton();
            this.lblGhiChu = new System.Windows.Forms.Label();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnNXoa = new Janus.Windows.EditControls.UIButton();
            this.btnNThem = new Janus.Windows.EditControls.UIButton();
            this.tabHopDong = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage8 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgThongTinHopDong = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTrangThaiThanhKhoan = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nguyenTeControl1 = new Company.Interface.Controls.NguyenTeControl();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ccNgayGiaHan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.nuocThue = new Company.Interface.Controls.NuocHControl();
            this.label1 = new System.Windows.Forms.Label();
            this.ccNgayKetThucHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayKyHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tpNguyenPhuLieu = new Janus.Windows.UI.Tab.UITabPage();
            this.btnXuatExcelNPL = new Janus.Windows.EditControls.UIButton();
            this.filterEditor1 = new Janus.Windows.FilterEditor.FilterEditor();
            this.dgNguyenPhuLieu = new Janus.Windows.GridEX.GridEX();
            this.ctmNPL = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemTKN = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKTXNPL = new System.Windows.Forms.ToolStripMenuItem();
            this.xemPKChuaNPL = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKCTNPLItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKXuatSPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnInNPL = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKCTNPL = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKTXNPL = new Janus.Windows.EditControls.UIButton();
            this.btnXemPKNPL = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKNNPL = new Janus.Windows.EditControls.UIButton();
            this.tpSanPham = new Janus.Windows.UI.Tab.UITabPage();
            this.btnXuatExcelSanPham = new Janus.Windows.EditControls.UIButton();
            this.filterEditor2 = new Janus.Windows.FilterEditor.FilterEditor();
            this.dgSanPham = new Janus.Windows.GridEX.GridEX();
            this.ctmSP = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemTKXSP = new System.Windows.Forms.ToolStripMenuItem();
            this.xemPKSP = new System.Windows.Forms.ToolStripMenuItem();
            this.xemDMItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKCTSPItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inDMSPItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnInSP = new Janus.Windows.EditControls.UIButton();
            this.btnInDMSP = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKCTSP = new Janus.Windows.EditControls.UIButton();
            this.btnXemPKSP = new Janus.Windows.EditControls.UIButton();
            this.btnXemDMSP = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKXSP = new Janus.Windows.EditControls.UIButton();
            this.tpThietBi = new Janus.Windows.UI.Tab.UITabPage();
            this.btnXemTKXTB = new Janus.Windows.EditControls.UIButton();
            this.btnXemPKTB = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKNTB = new Janus.Windows.EditControls.UIButton();
            this.dgThietBi = new Janus.Windows.GridEX.GridEX();
            this.ctmTB = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemTKNTB = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKXTB = new System.Windows.Forms.ToolStripMenuItem();
            this.xemPKTB = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTKN = new Janus.Windows.GridEX.GridEX();
            this.ctmTKN = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemChiTietTKN = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.btnXemLuongNPLXuatTK = new Janus.Windows.EditControls.UIButton();
            this.btnXemLuongNPLCungUngTK = new Janus.Windows.EditControls.UIButton();
            this.btnXemChiTietTK = new Janus.Windows.EditControls.UIButton();
            this.dgTKX = new Janus.Windows.GridEX.GridEX();
            this.ctmTKX = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemChiTietTKX = new System.Windows.Forms.ToolStripMenuItem();
            this.xemLuongNPLTKX = new System.Windows.Forms.ToolStripMenuItem();
            this.xemNPLCungUngItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgPhuKien = new Janus.Windows.GridEX.GridEX();
            this.ctmPK = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemChiTietPK = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTKCT = new Janus.Windows.GridEX.GridEX();
            this.ctmTKCT = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTabPage7 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTKCTXuat = new Janus.Windows.GridEX.GridEX();
            this.ctmTKCTXuat = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tpNPLCU = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNPLCungUng = new Janus.Windows.GridEX.GridEX();
            this.tpThanhKhoanMoi = new Janus.Windows.UI.Tab.UITabPage();
            this.dgThanhKhoanMoi = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage6 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgThanhKhoan = new Janus.Windows.GridEX.GridEX();
            this.uiCommandManager1 = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThanhKhoan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoan");
            this.cmdXuLyHD1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuLyHD");
            this.cmdLayToKhaiNhapSXXK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLayToKhaiNhapSXXK");
            this.LuuThongTin1 = new Janus.Windows.UI.CommandBars.UICommand("LuuThongTin");
            this.cmdThanhKhoan = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoan");
            this.cmdXuLyHD = new Janus.Windows.UI.CommandBars.UICommand("cmdXuLyHD");
            this.cmdLayToKhaiNhapSXXK = new Janus.Windows.UI.CommandBars.UICommand("cmdLayToKhaiNhapSXXK");
            this.cmdXuatExcelDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatExcelDinhMuc");
            this.LuuThongTin = new Janus.Windows.UI.CommandBars.UICommand("LuuThongTin");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.gridEXPrintNPL = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXPrintSP = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXExporterSP = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.gridEXExporterNPL = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).BeginInit();
            this.tabHopDong.SuspendLayout();
            this.uiTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThongTinHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.tpNguyenPhuLieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).BeginInit();
            this.ctmNPL.SuspendLayout();
            this.tpSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).BeginInit();
            this.ctmSP.SuspendLayout();
            this.tpThietBi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).BeginInit();
            this.ctmTB.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKN)).BeginInit();
            this.ctmTKN.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).BeginInit();
            this.ctmTKX.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPhuKien)).BeginInit();
            this.ctmPK.SuspendLayout();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKCT)).BeginInit();
            this.ctmTKCT.SuspendLayout();
            this.uiTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKCTXuat)).BeginInit();
            this.ctmTKCTXuat.SuspendLayout();
            this.tpNPLCU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCungUng)).BeginInit();
            this.tpThanhKhoanMoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoanMoi)).BeginInit();
            this.uiTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(1024, 409);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnTinhLuongTon);
            this.uiGroupBox1.Controls.Add(this.lblGhiChu);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.btnNXoa);
            this.uiGroupBox1.Controls.Add(this.btnNThem);
            this.uiGroupBox1.Controls.Add(this.tabHopDong);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 28);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1024, 409);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnTinhLuongTon
            // 
            this.btnTinhLuongTon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTinhLuongTon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTinhLuongTon.Icon = ((System.Drawing.Icon)(resources.GetObject("btnTinhLuongTon.Icon")));
            this.btnTinhLuongTon.Location = new System.Drawing.Point(5, 382);
            this.btnTinhLuongTon.Name = "btnTinhLuongTon";
            this.btnTinhLuongTon.Size = new System.Drawing.Size(190, 23);
            this.btnTinhLuongTon.TabIndex = 0;
            this.btnTinhLuongTon.Text = "Tính lượng tồn nhu cầu";
            this.btnTinhLuongTon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnTinhLuongTon.VisualStyleManager = this.vsmMain;
            this.btnTinhLuongTon.Click += new System.EventHandler(this.btnTinhLuongTon_Click);
            // 
            // lblGhiChu
            // 
            this.lblGhiChu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblGhiChu.AutoSize = true;
            this.lblGhiChu.BackColor = System.Drawing.Color.Transparent;
            this.lblGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChu.ForeColor = System.Drawing.Color.Red;
            this.lblGhiChu.Location = new System.Drawing.Point(3, 388);
            this.lblGhiChu.Name = "lblGhiChu";
            this.lblGhiChu.Size = new System.Drawing.Size(466, 13);
            this.lblGhiChu.TabIndex = 1;
            this.lblGhiChu.Text = "Các dòng màu đỏ là các sản phẩm có định mức chưa duyệt hoặc chưa có định mức";
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(877, 383);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(70, 23);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnNXoa
            // 
            this.btnNXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnNXoa.Icon")));
            this.btnNXoa.Location = new System.Drawing.Point(953, 383);
            this.btnNXoa.Name = "btnNXoa";
            this.btnNXoa.Size = new System.Drawing.Size(70, 23);
            this.btnNXoa.TabIndex = 4;
            this.btnNXoa.Text = "Đóng";
            this.btnNXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnNXoa.VisualStyleManager = this.vsmMain;
            this.btnNXoa.Click += new System.EventHandler(this.btnNXoa_Click);
            // 
            // btnNThem
            // 
            this.btnNThem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNThem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNThem.Icon = ((System.Drawing.Icon)(resources.GetObject("btnNThem.Icon")));
            this.btnNThem.Location = new System.Drawing.Point(801, 383);
            this.btnNThem.Name = "btnNThem";
            this.btnNThem.Size = new System.Drawing.Size(70, 23);
            this.btnNThem.TabIndex = 2;
            this.btnNThem.Text = "Thêm";
            this.btnNThem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnNThem.VisualStyleManager = this.vsmMain;
            this.btnNThem.Click += new System.EventHandler(this.btnNThem_Click_1);
            // 
            // tabHopDong
            // 
            this.tabHopDong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabHopDong.BackColor = System.Drawing.Color.Transparent;
            this.tabHopDong.Location = new System.Drawing.Point(0, 1);
            this.tabHopDong.Name = "tabHopDong";
            this.tabHopDong.Size = new System.Drawing.Size(1024, 377);
            this.tabHopDong.TabIndex = 0;
            this.tabHopDong.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage8,
            this.tpNguyenPhuLieu,
            this.tpSanPham,
            this.tpThietBi,
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage4,
            this.uiTabPage7,
            this.tpNPLCU,
            this.tpThanhKhoanMoi,
            this.uiTabPage6});
            this.tabHopDong.TabsStateStyles.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.tabHopDong.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            this.tabHopDong.VisualStyleManager = this.vsmMain;
            this.tabHopDong.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.tabHopDong_SelectedTabChanged);
            // 
            // uiTabPage8
            // 
            this.uiTabPage8.Controls.Add(this.uiGroupBox3);
            this.uiTabPage8.Controls.Add(this.uiGroupBox4);
            this.uiTabPage8.Key = "tpThongTinHopDong";
            this.uiTabPage8.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage8.Name = "uiTabPage8";
            this.uiTabPage8.Size = new System.Drawing.Size(1022, 355);
            this.uiTabPage8.TabStop = true;
            this.uiTabPage8.Text = "Thông tin hợp đồng";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgThongTinHopDong);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(11, 173);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1005, 179);
            this.uiGroupBox3.TabIndex = 13;
            this.uiGroupBox3.Text = "Thông tin chi tiết";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgThongTinHopDong
            // 
            this.dgThongTinHopDong.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThongTinHopDong.AlternatingColors = true;
            this.dgThongTinHopDong.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThongTinHopDong.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThongTinHopDong.ColumnAutoResize = true;
            dgThongTinHopDong_DesignTimeLayout.LayoutString = resources.GetString("dgThongTinHopDong_DesignTimeLayout.LayoutString");
            this.dgThongTinHopDong.DesignTimeLayout = dgThongTinHopDong_DesignTimeLayout;
            this.dgThongTinHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThongTinHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThongTinHopDong.GroupByBoxVisible = false;
            this.dgThongTinHopDong.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThongTinHopDong.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThongTinHopDong.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThongTinHopDong.ImageList = this.ImageList1;
            this.dgThongTinHopDong.Location = new System.Drawing.Point(3, 17);
            this.dgThongTinHopDong.Name = "dgThongTinHopDong";
            this.dgThongTinHopDong.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgThongTinHopDong.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThongTinHopDong.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgThongTinHopDong.Size = new System.Drawing.Size(999, 159);
            this.dgThongTinHopDong.TabIndex = 0;
            this.dgThongTinHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThongTinHopDong.VisualStyleManager = this.vsmMain;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtDiaChi);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtTenDoiTac);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.lblTrangThaiThanhKhoan);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.nguyenTeControl1);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox4.Controls.Add(this.ccNgayGiaHan);
            this.uiGroupBox4.Controls.Add(this.nuocThue);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.ccNgayKetThucHD);
            this.uiGroupBox4.Controls.Add(this.ccNgayKyHD);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(11, 7);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1005, 160);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Thông tin chung";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(430, 94);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(196, 21);
            this.txtDiaChi.TabIndex = 16;
            this.txtDiaChi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(350, 99);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Địa chỉ đối tác";
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoiTac.Location = new System.Drawing.Point(148, 94);
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(162, 21);
            this.txtTenDoiTac.TabIndex = 14;
            this.txtTenDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDoiTac.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(28, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Tên đối tác";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(26, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Trạng thái thanh khoản";
            // 
            // lblTrangThaiThanhKhoan
            // 
            this.lblTrangThaiThanhKhoan.AutoSize = true;
            this.lblTrangThaiThanhKhoan.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThaiThanhKhoan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThaiThanhKhoan.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThaiThanhKhoan.Location = new System.Drawing.Point(145, 130);
            this.lblTrangThaiThanhKhoan.Name = "lblTrangThaiThanhKhoan";
            this.lblTrangThaiThanhKhoan.Size = new System.Drawing.Size(110, 13);
            this.lblTrangThaiThanhKhoan.TabIndex = 12;
            this.lblTrangThaiThanhKhoan.Text = "Chưa thanh khoản";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Ngày kết thúc";
            // 
            // nguyenTeControl1
            // 
            this.nguyenTeControl1.BackColor = System.Drawing.Color.Transparent;
            this.nguyenTeControl1.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.nguyenTeControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nguyenTeControl1.Location = new System.Drawing.Point(430, 69);
            this.nguyenTeControl1.Ma = "";
            this.nguyenTeControl1.Name = "nguyenTeControl1";
            this.nguyenTeControl1.ReadOnly = false;
            this.nguyenTeControl1.Size = new System.Drawing.Size(211, 22);
            this.nguyenTeControl1.TabIndex = 10;
            this.nguyenTeControl1.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(28, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Ngày gia hạn";
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(148, 16);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(162, 21);
            this.txtSoHopDong.TabIndex = 0;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayGiaHan
            // 
            // 
            // 
            // 
            this.ccNgayGiaHan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayGiaHan.DropDownCalendar.Name = "";
            this.ccNgayGiaHan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiaHan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayGiaHan.IsNullDate = true;
            this.ccNgayGiaHan.Location = new System.Drawing.Point(148, 67);
            this.ccNgayGiaHan.Name = "ccNgayGiaHan";
            this.ccNgayGiaHan.Nullable = true;
            this.ccNgayGiaHan.NullButtonText = "Xóa";
            this.ccNgayGiaHan.ShowDropDown = false;
            this.ccNgayGiaHan.ShowNullButton = true;
            this.ccNgayGiaHan.Size = new System.Drawing.Size(95, 21);
            this.ccNgayGiaHan.TabIndex = 8;
            this.ccNgayGiaHan.TodayButtonText = "Hôm nay";
            this.ccNgayGiaHan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiaHan.VisualStyleManager = this.vsmMain;
            // 
            // nuocThue
            // 
            this.nuocThue.BackColor = System.Drawing.Color.Transparent;
            this.nuocThue.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.nuocThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nuocThue.Location = new System.Drawing.Point(430, 41);
            this.nuocThue.Ma = "";
            this.nuocThue.Name = "nuocThue";
            this.nuocThue.ReadOnly = false;
            this.nuocThue.Size = new System.Drawing.Size(211, 22);
            this.nuocThue.TabIndex = 6;
            this.nuocThue.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số hợp đồng";
            // 
            // ccNgayKetThucHD
            // 
            // 
            // 
            // 
            this.ccNgayKetThucHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayKetThucHD.DropDownCalendar.Name = "";
            this.ccNgayKetThucHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKetThucHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKetThucHD.IsNullDate = true;
            this.ccNgayKetThucHD.Location = new System.Drawing.Point(148, 42);
            this.ccNgayKetThucHD.Name = "ccNgayKetThucHD";
            this.ccNgayKetThucHD.Nullable = true;
            this.ccNgayKetThucHD.NullButtonText = "Xóa";
            this.ccNgayKetThucHD.ShowDropDown = false;
            this.ccNgayKetThucHD.ShowNullButton = true;
            this.ccNgayKetThucHD.Size = new System.Drawing.Size(95, 21);
            this.ccNgayKetThucHD.TabIndex = 4;
            this.ccNgayKetThucHD.TodayButtonText = "Hôm nay";
            this.ccNgayKetThucHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKetThucHD.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayKyHD
            // 
            // 
            // 
            // 
            this.ccNgayKyHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayKyHD.DropDownCalendar.Name = "";
            this.ccNgayKyHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKyHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKyHD.IsNullDate = true;
            this.ccNgayKyHD.Location = new System.Drawing.Point(430, 14);
            this.ccNgayKyHD.Name = "ccNgayKyHD";
            this.ccNgayKyHD.Nullable = true;
            this.ccNgayKyHD.NullButtonText = "Xóa";
            this.ccNgayKyHD.ShowDropDown = false;
            this.ccNgayKyHD.ShowNullButton = true;
            this.ccNgayKyHD.Size = new System.Drawing.Size(99, 21);
            this.ccNgayKyHD.TabIndex = 2;
            this.ccNgayKyHD.TodayButtonText = "Hôm nay";
            this.ccNgayKyHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKyHD.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(320, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Nguyên tệ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(320, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Nước thuê";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(320, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngày ký";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tpNguyenPhuLieu
            // 
            this.tpNguyenPhuLieu.Controls.Add(this.btnXuatExcelNPL);
            this.tpNguyenPhuLieu.Controls.Add(this.filterEditor1);
            this.tpNguyenPhuLieu.Controls.Add(this.uiButton1);
            this.tpNguyenPhuLieu.Controls.Add(this.btnInNPL);
            this.tpNguyenPhuLieu.Controls.Add(this.btnXemTKCTNPL);
            this.tpNguyenPhuLieu.Controls.Add(this.btnXemTKTXNPL);
            this.tpNguyenPhuLieu.Controls.Add(this.btnXemPKNPL);
            this.tpNguyenPhuLieu.Controls.Add(this.btnXemTKNNPL);
            this.tpNguyenPhuLieu.Controls.Add(this.dgNguyenPhuLieu);
            this.tpNguyenPhuLieu.Key = "tpNPL";
            this.tpNguyenPhuLieu.Location = new System.Drawing.Point(1, 21);
            this.tpNguyenPhuLieu.Name = "tpNguyenPhuLieu";
            this.tpNguyenPhuLieu.Size = new System.Drawing.Size(974, 349);
            this.tpNguyenPhuLieu.TabStop = true;
            this.tpNguyenPhuLieu.Text = "Nguyên phụ liệu";
            // 
            // btnXuatExcelNPL
            // 
            this.btnXuatExcelNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcelNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXuatExcelNPL.Icon")));
            this.btnXuatExcelNPL.Location = new System.Drawing.Point(602, 45);
            this.btnXuatExcelNPL.Name = "btnXuatExcelNPL";
            this.btnXuatExcelNPL.Size = new System.Drawing.Size(116, 23);
            this.btnXuatExcelNPL.TabIndex = 278;
            this.btnXuatExcelNPL.Text = "Xuất Excel";
            this.btnXuatExcelNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXuatExcelNPL.VisualStyleManager = this.vsmMain;
            this.btnXuatExcelNPL.Click += new System.EventHandler(this.btnXuatExcelNPL_Click);
            // 
            // filterEditor1
            // 
            this.filterEditor1.AutoApply = true;
            this.filterEditor1.BackColor = System.Drawing.Color.Transparent;
            this.filterEditor1.DefaultConditionOperator = Janus.Data.ConditionOperator.Equal;
            this.filterEditor1.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.UseFormatStyle;
            this.filterEditor1.Location = new System.Drawing.Point(1, 36);
            this.filterEditor1.MinSize = new System.Drawing.Size(394, 44);
            this.filterEditor1.Name = "filterEditor1";
            this.filterEditor1.Office2007ColorScheme = Janus.Windows.Common.Office2007ColorScheme.Default;
            this.filterEditor1.ScrollMode = Janus.Windows.UI.Dock.ScrollMode.Both;
            this.filterEditor1.ScrollStep = 15;
            this.filterEditor1.Size = new System.Drawing.Size(595, 45);
            this.filterEditor1.SourceControl = this.dgNguyenPhuLieu;
            // 
            // dgNguyenPhuLieu
            // 
            this.dgNguyenPhuLieu.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNguyenPhuLieu.AlternatingColors = true;
            this.dgNguyenPhuLieu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgNguyenPhuLieu.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNguyenPhuLieu.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNguyenPhuLieu.ContextMenuStrip = this.ctmNPL;
            dgNguyenPhuLieu_DesignTimeLayout.LayoutString = resources.GetString("dgNguyenPhuLieu_DesignTimeLayout.LayoutString");
            this.dgNguyenPhuLieu.DesignTimeLayout = dgNguyenPhuLieu_DesignTimeLayout;
            this.dgNguyenPhuLieu.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNguyenPhuLieu.FrozenColumns = 3;
            this.dgNguyenPhuLieu.GroupByBoxVisible = false;
            this.dgNguyenPhuLieu.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNguyenPhuLieu.ImageList = this.ImageList1;
            this.dgNguyenPhuLieu.Location = new System.Drawing.Point(0, 87);
            this.dgNguyenPhuLieu.Name = "dgNguyenPhuLieu";
            this.dgNguyenPhuLieu.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNguyenPhuLieu.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNguyenPhuLieu.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNguyenPhuLieu.Size = new System.Drawing.Size(974, 262);
            this.dgNguyenPhuLieu.TabIndex = 20;
            this.dgNguyenPhuLieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNguyenPhuLieu.VisualStyleManager = this.vsmMain;
            this.dgNguyenPhuLieu.UpdatingCell += new Janus.Windows.GridEX.UpdatingCellEventHandler(this.dgNguyenPhuLieu_UpdatingCell);
            this.dgNguyenPhuLieu.RecordUpdated += new System.EventHandler(this.dgNguyenPhuLieu_RecordUpdated);
            this.dgNguyenPhuLieu.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgNguyenPhuLieu_LoadingRow);
            this.dgNguyenPhuLieu.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgNguyenPhuLieu_FormattingRow);
            // 
            // ctmNPL
            // 
            this.ctmNPL.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemTKN,
            this.xemTKTXNPL,
            this.xemPKChuaNPL,
            this.xemTKCTNPLItem,
            this.xemTKXuatSPToolStripMenuItem});
            this.ctmNPL.Name = "ctmNPL";
            this.ctmNPL.Size = new System.Drawing.Size(232, 114);
            // 
            // xemTKN
            // 
            this.xemTKN.Name = "xemTKN";
            this.xemTKN.Size = new System.Drawing.Size(231, 22);
            this.xemTKN.Text = "Xem tờ khai nhập NPL";
            this.xemTKN.Click += new System.EventHandler(this.xemTKN_Click);
            // 
            // xemTKTXNPL
            // 
            this.xemTKTXNPL.Name = "xemTKTXNPL";
            this.xemTKTXNPL.Size = new System.Drawing.Size(231, 22);
            this.xemTKTXNPL.Text = "Xem tờ khai tái xuất NPL";
            this.xemTKTXNPL.Click += new System.EventHandler(this.xemTKTXNPL_Click);
            // 
            // xemPKChuaNPL
            // 
            this.xemPKChuaNPL.Name = "xemPKChuaNPL";
            this.xemPKChuaNPL.Size = new System.Drawing.Size(231, 22);
            this.xemPKChuaNPL.Text = "Xem phụ kiện liên quan đến NPL";
            this.xemPKChuaNPL.Click += new System.EventHandler(this.xemPKChuaNPL_Click);
            // 
            // xemTKCTNPLItem
            // 
            this.xemTKCTNPLItem.Name = "xemTKCTNPLItem";
            this.xemTKCTNPLItem.Size = new System.Drawing.Size(231, 22);
            this.xemTKCTNPLItem.Text = "Xem TK chuyển tiếp NPL";
            this.xemTKCTNPLItem.Click += new System.EventHandler(this.xemTKCTNPLItem_Click);
            // 
            // xemTKXuatSPToolStripMenuItem
            // 
            this.xemTKXuatSPToolStripMenuItem.Name = "xemTKXuatSPToolStripMenuItem";
            this.xemTKXuatSPToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.xemTKXuatSPToolStripMenuItem.Text = "Xem TK xuất SP";
            this.xemTKXuatSPToolStripMenuItem.Click += new System.EventHandler(this.xemTKXuatSPToolStripMenuItem_Click);
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(785, 10);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(138, 23);
            this.uiButton1.TabIndex = 277;
            this.uiButton1.Text = "Tính lượng nhu cầu";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // btnInNPL
            // 
            this.btnInNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnInNPL.Icon")));
            this.btnInNPL.Location = new System.Drawing.Point(729, 45);
            this.btnInNPL.Name = "btnInNPL";
            this.btnInNPL.Size = new System.Drawing.Size(116, 23);
            this.btnInNPL.TabIndex = 276;
            this.btnInNPL.Text = "In NPL";
            this.btnInNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnInNPL.VisualStyleManager = this.vsmMain;
            this.btnInNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemTKCTNPL
            // 
            this.btnXemTKCTNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKCTNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemTKCTNPL.Icon")));
            this.btnXemTKCTNPL.Location = new System.Drawing.Point(373, 10);
            this.btnXemTKCTNPL.Name = "btnXemTKCTNPL";
            this.btnXemTKCTNPL.Size = new System.Drawing.Size(230, 23);
            this.btnXemTKCTNPL.TabIndex = 270;
            this.btnXemTKCTNPL.Text = "Xem tờ khai Chuyển tiếp NPL";
            this.btnXemTKCTNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemTKCTNPL.VisualStyleManager = this.vsmMain;
            this.btnXemTKCTNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemTKTXNPL
            // 
            this.btnXemTKTXNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKTXNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemTKTXNPL.Icon")));
            this.btnXemTKTXNPL.Location = new System.Drawing.Point(177, 10);
            this.btnXemTKTXNPL.Name = "btnXemTKTXNPL";
            this.btnXemTKTXNPL.Size = new System.Drawing.Size(190, 23);
            this.btnXemTKTXNPL.TabIndex = 269;
            this.btnXemTKTXNPL.Text = "Xem tờ khai Tái Xuất NPL";
            this.btnXemTKTXNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemTKTXNPL.VisualStyleManager = this.vsmMain;
            this.btnXemTKTXNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemPKNPL
            // 
            this.btnXemPKNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemPKNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemPKNPL.Icon")));
            this.btnXemPKNPL.Location = new System.Drawing.Point(609, 10);
            this.btnXemPKNPL.Name = "btnXemPKNPL";
            this.btnXemPKNPL.Size = new System.Drawing.Size(170, 23);
            this.btnXemPKNPL.TabIndex = 268;
            this.btnXemPKNPL.Text = "Xem PK liên quan NPL";
            this.btnXemPKNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemPKNPL.VisualStyleManager = this.vsmMain;
            this.btnXemPKNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemTKNNPL
            // 
            this.btnXemTKNNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKNNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemTKNNPL.Icon")));
            this.btnXemTKNNPL.Location = new System.Drawing.Point(4, 10);
            this.btnXemTKNNPL.Name = "btnXemTKNNPL";
            this.btnXemTKNNPL.Size = new System.Drawing.Size(167, 23);
            this.btnXemTKNNPL.TabIndex = 267;
            this.btnXemTKNNPL.Text = "Xem tờ khai Nhập NPL";
            this.btnXemTKNNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemTKNNPL.VisualStyleManager = this.vsmMain;
            this.btnXemTKNNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // tpSanPham
            // 
            this.tpSanPham.Controls.Add(this.btnXuatExcelSanPham);
            this.tpSanPham.Controls.Add(this.filterEditor2);
            this.tpSanPham.Controls.Add(this.btnInSP);
            this.tpSanPham.Controls.Add(this.btnInDMSP);
            this.tpSanPham.Controls.Add(this.btnXemTKCTSP);
            this.tpSanPham.Controls.Add(this.btnXemPKSP);
            this.tpSanPham.Controls.Add(this.btnXemDMSP);
            this.tpSanPham.Controls.Add(this.btnXemTKXSP);
            this.tpSanPham.Controls.Add(this.dgSanPham);
            this.tpSanPham.Key = "tpSP";
            this.tpSanPham.Location = new System.Drawing.Point(1, 21);
            this.tpSanPham.Name = "tpSanPham";
            this.tpSanPham.Size = new System.Drawing.Size(974, 349);
            this.tpSanPham.TabStop = true;
            this.tpSanPham.Text = "Sản phẩm";
            // 
            // btnXuatExcelSanPham
            // 
            this.btnXuatExcelSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcelSanPham.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXuatExcelSanPham.Icon")));
            this.btnXuatExcelSanPham.Location = new System.Drawing.Point(592, 48);
            this.btnXuatExcelSanPham.Name = "btnXuatExcelSanPham";
            this.btnXuatExcelSanPham.Size = new System.Drawing.Size(116, 23);
            this.btnXuatExcelSanPham.TabIndex = 276;
            this.btnXuatExcelSanPham.Text = "Xuất Excel";
            this.btnXuatExcelSanPham.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXuatExcelSanPham.VisualStyleManager = this.vsmMain;
            this.btnXuatExcelSanPham.Click += new System.EventHandler(this.btnXuatExcelSanPham_Click);
            // 
            // filterEditor2
            // 
            this.filterEditor2.AutoApply = true;
            this.filterEditor2.BackColor = System.Drawing.Color.Transparent;
            this.filterEditor2.DefaultConditionOperator = Janus.Data.ConditionOperator.Equal;
            this.filterEditor2.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.UseFormatStyle;
            this.filterEditor2.Location = new System.Drawing.Point(2, 36);
            this.filterEditor2.MinSize = new System.Drawing.Size(419, 44);
            this.filterEditor2.Name = "filterEditor2";
            this.filterEditor2.Office2007ColorScheme = Janus.Windows.Common.Office2007ColorScheme.Default;
            this.filterEditor2.ScrollMode = Janus.Windows.UI.Dock.ScrollMode.Both;
            this.filterEditor2.ScrollStep = 15;
            this.filterEditor2.Size = new System.Drawing.Size(595, 56);
            this.filterEditor2.SourceControl = this.dgSanPham;
            // 
            // dgSanPham
            // 
            this.dgSanPham.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgSanPham.AlternatingColors = true;
            this.dgSanPham.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSanPham.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgSanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgSanPham.ColumnAutoResize = true;
            this.dgSanPham.ContextMenuStrip = this.ctmSP;
            dgSanPham_DesignTimeLayout.LayoutString = resources.GetString("dgSanPham_DesignTimeLayout.LayoutString");
            this.dgSanPham.DesignTimeLayout = dgSanPham_DesignTimeLayout;
            this.dgSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgSanPham.GroupByBoxVisible = false;
            this.dgSanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgSanPham.ImageList = this.ImageList1;
            this.dgSanPham.Location = new System.Drawing.Point(0, 98);
            this.dgSanPham.Name = "dgSanPham";
            this.dgSanPham.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgSanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgSanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgSanPham.Size = new System.Drawing.Size(974, 251);
            this.dgSanPham.TabIndex = 20;
            this.dgSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgSanPham.VisualStyleManager = this.vsmMain;
            this.dgSanPham.UpdatingCell += new Janus.Windows.GridEX.UpdatingCellEventHandler(this.dgSanPham_UpdatingCell);
            this.dgSanPham.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgSanPham_LoadingRow);
            // 
            // ctmSP
            // 
            this.ctmSP.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemTKXSP,
            this.xemPKSP,
            this.xemDMItem,
            this.xemTKCTSPItem,
            this.inDMSPItem});
            this.ctmSP.Name = "ctmSP";
            this.ctmSP.Size = new System.Drawing.Size(225, 114);
            // 
            // xemTKXSP
            // 
            this.xemTKXSP.Name = "xemTKXSP";
            this.xemTKXSP.Size = new System.Drawing.Size(224, 22);
            this.xemTKXSP.Text = "Xem tờ khai xuất SP";
            this.xemTKXSP.Click += new System.EventHandler(this.xemTKXSP_Click);
            // 
            // xemPKSP
            // 
            this.xemPKSP.Name = "xemPKSP";
            this.xemPKSP.Size = new System.Drawing.Size(224, 22);
            this.xemPKSP.Text = "Xem phụ kiện liên quan đến SP";
            this.xemPKSP.Click += new System.EventHandler(this.xemPKSP_Click);
            // 
            // xemDMItem
            // 
            this.xemDMItem.Name = "xemDMItem";
            this.xemDMItem.Size = new System.Drawing.Size(224, 22);
            this.xemDMItem.Text = "Xem định mức SP";
            this.xemDMItem.Click += new System.EventHandler(this.xemDMItem_Click);
            // 
            // xemTKCTSPItem
            // 
            this.xemTKCTSPItem.Name = "xemTKCTSPItem";
            this.xemTKCTSPItem.Size = new System.Drawing.Size(224, 22);
            this.xemTKCTSPItem.Text = "Xem TK chuyển tiếp SP";
            this.xemTKCTSPItem.Click += new System.EventHandler(this.xemTKCTSPItem_Click);
            // 
            // inDMSPItem
            // 
            this.inDMSPItem.Name = "inDMSPItem";
            this.inDMSPItem.Size = new System.Drawing.Size(224, 22);
            this.inDMSPItem.Text = "In định mức SP";
            this.inDMSPItem.Click += new System.EventHandler(this.inDMSPItem_Click);
            // 
            // btnInSP
            // 
            this.btnInSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInSP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnInSP.Icon")));
            this.btnInSP.Location = new System.Drawing.Point(786, 10);
            this.btnInSP.Name = "btnInSP";
            this.btnInSP.Size = new System.Drawing.Size(93, 23);
            this.btnInSP.TabIndex = 275;
            this.btnInSP.Text = "In SP";
            this.btnInSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnInSP.VisualStyleManager = this.vsmMain;
            this.btnInSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnInDMSP
            // 
            this.btnInDMSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInDMSP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnInDMSP.Icon")));
            this.btnInDMSP.Location = new System.Drawing.Point(687, 10);
            this.btnInDMSP.Name = "btnInDMSP";
            this.btnInDMSP.Size = new System.Drawing.Size(93, 23);
            this.btnInDMSP.TabIndex = 274;
            this.btnInDMSP.Text = "In ĐM";
            this.btnInDMSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnInDMSP.VisualStyleManager = this.vsmMain;
            this.btnInDMSP.Click += new System.EventHandler(this.btnInDMSP_Click);
            // 
            // btnXemTKCTSP
            // 
            this.btnXemTKCTSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKCTSP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemTKCTSP.Icon")));
            this.btnXemTKCTSP.Location = new System.Drawing.Point(176, 10);
            this.btnXemTKCTSP.Name = "btnXemTKCTSP";
            this.btnXemTKCTSP.Size = new System.Drawing.Size(230, 23);
            this.btnXemTKCTSP.TabIndex = 273;
            this.btnXemTKCTSP.Text = "Xem tờ khai Chuyển Tiếp SP";
            this.btnXemTKCTSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemTKCTSP.VisualStyleManager = this.vsmMain;
            this.btnXemTKCTSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemPKSP
            // 
            this.btnXemPKSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemPKSP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemPKSP.Icon")));
            this.btnXemPKSP.Location = new System.Drawing.Point(412, 10);
            this.btnXemPKSP.Name = "btnXemPKSP";
            this.btnXemPKSP.Size = new System.Drawing.Size(170, 23);
            this.btnXemPKSP.TabIndex = 272;
            this.btnXemPKSP.Text = "Xem PK liên quan SP";
            this.btnXemPKSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemPKSP.VisualStyleManager = this.vsmMain;
            this.btnXemPKSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemDMSP
            // 
            this.btnXemDMSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemDMSP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemDMSP.Icon")));
            this.btnXemDMSP.Location = new System.Drawing.Point(588, 10);
            this.btnXemDMSP.Name = "btnXemDMSP";
            this.btnXemDMSP.Size = new System.Drawing.Size(93, 23);
            this.btnXemDMSP.TabIndex = 271;
            this.btnXemDMSP.Text = "Xem ĐM";
            this.btnXemDMSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemDMSP.VisualStyleManager = this.vsmMain;
            this.btnXemDMSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemTKXSP
            // 
            this.btnXemTKXSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKXSP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemTKXSP.Icon")));
            this.btnXemTKXSP.Location = new System.Drawing.Point(3, 10);
            this.btnXemTKXSP.Name = "btnXemTKXSP";
            this.btnXemTKXSP.Size = new System.Drawing.Size(167, 23);
            this.btnXemTKXSP.TabIndex = 270;
            this.btnXemTKXSP.Text = "Xem tờ khai Xuất SP";
            this.btnXemTKXSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemTKXSP.VisualStyleManager = this.vsmMain;
            this.btnXemTKXSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // tpThietBi
            // 
            this.tpThietBi.Controls.Add(this.btnXemTKXTB);
            this.tpThietBi.Controls.Add(this.btnXemPKTB);
            this.tpThietBi.Controls.Add(this.btnXemTKNTB);
            this.tpThietBi.Controls.Add(this.dgThietBi);
            this.tpThietBi.Key = "tpTB";
            this.tpThietBi.Location = new System.Drawing.Point(1, 21);
            this.tpThietBi.Name = "tpThietBi";
            this.tpThietBi.Size = new System.Drawing.Size(974, 349);
            this.tpThietBi.TabStop = true;
            this.tpThietBi.Text = "Thiết bị";
            // 
            // btnXemTKXTB
            // 
            this.btnXemTKXTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKXTB.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemTKXTB.Icon")));
            this.btnXemTKXTB.Location = new System.Drawing.Point(177, 10);
            this.btnXemTKXTB.Name = "btnXemTKXTB";
            this.btnXemTKXTB.Size = new System.Drawing.Size(167, 23);
            this.btnXemTKXTB.TabIndex = 272;
            this.btnXemTKXTB.Text = "Xem tờ khai Xuất TB";
            this.btnXemTKXTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemTKXTB.VisualStyleManager = this.vsmMain;
            this.btnXemTKXTB.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemPKTB
            // 
            this.btnXemPKTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemPKTB.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemPKTB.Icon")));
            this.btnXemPKTB.Location = new System.Drawing.Point(350, 10);
            this.btnXemPKTB.Name = "btnXemPKTB";
            this.btnXemPKTB.Size = new System.Drawing.Size(170, 23);
            this.btnXemPKTB.TabIndex = 271;
            this.btnXemPKTB.Text = "Xem PK liên quan TB ";
            this.btnXemPKTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemPKTB.VisualStyleManager = this.vsmMain;
            this.btnXemPKTB.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemTKNTB
            // 
            this.btnXemTKNTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKNTB.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemTKNTB.Icon")));
            this.btnXemTKNTB.Location = new System.Drawing.Point(4, 10);
            this.btnXemTKNTB.Name = "btnXemTKNTB";
            this.btnXemTKNTB.Size = new System.Drawing.Size(167, 23);
            this.btnXemTKNTB.TabIndex = 270;
            this.btnXemTKNTB.Text = "Xem tờ khai Nhập TB";
            this.btnXemTKNTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemTKNTB.VisualStyleManager = this.vsmMain;
            this.btnXemTKNTB.Click += new System.EventHandler(this.btn_Click);
            // 
            // dgThietBi
            // 
            this.dgThietBi.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThietBi.AlternatingColors = true;
            this.dgThietBi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgThietBi.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThietBi.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThietBi.ContextMenuStrip = this.ctmTB;
            dgThietBi_DesignTimeLayout.LayoutString = resources.GetString("dgThietBi_DesignTimeLayout.LayoutString");
            this.dgThietBi.DesignTimeLayout = dgThietBi_DesignTimeLayout;
            this.dgThietBi.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThietBi.GroupByBoxVisible = false;
            this.dgThietBi.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThietBi.ImageList = this.ImageList1;
            this.dgThietBi.Location = new System.Drawing.Point(0, 37);
            this.dgThietBi.Name = "dgThietBi";
            this.dgThietBi.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgThietBi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgThietBi.Size = new System.Drawing.Size(974, 312);
            this.dgThietBi.TabIndex = 20;
            this.dgThietBi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThietBi.VisualStyleManager = this.vsmMain;
            this.dgThietBi.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgThietBi_LoadingRow);
            // 
            // ctmTB
            // 
            this.ctmTB.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemTKNTB,
            this.xemTKXTB,
            this.xemPKTB});
            this.ctmTB.Name = "ctmTB";
            this.ctmTB.Size = new System.Drawing.Size(245, 70);
            // 
            // xemTKNTB
            // 
            this.xemTKNTB.Name = "xemTKNTB";
            this.xemTKNTB.Size = new System.Drawing.Size(244, 22);
            this.xemTKNTB.Text = "Xem tờ khai nhập TB này";
            this.xemTKNTB.Click += new System.EventHandler(this.xemTKNTB_Click);
            // 
            // xemTKXTB
            // 
            this.xemTKXTB.Name = "xemTKXTB";
            this.xemTKXTB.Size = new System.Drawing.Size(244, 22);
            this.xemTKXTB.Text = "Xem tờ khai xuất TB này";
            this.xemTKXTB.Click += new System.EventHandler(this.xemTKXTB_Click);
            // 
            // xemPKTB
            // 
            this.xemPKTB.Name = "xemPKTB";
            this.xemPKTB.Size = new System.Drawing.Size(244, 22);
            this.xemPKTB.Text = "Xem phụ kiện liên quan đến TB này";
            this.xemPKTB.Click += new System.EventHandler(this.xemPKTB_Click);
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgTKN);
            this.uiTabPage1.Key = "tpTKNK";
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(974, 377);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Tờ khai NK";
            // 
            // dgTKN
            // 
            this.dgTKN.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKN.AlternatingColors = true;
            this.dgTKN.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKN.ColumnAutoResize = true;
            this.dgTKN.ContextMenuStrip = this.ctmTKN;
            dgTKN_DesignTimeLayout.LayoutString = resources.GetString("dgTKN_DesignTimeLayout.LayoutString");
            this.dgTKN.DesignTimeLayout = dgTKN_DesignTimeLayout;
            this.dgTKN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKN.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.dgTKN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKN.GroupByBoxVisible = false;
            this.dgTKN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKN.ImageList = this.ImageList1;
            this.dgTKN.Location = new System.Drawing.Point(0, 0);
            this.dgTKN.Name = "dgTKN";
            this.dgTKN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKN.Size = new System.Drawing.Size(974, 377);
            this.dgTKN.TabIndex = 21;
            this.dgTKN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKN.VisualStyleManager = this.vsmMain;
            this.dgTKN.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKN_RowDoubleClick);
            this.dgTKN.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKN_DeletingRecords);
            this.dgTKN.RecordUpdated += new System.EventHandler(this.dgTKN_RecordUpdated);
            this.dgTKN.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKN_LoadingRow);
            // 
            // ctmTKN
            // 
            this.ctmTKN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemChiTietTKN});
            this.ctmTKN.Name = "ctmTKN";
            this.ctmTKN.Size = new System.Drawing.Size(185, 26);
            // 
            // xemChiTietTKN
            // 
            this.xemChiTietTKN.Name = "xemChiTietTKN";
            this.xemChiTietTKN.Size = new System.Drawing.Size(184, 22);
            this.xemChiTietTKN.Text = "Xem chi tiết tờ khai này";
            this.xemChiTietTKN.Click += new System.EventHandler(this.xemChiTietTKN_Click);
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.btnXemLuongNPLXuatTK);
            this.uiTabPage2.Controls.Add(this.btnXemLuongNPLCungUngTK);
            this.uiTabPage2.Controls.Add(this.btnXemChiTietTK);
            this.uiTabPage2.Controls.Add(this.dgTKX);
            this.uiTabPage2.Key = "tpTKX";
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(974, 349);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Tờ khai XK";
            // 
            // btnXemLuongNPLXuatTK
            // 
            this.btnXemLuongNPLXuatTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemLuongNPLXuatTK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemLuongNPLXuatTK.Icon")));
            this.btnXemLuongNPLXuatTK.Location = new System.Drawing.Point(112, 10);
            this.btnXemLuongNPLXuatTK.Name = "btnXemLuongNPLXuatTK";
            this.btnXemLuongNPLXuatTK.Size = new System.Drawing.Size(282, 23);
            this.btnXemLuongNPLXuatTK.TabIndex = 272;
            this.btnXemLuongNPLXuatTK.Text = "Xem lượng NPL xuất của tờ khai";
            this.btnXemLuongNPLXuatTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemLuongNPLXuatTK.VisualStyleManager = this.vsmMain;
            this.btnXemLuongNPLXuatTK.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemLuongNPLCungUngTK
            // 
            this.btnXemLuongNPLCungUngTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemLuongNPLCungUngTK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemLuongNPLCungUngTK.Icon")));
            this.btnXemLuongNPLCungUngTK.Location = new System.Drawing.Point(398, 10);
            this.btnXemLuongNPLCungUngTK.Name = "btnXemLuongNPLCungUngTK";
            this.btnXemLuongNPLCungUngTK.Size = new System.Drawing.Size(288, 23);
            this.btnXemLuongNPLCungUngTK.TabIndex = 271;
            this.btnXemLuongNPLCungUngTK.Text = "Xem lượng NPL cung ứng của tờ khai";
            this.btnXemLuongNPLCungUngTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemLuongNPLCungUngTK.VisualStyleManager = this.vsmMain;
            this.btnXemLuongNPLCungUngTK.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemChiTietTK
            // 
            this.btnXemChiTietTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemChiTietTK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemChiTietTK.Icon")));
            this.btnXemChiTietTK.Location = new System.Drawing.Point(4, 10);
            this.btnXemChiTietTK.Name = "btnXemChiTietTK";
            this.btnXemChiTietTK.Size = new System.Drawing.Size(102, 23);
            this.btnXemChiTietTK.TabIndex = 270;
            this.btnXemChiTietTK.Text = "Xem chi tiết";
            this.btnXemChiTietTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemChiTietTK.VisualStyleManager = this.vsmMain;
            this.btnXemChiTietTK.Click += new System.EventHandler(this.btn_Click);
            // 
            // dgTKX
            // 
            this.dgTKX.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.AlternatingColors = true;
            this.dgTKX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTKX.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKX.ColumnAutoResize = true;
            this.dgTKX.ContextMenuStrip = this.ctmTKX;
            dgTKX_DesignTimeLayout.LayoutString = resources.GetString("dgTKX_DesignTimeLayout.LayoutString");
            this.dgTKX.DesignTimeLayout = dgTKX_DesignTimeLayout;
            this.dgTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKX.GroupByBoxVisible = false;
            this.dgTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKX.ImageList = this.ImageList1;
            this.dgTKX.Location = new System.Drawing.Point(0, 37);
            this.dgTKX.Name = "dgTKX";
            this.dgTKX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKX.Size = new System.Drawing.Size(974, 312);
            this.dgTKX.TabIndex = 22;
            this.dgTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKX.VisualStyleManager = this.vsmMain;
            this.dgTKX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKX_RowDoubleClick);
            this.dgTKX.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKX_DeletingRecords);
            this.dgTKX.RecordUpdated += new System.EventHandler(this.dgTKX_RecordUpdated);
            this.dgTKX.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKN_LoadingRow);
            // 
            // ctmTKX
            // 
            this.ctmTKX.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemChiTietTKX,
            this.xemLuongNPLTKX,
            this.xemNPLCungUngItem});
            this.ctmTKX.Name = "ctmTKX";
            this.ctmTKX.Size = new System.Drawing.Size(273, 70);
            // 
            // xemChiTietTKX
            // 
            this.xemChiTietTKX.Name = "xemChiTietTKX";
            this.xemChiTietTKX.Size = new System.Drawing.Size(272, 22);
            this.xemChiTietTKX.Text = "Xem chi tiết tờ khai này";
            this.xemChiTietTKX.Click += new System.EventHandler(this.xemChiTietTKX_Click);
            // 
            // xemLuongNPLTKX
            // 
            this.xemLuongNPLTKX.Name = "xemLuongNPLTKX";
            this.xemLuongNPLTKX.Size = new System.Drawing.Size(272, 22);
            this.xemLuongNPLTKX.Text = "Xem lượng NPL xuât tờ khai này";
            this.xemLuongNPLTKX.Click += new System.EventHandler(this.xemLuongNPLTKX_Click);
            // 
            // xemNPLCungUngItem
            // 
            this.xemNPLCungUngItem.Name = "xemNPLCungUngItem";
            this.xemNPLCungUngItem.Size = new System.Drawing.Size(272, 22);
            this.xemNPLCungUngItem.Text = "Xem lượng NPL cung ứng của tờ khai này";
            this.xemNPLCungUngItem.Click += new System.EventHandler(this.xemNPLCungUngItem_Click);
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.dgPhuKien);
            this.uiTabPage3.Key = "tpPhuKien";
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(974, 377);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Phụ kiện";
            // 
            // dgPhuKien
            // 
            this.dgPhuKien.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgPhuKien.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgPhuKien.AlternatingColors = true;
            this.dgPhuKien.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgPhuKien.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgPhuKien.ColumnAutoResize = true;
            this.dgPhuKien.ContextMenuStrip = this.ctmPK;
            dgPhuKien_DesignTimeLayout.LayoutString = resources.GetString("dgPhuKien_DesignTimeLayout.LayoutString");
            this.dgPhuKien.DesignTimeLayout = dgPhuKien_DesignTimeLayout;
            this.dgPhuKien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPhuKien.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.dgPhuKien.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgPhuKien.GroupByBoxVisible = false;
            this.dgPhuKien.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgPhuKien.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgPhuKien.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgPhuKien.ImageList = this.ImageList1;
            this.dgPhuKien.Location = new System.Drawing.Point(0, 0);
            this.dgPhuKien.Name = "dgPhuKien";
            this.dgPhuKien.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgPhuKien.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgPhuKien.Size = new System.Drawing.Size(974, 377);
            this.dgPhuKien.TabIndex = 20;
            this.dgPhuKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgPhuKien.VisualStyleManager = this.vsmMain;
            this.dgPhuKien.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgPhuKien_RowDoubleClick_1);
            this.dgPhuKien.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgPhuKien_DeletingRecords);
            this.dgPhuKien.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgPhuKien_LoadingRow_1);
            // 
            // ctmPK
            // 
            this.ctmPK.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemChiTietPK});
            this.ctmPK.Name = "ctmPK";
            this.ctmPK.Size = new System.Drawing.Size(174, 26);
            // 
            // xemChiTietPK
            // 
            this.xemChiTietPK.Name = "xemChiTietPK";
            this.xemChiTietPK.Size = new System.Drawing.Size(173, 22);
            this.xemChiTietPK.Text = "Xem chi tiết phụ kiện";
            this.xemChiTietPK.Click += new System.EventHandler(this.xemChiTietPK_Click);
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.dgTKCT);
            this.uiTabPage4.Key = "tpTKCTNhap";
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(974, 377);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "TKCT nhập";
            // 
            // dgTKCT
            // 
            this.dgTKCT.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKCT.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKCT.AlternatingColors = true;
            this.dgTKCT.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKCT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKCT.ColumnAutoResize = true;
            this.dgTKCT.ContextMenuStrip = this.ctmTKCT;
            dgTKCT_DesignTimeLayout.LayoutString = resources.GetString("dgTKCT_DesignTimeLayout.LayoutString");
            this.dgTKCT.DesignTimeLayout = dgTKCT_DesignTimeLayout;
            this.dgTKCT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKCT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKCT.GroupByBoxVisible = false;
            this.dgTKCT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKCT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKCT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKCT.ImageList = this.ImageList1;
            this.dgTKCT.Location = new System.Drawing.Point(0, 0);
            this.dgTKCT.Name = "dgTKCT";
            this.dgTKCT.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKCT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKCT.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKCT.Size = new System.Drawing.Size(974, 377);
            this.dgTKCT.TabIndex = 23;
            this.dgTKCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKCT.VisualStyleManager = this.vsmMain;
            this.dgTKCT.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKCT_RowDoubleClick);
            this.dgTKCT.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKCT_DeletingRecords);
            this.dgTKCT.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKCT_LoadingRow);
            // 
            // ctmTKCT
            // 
            this.ctmTKCT.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemToolStripMenuItem});
            this.ctmTKCT.Name = "ctmTKCT";
            this.ctmTKCT.Size = new System.Drawing.Size(243, 26);
            // 
            // xemToolStripMenuItem
            // 
            this.xemToolStripMenuItem.Name = "xemToolStripMenuItem";
            this.xemToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.xemToolStripMenuItem.Text = "Xem chi tiết tờ khai chuyên tiếp này";
            this.xemToolStripMenuItem.Click += new System.EventHandler(this.xemToolStripMenuItem_Click);
            // 
            // uiTabPage7
            // 
            this.uiTabPage7.Controls.Add(this.dgTKCTXuat);
            this.uiTabPage7.Key = "tpTKCTXuat";
            this.uiTabPage7.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage7.Name = "uiTabPage7";
            this.uiTabPage7.Size = new System.Drawing.Size(1006, 353);
            this.uiTabPage7.TabStop = true;
            this.uiTabPage7.Text = "TKCT Xuất";
            // 
            // dgTKCTXuat
            // 
            this.dgTKCTXuat.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKCTXuat.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKCTXuat.AlternatingColors = true;
            this.dgTKCTXuat.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKCTXuat.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKCTXuat.ColumnAutoResize = true;
            this.dgTKCTXuat.ContextMenuStrip = this.ctmTKCTXuat;
            dgTKCTXuat_DesignTimeLayout.LayoutString = resources.GetString("dgTKCTXuat_DesignTimeLayout.LayoutString");
            this.dgTKCTXuat.DesignTimeLayout = dgTKCTXuat_DesignTimeLayout;
            this.dgTKCTXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKCTXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKCTXuat.GroupByBoxVisible = false;
            this.dgTKCTXuat.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKCTXuat.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKCTXuat.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKCTXuat.ImageList = this.ImageList1;
            this.dgTKCTXuat.Location = new System.Drawing.Point(0, 0);
            this.dgTKCTXuat.Name = "dgTKCTXuat";
            this.dgTKCTXuat.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKCTXuat.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKCTXuat.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKCTXuat.Size = new System.Drawing.Size(1006, 353);
            this.dgTKCTXuat.TabIndex = 24;
            this.dgTKCTXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKCTXuat.VisualStyleManager = this.vsmMain;
            this.dgTKCTXuat.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKCTXuat_RowDoubleClick);
            this.dgTKCTXuat.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKCTXuat_DeletingRecords);
            this.dgTKCTXuat.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKCT_LoadingRow);
            // 
            // ctmTKCTXuat
            // 
            this.ctmTKCTXuat.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.ctmTKCTXuat.Name = "ctmTKCT";
            this.ctmTKCTXuat.Size = new System.Drawing.Size(243, 26);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(242, 22);
            this.toolStripMenuItem1.Text = "Xem chi tiết tờ khai chuyên tiếp này";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // tpNPLCU
            // 
            this.tpNPLCU.Controls.Add(this.dgNPLCungUng);
            this.tpNPLCU.Key = "tpNPLCU";
            this.tpNPLCU.Location = new System.Drawing.Point(1, 21);
            this.tpNPLCU.Name = "tpNPLCU";
            this.tpNPLCU.Size = new System.Drawing.Size(1014, 354);
            this.tpNPLCU.TabStop = true;
            this.tpNPLCU.Text = "NPL cung ứng";
            // 
            // dgNPLCungUng
            // 
            this.dgNPLCungUng.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNPLCungUng.AlternatingColors = true;
            this.dgNPLCungUng.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNPLCungUng.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNPLCungUng.ColumnAutoResize = true;
            dgNPLCungUng_DesignTimeLayout.LayoutString = resources.GetString("dgNPLCungUng_DesignTimeLayout.LayoutString");
            this.dgNPLCungUng.DesignTimeLayout = dgNPLCungUng_DesignTimeLayout;
            this.dgNPLCungUng.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNPLCungUng.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgNPLCungUng.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNPLCungUng.GroupByBoxVisible = false;
            this.dgNPLCungUng.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgNPLCungUng.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNPLCungUng.Hierarchical = true;
            this.dgNPLCungUng.ImageList = this.ImageList1;
            this.dgNPLCungUng.Location = new System.Drawing.Point(0, 0);
            this.dgNPLCungUng.Name = "dgNPLCungUng";
            this.dgNPLCungUng.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNPLCungUng.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCungUng.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgNPLCungUng.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgNPLCungUng.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgNPLCungUng.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgNPLCungUng.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNPLCungUng.Size = new System.Drawing.Size(1014, 354);
            this.dgNPLCungUng.TabIndex = 19;
            this.dgNPLCungUng.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNPLCungUng.VisualStyleManager = this.vsmMain;
            this.dgNPLCungUng.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgNPLCungUng.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgNPLCungUng_DeletingRecords);
            this.dgNPLCungUng.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgPhuKien_LoadingRow_1);
            // 
            // tpThanhKhoanMoi
            // 
            this.tpThanhKhoanMoi.Controls.Add(this.dgThanhKhoanMoi);
            this.tpThanhKhoanMoi.Key = "tpThanhKhoanMoi";
            this.tpThanhKhoanMoi.Location = new System.Drawing.Point(1, 21);
            this.tpThanhKhoanMoi.Name = "tpThanhKhoanMoi";
            this.tpThanhKhoanMoi.Size = new System.Drawing.Size(974, 349);
            this.tpThanhKhoanMoi.TabStop = true;
            this.tpThanhKhoanMoi.Text = "Báo cáo TK mới";
            // 
            // dgThanhKhoanMoi
            // 
            this.dgThanhKhoanMoi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoanMoi.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThanhKhoanMoi.AlternatingColors = true;
            this.dgThanhKhoanMoi.AutomaticSort = false;
            this.dgThanhKhoanMoi.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThanhKhoanMoi.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThanhKhoanMoi.ColumnAutoResize = true;
            dgThanhKhoanMoi_DesignTimeLayout.LayoutString = resources.GetString("dgThanhKhoanMoi_DesignTimeLayout.LayoutString");
            this.dgThanhKhoanMoi.DesignTimeLayout = dgThanhKhoanMoi_DesignTimeLayout;
            this.dgThanhKhoanMoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThanhKhoanMoi.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThanhKhoanMoi.GroupByBoxVisible = false;
            this.dgThanhKhoanMoi.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoanMoi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoanMoi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThanhKhoanMoi.ImageList = this.ImageList1;
            this.dgThanhKhoanMoi.Location = new System.Drawing.Point(0, 0);
            this.dgThanhKhoanMoi.Name = "dgThanhKhoanMoi";
            this.dgThanhKhoanMoi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoanMoi.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgThanhKhoanMoi.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoanMoi.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgThanhKhoanMoi.Size = new System.Drawing.Size(974, 349);
            this.dgThanhKhoanMoi.TabIndex = 21;
            this.dgThanhKhoanMoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThanhKhoanMoi.VisualStyleManager = this.vsmMain;
            this.dgThanhKhoanMoi.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgThanhKhoanMoi_RowDoubleClick);
            // 
            // uiTabPage6
            // 
            this.uiTabPage6.Controls.Add(this.dgThanhKhoan);
            this.uiTabPage6.Key = "tpThanhKhoan";
            this.uiTabPage6.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage6.Name = "uiTabPage6";
            this.uiTabPage6.Size = new System.Drawing.Size(974, 370);
            this.uiTabPage6.TabStop = true;
            this.uiTabPage6.Text = "Báo cáo TK cũ";
            // 
            // dgThanhKhoan
            // 
            this.dgThanhKhoan.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoan.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThanhKhoan.AlternatingColors = true;
            this.dgThanhKhoan.AutomaticSort = false;
            this.dgThanhKhoan.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThanhKhoan.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThanhKhoan.ColumnAutoResize = true;
            dgThanhKhoan_DesignTimeLayout.LayoutString = resources.GetString("dgThanhKhoan_DesignTimeLayout.LayoutString");
            this.dgThanhKhoan.DesignTimeLayout = dgThanhKhoan_DesignTimeLayout;
            this.dgThanhKhoan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThanhKhoan.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThanhKhoan.GroupByBoxVisible = false;
            this.dgThanhKhoan.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoan.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoan.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThanhKhoan.ImageList = this.ImageList1;
            this.dgThanhKhoan.Location = new System.Drawing.Point(0, 0);
            this.dgThanhKhoan.Name = "dgThanhKhoan";
            this.dgThanhKhoan.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoan.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgThanhKhoan.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoan.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgThanhKhoan.Size = new System.Drawing.Size(974, 370);
            this.dgThanhKhoan.TabIndex = 20;
            this.dgThanhKhoan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThanhKhoan.VisualStyleManager = this.vsmMain;
            this.dgThanhKhoan.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick_1);
            // 
            // uiCommandManager1
            // 
            this.uiCommandManager1.BottomRebar = this.BottomRebar1;
            this.uiCommandManager1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.uiCommandManager1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThanhKhoan,
            this.cmdXuLyHD,
            this.cmdLayToKhaiNhapSXXK,
            this.cmdXuatExcelDinhMuc,
            this.LuuThongTin});
            this.uiCommandManager1.ContainerControl = this;
            this.uiCommandManager1.Id = new System.Guid("7b61c354-7040-41ec-a33d-b4f402602227");
            this.uiCommandManager1.LeftRebar = this.LeftRebar1;
            this.uiCommandManager1.RightRebar = this.RightRebar1;
            this.uiCommandManager1.Tag = null;
            this.uiCommandManager1.TopRebar = this.TopRebar1;
            this.uiCommandManager1.VisualStyleManager = this.vsmMain;
            this.uiCommandManager1.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandManager1_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.uiCommandManager1;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 424);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(786, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.uiCommandManager1;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThanhKhoan1,
            this.cmdXuLyHD1,
            this.cmdLayToKhaiNhapSXXK1,
            this.LuuThongTin1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(535, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdThanhKhoan1
            // 
            this.cmdThanhKhoan1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThanhKhoan1.Icon")));
            this.cmdThanhKhoan1.Key = "cmdThanhKhoan";
            this.cmdThanhKhoan1.Name = "cmdThanhKhoan1";
            this.cmdThanhKhoan1.Shortcut = System.Windows.Forms.Shortcut.CtrlK;
            this.cmdThanhKhoan1.Text = "Thanh &khoản";
            // 
            // cmdXuLyHD1
            // 
            this.cmdXuLyHD1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdXuLyHD1.Icon")));
            this.cmdXuLyHD1.Key = "cmdXuLyHD";
            this.cmdXuLyHD1.Name = "cmdXuLyHD1";
            this.cmdXuLyHD1.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
            this.cmdXuLyHD1.Text = "&Xứ lý hợp đồng";
            // 
            // cmdLayToKhaiNhapSXXK1
            // 
            this.cmdLayToKhaiNhapSXXK1.Key = "cmdLayToKhaiNhapSXXK";
            this.cmdLayToKhaiNhapSXXK1.Name = "cmdLayToKhaiNhapSXXK1";
            this.cmdLayToKhaiNhapSXXK1.Text = "Đồng &bộ tờ khai nhập SXXK";
            // 
            // LuuThongTin1
            // 
            this.LuuThongTin1.Key = "LuuThongTin";
            this.LuuThongTin1.Name = "LuuThongTin1";
            // 
            // cmdThanhKhoan
            // 
            this.cmdThanhKhoan.Key = "cmdThanhKhoan";
            this.cmdThanhKhoan.Name = "cmdThanhKhoan";
            this.cmdThanhKhoan.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdThanhKhoan.Text = "Thanh Khoan";
            // 
            // cmdXuLyHD
            // 
            this.cmdXuLyHD.Key = "cmdXuLyHD";
            this.cmdXuLyHD.Name = "cmdXuLyHD";
            this.cmdXuLyHD.Text = "Xử lý hợp đồng";
            // 
            // cmdLayToKhaiNhapSXXK
            // 
            this.cmdLayToKhaiNhapSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLayToKhaiNhapSXXK.Icon")));
            this.cmdLayToKhaiNhapSXXK.Key = "cmdLayToKhaiNhapSXXK";
            this.cmdLayToKhaiNhapSXXK.Name = "cmdLayToKhaiNhapSXXK";
            this.cmdLayToKhaiNhapSXXK.Text = "Đồng bộ tờ khai nhập SXXK";
            // 
            // cmdXuatExcelDinhMuc
            // 
            this.cmdXuatExcelDinhMuc.Key = "cmdXuatExcelDinhMuc";
            this.cmdXuatExcelDinhMuc.Name = "cmdXuatExcelDinhMuc";
            this.cmdXuatExcelDinhMuc.Text = "Xuất Excel định mức sản phẩm";
            // 
            // LuuThongTin
            // 
            this.LuuThongTin.Icon = ((System.Drawing.Icon)(resources.GetObject("LuuThongTin.Icon")));
            this.LuuThongTin.Key = "LuuThongTin";
            this.LuuThongTin.Name = "LuuThongTin";
            this.LuuThongTin.Text = "Lưu thông tin hợp đồng";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.uiCommandManager1;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 396);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.uiCommandManager1;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(786, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 396);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.uiCommandManager1;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1024, 28);
            // 
            // gridEXPrintNPL
            // 
            this.gridEXPrintNPL.CardColumnsPerPage = 1;
            this.gridEXPrintNPL.DocumentName = "DanhSachNPL";
            this.gridEXPrintNPL.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintNPL.FooterDistance = 50;
            this.gridEXPrintNPL.GridEX = this.dgNguyenPhuLieu;
            this.gridEXPrintNPL.HeaderDistance = 50;
            this.gridEXPrintNPL.PageHeaderCenter = "DANH SÁCH NGUYÊN PHỤ LIỆU ĐĂNG KÝ CỦA HỢP ĐỒNG";
            this.gridEXPrintNPL.PageHeaderFormatStyle.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold);
            this.gridEXPrintNPL.PageHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEXPrintNPL.PageHeaderFormatStyle.LineAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEXPrintNPL.PageHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEXPrintNPL.RepeatHeaders = false;
            // 
            // gridEXPrintSP
            // 
            this.gridEXPrintSP.CardColumnsPerPage = 1;
            this.gridEXPrintSP.DocumentName = "DanhSachSP";
            this.gridEXPrintSP.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintSP.FooterDistance = 50;
            this.gridEXPrintSP.GridEX = this.dgSanPham;
            this.gridEXPrintSP.HeaderDistance = 50;
            this.gridEXPrintSP.PageHeaderCenter = "DANH SÁCH SẢN PHẨM ĐĂNG KÝ";
            this.gridEXPrintSP.PageHeaderFormatStyle.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold);
            this.gridEXPrintSP.PageHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEXPrintSP.PageHeaderFormatStyle.LineAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEXPrintSP.PageHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEXPrintSP.RepeatHeaders = false;
            // 
            // gridEXExporterSP
            // 
            this.gridEXExporterSP.GridEX = this.dgSanPham;
            this.gridEXExporterSP.SheetName = "SP";
            // 
            // gridEXExporterNPL
            // 
            this.gridEXExporterNPL.GridEX = this.dgNguyenPhuLieu;
            this.gridEXExporterNPL.SheetName = "NPL";
            // 
            // HopDongRegistedDetailForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(1024, 437);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HopDongRegistedDetailForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chi tiết hợp đồng gia công";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.HopDong_Form_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).EndInit();
            this.tabHopDong.ResumeLayout(false);
            this.uiTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThongTinHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            this.tpNguyenPhuLieu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).EndInit();
            this.ctmNPL.ResumeLayout(false);
            this.tpSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).EndInit();
            this.ctmSP.ResumeLayout(false);
            this.tpThietBi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).EndInit();
            this.ctmTB.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKN)).EndInit();
            this.ctmTKN.ResumeLayout(false);
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).EndInit();
            this.ctmTKX.ResumeLayout(false);
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPhuKien)).EndInit();
            this.ctmPK.ResumeLayout(false);
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKCT)).EndInit();
            this.ctmTKCT.ResumeLayout(false);
            this.uiTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKCTXuat)).EndInit();
            this.ctmTKCTXuat.ResumeLayout(false);
            this.tpNPLCU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCungUng)).EndInit();
            this.tpThanhKhoanMoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoanMoi)).EndInit();
            this.uiTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.Tab.UITab tabHopDong;
        private Janus.Windows.UI.Tab.UITabPage tpNguyenPhuLieu;
        private Janus.Windows.UI.Tab.UITabPage tpSanPham;
        private Janus.Windows.UI.Tab.UITabPage tpThietBi;
        private Janus.Windows.GridEX.GridEX dgThietBi;
        private Janus.Windows.GridEX.GridEX dgSanPham;
        private Janus.Windows.GridEX.GridEX dgNguyenPhuLieu;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.GridEX.GridEX dgTKN;
        private Janus.Windows.GridEX.GridEX dgTKX;
        private Janus.Windows.GridEX.GridEX dgPhuKien;
        private Janus.Windows.GridEX.GridEX dgTKCT;
        private ContextMenuStrip ctmNPL;
        private ContextMenuStrip ctmSP;
        private ContextMenuStrip ctmTB;
        private ContextMenuStrip ctmTKN;
        private ContextMenuStrip ctmTKX;
        private ContextMenuStrip ctmPK;
        private ContextMenuStrip ctmTKCT;
        private ToolStripMenuItem xemTKN;
        private ToolStripMenuItem xemTKXSP;
        private ToolStripMenuItem xemPKSP;
        private ToolStripMenuItem xemTKNTB;
        private ToolStripMenuItem xemPKTB;
        private ToolStripMenuItem xemChiTietTKN;
        private ToolStripMenuItem xemChiTietTKX;
        private ToolStripMenuItem xemLuongNPLTKX;
        private ToolStripMenuItem xemChiTietPK;
        private ToolStripMenuItem xemToolStripMenuItem;
        private ToolStripMenuItem xemTKTXNPL;
        private ToolStripMenuItem xemPKChuaNPL;
        private ToolStripMenuItem xemTKXTB;
        private ToolStripMenuItem xemDMItem;
        private ToolStripMenuItem xemNPLCungUngItem;
        private Janus.Windows.UI.Tab.UITabPage tpNPLCU;
        private Janus.Windows.GridEX.GridEX dgNPLCungUng;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage6;
        private Janus.Windows.GridEX.GridEX dgThanhKhoan;
        private Janus.Windows.UI.CommandBars.UICommandManager uiCommandManager1;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoan;
        private UIButton btnNXoa;
        private UIButton btnNThem;
        private UIButton btnXoa;
        private UIButton btnXemTKTXNPL;
        private UIButton btnXemPKNPL;
        private UIButton btnXemTKNNPL;
        private UIButton btnXemPKSP;
        private UIButton btnXemDMSP;
        private UIButton btnXemTKXSP;
        private UIButton btnXemTKXTB;
        private UIButton btnXemPKTB;
        private UIButton btnXemTKNTB;
        private UIButton btnXemLuongNPLXuatTK;
        private UIButton btnXemLuongNPLCungUngTK;
        private UIButton btnXemChiTietTK;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuLyHD1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuLyHD;
        private UIButton btnXemTKCTNPL;
        private UIButton btnXemTKCTSP;
        private ToolStripMenuItem xemTKCTSPItem;
        private ToolStripMenuItem inDMSPItem;
        private ToolStripMenuItem xemTKCTNPLItem;
        private UIButton btnInDMSP;
        private UIButton btnInNPL;
        private UIButton btnInSP;
        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintNPL;
        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintSP;
        private UIButton uiButton1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage7;
        private Janus.Windows.GridEX.GridEX dgTKCTXuat;
        private ContextMenuStrip ctmTKCTXuat;
        private ToolStripMenuItem toolStripMenuItem1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayToKhaiNhapSXXK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayToKhaiNhapSXXK;
        private Janus.Windows.UI.Tab.UITabPage tpThanhKhoanMoi;
        private Janus.Windows.GridEX.GridEX dgThanhKhoanMoi;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatExcelDinhMuc;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage8;
        private Label lblTrangThaiThanhKhoan;
        private Label label8;
        private Company.Interface.Controls.NguyenTeControl nguyenTeControl1;
        private Company.Interface.Controls.NuocHControl nuocThue;
        private Label label5;
        private Label label3;
        private Label label6;
        private CalendarCombo ccNgayGiaHan;
        private CalendarCombo ccNgayKyHD;
        private EditBox txtSoHopDong;
        private Label label2;
        private Label label4;
        private Label label1;
        private CalendarCombo ccNgayKetThucHD;
        private UIGroupBox uiGroupBox3;
        private UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.GridEX dgThongTinHopDong;
        private Janus.Windows.FilterEditor.FilterEditor filterEditor1;
        private Janus.Windows.FilterEditor.FilterEditor filterEditor2;
        private ToolStripMenuItem xemTKXuatSPToolStripMenuItem;
        private Label lblGhiChu;
        private UIButton btnTinhLuongTon;
        private UIButton btnXuatExcelSanPham;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporterSP;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporterNPL;
        private UIButton btnXuatExcelNPL;
        private EditBox txtTenDoiTac;
        private Label label7;
        private EditBox txtDiaChi;
        private Label label9;
        private Janus.Windows.UI.CommandBars.UICommand LuuThongTin1;
        private Janus.Windows.UI.CommandBars.UICommand LuuThongTin;
        
        
    }
}
