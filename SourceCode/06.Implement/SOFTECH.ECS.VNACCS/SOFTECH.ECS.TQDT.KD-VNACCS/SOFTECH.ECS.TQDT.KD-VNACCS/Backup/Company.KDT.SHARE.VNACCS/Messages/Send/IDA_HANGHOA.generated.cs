using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class IDA_HANGHOA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute CMD { get; set; }
public PropertiesAttribute GZC { get; set; }
public PropertiesAttribute TXA { get; set; }
public PropertiesAttribute TXB { get; set; }
public PropertiesAttribute TXC { get; set; }
public PropertiesAttribute TXD { get; set; }
public PropertiesAttribute CMN { get; set; }
public PropertiesAttribute OR { get; set; }
public PropertiesAttribute ORS { get; set; }
public PropertiesAttribute KWS { get; set; }
public PropertiesAttribute SPD { get; set; }
public PropertiesAttribute QN1 { get; set; }
public PropertiesAttribute QT1 { get; set; }
public PropertiesAttribute QN2 { get; set; }
public PropertiesAttribute QT2 { get; set; }
public PropertiesAttribute BPR { get; set; }
public PropertiesAttribute UPR { get; set; }
public PropertiesAttribute UPC { get; set; }
public PropertiesAttribute TSC { get; set; }
public PropertiesAttribute BPC { get; set; }
public PropertiesAttribute DPR { get; set; }
public GroupAttribute VN_ { get; set; }
public PropertiesAttribute TDL { get; set; }
public PropertiesAttribute TXN { get; set; }
public PropertiesAttribute TXR { get; set; }
public PropertiesAttribute RE { get; set; }
public PropertiesAttribute REG { get; set; }
public GroupAttribute TX_ { get; set; }

public IDA_HANGHOA()
        {
CMD = new PropertiesAttribute(12, typeof(string));
GZC = new PropertiesAttribute(7, typeof(string));
TXA = new PropertiesAttribute(10, typeof(int));
TXB = new PropertiesAttribute(10, typeof(int));
TXC = new PropertiesAttribute(4, typeof(string));
TXD = new PropertiesAttribute(3, typeof(string));
CMN = new PropertiesAttribute(600, typeof(string));
OR = new PropertiesAttribute(2, typeof(string));
ORS = new PropertiesAttribute(3, typeof(string));
KWS = new PropertiesAttribute(1, typeof(string));
SPD = new PropertiesAttribute(10, typeof(string));
QN1 = new PropertiesAttribute(12, typeof(int));
QT1 = new PropertiesAttribute(4, typeof(string));
QN2 = new PropertiesAttribute(12, typeof(int));
QT2 = new PropertiesAttribute(4, typeof(string));
BPR = new PropertiesAttribute(20, typeof(int));
UPR = new PropertiesAttribute(9, typeof(int));
UPC = new PropertiesAttribute(3, typeof(string));
TSC = new PropertiesAttribute(4, typeof(string));
BPC = new PropertiesAttribute(3, typeof(string));
DPR = new PropertiesAttribute(20, typeof(int));
TDL = new PropertiesAttribute(2, typeof(string));
TXN = new PropertiesAttribute(12, typeof(string));
TXR = new PropertiesAttribute(3, typeof(string));
RE = new PropertiesAttribute(5, typeof(string));
REG = new PropertiesAttribute(16, typeof(int));
#region VN_
List<PropertiesAttribute> listVN_ = new List<PropertiesAttribute>();
listVN_.Add(new PropertiesAttribute(1, 5, EnumGroupID.IDA_HANGHOA_VN_, typeof(int)));
VN_ = new GroupAttribute("VN_", 5, listVN_);
#endregion VN_
#region TX_
List<PropertiesAttribute> listTX_ = new List<PropertiesAttribute>();
listTX_.Add(new PropertiesAttribute(10, 5, EnumGroupID.IDA_HANGHOA_TX_, typeof(string)));
listTX_.Add(new PropertiesAttribute(5, 5, EnumGroupID.IDA_HANGHOA_TR_, typeof(string)));
listTX_.Add(new PropertiesAttribute(16, 5, EnumGroupID.IDA_HANGHOA_TG_, typeof(int)));
TX_ = new GroupAttribute("TX_", 5, listTX_);
#endregion TX_
TongSoByte = 643;
}

}public partial class EnumGroupID
    {
public static readonly string IDA_HANGHOA_VN_ = "IDA_HANGHOA_VN_";
public static readonly string IDA_HANGHOA_TX_ = "IDA_HANGHOA_TX_";
public static readonly string IDA_HANGHOA_TR_ = "IDA_HANGHOA_TR_";
public static readonly string IDA_HANGHOA_TG_ = "IDA_HANGHOA_TG_";
}

}
