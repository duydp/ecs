using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.KDT.GC
{
    public partial class ToKhaiChuyenTiep : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected long _IDHopDong;
        protected long _SoTiepNhan;
        protected DateTime _NgayTiepNhan = new DateTime(1900, 01, 01);
        protected int _TrangThaiXuLy = -1;
        protected string _MaHaiQuanTiepNhan = String.Empty;
        protected long _SoToKhai;
        protected string _CanBoDangKy = String.Empty;
        protected DateTime _NgayDangKy = new DateTime(1900, 01, 01);
        protected string _MaDoanhNghiep = String.Empty;
        protected string _SoHopDongDV = String.Empty;
        protected DateTime _NgayHDDV = new DateTime(1900, 01, 01);
        protected DateTime _NgayHetHanHDDV = new DateTime(1900, 01, 01);
        protected string _NguoiChiDinhDV = String.Empty;
        protected string _MaKhachHang = String.Empty;
        protected string _TenKH = String.Empty;
        protected string _SoHDKH = String.Empty;
        protected DateTime _NgayHDKH = new DateTime(1900, 01, 01);
        protected DateTime _NgayHetHanHDKH = new DateTime(1900, 01, 01);
        protected string _NguoiChiDinhKH = String.Empty;
        protected string _MaDaiLy = String.Empty;
        protected string _MaLoaiHinh = String.Empty;
        protected string _DiaDiemXepHang = String.Empty;
        protected decimal _TyGiaVND;
        protected decimal _TyGiaUSD;
        protected decimal _LePhiHQ;
        protected string _SoBienLai = String.Empty;
        protected DateTime _NgayBienLai = new DateTime(1900, 01, 01);
        protected string _ChungTu = String.Empty;
        protected string _NguyenTe_ID = String.Empty;
        protected string _MaHaiQuanKH = String.Empty;
        protected long _ID_Relation;
        protected string _GUIDSTR = String.Empty;
        protected string _DeXuatKhac = String.Empty;
        protected string _LyDoSua = String.Empty;
        protected int _ActionStatus;
        protected string _GuidReference = String.Empty;
        protected short _NamDK;
        protected string _HUONGDAN = String.Empty;
        protected string _PhanLuong = String.Empty;
        protected string _Huongdan_PL = String.Empty;
        public string _PTTT_ID { set; get; }
        public string _DKGH_ID { set; get; }
        public DateTime _ThoiGianGiaoHang { set; get; }
        public decimal SoKien { set; get; }
        public decimal TrongLuong { set; get; }
        public decimal TrongLuongTinh { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long IDHopDong
        {
            set { this._IDHopDong = value; }
            get { return this._IDHopDong; }
        }
        public long SoTiepNhan
        {
            set { this._SoTiepNhan = value; }
            get { return this._SoTiepNhan; }
        }
        public DateTime NgayTiepNhan
        {
            set { this._NgayTiepNhan = value; }
            get { return this._NgayTiepNhan; }
        }
        public int TrangThaiXuLy
        {
            set { this._TrangThaiXuLy = value; }
            get { return this._TrangThaiXuLy; }
        }
        public string MaHaiQuanTiepNhan
        {
            set { this._MaHaiQuanTiepNhan = value.Trim(); }
            get { return this._MaHaiQuanTiepNhan.Trim(); }
        }
        public long SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string CanBoDangKy
        {
            set { this._CanBoDangKy = value; }
            get { return this._CanBoDangKy; }
        }
        public DateTime NgayDangKy
        {
            set { this._NgayDangKy = value; }
            get { return this._NgayDangKy; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value.Trim(); }
            get { return this._MaDoanhNghiep.Trim(); }
        }
        public string SoHopDongDV
        {
            set { this._SoHopDongDV = value; }
            get { return this._SoHopDongDV; }
        }
        public DateTime NgayHDDV
        {
            set { this._NgayHDDV = value; }
            get { return this._NgayHDDV; }
        }
        public DateTime NgayHetHanHDDV
        {
            set { this._NgayHetHanHDDV = value; }
            get { return this._NgayHetHanHDDV; }
        }
        public string NguoiChiDinhDV
        {
            set { this._NguoiChiDinhDV = value; }
            get { return this._NguoiChiDinhDV; }
        }
        public string MaKhachHang
        {
            set { this._MaKhachHang = value; }
            get { return this._MaKhachHang; }
        }
        public string TenKH
        {
            set { this._TenKH = value; }
            get { return this._TenKH; }
        }
        public string SoHDKH
        {
            set { this._SoHDKH = value; }
            get { return this._SoHDKH; }
        }
        public DateTime NgayHDKH
        {
            set { this._NgayHDKH = value; }
            get { return this._NgayHDKH; }
        }
        public DateTime NgayHetHanHDKH
        {
            set { this._NgayHetHanHDKH = value; }
            get { return this._NgayHetHanHDKH; }
        }
        public string NguoiChiDinhKH
        {
            set { this._NguoiChiDinhKH = value; }
            get { return this._NguoiChiDinhKH; }
        }
        public string MaDaiLy
        {
            set { this._MaDaiLy = value; }
            get { return this._MaDaiLy; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh.Trim(); }
        }
        public string DiaDiemXepHang
        {
            set { this._DiaDiemXepHang = value; }
            get { return this._DiaDiemXepHang; }
        }
        public decimal TyGiaVND
        {
            set { this._TyGiaVND = value; }
            get { return this._TyGiaVND; }
        }
        public decimal TyGiaUSD
        {
            set { this._TyGiaUSD = value; }
            get { return this._TyGiaUSD; }
        }
        public decimal LePhiHQ
        {
            set { this._LePhiHQ = value; }
            get { return this._LePhiHQ; }
        }
        public string SoBienLai
        {
            set { this._SoBienLai = value; }
            get { return this._SoBienLai; }
        }
        public DateTime NgayBienLai
        {
            set { this._NgayBienLai = value; }
            get { return this._NgayBienLai; }
        }
        public string ChungTu
        {
            set { this._ChungTu = value; }
            get { return this._ChungTu; }
        }
        public string NguyenTe_ID
        {
            set { this._NguyenTe_ID = value; }
            get { return this._NguyenTe_ID; }
        }
        public string MaHaiQuanKH
        {
            set { this._MaHaiQuanKH = value.Trim(); }
            get { return this._MaHaiQuanKH.Trim(); }
        }
        public long ID_Relation
        {
            set { this._ID_Relation = value; }
            get { return this._ID_Relation; }
        }
        public string GUIDSTR
        {
            set { this._GUIDSTR = value; }
            get { return this._GUIDSTR; }
        }
        public string DeXuatKhac
        {
            set { this._DeXuatKhac = value; }
            get { return this._DeXuatKhac; }
        }
        public string LyDoSua
        {
            set { this._LyDoSua = value; }
            get { return this._LyDoSua; }
        }
        public int ActionStatus
        {
            set { this._ActionStatus = value; }
            get { return this._ActionStatus; }
        }
        public string GuidReference
        {
            set { this._GuidReference = value; }
            get { return this._GuidReference; }
        }
        public short NamDK
        {
            set { this._NamDK = value; }
            get { return this._NamDK; }
        }
        public string HUONGDAN
        {
            set { this._HUONGDAN = value; }
            get { return this._HUONGDAN; }
        }
        public string PhanLuong
        {
            set { this._PhanLuong = value; }
            get { return this._PhanLuong; }
        }
        public string Huongdan_PL
        {
            set { this._Huongdan_PL = value; }
            get { return this._Huongdan_PL; }
        }
        public string PTTT_ID
        {
            set { this._PTTT_ID = value; }
            get { return this._PTTT_ID; }
        }
        public string DKGH_ID
        {
            set { this._DKGH_ID = value; }
            get { return this._DKGH_ID; }
        }
        public DateTime ThoiGianGiaoHang
        {
            set { this._ThoiGianGiaoHang = value; }
            get { return this._ThoiGianGiaoHang; }
        }
        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) this._IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) this._MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) this._SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) this._NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) this._NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) this._NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) this._MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) this._TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) this._SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) this._NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) this._NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) this._NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) this._MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) this._TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) this._LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) this._SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) this._NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) this._ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) this._MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) this._ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) this._NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) this._Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) this.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                //Add by Hungtq
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongTinh"))) this.TrongLuongTinh = reader.GetDecimal(reader.GetOrdinal("TrongLuongTinh"));

                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiChuyenTiepCollection SelectCollectionBy_IDHopDong()
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);

            ToKhaiChuyenTiepCollection collection = new ToKhaiChuyenTiepCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                //Add by Hungtq
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongTinh"))) entity.TrongLuongTinh = reader.GetDecimal(reader.GetOrdinal("TrongLuongTinh"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_IDHopDong()
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiChuyenTiepCollection SelectCollectionAll()
        {
            ToKhaiChuyenTiepCollection collection = new ToKhaiChuyenTiepCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                //Add by Hungtq
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongTinh"))) entity.TrongLuongTinh = reader.GetDecimal(reader.GetOrdinal("TrongLuongTinh"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiChuyenTiepCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            ToKhaiChuyenTiepCollection collection = new ToKhaiChuyenTiepCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                //Add by Hungtq
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongTinh"))) entity.TrongLuongTinh = reader.GetDecimal(reader.GetOrdinal("TrongLuongTinh"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, this._MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, this._SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, this._NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, this._NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, this._NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, this._MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, this._TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, this._SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, this._NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, this._NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, this._NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, this._TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, this._LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, this._SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, this._NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this._ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, this._MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, this._ID_Relation);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);
            //Add by Hungtq
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@TrongLuongTinh", SqlDbType.Decimal, TrongLuongTinh);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(ToKhaiChuyenTiepCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiChuyenTiep item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, ToKhaiChuyenTiepCollection collection)
        {
            foreach (ToKhaiChuyenTiep item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, this._MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, this._SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, this._NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, this._NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, this._NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, this._MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, this._TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, this._SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, this._NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, this._NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, this._NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, this._TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, this._LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, this._SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, this._NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this._ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, this._MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, this._ID_Relation);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);
            //Add by Hungtq
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@TrongLuongTinh", SqlDbType.Decimal, TrongLuongTinh);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(ToKhaiChuyenTiepCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiChuyenTiep item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, this._MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, this._SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, this._NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, this._NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, this._NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, this._MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, this._TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, this._SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, this._NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, this._NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, this._NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, this._TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, this._LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, this._SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, this._NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this._ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, this._MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, this._ID_Relation);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);
            //Add by Hungtq
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@TrongLuongTinh", SqlDbType.Decimal, TrongLuongTinh);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransaction_V3(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, this._MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, this._SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, this._NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, this._NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, this._NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, this._MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, this._TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, this._SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, this._NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, this._NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, this._NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, this._TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, this._LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, this._SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, this._NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this._ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, this._MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, this._ID_Relation);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);
            //Add by Hungtq
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, TrongLuongNet);
            db.AddInParameter(dbCommand, "@DaiDienDoanhNghiep", SqlDbType.NVarChar, DaiDienDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, MaDonViUT);
            db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, TenDonViUT);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, NuocNK_ID);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, CuaKhau_ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(ToKhaiChuyenTiepCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiChuyenTiep item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete1()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(ToKhaiChuyenTiepCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiChuyenTiep item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(ToKhaiChuyenTiepCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiChuyenTiep item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int DeleteBy_IDHopDong()
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_DeleteBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);

            return this.db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteBy_IDHopDong(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_DeleteBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        #endregion
    }
}