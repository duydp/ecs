﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public partial class ucCuaKhauXuatNhap : ucBase
    {
        private static List<CategoryItem> dataItemss = null;

        private bool _showColumnCode = true;
        public bool ShowColumnCode
        {
            get { return _showColumnCode; }
            set { _showColumnCode = value; }
        }

        private bool _showColumnName = true;
        public bool ShowColumnName
        {
            get { return _showColumnName; }
            set { _showColumnName = value; }
        }

        /// <summary>
        /// Mã
        /// </summary>
        public string Code
        {
            get
            {
                if (SetValidate)
                {
                    if (!ValidateControl(false))
                        return txtCode.Text.Trim() != "" ? txtCode.Text.Trim() : "";
                    else
                    {
                        return cboCategory.EditValue.ToString().Trim();
                    }
                }
                else
                    return cboCategory.EditValue != null ? cboCategory.EditValue.ToString().Trim() : txtCode.Text.Trim();
            }
            set { cboCategory.EditValue = value; }
        }

        /// <summary>
        /// Tên Tiếng việt
        /// </summary>
        public string Name_VN
        {
            get
            {
                if (SetValidate)
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Text.Trim();
                    }
                }
                else
                    return cboCategory.Text.Trim();
            }
            set { cboCategory.Text = value; }
        }

        public string TagCode
        {
            get { return txtCode.Tag != null ? txtCode.Tag.ToString() : ""; }
            set { txtCode.Tag = value; }
        }

        public string TagName
        {
            get { return cboCategory.Tag != null ? cboCategory.Tag.ToString() : ""; }
            set { cboCategory.Tag = value; }
        }

        private EImportExport _importType = EImportExport.I;
        /// <summary>
        /// Xác định loại Nhập/ Xuất
        /// </summary>
        public EImportExport ImportType
        {
            get { return _importType; }
            set { _importType = value; }
        }

        private string _countryCode;
        /// <summary>
        /// Mã quốc gia
        /// </summary>
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        private bool _setValidate = false;
        public bool SetValidate
        {
            get { return _setValidate; }
            set { _setValidate = value; }
        }

        private bool _isValidate = false;
        public bool IsValidate
        {
            get
            {
                return SetValidate ? (_isValidate = ValidateControl(false)) : true;
            }
            set { _isValidate = value; }
        }

        private bool _setOnlyWarning = false;
        public bool SetOnlyWarning
        {
            get { return _setOnlyWarning; }
            set { _setOnlyWarning = value; }
        }

        private bool _isOnlyWarning = false;
        public bool IsOnlyWarning
        {
            get
            {
                return SetOnlyWarning ? (_isOnlyWarning = ValidateControl(true)) : false;
            }
            set
            {
                _isOnlyWarning = false;
            }
        }

        public bool IsUpperCase
        {
            set
            {
                if (value == true)
                {
                    txtCode.Properties.CharacterCasing = CharacterCasing.Upper;
                    cboCategory.Properties.CharacterCasing = CharacterCasing.Upper;
                }
                else
                {
                    txtCode.Properties.CharacterCasing = CharacterCasing.Normal;
                    cboCategory.Properties.CharacterCasing = CharacterCasing.Normal;
                }
            }
        }

        public ucCuaKhauXuatNhap()
        {
            InitializeComponent();

            cboCategory.Properties.NullText = "";

            txtCode.Enter += new EventHandler(txtCode_Enter);
            cboCategory.Enter += new EventHandler(cboCategory_Enter);
        }

        private void ucMaLoaiHinhIDA_Load(object sender, EventArgs e)
        {
            LoadData(false);
        }

        private void LoadData(bool isReload)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                dataItemss = new List<CategoryItem>();
                string whereCondition = "";
                string orderBy = "";
                string valueMember = "Code";
                string displayMember = "Name";
                string valueMemberCaption = "Mã";
                string displayMemberCaption = "Tên";

                //Lay du lieu tu City
                //whereCondition = base.WhereCondition != "" ? base.WhereCondition + " And " : "";
                //whereCondition += string.Format("CountryCode = '{0}'", CountryCode);
                //orderBy = "LOCODE asc";
                //dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionByCountryCode(ucBase.dataCityGlobals, string.IsNullOrEmpty(CountryCode) ? "" : CountryCode);

                whereCondition += string.Format("TableID like '{0}%'", ECategory.A016.ToString());
                //Create obj and load data
                dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionAllMinimize2("CountryCode = '" + CountryCode + "'");
                foreach (VNACC_Category_CityUNLOCODE item in dataCitys)
                {
                    dataItemss.Add(new CategoryItem(item.LOCODE, item.CityNameOrStateName));
                }

                //Lay du lieu tu Station
                //whereCondition = base.WhereCondition != "" ? base.WhereCondition + " And " : "";
                //whereCondition += string.Format("CountryCode = '{0}'", CountryCode);
                //orderBy = "StationCode asc";
                dataStations = VNACC_Category_Station.SelectCollectionByCountryCode(ucBase.dataStationGlobals, string.IsNullOrEmpty(CountryCode) ? "" : CountryCode);
                foreach (VNACC_Category_Station item in dataStations)
                {
                    dataItemss.Add(new CategoryItem(item.StationCode, item.StationName));
                }

                //Lay du lieu tu Station
                //whereCondition = base.WhereCondition != "" ? base.WhereCondition + " And " : "";
                //whereCondition += string.Format("BorderGateCode like '{0}%'", CountryCode);
                //orderBy = "BorderGateCode asc";
                dataBorderGates = VNACC_Category_BorderGate.SelectCollectionByCountryCode(ucBase.dataBorderGateGlobals, string.IsNullOrEmpty(CountryCode) ? "" : CountryCode);
                foreach (VNACC_Category_BorderGate item in dataBorderGates)
                {
                    dataItemss.Add(new CategoryItem(item.BorderGateCode, item.BorderGateName));
                }

                // Specify the data source to display in the dropdown.
                cboCategory.Properties.DataSource = dataItemss;

                //SET COLUMN
                cboCategory.Properties.Columns.Clear();
                // The field providing the editor's display text.
                if (ShowColumnCode && ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = displayMember;
                    cboCategory.Properties.ValueMember = valueMember;
                }
                else if (ShowColumnCode && !ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = valueMember;
                    cboCategory.Properties.ValueMember = valueMember;
                }
                else if (!ShowColumnCode && ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = displayMember;
                    cboCategory.Properties.ValueMember = displayMember;
                }

                // Add two columns to the dropdown.
                LookUpColumnInfoCollection coll = cboCategory.Properties.Columns;
                // A column to display the Format field's values.
                coll.Add(new LookUpColumnInfo(valueMember, valueMemberCaption, 0));
                if (displayMember != "")
                    coll.Add(new LookUpColumnInfo(displayMember, displayMemberCaption, 0));

                //  Set column widths according to their contents and resize the popup, if required.
                cboCategory.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                // Enable auto completion search mode.
                //cboCategory.Properties.SearchMode = SearchMode.AutoComplete; //Luu y: khi thiet lap thuoc tinh nay, khi chon Name co Code null se tu dong lay dong dau tien trong danh sach.
                // Specify the column against which to perform the search.
                cboCategory.Properties.AutoSearchColumnIndex = 1;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }
        }

        /// <summary>
        /// Tải mới lại dữ liệu danh mục
        /// </summary>
        public void ReLoadData()
        {
            LoadData(true);
        }

        private bool ValidateControl(bool isOnlyWarning)
        {
            bool isValid = true;

            isValid &= ValidationControl.ValidateNullLookUpEditDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);

            if (isValid)
            {
                isValid &= ValidationControl.ValidateChooseLookUpEditDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);
            }

            return isValid;
        }

        public delegate void EditValueChangedHandle(object sender, EventArgs e);
        public event EditValueChangedHandle EditValueChanged;
        private void cboCategory_EditValueChanged(object sender, EventArgs e)
        {
            txtCode.Text = Code;

            if (EditValueChanged != null)
            {
                EditValueChanged(sender, e);
            }
        }

        private void txtCode_Leave(object sender, EventArgs e)
        {
            Code = txtCode.Text.Trim();
        }

        public delegate void EnterHandle(object sender, EventArgs e);
        public event EnterHandle OnEnter;
        void cboCategory_Enter(object sender, EventArgs e)
        {
            if (OnEnter != null)
            {
                OnEnter(sender, e);
            }
        }
        void txtCode_Enter(object sender, EventArgs e)
        {
            if (OnEnter != null)
            {
                OnEnter(sender, e);
            }
        }
    }
}
