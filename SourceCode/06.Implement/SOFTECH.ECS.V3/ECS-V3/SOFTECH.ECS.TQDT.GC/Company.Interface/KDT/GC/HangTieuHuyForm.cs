﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.GC.BLL.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.Interface.GC;

namespace Company.Interface.KDT.GC
{
    public partial class HangTieuHuyForm : BaseForm
    {
        public List<HangGSTieuHuy> HangTieuHuys = new List<HangGSTieuHuy>();
        public HangGSTieuHuy hangTieuHuy = new HangGSTieuHuy();
        public bool IsEnable { private get; set; }
        public long IdHopDong;
        public HangTieuHuyForm()
        {
            IsEnable = true;
            InitializeComponent();
        }

        private void HangTieuHuyForm_Load(object sender, EventArgs e)
        {
            btnAdd.Enabled = btnXoa.Enabled = IsEnable;
            dgList.AllowDelete = IsEnable ? InheritableBoolean.True : InheritableBoolean.False;
            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            Set(hangTieuHuy);
            dgList.DataSource = HangTieuHuys;
            

        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            int i = Convert.ToInt32(e.Row.Cells["LoaiHang"].Value);
            string loaiHang = string.Empty;
            if (i == 1) loaiHang = "Nguyên phụ liệu";
            else if (i == 2) loaiHang = "Sản phẩm";
            else if (i == 3) loaiHang = "Thiết bị";
            else if (i == 4) loaiHang = "Hàng mẫu";
            e.Row.Cells["LoaiHang"].Text = loaiHang;
        }
        private void Set(HangGSTieuHuy hangTieuHuy)
        {
            txtMa.Text = hangTieuHuy.MaHang;
            txtTen.Text = hangTieuHuy.TenHang;
            txtSoLuong.Value = hangTieuHuy.SoLuong;
            txtMaHS.Text = hangTieuHuy.MaHS;
            cbDonViTinh.SelectedValue = hangTieuHuy.DVT_ID;
            txtGhiChu.Text = hangTieuHuy.GhiChu;
            if (hangTieuHuy.LoaiHang > 0) cbLoaiHang.SelectedValue = hangTieuHuy.LoaiHang;
        }
        private void Get(HangGSTieuHuy hangTieuHuy)
        {
            hangTieuHuy.MaHang = txtMa.Text;
            hangTieuHuy.TenHang = txtTen.Text;
            hangTieuHuy.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
            hangTieuHuy.MaHS = txtMaHS.Text;
            hangTieuHuy.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            hangTieuHuy.GhiChu = txtGhiChu.Text;
            hangTieuHuy.LoaiHang = Convert.ToInt32(cbLoaiHang.SelectedValue);
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            bool isValidate = false;
            isValidate = Globals.ValidateNull(txtMa, error, "Mã hàng");
            isValidate &= Globals.ValidateNull(txtTen, error, "Tên hàng");
            isValidate &= Globals.ValidateNull(cbDonViTinh, error, "Đơn vị tính");
            isValidate &= Globals.ValidateNull(txtMaHS, error, "Mã HS");
            isValidate &= Globals.ValidateNull(cbLoaiHang, error, "Loại hàng");
            if (!isValidate) return;
            HangTieuHuys.Remove(hangTieuHuy);
            Get(hangTieuHuy);
            //hangTieuHuy.InsertUpdate();
            HangTieuHuys.Add(hangTieuHuy);
            hangTieuHuy = new HangGSTieuHuy();
            Set(hangTieuHuy);
            txtMa.Focus();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            hangTieuHuy = (HangGSTieuHuy)e.Row.DataRow;
            if (hangTieuHuy != null)
                Set(hangTieuHuy);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count >0 && ShowMessage("Bạn có muốn xóa hàng không?",true)!="Yes" )
            {
                return;
            }
            foreach (GridEXSelectedItem item in items)
            {
                hangTieuHuy = (HangGSTieuHuy)item.GetRow().DataRow;
                if (hangTieuHuy != null)
                    HangTieuHuys.Remove(hangTieuHuy);
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void txtMa_ButtonClick(object sender, EventArgs e)
        {
            if(cbLoaiHang.SelectedValue.ToString()=="2")
            {
                SanPhamRegistedForm f = new SanPhamRegistedForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.SanPhamSelected.HopDong_ID = IdHopDong;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.SanPhamSelected.Ma))
                {
                    txtMa.Text = f.SanPhamSelected.Ma;
                    txtTen.Text = f.SanPhamSelected.Ten;
                    txtMaHS.Text = f.SanPhamSelected.MaHS;
                    cbDonViTinh.SelectedValue = f.SanPhamSelected.DVT_ID.PadRight(3);
                }
            }
            else if (cbLoaiHang.SelectedValue.ToString() == "1")
            {
                NguyenPhuLieuRegistedForm f = new NguyenPhuLieuRegistedForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.NguyenPhuLieuSelected.HopDong_ID = IdHopDong;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.NguyenPhuLieuSelected.Ma))
                {
                    txtMa.Text = f.NguyenPhuLieuSelected.Ma;
                    txtTen.Text = f.NguyenPhuLieuSelected.Ten;
                    txtMaHS.Text = f.NguyenPhuLieuSelected.MaHS;
                    cbDonViTinh.SelectedValue = f.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                }
            }
            else if (cbLoaiHang.SelectedValue.ToString() == "3")
            {
                ThietBiRegistedForm f = new ThietBiRegistedForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.ThietBiSelected.HopDong_ID = IdHopDong;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.ThietBiSelected.Ma))
                {
                    txtMa.Text = f.ThietBiSelected.Ma;
                    txtTen.Text = f.ThietBiSelected.Ten;
                    txtMaHS.Text = f.ThietBiSelected.MaHS;
                    cbDonViTinh.SelectedValue = f.ThietBiSelected.DVT_ID.PadRight(3);
                }
            }
            else if (cbLoaiHang.SelectedValue.ToString() == "4")
            {
                HangMauRegistedForm f = new HangMauRegistedForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.HangMauSelected.HopDong_ID = IdHopDong;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.HangMauSelected.Ma))
                {
                    txtMa.Text = f.HangMauSelected.Ma;
                    txtTen.Text = f.HangMauSelected.Ten;
                    txtMaHS.Text = f.HangMauSelected.MaHS;
                    cbDonViTinh.SelectedValue = f.HangMauSelected.DVT_ID.PadRight(3);
                }
            }
            else
                ShowMessage("Bạn chưa chọn loại hàng", false);
        }
    }
}
