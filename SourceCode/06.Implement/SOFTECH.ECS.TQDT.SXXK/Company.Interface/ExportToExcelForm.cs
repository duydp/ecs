﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT.ExportToExcel;
//using Company.BLL.DuLieuChuan;


namespace Company.Interface
{
    public partial class ExportToExcelForm : BaseForm
    {
        public ExportToExcelForm()
        {
            InitializeComponent();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;           
            btnExport.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                Workbook wb = new Workbook();                               
                //NPL Đã duyệt
                if (chkNPL.Checked)
                {
                    wb.Worksheets.Add("NPL_D");
                    Worksheet ws = wb.Worksheets["NPL_D"];
                    ws.Columns[0].Width = 5000;
                    ws.Columns[1].Width = 7000;
                    ws.Columns[2].Width = 3000;
                    ws.Columns[3].Width = 2000;
                    WorksheetRowCollection wsrc = ws.Rows;
                    WorksheetRow wsr0 = ws.Rows[0];
                    wsr0.Cells[0].Value = "MA_NPL";
                    wsr0.Cells[1].Value = "TEN_NPL";
                    wsr0.Cells[2].Value = "MA_HS";
                    wsr0.Cells[3].Value = "ĐVT";
                    DataTable dt = new SNPL().SelectDynamic(donViHaiQuanControl1.Ma, txtMaDN.Text).Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        WorksheetRow wsr = ws.Rows[i + 1];
                        wsr.Cells[0].Value = dt.Rows[i]["Ma"].ToString();
                        wsr.Cells[1].Value = dt.Rows[i]["Ten"].ToString();
                        string maHS = dt.Rows[i]["MaHS"].ToString().Trim();
                        while (maHS.Length < 10) maHS += "0";
                        wsr.Cells[2].Value = maHS;
                        wsr.Cells[3].Value = dt.Rows[i]["DVT_ID"].ToString();
                    }
                }
                
                //Sản phẩm đã duyệt :
                if (chkSanPham.Checked)
                {
                    wb.Worksheets.Add("SP_D");
                    Worksheet ws = wb.Worksheets["SP_D"];
                    ws.Columns[0].Width = 5000;
                    ws.Columns[1].Width = 7000;
                    ws.Columns[2].Width = 3000;
                    ws.Columns[3].Width = 2000;
                    WorksheetRowCollection wsrc = ws.Rows;
                    WorksheetRow wsr0 = ws.Rows[0];
                    wsr0.Cells[0].Value = "MA_SP";
                    wsr0.Cells[1].Value = "TEN_SP";
                    wsr0.Cells[2].Value = "MA_HS";
                    wsr0.Cells[3].Value = "ĐVT";
                    DataTable dt = new SSP().SelectDynamic(donViHaiQuanControl1.Ma, txtMaDN.Text).Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        WorksheetRow wsr = ws.Rows[i + 1];
                        wsr.Cells[0].Value = dt.Rows[i]["Ma"];
                        wsr.Cells[1].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(dt.Rows[i]["Ten"].ToString());
                        string maHS = dt.Rows[i]["MaHS"].ToString().Trim();
                        while (maHS.Length < 10) maHS += "0";
                        wsr.Cells[2].Value = maHS;
                        wsr.Cells[3].Value = dt.Rows[i]["DVT_ID"].ToString();
                    }
                }
                
                // Định mức đã duyệt :
                if (chkDMMoiKhai.Checked)
                {
                    wb.Worksheets.Add("TTDM_D");
                    Worksheet ws = wb.Worksheets["TTDM_D"];
                    ws.Columns[2].CellFormat.FormatString = "MM/dd/yyyy";
                    ws.Columns[3].CellFormat.FormatString = "MM/dd/yyyy";
                    ws.Columns[0].Width = 5000;
                    ws.Columns[1].Width = 4000;
                    ws.Columns[2].Width = 4000;
                    ws.Columns[3].Width = 4000;
                    WorksheetRowCollection wsrc = ws.Rows;
                    WorksheetRow wsr0 = ws.Rows[0];
                    wsr0.Cells[0].Value = "MA_SP";
                    wsr0.Cells[1].Value = "SO_DMUC";
                    wsr0.Cells[2].Value = "NGAY_DK";
                    wsr0.Cells[3].Value = "NGAY_AD";
                    DataTable dt = new DDINHMUC_TT().SelectDynamic(donViHaiQuanControl1.Ma, txtMaDN.Text).Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        WorksheetRow wsr = ws.Rows[i + 1];
                        wsr.Cells[0].Value = dt.Rows[i]["MaSanPham"];
                        wsr.Cells[1].Value = Convert.ToInt32(dt.Rows[i]["SoDinhMuc"].ToString());
                        if (dt.Rows[i]["NgayDangKy"].ToString() != "")
                            wsr.Cells[2].Value = Convert.ToDateTime(dt.Rows[i]["NgayDangKy"]);
                        if (dt.Rows[i]["NgayApDung"].ToString() != "")
                            wsr.Cells[3].Value = Convert.ToDateTime(dt.Rows[i]["NgayApDung"]);
                    }
                    wb.Worksheets.Add("DM_D");
                    Worksheet ws1 = wb.Worksheets["DM_D"];
                    WorksheetRowCollection wsrc1 = ws1.Rows;
                    WorksheetRow wsr01 = ws1.Rows[0];
                    wsr01.Cells[0].Value = "MA_SP";
                    wsr01.Cells[1].Value = "MA_NPL";
                    wsr01.Cells[2].Value = "DM_SDUNG";
                    wsr01.Cells[3].Value = "TYLE_HH";
                    wsr01.Cells[4].Value = "DMUC_CHUNG";
                    //wsr01.Cells[5].Value = "Ghi Chú";
                    DataTable dt1 = new DDINHMUC().SelectDynamic(donViHaiQuanControl1.Ma, txtMaDN.Text).Tables[0];
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        WorksheetRow wsr = ws1.Rows[i + 1];
                        wsr.Cells[0].Value = dt1.Rows[i]["MaSanPham"];
                        wsr.Cells[1].Value = dt1.Rows[i]["MaNguyenPhuLieu"];
                        wsr.Cells[2].Value = Convert.ToDecimal(dt1.Rows[i]["DinhMucSuDung"]);
                        wsr.Cells[3].Value = Convert.ToDecimal(dt1.Rows[i]["TyLeHaoHut"]);
                        wsr.Cells[4].Value = Convert.ToDecimal(dt1.Rows[i]["DinhMucChung"]);
                        //wsr.Cells[5].Value = Convert.ToDecimal(dt1.Rows[i]["GhiChu"]);

                    }


                }
                

                // Tờ khai đã duyệt :
                if (chkToKhaiMauDich.Checked)
                {
                    wb.Worksheets.Add("TKMD_D");
                    Worksheet ws1 = wb.Worksheets["TKMD_D"];
                    ws1.Columns[4].CellFormat.FormatString = "MM/dd/yyyy";
                    ws1.Columns[10].CellFormat.FormatString = "MM/dd/yyyy";
                    //ws1.Columns[13].CellFormat.FormatString = "MM/dd/yyyy";                    
                    ws1.Columns[14].CellFormat.FormatString = "MM/dd/yyyy";
                    ws1.Columns[16].CellFormat.FormatString = "MM/dd/yyyy";
                    ws1.Columns[17].CellFormat.FormatString = "MM/dd/yyyy";
                    ws1.Columns[18].CellFormat.FormatString = "MM/dd/yyyy";
                    ws1.Columns[37].CellFormat.FormatString = "MM/dd/yyyy";
                    ws1.Columns[40].CellFormat.FormatString = "MM/dd/yyyy";
                    ws1.Columns[41].CellFormat.FormatString = "MM/dd/yyyy";
                    ws1.Columns[44].CellFormat.FormatString = "MM/dd/yyyy";

                    WorksheetRowCollection wsrc1 = ws1.Rows;
                    WorksheetRow wsr01 = ws1.Rows[0];

                    wsr01.Cells[0].Value = "SOTK";
                    wsr01.Cells[1].Value = "MA_LH";
                    wsr01.Cells[2].Value = "MA_HQ";
                    wsr01.Cells[3].Value = "NAMDK";
                    wsr01.Cells[4].Value = "NGAY_DK"; 
                    wsr01.Cells[5].Value = "MA_DV"; // 
                    wsr01.Cells[6].Value = "MA_DVUT";
                    wsr01.Cells[7].Value = "DV_DT";
                    wsr01.Cells[8].Value = "MA_PTVT";
                    wsr01.Cells[9].Value = "SH_PTVT";
                   
                    wsr01.Cells[10].Value = "NGAYDEN";
                    wsr01.Cells[11].Value = "VAN_DON";                  
                    wsr01.Cells[12].Value = "SO_GP";
                    wsr01.Cells[13].Value = "NGAY_GP";
                    wsr01.Cells[14].Value = "NGAY_HHGP";
                    wsr01.Cells[15].Value = "SO_HD";
                    wsr01.Cells[16].Value = "TYGIA_USD";
                    wsr01.Cells[17].Value = "NGAY_HD";
                    wsr01.Cells[18].Value = "NGAY_HHHD";
                    wsr01.Cells[19].Value = "NUOC_XK";
                    wsr01.Cells[20].Value = "NUOC_NK";
                   // wsr01.Cells[24].Value = "MA_GH";
                    wsr01.Cells[21].Value = "SOHANG";
                    wsr01.Cells[22].Value = "MA_PTTT";

                    wsr01.Cells[23].Value = "MA_NT";
                    wsr01.Cells[24].Value = "TYGIA_VND";
                    wsr01.Cells[25].Value = "LEPHI_HQ";                   
                    wsr01.Cells[26].Value = "TENCH";                   
                    wsr01.Cells[27].Value = "DDIEM_KH";
                   // wsr01.Cells[36].Value = "PTHUC_KH";                  
                    wsr01.Cells[28].Value = "PHI_BH";
                    wsr01.Cells[29].Value = "PHI_VC";                    
                    wsr01.Cells[30].Value = "TONGTGKB";
                    wsr01.Cells[31].Value = "TONGTGTT";
                    wsr01.Cells[32].Value = "TR_LUONG";                   
                    wsr01.Cells[33].Value = "SO_KIEN";
                    wsr01.Cells[34].Value = "SO_CONTAINER";                  
                    wsr01.Cells[35].Value = "PLUONG";                    
                    wsr01.Cells[36].Value = "THANHKHOAN";
                    wsr01.Cells[37].Value = "NGAY_THN_THX";                   
                    wsr01.Cells[38].Value = "SO_CONTAINER40";                  
                    wsr01.Cells[39].Value = "SO_HDTM";
                    wsr01.Cells[40].Value = "NGAY_HDTM";
                   
                    wsr01.Cells[41].Value = "NGAY_VANDON";                    
                    wsr01.Cells[42].Value = "SO_PLTK";
                    wsr01.Cells[43].Value = "XUAT_NPL_SP";                 
                    wsr01.Cells[44].Value = "NGAY_HOANTHANH";               

                    wsr01.Cells[45].Value = "SO_VAN_DON";
                    wsr01.Cells[46].Value = "MA_DL_TTHQ";
                    wsr01.Cells[47].Value = "TEN_DL_TTHQ";
                    wsr01.Cells[48].Value = "CT_DVDT";
                    wsr01.Cells[59].Value = "QT_PTVT_ID";
                    wsr01.Cells[50].Value = "CUA_KHAU_ID";
                    wsr01.Cells[51].Value = "DKGH_ID";
                    wsr01.Cells[52].Value = "LOAITKGC";
                    wsr01.Cells[53].Value = "PHI_DO_HANG";
                    wsr01.Cells[54].Value = "PHI_KHAC";
                    wsr01.Cells[55].Value = "TT_TKHOAN";
                    wsr01.Cells[56].Value = "CHUNG_TU";
                    wsr01.Cells[57].Value = "TRANG_THAI";

                    DataTable dt1 = new ToKhaiMauDich().SelectDynamic(donViHaiQuanControl1.Ma, txtMaDN.Text.Trim()).Tables[0];
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        WorksheetRow wsr = ws1.Rows[i + 1];

                        wsr.Cells[0].Value = Convert.ToInt32(dt1.Rows[i]["SoToKhai"].ToString());
                        wsr.Cells[1].Value = dt1.Rows[i]["MaLoaiHinh"];
                        wsr.Cells[2].Value = dt1.Rows[i]["MaHaiQuan"];
                        wsr.Cells[3].Value = Convert.ToInt16(dt1.Rows[i]["NamDangKy"]);
                        if (dt1.Rows[i]["NgayDangKy"].ToString() != "")
                        wsr.Cells[4].Value = Convert.ToDateTime(dt1.Rows[i]["NgayDangKy"]);                        
                        wsr.Cells[5].Value = dt1.Rows[i]["MaDoanhNghiep"].ToString();//gl
                        wsr.Cells[6].Value = dt1.Rows[i]["MaDonViUT"].ToString();
                        wsr.Cells[7].Value = dt1.Rows[i]["TenDonViDoiTac"].ToString();
                        wsr.Cells[8].Value = dt1.Rows[i]["PTVT_ID"].ToString();
                        wsr.Cells[9].Value = dt1.Rows[i]["SoHieuPTVT"].ToString();
                        if (dt1.Rows[i]["NgayDenPTVT"].ToString() != "")
                        wsr.Cells[10].Value =  Convert.ToDateTime(dt1.Rows[i]["NgayDenPTVT"]);
                        wsr.Cells[11].Value =  dt1.Rows[i]["LoaiVanDon"].ToString();                      
                        wsr.Cells[12].Value = dt1.Rows[i]["SoGiayPhep"].ToString();
                        //
                        if (dt1.Rows[i]["NgayGiayPhep"].ToString() != "")
                            wsr.Cells[13].Value = Convert.ToDateTime(dt1.Rows[i]["NgayGiayPhep"]);

                        if (dt1.Rows[i]["NgayHetHanGiayPhep"].ToString() != "")
                            wsr.Cells[14].Value = Convert.ToDateTime(dt1.Rows[i]["NgayHetHanGiayPhep"]);

                        wsr.Cells[15].Value = dt1.Rows[i]["SoHopDong"].ToString();                                              
                        
                        wsr.Cells[16].Value = Convert.ToDecimal(dt1.Rows[i]["TyGiaUSD"]);

                        if (dt1.Rows[i]["NgayHopDong"].ToString() != "")
                            wsr.Cells[17].Value = Convert.ToDateTime(dt1.Rows[i]["NgayHopDong"]);
                        
                        if (dt1.Rows[i]["NgayHetHanHopDong"].ToString() != "")
                            wsr.Cells[18].Value = Convert.ToDateTime(dt1.Rows[i]["NgayHetHanHopDong"]);
                        wsr.Cells[19].Value = dt1.Rows[i]["NuocXK_ID"].ToString();
                        wsr.Cells[20].Value = dt1.Rows[i]["NuocNK_ID"].ToString(); 

                        wsr.Cells[21].Value = Convert.ToInt16(dt1.Rows[i]["SoHang"]);
                        wsr.Cells[22].Value = dt1.Rows[i]["PTTT_ID"].ToString();
                        wsr.Cells[23].Value = dt1.Rows[i]["NguyenTe_ID"].ToString();
                        wsr.Cells[24].Value = Convert.ToDecimal(dt1.Rows[i]["TyGiaTinhThue"]); 
                        wsr.Cells[25].Value = Convert.ToDecimal(dt1.Rows[i]["LePhiHaiQuan"]); 
                        wsr.Cells[26].Value = dt1.Rows[i]["TenChuHang"].ToString();
                        wsr.Cells[27].Value = dt1.Rows[i]["DiaDiemXepHang"].ToString();
                        wsr.Cells[28].Value = Convert.ToDecimal(dt1.Rows[i]["PhiBaoHiem"]);
                        wsr.Cells[29].Value = Convert.ToDecimal(dt1.Rows[i]["PhiVanChuyen"]); 
                        wsr.Cells[30].Value = Convert.ToDecimal(dt1.Rows[i]["TongTriGiaKhaiBao"]);
                        wsr.Cells[31].Value = Convert.ToDecimal(dt1.Rows[i]["TongTriGiaTinhThue"]);
                        wsr.Cells[32].Value = Convert.ToDecimal(dt1.Rows[i]["TrongLuong"]);
                        wsr.Cells[33].Value = Convert.ToDecimal(dt1.Rows[i]["SoKien"]);
                        wsr.Cells[34].Value = Convert.ToDecimal(dt1.Rows[i]["SoContainer20"]);
                        wsr.Cells[35].Value = dt1.Rows[i]["PhanLuong"].ToString();  
                        wsr.Cells[36].Value = dt1.Rows[i]["ThanhLy"].ToString();
                        if (dt1.Rows[i]["NGAY_THN_THX"].ToString() != "")
                            wsr.Cells[37].Value = Convert.ToDateTime(dt1.Rows[i]["NGAY_THN_THX"]);  
                        wsr.Cells[38].Value = Convert.ToDecimal(dt1.Rows[i]["SoContainer40"]);   
                        wsr.Cells[39].Value = dt1.Rows[i]["SoHoaDonThuongMai"].ToString();
                        if (dt1.Rows[i]["NgayHoaDonThuongMai"].ToString() != "")
                            wsr.Cells[40].Value = Convert.ToDateTime(dt1.Rows[i]["NgayHoaDonThuongMai"]);
                        if (dt1.Rows[i]["NgayVanDon"].ToString() != "")
                         wsr.Cells[41].Value = Convert.ToDateTime(dt1.Rows[i]["NgayVanDon"]);                   
                        wsr.Cells[42].Value = Convert.ToInt16(dt1.Rows[i]["SoLuongPLTK"]); 
                        wsr.Cells[43].Value = dt1.Rows[i]["Xuat_NPL_SP"].ToString();
                       // wsr.Cells[44].Value = dt1.Rows[i]["ThanhLy"];
                        if (dt1.Rows[i]["NgayHoanThanh"].ToString() != "")
                            wsr.Cells[44].Value = Convert.ToDateTime(dt1.Rows[i]["NgayHoanThanh"]);  
                        //
                        wsr.Cells[45].Value = dt1.Rows[i]["SoVanDon"].ToString();                      
                        wsr.Cells[46].Value = dt1.Rows[i]["MaDaiLyTTHQ"].ToString();
                        wsr.Cells[47].Value = dt1.Rows[i]["TenDaiLyTTHQ"].ToString();                       
                        wsr.Cells[48].Value = dt1.Rows[i]["ChiTietDonViDoiTac"].ToString(); 
                        wsr.Cells[49].Value = dt1.Rows[i]["QuocTichPTVT_ID"].ToString();                      
                        wsr.Cells[50].Value = dt1.Rows[i]["CuaKhau_ID"].ToString();
                        wsr.Cells[51].Value = dt1.Rows[i]["DKGH_ID"].ToString();                       
                        wsr.Cells[52].Value = dt1.Rows[i]["LoaiToKhaiGiaCong"].ToString();
                        wsr.Cells[53].Value = Convert.ToDecimal(dt1.Rows[i]["PhiXepDoHang"]);
                        wsr.Cells[54].Value = Convert.ToDecimal(dt1.Rows[i]["PhiKhac"]);
                    
                        wsr.Cells[55].Value = dt1.Rows[i]["TrangThaiThanhKhoan"].ToString();
                        wsr.Cells[56].Value = dt1.Rows[i]["ChungTu"].ToString();
                        wsr.Cells[57].Value = dt1.Rows[i]["TrangThai"];
                        //wsr.Cells[58].Value = Convert.ToDecimal(dt1.Rows[i]["PhiKhac"]);  
                    }
                }
                // Hàng mậu đã duyệt :
                if (chkHMD.Checked)
                {
                    wb.Worksheets.Add("HMD_D");
                    Worksheet ws1 = wb.Worksheets["HMD_D"];                                      
                    WorksheetRowCollection wsrc11 = ws1.Rows;
                    WorksheetRow wsr11 = ws1.Rows[0];

                    wsr11.Cells[0].Value = "SOTK";
                    wsr11.Cells[1].Value = "MA_LH";
                    wsr11.Cells[2].Value = "MA_HQ";
                    wsr11.Cells[3].Value = "NAMDK";
                    //wsr11.Cells[4].Value = "MA_HANG";
                    //wsr11.Cells[5].Value = "MA_NPLSP";
                    wsr11.Cells[4].Value = "SO_TTH";
                    
                    wsr11.Cells[5].Value = "MA_HS";
                    wsr11.Cells[6].Value = "MA_PHU";
                    wsr11.Cells[7].Value = "TEN_HANG";

                    //wsr11.Cells[10].Value = "DINH_MUC";
                    wsr11.Cells[8].Value = "NUOCXX_ID";
                    wsr11.Cells[9].Value = "DVT_ID";
                    wsr11.Cells[10].Value = "SO_LUONG";
                    wsr11.Cells[11].Value = "DGIA_KB";
                    wsr11.Cells[12].Value = "DGIA_TT";
                    //wsr11.Cells[16].Value = "MA_DG";
                    wsr11.Cells[13].Value = "TRIGIA_KB";
                    wsr11.Cells[14].Value = "TRIGIA_TT";
                    wsr11.Cells[15].Value = "TGKB_VND";

                    //wsr11.Cells[20].Value = "LOAITSXNK";
                    wsr11.Cells[16].Value = "TS_XNK";
                    wsr11.Cells[17].Value = "TS_TTDB";
                    wsr11.Cells[18].Value = "TS_VAT";
                    wsr11.Cells[19].Value = "THUE_XNK";                    
                    wsr11.Cells[20].Value = "THUE_TTDB";
                    wsr11.Cells[21].Value = "THUE_GTGT";
                    wsr11.Cells[27].Value = "PHU_THU";
                    wsr11.Cells[23].Value = "TL_THU_KHAC";
                    wsr11.Cells[24].Value = "TG_THU_KHAC";
                    wsr11.Cells[25].Value = "MIENTHUE";                    

                    //DataTable dt11 = new HangMauDich().SelectDynamic(donViHaiQuanControl1.Ma, txtMaDN.Text.Trim()).Tables[0];
                    DataTable dt11 = new HangMauDich().SelectDynamic(donViHaiQuanControl1.Ma).Tables[0];
                    for (int i = 0; i < dt11.Rows.Count; i++)
                    {
                        WorksheetRow wsr = ws1.Rows[i + 1];

                        wsr.Cells[0].Value = Convert.ToInt32(dt11.Rows[i]["SoToKhai"].ToString());
                        wsr.Cells[1].Value = dt11.Rows[i]["MaLoaiHinh"];
                        wsr.Cells[2].Value = dt11.Rows[i]["MaHaiQuan"];
                        wsr.Cells[3].Value = Convert.ToInt16(dt11.Rows[i]["NamDangKy"]); //
                        //wsr.Cells[4].Value = dt11.Rows[i]["MaHS"];
                       // wsr.Cells[5].Value = dt11.Rows[i]["MA_NPL_SP"].ToString(); 
                        wsr.Cells[4].Value = Convert.ToInt16(dt11.Rows[i]["SoThuTuHang"].ToString());  //
                        wsr.Cells[5].Value = dt11.Rows[i]["MaHS"]; //
                        wsr.Cells[6].Value = dt11.Rows[i]["MaPhu"]; //
                        wsr.Cells[7].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN (dt11.Rows[i]["TenHang"].ToString());
                       //wsr.Cells[10].Value = Convert.ToInt32(dt11.Rows[i]["DinhMuc"].ToString()); //
                        wsr.Cells[8].Value = dt11.Rows[i]["NuocXX_ID"].ToString();
                        wsr.Cells[9].Value = dt11.Rows[i]["DVT_ID"];
                        wsr.Cells[10].Value = Convert.ToDecimal(dt11.Rows[i]["SoLuong"]); //
                        wsr.Cells[11].Value = Convert.ToDecimal(dt11.Rows[i]["DonGiaKB"]);//
                        wsr.Cells[12].Value = Convert.ToDecimal(dt11.Rows[i]["DonGiaTT"]);//
                        wsr.Cells[13].Value = Convert.ToDecimal(dt11.Rows[i]["TriGiaKB"]);//
                        wsr.Cells[14].Value = Convert.ToDecimal(dt11.Rows[i]["TriGiaTT"]);//
                        wsr.Cells[15].Value = Convert.ToDecimal(dt11.Rows[i]["TriGiaKB_VND"]);//

                        wsr.Cells[16].Value = Convert.ToDecimal(dt11.Rows[i]["ThueSuatXNK"]);//
                        wsr.Cells[17].Value = Convert.ToDecimal(dt11.Rows[i]["ThueSuatTTDB"]);//
                        wsr.Cells[18].Value = Convert.ToDecimal(dt11.Rows[i]["ThueSuatGTGT"]);//

                        wsr.Cells[19].Value = Convert.ToDecimal(dt11.Rows[i]["ThueXNK"]);//
                        wsr.Cells[20].Value = Convert.ToDecimal(dt11.Rows[i]["ThueTTDB"]);//
                        wsr.Cells[21].Value = Convert.ToDecimal(dt11.Rows[i]["ThueGTGT"]);//
                        wsr.Cells[22].Value = Convert.ToDecimal(dt11.Rows[i]["PhuThu"]);//
                        wsr.Cells[23].Value = Convert.ToDecimal(dt11.Rows[i]["TriGiaThuKhac"]);//
                        wsr.Cells[24].Value = Convert.ToDecimal(dt11.Rows[i]["TyLeThuKhac"]);//
                        wsr.Cells[25].Value = Convert.ToInt16(dt11.Rows[i]["MienThue"]);//
                                                

                    }
                }
               
                saveFileDialog1.ShowDialog();
                string fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
                //saveFileDialog1.FileName = "ECS_Softech_Thong_Tin_" + DateTime.Now.Date.ToString("dd-MM-yyyy") + ".xls";
                //saveFileDialog1.AddExtension = true;
                //saveFileDialog1.DefaultExt = "*.xls";
                //saveFileDialog1.Filter = "Tệp tin Excel (*.xls)|*.xls|Tất cả (*.*)|*.*";

                //wb.Save(saveFileDialog1.FileName);

                DialogResult dr = MessageBox.Show("Xuất dữ liệu thành công. Có muốn mở file không?","Thông báo",MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                      System.Diagnostics.Process.Start(fileName); 
                    //System.Diagnostics.Process.Start(saveFileDialog1.FileName);
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
                btnExport.Enabled = true;
            }
            finally
            {
                btnExport.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }

        private void ExportToExcelForm_Load(object sender, EventArgs e)
        {
            txtMaDN.Text = GlobalSettings.MA_DON_VI;
            txtTenDN.Text = GlobalSettings.TEN_DON_VI;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            ccFromDate.Enabled = true;
            ccToDate.Enabled = false;
        }
    }
}