﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;


namespace Company.Interface.KDT.GC
{
    public partial class SanPhamCungUngEditForm : BaseForm
    {
        private DataTable dt = new DataTable();
        public SanPhanCungUng SPCU = new SanPhanCungUng();
        public BKCungUngDangKy BKCU = new BKCungUngDangKy();
        public SanPhamCungUngEditForm()
        {
            InitializeComponent();
        }

        private HangMauDich GetHMDCuaToKhaiByMaHang(string maHang, ToKhaiMauDich TKMD)
        {
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                if (hmd.MaPhu.Trim().ToUpper() == maHang.Trim().ToUpper())
                    return hmd;
            }
            return null;
        }

        private HangChuyenTiep GetHCTCuaToKhaiByMaHang(string maHang, ToKhaiChuyenTiep TKCT)
        {
            foreach (HangChuyenTiep hmd in TKCT.HCTCollection)
            {
                if (hmd.MaHang.Trim().ToUpper() == maHang.Trim().ToUpper())
                    return hmd;
            }
            return null;
        }
        private void SanPhamCungUngForm_Load(object sender, EventArgs e)
        {
            txtLuongXK.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            txtLuongCU.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            dgList.RootTable.Columns["LuongTCU"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.RootTable.Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;

            txtMaSP.Text = this.SPCU.MaSanPham;
            HopDong HD = new HopDong();
            if (this.BKCU.TKMD_ID > 0)
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = this.BKCU.TKMD_ID;
                TKMD.Load();
                TKMD.LoadHMDCollection();
                HangMauDich hmd = GetHMDCuaToKhaiByMaHang(txtMaSP.Text, TKMD);
                txtTenSP.Text = hmd.TenHang;
                txtDVT.Text = DonViTinh_GetName(hmd.DVT_ID);
                txtLuongXK.Value = hmd.SoLuong;
                txtLuongCU.Value = this.SPCU.LuongCUSanPham;
                HD.ID = TKMD.IDHopDong;
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = this.BKCU.TKCT_ID;
                TKCT = ToKhaiChuyenTiep.Load(this.BKCU.TKCT_ID);
                TKCT.LoadHCTCollection();
                HangChuyenTiep hct = GetHCTCuaToKhaiByMaHang(txtMaSP.Text, TKCT);
                txtTenSP.Text = hct.TenHang;
                txtDVT.Text = DonViTinh_GetName(hct.ID_DVT);
                txtLuongXK.Value = hct.SoLuong;
                txtLuongCU.Value = this.SPCU.LuongCUSanPham;
                HD.ID = TKCT.IDHopDong;
            }

            HD = HopDong.Load(HD.ID);

            this.dt = new Company.GC.BLL.GC.DinhMuc().getDinhMuc(HD.ID, txtMaSP.Text.Trim());

            DataColumn[] cols = new DataColumn[4];
            cols[0] = new DataColumn("LuongTCU", typeof(decimal));
            cols[1] = new DataColumn("DonGia", typeof(double));
            cols[2] = new DataColumn("TriGia", typeof(double));
            cols[3] = new DataColumn("HinhThucCU", typeof(string));
            dt.Columns.AddRange(cols);
            foreach (DataRow dr in this.dt.Rows)
            {
                NguyenPhuLieuCungUng npl = CheckExistNPLInSPCU(dr["MaNguyenPhuLieu"].ToString());
                if (npl == null)
                {
                    dr["LuongTCU"] = Convert.ToDecimal(dr["DinhMucSuDung"]) * (100 + Convert.ToDecimal(dr["TyLeHaoHut"])) / 100 * Convert.ToDecimal(txtLuongCU.Value);
                    dr["DonGia"] = 0;
                    dr["HinhThucCU"] = "Mua VN";
                }
                else
                {
                    dr["LuongTCU"] = npl.LuongCung;
                    dr["DonGia"] = npl.DonGia;
                    dr["HinhThucCU"] = npl.HinhThuCungUng;
                    dr["DinhMucSuDung"] = npl.DinhMucCungUng;
                    dr["TyLeHaoHut"] = npl.TyLeHH;
                    dr["HinhThucCU"] = npl.HinhThuCungUng;
                }
            }

            dgList.DataSource = this.dt;
            uiButton2.Visible = false;
            if (BKCU.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET && BKCU.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                uiButton2.Visible = true;
            }
        }
        private void setErro()
        {
            epError.Clear();
            epError.SetError(txtMaSP, null);
            epError.SetError(txtTenSP, null);
            epError.SetError(txtDVT, null);

        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtLuongCU.Text) > Convert.ToDecimal(txtLuongXK.Text))
            {
                showMsg("MSG_WRN21");
                //MLMessages("Lượng cung ứng không được lớn hơn lượng xuất khẩu.","MSG_WRN21","", false);
                return;
            }

            cvError.Validate();
            if (!cvError.IsValid) return;
            this.SPCU.NPLCungUngCollection = new NguyenPhuLieuCungUngCollection();
            this.SPCU.MaSanPham = txtMaSP.Text;
            this.SPCU.LuongCUSanPham = Convert.ToDecimal(txtLuongCU.Value);
            GridEXRow[] rows = dgList.GetCheckedRows();
            NguyenPhuLieuCungUng nplCungUng = new NguyenPhuLieuCungUng();
            foreach (GridEXRow row in rows)
            {
                DataRowView dv = (DataRowView)row.DataRow;
                NguyenPhuLieuCungUng npl = new NguyenPhuLieuCungUng();
                npl.MaNguyenPhuLieu = dv["MaNguyenPhuLieu"].ToString();
                npl.LuongCung = Convert.ToDecimal(dv["LuongTCU"]);
                npl.DonGia = Convert.ToDouble(dv["DonGia"]);
                npl.TriGia = npl.DonGia * Convert.ToDouble(npl.LuongCung);
                npl.DinhMucCungUng = Convert.ToDouble(dv["DinhMucSuDung"]);
                npl.TyLeHH = Convert.ToDouble(dv["TyLeHaoHut"]);
                npl.HinhThuCungUng = dv["HinhThucCU"].ToString();
                if (npl.LuongCung > 0)
                    this.SPCU.NPLCungUngCollection.Add(npl);

            }
            if (this.SPCU.NPLCungUngCollection.Count <= 0) showMsg("MSG_WRN22");
            else
            {
                this.setErro();
                this.Close();
            }
        }
        private NguyenPhuLieuCungUng CheckExistNPLInSPCU(string MaNPL)
        {
            foreach (NguyenPhuLieuCungUng npl in this.SPCU.NPLCungUngCollection)
                if (npl.MaNguyenPhuLieu.Trim().ToLower() == MaNPL.Trim().ToLower()) return npl;
            return null;
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                NguyenPhuLieuCungUng npl = CheckExistNPLInSPCU(e.Row.Cells["MaNguyenPhuLieu"].Text);
                if (npl != null)
                {
                    e.Row.CheckState = RowCheckState.Checked;
                }

            }
        }

        private void txtLuongCU_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtLuongCU.Text) > Convert.ToDecimal(txtLuongXK.Text))
            {
                showMsg("MSG_WRN21");
                //MLMessages("Lượng cung ứng không được lớn hơn lượng xuất khẩu.", "MSG_WRN21", "", false);
                return;
            }
            foreach (DataRow dr in this.dt.Rows)
            {
                if (dr.RowState != DataRowState.Deleted)
                    dr["LuongTCU"] = Convert.ToDecimal(dr["DinhMucSuDung"]) * (100 + Convert.ToDecimal(dr["TyLeHaoHut"])) / 100 * Convert.ToDecimal(txtLuongCU.Value);

            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}

