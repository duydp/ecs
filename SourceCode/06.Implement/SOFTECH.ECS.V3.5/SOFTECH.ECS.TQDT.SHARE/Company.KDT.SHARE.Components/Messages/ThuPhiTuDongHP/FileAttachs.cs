﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class FileAttachs
    {
        /// <summary>
        /// Product
        /// </summary>
       [XmlElement("File")]
       public List<File> File { get; set; }
    }
}
