﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Feedback.Components;

namespace Company.Interface.Feedback
{
    public partial class LinhVucUserControl : UserControl
    {
        private DataTable dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];

        public LinhVucUserControl()
        {
            InitializeComponent();
        }

        private void LinhVucUserControl_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            try
            {
                if (dt == null)
                    dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];

                if (dt != null)
                {
                    DataView dv = dt.Copy().DefaultView;
                    dv.RowFilter = "HienThi = 1";
                    dv.Sort = "ThuTu asc";

                    dgList.DataSource = dv;

                    CheckDefault();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public void ReLoad()
        {
            try
            {
                Company.KDT.SHARE.Components.Feedback.Components.FeedbackGlobalSettings.LamMoiDanhMucLinhVuc();

                dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];

                LoadData();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// Lay ma thong tin Linh vuc dang duoc chon va check
        /// </summary>
        /// <returns></returns>
        public string GetMa()
        {
            foreach (Janus.Windows.GridEX.GridEXRow item in dgList.GetCheckedRows())
            {
                if (item.RowType == Janus.Windows.GridEX.RowType.Record
                    && item.CheckState == Janus.Windows.GridEX.RowCheckState.Checked)
                {
                    return item.Cells["Ma"].Value.ToString();
                }
            }

            return "";
        }

        /// <summary>
        /// Luon luon kiem tra check 1 row mac dinh tren luoi
        /// </summary>
        private void CheckDefault()
        {
            //Neu da co check roi => thoat
            if (IsHaveRowChecked())
                return;

            //Neu chua co row nao check mac dinh => thuc hien
            DataRow[] rows = dt.Select("MacDinh = 1");

            if (rows.Length == 0)
                dgList.GetRow(0).CheckState = Janus.Windows.GridEX.RowCheckState.Checked;
            else
            {
                foreach (Janus.Windows.GridEX.GridEXRow item in dgList.GetRows())
                {
                    if (item.RowType == Janus.Windows.GridEX.RowType.Record
                        && !item.Cells["Ma"].Value.Equals(rows[0]["Ma"]))
                    {
                        item.CheckState = Janus.Windows.GridEX.RowCheckState.Unchecked;
                    }
                }

                dgList.GetRow(0).CheckState = Janus.Windows.GridEX.RowCheckState.Checked;
            }
        }

        private bool IsHaveRowChecked()
        {
            foreach (Janus.Windows.GridEX.GridEXRow item in dgList.GetRows())
            {
                if (item.RowType == Janus.Windows.GridEX.RowType.Record
                    && item.CheckState == Janus.Windows.GridEX.RowCheckState.Checked)
                    return true;
            }

            return false;
        }

        public delegate void LinhVucRowCheckStateChangedEventHandler(object sender, Janus.Windows.GridEX.RowCheckStateChangeEventArgs e);
        public event LinhVucRowCheckStateChangedEventHandler LinhVucRowCheckStateChanged;
        private void dgList_RowCheckStateChanged(object sender, Janus.Windows.GridEX.RowCheckStateChangeEventArgs e)
        {
            try
            {
                foreach (Janus.Windows.GridEX.GridEXRow item in dgList.GetCheckedRows())
                {
                    if (item.RowType == Janus.Windows.GridEX.RowType.Record
                        && !item.Cells["Ma"].Value.Equals(e.Row.Cells["Ma"].Value))
                    {
                        item.CheckState = Janus.Windows.GridEX.RowCheckState.Unchecked;
                    }
                }

                CheckDefault();

                if (LinhVucRowCheckStateChanged != null)
                {
                    LinhVucRowCheckStateChanged(sender, e);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}
