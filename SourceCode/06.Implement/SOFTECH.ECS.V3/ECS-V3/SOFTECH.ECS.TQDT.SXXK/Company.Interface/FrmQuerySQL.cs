﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface
{
    public partial class FrmQuerySQL : BaseForm
    {
        private DataSet _data = new DataSet();

        public FrmQuerySQL()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtQuery.Clear();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            Query();
        }

        private void Query()
        {
            Cursor = Cursors.WaitCursor;

            //Clear content.
            txtMessage.Clear();
            _data.Clear();

            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    txtQuery.SelectAll();
                    string sql = GetQueryString();

                    SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

                    if (sql.Trim().ToUpper().StartsWith("SELECT"))
                    {
                        _data = db.ExecuteDataSet(dbCommand);
                        grdResult.DataSource = _data;
                        grdResult.DataMember = "Table";

                        txtMessage.Text = grdResult.RowCount + " record(s).";
                    }
                    else if (sql.Trim().ToUpper().StartsWith("INSERT") ||
                        sql.Trim().ToUpper().StartsWith("UPDATE") ||
                        sql.Trim().ToUpper().StartsWith("DELETE"))
                    {
                        txtMessage.Text = db.ExecuteNonQuery(dbCommand) + " record(s).";
                    }

                    //Active Tab Grid after have result.
                    uiTabPage1.Selected = true;

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    txtMessage.Text = ex.Message + "\r\n\n" + ex.StackTrace;
                    uiTabPage2.Selected = true;
                }
                finally
                {
                    connection.Close();

                    Cursor = Cursors.Default;
                }
            }
        }

        private string GetQueryString()
        {
            string[] lines = txtQuery.Lines;
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < lines.Length; i++)
            {
                if (!lines[i].StartsWith("--"))
                {
                    sb.AppendLine(lines[i]);
                }
            }

            return sb.ToString();
        }

        private void FrmQuerySQL_Load(object sender, EventArgs e)
        {
            //this.btnQuery.Image = Company.Interface.Properties.Resources.bt_play;
            //this.btnClear.Image = Company.Interface.Properties.Resources.clear;
            //this.btnClose.Image = Company.Interface.Properties.Resources.exit;

            uiGroupBox1.Height = (int)(((double)(this.Parent.Height) * (double)75) / 100);
        }

        private void txtQuery_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F5)
            {
                Query();
            }
            else if (e.Control && e.KeyCode == Keys.A)
            {
                txtQuery.SelectAll();
            }

        }
    }
}
