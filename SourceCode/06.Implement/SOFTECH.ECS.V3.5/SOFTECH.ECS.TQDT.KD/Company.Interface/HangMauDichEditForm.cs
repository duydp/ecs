﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL.Utils;
using System.Data;

namespace Company.Interface
{
    public partial class HangMauDichEditForm : BaseForm
    {
        public bool IsEdited = false;
        public bool IsDeleted = false;
        //-----------------------------------------------------------------------------------------
        public HangMauDich HMD = new HangMauDich();
        public List<HangMauDich> collection = new List<HangMauDich>();
        public string LoaiHangHoa = "";
        public string NhomLoaiHinh = string.Empty;

        public double TyGiaTT;
        public string MaNguyenTe;
        public string MaNuocXX;

        //-----------------------------------------------------------------------------------------				
        // Tính thuế
        private decimal luong;
        private double dgnt;
        private double tgnt;

        private double tgtt_nk;
        private double tgtt_ttdb;
        private double tgtt_gtgt;
        private double clg;

        private double ts_nk;
        private double ts_ttdb;
        private double ts_gtgt;
        private double tl_clg;

        private double tt_nk;
        private double tt_ttdb;
        private double tt_gtgt;
        private double st_clg;
        //-----------------------------------------------------------------------------------------
        private string MaHangOld = "";
        private NguyenPhuLieuRegistedForm NPLRegistedForm;
        private SanPhamRegistedForm SPRegistedForm;
        private decimal LuongCon = 0;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------
        //DATLMQ bổ sung biến phục vụ cho việc tự động lưu nội dung tờ khai sửa đổi bổ sung 04/03/2011
        public static string maHS_Old = string.Empty;
        public static string tenHang_Old = string.Empty;
        public static string maHang_Old = string.Empty;
        public static string xuatXu_Old = string.Empty;
        public static decimal soLuong_Old;
        public static string dvt_Old = string.Empty;
        public static double donGiaNT_Old;
        public static double triGiaNT_Old;

        public HangMauDichEditForm()
        {
            InitializeComponent();
        }

        private void khoitao_GiaoDien()
        {

        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
        }

        private double tinhthue()
        {

            this.tgnt = Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));

            this.tgtt_nk = this.tgnt * this.TyGiaTT;

            if (!chkThueTuyetDoi.Checked)
                this.tt_nk = this.tgtt_nk * this.ts_nk;
            else
            {
                double dgntTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Value);
                this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDouble(luong));
            }
            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;

            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private double tinhthue2()
        {
            this.dgnt = Convert.ToDouble(Convert.ToDecimal(txtDGNT.Text));
            this.luong = Convert.ToDecimal(txtLuong.Text);
            if (!chkThueTuyetDoi.Checked)
            {
                this.ts_nk = Convert.ToDouble(txtTS_NK.Value) / 100;
            }

            this.ts_ttdb = Convert.ToDouble(txtTS_TTDB.Value) / 100;
            this.ts_gtgt = Convert.ToDouble(txtTS_GTGT.Value) / 100;
            this.tl_clg = Convert.ToDouble(txtTL_CLG.Value) / 100;

            this.tgnt = Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));

            this.tgtt_nk = Convert.ToDouble(txtTGTT_NK.Value);
            if (tgtt_nk == 0)
            {
                tgtt_nk = tgnt * TyGiaTT;
            }
            if (!chkThueTuyetDoi.Checked)
                this.tt_nk = this.tgtt_nk * this.ts_nk;
            else
            {
                double dgntTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Value);
                this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDouble(luong));
            }
            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);

            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private double tinhthue3()
        {
            this.tgnt = this.dgnt * Convert.ToDouble(this.luong);
            decimal tgntkophi = (decimal)tgnt;
            if (TKMD.DKGH_ID.Trim() == "CNF" || TKMD.DKGH_ID.Trim() == "CFR") tgntkophi = (decimal)this.tgnt - TKMD.PhiVanChuyen;

            this.tgtt_nk = (double)tgntkophi * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        //-----------------------------------------------------------------------------------------

        private void HangMauDichEditForm_Load(object sender, EventArgs e)
        {
            revMaHS.ValidationExpression = @"\d{8,10}";

            ctrNuocXX.Ma = this.HMD.NuocXX_ID;
            this.khoitao_DuLieuChuan();
            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            if (this.NhomLoaiHinh[0].ToString().Equals("X"))
                label16.Text = setText("Trị giá tính thuế XK", "Export Tax assessment");

            // Tính lại thuế.
            //this.HMD.TinhThue(this.TyGiaTT);
            lblTyGiaTT.Text = "  " + this.TyGiaTT.ToString("N") + " (VNĐ)";

            // Bind data.
            txtMaHang.Text = this.HMD.MaPhu;
            txtTenHang.Text = this.HMD.TenHang;
            txtMaHS.Text = this.HMD.MaHS;
            cbDonViTinh.SelectedValue = this.HMD.DVT_ID;
            //luu lai ma hang
            MaHangOld = this.HMD.MaPhu;

            maHS_Old = this.HMD.MaHS;
            tenHang_Old = this.HMD.TenHang;
            maHang_Old = this.HMD.MaPhu;
            xuatXu_Old = this.HMD.NuocXX_ID;
            soLuong_Old = this.HMD.SoLuong;
            dvt_Old = this.HMD.DVT_ID;
            triGiaNT_Old = this.HMD.TriGiaKB;
            donGiaNT_Old = this.HMD.DonGiaKB;

            txtDGNT.Value = this.dgnt = this.HMD.DonGiaKB;
            txtLuong.Value = this.luong = this.HMD.SoLuong;
            txtTGNT.Value = this.HMD.TriGiaKB;
            txtTriGiaKB.Value = this.HMD.TriGiaKB_VND;
            txtTGTT_NK.Value = this.HMD.TriGiaTT;
            txtTS_NK.Value = this.ts_nk = this.HMD.ThueSuatXNK;
            txtTS_TTDB.Value = this.ts_ttdb = this.HMD.ThueSuatTTDB;
            txtTS_GTGT.Value = this.ts_gtgt = this.HMD.ThueSuatGTGT;
            txtTL_CLG.Value = this.tl_clg = this.HMD.TyLeThuKhac;
            ctrNuocXX.Ma = this.HMD.NuocXX_ID;
            chkHangFOC.Checked = this.HMD.FOC;
            txtPhuThu.Value = this.HMD.PhuThu;
            txtTyLeThuKhac.Value = this.HMD.TyLeThuKhac;
            chkThueTuyetDoi.Checked = this.HMD.ThueTuyetDoi;
            this.ts_nk = this.HMD.ThueSuatXNK / 100;
            this.ts_ttdb = this.HMD.ThueSuatTTDB / 100;
            this.ts_gtgt = this.HMD.ThueSuatGTGT / 100;
            this.tl_clg = this.HMD.TyLeThuKhac / 100;
            if (chkThueTuyetDoi.Checked)
            {
                txtDonGiaTuyetDoi.Value = HMD.DonGiaTuyetDoi;
            }
            if (HMD.MienThue == 1)
                chkMienThue.Checked = true;
            this.tinhthue2();
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
                btnGhi.Enabled = false;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                btnGhi.Enabled = true;
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            this.luong = Convert.ToDecimal(txtLuong.Value);
            this.tinhthue();
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            this.dgnt = Convert.ToDouble(txtDGNT.Value);
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDouble(txtTS_NK.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDouble(txtTS_TTDB.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDouble(txtTS_GTGT.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDouble(txtTL_CLG.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        //-----------------------------------------------------------------------------------------------


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.NhomLoaiHinh[0].ToString())
            {
                case "N":
                    if (this.NPLRegistedForm == null)
                        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.isBrower = true;
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                    this.NPLRegistedForm.ShowDialog(this);
                    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "X":
                    if (this.SPRegistedForm == null)
                        this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.isBowrer = true;
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog(this);
                    if (this.SPRegistedForm.SanPhamSelected.Ma != "")
                    {
                        txtMaHang.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                    }
                    break;
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "Invalid input value"));
                return;
            }
            //if (checkMaHangExit(txtMaHang.Text.Trim()))
            //{
            //    ShowMessage("Đã có hàng này trong danh sách.", false);
            //    return;
            //}

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                if (maHS_Old != txtMaHS.Text)
                    ToKhaiMauDichForm.maHS_Edit = txtMaHS.Text;
                else
                    ToKhaiMauDichForm.maHS_Edit = maHS_Old;
                if (tenHang_Old != txtTenHang.Text)
                    ToKhaiMauDichForm.tenHang_Edit = txtTenHang.Text;
                else
                    ToKhaiMauDichForm.tenHang_Edit = tenHang_Old;
                if (maHang_Old != txtMaHang.Text)
                    ToKhaiMauDichForm.maHang_Edit = txtMaHang.Text;
                else
                    ToKhaiMauDichForm.maHang_Edit = maHang_Old;
                if (xuatXu_Old != ctrNuocXX.Ma)
                    ToKhaiMauDichForm.xuatXu_Edit = ctrNuocXX.Ma;
                else
                    ToKhaiMauDichForm.xuatXu_Edit = xuatXu_Old;
                if (dvt_Old != cbDonViTinh.SelectedValue.ToString())
                    ToKhaiMauDichForm.dvt_Edit = cbDonViTinh.SelectedValue.ToString();
                else
                    ToKhaiMauDichForm.dvt_Edit = dvt_Old;
                if (soLuong_Old != Convert.ToDecimal(txtLuong.Text))
                    ToKhaiMauDichForm.soLuong_Edit = Convert.ToDecimal(txtLuong.Text);
                else
                    ToKhaiMauDichForm.soLuong_Edit = soLuong_Old;
                if (donGiaNT_Old != Convert.ToDouble(txtDGNT.Text))
                    ToKhaiMauDichForm.dongiaNT_Edit = Convert.ToDouble(txtDGNT.Text);
                else
                    ToKhaiMauDichForm.dongiaNT_Edit = donGiaNT_Old;
                if (triGiaNT_Old != Convert.ToDouble(txtTGNT.Text))
                    ToKhaiMauDichForm.trigiaNT_Edit = Convert.ToDouble(txtTGNT.Text);
                else
                    ToKhaiMauDichForm.trigiaNT_Edit = triGiaNT_Old;

                try
                {
                    this.HMD.MaHS = txtMaHS.Text;
                    this.HMD.MaPhu = txtMaHang.Text.Trim();
                    this.HMD.TenHang = txtTenHang.Text.Trim();
                    this.HMD.NuocXX_ID = ctrNuocXX.Ma;
                    this.HMD.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                    this.HMD.SoLuong = Convert.ToDecimal(txtLuong.Text);
                    this.HMD.DonGiaKB = Convert.ToDouble(txtDGNT.Text);
                    this.HMD.DonGiaTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Text);
                    this.HMD.TriGiaKB = Convert.ToDouble(txtTGNT.Text);
                    this.HMD.TriGiaTT = Convert.ToDouble(txtTGTT_NK.Text);
                    this.HMD.TriGiaKB_VND = Convert.ToDouble(txtTriGiaKB.Value);
                    this.HMD.ThueSuatXNK = Convert.ToDouble(txtTS_NK.Text);
                    this.HMD.ThueSuatTTDB = Convert.ToDouble(txtTS_TTDB.Text);
                    this.HMD.ThueSuatGTGT = Convert.ToDouble(txtTS_GTGT.Text);
                    this.HMD.TriGiaThuKhac = Convert.ToDouble(txtTien_CLG.Text);
                    this.HMD.ThueXNK = Math.Round(Convert.ToDouble(txtTienThue_NK.Text), 0);
                    this.HMD.ThueTTDB = Math.Round(Convert.ToDouble(txtTienThue_TTDB.Text), 0);
                    this.HMD.ThueGTGT = Math.Round(Convert.ToDouble(txtTienThue_GTGT.Text), 0);
                    this.HMD.DonGiaTT = HMD.TriGiaTT / Convert.ToDouble(HMD.SoLuong);
                    this.HMD.TyLeThuKhac = Convert.ToDouble(txtTL_CLG.Text);
                    this.HMD.FOC = chkHangFOC.Checked;
                    this.HMD.ThueTuyetDoi = chkThueTuyetDoi.Checked;
                    this.HMD.ThueSuatXNKGiam = txtTSXNKGiam.Text.Trim();
                    this.HMD.ThueSuatTTDBGiam = txtTSTTDBGiam.Text.Trim();
                    this.HMD.ThueSuatVATGiam = txtTSVatGiam.Text.Trim();
                    this.HMD.PhuThu = Convert.ToDouble(txtPhuThu.Value);
                    this.HMD.TyLeThuKhac = Convert.ToDouble(txtTyLeThuKhac.Value);


                    if (chkMienThue.Checked)
                        this.HMD.MienThue = 1;
                    else
                        this.HMD.MienThue = 0;

                    // Tính thuế.            
                    this.IsEdited = true;
                    txtMaHang.Text = "";
                    txtTenHang.Text = "";
                    txtMaHS.Text = "";
                    txtLuong.Value = 0;
                    txtDGNT.Value = 0;
                    LuongCon = 0;
                    this.Close();
                }
                catch (Exception ex)
                {
                    ShowMessage(setText("Dữ liệu của bạn không hợp lệ ", "Invalid input value") + ex.Message, false);
                }
            }

            else
            {
                try
                {
                    this.HMD.MaHS = txtMaHS.Text;
                    this.HMD.MaPhu = txtMaHang.Text.Trim();
                    this.HMD.TenHang = txtTenHang.Text.Trim();
                    this.HMD.NuocXX_ID = ctrNuocXX.Ma;
                    this.HMD.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                    this.HMD.SoLuong = Convert.ToDecimal(txtLuong.Text);
                    this.HMD.DonGiaKB = Convert.ToDouble(txtDGNT.Text);
                    this.HMD.DonGiaTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Text);
                    this.HMD.TriGiaKB = Convert.ToDouble(txtTGNT.Text);
                    this.HMD.TriGiaTT = Convert.ToDouble(txtTGTT_NK.Text);
                    this.HMD.TriGiaKB_VND = Convert.ToDouble(txtTriGiaKB.Value);
                    this.HMD.ThueSuatXNK = Convert.ToDouble(txtTS_NK.Text);
                    this.HMD.ThueSuatTTDB = Convert.ToDouble(txtTS_TTDB.Text);
                    this.HMD.ThueSuatGTGT = Convert.ToDouble(txtTS_GTGT.Text);
                    this.HMD.ThueXNK = Math.Round(Convert.ToDouble(txtTienThue_NK.Text), 0);
                    this.HMD.ThueTTDB = Math.Round(Convert.ToDouble(txtTienThue_TTDB.Text), 0);
                    this.HMD.ThueGTGT = Math.Round(Convert.ToDouble(txtTienThue_GTGT.Text), 0);
                    this.HMD.DonGiaTT = HMD.TriGiaTT / Convert.ToDouble(HMD.SoLuong);
                    this.HMD.TyLeThuKhac = Convert.ToDouble(txtTL_CLG.Text);
                    this.HMD.FOC = chkHangFOC.Checked;
                    this.HMD.ThueTuyetDoi = chkThueTuyetDoi.Checked;
                    this.HMD.ThueSuatXNKGiam = txtTSXNKGiam.Text.Trim();
                    this.HMD.ThueSuatTTDBGiam = txtTSTTDBGiam.Text.Trim();
                    this.HMD.ThueSuatVATGiam = txtTSVatGiam.Text.Trim();
                    this.HMD.TriGiaThuKhac = Convert.ToDouble(txtTien_CLG.Text);
                    this.HMD.PhuThu = Convert.ToDouble(txtPhuThu.Value);
                    this.HMD.TyLeThuKhac = Convert.ToDouble(txtTyLeThuKhac.Value);
                    if (chkMienThue.Checked)
                        this.HMD.MienThue = 1;
                    else
                        this.HMD.MienThue = 0;

                    // Tính thuế.            
                    this.IsEdited = true;
                    txtMaHang.Text = "";
                    txtTenHang.Text = "";
                    txtMaHS.Text = "";
                    txtLuong.Value = 0;
                    txtDGNT.Value = 0;
                    LuongCon = 0;
                    this.Close();
                }
                catch (Exception ex)
                {
                    ShowMessage(setText("Dữ liệu của bạn không hợp lệ ", "Invalid input value") + ex.Message, false);
                }
            }
        }
        private void TinhTriGiaTinhThue()
        {
            double Phi = Convert.ToDouble(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            double TongTriGiaHang = 0;
            foreach (HangMauDich hmd in collection)
            {
                TongTriGiaHang += hmd.TriGiaKB;
            }
            if (TongTriGiaHang == 0) return;
            double TriGiaTTMotDong = Phi / TongTriGiaHang;
            foreach (HangMauDich hmd in collection)
            {
                hmd.TriGiaTT = Math.Round((TriGiaTTMotDong * hmd.TriGiaKB + hmd.TriGiaKB) * Convert.ToDouble(TyGiaTT), 0);
                hmd.DonGiaTT = hmd.TriGiaTT / Convert.ToDouble(hmd.SoLuong);
                hmd.ThueXNK = (hmd.TriGiaTT * hmd.ThueSuatXNK) / 100;
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, 0);
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB) * hmd.ThueSuatGTGT / 100, 0);
                hmd.TriGiaThuKhac = Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, 0);
            }

        }
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in collection)
            {
                if (hmd.MaPhu.Trim() == maHang && maHang != MaHangOld) return true;
            }
            return false;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.IsDeleted = true;
            this.Close();
        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            if (NhomLoaiHinh.StartsWith("N"))
            {
                Company.KD.BLL.SXXK.HangHoaNhap npl = new Company.KD.BLL.SXXK.HangHoaNhap();
                npl.Ma = txtMaHang.Text.Trim();
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = "";
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                }

            }
            else
            {
                Company.KD.BLL.SXXK.HangHoaXuat sp = new Company.KD.BLL.SXXK.HangHoaXuat();
                sp.Ma = txtMaHang.Text.Trim();
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = "";
                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    cbDonViTinh.SelectedValue = sp.DVT_ID;
                }

            }
        }

        private void txtDGNT_Click(object sender, EventArgs e)
        {

        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //    txtMaHS.Focus();
            //    epError.SetIconPadding(txtMaHS, -8);
            //    epError.SetError(txtMaHS,setText("Mã HS không hợp lệ.","invalid input value"));
            //}
            //else
            //{
            //    epError.SetError(txtMaHS, string.Empty);
            //}
            //string MoTa = MaHS.CheckExist(txtMaHS.Text);
            //if (MoTa == "")
            //{
            //    epError.SetIconPadding(txtMaHS, -8);
            //    epError.SetError(txtMaHS,setText( "Mã HS không có trong danh mục mã HS.","This value is not exist"));
            //}
            //else
            //{
            //    epError.SetError(txtMaHS, string.Empty);
            //}
        }

        public void chkMienThue_CheckedChanged(object sender, EventArgs e)
        {
            grbThue.Enabled = !chkMienThue.Checked;
            txtTS_NK.Value = 0;
            txtTS_TTDB.Value = 0;
            txtTS_GTGT.Value = 0;
            txtTL_CLG.Value = 0;
            if (TKMD.MaLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void chkThueTuyetDoi_CheckedChanged(object sender, EventArgs e)
        {
            if (chkThueTuyetDoi.Checked)
            {
                txtTS_NK.Enabled = false;
                txtTienThue_NK.ReadOnly = false;
                txtDonGiaTuyetDoi.Enabled = true;

                this.txtTienThue_NK.BackColor = Color.White;
            }
            else
            {
                txtTS_NK.Enabled = true;
                txtTienThue_NK.ReadOnly = true;
                txtDonGiaTuyetDoi.Enabled = false;
                this.txtTienThue_NK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            }
        }

        private void txtTienThue_NK_Leave(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtDonGiaTuyetDoi_Leave(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void txtTGNT_Leave(object sender, EventArgs e)
        {
            txtDGNT.Value = Convert.ToDecimal(txtTGNT.Value) / Convert.ToDecimal(txtLuong.Value);
            this.tinhthue2();
            txtPhuThu.Value = (Convert.ToDecimal(txtTGTT_NK.Value) / 100) * Convert.ToDecimal(txtTyLeThuKhac.Value);

        }

        private void chkMienThue_CheckedChanged_1(object sender, EventArgs e)
        {
            grbThue.Enabled = !chkMienThue.Checked;
            txtTS_NK.Value = 0;
            txtTS_TTDB.Value = 0;
            txtTS_GTGT.Value = 0;
            txtTL_CLG.Value = 0;
            if (TKMD.MaLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void txtTyLeThuKhac_Leave(object sender, EventArgs e)
        {
            txtPhuThu.Value = (Convert.ToDecimal(txtTGTT_NK.Value) / 100) * Convert.ToDecimal(txtTyLeThuKhac.Value);

        }

        private void txtPhuThu_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtTGTT_NK.Value) == 0) return;
            txtTyLeThuKhac.Value = Convert.ToDecimal(txtPhuThu.Value) / ((Convert.ToDecimal(txtTGTT_NK.Value) / 100));
        }
    }
}
