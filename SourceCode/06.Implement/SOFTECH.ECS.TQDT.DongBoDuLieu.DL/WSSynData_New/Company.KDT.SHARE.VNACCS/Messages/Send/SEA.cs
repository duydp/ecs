﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class  SEA : BasicVNACC
    {
        public List<SEA_HANGHOA> ListHangHoa { get; set; }

        public StringBuilder BuilEdiMessagesSEA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<SEA>(StrBuild, true, GlobalVNACC.PathConfig, EnumNghiepVu.SEA);
            foreach (SEA_HANGHOA item in ListHangHoa)
            {
                item.BuildEdiMessages<SEA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "SEA_HANGHOA");
            }
            return str;
        }
    }
}
