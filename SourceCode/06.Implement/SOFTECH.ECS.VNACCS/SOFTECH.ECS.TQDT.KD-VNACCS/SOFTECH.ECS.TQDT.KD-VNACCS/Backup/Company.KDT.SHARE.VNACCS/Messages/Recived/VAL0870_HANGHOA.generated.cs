using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAL0870_HANGHOA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute RNO { get; set; }
public PropertiesAttribute SNO { get; set; }
public PropertiesAttribute CMD { get; set; }
public PropertiesAttribute CMN { get; set; }
public PropertiesAttribute ORC { get; set; }
public PropertiesAttribute ORN { get; set; }
public PropertiesAttribute KBN { get; set; }
public PropertiesAttribute QN1 { get; set; }
public PropertiesAttribute QT1 { get; set; }
public PropertiesAttribute QN2 { get; set; }
public PropertiesAttribute QT2 { get; set; }
public PropertiesAttribute TNK { get; set; }
public PropertiesAttribute TNC { get; set; }
public PropertiesAttribute TSC { get; set; }
public PropertiesAttribute KKT { get; set; }
public PropertiesAttribute KKC { get; set; }
public PropertiesAttribute NSR { get; set; }
public PropertiesAttribute NBG { get; set; }
public PropertiesAttribute NBC { get; set; }

public VAL0870_HANGHOA()
        {
RNO = new PropertiesAttribute(3, typeof(int));
SNO = new PropertiesAttribute(40, typeof(string));
CMD = new PropertiesAttribute(12, typeof(string));
CMN = new PropertiesAttribute(200, typeof(string));
ORC = new PropertiesAttribute(2, typeof(string));
ORN = new PropertiesAttribute(30, typeof(string));
KBN = new PropertiesAttribute(35, typeof(string));
QN1 = new PropertiesAttribute(12, typeof(decimal),2);
QT1 = new PropertiesAttribute(4, typeof(string));
QN2 = new PropertiesAttribute(12, typeof(decimal),2);
QT2 = new PropertiesAttribute(4, typeof(string));
TNK = new PropertiesAttribute(9, typeof(decimal),4);
TNC = new PropertiesAttribute(3, typeof(string));
TSC = new PropertiesAttribute(3, typeof(string));
KKT = new PropertiesAttribute(20, typeof(decimal),4);
KKC = new PropertiesAttribute(3, typeof(string));
NSR = new PropertiesAttribute(20, typeof(string));
NBG = new PropertiesAttribute(9, typeof(decimal),4);
NBC = new PropertiesAttribute(3, typeof(string));
TongSoByte = 462;
}

}public partial class EnumGroupID
    {
}

}
