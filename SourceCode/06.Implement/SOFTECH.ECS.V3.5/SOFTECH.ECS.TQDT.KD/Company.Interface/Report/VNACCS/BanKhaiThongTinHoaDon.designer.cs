namespace Company.Interface.Report.VNACCS
{
    partial class BanKhaiThongTinHoaDon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhanHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiXuatNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemLapHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDaiLyHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayLapHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTChiThiHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanBietSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayThangNam = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhuongThucThanhToan = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.lblNuocSoTaiNguoiNhanGui = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDienThoaiNguoiNhanGui = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiNhanGui4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiNhanGui3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiNhanGui2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiNhanGui1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBuuChinhNguoiNhanGui = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiNhanGui = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiNhanGui = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiLapHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDienThoaiXuatNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiXuatNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBuuChinhXuatNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiXuatNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiXuatNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lblTenDiaDiemXepHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDiaDiemTrungChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDiaDiemTrungChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoiKyXepHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDiaDiemXepHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHieuChuyenDi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaKyHieu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenPhuongTienVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.lblSoTiepNhanHoaDon,
            this.lblPhanLoaiXuatNhapKhau,
            this.xrLabel6,
            this.xrLabel9,
            this.lblSoHoaDon,
            this.xrLabel12,
            this.xrLabel14,
            this.lblDiaDiemLapHoaDon,
            this.xrLabel16,
            this.lblMaDaiLyHaiQuan,
            this.lblNgayLapHoaDon});
            this.Detail.HeightF = 58.57766F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(9.999999F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Số tiếp nhận hóa đơn điện tử";
            // 
            // lblSoTiepNhanHoaDon
            // 
            this.lblSoTiepNhanHoaDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTiepNhanHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(173.54F, 0F);
            this.lblSoTiepNhanHoaDon.Name = "lblSoTiepNhanHoaDon";
            this.lblSoTiepNhanHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhanHoaDon.SizeF = new System.Drawing.SizeF(122.9999F, 11F);
            this.lblSoTiepNhanHoaDon.StylePriority.UseFont = false;
            this.lblSoTiepNhanHoaDon.StylePriority.UseTextAlignment = false;
            this.lblSoTiepNhanHoaDon.Text = "XXXXXXXXX1XE";
            this.lblSoTiepNhanHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiXuatNhapKhau
            // 
            this.lblPhanLoaiXuatNhapKhau.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiXuatNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(173.54F, 12F);
            this.lblPhanLoaiXuatNhapKhau.Name = "lblPhanLoaiXuatNhapKhau";
            this.lblPhanLoaiXuatNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiXuatNhapKhau.SizeF = new System.Drawing.SizeF(37.49994F, 11F);
            this.lblPhanLoaiXuatNhapKhau.StylePriority.UseFont = false;
            this.lblPhanLoaiXuatNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiXuatNhapKhau.Text = "X";
            this.lblPhanLoaiXuatNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(9.999999F, 12F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "Phân loại xuất nhập khẩu";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(451.4115F, 12F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(126.7917F, 11F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Mã đại lý Hải quan";
            // 
            // lblSoHoaDon
            // 
            this.lblSoHoaDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(173.54F, 24F);
            this.lblSoHoaDon.Name = "lblSoHoaDon";
            this.lblSoHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHoaDon.SizeF = new System.Drawing.SizeF(267.75F, 11F);
            this.lblSoHoaDon.StylePriority.UseFont = false;
            this.lblSoHoaDon.StylePriority.UseTextAlignment = false;
            this.lblSoHoaDon.Text = "XXXXXXXXX1XXXXXXXX2XXXXXXXX3XXXXE";
            this.lblSoHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(9.999999F, 24F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(151.54F, 10.99999F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.Text = "Số hóa đơn";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(451.4115F, 24F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(126.7917F, 11F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "Ngày lập hóa đơn";
            // 
            // lblDiaDiemLapHoaDon
            // 
            this.lblDiaDiemLapHoaDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaDiemLapHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(173.54F, 36.0417F);
            this.lblDiaDiemLapHoaDon.Name = "lblDiaDiemLapHoaDon";
            this.lblDiaDiemLapHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemLapHoaDon.SizeF = new System.Drawing.SizeF(267.75F, 11.00001F);
            this.lblDiaDiemLapHoaDon.StylePriority.UseFont = false;
            this.lblDiaDiemLapHoaDon.StylePriority.UseTextAlignment = false;
            this.lblDiaDiemLapHoaDon.Text = "XXXXXXXXX1XXXXXXXX2XXXXXXXX3XXXXE";
            this.lblDiaDiemLapHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(9.999999F, 36.0417F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(151.54F, 11.00001F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Địa điểm lập hóa đơn";
            // 
            // lblMaDaiLyHaiQuan
            // 
            this.lblMaDaiLyHaiQuan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDaiLyHaiQuan.LocationFloat = new DevExpress.Utils.PointFloat(578.2032F, 12F);
            this.lblMaDaiLyHaiQuan.Name = "lblMaDaiLyHaiQuan";
            this.lblMaDaiLyHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDaiLyHaiQuan.SizeF = new System.Drawing.SizeF(53.24994F, 11F);
            this.lblMaDaiLyHaiQuan.StylePriority.UseFont = false;
            this.lblMaDaiLyHaiQuan.StylePriority.UseTextAlignment = false;
            this.lblMaDaiLyHaiQuan.Text = "XXXXE";
            this.lblMaDaiLyHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayLapHoaDon
            // 
            this.lblNgayLapHoaDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNgayLapHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(578.2032F, 24.00001F);
            this.lblNgayLapHoaDon.Name = "lblNgayLapHoaDon";
            this.lblNgayLapHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayLapHoaDon.SizeF = new System.Drawing.SizeF(90.04163F, 11F);
            this.lblNgayLapHoaDon.StylePriority.UseFont = false;
            this.lblNgayLapHoaDon.StylePriority.UseTextAlignment = false;
            this.lblNgayLapHoaDon.Text = "dd/MM/yyyy";
            this.lblNgayLapHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
            this.TopMargin.HeightF = 95F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(225F, 62.5F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(312.4916F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Bản khai thông tin hóa đơn / kiện hàng";
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 21.18053F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader,
            this.GroupFooter1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail1.HeightF = 10.25F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(23.95834F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(731.04F, 10.25F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTChiThiHQ,
            this.lblPhanBietSo,
            this.lblSo,
            this.lblNgayThangNam});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.29285708836146762;
            // 
            // lblSTTChiThiHQ
            // 
            this.lblSTTChiThiHQ.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSTTChiThiHQ.Name = "lblSTTChiThiHQ";
            this.lblSTTChiThiHQ.StylePriority.UseFont = false;
            this.lblSTTChiThiHQ.StylePriority.UseTextAlignment = false;
            this.lblSTTChiThiHQ.Text = "1";
            this.lblSTTChiThiHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTTChiThiHQ.Weight = 0.15349557923395052;
            // 
            // lblPhanBietSo
            // 
            this.lblPhanBietSo.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanBietSo.Name = "lblPhanBietSo";
            this.lblPhanBietSo.StylePriority.UseFont = false;
            this.lblPhanBietSo.StylePriority.UseTextAlignment = false;
            this.lblPhanBietSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblPhanBietSo.Weight = 0.51580110175602578;
            // 
            // lblSo
            // 
            this.lblSo.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSo.Multiline = true;
            this.lblSo.Name = "lblSo";
            this.lblSo.StylePriority.UseFont = false;
            this.lblSo.StylePriority.UseTextAlignment = false;
            this.lblSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSo.Weight = 1.6303071583136548;
            // 
            // lblNgayThangNam
            // 
            this.lblNgayThangNam.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblNgayThangNam.Multiline = true;
            this.lblNgayThangNam.Name = "lblNgayThangNam";
            this.lblNgayThangNam.StylePriority.UseFont = false;
            this.lblNgayThangNam.StylePriority.UseTextAlignment = false;
            this.lblNgayThangNam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblNgayThangNam.Weight = 0.93978424142492378;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.ReportHeader.HeightF = 11F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(58.59801F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(696.4057F, 11F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.6742694161155007;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Mã phân loại";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell4.Weight = 0.53391280329186641;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Số";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell5.Weight = 1.6875531097060681;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Ngày tháng năm";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell6.Weight = 0.97280844971122948;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.lblPhuongThucThanhToan});
            this.GroupFooter1.HeightF = 17.45612F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(11.46F, 2F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(126.7917F, 11F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Phương thức thanh toán";
            // 
            // lblPhuongThucThanhToan
            // 
            this.lblPhuongThucThanhToan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhuongThucThanhToan.LocationFloat = new DevExpress.Utils.PointFloat(138.2517F, 2F);
            this.lblPhuongThucThanhToan.Name = "lblPhuongThucThanhToan";
            this.lblPhuongThucThanhToan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhuongThucThanhToan.SizeF = new System.Drawing.SizeF(256.9415F, 10.99999F);
            this.lblPhuongThucThanhToan.StylePriority.UseFont = false;
            this.lblPhuongThucThanhToan.StylePriority.UseTextAlignment = false;
            this.lblPhuongThucThanhToan.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXE";
            this.lblPhuongThucThanhToan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.DetailReport2});
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel42,
            this.xrLabel63,
            this.xrLabel38,
            this.xrLabel37,
            this.xrLabel35,
            this.xrLabel33,
            this.xrLine5,
            this.lblNuocSoTaiNguoiNhanGui,
            this.lblSoDienThoaiNguoiNhanGui,
            this.lblDiaChiNguoiNhanGui4,
            this.lblDiaChiNguoiNhanGui3,
            this.lblDiaChiNguoiNhanGui2,
            this.lblDiaChiNguoiNhanGui1,
            this.lblMaBuuChinhNguoiNhanGui,
            this.lblTenNguoiNhanGui,
            this.lblMaNguoiNhanGui,
            this.xrLabel30,
            this.xrLabel29,
            this.lblNguoiLapHoaDon,
            this.xrLabel28,
            this.lblSoDienThoaiXuatNhapKhau,
            this.lblDiaChiXuatNhapKhau,
            this.xrLabel25,
            this.xrLabel24,
            this.lblMaBuuChinhXuatNhapKhau,
            this.xrLabel22,
            this.lblTenNguoiXuatNhapKhau,
            this.xrLabel20,
            this.lblMaNguoiXuatNhapKhau,
            this.xrLabel17});
            this.Detail2.HeightF = 255F;
            this.Detail2.Name = "Detail2";
            // 
            // xrLabel42
            // 
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(41.67402F, 237.5F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(119.8659F, 10.99998F);
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.Text = "Mã nước sở tại";
            // 
            // xrLabel63
            // 
            this.xrLabel63.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(41.67408F, 226.5F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(119.8659F, 10.99998F);
            this.xrLabel63.StylePriority.UseFont = false;
            this.xrLabel63.Text = "Số điện thoại";
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(43.1341F, 178.5F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(119.866F, 11.00003F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.Text = "Địa chỉ";
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(43.13408F, 166F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(119.866F, 11.00002F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.Text = "Mã bưu chính";
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(43.13408F, 153.5F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(119.866F, 11.00002F);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.Text = "Tên";
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(43.13408F, 141F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(119.866F, 11.00002F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.Text = "Mã";
            // 
            // xrLine5
            // 
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.4999695F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(737F, 2F);
            // 
            // lblNuocSoTaiNguoiNhanGui
            // 
            this.lblNuocSoTaiNguoiNhanGui.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNuocSoTaiNguoiNhanGui.LocationFloat = new DevExpress.Utils.PointFloat(175F, 237.5F);
            this.lblNuocSoTaiNguoiNhanGui.Name = "lblNuocSoTaiNguoiNhanGui";
            this.lblNuocSoTaiNguoiNhanGui.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNuocSoTaiNguoiNhanGui.SizeF = new System.Drawing.SizeF(37.49998F, 11.00002F);
            this.lblNuocSoTaiNguoiNhanGui.StylePriority.UseFont = false;
            this.lblNuocSoTaiNguoiNhanGui.StylePriority.UseTextAlignment = false;
            this.lblNuocSoTaiNguoiNhanGui.Text = "XE";
            this.lblNuocSoTaiNguoiNhanGui.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoDienThoaiNguoiNhanGui
            // 
            this.lblSoDienThoaiNguoiNhanGui.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDienThoaiNguoiNhanGui.LocationFloat = new DevExpress.Utils.PointFloat(175F, 226.5F);
            this.lblSoDienThoaiNguoiNhanGui.Name = "lblSoDienThoaiNguoiNhanGui";
            this.lblSoDienThoaiNguoiNhanGui.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDienThoaiNguoiNhanGui.SizeF = new System.Drawing.SizeF(122.9999F, 11F);
            this.lblSoDienThoaiNguoiNhanGui.StylePriority.UseFont = false;
            this.lblSoDienThoaiNguoiNhanGui.StylePriority.UseTextAlignment = false;
            this.lblSoDienThoaiNguoiNhanGui.Text = "XXXXXXXXX1XE";
            this.lblSoDienThoaiNguoiNhanGui.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiNhanGui4
            // 
            this.lblDiaChiNguoiNhanGui4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaChiNguoiNhanGui4.LocationFloat = new DevExpress.Utils.PointFloat(452.8716F, 215.5001F);
            this.lblDiaChiNguoiNhanGui4.Name = "lblDiaChiNguoiNhanGui4";
            this.lblDiaChiNguoiNhanGui4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiNhanGui4.SizeF = new System.Drawing.SizeF(272.1284F, 11.00002F);
            this.lblDiaChiNguoiNhanGui4.StylePriority.UseFont = false;
            this.lblDiaChiNguoiNhanGui4.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiNhanGui4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblDiaChiNguoiNhanGui4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiNhanGui3
            // 
            this.lblDiaChiNguoiNhanGui3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaChiNguoiNhanGui3.LocationFloat = new DevExpress.Utils.PointFloat(175F, 215.5001F);
            this.lblDiaChiNguoiNhanGui3.Name = "lblDiaChiNguoiNhanGui3";
            this.lblDiaChiNguoiNhanGui3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiNhanGui3.SizeF = new System.Drawing.SizeF(267.75F, 11.00002F);
            this.lblDiaChiNguoiNhanGui3.StylePriority.UseFont = false;
            this.lblDiaChiNguoiNhanGui3.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiNhanGui3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblDiaChiNguoiNhanGui3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiNhanGui2
            // 
            this.lblDiaChiNguoiNhanGui2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaChiNguoiNhanGui2.LocationFloat = new DevExpress.Utils.PointFloat(175F, 189.5F);
            this.lblDiaChiNguoiNhanGui2.Name = "lblDiaChiNguoiNhanGui2";
            this.lblDiaChiNguoiNhanGui2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiNhanGui2.SizeF = new System.Drawing.SizeF(549.9999F, 26.00008F);
            this.lblDiaChiNguoiNhanGui2.StylePriority.UseFont = false;
            this.lblDiaChiNguoiNhanGui2.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiNhanGui2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblDiaChiNguoiNhanGui2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiNhanGui1
            // 
            this.lblDiaChiNguoiNhanGui1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaChiNguoiNhanGui1.LocationFloat = new DevExpress.Utils.PointFloat(175F, 178.5F);
            this.lblDiaChiNguoiNhanGui1.Name = "lblDiaChiNguoiNhanGui1";
            this.lblDiaChiNguoiNhanGui1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiNhanGui1.SizeF = new System.Drawing.SizeF(122.9999F, 11F);
            this.lblDiaChiNguoiNhanGui1.StylePriority.UseFont = false;
            this.lblDiaChiNguoiNhanGui1.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiNhanGui1.Text = "XXXXXXXXX1XE";
            this.lblDiaChiNguoiNhanGui1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaBuuChinhNguoiNhanGui
            // 
            this.lblMaBuuChinhNguoiNhanGui.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaBuuChinhNguoiNhanGui.LocationFloat = new DevExpress.Utils.PointFloat(175F, 166F);
            this.lblMaBuuChinhNguoiNhanGui.Name = "lblMaBuuChinhNguoiNhanGui";
            this.lblMaBuuChinhNguoiNhanGui.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBuuChinhNguoiNhanGui.SizeF = new System.Drawing.SizeF(122.9999F, 11F);
            this.lblMaBuuChinhNguoiNhanGui.StylePriority.UseFont = false;
            this.lblMaBuuChinhNguoiNhanGui.StylePriority.UseTextAlignment = false;
            this.lblMaBuuChinhNguoiNhanGui.Text = "XXXXXXE";
            this.lblMaBuuChinhNguoiNhanGui.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenNguoiNhanGui
            // 
            this.lblTenNguoiNhanGui.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenNguoiNhanGui.LocationFloat = new DevExpress.Utils.PointFloat(175F, 153.5F);
            this.lblTenNguoiNhanGui.Name = "lblTenNguoiNhanGui";
            this.lblTenNguoiNhanGui.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNguoiNhanGui.SizeF = new System.Drawing.SizeF(550F, 11.00002F);
            this.lblTenNguoiNhanGui.StylePriority.UseFont = false;
            this.lblTenNguoiNhanGui.StylePriority.UseTextAlignment = false;
            this.lblTenNguoiNhanGui.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTenNguoiNhanGui.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaNguoiNhanGui
            // 
            this.lblMaNguoiNhanGui.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaNguoiNhanGui.LocationFloat = new DevExpress.Utils.PointFloat(175F, 141F);
            this.lblMaNguoiNhanGui.Name = "lblMaNguoiNhanGui";
            this.lblMaNguoiNhanGui.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNguoiNhanGui.SizeF = new System.Drawing.SizeF(122.9999F, 11F);
            this.lblMaNguoiNhanGui.StylePriority.UseFont = false;
            this.lblMaNguoiNhanGui.StylePriority.UseTextAlignment = false;
            this.lblMaNguoiNhanGui.Text = "XXXXXXXXX1XE";
            this.lblMaNguoiNhanGui.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(11.46006F, 130F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.Text = "Người nhận (người gửi)";
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(11.46006F, 111.5F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.Text = "Người lập hóa đơn";
            // 
            // lblNguoiLapHoaDon
            // 
            this.lblNguoiLapHoaDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNguoiLapHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(175F, 111.5F);
            this.lblNguoiLapHoaDon.Name = "lblNguoiLapHoaDon";
            this.lblNguoiLapHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiLapHoaDon.SizeF = new System.Drawing.SizeF(550F, 11.00002F);
            this.lblNguoiLapHoaDon.StylePriority.UseFont = false;
            this.lblNguoiLapHoaDon.StylePriority.UseTextAlignment = false;
            this.lblNguoiLapHoaDon.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblNguoiLapHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(43.1341F, 97.99997F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(119.8659F, 10.99998F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.Text = "Số điện thoại";
            // 
            // lblSoDienThoaiXuatNhapKhau
            // 
            this.lblSoDienThoaiXuatNhapKhau.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDienThoaiXuatNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(175F, 97.99999F);
            this.lblSoDienThoaiXuatNhapKhau.Name = "lblSoDienThoaiXuatNhapKhau";
            this.lblSoDienThoaiXuatNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDienThoaiXuatNhapKhau.SizeF = new System.Drawing.SizeF(186.75F, 10.99999F);
            this.lblSoDienThoaiXuatNhapKhau.StylePriority.UseFont = false;
            this.lblSoDienThoaiXuatNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblSoDienThoaiXuatNhapKhau.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDienThoaiXuatNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiXuatNhapKhau
            // 
            this.lblDiaChiXuatNhapKhau.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaChiXuatNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(175F, 71.99998F);
            this.lblDiaChiXuatNhapKhau.Name = "lblDiaChiXuatNhapKhau";
            this.lblDiaChiXuatNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiXuatNhapKhau.SizeF = new System.Drawing.SizeF(550F, 25.99999F);
            this.lblDiaChiXuatNhapKhau.StylePriority.UseFont = false;
            this.lblDiaChiXuatNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblDiaChiXuatNhapKhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiXuatNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(43.13405F, 71.99998F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(119.866F, 10.99999F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.Text = "Địa chỉ";
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(43.13403F, 59.49999F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(119.866F, 10.99999F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.Text = "Mã bưu chính";
            // 
            // lblMaBuuChinhXuatNhapKhau
            // 
            this.lblMaBuuChinhXuatNhapKhau.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaBuuChinhXuatNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(175F, 59.49999F);
            this.lblMaBuuChinhXuatNhapKhau.Name = "lblMaBuuChinhXuatNhapKhau";
            this.lblMaBuuChinhXuatNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBuuChinhXuatNhapKhau.SizeF = new System.Drawing.SizeF(122.9999F, 11F);
            this.lblMaBuuChinhXuatNhapKhau.StylePriority.UseFont = false;
            this.lblMaBuuChinhXuatNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblMaBuuChinhXuatNhapKhau.Text = "XXXXXXE";
            this.lblMaBuuChinhXuatNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(43.13403F, 35.00002F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(119.866F, 10.99999F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.Text = "Tên";
            // 
            // lblTenNguoiXuatNhapKhau
            // 
            this.lblTenNguoiXuatNhapKhau.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenNguoiXuatNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(175F, 33.49999F);
            this.lblTenNguoiXuatNhapKhau.Multiline = true;
            this.lblTenNguoiXuatNhapKhau.Name = "lblTenNguoiXuatNhapKhau";
            this.lblTenNguoiXuatNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNguoiXuatNhapKhau.SizeF = new System.Drawing.SizeF(550F, 26F);
            this.lblTenNguoiXuatNhapKhau.StylePriority.UseFont = false;
            this.lblTenNguoiXuatNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblTenNguoiXuatNhapKhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiXuatNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(43.13403F, 22.5F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(119.866F, 10.99999F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.Text = "Mã";
            // 
            // lblMaNguoiXuatNhapKhau
            // 
            this.lblMaNguoiXuatNhapKhau.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaNguoiXuatNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(175F, 22.5F);
            this.lblMaNguoiXuatNhapKhau.Name = "lblMaNguoiXuatNhapKhau";
            this.lblMaNguoiXuatNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNguoiXuatNhapKhau.SizeF = new System.Drawing.SizeF(122.9999F, 11F);
            this.lblMaNguoiXuatNhapKhau.StylePriority.UseFont = false;
            this.lblMaNguoiXuatNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblMaNguoiXuatNhapKhau.Text = "XXXXXXXXX1-XXE";
            this.lblMaNguoiXuatNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(11.46001F, 10.00001F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.Text = "Người xuất nhập khẩu";
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3});
            this.DetailReport2.Level = 0;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lblTenDiaDiemXepHang,
            this.lblTenDiaDiemDoHang,
            this.lblTenDiaDiemTrungChuyen,
            this.lblMaDiaDiemTrungChuyen,
            this.xrLabel58,
            this.xrLabel57,
            this.lblMaDiaDiemDoHang,
            this.lblThoiKyXepHang,
            this.lblMaDiaDiemXepHang,
            this.xrLabel53,
            this.xrLabel52,
            this.xrLabel51,
            this.lblSoHieuChuyenDi,
            this.lblMaKyHieu,
            this.xrLabel47,
            this.xrLabel48,
            this.lblTenPhuongTienVanChuyen});
            this.Detail3.HeightF = 125.6945F;
            this.Detail3.Name = "Detail3";
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(737F, 2F);
            // 
            // lblTenDiaDiemXepHang
            // 
            this.lblTenDiaDiemXepHang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenDiaDiemXepHang.LocationFloat = new DevExpress.Utils.PointFloat(226.46F, 42.00006F);
            this.lblTenDiaDiemXepHang.Name = "lblTenDiaDiemXepHang";
            this.lblTenDiaDiemXepHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDiaDiemXepHang.SizeF = new System.Drawing.SizeF(181.1533F, 11.00002F);
            this.lblTenDiaDiemXepHang.StylePriority.UseFont = false;
            this.lblTenDiaDiemXepHang.StylePriority.UseTextAlignment = false;
            this.lblTenDiaDiemXepHang.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTenDiaDiemXepHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenDiaDiemDoHang
            // 
            this.lblTenDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenDiaDiemDoHang.LocationFloat = new DevExpress.Utils.PointFloat(226.4601F, 54.50001F);
            this.lblTenDiaDiemDoHang.Name = "lblTenDiaDiemDoHang";
            this.lblTenDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDiaDiemDoHang.SizeF = new System.Drawing.SizeF(181.1532F, 11.00001F);
            this.lblTenDiaDiemDoHang.StylePriority.UseFont = false;
            this.lblTenDiaDiemDoHang.StylePriority.UseTextAlignment = false;
            this.lblTenDiaDiemDoHang.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTenDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenDiaDiemTrungChuyen
            // 
            this.lblTenDiaDiemTrungChuyen.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenDiaDiemTrungChuyen.LocationFloat = new DevExpress.Utils.PointFloat(226.4601F, 67.00003F);
            this.lblTenDiaDiemTrungChuyen.Name = "lblTenDiaDiemTrungChuyen";
            this.lblTenDiaDiemTrungChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDiaDiemTrungChuyen.SizeF = new System.Drawing.SizeF(252.7101F, 11.00002F);
            this.lblTenDiaDiemTrungChuyen.StylePriority.UseFont = false;
            this.lblTenDiaDiemTrungChuyen.StylePriority.UseTextAlignment = false;
            this.lblTenDiaDiemTrungChuyen.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblTenDiaDiemTrungChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDiaDiemTrungChuyen
            // 
            this.lblMaDiaDiemTrungChuyen.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDiaDiemTrungChuyen.LocationFloat = new DevExpress.Utils.PointFloat(175F, 67.00003F);
            this.lblMaDiaDiemTrungChuyen.Name = "lblMaDiaDiemTrungChuyen";
            this.lblMaDiaDiemTrungChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDiaDiemTrungChuyen.SizeF = new System.Drawing.SizeF(51.46001F, 11.00003F);
            this.lblMaDiaDiemTrungChuyen.StylePriority.UseFont = false;
            this.lblMaDiaDiemTrungChuyen.StylePriority.UseTextAlignment = false;
            this.lblMaDiaDiemTrungChuyen.Text = "XXXXE";
            this.lblMaDiaDiemTrungChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel58
            // 
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 67.00003F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.Text = "Địa điểm trung chuyển";
            // 
            // xrLabel57
            // 
            this.xrLabel57.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 54.50002F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel57.StylePriority.UseFont = false;
            this.xrLabel57.Text = "Địa điểm dở hàng";
            // 
            // lblMaDiaDiemDoHang
            // 
            this.lblMaDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDiaDiemDoHang.LocationFloat = new DevExpress.Utils.PointFloat(175F, 54.50001F);
            this.lblMaDiaDiemDoHang.Name = "lblMaDiaDiemDoHang";
            this.lblMaDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDiaDiemDoHang.SizeF = new System.Drawing.SizeF(51.46001F, 11.00003F);
            this.lblMaDiaDiemDoHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemDoHang.StylePriority.UseTextAlignment = false;
            this.lblMaDiaDiemDoHang.Text = "XXXXE";
            this.lblMaDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThoiKyXepHang
            // 
            this.lblThoiKyXepHang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblThoiKyXepHang.LocationFloat = new DevExpress.Utils.PointFloat(644.9133F, 42.00006F);
            this.lblThoiKyXepHang.Name = "lblThoiKyXepHang";
            this.lblThoiKyXepHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoiKyXepHang.SizeF = new System.Drawing.SizeF(93.54694F, 11.00002F);
            this.lblThoiKyXepHang.StylePriority.UseFont = false;
            this.lblThoiKyXepHang.StylePriority.UseTextAlignment = false;
            this.lblThoiKyXepHang.Text = "dd/MM/yyyy";
            this.lblThoiKyXepHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDiaDiemXepHang
            // 
            this.lblMaDiaDiemXepHang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDiaDiemXepHang.LocationFloat = new DevExpress.Utils.PointFloat(175F, 42.00006F);
            this.lblMaDiaDiemXepHang.Name = "lblMaDiaDiemXepHang";
            this.lblMaDiaDiemXepHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDiaDiemXepHang.SizeF = new System.Drawing.SizeF(51.46001F, 11.00003F);
            this.lblMaDiaDiemXepHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemXepHang.StylePriority.UseTextAlignment = false;
            this.lblMaDiaDiemXepHang.Text = "XXXXE";
            this.lblMaDiaDiemXepHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(9.999968F, 42.00005F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.Text = "Đại điểm xếp hàng";
            // 
            // xrLabel52
            // 
            this.xrLabel52.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(481.3733F, 42.00006F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.Text = "Thời hạn xếp hàng";
            // 
            // xrLabel51
            // 
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(481.3733F, 29.50004F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.Text = "Số hiệu chuyến đi";
            // 
            // lblSoHieuChuyenDi
            // 
            this.lblSoHieuChuyenDi.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHieuChuyenDi.LocationFloat = new DevExpress.Utils.PointFloat(644.9133F, 29.50004F);
            this.lblSoHieuChuyenDi.Name = "lblSoHieuChuyenDi";
            this.lblSoHieuChuyenDi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHieuChuyenDi.SizeF = new System.Drawing.SizeF(93.54694F, 11.00001F);
            this.lblSoHieuChuyenDi.StylePriority.UseFont = false;
            this.lblSoHieuChuyenDi.StylePriority.UseTextAlignment = false;
            this.lblSoHieuChuyenDi.Text = "XXXXXXXXXE";
            this.lblSoHieuChuyenDi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaKyHieu
            // 
            this.lblMaKyHieu.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaKyHieu.LocationFloat = new DevExpress.Utils.PointFloat(175F, 2.000003F);
            this.lblMaKyHieu.Name = "lblMaKyHieu";
            this.lblMaKyHieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaKyHieu.SizeF = new System.Drawing.SizeF(561.9999F, 26.00003F);
            this.lblMaKyHieu.StylePriority.UseFont = false;
            this.lblMaKyHieu.StylePriority.UseTextAlignment = false;
            this.lblMaKyHieu.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            this.lblMaKyHieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(10F, 2.000015F);
            this.xrLabel47.Multiline = true;
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.Text = "Mã ký hiệu";
            // 
            // xrLabel48
            // 
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(9.999979F, 29.50003F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.Text = "Tên phương tiện vận chuyển";
            // 
            // lblTenPhuongTienVanChuyen
            // 
            this.lblTenPhuongTienVanChuyen.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenPhuongTienVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(175F, 29.50004F);
            this.lblTenPhuongTienVanChuyen.Name = "lblTenPhuongTienVanChuyen";
            this.lblTenPhuongTienVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenPhuongTienVanChuyen.SizeF = new System.Drawing.SizeF(306.3731F, 10.99997F);
            this.lblTenPhuongTienVanChuyen.StylePriority.UseFont = false;
            this.lblTenPhuongTienVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblTenPhuongTienVanChuyen.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTenPhuongTienVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BanKhaiThongTinHoaDon
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.DetailReport1});
            this.Margins = new System.Drawing.Printing.Margins(53, 27, 95, 21);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblGioKhaiBaoNopThue;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTChiThiHQ;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanBietSo;
        private DevExpress.XtraReports.UI.XRTableCell lblSo;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayThangNam;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblPhuongThucThanhToan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemLapHoaDon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lblNgayLapHoaDon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblSoHoaDon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lblMaDaiLyHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiXuatNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhanHoaDon;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lblSoDienThoaiXuatNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiXuatNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel lblMaBuuChinhXuatNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel lblTenNguoiXuatNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiXuatNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiLapHoaDon;
        private DevExpress.XtraReports.UI.XRLabel lblMaBuuChinhNguoiNhanGui;
        private DevExpress.XtraReports.UI.XRLabel lblTenNguoiNhanGui;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiNhanGui;
        private DevExpress.XtraReports.UI.XRLabel lblNuocSoTaiNguoiNhanGui;
        private DevExpress.XtraReports.UI.XRLabel lblSoDienThoaiNguoiNhanGui;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiNhanGui4;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiNhanGui3;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiNhanGui2;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiNhanGui1;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRLabel lblMaKyHieu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lblTenDiaDiemXepHang;
        private DevExpress.XtraReports.UI.XRLabel lblTenDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblTenDiaDiemTrungChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemTrungChuyen;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblThoiKyXepHang;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemXepHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel lblSoHieuChuyenDi;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel lblTenPhuongTienVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
    }
}
