
/****** Object:  View [dbo].[v_HangTon]    Script Date: 08/22/2013 11:49:47 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_HangTon]'))
DROP VIEW [dbo].[v_HangTon]
GO



/****** Object:  View [dbo].[v_HangTon]    Script Date: 08/22/2013 11:49:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[v_HangTon]
AS

SELECT  t.MaDoanhNghiep ,
t.MaHaiQuan ,
t.SoToKhai ,
t.MaLoaiHinh ,
t.NamDangKy ,
t.MaNPL ,
hmd.TenHang AS TenNPL ,
hmd.DVT_ID ,
dvt.Ten AS TenDVT ,
hmd.NuocXX_ID ,
hmd.TriGiaKB ,
t.Luong ,
t.Ton ,
t.ThueXNK ,
t.ThueXNKTon ,
t.ThueTTDB ,
t.ThueVAT ,
t.ThueCLGia ,
t.PhuThu ,
( SELECT    NgayDangKy
FROM      dbo.t_SXXK_ToKhaiMauDich
WHERE     SoToKhai = t.SoToKhai
AND MaLoaiHinh = t.MaLoaiHinh
AND NamDangKy = t.NamDangKy
AND MaHaiQuan = t.MaHaiQuan
AND MaDoanhNghiep = t.MaDoanhNghiep
) AS NgayDangKy ,
( SELECT    COUNT(*)
FROM      [dbo].[t_KDT_SXXK_NPLNhapTon]
WHERE     SoToKhai = t.SoToKhai
AND MaLoaiHinh = t.MaLoaiHinh
AND NamDangKy = t.NamDangKy
AND MaNPL = t.MaNPL
AND TonCuoi < TonDau
AND LanThanhLy IN ( SELECT  LanThanhLy
FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
AND MaDoanhNghiep = t.MaDoanhNghiep
) AS SoLanThanhLy ,
( SELECT    MAX(LanThanhLy)
FROM      [dbo].[t_KDT_SXXK_NPLNhapTon]
WHERE     SoToKhai = t.SoToKhai
AND MaLoaiHinh = t.MaLoaiHinh
AND NamDangKy = t.NamDangKy
AND MaNPL = t.MaNPL
AND TonCuoi < TonDau
AND LanThanhLy IN ( SELECT  LanThanhLy
FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
AND MaDoanhNghiep = t.MaDoanhNghiep
) AS LanThanhLy ,
( SELECT    TrangThaiThanhKhoan
FROM      t_KDT_SXXK_HoSoThanhLyDangKy
WHERE     LanThanhLy = ( SELECT   MAX(LanThanhLy)
FROM     [dbo].[t_KDT_SXXK_NPLNhapTon]
WHERE    SoToKhai = t.SoToKhai
AND MaLoaiHinh = t.MaLoaiHinh
AND NamDangKy = t.NamDangKy
AND MaNPL = t.MaNPL
AND TonCuoi < TonDau
AND LanThanhLy IN ( SELECT  LanThanhLy
FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
AND MaDoanhNghiep = t.MaDoanhNghiep
)
) AS TrangThaiThanhKhoan ,
( SELECT    COUNT(*)
FROM      [dbo].t_SXXK_ThanhLy_NPLNhapTon nt
INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu
AND nt.SoToKhai = h.SoToKhai
AND nt.MaLoaiHinh = h.MaLoaiHinh
AND nt.NamDangKy = h.NamDangKy
AND nt.MaHaiQuan = h.MaHaiQuan
WHERE     nt.SoToKhai = t.SoToKhai
AND nt.MaLoaiHinh = t.MaLoaiHinh
AND nt.NamDangKy = t.NamDangKy
AND nt.MaNPL = t.MaNPL
AND nt.MaHaiQuan = t.MaHaiQuan
AND nt.MaDoanhNghiep = t.MaDoanhNghiep
AND nt.Luong <> h.SoLuong
) AS SaiSoLuong ,
( SELECT    h.SoLuong
FROM      [dbo].t_SXXK_ThanhLy_NPLNhapTon nt
INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu
AND nt.SoToKhai = h.SoToKhai
AND nt.MaLoaiHinh = h.MaLoaiHinh
AND nt.NamDangKy = h.NamDangKy
AND nt.MaHaiQuan = h.MaHaiQuan
WHERE     nt.SoToKhai = t.SoToKhai
AND nt.MaLoaiHinh = t.MaLoaiHinh
AND nt.NamDangKy = t.NamDangKy
AND nt.MaNPL = t.MaNPL
AND nt.MaHaiQuan = t.MaHaiQuan
AND nt.MaDoanhNghiep = t.MaDoanhNghiep
AND nt.Luong <> h.SoLuong
) AS SoLuongDangKy ,
dbo.[f_KiemTra_TienTrinh_ThanhLy](t.MaHaiQuan, t.MaDoanhNghiep, t.SoToKhai, t.MaLoaiHinh, t.NamDangKy, t.MaNPL,t.Ton) AS TienTrinhChayThanhLy ,
( SELECT    ThanhLy
FROM      dbo.t_SXXK_ToKhaiMauDich tk
WHERE     tk.SoToKhai = t.SoToKhai
AND tk.MaLoaiHinh = t.MaLoaiHinh
AND tk.NamDangKy = t.NamDangKy
AND tk.MaHaiQuan = t.MaHaiQuan
AND tk.MaDoanhNghiep = t.MaDoanhNghiep
) AS ThanhLy
,CASE 
WHEN ( ( SELECT COUNT(*)  FROM[dbo].t_SXXK_ThanhLy_NPLNhapTon nt INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu  AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan WHERE nt.SoToKhai = t.SoToKhai AND nt.MaLoaiHinh = t.MaLoaiHinh AND nt.NamDangKy = t.NamDangKy AND nt.MaNPL = t.MaNPL AND nt.MaHaiQuan = t.MaHaiQuan AND nt.MaDoanhNghiep = t.MaDoanhNghiep AND nt.Luong <> h.SoLuong ) = 0 
--So lan thanh ly
AND ( SELECT COUNT(*) FROM [dbo].[t_KDT_SXXK_NPLNhapTon] WHERE SoToKhai = t.SoToKhai AND MaLoaiHinh = t.MaLoaiHinh AND NamDangKy = t.NamDangKy AND MaNPL = t.MaNPL AND TonCuoi < TonDau AND LanThanhLy IN ( SELECT LanThanhLy FROM t_KDT_SXXK_HoSoThanhLyDangKy ) AND MaDoanhNghiep = t.MaDoanhNghiep ) = 0 
--Luong nhap - Ton > 0 & < 1: Chenh lech luong lam tron
AND (t.Luong <> t.Ton) ) THEN 1
ELSE 0 END AS LechTon
,( SELECT    TenChuHang
FROM      dbo.t_SXXK_ToKhaiMauDich
WHERE     SoToKhai = t.SoToKhai
AND MaLoaiHinh = t.MaLoaiHinh
AND NamDangKy = t.NamDangKy
AND MaHaiQuan = t.MaHaiQuan
AND MaDoanhNghiep = t.MaDoanhNghiep
) AS TenChuHang
FROM    dbo.t_SXXK_ThanhLy_NPLNhapTon t
LEFT JOIN dbo.t_SXXK_HangMauDich hmd ON t.MaHaiQuan = hmd.MaHaiQuan
AND t.MaNPL = hmd.MaPhu
AND t.SoToKhai = hmd.SoToKhai
AND t.MaLoaiHinh = hmd.MaLoaiHinh
AND t.NamDangKy = hmd.NamDangKy
LEFT JOIN dbo.t_HaiQuan_DonViTinh dvt ON hmd.DVT_ID = dvt.ID
WHERE
CAST(t.SoToKhai AS VARCHAR(10)) + t.MaLoaiHinh + CAST(t.NamDangKy AS VARCHAR(4)) + t.MaHaiQuan IN ( SELECT  CAST(SoToKhai AS VARCHAR(10)) + MaLoaiHinh + CAST(NamDangKy AS VARCHAR(4)) + MaHaiQuan
FROM    dbo.t_SXXK_ToKhaiMauDich
WHERE   MaLoaiHinh LIKE 'N%'
)






GO




/****** Object:  UserDefinedFunction [dbo].[f_KiemTra_TienTrinh_ThanhLy]    Script Date: 08/22/2013 11:50:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[f_KiemTra_TienTrinh_ThanhLy]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
GO


/****** Object:  UserDefinedFunction [dbo].[f_KiemTra_TienTrinh_ThanhLy]    Script Date: 08/22/2013 11:50:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
(
	@MaHaiQuan VARCHAR(10),
	@MaDoanhNghiep VARCHAR(50),
	@SoToKhai INT,
	@MaLoaihinh VARCHAR(10),
	@NamDangKy INT,
	@MaNPL NVARCHAR(255),
	@LuongTonHienTai NUMERIC(18,8)
	
)
RETURNS NVARCHAR(500)
AS 
    
/*
@SOFTECH
HUNGTQ, CREATED 04/08/2011.
Kiem tra tien trinh thuc hien chay thanh ly: . 
Gia tri dung phai la so ton cuoi cua lan thanh ly dau = so ton dau cua lan thanh ly tiep theo. 
*/
	
BEGIN

--SET @MaHaiQuan = 'C34C'
--SET @MaDoanhNghiep = '0400101556'
--SET @SoToKhai = 152
--SET @MaLoaihinh = 'NSX01'
--SET @NamDangKy = 2010
--SET @MaNPL = 'NEPLUNG'
			
DECLARE
	@LanThanhLy INT,
	@Luong NUMERIC(18,8),
	@Luong_BanLuu NUMERIC(18,8),
	@Luong_NhapTon NUMERIC(18,8),
	@TonDau NUMERIC(18,8),
	@TonCuoi NUMERIC(18,8),
	@TonCuoi_BanLuu NUMERIC(18,8),
	@TonDauThueXNK NUMERIC(18,8),
	@TonCuoiThueXNK NUMERIC(18,8),
	@cnt1 INT,--Bien dem
	@str NVARCHAR(500),
	@ketqua NVARCHAR(500), --hien thi ket qua
	@SaiSoLuong INT,
	@Valid BIT,
	@Dem INT,
	@isDongHoSo INT
	-- Đếm số dòng thanh lý
	SELECT  @Dem = COUNT(*)
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon]
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
/*<I> Cap nhat lech luong nhap do bi lam tron so thap phan*/	
DECLARE curThanhLy CURSOR FOR
	SELECT  nplTon.LanThanhLy, Luong, TonDau, TonCuoi, TonDauThueXNK, TonCuoiThueXNK
	,CASE 
			WHEN  EXISTS(SELECT TrangThaiThanhKhoan  FROM t_KDT_SXXK_HoSoThanhLyDangKy hs WHERE hs.LanThanhLy = nplTon.LanThanhLy AND hs.TrangThaiThanhKhoan = 401) THEN 1
			ELSE  0
			END AS IsDongHoSo
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon] nplTon
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
	ORDER BY TonCuoi DESC
	
	
	
--BEGIN TRANSACTION T1 ;

	SET @Valid = 1
	SET @SaiSoLuong = 0	
    SET @cnt1 = 0;
    
	SET @str = '....................................................................' + char(10)
	SET @str = @str + '		STT' + ' - ' + + 'SoToKhai' + ' - ' + 'MaLoaiHinh' + ' - ' + 'NamDangKy' + ' - ' + 'MaNPL' + char(10)
	SET @str = @str + STR(@cnt1) + ' - ' + STR(@SoToKhai) + ' - ' + @MaLoaiHinh + ' - ' + str(@NamDangKy)+ ' - ' + @MaNPL + char(10)
	--PRINT @str								
	
    OPEN curThanhLy 
    FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK, @isDongHoSo
	--Fetch next record
    WHILE @@FETCH_STATUS = 0 
		BEGIN
							
			SET @cnt1 = @cnt1 + 1
						
			--PRINT STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau: ' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi: ' + cast(@TonCuoi AS VARCHAR(50))
										
			IF (@cnt1 = 1)
				BEGIN
					
					--Kiem tra so luong nhap dang ky ban dau so voi luong ton dau.
					IF (@Luong <> @TonDau)
						BEGIN
							SET @ketqua = N'Sai lượng nhập và lượng tồn đầu tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
							SET @Valid = 0
						END				
					
						  
					IF(@Valid = 1)
						BEGIN
							--Kiem tra so luong nhap dang ky ban dau so voi luong nhap luu trong ton.
							SELECT  @SaiSoLuong =  COUNT(*), @Luong_NhapTon = nt.Luong
								  FROM      [dbo].t_SXXK_ThanhLy_NPLNhapTon nt
											INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu
																				   AND nt.SoToKhai = h.SoToKhai
																				   AND nt.MaLoaiHinh = h.MaLoaiHinh
																				   AND nt.NamDangKy = h.NamDangKy
																				   AND nt.MaHaiQuan = h.MaHaiQuan
								  WHERE     nt.SoToKhai = @SoToKhai
											AND nt.MaLoaiHinh = @MaLoaiHinh
											AND nt.NamDangKy = @NamDangKy
											AND nt.MaNPL = @MaNPL
											AND nt.MaHaiQuan = @MaHaiQuan
											AND nt.MaDoanhNghiep = @MaDoanhNghiep
											AND nt.Luong <> h.SoLuong
								  GROUP BY nt.Luong
						  
							IF (@SaiSoLuong > 0 AND @Luong_NhapTon <> @Luong)
								BEGIN
									SET @ketqua = N'Sai lượng nhập tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
									SET @Valid = 0
								END
							ELSE
								BEGIN	  
									SET @ketqua = N'Thanh lý đúng'
									SET @Valid = 1
									SET @TonCuoi_BanLuu = @TonCuoi
								END
						END
				END
			IF(@cnt1 = @Dem)
			BEGIN
				IF(@isDongHoSo != 1 AND  @LuongTonHienTai <> @TonDau)
				BEGIN
					SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@LuongTonHienTai AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai số liệu hiện tại' + cast(@LanThanhLy AS VARCHAR(50))
				END
				IF(@isDongHoSo = 1 AND  @LuongTonHienTai <> @TonCuoi)
				BEGIN
					SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@LuongTonHienTai AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai số liệu hiện tại' + cast(@LanThanhLy AS VARCHAR(50))
				END
			END
			--Kiem tra so ton
			ELSE IF (@cnt1 > 1 AND @Valid = 1) --kiem tra tu dong thu 2 tro di
				BEGIN

					SET @str = STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi' + cast(@TonCuoi AS VARCHAR(50))
					
					IF (@TonDau <> @TonCuoi_BanLuu)
						BEGIN
							
							SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@TonCuoi_BanLuu AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
						END
					ELSE
						SET @ketqua = N'Thanh lý đúng'
				END						
			
			--Luu lai ton cuoi cua row truoc do
			SET @TonCuoi_BanLuu = @TonCuoi
				
			FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK, @isDongHoSo
        END
        
    CLOSE curThanhLy --Close cursor
    DEALLOCATE curThanhLy --Deallocate cursor
	
	--PRINT @ketqua
	
    --IF @@ERROR != 0 
    --    BEGIN
            --PRINT 'Qua trinh thuc hien xay ra loi.'
            --PRINT STR(@@ERROR)
            ----ROLLBACK TRANSACTION T1 ;
            --PRINT 'ROLLBACK TRANSACTION T1'
  --      END
  --  ELSE 
		--BEGIN
			----COMMIT TRANSACTION T1 ;	
			--PRINT ''
			----PRINT 'COMMIT TRANSACTION T1'
		--END
	
	RETURN @ketqua
	
END	

--SELECT ECS_TQ_SXXK_HOATHO_2011.dbo.[f_KiemTra_TienTrinh_ThanhLy]('C34C', '0400101556', 1782, 'NSX01', 2010, 'ÐINHTAN')

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '9.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('9.5',GETDATE(), N'Cập nhật quản lý tồn')
END