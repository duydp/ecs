﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class HoaDonThuongMaiDetail
    {
        #region Properties.

        public long ID { set; get; }
        public long HMD_ID { set; get; }
        public long HoaDonTM_ID { set; get; }
        public double GiaTriDieuChinhTang { set; get; }
        public double GiaiTriDieuChinhGiam { set; get; }
        public string GhiChu { set; get; }
        public int SoThuTuHang { set; get; }
        public string MaHS { set; get; }
        public string MaPhu { set; get; }
        public string TenHang { set; get; }
        public string NuocXX_ID { set; get; }
        public string DVT_ID { set; get; }
        public decimal SoLuong { set; get; }
        public double DonGiaKB { set; get; }
        public double TriGiaKB { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<HoaDonThuongMaiDetail> ConvertToCollection(IDataReader reader)
        {
            IList<HoaDonThuongMaiDetail> collection = new List<HoaDonThuongMaiDetail>();
            while (reader.Read())
            {
                HoaDonThuongMaiDetail entity = new HoaDonThuongMaiDetail();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("HMD_ID"))) entity.HMD_ID = reader.GetInt64(reader.GetOrdinal("HMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("HoaDonTM_ID"))) entity.HoaDonTM_ID = reader.GetInt64(reader.GetOrdinal("HoaDonTM_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaTriDieuChinhTang"))) entity.GiaTriDieuChinhTang = reader.GetDouble(reader.GetOrdinal("GiaTriDieuChinhTang"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaiTriDieuChinhGiam"))) entity.GiaiTriDieuChinhGiam = reader.GetDouble(reader.GetOrdinal("GiaiTriDieuChinhGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDouble(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDouble(reader.GetOrdinal("TriGiaKB"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static HoaDonThuongMaiDetail Load(long id)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<HoaDonThuongMaiDetail> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<HoaDonThuongMaiDetail> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<HoaDonThuongMaiDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<HoaDonThuongMaiDetail> SelectCollectionBy_HMD_ID(long hMD_ID)
        {
            IDataReader reader = SelectReaderBy_HMD_ID(hMD_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------
        public static IList<HoaDonThuongMaiDetail> SelectCollectionBy_HoaDonTM_ID(long hoaDonTM_ID)
        {
            IDataReader reader = SelectReaderBy_HoaDonTM_ID(hoaDonTM_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_HMD_ID(long hMD_ID)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_SelectBy_HMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, hMD_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_HoaDonTM_ID(long hoaDonTM_ID)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_SelectBy_HoaDonTM_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HoaDonTM_ID", SqlDbType.BigInt, hoaDonTM_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_HMD_ID(long hMD_ID)
        {
            const string spName = "p_KDT_HoaDonThuongMaiDetail_SelectBy_HMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, hMD_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static IDataReader SelectReaderBy_HoaDonTM_ID(long hoaDonTM_ID)
        {
            const string spName = "p_KDT_HoaDonThuongMaiDetail_SelectBy_HoaDonTM_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HoaDonTM_ID", SqlDbType.BigInt, hoaDonTM_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertHoaDonThuongMaiDetail(long hMD_ID, long hoaDonTM_ID, double giaTriDieuChinhTang, double giaiTriDieuChinhGiam, string ghiChu, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, double donGiaKB, double triGiaKB)
        {
            HoaDonThuongMaiDetail entity = new HoaDonThuongMaiDetail();
            entity.HMD_ID = hMD_ID;
            entity.HoaDonTM_ID = hoaDonTM_ID;
            entity.GiaTriDieuChinhTang = giaTriDieuChinhTang;
            entity.GiaiTriDieuChinhGiam = giaiTriDieuChinhGiam;
            entity.GhiChu = ghiChu;
            entity.SoThuTuHang = soThuTuHang;
            entity.MaHS = maHS;
            entity.MaPhu = maPhu;
            entity.TenHang = tenHang;
            entity.NuocXX_ID = nuocXX_ID;
            entity.DVT_ID = dVT_ID;
            entity.SoLuong = soLuong;
            entity.DonGiaKB = donGiaKB;
            entity.TriGiaKB = triGiaKB;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
            db.AddInParameter(dbCommand, "@HoaDonTM_ID", SqlDbType.BigInt, HoaDonTM_ID);
            db.AddInParameter(dbCommand, "@GiaTriDieuChinhTang", SqlDbType.Float, GiaTriDieuChinhTang);
            db.AddInParameter(dbCommand, "@GiaiTriDieuChinhGiam", SqlDbType.Float, GiaiTriDieuChinhGiam);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<HoaDonThuongMaiDetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HoaDonThuongMaiDetail item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateHoaDonThuongMaiDetail(long id, long hMD_ID, long hoaDonTM_ID, double giaTriDieuChinhTang, double giaiTriDieuChinhGiam, string ghiChu, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, double donGiaKB, double triGiaKB)
        {
            HoaDonThuongMaiDetail entity = new HoaDonThuongMaiDetail();
            entity.ID = id;
            entity.HMD_ID = hMD_ID;
            entity.HoaDonTM_ID = hoaDonTM_ID;
            entity.GiaTriDieuChinhTang = giaTriDieuChinhTang;
            entity.GiaiTriDieuChinhGiam = giaiTriDieuChinhGiam;
            entity.GhiChu = ghiChu;
            entity.SoThuTuHang = soThuTuHang;
            entity.MaHS = maHS;
            entity.MaPhu = maPhu;
            entity.TenHang = tenHang;
            entity.NuocXX_ID = nuocXX_ID;
            entity.DVT_ID = dVT_ID;
            entity.SoLuong = soLuong;
            entity.DonGiaKB = donGiaKB;
            entity.TriGiaKB = triGiaKB;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_HoaDonThuongMaiDetail_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
            db.AddInParameter(dbCommand, "@HoaDonTM_ID", SqlDbType.BigInt, HoaDonTM_ID);
            db.AddInParameter(dbCommand, "@GiaTriDieuChinhTang", SqlDbType.Float, GiaTriDieuChinhTang);
            db.AddInParameter(dbCommand, "@GiaiTriDieuChinhGiam", SqlDbType.Float, GiaiTriDieuChinhGiam);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<HoaDonThuongMaiDetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HoaDonThuongMaiDetail item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateHoaDonThuongMaiDetail(long id, long hMD_ID, long hoaDonTM_ID, double giaTriDieuChinhTang, double giaiTriDieuChinhGiam, string ghiChu, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, double donGiaKB, double triGiaKB)
        {
            HoaDonThuongMaiDetail entity = new HoaDonThuongMaiDetail();
            entity.ID = id;
            entity.HMD_ID = hMD_ID;
            entity.HoaDonTM_ID = hoaDonTM_ID;
            entity.GiaTriDieuChinhTang = giaTriDieuChinhTang;
            entity.GiaiTriDieuChinhGiam = giaiTriDieuChinhGiam;
            entity.GhiChu = ghiChu;
            entity.SoThuTuHang = soThuTuHang;
            entity.MaHS = maHS;
            entity.MaPhu = maPhu;
            entity.TenHang = tenHang;
            entity.NuocXX_ID = nuocXX_ID;
            entity.DVT_ID = dVT_ID;
            entity.SoLuong = soLuong;
            entity.DonGiaKB = donGiaKB;
            entity.TriGiaKB = triGiaKB;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
            db.AddInParameter(dbCommand, "@HoaDonTM_ID", SqlDbType.BigInt, HoaDonTM_ID);
            db.AddInParameter(dbCommand, "@GiaTriDieuChinhTang", SqlDbType.Float, GiaTriDieuChinhTang);
            db.AddInParameter(dbCommand, "@GiaiTriDieuChinhGiam", SqlDbType.Float, GiaiTriDieuChinhGiam);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<HoaDonThuongMaiDetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HoaDonThuongMaiDetail item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteHoaDonThuongMaiDetail(long id)
        {
            HoaDonThuongMaiDetail entity = new HoaDonThuongMaiDetail();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_HMD_ID(long hMD_ID)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, hMD_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_HoaDonTM_ID(long hoaDonTM_ID)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HoaDonTM_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HoaDonTM_ID", SqlDbType.BigInt, hoaDonTM_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<HoaDonThuongMaiDetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HoaDonThuongMaiDetail item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}