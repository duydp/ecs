﻿namespace Company.Interface.Controls
{
    partial class LoaiHinhChuyenTiepControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout cbTen_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoaiHinhChuyenTiepControl));
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbTen = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.ds = new System.Data.DataSet();
            this.dtLHChuyenTiep = new System.Data.DataTable();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ID = new System.Data.DataColumn();
            this.TenLH = new System.Data.DataColumn();
            ((System.ComponentModel.ISupportInitialize)(this.cbTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLHChuyenTiep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMa
            // 
            this.txtMa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa.Location = new System.Drawing.Point(0, 0);
            this.txtMa.Name = "txtMa";
            this.txtMa.ReadOnly = true;
            this.txtMa.Size = new System.Drawing.Size(49, 21);
            this.txtMa.TabIndex = 0;
            this.txtMa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMa.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // cbTen
            // 
            this.cbTen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTen.ComboStyle = Janus.Windows.GridEX.ComboStyle.DropDownList;
            this.cbTen.DataMember = "LHChuyenTiep";
            this.cbTen.DataSource = this.ds;
            cbTen_DesignTimeLayout.LayoutString = resources.GetString("cbTen_DesignTimeLayout.LayoutString");
            this.cbTen.DesignTimeLayout = cbTen_DesignTimeLayout;
            this.cbTen.DisplayMember = "Ten";
            this.cbTen.Location = new System.Drawing.Point(55, 0);
            this.cbTen.Name = "cbTen";
            this.cbTen.ReadOnly = true;
            this.cbTen.SelectedIndex = -1;
            this.cbTen.SelectedItem = null;
            this.cbTen.Size = new System.Drawing.Size(204, 21);
            this.cbTen.TabIndex = 1;
            this.cbTen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbTen.ValueMember = "ID";
            this.cbTen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbTen.ValueChanged += new System.EventHandler(this.cbTen_ValueChanged);
            // 
            // ds
            // 
            this.ds.DataSetName = "DuLieuChuan";
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtLHChuyenTiep});
            // 
            // dtLHChuyenTiep
            // 
            this.dtLHChuyenTiep.Columns.AddRange(new System.Data.DataColumn[] {
            this.ID,
            this.TenLH});
            this.dtLHChuyenTiep.TableName = "LHChuyenTiep";
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.cbTen;
            this.rfvTen.ErrorMessage = "\"Loại hình chuyển tiếp\" không được bỏ trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            // 
            // ID
            // 
            this.ID.ColumnName = "ID";
            // 
            // Ten
            // 
            this.TenLH.ColumnName = "Ten";
            // 
            // LoaiHinhChuyenTiepControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.cbTen);
            this.Controls.Add(this.txtMa);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "LoaiHinhChuyenTiepControl";
            this.Size = new System.Drawing.Size(273, 22);
            this.Load += new System.EventHandler(this.DonViHaiQuanControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLHChuyenTiep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtMa;
        private System.Data.DataSet ds;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
        public Janus.Windows.GridEX.EditControls.MultiColumnCombo cbTen;
        private System.Data.DataTable dtLHChuyenTiep;
        private System.Data.DataColumn ID;
        private System.Data.DataColumn TenLH;
    }
}