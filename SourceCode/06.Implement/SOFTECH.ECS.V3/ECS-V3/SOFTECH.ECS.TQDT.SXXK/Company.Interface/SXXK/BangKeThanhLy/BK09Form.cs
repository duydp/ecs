using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK09Form : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKNPLTamNopThueCollection bkCollection = new BKNPLTamNopThueCollection();
        private int SoToKhai;
        private string MaLoaiHinh;
        private DateTime NgayDangKy;
        private string TenNPL;
        private string DVT_ID;
        private decimal SoLuong;
        public BK09Form()
        {
            InitializeComponent();
        }

        private void BK09Form_Load(object sender, EventArgs e)
        {

            if (this.HSTL.TrangThaiThanhKhoan == 401)
            {
                cmdAdd.Enabled = cmdSave.Enabled = false;
                dgList2.AllowDelete = InheritableBoolean.False;
            }
            dgList1.DataSource = new BKToKhaiNhap().getNPL_BKToKhaiNhap(this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].ID).Tables[0];
            dgList2.DataSource = this.bkCollection;

        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            }
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {

                this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.TenNPL = e.Row.Cells["TenHang"].Text;
                this.DVT_ID = Convert.ToString(e.Row.Cells["DVT_ID"].Value);
                this.SoLuong = Convert.ToDecimal(e.Row.Cells["SoLuong"].Value);
                this.MaHaiQuan = Convert.ToString(e.Row.Cells["MaHaiQuan"].Value);
                txtSoToKhai.Text = e.Row.Cells["SoToKhai"].Text;
                txtMaNPL.Text = e.Row.Cells["Ma"].Text;
            }
        }
        private bool checkExit(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL)
        {
            foreach (BKNPLTamNopThue bk in this.bkCollection)
            {
                if (bk.SoToKhai == soToKhai && bk.MaLoaiHinh == maLoaiHinh && bk.NamDangKy == namDangKy && bk.MaHaiQuan == maHaiQuan && bk.MaNPL == maNPL)
                    return true;
            }
            return false;
        }

        private void dgList2_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (checkExit(this.SoToKhai, this.MaLoaiHinh, Convert.ToInt16(this.NgayDangKy.Year), this.MaHaiQuan.Trim(), txtMaNPL.Text))
                {
                   // ShowMessage("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác", false);
                    MLMessages("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác.", "MSG_THK81", "", false);
                    txtSoToKhai.Text = "";
                    txtMaNPL.Text = "";
                    return;
                }
                BKNPLTamNopThue bk = new BKNPLTamNopThue();
                bk.BangKeHoSoThanhLy_ID = 0;
                bk.SoToKhai = this.SoToKhai;
                bk.MaLoaiHinh = this.MaLoaiHinh;
                bk.NgayDangKy = this.NgayDangKy;
                bk.MaHaiQuan = this.MaHaiQuan.Trim();
                bk.NamDangKy = Convert.ToInt16(this.NgayDangKy.Year);
                bk.MaNPL = txtMaNPL.Text;
                bk.TenNPL = this.TenNPL;
                bk.NgayNopThue = ccNgayNopThue.Value;
                bk.Nop_NK = Convert.ToDecimal(txtNopNK.Value);
                bk.Nop_VAT = Convert.ToDecimal(txtNopVAT.Value);
                bk.Nop_TTDB = Convert.ToDecimal(txtNopTTDB.Value);
                bk.Nop_CLGia = Convert.ToDecimal(txtNopCLGia.Value);
                this.bkCollection.Add(bk);
                dgList2.Refetch();
                txtSoToKhai.Text = "";
                txtMaNPL.Text = "";
                txtNopNK.Value = 0;
                txtNopVAT.Value = 0;
                txtNopTTDB.Value = 0;
                txtNopCLGia.Value = 0;
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (this.bkCollection.Count == 0)
            {
                ShowMessage("Bạn chưa nhập NPL tạm nộp thuế.", false);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (this.HSTL.getBKNPLTamNopThue() < 0)
                {
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.MaterID = this.HSTL.ID;
                    bk.STTHang = this.HSTL.BKCollection.Count + 1;
                    bk.MaBangKe = "DTLCHITIETNT";
                    bk.TenBangKe = "DTLCHITIETNT";
                    bk.bkNPLTNTCollection = this.bkCollection;
                    this.HSTL.BKCollection.Add(bk);
                }
                else
                {
                    this.HSTL.BKCollection[this.HSTL.getBKNPLTamNopThue()].bkNPLTNTCollection = this.bkCollection;
                }
                this.HSTL.BKCollection[this.HSTL.getBKNPLTamNopThue()].InsertUpdate_BKNPLTNT(this.bkCollection);
                this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void txtMaNPL_TextChanged(object sender, EventArgs e)
        {

        }
    }
}