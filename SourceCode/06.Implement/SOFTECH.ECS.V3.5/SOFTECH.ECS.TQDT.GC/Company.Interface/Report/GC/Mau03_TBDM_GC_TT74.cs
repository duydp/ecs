﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
 

namespace Company.Interface.Report.GC
{
    public partial class Mau03_TBDM_GC_TT74 : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        //private int STT = 0;
        public bool inmathang = false;
        public string maSP;
        public string mota;
        
        public DataTable dt = new DataTable();
        public Mau03_TBDM_GC_TT74()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
           
            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamDangKyInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new DataSet();
            string where = "HopDong_ID = " + this.HD.ID + " and trangthai= 0";
            ds = new Company.GC.BLL.GC.NhomSanPham().SelectDynamic(where, null);
                      
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
            SP.Ma = maSP;
            SP.HopDong_ID = this.HD.ID;
            SP.Load();

            lblMathang.Text = loaiSP;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;
            lblMaHang.Text = SP.Ten + "  (Mã hàng: " + maSP + ")";
            lblSoLuongHang.Text = SP.SoLuongDangKy.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            lblDVTHang.Text = DonViTinh.GetName(SP.DVT_ID);
            this.DataSource = dt;

            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblDMSD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DMSD", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTLHH.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TiLeHH", "{00:F00}");
            lblDMAll.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DMAll", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblNguonNL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Nguon");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".GhiChu");
            xrLabel27.Text =  "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;

           // xrLabel27.Text = GlobalSettings.TieudeNgay;
            //lblMoTaSanPham.Text = dt.Rows[0]["GhiChu"].ToString();
        }

    }
}
