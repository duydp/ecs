﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.KD
{
    class NodeDeNghiChuyenCuaKhau
    {
        /// <summary>
        /// 
        /// </summary>
        public const string CustomsOfficeChangedRequest = "CustomsOfficeChangedRequest";

        public const string AdditionalDocument = "AdditionalDocument";
        /// <summary>
        /// Số vận đơn	1	an..35
        /// </summary>
        public const string reference = "reference";

        /// <summary>
        ///Ngày vận đơn	1	an19
        /// </summary>
        public const string issue = "issue";
        public const string AdditionalInformation = "AdditionalInformation";

        /// <summary>
        ///Nội dung đề nghị chuyển cửa khẩu	1	an..256
        /// </summary>
        public const string content = "content";		
		
        /// <summary>
        /// Địa điểm kiểm tra hàng hóa ngoài cửa khẩu	1	an..256
        /// </summary>
        public const string examinationPlace = "examinationPlace";	
		
	    /// <summary>
	    /// Thời gian dự kiến đến địa điểm kiểm tra	1	an19
	    /// </summary>
        public const string time = "time";	
		
		/// <summary>
        ///Tuyến đường vận chuyển	1	an..256
		/// </summary>
        public const string route = "route";				


    }
}
