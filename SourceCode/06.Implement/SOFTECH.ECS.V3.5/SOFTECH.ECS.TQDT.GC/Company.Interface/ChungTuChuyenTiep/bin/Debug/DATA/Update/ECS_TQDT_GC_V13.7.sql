--- cap nhat thue suat cho thue tieu thu dac biet
Update t_VNACC_Category_Common set Notes='65 ' where ReferenceDB='A522' and Code ='TB010'
Update t_VNACC_Category_Common set Notes='50 ' where ReferenceDB='A522' and Code ='TB020'
Update t_VNACC_Category_Common set Notes='25 ' where ReferenceDB='A522' and Code ='TB030'
Update t_VNACC_Category_Common set Notes='50 ' where ReferenceDB='A522' and Code ='TB040'
Update t_VNACC_Category_Common set Notes='45 ' where ReferenceDB='A522' and Code ='TB050'
Update t_VNACC_Category_Common set Notes='50 ' where ReferenceDB='A522' and Code ='TB060'
Update t_VNACC_Category_Common set Notes='60 ' where ReferenceDB='A522' and Code ='TB070'
Update t_VNACC_Category_Common set Notes='30 ' where ReferenceDB='A522' and Code ='TB080'
Update t_VNACC_Category_Common set Notes='15 ' where ReferenceDB='A522' and Code ='TB090'
Update t_VNACC_Category_Common set Notes='15 ' where ReferenceDB='A522' and Code ='TB100'
Update t_VNACC_Category_Common set Notes='31,5 ' where ReferenceDB='A522' and Code ='TB110'
Update t_VNACC_Category_Common set Notes='35 ' where ReferenceDB='A522' and Code ='TB120'
Update t_VNACC_Category_Common set Notes='42 ' where ReferenceDB='A522' and Code ='TB130'
Update t_VNACC_Category_Common set Notes='21 ' where ReferenceDB='A522' and Code ='TB140'
Update t_VNACC_Category_Common set Notes='10,5 ' where ReferenceDB='A522' and Code ='TB150'
Update t_VNACC_Category_Common set Notes='10,5 ' where ReferenceDB='A522' and Code ='TB160'
Update t_VNACC_Category_Common set Notes='22,5 ' where ReferenceDB='A522' and Code ='TB170'
Update t_VNACC_Category_Common set Notes='25 ' where ReferenceDB='A522' and Code ='TB180'
Update t_VNACC_Category_Common set Notes='30 ' where ReferenceDB='A522' and Code ='TB190'
Update t_VNACC_Category_Common set Notes='15 ' where ReferenceDB='A522' and Code ='TB200'
Update t_VNACC_Category_Common set Notes='7,5 ' where ReferenceDB='A522' and Code ='TB210'
Update t_VNACC_Category_Common set Notes='7,5 ' where ReferenceDB='A522' and Code ='TB220'
Update t_VNACC_Category_Common set Notes='25 ' where ReferenceDB='A522' and Code ='TB230'
Update t_VNACC_Category_Common set Notes='15 ' where ReferenceDB='A522' and Code ='TB240'
Update t_VNACC_Category_Common set Notes='10 ' where ReferenceDB='A522' and Code ='TB250'
Update t_VNACC_Category_Common set Notes='10 ' where ReferenceDB='A522' and Code ='TB260'
Update t_VNACC_Category_Common set Notes='20 ' where ReferenceDB='A522' and Code ='TB270'
Update t_VNACC_Category_Common set Notes='30 ' where ReferenceDB='A522' and Code ='TB280'
Update t_VNACC_Category_Common set Notes='30 ' where ReferenceDB='A522' and Code ='TB290'
Update t_VNACC_Category_Common set Notes='10 ' where ReferenceDB='A522' and Code ='TB300'
Update t_VNACC_Category_Common set Notes='10 ' where ReferenceDB='A522' and Code ='TB310'
Update t_VNACC_Category_Common set Notes='40 ' where ReferenceDB='A522' and Code ='TB320'
Update t_VNACC_Category_Common set Notes='70 ' where ReferenceDB='A522' and Code ='TB330'
-- cap nhat thue suat cho thue gia tri gia tang
Update t_VNACC_Category_Common set Notes='5' where ReferenceDB='A522' and Code in('VB015','VB025','VB035','VB045','VB055','VB065','VB075','VB085','VB095','VB105','VB115','VB125','VB135','VB145','VB155','VB165','VB175','VB185')
Update t_VNACC_Category_Common set Notes='10' where ReferenceDB='A522' and Code ='VB901'
--- cap nhat loai thue cho doanh muc thue
-- alter Table
alter table t_VNACC_Category_Common
	alter column Name_EN nvarchar(max)
--
Update t_VNACC_Category_Common set Name_EN=N'Thuế giá trị gia tăng' where ReferenceDB='A522' and Code like 'VB%'
Update t_VNACC_Category_Common set Name_EN=N'Thuế tiêu thụ đặc biệt' where ReferenceDB='A522' and Code like 'TB%'
Update t_VNACC_Category_Common set Name_EN=N'Thuế bảo vệ môi trường' where ReferenceDB='A522' and Code like 'MB%'
Update t_VNACC_Category_Common set Name_EN=N'Thuế tự vệ' where ReferenceDB='A522' and Code like 'BB%'
Update t_VNACC_Category_Common set Name_EN=N'Thuế chống bán phá giá' where ReferenceDB='A522' and Code like 'GB%'
Update t_VNACC_Category_Common set Name_EN=N'Thuế chống trợ cấp' where ReferenceDB='A522' and Code like 'GB%'
Update t_VNACC_Category_Common set Name_EN=N'Thuế chống phân biệt đối xử' where ReferenceDB='A522' and Code like 'PB%'
Update t_VNACC_Category_Common set Name_EN=N'Thuế nhập khẩu khác' where ReferenceDB='A522' and Code like 'DB%'
Update t_VNACC_Category_Common set Name_EN=N'Thuế nhập khẩu khác' where ReferenceDB='A522' and Code like 'EB%'
-- cap nhat danh muc E015
Update t_VNACC_Category_Common set Name_VN=N'Người xuất nhập khẩu' where ReferenceDB='E015'  and Code='1'

           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '13.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('13.7', GETDATE(), N'Cap nhat Danh muc  (A522,E015)')
END	
