﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.SXXK;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.Report
{
    public partial class PreviewFormITG : BaseForm
    {
        private BangKeToKhaiNhap BKTKN = new BangKeToKhaiNhap();
        private BangKeToKhaiXuat BKTKX = new BangKeToKhaiXuat();
        private BCNPLXuatNhapTon BCNPLXNT = new BCNPLXuatNhapTon();
        private Company.Interface.Report.SXXK.BCThueXNK BCThue = new Company.Interface.Report.SXXK.BCThueXNK();

        //DATLMQ bổ sung Thông tư 194
        private BangKeToKhaiXuat_TT194 BKTKX_TT194 = new BangKeToKhaiXuat_TT194();
        private BCNPLXuatNhapTon_TT194 BCNPLNXT_TT194 = new BCNPLXuatNhapTon_TT194();
        private BCThueXNK_TT194 BCThue_TT194 = new BCThueXNK_TT194();

        public int LanThanhLy;
        public int SoHoSo;
        public BKToKhaiNhapCollection TKNHoanThueCollection = new BKToKhaiNhapCollection();
        public BKToKhaiNhapCollection TKNKhongThuCollection = new BKToKhaiNhapCollection();
        private string WhereHoanThue = "";
        private string WhereKhongThu = "";
        private string WhereHoanThue1 = "";
        private string WhereKhongThu1 = "";

        public PreviewFormITG()
        {
            InitializeComponent();

            this.Load += new EventHandler(PreviewFormITG_Load);
        }

        void PreviewFormITG_Load(object sender, EventArgs e)
        {
            BindComBo();
        }

        private void explorerBar1_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "BKTKN":
                    this.BKTKN.LanThanhLy = this.LanThanhLy;
                    this.BKTKN.SoHoSo = this.SoHoSo;
                    this.BKTKN.BindReport("");
                    printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                    this.BKTKN.CreateDocument();
                    break;
                case "BKTKX":
                    this.BKTKX.BindReport(this.LanThanhLy);
                    this.BKTKX.SoHoSo = this.SoHoSo;
                    printControl1.PrintingSystem = this.BKTKX.PrintingSystem;
                    this.BKTKX.CreateDocument();
                    break;
                case "BCNPLNXT":
                    this.BCNPLXNT.LanThanhLy = this.LanThanhLy;
                    this.BCNPLXNT.SoHoSo = this.SoHoSo;
                    this.BCNPLXNT.BindReport("");
                    printControl1.PrintingSystem = this.BCNPLXNT.PrintingSystem;
                    this.BCNPLXNT.CreateDocument();
                    break;
                case "BCThueNK":
                    this.BCThue.LanThanhLy = this.LanThanhLy;
                    this.BCThue.SoHoSo = this.SoHoSo;
                    this.BCThue.BindReport("");
                    printControl1.PrintingSystem = this.BCThue.PrintingSystem;
                    this.BCThue.CreateDocument();
                    break;
                case "BKTKNHoanThue":
                    this.BKTKN.LanThanhLy = this.LanThanhLy;
                    this.BKTKN.SoHoSo = this.SoHoSo;
                    this.BKTKN.isHoanThue = true;
                    this.BKTKN.BindReport(this.WhereHoanThue1);
                    printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                    this.BKTKN.CreateDocument();
                    break;
                case "BKTKXHoanThue":
                    this.BKTKX.BindReport(this.LanThanhLy);
                    this.BKTKX.SoHoSo = this.SoHoSo;
                    this.BKTKX.isHoanThue = true;
                    printControl1.PrintingSystem = this.BKTKX.PrintingSystem;
                    this.BKTKX.CreateDocument();
                    break;
                case "BCNPLNXTHoanThue":
                    this.BCNPLXNT.LanThanhLy = this.LanThanhLy;
                    this.BCNPLXNT.SoHoSo = this.SoHoSo;
                    this.BCNPLXNT.isHoanThue = true;
                    this.BCNPLXNT.BindReport(this.WhereHoanThue);
                    printControl1.PrintingSystem = this.BCNPLXNT.PrintingSystem;
                    this.BCNPLXNT.CreateDocument();
                    break;
                case "BCThueNKHoanThue":
                    this.BCThue.LanThanhLy = this.LanThanhLy;
                    this.BCThue.SoHoSo = this.SoHoSo;
                    this.BCThue.isHoanThue = true;
                    this.BCThue.BindReport(this.WhereHoanThue);
                    printControl1.PrintingSystem = this.BCThue.PrintingSystem;
                    this.BCThue.CreateDocument();
                    break;
                case "BKTKNKhongThu":
                    this.BKTKN.LanThanhLy = this.LanThanhLy;
                    this.BKTKN.SoHoSo = this.SoHoSo;
                    this.BKTKN.isHoanThue = false;
                    this.BKTKN.BindReport(this.WhereKhongThu1);
                    printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                    this.BKTKN.CreateDocument();
                    break;
                case "BKTKXKhongThu":
                    this.BKTKX.BindReport(this.LanThanhLy);
                    this.BKTKX.SoHoSo = this.SoHoSo;
                    this.BKTKX.isHoanThue = false;
                    printControl1.PrintingSystem = this.BKTKX.PrintingSystem;
                    this.BKTKX.CreateDocument();
                    break;
                case "BCNPLNXTKhongThu":
                    this.BCNPLXNT.LanThanhLy = this.LanThanhLy;
                    this.BCNPLXNT.SoHoSo = this.SoHoSo;
                    this.BCNPLXNT.isHoanThue = false;
                    this.BCNPLXNT.BindReport(this.WhereKhongThu);
                    printControl1.PrintingSystem = this.BCNPLXNT.PrintingSystem;
                    this.BCNPLXNT.CreateDocument();
                    break;
                case "BCThueNKKhongThu":
                    this.BCThue.LanThanhLy = this.LanThanhLy;
                    this.BCThue.SoHoSo = this.SoHoSo;
                    this.BCThue.isHoanThue = false;
                    this.BCThue.BindReport(this.WhereKhongThu);
                    printControl1.PrintingSystem = this.BCThue.PrintingSystem;
                    this.BCThue.CreateDocument();
                    break;
                case "BKTKX_TT194":
                    this.BKTKX_TT194.BindReport(this.LanThanhLy);
                    this.BKTKX_TT194.SoHoSo = this.SoHoSo;
                    printControl1.PrintingSystem = this.BKTKX_TT194.PrintingSystem;
                    this.BKTKX_TT194.CreateDocument();
                    break;
                case "BCNPLNXT_TT194":
                    this.BCNPLNXT_TT194.LanThanhLy = this.LanThanhLy;
                    this.BCNPLNXT_TT194.SoHoSo = this.SoHoSo;
                    this.BCNPLNXT_TT194.BindReport("");
                    printControl1.PrintingSystem = this.BCNPLNXT_TT194.PrintingSystem;
                    this.BCNPLNXT_TT194.CreateDocument();
                    break;
                case "BCThueNK_TT194":
                    this.BCThue_TT194.LanThanhLy = this.LanThanhLy;
                    this.BCThue_TT194.SoHoSo = this.SoHoSo;
                    this.BCThue_TT194.BindReport("");
                    printControl1.PrintingSystem = this.BCThue_TT194.PrintingSystem;
                    this.BCThue_TT194.CreateDocument();
                    break;
            }
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            printPreviewBarItem4.Enabled = printPreviewBarItem5.Enabled = Program.isActivated;

            if (this.TKNHoanThueCollection.Count <= 0) explorerBar1.Groups[2].Visible = false;
            if (this.TKNKhongThuCollection.Count <= 0) explorerBar1.Groups[1].Visible = false;
            if (this.TKNHoanThueCollection.Count > 0)
            {
                this.WhereHoanThue = " AND (";
                foreach (BKToKhaiNhap tkn in this.TKNHoanThueCollection)
                {
                    this.WhereHoanThue += " (SoToKhaiNhap = " + tkn.SoToKhai + " AND MaLoaiHinhNhap = '" + tkn.MaLoaiHinh + "' AND year(NgayDangKyNhap)= " + tkn.NamDangKy + ") OR";
                }
                this.WhereHoanThue = this.WhereHoanThue.Remove(this.WhereHoanThue.Length - 2);
                this.WhereHoanThue += ")";
            }
            if (this.TKNKhongThuCollection.Count > 0)
            {
                this.WhereKhongThu = " AND (";
                foreach (BKToKhaiNhap tkn in this.TKNKhongThuCollection)
                {
                    this.WhereKhongThu += " (SoToKhaiNhap = " + tkn.SoToKhai + " AND MaLoaiHinhNhap = '" + tkn.MaLoaiHinh + "' AND year(NgayDangKyNhap)= " + tkn.NamDangKy + ") OR";
                }
                this.WhereKhongThu = this.WhereKhongThu.Remove(this.WhereKhongThu.Length - 2);
                this.WhereKhongThu += ")";
            }
            if (this.TKNHoanThueCollection.Count > 0)
            {
                this.WhereHoanThue1 = " WHERE ";
                foreach (BKToKhaiNhap tkn in this.TKNHoanThueCollection)
                {
                    this.WhereHoanThue1 += " (a.SoToKhai = " + tkn.SoToKhai + " AND a.MaLoaiHinh = '" + tkn.MaLoaiHinh + "' AND year(a.NgayDangKy)= " + tkn.NamDangKy + ") OR";
                }
                this.WhereHoanThue1 = this.WhereHoanThue1.Remove(this.WhereHoanThue1.Length - 2);
            }
            if (this.TKNKhongThuCollection.Count > 0)
            {
                this.WhereKhongThu1 = " WHERE ";
                foreach (BKToKhaiNhap tkn in this.TKNKhongThuCollection)
                {
                    this.WhereKhongThu1 += " (a.SoToKhai = " + tkn.SoToKhai + " AND a.MaLoaiHinh = '" + tkn.MaLoaiHinh + "' AND year(a.NgayDangKy)= " + tkn.NamDangKy + ") OR";
                }
                this.WhereKhongThu1 = this.WhereKhongThu1.Remove(this.WhereKhongThu1.Length - 2);
            }
            if (GlobalSettings.GiaoDien.Id == "Office2003")
                this.defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.BKTKN.LanThanhLy = this.LanThanhLy;
            this.BKTKN.SoHoSo = this.SoHoSo;
            this.BKTKN.BindReport("");
            printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
            this.BKTKN.CreateDocument();

        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.BCNPLNXT_TT194.LanThanhLy = this.LanThanhLy;
                this.BCNPLNXT_TT194.SoHoSo = this.SoHoSo;
                this.BCNPLNXT_TT194.BindReportByMaNPL("", cboToKhai.Text.ToString());
                printControl1.PrintingSystem = this.BCNPLNXT_TT194.PrintingSystem;
                this.BCNPLNXT_TT194.CreateDocument();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void BindComBo()
        {
            try
            {
                cboToKhai.DataSource = new Company.BLL.KDT.SXXK.BCXuatNhapTon().GetMaNPL(GlobalSettings.MA_DON_VI, this.LanThanhLy).Tables[0];
                cboToKhai.DisplayMember = "MaNPL";
                cboToKhai.ValueMember = "MaNPL";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}