﻿namespace Company.Interface.GC
{
    partial class BaoCaoQuyetToanHopDong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem15 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem16 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem17 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem18 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem19 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem20 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem21 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem22 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem23 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem24 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem25 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem26 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaoCaoQuyetToanHopDong));
            Janus.Windows.GridEX.GridEXLayout dgListDataSPQT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTotal_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dateNgayTN = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.cbbDVT = new Janus.Windows.EditControls.UIComboBox();
            this.cbbLoaiHinh = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamQT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSendEdit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdSendEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ReadExcelFile = new Janus.Windows.EditControls.UIButton();
            this.btnView = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.lblFileName = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbbTenSP = new Janus.Windows.EditControls.UIComboBox();
            this.cbbNhomSP = new Janus.Windows.EditControls.UIComboBox();
            this.cbbMaSP = new Janus.Windows.EditControls.UIComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnPrintReportNumber = new Janus.Windows.EditControls.UIButton();
            this.btnPrintReportValue = new Janus.Windows.EditControls.UIButton();
            this.btnProcess = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbTenNPL = new Janus.Windows.EditControls.UIComboBox();
            this.cbbMa = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpNPLQT = new System.Windows.Forms.TabPage();
            this.dgListNPL = new Janus.Windows.GridEX.GridEX();
            this.tpSPQT = new System.Windows.Forms.TabPage();
            this.dgListDataSPQT = new Janus.Windows.GridEX.GridEX();
            this.tpTotal = new System.Windows.Forms.TabPage();
            this.dgListTotal = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpNPLQT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).BeginInit();
            this.tpSPQT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataSPQT)).BeginInit();
            this.tpTotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(1330, 807);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExportExcel});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(141, 26);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(140, 22);
            this.mnuExportExcel.Text = "XUẤT EXCEL";
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.dateNgayTN);
            this.uiGroupBox1.Controls.Add(this.cbbDVT);
            this.uiGroupBox1.Controls.Add(this.cbbLoaiHinh);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.txtNamQT);
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1330, 146);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "THÔNG TIN CHUNG";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // dateNgayTN
            // 
            this.dateNgayTN.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateNgayTN.Appearance.Options.UseFont = true;
            this.dateNgayTN.Location = new System.Drawing.Point(469, 22);
            this.dateNgayTN.Name = "dateNgayTN";
            this.dateNgayTN.ReadOnly = false;
            this.dateNgayTN.Size = new System.Drawing.Size(152, 23);
            this.dateNgayTN.TabIndex = 2;
            this.dateNgayTN.TagName = "";
            this.dateNgayTN.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayTN.WhereCondition = "";
            // 
            // cbbDVT
            // 
            this.cbbDVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbDVT.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbDVT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "Trị giá";
            uiComboBoxItem14.Value = "1";
            uiComboBoxItem15.FormatStyle.Alpha = 0;
            uiComboBoxItem15.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiComboBoxItem15.IsSeparator = false;
            uiComboBoxItem15.Text = "Số lượng";
            uiComboBoxItem15.Value = "2";
            this.cbbDVT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem14,
            uiComboBoxItem15});
            this.cbbDVT.Location = new System.Drawing.Point(819, 66);
            this.cbbDVT.Name = "cbbDVT";
            this.cbbDVT.Size = new System.Drawing.Size(161, 22);
            this.cbbDVT.TabIndex = 5;
            this.cbbDVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbLoaiHinh
            // 
            this.cbbLoaiHinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiHinh.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbLoaiHinh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem16.FormatStyle.Alpha = 0;
            uiComboBoxItem16.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem16.IsSeparator = false;
            uiComboBoxItem16.Text = "Gia công";
            uiComboBoxItem16.Value = "1";
            this.cbbLoaiHinh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem16});
            this.cbbLoaiHinh.Location = new System.Drawing.Point(469, 66);
            this.cbbLoaiHinh.Name = "cbbLoaiHinh";
            this.cbbLoaiHinh.Size = new System.Drawing.Size(152, 22);
            this.cbbLoaiHinh.TabIndex = 4;
            this.cbbLoaiHinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(153, 22);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(150, 22);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(153, 111);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(1129, 22);
            this.txtGhiChu.TabIndex = 6;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNamQT
            // 
            this.txtNamQT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamQT.Location = new System.Drawing.Point(153, 66);
            this.txtNamQT.Name = "txtNamQT";
            this.txtNamQT.Size = new System.Drawing.Size(150, 22);
            this.txtNamQT.TabIndex = 3;
            this.txtNamQT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(815, 26);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(84, 14);
            this.lblTrangThai.TabIndex = 0;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(674, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Trạng thái: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(336, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Ngày tiếp nhận";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(674, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Đơn vị tính báo cáo :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(336, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Loại hình báo cáo :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Năm quyết toán :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(27, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ghi chú :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(26, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số tiếp nhận";
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSave,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdSendEdit,
            this.cmdUpdateGuidString});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.LockCommandBars = true;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend1,
            this.cmdSave1,
            this.cmdFeedback1,
            this.cmdResult1,
            this.cmdUpdateGuidString1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(552, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            // 
            // cmdSend
            // 
            this.cmdSend.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSendEdit1});
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSendEdit1
            // 
            this.cmdSendEdit1.Key = "cmdSendEdit";
            this.cmdSendEdit1.Name = "cmdSendEdit1";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu lại";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "Lấy phản hồi";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdSendEdit
            // 
            this.cmdSendEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendEdit.Image")));
            this.cmdSendEdit.Key = "cmdSendEdit";
            this.cmdSendEdit.Name = "cmdSendEdit";
            this.cmdSendEdit.Text = "Khai báo sửa";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Text = "Cập nhật chuỗi phản hồi";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1330, 28);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ReadExcelFile);
            this.uiGroupBox2.Controls.Add(this.btnView);
            this.uiGroupBox2.Controls.Add(this.btnAdd);
            this.uiGroupBox2.Controls.Add(this.lblFileName);
            this.uiGroupBox2.Controls.Add(this.label20);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 146);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1330, 47);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "FILE ĐÍNH KÈM";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ReadExcelFile
            // 
            this.ReadExcelFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ReadExcelFile.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReadExcelFile.Image = ((System.Drawing.Image)(resources.GetObject("ReadExcelFile.Image")));
            this.ReadExcelFile.ImageIndex = 4;
            this.ReadExcelFile.ImageSize = new System.Drawing.Size(20, 20);
            this.ReadExcelFile.Location = new System.Drawing.Point(1028, 15);
            this.ReadExcelFile.Name = "ReadExcelFile";
            this.ReadExcelFile.Size = new System.Drawing.Size(288, 25);
            this.ReadExcelFile.TabIndex = 10;
            this.ReadExcelFile.Text = "Đọc báo cáo quyết toán từ File Excel";
            this.ReadExcelFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.ReadExcelFile.Click += new System.EventHandler(this.ReadExcelFile_Click);
            // 
            // btnView
            // 
            this.btnView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnView.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.Image = ((System.Drawing.Image)(resources.GetObject("btnView.Image")));
            this.btnView.ImageIndex = 4;
            this.btnView.ImageSize = new System.Drawing.Size(20, 20);
            this.btnView.Location = new System.Drawing.Point(920, 16);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(105, 25);
            this.btnView.TabIndex = 9;
            this.btnView.Text = "Xem File";
            this.btnView.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageIndex = 4;
            this.btnAdd.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAdd.Location = new System.Drawing.Point(811, 16);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(105, 25);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Thêm File";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileName.Location = new System.Drawing.Point(148, 22);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(0, 14);
            this.lblFileName.TabIndex = 7;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(27, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 14);
            this.label20.TabIndex = 0;
            this.label20.Text = "File Name :";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox4.Controls.Add(this.btnClose);
            this.uiGroupBox4.Controls.Add(this.btnPrintReportNumber);
            this.uiGroupBox4.Controls.Add(this.btnPrintReportValue);
            this.uiGroupBox4.Controls.Add(this.btnProcess);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 711);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1330, 96);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.label11);
            this.uiGroupBox6.Controls.Add(this.cbbTenSP);
            this.uiGroupBox6.Controls.Add(this.cbbNhomSP);
            this.uiGroupBox6.Controls.Add(this.cbbMaSP);
            this.uiGroupBox6.Controls.Add(this.label12);
            this.uiGroupBox6.Controls.Add(this.label14);
            this.uiGroupBox6.Controls.Add(this.label13);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(332, 8);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(565, 85);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.Text = "XỬ LÝ SẢN PHẨM";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(148, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 17;
            // 
            // cbbTenSP
            // 
            this.cbbTenSP.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbTenSP.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem17.FormatStyle.Alpha = 0;
            uiComboBoxItem17.IsSeparator = false;
            uiComboBoxItem17.Text = "GỘP CÁC TÊN SP GIỐNG NHAU";
            uiComboBoxItem17.Value = true;
            uiComboBoxItem18.FormatStyle.Alpha = 0;
            uiComboBoxItem18.IsSeparator = false;
            uiComboBoxItem18.Text = "KHÔNG GỘP CÁC TÊN SP GIỐNG NHAU";
            uiComboBoxItem18.Value = false;
            this.cbbTenSP.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem17,
            uiComboBoxItem18});
            this.cbbTenSP.Location = new System.Drawing.Point(72, 49);
            this.cbbTenSP.Name = "cbbTenSP";
            this.cbbTenSP.Size = new System.Drawing.Size(231, 21);
            this.cbbTenSP.TabIndex = 14;
            this.cbbTenSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbNhomSP
            // 
            this.cbbNhomSP.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbNhomSP.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem19.FormatStyle.Alpha = 0;
            uiComboBoxItem19.IsSeparator = false;
            uiComboBoxItem19.Text = "GỘP THEO NHÓM SẢN PHẨM GIA CÔNG";
            uiComboBoxItem19.Value = true;
            uiComboBoxItem20.FormatStyle.Alpha = 0;
            uiComboBoxItem20.IsSeparator = false;
            uiComboBoxItem20.Text = "KHÔNG GỘP THEO NHÓM SẢN PHẨM GIA CÔNG";
            uiComboBoxItem20.Value = false;
            this.cbbNhomSP.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem19,
            uiComboBoxItem20});
            this.cbbNhomSP.Location = new System.Drawing.Point(320, 48);
            this.cbbNhomSP.Name = "cbbNhomSP";
            this.cbbNhomSP.Size = new System.Drawing.Size(231, 21);
            this.cbbNhomSP.TabIndex = 15;
            this.cbbNhomSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbMaSP
            // 
            this.cbbMaSP.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbMaSP.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem21.FormatStyle.Alpha = 0;
            uiComboBoxItem21.IsSeparator = false;
            uiComboBoxItem21.Text = "GỘP CÁC MÃ SP GIỐNG NHAU";
            uiComboBoxItem21.Value = true;
            uiComboBoxItem22.FormatStyle.Alpha = 0;
            uiComboBoxItem22.IsSeparator = false;
            uiComboBoxItem22.Text = "KHÔNG GỘP CÁC MÃ SP GIỐNG NHAU";
            uiComboBoxItem22.Value = false;
            this.cbbMaSP.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem21,
            uiComboBoxItem22});
            this.cbbMaSP.Location = new System.Drawing.Point(72, 19);
            this.cbbMaSP.Name = "cbbMaSP";
            this.cbbMaSP.Size = new System.Drawing.Size(231, 21);
            this.cbbMaSP.TabIndex = 13;
            this.cbbMaSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Tên SP ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(317, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(126, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Nhóm sản phẩm gia công";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã SP ";
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageIndex = 4;
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1060, 58);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(139, 28);
            this.btnClose.TabIndex = 19;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPrintReportNumber
            // 
            this.btnPrintReportNumber.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintReportNumber.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintReportNumber.Image")));
            this.btnPrintReportNumber.ImageIndex = 4;
            this.btnPrintReportNumber.ImageSize = new System.Drawing.Size(20, 20);
            this.btnPrintReportNumber.Location = new System.Drawing.Point(915, 22);
            this.btnPrintReportNumber.Name = "btnPrintReportNumber";
            this.btnPrintReportNumber.Size = new System.Drawing.Size(139, 28);
            this.btnPrintReportNumber.TabIndex = 16;
            this.btnPrintReportNumber.Text = "Mẫu 15 (Lượng)";
            this.btnPrintReportNumber.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnPrintReportNumber.Click += new System.EventHandler(this.btnPrintReportNumber_Click);
            // 
            // btnPrintReportValue
            // 
            this.btnPrintReportValue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintReportValue.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintReportValue.Image")));
            this.btnPrintReportValue.ImageIndex = 4;
            this.btnPrintReportValue.ImageSize = new System.Drawing.Size(20, 20);
            this.btnPrintReportValue.Location = new System.Drawing.Point(1060, 22);
            this.btnPrintReportValue.Name = "btnPrintReportValue";
            this.btnPrintReportValue.Size = new System.Drawing.Size(139, 28);
            this.btnPrintReportValue.TabIndex = 18;
            this.btnPrintReportValue.Text = "Mẫu 15 (Trị giá)";
            this.btnPrintReportValue.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnPrintReportValue.Click += new System.EventHandler(this.btnPrintReportValue_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnProcess.Image")));
            this.btnProcess.ImageIndex = 4;
            this.btnProcess.ImageSize = new System.Drawing.Size(20, 20);
            this.btnProcess.Location = new System.Drawing.Point(915, 58);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(139, 28);
            this.btnProcess.TabIndex = 17;
            this.btnProcess.Text = "Xử lý báo cáo";
            this.btnProcess.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.cbbTenNPL);
            this.uiGroupBox5.Controls.Add(this.cbbMa);
            this.uiGroupBox5.Controls.Add(this.label4);
            this.uiGroupBox5.Controls.Add(this.label10);
            this.uiGroupBox5.Controls.Add(this.label8);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(329, 85);
            this.uiGroupBox5.TabIndex = 0;
            this.uiGroupBox5.Text = "XỬ LÝ NGUYÊN PHỤ LIỆU";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // cbbTenNPL
            // 
            this.cbbTenNPL.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbTenNPL.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem23.FormatStyle.Alpha = 0;
            uiComboBoxItem23.IsSeparator = false;
            uiComboBoxItem23.Text = "GỘP CÁC TÊN NPL GIỐNG NHAU";
            uiComboBoxItem23.Value = true;
            uiComboBoxItem24.FormatStyle.Alpha = 0;
            uiComboBoxItem24.IsSeparator = false;
            uiComboBoxItem24.Text = "KHÔNG GỘP CÁC TÊN NPL GIỐNG NHAU";
            uiComboBoxItem24.Value = false;
            this.cbbTenNPL.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem23,
            uiComboBoxItem24});
            this.cbbTenNPL.Location = new System.Drawing.Point(91, 50);
            this.cbbTenNPL.Name = "cbbTenNPL";
            this.cbbTenNPL.Size = new System.Drawing.Size(231, 21);
            this.cbbTenNPL.TabIndex = 12;
            this.cbbTenNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbMa
            // 
            this.cbbMa.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbMa.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem25.FormatStyle.Alpha = 0;
            uiComboBoxItem25.IsSeparator = false;
            uiComboBoxItem25.Text = "GỘP CÁC MÃ NPL GIỐNG NHAU";
            uiComboBoxItem25.Value = true;
            uiComboBoxItem26.FormatStyle.Alpha = 0;
            uiComboBoxItem26.IsSeparator = false;
            uiComboBoxItem26.Text = "KHÔNG GỘP CÁC MÃ NPL GIỐNG NHAU";
            uiComboBoxItem26.Value = false;
            this.cbbMa.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem25,
            uiComboBoxItem26});
            this.cbbMa.Location = new System.Drawing.Point(91, 20);
            this.cbbMa.Name = "cbbMa";
            this.cbbMa.Size = new System.Drawing.Size(231, 21);
            this.cbbMa.TabIndex = 11;
            this.cbbMa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(148, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tên NPL ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Mã NPL";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(103)))));
            this.uiGroupBox3.Controls.Add(this.tabControl1);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.ForeColor = System.Drawing.Color.DarkGreen;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 193);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1330, 518);
            this.uiGroupBox3.TabIndex = 12;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpNPLQT);
            this.tabControl1.Controls.Add(this.tpSPQT);
            this.tabControl1.Controls.Add(this.tpTotal);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(3, 8);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1324, 507);
            this.tabControl1.TabIndex = 0;
            // 
            // tpNPLQT
            // 
            this.tpNPLQT.Controls.Add(this.dgListNPL);
            this.tpNPLQT.Location = new System.Drawing.Point(4, 22);
            this.tpNPLQT.Margin = new System.Windows.Forms.Padding(0);
            this.tpNPLQT.Name = "tpNPLQT";
            this.tpNPLQT.Size = new System.Drawing.Size(1316, 481);
            this.tpNPLQT.TabIndex = 0;
            this.tpNPLQT.Text = "TỔNG HỢP SỐ LIỆU NGUYÊN PHỤ LIỆU QUYẾT TOÁN (ĐỐI CHIẾU VÀ KIỂM TRA)";
            this.tpNPLQT.UseVisualStyleBackColor = true;
            // 
            // dgListNPL
            // 
            this.dgListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPL.AlternatingColors = true;
            this.dgListNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPL.ColumnAutoResize = true;
            this.dgListNPL.ContextMenuStrip = this.contextMenuStrip1;
            dgListNPL_DesignTimeLayout.LayoutString = resources.GetString("dgListNPL_DesignTimeLayout.LayoutString");
            this.dgListNPL.DesignTimeLayout = dgListNPL_DesignTimeLayout;
            this.dgListNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPL.FrozenColumns = 3;
            this.dgListNPL.GroupByBoxVisible = false;
            this.dgListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPL.Location = new System.Drawing.Point(0, 0);
            this.dgListNPL.Margin = new System.Windows.Forms.Padding(0);
            this.dgListNPL.Name = "dgListNPL";
            this.dgListNPL.RecordNavigator = true;
            this.dgListNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.Size = new System.Drawing.Size(1316, 481);
            this.dgListNPL.TabIndex = 7;
            this.dgListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpSPQT
            // 
            this.tpSPQT.Controls.Add(this.dgListDataSPQT);
            this.tpSPQT.Location = new System.Drawing.Point(4, 22);
            this.tpSPQT.Name = "tpSPQT";
            this.tpSPQT.Size = new System.Drawing.Size(1316, 481);
            this.tpSPQT.TabIndex = 1;
            this.tpSPQT.Text = "TỔNG HỢP SỐ LIỆU SẢN PHẨM QUYẾT TOÁN (ĐỐI CHIẾU VÀ KIỂM TRA)";
            this.tpSPQT.UseVisualStyleBackColor = true;
            // 
            // dgListDataSPQT
            // 
            this.dgListDataSPQT.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDataSPQT.AlternatingColors = true;
            this.dgListDataSPQT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDataSPQT.ColumnAutoResize = true;
            this.dgListDataSPQT.ContextMenuStrip = this.contextMenuStrip1;
            dgListDataSPQT_DesignTimeLayout.LayoutString = resources.GetString("dgListDataSPQT_DesignTimeLayout.LayoutString");
            this.dgListDataSPQT.DesignTimeLayout = dgListDataSPQT_DesignTimeLayout;
            this.dgListDataSPQT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDataSPQT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDataSPQT.FrozenColumns = 3;
            this.dgListDataSPQT.GroupByBoxVisible = false;
            this.dgListDataSPQT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataSPQT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataSPQT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDataSPQT.Location = new System.Drawing.Point(0, 0);
            this.dgListDataSPQT.Margin = new System.Windows.Forms.Padding(0);
            this.dgListDataSPQT.Name = "dgListDataSPQT";
            this.dgListDataSPQT.RecordNavigator = true;
            this.dgListDataSPQT.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDataSPQT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDataSPQT.Size = new System.Drawing.Size(1316, 481);
            this.dgListDataSPQT.TabIndex = 9;
            this.dgListDataSPQT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpTotal
            // 
            this.tpTotal.Controls.Add(this.dgListTotal);
            this.tpTotal.Location = new System.Drawing.Point(4, 22);
            this.tpTotal.Name = "tpTotal";
            this.tpTotal.Size = new System.Drawing.Size(1316, 481);
            this.tpTotal.TabIndex = 2;
            this.tpTotal.Text = "TỔNG HỢP SỐ LIỆU BÁO CÁO (SỐ LIỆU GỬI LÊN HQ)";
            this.tpTotal.UseVisualStyleBackColor = true;
            // 
            // dgListTotal
            // 
            this.dgListTotal.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotal.AlternatingColors = true;
            this.dgListTotal.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotal.ColumnAutoResize = true;
            this.dgListTotal.ContextMenuStrip = this.contextMenuStrip1;
            dgListTotal_DesignTimeLayout.LayoutString = resources.GetString("dgListTotal_DesignTimeLayout.LayoutString");
            this.dgListTotal.DesignTimeLayout = dgListTotal_DesignTimeLayout;
            this.dgListTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListTotal.FrozenColumns = 3;
            this.dgListTotal.GroupByBoxVisible = false;
            this.dgListTotal.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotal.Location = new System.Drawing.Point(0, 0);
            this.dgListTotal.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotal.Name = "dgListTotal";
            this.dgListTotal.RecordNavigator = true;
            this.dgListTotal.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotal.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotal.Size = new System.Drawing.Size(1316, 481);
            this.dgListTotal.TabIndex = 10;
            this.dgListTotal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // BaoCaoQuyetToanHopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1330, 835);
            this.Controls.Add(this.TopRebar1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BaoCaoQuyetToanHopDong";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Báo cáo quyết toán hợp đồng";
            this.Load += new System.EventHandler(this.BaoCaoQuyetToanHopDong_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpNPLQT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).EndInit();
            this.tpSPQT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataSPQT)).EndInit();
            this.tpTotal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamQT;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHinh;
        private Janus.Windows.EditControls.UIComboBox cbbDVT;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIComboBox cbbTenSP;
        private Janus.Windows.EditControls.UIComboBox cbbNhomSP;
        private Janus.Windows.EditControls.UIComboBox cbbMaSP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIComboBox cbbTenNPL;
        private Janus.Windows.EditControls.UIComboBox cbbMa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpNPLQT;
        private Janus.Windows.GridEX.GridEX dgListNPL;
        private System.Windows.Forms.TabPage tpSPQT;
        private Janus.Windows.GridEX.GridEX dgListDataSPQT;
        private System.Windows.Forms.TabPage tpTotal;
        private Janus.Windows.GridEX.GridEX dgListTotal;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString;
        private Janus.Windows.EditControls.UIButton ReadExcelFile;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.EditControls.UIButton btnPrintReportNumber;
        private Janus.Windows.EditControls.UIButton btnPrintReportValue;
        private Janus.Windows.EditControls.UIButton btnProcess;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayTN;
        private Janus.Windows.EditControls.UIButton btnView;
    }
}