namespace Company.Interface.SXXK.BangKe
{
    partial class BK09Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList2_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList2_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK09Form));
            Janus.Windows.GridEX.GridEXLayout dgList1_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList1_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvToKhai = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.dgList2 = new Janus.Windows.GridEX.GridEX();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cmdSave = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList1 = new Janus.Windows.GridEX.GridEX();
            this.cmdThuTucHQTruocDo = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.EditControls.UIGroupBox();
            this.cmbHinhThucXuLy = new Janus.Windows.EditControls.UIComboBox();
            this.donViTinhQuyDoiControl1 = new Company.Interface.Controls.DonViTinhQuyDoiControl();
            this.txtNopCLGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNopTTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNopVAT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNopNK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ccNgayNopThue = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.cmdAdd = new Janus.Windows.EditControls.UIButton();
            this.txtDonViTinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.dgList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Size = new System.Drawing.Size(977, 635);
            this.grbMain.Click += new System.EventHandler(this.grbMain_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            this.epError.Icon = ((System.Drawing.Icon)(resources.GetObject("epError.Icon")));
            // 
            // cvError
            // 
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvToKhai
            // 
            this.rfvToKhai.ErrorMessage = "\"Tờ khai nhập\" không được để trống.";
            this.rfvToKhai.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvToKhai.Icon")));
            // 
            // dgList2
            // 
            this.dgList2.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList2.AlternatingColors = true;
            this.dgList2.AutomaticSort = false;
            this.dgList2.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList2.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList2.ColumnAutoResize = true;
            dgList2_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList2_DesignTimeLayout_Reference_0.Instance")));
            dgList2_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList2_DesignTimeLayout_Reference_0});
            dgList2_DesignTimeLayout.LayoutString = resources.GetString("dgList2_DesignTimeLayout.LayoutString");
            this.dgList2.DesignTimeLayout = dgList2_DesignTimeLayout;
            this.dgList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList2.GroupByBoxVisible = false;
            this.dgList2.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList2.ImageList = this.ImageList1;
            this.dgList2.Location = new System.Drawing.Point(3, 8);
            this.dgList2.Name = "dgList2";
            this.dgList2.RecordNavigator = true;
            this.dgList2.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList2.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList2.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList2.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList2.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList2.Size = new System.Drawing.Size(971, 180);
            this.dgList2.TabIndex = 0;
            this.dgList2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList2.VisualStyleManager = this.vsmMain;
            this.dgList2.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList2_LoadingRow);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(889, 17);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Location = new System.Drawing.Point(767, 17);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(116, 23);
            this.cmdSave.TabIndex = 5;
            this.cmdSave.Text = "Lưu bảng kê";
            this.cmdSave.VisualStyleManager = this.vsmMain;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(423, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Danh sách nguyên phụ liệu của tờ khai nhập khẩu đưa vào thanh lý";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgList1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 14);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(977, 164);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // dgList1
            // 
            this.dgList1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList1.AlternatingColors = true;
            this.dgList1.AutomaticSort = false;
            this.dgList1.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList1.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList1.ColumnAutoResize = true;
            dgList1_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList1_DesignTimeLayout_Reference_0.Instance")));
            dgList1_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList1_DesignTimeLayout_Reference_0});
            dgList1_DesignTimeLayout.LayoutString = resources.GetString("dgList1_DesignTimeLayout.LayoutString");
            this.dgList1.DesignTimeLayout = dgList1_DesignTimeLayout;
            this.dgList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList1.GroupByBoxVisible = false;
            this.dgList1.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList1.ImageList = this.ImageList1;
            this.dgList1.Location = new System.Drawing.Point(3, 8);
            this.dgList1.Name = "dgList1";
            this.dgList1.RecordNavigator = true;
            this.dgList1.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList1.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList1.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList1.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList1.Size = new System.Drawing.Size(971, 153);
            this.dgList1.TabIndex = 0;
            this.dgList1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList1.VisualStyleManager = this.vsmMain;
            this.dgList1.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList1_RowDoubleClick);
            this.dgList1.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList1_LoadingRow);
            // 
            // cmdThuTucHQTruocDo
            // 
            this.cmdThuTucHQTruocDo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdThuTucHQTruocDo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdThuTucHQTruocDo.Image = ((System.Drawing.Image)(resources.GetObject("cmdThuTucHQTruocDo.Image")));
            this.cmdThuTucHQTruocDo.Location = new System.Drawing.Point(621, 17);
            this.cmdThuTucHQTruocDo.Name = "cmdThuTucHQTruocDo";
            this.cmdThuTucHQTruocDo.Size = new System.Drawing.Size(140, 23);
            this.cmdThuTucHQTruocDo.TabIndex = 8;
            this.cmdThuTucHQTruocDo.Text = "Thủ tục HQ trước đó";
            this.cmdThuTucHQTruocDo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmdThuTucHQTruocDo.Click += new System.EventHandler(this.cmdThuTucHQTruocDo_Click);
            // 
            // dgList
            // 
            this.dgList.AutoScroll = true;
            this.dgList.BackColor = System.Drawing.Color.Transparent;
            this.dgList.Controls.Add(this.cmbHinhThucXuLy);
            this.dgList.Controls.Add(this.donViTinhQuyDoiControl1);
            this.dgList.Controls.Add(this.txtNopCLGia);
            this.dgList.Controls.Add(this.label14);
            this.dgList.Controls.Add(this.txtNopTTDB);
            this.dgList.Controls.Add(this.label13);
            this.dgList.Controls.Add(this.txtNopVAT);
            this.dgList.Controls.Add(this.label11);
            this.dgList.Controls.Add(this.txtNopNK);
            this.dgList.Controls.Add(this.label8);
            this.dgList.Controls.Add(this.label9);
            this.dgList.Controls.Add(this.ccNgayNopThue);
            this.dgList.Controls.Add(this.label7);
            this.dgList.Controls.Add(this.cmdAdd);
            this.dgList.Controls.Add(this.txtDonViTinh);
            this.dgList.Controls.Add(this.txtTenNPL);
            this.dgList.Controls.Add(this.label3);
            this.dgList.Controls.Add(this.txtMaNPL);
            this.dgList.Controls.Add(this.label2);
            this.dgList.Controls.Add(this.txtSoToKhai);
            this.dgList.Controls.Add(this.label4);
            this.dgList.Controls.Add(this.label6);
            this.dgList.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgList.Location = new System.Drawing.Point(0, 178);
            this.dgList.Name = "dgList";
            this.dgList.Size = new System.Drawing.Size(977, 200);
            this.dgList.TabIndex = 9;
            this.dgList.Text = "Thông tin NPL tạm nộp thuế";
            this.dgList.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // cmbHinhThucXuLy
            // 
            this.cmbHinhThucXuLy.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbHinhThucXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Tiêu thụ nội địa";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Tiêu hủy";
            this.cmbHinhThucXuLy.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cmbHinhThucXuLy.Location = new System.Drawing.Point(283, 86);
            this.cmbHinhThucXuLy.Name = "cmbHinhThucXuLy";
            this.cmbHinhThucXuLy.Size = new System.Drawing.Size(117, 21);
            this.cmbHinhThucXuLy.TabIndex = 18;
            this.cmbHinhThucXuLy.Text = "Hình Thức xử lý";
            this.cmbHinhThucXuLy.VisualStyleManager = this.vsmMain;
            // 
            // donViTinhQuyDoiControl1
            // 
            this.donViTinhQuyDoiControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViTinhQuyDoiControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViTinhQuyDoiControl1.Location = new System.Drawing.Point(412, 24);
            this.donViTinhQuyDoiControl1.Name = "donViTinhQuyDoiControl1";
            this.donViTinhQuyDoiControl1.Size = new System.Drawing.Size(245, 86);
            this.donViTinhQuyDoiControl1.TabIndex = 17;
            // 
            // txtNopCLGia
            // 
            this.txtNopCLGia.DecimalDigits = 3;
            this.txtNopCLGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNopCLGia.Location = new System.Drawing.Point(283, 164);
            this.txtNopCLGia.Name = "txtNopCLGia";
            this.txtNopCLGia.Size = new System.Drawing.Size(117, 21);
            this.txtNopCLGia.TabIndex = 13;
            this.txtNopCLGia.Text = "0.000";
            this.txtNopCLGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtNopCLGia.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(230, 169);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "CL giá";
            // 
            // txtNopTTDB
            // 
            this.txtNopTTDB.DecimalDigits = 3;
            this.txtNopTTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNopTTDB.Location = new System.Drawing.Point(120, 164);
            this.txtNopTTDB.Name = "txtNopTTDB";
            this.txtNopTTDB.Size = new System.Drawing.Size(100, 21);
            this.txtNopTTDB.TabIndex = 11;
            this.txtNopTTDB.Text = "0.000";
            this.txtNopTTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtNopTTDB.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(19, 169);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Nộp TTDB";
            // 
            // txtNopVAT
            // 
            this.txtNopVAT.DecimalDigits = 3;
            this.txtNopVAT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNopVAT.Location = new System.Drawing.Point(499, 138);
            this.txtNopVAT.Name = "txtNopVAT";
            this.txtNopVAT.Size = new System.Drawing.Size(158, 21);
            this.txtNopVAT.TabIndex = 9;
            this.txtNopVAT.Text = "0.000";
            this.txtNopVAT.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtNopVAT.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(440, 143);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Nộp VAT";
            // 
            // txtNopNK
            // 
            this.txtNopNK.DecimalDigits = 3;
            this.txtNopNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNopNK.Location = new System.Drawing.Point(283, 138);
            this.txtNopNK.Name = "txtNopNK";
            this.txtNopNK.Size = new System.Drawing.Size(117, 21);
            this.txtNopNK.TabIndex = 7;
            this.txtNopNK.Text = "0.000";
            this.txtNopNK.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtNopNK.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(230, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "xử lý";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(232, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Nộp NK";
            // 
            // ccNgayNopThue
            // 
            this.ccNgayNopThue.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayNopThue.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayNopThue.DropDownCalendar.Name = "";
            this.ccNgayNopThue.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayNopThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayNopThue.Location = new System.Drawing.Point(120, 138);
            this.ccNgayNopThue.Name = "ccNgayNopThue";
            this.ccNgayNopThue.Nullable = true;
            this.ccNgayNopThue.NullButtonText = "Xóa";
            this.ccNgayNopThue.ShowNullButton = true;
            this.ccNgayNopThue.Size = new System.Drawing.Size(100, 21);
            this.ccNgayNopThue.TabIndex = 5;
            this.ccNgayNopThue.TodayButtonText = "Hôm nay";
            this.ccNgayNopThue.Value = new System.DateTime(2013, 3, 24, 0, 0, 0, 0);
            this.ccNgayNopThue.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayNopThue.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 143);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Ngày nộp thuế";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAdd.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdd.Image")));
            this.cmdAdd.Location = new System.Drawing.Point(557, 169);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(100, 23);
            this.cmdAdd.TabIndex = 16;
            this.cmdAdd.Text = "Thêm";
            this.cmdAdd.VisualStyleManager = this.vsmMain;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // txtDonViTinh
            // 
            this.txtDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonViTinh.Location = new System.Drawing.Point(117, 86);
            this.txtDonViTinh.Name = "txtDonViTinh";
            this.txtDonViTinh.ReadOnly = true;
            this.txtDonViTinh.Size = new System.Drawing.Size(103, 21);
            this.txtDonViTinh.TabIndex = 3;
            this.txtDonViTinh.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNPL.Location = new System.Drawing.Point(117, 54);
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.ReadOnly = true;
            this.txtTenNPL.Size = new System.Drawing.Size(283, 21);
            this.txtTenNPL.TabIndex = 3;
            this.txtTenNPL.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Đơn vị Tính";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNPL.Location = new System.Drawing.Point(283, 24);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.ReadOnly = true;
            this.txtMaNPL.Size = new System.Drawing.Size(117, 21);
            this.txtMaNPL.TabIndex = 3;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên NPL";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(117, 24);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.ReadOnly = true;
            this.txtSoToKhai.Size = new System.Drawing.Size(100, 21);
            this.txtSoToKhai.TabIndex = 1;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(230, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Mã NPL";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tờ khai nhập";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(0, 378);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(262, 14);
            this.label5.TabIndex = 10;
            this.label5.Text = "Danh sách nguyên phụ liệu tạm nộp thuế";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.cmdThuTucHQTruocDo);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.cmdSave);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 583);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(977, 52);
            this.uiGroupBox1.TabIndex = 11;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgList2);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 392);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(977, 191);
            this.uiGroupBox3.TabIndex = 12;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // BK09Form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(977, 635);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK09Form";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Bảng kê nguyên phụ liệu không xuất khẩu xin nộp thuế vào ngân sách";
            this.Load += new System.EventHandler(this.BK09Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.dgList.ResumeLayout(false);
            this.dgList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvToKhai;
        private Janus.Windows.GridEX.GridEX dgList2;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton cmdSave;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgList1;
        private Janus.Windows.EditControls.UIButton cmdThuTucHQTruocDo;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox dgList;
        private Janus.Windows.EditControls.UIComboBox cmbHinhThucXuLy;
        private Company.Interface.Controls.DonViTinhQuyDoiControl donViTinhQuyDoiControl1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNopCLGia;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNopTTDB;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNopVAT;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNopNK;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayNopThue;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIButton cmdAdd;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonViTinh;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
    }
}