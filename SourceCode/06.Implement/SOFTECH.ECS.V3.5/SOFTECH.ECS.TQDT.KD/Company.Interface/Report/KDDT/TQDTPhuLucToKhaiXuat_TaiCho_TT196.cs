using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif


using System.Collections.Generic;
using System.Linq;

namespace Company.Interface.Report
{
    public partial class TQDTPhuLucToKhaiXuat_TaiCho_TT196 : DevExpress.XtraReports.UI.XtraReport
    {
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        public int SoToKhai;
        public DateTime NgayDangKy;
        public ToKhaiMauDich TKMD;
        public Company.Interface.Report.ReportViewTKXTQDT_TaiCho_FormTT196 report;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public int soDongHang;
        public int sTT_Container;
        public bool isCuaKhau = true;
        public bool inTriGiaTT = false;
#if KD_V3 || KD_V4
        double _TongTriGiaNguyenTe = 0;
        double _TongThueXuatKhau = 0;
        double _TongThueThuKhac = 0;
         int formatSoLuong = GlobalSettings.SoThapPhan.SoLuongHMD;
         int formatDonGiaNT = GlobalSettings.SoThapPhan.DonGiaNT;
        int formatTriGiaNT = GlobalSettings.SoThapPhan.TriGiaNT;
#elif GC_V3 || GC_V4
        decimal _TongTriGiaNguyenTe = 0;
        decimal _TongThueXuatKhau = 0;
        decimal _TongThueThuKhac = 0;
        int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
        int formatDonGiaNT = GlobalSettings.DonGiaNT;
        int formatTriGiaNT = GlobalSettings.TriGiaNT;
#elif SXXK_V3 || SXXK_V4
        decimal _TongTriGiaNguyenTe = 0;
        decimal _TongThueXuatKhau = 0;
        decimal _TongThueThuKhac = 0;
        int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
        int formatDonGiaNT = GlobalSettings.DonGiaNT;
        int formatTriGiaNT = GlobalSettings.TriGiaNT;
        //#elif 
        //double _TongTriGiaNguyenTe = 0;
        //double _TongThueXuatKhau = 0;
        //double _TongThueThuKhac = 0;
#endif
        double _TinhTongTrongLuong = 0;

        public TQDTPhuLucToKhaiXuat_TaiCho_TT196()
        {
            InitializeComponent();
        }
        public string GetChiCucHQCK()
        {
            if (isCuaKhau)
            {
                string chicuc = NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                if (chicuc == string.Empty)
                    chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                return chicuc;
            }
            else
                return string.Empty;
        }
        private void setFont()
        {
            xrTable3.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            DetailThue.Font = DetailHangHoa.Font = DetailLuongHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
        }
        public void BindReport(string pls)
        {
            _TongTriGiaNguyenTe = 0;
            _TongThueXuatKhau = 0;
            _TongThueThuKhac = 0;
            _TinhTongTrongLuong = 0;
            try
            {
                float sizeFont = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8")) == 0 ? 8 : float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8"));
                setFont();
                TinhTong();
                sTT_Container = (Convert.ToInt16(pls)) * 4 - 3;
                #region Thông tin chính
                xtcChiCucHaiQuan.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                xtcChiCucCuaKhau.Text = GetChiCucHQCK(); /*CuaKhau.GetName(TKMD.CuaKhau_ID);*/
                xtcSoPhuLuc.Text = pls;
                xtcNgayDangKy.Text = NgayDangKy.ToString("dd-MM-yyyy");
                xtcSoToKhai.Text = TKMD.SoToKhai.ToString();
                xtcLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                #endregion

                #region Bảng mô tả hàng - Ô 15 - Ô 21
                if (inMaHang)
                {
                    List<HangMauDich> datasource = new List<HangMauDich>();
                    foreach (HangMauDich hmd in HMDCollection)
                    {
                        HangMauDich hangMD = new HangMauDich();
                        hangMD.MaPhu = hmd.MaPhu;
                        hangMD.SoThuTuHang = hmd.SoThuTuHang;
                        hangMD.TenHang = hmd.TenHang + "/" + hmd.MaPhu;
                        hangMD.MaHS = hmd.MaHS;
                        hangMD.NuocXX_ID = hmd.NuocXX_ID;
                        hangMD.SoLuong = hmd.SoLuong;
                        hangMD.DVT_ID = hmd.DVT_ID;
                        hangMD.DonGiaKB = hmd.DonGiaKB;
                        hangMD.TriGiaKB = hmd.TriGiaKB;
                        hangMD.TriGiaTT = hmd.TriGiaTT;
                        hangMD.ThueSuatXNK = hmd.ThueSuatXNK;
                        hangMD.ThueXNK = hmd.ThueXNK;
                        hangMD.TriGiaThuKhac = hmd.TriGiaThuKhac;
                        hangMD.TyLeThuKhac = hmd.TyLeThuKhac;
                        datasource.Add(hangMD);
                    }

                    //                     if (datasource.Count < 7)
                    //                     {
                    while (datasource.Count < 7)
                    {
                        datasource.Add(new HangMauDich { SoThuTuHang = datasource[datasource.Count - 1].SoThuTuHang + 1 });
                    }
                    //                    }
                    DetailHangHoa.DataSource = datasource;
                }
                else
                {

                    while (HMDCollection.Count < 7)
                    {
                        HMDCollection.Add(new HangMauDich { SoThuTuHang = HMDCollection[HMDCollection.Count - 1].SoThuTuHang + 1 });
                    }
                    DetailHangHoa.DataSource = HMDCollection;
                }

                xtcHang_STT.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoThuTuHang");
                xtcHang_MoTa.DataBindings.Add("Text", DetailHangHoa.DataSource, "TenHang");
                xtcHang_MaSo.DataBindings.Add("Text", DetailHangHoa.DataSource, "MaHS");
                xtcHang_XuatXu.DataBindings.Add("Text", DetailHangHoa.DataSource, "NuocXX_ID");
                xtcHang_LuongHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoLuong", Company.KDT.SHARE.Components.Globals.FormatNumber(formatSoLuong, true));
                xtcHang_DVT.DataBindings.Add("Text", DetailHangHoa.DataSource, "DVT_ID");
                xtcHang_DonGiaNT.DataBindings.Add("Text", DetailHangHoa.DataSource, "DonGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(formatDonGiaNT, true));
                xtcHang_TriGiaNT.DataBindings.Add("Text", DetailHangHoa.DataSource, "TriGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(formatTriGiaNT, true));
                xtcHang_Cong.Text = _TongTriGiaNguyenTe.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatTriGiaNT, true));
                #endregion
                #region Bảng Thuế xuất - Ô 22 + 23
                DetailThue.DataSource = HMDCollection;
                xtcThueXK_TriGiaTinhThue.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
                xtcThueXK_ThueSuat.DataBindings.Add("Text", DetailThue.DataSource, "ThueSuatXNK");
                xtcThueXK_Thue.DataBindings.Add("Text", DetailThue.DataSource, "ThueXNK", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
                xtcThueXK_STT.DataBindings.Add("Text", DetailThue.DataSource, "SoThuTuHang");
                // xtcThuKhac_TriGiaTT.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaThuKhac", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
                xtcThuKhac_TyLe.DataBindings.Add("Text", DetailThue.DataSource, "TyLeThuKhac");
                xtcThuKhac_SoTien.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaThuKhac", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
                xtcThueXK_Cong.Text = _TongThueXuatKhau.ToString("N0");
                xtcThuKhac_Cong.Text = _TongThueThuKhac.ToString("N0");
                #endregion
                //                 #region Bảng Container - Ô 25
                //                 if (TKMD.VanTaiDon != null && TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count <= 4)
                //                 {
                //                     DetailLuongHang.DataSource = TKMD.VanTaiDon.ContainerCollection;
                //                     xtcLuonghang_SoHieu.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoHieu");
                // #if KD_V4 || SXXK_V4 || GC_V4
                //                     xtcLuongHang_SoLuong.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoKien");
                //                     xtcLuongHang_TrongLuong.DataBindings.Add("Text", DetailLuongHang.DataSource, "TrongLuong");
                //                     xtcLuongHang_DiaDiem.DataBindings.Add("Text", DetailLuongHang.DataSource, "DiaDiemDongHang");
                //                     //Tong trọng luong container
                //                     double sumTrongLuong = TKMD.VanTaiDon.ContainerCollection.Sum(x => x.TrongLuong);
                //                     xtcLuongHang_Cong.Text = Convert.ToString(sumTrongLuong);
                // #endif
                //                 }
                //                 else if (TKMD.VanTaiDon != null && TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count > 4)
                //                     xtcLuonghang_SoHieu.Text = "(Phụ lục bản kê Container kèm theo)";
                //                 else
                //                 {
                //                     xrTableRow13.Size = new Size { Height = 60 };
                //                 }
                //                 #endregion

                #region Bảng Container - Ô 25
                // IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangVD = TKMD.VanTaiDon.ListHangOfVanDon.ToArray().Distinct(new DistinctSHContainer());
                List<Company.KDT.SHARE.QuanLyChungTu.Container> dataSourceContainer = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
                if (TKMD.VanTaiDon == null || TKMD.VanTaiDon.ContainerCollection == null || TKMD.VanTaiDon.ContainerCollection.Count == 0)
                {


                }
                else if (TKMD.VanTaiDon.ContainerCollection.Count <= 4)
                {
                    foreach (Company.KDT.SHARE.QuanLyChungTu.Container item in TKMD.VanTaiDon.ContainerCollection)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.Container cont = new Company.KDT.SHARE.QuanLyChungTu.Container()
                        {
                            SoHieu = item.SoHieu + "/ " + item.Seal_No,
                            Seal_No = item.Seal_No,
                            LoaiContainer = item.LoaiContainer,
                            Trang_thai = item.Trang_thai,
#if KD_V4 || SXXK_V4 || GC_V4
                            SoKien = item.SoKien,
                            TrongLuong = item.TrongLuong,
                            DiaDiemDongHang = item.DiaDiemDongHang
#endif
                        };
                        dataSourceContainer.Add(cont);
                    }
                }
                else if (TKMD.VanTaiDon.ContainerCollection.Count > 4)
                {
                    dataSourceContainer.Add(new Company.KDT.SHARE.QuanLyChungTu.Container { SoHieu = "Chi tiết phụ lục đính kèm" });
                }
                while (dataSourceContainer.Count < 4)
                    dataSourceContainer.Add(new Company.KDT.SHARE.QuanLyChungTu.Container());

                DetailLuongHang.DataSource = dataSourceContainer;
                xtcLuonghang_SoHieu.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoHieu");
#if KD_V4 || SXXK_V4 || GC_V4
                    xtcLuongHang_SoLuong.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoKien");
                    xtcLuongHang_TrongLuong.DataBindings.Add("Text", DetailLuongHang.DataSource, "TrongLuong");
                    xtcLuongHang_DiaDiem.DataBindings.Add("Text", DetailLuongHang.DataSource, "DiaDiemDongHang");
                    //Tong trọng luong container
                    double sumTrongLuong = dataSourceContainer.Sum(x => x.TrongLuong);
                    xtcLuongHang_Cong.Text = Convert.ToString(sumTrongLuong);
#endif
#if KD_V3 || SXXK_V3 || GC_V3
                //tong container
                // xtcLuongHang_CongTL.Text = TKMD.TrongLuong.ToString("###,###,##0.####") + " (kg)";
                // lblTongKien.Text = TKMD.SoKien.ToString() + " (kiện)";
#endif
                #endregion
#if SXXK_V4
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1)|| GlobalSettings.NgayHeThong)
                    lblNgayThangNam.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                else
                    lblNgayThangNam.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;
#else
                    if (TKMD.NgayDangKy == new DateTime(1900, 1, 1)||GlobalSettings.NgayHeThong)
                        lblNgayThangNam.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                    else
                        lblNgayThangNam.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;
#endif


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Co 1 loi ko xac dinh duoc la do hang ko nhap gia tri FOC ma de trong, bat buoc phai nhap = 0: Khi import excel
            }
        }

        private void xtcLuongHang_STT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRTableCell)sender).Text = sTT_Container.ToString();
            sTT_Container++;
        }
        private void TinhTong()
        {
            _TongThueThuKhac = _TongThueXuatKhau = _TongTriGiaNguyenTe = 0;
            _TinhTongTrongLuong = 0;
            foreach (HangMauDich item in HMDCollection)
            {
                _TongTriGiaNguyenTe += item.TriGiaKB;
                _TongThueXuatKhau += item.ThueXNK;
                _TongThueThuKhac += item.TriGiaThuKhac;
            }
        }
        private decimal TinhSoLuongHangContainer(string soHieuContainer)
        {
            IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangCollection = from h in TKMD.VanTaiDon.ListHangOfVanDon
                                                                                           where h.SoHieuContainer == soHieuContainer
                                                                                           select h;
            decimal soLuongHang = 0;
            foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in HangCollection)
                soLuongHang += item.SoLuong;
            return soLuongHang;
        }
        private double TinhTrongLuongHangContainer(string soHieuContainer)
        {
            IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangCollection = from h in TKMD.VanTaiDon.ListHangOfVanDon
                                                                                           where h.SoHieuContainer == soHieuContainer
                                                                                           select h;
            double trongLuong = 0;
            foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in HangCollection)
                trongLuong += item.TrongLuong;
            _TinhTongTrongLuong = +trongLuong;
            return trongLuong;
        }

        private void xtcLuongHang_SoLuong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTableCell slkien = (XRTableCell)sender;
            //if (!string.IsNullOrEmpty(slkien.Text))
            //    slkien.Text = TinhSoLuongHangContainer(slkien.Text).ToString();
        }

        private void xtcLuongHang_TrongLuong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTableCell TrongLuongHang = (XRTableCell)sender;
            //if (!string.IsNullOrEmpty(TrongLuongHang.Text))
            //    TrongLuongHang.Text = TinhTrongLuongHangContainer(TrongLuongHang.Text).ToString(); 
        }

        private void xtcLuongHang_Cong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTableCell tongTL = (XRTableCell)sender;
            //if (_TinhTongTrongLuong != 0)
            //    tongTL.Text = _TinhTongTrongLuong.ToString();

        }

        private void xtcHang_DVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dvt = (XRTableCell)sender;

            if (!string.IsNullOrEmpty(dvt.Text))
                dvt.Text = DonViTinh.GetName(dvt.Text.Trim());
            else
                dvt.Text = string.Empty;
        }

        private void xtcThuKhac_TriGiaTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            decimal TriGiaTinhThueXNK = 0;
            decimal TienThueXNK = 0;
            XRTableCell TriGiaTinhThueThuKhac = (XRTableCell)sender;
            // if (inTriGiaTT)
            // {
            double temp2 = 1;
            if (decimal.TryParse(xtcThueXK_TriGiaTinhThue.Text, out TriGiaTinhThueXNK) && decimal.TryParse(string.IsNullOrEmpty(xtcThueXK_Thue.Text) ? "0" : xtcThueXK_Thue.Text, out TienThueXNK))
            {
                TriGiaTinhThueThuKhac.Text = (TriGiaTinhThueXNK + TienThueXNK).ToString("N0");
            }
            //else if (string.IsNullOrEmpty(xtcThuKhac_SoTien.Text) || (double.TryParse(xtcThuKhac_SoTien.Text, out temp2) && temp2 == 0))
            //    TriGiaTinhThueThuKhac.Text = string.Empty;
            if (!inTriGiaTT && (string.IsNullOrEmpty(xtcThuKhac_SoTien.Text) || (double.TryParse(xtcThuKhac_SoTien.Text, out temp2) && temp2 == 0)))
                TriGiaTinhThueThuKhac.Text = string.Empty;

            double temp = 1;
            if (TriGiaTinhThueThuKhac.Text == "0" || (double.TryParse(TriGiaTinhThueThuKhac.Text, out temp) && temp == 0))
            {
                TriGiaTinhThueThuKhac.Text = string.Empty;

            }
            if (inTriGiaTT && string.IsNullOrEmpty(xtcThueXK_TriGiaTinhThue.Text))
                TriGiaTinhThueThuKhac.Text = string.Empty;
        }

        private void xtcHang_LuongHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            double temp = 1;
            XRTableCell cell = (XRTableCell)sender;
            if (cell.Text == "0" || (double.TryParse(cell.Text, out temp) && temp == 0))
            {
                cell.Text = string.Empty;

            }
        }

        private void xtcThueXK_Thue_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            double temp = 1;
            XRTableCell TienThueXK = (XRTableCell)sender;
            if (TienThueXK.Text == "0" || (double.TryParse(TienThueXK.Text, out temp) && temp == 0))
            {
                TienThueXK.Text = xtcThueXK_ThueSuat.Text =  string.Empty;
            }
        }

        private void xtcThueXK_TriGiaTinhThue_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell TriGiaTT = (XRTableCell)sender;
            if (!inTriGiaTT && (string.IsNullOrEmpty(xtcThueXK_Thue.Text) || xtcThueXK_Thue.Text == "0"))
            {
                TriGiaTT.Text = string.Empty;
            }
            double temp = 1;
            if (TriGiaTT.Text == "0" || (double.TryParse(TriGiaTT.Text, out temp) && temp == 0))
            {
                TriGiaTT.Text = string.Empty;

            }
        }

        private void xtcThuKhac_SoTien_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            double temp = 1;
            XRTableCell TienThuKhac = (XRTableCell)sender;
            if (TienThuKhac.Text == "0" || (double.TryParse(TienThuKhac.Text, out temp) && temp == 0))
            {
                TienThuKhac.Text = string.Empty;
                if (!inTriGiaTT)
                {
                    xtcThuKhac_TriGiaTT.Text = string.Empty;
                }
            }
        }


    }
}
