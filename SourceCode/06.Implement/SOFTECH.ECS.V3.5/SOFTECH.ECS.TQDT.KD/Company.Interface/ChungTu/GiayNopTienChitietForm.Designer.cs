﻿namespace Company.Interface
{
    partial class GiayNopTienChitietForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiayNopTienChitietForm));
            this.grpThongTinChung = new Janus.Windows.EditControls.UIGroupBox();
            this.cbSacThue = new Janus.Windows.EditControls.UIComboBox();
            this.txtTieuMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtKhoan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtChuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLoai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDieuChinhGiam = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSoTien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinChung)).BeginInit();
            this.grpThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.grpThongTinChung);
            this.grbMain.Size = new System.Drawing.Size(509, 255);
            // 
            // grpThongTinChung
            // 
            this.grpThongTinChung.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpThongTinChung.BackColor = System.Drawing.Color.Transparent;
            this.grpThongTinChung.Controls.Add(this.cbSacThue);
            this.grpThongTinChung.Controls.Add(this.txtTieuMuc);
            this.grpThongTinChung.Controls.Add(this.txtKhoan);
            this.grpThongTinChung.Controls.Add(this.txtChuong);
            this.grpThongTinChung.Controls.Add(this.txtGhiChu);
            this.grpThongTinChung.Controls.Add(this.txtMuc);
            this.grpThongTinChung.Controls.Add(this.txtLoai);
            this.grpThongTinChung.Controls.Add(this.txtDieuChinhGiam);
            this.grpThongTinChung.Controls.Add(this.label8);
            this.grpThongTinChung.Controls.Add(this.label1);
            this.grpThongTinChung.Controls.Add(this.label5);
            this.grpThongTinChung.Controls.Add(this.label9);
            this.grpThongTinChung.Controls.Add(this.label7);
            this.grpThongTinChung.Controls.Add(this.txtSoTien);
            this.grpThongTinChung.Controls.Add(this.label4);
            this.grpThongTinChung.Controls.Add(this.label3);
            this.grpThongTinChung.Controls.Add(this.label2);
            this.grpThongTinChung.Controls.Add(this.label6);
            this.grpThongTinChung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpThongTinChung.Location = new System.Drawing.Point(3, 12);
            this.grpThongTinChung.Name = "grpThongTinChung";
            this.grpThongTinChung.Size = new System.Drawing.Size(503, 208);
            this.grpThongTinChung.TabIndex = 0;
            this.grpThongTinChung.Text = "Thông tin chung";
            this.grpThongTinChung.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grpThongTinChung.VisualStyleManager = this.vsmMain;
            // 
            // cbSacThue
            // 
            this.cbSacThue.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbSacThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Thuế XNK";
            uiComboBoxItem1.Value = 1;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Thuế VAT";
            uiComboBoxItem2.Value = 2;
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Thuế TTĐB";
            uiComboBoxItem3.Value = 3;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Thuế BVMT";
            uiComboBoxItem4.Value = 6;
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Thuế khác";
            uiComboBoxItem5.Value = 5;
            this.cbSacThue.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5});
            this.cbSacThue.Location = new System.Drawing.Point(61, 60);
            this.cbSacThue.Name = "cbSacThue";
            this.cbSacThue.Size = new System.Drawing.Size(153, 21);
            this.cbSacThue.TabIndex = 2;
            this.cbSacThue.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbSacThue.VisualStyleManager = this.vsmMain;
            // 
            // txtTieuMuc
            // 
            this.txtTieuMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTieuMuc.Location = new System.Drawing.Point(317, 111);
            this.txtTieuMuc.MaxLength = 255;
            this.txtTieuMuc.Name = "txtTieuMuc";
            this.txtTieuMuc.Size = new System.Drawing.Size(153, 21);
            this.txtTieuMuc.TabIndex = 7;
            this.txtTieuMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTieuMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtKhoan
            // 
            this.txtKhoan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKhoan.Location = new System.Drawing.Point(317, 84);
            this.txtKhoan.MaxLength = 255;
            this.txtKhoan.Name = "txtKhoan";
            this.txtKhoan.Size = new System.Drawing.Size(153, 21);
            this.txtKhoan.TabIndex = 5;
            this.txtKhoan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKhoan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtChuong
            // 
            this.txtChuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChuong.Location = new System.Drawing.Point(317, 57);
            this.txtChuong.MaxLength = 255;
            this.txtChuong.Name = "txtChuong";
            this.txtChuong.Size = new System.Drawing.Size(153, 21);
            this.txtChuong.TabIndex = 3;
            this.txtChuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(61, 141);
            this.txtGhiChu.MaxLength = 2000;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(409, 54);
            this.txtGhiChu.TabIndex = 8;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMuc
            // 
            this.txtMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMuc.Location = new System.Drawing.Point(61, 114);
            this.txtMuc.MaxLength = 255;
            this.txtMuc.Name = "txtMuc";
            this.txtMuc.Size = new System.Drawing.Size(153, 21);
            this.txtMuc.TabIndex = 6;
            this.txtMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtLoai
            // 
            this.txtLoai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoai.Location = new System.Drawing.Point(61, 87);
            this.txtLoai.MaxLength = 255;
            this.txtLoai.Name = "txtLoai";
            this.txtLoai.Size = new System.Drawing.Size(153, 21);
            this.txtLoai.TabIndex = 4;
            this.txtLoai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLoai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDieuChinhGiam
            // 
            this.txtDieuChinhGiam.DecimalDigits = 20;
            this.txtDieuChinhGiam.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDieuChinhGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDieuChinhGiam.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDieuChinhGiam.Location = new System.Drawing.Point(317, 29);
            this.txtDieuChinhGiam.MaxLength = 15;
            this.txtDieuChinhGiam.Name = "txtDieuChinhGiam";
            this.txtDieuChinhGiam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDieuChinhGiam.Size = new System.Drawing.Size(153, 21);
            this.txtDieuChinhGiam.TabIndex = 1;
            this.txtDieuChinhGiam.Text = "0";
            this.txtDieuChinhGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDieuChinhGiam.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDieuChinhGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDieuChinhGiam.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(223, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Tiểu mục";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(223, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Điều chỉnh giảm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(223, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Khoản";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 162);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Ghi chú";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 119);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Mục";
            // 
            // txtSoTien
            // 
            this.txtSoTien.DecimalDigits = 20;
            this.txtSoTien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTien.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTien.Location = new System.Drawing.Point(61, 33);
            this.txtSoTien.MaxLength = 15;
            this.txtSoTien.Name = "txtSoTien";
            this.txtSoTien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTien.Size = new System.Drawing.Size(153, 21);
            this.txtSoTien.TabIndex = 0;
            this.txtSoTien.Text = "0";
            this.txtSoTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTien.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Loại";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(223, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Chương";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sắc thuế";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Số tiền";
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(269, 226);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 1;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(431, 226);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(342, 226);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 2;
            this.btnGhi.Text = "&Lưu";
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // GiayNopTienChitietForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 255);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "GiayNopTienChitietForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chi tiết giấy nộp tiền";
            this.Load += new System.EventHandler(this.GiayNopTienChitietForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinChung)).EndInit();
            this.grpThongTinChung.ResumeLayout(false);
            this.grpThongTinChung.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox grpThongTinChung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDieuChinhGiam;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTien;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTieuMuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtKhoan;
        private Janus.Windows.GridEX.EditControls.EditBox txtChuong;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.EditControls.EditBox txtMuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoai;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIComboBox cbSacThue;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
    }
}