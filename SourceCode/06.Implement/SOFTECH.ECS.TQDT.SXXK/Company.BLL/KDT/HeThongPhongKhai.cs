using System;
using System.Data;
using System.Data.SqlClient;
using Company.BLL;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.BLL.KDT;

namespace Company.BLL.KDT
{
    public partial class HeThongPhongKhai
    {
        public DataSet SelectDanhSachDoanhNghiep(long top)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spQuery = "SELECT DISTINCT top " + top + " MaDoanhNghiep, TenDoanhNghiep FROM t_HeThongPhongKhai where role=0";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spQuery);
            return db.ExecuteDataSet(dbCommand);
        }
        public DataSet SelectCauHinhByMaDoanhNghiep(string maDoanhNghiep)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spQuery = "SELECT * FROM t_HeThongPhongKhai WHERE MaDoanhNghiep = '" + maDoanhNghiep + "'";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spQuery);
            return db.ExecuteDataSet(dbCommand);
        }
        public static bool KiemTraQuyenUuTien(string MaDN)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spQuery = "SELECT UuTien FROM t_HeThongPhongKhai WHERE MaDoanhNghiep = @MaDN";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spQuery);
            db.AddInParameter(dbCommand, "@MaDN", SqlDbType.VarChar, MaDN);
            return Boolean.Parse(db.ExecuteDataSet(dbCommand).Tables[0].Rows[0][0].ToString());
        }

        //---------------------------------THOILV EDIT------------------------------------------------------------
        //Selected Login
        public DataSet SelectLogin()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spQuery = "SELECT MaDoanhNghiep,PassWord,Role,UuTien,Value_Config,Key_Config FROM t_HeThongPhongKhai ";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spQuery);
            return db.ExecuteDataSet(dbCommand);
        }

        //Seclected Settings
        public string SelectedSettings(string maDoanhNghiep, string KeyConfig)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string strDS = "";
            try
            {
                string spQuery = "SELECT Value_Config FROM t_HeThongPhongKhai WHERE MaDoanhNghiep='" + maDoanhNghiep + "' AND Key_Config ='" + KeyConfig + "'";
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spQuery);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                strDS = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex)
            {
                string strS = ex.Message;
            }
            return strDS;
        }

        public string SelectedSettingsName(string maDoanhNghiep, string KeyConfig)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string strDS = "";
            try
            {
                string spQuery = "SELECT TenDoanhNghiep FROM t_HeThongPhongKhai WHERE MaDoanhNghiep='" + maDoanhNghiep + "' AND Key_Config ='" + KeyConfig + "'";
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spQuery);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                strDS = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex)
            {
                string strS = ex.Message;
            }
            return strDS;
        }

        //---PASS_ROOT---
        public string SelectedPassWord(string maDoanhNghiep, string KeyConfig)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string strDS = "";
            try
            {
                string spQuery = "SELECT PassWord FROM t_HeThongPhongKhai WHERE MaDoanhNghiep='" + maDoanhNghiep + "' AND Key_Config ='" + KeyConfig + "'";
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spQuery);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                strDS = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex)
            {
                string strS = ex.Message;
            }
            return strDS;
        }
        //---------------
        public IDataReader SelectReaderByRole()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            //string spName = " SELECT * FROM t_HaiQuanPhongKhai_CauHinh WHERE Role =1" ;
            string spName = "SELECT MaDoanhNghiep,[PassWord],TenDoanhNghiep,[Role],UuTien," +
                            " case [Role] when 1 then 'admin' end  as vaitro,Value_Config,Key_Config " +
                            " FROM t_HeThongPhongKhai WHERE [Role] = 1 ";

            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteReader(dbCommand);
        }
        public HeThongPhongKhaiCollection SelectCollectionByRole()
        {
            HeThongPhongKhaiCollection collection = new HeThongPhongKhaiCollection();

            IDataReader reader = this.SelectReaderByRole();
            while (reader.Read())
            {
                HeThongPhongKhai entity = new HeThongPhongKhai();

                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) entity.PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Key_Config"))) entity.Key_Config = reader.GetString(reader.GetOrdinal("Key_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Value_Config"))) entity.Value_Config = reader.GetString(reader.GetOrdinal("Value_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Role"))) entity.Role = reader.GetInt16(reader.GetOrdinal("Role"));
                if (!reader.IsDBNull(reader.GetOrdinal("vaitro"))) entity.vaitro = reader.GetString(reader.GetOrdinal("vaitro"));
                if (!reader.IsDBNull(reader.GetOrdinal("UuTien"))) entity.UuTien = reader.GetBoolean(reader.GetOrdinal("UuTien"));
                collection.Add(entity);
            }
            return collection;
        }
        //-------------------------------------
        public IDataReader SelectReaderByRoleDoanhNghiep()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            //string spName = " SELECT * FROM t_HaiQuanPhongKhai_CauHinh WHERE Role =1" ;
            string spName = "SELECT MaDoanhNghiep,[PassWord],TenDoanhNghiep,[Role],UuTien," +
                            " case [Role] when 0 then 'User' end  as vaitro,Value_Config,Key_Config " +
                            " FROM t_HeThongPhongKhai WHERE [Role] = 0  And Key_Config=\'CauHinh\'";

            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteReader(dbCommand);
        }
        public HeThongPhongKhaiCollection SelectCollectionByRoleDoanhNghiep()
        {
            HeThongPhongKhaiCollection collection = new HeThongPhongKhaiCollection();

            IDataReader reader = this.SelectReaderByRoleDoanhNghiep();
            while (reader.Read())
            {
                HeThongPhongKhai entity = new HeThongPhongKhai();

                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) entity.PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Key_Config"))) entity.Key_Config = reader.GetString(reader.GetOrdinal("Key_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Value_Config"))) entity.Value_Config = reader.GetString(reader.GetOrdinal("Value_Config"));
                if (!reader.IsDBNull(reader.GetOrdinal("Role"))) entity.Role = reader.GetInt16(reader.GetOrdinal("Role"));
                if (!reader.IsDBNull(reader.GetOrdinal("vaitro"))) entity.vaitro = reader.GetString(reader.GetOrdinal("vaitro"));
                if (!reader.IsDBNull(reader.GetOrdinal("UuTien"))) entity.UuTien = reader.GetBoolean(reader.GetOrdinal("UuTien"));
                collection.Add(entity);
            }
            return collection;
        }
        //
        //Dellete Method
        public int DeleteALL()
        {
            return this.DeleteTransactionALL(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransactionALL(SqlTransaction transaction)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            string spName = "p_HeThongPhongKhai_DeleteALL";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public static int SelectedCountDN()
        {
            string strDS = "";
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                string spQuery = "SELECT distinct MaDoanhNghiep FROM t_HeThongPhongKhai where role=0";
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spQuery);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }
        //------------------------------------------------------
    }
}