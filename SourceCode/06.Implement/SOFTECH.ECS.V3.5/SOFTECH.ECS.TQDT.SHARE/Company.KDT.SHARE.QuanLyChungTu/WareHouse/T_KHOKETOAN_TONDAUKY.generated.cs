﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.WareHouse
{
	public partial class T_KHOKETOAN_TONDAUKY : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MAKHO { set; get; }
		public string TENKHO { set; get; }
		public string MAHANGHOA { set; get; }
		public string TENHANGHOA { set; get; }
		public string DVT { set; get; }
		public decimal LOAIHANGHOA { set; get; }
		public decimal LUONGTON { set; get; }
		public decimal DONGIA { set; get; }
		public decimal TRIGIA { set; get; }
		public string NGUYENTE { set; get; }
		public decimal TYGIA { set; get; }
		public int THUESUATXNK { set; get; }
		public string DINHKHOAN { set; get; }
		public string GHICHU { set; get; }
		public long HOPDONG_ID { set; get; }
		public long NAMQT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KHOKETOAN_TONDAUKY> ConvertToCollection(IDataReader reader)
		{
			List<T_KHOKETOAN_TONDAUKY> collection = new List<T_KHOKETOAN_TONDAUKY>();
			while (reader.Read())
			{
				T_KHOKETOAN_TONDAUKY entity = new T_KHOKETOAN_TONDAUKY();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAKHO"))) entity.MAKHO = reader.GetString(reader.GetOrdinal("MAKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENKHO"))) entity.TENKHO = reader.GetString(reader.GetOrdinal("TENKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAHANGHOA"))) entity.MAHANGHOA = reader.GetString(reader.GetOrdinal("MAHANGHOA"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENHANGHOA"))) entity.TENHANGHOA = reader.GetString(reader.GetOrdinal("TENHANGHOA"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LOAIHANGHOA"))) entity.LOAIHANGHOA = reader.GetDecimal(reader.GetOrdinal("LOAIHANGHOA"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGTON"))) entity.LUONGTON = reader.GetDecimal(reader.GetOrdinal("LUONGTON"));
				if (!reader.IsDBNull(reader.GetOrdinal("DONGIA"))) entity.DONGIA = reader.GetDecimal(reader.GetOrdinal("DONGIA"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA"))) entity.TRIGIA = reader.GetDecimal(reader.GetOrdinal("TRIGIA"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGUYENTE"))) entity.NGUYENTE = reader.GetString(reader.GetOrdinal("NGUYENTE"));
				if (!reader.IsDBNull(reader.GetOrdinal("TYGIA"))) entity.TYGIA = reader.GetDecimal(reader.GetOrdinal("TYGIA"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUESUATXNK"))) entity.THUESUATXNK = reader.GetInt32(reader.GetOrdinal("THUESUATXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("DINHKHOAN"))) entity.DINHKHOAN = reader.GetString(reader.GetOrdinal("DINHKHOAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("GHICHU"))) entity.GHICHU = reader.GetString(reader.GetOrdinal("GHICHU"));
				if (!reader.IsDBNull(reader.GetOrdinal("HOPDONG_ID"))) entity.HOPDONG_ID = reader.GetInt64(reader.GetOrdinal("HOPDONG_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NAMQT"))) entity.NAMQT = reader.GetInt64(reader.GetOrdinal("NAMQT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KHOKETOAN_TONDAUKY> collection, long id)
        {
            foreach (T_KHOKETOAN_TONDAUKY item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KHOKETOAN_TONDAUKY VALUES(@MAKHO, @TENKHO, @MAHANGHOA, @TENHANGHOA, @DVT, @LOAIHANGHOA, @LUONGTON, @DONGIA, @TRIGIA, @NGUYENTE, @TYGIA, @THUESUATXNK, @DINHKHOAN, @GHICHU, @HOPDONG_ID, @NAMQT)";
            string update = "UPDATE T_KHOKETOAN_TONDAUKY SET MAKHO = @MAKHO, TENKHO = @TENKHO, MAHANGHOA = @MAHANGHOA, TENHANGHOA = @TENHANGHOA, DVT = @DVT, LOAIHANGHOA = @LOAIHANGHOA, LUONGTON = @LUONGTON, DONGIA = @DONGIA, TRIGIA = @TRIGIA, NGUYENTE = @NGUYENTE, TYGIA = @TYGIA, THUESUATXNK = @THUESUATXNK, DINHKHOAN = @DINHKHOAN, GHICHU = @GHICHU, HOPDONG_ID = @HOPDONG_ID, NAMQT = @NAMQT WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_TONDAUKY WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHANGHOA", SqlDbType.NVarChar, "MAHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENHANGHOA", SqlDbType.NVarChar, "TENHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAIHANGHOA", SqlDbType.Decimal, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTON", SqlDbType.Decimal, "LUONGTON", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIA", SqlDbType.Decimal, "TRIGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGUYENTE", SqlDbType.VarChar, "NGUYENTE", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYGIA", SqlDbType.Decimal, "TYGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THUESUATXNK", SqlDbType.Int, "THUESUATXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DINHKHOAN", SqlDbType.NVarChar, "DINHKHOAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NAMQT", SqlDbType.BigInt, "NAMQT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHANGHOA", SqlDbType.NVarChar, "MAHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENHANGHOA", SqlDbType.NVarChar, "TENHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAIHANGHOA", SqlDbType.Decimal, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTON", SqlDbType.Decimal, "LUONGTON", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIA", SqlDbType.Decimal, "TRIGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGUYENTE", SqlDbType.VarChar, "NGUYENTE", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYGIA", SqlDbType.Decimal, "TYGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THUESUATXNK", SqlDbType.Int, "THUESUATXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DINHKHOAN", SqlDbType.NVarChar, "DINHKHOAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NAMQT", SqlDbType.BigInt, "NAMQT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KHOKETOAN_TONDAUKY VALUES(@MAKHO, @TENKHO, @MAHANGHOA, @TENHANGHOA, @DVT, @LOAIHANGHOA, @LUONGTON, @DONGIA, @TRIGIA, @NGUYENTE, @TYGIA, @THUESUATXNK, @DINHKHOAN, @GHICHU, @HOPDONG_ID, @NAMQT)";
            string update = "UPDATE T_KHOKETOAN_TONDAUKY SET MAKHO = @MAKHO, TENKHO = @TENKHO, MAHANGHOA = @MAHANGHOA, TENHANGHOA = @TENHANGHOA, DVT = @DVT, LOAIHANGHOA = @LOAIHANGHOA, LUONGTON = @LUONGTON, DONGIA = @DONGIA, TRIGIA = @TRIGIA, NGUYENTE = @NGUYENTE, TYGIA = @TYGIA, THUESUATXNK = @THUESUATXNK, DINHKHOAN = @DINHKHOAN, GHICHU = @GHICHU, HOPDONG_ID = @HOPDONG_ID, NAMQT = @NAMQT WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_TONDAUKY WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHANGHOA", SqlDbType.NVarChar, "MAHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENHANGHOA", SqlDbType.NVarChar, "TENHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAIHANGHOA", SqlDbType.Decimal, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTON", SqlDbType.Decimal, "LUONGTON", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIA", SqlDbType.Decimal, "TRIGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGUYENTE", SqlDbType.VarChar, "NGUYENTE", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYGIA", SqlDbType.Decimal, "TYGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THUESUATXNK", SqlDbType.Int, "THUESUATXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DINHKHOAN", SqlDbType.NVarChar, "DINHKHOAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NAMQT", SqlDbType.BigInt, "NAMQT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHANGHOA", SqlDbType.NVarChar, "MAHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENHANGHOA", SqlDbType.NVarChar, "TENHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAIHANGHOA", SqlDbType.Decimal, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTON", SqlDbType.Decimal, "LUONGTON", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIA", SqlDbType.Decimal, "TRIGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGUYENTE", SqlDbType.VarChar, "NGUYENTE", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYGIA", SqlDbType.Decimal, "TYGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THUESUATXNK", SqlDbType.Int, "THUESUATXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DINHKHOAN", SqlDbType.NVarChar, "DINHKHOAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NAMQT", SqlDbType.BigInt, "NAMQT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KHOKETOAN_TONDAUKY Load(long id)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_TONDAUKY_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KHOKETOAN_TONDAUKY> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KHOKETOAN_TONDAUKY> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KHOKETOAN_TONDAUKY> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_TONDAUKY_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_TONDAUKY_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_TONDAUKY_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_TONDAUKY_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KHOKETOAN_TONDAUKY(string mAKHO, string tENKHO, string mAHANGHOA, string tENHANGHOA, string dVT, decimal lOAIHANGHOA, decimal lUONGTON, decimal dONGIA, decimal tRIGIA, string nGUYENTE, decimal tYGIA, int tHUESUATXNK, string dINHKHOAN, string gHICHU, long hOPDONG_ID, long nAMQT)
		{
			T_KHOKETOAN_TONDAUKY entity = new T_KHOKETOAN_TONDAUKY();	
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			entity.MAHANGHOA = mAHANGHOA;
			entity.TENHANGHOA = tENHANGHOA;
			entity.DVT = dVT;
			entity.LOAIHANGHOA = lOAIHANGHOA;
			entity.LUONGTON = lUONGTON;
			entity.DONGIA = dONGIA;
			entity.TRIGIA = tRIGIA;
			entity.NGUYENTE = nGUYENTE;
			entity.TYGIA = tYGIA;
			entity.THUESUATXNK = tHUESUATXNK;
			entity.DINHKHOAN = dINHKHOAN;
			entity.GHICHU = gHICHU;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQT = nAMQT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KHOKETOAN_TONDAUKY_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			db.AddInParameter(dbCommand, "@MAHANGHOA", SqlDbType.NVarChar, MAHANGHOA);
			db.AddInParameter(dbCommand, "@TENHANGHOA", SqlDbType.NVarChar, TENHANGHOA);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LOAIHANGHOA", SqlDbType.Decimal, LOAIHANGHOA);
			db.AddInParameter(dbCommand, "@LUONGTON", SqlDbType.Decimal, LUONGTON);
			db.AddInParameter(dbCommand, "@DONGIA", SqlDbType.Decimal, DONGIA);
			db.AddInParameter(dbCommand, "@TRIGIA", SqlDbType.Decimal, TRIGIA);
			db.AddInParameter(dbCommand, "@NGUYENTE", SqlDbType.VarChar, NGUYENTE);
			db.AddInParameter(dbCommand, "@TYGIA", SqlDbType.Decimal, TYGIA);
			db.AddInParameter(dbCommand, "@THUESUATXNK", SqlDbType.Int, THUESUATXNK);
			db.AddInParameter(dbCommand, "@DINHKHOAN", SqlDbType.NVarChar, DINHKHOAN);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQT", SqlDbType.BigInt, NAMQT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KHOKETOAN_TONDAUKY> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_TONDAUKY item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KHOKETOAN_TONDAUKY(long id, string mAKHO, string tENKHO, string mAHANGHOA, string tENHANGHOA, string dVT, decimal lOAIHANGHOA, decimal lUONGTON, decimal dONGIA, decimal tRIGIA, string nGUYENTE, decimal tYGIA, int tHUESUATXNK, string dINHKHOAN, string gHICHU, long hOPDONG_ID, long nAMQT)
		{
			T_KHOKETOAN_TONDAUKY entity = new T_KHOKETOAN_TONDAUKY();			
			entity.ID = id;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			entity.MAHANGHOA = mAHANGHOA;
			entity.TENHANGHOA = tENHANGHOA;
			entity.DVT = dVT;
			entity.LOAIHANGHOA = lOAIHANGHOA;
			entity.LUONGTON = lUONGTON;
			entity.DONGIA = dONGIA;
			entity.TRIGIA = tRIGIA;
			entity.NGUYENTE = nGUYENTE;
			entity.TYGIA = tYGIA;
			entity.THUESUATXNK = tHUESUATXNK;
			entity.DINHKHOAN = dINHKHOAN;
			entity.GHICHU = gHICHU;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQT = nAMQT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KHOKETOAN_TONDAUKY_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			db.AddInParameter(dbCommand, "@MAHANGHOA", SqlDbType.NVarChar, MAHANGHOA);
			db.AddInParameter(dbCommand, "@TENHANGHOA", SqlDbType.NVarChar, TENHANGHOA);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LOAIHANGHOA", SqlDbType.Decimal, LOAIHANGHOA);
			db.AddInParameter(dbCommand, "@LUONGTON", SqlDbType.Decimal, LUONGTON);
			db.AddInParameter(dbCommand, "@DONGIA", SqlDbType.Decimal, DONGIA);
			db.AddInParameter(dbCommand, "@TRIGIA", SqlDbType.Decimal, TRIGIA);
			db.AddInParameter(dbCommand, "@NGUYENTE", SqlDbType.VarChar, NGUYENTE);
			db.AddInParameter(dbCommand, "@TYGIA", SqlDbType.Decimal, TYGIA);
			db.AddInParameter(dbCommand, "@THUESUATXNK", SqlDbType.Int, THUESUATXNK);
			db.AddInParameter(dbCommand, "@DINHKHOAN", SqlDbType.NVarChar, DINHKHOAN);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQT", SqlDbType.BigInt, NAMQT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KHOKETOAN_TONDAUKY> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_TONDAUKY item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KHOKETOAN_TONDAUKY(long id, string mAKHO, string tENKHO, string mAHANGHOA, string tENHANGHOA, string dVT, decimal lOAIHANGHOA, decimal lUONGTON, decimal dONGIA, decimal tRIGIA, string nGUYENTE, decimal tYGIA, int tHUESUATXNK, string dINHKHOAN, string gHICHU, long hOPDONG_ID, long nAMQT)
		{
			T_KHOKETOAN_TONDAUKY entity = new T_KHOKETOAN_TONDAUKY();			
			entity.ID = id;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			entity.MAHANGHOA = mAHANGHOA;
			entity.TENHANGHOA = tENHANGHOA;
			entity.DVT = dVT;
			entity.LOAIHANGHOA = lOAIHANGHOA;
			entity.LUONGTON = lUONGTON;
			entity.DONGIA = dONGIA;
			entity.TRIGIA = tRIGIA;
			entity.NGUYENTE = nGUYENTE;
			entity.TYGIA = tYGIA;
			entity.THUESUATXNK = tHUESUATXNK;
			entity.DINHKHOAN = dINHKHOAN;
			entity.GHICHU = gHICHU;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQT = nAMQT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_TONDAUKY_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			db.AddInParameter(dbCommand, "@MAHANGHOA", SqlDbType.NVarChar, MAHANGHOA);
			db.AddInParameter(dbCommand, "@TENHANGHOA", SqlDbType.NVarChar, TENHANGHOA);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LOAIHANGHOA", SqlDbType.Decimal, LOAIHANGHOA);
			db.AddInParameter(dbCommand, "@LUONGTON", SqlDbType.Decimal, LUONGTON);
			db.AddInParameter(dbCommand, "@DONGIA", SqlDbType.Decimal, DONGIA);
			db.AddInParameter(dbCommand, "@TRIGIA", SqlDbType.Decimal, TRIGIA);
			db.AddInParameter(dbCommand, "@NGUYENTE", SqlDbType.VarChar, NGUYENTE);
			db.AddInParameter(dbCommand, "@TYGIA", SqlDbType.Decimal, TYGIA);
			db.AddInParameter(dbCommand, "@THUESUATXNK", SqlDbType.Int, THUESUATXNK);
			db.AddInParameter(dbCommand, "@DINHKHOAN", SqlDbType.NVarChar, DINHKHOAN);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQT", SqlDbType.BigInt, NAMQT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KHOKETOAN_TONDAUKY> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_TONDAUKY item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KHOKETOAN_TONDAUKY(long id)
		{
			T_KHOKETOAN_TONDAUKY entity = new T_KHOKETOAN_TONDAUKY();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_TONDAUKY_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_TONDAUKY_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KHOKETOAN_TONDAUKY> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_TONDAUKY item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}