﻿namespace Company.Interface.GC
{
    partial class PhieuXuatKho_ThongKeTon_ChiTiet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout cbHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgList_Details_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhieuXuatKho_ThongKeTon_ChiTiet));
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbHopDong = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.btnExport2 = new Janus.Windows.EditControls.UIButton();
            this.btnExport = new Janus.Windows.EditControls.UIButton();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblTrangThaiXuLy = new System.Windows.Forms.Label();
            this.btnXuatBC_XNT = new Janus.Windows.EditControls.UIButton();
            this.lblPercen = new System.Windows.Forms.Label();
            this.btnXuLyToKhai = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList_Details = new Janus.Windows.GridEX.GridEX();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList_Details)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(903, 473);
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 6;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.Location = new System.Drawing.Point(3, 17);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.ScrollBarWidth = 17;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(724, 162);
            this.dgList.TabIndex = 1;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.cbHopDong);
            this.uiGroupBox4.Controls.Add(this.btnExport2);
            this.uiGroupBox4.Controls.Add(this.btnExport);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(903, 58);
            this.uiGroupBox4.TabIndex = 11;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // cbHopDong
            // 
            cbHopDong_DesignTimeLayout.LayoutString = resources.GetString("cbHopDong_DesignTimeLayout.LayoutString");
            this.cbHopDong.DesignTimeLayout = cbHopDong_DesignTimeLayout;
            this.cbHopDong.Location = new System.Drawing.Point(78, 23);
            this.cbHopDong.Name = "cbHopDong";
            this.cbHopDong.SelectedIndex = -1;
            this.cbHopDong.SelectedItem = null;
            this.cbHopDong.Size = new System.Drawing.Size(331, 21);
            this.cbHopDong.TabIndex = 9;
            this.cbHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.cbHopDong.VisualStyleManager = this.vsmMain;
            // 
            // btnExport2
            // 
            this.btnExport2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport2.Icon = ((System.Drawing.Icon)(resources.GetObject("btnExport2.Icon")));
            this.btnExport2.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnExport2.Location = new System.Drawing.Point(554, 23);
            this.btnExport2.Name = "btnExport2";
            this.btnExport2.Size = new System.Drawing.Size(133, 23);
            this.btnExport2.TabIndex = 10;
            this.btnExport2.Text = "Xuất Excel bảng 2";
            this.btnExport2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnExport2.Click += new System.EventHandler(this.btnExport2_Click);
            // 
            // btnExport
            // 
            this.btnExport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.Icon = ((System.Drawing.Icon)(resources.GetObject("btnExport.Icon")));
            this.btnExport.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnExport.Location = new System.Drawing.Point(415, 23);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(133, 23);
            this.btnExport.TabIndex = 10;
            this.btnExport.Text = "Xuất Excel bảng 1";
            this.btnExport.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnExport.VisualStyleManager = this.vsmMain;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Hợp đồng";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.progressBar1);
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.lblTrangThaiXuLy);
            this.uiGroupBox1.Controls.Add(this.btnXuatBC_XNT);
            this.uiGroupBox1.Controls.Add(this.lblPercen);
            this.uiGroupBox1.Controls.Add(this.btnXuLyToKhai);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 58);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(903, 182);
            this.uiGroupBox1.TabIndex = 12;
            this.uiGroupBox1.Text = "1.Tổng lượng Nhập - Xuất - Tồn";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(733, 140);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(163, 23);
            this.progressBar1.TabIndex = 11;
            // 
            // lblTrangThaiXuLy
            // 
            this.lblTrangThaiXuLy.AutoSize = true;
            this.lblTrangThaiXuLy.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThaiXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThaiXuLy.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThaiXuLy.Location = new System.Drawing.Point(771, 124);
            this.lblTrangThaiXuLy.Name = "lblTrangThaiXuLy";
            this.lblTrangThaiXuLy.Size = new System.Drawing.Size(67, 13);
            this.lblTrangThaiXuLy.TabIndex = 8;
            this.lblTrangThaiXuLy.Text = "Chưa xử lý";
            // 
            // btnXuatBC_XNT
            // 
            this.btnXuatBC_XNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatBC_XNT.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnXuatBC_XNT.Location = new System.Drawing.Point(733, 58);
            this.btnXuatBC_XNT.Name = "btnXuatBC_XNT";
            this.btnXuatBC_XNT.Size = new System.Drawing.Size(158, 35);
            this.btnXuatBC_XNT.TabIndex = 10;
            this.btnXuatBC_XNT.Text = "Xuất báo cáo XNT";
            this.btnXuatBC_XNT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXuatBC_XNT.Click += new System.EventHandler(this.btnXuatBC_XNT_Click);
            // 
            // lblPercen
            // 
            this.lblPercen.AutoSize = true;
            this.lblPercen.BackColor = System.Drawing.Color.Transparent;
            this.lblPercen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPercen.Location = new System.Drawing.Point(794, 166);
            this.lblPercen.Name = "lblPercen";
            this.lblPercen.Size = new System.Drawing.Size(27, 13);
            this.lblPercen.TabIndex = 8;
            this.lblPercen.Text = "0%";
            // 
            // btnXuLyToKhai
            // 
            this.btnXuLyToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuLyToKhai.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnXuLyToKhai.Location = new System.Drawing.Point(733, 17);
            this.btnXuLyToKhai.Name = "btnXuLyToKhai";
            this.btnXuLyToKhai.Size = new System.Drawing.Size(158, 35);
            this.btnXuLyToKhai.TabIndex = 10;
            this.btnXuLyToKhai.Text = "Xử lý chi tiết tờ khai";
            this.btnXuLyToKhai.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXuLyToKhai.Click += new System.EventHandler(this.btnXuLyToKhai_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgList_Details);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 240);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(903, 233);
            this.uiGroupBox2.TabIndex = 14;
            this.uiGroupBox2.Text = "2.Tổng lượng Nhập - Xuất - Tồn (Chi tiết các tờ khai)";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // dgList_Details
            // 
            this.dgList_Details.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList_Details.AlternatingColors = true;
            this.dgList_Details.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList_Details.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_Details_DesignTimeLayout.LayoutString = resources.GetString("dgList_Details_DesignTimeLayout.LayoutString");
            this.dgList_Details.DesignTimeLayout = dgList_Details_DesignTimeLayout;
            this.dgList_Details.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList_Details.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList_Details.FrozenColumns = 3;
            this.dgList_Details.GroupByBoxVisible = false;
            this.dgList_Details.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList_Details.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList_Details.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList_Details.Hierarchical = true;
            this.dgList_Details.Location = new System.Drawing.Point(3, 17);
            this.dgList_Details.Name = "dgList_Details";
            this.dgList_Details.RecordNavigator = true;
            this.dgList_Details.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList_Details.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList_Details.ScrollBarWidth = 17;
            this.dgList_Details.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList_Details.Size = new System.Drawing.Size(897, 213);
            this.dgList_Details.TabIndex = 1;
            this.dgList_Details.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList_Details.VisualStyleManager = this.vsmMain;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // PhieuXuatKho_ThongKeTon_ChiTiet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 473);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "PhieuXuatKho_ThongKeTon_ChiTiet";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thống kê hàng tồn phiếu xuất kho chi tiết";
            this.Load += new System.EventHandler(this.PhieuXuatKho_ThongKeTon_ChiTiet_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList_Details)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        public Janus.Windows.GridEX.EditControls.MultiColumnCombo cbHopDong;
        private Janus.Windows.EditControls.UIButton btnExport;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgList_Details;
        private Janus.Windows.EditControls.UIButton btnXuLyToKhai;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lblTrangThaiXuLy;
        private System.Windows.Forms.Label lblPercen;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private Janus.Windows.EditControls.UIButton btnXuatBC_XNT;
        private Janus.Windows.EditControls.UIButton btnExport2;
    }
}