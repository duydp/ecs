﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class Receipt
    {
        // Số lệnh
        [XmlElement("reference")]
        public string Reference { get; set; }
        // Ngày phát lệnh
        [XmlElement("issue")]
        public string Issue { get; set; }
        // Người nộp
        [XmlElement("Payer")]
        public Payer Payer { get; set; }
        // Người phát lệnh
        [XmlElement("TaxPayer")]
        public Payer TaxPayer { get; set; }
        // Người nhận lệnh
        [XmlElement("Payee")]
        public Payer Payee { get; set; }
        // Nội dung các khoản nộp
        [XmlElement("DutyTaxFee")]
        public List<DutyTaxFee> DutyTaxFee { get; set; }
        // Nội dung khác
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        // Các loại tài liệu / Chứng từ
        [XmlElement("AdditionalDocument")]
        public List<AdditionalDocument> AdditionalDocument { get; set; }
    }
}
