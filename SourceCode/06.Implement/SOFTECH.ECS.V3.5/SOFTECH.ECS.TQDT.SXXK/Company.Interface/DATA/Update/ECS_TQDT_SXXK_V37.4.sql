GO
 IF OBJECT_ID(N'[dbo].[t_KDT_SXXK_BCXNT]') IS NOT NULL
	DROP TABLE [dbo].[t_KDT_SXXK_BCXNT]
GO

 CREATE TABLE [dbo].[t_KDT_SXXK_BCXNT]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[STT] [bigint] NOT NULL,
[TuNgay] DATETIME NOT NULL,
[DenNgay] DATETIME NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNPL] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenNPL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDVT_NPL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoToKhaiNhap] BIGINT NOT NULL,
[NgayDangKyNhap] [datetime] NOT NULL,
[NgayHoanThanhNhap] [datetime] NOT NULL,
[MaLoaiHinhNhap] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LuongNhap] [numeric] (18, 8) NOT NULL,
[MaSP] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenSP] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoToKhaiXuat] BIGINT NULL,
[NgayDangKyXuat] [datetime] NULL,
[NgayHoanThanhXuat] [datetime] NULL,
[MaLoaiHinhXuat] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LuongSPXuat] [numeric] (18, 8) NULL,
[TenDVT_SP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DinhMuc] [numeric] (18, 8) NULL,
[LuongNPLSuDung] [numeric] (18, 8) NULL,
[SoToKhaiTaiXuat] BIGINT NULL,
[NgayTaiXuat] [datetime] NULL,
[LuongNPLTaiXuat] [numeric] (18, 8) NULL,
[LuongTonCuoi] [numeric] (18, 8) NULL,
[ThanhKhoanTiep] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChuyenMucDichKhac] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DonGiaTT] [float] NULL,
[TyGiaTT] [money] NULL,
[ThueSuat] [numeric] (5, 2) NULL,
[ThueXNK] [float] NULL,
[ThueXNKTon] [float] NULL,
[NgayThucXuat] [datetime] NULL,
[SoThuTuHang] [int] NULL
) ON [PRIMARY]
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCXNT_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCXNT_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCXNT_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCXNT_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCXNT_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCXNT_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCXNT_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCXNT_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXNT_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_Insert]
	@STT bigint,
	@TuNgay datetime,
	@DenNgay datetime,
	@MaDoanhNghiep varchar(14),
	@MaNPL varchar(30),
	@TenNPL nvarchar(255),
	@TenDVT_NPL varchar(50),
	@SoToKhaiNhap bigint,
	@NgayDangKyNhap datetime,
	@NgayHoanThanhNhap datetime,
	@MaLoaiHinhNhap char(5),
	@LuongNhap numeric(18, 8),
	@MaSP varchar(30),
	@TenSP nvarchar(255),
	@SoToKhaiXuat bigint,
	@NgayDangKyXuat datetime,
	@NgayHoanThanhXuat datetime,
	@MaLoaiHinhXuat char(5),
	@LuongSPXuat numeric(18, 8),
	@TenDVT_SP varchar(50),
	@DinhMuc numeric(18, 8),
	@LuongNPLSuDung numeric(18, 8),
	@SoToKhaiTaiXuat bigint,
	@NgayTaiXuat datetime,
	@LuongNPLTaiXuat numeric(18, 8),
	@LuongTonCuoi numeric(18, 8),
	@ThanhKhoanTiep nvarchar(255),
	@ChuyenMucDichKhac nvarchar(255),
	@DonGiaTT float,
	@TyGiaTT money,
	@ThueSuat numeric(5, 2),
	@ThueXNK float,
	@ThueXNKTon float,
	@NgayThucXuat datetime,
	@SoThuTuHang int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_BCXNT]
(
	[STT],
	[TuNgay],
	[DenNgay],
	[MaDoanhNghiep],
	[MaNPL],
	[TenNPL],
	[TenDVT_NPL],
	[SoToKhaiNhap],
	[NgayDangKyNhap],
	[NgayHoanThanhNhap],
	[MaLoaiHinhNhap],
	[LuongNhap],
	[MaSP],
	[TenSP],
	[SoToKhaiXuat],
	[NgayDangKyXuat],
	[NgayHoanThanhXuat],
	[MaLoaiHinhXuat],
	[LuongSPXuat],
	[TenDVT_SP],
	[DinhMuc],
	[LuongNPLSuDung],
	[SoToKhaiTaiXuat],
	[NgayTaiXuat],
	[LuongNPLTaiXuat],
	[LuongTonCuoi],
	[ThanhKhoanTiep],
	[ChuyenMucDichKhac],
	[DonGiaTT],
	[TyGiaTT],
	[ThueSuat],
	[ThueXNK],
	[ThueXNKTon],
	[NgayThucXuat],
	[SoThuTuHang]
)
VALUES 
(
	@STT,
	@TuNgay,
	@DenNgay,
	@MaDoanhNghiep,
	@MaNPL,
	@TenNPL,
	@TenDVT_NPL,
	@SoToKhaiNhap,
	@NgayDangKyNhap,
	@NgayHoanThanhNhap,
	@MaLoaiHinhNhap,
	@LuongNhap,
	@MaSP,
	@TenSP,
	@SoToKhaiXuat,
	@NgayDangKyXuat,
	@NgayHoanThanhXuat,
	@MaLoaiHinhXuat,
	@LuongSPXuat,
	@TenDVT_SP,
	@DinhMuc,
	@LuongNPLSuDung,
	@SoToKhaiTaiXuat,
	@NgayTaiXuat,
	@LuongNPLTaiXuat,
	@LuongTonCuoi,
	@ThanhKhoanTiep,
	@ChuyenMucDichKhac,
	@DonGiaTT,
	@TyGiaTT,
	@ThueSuat,
	@ThueXNK,
	@ThueXNKTon,
	@NgayThucXuat,
	@SoThuTuHang
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXNT_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_Update]
	@ID bigint,
	@STT bigint,
	@TuNgay datetime,
	@DenNgay datetime,
	@MaDoanhNghiep varchar(14),
	@MaNPL varchar(30),
	@TenNPL nvarchar(255),
	@TenDVT_NPL varchar(50),
	@SoToKhaiNhap bigint,
	@NgayDangKyNhap datetime,
	@NgayHoanThanhNhap datetime,
	@MaLoaiHinhNhap char(5),
	@LuongNhap numeric(18, 8),
	@MaSP varchar(30),
	@TenSP nvarchar(255),
	@SoToKhaiXuat bigint,
	@NgayDangKyXuat datetime,
	@NgayHoanThanhXuat datetime,
	@MaLoaiHinhXuat char(5),
	@LuongSPXuat numeric(18, 8),
	@TenDVT_SP varchar(50),
	@DinhMuc numeric(18, 8),
	@LuongNPLSuDung numeric(18, 8),
	@SoToKhaiTaiXuat bigint,
	@NgayTaiXuat datetime,
	@LuongNPLTaiXuat numeric(18, 8),
	@LuongTonCuoi numeric(18, 8),
	@ThanhKhoanTiep nvarchar(255),
	@ChuyenMucDichKhac nvarchar(255),
	@DonGiaTT float,
	@TyGiaTT money,
	@ThueSuat numeric(5, 2),
	@ThueXNK float,
	@ThueXNKTon float,
	@NgayThucXuat datetime,
	@SoThuTuHang int
AS

UPDATE
	[dbo].[t_KDT_SXXK_BCXNT]
SET
	[STT] = @STT,
	[TuNgay] = @TuNgay,
	[DenNgay] = @DenNgay,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[TenDVT_NPL] = @TenDVT_NPL,
	[SoToKhaiNhap] = @SoToKhaiNhap,
	[NgayDangKyNhap] = @NgayDangKyNhap,
	[NgayHoanThanhNhap] = @NgayHoanThanhNhap,
	[MaLoaiHinhNhap] = @MaLoaiHinhNhap,
	[LuongNhap] = @LuongNhap,
	[MaSP] = @MaSP,
	[TenSP] = @TenSP,
	[SoToKhaiXuat] = @SoToKhaiXuat,
	[NgayDangKyXuat] = @NgayDangKyXuat,
	[NgayHoanThanhXuat] = @NgayHoanThanhXuat,
	[MaLoaiHinhXuat] = @MaLoaiHinhXuat,
	[LuongSPXuat] = @LuongSPXuat,
	[TenDVT_SP] = @TenDVT_SP,
	[DinhMuc] = @DinhMuc,
	[LuongNPLSuDung] = @LuongNPLSuDung,
	[SoToKhaiTaiXuat] = @SoToKhaiTaiXuat,
	[NgayTaiXuat] = @NgayTaiXuat,
	[LuongNPLTaiXuat] = @LuongNPLTaiXuat,
	[LuongTonCuoi] = @LuongTonCuoi,
	[ThanhKhoanTiep] = @ThanhKhoanTiep,
	[ChuyenMucDichKhac] = @ChuyenMucDichKhac,
	[DonGiaTT] = @DonGiaTT,
	[TyGiaTT] = @TyGiaTT,
	[ThueSuat] = @ThueSuat,
	[ThueXNK] = @ThueXNK,
	[ThueXNKTon] = @ThueXNKTon,
	[NgayThucXuat] = @NgayThucXuat,
	[SoThuTuHang] = @SoThuTuHang
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXNT_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_InsertUpdate]
	@ID bigint,
	@STT bigint,
	@TuNgay datetime,
	@DenNgay datetime,
	@MaDoanhNghiep varchar(14),
	@MaNPL varchar(30),
	@TenNPL nvarchar(255),
	@TenDVT_NPL varchar(50),
	@SoToKhaiNhap bigint,
	@NgayDangKyNhap datetime,
	@NgayHoanThanhNhap datetime,
	@MaLoaiHinhNhap char(5),
	@LuongNhap numeric(18, 8),
	@MaSP varchar(30),
	@TenSP nvarchar(255),
	@SoToKhaiXuat bigint,
	@NgayDangKyXuat datetime,
	@NgayHoanThanhXuat datetime,
	@MaLoaiHinhXuat char(5),
	@LuongSPXuat numeric(18, 8),
	@TenDVT_SP varchar(50),
	@DinhMuc numeric(18, 8),
	@LuongNPLSuDung numeric(18, 8),
	@SoToKhaiTaiXuat bigint,
	@NgayTaiXuat datetime,
	@LuongNPLTaiXuat numeric(18, 8),
	@LuongTonCuoi numeric(18, 8),
	@ThanhKhoanTiep nvarchar(255),
	@ChuyenMucDichKhac nvarchar(255),
	@DonGiaTT float,
	@TyGiaTT money,
	@ThueSuat numeric(5, 2),
	@ThueXNK float,
	@ThueXNKTon float,
	@NgayThucXuat datetime,
	@SoThuTuHang int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_BCXNT] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_BCXNT] 
		SET
			[STT] = @STT,
			[TuNgay] = @TuNgay,
			[DenNgay] = @DenNgay,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[TenDVT_NPL] = @TenDVT_NPL,
			[SoToKhaiNhap] = @SoToKhaiNhap,
			[NgayDangKyNhap] = @NgayDangKyNhap,
			[NgayHoanThanhNhap] = @NgayHoanThanhNhap,
			[MaLoaiHinhNhap] = @MaLoaiHinhNhap,
			[LuongNhap] = @LuongNhap,
			[MaSP] = @MaSP,
			[TenSP] = @TenSP,
			[SoToKhaiXuat] = @SoToKhaiXuat,
			[NgayDangKyXuat] = @NgayDangKyXuat,
			[NgayHoanThanhXuat] = @NgayHoanThanhXuat,
			[MaLoaiHinhXuat] = @MaLoaiHinhXuat,
			[LuongSPXuat] = @LuongSPXuat,
			[TenDVT_SP] = @TenDVT_SP,
			[DinhMuc] = @DinhMuc,
			[LuongNPLSuDung] = @LuongNPLSuDung,
			[SoToKhaiTaiXuat] = @SoToKhaiTaiXuat,
			[NgayTaiXuat] = @NgayTaiXuat,
			[LuongNPLTaiXuat] = @LuongNPLTaiXuat,
			[LuongTonCuoi] = @LuongTonCuoi,
			[ThanhKhoanTiep] = @ThanhKhoanTiep,
			[ChuyenMucDichKhac] = @ChuyenMucDichKhac,
			[DonGiaTT] = @DonGiaTT,
			[TyGiaTT] = @TyGiaTT,
			[ThueSuat] = @ThueSuat,
			[ThueXNK] = @ThueXNK,
			[ThueXNKTon] = @ThueXNKTon,
			[NgayThucXuat] = @NgayThucXuat,
			[SoThuTuHang] = @SoThuTuHang
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_BCXNT]
		(
			[STT],
			[TuNgay],
			[DenNgay],
			[MaDoanhNghiep],
			[MaNPL],
			[TenNPL],
			[TenDVT_NPL],
			[SoToKhaiNhap],
			[NgayDangKyNhap],
			[NgayHoanThanhNhap],
			[MaLoaiHinhNhap],
			[LuongNhap],
			[MaSP],
			[TenSP],
			[SoToKhaiXuat],
			[NgayDangKyXuat],
			[NgayHoanThanhXuat],
			[MaLoaiHinhXuat],
			[LuongSPXuat],
			[TenDVT_SP],
			[DinhMuc],
			[LuongNPLSuDung],
			[SoToKhaiTaiXuat],
			[NgayTaiXuat],
			[LuongNPLTaiXuat],
			[LuongTonCuoi],
			[ThanhKhoanTiep],
			[ChuyenMucDichKhac],
			[DonGiaTT],
			[TyGiaTT],
			[ThueSuat],
			[ThueXNK],
			[ThueXNKTon],
			[NgayThucXuat],
			[SoThuTuHang]
		)
		VALUES 
		(
			@STT,
			@TuNgay,
			@DenNgay,
			@MaDoanhNghiep,
			@MaNPL,
			@TenNPL,
			@TenDVT_NPL,
			@SoToKhaiNhap,
			@NgayDangKyNhap,
			@NgayHoanThanhNhap,
			@MaLoaiHinhNhap,
			@LuongNhap,
			@MaSP,
			@TenSP,
			@SoToKhaiXuat,
			@NgayDangKyXuat,
			@NgayHoanThanhXuat,
			@MaLoaiHinhXuat,
			@LuongSPXuat,
			@TenDVT_SP,
			@DinhMuc,
			@LuongNPLSuDung,
			@SoToKhaiTaiXuat,
			@NgayTaiXuat,
			@LuongNPLTaiXuat,
			@LuongTonCuoi,
			@ThanhKhoanTiep,
			@ChuyenMucDichKhac,
			@DonGiaTT,
			@TyGiaTT,
			@ThueSuat,
			@ThueXNK,
			@ThueXNKTon,
			@NgayThucXuat,
			@SoThuTuHang
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXNT_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_BCXNT]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXNT_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_BCXNT] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXNT_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[STT],
	[TuNgay],
	[DenNgay],
	[MaDoanhNghiep],
	[MaNPL],
	[TenNPL],
	[TenDVT_NPL],
	[SoToKhaiNhap],
	[NgayDangKyNhap],
	[NgayHoanThanhNhap],
	[MaLoaiHinhNhap],
	[LuongNhap],
	[MaSP],
	[TenSP],
	[SoToKhaiXuat],
	[NgayDangKyXuat],
	[NgayHoanThanhXuat],
	[MaLoaiHinhXuat],
	[LuongSPXuat],
	[TenDVT_SP],
	[DinhMuc],
	[LuongNPLSuDung],
	[SoToKhaiTaiXuat],
	[NgayTaiXuat],
	[LuongNPLTaiXuat],
	[LuongTonCuoi],
	[ThanhKhoanTiep],
	[ChuyenMucDichKhac],
	[DonGiaTT],
	[TyGiaTT],
	[ThueSuat],
	[ThueXNK],
	[ThueXNKTon],
	[NgayThucXuat],
	[SoThuTuHang]
FROM
	[dbo].[t_KDT_SXXK_BCXNT]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXNT_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[STT],
	[TuNgay],
	[DenNgay],
	[MaDoanhNghiep],
	[MaNPL],
	[TenNPL],
	[TenDVT_NPL],
	[SoToKhaiNhap],
	[NgayDangKyNhap],
	[NgayHoanThanhNhap],
	[MaLoaiHinhNhap],
	[LuongNhap],
	[MaSP],
	[TenSP],
	[SoToKhaiXuat],
	[NgayDangKyXuat],
	[NgayHoanThanhXuat],
	[MaLoaiHinhXuat],
	[LuongSPXuat],
	[TenDVT_SP],
	[DinhMuc],
	[LuongNPLSuDung],
	[SoToKhaiTaiXuat],
	[NgayTaiXuat],
	[LuongNPLTaiXuat],
	[LuongTonCuoi],
	[ThanhKhoanTiep],
	[ChuyenMucDichKhac],
	[DonGiaTT],
	[TyGiaTT],
	[ThueSuat],
	[ThueXNK],
	[ThueXNKTon],
	[NgayThucXuat],
	[SoThuTuHang]
FROM [dbo].[t_KDT_SXXK_BCXNT] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXNT_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXNT_SelectAll]




































AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[STT],
	[TuNgay],
	[DenNgay],
	[MaDoanhNghiep],
	[MaNPL],
	[TenNPL],
	[TenDVT_NPL],
	[SoToKhaiNhap],
	[NgayDangKyNhap],
	[NgayHoanThanhNhap],
	[MaLoaiHinhNhap],
	[LuongNhap],
	[MaSP],
	[TenSP],
	[SoToKhaiXuat],
	[NgayDangKyXuat],
	[NgayHoanThanhXuat],
	[MaLoaiHinhXuat],
	[LuongSPXuat],
	[TenDVT_SP],
	[DinhMuc],
	[LuongNPLSuDung],
	[SoToKhaiTaiXuat],
	[NgayTaiXuat],
	[LuongNPLTaiXuat],
	[LuongTonCuoi],
	[ThanhKhoanTiep],
	[ChuyenMucDichKhac],
	[DonGiaTT],
	[TyGiaTT],
	[ThueSuat],
	[ThueXNK],
	[ThueXNKTon],
	[NgayThucXuat],
	[SoThuTuHang]
FROM
	[dbo].[t_KDT_SXXK_BCXNT]	

GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '37.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('37.4',GETDATE(), N'CẬP NHẬT PROCEDURE KẾT XUẤT DỮ LIỆU')
END