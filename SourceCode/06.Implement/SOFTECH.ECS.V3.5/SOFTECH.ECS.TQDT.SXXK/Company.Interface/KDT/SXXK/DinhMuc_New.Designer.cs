﻿using System.ComponentModel;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;

namespace Company.Interface.KDT.SXXK
{
    partial class DinhMuc_New
    {
        private Label label1;
        private ImageList ImageList1;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdMain_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DinhMuc_New));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.InDM1 = new Janus.Windows.UI.CommandBars.UICommand("InDM");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdAdd = new Janus.Windows.UI.CommandBars.UICommand("cmdAdd");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.SaoChep = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.cmdImportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.InDM = new Janus.Windows.UI.CommandBars.UICommand("InDM");
            this.Dinhmuc1 = new Janus.Windows.UI.CommandBars.UICommand("Dinhmuc");
            this.InPhieuTN2 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.InPhieuTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.Dinhmuc = new Janus.Windows.UI.CommandBars.UICommand("Dinhmuc");
            this.cmdSuaDM = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaDM");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.grdMain = new Janus.Windows.GridEX.GridEX();
            this.donViHaiQuanNewControl1 = new Company.Interface.Controls.DonViHaiQuanNewControl();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.dtpNgayTao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSheetName = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnImport = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnCheckLuongTonTK = new Janus.Windows.EditControls.UIButton();
            this.btnCapNhatNgayTK = new Janus.Windows.EditControls.UIButton();
            this.btnCapNhatTenNPL = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.dtpNgayTao);
            this.grbMain.Controls.Add(this.btnCapNhatTenNPL);
            this.grbMain.Controls.Add(this.btnCapNhatNgayTK);
            this.grbMain.Controls.Add(this.btnCheckLuongTonTK);
            this.grbMain.Controls.Add(this.btnDelete);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.donViHaiQuanNewControl1);
            this.grbMain.Controls.Add(this.grdMain);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.txtSoTiepNhan);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(1114, 447);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "printer.png");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(183, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ngày tạo";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.BackColor = System.Drawing.Color.White;
            this.txtSoTiepNhan.Location = new System.Drawing.Point(128, 25);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(49, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.Text = "0";
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số tiếp nhận";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Hải quan tiếp nhận";
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdSend,
            this.cmdAdd,
            this.XacNhan,
            this.SaoChep,
            this.cmdImportExcel,
            this.InDM,
            this.InPhieuTN,
            this.Dinhmuc,
            this.cmdSuaDM});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave1,
            this.Separator2,
            this.InDM1,
            this.Separator1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.MergeRowOrder = 1;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(204, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
            this.cmdSave1.ToolTipText = "Ctrl + S";
            this.cmdSave1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // InDM1
            // 
            this.InDM1.Key = "InDM";
            this.InDM1.Name = "InDM1";
            this.InDM1.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.InDM1.Text = "In";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdSave
            // 
            this.cmdSave.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave.Icon")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSave.Text = "Lưu thông tin";
            this.cmdSave.ToolTipText = "Lưu thông tin (Ctrl + S)";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Gửi thông tin";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAdd.Icon")));
            this.cmdAdd.Key = "cmdAdd";
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.cmdAdd.Text = "Thêm mới định mức";
            this.cmdAdd.ToolTipText = "Thêm mới định mức (Ctrl + N)";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // SaoChep
            // 
            this.SaoChep.Key = "SaoChep";
            this.SaoChep.Name = "SaoChep";
            this.SaoChep.Text = "Sao chép định mức";
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Key = "cmdImportExcel";
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Text = "Nhập từ file Excel";
            // 
            // InDM
            // 
            this.InDM.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Dinhmuc1,
            this.InPhieuTN2});
            this.InDM.Icon = ((System.Drawing.Icon)(resources.GetObject("InDM.Icon")));
            this.InDM.Key = "InDM";
            this.InDM.Name = "InDM";
            this.InDM.Text = "In định mức";
            // 
            // Dinhmuc1
            // 
            this.Dinhmuc1.ImageIndex = 4;
            this.Dinhmuc1.Key = "Dinhmuc";
            this.Dinhmuc1.Name = "Dinhmuc1";
            // 
            // InPhieuTN2
            // 
            this.InPhieuTN2.ImageIndex = 4;
            this.InPhieuTN2.Key = "InPhieuTN";
            this.InPhieuTN2.Name = "InPhieuTN2";
            // 
            // InPhieuTN
            // 
            this.InPhieuTN.Key = "InPhieuTN";
            this.InPhieuTN.Name = "InPhieuTN";
            this.InPhieuTN.Text = "Phiếu tiếp nhận";
            // 
            // Dinhmuc
            // 
            this.Dinhmuc.Key = "Dinhmuc";
            this.Dinhmuc.Name = "Dinhmuc";
            this.Dinhmuc.Text = "Định mức";
            // 
            // cmdSuaDM
            // 
            this.cmdSuaDM.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuaDM.Image")));
            this.cmdSuaDM.Key = "cmdSuaDM";
            this.cmdSuaDM.Name = "cmdSuaDM";
            this.cmdSuaDM.Text = "Sửa định mức";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1114, 32);
            // 
            // grdMain
            // 
            this.grdMain.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdMain.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.grdMain.DataMember = "SanPham";
            grdMain_DesignTimeLayout.LayoutString = resources.GetString("grdMain_DesignTimeLayout.LayoutString");
            this.grdMain.DesignTimeLayout = grdMain_DesignTimeLayout;
            this.grdMain.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdMain.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdMain.FocusCellFormatStyle.ForeColor = System.Drawing.Color.Black;
            this.grdMain.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grdMain.FrozenColumns = 8;
            this.grdMain.GroupByBoxVisible = false;
            this.grdMain.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdMain.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grdMain.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdMain.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdMain.Hierarchical = true;
            this.grdMain.ImageList = this.ImageList1;
            this.grdMain.Location = new System.Drawing.Point(10, 96);
            this.grdMain.Name = "grdMain";
            this.grdMain.RecordNavigator = true;
            this.grdMain.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdMain.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdMain.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdMain.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdMain.SelectedFormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grdMain.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.grdMain.Size = new System.Drawing.Size(1091, 312);
            this.grdMain.TabIndex = 8;
            this.grdMain.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.grdMain.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grdMain.VisualStyleManager = this.vsmMain;
            this.grdMain.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.grdMain.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.grdMain.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            this.grdMain.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grdMain_FormattingRow);
            // 
            // donViHaiQuanNewControl1
            // 
            this.donViHaiQuanNewControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanNewControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanNewControl1.Location = new System.Drawing.Point(127, 56);
            this.donViHaiQuanNewControl1.Ma = "";
            this.donViHaiQuanNewControl1.MaCuc = "";
            this.donViHaiQuanNewControl1.Name = "donViHaiQuanNewControl1";
            this.donViHaiQuanNewControl1.ReadOnly = true;
            this.donViHaiQuanNewControl1.Size = new System.Drawing.Size(419, 22);
            this.donViHaiQuanNewControl1.TabIndex = 5;
            this.donViHaiQuanNewControl1.Ten = "";
            this.donViHaiQuanNewControl1.VisualStyleManager = null;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(939, 414);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 23);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(1022, 414);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(79, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dtpNgayTao
            // 
            // 
            // 
            // 
            this.dtpNgayTao.DropDownCalendar.Name = "";
            this.dtpNgayTao.Location = new System.Drawing.Point(246, 25);
            this.dtpNgayTao.Name = "dtpNgayTao";
            this.dtpNgayTao.Size = new System.Drawing.Size(200, 21);
            this.dtpNgayTao.TabIndex = 36;
            this.dtpNgayTao.Value = new System.DateTime(2015, 8, 1, 0, 0, 0, 0);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.txtSheetName);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.btnImport);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(564, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(538, 87);
            this.uiGroupBox2.TabIndex = 37;
            this.uiGroupBox2.Text = " Import Excel ";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(338, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Xem mẫu Excel";
            this.label4.MouseLeave += new System.EventHandler(this.label4_MouseLeave);
            this.label4.Click += new System.EventHandler(this.label4_Click);
            this.label4.MouseHover += new System.EventHandler(this.label4_MouseHover);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Chọn tập tin";
            // 
            // txtSheetName
            // 
            this.txtSheetName.Location = new System.Drawing.Point(110, 49);
            this.txtSheetName.Name = "txtSheetName";
            this.txtSheetName.Size = new System.Drawing.Size(91, 21);
            this.txtSheetName.TabIndex = 3;
            this.txtSheetName.Text = "Sheet1";
            this.txtSheetName.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tên Sheet";
            // 
            // btnImport
            // 
            this.btnImport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Icon = ((System.Drawing.Icon)(resources.GetObject("btnImport.Icon")));
            this.btnImport.Location = new System.Drawing.Point(222, 49);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(88, 23);
            this.btnImport.TabIndex = 1;
            this.btnImport.Text = "Import";
            this.btnImport.VisualStyleManager = this.vsmMain;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFilePath.Location = new System.Drawing.Point(110, 17);
            this.txtFilePath.Multiline = true;
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(397, 23);
            this.txtFilePath.TabIndex = 0;
            this.txtFilePath.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            this.txtFilePath.Click += new System.EventHandler(this.txtFilePath_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnCheckLuongTonTK
            // 
            this.btnCheckLuongTonTK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheckLuongTonTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckLuongTonTK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCheckLuongTonTK.Icon")));
            this.btnCheckLuongTonTK.Location = new System.Drawing.Point(333, 414);
            this.btnCheckLuongTonTK.Name = "btnCheckLuongTonTK";
            this.btnCheckLuongTonTK.Size = new System.Drawing.Size(97, 23);
            this.btnCheckLuongTonTK.TabIndex = 6;
            this.btnCheckLuongTonTK.Text = "3.Kiểm Tra";
            this.btnCheckLuongTonTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnCheckLuongTonTK.Click += new System.EventHandler(this.btnCheckLuongTonTK_Click);
            // 
            // btnCapNhatNgayTK
            // 
            this.btnCapNhatNgayTK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCapNhatNgayTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatNgayTK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCapNhatNgayTK.Icon")));
            this.btnCapNhatNgayTK.Location = new System.Drawing.Point(12, 414);
            this.btnCapNhatNgayTK.Name = "btnCapNhatNgayTK";
            this.btnCapNhatNgayTK.Size = new System.Drawing.Size(153, 23);
            this.btnCapNhatNgayTK.TabIndex = 6;
            this.btnCapNhatNgayTK.Text = "1.Cập nhật ngày CT";
            this.btnCapNhatNgayTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnCapNhatNgayTK.Click += new System.EventHandler(this.btnCapNhatNgayTK_Click);
            // 
            // btnCapNhatTenNPL
            // 
            this.btnCapNhatTenNPL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCapNhatTenNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatTenNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCapNhatTenNPL.Icon")));
            this.btnCapNhatTenNPL.Location = new System.Drawing.Point(171, 414);
            this.btnCapNhatTenNPL.Name = "btnCapNhatTenNPL";
            this.btnCapNhatTenNPL.Size = new System.Drawing.Size(153, 23);
            this.btnCapNhatTenNPL.TabIndex = 6;
            this.btnCapNhatTenNPL.Text = "2.Cập nhật tên NPL";
            this.btnCapNhatTenNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnCapNhatTenNPL.Click += new System.EventHandler(this.btnCapNhatTenNPL_Click);
            // 
            // DinhMuc_New
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(1114, 479);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MinimizeBox = false;
            this.Name = "DinhMuc_New";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Khai báo định mức";
            this.Load += new System.EventHandler(this.DinhMucSendForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private EditBox txtSoTiepNhan;
        private Label label2;
        private Label label3;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdAdd;
        private GridEX grdMain;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep;
        private Janus.Windows.UI.CommandBars.UICommand cmdImportExcel;
        private Janus.Windows.UI.CommandBars.UICommand InDM1;
        private Janus.Windows.UI.CommandBars.UICommand InDM;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Company.Interface.Controls.DonViHaiQuanNewControl donViHaiQuanNewControl1;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN;
        private Janus.Windows.UI.CommandBars.UICommand Dinhmuc;
        private Janus.Windows.UI.CommandBars.UICommand Dinhmuc1;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN2;
        private UIButton btnDelete;
        private UIButton btnClose;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaDM;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayTao;
        private UIGroupBox uiGroupBox2;
        private Label label4;
        private Label label9;
        private EditBox txtSheetName;
        private Label label8;
        private UIButton btnImport;
        private EditBox txtFilePath;
        private OpenFileDialog openFileDialog1;
        private UIButton btnCheckLuongTonTK;
        private UIButton btnCapNhatNgayTK;
        private UIButton btnCapNhatTenNPL;

    }
}
