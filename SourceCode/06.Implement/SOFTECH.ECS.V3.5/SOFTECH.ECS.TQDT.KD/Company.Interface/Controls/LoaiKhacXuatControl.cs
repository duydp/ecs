using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Controls
{
    public partial class LoaiHinhKhacXuatControl : UserControl
    {
        //public string code ="";
        public LoaiHinhKhacXuatControl()
        {
            this.InitializeComponent();
        }


        public string Ma
        {
            set
            {
                this.txtMa.Text = value;
                this.cbTen.Value = this.txtMa.Text.PadRight(5);
            }
            get { return this.txtMa.Text; }
        }
        public bool ReadOnly
        {
            set
            {
                this.txtMa.ReadOnly = value;
                this.cbTen.ReadOnly = value;
            }
            get { return this.cbTen.ReadOnly; }
        }
        private string _Nhom = string.Empty;
        public string Nhom
        {
            set
            {
                this._Nhom = value;
                if (!this.DesignMode)
                {
                    this.loadData();
                }

            }
            get { return this._Nhom; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.txtMa.VisualStyleManager = value;
                this.cbTen.VisualStyleManager = value;
            }
            get
            {
                return this.txtMa.VisualStyleManager;
            }
        }
        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);

        private void loadData()
        {
            if (this._Nhom.Trim().Length == 0) this._Nhom = GlobalSettings.NHOM_LOAI_HINH_KHAC_NHAP;
            this.dtLoaiHinhMauDich.Clear();
            DataTable dt;
            if (this._Nhom.Trim().Length > 0)
                dt = LoaiHinhMauDich.SelectBy_NhomOne(this._Nhom);
            else
                dt = LoaiHinhMauDich.SelectAll();
            dtLoaiHinhMauDich.Rows.Clear();
            foreach (DataRow row in dt.Rows)
            {
                this.dtLoaiHinhMauDich.ImportRow(row);
            }
            //    this.cbTen.Value = txtMa.Text.PadRight(5);
        }

        private void cbLoaiHinhMauDich_ValueChanged(object sender, EventArgs e)
        {
            this.txtMa.Text = this.cbTen.Value.ToString().Trim();
            if (this.ValueChanged != null) this.ValueChanged(sender, e);
        }

        private void txtMaLoaiHinh_Leave(object sender, EventArgs e)
        {
            this.txtMa.Text = this.txtMa.Text.Trim();
            if (this.dtLoaiHinhMauDich.Select("ID='" + txtMa.Text.Trim() + "'").Length == 0)
            {
                this.txtMa.Text = GlobalSettings.NHOM_LOAI_HINH_KHAC_NHAP;
            }
            this.cbTen.Value = this.txtMa.Text.PadRight(5);
        }
    }
}