using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V2
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V2
using Company.GC.BLL.KDT;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.Utils;
#elif SXXK_V2
using Company.BLL.KDT;
using Company.BLL.DuLieuChuan;
using Company.BLL.Utils;
#endif
using System.Collections.Generic;
using System.Linq;
namespace Company.Interface.Report
{
    public partial class TQDTPhuLucToKhaiNhap_TT15 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public List<Company.KDT.SHARE.QuanLyChungTu.Container> ContCollection = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        public Company.Interface.Report.ReportViewTKNTQDTFormTT15 report;
        public int SoToKhai;
        public DateTime NgayDangKy;
      
        public bool BanLuuHaiQuan = false;
        public bool MienThueNK = false;
        public bool MienThueGTGT = false;
        public bool InMaHang = false;
        public int SoDongHang;
        public TQDTPhuLucToKhaiNhap_TT15()
        {
            InitializeComponent();
        }

        public void BindReport(string pls)
        {
            try
            {
                ResetEmpty();

                this.PrintingSystem.ShowMarginsWarning = false;
                int pluc = report.index;

                //Set ngay mac dinh
                DateTime minDate = new DateTime(1900, 1, 1);

                #region Thong tin chinh
                //Chi cục HD đăng ký
                lblChiCucHQDangKy.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                lblChiCucHQDangKy.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //Chi cục Hải quan cửa khẩu nhập
                lblChiCucHQCuaKhau.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);//DonViHaiQuan.GetName(this.TKMD.CuaKhau_ID);
                lblChiCucHQCuaKhau.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //So luong phu luc to khai
                if (this.TKMD.SoLuongPLTK > 0)
                    lblSoPhuLucToKhai.Text = pluc.ToString("00");
                else
                {
                    lblSoPhuLucToKhai.Text = this.TKMD.HMDCollection.Count > 0 ? (this.TKMD.HMDCollection.Count / 3).ToString("00") : "0";
                }

                //Số tờ khai
                if (this.TKMD.SoToKhai > 0)
                    lblSoToKhai.Text = this.TKMD.SoToKhai + "";

                //Ngày giờ đăng ký
                if (this.TKMD.NgayDangKy > minDate)
                    lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy HH:mm");
                else
                    lblNgayDangKy.Text = "";
                // Ma Loai Hinh
                string stlh = "";
                stlh = LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                lblLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + stlh;
                lblLoaiHinh.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                #endregion

                #region Thong tin hang

                for (int j = 0; j < this.HMDCollection.Count; j++)
                {
                    if (j == 0)
                    {
                        fillHang(xrTableHang1, xrTableThue1, j);
                    }
                    else if (j == 1)
                    {
                        fillHang(xrTableHang2, xrTableThue2, j);
                    }
                    else
                        fillHang(xrTableHang3, xrTableThue3, j);
                }


                #endregion

                #region Container
                for (int i = 0; i < this.ContCollection.Count; i++)
                {
                    fillContainer(xrTableCont, i);
                }
                //Tong trong luong container
                //double sumTrongLuong = TKMD.VanTaiDon.ListHangOfVanDon.Sum(x => x.TrongLuong);
                //lblTongContainer.Text = string.Format("{0:N3}", sumTrongLuong);
                #endregion

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }


        }
        #region dien thong tin container
        private void fillContainer(XRTable tableCont, int contItem)
        {
            int phulucso = report.index;
            XRControl control = new XRControl();
            Company.KDT.SHARE.QuanLyChungTu.Container cont = this.ContCollection[contItem];
            control = tableCont.Rows[contItem].Controls["lblSTTCont" + (contItem + 1)];
            control.Text = (contItem + 1 + ((phulucso - 1) * 4)).ToString();
            control = tableCont.Rows[contItem].Controls["lblSoHieuCont" + (contItem + 1)];
            control.Text = cont.SoHieu;//TKMD.VanTaiDon.ContainerCollection[contItem].SoHieu;
            //int countSoLuongKien = TKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer == cont.SoHieu).GroupBy(x => x.SoHieuKien).Count();
            //control = tableCont.Rows[contItem].Controls["lblSoLuongKienCont" + (contItem + 1)];
            //control.Text = string.Format("{0:00}", countSoLuongKien);
            //double countTrongLuong = TKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer ==cont.SoHieu).Sum(x => x.TrongLuong);
            //control = tableCont.Rows[contItem].Controls["lblTrongLuongCont" + (contItem + 1)];
            //control.Text = string.Format("{0:N3}", countTrongLuong);
        }
   
        #endregion

        #region Dien thong tin hang và thue phu luc
        private void fillHang(XRTable tableHang,XRTable tableThue, int hangItem)
        {
#if KD_V2
            double tongTriGiaNT = 0;
            double tongTienThueXNK = 0;
            double tongTienThueTatCa = 0;
            double tongThueGTGT = 0;
            double tongTriGiaThuKhac = 0;
            double TriGiaTTTTDB;
            double TriGiaTTGTGT;
#elif GC_V2
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            decimal TriGiaTTTTDB;
            decimal TriGiaTTGTGT;
#elif SXXK_V2
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            decimal TriGiaTTTTDB;
            decimal TriGiaTTGTGT;
#endif
            int phulucso = report.index;
            XRControl control = new XRControl();
            HangMauDich hmd = this.HMDCollection[hangItem];
            // STT hang
            control = tableHang.Rows[0].Controls["lblSTTHang" + (hangItem + 1)];
            control.Text = (hangItem + 1+((phulucso-1)*3)).ToString();
            //18. Mo ta hang hoa - Ten hang
            control = tableHang.Rows[0].Controls["lblMoTaHang" + (hangItem + 1)];
            if (!InMaHang)
                control.Text = hmd.TenHang;
            else
            {
                if (hmd.MaPhu.Trim().Length > 0)
                    control.Text = hmd.TenHang + "/" + hmd.MaPhu;
                else
                    control.Text = hmd.TenHang;
            }
            control.WordWrap = true;
            control.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            //19. Ma HS hang
            control = tableHang.Rows[0].Controls["lblMaHSHang" + (hangItem + 1)];
            control.Text = hmd.MaHS;
            //20. Xuat xu
            control = tableHang.Rows[0].Controls["lblXuatXuHang" + (hangItem + 1)];
            control.Text = hmd.NuocXX_ID;

            //21. Che do uu dai

            //22. luong hang
            control = tableHang.Rows[0].Controls["lblLuongHang" + (hangItem + 1)];
            control.Text = hmd.SoLuong.ToString("G15");
            //23.Don vi tinh
            control = tableHang.Rows[0].Controls["lblDVTHang" + (hangItem + 1)];
            control.Text = DonViTinh.GetName(hmd.DVT_ID);
            //24. Don gia nguyen te
            control = tableHang.Rows[0].Controls["lblDonGiaHang" + (hangItem + 1)];
            control.Text = hmd.DonGiaKB.ToString("G10");
            //25. Tri gia nguyen te
            control = tableHang.Rows[0].Controls["lblTGNTHang" + (hangItem + 1)];
            control.Text = hmd.TriGiaKB.ToString("N2");

            //26. Thue Nhap khau - NK
            //Tri gia tinh thue
            control = tableThue.Rows[1].Controls["lblTGTTNKHang" + (hangItem + 1)];
            control.Text = hmd.TriGiaTT.ToString("N0");
            //Thue suat
            control = tableThue.Rows[1].Controls["lblThueSuatNKHang" + (hangItem + 1)];
        #if (KD_V2 ||GC_V2)
           if (hmd.ThueSuatXNKGiam.Trim() == "")
                    control.Text = hmd.ThueSuatXNK.ToString("N0");
                else
                    control.Text = hmd.ThueSuatXNKGiam;
        #elif SXXK_V2
            control.Text = hmd.ThueSuatXNK.ToString("N0");
        #endif
            
            //}
            //Tien thue
            control = tableThue.Rows[1].Controls["lblTienThueNKHang" + (hangItem + 1)];
            control.Text = hmd.ThueXNK.ToString("N0");

            //27. Thue thieu thu dac biet - TTDB
            control = tableThue.Rows[2].Controls["lblTGTTTTDBHang" + (hangItem + 1)];
            TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
            control.Text = TriGiaTTTTDB.ToString("N0");
            //thue suat
            control = tableThue.Rows[2].Controls["lblThueSuatTTDBHang" + (hangItem + 1)];
        #if (KD_V2 ||GC_V2)
           if (hmd.ThueSuatTTDBGiam.Trim() == "")
                control.Text = hmd.ThueSuatTTDB.ToString("N0");
            else
                control.Text = hmd.ThueSuatTTDBGiam;
        #elif SXXK_V2
            control.Text = hmd.ThueSuatTTDB.ToString("N0");
        #endif
           
            //tien thue
            control = tableThue.Rows[2].Controls["lblTienThueTTDBHang" + (hangItem + 1)];
            control.Text = hmd.ThueTTDB.ToString("N0");

            //28. Thue Bao ve moi truong - BVMT
            control = tableThue.Rows[3].Controls["lblThueSuatBVMTHang" + (hangItem + 1)];
            if (hmd.TyLeThuKhac != 0)
                control.Text = hmd.TyLeThuKhac.ToString("N0");
            else
                control.Text = "";
            //tien thue
            control = tableThue.Rows[3].Controls["lblTienThueBVMTHang" + (hangItem + 1)];
            control.Text = hmd.TriGiaThuKhac.ToString("N0");

            //29. Thue gia tri gia tang - VAT
            //tri gia tinh thue
            control = tableThue.Rows[4].Controls["lblTGTTVATHang" + (hangItem + 1)];
            TriGiaTTGTGT = TriGiaTTTTDB + hmd.ThueTTDB + hmd.TriGiaThuKhac;
            control.Text = TriGiaTTGTGT.ToString("N0");
            //thue suat
            control = tableThue.Rows[4].Controls["lblThueSuatVATHang" + (hangItem + 1)];
        #if (KD_V2 ||GC_V2)
           if (hmd.ThueSuatVATGiam.Trim() == "")
                control.Text = hmd.ThueSuatGTGT.ToString("N0");
            else
                control.Text = hmd.ThueSuatVATGiam;
        #elif SXXK_V2
            control.Text = hmd.ThueSuatGTGT.ToString("N0");
        #endif
            
            //tien thue
            control = tableThue.Rows[4].Controls["lblTienThueVATHang" + (hangItem + 1)];
            control.Text = hmd.ThueGTGT.ToString("N0");

            // tong thue
            tongTienThueXNK += hmd.ThueXNK;
            tongThueGTGT += hmd.ThueTTDB;
            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
            tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
            tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
            control = tableThue.Rows[5].Controls["lblTongThueHang" + (hangItem + 1)];
            control.Text = tongTienThueTatCa.ToString("N0");

        }
        #endregion

        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        private void ResetEmpty()
        {
            try
            {
               
                //Chi cuc Hai quan dang ky
                lblChiCucHQDangKy.Text = "";

                //Chi cuc Hai quan cua khau
                lblChiCucHQCuaKhau.Text = "";

                //Số tờ khai
                lblSoToKhai.Text = "";

                //Ngày giờ đăng ký
                lblNgayDangKy.Text = "";

                //So luong phu luc to khai
                lblSoPhuLucToKhai.Text = "";
             
                // Ma Loai Hinh
                lblLoaiHinh.Text = "";

            
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}
