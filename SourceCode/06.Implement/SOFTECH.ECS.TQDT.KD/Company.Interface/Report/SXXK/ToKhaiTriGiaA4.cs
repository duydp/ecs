﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiTriGiaA4 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiTriGia TKTG = new ToKhaiTriGia();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKTGA4Form report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public ToKhaiTriGiaA4()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            DateTime minDate = new DateTime(1900, 1, 1);
            if (TKMD.SoToKhai > 0)
                lblKemTheoSTK.Text = "Kèm theo tờ khai hàng hóa nhập khẩu HQ/2009-TKDTNK Số... " + TKMD.SoToKhai + ".../NK/..." + TKMD.MaLoaiHinh + ".../..........Ngày........../........20" + "";
            else
                lblKemTheoSTK.Text = "Kèm theo tờ khai hàng hóa nhập khẩu HQ/2009-TKDTNK Số... " + ".../NK/..." + TKMD.MaLoaiHinh + ".../..........Ngày........../........20" + "";
            if (TKTG.QuyenSuDung)
                chkYes2.Text = "X";
            else
                chkNo2.Text = "X";
            if (TKTG.KhongXacDinh)
                chkYes3.Text = "X";
            else
                chkNo3.Text = "X";
            if (TKTG.TraThem)
                chkYes41.Text = "X";
            else
                chkNo41.Text = "X";
            if (TKTG.TienTra)
                chkYes42.Text = "X";
            else
                chkNo42.Text = "X";
            if (TKTG.CoQuanHeDacBiet)
                chkYes51.Text = "X";
            else
                chkNo51.Text = "X";
            if (TKTG.AnhHuongQuanHe)
                chkYes52.Text = "X";
            else
                chkNo52.Text = "X";

            if (this.TKTG.HTGCollection.Count <= 8)
            {
                if (this.TKTG.HTGCollection.Count >= 1)
                {
                    for (int i = 0; i < TKTG.HTGCollection.Count; i++)
                    {
                        int j = 0, k = 0, l = 0, m = 0;
                        XRControl control = new XRControl();
                        HangTriGia htg = this.TKTG.HTGCollection[i];
                        if (i <= 3)
                        {
                            control = this.xrTable12.Rows[j].Controls["lblHang7" + (i + 1)];
                            control.Text = checkZero(htg.GiaTrenHoaDon);
                            j++;
                            control = this.xrTable12.Rows[j].Controls["lblHang8" + (i + 1)];
                            control.Text = checkZero(htg.KhoanThanhToanGianTiep);
                            j++;
                            control = this.xrTable12.Rows[j].Controls["lblHang9" + (i + 1)];
                            control.Text = checkZero(htg.TraTruoc);
                            //

                            control = this.xrTable8.Rows[k].Controls["lblHang10" + (i + 1)];
                            control.Text = checkZero(htg.HoaHong);
                            k++;

                            control = this.xrTable8.Rows[k].Controls["lblHang11" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiBaoBi);
                            k++;

                            control = this.xrTable8.Rows[k].Controls["lblHang12" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiDongGoi);
                            k++;

                            control = this.xrTable8.Rows[k].Controls["lblHang13" + (i + 1)];
                            control.Text = checkZero(htg.TroGiup);
                            k++;

                            control = this.xrTable8.Rows[k].Controls["lblHanga" + (i + 1)];
                            control.Text = checkZero(htg.NguyenLieu);
                            k++;

                            control = this.xrTable8.Rows[k].Controls["lblHangb" + (i + 1)];
                            control.Text = checkZero(htg.VatLieu);
                            k++;

                            control = this.xrTable8.Rows[k].Controls["lblHangc" + (i + 1)];
                            control.Text = checkZero(htg.CongCu);
                            k++;

                            control = this.xrTable8.Rows[k].Controls["lblHangd" + (i + 1)];
                            control.Text = checkZero(htg.ThietKe);
                            k++;

                            control = this.xrTable8.Rows[k].Controls["lblHang14" + (i + 1)];
                            control.Text = checkZero(htg.BanQuyen);
                            k++;
                            //
                            control = this.xrTable8.Rows[k].Controls["lblHang15" + (i + 1)];
                            control.Text = checkZero(htg.TienTraSuDung);
                            k++;
                            control = this.xrTable8.Rows[k].Controls["lblHang16" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiVanChuyen);
                            k++;
                            
                            control = this.xrTable8.Rows[k].Controls["lblHang17" + (i + 1)];
                            control.Text = checkZero(htg.PhiBaoHiem);

                            control = this.xrTable10.Rows[l].Controls["lblHang18" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiNoiDia);
                            l++;
                            control = this.xrTable10.Rows[l].Controls["lblHang19" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiPhatSinh);
                            l++;
                            control = this.xrTable10.Rows[l].Controls["lblHang20" + (i + 1)];
                            control.Text = checkZero(htg.TienLai);
                            l++;
                            control = this.xrTable10.Rows[l].Controls["lblHang21" + (i + 1)];
                            control.Text = checkZero(htg.TienThue);
                            l++;
                            control = this.xrTable10.Rows[l].Controls["lblHang22" + (i + 1)];
                            control.Text = checkZero(htg.GiamGia);

                            control = this.xrTable6.Rows[m].Controls["lblHang23" + (i + 1)];
                            control.Text = checkZero(htg.TriGiaNguyenTe);
                            m++;
                            control = this.xrTable6.Rows[m].Controls["lblHang24" + (i + 1)];
                            control.Text = checkZero(htg.TriGiaVND);
                        }
                        else
                        {
                            j = 0;
                            k = 0;
                            l = 0;
                            m = 0;
                            //Xtable 3
                            control = this.xrTable3.Rows[j].Controls["lblHang7" + (i + 1)];
                            control.Text = checkZero(htg.GiaTrenHoaDon);
                            j++;
                            control = this.xrTable3.Rows[j].Controls["lblHang8" + (i + 1)];
                            control.Text = checkZero(htg.KhoanThanhToanGianTiep);
                            j++;
                            control = this.xrTable3.Rows[j].Controls["lblHang9" + (i + 1)];
                            control.Text = checkZero(htg.TraTruoc);
                            //
                            //Xtable 7
                            control = this.xrTable7.Rows[k].Controls["lblHang10" + (i + 1)];
                            control.Text = checkZero(htg.HoaHong);
                            k++;

                            control = this.xrTable7.Rows[k].Controls["lblHang11" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiBaoBi);
                            k++;

                            control = this.xrTable7.Rows[k].Controls["lblHang12" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiDongGoi);
                            k++;

                            control = this.xrTable7.Rows[k].Controls["lblHang13" + (i + 1)];
                            control.Text = checkZero(htg.TroGiup);
                            k++;

                            control = this.xrTable7.Rows[k].Controls["lblHanga" + (i + 1)];
                            control.Text = checkZero(htg.NguyenLieu);
                            k++;

                            control = this.xrTable7.Rows[k].Controls["lblHangb" + (i + 1)];
                            control.Text = checkZero(htg.VatLieu);
                            k++;

                            control = this.xrTable7.Rows[k].Controls["lblHangc" + (i + 1)];
                            control.Text = checkZero(htg.CongCu);
                            k++;

                            control = this.xrTable7.Rows[k].Controls["lblHangd" + (i + 1)];
                            control.Text = checkZero(htg.ThietKe);
                            k++;

                            control = this.xrTable7.Rows[k].Controls["lblHang14" + (i + 1)];
                            control.Text = checkZero(htg.BanQuyen);
                            k++;
                            //
                            control = this.xrTable7.Rows[k].Controls["lblHang15" + (i + 1)];
                            control.Text = checkZero(htg.TienTraSuDung);
                            k++;
                            control = this.xrTable7.Rows[k].Controls["lblHang16" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiVanChuyen);
                            k++;

                            control = this.xrTable7.Rows[k].Controls["lblHang17" + (i + 1)];
                            control.Text = checkZero(htg.PhiBaoHiem);

                            //Xtable 5
                            control = this.xrTable5.Rows[l].Controls["lblHang18" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiNoiDia);
                            l++;
                            control = this.xrTable5.Rows[l].Controls["lblHang19" + (i + 1)];
                            control.Text = checkZero(htg.ChiPhiPhatSinh);
                            l++;
                            control = this.xrTable5.Rows[l].Controls["lblHang20" + (i + 1)];
                            control.Text = checkZero(htg.TienLai);
                            l++;
                            control = this.xrTable5.Rows[l].Controls["lblHang21" + (i + 1)];
                            control.Text = checkZero(htg.TienThue);
                            l++;
                            control = this.xrTable5.Rows[l].Controls["lblHang22" + (i + 1)];
                            control.Text = checkZero(htg.GiamGia);
                            //Xtable 2
                            control = this.xrTable2.Rows[m].Controls["lblHang23" + (i + 1)];
                            control.Text = checkZero(htg.TriGiaNguyenTe);
                            m++;
                            control = this.xrTable2.Rows[m].Controls["lblHang24" + (i + 1)];
                            control.Text = checkZero(htg.TriGiaVND);
                        }
                        if (i == 7) break;
                    }
                    //lblHang231.Text = Convert.ToDouble(lblHang71.Text) + Convert.ToDouble(lblHang81.Text) + Convert.ToDouble(lblHang91.Text) + Convert.ToDouble(lblHang101.Text) + Convert.ToDouble(lblHang111.Text) + Convert.ToDouble(lblHang121.Text) + Convert.ToDouble(lblHang131.Text) + Convert.ToDouble(lblHanga1.Text) + Convert.ToDouble(lblHangb1.Text) + Convert.ToDouble(lblHangc1.Text) + Convert.ToDouble(lblHangd1.Text) + Convert.ToDouble(lblHang141.Text) + Convert.ToDouble(lblHang151.Text) + Convert.ToDouble(lblHang161.Text) + Convert.ToDouble(lblHang171.Text) - Convert.ToDouble(lblHang181.Text) - Convert.ToDouble(lblHang191.Text) - Convert.ToDouble(lblHang120.Text) - Convert.ToDouble(lblHang211.Text) - Convert.ToDouble(lblHang221.Text) + "";
                }
            }
            else
            {
                //Hien thi thong bao Kem theo phu luc :
                lblInPL.Text = " ( Có phụ lục đính kèm ) ";

            }
        }

        private string checkZero(double number)
        {
            try
            {
                if (number.ToString("N2").Equals("0,00"))
                    return "";
                else
                    return number.ToString("N2");
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + "   ";
            temp += s[s.Length - 1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            
        }
        public void setNhomHang(string tenNhomHang)
        {
          //  TenHang1.Text = tenNhomHang.ToUpper();
        }
        private bool IsHaveTax()
        {
           /// foreach (HangTriGia htg in this.TKTG.HTGCollection)
                ////if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            //lblMienThue2.Visible = t;
            //xrTable2.Visible = !t;
            //lblTongThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = xrTablesaaa.Visible = lblTongThueXNKChu.Visible = !t;

        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void xrKD_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            if (cell.Text.Trim() == "")
                cell.Text = "X";
            else
                cell.Text = "";
            this.CreateDocument();
        }
    }
}
