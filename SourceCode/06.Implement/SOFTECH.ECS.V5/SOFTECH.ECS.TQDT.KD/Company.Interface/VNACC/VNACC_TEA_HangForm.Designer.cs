﻿namespace Company.Interface
{
    partial class VNACC_TEA_HangForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grListHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TEA_HangForm));
            this.UIGroup = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.ctrSoLuongDaSuDung = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrSoLuongDangKyMT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTriGiaDuKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongDaSuDung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongDangKyMT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMoTaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grListHang = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UIGroup)).BeginInit();
            this.UIGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.UIGroup);
            this.grbMain.Size = new System.Drawing.Size(716, 421);
            // 
            // UIGroup
            // 
            this.UIGroup.BackColor = System.Drawing.Color.Transparent;
            this.UIGroup.Controls.Add(this.btnClose);
            this.UIGroup.Controls.Add(this.btnGhi);
            this.UIGroup.Controls.Add(this.btnXoa);
            this.UIGroup.Controls.Add(this.ctrSoLuongDaSuDung);
            this.UIGroup.Controls.Add(this.ctrSoLuongDangKyMT);
            this.UIGroup.Controls.Add(this.txtTriGiaDuKien);
            this.UIGroup.Controls.Add(this.txtSoLuongDaSuDung);
            this.UIGroup.Controls.Add(this.txtTriGia);
            this.UIGroup.Controls.Add(this.txtSoLuongDangKyMT);
            this.UIGroup.Controls.Add(this.label9);
            this.UIGroup.Controls.Add(this.label8);
            this.UIGroup.Controls.Add(this.label3);
            this.UIGroup.Controls.Add(this.label4);
            this.UIGroup.Controls.Add(this.label5);
            this.UIGroup.Controls.Add(this.label6);
            this.UIGroup.Controls.Add(this.txtMoTaHangHoa);
            this.UIGroup.Controls.Add(this.label1);
            this.UIGroup.Controls.Add(this.label7);
            this.UIGroup.Controls.Add(this.label2);
            this.UIGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.UIGroup.Location = new System.Drawing.Point(0, 0);
            this.UIGroup.Name = "UIGroup";
            this.UIGroup.Size = new System.Drawing.Size(716, 159);
            this.UIGroup.TabIndex = 0;
            this.UIGroup.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(634, 128);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(482, 128);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(70, 23);
            this.btnGhi.TabIndex = 7;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(558, 128);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(70, 23);
            this.btnXoa.TabIndex = 8;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // ctrSoLuongDaSuDung
            // 
            this.ctrSoLuongDaSuDung.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrSoLuongDaSuDung.Code = "";
            this.ctrSoLuongDaSuDung.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrSoLuongDaSuDung.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrSoLuongDaSuDung.IsValidate = false;
            this.ctrSoLuongDaSuDung.Location = new System.Drawing.Point(345, 92);
            this.ctrSoLuongDaSuDung.Name = "ctrSoLuongDaSuDung";
            this.ctrSoLuongDaSuDung.Name_VN = "";
            this.ctrSoLuongDaSuDung.ShowColumnCode = true;
            this.ctrSoLuongDaSuDung.ShowColumnName = false;
            this.ctrSoLuongDaSuDung.Size = new System.Drawing.Size(79, 26);
            this.ctrSoLuongDaSuDung.TabIndex = 4;
            this.ctrSoLuongDaSuDung.WhereCondition = "";
            // 
            // ctrSoLuongDangKyMT
            // 
            this.ctrSoLuongDangKyMT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrSoLuongDangKyMT.Code = "";
            this.ctrSoLuongDangKyMT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrSoLuongDangKyMT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrSoLuongDangKyMT.IsValidate = false;
            this.ctrSoLuongDangKyMT.Location = new System.Drawing.Point(344, 62);
            this.ctrSoLuongDangKyMT.Name = "ctrSoLuongDangKyMT";
            this.ctrSoLuongDangKyMT.Name_VN = "";
            this.ctrSoLuongDangKyMT.ShowColumnCode = true;
            this.ctrSoLuongDangKyMT.ShowColumnName = true;
            this.ctrSoLuongDangKyMT.Size = new System.Drawing.Size(79, 26);
            this.ctrSoLuongDangKyMT.TabIndex = 2;
            this.ctrSoLuongDangKyMT.WhereCondition = "";
            // 
            // txtTriGiaDuKien
            // 
            this.txtTriGiaDuKien.DecimalDigits = 20;
            this.txtTriGiaDuKien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaDuKien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaDuKien.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaDuKien.Location = new System.Drawing.Point(524, 90);
            this.txtTriGiaDuKien.MaxLength = 15;
            this.txtTriGiaDuKien.Name = "txtTriGiaDuKien";
            this.txtTriGiaDuKien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaDuKien.Size = new System.Drawing.Size(143, 21);
            this.txtTriGiaDuKien.TabIndex = 6;
            this.txtTriGiaDuKien.Text = "0";
            this.txtTriGiaDuKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaDuKien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaDuKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuongDaSuDung
            // 
            this.txtSoLuongDaSuDung.DecimalDigits = 20;
            this.txtSoLuongDaSuDung.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongDaSuDung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongDaSuDung.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongDaSuDung.Location = new System.Drawing.Point(165, 93);
            this.txtSoLuongDaSuDung.MaxLength = 15;
            this.txtSoLuongDaSuDung.Name = "txtSoLuongDaSuDung";
            this.txtSoLuongDaSuDung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongDaSuDung.Size = new System.Drawing.Size(146, 21);
            this.txtSoLuongDaSuDung.TabIndex = 3;
            this.txtSoLuongDaSuDung.Text = "0";
            this.txtSoLuongDaSuDung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongDaSuDung.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongDaSuDung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGia
            // 
            this.txtTriGia.DecimalDigits = 20;
            this.txtTriGia.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGia.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGia.Location = new System.Drawing.Point(524, 60);
            this.txtTriGia.MaxLength = 15;
            this.txtTriGia.Name = "txtTriGia";
            this.txtTriGia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGia.Size = new System.Drawing.Size(143, 21);
            this.txtTriGia.TabIndex = 5;
            this.txtTriGia.Text = "0";
            this.txtTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuongDangKyMT
            // 
            this.txtSoLuongDangKyMT.DecimalDigits = 20;
            this.txtSoLuongDangKyMT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongDangKyMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongDangKyMT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongDangKyMT.Location = new System.Drawing.Point(165, 63);
            this.txtSoLuongDangKyMT.MaxLength = 15;
            this.txtSoLuongDangKyMT.Name = "txtSoLuongDangKyMT";
            this.txtSoLuongDangKyMT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongDangKyMT.Size = new System.Drawing.Size(146, 21);
            this.txtSoLuongDangKyMT.TabIndex = 1;
            this.txtSoLuongDangKyMT.Text = "0";
            this.txtSoLuongDangKyMT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongDangKyMT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongDangKyMT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongDangKyMT.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(673, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 56;
            this.label9.Text = "ĐVT";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(673, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 55;
            this.label8.Text = "ĐVT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(315, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "ĐVT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(315, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 55;
            this.label4.Text = "ĐVT";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "Số lượng đã sử dụng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 13);
            this.label6.TabIndex = 53;
            this.label6.Text = "Số lượng đăng ký miễn thuế";
            // 
            // txtMoTaHangHoa
            // 
            this.txtMoTaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoTaHangHoa.Location = new System.Drawing.Point(165, 17);
            this.txtMoTaHangHoa.MaxLength = 100;
            this.txtMoTaHangHoa.Multiline = true;
            this.txtMoTaHangHoa.Name = "txtMoTaHangHoa";
            this.txtMoTaHangHoa.Size = new System.Drawing.Size(539, 40);
            this.txtMoTaHangHoa.TabIndex = 0;
            this.txtMoTaHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMoTaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMoTaHangHoa.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(444, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "Trị giá dự kiến";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(444, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "Trị giá";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Mô tả hàng hóa";
            // 
            // grListHang
            // 
            this.grListHang.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grListHang_DesignTimeLayout.LayoutString = resources.GetString("grListHang_DesignTimeLayout.LayoutString");
            this.grListHang.DesignTimeLayout = grListHang_DesignTimeLayout;
            this.grListHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListHang.GroupByBoxVisible = false;
            this.grListHang.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListHang.Location = new System.Drawing.Point(3, 17);
            this.grListHang.Name = "grListHang";
            this.grListHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHang.Size = new System.Drawing.Size(710, 242);
            this.grListHang.TabIndex = 1;
            this.grListHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListHang.VisualStyleManager = this.vsmMain;
            this.grListHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListHang_RowDoubleClick);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.grListHang);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 159);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(716, 262);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Danh sách hàng hóa";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // VNACC_TEA_HangForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(716, 421);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_TEA_HangForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "VNACC_TEA_HangForm";
            this.Load += new System.EventHandler(this.VNACC_TEA_HangForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UIGroup)).EndInit();
            this.UIGroup.ResumeLayout(false);
            this.UIGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grListHang;
        private Janus.Windows.EditControls.UIGroupBox UIGroup;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMoTaHangHoa;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrSoLuongDaSuDung;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrSoLuongDangKyMT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongDaSuDung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongDangKyMT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaDuKien;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGia;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}