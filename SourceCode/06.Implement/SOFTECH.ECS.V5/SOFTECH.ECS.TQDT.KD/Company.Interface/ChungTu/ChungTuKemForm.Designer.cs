﻿namespace Company.Interface
{
    partial class ChungTuKemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChungTuKemForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbLoaiCT = new Janus.Windows.EditControls.UIComboBox();
            this.txtThongTinKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayChungTu = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvNgayGiayPhep = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoGiayPhep = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvTenDVdcCap = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.btnXemFile = new Janus.Windows.EditControls.UIButton();
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.btnKetQuaXuLy = new Janus.Windows.EditControls.UIButton();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lbTrangThai = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnKhaiBao = new Janus.Windows.EditControls.UIButton();
            this.btnLayPhanHoi = new Janus.Windows.EditControls.UIButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnXoaChungTu = new Janus.Windows.EditControls.UIButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblLuuY = new DevExpress.XtraEditors.LabelControl();
            this.lblTongDungLuong = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayGiayPhep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoGiayPhep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVdcCap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.lblTongDungLuong);
            this.grbMain.Controls.Add(this.lblLuuY);
            this.grbMain.Controls.Add(this.labelControl2);
            this.grbMain.Controls.Add(this.labelControl1);
            this.grbMain.Controls.Add(this.btnKhaiBao);
            this.grbMain.Controls.Add(this.btnLayPhanHoi);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Controls.Add(this.btnXemFile);
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnXoaChungTu);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.btnAddNew);
            this.grbMain.Size = new System.Drawing.Size(711, 422);
            this.grbMain.Click += new System.EventHandler(this.grbMain_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cbLoaiCT);
            this.uiGroupBox2.Controls.Add(this.txtThongTinKhac);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.txtSoChungTu);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayChungTu);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(9, 57);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(689, 170);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // cbLoaiCT
            // 
            this.cbLoaiCT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Giấy đăng ký kiểm tra";
            uiComboBoxItem4.Value = "200";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Giấy kêt quả kiểm tra,miền kiểm tra";
            uiComboBoxItem5.Value = "201";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Giấy nộp tiền";
            uiComboBoxItem6.Value = "202";
            this.cbLoaiCT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbLoaiCT.Location = new System.Drawing.Point(114, 55);
            this.cbLoaiCT.Name = "cbLoaiCT";
            this.cbLoaiCT.Size = new System.Drawing.Size(539, 21);
            this.cbLoaiCT.TabIndex = 2;
            this.cbLoaiCT.VisualStyleManager = this.vsmMain;
            // 
            // txtThongTinKhac
            // 
            this.txtThongTinKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThongTinKhac.Location = new System.Drawing.Point(114, 82);
            this.txtThongTinKhac.MaxLength = 255;
            this.txtThongTinKhac.Multiline = true;
            this.txtThongTinKhac.Name = "txtThongTinKhac";
            this.txtThongTinKhac.Size = new System.Drawing.Size(539, 69);
            this.txtThongTinKhac.TabIndex = 3;
            this.txtThongTinKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThongTinKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtThongTinKhac.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Loại chứng từ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Thông tin khác";
            // 
            // txtSoChungTu
            // 
            this.txtSoChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoChungTu.Location = new System.Drawing.Point(114, 20);
            this.txtSoChungTu.MaxLength = 255;
            this.txtSoChungTu.Name = "txtSoChungTu";
            this.txtSoChungTu.Size = new System.Drawing.Size(136, 21);
            this.txtSoChungTu.TabIndex = 0;
            this.txtSoChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoChungTu.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(282, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày chứng từ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(12, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(66, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số chứng từ";
            // 
            // ccNgayChungTu
            // 
            // 
            // 
            // 
            this.ccNgayChungTu.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayChungTu.DropDownCalendar.Name = "";
            this.ccNgayChungTu.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayChungTu.IsNullDate = true;
            this.ccNgayChungTu.Location = new System.Drawing.Point(367, 21);
            this.ccNgayChungTu.Name = "ccNgayChungTu";
            this.ccNgayChungTu.Nullable = true;
            this.ccNgayChungTu.NullButtonText = "Xóa";
            this.ccNgayChungTu.ShowNullButton = true;
            this.ccNgayChungTu.Size = new System.Drawing.Size(90, 21);
            this.ccNgayChungTu.TabIndex = 1;
            this.ccNgayChungTu.TodayButtonText = "Hôm nay";
            this.ccNgayChungTu.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayChungTu.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.AutoEdit = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(9, 233);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(690, 108);
            this.dgList.TabIndex = 2;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(518, 356);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(88, 23);
            this.btnXoa.TabIndex = 4;
            this.btnXoa.Text = "Xoá file";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(624, 394);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddNew.Icon")));
            this.btnAddNew.Location = new System.Drawing.Point(415, 356);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(97, 23);
            this.btnAddNew.TabIndex = 3;
            this.btnAddNew.Text = "Thêm file";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddNew.VisualStyleManager = this.vsmMain;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(548, 394);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(69, 23);
            this.btnGhi.TabIndex = 9;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvNgayGiayPhep
            // 
            this.rfvNgayGiayPhep.ControlToValidate = this.ccNgayChungTu;
            this.rfvNgayGiayPhep.ErrorMessage = "\"Ngày giấy phép\" không được bỏ trống.";
            this.rfvNgayGiayPhep.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayGiayPhep.Icon")));
            this.rfvNgayGiayPhep.Tag = "rfvNgayGiayPhep";
            // 
            // rfvSoGiayPhep
            // 
            this.rfvSoGiayPhep.ControlToValidate = this.txtSoChungTu;
            this.rfvSoGiayPhep.ErrorMessage = "\"Số giấy phép\" không được để trống.";
            this.rfvSoGiayPhep.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoGiayPhep.Icon")));
            this.rfvSoGiayPhep.Tag = "rfvSoGiayPhep";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvTenDVdcCap
            // 
            this.rfvTenDVdcCap.ControlToValidate = this.cbLoaiCT;
            this.rfvTenDVdcCap.ErrorMessage = "\"Tên đơn vị được cấp\" không được bỏ trống.";
            this.rfvTenDVdcCap.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDVdcCap.Icon")));
            this.rfvTenDVdcCap.Tag = "rfvTenDVdcCap";
            // 
            // btnXemFile
            // 
            this.btnXemFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXemFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemFile.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemFile.Icon")));
            this.btnXemFile.Location = new System.Drawing.Point(612, 356);
            this.btnXemFile.Name = "btnXemFile";
            this.btnXemFile.Size = new System.Drawing.Size(86, 23);
            this.btnXemFile.TabIndex = 5;
            this.btnXemFile.Text = "Xem file";
            this.btnXemFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemFile.VisualStyleManager = this.vsmMain;
            this.btnXemFile.Click += new System.EventHandler(this.btnChonGP_Click);
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXuLy);
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.lbTrangThai);
            this.grpTiepNhan.Controls.Add(this.label10);
            this.grpTiepNhan.Controls.Add(this.label11);
            this.grpTiepNhan.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(0, 0);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(711, 51);
            this.grpTiepNhan.TabIndex = 0;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grpTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // btnKetQuaXuLy
            // 
            this.btnKetQuaXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKetQuaXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXuLy.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKetQuaXuLy.Icon")));
            this.btnKetQuaXuLy.Location = new System.Drawing.Point(569, 15);
            this.btnKetQuaXuLy.Name = "btnKetQuaXuLy";
            this.btnKetQuaXuLy.Size = new System.Drawing.Size(109, 23);
            this.btnKetQuaXuLy.TabIndex = 2;
            this.btnKetQuaXuLy.Text = "Kết quả xử lý";
            this.btnKetQuaXuLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKetQuaXuLy.Click += new System.EventHandler(this.btnKetQuaXuLy_Click);
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(333, 15);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(94, 21);
            this.ccNgayTiepNhan.TabIndex = 1;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(121, 15);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(112, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // lbTrangThai
            // 
            this.lbTrangThai.AutoSize = true;
            this.lbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTrangThai.Location = new System.Drawing.Point(433, 20);
            this.lbTrangThai.Name = "lbTrangThai";
            this.lbTrangThai.Size = new System.Drawing.Size(56, 13);
            this.lbTrangThai.TabIndex = 2;
            this.lbTrangThai.Text = "Trạng thái";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(254, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Ngày tiếp nhận";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số tiếp nhận";
            // 
            // btnKhaiBao
            // 
            this.btnKhaiBao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKhaiBao.Icon")));
            this.btnKhaiBao.Location = new System.Drawing.Point(9, 394);
            this.btnKhaiBao.Name = "btnKhaiBao";
            this.btnKhaiBao.Size = new System.Drawing.Size(109, 23);
            this.btnKhaiBao.TabIndex = 6;
            this.btnKhaiBao.Text = "Khai  báo";
            this.btnKhaiBao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKhaiBao.Click += new System.EventHandler(this.btnKhaiBao_Click);
            // 
            // btnLayPhanHoi
            // 
            this.btnLayPhanHoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLayPhanHoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLayPhanHoi.Icon")));
            this.btnLayPhanHoi.Location = new System.Drawing.Point(124, 394);
            this.btnLayPhanHoi.Name = "btnLayPhanHoi";
            this.btnLayPhanHoi.Size = new System.Drawing.Size(109, 23);
            this.btnLayPhanHoi.TabIndex = 7;
            this.btnLayPhanHoi.Text = "Lấy phản hồi";
            this.btnLayPhanHoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLayPhanHoi.Click += new System.EventHandler(this.btnLayPhanHoi_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnXoaChungTu
            // 
            this.btnXoaChungTu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoaChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaChungTu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoaChungTu.Icon")));
            this.btnXoaChungTu.Location = new System.Drawing.Point(433, 394);
            this.btnXoaChungTu.Name = "btnXoaChungTu";
            this.btnXoaChungTu.Size = new System.Drawing.Size(109, 23);
            this.btnXoaChungTu.TabIndex = 8;
            this.btnXoaChungTu.Text = "Xoá chứng từ";
            this.btnXoaChungTu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoaChungTu.Click += new System.EventHandler(this.btnXoaChungTu_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(9, 347);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(97, 13);
            this.labelControl1.TabIndex = 27;
            this.labelControl1.Text = "Tổng dung lượng:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(9, 366);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(293, 13);
            this.labelControl2.TabIndex = 28;
            this.labelControl2.Text = "Lưu ý: Tổng dung lượng các file đính kèm không quá ";
            // 
            // lblLuuY
            // 
            this.lblLuuY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLuuY.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblLuuY.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblLuuY.Appearance.Options.UseFont = true;
            this.lblLuuY.Appearance.Options.UseForeColor = true;
            this.lblLuuY.Location = new System.Drawing.Point(307, 366);
            this.lblLuuY.Name = "lblLuuY";
            this.lblLuuY.Size = new System.Drawing.Size(27, 13);
            this.lblLuuY.TabIndex = 29;
            this.lblLuuY.Text = "2 MB";
            // 
            // lblTongDungLuong
            // 
            this.lblTongDungLuong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTongDungLuong.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblTongDungLuong.Appearance.Options.UseForeColor = true;
            this.lblTongDungLuong.Location = new System.Drawing.Point(121, 347);
            this.lblTongDungLuong.Name = "lblTongDungLuong";
            this.lblTongDungLuong.Size = new System.Drawing.Size(70, 13);
            this.lblTongDungLuong.TabIndex = 29;
            this.lblTongDungLuong.Text = "0 MB (0 Bytes)";
            // 
            // ChungTuKemForm
            // 
            this.AcceptButton = this.btnGhi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(711, 422);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ChungTuKemForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin chứng từ kèm";
            this.Load += new System.EventHandler(this.GiayPhepForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayGiayPhep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoGiayPhep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVdcCap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTu;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayChungTu;
        private Janus.Windows.GridEX.EditControls.EditBox txtThongTinKhac;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnAddNew;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayGiayPhep;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoGiayPhep;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDVdcCap;
        private Janus.Windows.EditControls.UIButton btnXemFile;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label lbTrangThai;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIButton btnKhaiBao;
        private Janus.Windows.EditControls.UIButton btnLayPhanHoi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Janus.Windows.EditControls.UIButton btnXoaChungTu;
        private Janus.Windows.EditControls.UIComboBox cbLoaiCT;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblTongDungLuong;
        private DevExpress.XtraEditors.LabelControl lblLuuY;
        private Janus.Windows.EditControls.UIButton btnKetQuaXuLy;
    }
}
