using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class GiamSatTieuHuy
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HopDong_ID { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string MaHaiQuan { set; get; }
		public string GUIDSTR { set; get; }
		public string SoGiayPhep { set; get; }
		public DateTime NgayGiayPhep { set; get; }
		public DateTime NgayHetHan { set; get; }
		public string ToChucCap { set; get; }
		public string CacBenThamGia { set; get; }
		public DateTime ThoiGianTieuHuy { set; get; }
		public string DiaDiemTieuHuy { set; get; }
		public string GhiChuKhac { set; get; }
		public int TrangThaiXuLy { set; get; }
		public int ActionStatus { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static GiamSatTieuHuy Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			GiamSatTieuHuy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new GiamSatTieuHuy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("CacBenThamGia"))) entity.CacBenThamGia = reader.GetString(reader.GetOrdinal("CacBenThamGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianTieuHuy"))) entity.ThoiGianTieuHuy = reader.GetDateTime(reader.GetOrdinal("ThoiGianTieuHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemTieuHuy"))) entity.DiaDiemTieuHuy = reader.GetString(reader.GetOrdinal("DiaDiemTieuHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuKhac"))) entity.GhiChuKhac = reader.GetString(reader.GetOrdinal("GhiChuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<GiamSatTieuHuy> SelectCollectionAll()
		{
			List<GiamSatTieuHuy> collection = new List<GiamSatTieuHuy>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				GiamSatTieuHuy entity = new GiamSatTieuHuy();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("CacBenThamGia"))) entity.CacBenThamGia = reader.GetString(reader.GetOrdinal("CacBenThamGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianTieuHuy"))) entity.ThoiGianTieuHuy = reader.GetDateTime(reader.GetOrdinal("ThoiGianTieuHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemTieuHuy"))) entity.DiaDiemTieuHuy = reader.GetString(reader.GetOrdinal("DiaDiemTieuHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuKhac"))) entity.GhiChuKhac = reader.GetString(reader.GetOrdinal("GhiChuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<GiamSatTieuHuy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<GiamSatTieuHuy> collection = new List<GiamSatTieuHuy>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				GiamSatTieuHuy entity = new GiamSatTieuHuy();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("CacBenThamGia"))) entity.CacBenThamGia = reader.GetString(reader.GetOrdinal("CacBenThamGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianTieuHuy"))) entity.ThoiGianTieuHuy = reader.GetDateTime(reader.GetOrdinal("ThoiGianTieuHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemTieuHuy"))) entity.DiaDiemTieuHuy = reader.GetString(reader.GetOrdinal("DiaDiemTieuHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuKhac"))) entity.GhiChuKhac = reader.GetString(reader.GetOrdinal("GhiChuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertGiamSatTieuHuy(long hopDong_ID, long soTiepNhan, DateTime ngayTiepNhan, string maDoanhNghiep, string maHaiQuan, string gUIDSTR, string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHan, string toChucCap, string cacBenThamGia, DateTime thoiGianTieuHuy, string diaDiemTieuHuy, string ghiChuKhac, int trangThaiXuLy, int actionStatus)
		{
			GiamSatTieuHuy entity = new GiamSatTieuHuy();	
			entity.HopDong_ID = hopDong_ID;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaHaiQuan = maHaiQuan;
			entity.GUIDSTR = gUIDSTR;
			entity.SoGiayPhep = soGiayPhep;
			entity.NgayGiayPhep = ngayGiayPhep;
			entity.NgayHetHan = ngayHetHan;
			entity.ToChucCap = toChucCap;
			entity.CacBenThamGia = cacBenThamGia;
			entity.ThoiGianTieuHuy = thoiGianTieuHuy;
			entity.DiaDiemTieuHuy = diaDiemTieuHuy;
			entity.GhiChuKhac = ghiChuKhac;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.ActionStatus = actionStatus;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, SoGiayPhep);
			db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year <= 1753 ? DBNull.Value : (object) NgayGiayPhep);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
			db.AddInParameter(dbCommand, "@CacBenThamGia", SqlDbType.NVarChar, CacBenThamGia);
			db.AddInParameter(dbCommand, "@ThoiGianTieuHuy", SqlDbType.DateTime, ThoiGianTieuHuy.Year <= 1753 ? DBNull.Value : (object) ThoiGianTieuHuy);
			db.AddInParameter(dbCommand, "@DiaDiemTieuHuy", SqlDbType.NVarChar, DiaDiemTieuHuy);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<GiamSatTieuHuy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiamSatTieuHuy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateGiamSatTieuHuy(long id, long hopDong_ID, long soTiepNhan, DateTime ngayTiepNhan, string maDoanhNghiep, string maHaiQuan, string gUIDSTR, string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHan, string toChucCap, string cacBenThamGia, DateTime thoiGianTieuHuy, string diaDiemTieuHuy, string ghiChuKhac, int trangThaiXuLy, int actionStatus)
		{
			GiamSatTieuHuy entity = new GiamSatTieuHuy();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaHaiQuan = maHaiQuan;
			entity.GUIDSTR = gUIDSTR;
			entity.SoGiayPhep = soGiayPhep;
			entity.NgayGiayPhep = ngayGiayPhep;
			entity.NgayHetHan = ngayHetHan;
			entity.ToChucCap = toChucCap;
			entity.CacBenThamGia = cacBenThamGia;
			entity.ThoiGianTieuHuy = thoiGianTieuHuy;
			entity.DiaDiemTieuHuy = diaDiemTieuHuy;
			entity.GhiChuKhac = ghiChuKhac;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.ActionStatus = actionStatus;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_GiamSatTieuHuy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, SoGiayPhep);
			db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year == 1753 ? DBNull.Value : (object) NgayGiayPhep);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
			db.AddInParameter(dbCommand, "@CacBenThamGia", SqlDbType.NVarChar, CacBenThamGia);
			db.AddInParameter(dbCommand, "@ThoiGianTieuHuy", SqlDbType.DateTime, ThoiGianTieuHuy.Year == 1753 ? DBNull.Value : (object) ThoiGianTieuHuy);
			db.AddInParameter(dbCommand, "@DiaDiemTieuHuy", SqlDbType.NVarChar, DiaDiemTieuHuy);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<GiamSatTieuHuy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiamSatTieuHuy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateGiamSatTieuHuy(long id, long hopDong_ID, long soTiepNhan, DateTime ngayTiepNhan, string maDoanhNghiep, string maHaiQuan, string gUIDSTR, string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHan, string toChucCap, string cacBenThamGia, DateTime thoiGianTieuHuy, string diaDiemTieuHuy, string ghiChuKhac, int trangThaiXuLy, int actionStatus)
		{
			GiamSatTieuHuy entity = new GiamSatTieuHuy();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaHaiQuan = maHaiQuan;
			entity.GUIDSTR = gUIDSTR;
			entity.SoGiayPhep = soGiayPhep;
			entity.NgayGiayPhep = ngayGiayPhep;
			entity.NgayHetHan = ngayHetHan;
			entity.ToChucCap = toChucCap;
			entity.CacBenThamGia = cacBenThamGia;
			entity.ThoiGianTieuHuy = thoiGianTieuHuy;
			entity.DiaDiemTieuHuy = diaDiemTieuHuy;
			entity.GhiChuKhac = ghiChuKhac;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.ActionStatus = actionStatus;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, SoGiayPhep);
			db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year == 1753 ? DBNull.Value : (object) NgayGiayPhep);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
			db.AddInParameter(dbCommand, "@CacBenThamGia", SqlDbType.NVarChar, CacBenThamGia);
			db.AddInParameter(dbCommand, "@ThoiGianTieuHuy", SqlDbType.DateTime, ThoiGianTieuHuy.Year == 1753 ? DBNull.Value : (object) ThoiGianTieuHuy);
			db.AddInParameter(dbCommand, "@DiaDiemTieuHuy", SqlDbType.NVarChar, DiaDiemTieuHuy);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<GiamSatTieuHuy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiamSatTieuHuy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteGiamSatTieuHuy(long id)
		{
			GiamSatTieuHuy entity = new GiamSatTieuHuy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<GiamSatTieuHuy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiamSatTieuHuy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}