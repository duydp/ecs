﻿/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 09/05/2012 1:52:02 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[t_SXXK_HangMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_HangMauDich] DROP
CONSTRAINT [FK_t_SXXK_HangMauDich_t_SXXK_ToKhaiMauDich]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[t_SXXK_HangMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_HangMauDich] DROP CONSTRAINT [PK_t_SXXK_HangMauDich_1]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_SXXK_HangMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_HangMauDich] ALTER COLUMN [SoLuong] [numeric] (18, 5) NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_SXXK_HangMauDich] on [dbo].[t_SXXK_HangMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_HangMauDich] ADD CONSTRAINT [PK_t_SXXK_HangMauDich] PRIMARY KEY CLUSTERED  ([SoToKhai], [MaLoaiHinh], [MaHaiQuan], [NamDangKy], [SoThuTuHang], [MaPhu])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_HangMauDich_InsertUpdate]'
GO
  
      ------------------------------------------------------------------------------------------------------------------------          
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_InsertUpdate]          
-- Database: HaiQuan          
-- Author: Ngo Thanh Tung          
-- Time created: Tuesday, October 28, 2008          
------------------------------------------------------------------------------------------------------------------------          
          
ALTER PROCEDURE [dbo].[p_SXXK_HangMauDich_InsertUpdate]      
    @SoToKhai INT ,      
    @MaLoaiHinh CHAR(5) ,      
    @MaHaiQuan CHAR(6) ,      
    @NamDangKy SMALLINT ,      
    @SoThuTuHang SMALLINT ,      
    @MaHS VARCHAR(12) ,      
    @MaPhu VARCHAR(30) ,      
    @TenHang NVARCHAR(80) ,      
    @NuocXX_ID CHAR(3) ,      
    @DVT_ID CHAR(3) ,      
    @SoLuong NUMERIC(18, 5) ,      
    @DonGiaKB NUMERIC(38, 15) ,      
    @DonGiaTT NUMERIC(38, 15) ,      
    @TriGiaKB NUMERIC(38, 15) ,      
    @TriGiaTT NUMERIC(38, 15) ,      
    @TriGiaKB_VND NUMERIC(38, 15) ,      
    @ThueSuatXNK NUMERIC(5, 2) ,      
    @ThueSuatTTDB NUMERIC(5, 2) ,      
    @ThueSuatGTGT NUMERIC(5, 2) ,      
    @ThueXNK MONEY ,      
    @ThueTTDB MONEY ,      
    @ThueGTGT MONEY ,      
    @PhuThu MONEY ,      
    @TyLeThuKhac NUMERIC(5, 2) ,      
    @TriGiaThuKhac MONEY ,      
    @MienThue TINYINT      
AS       
    IF EXISTS ( SELECT  [SoToKhai] ,      
                        [MaLoaiHinh] ,      
                        [MaHaiQuan] ,      
                        [NamDangKy] ,      
                        [SoThuTuHang]      
                FROM    [dbo].[t_SXXK_HangMauDich]      
                WHERE   [SoToKhai] = @SoToKhai      
                        AND [MaLoaiHinh] = @MaLoaiHinh      
                        AND [MaHaiQuan] = @MaHaiQuan      
                        AND [NamDangKy] = @NamDangKy      
                        AND MaPhu = @MaPhu )       
        BEGIN          
            UPDATE  [dbo].[t_SXXK_HangMauDich]      
            SET     [MaHS] = @MaHS ,      
                    [MaPhu] = @MaPhu ,      
                    [TenHang] = @TenHang ,      
                    [NuocXX_ID] = @NuocXX_ID ,      
                    [DVT_ID] = @DVT_ID ,      
                    [SoLuong] = @SoLuong ,      
                    [DonGiaKB] = @DonGiaKB ,      
                    [DonGiaTT] = @DonGiaTT ,      
                    [TriGiaKB] = @TriGiaKB ,      
                    [TriGiaTT] = @TriGiaTT ,      
                    [TriGiaKB_VND] = @TriGiaKB_VND ,      
                    [ThueSuatXNK] = @ThueSuatXNK ,      
                    [ThueSuatTTDB] = @ThueSuatTTDB ,      
                    [ThueSuatGTGT] = @ThueSuatGTGT ,      
                    [ThueXNK] = @ThueXNK ,      
                    [ThueTTDB] = @ThueTTDB ,      
                    [ThueGTGT] = @ThueGTGT ,      
                    [PhuThu] = @PhuThu ,      
                    [TyLeThuKhac] = @TyLeThuKhac ,      
                    [TriGiaThuKhac] = @TriGiaThuKhac ,      
                    [MienThue] = @MienThue      
            WHERE   [SoToKhai] = @SoToKhai      
                    AND [MaLoaiHinh] = @MaLoaiHinh      
                    AND [MaHaiQuan] = @MaHaiQuan      
                    AND [NamDangKy] = @NamDangKy      
                    AND MaPhu = @MaPhu      
        END          
    ELSE       
        BEGIN          
            INSERT  INTO [dbo].[t_SXXK_HangMauDich]      
                    ( [SoToKhai] ,      
                      [MaLoaiHinh] ,      
                      [MaHaiQuan] ,      
                      [NamDangKy] ,      
                      [SoThuTuHang] ,      
                      [MaHS] ,      
                      [MaPhu] ,      
                      [TenHang] ,      
                      [NuocXX_ID] ,      
                      [DVT_ID] ,      
                      [SoLuong] ,      
                      [DonGiaKB] ,      
                      [DonGiaTT] ,      
                      [TriGiaKB] ,      
                   [TriGiaTT] ,      
                      [TriGiaKB_VND] ,      
                      [ThueSuatXNK] ,      
                      [ThueSuatTTDB] ,      
     [ThueSuatGTGT] ,      
                      [ThueXNK] ,      
                      [ThueTTDB] ,      
                      [ThueGTGT] ,      
                      [PhuThu] ,      
                      [TyLeThuKhac] ,      
                      [TriGiaThuKhac] ,      
                      [MienThue]          
                    )      
            VALUES  ( @SoToKhai ,      
                      @MaLoaiHinh ,      
                      @MaHaiQuan ,      
                      @NamDangKy ,      
                      @SoThuTuHang ,      
                      @MaHS ,      
                      @MaPhu ,      
                      @TenHang ,      
                      @NuocXX_ID ,      
                      @DVT_ID ,      
                      @SoLuong ,      
                      @DonGiaKB ,      
                      @DonGiaTT ,      
                      @TriGiaKB ,      
                      @TriGiaTT ,      
                      @TriGiaKB_VND ,      
                      @ThueSuatXNK ,      
                      @ThueSuatTTDB ,      
                      @ThueSuatGTGT ,      
                      @ThueXNK ,      
                      @ThueTTDB ,      
                      @ThueGTGT ,      
                      @PhuThu ,      
                      @TyLeThuKhac ,      
                      @TriGiaThuKhac ,      
                      @MienThue          
                    )           
        END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_HangMauDich_Insert]'
GO

------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_Insert]    
-- Database: HaiQuan    
-- Author: Ngo Thanh Tung    
-- Time created: Tuesday, October 28, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_SXXK_HangMauDich_Insert]    
 @SoToKhai int,    
 @MaLoaiHinh char(5),    
 @MaHaiQuan char(6),    
 @NamDangKy smallint,    
 @SoThuTuHang smallint,    
 @MaHS varchar(12),    
 @MaPhu varchar(30),    
 @TenHang nvarchar(80),    
 @NuocXX_ID char(3),    
 @DVT_ID char(3),    
 @SoLuong numeric(18, 5),    
 @DonGiaKB numeric(38, 15),    
 @DonGiaTT numeric(38, 15),    
 @TriGiaKB numeric(38, 15),    
 @TriGiaTT numeric(38, 15),    
 @TriGiaKB_VND numeric(38, 15),    
 @ThueSuatXNK numeric(5, 2),    
 @ThueSuatTTDB numeric(5, 2),    
 @ThueSuatGTGT numeric(5, 2),    
 @ThueXNK money,    
 @ThueTTDB money,    
 @ThueGTGT money,    
 @PhuThu money,    
 @TyLeThuKhac numeric(5, 2),    
 @TriGiaThuKhac money,    
 @MienThue tinyint    
AS    
INSERT INTO [dbo].[t_SXXK_HangMauDich]    
(    
 [SoToKhai],    
 [MaLoaiHinh],    
 [MaHaiQuan],    
 [NamDangKy],    
 [SoThuTuHang],    
 [MaHS],    
 [MaPhu],    
 [TenHang],    
 [NuocXX_ID],    
 [DVT_ID],    
 [SoLuong],    
 [DonGiaKB],    
 [DonGiaTT],    
 [TriGiaKB],    
 [TriGiaTT],    
 [TriGiaKB_VND],    
 [ThueSuatXNK],    
 [ThueSuatTTDB],    
 [ThueSuatGTGT],    
 [ThueXNK],    
 [ThueTTDB],    
 [ThueGTGT],    
 [PhuThu],    
 [TyLeThuKhac],    
 [TriGiaThuKhac],    
 [MienThue]    
)    
VALUES    
(    
 @SoToKhai,    
 @MaLoaiHinh,    
 @MaHaiQuan,    
 @NamDangKy,    
 @SoThuTuHang,    
 @MaHS,    
 @MaPhu,    
 @TenHang,    
 @NuocXX_ID,    
 @DVT_ID,    
 @SoLuong,    
 @DonGiaKB,    
 @DonGiaTT,    
 @TriGiaKB,    
 @TriGiaTT,    
 @TriGiaKB_VND,    
 @ThueSuatXNK,    
 @ThueSuatTTDB,    
 @ThueSuatGTGT,    
 @ThueXNK,    
 @ThueTTDB,    
 @ThueGTGT,    
 @PhuThu,    
 @TyLeThuKhac,    
 @TriGiaThuKhac,    
 @MienThue    
)    
    
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_HangMauDich_Update]'
GO

------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_Update]    
-- Database: HaiQuan    
-- Author: Ngo Thanh Tung    
-- Time created: Tuesday, October 28, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_SXXK_HangMauDich_Update]    
 @SoToKhai int,    
 @MaLoaiHinh char(5),    
 @MaHaiQuan char(6),    
 @NamDangKy smallint,    
 @SoThuTuHang smallint,    
 @MaHS varchar(12),    
 @MaPhu varchar(30),    
 @TenHang nvarchar(80),    
 @NuocXX_ID char(3),    
 @DVT_ID char(3),    
 @SoLuong numeric(18, 5),    
 @DonGiaKB numeric(38, 15),    
 @DonGiaTT numeric(38, 15),    
 @TriGiaKB numeric(38, 15),    
 @TriGiaTT numeric(38, 15),    
 @TriGiaKB_VND numeric(38, 15),    
 @ThueSuatXNK numeric(5, 2),    
 @ThueSuatTTDB numeric(5, 2),    
 @ThueSuatGTGT numeric(5, 2),    
 @ThueXNK money,    
 @ThueTTDB money,    
 @ThueGTGT money,    
 @PhuThu money,    
 @TyLeThuKhac numeric(5, 2),    
 @TriGiaThuKhac money,    
 @MienThue tinyint    
AS    
UPDATE    
 [dbo].[t_SXXK_HangMauDich]    
SET    
 [MaHS] = @MaHS,    
 [MaPhu] = @MaPhu,    
 [TenHang] = @TenHang,    
 [NuocXX_ID] = @NuocXX_ID,    
 [DVT_ID] = @DVT_ID,    
 [SoLuong] = @SoLuong,    
 [DonGiaKB] = @DonGiaKB,    
 [DonGiaTT] = @DonGiaTT,    
 [TriGiaKB] = @TriGiaKB,    
 [TriGiaTT] = @TriGiaTT,    
 [TriGiaKB_VND] = @TriGiaKB_VND,    
 [ThueSuatXNK] = @ThueSuatXNK,    
 [ThueSuatTTDB] = @ThueSuatTTDB,    
 [ThueSuatGTGT] = @ThueSuatGTGT,    
 [ThueXNK] = @ThueXNK,    
 [ThueTTDB] = @ThueTTDB,    
 [ThueGTGT] = @ThueGTGT,    
 [PhuThu] = @PhuThu,    
 [TyLeThuKhac] = @TyLeThuKhac,    
 [TriGiaThuKhac] = @TriGiaThuKhac,    
 [MienThue] = @MienThue    
WHERE    
 [SoToKhai] = @SoToKhai    
 AND [MaLoaiHinh] = @MaLoaiHinh    
 AND [MaHaiQuan] = @MaHaiQuan    
 AND [NamDangKy] = @NamDangKy    
 AND [SoThuTuHang] = @SoThuTuHang    
    
    
      
    
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_HangTon]'
GO
EXEC sp_refreshview N'[dbo].[v_HangTon]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_SXXK_NPLNhapTon]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SXXK_NPLNhapTon]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_SXXK_NPLXuatTon]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SXXK_NPLXuatTon]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_HangMauDich]'
GO
EXEC sp_refreshview N'[dbo].[t_View_HangMauDich]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_LayThongTin_TKSua_ChuaCapNhat]'
GO

ALTER PROC p_LayThongTin_TKSua_ChuaCapNhat  
(  
 @MaHaiQuan VARCHAR(10),  
 @MaDoanhNghiep VARCHAR(30)  
)  
AS  
/*  
Hungtq created 24/08/2012.  
Lay danh sach to khai sua da duyet chua cap nhat thong tin moi so voi to khai chinh.  
*/  
begin  
  
SELECT MaHaiQuan, SoToKhai, MaLoaiHinh, NamDangKy   
FROM   
(  
 select b.MaHaiQuan, b.SoToKhai, b.MaLoaiHinh, b.NamDangKy, b.MaPhu, b.SoLuong AS SoLuongMoi, a.SoLuong AS SoLuongCu, a.SoThuTuHang from   
 (  
  select t.SoToKhai, t.MaLoaiHinh, year(t.NgayDangKy) as NamDangKy, h.MaPhu, t.MaHaiQuan, t.MaDoanhNghiep, h.SoLuong, h.SoThuTuHang  
  from t_KDT_HangMauDich h   
   inner join t_KDT_ToKhaiMauDich t on h.TKMD_ID = t.ID  
  WHERE t.ID IN (SELECT ItemID FROM dbo.t_KDT_Messages WHERE (TieuDeThongBao LIKE N'Tờ khai sửa được duyệt' OR TieuDeThongBao LIKE N'Tờ khai được cấp số') GROUP BY ItemID)   
   AND t.TrangThaiXuLy = 1  
 ) as a   
 full join  
 (  
  select h.SoToKhai, h.MaLoaiHinh, h.NamDangKy, h.MaPhu, h.MaHaiQuan, h.SoLuong, h.SoThuTuHang  
  from t_sxxk_HangMauDich h   
 ) as b  
 on a.SoToKhai = b.SoToKhai and a.MaLoaiHinh = b.MaLoaiHinh and a.NamDangKy = b.NamDangKy  
  and a.MaHaiQuan = b.MaHaiQuan and a.MaPhu = b.MaPhu AND a.SoThuTuHang = b.SoThuTuHang  
 where a.SoLuong <> b.SoLuong  
 AND a.MaHaiQuan = @MaHaiQuan AND a.MaDoanhNghiep = @MaDoanhNghiep  
) AS v  
GROUP BY MaHaiQuan, SoToKhai, MaLoaiHinh, NamDangKy  
  
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_SXXK_HangMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_HangMauDich] ADD
CONSTRAINT [FK_t_SXXK_HangMauDich_t_SXXK_ToKhaiMauDich] FOREIGN KEY ([MaHaiQuan], [SoToKhai], [MaLoaiHinh], [NamDangKy]) REFERENCES [dbo].[t_SXXK_ToKhaiMauDich] ([MaHaiQuan], [SoToKhai], [MaLoaiHinh], [NamDangKy]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='4.3', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('4.3', getdate(), null)
	end 