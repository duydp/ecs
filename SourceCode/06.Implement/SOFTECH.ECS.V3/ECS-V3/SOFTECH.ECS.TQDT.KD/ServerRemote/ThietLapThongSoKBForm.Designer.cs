﻿namespace Company.Interface
{
    partial class ThietLapThongSoKBForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThietLapThongSoKBForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageKetNoi = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnChange = new Janus.Windows.EditControls.UIButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbDatabaseSource = new Janus.Windows.EditControls.UIComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtServerName = new Janus.Windows.EditControls.UIComboBox();
            this.txtPass = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiTabPageTaoCSDL = new Janus.Windows.UI.Tab.UITabPage();
            this.btnTaoCSDL = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPageKetNoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.uiTabPageTaoCSDL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Margin = new System.Windows.Forms.Padding(4);
            this.grbMain.Size = new System.Drawing.Size(725, 317);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiTab1);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(725, 317);
            this.uiGroupBox1.TabIndex = 0;
            // 
            // uiTab1
            // 
            this.uiTab1.BackColor = System.Drawing.Color.Transparent;
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiTab1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Margin = new System.Windows.Forms.Padding(0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(725, 317);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageKetNoi,
            this.uiTabPageTaoCSDL});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPageKetNoi
            // 
            this.uiTabPageKetNoi.Controls.Add(this.uiGroupBox4);
            this.uiTabPageKetNoi.Location = new System.Drawing.Point(1, 24);
            this.uiTabPageKetNoi.Margin = new System.Windows.Forms.Padding(4);
            this.uiTabPageKetNoi.Name = "uiTabPageKetNoi";
            this.uiTabPageKetNoi.Size = new System.Drawing.Size(723, 292);
            this.uiTabPageKetNoi.TabStop = true;
            this.uiTabPageKetNoi.Text = "Thiết lập thông số kết nối cơ sở dữ liệu";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnChange);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.cbDatabaseSource);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.txtServerName);
            this.uiGroupBox4.Controls.Add(this.txtPass);
            this.uiGroupBox4.Controls.Add(this.txtSa);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(723, 292);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // btnChange
            // 
            this.btnChange.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChange.Image = ((System.Drawing.Image)(resources.GetObject("btnChange.Image")));
            this.btnChange.Location = new System.Drawing.Point(337, 186);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(100, 23);
            this.btnChange.TabIndex = 25;
            this.btnChange.Text = "Lưu lại";
            this.btnChange.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnChange.Click += new System.EventHandler(this.uiButton1_Click_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(547, 112);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 16);
            this.label9.TabIndex = 14;
            this.label9.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(547, 72);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 16);
            this.label8.TabIndex = 13;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(547, 34);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "*";
            // 
            // cbDatabaseSource
            // 
            this.cbDatabaseSource.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDatabaseSource.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbDatabaseSource.Location = new System.Drawing.Point(250, 147);
            this.cbDatabaseSource.Margin = new System.Windows.Forms.Padding(4);
            this.cbDatabaseSource.Name = "cbDatabaseSource";
            this.cbDatabaseSource.Size = new System.Drawing.Size(289, 23);
            this.cbDatabaseSource.TabIndex = 3;
            this.cbDatabaseSource.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbDatabaseSource.Closed += new System.EventHandler(this.cbDatabaseSource_Closed);
            this.cbDatabaseSource.SelectedIndexChanged += new System.EventHandler(this.cbDatabaseSource_SelectedIndexChanged);
            this.cbDatabaseSource.DropDown += new System.EventHandler(this.cbDatabaseSource_DropDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(98, 150);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Tên cơ sở dữ liệu";
            // 
            // txtServerName
            // 
            this.txtServerName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServerName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtServerName.Location = new System.Drawing.Point(250, 29);
            this.txtServerName.Margin = new System.Windows.Forms.Padding(4);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(289, 23);
            this.txtServerName.TabIndex = 0;
            this.txtServerName.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.txtServerName.SelectedIndexChanged += new System.EventHandler(this.txtServerName_SelectedIndexChanged);
            // 
            // txtPass
            // 
            this.txtPass.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPass.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPass.Location = new System.Drawing.Point(250, 107);
            this.txtPass.Margin = new System.Windows.Forms.Padding(4);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(289, 23);
            this.txtPass.TabIndex = 2;
            this.txtPass.Text = "123456";
            this.txtPass.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSa
            // 
            this.txtSa.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSa.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSa.Location = new System.Drawing.Point(250, 67);
            this.txtSa.Margin = new System.Windows.Forms.Padding(4);
            this.txtSa.Name = "txtSa";
            this.txtSa.Size = new System.Drawing.Size(289, 23);
            this.txtSa.TabIndex = 1;
            this.txtSa.Text = "sa";
            this.txtSa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(98, 110);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Mật khẩu truy nhập";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(98, 70);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tên truy nhập";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(98, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên / IP của máy chủ";
            // 
            // uiTabPageTaoCSDL
            // 
            this.uiTabPageTaoCSDL.Controls.Add(this.btnTaoCSDL);
            this.uiTabPageTaoCSDL.Controls.Add(this.label1);
            this.uiTabPageTaoCSDL.Location = new System.Drawing.Point(1, 24);
            this.uiTabPageTaoCSDL.Margin = new System.Windows.Forms.Padding(4);
            this.uiTabPageTaoCSDL.Name = "uiTabPageTaoCSDL";
            this.uiTabPageTaoCSDL.Size = new System.Drawing.Size(723, 292);
            this.uiTabPageTaoCSDL.TabStop = true;
            this.uiTabPageTaoCSDL.Text = "Tạo cơ sở dữ liệu";
            // 
            // btnTaoCSDL
            // 
            this.btnTaoCSDL.Image = ((System.Drawing.Image)(resources.GetObject("btnTaoCSDL.Image")));
            this.btnTaoCSDL.Location = new System.Drawing.Point(357, 23);
            this.btnTaoCSDL.Name = "btnTaoCSDL";
            this.btnTaoCSDL.Size = new System.Drawing.Size(100, 23);
            this.btnTaoCSDL.TabIndex = 26;
            this.btnTaoCSDL.Text = "Tạo CSDL";
            this.btnTaoCSDL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTaoCSDL.Click += new System.EventHandler(this.btnTaoCSDL_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(312, 112);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tạo Cở sở dữ liệu mặc đinh ban đầu sau khi cài đặt.\r\n\r\nChỉ áp dụng cho trường hợp" +
                " chưa có Cơ sở dữ liệu. \r\n\r\nLưu ý:\r\nNếu đã có cấu hình sử dụng 1 CSDL khác, khôn" +
                "g phải\r\nsử dụng chức năng này.";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(267, 131);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "uiGroupBox2";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ThietLapThongSoKBForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(725, 317);
            this.Controls.Add(this.uiGroupBox2);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "ThietLapThongSoKBForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cấu hình hệ thống";
            this.Load += new System.EventHandler(this.SendForm_Load);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPageKetNoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            this.uiTabPageTaoCSDL.ResumeLayout(false);
            this.uiTabPageTaoCSDL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtPass;
        private Janus.Windows.GridEX.EditControls.EditBox txtSa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Janus.Windows.EditControls.UIComboBox txtServerName;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIComboBox cbDatabaseSource;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageTaoCSDL;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageKetNoi;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnChange;
        private Janus.Windows.EditControls.UIButton btnTaoCSDL;

    }
}