﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
    public partial class HangMau
    {
        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

        public int DeleteBy_HopDong_ID(SqlTransaction transaction)
        {
            string spName = "p_GC_HangMau_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
    }
}