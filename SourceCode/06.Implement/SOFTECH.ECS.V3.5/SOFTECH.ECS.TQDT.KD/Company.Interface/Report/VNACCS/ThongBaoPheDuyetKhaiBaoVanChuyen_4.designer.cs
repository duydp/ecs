namespace Company.Interface.Report.VNACCS
{
    partial class ThongBaoPheDuyetKhaiBaoVanChuyen_4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongBaoPheDuyetKhaiBaoVanChuyen_4));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaCoBaoYeuCauXN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCoBaoYeuCauXN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoBaoXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayLapTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTTDongHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayPhatHanhVanDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblKyHieuSoHieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayNhapKhoHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiSP = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNuocSX = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNuocSX = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemXuatPhat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDiaDiemXuatPhat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemDich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDiaDiemDich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiManifest = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhuongTienVC2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenPhuongTienVC2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDuKienDenDi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVBPL1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVBPL2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVBPL3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVBPL4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVBPL5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTTriGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTheTich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTTheTich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDanhDauDDKhoiHanh1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDanhDauDDKhoiHanh2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDanhDauDDKhoiHanh3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDanhDauDDKhoiHanh4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDanhDauDDKhoiHanh5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoGiayPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayCapPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHetHanCapPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoToKhaiXK46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXK50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiBuuChinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenBuuChinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVach = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayPheDuyetVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDuKienBatDauVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioDuKienBatDauVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDuKienKetThucVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioDuKienKetThucVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.TongSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 9F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 110F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TongSoTrang,
            this.lblSoTrang,
            this.xrLine1,
            this.xrTable2,
            this.lblTenThongTinXuat});
            this.ReportHeader.HeightF = 84.375F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(785.0767F, 5.000003F);
            this.lblSoTrang.Multiline = true;
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTrang.SizeF = new System.Drawing.SizeF(19.92328F, 21.25F);
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "4 /";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 73.74998F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(825F, 10.00002F);
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(32.24233F, 33.74999F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27,
            this.xrTableRow28});
            this.xrTable2.SizeF = new System.Drawing.SizeF(792.7576F, 40F);
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171,
            this.xrTableCell174,
            this.lblMaCoBaoYeuCauXN,
            this.xrTableCell172,
            this.lblTenCoBaoYeuCauXN,
            this.xrTableCell177,
            this.xrTableCell176,
            this.xrTableCell179,
            this.lblCoQuanHQ});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.8;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Text = "Cờ báo yêu cầu xác nhận/ niêm phong";
            this.xrTableCell171.Weight = 1.9558137486142411;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Weight = 0.050000000061550018;
            // 
            // lblMaCoBaoYeuCauXN
            // 
            this.lblMaCoBaoYeuCauXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaCoBaoYeuCauXN.Name = "lblMaCoBaoYeuCauXN";
            this.lblMaCoBaoYeuCauXN.StylePriority.UseFont = false;
            this.lblMaCoBaoYeuCauXN.Text = "X";
            this.lblMaCoBaoYeuCauXN.Weight = 0.16366943278882012;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Text = "-";
            this.xrTableCell172.Weight = 0.099999999713554236;
            // 
            // lblTenCoBaoYeuCauXN
            // 
            this.lblTenCoBaoYeuCauXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCoBaoYeuCauXN.Name = "lblTenCoBaoYeuCauXN";
            this.lblTenCoBaoYeuCauXN.StylePriority.UseFont = false;
            this.lblTenCoBaoYeuCauXN.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWE";
            this.lblTenCoBaoYeuCauXN.Weight = 3.6414587401013137;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Weight = 0.050000000000035169;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Text = "Cơ quan Hải quan";
            this.xrTableCell176.Weight = 1.0187979114347625;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 0.049999999999988165;
            // 
            // lblCoQuanHQ
            // 
            this.lblCoQuanHQ.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoQuanHQ.Name = "lblCoQuanHQ";
            this.lblCoQuanHQ.StylePriority.UseFont = false;
            this.lblCoQuanHQ.Text = "XXXXXXXXXE";
            this.lblCoQuanHQ.Weight = 0.897836308643158;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180,
            this.xrTableCell181,
            this.lblSoToKhaiVC,
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185,
            this.lblCoBaoXNK,
            this.xrTableCell187,
            this.xrTableCell190,
            this.xrTableCell189,
            this.lblNgayLapTK});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.79999999999999993;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "Số tờ khai vận chuyển";
            this.xrTableCell180.Weight = 1.1950402806143874;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Weight = 0.050000000049673005;
            // 
            // lblSoToKhaiVC
            // 
            this.lblSoToKhaiVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiVC.Name = "lblSoToKhaiVC";
            this.lblSoToKhaiVC.StylePriority.UseFont = false;
            this.lblSoToKhaiVC.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiVC.Weight = 1.2659251429199241;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Weight = 0.053198547363281334;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "Cờ báo nhập khẩu/ xuất khẩu";
            this.xrTableCell184.Weight = 1.8565628051757819;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Weight = 0.053198852539062313;
            // 
            // lblCoBaoXNK
            // 
            this.lblCoBaoXNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoBaoXNK.Name = "lblCoBaoXNK";
            this.lblCoBaoXNK.StylePriority.UseFont = false;
            this.lblCoBaoXNK.Text = "X";
            this.lblCoBaoXNK.Weight = 1.4370162926173746;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 0.050000610351550789;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Text = "Ngày lập tờ khai";
            this.xrTableCell190.Weight = 1.0187982166105849;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Weight = 0.049999389648413994;
            // 
            // lblNgayLapTK
            // 
            this.lblNgayLapTK.Name = "lblNgayLapTK";
            this.lblNgayLapTK.Text = "dd/MM/yyyy";
            this.lblNgayLapTK.Weight = 0.89783600346738857;
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(205.19F, 5F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(420F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Thông báo phê duyệt khai báo vận chuyển";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel2,
            this.lblSoTTDongHang,
            this.xrTable1,
            this.xrLine2});
            this.Detail1.HeightF = 505.2083F;
            this.Detail1.Name = "Detail1";
            // 
            // xrLabel3
            // 
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(9.791668F, 16.99999F);
            this.xrLabel3.Text = ">";
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(10F, 17F);
            this.xrLabel2.Text = "<";
            // 
            // lblSoTTDongHang
            // 
            this.lblSoTTDongHang.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
            this.lblSoTTDongHang.Name = "lblSoTTDongHang";
            this.lblSoTTDongHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTTDongHang.SizeF = new System.Drawing.SizeF(9.999998F, 16.99999F);
            this.lblSoTTDongHang.StylePriority.UseTextAlignment = false;
            this.lblSoTTDongHang.Text = "5";
            this.lblSoTTDongHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(32.24233F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow4,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow5,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow20,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow1,
            this.xrTableRow26});
            this.xrTable1.SizeF = new System.Drawing.SizeF(792.7577F, 494.1666F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell18,
            this.lblSoHangHoa,
            this.xrTableCell25,
            this.xrTableCell2,
            this.xrTableCell1,
            this.lblNgayPhatHanhVanDon});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.67999999999999994;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "Số hàng hóa (Số B/L / AWB)";
            this.xrTableCell4.Weight = 0.574332443632066;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 0.018574794490784183;
            // 
            // lblSoHangHoa
            // 
            this.lblSoHangHoa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHangHoa.Name = "lblSoHangHoa";
            this.lblSoHangHoa.StylePriority.UseFont = false;
            this.lblSoHangHoa.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoHangHoa.Weight = 1.4097250450270711;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.042680978849905127;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Ngày phát hành vận đơn";
            this.xrTableCell2.Weight = 0.54763150274111494;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Weight = 0.018574806139580957;
            // 
            // lblNgayPhatHanhVanDon
            // 
            this.lblNgayPhatHanhVanDon.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayPhatHanhVanDon.Name = "lblNgayPhatHanhVanDon";
            this.lblNgayPhatHanhVanDon.StylePriority.UseFont = false;
            this.lblNgayPhatHanhVanDon.Text = "dd/MM/yyyy";
            this.lblNgayPhatHanhVanDon.Weight = 0.3335426588036548;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell27,
            this.lblMoTaHangHoa});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.68000016157363408;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Text = "Tên hàng";
            this.xrTableCell26.Weight = 0.24147231865256419;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.018574796158678516;
            // 
            // lblMoTaHangHoa
            // 
            this.lblMoTaHangHoa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoTaHangHoa.Name = "lblMoTaHangHoa";
            this.lblMoTaHangHoa.StylePriority.UseFont = false;
            this.lblMoTaHangHoa.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblMoTaHangHoa.Weight = 2.6850151148729346;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.lblMaHS});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.680000161573634;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "Mã HS (4 số)";
            this.xrTableCell37.Weight = 0.3320683959964858;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Weight = 0.018574966008928585;
            // 
            // lblMaHS
            // 
            this.lblMaHS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaHS.Name = "lblMaHS";
            this.lblMaHS.StylePriority.UseFont = false;
            this.lblMaHS.Text = "XXXE";
            this.lblMaHS.Weight = 2.5944188676787632;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29,
            this.lblKyHieuSoHieu});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1.2800003133138573;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Text = "Ký hiệu số hiệu";
            this.xrTableCell28.Weight = 0.33206844055575441;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.018574796015840042;
            // 
            // lblKyHieuSoHieu
            // 
            this.lblKyHieuSoHieu.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblKyHieuSoHieu.Name = "lblKyHieuSoHieu";
            this.lblKyHieuSoHieu.StylePriority.UseFont = false;
            this.lblKyHieuSoHieu.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWW0WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
            this.lblKyHieuSoHieu.Weight = 2.5944189931125825;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.xrTableCell32,
            this.lblNgayNhapKhoHQ});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.6800002237956202;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "Ngày nhập ngoại quan kho lần đầu";
            this.xrTableCell31.Weight = 0.67775333433221086;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 0.018574793782493632;
            // 
            // lblNgayNhapKhoHQ
            // 
            this.lblNgayNhapKhoHQ.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayNhapKhoHQ.Name = "lblNgayNhapKhoHQ";
            this.lblNgayNhapKhoHQ.StylePriority.UseFont = false;
            this.lblNgayNhapKhoHQ.Text = "dd/MM/yyyy";
            this.lblNgayNhapKhoHQ.Weight = 2.2487341015694726;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell34,
            this.lblPhanLoaiSP,
            this.xrTableCell51,
            this.xrTableCell54,
            this.lblMaNuocSX,
            this.xrTableCell3,
            this.lblTenNuocSX});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.6800002237956202;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Text = "Phân loại sản phẩm sản xuất từ hàng hóa nhập khẩu";
            this.xrTableCell33.Weight = 1.0247974864236598;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Weight = 0.018292498627582754;
            // 
            // lblPhanLoaiSP
            // 
            this.lblPhanLoaiSP.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiSP.Name = "lblPhanLoaiSP";
            this.lblPhanLoaiSP.StylePriority.UseFont = false;
            this.lblPhanLoaiSP.Text = "X";
            this.lblPhanLoaiSP.Weight = 0.22630469302159953;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.018574788994650315;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Nước sản xuất hoặc nơi sản xuất";
            this.xrTableCell54.Weight = 0.71466262890435228;
            // 
            // lblMaNuocSX
            // 
            this.lblMaNuocSX.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNuocSX.Name = "lblMaNuocSX";
            this.lblMaNuocSX.StylePriority.UseFont = false;
            this.lblMaNuocSX.Text = "XE";
            this.lblMaNuocSX.Weight = 0.086799518471994019;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "-";
            this.xrTableCell3.Weight = 0.037149590055939929;
            // 
            // lblTenNuocSX
            // 
            this.lblTenNuocSX.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNuocSX.Name = "lblTenNuocSX";
            this.lblTenNuocSX.StylePriority.UseFont = false;
            this.lblTenNuocSX.Text = "XXXXXXE";
            this.lblTenNuocSX.Weight = 0.81848102518439847;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.lblMaDiaDiemXuatPhat,
            this.xrTableCell55,
            this.lblTenDiaDiemXuatPhat,
            this.xrTableCell9,
            this.xrTableCell7,
            this.xrTableCell11,
            this.lblMaDiaDiemDich,
            this.xrTableCell10,
            this.lblTenDiaDiemDich,
            this.xrTableCell14,
            this.xrTableCell13,
            this.xrTableCell16,
            this.lblLoaiManifest});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.68000022379562042;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "Địa điểm xuất phát";
            this.xrTableCell45.Weight = 0.38661688000373273;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 0.01857479378249359;
            // 
            // lblMaDiaDiemXuatPhat
            // 
            this.lblMaDiaDiemXuatPhat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemXuatPhat.Name = "lblMaDiaDiemXuatPhat";
            this.lblMaDiaDiemXuatPhat.StylePriority.UseFont = false;
            this.lblMaDiaDiemXuatPhat.Text = "XXXXE";
            this.lblMaDiaDiemXuatPhat.Weight = 0.16914074237588123;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Text = "-";
            this.xrTableCell55.Weight = 0.0371495914770433;
            // 
            // lblTenDiaDiemXuatPhat
            // 
            this.lblTenDiaDiemXuatPhat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDiaDiemXuatPhat.Name = "lblTenDiaDiemXuatPhat";
            this.lblTenDiaDiemXuatPhat.StylePriority.UseFont = false;
            this.lblTenDiaDiemXuatPhat.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTenDiaDiemXuatPhat.Weight = 0.65791274279905065;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 0.018574790062747148;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "Địa điểm đích";
            this.xrTableCell7.Weight = 0.285867516348925;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.018574797148468936;
            // 
            // lblMaDiaDiemDich
            // 
            this.lblMaDiaDiemDich.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemDich.Name = "lblMaDiaDiemDich";
            this.lblMaDiaDiemDich.StylePriority.UseFont = false;
            this.lblMaDiaDiemDich.Text = "XXXXE";
            this.lblMaDiaDiemDich.Weight = 0.18743605528740573;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "-";
            this.xrTableCell10.Weight = 0.0371495973350181;
            // 
            // lblTenDiaDiemDich
            // 
            this.lblTenDiaDiemDich.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDiaDiemDich.Name = "lblTenDiaDiemDich";
            this.lblTenDiaDiemDich.StylePriority.UseFont = false;
            this.lblTenDiaDiemDich.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTenDiaDiemDich.Weight = 0.61788352779640621;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Weight = 0.018574796896078612;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "Loại manifest";
            this.xrTableCell13.Weight = 0.32471814123692383;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Weight = 0.01857479225639419;
            // 
            // lblLoaiManifest
            // 
            this.lblLoaiManifest.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiManifest.Name = "lblLoaiManifest";
            this.lblLoaiManifest.StylePriority.UseFont = false;
            this.lblLoaiManifest.Text = "X";
            this.lblLoaiManifest.Weight = 0.148313464877608;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49,
            this.lblMaPhuongTienVC2,
            this.xrTableCell57,
            this.lblTenPhuongTienVC2});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.68000022379559732;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Text = "Phương tiện vận chuyển";
            this.xrTableCell48.Weight = 0.4643698475433774;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.01857473709671955;
            // 
            // lblMaPhuongTienVC2
            // 
            this.lblMaPhuongTienVC2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhuongTienVC2.Name = "lblMaPhuongTienVC2";
            this.lblMaPhuongTienVC2.StylePriority.UseFont = false;
            this.lblMaPhuongTienVC2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXE";
            this.lblMaPhuongTienVC2.Weight = 1.1791004172729447;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Text = "-";
            this.xrTableCell57.Weight = 0.037149815874477986;
            // 
            // lblTenPhuongTienVC2
            // 
            this.lblTenPhuongTienVC2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenPhuongTienVC2.Name = "lblTenPhuongTienVC2";
            this.lblTenPhuongTienVC2.StylePriority.UseFont = false;
            this.lblTenPhuongTienVC2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTenPhuongTienVC2.Weight = 1.2458674118966575;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59,
            this.xrTableCell60,
            this.lblNgayDuKienDenDi});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.68000022379559732;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Text = "Ngày dự kiến đến/ đi";
            this.xrTableCell59.Weight = 0.46436980605500028;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 0.018574794213527916;
            // 
            // lblNgayDuKienDenDi
            // 
            this.lblNgayDuKienDenDi.Name = "lblNgayDuKienDenDi";
            this.lblNgayDuKienDenDi.Text = "dd/MM/yyyy";
            this.lblNgayDuKienDenDi.Weight = 2.4621176294156495;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell19,
            this.lblMaNguoiNK,
            this.xrTableCell17,
            this.lblTenNguoiNK});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1.2800003755358205;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Text = "Người nhập khẩu";
            this.xrTableCell23.Weight = 0.46436986125103152;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.018574794450041288;
            // 
            // lblMaNguoiNK
            // 
            this.lblMaNguoiNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiNK.Name = "lblMaNguoiNK";
            this.lblMaNguoiNK.StylePriority.UseFont = false;
            this.lblMaNguoiNK.Text = "XXXXXXXXX1 - XXE";
            this.lblMaNguoiNK.Weight = 0.4600565273298507;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Text = "-";
            this.xrTableCell17.Weight = 0.059845665868726722;
            // 
            // lblTenNguoiNK
            // 
            this.lblTenNguoiNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiNK.Name = "lblTenNguoiNK";
            this.lblTenNguoiNK.StylePriority.UseFont = false;
            this.lblTenNguoiNK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiNK.Weight = 1.9422153807845275;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell69,
            this.xrTableCell68,
            this.lblDiaChiNguoiNK});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1.2800003099499246;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 0.16806064435949991;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Text = "Địa chỉ";
            this.xrTableCell69.Weight = 0.79763663550888053;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.037149815931925581;
            // 
            // lblDiaChiNguoiNK
            // 
            this.lblDiaChiNguoiNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiNK.Name = "lblDiaChiNguoiNK";
            this.lblDiaChiNguoiNK.StylePriority.UseFont = false;
            this.lblDiaChiNguoiNK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiNK.Weight = 1.9422151338838716;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell83,
            this.lblMaNguoiXK,
            this.xrTableCell85,
            this.lblTenNguoiXK});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1.2800003099499246;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Text = "Người xuất khẩu";
            this.xrTableCell81.Weight = 0.46436970593814314;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.018575083292037442;
            // 
            // lblMaNguoiXK
            // 
            this.lblMaNguoiXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiXK.Name = "lblMaNguoiXK";
            this.lblMaNguoiXK.StylePriority.UseFont = false;
            this.lblMaNguoiXK.Text = "XXXXXXXXX1 - XXE";
            this.lblMaNguoiXK.Weight = 0.46005617434230506;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Text = "-";
            this.xrTableCell85.Weight = 0.059846006624884346;
            // 
            // lblTenNguoiXK
            // 
            this.lblTenNguoiXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiXK.Name = "lblTenNguoiXK";
            this.lblTenNguoiXK.StylePriority.UseFont = false;
            this.lblTenNguoiXK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiXK.Weight = 1.9422152594868076;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell90,
            this.xrTableCell91,
            this.lblDiaChiNguoiXK});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1.2799994493772879;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 0.16806077877599623;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Text = "Địa chỉ";
            this.xrTableCell90.Weight = 0.797636735186322;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Weight = 0.0371494539528775;
            // 
            // lblDiaChiNguoiXK
            // 
            this.lblDiaChiNguoiXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiXK.Name = "lblDiaChiNguoiXK";
            this.lblDiaChiNguoiXK.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiXK.Weight = 1.9422152617689816;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell63,
            this.lblMaNguoiUyThac,
            this.xrTableCell41,
            this.lblTenNguoiUyThac});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1.2800002853907795;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Text = "Người ủy thác (Trustor)";
            this.xrTableCell61.Weight = 0.46436983290814449;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.01857468107849311;
            // 
            // lblMaNguoiUyThac
            // 
            this.lblMaNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiUyThac.Name = "lblMaNguoiUyThac";
            this.lblMaNguoiUyThac.StylePriority.UseFont = false;
            this.lblMaNguoiUyThac.Text = "XXXXXXXXX1 - XXE";
            this.lblMaNguoiUyThac.Weight = 0.4600566123585117;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "-";
            this.xrTableCell41.Weight = 0.0598460059833712;
            // 
            // lblTenNguoiUyThac
            // 
            this.lblTenNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiUyThac.Name = "lblTenNguoiUyThac";
            this.lblTenNguoiUyThac.StylePriority.UseFont = false;
            this.lblTenNguoiUyThac.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiUyThac.Weight = 1.9422150973556569;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144,
            this.lblDiaChiNguoiUyThac});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1.2800004099356424;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 0.16806075773104806;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Text = "Địa chỉ";
            this.xrTableCell143.Weight = 0.7976368799662813;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Weight = 0.037149458102976651;
            // 
            // lblDiaChiNguoiUyThac
            // 
            this.lblDiaChiNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiUyThac.Name = "lblDiaChiNguoiUyThac";
            this.lblDiaChiNguoiUyThac.StylePriority.UseFont = false;
            this.lblDiaChiNguoiUyThac.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiUyThac.Weight = 1.9422151338838716;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell147,
            this.xrTableCell152,
            this.lblMaVBPL1,
            this.xrTableCell82,
            this.lblMaVBPL2,
            this.xrTableCell88,
            this.lblMaVBPL3,
            this.xrTableCell89,
            this.lblMaVBPL4,
            this.xrTableCell94,
            this.lblMaVBPL5,
            this.xrTableCell93,
            this.xrTableCell78,
            this.xrTableCell99,
            this.lblMaDVTTriGia,
            this.xrTableCell97,
            this.lblTriGia});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 0.68000028055075812;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Text = "Luật khác";
            this.xrTableCell147.Weight = 0.25601619392537667;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Weight = 0.018292311812250515;
            // 
            // lblMaVBPL1
            // 
            this.lblMaVBPL1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVBPL1.Name = "lblMaVBPL1";
            this.lblMaVBPL1.StylePriority.UseFont = false;
            this.lblMaVBPL1.Text = "XE";
            this.lblMaVBPL1.Weight = 0.074299175532681463;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Weight = 0.018574806354001161;
            // 
            // lblMaVBPL2
            // 
            this.lblMaVBPL2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVBPL2.Name = "lblMaVBPL2";
            this.lblMaVBPL2.StylePriority.UseFont = false;
            this.lblMaVBPL2.Text = "XE";
            this.lblMaVBPL2.Weight = 0.074299187436641545;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 0.018574775601995053;
            // 
            // lblMaVBPL3
            // 
            this.lblMaVBPL3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVBPL3.Name = "lblMaVBPL3";
            this.lblMaVBPL3.StylePriority.UseFont = false;
            this.lblMaVBPL3.Text = "XE";
            this.lblMaVBPL3.Weight = 0.074299158952118916;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 0.018574775601995053;
            // 
            // lblMaVBPL4
            // 
            this.lblMaVBPL4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVBPL4.Name = "lblMaVBPL4";
            this.lblMaVBPL4.StylePriority.UseFont = false;
            this.lblMaVBPL4.Text = "XE";
            this.lblMaVBPL4.Weight = 0.0742991589521187;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Weight = 0.0185747897734386;
            // 
            // lblMaVBPL5
            // 
            this.lblMaVBPL5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVBPL5.Name = "lblMaVBPL5";
            this.lblMaVBPL5.StylePriority.UseFont = false;
            this.lblMaVBPL5.Text = "XE";
            this.lblMaVBPL5.Weight = 0.074299175461863709;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 0.037149586561781134;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "Giá tiền";
            this.xrTableCell78.Weight = 0.18574794223292909;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Weight = 0.018574806354001105;
            // 
            // lblMaDVTTriGia
            // 
            this.lblMaDVTTriGia.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDVTTriGia.Name = "lblMaDVTTriGia";
            this.lblMaDVTTriGia.StylePriority.UseFont = false;
            this.lblMaDVTTriGia.Text = "XXE";
            this.lblMaDVTTriGia.Weight = 0.111448764432764;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Text = "-";
            this.xrTableCell97.Weight = 0.037149534765063219;
            // 
            // lblTriGia
            // 
            this.lblTriGia.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTriGia.Name = "lblTriGia";
            this.lblTriGia.StylePriority.UseFont = false;
            this.lblTriGia.Text = "12.345.678.901.234.567.890";
            this.lblTriGia.Weight = 1.8348880859331576;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148,
            this.xrTableCell153,
            this.lblSoLuong,
            this.xrTableCell108,
            this.lblMaDVTSoLuong,
            this.xrTableCell106,
            this.xrTableCell165,
            this.xrTableCell103,
            this.lblTongTrongLuong,
            this.xrTableCell104,
            this.lblMaDVTTrongLuong,
            this.xrTableCell162,
            this.xrTableCell102,
            this.xrTableCell105,
            this.lblTheTich,
            this.xrTableCell100,
            this.lblMaDVTTheTich});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.68000028055075812;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Text = "Số lượng";
            this.xrTableCell148.Weight = 0.25601619392537667;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Weight = 0.018292311812250515;
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.StylePriority.UseFont = false;
            this.lblSoLuong.Text = "12.345.678";
            this.lblSoLuong.Weight = 0.24147233473054569;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Text = "-";
            this.xrTableCell108.Weight = 0.03714958672188777;
            // 
            // lblMaDVTSoLuong
            // 
            this.lblMaDVTSoLuong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDVTSoLuong.Name = "lblMaDVTSoLuong";
            this.lblMaDVTSoLuong.StylePriority.UseFont = false;
            this.lblMaDVTSoLuong.Text = "XXE";
            this.lblMaDVTSoLuong.Weight = 0.16717316204362015;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 0.037149655400910753;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Text = "Tổng trọng lượng";
            this.xrTableCell165.Weight = 0.42160584948745267;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Weight = 0.037149592894604927;
            // 
            // lblTongTrongLuong
            // 
            this.lblTongTrongLuong.Name = "lblTongTrongLuong";
            this.lblTongTrongLuong.Text = "1.234.567.890";
            this.lblTongTrongLuong.Weight = 0.29719671519518265;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Text = "-";
            this.xrTableCell104.Weight = 0.03714959477528984;
            // 
            // lblMaDVTTrongLuong
            // 
            this.lblMaDVTTrongLuong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDVTTrongLuong.Name = "lblMaDVTTrongLuong";
            this.lblMaDVTTrongLuong.StylePriority.UseFont = false;
            this.lblMaDVTTrongLuong.Text = "XXE";
            this.lblMaDVTTrongLuong.Weight = 0.14883995624466018;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.037149592839448853;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Text = "Thể tích";
            this.xrTableCell102.Weight = 0.19012501894077877;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Weight = 0.037923461770919042;
            // 
            // lblTheTich
            // 
            this.lblTheTich.Name = "lblTheTich";
            this.lblTheTich.Text = "1.234.567.890";
            this.lblTheTich.Weight = 0.29719670988377717;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Text = "-";
            this.xrTableCell100.Weight = 0.037149590718271897;
            // 
            // lblMaDVTTheTich
            // 
            this.lblMaDVTTheTich.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDVTTheTich.Name = "lblMaDVTTheTich";
            this.lblMaDVTTheTich.StylePriority.UseFont = false;
            this.lblMaDVTTheTich.Text = "XXE";
            this.lblMaDVTTheTich.Weight = 0.64632290229919942;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.xrTableCell157,
            this.lblMaDanhDauDDKhoiHanh1,
            this.xrTableCell169,
            this.lblMaDanhDauDDKhoiHanh2,
            this.xrTableCell170,
            this.lblMaDanhDauDDKhoiHanh3,
            this.xrTableCell110,
            this.lblMaDanhDauDDKhoiHanh4,
            this.xrTableCell109,
            this.lblMaDanhDauDDKhoiHanh5});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.68000012820314693;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Text = "Mã đánh dấu hàng hóa tại điểm khởi hành";
            this.xrTableCell155.Weight = 0.798716178698435;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Weight = 0.018382924022114965;
            // 
            // lblMaDanhDauDDKhoiHanh1
            // 
            this.lblMaDanhDauDDKhoiHanh1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDanhDauDDKhoiHanh1.Name = "lblMaDanhDauDDKhoiHanh1";
            this.lblMaDanhDauDDKhoiHanh1.StylePriority.UseFont = false;
            this.lblMaDanhDauDDKhoiHanh1.Text = "XXXXE";
            this.lblMaDanhDauDDKhoiHanh1.Weight = 0.18574794262329583;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Weight = 0.018574794846546665;
            // 
            // lblMaDanhDauDDKhoiHanh2
            // 
            this.lblMaDanhDauDDKhoiHanh2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDanhDauDDKhoiHanh2.Name = "lblMaDanhDauDDKhoiHanh2";
            this.lblMaDanhDauDDKhoiHanh2.StylePriority.UseFont = false;
            this.lblMaDanhDauDDKhoiHanh2.Text = "XXXXE";
            this.lblMaDanhDauDDKhoiHanh2.Weight = 0.18574794610849107;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 0.018574794738788272;
            // 
            // lblMaDanhDauDDKhoiHanh3
            // 
            this.lblMaDanhDauDDKhoiHanh3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDanhDauDDKhoiHanh3.Name = "lblMaDanhDauDDKhoiHanh3";
            this.lblMaDanhDauDDKhoiHanh3.StylePriority.UseFont = false;
            this.lblMaDanhDauDDKhoiHanh3.Text = "XXXXE";
            this.lblMaDanhDauDDKhoiHanh3.Weight = 0.1857479351582382;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 0.018574795736449407;
            // 
            // lblMaDanhDauDDKhoiHanh4
            // 
            this.lblMaDanhDauDDKhoiHanh4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDanhDauDDKhoiHanh4.Name = "lblMaDanhDauDDKhoiHanh4";
            this.lblMaDanhDauDDKhoiHanh4.StylePriority.UseFont = false;
            this.lblMaDanhDauDDKhoiHanh4.Text = "XXXXE";
            this.lblMaDanhDauDDKhoiHanh4.Weight = 0.1857479457868208;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 0.01857479573644949;
            // 
            // lblMaDanhDauDDKhoiHanh5
            // 
            this.lblMaDanhDauDDKhoiHanh5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDanhDauDDKhoiHanh5.Name = "lblMaDanhDauDDKhoiHanh5";
            this.lblMaDanhDauDDKhoiHanh5.StylePriority.UseFont = false;
            this.lblMaDanhDauDDKhoiHanh5.Text = "XXXXE";
            this.lblMaDanhDauDDKhoiHanh5.Weight = 1.310672176228548;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTableCell70,
            this.lblSoGiayPhep,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.lblNgayCapPhep,
            this.xrTableCell115,
            this.xrTableCell116,
            this.xrTableCell113,
            this.lblNgayHetHanCapPhep});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.680000128203147;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Text = "Số giấy phép";
            this.xrTableCell67.Weight = 0.25601615756674251;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 0.055441957398107089;
            // 
            // lblSoGiayPhep
            // 
            this.lblSoGiayPhep.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGiayPhep.Name = "lblSoGiayPhep";
            this.lblSoGiayPhep.StylePriority.UseFont = false;
            this.lblSoGiayPhep.Text = "XXXXXXXXX1E";
            this.lblSoGiayPhep.Weight = 0.36629520118082459;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Weight = 0.018574794846546644;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Text = "Ngày cấp phép";
            this.xrTableCell73.Weight = 0.30651918081360946;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Weight = 0.021950092470518177;
            // 
            // lblNgayCapPhep
            // 
            this.lblNgayCapPhep.Name = "lblNgayCapPhep";
            this.lblNgayCapPhep.Text = "dd/MM/yyyy";
            this.lblNgayCapPhep.Weight = 0.38669527492040479;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Weight = 0.018574568993353135;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Text = "Ngày hết hạn";
            this.xrTableCell116.Weight = 0.27426390091122477;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.032013277382590632;
            // 
            // lblNgayHetHanCapPhep
            // 
            this.lblNgayHetHanCapPhep.Name = "lblNgayHetHanCapPhep";
            this.lblNgayHetHanCapPhep.Text = "dd/MM/yyyy";
            this.lblNgayHetHanCapPhep.Weight = 1.2087178232002556;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.xrTableCell160,
            this.lblGhiChu2});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 2.2457180622957225;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Text = "Ghi chú 2";
            this.xrTableCell159.Weight = 0.25601619392537667;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Weight = 0.018292311812250515;
            // 
            // lblGhiChu2
            // 
            this.lblGhiChu2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChu2.Name = "lblGhiChu2";
            this.lblGhiChu2.StylePriority.UseFont = false;
            this.lblGhiChu2.Text = resources.GetString("lblGhiChu2.Text");
            this.lblGhiChu2.Weight = 2.67075372394655;
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 494.1666F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(825F, 10.00003F);
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2});
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5,
            this.xrTable4,
            this.lblMaVach,
            this.xrTable3});
            this.Detail2.HeightF = 404.1667F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(32.24233F, 151.6666F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40});
            this.xrTable5.SizeF = new System.Drawing.SizeF(661.5871F, 232.7084F);
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.67999999999999994;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Text = "Số tờ khai xuất khẩu";
            this.xrTableCell191.Weight = 1.6576100158691407;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 1.3423899841308593;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK1,
            this.xrTableCell195,
            this.lblSoToKhaiXK2,
            this.xrTableCell201,
            this.lblSoToKhaiXK3,
            this.xrTableCell199,
            this.lblSoToKhaiXK4,
            this.xrTableCell200,
            this.lblSoToKhaiXK5});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK1
            // 
            this.lblSoToKhaiXK1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK1.Name = "lblSoToKhaiXK1";
            this.lblSoToKhaiXK1.StylePriority.UseFont = false;
            this.lblSoToKhaiXK1.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK1.Weight = 0.49880052989342638;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK2
            // 
            this.lblSoToKhaiXK2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK2.Name = "lblSoToKhaiXK2";
            this.lblSoToKhaiXK2.StylePriority.UseFont = false;
            this.lblSoToKhaiXK2.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK2.Weight = 0.49880052918255591;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK3
            // 
            this.lblSoToKhaiXK3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK3.Name = "lblSoToKhaiXK3";
            this.lblSoToKhaiXK3.StylePriority.UseFont = false;
            this.lblSoToKhaiXK3.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK3.Weight = 0.49880052558402704;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK4
            // 
            this.lblSoToKhaiXK4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK4.Name = "lblSoToKhaiXK4";
            this.lblSoToKhaiXK4.StylePriority.UseFont = false;
            this.lblSoToKhaiXK4.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK4.Weight = 0.49880054725170225;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK5
            // 
            this.lblSoToKhaiXK5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK5.Name = "lblSoToKhaiXK5";
            this.lblSoToKhaiXK5.StylePriority.UseFont = false;
            this.lblSoToKhaiXK5.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK5.Weight = 0.64203384048896417;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK6,
            this.xrTableCell203,
            this.lblSoToKhaiXK7,
            this.xrTableCell205,
            this.lblSoToKhaiXK8,
            this.xrTableCell207,
            this.lblSoToKhaiXK9,
            this.xrTableCell209,
            this.lblSoToKhaiXK10});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK6
            // 
            this.lblSoToKhaiXK6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK6.Name = "lblSoToKhaiXK6";
            this.lblSoToKhaiXK6.StylePriority.UseFont = false;
            this.lblSoToKhaiXK6.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK6.Weight = 0.49880052989342638;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK7
            // 
            this.lblSoToKhaiXK7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK7.Name = "lblSoToKhaiXK7";
            this.lblSoToKhaiXK7.StylePriority.UseFont = false;
            this.lblSoToKhaiXK7.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK7.Weight = 0.49880052918255591;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK8
            // 
            this.lblSoToKhaiXK8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK8.Name = "lblSoToKhaiXK8";
            this.lblSoToKhaiXK8.StylePriority.UseFont = false;
            this.lblSoToKhaiXK8.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK8.Weight = 0.49880052558402704;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK9
            // 
            this.lblSoToKhaiXK9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK9.Name = "lblSoToKhaiXK9";
            this.lblSoToKhaiXK9.StylePriority.UseFont = false;
            this.lblSoToKhaiXK9.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK9.Weight = 0.49880054725170225;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK10
            // 
            this.lblSoToKhaiXK10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK10.Name = "lblSoToKhaiXK10";
            this.lblSoToKhaiXK10.StylePriority.UseFont = false;
            this.lblSoToKhaiXK10.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK10.Weight = 0.64203384048896417;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK11,
            this.xrTableCell212,
            this.lblSoToKhaiXK12,
            this.xrTableCell214,
            this.lblSoToKhaiXK13,
            this.xrTableCell216,
            this.lblSoToKhaiXK14,
            this.xrTableCell218,
            this.lblSoToKhaiXK15});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK11
            // 
            this.lblSoToKhaiXK11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK11.Name = "lblSoToKhaiXK11";
            this.lblSoToKhaiXK11.StylePriority.UseFont = false;
            this.lblSoToKhaiXK11.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK11.Weight = 0.49880052989342638;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK12
            // 
            this.lblSoToKhaiXK12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK12.Name = "lblSoToKhaiXK12";
            this.lblSoToKhaiXK12.StylePriority.UseFont = false;
            this.lblSoToKhaiXK12.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK12.Weight = 0.49880052918255591;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK13
            // 
            this.lblSoToKhaiXK13.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK13.Name = "lblSoToKhaiXK13";
            this.lblSoToKhaiXK13.StylePriority.UseFont = false;
            this.lblSoToKhaiXK13.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK13.Weight = 0.49880052558402704;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK14
            // 
            this.lblSoToKhaiXK14.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK14.Name = "lblSoToKhaiXK14";
            this.lblSoToKhaiXK14.StylePriority.UseFont = false;
            this.lblSoToKhaiXK14.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK14.Weight = 0.49880054725170225;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK15
            // 
            this.lblSoToKhaiXK15.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK15.Name = "lblSoToKhaiXK15";
            this.lblSoToKhaiXK15.StylePriority.UseFont = false;
            this.lblSoToKhaiXK15.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK15.Weight = 0.64203384048896417;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK16,
            this.xrTableCell221,
            this.lblSoToKhaiXK17,
            this.xrTableCell223,
            this.lblSoToKhaiXK18,
            this.xrTableCell225,
            this.lblSoToKhaiXK19,
            this.xrTableCell227,
            this.lblSoToKhaiXK20});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK16
            // 
            this.lblSoToKhaiXK16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK16.Name = "lblSoToKhaiXK16";
            this.lblSoToKhaiXK16.StylePriority.UseFont = false;
            this.lblSoToKhaiXK16.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK16.Weight = 0.49880052989342638;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK17
            // 
            this.lblSoToKhaiXK17.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK17.Name = "lblSoToKhaiXK17";
            this.lblSoToKhaiXK17.StylePriority.UseFont = false;
            this.lblSoToKhaiXK17.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK17.Weight = 0.49880052918255591;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK18
            // 
            this.lblSoToKhaiXK18.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK18.Name = "lblSoToKhaiXK18";
            this.lblSoToKhaiXK18.StylePriority.UseFont = false;
            this.lblSoToKhaiXK18.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK18.Weight = 0.49880052558402704;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK19
            // 
            this.lblSoToKhaiXK19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK19.Name = "lblSoToKhaiXK19";
            this.lblSoToKhaiXK19.StylePriority.UseFont = false;
            this.lblSoToKhaiXK19.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK19.Weight = 0.49880054725170225;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK20
            // 
            this.lblSoToKhaiXK20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK20.Name = "lblSoToKhaiXK20";
            this.lblSoToKhaiXK20.StylePriority.UseFont = false;
            this.lblSoToKhaiXK20.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK20.Weight = 0.64203384048896417;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK21,
            this.xrTableCell230,
            this.lblSoToKhaiXK22,
            this.xrTableCell232,
            this.lblSoToKhaiXK23,
            this.xrTableCell234,
            this.lblSoToKhaiXK24,
            this.xrTableCell236,
            this.lblSoToKhaiXK25});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK21
            // 
            this.lblSoToKhaiXK21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK21.Name = "lblSoToKhaiXK21";
            this.lblSoToKhaiXK21.StylePriority.UseFont = false;
            this.lblSoToKhaiXK21.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK21.Weight = 0.49880052989342638;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK22
            // 
            this.lblSoToKhaiXK22.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK22.Name = "lblSoToKhaiXK22";
            this.lblSoToKhaiXK22.StylePriority.UseFont = false;
            this.lblSoToKhaiXK22.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK22.Weight = 0.49880052918255591;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK23
            // 
            this.lblSoToKhaiXK23.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK23.Name = "lblSoToKhaiXK23";
            this.lblSoToKhaiXK23.StylePriority.UseFont = false;
            this.lblSoToKhaiXK23.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK23.Weight = 0.49880052558402704;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK24
            // 
            this.lblSoToKhaiXK24.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK24.Name = "lblSoToKhaiXK24";
            this.lblSoToKhaiXK24.StylePriority.UseFont = false;
            this.lblSoToKhaiXK24.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK24.Weight = 0.49880054725170225;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK25
            // 
            this.lblSoToKhaiXK25.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK25.Name = "lblSoToKhaiXK25";
            this.lblSoToKhaiXK25.StylePriority.UseFont = false;
            this.lblSoToKhaiXK25.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK25.Weight = 0.64203384048896417;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK26,
            this.xrTableCell239,
            this.lblSoToKhaiXK27,
            this.xrTableCell241,
            this.lblSoToKhaiXK28,
            this.xrTableCell243,
            this.lblSoToKhaiXK29,
            this.xrTableCell245,
            this.lblSoToKhaiXK30});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK26
            // 
            this.lblSoToKhaiXK26.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK26.Name = "lblSoToKhaiXK26";
            this.lblSoToKhaiXK26.StylePriority.UseFont = false;
            this.lblSoToKhaiXK26.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK26.Weight = 0.49880052989342638;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK27
            // 
            this.lblSoToKhaiXK27.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK27.Name = "lblSoToKhaiXK27";
            this.lblSoToKhaiXK27.StylePriority.UseFont = false;
            this.lblSoToKhaiXK27.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK27.Weight = 0.49880052918255591;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK28
            // 
            this.lblSoToKhaiXK28.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK28.Name = "lblSoToKhaiXK28";
            this.lblSoToKhaiXK28.StylePriority.UseFont = false;
            this.lblSoToKhaiXK28.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK28.Weight = 0.49880052558402704;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK29
            // 
            this.lblSoToKhaiXK29.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK29.Name = "lblSoToKhaiXK29";
            this.lblSoToKhaiXK29.StylePriority.UseFont = false;
            this.lblSoToKhaiXK29.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK29.Weight = 0.49880054725170225;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK30
            // 
            this.lblSoToKhaiXK30.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK30.Name = "lblSoToKhaiXK30";
            this.lblSoToKhaiXK30.StylePriority.UseFont = false;
            this.lblSoToKhaiXK30.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK30.Weight = 0.64203384048896417;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK31,
            this.xrTableCell248,
            this.lblSoToKhaiXK32,
            this.xrTableCell250,
            this.lblSoToKhaiXK33,
            this.xrTableCell252,
            this.lblSoToKhaiXK34,
            this.xrTableCell254,
            this.lblSoToKhaiXK35});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK31
            // 
            this.lblSoToKhaiXK31.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK31.Name = "lblSoToKhaiXK31";
            this.lblSoToKhaiXK31.StylePriority.UseFont = false;
            this.lblSoToKhaiXK31.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK31.Weight = 0.49880052989342638;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK32
            // 
            this.lblSoToKhaiXK32.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK32.Name = "lblSoToKhaiXK32";
            this.lblSoToKhaiXK32.StylePriority.UseFont = false;
            this.lblSoToKhaiXK32.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK32.Weight = 0.49880052918255591;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK33
            // 
            this.lblSoToKhaiXK33.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK33.Name = "lblSoToKhaiXK33";
            this.lblSoToKhaiXK33.StylePriority.UseFont = false;
            this.lblSoToKhaiXK33.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK33.Weight = 0.49880052558402704;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK34
            // 
            this.lblSoToKhaiXK34.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK34.Name = "lblSoToKhaiXK34";
            this.lblSoToKhaiXK34.StylePriority.UseFont = false;
            this.lblSoToKhaiXK34.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK34.Weight = 0.49880054725170225;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK35
            // 
            this.lblSoToKhaiXK35.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK35.Name = "lblSoToKhaiXK35";
            this.lblSoToKhaiXK35.StylePriority.UseFont = false;
            this.lblSoToKhaiXK35.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK35.Weight = 0.64203384048896417;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK36,
            this.xrTableCell257,
            this.lblSoToKhaiXK37,
            this.xrTableCell259,
            this.lblSoToKhaiXK38,
            this.xrTableCell261,
            this.lblSoToKhaiXK39,
            this.xrTableCell263,
            this.lblSoToKhaiXK40});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK36
            // 
            this.lblSoToKhaiXK36.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK36.Name = "lblSoToKhaiXK36";
            this.lblSoToKhaiXK36.StylePriority.UseFont = false;
            this.lblSoToKhaiXK36.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK36.Weight = 0.49880052989342638;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK37
            // 
            this.lblSoToKhaiXK37.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK37.Name = "lblSoToKhaiXK37";
            this.lblSoToKhaiXK37.StylePriority.UseFont = false;
            this.lblSoToKhaiXK37.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK37.Weight = 0.49880052918255591;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK38
            // 
            this.lblSoToKhaiXK38.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK38.Name = "lblSoToKhaiXK38";
            this.lblSoToKhaiXK38.StylePriority.UseFont = false;
            this.lblSoToKhaiXK38.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK38.Weight = 0.49880052558402704;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK39
            // 
            this.lblSoToKhaiXK39.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK39.Name = "lblSoToKhaiXK39";
            this.lblSoToKhaiXK39.StylePriority.UseFont = false;
            this.lblSoToKhaiXK39.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK39.Weight = 0.49880054725170225;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK40
            // 
            this.lblSoToKhaiXK40.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK40.Name = "lblSoToKhaiXK40";
            this.lblSoToKhaiXK40.StylePriority.UseFont = false;
            this.lblSoToKhaiXK40.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK40.Weight = 0.64203384048896417;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK41,
            this.xrTableCell266,
            this.lblSoToKhaiXK42,
            this.xrTableCell268,
            this.lblSoToKhaiXK43,
            this.xrTableCell270,
            this.lblSoToKhaiXK44,
            this.xrTableCell272,
            this.lblSoToKhaiXK45});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK41
            // 
            this.lblSoToKhaiXK41.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK41.Name = "lblSoToKhaiXK41";
            this.lblSoToKhaiXK41.StylePriority.UseFont = false;
            this.lblSoToKhaiXK41.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK41.Weight = 0.49880052989342638;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK42
            // 
            this.lblSoToKhaiXK42.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK42.Name = "lblSoToKhaiXK42";
            this.lblSoToKhaiXK42.StylePriority.UseFont = false;
            this.lblSoToKhaiXK42.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK42.Weight = 0.49880052918255591;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK43
            // 
            this.lblSoToKhaiXK43.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK43.Name = "lblSoToKhaiXK43";
            this.lblSoToKhaiXK43.StylePriority.UseFont = false;
            this.lblSoToKhaiXK43.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK43.Weight = 0.49880052558402704;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK44
            // 
            this.lblSoToKhaiXK44.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK44.Name = "lblSoToKhaiXK44";
            this.lblSoToKhaiXK44.StylePriority.UseFont = false;
            this.lblSoToKhaiXK44.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK44.Weight = 0.49880054725170225;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK45
            // 
            this.lblSoToKhaiXK45.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK45.Name = "lblSoToKhaiXK45";
            this.lblSoToKhaiXK45.StylePriority.UseFont = false;
            this.lblSoToKhaiXK45.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK45.Weight = 0.64203384048896417;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoToKhaiXK46,
            this.xrTableCell275,
            this.lblSoToKhaiXK47,
            this.xrTableCell277,
            this.lblSoToKhaiXK48,
            this.xrTableCell279,
            this.lblSoToKhaiXK49,
            this.xrTableCell281,
            this.lblSoToKhaiXK50});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 0.67999999999999994;
            // 
            // lblSoToKhaiXK46
            // 
            this.lblSoToKhaiXK46.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK46.Name = "lblSoToKhaiXK46";
            this.lblSoToKhaiXK46.StylePriority.UseFont = false;
            this.lblSoToKhaiXK46.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK46.Weight = 0.49880052989342638;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.Weight = 0.09069100848016437;
            // 
            // lblSoToKhaiXK47
            // 
            this.lblSoToKhaiXK47.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK47.Name = "lblSoToKhaiXK47";
            this.lblSoToKhaiXK47.StylePriority.UseFont = false;
            this.lblSoToKhaiXK47.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK47.Weight = 0.49880052918255591;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.Weight = 0.090691003459894434;
            // 
            // lblSoToKhaiXK48
            // 
            this.lblSoToKhaiXK48.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK48.Name = "lblSoToKhaiXK48";
            this.lblSoToKhaiXK48.StylePriority.UseFont = false;
            this.lblSoToKhaiXK48.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK48.Weight = 0.49880052558402704;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.Weight = 0.090691007829632936;
            // 
            // lblSoToKhaiXK49
            // 
            this.lblSoToKhaiXK49.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK49.Name = "lblSoToKhaiXK49";
            this.lblSoToKhaiXK49.StylePriority.UseFont = false;
            this.lblSoToKhaiXK49.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK49.Weight = 0.49880054725170225;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.Weight = 0.090691007829632783;
            // 
            // lblSoToKhaiXK50
            // 
            this.lblSoToKhaiXK50.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXK50.Name = "lblSoToKhaiXK50";
            this.lblSoToKhaiXK50.StylePriority.UseFont = false;
            this.lblSoToKhaiXK50.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXK50.Weight = 0.64203384048896417;
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(31.0812F, 71.25002F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow21,
            this.xrTableRow29});
            this.xrTable4.SizeF = new System.Drawing.SizeF(661.5872F, 69.04169F);
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell134});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.67999999999999994;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Text = "Đơn vị Hải quan nơi gửi trả";
            this.xrTableCell132.Weight = 3.5937498663499357;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Weight = 2.3288035394117825;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.xrTableCell133,
            this.xrTableCell146,
            this.lblMaBuuChinh});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.67999999999999994;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Weight = 0.14725754945512329;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Text = "Mã bưu chính";
            this.xrTableCell133.Weight = 1.1585771908498486;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Weight = 0.046636946587686551;
            // 
            // lblMaBuuChinh
            // 
            this.lblMaBuuChinh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinh.Name = "lblMaBuuChinh";
            this.lblMaBuuChinh.StylePriority.UseFont = false;
            this.lblMaBuuChinh.Text = "XXXXXXE";
            this.lblMaBuuChinh.Weight = 4.5700817188690586;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell149,
            this.lblDiaChiBuuChinh});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.67999999999999994;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Weight = 0.14725747829279112;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "Địa chỉ";
            this.xrTableCell138.Weight = 0.73884457035703455;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Weight = 0.046636946587686579;
            // 
            // lblDiaChiBuuChinh
            // 
            this.lblDiaChiBuuChinh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiBuuChinh.Name = "lblDiaChiBuuChinh";
            this.lblDiaChiBuuChinh.StylePriority.UseFont = false;
            this.lblDiaChiBuuChinh.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWE";
            this.lblDiaChiBuuChinh.Weight = 4.9898144105242048;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell150,
            this.lblTenBuuChinh});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.67999999999999994;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Weight = 0.14725754945512326;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Text = "Tên";
            this.xrTableCell141.Weight = 0.73884446053029;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 0.044760189875822573;
            // 
            // lblTenBuuChinh
            // 
            this.lblTenBuuChinh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenBuuChinh.Name = "lblTenBuuChinh";
            this.lblTenBuuChinh.StylePriority.UseFont = false;
            this.lblTenBuuChinh.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWE";
            this.lblTenBuuChinh.Weight = 4.99169120590048;
            // 
            // lblMaVach
            // 
            this.lblMaVach.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVach.LocationFloat = new DevExpress.Utils.PointFloat(641.0215F, 17.00001F);
            this.lblMaVach.Name = "lblMaVach";
            this.lblMaVach.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaVach.SizeF = new System.Drawing.SizeF(100F, 17.00001F);
            this.lblMaVach.StylePriority.UseFont = false;
            this.lblMaVach.Text = "NNNNNNNNN1NE";
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(31.08125F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow16,
            this.xrTableRow17});
            this.xrTable3.SizeF = new System.Drawing.SizeF(381.1095F, 60F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.lblNgayPheDuyetVC,
            this.xrTableCell119});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.65939363386332572;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Text = "Ngày phê duyệt vận chuyển";
            this.xrTableCell117.Weight = 1.6137090437398389;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 0.039358759002222243;
            // 
            // lblNgayPheDuyetVC
            // 
            this.lblNgayPheDuyetVC.Name = "lblNgayPheDuyetVC";
            this.lblNgayPheDuyetVC.Text = "dd/MM/yyyy";
            this.lblNgayPheDuyetVC.Weight = 0.84693219725793889;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Weight = 0.5;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell120,
            this.xrTableCell121,
            this.lblNgayDuKienBatDauVC,
            this.xrTableCell126,
            this.lblGioDuKienBatDauVC});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.65939363386332583;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Text = "Ngày dự kiến bắt đầu vận chuyển";
            this.xrTableCell120.Weight = 1.6137093781683884;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Weight = 0.039358634772150791;
            // 
            // lblNgayDuKienBatDauVC
            // 
            this.lblNgayDuKienBatDauVC.Name = "lblNgayDuKienBatDauVC";
            this.lblNgayDuKienBatDauVC.Text = "dd/MM/yyyy";
            this.lblNgayDuKienBatDauVC.Weight = 0.67346599352973047;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Text = "-";
            this.xrTableCell126.Weight = 0.094748815621662541;
            // 
            // lblGioDuKienBatDauVC
            // 
            this.lblGioDuKienBatDauVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioDuKienBatDauVC.Name = "lblGioDuKienBatDauVC";
            this.lblGioDuKienBatDauVC.StylePriority.UseFont = false;
            this.lblGioDuKienBatDauVC.Text = "XE";
            this.lblGioDuKienBatDauVC.Weight = 0.578717177908068;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.xrTableCell128,
            this.lblNgayDuKienKetThucVC,
            this.xrTableCell130,
            this.lblGioDuKienKetThucVC});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.65939369674800508;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Text = "Ngày dự kiến kết thúc vận chuyển";
            this.xrTableCell127.Weight = 1.6137093781683884;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 0.039358634772150791;
            // 
            // lblNgayDuKienKetThucVC
            // 
            this.lblNgayDuKienKetThucVC.Name = "lblNgayDuKienKetThucVC";
            this.lblNgayDuKienKetThucVC.Text = "dd/MM/yyyy";
            this.lblNgayDuKienKetThucVC.Weight = 0.67346599352973047;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "-";
            this.xrTableCell130.Weight = 0.094748815621662541;
            // 
            // lblGioDuKienKetThucVC
            // 
            this.lblGioDuKienKetThucVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioDuKienKetThucVC.Name = "lblGioDuKienKetThucVC";
            this.lblGioDuKienKetThucVC.StylePriority.UseFont = false;
            this.lblGioDuKienKetThucVC.Text = "XE";
            this.lblGioDuKienKetThucVC.Weight = 0.578717177908068;
            // 
            // TongSoTrang
            // 
            this.TongSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(804.9999F, 4.999987F);
            this.TongSoTrang.Multiline = true;
            this.TongSoTrang.Name = "TongSoTrang";
            this.TongSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TongSoTrang.SizeF = new System.Drawing.SizeF(20F, 21.25F);
            this.TongSoTrang.StylePriority.UseTextAlignment = false;
            this.TongSoTrang.Text = "5";
            this.TongSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ThongBaoPheDuyetKhaiBaoVanChuyen_4
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.DetailReport,
            this.DetailReport1});
            this.Margins = new System.Drawing.Printing.Margins(14, 11, 9, 110);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHangHoa;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayPhatHanhVanDon;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHangHoa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHS;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell lblKyHieuSoHieu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayNhapKhoHQ;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiSP;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNuocSX;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNuocSX;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiManifest;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell lblTenPhuongTienVC2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemXuatPhat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhuongTienVC2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDuKienDenDi;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiNK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiNK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiUyThac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiUyThac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGia;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTTrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTTheTich;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDanhDauDDKhoiHanh5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChu2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDanhDauDDKhoiHanh1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDanhDauDDKhoiHanh2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanHQ;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell lblMaCoBaoYeuCauXN;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCoBaoYeuCauXN;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiVC;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell lblCoBaoXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayLapTK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDiaDiemXuatPhat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemDich;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDiaDiemDich;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiXK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiUyThac;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVBPL1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVBPL2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVBPL3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVBPL4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell lblSoGiayPhep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHetHanCapPhep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVBPL5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTTriGia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSoLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell lblTheTich;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDanhDauDDKhoiHanh3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDanhDauDDKhoiHanh4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayCapPhep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell lblGioDuKienBatDauVC;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRLabel lblMaVach;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayPheDuyetVC;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDuKienBatDauVC;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDuKienKetThucVC;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell lblGioDuKienKetThucVC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiBuuChinh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell lblTenBuuChinh;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK40;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK45;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXK50;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lblSoTrang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblSoTTDongHang;
        private DevExpress.XtraReports.UI.XRLabel TongSoTrang;
    }
}
