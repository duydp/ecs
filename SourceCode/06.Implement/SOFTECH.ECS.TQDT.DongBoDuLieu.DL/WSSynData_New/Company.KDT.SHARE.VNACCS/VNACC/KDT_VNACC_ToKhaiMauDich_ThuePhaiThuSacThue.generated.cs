using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long ThuePhaiThu_ID { set; get; }
		public string TenSacThue { set; get; }
		public decimal TieuMuc { set; get; }
		public decimal TienThue { set; get; }
		public decimal SoTienMienThue { set; get; }
		public decimal SoTienGiamThue { set; get; }
		public decimal SoThuePhaiNop { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> collection = new List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue>();
			while (reader.Read())
			{
				KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue entity = new KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThuePhaiThu_ID"))) entity.ThuePhaiThu_ID = reader.GetInt64(reader.GetOrdinal("ThuePhaiThu_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSacThue"))) entity.TenSacThue = reader.GetString(reader.GetOrdinal("TenSacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuMuc"))) entity.TieuMuc = reader.GetDecimal(reader.GetOrdinal("TieuMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienThue"))) entity.TienThue = reader.GetDecimal(reader.GetOrdinal("TienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienMienThue"))) entity.SoTienMienThue = reader.GetDecimal(reader.GetOrdinal("SoTienMienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienGiamThue"))) entity.SoTienGiamThue = reader.GetDecimal(reader.GetOrdinal("SoTienGiamThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuePhaiNop"))) entity.SoThuePhaiNop = reader.GetDecimal(reader.GetOrdinal("SoThuePhaiNop"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> collection, long id)
        {
            foreach (KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue VALUES(@ThuePhaiThu_ID, @TenSacThue, @TieuMuc, @TienThue, @SoTienMienThue, @SoTienGiamThue, @SoThuePhaiNop)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue SET ThuePhaiThu_ID = @ThuePhaiThu_ID, TenSacThue = @TenSacThue, TieuMuc = @TieuMuc, TienThue = @TienThue, SoTienMienThue = @SoTienMienThue, SoTienGiamThue = @SoTienGiamThue, SoThuePhaiNop = @SoThuePhaiNop WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, "ThuePhaiThu_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuMuc", SqlDbType.Decimal, "TieuMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienThue", SqlDbType.Decimal, "TienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienMienThue", SqlDbType.Decimal, "SoTienMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienGiamThue", SqlDbType.Decimal, "SoTienGiamThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuePhaiNop", SqlDbType.Decimal, "SoThuePhaiNop", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, "ThuePhaiThu_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuMuc", SqlDbType.Decimal, "TieuMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienThue", SqlDbType.Decimal, "TienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienMienThue", SqlDbType.Decimal, "SoTienMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienGiamThue", SqlDbType.Decimal, "SoTienGiamThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuePhaiNop", SqlDbType.Decimal, "SoThuePhaiNop", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue VALUES(@ThuePhaiThu_ID, @TenSacThue, @TieuMuc, @TienThue, @SoTienMienThue, @SoTienGiamThue, @SoThuePhaiNop)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue SET ThuePhaiThu_ID = @ThuePhaiThu_ID, TenSacThue = @TenSacThue, TieuMuc = @TieuMuc, TienThue = @TienThue, SoTienMienThue = @SoTienMienThue, SoTienGiamThue = @SoTienGiamThue, SoThuePhaiNop = @SoThuePhaiNop WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, "ThuePhaiThu_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuMuc", SqlDbType.Decimal, "TieuMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienThue", SqlDbType.Decimal, "TienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienMienThue", SqlDbType.Decimal, "SoTienMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienGiamThue", SqlDbType.Decimal, "SoTienGiamThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuePhaiNop", SqlDbType.Decimal, "SoThuePhaiNop", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, "ThuePhaiThu_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuMuc", SqlDbType.Decimal, "TieuMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienThue", SqlDbType.Decimal, "TienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienMienThue", SqlDbType.Decimal, "SoTienMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienGiamThue", SqlDbType.Decimal, "SoTienGiamThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuePhaiNop", SqlDbType.Decimal, "SoThuePhaiNop", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> SelectCollectionBy_ThuePhaiThu_ID(long thuePhaiThu_ID)
		{
            IDataReader reader = SelectReaderBy_ThuePhaiThu_ID(thuePhaiThu_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ThuePhaiThu_ID(long thuePhaiThu_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, thuePhaiThu_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_ThuePhaiThu_ID(long thuePhaiThu_ID)
		{
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, thuePhaiThu_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue(long thuePhaiThu_ID, string tenSacThue, decimal tieuMuc, decimal tienThue, decimal soTienMienThue, decimal soTienGiamThue, decimal soThuePhaiNop)
		{
			KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue entity = new KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue();	
			entity.ThuePhaiThu_ID = thuePhaiThu_ID;
			entity.TenSacThue = tenSacThue;
			entity.TieuMuc = tieuMuc;
			entity.TienThue = tienThue;
			entity.SoTienMienThue = soTienMienThue;
			entity.SoTienGiamThue = soTienGiamThue;
			entity.SoThuePhaiNop = soThuePhaiNop;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, ThuePhaiThu_ID);
			db.AddInParameter(dbCommand, "@TenSacThue", SqlDbType.NVarChar, TenSacThue);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Decimal, TieuMuc);
			db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Decimal, TienThue);
			db.AddInParameter(dbCommand, "@SoTienMienThue", SqlDbType.Decimal, SoTienMienThue);
			db.AddInParameter(dbCommand, "@SoTienGiamThue", SqlDbType.Decimal, SoTienGiamThue);
			db.AddInParameter(dbCommand, "@SoThuePhaiNop", SqlDbType.Decimal, SoThuePhaiNop);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue(long id, long thuePhaiThu_ID, string tenSacThue, decimal tieuMuc, decimal tienThue, decimal soTienMienThue, decimal soTienGiamThue, decimal soThuePhaiNop)
		{
			KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue entity = new KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue();			
			entity.ID = id;
			entity.ThuePhaiThu_ID = thuePhaiThu_ID;
			entity.TenSacThue = tenSacThue;
			entity.TieuMuc = tieuMuc;
			entity.TienThue = tienThue;
			entity.SoTienMienThue = soTienMienThue;
			entity.SoTienGiamThue = soTienGiamThue;
			entity.SoThuePhaiNop = soThuePhaiNop;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, ThuePhaiThu_ID);
			db.AddInParameter(dbCommand, "@TenSacThue", SqlDbType.NVarChar, TenSacThue);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Decimal, TieuMuc);
			db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Decimal, TienThue);
			db.AddInParameter(dbCommand, "@SoTienMienThue", SqlDbType.Decimal, SoTienMienThue);
			db.AddInParameter(dbCommand, "@SoTienGiamThue", SqlDbType.Decimal, SoTienGiamThue);
			db.AddInParameter(dbCommand, "@SoThuePhaiNop", SqlDbType.Decimal, SoThuePhaiNop);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue(long id, long thuePhaiThu_ID, string tenSacThue, decimal tieuMuc, decimal tienThue, decimal soTienMienThue, decimal soTienGiamThue, decimal soThuePhaiNop)
		{
			KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue entity = new KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue();			
			entity.ID = id;
			entity.ThuePhaiThu_ID = thuePhaiThu_ID;
			entity.TenSacThue = tenSacThue;
			entity.TieuMuc = tieuMuc;
			entity.TienThue = tienThue;
			entity.SoTienMienThue = soTienMienThue;
			entity.SoTienGiamThue = soTienGiamThue;
			entity.SoThuePhaiNop = soThuePhaiNop;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, ThuePhaiThu_ID);
			db.AddInParameter(dbCommand, "@TenSacThue", SqlDbType.NVarChar, TenSacThue);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Decimal, TieuMuc);
			db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Decimal, TienThue);
			db.AddInParameter(dbCommand, "@SoTienMienThue", SqlDbType.Decimal, SoTienMienThue);
			db.AddInParameter(dbCommand, "@SoTienGiamThue", SqlDbType.Decimal, SoTienGiamThue);
			db.AddInParameter(dbCommand, "@SoThuePhaiNop", SqlDbType.Decimal, SoThuePhaiNop);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue(long id)
		{
			KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue entity = new KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_ThuePhaiThu_ID(long thuePhaiThu_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ThuePhaiThu_ID", SqlDbType.BigInt, thuePhaiThu_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}