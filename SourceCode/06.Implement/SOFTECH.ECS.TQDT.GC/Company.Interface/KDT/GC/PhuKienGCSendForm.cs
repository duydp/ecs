﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.KDT.GC.PCTFormTemplate;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;

namespace Company.Interface.KDT.GC
{
    public partial class PhuKienGCSendForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public PhuKienDangKy pkdk = new PhuKienDangKy();
        private string xmlCurrent = "";
        //-----------------------------------------------------------------------------------------
        public PhuKienGCSendForm()
        {
            InitializeComponent();
            createLoaiPhuKien();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll();
            this._DonViTinh = DonViTinh.SelectAll();
            HD.Load();
            txtSoHopDong.Text = HD.SoHopDong;
            createLoaiPhuKien();
        }

        private void setCommandStatus()
        {
            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval ");
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else
            {
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                btnDelete.Enabled = true;
            }
        }
        //-----------------------------------------------------------------------------------------
        private void PhuKienGCSendForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();

            if (pkdk.ID > 0)
            {
                txtSoPhuKien.Text = pkdk.SoPhuKien.Trim();
                ccNgayPhuKien.Value = pkdk.NgayPhuKien;
                txtGhiChu.Text = pkdk.GhiChu;
                txtVBCP.Text = pkdk.VanBanChoPhep;
                txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                txtNguoiDuyet.Text = pkdk.NguoiDuyet;
                //cbLoaiPhuKien.Value = pkdk.MaP
            }
            dgList.DataSource = pkdk.PKCollection;
            setCommandStatus();
            if (pkdk.ID > 0)
            {
                Company.GC.BLL.KDT.SXXK.MsgSend msg = new Company.GC.BLL.KDT.SXXK.MsgSend();
                msg.master_id = this.pkdk.ID;
                msg.LoaiHS = "PK";
                if (msg.Load())
                {
                    lblTrangThai.Text = setText("Chờ xác nhận ", "Wait for confirmation");
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
            }


        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo thông tin về loại phụ kiện.
        /// </summary>
        private void createLoaiPhuKien()
        {
            cbLoaiPhuKien.DataSource = Company.GC.BLL.DuLieuChuan.LoaiPhuKien.SelectAll();
            cbLoaiPhuKien.DisplayMember = "TenLoaiPhuKien";
            cbLoaiPhuKien.ValueMember = "ID_LoaiPhuKien";
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //MLMessages("Thông tin này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_SEN11","", false);
                    return;
                }
            }

            if (pkdk.PKCollection.Count == 0)
            {
                showMsg("MSG_2702015");
                //MLMessages("Chưa nhập phụ kiện","MSG_WRN19","", false);
                return;
            }
            //try
            //{
            //    pkdk.InsertUpDateFull();
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage("Lưu thông tin vào cơ sở dữ liệu bị lỗi. Không khai báo thông tin tới hải quan được.Lỗi : " + ex.Message, false);
            //    return;
            //}
            string xmlCurrent = "";
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = pkdk.WSSend(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Phụ kiện . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LaySoTiepNhanDT()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //MLMessages("Chưa có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN01","", false);
                    return;
                }
            }
            string xmlCurrent = "";
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = pkdk.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", pkdk.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkdk.SoTiepNhan,"MSG_SEN02",pkdk.SoTiepNhan.ToString(), false);
                    lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");

                    txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702019");
                    //MLMessages("Đã hủy phụ kiện này","MSG_CAN01","", false);
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                    txtSoTiepNhan.Text = "";
                }
                else if (sendXML.func == 2)
                {
                    if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.","MSG_SEN03","", false);
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu","MSG_SEN05","", false);
                        lblTrangThai.Text = setText("Không phê duyệt", "Not Approved");
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            string xmlCurrent = "";
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 08/12/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, pkdk.GUIDSTR.Trim()));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProram();
                docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN.Trim();
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.GC.BLL.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                //this.GUIDSTR = (System.Guid.NewGuid().ToString()); ;
                //this.Update();
                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = HD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = pkdk.LayPhanHoi(pass, sendXML.msg);//linh mark
                xmlCurrent = pkdk.TQDTLayPhanHoi(pass, xml.InnerXml);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        else
                        {
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", pkdk.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkdk.SoTiepNhan,"MSG_SEN02",pkdk.SoTiepNhan.ToString(), false);
                    lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                    txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702019");
                    //MLMessages("Đã hủy phụ kiện này", "MSG_CAN01", "", false);
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                    txtSoTiepNhan.Text = "";
                }
                else if (sendXML.func == 2)
                {
                    if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";

                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN03", "", false);
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");

                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa xử lý danh sách sản phẩm này!", "MSG_STN06", "", false);
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN05", "", false);
                        lblTrangThai.Text = setText("Không phê duyệt", "Not Approved");
                        txtSoTiepNhan.Text = "";
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới phụ kiện.
        /// </summary>
        private void add()
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;

            if (pkdk.ID == 0)
            {
                pkdk.HopDong_ID = this.HD.ID;
                long id = pkdk.checkExitsSoPhuKien(txtSoPhuKien.Text.Trim());
                if (id > 0)
                {
                    // showMsg("MSG_240202", id);
                    //MLMessages("Số phụ kiện này đã được đăng ký trong danh sách phụ kiện có ID=" + id.ToString(),"MSG_WRN33",id.ToString(), false);
                    // return;
                }
                pkdk.GhiChu = txtGhiChu.Text.Trim();
                pkdk.SoPhuKien = txtSoPhuKien.Text.Trim();
                pkdk.TrangThaiXuLy = -1;
                pkdk.VanBanChoPhep = txtVBCP.Text.Trim();
                pkdk.NgayPhuKien = ccNgayPhuKien.Value;
                pkdk.NguoiDuyet = txtNguoiDuyet.Text.Trim();
                pkdk.MaHaiQuan = HD.MaHaiQuan;
                pkdk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (pkdk.ID == 0)
                    pkdk.Insert();
                else
                    pkdk.Update();
                //pkdk.InsertUpDateFull();
                this.setErro();
                setCommandStatus();

            }
            if (cbLoaiPhuKien.Value == null || cbLoaiPhuKien.Value.ToString() == "")
            {
                showMsg("MSG_2702020");
                //MLMessages("Bạn chưa chọn loại phụ kiện.","MSG_WRN11","", false);
                return;
            }
            string id_loaiphukien = cbLoaiPhuKien.Value.ToString();
            //long id = pkdk.checkPhuKienCungLoai(id_loaiphukien, pkdk.ID, pkdk.HopDong_ID);
            //if (id > 0 && id_loaiphukien != "S13" && id_loaiphukien != "T07" && id_loaiphukien!="N01")
            //{
            //    showMsg("MSG_240201", id);
            //    //MLMessages("Có phụ kiện cùng loại này chưa được hải quan duyệt nằm trong danh sách phụ kiện có ID=" + id.ToString(), "MSG_WRN32", id.ToString(), false);
            //    return;
            //}
            switch (id_loaiphukien.Trim())
            {
                case "H06": { showPKGiaHanHD(false); } break;
                case "H10": { showPKHuyHD(false); } break;
                case "H11": { showPKMoPhuKienDocLap(false); } break;
                case "N01": { showPKBoSungNPL(false); } break;
                case "N05": { showPKDieuChinhSoLuongNPL(false); } break;
                case "N06": { showPKDieuChinhDVTNPL(false); } break;
                case "N11": { showPKBoSungNPLVietNam(false); } break;
                case "S06": { showPKDieuChinhDVTSP(false); } break;
                case "S10": { showPKDieuChinhMaSP(false); } break;
                case "S13": { showPKDieuChinhChiTietMaSP(false); } break;
                case "S15": { showPKDieuChinhNhomSP(false); } break;
                case "T01": { showPKDieuChinhThietBi(false); } break;
                case "T07": { showPKBoSungThietBi(false); } break;
                case "S09": { showPKDieuChinhSLSanPham(false); } break;
            }
        }
        private void showPKDieuChinhSLSanPham(bool isAdd)
        {
            FormDieuChinhSLSP f = new FormDieuChinhSLSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            string id_loaiphukien = e.Row.Cells[1].Text.Trim();
            switch (id_loaiphukien.Trim())
            {
                case "H06": { showPKGiaHanHD(false); } break;
                case "H10": { showPKHuyHD(false); } break;
                case "H11": { showPKMoPhuKienDocLap(false); } break;
                case "N01": { showPKBoSungNPL(false); } break;
                case "N05": { showPKDieuChinhSoLuongNPL(false); } break;
                case "N06": { showPKDieuChinhDVTNPL(false); } break;
                case "N11": { showPKBoSungNPLVietNam(false); } break;
                case "S06": { showPKDieuChinhDVTSP(false); } break;
                case "S10": { showPKDieuChinhMaSP(false); } break;
                case "S13": { showPKDieuChinhChiTietMaSP(false); } break;
                case "S15": { showPKDieuChinhNhomSP(false); } break;
                case "T01": { showPKDieuChinhThietBi(false); } break;
                case "T07": { showPKBoSungThietBi(false); } break;
                case "S09": { showPKDieuChinhSLSanPham(false); } break;
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        //private static string loaipk = "";
        private void save()
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            try
            {
                HD.Load();
                pkdk.HopDong_ID = this.HD.ID;
                long id = pkdk.checkExitsSoPhuKien(txtSoPhuKien.Text.Trim());
                if (id > 0)
                {
                    //showMsg("MSG_240202", id);
                    ////MLMessages("Số phụ kiện này đã được đăng ký trong danh sách phụ kiện có ID=" + id.ToString(),"MSG_WRN33",id.ToString(), false);
                    //return;
                }
                this.Cursor = Cursors.WaitCursor;
                {
                    pkdk.GhiChu = txtGhiChu.Text.Trim();

                    pkdk.SoPhuKien = txtSoPhuKien.Text.Trim();
                    pkdk.TrangThaiXuLy = -1;
                    pkdk.VanBanChoPhep = txtVBCP.Text.Trim();
                    pkdk.NgayPhuKien = ccNgayPhuKien.Value;
                    pkdk.NguoiDuyet = txtNguoiDuyet.Text.Trim();
                    pkdk.MaHaiQuan = HD.MaHaiQuan;
                    //pkdk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    pkdk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    if (string.IsNullOrEmpty(pkdk.GUIDSTR))
                        pkdk.GUIDSTR = Guid.NewGuid().ToString();
                    if (pkdk.ID == 0)
                    {

                        pkdk.Insert();
                    }
                    else
                        pkdk.Update();

                    #region Lưu log thao tác
                    if (this.OpenType == OpenFormType.Insert)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", pkdk.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.PhuKien);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.PhuKien;
                            log.ID_DK = pkdk.ID;
                            log.GUIDSTR_DK = pkdk.GUIDSTR;
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                    }
                    else if (this.OpenType == OpenFormType.Edit)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", pkdk.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.PhuKien);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                    }
                    #endregion

                    //pkdk.InsertUpDateFull();
                    showMsg("MSG_SAV02");
                    this.setErro();
                    setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //MessageBox.Show("Có lỗi:" + ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtSoHopDong, null);
            error.SetError(ccNgayPhuKien, null);
            //error.SetError(txtMaHS, null);
            error.SetError(txtNguoiDuyet, null);
            error.SetError(txtVBCP, null);
            error.SetError(cbLoaiPhuKien, null);

        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSend":
                    this.send();
                    break;
                case "HuyKhaiBao":
                    this.Huy();
                    break;
                case "NhanDuLieuPK":
                    this.NhanDuLieuPK12();
                    break;
                case "XacNhanThongTin":
                    this.LaySoTiepNhanDT();
                    break;
                case "InPhieuTN": this.inPhieuTN(); break;
            }
        }
        private void inPhieuTN()
        {
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "PHỤ KIỆN";
            phieuTN.soTN = this.pkdk.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.pkdk.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = pkdk.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        public void NhanDuLieuPK12()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //MLMessages("thông tin này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_SEN11","", false);
                    return;
                }
            }
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                //DATLMQ comment 09/01/2011
                //string xmlCurrent = pkdk.WSDownLoad(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        public void Huy()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //MLMessages("Thông tin này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_SEN11","", false);
                    return;
                }
            }

            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string xmlCurrent = pkdk.WSCancel(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void showPKGiaHanHD(bool isAdd)
        {
            PKGiahanHDForm f = new PKGiahanHDForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void showPKHuyHD(bool isAdd)
        {
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "H10")
                {
                    return;
                }
            }
            if (isAdd)
            {
                try
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "H10";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    else
                        LoaiPK.Update();
                    pkdk.PKCollection.Add(LoaiPK);
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKMoPhuKienDocLap(bool isAdd)
        {
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "H11")
                {
                    return;
                }
            }
            if (isAdd)
            {
                try
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "H11";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    else
                        LoaiPK.Update();
                    pkdk.PKCollection.Add(LoaiPK);
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKBoSungNPL(bool isAdd)
        {
            NguyenPhuLieuGCBoSungForm f = new NguyenPhuLieuGCBoSungForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhSoLuongNPL(bool isAdd)
        {
            FormDieuChinhSLNPL f = new FormDieuChinhSLNPL();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhDVTNPL(bool isAdd)
        {
            FormDieuChinhDVTNPL f = new FormDieuChinhDVTNPL();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKBoSungNPLVietNam(bool isAdd)
        {
            FormNPLNhapVN f = new FormNPLNhapVN();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhDVTSP(bool isAdd)
        {
            FormDieuChinhDVTSP f = new FormDieuChinhDVTSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhMaSP(bool isAdd)
        {
            FormDieuChinhMaSP f = new FormDieuChinhMaSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhChiTietMaSP(bool isAdd)
        {
            SanPhamGCBoSungForm f = new SanPhamGCBoSungForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhNhomSP(bool isAdd)
        {

            FormBoSungNhomSP f = new FormBoSungNhomSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKBoSungThietBi(bool isAdd)
        {
            ThietBiGCBoSungForm f = new ThietBiGCBoSungForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhThietBi(bool isAdd)
        {
            FormDieuChinhSLThietBi f = new FormDieuChinhSLThietBi();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        public bool CheckExitNPLInMuaVN()
        {
            Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
            Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPKMuaVN = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "N01")
                {
                    LoaiPK = pk;
                    break;
                }
            }

            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "N11")
                {
                    LoaiPKMuaVN = pk;
                    break;
                }
            }
            foreach (HangPhuKien HangPKNPLBoSung in LoaiPK.HPKCollection)
            {
                foreach (HangPhuKien HangPK in LoaiPKMuaVN.HPKCollection)
                {
                    if (HangPK.MaHang.Trim().ToUpper() == HangPKNPLBoSung.MaHang.Trim().ToUpper())
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = (Company.GC.BLL.KDT.GC.LoaiPhuKien)e.Row.DataRow;
                    if (LoaiPK.ID > 0)
                    {
                        if (LoaiPK.MaPhuKien.Trim() == "N01")
                        {
                            if (CheckExitNPLInMuaVN())
                            {
                                showMsg("MSG_2702067");
                                e.Cancel = true;
                                return;
                            }
                        }
                        LoaiPK.Delete(this.pkdk.HopDong_ID);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void txtSoPhuKien_TextChanged(object sender, EventArgs e)
        {
            // this.isUpdate = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                Company.GC.BLL.KDT.GC.LoaiPhuKienCollection loaiPKColl = new LoaiPhuKienCollection();
                foreach (GridEXSelectedItem item in items)
                {
                    if (item.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = (Company.GC.BLL.KDT.GC.LoaiPhuKien)item.GetRow().DataRow;
                        loaiPKColl.Add(LoaiPK);
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien item in loaiPKColl)
                {
                    if (item.ID > 0)
                    {
                        if (item.MaPhuKien.Trim() == "N01")
                        {
                            if (CheckExitNPLInMuaVN())
                            {
                                showMsg("MSG_2702067");
                                //MLMessages("Danh sách nguyên phụ liệu bổ sung có nguyên phụ liệu liên quan tới bên loại phụ kiện mua Việt Nam nên không xóa được.","MSG_WRN34","", false);
                                return;
                            }
                        }
                        pkdk.PKCollection.Remove(item);
                    }
                }
                dgList.DataSource = pkdk.PKCollection;
                dgList.Refetch();
            }
        }

    }
}
