using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class SelectNPLBySanPhamForm : BaseForm
    {
        public NguyenPhuLieuCollection NPLCollectionSelect = new NguyenPhuLieuCollection();
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        public string MaSanPham;
        NguyenPhuLieuCollection NPLTMPCollection = new NguyenPhuLieuCollection();
        public NguyenPhuLieu NPL = new NguyenPhuLieu();
        public SelectNPLBySanPhamForm()
        {
            InitializeComponent();
        }
        private void RemoveNPL(NguyenPhuLieu NPL, NguyenPhuLieuCollection NPLCollection)
        {
            foreach (NguyenPhuLieu NPLDelete in NPLCollection)
            {
                if (NPLDelete.Ma == NPL.Ma)
                {
                    NPLCollection.Remove(NPLDelete);
                    break;
                }
            }
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
         
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void dgListSelect_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
             this.Close();
        }

        private void SelectNPLForm_Load(object sender, EventArgs e)
        {
            this.NPLCollection = NPL.GetNPLTheoSanPham(MaSanPham, MaHaiQuan, MaDoanhNghiep);
            dgList.DataSource = this.NPLCollection;
            dgList.Refetch();
          
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            NPL = (NguyenPhuLieu)dgList.CurrentRow.DataRow;
            this.Close();
        }

     
    }
}
