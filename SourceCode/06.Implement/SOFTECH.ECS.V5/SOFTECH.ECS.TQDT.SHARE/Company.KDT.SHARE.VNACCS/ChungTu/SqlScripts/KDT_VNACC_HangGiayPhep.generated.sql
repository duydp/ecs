-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Insert]
	@GiayPhepType varchar(3),
	@GiayPhep_ID bigint,
	@TenHangHoa nvarchar(768),
	@MaSoHangHoa varchar(12),
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@KhoiLuong numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@XuatXu varchar(2),
	@TinhBiet varchar(1),
	@Tuoi numeric(3, 0),
	@NoiSanXuat nvarchar(150),
	@QuyCachDongGoi nvarchar(50),
	@TongSoLuongNhapKhau numeric(8, 0),
	@DonViTinhTongSoLuongNhapKhau varchar(3),
	@KichCoCaThe nvarchar(10),
	@TrongLuongTinh numeric(10, 3),
	@DonViTinhTrongLuong varchar(3),
	@TrongLuongCaBi numeric(10, 3),
	@DonViTinhTrongLuongCaBi varchar(3),
	@SoLuongKiemDich numeric(8, 0),
	@DonViTinhSoLuongKiemDich varchar(3),
	@LoaiBaoBi nvarchar(100),
	@MucDichSuDung nvarchar(100),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep]
(
	[GiayPhepType],
	[GiayPhep_ID],
	[TenHangHoa],
	[MaSoHangHoa],
	[SoLuong],
	[DonVitinhSoLuong],
	[KhoiLuong],
	[DonVitinhKhoiLuong],
	[XuatXu],
	[TinhBiet],
	[Tuoi],
	[NoiSanXuat],
	[QuyCachDongGoi],
	[TongSoLuongNhapKhau],
	[DonViTinhTongSoLuongNhapKhau],
	[KichCoCaThe],
	[TrongLuongTinh],
	[DonViTinhTrongLuong],
	[TrongLuongCaBi],
	[DonViTinhTrongLuongCaBi],
	[SoLuongKiemDich],
	[DonViTinhSoLuongKiemDich],
	[LoaiBaoBi],
	[MucDichSuDung],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@GiayPhepType,
	@GiayPhep_ID,
	@TenHangHoa,
	@MaSoHangHoa,
	@SoLuong,
	@DonVitinhSoLuong,
	@KhoiLuong,
	@DonVitinhKhoiLuong,
	@XuatXu,
	@TinhBiet,
	@Tuoi,
	@NoiSanXuat,
	@QuyCachDongGoi,
	@TongSoLuongNhapKhau,
	@DonViTinhTongSoLuongNhapKhau,
	@KichCoCaThe,
	@TrongLuongTinh,
	@DonViTinhTrongLuong,
	@TrongLuongCaBi,
	@DonViTinhTrongLuongCaBi,
	@SoLuongKiemDich,
	@DonViTinhSoLuongKiemDich,
	@LoaiBaoBi,
	@MucDichSuDung,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Update]
	@ID bigint,
	@GiayPhepType varchar(3),
	@GiayPhep_ID bigint,
	@TenHangHoa nvarchar(768),
	@MaSoHangHoa varchar(12),
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@KhoiLuong numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@XuatXu varchar(2),
	@TinhBiet varchar(1),
	@Tuoi numeric(3, 0),
	@NoiSanXuat nvarchar(150),
	@QuyCachDongGoi nvarchar(50),
	@TongSoLuongNhapKhau numeric(8, 0),
	@DonViTinhTongSoLuongNhapKhau varchar(3),
	@KichCoCaThe nvarchar(10),
	@TrongLuongTinh numeric(10, 3),
	@DonViTinhTrongLuong varchar(3),
	@TrongLuongCaBi numeric(10, 3),
	@DonViTinhTrongLuongCaBi varchar(3),
	@SoLuongKiemDich numeric(8, 0),
	@DonViTinhSoLuongKiemDich varchar(3),
	@LoaiBaoBi nvarchar(100),
	@MucDichSuDung nvarchar(100),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangGiayPhep]
SET
	[GiayPhepType] = @GiayPhepType,
	[GiayPhep_ID] = @GiayPhep_ID,
	[TenHangHoa] = @TenHangHoa,
	[MaSoHangHoa] = @MaSoHangHoa,
	[SoLuong] = @SoLuong,
	[DonVitinhSoLuong] = @DonVitinhSoLuong,
	[KhoiLuong] = @KhoiLuong,
	[DonVitinhKhoiLuong] = @DonVitinhKhoiLuong,
	[XuatXu] = @XuatXu,
	[TinhBiet] = @TinhBiet,
	[Tuoi] = @Tuoi,
	[NoiSanXuat] = @NoiSanXuat,
	[QuyCachDongGoi] = @QuyCachDongGoi,
	[TongSoLuongNhapKhau] = @TongSoLuongNhapKhau,
	[DonViTinhTongSoLuongNhapKhau] = @DonViTinhTongSoLuongNhapKhau,
	[KichCoCaThe] = @KichCoCaThe,
	[TrongLuongTinh] = @TrongLuongTinh,
	[DonViTinhTrongLuong] = @DonViTinhTrongLuong,
	[TrongLuongCaBi] = @TrongLuongCaBi,
	[DonViTinhTrongLuongCaBi] = @DonViTinhTrongLuongCaBi,
	[SoLuongKiemDich] = @SoLuongKiemDich,
	[DonViTinhSoLuongKiemDich] = @DonViTinhSoLuongKiemDich,
	[LoaiBaoBi] = @LoaiBaoBi,
	[MucDichSuDung] = @MucDichSuDung,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]
	@ID bigint,
	@GiayPhepType varchar(3),
	@GiayPhep_ID bigint,
	@TenHangHoa nvarchar(768),
	@MaSoHangHoa varchar(12),
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@KhoiLuong numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@XuatXu varchar(2),
	@TinhBiet varchar(1),
	@Tuoi numeric(3, 0),
	@NoiSanXuat nvarchar(150),
	@QuyCachDongGoi nvarchar(50),
	@TongSoLuongNhapKhau numeric(8, 0),
	@DonViTinhTongSoLuongNhapKhau varchar(3),
	@KichCoCaThe nvarchar(10),
	@TrongLuongTinh numeric(10, 3),
	@DonViTinhTrongLuong varchar(3),
	@TrongLuongCaBi numeric(10, 3),
	@DonViTinhTrongLuongCaBi varchar(3),
	@SoLuongKiemDich numeric(8, 0),
	@DonViTinhSoLuongKiemDich varchar(3),
	@LoaiBaoBi nvarchar(100),
	@MucDichSuDung nvarchar(100),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangGiayPhep] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangGiayPhep] 
		SET
			[GiayPhepType] = @GiayPhepType,
			[GiayPhep_ID] = @GiayPhep_ID,
			[TenHangHoa] = @TenHangHoa,
			[MaSoHangHoa] = @MaSoHangHoa,
			[SoLuong] = @SoLuong,
			[DonVitinhSoLuong] = @DonVitinhSoLuong,
			[KhoiLuong] = @KhoiLuong,
			[DonVitinhKhoiLuong] = @DonVitinhKhoiLuong,
			[XuatXu] = @XuatXu,
			[TinhBiet] = @TinhBiet,
			[Tuoi] = @Tuoi,
			[NoiSanXuat] = @NoiSanXuat,
			[QuyCachDongGoi] = @QuyCachDongGoi,
			[TongSoLuongNhapKhau] = @TongSoLuongNhapKhau,
			[DonViTinhTongSoLuongNhapKhau] = @DonViTinhTongSoLuongNhapKhau,
			[KichCoCaThe] = @KichCoCaThe,
			[TrongLuongTinh] = @TrongLuongTinh,
			[DonViTinhTrongLuong] = @DonViTinhTrongLuong,
			[TrongLuongCaBi] = @TrongLuongCaBi,
			[DonViTinhTrongLuongCaBi] = @DonViTinhTrongLuongCaBi,
			[SoLuongKiemDich] = @SoLuongKiemDich,
			[DonViTinhSoLuongKiemDich] = @DonViTinhSoLuongKiemDich,
			[LoaiBaoBi] = @LoaiBaoBi,
			[MucDichSuDung] = @MucDichSuDung,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep]
		(
			[GiayPhepType],
			[GiayPhep_ID],
			[TenHangHoa],
			[MaSoHangHoa],
			[SoLuong],
			[DonVitinhSoLuong],
			[KhoiLuong],
			[DonVitinhKhoiLuong],
			[XuatXu],
			[TinhBiet],
			[Tuoi],
			[NoiSanXuat],
			[QuyCachDongGoi],
			[TongSoLuongNhapKhau],
			[DonViTinhTongSoLuongNhapKhau],
			[KichCoCaThe],
			[TrongLuongTinh],
			[DonViTinhTrongLuong],
			[TrongLuongCaBi],
			[DonViTinhTrongLuongCaBi],
			[SoLuongKiemDich],
			[DonViTinhSoLuongKiemDich],
			[LoaiBaoBi],
			[MucDichSuDung],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@GiayPhepType,
			@GiayPhep_ID,
			@TenHangHoa,
			@MaSoHangHoa,
			@SoLuong,
			@DonVitinhSoLuong,
			@KhoiLuong,
			@DonVitinhKhoiLuong,
			@XuatXu,
			@TinhBiet,
			@Tuoi,
			@NoiSanXuat,
			@QuyCachDongGoi,
			@TongSoLuongNhapKhau,
			@DonViTinhTongSoLuongNhapKhau,
			@KichCoCaThe,
			@TrongLuongTinh,
			@DonViTinhTrongLuong,
			@TrongLuongCaBi,
			@DonViTinhTrongLuongCaBi,
			@SoLuongKiemDich,
			@DonViTinhSoLuongKiemDich,
			@LoaiBaoBi,
			@MucDichSuDung,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangGiayPhep]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhepType],
	[GiayPhep_ID],
	[TenHangHoa],
	[MaSoHangHoa],
	[SoLuong],
	[DonVitinhSoLuong],
	[KhoiLuong],
	[DonVitinhKhoiLuong],
	[XuatXu],
	[TinhBiet],
	[Tuoi],
	[NoiSanXuat],
	[QuyCachDongGoi],
	[TongSoLuongNhapKhau],
	[DonViTinhTongSoLuongNhapKhau],
	[KichCoCaThe],
	[TrongLuongTinh],
	[DonViTinhTrongLuong],
	[TrongLuongCaBi],
	[DonViTinhTrongLuongCaBi],
	[SoLuongKiemDich],
	[DonViTinhSoLuongKiemDich],
	[LoaiBaoBi],
	[MucDichSuDung],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhepType],
	[GiayPhep_ID],
	[TenHangHoa],
	[MaSoHangHoa],
	[SoLuong],
	[DonVitinhSoLuong],
	[KhoiLuong],
	[DonVitinhKhoiLuong],
	[XuatXu],
	[TinhBiet],
	[Tuoi],
	[NoiSanXuat],
	[QuyCachDongGoi],
	[TongSoLuongNhapKhau],
	[DonViTinhTongSoLuongNhapKhau],
	[KichCoCaThe],
	[TrongLuongTinh],
	[DonViTinhTrongLuong],
	[TrongLuongCaBi],
	[DonViTinhTrongLuongCaBi],
	[SoLuongKiemDich],
	[DonViTinhSoLuongKiemDich],
	[LoaiBaoBi],
	[MucDichSuDung],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_HangGiayPhep] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhepType],
	[GiayPhep_ID],
	[TenHangHoa],
	[MaSoHangHoa],
	[SoLuong],
	[DonVitinhSoLuong],
	[KhoiLuong],
	[DonVitinhKhoiLuong],
	[XuatXu],
	[TinhBiet],
	[Tuoi],
	[NoiSanXuat],
	[QuyCachDongGoi],
	[TongSoLuongNhapKhau],
	[DonViTinhTongSoLuongNhapKhau],
	[KichCoCaThe],
	[TrongLuongTinh],
	[DonViTinhTrongLuong],
	[TrongLuongCaBi],
	[DonViTinhTrongLuongCaBi],
	[SoLuongKiemDich],
	[DonViTinhSoLuongKiemDich],
	[LoaiBaoBi],
	[MucDichSuDung],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep]	

GO

