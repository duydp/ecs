﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using System.Text;
using System.Security.Cryptography;

namespace Company.Interface
{
    public partial class WSDBDLForm : BaseForm
    {
        public bool IsReady = false;
        public static bool IsSuccess = false;
        public WSDBDLForm()
        {
            InitializeComponent();
        }

        private void WSForm_Load(object sender, EventArgs e)
        {
            //txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(txtMaDoanhNghiep.Text) && !string.IsNullOrEmpty(txtMatKhau.Text))
            {
                //Company.KDT.SHARE.Components.ISyncData wsSync = Company.KDT.SHARE.Components.WebService.SyncService();
                //wsSync.Se
                if (ckLuu.Checked)
                {
                    GlobalSettings.USERNAME_DONGBO = txtMaDoanhNghiep.Text;
                    GlobalSettings.PASSWOR_DONGBO = txtMatKhau.Text;
                }
                IsReady = true;
                this.Close();
            }
            else
            {
                ShowMessage("Chưa nhập [Tên đăng nhâp] hoặc [Mật khẩu] ", false);
                return;
            }
        }
        private void WSForm2_FormClosing(object sender, FormClosingEventArgs e)
        {
            //IsReady = false;
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            IsReady = false;
        }
      
    }
}