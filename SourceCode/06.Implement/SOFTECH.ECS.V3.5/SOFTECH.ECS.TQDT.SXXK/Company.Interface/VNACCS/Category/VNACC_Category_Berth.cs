﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
namespace Company.Interface.VNACCS.Category
{
    public partial class VNACC_Category_Berth : BaseForm
    {
        public Company.KDT.SHARE.VNACCS.VNACC_Category_Berth berth ;
        public string where = "" ;
        public VNACC_Category_Berth()
        {
            InitializeComponent();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                
                if (e.Row.RowType == RowType.Record)
                {
                    berth = new Company.KDT.SHARE.VNACCS.VNACC_Category_Berth();
                    berth = (Company.KDT.SHARE.VNACCS.VNACC_Category_Berth)e.Row.DataRow;
                    Set();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_Category_Berth_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMaCang, errorProvider, "Mã cảng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenCang, errorProvider, "Tên cảng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenMoTa, errorProvider, "Tên mô tả hệ thống", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChi, errorProvider, "Địa chỉ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void Get()
        {
            try
            {
                berth.Code = txtMaCang.Text.Trim().ToUpper();
                berth.MoTa_HeThong = txtTenMoTa.Text.Trim().ToUpper();
                berth.DiaChi = txtDiaChi.Text.Trim().ToUpper();
                berth.TenCang = txtTenCang.Text.Trim().ToUpper();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void Set()
        {
            try
            {
                txtMaCang.Text = berth.Code;
                txtTenCang.Text = berth.TenCang;
                txtTenMoTa.Text = berth.MoTa_HeThong;
                txtDiaChi.Text = berth.DiaChi;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtMaCang.Text))
                    where += " AND Code = '" + txtMaCang.Text.Trim() + "'";
                if (!String.IsNullOrEmpty(txtTenCang.Text))
                    where += " AND TenCang LIKE N'%" + txtTenCang.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtTenMoTa.Text))
                    where += " AND MoTa_HeThong LIKE N'%" + txtTenMoTa.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtDiaChi.Text))
                    where += " AND DiaChi LIKE N'%" + txtDiaChi.Text.Trim() + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return where;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                Get();
                berth.ReferenceDB = "A305";
                berth.InsertUpdate();
                ShowMessage("Lưu thành công ", false);
                berth = new Company.KDT.SHARE.VNACCS.VNACC_Category_Berth();
                ClearText();
                btnSearch_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ClearText()
        {
            //txtMaCang.Text = String.Empty;
            txtTenCang.Text = String.Empty;
            txtTenMoTa.Text = String.Empty;
            txtDiaChi.Text = String.Empty;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Get();
                if (ShowMessage("Bạn có muốn xóa dòng này không ? ", true) == "Yes")
                {
                    berth.Delete();
                }
                ShowMessage("Xóa thành công ",false);
                berth = new Company.KDT.SHARE.VNACCS.VNACC_Category_Berth();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = Company.KDT.SHARE.VNACCS.VNACC_Category_Berth.SelectCollectionDynamic(GetSearchWhere(),null);
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

    }
}
