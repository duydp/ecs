﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class Machine
    {
        /// <summary>
        /// Số lượng sở hữu
        /// </summary>
        [XmlElement("ownedQuantity")]
        public decimal OwnedQuantity { get; set; }

        /// <summary>
        /// Số lượng đi thuê
        /// </summary>
        [XmlElement("rentQuantity")]
        public decimal RentQuantity { get; set; }

        /// <summary>
        /// Số lượng khác
        /// </summary>
        [XmlElement("otherQuantity")]
        public decimal OtherQuantity { get; set; }

        /// <summary>
        /// Tổng số lượng
        /// </summary>
        [XmlElement("totalQuantity")]
        public decimal TotalQuantity { get; set; }

        /// <summary>
        /// Năng lực sản xuất
        /// </summary>
        [XmlElement("productionCapacity")]
        public string ProductionCapacity { get; set; }
    }
}
