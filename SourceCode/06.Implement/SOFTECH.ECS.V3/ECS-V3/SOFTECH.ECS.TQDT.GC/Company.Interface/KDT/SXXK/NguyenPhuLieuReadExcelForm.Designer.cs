namespace Company.Interface.KDT.SXXK
{
    partial class NguyenPhuLieuReadExcelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NguyenPhuLieuReadExcelForm));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDonGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDVTColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSelectFile = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSP = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvRow = new Company.Controls.CustomValidation.RangeValidator();
            this.lblLinkExcel = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvRow)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.lblLinkExcel);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(691, 201);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excel 97 - 2003 files (*.xls)|*.xls";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(173, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cột mã NPL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(258, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cột tên NPL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(345, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cột mã HS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(427, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Cột ĐVT";
            // 
            // txtRow
            // 
            this.txtRow.Location = new System.Drawing.Point(104, 40);
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(66, 21);
            this.txtRow.TabIndex = 3;
            this.txtRow.Text = "2";
            this.txtRow.Value = 2;
            this.txtRow.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRow.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Sheet";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(102, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Dòng đầu";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.txtDonGia);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.txtSoLuong);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtSheet);
            this.uiGroupBox1.Controls.Add(this.txtDVTColumn);
            this.uiGroupBox1.Controls.Add(this.txtMaHSColumn);
            this.uiGroupBox1.Controls.Add(this.txtTenHangColumn);
            this.uiGroupBox1.Controls.Add(this.txtMaHangColumn);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtRow);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(664, 83);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(485, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(409, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(328, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(243, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(160, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(82, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "*";
            // 
            // txtDonGia
            // 
            this.txtDonGia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGia.Location = new System.Drawing.Point(584, 40);
            this.txtDonGia.MaxLength = 1;
            this.txtDonGia.Name = "txtDonGia";
            this.txtDonGia.Size = new System.Drawing.Size(71, 21);
            this.txtDonGia.TabIndex = 14;
            this.txtDonGia.Text = "F";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(581, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Cột đơn giá";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuong.Location = new System.Drawing.Point(507, 40);
            this.txtSoLuong.MaxLength = 1;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(71, 21);
            this.txtSoLuong.TabIndex = 13;
            this.txtSoLuong.Text = "E";
            this.txtSoLuong.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(504, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Cột số lượng";
            // 
            // txtSheet
            // 
            this.txtSheet.Location = new System.Drawing.Point(14, 40);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(84, 21);
            this.txtSheet.TabIndex = 1;
            this.txtSheet.VisualStyleManager = this.vsmMain;
            // 
            // txtDVTColumn
            // 
            this.txtDVTColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTColumn.Location = new System.Drawing.Point(430, 40);
            this.txtDVTColumn.MaxLength = 1;
            this.txtDVTColumn.Name = "txtDVTColumn";
            this.txtDVTColumn.Size = new System.Drawing.Size(71, 21);
            this.txtDVTColumn.TabIndex = 11;
            this.txtDVTColumn.Text = "D";
            this.txtDVTColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHSColumn
            // 
            this.txtMaHSColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHSColumn.Location = new System.Drawing.Point(345, 40);
            this.txtMaHSColumn.MaxLength = 1;
            this.txtMaHSColumn.Name = "txtMaHSColumn";
            this.txtMaHSColumn.Size = new System.Drawing.Size(77, 21);
            this.txtMaHSColumn.TabIndex = 9;
            this.txtMaHSColumn.Text = "C";
            this.txtMaHSColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtTenHangColumn
            // 
            this.txtTenHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangColumn.Location = new System.Drawing.Point(260, 40);
            this.txtTenHangColumn.MaxLength = 1;
            this.txtTenHangColumn.Name = "txtTenHangColumn";
            this.txtTenHangColumn.Size = new System.Drawing.Size(80, 21);
            this.txtTenHangColumn.TabIndex = 7;
            this.txtTenHangColumn.Text = "B";
            this.txtTenHangColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHangColumn
            // 
            this.txtMaHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangColumn.Location = new System.Drawing.Point(176, 40);
            this.txtMaHangColumn.MaxLength = 1;
            this.txtMaHangColumn.Name = "txtMaHangColumn";
            this.txtMaHangColumn.Size = new System.Drawing.Size(78, 21);
            this.txtMaHangColumn.TabIndex = 5;
            this.txtMaHangColumn.Text = "A";
            this.txtMaHangColumn.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.btnSelectFile);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 107);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(664, 53);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập     ";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(183, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "*";
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSelectFile.Icon")));
            this.btnSelectFile.Location = new System.Drawing.Point(584, 17);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(70, 23);
            this.btnSelectFile.TabIndex = 1;
            this.btnSelectFile.Text = "Browse";
            this.btnSelectFile.VisualStyleManager = this.vsmMain;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(13, 19);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(565, 21);
            this.txtFilePath.TabIndex = 0;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(607, 167);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(68, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(533, 167);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(68, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Ghi";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cvError
            // 
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaHangColumn;
            this.rfvMa.ErrorMessage = "\"Cột mã NPL\" bắt buộc phải nhập.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.txtSheet;
            this.rfvTenSheet.ErrorMessage = "\"Tên sheet\" bắt buộc phải nhập.";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            this.rfvTenSheet.Tag = "rfvTenSheet";
            // 
            // rfvDVT
            // 
            this.rfvDVT.ControlToValidate = this.txtDVTColumn;
            this.rfvDVT.ErrorMessage = "\"Cột đơn vị tính\" bắt buộc phải chọn.";
            this.rfvDVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDVT.Icon")));
            this.rfvDVT.Tag = "rfvDVT";
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "\"Tên file không được để trống\"";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            this.rfvFilePath.Tag = "rfvFilePath";
            // 
            // rfvTenSP
            // 
            this.rfvTenSP.ControlToValidate = this.txtTenHangColumn;
            this.rfvTenSP.ErrorMessage = "\"Cột tên NPL\" bắt buộc phải chọn.";
            this.rfvTenSP.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSP.Icon")));
            this.rfvTenSP.Tag = "rfvTenSP";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHSColumn;
            this.rfvMaHS.ErrorMessage = "\"Cột mã HS\" bắt buộc phải chọn.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // rvRow
            // 
            this.rvRow.ControlToValidate = this.txtRow;
            this.rvRow.ErrorMessage = "\"Dòng bắt đầu\" không hợp lệ";
            this.rvRow.Icon = ((System.Drawing.Icon)(resources.GetObject("rvRow.Icon")));
            this.rvRow.MaximumValue = "1000";
            this.rvRow.MinimumValue = "1";
            this.rvRow.Tag = "rvRow";
            this.rvRow.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // lblLinkExcel
            // 
            this.lblLinkExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLinkExcel.AutoSize = true;
            this.lblLinkExcel.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkExcel.Location = new System.Drawing.Point(22, 177);
            this.lblLinkExcel.Name = "lblLinkExcel";
            this.lblLinkExcel.Size = new System.Drawing.Size(106, 13);
            this.lblLinkExcel.TabIndex = 220;
            this.lblLinkExcel.TabStop = true;
            this.lblLinkExcel.Text = "Mở tệp tin mẫu Excel";
            this.lblLinkExcel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLinkExcel_LinkClicked);
            // 
            // NguyenPhuLieuReadExcelForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(691, 201);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(697, 229);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(697, 229);
            this.Name = "NguyenPhuLieuReadExcelForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đọc danh sách nguyên phụ liệu từ file Excel";
            this.Load += new System.EventHandler(this.NguyenPhuLieuReadExcelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvRow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnSelectFile;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTColumn;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDVT;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSP;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuong;
        private System.Windows.Forms.Label label1;
        private Company.Controls.CustomValidation.RangeValidator rvRow;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGia;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.LinkLabel lblLinkExcel;
    }
}