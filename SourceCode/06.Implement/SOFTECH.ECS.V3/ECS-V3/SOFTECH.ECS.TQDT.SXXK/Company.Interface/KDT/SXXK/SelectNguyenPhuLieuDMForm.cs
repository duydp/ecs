using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class SelectNguyenPhuLieuDMForm : BaseForm
    {
        public NguyenPhuLieu NguyenPhuLieuSelected = new NguyenPhuLieu();
        public bool status = true;//0 mo moi hoan toan//1 da co roi mo len
        private DataSet dsRegistedList;
        public SelectNguyenPhuLieuDMForm()
        {
            InitializeComponent();
            this.ctrDonViHaiQuan.ValueChanged -= new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(this.ctrDonViHaiQuan_ValueChanged);            
     
        }
        public void BindDanhSachNPL(string mahq)
        {
            string where = "TrangThaiXuLy<>1";
            if (mahq != "")
                where += " and MaHaiQuan='" + mahq + "' AND MaDoanhNghiep ='"+ GlobalSettings.MA_DON_VI+"'";
            Company.BLL.KDT.SXXK.NguyenPhuLieuDangKyCollection collection = Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.SelectCollectionDynamic(where, "");
            Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy npldk = new Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy();
            npldk.ID = 0;
            npldk.SoTiepNhan = 0;
            collection.Add(npldk);
       }
        public void BindData()
        {
            dgList.DataSource = new NguyenPhuLieu().SelectDynamic("MaDoanhNghiep = '"+ GlobalSettings.MA_DON_VI+ "' AND MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN+ "'","").Tables[0];            
        }
        //-----------------------------------------------------------------------------------------
        
        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            
            if (this.CalledForm == "DinhMucSendForm")
            {
                ctrDonViHaiQuan.Enabled = false;
            }
            
            lblHint.Visible = this.CalledForm != string.Empty;
        }
        private void dsSanPham_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TrangThaiXL"].Text == "0" && e.Row.Cells["ID"].Text != "0")
                    e.Row.Cells["TrangThaiXL"].Text = "Chờ duyệt";
                else if (e.Row.Cells["TrangThaiXL"].Text == "-1" && e.Row.Cells["ID"].Text != "0")
                    e.Row.Cells["TrangThaiXL"].Text = "Chưa khai báo";

                if (e.Row.Cells["ID"].Text != "0")
                {
                    e.Row.Cells["ThongTin"].Text = "Danh sách sản phẩm " + e.Row.Cells["ID"].Text;
                }
                else
                {
                    e.Row.Cells["ThongTin"].Text = "Danh sách đã duyệt";
                }
            }
        }
     
        //-----------------------------------------------------------------------------------------
        
        private void NguyenPhuLieuRegistedForm_Load(object sender, EventArgs e)
        {
            if (status)
            {
                this.khoitao_DuLieuChuan();
                // Nguyên phụ liệu đã đăng ký.
                this.BindDanhSachNPL(ctrDonViHaiQuan.Ma);
                this.BindData();
                // Doanh nghiệp / Đại lý TTHQ.
                dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;
                this.ctrDonViHaiQuan.ValueChanged += new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(this.ctrDonViHaiQuan_ValueChanged);                
                status = false;
            }
        }

        //-----------------------------------------------------------------------------------------
        
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------
        
   
        //-----------------------------------------------------------------------------------------
        
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (lblHint.Visible)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string maNPL = e.Row.Cells["Ma"].Text;
                    this.NguyenPhuLieuSelected.Ma = maNPL;
                    this.NguyenPhuLieuSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; //this.MaHaiQuan.Trim().Trim(); Hungtq comment 16/12/2010
                    this.NguyenPhuLieuSelected.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.NguyenPhuLieuSelected.Ten = e.Row.Cells["Ten"].Text;
                    this.NguyenPhuLieuSelected.MaHS = e.Row.Cells["MaHS"].Text;
                    this.NguyenPhuLieuSelected.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                    this.NguyenPhuLieuSelected.Load();
                    this.Hide();
                }
            }
            else
            {
  
            }
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {            
                dgList.DataSource = new SanPham().SearchBy_MaHaiQuan(ctrDonViHaiQuan.Ma).Tables[0];          
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void dsSanPham_ValueChanged(object sender, EventArgs e)
        {
            this.ctrDonViHaiQuan.ValueChanged -= new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(this.ctrDonViHaiQuan_ValueChanged);
            BindData();
            this.ctrDonViHaiQuan.ValueChanged += new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(this.ctrDonViHaiQuan_ValueChanged);          
   
        }

        //-----------------------------------------------------------------------------------------
    }
}