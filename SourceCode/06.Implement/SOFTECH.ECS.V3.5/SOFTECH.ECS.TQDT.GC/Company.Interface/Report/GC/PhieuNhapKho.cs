﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Collections.Generic;

namespace Company.Interface.Report.GC
{
    public partial class PhieuNhapKho : DevExpress.XtraReports.UI.XtraReport
    {
        
        public PhieuNhapKho()
        {
            InitializeComponent();
        }
        public void BindReport(KDT_GC_PhieuNhapKho PNK)
        {
            lblTenDN.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblEmail.Text = GlobalSettings.MailDoanhNghiep;
            lblSoDT.Text = GlobalSettings.SoDienThoaiDN;
            lblFax.Text = GlobalSettings.SoFaxDN;
            List<KDT_GC_PhieuNhapKho_Hang> hang = PNK.HangNhapCollection;
            lblNgay.Text = "Ngày " + PNK.NgayNhapKho.Day.ToString() + " tháng " + PNK.NgayNhapKho.Month.ToString() + " năm " + PNK.NgayNhapKho.Year.ToString();
            //lblThang.Text = PNK.NgayNhapKho.Month.ToString();
            //lblNam.Text = PNK.NgayNhapKho.Year.ToString();
            //lblSoPhieu.Text = PNK.SoPhieu.ToString();
            lblSoHDong.Text = PNK.SoPhieu.ToString() +" / " + PNK.SoHopDong;
            lblSoHD.Text = PNK.SoHopDong;
            lblKhachHang.Text = PNK.KhachHang;
            lblNhaCungCap.Text = PNK.NhaCungCap;
            lblNhapTaiKho.Text = PNK.NhapTaiKho;
            lblNgoaiTe.Text = PNK.NgoaiTe;
            lblTyGia.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(2, false), Convert.ToDecimal(PNK.TyGia.ToString().Replace(".", ",")));
            lblToKhai.Text = PNK.ToKhai.ToString();
            lblSoInvoice.Text = PNK.SoInvoice; ;
            lblNgayInvoice.Text = Convert.ToDateTime(PNK.NgayInvoice).ToString("dd/MM/yyyy");
            lblLyDo.Text = PNK.LyDo;
            lblSoBL.Text = PNK.SoBL;
            lblSoKien.Text = PNK.SoKien.ToString();
            lblDGNgoaiTe.Text = PNK.NgoaiTe;
            lblTTNgoaiTe.Text = PNK.NgoaiTe;

            DataTable dt = new DataTable();

            this.DataSource = PNK.HangNhapCollection;
            lblMSVT.DataBindings.Add("Text", this.DataSource, "Ma2");
            //lblMa2.DataBindings.Add("Text", this.DataSource, "Ma2");
            lblTenVT.DataBindings.Add("Text", this.DataSource, "TenHang");
            //lblMau.DataBindings.Add("Text", this.DataSource, "Mau");
            //lblSize.DataBindings.Add("Text", this.DataSource, "Size");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVTLuong1");
            lblChungTu.DataBindings.Add("Text", this.DataSource, "SoLuong1", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            lblThucNhap.DataBindings.Add("Text", this.DataSource, "SoLuong1", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            //if (PNK.NgoaiTe == "VND")
            //{
            //    lblDonGiaVND.DataBindings.Add("Text", this.DataSource, "DonGiaHoaDon", Company.KDT.SHARE.Components.Globals.FormatNumber(0, false));
            //}
            //else
            //{
            //    lblDonGiaNgoaiTe.DataBindings.Add("Text", this.DataSource, "DonGiaHoaDon", Company.KDT.SHARE.Components.Globals.FormatNumber(6, false));
            //}
            //if (PNK.NgoaiTe == "VND")
            //{
            //    lblThanhTienVND.DataBindings.Add("Text", this.DataSource, "TriGiaHoaDon", Company.KDT.SHARE.Components.Globals.FormatNumber(0, false));
            //}
            //else
            //{
            //    lblThanhTienNgoaiTe.DataBindings.Add("Text", this.DataSource, "TriGiaHoaDon", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            //}
            lblDonGiaVND.DataBindings.Add("Text", this.DataSource, "DonGiaHoaDon", Company.KDT.SHARE.Components.Globals.FormatNumber(0, false));
            lblDonGiaNgoaiTe.DataBindings.Add("Text", this.DataSource, "DonGiaNgoaiTe", Company.KDT.SHARE.Components.Globals.FormatNumber(2, true));
            lblThanhTienVND.DataBindings.Add("Text", this.DataSource, "TriGiaHoaDon", Company.KDT.SHARE.Components.Globals.FormatNumber(0, false));
            lblThanhTienNgoaiTe.DataBindings.Add("Text", this.DataSource, "ThanhTienNgoaiTe", Company.KDT.SHARE.Components.Globals.FormatNumber(2, true));
            lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");

            decimal TongSLChungTu = 0;
            decimal TongSLThucNhap = 0;
            decimal TongTriGiaNT = 0;
            decimal TongTriGiaVND = 0;
            decimal TongTienHang = 0;
            decimal TongVAT = 0;
            decimal TongCong = 0;
            decimal tygia = PNK.TyGia;
            foreach (KDT_GC_PhieuNhapKho_Hang item in PNK.HangNhapCollection)
            {
                TongSLChungTu += item.SoLuong1;
                TongSLThucNhap += item.SoLuong1;
                //if (PNK.NgoaiTe == "VND")
                //{
                //    TongTriGiaVND += item.TriGiaHoaDon;
                //    TongTriGiaNT = TongTriGiaVND / tygia;
                //}
                //else
                //{
                //    TongTriGiaNT += item.TriGiaHoaDon;
                //    TongTriGiaVND = TongTriGiaNT * tygia;
                //}
                TongTriGiaVND += item.TriGiaHoaDon;
                TongTriGiaNT += item.ThanhTienNgoaiTe;
            }
            lblTongSLChungTu.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(TongSLChungTu.ToString().Replace(".", ",")));
            lblTongSLThucNhap.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(TongSLThucNhap.ToString().Replace(".", ",")));
            //if (PNK.NgoaiTe == "VND")
            //{
            //    lblTongTienVND.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(0, false), Convert.ToDecimal(TongTriGiaVND.ToString().Replace(".", ",")));
            //    lblTongTienNT.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(TongTriGiaNT.ToString().Replace(".", ",")));
            //}
            //else
            //{
            //    lblTongTienNT.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(TongTriGiaNT.ToString().Replace(".", ",")));
            //    lblTongTienVND.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(0, false), Convert.ToDecimal(TongTriGiaVND.ToString().Replace(".", ",")));
            //}
            lblTongTienVND.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(0, false), Convert.ToDecimal(TongTriGiaVND.ToString().Replace(".", ",")));
            lblTongTienNT.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(TongTriGiaNT.ToString().Replace(".", ",")));
            TongTienHang = TongTriGiaVND;
            TongVAT = PNK.TongVAT;
            TongCong = TongTienHang + TongVAT;
            lblCongTienHang.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(0, false), Convert.ToDecimal(TongTienHang.ToString().Replace(".", ",")));
            lblTongVAT.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(0, false), Convert.ToDecimal(TongVAT.ToString().Replace(".", ",")));
            lblTongCong.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(0, false), Convert.ToDecimal(TongCong.ToString().Replace(".", ",")));

            string s = Company.GC.BLL.Utils.VNCurrency.ToString((decimal) Convert.ToDecimal(this.lblTongCong.Text)).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);
            lblBangChu.Text = s.Replace("  ", " ");
        }
        int stt = 0;

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            stt += 1;
            lblSTT.Text = stt.ToString();

        }
        //decimal dongiangoaite = 0;
        //decimal dongiaVN = 0;
        //private void lblDonGiaNgoaiTe_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    if (lblNgoaiTe.Text == "VND")
        //    {
        //        decimal tygia = Convert.ToDecimal(lblTyGia.Text);
        //        decimal dongiaVND = Convert.ToDecimal(lblDonGiaVND.Text);

        //        dongiangoaite = dongiaVND / tygia;
        //        lblDonGiaNgoaiTe.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(6, false), dongiangoaite);
        //    }
        //}

        //private void lblThanhTienNgoaiTe_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    if (lblNgoaiTe.Text == "VND")
        //    {
        //        decimal thanhtienngoaite = 0;
        //        decimal soluong = Convert.ToDecimal(lblThucNhap.Text);
        //        thanhtienngoaite = dongiangoaite * soluong;
        //        lblThanhTienNgoaiTe.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), thanhtienngoaite);
        //    }
        //}

        //private void lblDonGiaVND_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    if (lblNgoaiTe.Text != "VND")
        //    {
        //        decimal tygia = Convert.ToDecimal(lblTyGia.Text);
        //        decimal dongiaNT= Convert.ToDecimal(lblDonGiaNgoaiTe.Text);

        //        dongiaVN = dongiaNT * tygia;
        //        lblDonGiaVND.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(0, false), dongiaVN);
        //    }
        //}

        //private void lblThanhTienVND_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    if (lblNgoaiTe.Text != "VND")
        //    {
        //        decimal thanhtienVN = 0;
        //        decimal soluong = Convert.ToDecimal(lblThucNhap.Text);
        //        thanhtienVN = dongiaVN * soluong;
        //        lblThanhTienVND.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(0, false), thanhtienVN);
        //    }
        //}
    }
}
