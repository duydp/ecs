﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KeyServer;
using KeyServer.User;
using KeySecurity;

namespace LicenseCode
{
    public partial class frmLockKey : Form
    {
        public LicenseCode.ProductKey.Key keyLock;
        public Key keyGen;
        public User user = new User();
        private ProductKey.ProductKey productServices = new ProductKey.ProductKey();
        public frmLockKey()
        {
            InitializeComponent();
        }

        private void GetData()
        {
            if (keyGen != null)
            {
                txtActiveCode.Text = keyGen.ActivedKey;
                lblMaDoanhNghiep.Text = keyGen.CustomerCode;
                lblTenDoanhNghiep.Text = keyGen.CustomerName;
                lblProductID.Text = keyGen.ProductId;
                lblNgayCuoi.Value = keyGen.LastCheckStatus;
                lblNgayActive.Value = keyGen.ActivedDate;
                lblNgayHetHan.Value = keyGen.ExpiredDate;
                lblNguoiCapKey.Text = keyGen.RequestedBy;
                lblNgayCapKey.Value = keyGen.RequestedDate;

                if (keyGen.ActivedDate.Year > 1900)
                {
                    lblStatus.ForeColor = Color.Blue;
                    lblStatus.Text = "Key đã được kích hoạt";
                }
                else
                {
                    lblStatus.ForeColor = Color.Red;
                    lblStatus.Text = "Key chưa được kích hoạt";
                }
                if (keyGen.Status == "1")
                    lblStatus.Text = "Key License";
                else if (keyGen.Status == "0")
                    lblStatus.Text = "Key Trial";
                else if (keyGen.Status.ToUpper() == "LOCK")
                {
                    lblStatus.Text = "Key đã LOCK";
                    string[] notes = keyGen.Notes.Split(',');
                    string lydoLock = string.Empty;
                    foreach (string item in notes)
                    {
                        if (item.Contains("LOCK"))
                        {
                            lydoLock = item;
                            break;
                        }
                    }
                    txtLyDo.Text = lydoLock;
                    btnLock.Enabled = false;

                }
                Clipboard.SetText(keyGen.ActivedKey);                
            }
            else if (keyLock != null)
            {
                txtActiveCode.Text = keyLock.ActivedKey;
                lblMaDoanhNghiep.Text = keyLock.CustomerCode;
                lblTenDoanhNghiep.Text = keyLock.CustomerName;
                lblProductID.Text = keyLock.ProductId;
                lblNgayCuoi.Value = keyLock.LastCheckStatus;
                lblNgayActive.Value = keyLock.ActivedDate;
                lblNgayHetHan.Value = keyLock.ExpiredDate;
                lblNguoiCapKey.Text = keyLock.RequestedBy;
                lblNgayCapKey.Value = keyLock.RequestedDate;

                if (keyLock.ActivedDate.Year > 1900)
                {
                    lblStatus.ForeColor = Color.Blue;
                    lblStatus.Text = "Key đã được kích hoạt";
                }
                else
                {
                    lblStatus.ForeColor = Color.Red;
                    lblStatus.Text = "Key chưa được kích hoạt";
                }
                if (keyLock.Status == "1")
                    lblStatus.Text = "Key License";
                else if (keyLock.Status == "0")
                    lblStatus.Text = "Key Trial";
                else if (keyLock.Status.ToUpper() == "LOCK")
                {
                    lblStatus.Text = "Key đã LOCK";
                    string[] notes = keyLock.Notes.Split(',');
                    string lydoLock = string.Empty;
                    foreach (string item in notes)
                    {
                        if (item.Contains("LOCK"))
                        {
                            lydoLock = item;
                            break;
                        }
                    }
                    txtLyDo.Text = lydoLock;
                    btnLock.Enabled = false;

                }
                Clipboard.SetText(keyLock.ActivedKey);
            }
            else
            {
                MessageBoxControl.ShowMessage("Key không hợp lệ");
            }

        }

        private void frmLockKey_Load(object sender, EventArgs e)
        {
            GetData();
            productServices.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            productServices.Timeout = 240000;
        }

        private void btnLock_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLyDo.Text))
            {
                MessageBoxControl.ShowMessage("Bạn chưa nhập lý do lock");
                return;
            }
            if (MessageBoxControl.ShowMessageConfirm("Xác nhận lock Key của công ty: " + keyLock.CustomerName) == "No")
                return;
            string result = string.Empty;
            try
            {
                this.UseWaitCursor = true;
                if (keyLock != null)
                {
                    result = productServices.LockKey(user.UserName, KeyCode.ConvertToBase64(user.PassWord), keyLock.SerialNumber, txtLyDo.Text);
                }
                else if (keyGen != null)
                {
                    result = productServices.LockKey(user.UserName, KeyCode.ConvertToBase64(user.PassWord), keyGen.SerialNumber, txtLyDo.Text);
                }
                if (string.IsNullOrEmpty(result))
                {
                    MessageBoxControl.ShowMessage("Lock key thành công");
                    this.UseWaitCursor = false;
                    this.Close();
                }
                else
                    MessageBoxControl.ShowMessage("Lỗi khi lock key: " + result);
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Không kết nối được đến cơ sở dữ liệu. Lỗi: " + ex.Message);
            }
        }
    }
}
