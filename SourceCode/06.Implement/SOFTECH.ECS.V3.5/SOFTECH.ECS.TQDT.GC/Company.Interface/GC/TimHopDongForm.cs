using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Janus.Windows.EditControls;
using Company.GC.BLL;
using Company.Interface.Report.SXXK;
using System.IO;
using Company.Interface.Report.GC; 
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.GC
{
    public partial class TimHopDongForm : BaseForm
    {
        public HopDong HopDongSelected = new HopDong();
        public bool IsBrowseForm = false;
        public bool IsBrowseFormCT = false;
        public string soHD;
        public long IDHD = 0;
        public TimHopDongForm()
        {
            InitializeComponent();            
        }

        public void BindData()
        {
            string where = string.Format("MaDoanhNghiep = '{0}'", GlobalSettings.MA_DON_VI);
            where += "AND TrangThaiXuLy = 1";
            //if (GlobalSettin1gs.MA_HAI_QUAN != "")
            //where += string.Format(" AND MaHaiQuan = '{0}' ", GlobalSet1tings.MA_HAI_QUAN);
            if (txtSoHopDong.Text.Trim().Length > 0)
            {
                where += string.Format(" AND SoHopDong = '{0}' ", txtSoHopDong.Text);
            }
            if ((int)txtNamDangKy.Value > 0)
            {
                where += string.Format(" AND Year(NgayKy) = '{0}' ", txtNamDangKy.Value);
            }
            dgList.DataSource = HopDong.SelectCollectionDynamic(where, "NgayKy DESC");
        }
        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];

        }

        private void btnTimHD_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void TimHopDongForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            txtNamDangKy.Value = DateTime.Today.Year;
            BindData();
        }

        private void dgList_DoubleClick(object sender, EventArgs e)
        {
            

        }

        private void btnChonHD_Click(object sender, EventArgs e)
        {

        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                 this.HopDongSelected = (HopDong)e.Row.DataRow;
                 string soHD = e.Row.Cells[""].ToString();

                //HopDongRegistedDetailForm f = new HopDongRegistedDetailForm();
                //f.HD = this.HopDongSelected;
                //f.ShowDialog();


            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

       
    }
}