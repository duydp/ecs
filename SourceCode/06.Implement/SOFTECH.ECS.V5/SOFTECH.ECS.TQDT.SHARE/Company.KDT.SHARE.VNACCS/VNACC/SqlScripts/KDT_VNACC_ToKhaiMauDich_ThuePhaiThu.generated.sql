-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoTienThue numeric(12, 0),
	@TongSoTienMien numeric(11, 0),
	@TongSoTienGiam numeric(11, 0),
	@TongSoThuePhaiNop numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
(
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoTienThue],
	[TongSoTienMien],
	[TongSoTienGiam],
	[TongSoThuePhaiNop],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
)
VALUES 
(
	@TenCoQuanHaiQuan,
	@TenChiCucHaiQuanNoiMoToKhai,
	@SoChungTu,
	@TenDonViXuatNhapKhau,
	@MaDonViXuatNhapKhau,
	@MaBuuChinh,
	@DiaChiNguoiXuatNhapKhau,
	@SoDienThoaiNguoiXuatNhapKhau,
	@SoToKhai,
	@NgayDangKyToKhai,
	@MaPhanLoaiToKhai,
	@TenNganHangBaoLanh,
	@MaNganHangBaoLanh,
	@KiHieuChungTuBaoLanh,
	@SoChungTuBaoLanh,
	@LoaiBaoLanh,
	@TenNganHangTraThay,
	@MaNganHangTraThay,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoHieuPhatHanhHanMuc,
	@TongSoTienThue,
	@TongSoTienMien,
	@TongSoTienGiam,
	@TongSoThuePhaiNop,
	@MaTienTe,
	@TyGiaQuyDoi,
	@SoNgayDuocAnHan,
	@NgayHetHieuLucTamNhapTaiXuat,
	@SoTaiKhoanKhoBac,
	@TenKhoBac,
	@LaiSuatPhatChamNop,
	@NgayPhatHanhChungTu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoTienThue numeric(12, 0),
	@TongSoTienMien numeric(11, 0),
	@TongSoTienGiam numeric(11, 0),
	@TongSoThuePhaiNop numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
SET
	[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
	[TenChiCucHaiQuanNoiMoToKhai] = @TenChiCucHaiQuanNoiMoToKhai,
	[SoChungTu] = @SoChungTu,
	[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
	[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
	[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
	[SoToKhai] = @SoToKhai,
	[NgayDangKyToKhai] = @NgayDangKyToKhai,
	[MaPhanLoaiToKhai] = @MaPhanLoaiToKhai,
	[TenNganHangBaoLanh] = @TenNganHangBaoLanh,
	[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
	[KiHieuChungTuBaoLanh] = @KiHieuChungTuBaoLanh,
	[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
	[LoaiBaoLanh] = @LoaiBaoLanh,
	[TenNganHangTraThay] = @TenNganHangTraThay,
	[MaNganHangTraThay] = @MaNganHangTraThay,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
	[TongSoTienThue] = @TongSoTienThue,
	[TongSoTienMien] = @TongSoTienMien,
	[TongSoTienGiam] = @TongSoTienGiam,
	[TongSoThuePhaiNop] = @TongSoThuePhaiNop,
	[MaTienTe] = @MaTienTe,
	[TyGiaQuyDoi] = @TyGiaQuyDoi,
	[SoNgayDuocAnHan] = @SoNgayDuocAnHan,
	[NgayHetHieuLucTamNhapTaiXuat] = @NgayHetHieuLucTamNhapTaiXuat,
	[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
	[TenKhoBac] = @TenKhoBac,
	[LaiSuatPhatChamNop] = @LaiSuatPhatChamNop,
	[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoTienThue numeric(12, 0),
	@TongSoTienMien numeric(11, 0),
	@TongSoTienGiam numeric(11, 0),
	@TongSoThuePhaiNop numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] 
		SET
			[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
			[TenChiCucHaiQuanNoiMoToKhai] = @TenChiCucHaiQuanNoiMoToKhai,
			[SoChungTu] = @SoChungTu,
			[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
			[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
			[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
			[SoToKhai] = @SoToKhai,
			[NgayDangKyToKhai] = @NgayDangKyToKhai,
			[MaPhanLoaiToKhai] = @MaPhanLoaiToKhai,
			[TenNganHangBaoLanh] = @TenNganHangBaoLanh,
			[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
			[KiHieuChungTuBaoLanh] = @KiHieuChungTuBaoLanh,
			[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
			[LoaiBaoLanh] = @LoaiBaoLanh,
			[TenNganHangTraThay] = @TenNganHangTraThay,
			[MaNganHangTraThay] = @MaNganHangTraThay,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
			[TongSoTienThue] = @TongSoTienThue,
			[TongSoTienMien] = @TongSoTienMien,
			[TongSoTienGiam] = @TongSoTienGiam,
			[TongSoThuePhaiNop] = @TongSoThuePhaiNop,
			[MaTienTe] = @MaTienTe,
			[TyGiaQuyDoi] = @TyGiaQuyDoi,
			[SoNgayDuocAnHan] = @SoNgayDuocAnHan,
			[NgayHetHieuLucTamNhapTaiXuat] = @NgayHetHieuLucTamNhapTaiXuat,
			[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
			[TenKhoBac] = @TenKhoBac,
			[LaiSuatPhatChamNop] = @LaiSuatPhatChamNop,
			[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
		(
			[TenCoQuanHaiQuan],
			[TenChiCucHaiQuanNoiMoToKhai],
			[SoChungTu],
			[TenDonViXuatNhapKhau],
			[MaDonViXuatNhapKhau],
			[MaBuuChinh],
			[DiaChiNguoiXuatNhapKhau],
			[SoDienThoaiNguoiXuatNhapKhau],
			[SoToKhai],
			[NgayDangKyToKhai],
			[MaPhanLoaiToKhai],
			[TenNganHangBaoLanh],
			[MaNganHangBaoLanh],
			[KiHieuChungTuBaoLanh],
			[SoChungTuBaoLanh],
			[LoaiBaoLanh],
			[TenNganHangTraThay],
			[MaNganHangTraThay],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoHieuPhatHanhHanMuc],
			[TongSoTienThue],
			[TongSoTienMien],
			[TongSoTienGiam],
			[TongSoThuePhaiNop],
			[MaTienTe],
			[TyGiaQuyDoi],
			[SoNgayDuocAnHan],
			[NgayHetHieuLucTamNhapTaiXuat],
			[SoTaiKhoanKhoBac],
			[TenKhoBac],
			[LaiSuatPhatChamNop],
			[NgayPhatHanhChungTu]
		)
		VALUES 
		(
			@TenCoQuanHaiQuan,
			@TenChiCucHaiQuanNoiMoToKhai,
			@SoChungTu,
			@TenDonViXuatNhapKhau,
			@MaDonViXuatNhapKhau,
			@MaBuuChinh,
			@DiaChiNguoiXuatNhapKhau,
			@SoDienThoaiNguoiXuatNhapKhau,
			@SoToKhai,
			@NgayDangKyToKhai,
			@MaPhanLoaiToKhai,
			@TenNganHangBaoLanh,
			@MaNganHangBaoLanh,
			@KiHieuChungTuBaoLanh,
			@SoChungTuBaoLanh,
			@LoaiBaoLanh,
			@TenNganHangTraThay,
			@MaNganHangTraThay,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoHieuPhatHanhHanMuc,
			@TongSoTienThue,
			@TongSoTienMien,
			@TongSoTienGiam,
			@TongSoThuePhaiNop,
			@MaTienTe,
			@TyGiaQuyDoi,
			@SoNgayDuocAnHan,
			@NgayHetHieuLucTamNhapTaiXuat,
			@SoTaiKhoanKhoBac,
			@TenKhoBac,
			@LaiSuatPhatChamNop,
			@NgayPhatHanhChungTu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoTienThue],
	[TongSoTienMien],
	[TongSoTienGiam],
	[TongSoThuePhaiNop],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoTienThue],
	[TongSoTienMien],
	[TongSoTienGiam],
	[TongSoThuePhaiNop],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoTienThue],
	[TongSoTienMien],
	[TongSoTienGiam],
	[TongSoThuePhaiNop],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]	

GO

