using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.DuLieuChuan
{
	public partial class KDT_GC_PhieuNhapKho : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public DateTime NgayNhapKho { set; get; }
		public decimal SoPhieu { set; get; }
		public string SoHopDong { set; get; }
		public string KhachHang { set; get; }
		public string NhapTaiKho { set; get; }
		public string NgoaiTe { set; get; }
		public decimal TyGia { set; get; }
		public string LyDo { set; get; }
		public string NhaCungCap { set; get; }
		public decimal ToKhai { set; get; }
		public string SoInvoice { set; get; }
		public DateTime NgayInvoice { set; get; }
		public string SoBL { set; get; }
		public decimal SoKien { set; get; }
		public decimal TongVAT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_PhieuNhapKho> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_PhieuNhapKho> collection = new List<KDT_GC_PhieuNhapKho>();
			while (reader.Read())
			{
				KDT_GC_PhieuNhapKho entity = new KDT_GC_PhieuNhapKho();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNhapKho"))) entity.NgayNhapKho = reader.GetDateTime(reader.GetOrdinal("NgayNhapKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoPhieu"))) entity.SoPhieu = reader.GetDecimal(reader.GetOrdinal("SoPhieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhachHang"))) entity.KhachHang = reader.GetString(reader.GetOrdinal("KhachHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhapTaiKho"))) entity.NhapTaiKho = reader.GetString(reader.GetOrdinal("NhapTaiKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgoaiTe"))) entity.NgoaiTe = reader.GetString(reader.GetOrdinal("NgoaiTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGia"))) entity.TyGia = reader.GetDecimal(reader.GetOrdinal("TyGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDo"))) entity.LyDo = reader.GetString(reader.GetOrdinal("LyDo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhaCungCap"))) entity.NhaCungCap = reader.GetString(reader.GetOrdinal("NhaCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("ToKhai"))) entity.ToKhai = reader.GetDecimal(reader.GetOrdinal("ToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoInvoice"))) entity.SoInvoice = reader.GetString(reader.GetOrdinal("SoInvoice"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayInvoice"))) entity.NgayInvoice = reader.GetDateTime(reader.GetOrdinal("NgayInvoice"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBL"))) entity.SoBL = reader.GetString(reader.GetOrdinal("SoBL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongVAT"))) entity.TongVAT = reader.GetDecimal(reader.GetOrdinal("TongVAT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_PhieuNhapKho> collection, long id)
        {
            foreach (KDT_GC_PhieuNhapKho item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_PhieuNhapKho VALUES(@TKMD_ID, @NgayNhapKho, @SoPhieu, @SoHopDong, @KhachHang, @NhapTaiKho, @NgoaiTe, @TyGia, @LyDo, @NhaCungCap, @ToKhai, @SoInvoice, @NgayInvoice, @SoBL, @SoKien, @TongVAT)";
            string update = "UPDATE t_KDT_GC_PhieuNhapKho SET TKMD_ID = @TKMD_ID, NgayNhapKho = @NgayNhapKho, SoPhieu = @SoPhieu, SoHopDong = @SoHopDong, KhachHang = @KhachHang, NhapTaiKho = @NhapTaiKho, NgoaiTe = @NgoaiTe, TyGia = @TyGia, LyDo = @LyDo, NhaCungCap = @NhaCungCap, ToKhai = @ToKhai, SoInvoice = @SoInvoice, NgayInvoice = @NgayInvoice, SoBL = @SoBL, SoKien = @SoKien, TongVAT = @TongVAT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhieuNhapKho WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayNhapKho", SqlDbType.DateTime, "NgayNhapKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhieu", SqlDbType.Decimal, "SoPhieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhapTaiKho", SqlDbType.NVarChar, "NhapTaiKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgoaiTe", SqlDbType.VarChar, "NgoaiTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGia", SqlDbType.Decimal, "TyGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDo", SqlDbType.NVarChar, "LyDo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhaCungCap", SqlDbType.NVarChar, "NhaCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ToKhai", SqlDbType.Decimal, "ToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoInvoice", SqlDbType.NVarChar, "SoInvoice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayInvoice", SqlDbType.DateTime, "NgayInvoice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoBL", SqlDbType.NVarChar, "SoBL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoKien", SqlDbType.Decimal, "SoKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongVAT", SqlDbType.Decimal, "TongVAT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayNhapKho", SqlDbType.DateTime, "NgayNhapKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhieu", SqlDbType.Decimal, "SoPhieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhapTaiKho", SqlDbType.NVarChar, "NhapTaiKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgoaiTe", SqlDbType.VarChar, "NgoaiTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGia", SqlDbType.Decimal, "TyGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDo", SqlDbType.NVarChar, "LyDo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhaCungCap", SqlDbType.NVarChar, "NhaCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ToKhai", SqlDbType.Decimal, "ToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoInvoice", SqlDbType.NVarChar, "SoInvoice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayInvoice", SqlDbType.DateTime, "NgayInvoice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoBL", SqlDbType.NVarChar, "SoBL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoKien", SqlDbType.Decimal, "SoKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongVAT", SqlDbType.Decimal, "TongVAT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_PhieuNhapKho VALUES(@TKMD_ID, @NgayNhapKho, @SoPhieu, @SoHopDong, @KhachHang, @NhapTaiKho, @NgoaiTe, @TyGia, @LyDo, @NhaCungCap, @ToKhai, @SoInvoice, @NgayInvoice, @SoBL, @SoKien, @TongVAT)";
            string update = "UPDATE t_KDT_GC_PhieuNhapKho SET TKMD_ID = @TKMD_ID, NgayNhapKho = @NgayNhapKho, SoPhieu = @SoPhieu, SoHopDong = @SoHopDong, KhachHang = @KhachHang, NhapTaiKho = @NhapTaiKho, NgoaiTe = @NgoaiTe, TyGia = @TyGia, LyDo = @LyDo, NhaCungCap = @NhaCungCap, ToKhai = @ToKhai, SoInvoice = @SoInvoice, NgayInvoice = @NgayInvoice, SoBL = @SoBL, SoKien = @SoKien, TongVAT = @TongVAT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhieuNhapKho WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayNhapKho", SqlDbType.DateTime, "NgayNhapKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhieu", SqlDbType.Decimal, "SoPhieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhapTaiKho", SqlDbType.NVarChar, "NhapTaiKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgoaiTe", SqlDbType.VarChar, "NgoaiTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGia", SqlDbType.Decimal, "TyGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDo", SqlDbType.NVarChar, "LyDo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhaCungCap", SqlDbType.NVarChar, "NhaCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ToKhai", SqlDbType.Decimal, "ToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoInvoice", SqlDbType.NVarChar, "SoInvoice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayInvoice", SqlDbType.DateTime, "NgayInvoice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoBL", SqlDbType.NVarChar, "SoBL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoKien", SqlDbType.Decimal, "SoKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongVAT", SqlDbType.Decimal, "TongVAT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayNhapKho", SqlDbType.DateTime, "NgayNhapKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhieu", SqlDbType.Decimal, "SoPhieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhapTaiKho", SqlDbType.NVarChar, "NhapTaiKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgoaiTe", SqlDbType.VarChar, "NgoaiTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGia", SqlDbType.Decimal, "TyGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDo", SqlDbType.NVarChar, "LyDo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhaCungCap", SqlDbType.NVarChar, "NhaCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ToKhai", SqlDbType.Decimal, "ToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoInvoice", SqlDbType.NVarChar, "SoInvoice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayInvoice", SqlDbType.DateTime, "NgayInvoice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoBL", SqlDbType.NVarChar, "SoBL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoKien", SqlDbType.Decimal, "SoKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongVAT", SqlDbType.Decimal, "TongVAT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_PhieuNhapKho Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_PhieuNhapKho> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_PhieuNhapKho> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_PhieuNhapKho> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_PhieuNhapKho(long tKMD_ID, DateTime ngayNhapKho, decimal soPhieu, string soHopDong, string khachHang, string nhapTaiKho, string ngoaiTe, decimal tyGia, string lyDo, string nhaCungCap, decimal toKhai, string soInvoice, DateTime ngayInvoice, string soBL, decimal soKien, decimal tongVAT)
		{
			KDT_GC_PhieuNhapKho entity = new KDT_GC_PhieuNhapKho();	
			entity.TKMD_ID = tKMD_ID;
			entity.NgayNhapKho = ngayNhapKho;
			entity.SoPhieu = soPhieu;
			entity.SoHopDong = soHopDong;
			entity.KhachHang = khachHang;
			entity.NhapTaiKho = nhapTaiKho;
			entity.NgoaiTe = ngoaiTe;
			entity.TyGia = tyGia;
			entity.LyDo = lyDo;
			entity.NhaCungCap = nhaCungCap;
			entity.ToKhai = toKhai;
			entity.SoInvoice = soInvoice;
			entity.NgayInvoice = ngayInvoice;
			entity.SoBL = soBL;
			entity.SoKien = soKien;
			entity.TongVAT = tongVAT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayNhapKho", SqlDbType.DateTime, NgayNhapKho.Year <= 1753 ? DBNull.Value : (object) NgayNhapKho);
			db.AddInParameter(dbCommand, "@SoPhieu", SqlDbType.Decimal, SoPhieu);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@KhachHang", SqlDbType.NVarChar, KhachHang);
			db.AddInParameter(dbCommand, "@NhapTaiKho", SqlDbType.NVarChar, NhapTaiKho);
			db.AddInParameter(dbCommand, "@NgoaiTe", SqlDbType.VarChar, NgoaiTe);
			db.AddInParameter(dbCommand, "@TyGia", SqlDbType.Decimal, TyGia);
			db.AddInParameter(dbCommand, "@LyDo", SqlDbType.NVarChar, LyDo);
			db.AddInParameter(dbCommand, "@NhaCungCap", SqlDbType.NVarChar, NhaCungCap);
			db.AddInParameter(dbCommand, "@ToKhai", SqlDbType.Decimal, ToKhai);
			db.AddInParameter(dbCommand, "@SoInvoice", SqlDbType.NVarChar, SoInvoice);
			db.AddInParameter(dbCommand, "@NgayInvoice", SqlDbType.DateTime, NgayInvoice.Year <= 1753 ? DBNull.Value : (object) NgayInvoice);
			db.AddInParameter(dbCommand, "@SoBL", SqlDbType.NVarChar, SoBL);
			db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
			db.AddInParameter(dbCommand, "@TongVAT", SqlDbType.Decimal, TongVAT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_PhieuNhapKho> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuNhapKho item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_PhieuNhapKho(long id, long tKMD_ID, DateTime ngayNhapKho, decimal soPhieu, string soHopDong, string khachHang, string nhapTaiKho, string ngoaiTe, decimal tyGia, string lyDo, string nhaCungCap, decimal toKhai, string soInvoice, DateTime ngayInvoice, string soBL, decimal soKien, decimal tongVAT)
		{
			KDT_GC_PhieuNhapKho entity = new KDT_GC_PhieuNhapKho();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayNhapKho = ngayNhapKho;
			entity.SoPhieu = soPhieu;
			entity.SoHopDong = soHopDong;
			entity.KhachHang = khachHang;
			entity.NhapTaiKho = nhapTaiKho;
			entity.NgoaiTe = ngoaiTe;
			entity.TyGia = tyGia;
			entity.LyDo = lyDo;
			entity.NhaCungCap = nhaCungCap;
			entity.ToKhai = toKhai;
			entity.SoInvoice = soInvoice;
			entity.NgayInvoice = ngayInvoice;
			entity.SoBL = soBL;
			entity.SoKien = soKien;
			entity.TongVAT = tongVAT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_PhieuNhapKho_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayNhapKho", SqlDbType.DateTime, NgayNhapKho.Year <= 1753 ? DBNull.Value : (object) NgayNhapKho);
			db.AddInParameter(dbCommand, "@SoPhieu", SqlDbType.Decimal, SoPhieu);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@KhachHang", SqlDbType.NVarChar, KhachHang);
			db.AddInParameter(dbCommand, "@NhapTaiKho", SqlDbType.NVarChar, NhapTaiKho);
			db.AddInParameter(dbCommand, "@NgoaiTe", SqlDbType.VarChar, NgoaiTe);
			db.AddInParameter(dbCommand, "@TyGia", SqlDbType.Decimal, TyGia);
			db.AddInParameter(dbCommand, "@LyDo", SqlDbType.NVarChar, LyDo);
			db.AddInParameter(dbCommand, "@NhaCungCap", SqlDbType.NVarChar, NhaCungCap);
			db.AddInParameter(dbCommand, "@ToKhai", SqlDbType.Decimal, ToKhai);
			db.AddInParameter(dbCommand, "@SoInvoice", SqlDbType.NVarChar, SoInvoice);
			db.AddInParameter(dbCommand, "@NgayInvoice", SqlDbType.DateTime, NgayInvoice.Year <= 1753 ? DBNull.Value : (object) NgayInvoice);
			db.AddInParameter(dbCommand, "@SoBL", SqlDbType.NVarChar, SoBL);
			db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
			db.AddInParameter(dbCommand, "@TongVAT", SqlDbType.Decimal, TongVAT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_PhieuNhapKho> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuNhapKho item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_PhieuNhapKho(long id, long tKMD_ID, DateTime ngayNhapKho, decimal soPhieu, string soHopDong, string khachHang, string nhapTaiKho, string ngoaiTe, decimal tyGia, string lyDo, string nhaCungCap, decimal toKhai, string soInvoice, DateTime ngayInvoice, string soBL, decimal soKien, decimal tongVAT)
		{
			KDT_GC_PhieuNhapKho entity = new KDT_GC_PhieuNhapKho();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayNhapKho = ngayNhapKho;
			entity.SoPhieu = soPhieu;
			entity.SoHopDong = soHopDong;
			entity.KhachHang = khachHang;
			entity.NhapTaiKho = nhapTaiKho;
			entity.NgoaiTe = ngoaiTe;
			entity.TyGia = tyGia;
			entity.LyDo = lyDo;
			entity.NhaCungCap = nhaCungCap;
			entity.ToKhai = toKhai;
			entity.SoInvoice = soInvoice;
			entity.NgayInvoice = ngayInvoice;
			entity.SoBL = soBL;
			entity.SoKien = soKien;
			entity.TongVAT = tongVAT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayNhapKho", SqlDbType.DateTime, NgayNhapKho.Year <= 1753 ? DBNull.Value : (object) NgayNhapKho);
			db.AddInParameter(dbCommand, "@SoPhieu", SqlDbType.Decimal, SoPhieu);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@KhachHang", SqlDbType.NVarChar, KhachHang);
			db.AddInParameter(dbCommand, "@NhapTaiKho", SqlDbType.NVarChar, NhapTaiKho);
			db.AddInParameter(dbCommand, "@NgoaiTe", SqlDbType.VarChar, NgoaiTe);
			db.AddInParameter(dbCommand, "@TyGia", SqlDbType.Decimal, TyGia);
			db.AddInParameter(dbCommand, "@LyDo", SqlDbType.NVarChar, LyDo);
			db.AddInParameter(dbCommand, "@NhaCungCap", SqlDbType.NVarChar, NhaCungCap);
			db.AddInParameter(dbCommand, "@ToKhai", SqlDbType.Decimal, ToKhai);
			db.AddInParameter(dbCommand, "@SoInvoice", SqlDbType.NVarChar, SoInvoice);
			db.AddInParameter(dbCommand, "@NgayInvoice", SqlDbType.DateTime, NgayInvoice.Year <= 1753 ? DBNull.Value : (object) NgayInvoice);
			db.AddInParameter(dbCommand, "@SoBL", SqlDbType.NVarChar, SoBL);
			db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
			db.AddInParameter(dbCommand, "@TongVAT", SqlDbType.Decimal, TongVAT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_PhieuNhapKho> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuNhapKho item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_PhieuNhapKho(long id)
		{
			KDT_GC_PhieuNhapKho entity = new KDT_GC_PhieuNhapKho();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_PhieuNhapKho> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuNhapKho item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}