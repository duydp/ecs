using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.Interface.Report.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class ToKhaiSapHetHanForm : Company.Interface.BaseForm
    {
        private DataTable dt = new DataTable();

        public ToKhaiSapHetHanForm()
        {
            InitializeComponent();
        }

        private void ToKhaiSapHetHanForm_Load(object sender, EventArgs e)
        {
            int hanThanhKhoan = GlobalSettings.HanThanhKhoan;// Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["HanThanhKhoan"]);
            int thoiGianTK = GlobalSettings.ThongBaoHetHan;
            this.dt = new ToKhaiMauDich().GetToKhaiSapHetHanTK(hanThanhKhoan, thoiGianTK, GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
            dgList.DataSource = this.dt;
            dgList.Tables[0].Columns["Luong"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["Ton"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;

            GetSumToKhai();
        }

        private void GetSumToKhai()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string sql = "SELECT CASE  WHEN a.MaLoaiHinh LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM  t_VNACCS_CapSoToKhai WHERE SoTK = a.SoToKhai) " +
	"ELSE a.SoToKhai END AS SoToKhaiVNACCS, " + "a.SoToKhai, a.MaLoaiHinh, b.NgayDangKy, a.NamDangKy, a.MaHaiQuan, @HanThanhKhoan - DATEDIFF(day, NgayDangKy, GETDATE()) as SoNgay " +
                        "FROM dbo.t_SXXK_ThanhLy_NPLNhapTon a INNER JOIN t_SXXK_ToKhaiMauDich b " +
                        "ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                        "AND a.NamDangKY = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                        "INNER JOIN t_SXXK_NguyenPhuLieu c ON a.MaNPL = c.Ma " +
                        "WHERE (@HanThanhKhoan - DATEDIFF(day, b.NgayDangKy, GETDATE())<= @ThoiGianTK) " +
                                "AND (@HanThanhKhoan - DATEDIFF(day, b.NgayDangKy, GETDATE()) >= 0) " +
                                "AND b.ThanhLy != 'H' AND (a.MaLoaiHinh LIKE 'NSX%' OR a.MaLoaiHinh LIKE 'NV%')AND a.MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "' AND a.MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'"
                                + " GROUP BY a.SoToKhai, a.MaLoaiHinh, b.NgayDangKy, a.NamDangKy, a.MaHaiQuan, @HanThanhKhoan - DATEDIFF(day, NgayDangKy, GETDATE())"
                                + " ORDER BY @HanThanhKhoan - DATEDIFF(day, NgayDangKy, GETDATE())";

                Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase db = (Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase)Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
                System.Data.SqlClient.SqlCommand dbCommand = (System.Data.SqlClient.SqlCommand)db.GetSqlStringCommand(sql);
                db.AddInParameter(dbCommand, "@HanThanhKhoan", SqlDbType.Int, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["HanThanhKhoan"]));
                db.AddInParameter(dbCommand, "@ThoiGianTK", SqlDbType.Int, GlobalSettings.ThongBaoHetHan);
                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];

                if (dt == null)
                    return;

                DataRow[] filters = dt.Select("SoNgay > 0");

                lblGanDenHanThanhKhoan.Text = "" + (filters != null ? filters.Length : 0);

                lblHetHanThanhKhoan.Text = "" + (dt != null ? dt.Rows.Count - (filters != null ? filters.Length : 0) : 0);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (this.dt.Rows.Count > 0)
            {
                ToKhaiHetHanTKReport report = new ToKhaiHetHanTKReport();
                report.dt = this.dt;
                report.BindReport();
                report.ShowPreview();
            }
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_FormattingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoNgay"].Text = string.Format("C�n {0} ng�y", e.Row.Cells["SoNgay"].Value.ToString());
            }
        }

    }
}

