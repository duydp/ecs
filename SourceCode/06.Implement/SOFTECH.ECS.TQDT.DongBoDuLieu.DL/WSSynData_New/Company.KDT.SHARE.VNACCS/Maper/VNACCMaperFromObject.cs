﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS.Maper
{
    public class VNACCMaperFromObject
    {
        /*HOA DON*/
        public static IVA IVAMapper(KDT_VNACC_HoaDon hoadon)
        {
            IVA iva = new IVA();
            iva.NIV.SetValue(hoadon.SoTiepNhan);
            iva.YNK.SetValue(hoadon.PhanLoaiXuatNhap);
            iva.TII.SetValue(hoadon.MaDaiLyHQ);
            iva.IVN.SetValue(hoadon.SoHoaDon);
            iva.IVD.SetValue(hoadon.NgayLapHoaDon);
            iva.IVB.SetValue(hoadon.DiaDiemLapHoaDon);
            iva.HOS.SetValue(hoadon.PhuongThucThanhToan);
            iva.IMC.SetValue(hoadon.MaNguoiXNK);
            iva.IMN.SetValue(hoadon.TenNguoiXNK);
            iva.IMY.SetValue(hoadon.MaBuuChinh_NguoiXNK);
            iva.IMA.SetValue(hoadon.DiaChi_NguoiXNK);
            iva.IMT.SetValue(hoadon.SoDienThoai_NguoiXNK);
            iva.SAA.SetValue(hoadon.NguoiLapHoaDon);
            iva.EPC.SetValue(hoadon.MaNguoiGuiNhan);
            iva.EPN.SetValue(hoadon.TenNguoiGuiNhan);
            iva.EP1.SetValue(hoadon.MaBuuChinhNguoiGuiNhan);
            iva.EPA.SetValue(hoadon.DiaChiNguoiNhanGui1);
            iva.EP2.SetValue(hoadon.DiaChiNguoiNhanGui2);
            iva.EP3.SetValue(hoadon.DiaChiNguoiNhanGui3);
            iva.EP4.SetValue(hoadon.DiaChiNguoiNhanGui4);
            iva.EP6.SetValue(hoadon.SoDienThoaiNhanGui);
            iva.EEE.SetValue(hoadon.NuocSoTaiNhanGui);
            iva.KNO.SetValue(hoadon.MaKyHieu);
            iva.TTP.SetValue(hoadon.PhanLoaiVanChuyen);
            iva.VSS.SetValue(hoadon.TenPTVC);
            iva.VNO.SetValue(hoadon.SoHieuChuyenDi);
            iva.PSC.SetValue(hoadon.MaDiaDiemXepHang);
            iva.PSN.SetValue(hoadon.TenDiaDiemXepHang);
            iva.PSD.SetValue(hoadon.ThoiKyXephang);
            iva.DST.SetValue(hoadon.MaDiaDiemDoHang);
            iva.DCN.SetValue(hoadon.TenDiaDiemDoHang);
            iva.KYC.SetValue(hoadon.MaDiaDiemTrungChuyen);
            iva.KYN.SetValue(hoadon.TenDiaDiemTrungChuyen);
            iva.GWJ.SetValue(hoadon.TrongLuongGross);
            iva.GWT.SetValue(hoadon.MaDVT_TrongLuongGross);
            iva.JWJ.SetValue(hoadon.TrongLuongThuan);
            iva.JWT.SetValue(hoadon.MaDVT_TrongLuongThuan);
            iva.STJ.SetValue(hoadon.TongTheTich);
            iva.STT.SetValue(hoadon.MaDVT_TheTich);
            iva.NOJ.SetValue(hoadon.TongSoKienHang);
            iva.NOT.SetValue(hoadon.MaDVT_KienHang);
            iva.NT3.SetValue(hoadon.GhiChuChuHang);
            iva.PLN.SetValue(hoadon.SoPL);
            iva.LCB.SetValue(hoadon.NganHangLC);
            iva.FON.SetValue(hoadon.TriGiaFOB);
            iva.FOT.SetValue(hoadon.MaTT_FOB);
            iva.FKK.SetValue(hoadon.SoTienFOB);
            iva.FKT.SetValue(hoadon.MaTT_TienFOB);
            iva.FR3.SetValue(hoadon.PhiVanChuyen);
            iva.FR2.SetValue(hoadon.MaTT_PhiVC);
            iva.FR4.SetValue(hoadon.NoiThanhToanPhiVC);
            iva.FS1.SetValue(hoadon.ChiPhiXepHang1);
            iva.FT1.SetValue(hoadon.MaTT_ChiPhiXepHang1);
            iva.FH1.SetValue(hoadon.LoaiChiPhiXepHang1);
            iva.FS2.SetValue(hoadon.ChiPhiXepHang2);
            iva.FT2.SetValue(hoadon.MaTT_ChiPhiXepHang2);
            iva.FH2.SetValue(hoadon.LoaiChiPhiXepHang2);
            iva.NUH.SetValue(hoadon.PhiVC_DuongBo);
            iva.NTK.SetValue(hoadon.MaTT_PhiVC_DuongBo);
            iva.IN3.SetValue(hoadon.PhiBaoHiem);
            iva.IN2.SetValue(hoadon.MaTT_PhiBaoHiem);
            iva.IK1.SetValue(hoadon.SoTienPhiBaoHiem);
            iva.IK2.SetValue(hoadon.MaTT_TienPhiBaoHiem);
            iva.NBG.SetValue(hoadon.SoTienKhauTru);
            iva.NBC.SetValue(hoadon.MaTT_TienKhauTru);
            iva.NBS.SetValue(hoadon.LoaiKhauTru);
            iva.SKG.SetValue(hoadon.SoTienKhac);
            iva.SKC.SetValue(hoadon.MaTT_TienKhac);
            iva.SKS.SetValue(hoadon.LoaiSoTienKhac);
            iva.IP4.SetValue(hoadon.TongTriGiaHoaDon);
            iva.IP3.SetValue(hoadon.MaTT_TongTriGia);
            iva.IP2.SetValue(hoadon.DieuKienGiaHoaDon);
            iva.HWT.SetValue(hoadon.DiaDiemGiaoHang);
            iva.NT1.SetValue(hoadon.GhiChuDacBiet);
            iva.SNM.SetValue(hoadon.TongSoDongHang);
            #region Chứng từ phân loại
            for (int i = 0; i < hoadon.PhanLoaiCollection.Count && i < 12; i++)
            {
                iva.NOM.SetValueCollection(hoadon.PhanLoaiCollection[i].MaPhaLoai, EnumGroupID.IVA_N__, i);
                iva.NOM.SetValueCollection(hoadon.PhanLoaiCollection[i].So, EnumGroupID.IVA_O__, i);
                iva.NOM.SetValueCollection(hoadon.PhanLoaiCollection[i].NgayThangNam, EnumGroupID.IVA_M__, i);
            }
            #endregion
            #region Phân loại bên liên quan
            iva.AT_.SetValueCollection("", EnumGroupID.IVA_AT_, 0);
            iva.AT_.SetValueCollection("", EnumGroupID.IVA_AT_, 1);
            #endregion
            iva.HangHoaTrongHoaDon = new List<IVA_HANGHOA>();
            int index = 0;
            foreach (var item in hoadon.HangCollection)
            {
                index++;
                IVA_HANGHOA ivaHangHoa = new IVA_HANGHOA();
                ivaHangHoa.RNO.SetValue(index);
                ivaHangHoa.SNO.SetValue(item.MaHang);
                ivaHangHoa.CMD.SetValue(item.MaSoHang);
                ivaHangHoa.CMN.SetValue(item.TenHang);
                ivaHangHoa.ORC.SetValue(item.MaNuocXX);
                ivaHangHoa.ORN.SetValue(item.TenNuocXX);
                ivaHangHoa.KBN.SetValue(item.SoKienHang);
                ivaHangHoa.QN1.SetValue(item.SoLuong1);
                ivaHangHoa.QT1.SetValue(item.MaDVT_SoLuong1);
                ivaHangHoa.QN2.SetValue(item.SoLuong2);
                ivaHangHoa.QT2.SetValue(item.MaDVT_SoLuong2);
                ivaHangHoa.TNK.SetValue(item.DonGiaHoaDon);
                ivaHangHoa.TNC.SetValue(item.MaTT_DonGia);
                ivaHangHoa.TSC.SetValue(item.MaDVT_DonGia);
                ivaHangHoa.KKT.SetValue(item.TriGiaHoaDon);
                ivaHangHoa.KKC.SetValue(item.MaTT_GiaTien);
                ivaHangHoa.NSR.SetValue(item.LoaiKhauTru);
                ivaHangHoa.NGA.SetValue(item.SoTienKhauTru);
                ivaHangHoa.NTC.SetValue(item.MaTT_TienKhauTru);

                iva.HangHoaTrongHoaDon.Add(ivaHangHoa);
            }

            return iva;
        }
        public static IVA01 IVA01Maper(decimal SoTiepNhan)
        {
            IVA01 IVA01 = new IVA01();
            IVA01.NIV.SetValue(SoTiepNhan);
            return IVA01;
        }
        public static IIV IIVMaper(decimal SoTiepNhan)
        {
            IIV iiv = new IIV();
            iiv.NIV.SetValue(SoTiepNhan);
            return iiv;
        }
        /*DANH MUC MIEN THUE*/
        public static TEA TEAMapper(KDT_VNACCS_TEA DMMT)
        {
            TEA tea = new TEA();
            tea.TEN.SetValue(DMMT.SoDanhMucMienThue);
            tea.IEC.SetValue(DMMT.PhanLoaiXuatNhapKhau);
            tea.CH.SetValue(DMMT.CoQuanHaiQuan);
            tea.IMA.SetValue(DMMT.DiaChiCuaNguoiKhai);
            tea.IMT.SetValue(DMMT.SDTCuaNguoiKhai);
            tea.TED.SetValue(DMMT.ThoiHanMienThue);
            tea.IPN.SetValue(DMMT.TenDuAnDauTu);
            tea.PPC.SetValue(DMMT.DiaDiemXayDungDuAn);
            tea.PO.SetValue(DMMT.MucTieuDuAn);
            tea.RE.SetValue(DMMT.MaMienGiam);
            tea.SEL.SetValue(DMMT.PhamViDangKyDMMT);
            tea.PTI.SetValue(DMMT.NgayDuKienXNK);
            tea.ICN.SetValue(DMMT.GP_GCNDauTuSo);
            tea.DOC.SetValue(DMMT.NgayChungNhan);
            tea.IB.SetValue(DMMT.CapBoi);
            if (DMMT.DieuChinhCollection != null && DMMT.DieuChinhCollection.Count > 0)
            {
                #region Giay phep dieu chinh 
                for (int i = 0; i < DMMT.DieuChinhCollection.Count & i < 5; i++)
                {
                    tea.IC_.SetValueCollection(DMMT.DieuChinhCollection[i].LanDieuChinhGP_GCN, EnumGroupID.TEA_IC_, i);
                    tea.IC_.SetValueCollection(DMMT.DieuChinhCollection[i].ChungNhanDieuChinhSo, EnumGroupID.TEA_CA_, i);
                    tea.IC_.SetValueCollection(DMMT.DieuChinhCollection[i].NgayChungNhanDieuChinh, EnumGroupID.TEA_DA_, i);
                    tea.IC_.SetValueCollection(DMMT.DieuChinhCollection[i].DieuChinhBoi, EnumGroupID.TEA_AI_, i);                 
                }
                #endregion
            }
            if (DMMT.NguoiXNKCollection != null && DMMT.NguoiXNKCollection.Count > 0)
            {
                #region nguoi XNK
                for (int i = 0; i < DMMT.NguoiXNKCollection.Count & i < 15; i++)
                {
                    tea.C__.SetValueCollection(DMMT.NguoiXNKCollection[i].MaNguoiXNK, EnumGroupID.TEA_C__, i);
                    tea.C__.SetValueCollection(DMMT.NguoiXNKCollection[i].TenNguoiXNK, EnumGroupID.TEA_N__, i);
                }
                #endregion
            }
            tea.IR.SetValue(DMMT.GhiChu);
            tea.CRP.SetValue(DMMT.CamKetSuDung);

            tea.listHang = new List<TEA_HANG>();
            foreach (KDT_VNACCS_TEA_HangHoa item in DMMT.HangCollection)
            {
                tea.listHang.Add(TEA_HANGMapper(item));
            }

            return tea;
        }
        public static TEA_HANG TEA_HANGMapper(KDT_VNACCS_TEA_HangHoa DMMT)
        {
            TEA_HANG tea_hang = new TEA_HANG();
            tea_hang.CMN.SetValue(DMMT.MoTaHangHoa);
            tea_hang.QT.SetValue(DMMT.SoLuongDangKyMT,true);
            tea_hang.QTU.SetValue(DMMT.DVTSoLuongDangKyMT);
            tea_hang.UT.SetValue(DMMT.SoLuongDaSuDung,true);
            tea_hang.UTU.SetValue(DMMT.DVTSoLuongDaSuDung);
            tea_hang.VA.SetValue(DMMT.TriGia);
            tea_hang.EVA.SetValue(DMMT.TriGiaDuKien);
            return tea_hang;
        }
        public static ITE ITEMapper(KDT_VNACCS_TEA DMMT)
        {
            ITE ite = new ITE();
            ite.CH.SetValue(DMMT.CoQuanHaiQuan);
            ite.IED.SetValue(DMMT.PhanLoaiXuatNhapKhau);
            ite.UCD.SetValue(DMMT.MaNguoiKhai);
            ite.IDF.SetValue(DMMT.KhaiBaoTuNgay);
            ite.IDT.SetValue(DMMT.KhaiBaoDenNgay);
            ite.CCN.SetValue(DMMT.MaSoQuanLyDSMT);
            ite.ICN.SetValue(DMMT.GP_GCNDauTuSo);
            ite.APC.SetValue(DMMT.PhanLoaiCapPhep);
            ite.TEN.SetValue(DMMT.SoDanhMucMienThue);
            return ite;
        }
        /*DANH MUC HANG TAM NHAP TAI XUAT*/
        public static TIA TIAMapper(KDT_VNACCS_TIA vnaccs_TIA)
        {
            TIA tia = new TIA();
            tia.ECN.SetValue(vnaccs_TIA.SoToKhai);
            tia.CFD.SetValue(vnaccs_TIA.MaNguoiKhaiDauTien);
            tia.NFD.SetValue(vnaccs_TIA.TenNguoiKhaiDauTien);
            tia.IEC.SetValue(vnaccs_TIA.MaNguoiXuatNhapKhau);
            tia.IEN.SetValue(vnaccs_TIA.TenNguoiXuatNhapKhau);
            tia.TED.SetValue(vnaccs_TIA.ThoiHanTaiXuatNhap);
            tia.listHang = new List<TIA_HANG>();
            foreach (KDT_VNACCS_TIA_HangHoa item in vnaccs_TIA.HangCollection)
            {
                tia.listHang.Add(TIA_HANGMapper(item));
            }

            return tia;
        }
        public static TIA_HANG TIA_HANGMapper(KDT_VNACCS_TIA_HangHoa vnaccs_TIA)
        {
            TIA_HANG tia = new TIA_HANG();
            tia.CMD.SetValue(vnaccs_TIA.MaSoHangHoa);
            tia.QT.SetValue(vnaccs_TIA.SoLuongBanDau);
            tia.QTU.SetValue(vnaccs_TIA.DVTSoLuongBanDau);
            tia.UT.SetValue(vnaccs_TIA.SoLuongDaTaiNhapTaiXuat);
            tia.UTU.SetValue(vnaccs_TIA.DVTSoLuongDaTaiNhapTaiXuat);
            return tia;
        }
        public static ITI ITIMapper(KDT_VNACCS_TIA vnaccs_TIA)
        {
            ITI iti = new ITI();
            iti.IEI.SetValue(vnaccs_TIA.CoBaoNhapKhauXuatKhau);
            iti.CH.SetValue(vnaccs_TIA.CoQuanHaiQuan);
            iti.TED.SetValue(vnaccs_TIA.ThoiHanTaiXuatNhap);
            iti.UCD.SetValue(vnaccs_TIA.MaNguoiXuatNhapKhau);
            iti.ICN.SetValue(vnaccs_TIA.SoToKhai);

            return iti;
        }
        /*TO KHAI VAN CHUYEN*/
        public static OLA OLAMapper(KDT_VNACC_ToKhaiVanChuyen TKVC)
        {       
            OLA ola = new OLA();
            ola.OLT.SetValue(TKVC.SoToKhaiVC);
            ola.IEI.SetValue(TKVC.CoBaoXuatNhapKhau);
            ola.CHC.SetValue(TKVC.CoQuanHaiQuan);
            ola.TRC.SetValue(TKVC.MaNguoiVC);
            ola.TRN.SetValue(TKVC.TenNguoiVC);
            ola.TRA.SetValue(TKVC.DiaChiNguoiVC);
            ola.TCN.SetValue(TKVC.SoHopDongVC);
            ola.TCD.SetValue(TKVC.NgayHopDongVC);
            ola.EDC.SetValue(TKVC.NgayHetHanHopDongVC);
            ola.BYA.SetValue(TKVC.MaPhuongTienVC);
            ola.OBJ.SetValue(TKVC.MaMucDichVC);
            ola.USS.SetValue(TKVC.LoaiHinhVanTai);
            ola.SDT.SetValue(TKVC.NgayDuKienBatDauVC);
            ola.SDH.SetValue(TKVC.GioDuKienBatDauVC);
            ola.EDT.SetValue(TKVC.NgayDuKienKetThucVC);
            ola.EDH.SetValue(TKVC.GioDuKienKetThucVC);
            ola.DPR.SetValue(TKVC.MaDiaDiemXepHang);
            ola.BRT.SetValue(TKVC.MaViTriXepHang);
            ola.PRT.SetValue(TKVC.MaCangCuaKhauGaXepHang);
            ola.DPN.SetValue(TKVC.DiaDiemXepHang);
            ola.ARR.SetValue(TKVC.MaDiaDiemDoHang);
            ola.ARB.SetValue(TKVC.MaViTriDoHang);
            ola.ARP.SetValue(TKVC.MaCangCuaKhauGaDoHang);
            ola.ARN.SetValue(TKVC.DiaDiemDoHang);
            ola.ROU.SetValue(TKVC.TuyenDuongVC);
            ola.TOS.SetValue(TKVC.LoaiBaoLanh);
            ola.SBC.SetValue(TKVC.MaNganHangBaoLanh);
            ola.RYA.SetValue(TKVC.NamPhatHanhBaoLanh);
            ola.SCM.SetValue(TKVC.KyHieuChungTuBaoLanh);
            ola.SCN.SetValue(TKVC.SoChungTuBaoLanh);
            ola.SEP.SetValue(TKVC.SoTienBaoLanh);
            ola.NTA.SetValue(TKVC.GhiChu);
            if (TKVC.VanDonCollection != null && TKVC.VanDonCollection.Count > 0)
            {
                #region Vân đơn
                for (int i = 0; i < TKVC.VanDonCollection.Count & i < 5; i++)
                {
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].SoVanDon, EnumGroupID.OLA_CG_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayPhatHanhVD, EnumGroupID.OLA_DB_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MoTaHangHoa, EnumGroupID.OLA_CM_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaHS, EnumGroupID.OLA_HS_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].KyHieuVaSoHieu, EnumGroupID.OLA_MR_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayNhapKhoHQLanDau, EnumGroupID.OLA_IS_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].PhanLoaiSanPhan, EnumGroupID.OLA_RM_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaNuocSanXuat, EnumGroupID.OLA_OR_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDiaDiemXuatPhatVC, EnumGroupID.OLA_LP_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDiaDiemDichVC, EnumGroupID.OLA_PA_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].LoaiHangHoa, EnumGroupID.OLA_TM_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaPhuongTienVC, EnumGroupID.OLA_TE_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].TenPhuongTienVC, EnumGroupID.OLA_SN_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayHangDuKienDenDi, EnumGroupID.OLA_DD_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaNguoiNhapKhau, EnumGroupID.OLA_IM_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].TenNguoiNhapKhau, EnumGroupID.OLA_IN_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].DiaChiNguoiNhapKhau, EnumGroupID.OLA_IA_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaNguoiXuatKhua, EnumGroupID.OLA_EM_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].TenNguoiXuatKhau, EnumGroupID.OLA_EN_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].DiaChiNguoiXuatKhau, EnumGroupID.OLA_EA_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaNguoiUyThac, EnumGroupID.OLA_KM_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].TenNguoiUyThac, EnumGroupID.OLA_KN_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].DiaChiNguoiUyThac, EnumGroupID.OLA_KA_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat1, EnumGroupID.OLA_OA_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat2, EnumGroupID.OLA_OB_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat3, EnumGroupID.OLA_OC_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat4, EnumGroupID.OLA_OD_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat5, EnumGroupID.OLA_OE_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].SoLuong, EnumGroupID.OLA_PC_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDVTSoLuong, EnumGroupID.OLA_PU_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].TongTrongLuong, EnumGroupID.OLA_GW_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDVTTrongLuong, EnumGroupID.OLA_WU_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].TheTich, EnumGroupID.OLA_VO_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDVTTheTich, EnumGroupID.OLA_VU_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].TriGia, EnumGroupID.OLA_PR_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDVTTriGia, EnumGroupID.OLA_RT_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH1, EnumGroupID.OLA_RA_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH2, EnumGroupID.OLA_RB_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH3, EnumGroupID.OLA_RC_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH4, EnumGroupID.OLA_RD_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH5, EnumGroupID.OLA_RE, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].SoGiayPhep, EnumGroupID.OLA_PM_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayCapPhep, EnumGroupID.OLA_PD_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayHetHanCapPhep, EnumGroupID.OLA_EP_, i);
                    ola.CG_.SetValueCollection(TKVC.VanDonCollection[i].GhiChu, EnumGroupID.OLA_NT_, i);
                }
                #endregion

            }
            if (TKVC.TKXuatCollection != null && TKVC.TKXuatCollection.Count > 0)
            {
                #region SoTKXuat
                for (int i = 0; i < TKVC.TKXuatCollection.Count & i < 50; i++)
                {
                    ola.E__.SetValueCollection(TKVC.TKXuatCollection[i].SoToKhaiXuat, EnumGroupID.OLA_E__, i);
                }
                #endregion

            }

            ola.listCont = new List<OLA_HANG>();
            foreach (KDT_VNACC_TKVC_Container item in TKVC.ContainerCollection)
            {
                ola.listCont.Add(OLA_HANGMapper(item));
            }

            return ola;
        }
        public static OLA_HANG OLA_HANGMapper(KDT_VNACC_TKVC_Container TKVC_Cont)
        {
            OLA_HANG ola = new OLA_HANG();
            ola.CNO.SetValue(TKVC_Cont.SoHieuContainer);
            ola.RNO.SetValue(TKVC_Cont.SoDongHangTrenTK);
            ola.SE1.SetValue(TKVC_Cont.SoSeal1);
            ola.SE2.SetValue(TKVC_Cont.SoSeal2);
            ola.SE3.SetValue(TKVC_Cont.SoSeal3);
            ola.SE4.SetValue(TKVC_Cont.SoSeal4);
            ola.SE5.SetValue(TKVC_Cont.SoSeal5);
            ola.SE6.SetValue(TKVC_Cont.SoSeal6);
            return ola;
        }
        public static OLC OLCMapper(KDT_VNACC_ToKhaiVanChuyen TKVC,bool XacNhanCungSoQuanLyHangHoa)
        {
            OLC olc = new OLC();
            olc.OLT.SetValue(TKVC.SoToKhaiVC);
            olc.SCG.SetValue(XacNhanCungSoQuanLyHangHoa ? 1 : 0,true);
            return olc;
        }
        public static COT cotMapper(KDT_VNACC_ToKhaiVanChuyen TKVC, bool isHuy)
        {
            COT cot = new COT();
            cot.KND.SetValue(isHuy ? "1" : "5");
            cot.OLT.SetValue(TKVC.SoToKhaiVC);
            cot.IEI.SetValue(TKVC.CoBaoXuatNhapKhau);
            cot.CHC.SetValue(TKVC.CoQuanHaiQuan);
            cot.TRC.SetValue(TKVC.MaNguoiVC);
            cot.TRN.SetValue(TKVC.TenNguoiVC);
            cot.TRA.SetValue(TKVC.DiaChiNguoiVC);
            cot.TCN.SetValue(TKVC.SoHopDongVC);
            cot.TCD.SetValue(TKVC.NgayHopDongVC);
            cot.EDC.SetValue(TKVC.NgayHetHanHopDongVC);
            cot.BYA.SetValue(TKVC.MaPhuongTienVC);
            cot.OBJ.SetValue(TKVC.MaMucDichVC);
            cot.USS.SetValue(TKVC.LoaiHinhVanTai);
            cot.SDT.SetValue(TKVC.NgayDuKienBatDauVC);
            cot.SDH.SetValue(TKVC.GioDuKienBatDauVC);
            cot.EDT.SetValue(TKVC.NgayDuKienKetThucVC);
            cot.EDH.SetValue(TKVC.GioDuKienKetThucVC);
            cot.DPD.SetValue(TKVC.NgayDenDiaDiem_XH);
            if (TKVC.TrungChuyenCollection != null && TKVC.TrungChuyenCollection.Count > 0)
            {
                #region TrungChuyen
                for (int i = 0; i < TKVC.TrungChuyenCollection.Count & i < 3; i++)
                {
                    cot.TR_.SetValueCollection(TKVC.TrungChuyenCollection[i].MaDiaDiemTrungChuyen, EnumGroupID.COT_TR_, i);
                    cot.TR_.SetValueCollection(TKVC.TrungChuyenCollection[i].NgayDenDiaDiem_TC, EnumGroupID.COT_AR_, i);
                    cot.TR_.SetValueCollection(TKVC.TrungChuyenCollection[i].NgayDenDiaDiem_TC, EnumGroupID.COT_ZB_, i);
                    cot.TR_.SetValueCollection(TKVC.TrungChuyenCollection[i].NgayDiDiaDiem_TC, EnumGroupID.COT_DP_, i);
                    cot.TR_.SetValueCollection(TKVC.TrungChuyenCollection[i].NgayDiDiaDiem_TC, EnumGroupID.COT_ZC_, i);
                }
                #endregion

            }
            //cot.BRT.SetValue(TKVC.MaViTriXepHang);
            //cot.PRT.SetValue(TKVC.MaCangCuaKhauGaXepHang);
            //cot.DPN.SetValue(TKVC.DiaDiemXepHang);
            cot.ARR.SetValue(TKVC.MaDiaDiemDoHang);
            cot.ARB.SetValue(TKVC.MaViTriDoHang);
            cot.ARP.SetValue(TKVC.MaCangCuaKhauGaDoHang);
            cot.ARN.SetValue(TKVC.DiaDiemDoHang);
            cot.ARA.SetValue(TKVC.NgayDenDiaDiem_DH);
            cot.ROU.SetValue(TKVC.TuyenDuongVC);
            cot.TOS.SetValue(TKVC.LoaiBaoLanh);
            cot.SBC.SetValue(TKVC.MaNganHangBaoLanh);
            cot.RYA.SetValue(TKVC.NamPhatHanhBaoLanh);
            cot.SCM.SetValue(TKVC.KyHieuChungTuBaoLanh);
            cot.SCN.SetValue(TKVC.SoChungTuBaoLanh);
            cot.SEP.SetValue(TKVC.SoTienBaoLanh);
            cot.NTA.SetValue(TKVC.GhiChu);
            if (TKVC.VanDonCollection != null && TKVC.VanDonCollection.Count > 0)
            {
                #region Vân đơn
                for (int i = 0; i < TKVC.VanDonCollection.Count & i < 5; i++)
                {
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].SoVanDon, EnumGroupID.COT_CG_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayPhatHanhVD, EnumGroupID.COT_DB_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MoTaHangHoa, EnumGroupID.COT_CM_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaHS, EnumGroupID.COT_HS_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].KyHieuVaSoHieu, EnumGroupID.COT_MR_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayNhapKhoHQLanDau, EnumGroupID.COT_IS_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].PhanLoaiSanPhan, EnumGroupID.COT_RM_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaNuocSanXuat, EnumGroupID.COT_OR_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDiaDiemXuatPhatVC, EnumGroupID.COT_LP_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDiaDiemDichVC, EnumGroupID.COT_PA_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].LoaiHangHoa, EnumGroupID.COT_TM_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaPhuongTienVC, EnumGroupID.COT_TE_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].TenPhuongTienVC, EnumGroupID.COT_SN_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayHangDuKienDenDi, EnumGroupID.COT_DD_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaNguoiNhapKhau, EnumGroupID.COT_IM_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].TenNguoiNhapKhau, EnumGroupID.COT_IN_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].DiaChiNguoiNhapKhau, EnumGroupID.COT_IA_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaNguoiXuatKhua, EnumGroupID.COT_EM_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].TenNguoiXuatKhau, EnumGroupID.COT_EN_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].DiaChiNguoiXuatKhau, EnumGroupID.COT_EA_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaNguoiUyThac, EnumGroupID.COT_KM_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].TenNguoiUyThac, EnumGroupID.COT_KN_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].DiaChiNguoiUyThac, EnumGroupID.COT_KA_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat1, EnumGroupID.COT_OA_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat2, EnumGroupID.COT_OB_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat3, EnumGroupID.COT_OC_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat4, EnumGroupID.COT_OD_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaVanBanPhapLuat5, EnumGroupID.COT_OE_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].SoLuong, EnumGroupID.COT_PC_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDVTSoLuong, EnumGroupID.COT_PU_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].TongTrongLuong, EnumGroupID.COT_GW_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDVTTrongLuong, EnumGroupID.COT_WU_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].TheTich, EnumGroupID.COT_VO_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDVTTheTich, EnumGroupID.COT_VU_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].TriGia, EnumGroupID.COT_PR_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDVTTriGia, EnumGroupID.COT_RT_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH1, EnumGroupID.COT_RA_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH2, EnumGroupID.COT_RB_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH3, EnumGroupID.COT_RC_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH4, EnumGroupID.COT_RD_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].MaDanhDauDDKH5, EnumGroupID.COT_RE, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].SoGiayPhep, EnumGroupID.COT_PM_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayCapPhep, EnumGroupID.COT_PD_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].NgayHetHanCapPhep, EnumGroupID.COT_EP_, i);
                    cot.CG_.SetValueCollection(TKVC.VanDonCollection[i].GhiChu, EnumGroupID.COT_NT_, i);
                }
                #endregion

            }
            if (TKVC.TKXuatCollection != null && TKVC.TKXuatCollection.Count > 0)
            {
                #region SoTKXuat
                for (int i = 0; i < TKVC.TKXuatCollection.Count & i < 50; i++)
                {
                    cot.E__.SetValueCollection(TKVC.TKXuatCollection[i].SoToKhaiXuat, EnumGroupID.COT_E__, i);
                }
                #endregion

            }

            cot.listCont = new List<COT_HANG>();
            foreach (KDT_VNACC_TKVC_Container item in TKVC.ContainerCollection)
            {
                cot.listCont.Add(COT_HANGMapper(item));
            }

            return cot;
        }
        public static COT_HANG COT_HANGMapper(KDT_VNACC_TKVC_Container TKVC_Cont)
        {
            COT_HANG cot = new COT_HANG();
            cot.CNO.SetValue(TKVC_Cont.SoHieuContainer);
            cot.RNO.SetValue(TKVC_Cont.SoDongHangTrenTK);
            cot.SE1.SetValue(TKVC_Cont.SoSeal1);
            cot.SE2.SetValue(TKVC_Cont.SoSeal2);
            cot.SE3.SetValue(TKVC_Cont.SoSeal3);
            cot.SE4.SetValue(TKVC_Cont.SoSeal4);
            cot.SE5.SetValue(TKVC_Cont.SoSeal5);
            cot.SE6.SetValue(TKVC_Cont.SoSeal6);
            return cot;
        }
        public static BOA BOAMapper(KDT_VNACC_ToKhaiVanChuyen TKVC)
        {
            BOA boa = new BOA();
            boa.KND.SetValue(TKVC.MaPhanLoaiXuLy);
            boa.CGO.SetValue(TKVC.SoToKhaiVC);
            boa.DPR.SetValue(TKVC.MaDiaDiemXepHang);
            boa.BRT.SetValue(TKVC.MaViTriXepHang);
            boa.PRT.SetValue(TKVC.MaCangCuaKhauGaXepHang);
            boa.ARR.SetValue(TKVC.MaDiaDiemDoHang);
            boa.ARB.SetValue(TKVC.MaViTriDoHang);
            boa.ARP.SetValue(TKVC.MaCangCuaKhauGaDoHang);
            boa.DTM.SetValue(TKVC.NgayDuKienBatDauVC);
            boa.TIM.SetValue(TKVC.GioDuKienBatDauVC);
            return boa;
        }
        /*TO KHAI*/
        public static IDA IDAMapper(KDT_VNACC_ToKhaiMauDich TKMD)
        {
            IDA ida = new IDA();
            ida.ICN.SetValue(TKMD.SoToKhai);
            ida.FIC.SetValue(TKMD.SoToKhaiDauTien);
            ida.BNO.SetValue(TKMD.SoNhanhToKhai);
            ida.DNO.SetValue(TKMD.TongSoTKChiaNho);
            ida.TDN.SetValue(TKMD.SoToKhaiTNTX);
            ida.ICB.SetValue(TKMD.MaLoaiHinh);
            ida.CCC.SetValue(TKMD.MaPhanLoaiHH);
            ida.MTC.SetValue(TKMD.MaPhuongThucVT);
            ida.SKB.SetValue(TKMD.PhanLoaiToChuc);
            ida.CH.SetValue(TKMD.CoQuanHaiQuan);
            ida.CHB.SetValue(TKMD.NhomXuLyHS);
            ida.RED.SetValue(TKMD.ThoiHanTaiNhapTaiXuat);
            ida.ICD.SetValue(TKMD.NgayDangKy.Year > 1900 ? TKMD.NgayDangKy : new DateTime(1900,1,1)); // Khai IDA không có ngày đăng ký
            ida.IMC.SetValue(TKMD.MaDonVi);
            ida.IMN.SetValue(TKMD.TenDonVi);
            ida.IMY.SetValue(TKMD.MaBuuChinhDonVi);
            ida.IMA.SetValue(TKMD.DiaChiDonVi);
            ida.IMT.SetValue(TKMD.SoDienThoaiDonVi);
            ida.NMC.SetValue(TKMD.MaUyThac);
            ida.NMN.SetValue(TKMD.TenUyThac);
            ida.EPC.SetValue(TKMD.MaDoiTac);
            ida.EPN.SetValue(TKMD.TenDoiTac);
            ida.EPY.SetValue(TKMD.MaBuuChinhDoiTac);
            ida.EPA.SetValue(TKMD.DiaChiDoiTac1);
            ida.EP2.SetValue(TKMD.DiaChiDoiTac2);
            ida.EP3.SetValue(TKMD.DiaChiDoiTac3);
            ida.EP4.SetValue(TKMD.DiaChiDoiTac4);
            ida.EPO.SetValue(TKMD.MaNuocDoiTac);
            ida.ENM.SetValue(TKMD.NguoiUyThacXK);
            ida.ICC.SetValue(TKMD.MaDaiLyHQ);
            if (TKMD.VanDonCollection != null && TKMD.VanDonCollection.Count > 0)
            {
                #region Vân đơn
                for (int i = 0; i < TKMD.VanDonCollection.Count & i < 5; i++)
                {
                    //if (TKMD.VanDonCollection[i].LoaiDinhDanh == "2722")
                    //{
                    //    ida.BL_.SetValueCollection(TKMD.VanDonCollection[i].NgayVanDon.ToString("dd/MM/yyyy").Replace("/","") + "" + TKMD.VanDonCollection[i].SoVanDon, EnumGroupID.IDA_BL_, i);
                    //}
                    //else
                    //{
                        ida.BL_.SetValueCollection(TKMD.VanDonCollection[i].SoDinhDanh, EnumGroupID.IDA_BL_, i);
                    //}
                }
                #endregion
            }
            ida.NO.SetValue(TKMD.SoLuong);
            ida.NOT.SetValue(TKMD.MaDVTSoLuong);
            ida.GW.SetValue(TKMD.TrongLuong);
            ida.GWT.SetValue(TKMD.MaDVTTrongLuong);
            ida.ST.SetValue(TKMD.MaDDLuuKho);
            ida.MRK.SetValue(TKMD.SoHieuKyHieu);
            ida.VSC.SetValue(TKMD.MaPTVC);
            ida.VSN.SetValue(TKMD.TenPTVC);
            ida.ARR.SetValue(TKMD.NgayHangDen);
            ida.DST.SetValue(TKMD.MaDiaDiemDoHang);
            ida.DSN.SetValue(TKMD.TenDiaDiemDohang);
            ida.PSC.SetValue(TKMD.MaDiaDiemXepHang);
            ida.PSN.SetValue(TKMD.TenDiaDiemXepHang);
            ida.COC.SetValue(TKMD.SoLuongCont);
            ida.N4.SetValue(TKMD.MaKetQuaKiemTra);
            if (TKMD.GiayPhepCollection != null && TKMD.GiayPhepCollection.Count > 0)
            {
                #region giấy phép
                for (int i = 0; i < TKMD.GiayPhepCollection.Count && i < 5; i++)
                {
                    ida.SS_.SetValueCollection(TKMD.GiayPhepCollection[i].PhanLoai, EnumGroupID.IDA_SS_, i);
                    ida.SS_.SetValueCollection(TKMD.GiayPhepCollection[i].SoGiayPhep, EnumGroupID.IDA_SN_, i);
                }
                #endregion
            }
            ida.IV1.SetValue(TKMD.PhanLoaiHD);
            ida.IV2.SetValue(TKMD.SoTiepNhanHD);
            ida.IV3.SetValue(TKMD.SoHoaDon);
            ida.IVD.SetValue(TKMD.NgayPhatHanhHD);
            ida.IVP.SetValue(TKMD.PhuongThucTT);
            ida.IP1.SetValue(TKMD.PhanLoaiGiaHD);
            ida.IP2.SetValue(TKMD.MaDieuKienGiaHD);
            ida.IP3.SetValue(TKMD.MaTTHoaDon);

            ida.IP4.SetValue(TKMD.TongTriGiaHD);
            ida.VD1.SetValue(TKMD.MaPhanLoaiTriGia);
            
            if (TKMD.SoTiepNhanTKTriGia == 0)
                ida.VD2.SetValue("");
            else
                ida.VD2.SetValue(TKMD.SoTiepNhanTKTriGia);
            ida.VCC.SetValue(TKMD.MaTTHieuChinhTriGia);
            ida.VPC.SetValue(TKMD.GiaHieuChinhTriGia);
            ida.FR1.SetValue(TKMD.MaPhanLoaiPhiVC);
            ida.FR2.SetValue(TKMD.MaTTPhiVC);
            ida.FR3.SetValue(TKMD.PhiVanChuyen, !string.IsNullOrEmpty(TKMD.MaPhanLoaiPhiVC));
            ida.IN1.SetValue(TKMD.MaPhanLoaiPhiBH);
            ida.IN2.SetValue(TKMD.MaTTPhiBH);
            ida.IN3.SetValue(TKMD.PhiBaoHiem);
            ida.IN4.SetValue(TKMD.SoDangKyBH);
            if (TKMD.KhoanDCCollection != null && TKMD.KhoanDCCollection.Count > 0)
            {
                #region Khoảng điều chỉnh
                for (int i = 0; i < TKMD.KhoanDCCollection.Count && i < 5; i++)
                {
                    ida.VR_.SetValueCollection(TKMD.KhoanDCCollection[i].MaTenDieuChinh, EnumGroupID.IDA_VR_, i);
                    ida.VR_.SetValueCollection(TKMD.KhoanDCCollection[i].MaPhanLoaiDieuChinh, EnumGroupID.IDA_VI_, i);
                    ida.VR_.SetValueCollection(TKMD.KhoanDCCollection[i].MaTTDieuChinhTriGia, EnumGroupID.IDA_VC_, i);
                    ida.VR_.SetValueCollection(TKMD.KhoanDCCollection[i].TriGiaKhoanDieuChinh, EnumGroupID.IDA_VP_, i);
                    ida.VR_.SetValueCollection(TKMD.KhoanDCCollection[i].TongHeSoPhanBo, EnumGroupID.IDA_VT_, i);
                }
                #endregion
            }
            ida.VLD.SetValue(TKMD.ChiTietKhaiTriGia);
            //them tổng hệ số phân bổ trị giá
            if (TKMD.TongHeSoPhanBoTG != 0)
                ida.TP.SetValue(TKMD.TongHeSoPhanBoTG);
            else
                ida.TP.SetValue("");
            ida.TPM.SetValue(TKMD.NguoiNopThue);
            ida.BP.SetValue(TKMD.MaLyDoDeNghiBP);
            ida.BRC.SetValue(TKMD.MaNHTraThueThay);
            ida.BYA.SetValue(TKMD.NamPhatHanhHM);
            ida.BCM.SetValue(TKMD.KyHieuCTHanMuc);
            ida.BCN.SetValue(TKMD.SoCTHanMuc);
            ida.ENC.SetValue(TKMD.MaXDThoiHanNopThue);
            ida.SBC.SetValue(TKMD.MaNHBaoLanh);
            ida.RYA.SetValue(TKMD.NamPhatHanhBL);
            ida.SCM.SetValue(TKMD.KyHieuCTBaoLanh);
            ida.SCN.SetValue(TKMD.SoCTBaoLanh);
            if (TKMD.DinhKemCollection != null && TKMD.DinhKemCollection.Count > 0)
            {
                #region Đính kèm khai báo điện tử
                for (int i = 0; i < TKMD.DinhKemCollection.Count && i < 3; i++)
                {
                    ida.EA_.SetValueCollection(TKMD.DinhKemCollection[i].PhanLoai, EnumGroupID.IDA_EA_, i);
                    ida.EA_.SetValueCollection(TKMD.DinhKemCollection[i].SoDinhKemKhaiBaoDT, EnumGroupID.IDA_EB_, i);
                }
                #endregion
            }
            ida.ISD.SetValue(TKMD.NgayNhapKhoDau);
            ida.DPD.SetValue(TKMD.NgayKhoiHanhVC);
            
            for (int phapquy = 0; phapquy < 5;phapquy++ )
            {
                switch (phapquy)
                {
                    case 0:
                        ida.OL_.SetValueCollection(TKMD.MaVanbanPhapQuy1, EnumGroupID.IDA_OL_,phapquy);
                        break;
                    case 1:
                        ida.OL_.SetValueCollection(TKMD.MaVanbanPhapQuy2, EnumGroupID.IDA_OL_, phapquy);
                        break;
                    case 2:
                        ida.OL_.SetValueCollection(TKMD.MaVanbanPhapQuy3, EnumGroupID.IDA_OL_, phapquy);
                        break;
                    case 3:
                        ida.OL_.SetValueCollection(TKMD.MaVanbanPhapQuy4, EnumGroupID.IDA_OL_, phapquy);
                        break;
                    case 4:
                        ida.OL_.SetValueCollection(TKMD.MaVanbanPhapQuy5, EnumGroupID.IDA_OL_, phapquy);
                        break;
                    default:
                        break;
                }

            }
           
            ida.ARP.SetValue(TKMD.DiaDiemDichVC);
            ida.ADT.SetValue(TKMD.NgayDen);
            ida.NT2.SetValue(TKMD.GhiChu);
            ida.REF.SetValue(TKMD.SoQuanLyNoiBoDN);
            //ida.CCM.SetValue(TKMD.PhanLoai);
            if (TKMD.ChiThiHQCollection != null && TKMD.ChiThiHQCollection.Count > 0)
            {
                //if (TKMD.ChiThiHQCollection.Count > 0) ida.CCM.SetValue(TKMD.ChiThiHQCollection[0].PhanLoai);
                for (int i = 0; i < TKMD.ChiThiHQCollection.Count && i < 10; i++)
                {
                    ida.DTI.SetValueCollection(TKMD.ChiThiHQCollection[i].Ngay, EnumGroupID.IDA_D__, i);
                    ida.DTI.SetValueCollection(TKMD.ChiThiHQCollection[i].Ten, EnumGroupID.IDA_T__, i);
                    ida.DTI.SetValueCollection(TKMD.ChiThiHQCollection[i].NoiDung, EnumGroupID.IDA_I__, i);
                }
            }
            if (TKMD.TrungChuyenCollection.Count > 0)
            {
                #region Địa điểm trung chuyển
                for (int i = 0; i < TKMD.TrungChuyenCollection.Count && i < 3; i++)
                {
                    ida.ST_.SetValueCollection(TKMD.TrungChuyenCollection[i].MaDiaDiem, EnumGroupID.IDA_ST_, i);
                    ida.ST_.SetValueCollection(TKMD.TrungChuyenCollection[i].MaDiaDiem, EnumGroupID.IDA_ST_, i);
                    ida.ST_.SetValueCollection(TKMD.TrungChuyenCollection[i].MaDiaDiem, EnumGroupID.IDA_ST_, i);
                }
                #endregion

            }


            if (ida.ListHangHoa == null) ida.ListHangHoa = new List<IDA_HANGHOA>();
            foreach (var item in TKMD.HangCollection)
            {
                //minhnd Check to khai nhập-xuất
                if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                    item.tkxuat = false;
                else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                    item.tkxuat = true;
                //minhnd Check to khai nhập-xuất
                ida.ListHangHoa.Add(IDA_HANGHOAMapper(item));
            }
            return ida;
        }

        public static IDA_HANGHOA IDA_HANGHOAMapper(KDT_VNACC_HangMauDich HMD)
        {
            IDA_HANGHOA ida_hanghoa = new IDA_HANGHOA();
            ida_hanghoa.CMD.SetValue(HMD.MaSoHang);
            ida_hanghoa.GZC.SetValue(HMD.MaQuanLy);
            //int thuesuat = int.Parse(HMD.ThueSuat.ToString());
            //if (HMD.ThueSuat.ToString() != "0,000")
                //ida_hanghoa.TXA.SetValue(HMD.ThueSuat ,HMD.MaBieuThueNK.ToString().Trim() == "B30");
            if (HMD.MaBieuThueNK.ToString().Trim() == "B30")
                ida_hanghoa.TXA.SetValue(HMD.ThueSuat, HMD.MaBieuThueNK.ToString().Trim() == "B30");
                //0% for khai FOC
            else if (HMD.ThueSuat == 0)
            {
                ida_hanghoa.TXA.SetValue("");
            }
            else
                ida_hanghoa.TXA.SetValue(HMD.ThueSuat);
                
            //else
                //ida_hanghoa.TXA.SetValue("", HMD.MaBieuThueNK.ToString().Trim() == "B30");
            if (HMD.ThueSuatTuyetDoi!=0)
                ida_hanghoa.TXB.SetValue(HMD.ThueSuatTuyetDoi);
            else
                ida_hanghoa.TXB.SetValue("");
            ida_hanghoa.TXC.SetValue(HMD.MaDVTTuyetDoi);
            ida_hanghoa.TXD.SetValue(HMD.MaTTTuyetDoi);
            ida_hanghoa.CMN.SetValue(HMD.MaHangHoaKhaiBao);
            ida_hanghoa.OR.SetValue(HMD.NuocXuatXu);
            ida_hanghoa.ORS.SetValue(HMD.MaBieuThueNK);
            ida_hanghoa.KWS.SetValue(HMD.MaHanNgach);
            ida_hanghoa.SPD.SetValue(HMD.MaThueNKTheoLuong);
            ida_hanghoa.QN1.SetValue(HMD.SoLuong1);
            ida_hanghoa.QT1.SetValue(HMD.DVTLuong1);
            //if (HMD.SoLuong2==0)
            //    ida_hanghoa.QN2.SetValue("");
            //else
                ida_hanghoa.QN2.SetValue(HMD.SoLuong2);
            ida_hanghoa.QT2.SetValue(HMD.DVTLuong2);
            //minhnd
            if (HMD.TriGiaHoaDon == 0)
            {
                ida_hanghoa.BPR.SetValue("0");
            }
            else
            {
                ida_hanghoa.BPR.SetValue(HMD.TriGiaHoaDon);
            }
            if (HMD.DonGiaHoaDon == 0)
            {
                ida_hanghoa.UPR.SetValue("0");
            }
            else
            {
                ida_hanghoa.UPR.SetValue(HMD.DonGiaHoaDon);
            }

            //ida_hanghoa.BPR.SetValue(HMD.TriGiaHoaDon);
            //ida_hanghoa.UPR.SetValue(HMD.DonGiaHoaDon);
            //if (HMD.DonGiaHoaDon == 0)
            //    ida_hanghoa.UPC.SetValue(string.Empty);
            //else
                ida_hanghoa.UPC.SetValue(HMD.MaTTDonGia);
            //if(HMD.DonGiaHoaDon==0)
            //    ida_hanghoa.TSC.SetValue(string.Empty);
            //else
                ida_hanghoa.TSC.SetValue(HMD.DVTDonGia);
            ida_hanghoa.BPC.SetValue(HMD.MaTTTriGiaTinhThue);
            //if (HMD.TriGiaTinhThue!=0)
                ida_hanghoa.DPR.SetValue(HMD.TriGiaTinhThue);
            //else
            //    ida_hanghoa.DPR.SetValue("");

            #region Số mục khai khoản điều chỉnh
            if (!string.IsNullOrEmpty(HMD.SoMucKhaiKhoanDC))
            {
                char[] SoMuc = HMD.SoMucKhaiKhoanDC.Trim().ToCharArray();
                for (int i = 0; i < SoMuc.Length & i < 5; i++)
                {
                    ida_hanghoa.VN_.SetValueCollection(System.Convert.ToInt16(SoMuc[i].ToString()), EnumGroupID.IDA_HANGHOA_VN_, i);
                }
            }
            #endregion
            if (HMD.SoTTDongHangTKTNTX=="0")
                ida_hanghoa.TDL.SetValue("");
            else
                ida_hanghoa.TDL.SetValue(HMD.SoTTDongHangTKTNTX);
            if (HMD.SoDMMienThue == "0")
                ida_hanghoa.TXN.SetValue("");
            else
                ida_hanghoa.TXN.SetValue(HMD.SoDMMienThue);
            if (HMD.SoDongDMMienThue!="0")
                ida_hanghoa.TXR.SetValue(HMD.SoDongDMMienThue);
            else
                ida_hanghoa.TXR.SetValue("");
            ida_hanghoa.RE.SetValue(HMD.MaMienGiamThue);
            if (HMD.SoTienMienGiam==0)
                ida_hanghoa.REG.SetValue("");
            else
                ida_hanghoa.REG.SetValue(HMD.SoTienMienGiam);

            #region  thuế thu khác
            for (int i = 0; i < HMD.ThueThuKhacCollection.Count; i++)
            {
                ida_hanghoa.TX_.SetValueCollection(HMD.ThueThuKhacCollection[i].MaTSThueThuKhac, EnumGroupID.IDA_HANGHOA_TX_, i);
                ida_hanghoa.TX_.SetValueCollection(HMD.ThueThuKhacCollection[i].MaMGThueThuKhac, EnumGroupID.IDA_HANGHOA_TR_, i);
                if (HMD.ThueThuKhacCollection[i].SoTienGiamThueThuKhac==0)
                    ida_hanghoa.TX_.SetValueCollection("", EnumGroupID.IDA_HANGHOA_TG_, i);
                else
                    ida_hanghoa.TX_.SetValueCollection(HMD.ThueThuKhacCollection[i].SoTienGiamThueThuKhac, EnumGroupID.IDA_HANGHOA_TG_, i);

            }
            //ida_hanghoa.TX_.SetValueCollection(HMD.MaTSThueThuKhac1, EnumGroupID.IDA_HANGHOA_TX_, 1);
            //ida_hanghoa.TX_.SetValueCollection(HMD.MaTSThueThuKhac2, EnumGroupID.IDA_HANGHOA_TX_, 2);
            //ida_hanghoa.TX_.SetValueCollection(HMD.MaTSThueThuKhac3, EnumGroupID.IDA_HANGHOA_TX_, 3);
            //ida_hanghoa.TX_.SetValueCollection(HMD.MaTSThueThuKhac4, EnumGroupID.IDA_HANGHOA_TX_, 4);
            //ida_hanghoa.TX_.SetValueCollection(HMD.MaTSThueThuKhac5, EnumGroupID.IDA_HANGHOA_TX_, 5);

            //ida_hanghoa.TX_.SetValueCollection(HMD.MaMGThueThuKhac1, EnumGroupID.IDA_HANGHOA_TR_, 1);
            //ida_hanghoa.TX_.SetValueCollection(HMD.MaMGThueThuKhac2, EnumGroupID.IDA_HANGHOA_TR_, 2);
            //ida_hanghoa.TX_.SetValueCollection(HMD.MaMGThueThuKhac3, EnumGroupID.IDA_HANGHOA_TR_, 3);
            //ida_hanghoa.TX_.SetValueCollection(HMD.MaMGThueThuKhac4, EnumGroupID.IDA_HANGHOA_TR_, 4);
            //ida_hanghoa.TX_.SetValueCollection(HMD.MaMGThueThuKhac5, EnumGroupID.IDA_HANGHOA_TR_, 5);

            //ida_hanghoa.TX_.SetValueCollection(HMD.SoTienGiamThueThuKhac1, EnumGroupID.IDA_HANGHOA_TG_, 1);
            //ida_hanghoa.TX_.SetValueCollection(HMD.SoTienGiamThueThuKhac2, EnumGroupID.IDA_HANGHOA_TG_, 2);
            //ida_hanghoa.TX_.SetValueCollection(HMD.SoTienGiamThueThuKhac3, EnumGroupID.IDA_HANGHOA_TG_, 3);
            //ida_hanghoa.TX_.SetValueCollection(HMD.SoTienGiamThueThuKhac4, EnumGroupID.IDA_HANGHOA_TG_, 4);
            //ida_hanghoa.TX_.SetValueCollection(HMD.SoTienGiamThueThuKhac5, EnumGroupID.IDA_HANGHOA_TG_, 5);

            #endregion

            return ida_hanghoa;
        }

        public static IDB IDBMapper(decimal SoToKhai, string LoaiManifest, string SoVanDon, decimal SoTiepNhanHoaDon)
        {
            IDB idb = new IDB();

            idb.ICN.SetValue(SoToKhai);
            idb.TOM.SetValue(LoaiManifest);
            idb.BLN.SetValue(SoVanDon);
            idb.IVU.SetValue(SoTiepNhanHoaDon);

            return idb;
        }

        public static IDC IDCMapper(decimal SoToKhai, string PhanLoaiXuatBaoCaoSuaDoi)
        {
            IDC idc = new IDC();
            idc.ICN.SetValue(SoToKhai);
            idc.JKN.SetValue(PhanLoaiXuatBaoCaoSuaDoi);
            return idc;
        }

        public static IDD IDDMapper(decimal SoToKhai)
        {
            IDD idd = new IDD();
            idd.ICN.SetValue(SoToKhai);
            return idd;
        }

        public static IDE IDEMapper(decimal SoToKhai, string PhanLoaiXuatBaoCaoSuaDoi)
        {
            IDE ide = new IDE();
            ide.ICN.SetValue(SoToKhai);
            ide.JKN.SetValue(PhanLoaiXuatBaoCaoSuaDoi);
            return ide;
        }

        public static MIC MICMapper(KDT_VNACCS_TKTriGiaThap ToKhaiTGThap)
        {
            MIC mic = new MIC();
            mic.ICN.SetValue(ToKhaiTGThap.SoToKhai);
            mic.JYO.SetValue(ToKhaiTGThap.PhanLoaiBaoCaoSua);
            mic.SKB.SetValue(ToKhaiTGThap.PhanLoaiToChuc);
            mic.CH.SetValue(ToKhaiTGThap.CoQuanHaiQuan);
            mic.CHB.SetValue(ToKhaiTGThap.NhomXuLyHS);
            mic.IMC.SetValue(ToKhaiTGThap.MaDonVi);
            mic.IMN.SetValue(ToKhaiTGThap.TenDonVi);
            mic.IMY.SetValue(ToKhaiTGThap.MaBuuChinhDonVi);
            mic.IMA.SetValue(ToKhaiTGThap.DiaChiDonVi);
            mic.IMT.SetValue(ToKhaiTGThap.SoDienThoaiDonVi);
            mic.EPC.SetValue(ToKhaiTGThap.MaDoiTac);
            mic.EPN.SetValue(ToKhaiTGThap.TenDoiTac);
            mic.EPY.SetValue(ToKhaiTGThap.MaBuuChinhDoiTac);
            mic.EPA.SetValue(ToKhaiTGThap.DiaChiDoiTac1);
            mic.EP2.SetValue(ToKhaiTGThap.DiaChiDoiTac2);
            mic.EP3.SetValue(ToKhaiTGThap.DiaChiDoiTac3);
            mic.EP4.SetValue(ToKhaiTGThap.DiaChiDoiTac4);
            mic.EPO.SetValue(ToKhaiTGThap.MaNuocDoiTac);
            mic.HAB.SetValue(ToKhaiTGThap.SoHouseAWB);
            mic.MAB.SetValue(ToKhaiTGThap.SoMasterAWB);
            mic.NO.SetValue(ToKhaiTGThap.SoLuong);
            mic.GW.SetValue(ToKhaiTGThap.TrongLuong);
            mic.ST.SetValue(ToKhaiTGThap.MaDDLuuKho);
            mic.VSN.SetValue(ToKhaiTGThap.TenMayBayChoHang);
            mic.ARR.SetValue(ToKhaiTGThap.NgayHangDen);
            mic.DST.SetValue(ToKhaiTGThap.MaCangDoHang);
            mic.PSC.SetValue(ToKhaiTGThap.MaDiaDiemXepHang);
            mic.IP1.SetValue(ToKhaiTGThap.PhanLoaiGiaHD);
            mic.IP2.SetValue(ToKhaiTGThap.MaDieuKienGiaHD);
            mic.IP3.SetValue(ToKhaiTGThap.MaTTHoaDon);
            mic.IP4.SetValue(ToKhaiTGThap.TongTriGiaHD);
            mic.FR1.SetValue(ToKhaiTGThap.MaPhanLoaiPhiVC);
            mic.FR2.SetValue(ToKhaiTGThap.MaTTPhiVC);
            mic.FR3.SetValue(ToKhaiTGThap.PhiVanChuyen);
            mic.IN1.SetValue(ToKhaiTGThap.MaPhanLoaiPhiBH);
            mic.IN2.SetValue(ToKhaiTGThap.MaTTPhiBH);
            mic.IN3.SetValue(ToKhaiTGThap.PhiBaoHiem);
            mic.CMN.SetValue(ToKhaiTGThap.MoTaHangHoa);
            mic.OR.SetValue(ToKhaiTGThap.MaNuocXuatXu);
            mic.DPR.SetValue(ToKhaiTGThap.TriGiaTinhThue);
            mic.NT1.SetValue(ToKhaiTGThap.GhiChu);
            mic.REF.SetValue(ToKhaiTGThap.SoQuanLyNoiBoDN);

            return mic;

        }

        public static MID MIDMapper(decimal SoToKhai, string soBill)
        {
            MID mid = new MID();
            mid.ICN.SetValue(SoToKhai);
            mid.HAB.SetValue(soBill);
            return mid;
        }

        public static IID IIDMapper(decimal SoToKhai)
        {
            IID iid = new IID();
            iid.ICN.SetValue(SoToKhai);
            return iid;
        }
        public static IEX IEXMapper(decimal SoToKhai)
        {
            IEX iex = new IEX();
            iex.ECN.SetValue(SoToKhai);
            return iex;
        }
        public static EDA EDAMapper(KDT_VNACC_ToKhaiMauDich tkmd)
        {
            EDA eda = new EDA();
            eda.ECN.SetValue(tkmd.SoToKhai);
            eda.FIC.SetValue(tkmd.SoToKhaiDauTien);
            eda.BNO.SetValue(tkmd.SoNhanhToKhai);
            eda.DNO.SetValue(tkmd.TongSoTKChiaNho);
            eda.TDN.SetValue(tkmd.SoToKhaiTNTX);
            eda.ECB.SetValue(tkmd.MaLoaiHinh);
            eda.CCC.SetValue(tkmd.MaPhanLoaiHH);
            eda.MTC.SetValue(tkmd.MaPhuongThucVT);
            eda.RID.SetValue(tkmd.ThoiHanTaiNhapTaiXuat);
            eda.CH.SetValue(tkmd.CoQuanHaiQuan);
            eda.CHB.SetValue(tkmd.NhomXuLyHS);
            eda.ECD.SetValue(tkmd.NgayDangKy);
            eda.EPC.SetValue(tkmd.MaDonVi);
            eda.EPN.SetValue(tkmd.TenDonVi);
            eda.EPP.SetValue(tkmd.MaBuuChinhDonVi);
            eda.EPA.SetValue(tkmd.DiaChiDonVi);
            eda.EPT.SetValue(tkmd.SoDienThoaiDonVi);
            eda.EXC.SetValue(tkmd.MaUyThac);
            eda.EXN.SetValue(tkmd.TenUyThac);
            eda.CGC.SetValue(tkmd.MaDoiTac);
            eda.CGN.SetValue(tkmd.TenDoiTac);
            eda.CGP.SetValue(tkmd.MaBuuChinhDoiTac);
            eda.CGA.SetValue(tkmd.DiaChiDoiTac1);
            eda.CAT.SetValue(tkmd.DiaChiDoiTac2);
            eda.CAC.SetValue(tkmd.DiaChiDoiTac3);
            eda.CAS.SetValue(tkmd.DiaChiDoiTac4);
            eda.CGK.SetValue(tkmd.MaNuocDoiTac);
            eda.ECC.SetValue(tkmd.MaDaiLyHQ);
            if (tkmd.VanDonCollection != null && tkmd.VanDonCollection.Count > 0)
                eda.EKN.SetValue(tkmd.VanDonCollection[0].SoDinhDanh);
            eda.NO.SetValue(tkmd.SoLuong);
            eda.NOT.SetValue(tkmd.MaDVTSoLuong);
            eda.GW.SetValue(tkmd.TrongLuong);
            eda.GWT.SetValue(tkmd.MaDVTTrongLuong);
            eda.ST.SetValue(tkmd.MaDDLuuKho);
            eda.DSC.SetValue(tkmd.MaDiaDiemDoHang);
            eda.DSN.SetValue(tkmd.TenDiaDiemDohang);
            eda.PSC.SetValue(tkmd.MaDiaDiemXepHang);
            eda.PSN.SetValue(tkmd.TenDiaDiemXepHang);
            eda.VSC.SetValue(tkmd.MaPTVC);
            eda.VSN.SetValue(tkmd.TenPTVC);
            eda.SYM.SetValue(tkmd.NgayHangDen);
            eda.MRK.SetValue(tkmd.SoHieuKyHieu);
            if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
                #region giấy phép
                for (int i = 0; i < tkmd.GiayPhepCollection.Count && i < 5; i++)
                {
                    eda.SS_.SetValueCollection(tkmd.GiayPhepCollection[i].PhanLoai, EnumGroupID.EDA_SS_, i);
                    eda.SS_.SetValueCollection(tkmd.GiayPhepCollection[i].SoGiayPhep, EnumGroupID.EDA_SN_, i);
                }
                #endregion

            eda.IV1.SetValue(tkmd.PhanLoaiHD);
            eda.IV2.SetValue(tkmd.SoTiepNhanHD);
            eda.IV3.SetValue(tkmd.SoHoaDon);
            eda.IVD.SetValue(tkmd.NgayPhatHanhHD);
            eda.IVP.SetValue(tkmd.PhuongThucTT);
            eda.IP2.SetValue(tkmd.MaDieuKienGiaHD);
            eda.IP3.SetValue(tkmd.MaTTHoaDon);
            eda.IP4.SetValue(tkmd.TongTriGiaHD);
            eda.IP1.SetValue(tkmd.PhanLoaiGiaHD);
            eda.FCD.SetValue(tkmd.MaTTTriGiaTinhThue);
            eda.FKK.SetValue(tkmd.TriGiaTinhThue);
            eda.CNV.SetValue(tkmd.PhanLoaiKhongQDVND);
            eda.TP.SetValue(tkmd.TongHeSoPhanBoTG);
            eda.TPM.SetValue(tkmd.NguoiNopThue);
            eda.BRC.SetValue(tkmd.MaNHTraThueThay);
            eda.BYA.SetValue(tkmd.NamPhatHanhHM);
            eda.BCM.SetValue(tkmd.KyHieuCTHanMuc);
            eda.BCN.SetValue(tkmd.SoCTHanMuc);
            eda.ENC.SetValue(tkmd.MaXDThoiHanNopThue);
            eda.SBC.SetValue(tkmd.MaNHBaoLanh);
            eda.RYA.SetValue(tkmd.NamPhatHanhBL);
            eda.SCM.SetValue(tkmd.KyHieuCTBaoLanh);
            eda.SCN.SetValue(tkmd.SoCTBaoLanh);
            if (tkmd.DinhKemCollection != null && tkmd.DinhKemCollection.Count > 0)
            {
                #region Đính kèm khai báo điện tử
                for (int i = 0; i < tkmd.DinhKemCollection.Count && i < 3; i++)
                {
                    eda.EA_.SetValueCollection(tkmd.DinhKemCollection[i].PhanLoai, EnumGroupID.EDA_EA_, i);
                    eda.EA_.SetValueCollection(tkmd.DinhKemCollection[i].SoDinhKemKhaiBaoDT, EnumGroupID.EDA_EB_, i);
                }
                #endregion
            }

            eda.DPD.SetValue(tkmd.NgayKhoiHanhVC);
            if (tkmd.TrungChuyenCollection != null && tkmd.TrungChuyenCollection.Count > 0)
            {
                #region Địa điểm trung chuyển
                for (int i = 0; i < tkmd.TrungChuyenCollection.Count && i < 3; i++)
                {
                    eda.ST_.SetValueCollection(tkmd.TrungChuyenCollection[i].MaDiaDiem, EnumGroupID.EDA_ST_, i);
                    eda.ST_.SetValueCollection(tkmd.TrungChuyenCollection[i].MaDiaDiem, EnumGroupID.EDA_ST_, i);
                    eda.ST_.SetValueCollection(tkmd.TrungChuyenCollection[i].MaDiaDiem, EnumGroupID.EDA_ST_, i);
                }
                #endregion

            }


            eda.ARP.SetValue(tkmd.DiaDiemDichVC);
            eda.ADT.SetValue(tkmd.NgayDen);
            eda.NT2.SetValue(tkmd.GhiChu);
            eda.REF.SetValue(tkmd.SoQuanLyNoiBoDN);
            if (tkmd.ContainerTKMD != null)
            {
                eda.VC_.SetValueCollection(tkmd.ContainerTKMD.MaDiaDiem1, EnumGroupID.EDA_VC_, 0);
                eda.VC_.SetValueCollection(tkmd.ContainerTKMD.MaDiaDiem2, EnumGroupID.EDA_VC_, 1);
                eda.VC_.SetValueCollection(tkmd.ContainerTKMD.MaDiaDiem3, EnumGroupID.EDA_VC_, 2);
                eda.VC_.SetValueCollection(tkmd.ContainerTKMD.MaDiaDiem4, EnumGroupID.EDA_VC_, 3);
                eda.VC_.SetValueCollection(tkmd.ContainerTKMD.MaDiaDiem5, EnumGroupID.EDA_VC_, 4);
                eda.VN.SetValue(tkmd.ContainerTKMD.TenDiaDiem);
                eda.VA.SetValue(tkmd.ContainerTKMD.DiaChiDiaDiem);
                if (tkmd.ContainerTKMD.SoContainerCollection != null && tkmd.ContainerTKMD.SoContainerCollection.Count > 0)
                {
                    int index = 0;
                    foreach (KDT_VNACC_TK_SoContainer item in tkmd.ContainerTKMD.SoContainerCollection)
                    {
                        eda.C__.SetValueCollection(item.SoContainer, EnumGroupID.EDA_C__, index);
                        index++;
                    }

                }
            }
            if (tkmd.ChiThiHQCollection != null && tkmd.ChiThiHQCollection.Count > 0)
            {
                //eda.CCM.SetValue(tkmd.ChiThiHQCollection[0].PhanLoai);
                for (int i = 0; i < tkmd.ChiThiHQCollection.Count; i++)
                {
                    eda.D__.SetValueCollection(tkmd.ChiThiHQCollection[i].Ngay, EnumGroupID.EDA_D__, i);
                    eda.D__.SetValueCollection(tkmd.ChiThiHQCollection[i].Ten, EnumGroupID.EDA_T__, i);
                    eda.D__.SetValueCollection(tkmd.ChiThiHQCollection[i].NoiDung, EnumGroupID.EDA_I__, i);
                }
            }
            //for (int phapquy = 1; phapquy <= 5; phapquy++)
            //{
            //    switch (phapquy)
            //    {
            //        case 1:
            //            eda.OL.SetValueCollection(tkmd.MaVanbanPhapQuy1, EnumGroupID.IDA_OL_, phapquy);
            //            break;
            //        case 2:
            //            eda.OL_.SetValueCollection(tkmd.MaVanbanPhapQuy2, EnumGroupID.IDA_OL_, phapquy);
            //            break;
            //        case 3:
            //            eda.OL_.SetValueCollection(tkmd.MaVanbanPhapQuy3, EnumGroupID.IDA_OL_, phapquy);
            //            break;
            //        case 4:
            //            eda.OL_.SetValueCollection(tkmd.MaVanbanPhapQuy4, EnumGroupID.IDA_OL_, phapquy);
            //            break;
            //        case 5:
            //            eda.OL_.SetValueCollection(tkmd.MaVanbanPhapQuy5, EnumGroupID.IDA_OL_, phapquy);
            //            break;
            //        default:
            //            break;
            //    }

            //}
            if (eda.ListHangHoa == null) eda.ListHangHoa = new List<EDA_HANGHOA>();
            foreach (var item in tkmd.HangCollection)
            {
                //minhnd Check to khai nhập-xuất
                if (tkmd.SoToKhai.ToString().Substring(0, 1) == "1")
                    item.tkxuat = false;
                else if (tkmd.SoToKhai.ToString().Substring(0, 1) == "3")
                    item.tkxuat = true;
                //minhnd Check to khai nhập-xuất
                eda.ListHangHoa.Add(EDA_HANGHOAMapper(item));
            }


            return eda;

        }

        public static EDA_HANGHOA EDA_HANGHOAMapper(KDT_VNACC_HangMauDich HMD)
        {
            EDA_HANGHOA eda_hanghoa = new EDA_HANGHOA();
            eda_hanghoa.CMD.SetValue(HMD.MaSoHang);
            eda_hanghoa.COC.SetValue(HMD.MaQuanLy);
            if(HMD.ThueSuat!=0)
                eda_hanghoa.TXA.SetValue(HMD.ThueSuat, false);
            else
                eda_hanghoa.TXA.SetValue("");
            eda_hanghoa.TXB.SetValue(HMD.ThueSuatTuyetDoi);
            eda_hanghoa.TXC.SetValue(HMD.MaDVTTuyetDoi);
            eda_hanghoa.TXD.SetValue(HMD.MaTTTuyetDoi);
            eda_hanghoa.CMN.SetValue(HMD.MaHangHoaKhaiBao);
            eda_hanghoa.RE.SetValue(HMD.MaMienGiamThue);
            eda_hanghoa.REG.SetValue(HMD.SoTienMienGiam);
            eda_hanghoa.QN1.SetValue(HMD.SoLuong1);
            eda_hanghoa.QT1.SetValue(HMD.DVTLuong1);
            eda_hanghoa.QN2.SetValue(HMD.SoLuong2);
            eda_hanghoa.QT2.SetValue(HMD.DVTLuong2);
            eda_hanghoa.BPR.SetValue(HMD.TriGiaHoaDon);
            eda_hanghoa.BPC.SetValue(HMD.MaTTTriGiaTinhThue);
            eda_hanghoa.DPR.SetValue(HMD.TriGiaTinhThue);
            eda_hanghoa.UPR.SetValue(HMD.DonGiaHoaDon);
            eda_hanghoa.UPC.SetValue(HMD.MaTTDonGia);
            eda_hanghoa.TSC.SetValue(HMD.DVTDonGia);
            eda_hanghoa.TDL.SetValue(HMD.SoTTDongHangTKTNTX,true);
            eda_hanghoa.TXN.SetValue(HMD.SoDMMienThue);
            if (HMD.SoDongDMMienThue!="0")
                eda_hanghoa.TXR.SetValue(HMD.SoDongDMMienThue);
            // Thiếu mã văn bản pháp quy khác
            eda_hanghoa.B59.SetValue(HMD.DieuKhoanMienGiam);




            return eda_hanghoa;

        }

        public static EDB EDBMapper(decimal SoToKhai, decimal SoTiepNhanHoaDon)
        {
            EDB edb = new EDB();

            edb.ECN.SetValue(SoToKhai);
            edb.IVU.SetValue(SoTiepNhanHoaDon);


            return edb;
        }

        public static EDC EDCMapper(decimal SoToKhai, string PhanLoaiXuatBaoCaoSuaDoi)
        {
            EDC edc = new EDC();
            edc.ECN.SetValue(SoToKhai);
            edc.JKN.SetValue(PhanLoaiXuatBaoCaoSuaDoi);
            return edc;
        }

        public static EDD EDDMapper(decimal SoToKhai)
        {
            EDD edd = new EDD();
            edd.ECN.SetValue(SoToKhai);
            return edd;
        }

        public static EDE EDEMapper(decimal SoToKhai, string PhanLoaiXuatBaoCaoSuaDoi)
        {
            EDE ede = new EDE();
            ede.ECN.SetValue(SoToKhai);
            ede.JKN.SetValue(PhanLoaiXuatBaoCaoSuaDoi);
            return ede;
        }

        public static MEC MECMapper(KDT_VNACCS_TKTriGiaThap ToKhaiTGThap)
        {
            MEC mec = new MEC();

            mec.ECN.SetValue(ToKhaiTGThap.SoToKhai);
            mec.JKN.SetValue(ToKhaiTGThap.PhanLoaiBaoCaoSua);
            mec.CH.SetValue(ToKhaiTGThap.CoQuanHaiQuan);
            mec.CHB.SetValue(ToKhaiTGThap.NhomXuLyHS);
            mec.EPC.SetValue(ToKhaiTGThap.MaDonVi);
            mec.EPN.SetValue(ToKhaiTGThap.TenDonVi);
            mec.EPP.SetValue(ToKhaiTGThap.MaBuuChinhDonVi);
            mec.EPA.SetValue(ToKhaiTGThap.DiaChiDonVi);
            mec.EPT.SetValue(ToKhaiTGThap.SoDienThoaiDonVi);
            mec.CGC.SetValue(ToKhaiTGThap.MaDoiTac);
            mec.CGN.SetValue(ToKhaiTGThap.TenDoiTac);
            mec.CGP.SetValue(ToKhaiTGThap.MaBuuChinhDoiTac);
            mec.CGA.SetValue(ToKhaiTGThap.DiaChiDoiTac1);
            mec.CAT.SetValue(ToKhaiTGThap.DiaChiDoiTac2);
            mec.CAC.SetValue(ToKhaiTGThap.DiaChiDoiTac3);
            mec.CAS.SetValue(ToKhaiTGThap.DiaChiDoiTac4);
            mec.CGK.SetValue(ToKhaiTGThap.MaNuocDoiTac);
            mec.AWB.SetValue(ToKhaiTGThap.SoHouseAWB);
            mec.NO.SetValue(ToKhaiTGThap.SoLuong);
            mec.GW.SetValue(ToKhaiTGThap.TrongLuong);
            mec.ST.SetValue(ToKhaiTGThap.MaDDLuuKho);
            mec.DSC.SetValue(ToKhaiTGThap.MaDDNhanHangCuoiCung);
            mec.PSC.SetValue(ToKhaiTGThap.MaDiaDiemXepHang);
            mec.FCD.SetValue(ToKhaiTGThap.MaTTTongTriGiaTinhThue);
            mec.FKK.SetValue(ToKhaiTGThap.TongTriGiaTinhThue);
            mec.SKK.SetValue(ToKhaiTGThap.GiaKhaiBao);
            mec.CMN.SetValue(ToKhaiTGThap.MoTaHangHoa);
            mec.NT.SetValue(ToKhaiTGThap.GhiChu);
            mec.REF.SetValue(ToKhaiTGThap.SoQuanLyNoiBoDN);

            return mec;

        }

        public static MED MEDMapper(decimal SoToKhai)
        {
            MED med = new MED();
            med.ECN.SetValue(SoToKhai);
            return med;
        }

        /*GIAY PHEP*/

        public static SEA SEAMapper(KDT_VNACC_GiayPhep_SEA GiayPhepSEA)
        {
            SEA sea = new SEA();
            sea.SMC.SetValue(GiayPhepSEA.MaNguoiKhai);
            sea.APN.SetValue(GiayPhepSEA.SoDonXinCapPhep);
            sea.FNC.SetValue(GiayPhepSEA.ChucNangChungTu);
            sea.APT.SetValue(GiayPhepSEA.LoaiGiayPhep);
            sea.EIC.SetValue(GiayPhepSEA.LoaiHinhXNK);
            sea.APP.SetValue(GiayPhepSEA.MaDV_CapPhep);
            sea.ENC.SetValue(GiayPhepSEA.MaDoanhNghiep);
            sea.ENN.SetValue(GiayPhepSEA.TenDoanhNghiep);
            sea.EDN.SetValue(GiayPhepSEA.QuyetDinhSo);
            sea.RCN.SetValue(GiayPhepSEA.SoGiayChungNhan);
            sea.IP.SetValue(GiayPhepSEA.NoiCap);
            sea.ID.SetValue(GiayPhepSEA.NgayCap);
            sea.BAP.SetValue(GiayPhepSEA.MaBuuChinh);
            sea.IMA.SetValue(GiayPhepSEA.DiaChi);
            sea.BCC.SetValue(GiayPhepSEA.MaQuocGia);
            sea.BAN.SetValue(GiayPhepSEA.SoDienThoai);
            sea.BAF.SetValue(GiayPhepSEA.SoFax);
            sea.BAM.SetValue(GiayPhepSEA.Email);
            sea.PCN.SetValue(GiayPhepSEA.SoHopDongMua);
            sea.PCD.SetValue(GiayPhepSEA.NgayHopDongMua);
            sea.SCN.SetValue(GiayPhepSEA.SoHopDongBan);
            sea.SCD.SetValue(GiayPhepSEA.NgayHopDongBan);
            sea.MTT.SetValue(GiayPhepSEA.PhuongTienVanChuyen);
            sea.IPC.SetValue(GiayPhepSEA.MaCuaKhauXuatNhap);
            sea.IPN.SetValue(GiayPhepSEA.TenCuaKhauXuatNhap);
            sea.ESI.SetValue(GiayPhepSEA.NgayBatDau);
            sea.EEI.SetValue(GiayPhepSEA.NgayKetThuc);
            sea.AD.SetValue(GiayPhepSEA.HoSoLienQuan);
            sea.RMK.SetValue(GiayPhepSEA.GhiChu);
            sea.PIC.SetValue(GiayPhepSEA.TenGiamDoc);
            if (sea.ListHangHoa == null) sea.ListHangHoa = new List<SEA_HANGHOA>();
            foreach (var HangSEA in GiayPhepSEA.HangCollection)
            {
                SEA_HANGHOA sea_hanghoa = new SEA_HANGHOA();
                sea_hanghoa.GDN.SetValue(HangSEA.TenHangHoa);
                sea_hanghoa.HSC.SetValue(HangSEA.MaSoHangHoa);
                sea_hanghoa.GW.SetValue(HangSEA.SoLuong);
                sea_hanghoa.GWU.SetValue(HangSEA.DonVitinhSoLuong);
                sea.ListHangHoa.Add(sea_hanghoa);
            }


            return sea;
        }
        public static SEB SEBMapper(string SoGiayPhep)
        {
            SEB seb = new SEB();
            seb.APN.SetValue(SoGiayPhep);
            return seb;
        }
        public static ISE ISEMapper(string SoGiayPhep, string PhanLoai)
        {
            ISE ise = new ISE();
            ise.APN.SetValue(SoGiayPhep);
            ise.KBN.SetValue(PhanLoai);
            return ise;
        }

        public static SFA SFAMapper(KDT_VNACC_GiayPhep_SFA GiayPhepSFA)
        {
            SFA sfa = new SFA();
            sfa.SMC.SetValue(GiayPhepSFA.MaNguoiKhai);
            sfa.APP.SetValue(GiayPhepSFA.MaDonViCapPhep);
            sfa.APN.SetValue(GiayPhepSFA.SoDonXinCapPhep);
            sfa.FNC.SetValue(GiayPhepSFA.ChucNangChungTu);
            sfa.APT.SetValue(GiayPhepSFA.LoaiGiayPhep);
            sfa.EXN.SetValue(GiayPhepSFA.TenThuongNhanXuatKhau);
            sfa.EPC.SetValue(GiayPhepSFA.MaBuuChinhXuatKhau);
            sfa.EXA.SetValue(GiayPhepSFA.SoNhaTenDuongXuatKhau);
            sfa.EXS.SetValue(GiayPhepSFA.PhuongXaXuatKhau);
            sfa.EXD.SetValue(GiayPhepSFA.QuanHuyenXuatKhau);
            sfa.EXC.SetValue(GiayPhepSFA.TinhThanhPhoXuatKhau);
            sfa.ECC.SetValue(GiayPhepSFA.MaQuocGiaXuatKhau);
            sfa.EPN.SetValue(GiayPhepSFA.SoDienThoaiXuatKhau);
            sfa.EXF.SetValue(GiayPhepSFA.SoFaxXuatKhau);
            sfa.EXE.SetValue(GiayPhepSFA.EmailXuatKhau);
            sfa.CNN.SetValue(GiayPhepSFA.SoHopDong);
            sfa.BLN.SetValue(GiayPhepSFA.SoVanDon);
            sfa.DPC.SetValue(GiayPhepSFA.MaBenDi);
            sfa.DPN.SetValue(GiayPhepSFA.TenBenDi);
            sfa.IMC.SetValue(GiayPhepSFA.MaThuongNhanNhapKhau);
            sfa.IMN.SetValue(GiayPhepSFA.TenThuongNhanNhapKhau);
            sfa.IPC.SetValue(GiayPhepSFA.MaBuuChinhNhapKhau);
            sfa.IMA.SetValue(GiayPhepSFA.SoNhaTenDuongNhapKhau);
            sfa.ICC.SetValue(GiayPhepSFA.MaQuocGiaNhapKhau);
            sfa.IPN.SetValue(GiayPhepSFA.SoDienThoaiNhapKhau);
            sfa.IMF.SetValue(GiayPhepSFA.SoFaxNhapKhau);
            sfa.IME.SetValue(GiayPhepSFA.EmailNhapKhau);
            sfa.APC.SetValue(GiayPhepSFA.MaBenDen);
            sfa.ARN.SetValue(GiayPhepSFA.TenBenDen);
            sfa.EDI.SetValue(GiayPhepSFA.ThoiGianNhapKhauDuKien);
            sfa.TLV.SetValue(GiayPhepSFA.GiaTriHangHoa);
            sfa.CU.SetValue(GiayPhepSFA.DonViTienTe);
            sfa.PC.SetValue(GiayPhepSFA.DiaDiemTapKetHangHoa);
            sfa.DI.SetValue(GiayPhepSFA.ThoiGianKiemTra);
            sfa.PI.SetValue(GiayPhepSFA.DiaDiemKiemTra);
            sfa.AD.SetValue(GiayPhepSFA.HoSoLienQuan);
            sfa.RMK.SetValue(GiayPhepSFA.GhiChu);
            sfa.PIC.SetValue(GiayPhepSFA.DaiDienThuongNhanNhapKhau);

            if (sfa.ListHangHoa == null) sfa.ListHangHoa = new List<SFA_HANGHOA>();
            foreach (var HangHoa in GiayPhepSFA.HangCollection)
            {
                SFA_HANGHOA sfa_hanghoa = new SFA_HANGHOA();
                sfa_hanghoa.GDS.SetValue(HangHoa.TenHangHoa);
                sfa_hanghoa.HSC.SetValue(HangHoa.MaSoHangHoa);
                sfa_hanghoa.CO.SetValue(HangHoa.XuatXu);
                sfa_hanghoa.QT.SetValue(HangHoa.SoLuong);
                sfa_hanghoa.QTU.SetValue(HangHoa.DonVitinhSoLuong);
                sfa_hanghoa.WG.SetValue(HangHoa.KhoiLuong);
                sfa_hanghoa.WGU.SetValue(HangHoa.DonVitinhKhoiLuong);
                sfa.ListHangHoa.Add(sfa_hanghoa);
            }

            return sfa;
        }
        public static SFB SFBMapper(string SoGiayPhep)
        {
            SFB sfb = new SFB();
            sfb.APN.SetValue(SoGiayPhep);
            return sfb;
        }
        public static ISF ISFMapper(string SoGiayPhep, string PhanLoai)
        {
            ISF isf = new ISF();
            isf.APN.SetValue(SoGiayPhep);
            isf.KBN.SetValue(PhanLoai);
            return isf;
        }

        public static SAA SAAMapper(KDT_VNACC_GiayPhep_SAA GiayPhepSAA)
        {
            SAA saa = new SAA();
            saa.SMC.SetValue(GiayPhepSAA.MaNguoiKhai);
            saa.SPC.SetValue(GiayPhepSAA.MaBuuChinhNguoiKhai);
            saa.ADS.SetValue(GiayPhepSAA.DiaChiNguoiKhai);
            saa.SCC.SetValue(GiayPhepSAA.MaNuocNguoiKhai);
            saa.SPN.SetValue(GiayPhepSAA.SoDienThoaiNguoiKhai);
            saa.SFN.SetValue(GiayPhepSAA.SoFaxNguoiKhai);
            saa.SBE.SetValue(GiayPhepSAA.EmailNguoiKhai);
            saa.APN.SetValue(GiayPhepSAA.SoDonXinCapPhep);
            saa.FNC.SetValue(GiayPhepSAA.ChucNangChungTu);
            saa.APT.SetValue(GiayPhepSAA.LoaiGiayPhep);
            saa.APP.SetValue(GiayPhepSAA.MaDonViCapPhep);
            saa.CON.SetValue(GiayPhepSAA.SoHopDong);
            saa.EXN.SetValue(GiayPhepSAA.TenThuongNhanXuatKhau);
            saa.EPC.SetValue(GiayPhepSAA.MaBuuChinhXuatKhau);
            saa.EXA.SetValue(GiayPhepSAA.SoNhaTenDuongXuatKhau);
            saa.EXS.SetValue(GiayPhepSAA.PhuongXaXuatKhau);
            saa.EXD.SetValue(GiayPhepSAA.QuanHuyenXuatKhau);
            saa.EXC.SetValue(GiayPhepSAA.TinhThanhPhoXuatKhau);
            saa.ECC.SetValue(GiayPhepSAA.MaQuocGiaXuatKhau);
            saa.EPN.SetValue(GiayPhepSAA.SoDienThoaiXuatKhau);
            saa.EXF.SetValue(GiayPhepSAA.SoFaxXuatKhau);
            saa.EXE.SetValue(GiayPhepSAA.EmailXuatKhau);
            saa.EOC.SetValue(GiayPhepSAA.NuocXuatKhau);
            saa.IMC.SetValue(GiayPhepSAA.MaThuongNhanNhapKhau);
            saa.IMN.SetValue(GiayPhepSAA.TenThuongNhanNhapKhau);
            saa.RCN.SetValue(GiayPhepSAA.SoDangKyKinhDoanh);
            saa.BPC.SetValue(GiayPhepSAA.MaBuuChinhNhapKhau);
            saa.IMA.SetValue(GiayPhepSAA.DiaChiThuongNhanNhapKhau);
            saa.BCC.SetValue(GiayPhepSAA.MaQuocGiaNhapKhau);
            saa.BPN.SetValue(GiayPhepSAA.SoDienThoaiNhapKhau);
            saa.PFX.SetValue(GiayPhepSAA.SoFaxNhapKhau);
            saa.BEM.SetValue(GiayPhepSAA.EmailNhapKhau);
            saa.TCC.SetValue(GiayPhepSAA.NuocQuaCanh);
            saa.MTT.SetValue(GiayPhepSAA.PhuongTienVanChuyen);
            saa.IBC.SetValue(GiayPhepSAA.MaCuaKhauNhap);
            saa.IBN.SetValue(GiayPhepSAA.TenCuaKhauNhap);
            saa.PMN.SetValue(GiayPhepSAA.SoGiayChungNhan);
            saa.QLN.SetValue(GiayPhepSAA.DiaDiemKiemDich);
            saa.VCD.SetValue(GiayPhepSAA.BenhMienDich);
            saa.DVC.SetValue(GiayPhepSAA.NgayTiemPhong);
            saa.ATT.SetValue(GiayPhepSAA.KhuTrung);
            saa.CCT.SetValue(GiayPhepSAA.NongDo);
            saa.PDC.SetValue(GiayPhepSAA.DiaDiemNuoiTrong);
            saa.QRT.SetValue(GiayPhepSAA.NgayKiemDich);
            saa.QRH.SetValue(GiayPhepSAA.ThoiGianKiemDich.ToString("hhmm"));
            saa.CTL.SetValue(GiayPhepSAA.DiaDiemGiamSat);
            saa.CLT.SetValue(GiayPhepSAA.NgayGiamSat);
            saa.CLH.SetValue(GiayPhepSAA.ThoiGianGiamSat.ToString("hhmm"));
            saa.NMC.SetValue(GiayPhepSAA.SoBanCanCap);
            saa.OEQ.SetValue(GiayPhepSAA.VatDungKhac);
            saa.AD.SetValue(GiayPhepSAA.HoSoLienQuan);
            saa.ALC.SetValue(GiayPhepSAA.NoiChuyenDen);
            saa.TES.SetValue(GiayPhepSAA.NguoiKiemTra);
            saa.TER.SetValue(GiayPhepSAA.KetQuaKiemTra);
            saa.SHN.SetValue(GiayPhepSAA.TenTau);
            saa.NAT.SetValue(GiayPhepSAA.QuocTich);
            saa.CTN.SetValue(GiayPhepSAA.TenThuyenTruong);
            saa.DTN.SetValue(GiayPhepSAA.TenBacSi);
            saa.CRN.SetValue(GiayPhepSAA.SoThuyenVien);
            saa.PGN.SetValue(GiayPhepSAA.SoHanhKhach);
            saa.LDP.SetValue(GiayPhepSAA.CangRoiCuoiCung);
            saa.NAP.SetValue(GiayPhepSAA.CangDenTiepTheo);
            saa.DCP.SetValue(GiayPhepSAA.CangBocHangDauTien);
            saa.DOD.SetValue(GiayPhepSAA.NgayRoiCang);
            saa.GDC.SetValue(GiayPhepSAA.TenHangCangDauTien);
            saa.FQ.SetValue(GiayPhepSAA.SoLuongHangCangDauTien);
            saa.FQU.SetValue(GiayPhepSAA.DonViTinhSoLuongHangCangDauTien);
            saa.FW.SetValue(GiayPhepSAA.KhoiLuongHangCangDauTien);
            saa.FWU.SetValue(GiayPhepSAA.DonViTinhKhoiLuongHangCangDauTien);
            if (GiayPhepSAA.CangTrungGianCollection != null && GiayPhepSAA.CangTrungGianCollection.Count > 0)
            {
                for (int i = 0; i < GiayPhepSAA.CangTrungGianCollection.Count; i++)
                {
                    saa.D__.SetValueCollection(GiayPhepSAA.CangTrungGianCollection[i].TenHangCangTrungGian, EnumGroupID.SAA_D__, i);
                    saa.D__.SetValueCollection(GiayPhepSAA.CangTrungGianCollection[i].SoLuongHangCangTrungGian, EnumGroupID.SAA_Q__, i);
                    saa.D__.SetValueCollection(GiayPhepSAA.CangTrungGianCollection[i].DonViTinhSoLuongHangCangTrungGian, EnumGroupID.SAA_U__, i);
                    saa.D__.SetValueCollection(GiayPhepSAA.CangTrungGianCollection[i].KhoiLuongHangCangTrungGian, EnumGroupID.SAA_W__, i);
                    saa.D__.SetValueCollection(GiayPhepSAA.CangTrungGianCollection[i].DonViTinhKhoiLuongHangCangTrungGian, EnumGroupID.SAA_X__, i);
                    saa.D__.SetValueCollection(GiayPhepSAA.CangTrungGianCollection[i].TenCangTrungGian, EnumGroupID.SAA_P__, i);
                }
            }
            saa.DGD.SetValue(GiayPhepSAA.TenHangCanBoc);
            saa.DQ.SetValue(GiayPhepSAA.SoLuongHangCanBoc);
            saa.DQU.SetValue(GiayPhepSAA.DonViTinhSoLuongHangCanBoc);
            saa.DW.SetValue(GiayPhepSAA.KhoiLuongHangCanBoc);
            saa.DWU.SetValue(GiayPhepSAA.DonViTinhKhoiLuongHangCanBoc);

            if (saa.ListHangHoa == null) saa.ListHangHoa = new List<SAA_HANGHOA>();
            foreach (var HangHoa in GiayPhepSAA.HangCollection)
            {
                SAA_HANGHOA saa_hanghoa = new SAA_HANGHOA();
                saa_hanghoa.GDA.SetValue(HangHoa.TenHangHoa);
                saa_hanghoa.HSC.SetValue(HangHoa.MaSoHangHoa);
                saa_hanghoa.SEX.SetValue(HangHoa.TinhBiet);
                saa_hanghoa.AGE.SetValue(HangHoa.Tuoi);
                saa_hanghoa.PRC.SetValue(HangHoa.NoiSanXuat);
                saa_hanghoa.PCM.SetValue(HangHoa.QuyCachDongGoi);
                saa_hanghoa.TQ.SetValue(HangHoa.TongSoLuongNhapKhau);
                saa_hanghoa.TQU.SetValue(HangHoa.DonViTinhTongSoLuongNhapKhau);
                saa_hanghoa.SOG.SetValue(HangHoa.KichCoCaThe);
                saa_hanghoa.NW.SetValue(HangHoa.TrongLuongTinh);
                saa_hanghoa.NWU.SetValue(HangHoa.DonViTinhTrongLuong);
                saa_hanghoa.GW.SetValue(HangHoa.TrongLuongCaBi);
                saa_hanghoa.GWU.SetValue(HangHoa.DonViTinhTrongLuongCaBi);
                saa_hanghoa.QQ.SetValue(HangHoa.SoLuongKiemDich);
                saa_hanghoa.QQU.SetValue(HangHoa.DonViTinhSoLuongKiemDich);
                saa_hanghoa.KPG.SetValue(HangHoa.LoaiBaoBi);
                saa_hanghoa.UP.SetValue(HangHoa.MucDichSuDung);

                saa.ListHangHoa.Add(saa_hanghoa);
            }


            return saa;
        }
        public static SAB SABMapper(string SoGiayPhep, string MaPhanLoai)
        {
            SAB sab = new SAB();
            sab.APN.SetValue(SoGiayPhep);
            sab.KBN.SetValue(MaPhanLoai);
            return sab;
        }
        public static ISA ISAMapper(string MaDonViCapPhep, string SoGiayPhep, string ThongTinTinhTrang, string PhanLoai)
        {
            ISA isa = new ISA();
            isa.APN.SetValue(SoGiayPhep);
            isa.APP.SetValue(MaDonViCapPhep);
            isa.STS.SetValue(ThongTinTinhTrang);
            isa.KBN.SetValue(PhanLoai);
            return isa;
        }

        //TODO: Thieu SMA, SMB, ISM
        public static SMA SMAMapper(KDT_VNACC_GiayPhep_SMA GiayPhep)
        {
            SMA obj = new SMA();
            obj.SMC.SetValue(GiayPhep.MaNguoiKhai); //Mã người khai
            obj.APN.SetValue(GiayPhep.SoDonXinCapPhep); //Số đơn xin cấp phép
            obj.FNC.SetValue(GiayPhep.ChucNangChungTu); //Chức năng của chứng từ
            obj.APT.SetValue(GiayPhep.LoaiGiayPhep); //Loại giấy phép
            obj.APP.SetValue(GiayPhep.MaDonViCapPhep); //Mã đơn vị cấp phép
            obj.IMC.SetValue(GiayPhep.MaThuongNhanNhapKhau); //Mã doanh nghiệp nhập khẩu
            obj.IMN.SetValue(GiayPhep.TenThuongNhanNhapKhau); //Tên doanh nghiệp nhập khẩu
            obj.IPC.SetValue(GiayPhep.MaBuuChinhNhapKhau); //Địa chỉ của doanh nghiệp nhập khẩu (Mã bưu chính)
            obj.IMA.SetValue(GiayPhep.DiaChiThuongNhanNhapKhau); //Địa chỉ của doanh nghiệp nhập khẩu (Số nhà, tên đường)
            obj.ICC.SetValue(GiayPhep.MaQuocGiaNhapKhau); //Địa chỉ của doanh nghiệp nhập khẩu (Mã quốc gia)  xuất khẩu
            obj.IPN.SetValue(GiayPhep.SoDienThoaiNhapKhau); //Địa chỉ của doanh nghiệp nhập khẩu (Số điện thoại)
            obj.IMF.SetValue(GiayPhep.SoFaxNhapKhau); //Địa chỉ của doanh nghiệp nhập khẩu (Số fax)
            obj.IME.SetValue(GiayPhep.EmailNhapKhau); //Địa chỉ của doanh nghiệp nhập khẩu (Email)
            obj.FSC.SetValue(GiayPhep.SoCuaGiayPhepLuuHanh); //Số của Giấy phép lưu hành nước sở tại
            obj.GMP.SetValue(GiayPhep.SoGiayPhepThucHanh); //Số của Giấy phép thực hành tốt sản xuất thuốc 
            obj.EPC.SetValue(GiayPhep.MaCuaKhauNhapDuKien); //Mã cửa khẩu nhập dự kiến 
            obj.EPN.SetValue(GiayPhep.TenCuaKhauNhapDuKien); //Tên cửa khẩu nhập dự kiến 
            obj.AD.SetValue(GiayPhep.HoSoLienQuan); //Hồ sơ liên quan
            obj.RMK.SetValue(GiayPhep.GhiChu); //Ghi chú
            obj.SUD.SetValue(GiayPhep.TonKhoDenNgay); //Tồn kho đến ngày
            obj.PIC.SetValue(GiayPhep.TenGiamDocDoanhNghiep); //Giám đốc doanh nghiệp

            if (obj.ListHangHoa == null) obj.ListHangHoa = new List<SMA_HANGHOA>();
            foreach (var HangHoa in GiayPhep.HangCollection)
            {
                SMA_HANGHOA objHang = new SMA_HANGHOA();
                objHang.MDC.SetValue(HangHoa.TenThuocQuyCachDongGoi); //Tên thuốc, hàm lượng, dạng bào chế, quy cách đóng gói
                objHang.HSC.SetValue(HangHoa.MaSoHangHoa); //Mã số hàng hóa
                objHang.AET.SetValue(HangHoa.HoatChat); //Hoạt chất
                objHang.QST.SetValue(HangHoa.TieuChuanChatLuong); //Tiêu chuẩn chất lượng
                objHang.RGN.SetValue(HangHoa.SoDangKy); //Số đăng ký
                objHang.EOM.SetValue(HangHoa.HanDung); //Hạn dùng
                objHang.QT.SetValue(HangHoa.SoLuong); //Số lượng 
                objHang.QTU.SetValue(HangHoa.DonVitinhSoLuong); //Đơn vị tính số lượng
                objHang.QEA.SetValue(HangHoa.TenHoatChatGayNghien); //Tên hoạt chất gây nghiện 
                objHang.EFE.SetValue(HangHoa.CongDung); //Công dụng
                objHang.TW.SetValue(HangHoa.TongSoKhoiLuongHoatChatGayNghien); //Tổng số khối lượng hoạt chất gây nghiện
                objHang.TWU.SetValue(HangHoa.DonVitinhKhoiLuong); //Đơn vị tính
                objHang.IMP.SetValue(HangHoa.GiaNhapKhauVND); //Giá nhập khẩu (VNĐ)
                objHang.SWP.SetValue(HangHoa.GiaBanBuonVND); //Giá bán buôn (VNĐ)
                objHang.ESP.SetValue(HangHoa.GiaBanLeVND); //Giá bán lẻ (VNĐ)
                objHang.EFN.SetValue(HangHoa.TenThuongNhanXuatKhau); //Tên của công ty xuất khẩu
                objHang.EXC.SetValue(HangHoa.MaBuuChinhXuatKhau); //Mã bưu chính của địa chỉ công ty xuất khẩu
                objHang.EFA.SetValue(HangHoa.SoNhaTenDuongXuatKhau); //Địa chỉ của công ty xuất khẩu (Số nhà, tên đường)
                objHang.EFS.SetValue(HangHoa.PhuongXaXuatKhau); //Địa chỉ của công ty xuất khẩu (Phường, xã)
                objHang.EFD.SetValue(HangHoa.QuanHuyenXuatKhau); //Địa chỉ của công ty xuất khẩu (Quận, huyện, thị xã)
                objHang.EFC.SetValue(HangHoa.TinhThanhPhoXuatKhau); //Địa chỉ của công ty xuất khẩu (Tỉnh, thành phố)
                objHang.ECC.SetValue(HangHoa.MaQuocGiaXuatKhau); //Địa chỉ của công ty xuất khẩu (Mã quốc gia)
                objHang.MFN.SetValue(HangHoa.TenCongTySanXuat); //Tên của công ty sản xuất
                objHang.MPC.SetValue(HangHoa.MaBuuChinhCongTySanXuat); //Mã bưu chính của địa chỉ công ty sản xuất
                objHang.MFA.SetValue(HangHoa.SoNhaTenDuongCongTySanXuat); //Địa chỉ của công ty sản xuất (Số nhà, tên đường)
                objHang.MFS.SetValue(HangHoa.PhuongXaCongTySanXuat); //Địa chỉ của công ty sản xuất (Phường, xã)
                objHang.MFD.SetValue(HangHoa.QuanHuyenCongTySanXuat); //Địa chỉ của công ty sản xuất (Quận, huyện, thị xã)
                objHang.MFC.SetValue(HangHoa.TinhThanhPhoCongTySanXuat); //Địa chỉ của công ty sản xuất (Tỉnh, thành phố)
                objHang.MCC.SetValue(HangHoa.MaQuocGiaCongTySanXuat); //Địa chỉ của công ty sản xuất (Mã quốc gia)
                objHang.DBN.SetValue(HangHoa.TenCongTyCungCap); //Tên của công ty cung cấp
                objHang.DPC.SetValue(HangHoa.MaBuuChinhCongTyCungCap); //Mã bưu chính của công ty cung cấp
                objHang.DAB.SetValue(HangHoa.SoNhaTenDuongCongTyCungCap); //Địa chỉ của công ty cung cấp (Số nhà, tên đường)
                objHang.DAS.SetValue(HangHoa.PhuongXaCongTyCungCap); //Địa chỉ của công ty cung cấp (Phường, xã)
                objHang.DAD.SetValue(HangHoa.QuanHuyenCongTyCungCap); //Địa chỉ của công ty cung cấp (Quận, huyện, thị xã)
                objHang.DAN.SetValue(HangHoa.TinhThanhPhoCongTyCungCap); //Địa chỉ của công ty cung cấp (Tỉnh, thành phố)
                objHang.DCC.SetValue(HangHoa.MaQuocGiaCongTyCungCap); //Địa chỉ của công ty cung cấp (Mã quốc gia)
                objHang.TPN.SetValue(HangHoa.TenDonViUyThac); //Tên của đơn vị ủy thác
                objHang.PPC.SetValue(HangHoa.MaBuuChinhDonViUyThac); //Mã bưu chính của đơn vị ủy thác
                objHang.TPB.SetValue(HangHoa.SoNhaTenDuongDonViUyThac); //Địa chỉ của đơn vị ủy thác (Số nhà, tên đường)
                objHang.TPS.SetValue(HangHoa.PhuongXaDonViUyThac); //Địa chỉ của đơn vị ủy thác (Phường, xã)
                objHang.TPD.SetValue(HangHoa.QuanHuyenDonViUyThac); //Địa chỉ của đơn vị ủy thác (Quận, huyện, thị xã)
                objHang.TPC.SetValue(HangHoa.TinhThanhPhoDonViUyThac); //Địa chỉ của đơn vị ủy thác (Tỉnh, thành phố)
                objHang.TCC.SetValue(HangHoa.MaQuocGiaDonViUyThac); //Địa chỉ của đơn vị ủy thác (Mã quốc gia)
                objHang.QS.SetValue(HangHoa.LuongTonKhoKyTruoc); //Lượng tồn kho kỳ trước
                objHang.QSU.SetValue(HangHoa.DonViTinhLuongTonKyTruoc); //Đơn vị tính
                objHang.QI.SetValue(HangHoa.LuongNhapTrongKy); //Lượng nhập trong kỳ
                objHang.TQ.SetValue(HangHoa.TongSo); //Tổng số
                objHang.TE.SetValue(HangHoa.TongSoXuatTrongKy); //Tổng số xuất trong kỳ
                objHang.ISD.SetValue(HangHoa.SoLuongTonKhoDenNgay); //Số lượng tồn kho đến ngày 
                objHang.DMD.SetValue(HangHoa.HuHao); //Hư hao
                objHang.REM.SetValue(HangHoa.GhiChuChiTiet); //Ghi chú (chi tiết)

                obj.ListHangHoa.Add(objHang);
            }

            return obj;
        }
        public static SMB SFMMapper(string SoGiayPhep)
        {
            SMB obj = new SMB();
            obj.APN.SetValue(SoGiayPhep);
            return obj;
        }
        public static ISM ISMMapper(string SoGiayPhep, string PhanLoai)
        {
            ISM obj = new ISM();
            obj.APN.SetValue(SoGiayPhep);
            obj.KBN.SetValue(PhanLoai);
            return obj;
        }

        //TODO: Thieu IAS, IBA
        public static IAS IASMapper(KDT_VNACC_ChungTuThanhToan cttt)
        {
            IAS obj = new IAS();
            obj.TYS.SetValue(cttt.LoaiHinhBaoLanh); //Loại hình bảo lãnh
            obj.SBC.SetValue(cttt.MaNganHangCungCapBaoLanh); //Mã ngân hàng cung cấp bảo lãnh
            obj.RYA.SetValue(cttt.NamPhatHanh); //Năm phát hành
            obj.SCM.SetValue(cttt.KiHieuChungTuPhatHanhBaoLanh); //Kí hiệu chứng từ phát hành bảo lãnh
            obj.SCN.SetValue(cttt.SoChungTuPhatHanhBaoLanh); //Số chứng từ  phát hành bảo lãnh
            obj.CCC.SetValue(cttt.MaTienTe); //Mã tiền tệ

            return obj;
        }
        public static IBA IBAMapper(KDT_VNACC_ChungTuThanhToan cttt)
        {
            IBA obj = new IBA();
            obj.BRC.SetValue(cttt.MaNganHangTraThay); //Mã ngân hàng trả thay
            obj.UBP.SetValue(cttt.MaDonViSuDungHanMuc); //Mã đơn vị sử dụng hạn mức
            obj.RYA.SetValue(cttt.NamPhatHanh); //Năm phát hành
            obj.BPS.SetValue(cttt.KiHieuChungTuPhatHanhHanMuc); //Kí hiệu chứng từ phát hành hạn mức
            obj.BPN.SetValue(cttt.SoHieuPhatHanhHanMuc); //Số hiệu phát hành hạn mức
            obj.CCC.SetValue(cttt.MaTienTe); //Mã tiền tệ

            return obj;
        }

        




        //TODO: Thieu MSB, HYS
        public static HYS HYSMapper(KDT_VNACC_ChungTuDinhKem chungtukem)
        {
            HYS hys = new HYS();
            hys.CH.SetValue(chungtukem.CoQuanHaiQuan);
            hys.CHB.SetValue(chungtukem.NhomXuLyHoSo);
            hys.TSB.SetValue(chungtukem.PhanLoaiThuTucKhaiBao);
            hys.STL.SetValue(chungtukem.SoDienThoaiNguoiKhaiBao);
            hys.REF.SetValue(chungtukem.SoQuanLyTrongNoiBoDoanhNghiep);
            hys.KIJ.SetValue(chungtukem.GhiChu);
            hys.AttachDatas = new List<HYS_AttachData>();
            foreach (KDT_VNACC_ChungTuDinhKem_ChiTiet item in chungtukem.ChungTuDinhKemChiTietCollection)
            {
                HYS_AttachData data = new HYS_AttachData();
                data.FileName = item.FileName;
                data.Data = item.NoiDung;
                hys.AttachDatas.Add(data);
            }
            return hys;
        }

        public static MSB MSBMapper(KDT_VNACC_ChungTuDinhKem chungtukem)
        {
            MSB msb = new MSB();
            msb.CHN.SetValue(chungtukem.CoQuanHaiQuan);
            msb.CHB.SetValue(chungtukem.NhomXuLyHoSo);
            msb.SUB.SetValue(chungtukem.TieuDe);
            msb.ICN.SetValue(chungtukem.SoToKhai);
            msb.TUS.SetValue(chungtukem.GhiChu);
//             msb.TSB.SetValue(chungtukem.PhanLoaiThuTucKhaiBao);
//             msb.STL.SetValue(chungtukem.SoDienThoaiNguoiKhaiBao);
//             msb.REF.SetValue(chungtukem.SoQuanLyTrongNoiBoDoanhNghiep);
//             msb.KIJ.SetValue(chungtukem.GhiChu);
            msb.AttachDatas = new List<HYS_AttachData>();
            foreach (KDT_VNACC_ChungTuDinhKem_ChiTiet item in chungtukem.ChungTuDinhKemChiTietCollection)
            {
                HYS_AttachData data = new HYS_AttachData();
                data.FileName = item.FileName;
                data.Data = item.NoiDung;
                msb.AttachDatas.Add(data);
            }
            return msb;
        }


        //TODO: Thieu AMA, AMB, AMC, IAD
        public static AMA AMAMapper(KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBS)
        {
            AMA obj = new AMA();
            obj.SYN.SetValue(TKBS.SoToKhaiBoSung); //Số tờ khai bổ sung
            obj.CHS.SetValue(TKBS.CoQuanHaiQuan); //Cơ quan Hải quan
            obj.CHB.SetValue(TKBS.NhomXuLyHoSo); //Nhóm xử lý hồ sơ
            obj.YNK.SetValue(TKBS.PhanLoaiXuatNhapKhau); //Phân loại xuất nhập khẩu
            obj.ICN.SetValue(TKBS.SoToKhai); //Số tờ khai
            obj.ICB.SetValue(TKBS.MaLoaiHinh); //Mã loại hình
            obj.IDD.SetValue(TKBS.NgayKhaiBao); //Ngày khai báo xuất nhập khẩu
            obj.IPD.SetValue(TKBS.NgayCapPhep); //Ngày cấp phép xuất nhập khẩu
            obj.RED.SetValue(TKBS.ThoiHanTaiNhapTaiXuat); //Thời hạn tái nhập/ tái xuất
            obj.IMC.SetValue(TKBS.MaNguoiKhai); //Mã người khai
            obj.IMN.SetValue(TKBS.TenNguoiKhai); //Tên người khai
            obj.IMY.SetValue(TKBS.MaBuuChinh); //Mã bưu chính
            obj.IMA.SetValue(TKBS.DiaChiNguoiKhai); //Địa chỉ của người khai
            obj.IMT.SetValue(TKBS.SoDienThoaiNguoiKhai); //Số điện thoại của người khai
            obj.REC.SetValue(TKBS.MaLyDoKhaiBoSung); //Mã lý do khai bổ sung
            obj.TTC.SetValue(TKBS.MaTienTeTienThue); //Mã tiền tệ của tiền thuế
            obj.BRC.SetValue(TKBS.MaNganHangTraThueThay); //Mã ngân hàng trả thuế thay
            obj.BYA.SetValue(TKBS.NamPhatHanhHanMuc); //Năm phát hành hạn mức
            obj.BCM.SetValue(TKBS.KiHieuChungTuPhatHanhHanMuc); //Kí hiệu chứng từ phát hành hạn mức
            obj.BCN.SetValue(TKBS.SoChungTuPhatHanhHanMuc); //Số chứng từ phát hành hạn mức
            obj.ENC.SetValue(TKBS.MaXacDinhThoiHanNopThue); //Mã xác định thời hạn nộp thuế
            obj.SBC.SetValue(TKBS.MaNganHangBaoLanh); //Mã ngân hàng bảo lãnh
            obj.RYA.SetValue(TKBS.NamPhatHanhBaoLanh); //Năm phát hành bảo lãnh
            obj.SCM.SetValue(TKBS.KyHieuPhatHanhChungTuBaoLanh); //Ký hiệu phát hành chứng từ bảo lãnh
            obj.SCN.SetValue(TKBS.SoHieuPhatHanhChungTuBaoLanh); //Số hiệu phát hành chứng từ bảo lãnh
            obj.CDB.SetValue(TKBS.MaTienTeTruocKhiKhaiBoSung); //Mã tiền tệ trước khi khai bổ sung
            obj.RTB.SetValue(TKBS.TyGiaHoiDoaiTruocKhiKhaiBoSung); //Tỷ giá hối đoái trước khi khai bổ sung
            obj.CDA.SetValue(TKBS.MaTienTeSauKhiKhaiBoSung); //Mã tiền tệ sau khi khai bổ sung
            obj.RTA.SetValue(TKBS.TyGiaHoiDoaiSauKhiKhaiBoSung); //Tỷ giá hối đoái sau khi khai bổ sung
            obj.REF.SetValue(TKBS.SoQuanLyTrongNoiBoDoanhNghiep); //Số quản lý trong nội bộ doanh nghiệp
            obj.NTB.SetValue(TKBS.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung); //Ghi chú nội dung liên quan trước khi khai bổ sung
            obj.NTA.SetValue(TKBS.GhiChuNoiDungLienQuanSauKhiKhaiBoSung); //Ghi chú nội dung liên quan sau khi khai bổ sung
            

            if (obj.ListHangHoa == null) obj.ListHangHoa = new List<AMA_HANGHOA>();
            foreach (var HangHoa in TKBS.HangCollection)
            {
                AMA_HANGHOA objHang = new AMA_HANGHOA();
                objHang.RAN.SetValue(HangHoa.SoThuTuDongHangTrenToKhaiGoc); //Số thứ tự dòng/hàng trên tờ khai gốc
                objHang.CMB.SetValue(HangHoa.MoTaHangHoaTruocKhiKhaiBoSung); //Mô tả hàng hóa trước khi khai bổ sung
                objHang.CMA.SetValue(HangHoa.MoTaHangHoaSauKhiKhaiBoSung); //Mô tả hàng hóa sau khi khai bổ sung
                objHang.ORB.SetValue(HangHoa.MaNuocXuatXuTruocKhiKhaiBoSung); //Mã nước xuất xứ trước khi khai bổ sung
                objHang.ORA.SetValue(HangHoa.MaNuocXuatXuSauKhiKhaiBoSung); //Mã nước xuất xứ sau khi khai bổ sung
                objHang.MKB.SetValue(HangHoa.TriGiaTinhThueTruocKhiKhaiBoSung,true); //Trị giá tính thuế trước khi khai bổ sung
                objHang.MKQ.SetValue(HangHoa.SoLuongTinhThueTruocKhiKhaiBoSung,true); //Số lượng tính thuế trước khi khai bổ sung
                objHang.MKC.SetValue(HangHoa.MaDonViTinhSoLuongTinhThueTruocKhaiBoSung); //Mã đơn vị tính của số lượng tính thuếtrước khai bổ sung
                objHang.MKT.SetValue(HangHoa.MaSoHangHoaTruocKhiKhaiBoSung); //Mã số hàng hóa trước khi khai bổ sung
                objHang.MKR.SetValue(HangHoa.ThueSuatTruocKhiKhaiBoSung); //Thuế suất trước khi khai bổ sung
                objHang.MKA.SetValue(HangHoa.SoTienThueTruocKhiKhaiBoSung); //Số tiền thuế trước khi khai bổ sung
                objHang.AKB.SetValue(HangHoa.TriGiaTinhThueSauKhiKhaiBoSung == 0 ? 0 : HangHoa.TriGiaTinhThueSauKhiKhaiBoSung,true); //Trị giá tính thuế sau khi khai bổ sung
                objHang.AKQ.SetValue(HangHoa.SoLuongTinhThueSauKhiKhaiBoSung,true); //Số lượng tính thuế sau khi khai bổ sung
                objHang.AKC.SetValue(HangHoa.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung); //Mã đơn vị tính số lượng tính thuế sau khi khai bổ sung
                objHang.AKT.SetValue(HangHoa.MaSoHangHoaSauKhiKhaiBoSung); //Mã số hàng hóa sau khi khai bổ sung
                objHang.AKR.SetValue(HangHoa.ThueSuatSauKhiKhaiBoSung,true); //Thuế suất sau khi khai bổ sung
                objHang.AKA.SetValue(HangHoa.SoTienThueSauKhiKhaiBoSung,true); //Số tiền thuế sau khi khai bổ sung
                if (HangHoa.HangThuKhacCollection != null && HangHoa.HangThuKhacCollection.Count > 0)
                {
                    for (int i = 0; i < HangHoa.HangThuKhacCollection.Count & i < 5; i++)
                    {
                        KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac item = HangHoa.HangThuKhacCollection[i];
                        objHang.MB_.SetValueCollection(item.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac == 0 ? 0 : item.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_MB_, i);
                        objHang.MB_.SetValueCollection(item.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac == 0 ? 0 : item.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_MQ_, i);
                        objHang.MB_.SetValueCollection(item.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_MC_, i);
                        objHang.MB_.SetValueCollection(item.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_MK_, i);
                        objHang.MB_.SetValueCollection(item.ThueSuatTruocKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_MR_, i);
                        objHang.MB_.SetValueCollection(item.SoTienThueTruocKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_MA_, i);
                        objHang.MB_.SetValueCollection(item.TriGiaTinhThueSauKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_AB_, i);
                        objHang.MB_.SetValueCollection(item.SoLuongTinhThueSauKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_AQ_, i);
                        objHang.MB_.SetValueCollection(item.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_AC_, i);
                        //objHang.MB_.SetValueCollection(item.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_MC_, i);
                        objHang.MB_.SetValueCollection(item.MaApDungThueSuatSauKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_AK_, i);
                        objHang.MB_.SetValueCollection(item.ThueSuatSauKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_AR_, i);
                        objHang.MB_.SetValueCollection(item.SoTienThueSauKhiKhaiBoSungThuKhac, EnumGroupID.AMA_HANGHOA_AA_, i);
                        //item = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();
                        //obj.ListHangHoa.Add(objHang);
                    }
                    
                }
                //objHang.MB_.SetValue(HangHoa.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac); //Trị giá tính thuế trước khi khai bổ sung (thuế và thu khác)
                //objHang.MQ_.SetValue(HangHoa.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac); //Số lượng tính thuế trước khi khai bổ sung (thuế và thu khác)
                //objHang.MC_.SetValue(HangHoa.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac); //Mã đơn vị tính số lượng tính thuế trước khi khai bổ sung (thuế và thu khác) 
                //objHang.MK_.SetValue(HangHoa.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac); //Mã áp dụng thuế suất trước khi khai bổ sung (thuế và thu khác)
                //objHang.MR_.SetValue(HangHoa.ThueSuatTruocKhiKhaiBoSungThuKhac); //Thuế suất trước khi khai bổ sung (thuế và thu khác)
                //objHang.MA_.SetValue(HangHoa.SoTienThueTruocKhiKhaiBoSungThuKhac); //Số tiền thuế trước khi khai bổ sung (thuế và thu khác)
                //objHang.AB_.SetValue(HangHoa.TriGiaTinhThueSauKhiKhaiBoSungThuKhac); //Trị giá tính thuế sau khi khai bổ sung (thuế và thu khác)
                //objHang.AQ_.SetValue(HangHoa.SoLuongTinhThueSauKhiKhaiBoSungThuKhac); //Số lượng tính thuế sau khi khai bổ sung ( thuế và thu khác)
                //objHang.AC_.SetValue(HangHoa.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac); //Mã đơn vị tính số lượng tính thuế sau khi khai bổ sung (thuế và thu khác)
                //objHang.AK_.SetValue(HangHoa.MaApDungThueSuatSauKhiKhaiBoSungThuKhac); //Mã áp dụng thuế suất sau khi khai bổ sung (thuế và thu khác)
                //objHang.AR_.SetValue(HangHoa.ThueSuatSauKhiKhaiBoSungThuKhac); //Thuế suất sau khi khai bổ sung (thuế và thu khác)
                //objHang.AA_.SetValue(HangHoa.SoTienThueSauKhiKhaiBoSungThuKhac); //Số tiền thuế sau khi khai bổ sung (thuế và thu khác)

                obj.ListHangHoa.Add(objHang);
            }

            return obj;
        }


        public static AMC AMCMapper(decimal SoToKhaiBS)
        {
            AMC obj = new AMC();
            obj.SYN.SetValue(SoToKhaiBS); //Số tờ khai bổ sung
            return obj;
        }
    }
}
