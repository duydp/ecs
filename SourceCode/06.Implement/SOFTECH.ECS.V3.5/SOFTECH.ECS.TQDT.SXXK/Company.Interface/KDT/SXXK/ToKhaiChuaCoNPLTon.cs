﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Infragistics.Excel;
using Company.BLL.KDT;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface.SXXK
{
    public partial class ToKhaiChuaCoNPLTon : BaseForm
    {
        public DataSet ds = new DataSet();
        public ToKhaiChuaCoNPLTon()
        {
            InitializeComponent();
            
        }

        
        private void btnUpdateDM_Click(object sender, EventArgs e)
        {
            string msg = "DS tờ khai: ";
            bool flag = true;
            for (int i = 0; i < dgList.RowCount; i++)
            {
                int sotokhai = int.Parse(dgList.GetRow(i).Cells[0].Value.ToString());
                string maloaihinh = dgList.GetRow(i).Cells[2].Value.ToString();
                int namdangky = int.Parse(dgList.GetRow(i).Cells[4].Value.ToString());
                ToKhaiMauDich tkmd = ToKhaiMauDich.LoadBySoToKhai(sotokhai,maloaihinh,namdangky);

                if (!tkmd.CapNhatThongTinHangToKhaiSua())
                {
                    flag = false;
                    msg += sotokhai.ToString() + " | ";
                }
            }
            msg += " không thể cập nhật.";
            if (flag)
            {
                MessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK);
                this.Close();
            }
            else 
            {
                MessageBox.Show(msg, "Lỗi", MessageBoxButtons.OK);
                this.ToKhaiChuaCoNPLTon_Load(sender,e);
            }
                
        }

        private void ToKhaiChuaCoNPLTon_Load(object sender, EventArgs e)
        {
            this.dgList.DataSource = ds.Tables[0];
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

    }
}