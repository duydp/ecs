﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Danh Sách hợp đồng khai kèm
    /// </summary>
    public class ContractDocument : IssueBase
    {
        /// <summary>
        /// Thời hạn
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }
        /// <summary>
        /// Thanh toán
        /// </summary>
        [XmlElement("Payment")]
        public Payment Payment { get; set; }
        /// <summary>
        /// Điều kiện giao hàng
        /// </summary>
        [XmlElement("TradeTerm")]
        public TradeTerm TradeTerm { get; set; }
        /// <summary>
        /// Địa Điểm giao hàng
        /// </summary>
        [XmlElement("DeliveryDestination")]
        public DeliveryDestination DeliveryDestination { get; set; }
        /// <summary>
        /// nguyên tệ
        /// </summary>
        [XmlElement("CurrencyExchange")]
        public CurrencyExchange CurrencyExchange { get; set; }
        /// <summary>
        /// bên gia công
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }
        /// <summary>
        /// Bên thuê gia công
        /// </summary>
        [XmlElement("Exporter")]
        public NameBase Exporter { get; set; }
        /// <summary>
        /// Giá trị hợp đồng
        /// </summary>
        [XmlElement("CustomsValue")]
        public CustomsValue CustomsValue { get; set; }
        /// <summary>
        /// nước gia công
        /// </summary>
        [XmlElement("importationCountry")]
        public string ImportationCountry { get; set; }
        /// <summary>
        /// nước thuê gia công
        /// </summary>
        [XmlElement("ExportationCountry")]
        public string ExportationCountry { get; set; }
        /// <summary>
        /// (Phần GC_Hợp đồng)
        /// </summary>
        [XmlArray("ContractItems")]
        [XmlArrayItem("Item")]
        public List<Item> Item { get; set; }
        /// <summary>
        /// (Phần GC_Hợp đồng)
        /// </summary>
        [XmlArray("Products")]
        [XmlArrayItem("Product")]
        public List<Product> Product { get; set; }
        /// <summary>
        /// Tổng giá trị
        /// </summary>
        [XmlElement("totalValue")]
        public string TotalValue { get; set; }
        /// <summary>
        /// Người nhập
        /// </summary>
        [XmlElement("Buyer")]
        public NameBase Buyer { get; set; }
        /// <summary>
        /// Người xuất
        /// </summary>
        [XmlElement("Seller")]
        public NameBase Seller { get; set; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("ContractItem")]
        public List<ContractItem> ContractItems { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("Equipment")]
        public List<Equipment> Equipment { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("SampleProduct")]
        public List<Product> SampleProduct { get; set; }
        



    }
}
