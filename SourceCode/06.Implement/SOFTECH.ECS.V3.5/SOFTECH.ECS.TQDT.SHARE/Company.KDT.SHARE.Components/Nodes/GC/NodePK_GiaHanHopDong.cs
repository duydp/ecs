﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.GC
{
    public class NodePK_GiaHanHopDong
    {
        /// <summary>
        /// Ngày hợp đồng cũ
        /// </summary>
        public const string oldExpire = "oldExpire";
        /// <summary>
        /// Ngày gia hạn mới
        /// </summary>
        public const string newExpire = "newExpire";            
    }
}
