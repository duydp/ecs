﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class LenhSanXuatSPManagerForm : Company.Interface.BaseForm
    {
        public KDT_LenhSanXuat lenhSanXuat = new KDT_LenhSanXuat();
        public string where = "";
        public LenhSanXuatSPManagerForm()
        {
            InitializeComponent();
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    switch (e.Row.Cells["TinhTrang"].Text.ToString())
                    {
                        case "0":
                            e.Row.Cells["TinhTrang"].Text = "Tạo mới";
                            break;
                        case "1":
                            e.Row.Cells["TinhTrang"].Text = "Đang sản xuất";
                            break;
                        case "2":
                            e.Row.Cells["TinhTrang"].Text = "Đã hoàn thành";
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    lenhSanXuat = (KDT_LenhSanXuat)e.Row.DataRow;
                    lenhSanXuat.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(lenhSanXuat.ID);
                    LenhSanXuatSPForm f = new LenhSanXuatSPForm();
                    f.lenhSanXuat = lenhSanXuat;
                    f.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1=1";
                if (!String.IsNullOrEmpty(txtMaDDSX.Text))
                    where += " AND SoLenhSanXuat LIKE '%" + txtMaDDSX.Text + "%'";
                if (cbbTinhTrang.SelectedValue!= null)
                    where += " AND TinhTrang =" + cbbTinhTrang.SelectedValue;
                if (!String.IsNullOrEmpty(txtPO.Text))
                    where += " AND SoDonHang LIKE '%" + txtPO.Text + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                grList.Refetch();
                grList.DataSource = KDT_LenhSanXuat.SelectCollectionDynamic(GetSearchWhere(), "");
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_LenhSanXuat> ItemColl = new List<KDT_LenhSanXuat>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_LenhSanXuat)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_LenhSanXuat item in ItemColl)
                    {
                        item.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
                        foreach (KDT_LenhSanXuat_SP sp in item.SPCollection)
                        {
                            sp.Delete();
                        }
                        List<SXXK_DinhMuc> DinhMucCollection = SXXK_DinhMuc.SelectCollectionDynamic(" MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "' AND LenhSanXuat_ID = " + item.ID + "", "");
                        foreach (SXXK_DinhMuc ite in DinhMucCollection)
                        {
                            ite.LenhSanXuat_ID = 0;
                            ite.Update();
                        }
                        item.Delete();
                    }
                    btnSearch_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LenhSanXuatSPManagerForm_Load(object sender, EventArgs e)
        {
            cbbTinhTrang.SelectedValue = "0";
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH LỆNH SẢN XUẤT.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
