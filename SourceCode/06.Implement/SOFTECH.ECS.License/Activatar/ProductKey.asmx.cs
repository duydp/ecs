﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using KeyServer;
using KeySecurity;

namespace Activation
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://Softech.VN/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class ProductKey : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }


        #region Product
        [WebMethod]
        public string GetInfoProduct()
        {
            return KeyCode.ConvertToBase64(KeyCode.Serializer(Key.SelectProduct(), true, true));
        }
        [WebMethod]
        public string GetGroupProduct(string userName, string Pass)
        {
            KeyServer.User.User userlogin = new KeyServer.User.User();
            if (userlogin.IsLogin(userName, KeyCode.ConvertFromBase64(Pass)))
            {
                List<ProductGroup> listGroup = new List<ProductGroup>();
                listGroup = ProductGroup.LoadByPermission(userlogin.Permission.ID);
                if (listGroup.Count > 0)
                {
                    foreach (ProductGroup item in listGroup)
                    {
                        item.LoadProduct();
                    }
                    return KeyCode.ConvertToBase64(KeyCode.Serializer(listGroup));
                }
                
            }
            return string.Empty;
        }
        [WebMethod]
        public string GetListProduct(string userName, string Pass)
        {
            KeyServer.User.User userlogin = new KeyServer.User.User();
            if (userlogin.IsLogin(userName, KeyCode.ConvertFromBase64(Pass)))
            {
                List<Product> listProduct = new List<Product>();
                listProduct = Product.LoadByPermission(userlogin.Permission.ID);
                if (listProduct.Count > 0)
                    return KeyCode.ConvertToBase64(KeyCode.Serializer(listProduct));
                else
                    return null;
            }
            else
                return "Sai phân quyền";

        }

        #endregion


        #region GenKey
        [WebMethod]
        public string GenKey(string userName, string Pass, string Productid, string SearialNum, int days, int soLuong, bool trial, string createdBy)
        {
            if (CheckLoginPermission(userName, Pass, "GenKey"))
            {
                return Key.GenKey(Productid, SearialNum, days, soLuong, trial, createdBy);
            }
            else
                return "Sai phân quyền";
        }
        #endregion

        #region Delete Key
        [WebMethod]
        public bool Delete(string userName, string Pass, Key key)
        {
            try
            {
                key.Delete();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Notice

        [WebMethod]
        public string UpdateNotices(string userName, string Pass, string text, string product, bool trial, bool all)
        {
            try
            {
                if (CheckLoginPermission(userName, Pass, "UpdateNotices"))
                {
                    return Key.UpdateNotices(text, product, trial, all);
                }
                else
                {
                    return "Sai phân quyền";
                }
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }

        }

        #endregion
        
      

        #region Get Key

        [WebMethod]
        public string GetKeyTrial(string userName, string Pass, string productID, int soNgay, int soluong)
        {
            try
            {
                if (CheckLoginPermission(userName, Pass, "GetTrial"))
                {
                    List<Key> newKey = new List<Key>();
                    newKey = Key.GetKey(productID, userName, soNgay, soluong, "0");
                    if (newKey == null)
                        return KeyCode.ConvertToBase64("Không tìm thấy key như yêu cầu");
                    else
                        return KeyCode.ConvertToBase64(KeySecurity.KeyCode.Serializer(newKey));
                }
                else
                    return KeyCode.ConvertToBase64("Sai Phân quyền");
            }
            catch (System.Exception ex)
            {
                return KeySecurity.KeyCode.ConvertToBase64(ex.Message);
            }
        }
        [WebMethod]
        public string ProposalKeyLicense(string userName, string Pass, string CustomerCode, string CustomerName, string CustomerDescription, string productID, int soNgay, int soluong)
        {
            try
            {
                if (CheckLoginPermission(userName, Pass, "GetTrial"))
                {
                    return Key.ProposalKeyLicense(productID, userName, soNgay, soluong, CustomerCode, CustomerName, CustomerDescription);
                }
                else
                    return "Sai Phân quyền";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }
        [WebMethod]
        public string ApproveKeyLicense(string userName, string Pass, string SerialNumber, bool Approve)
        {
            try
            {
                if (CheckLoginPermission(userName, Pass, "GetLicense"))
                {
                    return Key.ApproveKeyLicense(SerialNumber, Approve, userName);
                }
                else
                    return "Sai Phân quyền";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod]
        public string GetKeyLicense(string userName, string Pass, string productID, string RequestBy, int soNgay, int soluong, string CustomerCode, string CustomerName, string CustomerDes)
        {
            try
            {
                if (CheckLoginPermission(userName, Pass, "GetLicense"))
                {
                    List<Key> newKey = new List<Key>();
                    newKey = Key.GetKey(productID, RequestBy, soNgay, soluong, "1", CustomerCode,CustomerName,CustomerDes);
                    if (newKey == null)
                        return KeyCode.ConvertToBase64("Không tìm thấy key như yêu cầu");
                    else
                        return KeyCode.ConvertToBase64(KeySecurity.KeyCode.Serializer(newKey));
                }
                else
                    return KeyCode.ConvertToBase64("Sai Phân quyền");
            }
            catch (System.Exception ex)
            {
                return KeySecurity.KeyCode.ConvertToBase64(ex.Message);
            }
        }

        #endregion


        #region  Seach Key

        [WebMethod]
        public string GetInfoKey(string ActiveKey)
        {
            try
            {
                Key key = new Key();
                key = Key.checkKey(ActiveKey);
                if (key == null)
                    return KeyCode.ConvertToBase64("Không tìm thấy key");
                else
                {
                    key.ActivedKey = string.Empty;
                }
                return KeyCode.ConvertToBase64(KeyCode.Serializer(key));
            }
            catch (System.Exception ex)
            {
                return KeyCode.ConvertToBase64("Không tìm thấy key. " + ex.Message);
            }
        }

        [WebMethod]
        public string SeachKey(string UserName, string Pass, string TypeKey, bool? status, DateTime FromDay, DateTime ToDay, string ProductID)
        {
            try
            {
                List<Key> listKey = new List<Key>();
                if (CheckLoginPermission(UserName, Pass, "GetLicense"))
                {
                    if (TypeKey == "-1")
                        listKey = Key.SeachKey(TypeKey, status, FromDay, ToDay, string.Empty, string.Empty); //Tìm kiếm key chờ Approve
                    else
                        listKey = Key.SeachKey(TypeKey, status, FromDay, ToDay, ProductID, UserName); // Tìm kiếm key đã Get
                    if (listKey.Count == 0)
                        return string.Empty;
                    return KeyCode.ConvertToBase64(KeyCode.Serializer(listKey));
                }
                else if (CheckLoginPermission(UserName, Pass, "GetTrial"))
                {
                    if (TypeKey == "-1")
                        listKey = Key.SeachKey(TypeKey, status, FromDay, ToDay, string.Empty, UserName); //Tìm kiếm key chờ Approve
                    else
                        listKey = Key.SeachKey(TypeKey, status, FromDay, ToDay, ProductID, UserName); // Tìm kiếm key đã Get
                    if (listKey.Count == 0)
                        return string.Empty;
                    return KeyCode.ConvertToBase64(KeyCode.Serializer(listKey));
                }
                else
                    return "Sai phân quyền";

            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod]
        public string SeachAllKey(string UserName, string Pass, DateTime FromDay, DateTime ToDay)
        {
            try
            {
                List<Key> listKey = new List<Key>();
                KeyServer.User.User userlogin = new KeyServer.User.User();
                if (userlogin.IsLogin(UserName, KeyCode.ConvertFromBase64(Pass)))
                {
                    listKey = Key.SeachAllKey(userlogin.Permission.ID, FromDay, ToDay);
                    if (listKey.Count > 0)
                        return KeyCode.ConvertToBase64(KeyCode.Serializer(listKey));
                    else
                        return null;
                }
                else
                    return "Sai phân quyền";

            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod]
        public string SeachKeyByCustomer(string UserName, string Pass,string CustomerCode, string WhereCondition)
        {
            try
            {
                List<Key> listKey = new List<Key>();
                KeyServer.User.User userlogin = new KeyServer.User.User();
                if (userlogin.IsLogin(UserName, KeyCode.ConvertFromBase64(Pass)))
                {
                    listKey = Key.SeachByCustomer(CustomerCode, WhereCondition);
                    if (listKey.Count > 0)
                        return KeyCode.ConvertToBase64(KeyCode.Serializer(listKey));
                    else
                        return null;
                }
                else
                    return "Sai phân quyền";

            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }
        [WebMethod]
        public List<Key> SelectCollectionDynamic(string UserName, string Pass, string WhereCondition)
        {
            try
            {
                List<Key> listKey = new List<Key>();
                KeyServer.User.User userlogin = new KeyServer.User.User();
                if (userlogin.IsLogin(UserName, KeyCode.ConvertFromBase64(Pass)))
                {
                    listKey = Key.SelectCollectionDynamic(WhereCondition,"");
                    if (listKey.Count > 0)
                        return listKey;
                    else
                        return null;
                }
                else
                    return null;

            }
            catch (System.Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region  LOCK key
        [WebMethod]
        public string LockKey(string userName, string pass, string serialKey, string LyDoLock)
        {
            try
            {
                if (CheckLoginPermission(userName, pass, "GetLicense"))
                {
                    return Key.LockKey(serialKey, LyDoLock, userName);
                }
                else
                    return "Sai phân quyền";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            
        
        }
        #endregion

        #region User

        [WebMethod]
        public string CreateUser(string userName, string Pass, string User)
        {
            try
            {
                KeyServer.User.User UserUpdate = KeyCode.Deserialize<KeyServer.User.User>(KeyCode.ConvertFromBase64(User));
                if (CheckLoginPermission(userName, Pass, "Admin"))
                {
                    if (!KeyServer.User.User.IsExit(UserUpdate.UserName))
                        UserUpdate.InsertFull();
                    else
                        return "User đã tồn tại";
                }
                else if (CheckLoginPermission(userName, Pass, "TeamLead"))
                {
                    if (!KeyServer.User.User.IsExit(UserUpdate.UserName))
                    {
                        if (!KeyServer.User.User.IsExit(UserUpdate.UserName))
                        {

                            UserUpdate.InsertFull();
                        }
                        else
                            return "User đã tồn tại";
                    }
                }
                else
                    return "Bạn không có quyền tạo User";
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }


        [WebMethod]
        public string UpdateUser(string userName, string Pass, string User)
        {
            try
            {
                KeyServer.User.User userlogin = new KeyServer.User.User();
                KeyServer.User.User UserUpdate = KeyCode.Deserialize<KeyServer.User.User>(KeyCode.ConvertFromBase64(User));
                if (userlogin.IsLogin(userName, KeyCode.ConvertFromBase64(Pass)))
                {
                    if (userlogin.ID == UserUpdate.ID)
                        UserUpdate.Update();
                    else
                        return "Bạn không có quyền hạn";
                }
                else
                    return "Sai user hoặc password";
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod]
        public string DeleteUser(string userName, string pass, string User)
        {
            try
            {
                KeyServer.User.User UserDelete = KeyCode.Deserialize<KeyServer.User.User>(KeyCode.ConvertFromBase64(User));
                if (CheckLoginPermission(userName, pass, "Admin"))
                {
                    if (KeyServer.User.User.IsExit(UserDelete.UserName) && UserDelete.Permission.ID > 0)
                    {
                        UserDelete.Permission.Delete();
                        UserDelete.Delete();
                        return string.Empty;
                    }
                    else
                        return "User không tồn tại";
                }
                else
                    return "Bạn không có quyền hạn thực hiện";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod]
        public string GetInfoUser(string UserName, string Pass)
        {
            try
            {
                if (CheckLoginPermission(UserName, Pass, "Admin"))
                {
                    List<KeyServer.User.User> listUser = new List<KeyServer.User.User>();
                    listUser = KeyServer.User.User.LoadFull();
                    return KeyCode.ConvertToBase64(KeyCode.Serializer(listUser));
                }
                else
                    return string.Empty;
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod]
        public string UpdatePermission(string userName, string pass, string User)
        {
            try
            {
                KeyServer.User.User UserUpdate = KeyCode.Deserialize<KeyServer.User.User>(KeyCode.ConvertFromBase64(User));
                if (CheckLoginPermission(userName, pass, "Admin"))
                {
                    if (KeyServer.User.User.IsExit(UserUpdate.UserName) && UserUpdate.Permission.ID > 0)
                    {
                        UserUpdate.Update();
                        UserUpdate.Permission.Update();
                        return string.Empty;
                    }
                    else
                        return "User không tồn tại";
                }
                else
                    return "Bạn không có quyền hạn thực hiện";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region Security

        [WebMethod]
        public string Login(string userName, string Pass)
        {
            try
            {
                KeyServer.User.User userlogin = new KeyServer.User.User();
                if (userlogin.IsLogin(userName, KeyCode.ConvertFromBase64(Pass)))
                    return KeyCode.ConvertToBase64(KeyCode.Serializer(userlogin));
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        
        private bool CheckLoginPermission(string userName, string Pass, string permission)
        {
            return this.CheckLoginPermission(userName, Pass, permission, 0, string.Empty, 0);
        }
        private bool CheckLoginPermission(string userName, string Pass, string permission, long userID)
        {
            return this.CheckLoginPermission(userName, Pass, permission, 0, string.Empty, userID);
        }
        private bool CheckLoginPermission(string userName, string Pass, string permission,  string Produc_ID)
        {
            return this.CheckLoginPermission(userName, Pass, permission, 0, Produc_ID, 0);
        }
        private bool CheckLoginPermission(string userName, string Pass, string permission, int Group_ID)
        {
            return this.CheckLoginPermission(userName, Pass, permission, Group_ID, string.Empty, 0);
        }
        private bool CheckLoginPermission(string userName, string Pass, string permission, int Group_ID, string Produc_ID, long userID)
        {
            try
            {
                KeyServer.User.User userlogin = new KeyServer.User.User();
                if (userlogin.IsLogin(userName, KeyCode.ConvertFromBase64(Pass)))
                {
                    #region Check permission product Group
                    
                    if (Group_ID > 0)
                    {
                        bool check = false;
                        List<ProductGroup> listgroup = ProductGroup.LoadByPermission(userlogin.Permission.ID);
                        if (listgroup == null || listgroup.Count == 0)
                            return false;
                        else
                            foreach (ProductGroup group in listgroup)
                            {
                                if (group.ID == Group_ID)
                                {
                                    check = true;
                                    break;
                                }
                            }
                        if (!check) return false;
                    }
                    #endregion

                    #region Check permission user cấp dưới

                    if (userID > 0)
                    {
                        bool check = false;
                        List<KeyServer.User.User> listuser = KeyServer.User.User.LoadUserByPermission(userlogin.Permission.ID);
                        if (listuser == null || listuser.Count == 0)
                            return false;
                        else
                            foreach (KeyServer.User.User user in listuser)
                            {
                                if (user.ID == userID)
                                {
                                    check = true;
                                    break;
                                }
                            }
                        if (!check) return false;
                    }
                    #endregion

                    #region Check permission product

                    if (!string.IsNullOrEmpty(Produc_ID))
                    {
                        bool check = false;
                        List<Product> listProduct = Product.LoadByPermission(userlogin.Permission.ID);
                        if (listProduct == null || listProduct.Count == 0)
                            return false;
                        else
                            foreach (Product product in listProduct)
                            {
                                if (product.Product_ID == Produc_ID)
                                {
                                    check = true;
                                    break;
                                }
                            }
                        if (!check) return false;
                    }
                    #endregion

                    switch (permission)
                    {
                        case "Admin":
                            return userlogin.Permission.Admin;
                        case "GenKey":
                            return userlogin.Permission.GenKey;
                        case "GetLicense":
                            return userlogin.Permission.GetLicense;
                        case "GetTrial":
                            return userlogin.Permission.GetTrial;
                        case "UpdateNotices":
                            return userlogin.Permission.UpdateNotices;
                        case "NO": // Không quan tâm đến permission
                            return true;
                        case "TeamLead": // quyền quản trị các user cấp dưới
                            return userlogin.Permission.TeamLead;
                            default:
                            return false;
                    }
                }
                else
                    return false;
            }
            catch 
            {
                return false;
            }
        }


        #endregion

        #region Customer
        [WebMethod]
        public string CustomerLoad(string UserName, string pass)
        {
            try
            {
                if (CheckLoginPermission(UserName, pass, "NO"))
                {
                    List<Customer> listCust = new List<Customer>();
                    listCust = Customer.SelectCollectionAll();
                    if (listCust.Count > 0)
                    {
                        return KeyCode.ConvertToBase64(KeyCode.Serializer(listCust));
                    }
                    else
                        return string.Empty;
                }
                else
                    return "User không tồn tại";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }


        [WebMethod]
        public string CustomerLoadByProduct(string UserName, string pass, string Produc_ID)
        {
            try
            {
                if (CheckLoginPermission(UserName, pass, "NO"))
                {
                    List<Customer> listCust = new List<Customer>();
                    listCust = Customer.LoadFromKeys(Produc_ID);
                    if (listCust.Count > 0)
                    {
                        return KeyCode.Serializer(listCust);
                    }
                    else
                        return string.Empty;
                }
                else
                    return "User không tồn tại";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion Customer

    }
}
