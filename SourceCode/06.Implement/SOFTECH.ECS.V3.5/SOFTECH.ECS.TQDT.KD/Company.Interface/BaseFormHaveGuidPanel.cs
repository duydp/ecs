﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    //public delegate string ClickHandler(object sender, IData data);

    public partial class BaseFormHaveGuidPanel : Company.Interface.BaseForm
    {
        public BaseFormHaveGuidPanel()
        {
            InitializeComponent();
            //minhnd set font size 04/10/2014
            float sizefont = Convert.ToSingle(GlobalSettings.fontsize);
            if (sizefont == 0.0)
                txtGuide.Font = new System.Drawing.Font("Tahoma", 10);
            else
                txtGuide.Font = new System.Drawing.Font("Tahoma", sizefont);
                    
        }

        public XmlDocument docGuide;

        /// <summary>
        /// Hien thi Guid theo control nhap lieu duoc chon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowGuide(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Control ctrl = sender as Control;

                if (ctrl.GetType().BaseType == typeof(TextBoxBase)
                    || ctrl.GetType().BaseType == typeof(Janus.Windows.GridEX.EditControls.EditBase)
                    || ctrl.GetType().BaseType == typeof(Janus.Windows.GridEX.EditControls.ComboBase)
                    || ctrl.GetType().BaseType == typeof(Janus.Windows.GridEX.EditControls.UpDownBase)
                    || ctrl.GetType().BaseType == typeof(DevExpress.XtraEditors.BaseEdit)
                    || ctrl.GetType().BaseType == typeof(DevExpress.XtraEditors.PopupBaseAutoSearchEdit)
                    || ctrl.GetType().BaseType == typeof(DevExpress.XtraEditors.LookUpEditBase)
                    || ctrl.GetType() == typeof(Janus.Windows.CalendarCombo.CalendarCombo)
                    || ctrl.GetType().BaseType == typeof(DevExpress.XtraEditors.GridLookUpEditBase)
                    || ctrl.GetType() == typeof(Janus.Windows.EditControls.UIComboBox)
                    )
                {
              
                    txtGuide.Text = Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuide(docGuide, ctrl.Tag != null ? ctrl.Tag.ToString() : "");
                    //minhnd Chuẩn hóa Guide 04/10/2014
                    /*if (ctrl.Tag != null)
                    {
                        string text = txtGuide.Text;
                        text = splitString(text, '.');
                        text = splitString(text, ':');
                        txtGuide.Text = text;
                    }
                     */
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        /// <summary>
        /// minhnd Hàm tách chuỗi 04/10/2014
        /// </summary>
        /// <param name="str">Chuỗi</param>
        /// <param name="separator">Ký tự tách</param>
        /// <returns></returns>
        private string splitString(string str, char separator) 
        {
            string[] words = str.Split(separator);
            str = "";
            int stt = 1;
            foreach (var word in words)
            {

                if (stt < words.Length)
                    str += word + separator + "\r\n";
                else
                    str += word;

                stt++;
            }
            return str;
        }

        /// <summary>
        /// Thiet lap su kien Enter tren cac control de hien thi thong tin Guide theo control ID
        /// </summary>
        /// <param name="ctrl"></param>
        protected void SetHandler(Control ctrl)
        {
            foreach (Control cItem in ctrl.Controls)
            {
                cItem.Enter += new EventHandler(ShowGuide);

                if (cItem.Controls.Count > 0)
                    SetHandler(cItem);
            }
        }

        protected void SetTextChanged_Handler(object sender, EventArgs e)
        {
            Control ctrl = sender as Control;
            if (ctrl.GetType().BaseType == typeof(TextBoxBase)
                    || ctrl.GetType().BaseType == typeof(Janus.Windows.GridEX.EditControls.EditBase)
                    || ctrl.GetType().BaseType == typeof(DevExpress.XtraEditors.BaseEdit))
            {
                ctrl.Text = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2ASCII_New(ctrl.Text);
            }
        }
    }
}
