﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.GC;
using System.Globalization;
using Company.KDT.SHARE.Components;


namespace Company.Interface.KDT.GC.PCTFormTemplate
{
    public partial class FormDieuChinhDVTNPL : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();             
        private string dvt = string.Empty;
        public bool boolFlag;
        public FormDieuChinhDVTNPL()
        {
            InitializeComponent();
        }
    

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {           
            NguyenPhuLieuRegistedForm f = new NguyenPhuLieuRegistedForm();
            f.NguyenPhuLieuSelected.HopDong_ID = this.HD.ID;
            f.isBrower = true;
            f.isDisplayAll = 2;
            f.khaiPKN06 = true;
            f.ShowDialog();
            if (f.NguyenPhuLieuSelected != null && f.NguyenPhuLieuSelected.Ma != "")
            {
                txtMaNPL.Text = f.NguyenPhuLieuSelected.Ma;
                txtTenNPL.Text = f.NguyenPhuLieuSelected.Ten;
                txtDonViTinhCu.Text = DonViTinh_GetName(f.NguyenPhuLieuSelected.DVT_ID);
                txtMaHSNPL.Text = f.NguyenPhuLieuSelected.MaHS;
                txtSoLuongCu.Text = f.NguyenPhuLieuSelected.SoLuongDangKy.ToString();
                dvt = f.NguyenPhuLieuSelected.DVT_ID;
                cbDonViTinh.Focus();
            }            
            
        }
        private bool CheckNPLExit(string maNPL)
        {
            foreach(HangPhuKien pk1 in LoaiPK.HPKCollection)
            {
                if (pk1.MaHang.Trim() == maNPL.Trim())
                {
                    return true;
                }
            }
            return false; ;
        }

        private void setErro()
        {
            //error.Clear();
            error.SetError(txtMaNPL , null);
            error.SetError(txtTenNPL , null);
            error.SetError(txtMaHSNPL , null);
            error.SetError(txtSoLuongCu , null);
            //error.SetError(txtTGNT, null);

        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                txtMaNPL.Focus();
                cvError.Validate();
                if (!cvError.IsValid) return;
                if (txtDonViTinhCu.Text.Trim() == cbDonViTinh.Text.Trim())
                {
                    showMsg("MSG_240216");
                    //ShowMessage("Phải chọn đơn vị tính khác với đơn vị tính ban đầu.", false);
                    return;
                }
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = this.HD.ID;
                npl.Ma = txtMaNPL.Text.Trim();
                if (!npl.Load())
                {
                    showMsg("MSG_240217");
                    //ShowMessage("Nguyên phụ liệu này không có trong hệ thống.", false);
                    return;
                }
                LoaiPK.HPKCollection.Remove(HangPK);
                if (CheckNPLExit(txtMaNPL.Text.Trim()))
                {
                    showMsg("MSG_240218");
                    //ShowMessage("Đã điều chỉnh nguyên phụ liệu này rồi", false);
                    if (HangPK.MaHang.Trim() != "")
                        LoaiPK.HPKCollection.Add(HangPK);
                    return;
                }
                HangPK.MaHang = txtMaNPL.Text.Trim();
                HangPK.TenHang = txtTenNPL.Text.Trim();
                HangPK.SoLuong = Convert.ToDecimal(txtSoLuongCu.Text);
                HangPK.MaHS = txtMaHSNPL.Text.Trim();
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                HangPK.ThongTinCu = txtDonViTinhCu.Text.Trim();                              
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                if (boolFlag == true)
                {
                    LoaiPK.InsertUpdateDieuChinhDVTNPLDaDangKy (pkdk.HopDong_ID);
                    boolFlag = false ;
                }
                else 
                {
                    LoaiPK.InsertUpdateDieuChinhDVTNPL(pkdk.HopDong_ID);
                }
               
                //PhuKienGCSendForm.isEdit = true;
                reset();
                this.setErro();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
       }
        private void reset()
        {
            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtDonViTinhCu.Text = "";
            txtMaHSNPL.Text = "";
            txtDonViTinhCu.Text = "";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3); 

            txtSoLuongCu.Text = "0";
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            HangPK = new HangPhuKien();
        }
      
        private void DieuChinhSoLuongNPLForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            txtSoLuongCu.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;

            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";           
            
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "N06")
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                LoaiPK.LoadCollection();
                btnXoa.Enabled = false;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                btnXoa.Enabled = true ;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                btnXoa.Enabled = false;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            //Registered
            if (boolFlag == true)
            {
                btnUpdate.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }

            LoaiPK.MaPhuKien = "N06";
            dgList.DataSource = LoaiPK.HPKCollection;
            txtMaNPL.Focus();
        }
    

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien,pkdk.HopDong_ID);
                            //pkDelete.DeleteTransaction(null);
                        }
                       
                    }
                }

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            HangPK = (HangPhuKien)e.Row.DataRow;
            txtMaNPL.Text = HangPK.MaHang.Trim();
            txtTenNPL.Text = HangPK.TenHang.Trim();
            txtMaHSNPL.Text = HangPK.MaHS.Trim();
            txtSoLuongCu.Text = HangPK.SoLuong.ToString().Trim();
            txtDonViTinhCu.Text = HangPK.ThongTinCu;
            cbDonViTinh.SelectedValue = HangPK.DVT_ID;   
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            npl.HopDong_ID = this.HD.ID;
            npl.Ma = txtMaNPL.Text.Trim();
            if (npl.Load())
            {
                if (npl.TrangThai ==0)
                {
                    txtMaNPL.Text = npl.Ma.Trim();
                    txtTenNPL.Text = npl.Ten.Trim();
                    txtMaHSNPL.Text = npl.MaHS.Trim();
                    dvt = npl.DVT_ID;
                    txtDonViTinhCu.Text = DonViTinh.GetName(npl.DVT_ID).Trim();
                    txtSoLuongCu.Text = npl.SoLuongDangKy.ToString();
                    cbDonViTinh.Focus();
                }
                else if (npl.TrangThai != 0 && HangPK.ID>0)
                {
                    txtMaNPL.Text = npl.Ma.Trim();
                    txtTenNPL.Text = npl.Ten.Trim();
                    txtMaHSNPL.Text = npl.MaHS.Trim();
                    dvt = npl.DVT_ID;
                    //txtDonViTinhCu.Text = DonViTinh.GetName(npl.DVT_ID).Trim();
                    txtSoLuongCu.Text = npl.SoLuongDangKy.ToString();
                    //
                    error.SetError(txtMaNPL, null);
                    cbDonViTinh.Focus();
                }
                else
                {
                    error.SetError(txtMaNPL,setText( "Nguyên phụ liệu đang được điều chỉnh nhưng chưa được hải quan duyệt nên không thể điều chỉnh đơn vị tính.","This raw material have been updated and is pending approval so can not update now"));
                    txtMaNPL.Focus();
                    return;
                }
            }
            else
            {
                error.SetError(txtMaNPL,setText( "Không tồn tại nguyên phụ liệu này.","This value is not exist"));
                txtMaNPL.Focus();
                return;
            }
        }

        private void FormDieuChinhDVTNPL_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (LoaiPK.HPKCollection.Count > 0)
            {
                LoaiPK.MaPhuKien = "N06";
                LoaiPK.NoiDung = LoaiPhuKien_GetName("N06");
                pkdk.PKCollection.Remove(LoaiPK);
                pkdk.PKCollection.Add(LoaiPK);
            }
            else
            {
                pkdk.PKCollection.Remove(LoaiPK);
                LoaiPK.Delete(pkdk.HopDong_ID);;
                //LoaiPK.DeleteTransaction(null);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            HangPhuKienCollection hpkColl = new HangPhuKienCollection();
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            {                
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        try
                        {
                            if (pkDelete.ID > 0)
                            {
                                pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                                //pkDelete.DeleteTransaction(null);
                            }
                            hpkColl.Add(pkDelete);
                            //LoaiPK.HPKCollection.Remove(pkDelete);
                        }
                        catch { }
                       
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.HangPhuKien hpkk in hpkColl)
                {
                    try
                    {
                        LoaiPK.HPKCollection.Remove(hpkk);
                    }
                    catch { }

                }
                dgList.DataSource = LoaiPK.HPKCollection;
                this.setErro();
                try 
                { 
                   
                    dgList.Refetch();
                }
                catch { dgList.Refresh(); }
            }
        }
       
    }
}