﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Feedback.Components;

namespace Company.Interface.Feedback
{
    public partial class CauHoiUserControl : UserControl
    {
        private DataTable dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["ThuVienCauHoi"];
        public int ID = 0;
        public CauHoiUserControl()
        {
            InitializeComponent();
        }

        public void DataBinding(int LinhVucID)
        {
            try
            {
                dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["ThuVienCauHoi"];

                if (dt == null || dt.Rows.Count==0)
                    return;
                if (LinhVucID == 0)
                {
                    dgList.DataSource = dt;
                }
                else
                {
                    DataView dv = dt.Copy().DefaultView;
                    dv.RowFilter = "HienThi = 1 and LinhVuc_ID = " + LinhVucID + " ";
                    dv.Sort = "NgayTao asc";
                    dgList.DataSource = dv.ToTable();
                }
                dgList.Refresh();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw ex; }
        }

        public DataRow CurrentRow()
        {
            Janus.Windows.GridEX.GridEXRow[] rows = dgList.GetRows();

            foreach (Janus.Windows.GridEX.GridEXRow item in rows)
            {
                if (item.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    if (item.Selected)
                    {
                        return item.DataRow.GetType() == typeof(System.Data.DataRowView) ? ((System.Data.DataRowView)item.DataRow).Row : (System.Data.DataRow)item.DataRow;
                    }
                }
            }

            return null;
        }

        public delegate void CauHoiSelectionChangedEventHandler(object sender, EventArgs e);
        public event CauHoiSelectionChangedEventHandler CauHoiSelectionChanged;
        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            if (CauHoiSelectionChanged != null)
            {
                CauHoiSelectionChanged(sender, e);
            }
        }
        
        public void CauHoiUserControl_Load(object sender, EventArgs e)
        {
            try
            {
                dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["ThuVienCauHoi"];

                if (dt == null || dt.Rows.Count == 0)
                    return;
                dgList.DataSource = dt;
               // dgList.Refresh();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw ex; }
            
        }

        public void dgList_DoubleClick(object sender, EventArgs e)
        {
            //int maCauHoi = Convert.ToInt32(dgList.CurrentRow.Cells["MaCauHoi"].Text);
            //CauHoiForm frm = new CauHoiForm();
            //frm.MaCauHoi = maCauHoi;
            //frm.ShowDialog();
            //CauHoiUserControl_Load(null, null);
        }

        

    }
}
