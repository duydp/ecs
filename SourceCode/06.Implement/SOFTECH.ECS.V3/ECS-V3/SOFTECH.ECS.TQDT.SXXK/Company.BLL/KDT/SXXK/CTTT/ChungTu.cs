﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.BLL.KDT.SXXK.CTTT
{
    public partial class ChungTu
    {
        public List<ChungTuChiTiet> ChungTuChiTietCollections=new List<ChungTuChiTiet>();
        public List<ChiPhiKhac> ChiPhiKhacCollections = new List<ChiPhiKhac>();
        public void LoadChungTuChiTiet()
        {
            ChungTuChiTietCollections = (List<ChungTuChiTiet>)ChungTuChiTiet.SelectCollectionBy_IDCT(this.ID);
        }
        public void LoadChiPhiKhac()
        {
            ChiPhiKhacCollections = (List<ChiPhiKhac>)ChiPhiKhac.SelectCollectionBy_IDCT(this.ID);
        }

    }
}
