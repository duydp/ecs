﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public partial class ucBase : DevExpress.XtraEditors.XtraUserControl
    {
        protected static List<VNACC_Category_Common> dataGlobals = null;
        protected static List<VNACC_Category_Station> dataStationGlobals = null;
        protected static List<VNACC_Category_QuantityUnit> dataQuantityUnitGlobals = null;
        protected static List<VNACC_Category_TaxClassificationCode> dataTaxClassificationCodeGlobals = null;
        protected static List<VNACC_Category_TransportMean> dataTransportMeanGlobals = null;
        protected static List<VNACC_Category_Berth> dataBerthGlobals = null;
        protected static List<VNACC_Category_PackagesUnit> dataPackagesUnitGlobals = null;
        protected static List<VNACC_Category_OGAUser> dataOGAUserGlobals = null;
        protected static List<VNACC_Category_Nation> dataNationGlobals = null;
        protected static List<VNACC_Category_HSCode> dataHSCodeGlobals = null;
        protected static List<VNACC_Category_CustomsSubSection> dataCustomsSubSectionGlobals = null;
        protected static List<VNACC_Category_CustomsOffice> dataCustomsOfficeGlobals = null;
        protected static List<VNACC_Category_CurrencyExchange> dataCurrencyExchangeGlobals = null;
        protected static List<VNACC_Category_ContainerSize> dataContainerSizeGlobals = null;
        protected static List<VNACC_Category_CityUNLOCODE> dataCityGlobals = null;
        protected static List<VNACC_Category_CityUNLOCODE> dataCityGlobals2 = null; // Mã cửa khẩu của Việt Nam
        protected static List<VNACC_Category_Cargo> dataCargoGlobals = null;
        protected static List<VNACC_Category_BorderGate> dataBorderGateGlobals = null;
        protected static List<VNACC_Category_ApplicationProcedureType> dataApplicationProcedureTypeGlobals = null;

        protected static List<VNACC_Category_Common> datas = null;
        protected static List<VNACC_Category_Station> dataStations = null;
        protected static List<VNACC_Category_QuantityUnit> dataQuantityUnits = null;
        protected static List<VNACC_Category_TaxClassificationCode> dataTaxClassificationCodes = null;
        protected static List<VNACC_Category_TransportMean> dataTransportMeans = null;
        protected static List<VNACC_Category_Berth> dataBerth = null;
        protected static List<VNACC_Category_PackagesUnit> dataPackagesUnits = null;
        protected static List<VNACC_Category_OGAUser> dataOGAUsers = null;
        protected static List<VNACC_Category_Nation> dataNations = null;
        protected static List<VNACC_Category_HSCode> dataHSCodes = null;
        protected static List<VNACC_Category_HSCode> dataHSCodes4 = null;
        protected static List<VNACC_Category_CustomsSubSection> dataCustomsSubSections = null;
        protected static List<VNACC_Category_CustomsOffice> dataCustomsOffices = null;
        protected static List<VNACC_Category_CurrencyExchange> dataCurrencyExchanges = null;
        protected static List<VNACC_Category_ContainerSize> dataContainerSizes = null;
        protected static List<VNACC_Category_CityUNLOCODE> dataCitys = null;
        protected static List<VNACC_Category_Cargo> dataCargos = null;
        protected static List<VNACC_Category_BorderGate> dataBorderGates = null;
        protected static List<VNACC_Category_ApplicationProcedureType> dataApplicationProcedureTypes = null;


        protected object TagControl
        {
            get { return this.Tag; }
            set { this.Tag = value; }
        }

        private string _whereCondition = "";
        public string WhereCondition
        {
            get { return _whereCondition; }
            set { _whereCondition = value; }
        }

        public ucBase()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Khoi tao du lieu cac danh muc VNACCS
        /// </summary>
        /// <param name="isRefresh"></param>
        public static void InitialData(object isRefresh)
        {
            try
            {
                bool isrefress = Convert.ToBoolean(isRefresh);
                if (isrefress)
                {
                    ReloadData();
                    return;
                }

                if (dataGlobals == null)
                    dataGlobals = VNACC_Category_Common.SelectCollectionAllMinimize();

                if (dataStationGlobals == null)
                    dataStationGlobals = VNACC_Category_Station.SelectCollectionAllMinimize();

                if (dataQuantityUnitGlobals == null)
                    dataQuantityUnitGlobals = VNACC_Category_QuantityUnit.SelectCollectionAllMinimize();

                if (dataTaxClassificationCodeGlobals == null)
                    dataTaxClassificationCodeGlobals = VNACC_Category_TaxClassificationCode.SelectCollectionAllMinimize();

                if (dataTransportMeanGlobals == null)
                    dataTransportMeanGlobals = VNACC_Category_TransportMean.SelectCollectionAllMinimize();

                if (dataBerthGlobals == null)
                    dataBerthGlobals = VNACC_Category_Berth.SelectCollectionAllMinimize();

                if (dataPackagesUnitGlobals == null)
                    dataPackagesUnitGlobals = VNACC_Category_PackagesUnit.SelectCollectionAllMinimize();

                if (dataOGAUserGlobals == null)
                    dataOGAUserGlobals = VNACC_Category_OGAUser.SelectCollectionAllMinimize();

                if (dataNationGlobals == null)
                    dataNationGlobals = VNACC_Category_Nation.SelectCollectionAllMinimize();

                if (dataHSCodeGlobals == null)
                    dataHSCodeGlobals = VNACC_Category_HSCode.SelectCollectionAllMinimize();
              

                if (dataCustomsSubSectionGlobals == null)
                    dataCustomsSubSectionGlobals = VNACC_Category_CustomsSubSection.SelectCollectionAllMinimize();

                if (dataCustomsOfficeGlobals == null)
                    dataCustomsOfficeGlobals = VNACC_Category_CustomsOffice.SelectCollectionAllMinimize();

                if (dataCurrencyExchangeGlobals == null)
                    dataCurrencyExchangeGlobals = VNACC_Category_CurrencyExchange.SelectCollectionAllMinimize();

                if (dataContainerSizeGlobals == null)
                    dataContainerSizeGlobals = VNACC_Category_ContainerSize.SelectCollectionAllMinimize();

                if (dataCityGlobals == null)
                    dataCityGlobals = VNACC_Category_CityUNLOCODE.SelectCollectionAllMinimize2("");

                if (dataCityGlobals2 == null)
                    dataCityGlobals2 = VNACC_Category_CityUNLOCODE.SelectForImporterExporter();

                if (dataCargoGlobals == null)
                    dataCargoGlobals = VNACC_Category_Cargo.SelectCollectionAllMinimize();

                if (dataBorderGateGlobals == null)
                    dataBorderGateGlobals = VNACC_Category_BorderGate.SelectCollectionAllMinimize();

                if (dataApplicationProcedureTypeGlobals == null)
                    dataApplicationProcedureTypeGlobals = VNACC_Category_ApplicationProcedureType.SelectCollectionAllMinimize();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private static void ReloadData()
        {
            try
            {
                dataGlobals = VNACC_Category_Common.SelectCollectionAll();
                dataStationGlobals = VNACC_Category_Station.SelectCollectionAll();
                dataQuantityUnitGlobals = VNACC_Category_QuantityUnit.SelectCollectionAll();
                dataTaxClassificationCodeGlobals = VNACC_Category_TaxClassificationCode.SelectCollectionAll();
                dataTransportMeanGlobals = VNACC_Category_TransportMean.SelectCollectionAll();
                dataBerthGlobals = VNACC_Category_Berth.SelectCollectionAll();
                dataPackagesUnitGlobals = VNACC_Category_PackagesUnit.SelectCollectionAll();
                dataOGAUserGlobals = VNACC_Category_OGAUser.SelectCollectionAll();
                dataNationGlobals = VNACC_Category_Nation.SelectCollectionAll();
                dataHSCodeGlobals = VNACC_Category_HSCode.SelectCollectionAll();
                dataCustomsSubSectionGlobals = VNACC_Category_CustomsSubSection.SelectCollectionAll();
                dataCustomsOfficeGlobals = VNACC_Category_CustomsOffice.SelectCollectionAll();
                dataCurrencyExchangeGlobals = VNACC_Category_CurrencyExchange.SelectCollectionAll();
                dataContainerSizeGlobals = VNACC_Category_ContainerSize.SelectCollectionAll();
                dataCityGlobals = VNACC_Category_CityUNLOCODE.SelectCollectionAll();
                dataCargoGlobals = VNACC_Category_Cargo.SelectCollectionAll();
                dataBorderGateGlobals = VNACC_Category_BorderGate.SelectCollectionAll();
                dataApplicationProcedureTypeGlobals = VNACC_Category_ApplicationProcedureType.SelectCollectionAll();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

    }
}
