GO
ALTER TABLE [dbo].[t_KDT_Messages] DROP CONSTRAINT [PK_t_KDT_ThongDiep]
GO
ALTER TABLE [dbo].[t_KDT_Messages] 
ALTER COLUMN [ID] BIGINT NOT NULL
GO
ALTER TABLE [dbo].[t_KDT_Messages] ADD CONSTRAINT [PK_t_KDT_ThongDiep] PRIMARY KEY CLUSTERED ([ID]) ON [PRIMARY]
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[t_View_LuongNPLTrongTKXuat]') AND TYPE IN (N'P',N'PC',N'V'))
DROP VIEW [dbo].[t_View_LuongNPLTrongTKXuat] 
GO
CREATE VIEW [dbo].[t_View_LuongNPLTrongTKXuat]  
AS  
    SELECT  CASE WHEN t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%V%'  
                 THEN t_KDT_ToKhaiMauDich.LoaiVanDon  
                 ELSE CONVERT(VARCHAR(12), t_KDT_ToKhaiMauDich.SoToKhai)  
            END AS SoToKhaiVNACCS ,  
            dbo.t_KDT_ToKhaiMauDich.MaHaiQuan ,  
            dbo.t_KDT_ToKhaiMauDich.SoToKhai ,  
            dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh ,  
            dbo.t_KDT_HangMauDich.MaPhu AS MaSP ,  
            dbo.t_KDT_HangMauDich.TenHang ,  
		    dbo.t_KDT_HangMauDich.DVT_ID,  
            dbo.t_KDT_HangMauDich.SoLuong ,  
            dbo.t_GC_DinhMuc.MaNguyenPhuLieu ,  
            dbo.t_GC_NguyenPhuLieu.Ten ,  
            dbo.t_KDT_HangMauDich.SoLuong * ( dbo.t_GC_DinhMuc.DinhMucSuDung  
                                              / 100  
                                              * dbo.t_GC_DinhMuc.TyLeHaoHut  
                                              + dbo.t_GC_DinhMuc.DinhMucSuDung ) AS LuongNPL ,  
			dbo.t_KDT_HangMauDich.TriGiaTT,  
            dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep ,  
            dbo.t_KDT_ToKhaiMauDich.IDHopDong ,  
            dbo.t_KDT_ToKhaiMauDich.ID ,  
            dbo.t_GC_DinhMuc.DinhMucSuDung ,  
            dbo.t_GC_DinhMuc.TyLeHaoHut ,  
            YEAR(dbo.t_KDT_ToKhaiMauDich.NgayDangKy) AS NamDK  
    FROM    dbo.t_KDT_ToKhaiMauDich  
            INNER JOIN dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID  
            INNER JOIN dbo.t_GC_DinhMuc ON dbo.t_GC_DinhMuc.MaSanPham = dbo.t_KDT_HangMauDich.MaPhu  
                                           AND dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_KDT_ToKhaiMauDich.IDHopDong  
            INNER JOIN dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID  
                                                 AND dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma  
    WHERE   ( dbo.t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' ); 

GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('p_SelectToTalNPLNhapBy_HD_ID') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE p_SelectToTalNPLNhapBy_HD_ID
GO
CREATE PROCEDURE p_SelectToTalNPLNhapBy_HD_ID        
 @HopDong_ID BIGINT,        
 @NamQuyetToan INT        
 AS   
 BEGIN
 SELECT
MaNPL,
SUM(LUONGNHAP) AS LUONGNHAP,
SUM(TRIGIANHAP) AS TRIGIANHAP
FROM
(  
SELECT MaNPL ,  
 T.TenNPL,  
 T.DVT_ID AS DVT,  
SUM(LuongNhap) AS LUONGNHAP,  
SUM(TriGiaNhap) AS TRIGIANHAP  
FROM  
(  
SELECT TABLETEMP.MaPhu AS MaNPL,  
    TABLETEMP.TenHang AS TenNPL,  
    TABLETEMP.DVT_ID,  
 SUM(TABLETEMP.SoLuong) AS LuongNhap ,  
 SUM(TABLETEMP.TriGiaTT) AS TriGiaNhap  
FROM  
(  
SELECT  tkmd.ID ,  
        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon  
             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)  
        END AS SoToKhaiVNACCS ,  
        tkmd.SoToKhai ,  
        tkmd.SoTiepNhan ,  
        tkmd.MaLoaiHinh ,  
        tkmd.NgayDangKy ,  
        tkmd.NgayTiepNhan ,  
  tkmd.IDHopDong,  
        hmd.MaPhu ,  
  hmd.TenHang,  
  hmd.DVT_ID,  
        hmd.SoLuong,  
  hmd.TriGiaTT  
FROM    t_KDT_ToKhaiMauDich tkmd  
        INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID  
WHERE   --hmd.MaPhu = 'NLC001'  
        tkmd.MaLoaiHinh LIKE 'N%'  
        AND tkmd.LoaiHangHoa = 'N'  
        AND tkmd.IDHopDong = @HopDong_ID  
        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )  
  AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan  
  ) TABLETEMP GROUP BY TABLETEMP.MaPhu,TABLETEMP.TenHang,TABLETEMP.DVT_ID --ORDER BY TABLETEMP.MaPhu  
  UNION ALL  
    SELECT TABLETEMP.MaPhu AS MaNPL,  
        TABLETEMP.TenHang AS TenNPL,  
    TABLETEMP.ID_DVT,  
 SUM(TABLETEMP.SoLuong) AS LuongNhap ,  
 SUM(TABLETEMP.TriGiaTT) AS TriGiaNhap  
FROM  
(  
                SELECT  tkmd.ID ,  
                        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%'  
                             THEN tkmd.Huongdan_PL  
                             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)  
                        END AS SoToKhaiVNACCS ,  
                        tkmd.SoToKhai ,  
                        tkmd.SoTiepNhan ,  
                        tkmd.MaLoaiHinh ,  
                        tkmd.NgayDangKy ,  
                        tkmd.NgayTiepNhan ,  
      tkmd.IDHopDong,  
                        hmd.MaHang AS MaPhu ,  
      hmd.TenHang,  
      hmd.ID_DVT,  
                        hmd.SoLuong,  
      hmd.TriGiaTT  
                FROM    t_KDT_GC_ToKhaiChuyenTiep tkmd  
                        INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID  
                WHERE     
                        ( tkmd.MaLoaiHinh LIKE '%PL%'  
                              OR tkmd.MaLoaiHinh LIKE '%18%'  
                              OR tkmd.MaLoaiHinh LIKE '%NV%'  
                            )  
                        AND tkmd.IDHopDong = @HopDong_ID  
                        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )  
      AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan  
      ) TABLETEMP GROUP BY TABLETEMP.MaPhu,TABLETEMP.TenHang,TABLETEMP.ID_DVT --ORDER BY TABLETEMP.MaPhu  
) T GROUP BY MaNPL,T.TenNPL,T.DVT_ID   
) TB GROUP BY TB.MaNPL ORDER BY TB.MaNPL    
 END  


GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('p_GetToTalNPLNhapBy_HD_ID') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE p_GetToTalNPLNhapBy_HD_ID
GO
 CREATE PROCEDURE p_GetToTalNPLNhapBy_HD_ID
@HopDong_ID BIGINT ,
@NamQuyetToan INT 
 AS
 BEGIN
     SELECT  tkmd.ID ,
        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon
             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)
        END AS SoToKhaiVNACCS ,
        tkmd.SoToKhai ,
        tkmd.SoTiepNhan ,
        tkmd.MaLoaiHinh ,
        tkmd.NgayDangKy ,
        tkmd.NgayTiepNhan ,
		tkmd.IDHopDong,
        hmd.MaPhu ,
		hmd.TenHang,
		hmd.DVT_ID,
        hmd.SoLuong,
		hmd.TriGiaTT
FROM    t_KDT_ToKhaiMauDich tkmd
        INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID
WHERE   tkmd.MaLoaiHinh LIKE 'N%'
        AND tkmd.LoaiHangHoa = 'N'
        AND tkmd.IDHopDong = @HopDong_ID
        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )
		AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan
		UNION ALL
 SELECT  tkmd.ID ,
                        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%'
                             THEN tkmd.Huongdan_PL
                             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)
                        END AS SoToKhaiVNACCS ,
                        tkmd.SoToKhai ,
                        tkmd.SoTiepNhan ,
                        tkmd.MaLoaiHinh ,
                        tkmd.NgayDangKy ,
                        tkmd.NgayTiepNhan ,
						tkmd.IDHopDong,
                        hmd.MaHang AS MaPhu ,
						hmd.TenHang,
						hmd.ID_DVT,
                        hmd.SoLuong,
						hmd.TriGiaTT
                FROM    t_KDT_GC_ToKhaiChuyenTiep tkmd
                        INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID
                WHERE   
                        ( tkmd.MaLoaiHinh LIKE '%PL%'
                              OR tkmd.MaLoaiHinh LIKE '%18%'
                              OR tkmd.MaLoaiHinh LIKE '%NV%'
                            )
                        AND tkmd.IDHopDong = @HopDong_ID
                        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )
						AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan ORDER BY hmd.MaPhu

 END

GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('p_GetToTalNPLXuatBy_HD_ID') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE p_GetToTalNPLXuatBy_HD_ID
GO
 CREATE PROCEDURE p_GetToTalNPLXuatBy_HD_ID
@HopDong_ID BIGINT ,
@NamQuyetToan INT 
AS
BEGIN
   	 SELECT  
                tkmd.SoToKhai ,
                CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon
                     ELSE CONVERT(DECIMAL(12), tkmd.SoToKhai)
                END AS SoToKhaiVNACCS ,
                tkmd.MaLoaiHinh ,
                hmd.MaPhu ,
				hmd.TenHang,
				hmd.DVT_ID,
                hmd.SoLuong,
				hmd.TriGiaTT,
				YEAR(tkmd.NgayDangKy) AS NamDK
        FROM    t_KDT_ToKhaiMauDich tkmd
                INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID
        WHERE   
                tkmd.MaLoaiHinh LIKE '%X%'
                AND tkmd.LoaiHangHoa = 'N'
                AND tkmd.IDHopDong = @HopDong_ID
                AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )
				AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan
				UNION ALL

                SELECT  
				        tkmd.SoToKhai ,
                        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%'
                             THEN tkmd.Huongdan_PL
                             ELSE CONVERT(DECIMAL(12), tkmd.SoToKhai)
                        END AS SoToKhaiVNACCS ,
                        tkmd.MaLoaiHinh ,
                        hmd.MaHang AS MaPhu ,
						hmd.TenHang,
						hmd.ID_DVT,
                        hmd.SoLuong,
						hmd.TriGiaTT,
						YEAR(tkmd.NgayDangKy) AS NamDK
                FROM    t_KDT_GC_ToKhaiChuyenTiep tkmd
                        INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID
                WHERE   
                        ( tkmd.MaLoaiHinh LIKE '%PL%'
                              OR tkmd.MaLoaiHinh LIKE '%18%'
                              OR tkmd.MaLoaiHinh LIKE '%XV%'
                            )
                        AND tkmd.IDHopDong = @HopDong_ID
                        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 ) 
						AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan

						UNION ALL
						                         SELECT SoToKhai ,
												 SoToKhaiVNACCS,
												 MaLoaiHinh,
                                                        MaNguyenPhuLieu AS MaPhu ,
                                                        Ten AS TenHang ,
														DVT_ID,
                                                        LuongNPL AS SoLuong ,
                                                        TriGiaTT ,
                                                        NamDK
                         FROM   t_View_LuongNPLTrongTKXuat
                         WHERE  IDHopDong = @HopDong_ID AND NamDK= @NamQuyetToan ORDER BY hmd.MaPhu 
END
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('p_SelectToTalNPLXuatBy_HD_ID') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE p_SelectToTalNPLXuatBy_HD_ID
GO
CREATE PROCEDURE p_SelectToTalNPLXuatBy_HD_ID          
 @HopDong_ID BIGINT,          
 @NamQuyetToan INT          
 AS     
 BEGIN
 SELECT 
MaNPL,
SUM(TB.LUONGXUAT) AS LUONGXUAT,
SUM(TB.TRIGIAXUAT) AS TRIGIAXUAT
FROM
(    
SELECT MaNPL ,    
T.TenNPL,    
T.DVT,    
SUM(LuongXuat) AS LUONGXUAT,    
SUM(TriGiaXuat) AS TRIGIAXUAT    
FROM    
(    
  SELECT TABLETEMP.MaPhu AS MaNPL,    
  TABLETEMP.TenHang AS TenNPL,    
  TABLETEMP.DVT_ID  AS DVT,    
 SUM(TABLETEMP.SoLuong) AS LuongXuat,    
 SUM(TABLETEMP.TriGiaTT) AS TriGiaXuat    
FROM    
(    
        SELECT  tkmd.ID ,    
                tkmd.SoToKhai ,    
                CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon    
                     ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)    
                END AS SoToKhaiVNACCS ,    
                tkmd.SoTiepNhan ,    
                tkmd.MaLoaiHinh ,    
                tkmd.NgayDangKy ,    
                tkmd.NgayTiepNhan ,    
                hmd.MaPhu ,    
    hmd.TenHang,    
    hmd.DVT_ID,    
                hmd.SoLuong,    
    hmd.TriGiaTT    
        FROM    t_KDT_ToKhaiMauDich tkmd    
                INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID    
        WHERE       
                tkmd.MaLoaiHinh LIKE 'X%'    
                AND tkmd.LoaiHangHoa = 'N'    
                AND tkmd.IDHopDong = @HopDong_ID    
                AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )    
    AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan    
    ) TABLETEMP GROUP BY TABLETEMP.MaPhu,TABLETEMP.TenHang,TABLETEMP.DVT_ID --ORDER BY TABLETEMP.MaPhu    
    UNION ALL    
  SELECT TABLETEMP.MaPhu AS MaNPL,    
  TABLETEMP.TenHang AS TenNPL,    
  TABLETEMP.ID_DVT AS DVT,    
 SUM(TABLETEMP.SoLuong) AS LuongXuat,    
 SUM(TABLETEMP.TriGiaTT) AS TriGiaXuat    
FROM    
(    
                SELECT  tkmd.ID ,    
                        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%'    
                             THEN tkmd.Huongdan_PL    
                             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)    
                        END AS SoToKhaiVNACCS ,    
                        tkmd.SoToKhai ,    
                        tkmd.SoTiepNhan ,    
                        tkmd.MaLoaiHinh ,    
                        tkmd.NgayDangKy ,    
                        tkmd.NgayTiepNhan ,    
                        hmd.MaHang AS MaPhu ,    
      hmd.TenHang,    
      hmd.ID_DVT,    
                        hmd.SoLuong,    
      hmd.TriGiaTT    
                FROM    t_KDT_GC_ToKhaiChuyenTiep tkmd    
                        INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID    
                WHERE       
                        ( tkmd.MaLoaiHinh LIKE '%PL%'    
                              OR tkmd.MaLoaiHinh LIKE '%18%'    
                              OR tkmd.MaLoaiHinh LIKE '%XV%'    
                            )    
                        AND tkmd.IDHopDong = @HopDong_ID    
                        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )    
      AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan    
      ) TABLETEMP GROUP BY TABLETEMP.MaPhu,TABLETEMP.TenHang,TABLETEMP.ID_DVT --ORDER BY TABLETEMP.MaPhu    
      UNION ALL    
  SELECT TABLETEMP.MaNguyenPhuLieu AS MaNPL,    
  TABLETEMP.Ten AS TenNPL,  
  TABLETEMP.DVT,     
 SUM(TABLETEMP.LuongNPL) AS LuongXuat,    
 SUM(TABLETEMP.TriGiaTT) AS TriGiaXuat    
FROM    
(    
                         SELECT SoToKhaiVNACCS ,  
                                MaHaiQuan ,  
                                SoToKhai ,  
                                MaLoaiHinh ,  
                                MaSP ,  
                                TenHang ,  
                                DVT_ID ,  
                                SoLuong ,  
                                MaNguyenPhuLieu ,  
                                Ten ,  
         CASE WHEN Ten IS NOT NULL THEN (SELECT TOP 1 DVT_ID FROM dbo.t_GC_NguyenPhuLieu WHERE Ten=NPL.Ten) ELSE DVT_ID END  AS DVT,   
                                LuongNPL ,  
                                TriGiaTT ,  
                                MaDoanhNghiep ,  
                                IDHopDong ,
                                DinhMucSuDung ,  
                                TyLeHaoHut ,  
                                NamDK  
           
                         FROM   t_View_LuongNPLTrongTKXuat NPL  
                         WHERE  IDHopDong = @HopDong_ID     
       AND NamDK = @NamQuyetToan    
       ) TABLETEMP GROUP BY TABLETEMP.MaNguyenPhuLieu,TABLETEMP.Ten,TABLETEMP.DVT --ORDER BY MaNguyenPhuLieu    
       ) T GROUP BY MaNPL,T.TenNPL,T.DVT         
	   ) TB GROUP BY TB.MaNPL ORDER BY TB.MaNPL
 END 
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('p_GC_NguyenPhuLieuBy_HD_ID') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE p_GC_NguyenPhuLieuBy_HD_ID
GO
 CREATE PROCEDURE p_GC_NguyenPhuLieuBy_HD_ID          
  @HopDong_ID NVARCHAR(1000)          
  AS          
  BEGIN          
  SELECT           
  Ma,          
  Ten,          
  CASE WHEN DVT_ID <> 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID =DVT_ID)ELSE DVT_ID END AS TenDVT           
  FROM dbo.t_GC_NguyenPhuLieu           
  WHERE HopDong_ID IN (@HopDong_ID)                
  END    

GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('p_T_GC_NPL_QUYETOAN_DeleteBy_HD_ID') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE p_T_GC_NPL_QUYETOAN_DeleteBy_HD_ID
GO
 CREATE PROCEDURE p_T_GC_NPL_QUYETOAN_DeleteBy_HD_ID    
 @HOPDONG_ID BIGINT,    
 @NAMQUYETTOAN INT    
 AS    
 DELETE FROM dbo.T_GC_NPL_QUYETOAN WHERE HOPDONG_ID=@HOPDONG_ID AND NAMQUYETTOAN =@NAMQUYETTOAN 



GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('P_GC_NPL_QUYETOAN_UPDATE_TONDAUKY') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE P_GC_NPL_QUYETOAN_UPDATE_TONDAUKY
GO
CREATE PROCEDURE P_GC_NPL_QUYETOAN_UPDATE_TONDAUKY    
@LUONGTONDK NUMERIC(18,6),    
@TRIGIATONDK NUMERIC(18,6),    
@HOPDONG_ID BIGINT,    
@MANPL NVARCHAR(500),    
@NAMQUYETTOAN INT    
AS    
 UPDATE T_GC_NPL_QUYETOAN SET LUONGTONDK =@LUONGTONDK , TRIGIATONDK =@TRIGIATONDK WHERE HOPDONG_ID=@HOPDONG_ID AND MANPL=@MANPL AND NAMQUYETTOAN=@NAMQUYETTOAN 



GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('P_GC_NPL_QUYETOAN_UPDATE_NHAPTRONGKY') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE P_GC_NPL_QUYETOAN_UPDATE_NHAPTRONGKY
GO
CREATE PROCEDURE P_GC_NPL_QUYETOAN_UPDATE_NHAPTRONGKY    
@LUONGNHAPTK NUMERIC(18,6),    
@TRIGIANHAPTK NUMERIC(18,6),    
@HopDong_ID BIGINT,    
@MANPL NVARCHAR(500),    
@NAMQUYETTOAN INT    
AS    
 UPDATE T_GC_NPL_QUYETOAN SET LUONGNHAPTK =@LUONGNHAPTK , TRIGIANHAPTK =@TRIGIANHAPTK WHERE HOPDONG_ID=@HopDong_ID AND MANPL=@MANPL AND NAMQUYETTOAN=@NAMQUYETTOAN    




GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('P_GC_NPL_QUYETOAN_UPDATE_XUATTRONGKY') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE P_GC_NPL_QUYETOAN_UPDATE_XUATTRONGKY
GO
CREATE PROCEDURE P_GC_NPL_QUYETOAN_UPDATE_XUATTRONGKY    
@LUONGXUATTK NUMERIC(18,6),    
@TRIGIAXUATTK NUMERIC(18,6),    
@HOPDONG_ID BIGINT,    
@MANPL NVARCHAR(500),    
@NAMQUYETTOAN INT    
AS    
 UPDATE T_GC_NPL_QUYETOAN SET LUONGXUATTK =@LUONGXUATTK , TRIGIAXUATTK =@TRIGIAXUATTK WHERE HOPDONG_ID=@HOPDONG_ID AND MANPL=@MANPL AND NAMQUYETTOAN=@NAMQUYETTOAN      



GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('P_GC_NPL_QUYETOAN_UPDATE_TONCUOIKY') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE P_GC_NPL_QUYETOAN_UPDATE_TONCUOIKY
GO
 CREATE PROCEDURE P_GC_NPL_QUYETOAN_UPDATE_TONCUOIKY    
@LUONGTONCK NUMERIC(18,6),    
@TRIGIATONCK NUMERIC(18,6),    
@HOPDONG_ID BIGINT,    
@MANPL NVARCHAR(500),    
@NAMQUYETTOAN INT    
AS    
 UPDATE T_GC_NPL_QUYETOAN SET LUONGTONCK =@LUONGTONCK , TRIGIATONCK =@TRIGIATONCK WHERE HOPDONG_ID=@HOPDONG_ID AND MANPL=@MANPL AND NAMQUYETTOAN=@NAMQUYETTOAN 


GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete_By_HD_ID]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete_By_HD_ID]
GO
 ------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete_By_HD_ID]      
-- Database: ECS_GC_V4      
-- Author: Ngo Thanh Tung      
-- Time created: Tuesday, November 1, 2016      
------------------------------------------------------------------------------------------------------------------------      
      
CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete_By_HD_ID]      
 @HOPDONG_ID bigint ,    
  @NAMQUYETTOAN INT     
AS      
      
DELETE FROM       
 [dbo].[T_GC_SANPHAM_QUYETOAN]      
WHERE      
  [HOPDONG_ID] = @HOPDONG_ID      
  AND NAMQUYETTOAN =@NAMQUYETTOAN 




GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('p_T_GC_SANPHAM_SelectSumSPXuatBy_HD_ID') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE p_T_GC_SANPHAM_SelectSumSPXuatBy_HD_ID
GO
  
CREATE PROCEDURE p_T_GC_SANPHAM_SelectSumSPXuatBy_HD_ID      
@HopDong_ID BIGINT,      
@NamQuyetToan INT      
AS      
SELECT DISTINCT    
        dbo.t_KDT_HangMauDich.MaPhu AS MASP,    
  t_KDT_HangMauDich.TenHang AS TENSP,    
  CASE WHEN DVT_ID <> 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID=DVT_ID) ELSE DVT_ID END AS DVT,    
  SUM(dbo.t_KDT_HangMauDich.SoLuong) AS LUONGXUATTK,    
  SUM(dbo.t_KDT_HangMauDich.TriGiaKB) AS TRIGIAXUATTK,    
  SUM(dbo.t_KDT_HangMauDich.SoLuong) AS LUONGNHAPTK,    
  SUM(dbo.t_KDT_HangMauDich.TriGiaKB) AS TRIGIANHAPTK,    
  @HopDong_ID AS HOPDONG_ID,    
  @NamQuyetToan AS NAMQUYETTOAN    
FROM    dbo.t_KDT_ToKhaiMauDich    
        INNER JOIN dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID    
WHERE   IDHopDong = @HopDong_ID    
        AND YEAR(NgayDangKy) = @NamQuyetToan AND LoaiHangHoa='S' GROUP BY MaPhu,TenHang,DVT_ID  ORDER BY MaPhu,TenHang    




GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteBy_HD_ID]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteBy_HD_ID]
GO
 ------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteBy_HD_ID]      
-- Database: ECS_GC_V4      
-- Author: Ngo Thanh Tung      
-- Time created: Tuesday, November 1, 2016      
------------------------------------------------------------------------------------------------------------------------      
      
CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteBy_HD_ID]       
 @HOPDONG_ID bigint ,     
 @NAMQUYETTOAN INT    
AS      
DELETE FROM       
 [dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]      
WHERE      
  [HOPDONG_ID] = @HOPDONG_ID    
  AND NAMQUYETTOAN =@NAMQUYETTOAN    




GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectSPXuatBy_HD_ID') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectSPXuatBy_HD_ID
GO

CREATE PROCEDURE p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectSPXuatBy_HD_ID    
@HopDong_ID BIGINT,    
@NamQuyetToan INT    
AS    
SELECT DISTINCT    
  CASE WHEN MaLoaiHinh LIKE '%V%' THEN ( SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK= t_KDT_ToKhaiMauDich.SoToKhai) ELSE SoToKhai END AS SoTKVNACCS,    
        dbo.t_KDT_HangMauDich.MaPhu,    
  t_KDT_HangMauDich.TenHang,    
  CASE WHEN DVT_ID <> 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID=DVT_ID) ELSE DVT_ID END AS DVT_ID,    
  dbo.t_KDT_HangMauDich.SoLuong AS LUONGXUAT,    
  dbo.t_KDT_HangMauDich.DonGiaKB AS DONGIA,    
  dbo.t_KDT_HangMauDich.TriGiaKB AS TRIGIAXUAT,    
  CASE WHEN SoToKhai <> 0 THEN (SELECT TOP 1 TyGiaTinhThue FROM dbo.t_KDT_VNACC_TK_PhanHoi_TyGia WHERE Master_ID = ( SELECT TOP 1 ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE SoToKhai =(SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK=SoToKhai))) ELSE 0 END AS TYGIATINHTHUE,    
  @HopDong_ID AS HOPDONG_ID,    
  @NamQuyetToan AS NAMQUYETTOAN    
FROM    dbo.t_KDT_ToKhaiMauDich    
        INNER JOIN dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID    
WHERE   IDHopDong = @HopDong_ID    
        AND YEAR(NgayDangKy) = @NamQuyetToan AND LoaiHangHoa='S' ORDER BY MaPhu,TenHang    




GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_GC_NguyenPhuLieu_SelectAll]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectAll]
GO
------------------------------------------------------------------------------------------------------------------------        
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_SelectAll]        
-- Database: ECS_GC        
-- Author: Ngo Thanh Tung        
-- Time created: Tuesday, September 08, 2009        
------------------------------------------------------------------------------------------------------------------------        
        
CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectAll]        
AS        
        
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED        
        
SELECT        
 [HopDong_ID],        
 [Ma],        
 [Ten],        
 [MaHS],        
CASE WHEN DVT_ID <> 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID =DVT_ID) ELSE DVT_ID END AS TenDVT,         
 [SoLuongDangKy],        
 [SoLuongDaNhap],        
 [SoLuongDaDung],        
 [SoLuongCungUng],        
 [TongNhuCau],        
 [STTHang],        
 [TrangThai],        
 [DonGia]        
FROM        
 [dbo].[t_GC_NguyenPhuLieu] 





GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_GC_SanPham_SelectAll]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_GC_SanPham_SelectAll]
GO
 CREATE PROCEDURE [dbo].[p_GC_SanPham_SelectAll]        
AS        
    
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED        
        
SELECT        
 [HopDong_ID],        
 [Ma],        
 [Ten],        
 [MaHS],        
CASE WHEN DVT_ID <> 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID =DVT_ID) ELSE DVT_ID END AS TenDVT,      
 [SoLuongDangKy],        
 [SoLuongDaXuat],        
 [NhomSanPham_ID],        
 [STTHang],        
 [TrangThai],        
 [DonGia]        
FROM        
 [dbo].[t_GC_SanPham]     


GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('p_T_GC_SANPHAM_QUYETOAN_SelectDynamic') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE p_T_GC_SANPHAM_QUYETOAN_SelectDynamic
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectDynamic]  
-- Database: ECS_GC_V4  
-- Author: Ngo Thanh Tung  
-- Time created: Tuesday, November 29, 2016  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectDynamic]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT   
 [MASP],
 CASE WHEN MASP IS NOT NULL THEN (SELECT TOP 1  UPPER(TenSanPham) FROM dbo.t_HaiQuan_DanhMucSanPhamGiaCong WHERE MaSanPham = (SELECT TOP 1 NhomSanPham_ID FROM dbo.t_GC_SanPham WHERE MASP = Ma)) ELSE MASP END AS NHOMSP,  
 [TENSP],  
 [DVT],  
 [LUONGTONDK],  
 [TRIGIATONDK],  
 [LUONGNHAPTK],  
 [TRIGIANHAPTK],  
 [LUONGXUATTK],  
 [TRIGIAXUATTK],  
 [LUONGTONCK],  
 [TRIGIATONCK],  
 [HOPDONG_ID],  
 [NAMQUYETTOAN]  
FROM [dbo].[T_GC_SANPHAM_QUYETOAN]   
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
 





GO



GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Insert]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Insert]
GO
-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Insert]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Insert]
	@TOKHAIXUAT decimal(18, 0),
	@MASP nvarchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@DONGIA numeric(18, 6),
	@TRIGIAXUAT numeric(18, 6),
	@TYGIATINHTHUE int,
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]
(
	[TOKHAIXUAT],
	[MASP],
	[TENSP],
	[DVT],
	[LUONGXUAT],
	[DONGIA],
	[TRIGIAXUAT],
	[TYGIATINHTHUE],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
)
VALUES 
(
	@TOKHAIXUAT,
	@MASP,
	@TENSP,
	@DVT,
	@LUONGXUAT,
	@DONGIA,
	@TRIGIAXUAT,
	@TYGIATINHTHUE,
	@HOPDONG_ID,
	@NAMQUYETTOAN
)

SET @ID = SCOPE_IDENTITY()
GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '24.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('24.7',GETDATE(), N'CẬP NHẬT KHAI BÁO BÁO BÁO CÁO QUYẾT TOÁN')
END