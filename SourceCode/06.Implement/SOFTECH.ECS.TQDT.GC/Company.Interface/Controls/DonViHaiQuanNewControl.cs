using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;
using Company.Interface;

namespace Company.Interface.Controls
{
    public partial class DonViHaiQuanNewControl : UserControl
    {
        public DonViHaiQuanNewControl()
        {
            this.InitializeComponent();
        }

        public bool ReadOnly
        {
            set
            {
                this.txtMa.ReadOnly = value;
                this.txtTen.ReadOnly = value;
            }
            get { return this.txtMa.ReadOnly; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.txtTen.VisualStyleManager = value;
                this.txtMa.VisualStyleManager = value;
            }
            get
            {
                return this.txtMa.VisualStyleManager;
            }
        }
        private string _MaCuc = string.Empty;
        public string MaCuc
        {
            set { this._MaCuc = value; }
            get { return this._MaCuc; }
        }
        public string Ma
        {
            set { txtMa.Text = value; }
            get { return txtMa.Text; }
        }
        public string Ten
        {
            set { txtTen.Text = value; }
            get { return txtTen.Text; }
        }


        private void DonViHaiQuanControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.loadData();
            }
        }

        private void loadData()
        {
            txtMa.Text = GlobalSettings.MA_HAI_QUAN;
            txtTen.Text = GlobalSettings.TEN_HAI_QUAN;
        }

    }
}