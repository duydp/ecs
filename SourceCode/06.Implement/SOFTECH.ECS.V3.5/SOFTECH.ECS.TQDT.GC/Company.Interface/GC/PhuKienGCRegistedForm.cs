﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Data;
using Company.GC.BLL.KDT;
using System.IO;
using Company.KDT.SHARE.Components;

namespace Company.Interface.GC
{
    public partial class PhuKienGCRegistedForm : BaseForm
    {
        PhuKienDangKyCollection collection = new PhuKienDangKyCollection();
        PhuKienDangKy pkDangKy = new PhuKienDangKy();
        //-----------------------------------------------------------------------------------------
        public PhuKienGCRegistedForm()
        {
            InitializeComponent();
        }
        private void BindHopDong()
        {
            DataTable dt;
            {
                string where = string.Format("madoanhnghiep='{0}'", GlobalSettings.MA_DON_VI);
                HopDong HD = new HopDong();
                dt = HopDong.SelectDynamic(where, "").Tables[0];
            }
            cbHopDong.DataSource = dt;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            BindHopDong();
        }

        //-----------------------------------------------------------------------------------------
        private void PhuKienGCManageForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            setDataToComboUserKB();
            cbUserKB.SelectedIndex = 0;
            btnSearch_Click(null, null);
            //dgList.DataSource = this.dmDangKy.DMCollection;
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "PK";
            sendXML.master_id = pkDangKy.ID;
            string xmlCurrent = "";
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //ShowMessage("Phụ kiện đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            pkDangKy.LoadCollection();
            if (pkDangKy.PKCollection.Count == 0)
            {
                showMsg("MSG_2702021");
                //ShowMessage("Bạn chưa nhập thông tin cho loại phụ kiện nên không khai báo tới hải quan được.", false);
                return;
            }
            string password = "";
            try
            {


                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = pkDangKy.WSSend(password);
                this.Cursor = Cursors.Default;
                sendXML.func = 1;
                sendXML.msg = xmlCurrent;
                sendXML.master_id = pkDangKy.ID;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    this.Cursor = Cursors.Default;
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            showMsg("MSG_WRN13", ex.Message);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setcommandstatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Phụ kiện . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //e.Row.Cells[].Text = this.LoaiPhuKien_GetName(e.Row.Cells[1].Text);            
            if (e.Row.RowType == RowType.Record)
            {
                string st = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                if (st == "-1")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                else if (st == "0")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                else if (st == "1")
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                if (e.Row.Cells["NgayTiepNhan"].Value != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }
                if (e.Row.Cells["NgayPhuKien"].Value != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayPhuKien"].Value);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayPhuKien"].Text = "";
                }
                HopDong hd = new HopDong();
                hd.ID = Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value);
                hd = HopDong.Load(hd.ID);

                e.Row.Cells["HopDong_ID"].Text = hd.SoHopDong;
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                Company.Interface.KDT.GC.PhuKienGCSendForm pk = new Company.Interface.KDT.GC.PhuKienGCSendForm();
                pk.HD.ID = Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value);
                pk.HD = HopDong.Load(pk.HD.ID);
                pk.pkdk.ID = Convert.ToInt64(e.Row.Cells["ID"].Value);
                pk.pkdk.Load();
                pk.pkdk.LoadCollection();
                foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien loaiPK in pk.pkdk.PKCollection)
                {
                    loaiPK.Load();
                    loaiPK.LoadCollection();
                }
                pk.ShowDialog();
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            //switch (e.Command.Key)
            //{
            //    case "KhaiBao": send(); break;
            //    case "cmdGet": nhandulieuHD(); break;
            //    case "Huy": HuyNhieuHD(); break;
            //    case "XacNhanThongTin": LaySoTiepNhanDT(); break;
            //}
        }
        private void nhandulieuHD()
        {
            NhanDuLieuCTMenu_Click(null, null);
        }

        private void setDataToComboUserKB()
        {
            DataTable dt = Company.QuanTri.User.SelectAll().Tables[0];
            DataRow dr = dt.NewRow();
            dr["USER_NAME"] = -1;
            dr["HO_TEN"] = "--Tất cả--";
            dt.Rows.InsertAt(dr, 0);
            cbUserKB.DataSource = dt;
            cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            long idHopDong = 0;
            try
            {
                idHopDong = Convert.ToInt64(cbHopDong.Value);
            }
            catch
            {
                showMsg("MSG_240205");
                //ShowMessage("Không có hợp đồng này trong hệ thống.", false);
                return;
            }

            string where = "1=1  and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' ";
            if (txtSoTiepNhan.Text.Trim().Length > 0)
            {
                where += " and sotiepnhan=" + txtSoTiepNhan.Text;
            }
            if (cbHopDong.Text.Length > 0)
            {
                where += " and HopDong_ID=" + idHopDong;
            }

            if (txtNamTiepNhan.Text.Trim().Length > 0)
            {
                where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text;
            }
            if (txtSoPhuKien.Text.Trim().Length > 0)
            {
                where += " and SoPhuKien LIKE '%" + txtSoPhuKien.Text + "%'";
            }
            if (cbUserKB.SelectedValue != null && cbUserKB.SelectedIndex != 0)
            {
                where += " AND ID IN (SELECT ID_DK FROM dbo.t_KDT_SXXK_LogKhaiBao WHERE LoaiKhaiBao = 'PK' AND UserNameKhaiBao = '" + cbUserKB.SelectedItem.Value.ToString() + "')";
            }
            if (ckbTimKiem.Checked)
            {
                DateTime fromDate = clcTuNgay.Value;
                DateTime toDate = ucCalendar1.Value;
                if (toDate.Year <= 1900) toDate = DateTime.Now;
                toDate = toDate.AddDays(1);
                where = where + " AND (ngaytiepnhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
            }
            collection = pkDangKy.SelectCollectionDynamic(where, "");
            dgPhuKien.DataSource = collection;
            setcommandstatus();
        }


        private void khaibaoCTMenu_Click(object sender, EventArgs e)
        {
            string xmlCurrent = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            try
            {
                if (dgPhuKien.GetRow() != null)
                {
                    pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
                    {
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkDangKy.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_240203");
                            //ShowMessage("Phụ kiện này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.", false);
                            return;
                        }
                    }

                    pkDangKy.Load();
                    pkDangKy.LoadCollection();
                    foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien loaiPK in pkDangKy.PKCollection)
                    {
                        loaiPK.Load();
                        loaiPK.LoadCollection();
                    }
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = pkDangKy.WSSend(wsForm.txtMatKhau.Text.Trim());
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "PK";
                    sendXML.master_id = pkDangKy.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 1;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(wsForm.txtMatKhau.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}

                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            showMsg("MSG_WRN13", ex.Message);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setcommandstatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Phụ kiện . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        private void NhanDuLieuCTMenu_Click(object sender, EventArgs e)
        {
            string xmlCurrent = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                if (dgPhuKien.GetRow() != null)
                {
                    pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
                    {
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkDangKy.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_240203");
                            //ShowMessage("Phụ kiện này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.", false);
                            return;
                        }

                        if (GlobalSettings.PassWordDT == "")
                        {
                            wsForm.ShowDialog(this);
                            if (!wsForm.IsReady) return;
                        }
                        if (GlobalSettings.PassWordDT != "")
                            password = GlobalSettings.PassWordDT;
                        else
                            password = wsForm.txtMatKhau.Text.Trim();
                        this.Cursor = Cursors.WaitCursor;
                        xmlCurrent = pkDangKy.WSDownLoad(password);
                        this.Cursor = Cursors.Default;
                        sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkDangKy.ID;
                        sendXML.msg = xmlCurrent;
                        sendXML.func = 2;
                        xmlCurrent = "";
                        sendXML.InsertUpdate();
                        LayPhanHoi(password);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setcommandstatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void HuyCTMenu_Click(object sender, EventArgs e)
        {
            WSForm wsForm = new WSForm();
            string password = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {
                if (dgPhuKien.GetRow() != null)
                {
                    pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;

                    {
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkDangKy.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_240203");
                            //ShowMessage("Phụ kiện này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.", false);
                            return;
                        }
                    }

                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    string xmlCurrent = pkDangKy.WSCancel(password);
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "PK";
                    sendXML.master_id = pkDangKy.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setcommandstatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }
        private void setcommandstatus()
        {

        }

        private void LaySoTiepNhanDT()
        {
            if (dgPhuKien.GetRow() != null)
            {
                pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                {
                    sendXML.LoaiHS = "PK";
                    sendXML.master_id = pkDangKy.ID;
                    if (!sendXML.Load())
                    {
                        showMsg("MSG_240203");
                        //ShowMessage("Phụ kiện không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                        return;
                    }
                }
                string xmlCurrent = "";
                WSForm wsForm = new WSForm();
                string password = "";
                try
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    {
                        xmlCurrent = pkDangKy.LayPhanHoi(password, sendXML.msg);
                    }
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        return;
                    }

                    if (sendXML.func == 1)
                    {
                        //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkDangKy.SoTiepNhan, false);
                        showMsg("MSG_SEN02", pkDangKy.SoTiepNhan);
                    }
                    else if (sendXML.func == 3)
                    {
                        showMsg("MSG_2702017");
                        //ShowMessage("Đã hủy phụ kiện này", false);                      
                    }
                    else if (sendXML.func == 2)
                    {
                        if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            showMsg("MSG_2702018");
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);                            
                        }
                        else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            showMsg("MSG_SEN04");
                            //ShowMessage("Hải quan chưa xử lý !", false);
                        }
                        else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            showMsg("MSG_SEN05");
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);                            
                        }
                    }
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                    this.btnSearch_Click(null, null);
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    HangDoi hd = new HangDoi();
                                //    hd.ID = TKMD.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //    hd.TrangThai = TKMD.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //    hd.PassWord = pass;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                                if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                                {
                                    sendXML.Delete();
                                    setcommandstatus();
                                }
                                else
                                {
                                    GlobalSettings.PassWordDT = "";
                                }
                            }
                        }
                        else
                        {
                            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                showMsg("MSG_WRN13", ex.Message);
                                //ShowMessage("Xảy ra lỗi không xác định.", false);
                                sendXML.Delete();
                                setcommandstatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                                showMsg("MSG_2702004", ex.Message);
                                //ShowMessage(ex.Message, false);
                            }
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }
        private void LayPhanHoi(string pass)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            string xmlCurrent = "";
            try
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkDangKy.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = pkDangKy.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    if (showMsg("MSG_STN02", true) == "Yes")
                    //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }

                if (sendXML.func == 1)
                {
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkDangKy.SoTiepNhan, false);
                    showMsg("MSG_SEN02", pkDangKy.SoTiepNhan);
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702019");
                    //ShowMessage("Đã hủy danh sách phụ kiện này", false);                    
                }
                else if (sendXML.func == 2)
                {
                    if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                        showMsg("MSG_2702018");
                    }
                    else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        //ShowMessage("Hải quan chưa xử lý danh sách sản phẩm này!", false);
                        showMsg("MSG_SEN04");
                    }
                    else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                        showMsg("MSG_SEN05");
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                this.btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setcommandstatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            sendXML.Delete();
                            setcommandstatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgPhuKien_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa không ?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        PhuKienDangKy pkdkDelete = (PhuKienDangKy)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkdkDelete.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_2702012", i.Position + 1);
                            //ShowMessage("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
                        }
                        else
                        {
                            if (pkdkDelete.ID > 0)
                            {
                                pkdkDelete.Delete();
                            }
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa không ?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        PhuKienDangKy pkdkDelete = (PhuKienDangKy)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkdkDelete.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_2702012", i.Position + 1);
                            //ShowMessage("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
                        }
                        else
                        {
                            if (pkdkDelete.ID > 0)
                            {
                                pkdkDelete.Delete();
                            }
                        }
                    }
                }
                long idHopDong = 0;
                try
                {
                    idHopDong = Convert.ToInt64(cbHopDong.Value);
                }
                catch
                {
                    showMsg("MSG_240205");
                    //ShowMessage("Không có hợp đồng này trong hệ thống.", false);
                    return;
                }

                string where = "1=1   and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' ";
                if (txtSoTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and sotiepnhan=" + txtSoTiepNhan.Text;
                }
                if (cbHopDong.Text.Length > 0)
                {
                    where += " and HopDong_ID=" + idHopDong;
                }

                if (txtNamTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text;
                }
                if (txtSoPhuKien.Text.Trim().Length > 0)
                {
                    where += " and SoPhuKien LIKE '%" + txtSoPhuKien.Text + "%'";
                }
                collection = pkDangKy.SelectCollectionDynamic(where, "");
                dgPhuKien.DataSource = collection;
                setcommandstatus();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtNamTiepNhan_Click(object sender, EventArgs e)
        {

        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Danh sách phụ kiện đã đăng ký " + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sf.Filter = "Excel File | *.xls ";

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                grdExport.GridEX = dgPhuKien;

                try
                {
                    System.IO.Stream str = sf.OpenFile();
                    grdExport.Export(str);
                    str.Close();
                    if (ShowMessage("Bạn có muốn mở File này không", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình mở File" + ex.Message, false);
                }
            }
            else
            {
                ShowMessage("Xuất Excel không thành công", false);
            }
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void cbUserKB_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }
    }
}