﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.BLL.SXXK;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface
{
  public partial  class ProcessBackgroundForm
    {
        public event EventHandler<ProcessBackgroundEventArgs> ProcessBackgroundEventArg;
        public DateTime dateFrom;
        public DateTime dateTo;

        public void OnProcess(ProcessBackgroundEventArgs e)
        {
            if (ProcessBackgroundEventArg != null)
            {
                ProcessBackgroundEventArg(this, e);
            }
        }
        public class ProcessBackgroundEventArgs : EventArgs
        {
            public Exception Error { set; get; }
            public string Status { get; set; }
            public int Percent { get; set; }
            public object Data { get; set; }

            public ProcessBackgroundEventArgs(Exception ex, string TrangThai, int per, object data)
            {
                this.Error = ex;
                this.Status = TrangThai;
                this.Percent = per;
                this.Data = data;
            }
            public ProcessBackgroundEventArgs(string TrangThai, int percent)
                : this(null, TrangThai, percent, null)
            {

            }
            public ProcessBackgroundEventArgs(Exception ex, string TrangThai, int percent)
                : this(ex, TrangThai, percent, null)
            {

            }
        }
        public void ProcessTotalExport(string dateFrom, string dateTo)
        {
            try
            {
                this.OnProcess(new ProcessBackgroundEventArgs("Lấy danh sách tất cả các nguyên phụ liệu",2));
                #region Lấy danh sách tất cả các nguyên phụ liệu
                Company.BLL.KDT.SXXK.NguyenPhuLieuCollection NPLCollection = Company.BLL.KDT.SXXK.NguyenPhuLieu.SelectCollectionAll();
                foreach (Company.BLL.KDT.SXXK.NguyenPhuLieu NPL in NPLCollection)
                {
                    SXXK_NguyenPhuLieu_Ton npl = new SXXK_NguyenPhuLieu_Ton();
                    npl.Ma = NPL.Ma;
                    npl.Ten = NPL.Ten;
                    npl.MaHS = NPL.MaHS;
                    npl.DVT_ID = NPL.DVT_ID;
                    npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    npl.LuongNhap = 0;
                    npl.LuongXuat = 0;
                    npl.LuongTon = 0;
                    npl.InsertUpdate();
                }
                this.OnProcess(new ProcessBackgroundEventArgs("Cập nhật nguyên phụ liệu ", 4));
                List<SXXK_NguyenPhuLieu_Ton> NPLTonCollection = SXXK_NguyenPhuLieu_Ton.SelectCollectionAll();
                foreach (SXXK_NguyenPhuLieu_Ton NPL in NPLTonCollection)
                {
                    NPL.LuongNhap = 0;
                    NPL.LuongXuat = 0;
                    NPL.LuongTon = 0;
                }
                #endregion
                #region Lấy danh sách tất cả các tờ khai xuất theo khoảng ngày tháng
                //string dateFrom = dtpFrom.Value.ToString("yyyy-MM-dd 00:00:00");
                //string dateTo = dtpTo.Value.ToString("yyyy-MM-dd 23:59:59");
                List<KDT_VNACC_ToKhaiMauDich> TKMDCollection = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic(" TrangThaiXuLy=3 AND MaLoaiHinh NOT IN ('A42') AND NgayDangKy BETWEEN '" + dateFrom + "' AND '" + dateTo + "'", "");
                int indexTKMD = 0;
                foreach (KDT_VNACC_ToKhaiMauDich TKMD in TKMDCollection)
                {
                    indexTKMD++;
                    this.OnProcess(new ProcessBackgroundEventArgs("Xử lý danh sách tờ khai mậu dịch", (indexTKMD * 80 / TKMDCollection.Count) + 12));
                    string where = "TKMD_ID=" + TKMD.ID;
                    //Hang mau dich Collection
                    List<KDT_VNACC_HangMauDich> listHang = new List<KDT_VNACC_HangMauDich>();
                    listHang = KDT_VNACC_HangMauDich.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_HangMauDich hang in listHang)
                    {
                        //List<KDT_VNACC_HangMauDich_ThueThuKhac> listThueThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                        //listThueThuKhac = KDT_VNACC_HangMauDich_ThueThuKhac.SelectCollectionDynamic("Master_id=" + hang.ID, "ID");
                        //foreach (KDT_VNACC_HangMauDich_ThueThuKhac thue in listThueThuKhac)
                        //{
                        //    hang.ThueThuKhacCollection.Add(thue);
                        //}
                        hang.loadThueVaThuKhac();
                        TKMD.HangCollection.Add(hang);
                    }
                    foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                    {
                        if (TKMD.LoaiHang == "N")
                        {
                            foreach (SXXK_NguyenPhuLieu_Ton item in NPLTonCollection)
                            {
                                if (HMD.MaHangHoa == item.Ma)
                                {
                                    item.LuongNhap += Math.Round(HMD.SoLuong1, GlobalSettings.SoThapPhan.LuongNPL);
                                }
                            }
                        }
                        else
                        {
                            //SXXK_NguyenPhuLieu_Ton npl = new SXXK_NguyenPhuLieu_Ton();
                            DataSet dsLuongNPL = Company.BLL.KDT.SXXK.DinhMuc.GetLuongNguyenPhuLieuTheoDM(HMD.MaHangHoa, HMD.SoLuong1, GlobalSettings.SoThapPhan.LuongNPL);
                            if (dsLuongNPL.Tables[0].Rows.Count == 0)
                            {
                                //continue;
                                string msg = "Mã sản phẩm : " + HMD.MaHangHoa + " trong tờ khai mậu dịch có ";
                                if (TKMD.SoToKhai == 0)
                                    msg += "ID=" + TKMD.ID;
                                else
                                {
                                    msg += "Số tờ khai mậu dịch VNACCS : " + TKMD.SoToKhai;

                                }
                                msg += " chưa có định mức.";
                                throw new Exception(msg);
                            }
                            foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                            {
                                string nplTemp = row["MaNguyenPhuLieu"].ToString();
                                foreach (SXXK_NguyenPhuLieu_Ton item in NPLTonCollection)
                                {
                                    if (nplTemp == item.Ma)
                                    {
                                        item.LuongXuat += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), GlobalSettings.SoThapPhan.LuongNPL);
                                    }
                                }
                                //SXXK_NguyenPhuLieu_Ton NPLTon = new SXXK_NguyenPhuLieu_Ton();
                                //string maNPL = row["MaNguyenPhuLieu"].ToString();
                                //NPLTon = getNPL(maNPL, NPLTonCollection);
                                //if (NPLTon == null)
                                //{
                                //    string msg = "Mã sản phẩm : " + HMD.MaHangHoa + " trong tờ khai mậu dịch có ";
                                //    if (TKMD.SoToKhai == 0)
                                //        msg += "ID=" + TKMD.ID;
                                //    else
                                //    {
                                //        msg += "Số tờ khai mậu dịch : " + TKMD.SoToKhai.ToString();
                                //    }
                                //    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                //    throw new Exception(msg);
                                //}
                                //NPLTon.LuongXuat += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), GlobalSettings.SoThapPhan.LuongNPL);  
                            }
                        }
                    }
                }
                foreach (SXXK_NguyenPhuLieu_Ton NPL in NPLTonCollection)
                {
                    NPL.LuongTon = NPL.LuongNhap - NPL.LuongXuat;
                    NPL.InsertUpdate();
                }
                //BindDataTotalExport();
                #endregion
            }
            catch (Exception ex)
            {
                //ShowMessage("Lỗi \r\n " + ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
