/*
Run this script on:

        192.168.72.151.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.151.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 10/13/2011 9:06:52 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_SXXK_DinhMuc_Delete]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_DinhMuc_Delete] ALTER COLUMN [DinhMucSuDung] [numeric] (18, 10) NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_SXXK_DinhMuc]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_DinhMuc] ADD
[IsFromVietNam] [bit] NULL CONSTRAINT [DF_t_KDT_SXXK_DinhMuc_IsFromVietNam] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[t_KDT_SXXK_DinhMuc] ALTER COLUMN [DinhMucSuDung] [numeric] (18, 10) NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_SXXK_DinhMuc]'
GO
ALTER TABLE [dbo].[t_SXXK_DinhMuc] ADD
[IsFromVietNam] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[f_KiemTra_TienTrinh_ThanhLy]'
GO

ALTER FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
(
	@MaHaiQuan VARCHAR(10),
	@MaDoanhNghiep VARCHAR(50),
	@SoToKhai INT,
	@MaLoaihinh VARCHAR(10),
	@NamDangKy INT,
	@MaNPL NVARCHAR(255)
)
RETURNS NVARCHAR(500)
AS 
    
/*
@SOFTECH
HUNGTQ, CREATED 04/08/2011.
Kiem tra tien trinh thuc hien chay thanh ly: . 
Gia tri dung phai la so ton cuoi cua lan thanh ly dau = so ton dau cua lan thanh ly tiep theo. 
*/
	
BEGIN

--SET @MaHaiQuan = 'C34C'
--SET @MaDoanhNghiep = '0400101556'
--SET @SoToKhai = 152
--SET @MaLoaihinh = 'NSX01'
--SET @NamDangKy = 2010
--SET @MaNPL = 'NEPLUNG'
			
DECLARE
	@LanThanhLy INT,
	@Luong NUMERIC(18,8),
	@Luong_BanLuu NUMERIC(18,8),
	@Luong_NhapTon NUMERIC(18,8),
	@TonDau NUMERIC(18,8),
	@TonCuoi NUMERIC(18,8),
	@TonCuoi_BanLuu NUMERIC(18,8),
	@TonDauThueXNK NUMERIC(18,8),
	@TonCuoiThueXNK NUMERIC(18,8),
	@cnt1 INT,--Bien dem
	@str NVARCHAR(500),
	@ketqua NVARCHAR(500), --hien thi ket qua
	@SaiSoLuong INT,
	@Valid BIT
	
/*<I> Cap nhat lech luong nhap do bi lam tron so thap phan*/	
DECLARE curThanhLy CURSOR FOR
	SELECT  LanThanhLy, Luong, TonDau, TonCuoi, TonDauThueXNK, TonCuoiThueXNK
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon]
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
	ORDER BY TonCuoi DESC
	
--BEGIN TRANSACTION T1 ;

	SET @Valid = 1
	SET @SaiSoLuong = 0	
    SET @cnt1 = 0;
    
	SET @str = '....................................................................' + char(10)
	SET @str = @str + '		STT' + ' - ' + + 'SoToKhai' + ' - ' + 'MaLoaiHinh' + ' - ' + 'NamDangKy' + ' - ' + 'MaNPL' + char(10)
	SET @str = @str + STR(@cnt1) + ' - ' + STR(@SoToKhai) + ' - ' + @MaLoaiHinh + ' - ' + str(@NamDangKy)+ ' - ' + @MaNPL + char(10)
	--PRINT @str								
	
    OPEN curThanhLy 
    FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK
	--Fetch next record
    WHILE @@FETCH_STATUS = 0 
		BEGIN
							
			SET @cnt1 = @cnt1 + 1
						
			--PRINT STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau: ' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi: ' + cast(@TonCuoi AS VARCHAR(50))
										
			IF (@cnt1 = 1)
				BEGIN
					
					--Kiem tra so luong nhap dang ky ban dau so voi luong ton dau.
					IF (@Luong <> @TonDau)
						BEGIN
							SET @ketqua = N'Sai lượng nhập và lượng tồn đầu tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
							SET @Valid = 0
						END				
					
						  
					IF(@Valid = 1)
						BEGIN
							--Kiem tra so luong nhap dang ky ban dau so voi luong nhap luu trong ton.
							SELECT  @SaiSoLuong =  COUNT(*), @Luong_NhapTon = nt.Luong
								  FROM      [dbo].t_SXXK_ThanhLy_NPLNhapTon nt
											INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu
																				   AND nt.SoToKhai = h.SoToKhai
																				   AND nt.MaLoaiHinh = h.MaLoaiHinh
																				   AND nt.NamDangKy = h.NamDangKy
																				   AND nt.MaHaiQuan = h.MaHaiQuan
								  WHERE     nt.SoToKhai = @SoToKhai
											AND nt.MaLoaiHinh = @MaLoaiHinh
											AND nt.NamDangKy = @NamDangKy
											AND nt.MaNPL = @MaNPL
											AND nt.MaHaiQuan = @MaHaiQuan
											AND nt.MaDoanhNghiep = @MaDoanhNghiep
											AND nt.Luong <> h.SoLuong
								  GROUP BY nt.Luong
						  
							IF (@SaiSoLuong > 0 AND @Luong_NhapTon <> @Luong)
								BEGIN
									SET @ketqua = N'Sai lượng nhập tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
									SET @Valid = 0
								END
							ELSE
								BEGIN	  
									SET @ketqua = N'Thanh lý đúng'
									SET @Valid = 1
									SET @TonCuoi_BanLuu = @TonCuoi
								END
						END
				END

			--Kiem tra so ton
			IF (@cnt1 > 1 AND @Valid = 1) --kiem tra tu dong thu 2 tro di
				BEGIN

					SET @str = STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi' + cast(@TonCuoi AS VARCHAR(50))
					
					IF (@TonDau <> @TonCuoi_BanLuu)
						BEGIN
							
							SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@TonCuoi_BanLuu AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
						END
					ELSE
						SET @ketqua = N'Thanh lý đúng'
				END						
			
			--Luu lai ton cuoi cua row truoc do
			SET @TonCuoi_BanLuu = @TonCuoi
				
			FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK
        END
        
    CLOSE curThanhLy --Close cursor
    DEALLOCATE curThanhLy --Deallocate cursor
	
	--PRINT @ketqua
	
    --IF @@ERROR != 0 
    --    BEGIN
            --PRINT 'Qua trinh thuc hien xay ra loi.'
            --PRINT STR(@@ERROR)
            ----ROLLBACK TRANSACTION T1 ;
            --PRINT 'ROLLBACK TRANSACTION T1'
  --      END
  --  ELSE 
		--BEGIN
			----COMMIT TRANSACTION T1 ;	
			--PRINT ''
			----PRINT 'COMMIT TRANSACTION T1'
		--END
	
	RETURN @ketqua
	
END	

--SELECT ECS_TQ_SXXK_HOATHO_2011.dbo.[f_KiemTra_TienTrinh_ThanhLy]('C34C', '0400101556', 1782, 'NSX01', 2010, 'ÐINHTAN')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_SXXK_NPLNhapTon]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SXXK_NPLNhapTon]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_DinhMuc_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Load]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_DinhMuc_Load]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
FROM
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_DinhMuc_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Update]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_DinhMuc_Update]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam  BIT
AS
UPDATE
	[dbo].[t_SXXK_DinhMuc]
SET
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[DinhMucChung] = @DinhMucChung,
	[GhiChu] = @GhiChu,
	[IsFromVietNam] = @IsFromVietNam
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_SXXK_NPLXuatTon]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SXXK_NPLXuatTon]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_DinhMuc_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Insert]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_DinhMuc_Insert]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@IsFromVietNam BIT,
	@GhiChu varchar(250)
AS
INSERT INTO [dbo].[t_SXXK_DinhMuc]
(
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
)
VALUES
(
	@MaHaiQuan,
	@MaDoanhNghiep,
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@DinhMucChung,
	@GhiChu,
	@IsFromVietNam
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KiemTraDuLieuDinhMuc]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KiemTraDuLieuDinhMuc]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_DinhMuc_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_InsertUpdate]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_InsertUpdate]
	@ID bigint,
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 10),
	@TyLeHaoHut numeric(18, 5),
	@GhiChu varchar(240),
	@STTHang int,
	@IsFromVietNam BIT,
	@Master_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_DinhMuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_DinhMuc] 
		SET
			[MaSanPham] = @MaSanPham,
			[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
			[DVT_ID] = @DVT_ID,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[GhiChu] = @GhiChu,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID,
			[IsFromVietNam] =@IsFromVietNam
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_DinhMuc]
		(
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DVT_ID],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[GhiChu],
			[STTHang],
			[Master_ID],
			[IsFromVietNam]
		)
		VALUES 
		(
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DVT_ID,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@GhiChu,
			@STTHang,
			@Master_ID,
			@IsFromVietNam
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_DinhMuc_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Load]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam]
FROM
	[dbo].[t_KDT_SXXK_DinhMuc]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_DinhMuc_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_SelectAll]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam]
FROM
	[dbo].[t_KDT_SXXK_DinhMuc]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_DanhSachNPLNhapTon]'
GO
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_GC_DinhMucDangKy_SelectAll]    
-- Database: Haiquan    
-- Author: Ngo Thanh Tung    
-- Time created: Monday, March 31, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_KDT_SXXK_DanhSachNPLNhapTon]    
 @BangKeHoSoThanhLy_ID bigint,    
 @SoThapPhanNPL int    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
SELECT    
 SoToKhai,    
 MaLoaiHinh,    
 NamDangKy,    
 NgayDangKy,    
 MaHaiQuan,    
 MaNPL,    
 TenNPL,    
 TenDVT_NPL,    
 round(Luong,@SoThapPhanNPL) as Luong,    
 round(TonDau,@SoThapPhanNPL) as TonDau ,    
 round(TonCuoi,@SoThapPhanNPL) as TonCuoi,    
 NgayThucNhap,    
 NgayHoanThanh    
    ,BangKeHoSoThanhLy_ID    
    ,round(ThueXNK,0)as ThueXNK    
    ,TonDauThueXNK    
    ,TonCuoiThueXNK    
    ,DonGiaTT    
    ,ThueSuat    
    ,TyGiaTT    
 ,0 as LanDieuChinh    
FROM    
 dbo.v_KDT_SXXK_NPLNhapTon    
where BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID    
ORDER BY   
NgayHoanThanh,--HungTQ Updated 29/10/2010. Bo sung tieu chi sap xep theo 'NgayHoanThanh' vi thuc hien thanh khoan uu tien TK co ngay hoan thanh nho nhat.  
--NgayThucNhap, --Comment by Hungtq 06/10/2011
--NgayDangKy,	--Comment by Hungtq 06/10/2011
SoToKhai,MaNPL,DonGiaTT 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_DinhMuc_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectAll]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
FROM
	[dbo].[t_SXXK_DinhMuc]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_DinhMuc_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Insert]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Insert]
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 10),
	@TyLeHaoHut numeric(18, 5),
	@GhiChu varchar(240),
	@STTHang int,
	@Master_ID bigint,
	@IsFromVietNam BIT,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_SXXK_DinhMuc]
(
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam]
)
VALUES 
(
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DVT_ID,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@GhiChu,
	@STTHang,
	@Master_ID,
	@IsFromVietNam
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_DinhMuc_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_InsertUpdate]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_DinhMuc_InsertUpdate]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam BIT
AS
IF EXISTS(SELECT [MaHaiQuan], [MaDoanhNghiep], [MaSanPham], [MaNguyenPhuLieu] FROM [dbo].[t_SXXK_DinhMuc] WHERE [MaHaiQuan] = @MaHaiQuan AND [MaDoanhNghiep] = @MaDoanhNghiep AND [MaSanPham] = @MaSanPham AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_DinhMuc] 
		SET
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[DinhMucChung] = @DinhMucChung,
			[GhiChu] = @GhiChu,
			[IsFromVietNam]=@IsFromVietNam
		WHERE
			[MaHaiQuan] = @MaHaiQuan
			AND [MaDoanhNghiep] = @MaDoanhNghiep
			AND [MaSanPham] = @MaSanPham
			AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_DinhMuc]
	(
			[MaHaiQuan],
			[MaDoanhNghiep],
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[DinhMucChung],
			[GhiChu],
			[IsFromVietNam]
	)
	VALUES
	(
			@MaHaiQuan,
			@MaDoanhNghiep,
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@DinhMucChung,
			@GhiChu,
			@IsFromVietNam
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
	@MaSanPham varchar(30),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
FROM
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_DinhMuc_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Update]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Update]
	@ID bigint,
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 10),
	@TyLeHaoHut numeric(18, 5),
	@GhiChu varchar(240),
	@STTHang int,
	@IsFromVietNam BIT,
	@Master_ID bigint
AS
UPDATE
	[dbo].[t_KDT_SXXK_DinhMuc]
SET
	[MaSanPham] = @MaSanPham,
	[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
	[DVT_ID] = @DVT_ID,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[GhiChu] = @GhiChu,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID,
	[IsFromVietNam] =@IsFromVietNam
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_DinhMuc_InsertUpdateBy]'
GO
  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_InsertUpdate]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Monday, August 25, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_InsertUpdateBy]
    @ID BIGINT ,
    @MaSanPham VARCHAR(30) ,
    @MaNguyenPhuLieu VARCHAR(30) ,
    @DVT_ID CHAR(3) ,
    @DinhMucSuDung NUMERIC(18, 10) ,
    @TyLeHaoHut NUMERIC(18, 5) ,
    @GhiChu VARCHAR(240) ,
    @STTHang INT ,
    @IsFromVietNam BIT,
    @Master_ID BIGINT
AS 
    IF EXISTS ( SELECT  [ID]
                FROM    [dbo].[t_KDT_SXXK_DinhMuc]
                WHERE   [MaSanPham] = @MaSanPham
                        AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
                        AND [DVT_ID] = @DVT_ID
                        AND [Master_ID] = @Master_ID ) 
        BEGIN  
            UPDATE  [dbo].[t_KDT_SXXK_DinhMuc]
            SET     [MaSanPham] = @MaSanPham ,
                    [MaNguyenPhuLieu] = @MaNguyenPhuLieu ,
                    [DVT_ID] = @DVT_ID ,
                    [DinhMucSuDung] = @DinhMucSuDung ,
                    [TyLeHaoHut] = @TyLeHaoHut ,
                    [GhiChu] = @GhiChu ,
                    [STTHang] = @STTHang ,
                    [IsFromVietNam] =@IsFromVietNam,
                    [Master_ID] = @Master_ID
            WHERE   [MaSanPham] = @MaSanPham
                    AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
                    AND [DVT_ID] = @DVT_ID
                    AND [Master_ID] = @Master_ID
        END  
    ELSE 
        BEGIN  
    
            INSERT  INTO [dbo].[t_KDT_SXXK_DinhMuc]
                    ( [MaSanPham] ,
                      [MaNguyenPhuLieu] ,
                      [DVT_ID] ,
                      [DinhMucSuDung] ,
                      [TyLeHaoHut] ,
                      [GhiChu] ,
                      [STTHang] ,
                      [Master_ID] , 
                      [IsFromVietNam]
                    )
            VALUES  ( @MaSanPham ,
                      @MaNguyenPhuLieu ,
                      @DVT_ID ,
                      @DinhMucSuDung ,
                      @TyLeHaoHut ,
                      @GhiChu ,
                      @STTHang ,
                      @Master_ID  ,
                      @IsFromVietNam
                      
                    )    
        END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_DinhMuc_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
 FROM [dbo].[t_SXXK_DinhMuc] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
UPDATE    dbo.t_HaiQuan_Version
SET              [Version] =1.2, Date ='2011-10-13 09:08:26.467'
GO