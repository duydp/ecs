﻿namespace Company.Interface.Feedback
{
    partial class CauHoiDoanhNghiepForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CauHoiDoanhNghiepForm));
            this.txtTieuDe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoiDung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnReset = new Janus.Windows.EditControls.UIButton();
            this.txtTraLoi = new Khendys.Controls.ExRichTextBox();
            this.phanLoaiUserControl1 = new Company.Interface.Feedback.PhanLoaiUserControl();
            this.linhVucUserControl1 = new Company.Interface.Feedback.LinhVucUserControl();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.linhVucUserControl1);
            this.grbMain.Controls.Add(this.phanLoaiUserControl1);
            this.grbMain.Controls.Add(this.txtTraLoi);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnReset);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.txtNoiDung);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.txtTieuDe);
            this.grbMain.Size = new System.Drawing.Size(844, 643);
            // 
            // txtTieuDe
            // 
            this.txtTieuDe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTieuDe.Location = new System.Drawing.Point(15, 126);
            this.txtTieuDe.Multiline = true;
            this.txtTieuDe.Name = "txtTieuDe";
            this.txtTieuDe.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTieuDe.Size = new System.Drawing.Size(383, 50);
            this.txtTieuDe.TabIndex = 2;
            this.txtTieuDe.VisualStyleManager = this.vsmMain;
            this.txtTieuDe.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Tiêu đề câu hỏi:";
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoiDung.Location = new System.Drawing.Point(15, 202);
            this.txtNoiDung.Multiline = true;
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNoiDung.Size = new System.Drawing.Size(815, 65);
            this.txtNoiDung.TabIndex = 3;
            this.txtNoiDung.VisualStyleManager = this.vsmMain;
            this.txtNoiDung.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Nội dung câu hỏi:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 278);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Nội dung trả lời:";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(753, 614);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(672, 614);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Icon = ((System.Drawing.Icon)(resources.GetObject("btnReset.Icon")));
            this.btnReset.Location = new System.Drawing.Point(591, 614);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "Làm mới";
            this.btnReset.VisualStyleManager = this.vsmMain;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // txtTraLoi
            // 
            this.txtTraLoi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTraLoi.HiglightColor = Khendys.Controls.RtfColor.White;
            this.txtTraLoi.Location = new System.Drawing.Point(15, 298);
            this.txtTraLoi.Name = "txtTraLoi";
            this.txtTraLoi.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtTraLoi.Size = new System.Drawing.Size(815, 305);
            this.txtTraLoi.TabIndex = 4;
            this.txtTraLoi.Text = "";
            this.txtTraLoi.TextColor = Khendys.Controls.RtfColor.Black;
            this.txtTraLoi.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // phanLoaiUserControl1
            // 
            this.phanLoaiUserControl1.BackColor = System.Drawing.Color.Transparent;
            this.phanLoaiUserControl1.GopYChecked = false;
            this.phanLoaiUserControl1.Location = new System.Drawing.Point(15, 6);
            this.phanLoaiUserControl1.LoiChecked = false;
            this.phanLoaiUserControl1.Name = "phanLoaiUserControl1";
            this.phanLoaiUserControl1.Size = new System.Drawing.Size(383, 95);
            this.phanLoaiUserControl1.TabIndex = 0;
            this.phanLoaiUserControl1.ThacMacChecked = false;
            // 
            // linhVucUserControl1
            // 
            this.linhVucUserControl1.BackColor = System.Drawing.Color.Transparent;
            this.linhVucUserControl1.Location = new System.Drawing.Point(404, 6);
            this.linhVucUserControl1.Name = "linhVucUserControl1";
            this.linhVucUserControl1.Size = new System.Drawing.Size(426, 170);
            this.linhVucUserControl1.TabIndex = 1;
            // 
            // CauHoiDoanhNghiepForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(844, 643);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CauHoiDoanhNghiepForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Câu hỏi";
            this.Load += new System.EventHandler(this.CauHoiForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtTieuDe;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDung;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        public Janus.Windows.EditControls.UIButton btnClose;
        public Janus.Windows.EditControls.UIButton btnSave;
        public Janus.Windows.EditControls.UIButton btnReset;
        private Khendys.Controls.ExRichTextBox txtTraLoi;
        private LinhVucUserControl linhVucUserControl1;
        private PhanLoaiUserControl phanLoaiUserControl1;
    }
}