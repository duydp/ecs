﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using System.Collections.Generic;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.GC;
#endif

namespace Company.Interface
{
    public partial class ListGiayPhepTKMDFormGCCT : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiChuyenTiep TKCT;
        //-----------------------------------------------------------------------------------------

        public ListGiayPhepTKMDFormGCCT()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKCT.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            //if (TKCT.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
                //btnTaoMoi.Enabled = false;
                //btnXoa.Enabled = false;
            //}
                btnTaoMoi.Enabled = true;
                btnXoa.Enabled = true ;
        }

  
      

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<GiayPhep> listGiayPhep = new List<GiayPhep>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiayPhep gp = (GiayPhep)i.GetRow().DataRow;
                        listGiayPhep.Add(gp);
                    }
                }
            }

            foreach (GiayPhep gp in listGiayPhep)
            {
                //Update by KHANHHN - 03/03/2012
                if (gp.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || gp.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    gp.Delete();
                    TKCT.GiayPhepCollection.Remove(gp);
                }
                else
                {
                    this.ShowMessage(string.Format("Hợp đồng thương mại bổ sung số : {0} đã được gửi đến hải quan. Không thể xóa", gp.SoGiayPhep), false);
                }
            }

            dgList.DataSource = TKCT.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

      
        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            GiayPhepFormGCCT f = new GiayPhepFormGCCT();
            f.TKCT = TKCT;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKCT.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GiayPhepFormGCCT f = new GiayPhepFormGCCT();
            f.TKCT = TKCT;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.giayPhep = (GiayPhep)e.Row.DataRow;
            if (f.giayPhep.ID > 0 && f.giayPhep.ListHMDofGiayPhep.Count == 0)
                f.giayPhep.LoadListHMDofGiayPhep();

            f.ShowDialog();
            dgList.DataSource = TKCT.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

     


    }
}
