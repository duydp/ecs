
IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_HaiQuan_DoiTac' AND COLUMN_NAME = 'DiaChi2')
  Begin
  ALTER TABLE t_HaiQuan_DoiTac
   ADD DiaChi2 NVARCHAR(50)  NULL
  END
  GO
IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_HaiQuan_DoiTac' AND COLUMN_NAME = 'BuuChinh')
  Begin
  ALTER TABLE t_HaiQuan_DoiTac
   ADD BuuChinh VARCHAR(7)  NULL
  END
  GO
  IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_HaiQuan_DoiTac' AND COLUMN_NAME = 'TinhThanh')
  Begin
  ALTER TABLE t_HaiQuan_DoiTac
   ADD TinhThanh NVARCHAR(50)  NULL
   end
GO
  IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_HaiQuan_DoiTac' AND COLUMN_NAME = 'QuocGia')
  Begin
  ALTER TABLE t_HaiQuan_DoiTac
   ADD QuocGia NVARCHAR(50)  NULL
  END
  GO
    IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_HaiQuan_DoiTac' AND COLUMN_NAME = 'MaDaiLyHQ')
  Begin
  ALTER TABLE t_HaiQuan_DoiTac
   ADD MaDaiLyHQ VARCHAR(5)  NULL
  END
  GO
      IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_HaiQuan_DoiTac' AND COLUMN_NAME = 'TenDaiLyHQ')
  Begin
  ALTER TABLE t_HaiQuan_DoiTac
   ADD TenDaiLyHQ NVARCHAR(500)  NULL
  END
  GO
  IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_HaiQuan_DoiTac' AND COLUMN_NAME = 'MaNuoc')
  Begin
  ALTER TABLE t_HaiQuan_DoiTac
   ADD MaNuoc VARCHAR(2)  NULL
  END
   IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_HaiQuan_DoiTac' AND COLUMN_NAME = 'DateCreated')
  Begin
  ALTER TABLE t_HaiQuan_DoiTac
   ADD DateCreated datetime  NULL
  END
  IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_HaiQuan_DoiTac' AND COLUMN_NAME = 'DateModified')
  Begin
  ALTER TABLE t_HaiQuan_DoiTac
   ADD DateModified datetime  NULL
  END


GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_DoiTac_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_Insert]

IF OBJECT_ID(N'[dbo].[p_DoiTac_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_Update]

IF OBJECT_ID(N'[dbo].[p_DoiTac_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_DoiTac_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_Delete]

IF OBJECT_ID(N'[dbo].[p_DoiTac_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_DoiTac_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_Load]

IF OBJECT_ID(N'[dbo].[p_DoiTac_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_DoiTac_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_Insert]
	@MaCongTy varchar(100),
	@TenCongTy varchar(500),
	@DiaChi nvarchar(500),
	@DienThoai varchar(50),
	@Email varchar(50),
	@Fax varchar(50),
	@GhiChu nvarchar(1000),
	@MaDoanhNghiep varchar(50),
	@DateCreated datetime,
	@DateModified datetime,
	@DiaChi2 nvarchar(50),
	@BuuChinh varchar(7),
	@TinhThanh nvarchar(50),
	@QuocGia nvarchar(50),
	@MaNuoc varchar(2),
	@MaDaiLyHQ varchar(5),
	@TenDaiLyHQ nvarchar(500),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_HaiQuan_DoiTac]
(
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep],
	[DateCreated],
	[DateModified],
	[DiaChi2],
	[BuuChinh],
	[TinhThanh],
	[QuocGia],
	[MaNuoc],
	[MaDaiLyHQ],
	[TenDaiLyHQ]
)
VALUES 
(
	@MaCongTy,
	@TenCongTy,
	@DiaChi,
	@DienThoai,
	@Email,
	@Fax,
	@GhiChu,
	@MaDoanhNghiep,
	@DateCreated,
	@DateModified,
	@DiaChi2,
	@BuuChinh,
	@TinhThanh,
	@QuocGia,
	@MaNuoc,
	@MaDaiLyHQ,
	@TenDaiLyHQ
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_Update]
	@ID bigint,
	@MaCongTy varchar(100),
	@TenCongTy varchar(500),
	@DiaChi nvarchar(500),
	@DienThoai varchar(50),
	@Email varchar(50),
	@Fax varchar(50),
	@GhiChu nvarchar(1000),
	@MaDoanhNghiep varchar(50),
	@DateCreated datetime,
	@DateModified datetime,
	@DiaChi2 nvarchar(50),
	@BuuChinh varchar(7),
	@TinhThanh nvarchar(50),
	@QuocGia nvarchar(50),
	@MaNuoc varchar(2),
	@MaDaiLyHQ varchar(5),
	@TenDaiLyHQ nvarchar(500)
AS

UPDATE
	[dbo].[t_HaiQuan_DoiTac]
SET
	[MaCongTy] = @MaCongTy,
	[TenCongTy] = @TenCongTy,
	[DiaChi] = @DiaChi,
	[DienThoai] = @DienThoai,
	[Email] = @Email,
	[Fax] = @Fax,
	[GhiChu] = @GhiChu,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified,
	[DiaChi2] = @DiaChi2,
	[BuuChinh] = @BuuChinh,
	[TinhThanh] = @TinhThanh,
	[QuocGia] = @QuocGia,
	[MaNuoc] = @MaNuoc,
	[MaDaiLyHQ] = @MaDaiLyHQ,
	[TenDaiLyHQ] = @TenDaiLyHQ
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_InsertUpdate]
	@ID bigint,
	@MaCongTy varchar(100),
	@TenCongTy varchar(500),
	@DiaChi nvarchar(500),
	@DienThoai varchar(50),
	@Email varchar(50),
	@Fax varchar(50),
	@GhiChu nvarchar(1000),
	@MaDoanhNghiep varchar(50),
	@DateCreated datetime,
	@DateModified datetime,
	@DiaChi2 nvarchar(50),
	@BuuChinh varchar(7),
	@TinhThanh nvarchar(50),
	@QuocGia nvarchar(50),
	@MaNuoc varchar(2),
	@MaDaiLyHQ varchar(5),
	@TenDaiLyHQ nvarchar(500)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DoiTac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DoiTac] 
		SET
			[MaCongTy] = @MaCongTy,
			[TenCongTy] = @TenCongTy,
			[DiaChi] = @DiaChi,
			[DienThoai] = @DienThoai,
			[Email] = @Email,
			[Fax] = @Fax,
			[GhiChu] = @GhiChu,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified,
			[DiaChi2] = @DiaChi2,
			[BuuChinh] = @BuuChinh,
			[TinhThanh] = @TinhThanh,
			[QuocGia] = @QuocGia,
			[MaNuoc] = @MaNuoc,
			[MaDaiLyHQ] = @MaDaiLyHQ,
			[TenDaiLyHQ] = @TenDaiLyHQ
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_HaiQuan_DoiTac]
		(
			[MaCongTy],
			[TenCongTy],
			[DiaChi],
			[DienThoai],
			[Email],
			[Fax],
			[GhiChu],
			[MaDoanhNghiep],
			[DateCreated],
			[DateModified],
			[DiaChi2],
			[BuuChinh],
			[TinhThanh],
			[QuocGia],
			[MaNuoc],
			[MaDaiLyHQ],
			[TenDaiLyHQ]
		)
		VALUES 
		(
			@MaCongTy,
			@TenCongTy,
			@DiaChi,
			@DienThoai,
			@Email,
			@Fax,
			@GhiChu,
			@MaDoanhNghiep,
			@DateCreated,
			@DateModified,
			@DiaChi2,
			@BuuChinh,
			@TinhThanh,
			@QuocGia,
			@MaNuoc,
			@MaDaiLyHQ,
			@TenDaiLyHQ
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DoiTac]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DoiTac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep],
	[DateCreated],
	[DateModified],
	[DiaChi2],
	[BuuChinh],
	[TinhThanh],
	[QuocGia],
	[MaNuoc],
	[MaDaiLyHQ],
	[TenDaiLyHQ]
FROM
	[dbo].[t_HaiQuan_DoiTac]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep],
	[DateCreated],
	[DateModified],
	[DiaChi2],
	[BuuChinh],
	[TinhThanh],
	[QuocGia],
	[MaNuoc],
	[MaDaiLyHQ],
	[TenDaiLyHQ]
FROM [dbo].[t_HaiQuan_DoiTac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep],
	[DateCreated],
	[DateModified],
	[DiaChi2],
	[BuuChinh],
	[TinhThanh],
	[QuocGia],
	[MaNuoc],
	[MaDaiLyHQ],
	[TenDaiLyHQ]
FROM
	[dbo].[t_HaiQuan_DoiTac]	

GO

 IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_KDT_VNACC_TKVC_TrungChuyen' AND COLUMN_NAME = 'NgayDenThucTe')
  Begin
  ALTER TABLE t_KDT_VNACC_TKVC_TrungChuyen
   ADD NgayDenThucTe DateTime  NULL
  END

GO
 IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_KDT_VNACC_TKVC_TrungChuyen' AND COLUMN_NAME = 'NgayDiThucTe')
  Begin
  ALTER TABLE t_KDT_VNACC_TKVC_TrungChuyen
   ADD NgayDiThucTe DateTime  NULL
  END

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]
	@Master_ID bigint,
	@MaDiaDiemTrungChuyen varchar(7),
	@TenDiaDiemTrungChuyen varchar(20),
	@NgayDenDiaDiem_TC datetime,
	@NgayDenThucTe datetime,
	@NgayDiDiaDiem_TC datetime,
	@NgayDiThucTe datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
(
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDenThucTe],
	[NgayDiDiaDiem_TC],
	[NgayDiThucTe]
)
VALUES 
(
	@Master_ID,
	@MaDiaDiemTrungChuyen,
	@TenDiaDiemTrungChuyen,
	@NgayDenDiaDiem_TC,
	@NgayDenThucTe,
	@NgayDiDiaDiem_TC,
	@NgayDiThucTe
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaDiaDiemTrungChuyen varchar(7),
	@TenDiaDiemTrungChuyen varchar(20),
	@NgayDenDiaDiem_TC datetime,
	@NgayDenThucTe datetime,
	@NgayDiDiaDiem_TC datetime,
	@NgayDiThucTe datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
SET
	[Master_ID] = @Master_ID,
	[MaDiaDiemTrungChuyen] = @MaDiaDiemTrungChuyen,
	[TenDiaDiemTrungChuyen] = @TenDiaDiemTrungChuyen,
	[NgayDenDiaDiem_TC] = @NgayDenDiaDiem_TC,
	[NgayDenThucTe] = @NgayDenThucTe,
	[NgayDiDiaDiem_TC] = @NgayDiDiaDiem_TC,
	[NgayDiThucTe] = @NgayDiThucTe
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaDiaDiemTrungChuyen varchar(7),
	@TenDiaDiemTrungChuyen varchar(20),
	@NgayDenDiaDiem_TC datetime,
	@NgayDenThucTe datetime,
	@NgayDiDiaDiem_TC datetime,
	@NgayDiThucTe datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TKVC_TrungChuyen] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TKVC_TrungChuyen] 
		SET
			[Master_ID] = @Master_ID,
			[MaDiaDiemTrungChuyen] = @MaDiaDiemTrungChuyen,
			[TenDiaDiemTrungChuyen] = @TenDiaDiemTrungChuyen,
			[NgayDenDiaDiem_TC] = @NgayDenDiaDiem_TC,
			[NgayDenThucTe] = @NgayDenThucTe,
			[NgayDiDiaDiem_TC] = @NgayDiDiaDiem_TC,
			[NgayDiThucTe] = @NgayDiThucTe
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
		(
			[Master_ID],
			[MaDiaDiemTrungChuyen],
			[TenDiaDiemTrungChuyen],
			[NgayDenDiaDiem_TC],
			[NgayDenThucTe],
			[NgayDiDiaDiem_TC],
			[NgayDiThucTe]
		)
		VALUES 
		(
			@Master_ID,
			@MaDiaDiemTrungChuyen,
			@TenDiaDiemTrungChuyen,
			@NgayDenDiaDiem_TC,
			@NgayDenThucTe,
			@NgayDiDiaDiem_TC,
			@NgayDiThucTe
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TKVC_TrungChuyen] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDenThucTe],
	[NgayDiDiaDiem_TC],
	[NgayDiThucTe]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDenThucTe],
	[NgayDiDiaDiem_TC],
	[NgayDiThucTe]
FROM [dbo].[t_KDT_VNACC_TKVC_TrungChuyen] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDenThucTe],
	[NgayDiDiaDiem_TC],
	[NgayDiThucTe]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]	

GO

           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '14.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('14.5', GETDATE(), N'Cap nhat bang trung chuyen cua to khai van chuyen')
END	