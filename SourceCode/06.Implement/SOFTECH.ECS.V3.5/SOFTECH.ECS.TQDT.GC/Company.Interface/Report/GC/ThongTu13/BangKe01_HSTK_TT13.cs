﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe01_HSTK_TT13 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewBC01TT74Form report;
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;

        public BangKe01_HSTK_TT13()
        {
            InitializeComponent();
        }

        public void BindReport(DataTable dt)
        {
            lblNgayKi.Text = GlobalSettings.TieudeNgay;
            this.ReportFooter.Visible = true;
            this.DataSource = dt;

            
            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            lblMathang.Text = loaiSP;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;


            lblSTT.DataBindings.Add("Text", this.DataSource, "STT");
            lblToKhaiNhap.DataBindings.Add("Text", this.DataSource, "SoToKhai");
            lblNgayDK.DataBindings.Add("Text", this.DataSource, "NgayDangKy", "{0:dd/MM/yyyy}");
            lblTenNguyenLieuVT.DataBindings.Add("Text", this.DataSource, "TenNguyenPhuLieu");
            lblMaNLVT.DataBindings.Add("Text", this.DataSource, "MaNguyenPhuLieu");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT");
            lblLuonghang.DataBindings.Add("Text", this.DataSource, "SoLuong", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuong.DataBindings.Add("Text", this.DataSource, "TongLuong", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");

        }

        private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            //report.txtName.Text = label.Text;
            //report.lblName.Text = label.Tag.ToString();
        }

        public void setText(XRLabel label, string text)
        {
            //label.Text = text;
        }
    }
}
