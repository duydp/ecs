DELETE FROM dbo.t_HaiQuan_DonViHaiQuan
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B50B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B50B', N'Chi cục HQ CK Tịnh Biên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Tịnh Biên' WHERE ID='B50B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B50C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B50C', N'Chi cục HQ Vĩnh Hội Đông', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Vĩnh Hội Đông' WHERE ID='B50C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B50D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B50D', N'Chi cục HQ CK Vĩnh Xương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Vĩnh Xương' WHERE ID='B50D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B50J')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B50J', N'Chi cục HQ Bắc Đai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bắc Đai' WHERE ID='B50J'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B50K')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B50K', N'Chi cục HQ Khánh Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Khánh Bình' WHERE ID='B50K'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C50E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C50E', N'Chi cục HQ Cảng Mỹ Thới', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Cảng Mỹ Thới' WHERE ID='C50E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B51E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B51E', N'Chi cục HQ Cảng Cát Lở', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Cảng Cát Lở' WHERE ID='B51E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C511')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C511', N'Chi cục HQ CK Cảng Phú Mỹ', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Phú Mỹ' WHERE ID='C511'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C512')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C512', N'Chi cục HQ CK Cảng Phú Mỹ', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Phú Mỹ' WHERE ID='C512'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C51B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C51B', N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu' WHERE ID='C51B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C51H')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C51H', N'Chi cục HQ Côn Đảo', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Côn Đảo' WHERE ID='C51H'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C51I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C51I', N'Chi cục HQ CK cảng Cái Mép', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK cảng Cái Mép' WHERE ID='C51I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='A181')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'A181', N'Chi cục HQ Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bắc Ninh' WHERE ID='A181'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='A182')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'A182', N'Chi cục HQ Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bắc Ninh' WHERE ID='A182'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='A183')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'A183', N'Chi cục HQ Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bắc Ninh' WHERE ID='A183'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B181')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B181', N'Chi cục HQ Thái Nguyên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Thái Nguyên' WHERE ID='B181'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B182')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B182', N'Chi cục HQ Thái Nguyên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Thái Nguyên' WHERE ID='B182'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B18C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B18C', N'Chi cục HQ Quản lý các KCN Bắc Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý các KCN Bắc Giang' WHERE ID='B18C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='I18D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'I18D', N'Chi cục HQ Cảng nội địa Tiên Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Cảng nội địa Tiên Sơn' WHERE ID='I18D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C37B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C37B', N'Chi cục HQ CK Cảng Qui Nhơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Qui Nhơn' WHERE ID='C37B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='T37C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'T37C', N'Chi cục HQ Phú Yên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Phú Yên' WHERE ID='T37C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C43N')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C43N', N'Chi cục HQ CK Cảng tổng hợp Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng tổng hợp Bình Dương' WHERE ID='C43N'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='I43H')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'I43H', N'Chi cục HQ Sóng Thần', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Sóng Thần' WHERE ID='I43H'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='K431')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'K431', N'Chi cục HQ KCN Mỹ Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Mỹ Phước' WHERE ID='K431'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='K432')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'K432', N'Chi cục HQ KCN Mỹ Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Mỹ Phước' WHERE ID='K432'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='K433')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'K433', N'Chi cục HQ KCN Mỹ Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Mỹ Phước' WHERE ID='K433'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N43D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N43D', N'Chi cục HQ KCN Sóng Thần', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Sóng Thần' WHERE ID='N43D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N43F')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N43F', N'Chi cục HQ KCN Việt Nam - Singapore', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Việt Nam - Singapore' WHERE ID='N43F'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N43G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N43G', N'Chi cục HQ KCN Việt Hương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Việt Hương' WHERE ID='N43G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P43B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P43B', N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN' WHERE ID='P43B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B61A')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B61A', N'Chi cục HQ CK Quốc tế Hoa Lư', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Hoa Lư' WHERE ID='B61A'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B61A')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B61A', N'Chi cục HQ CK Quốc tế Hoa Lư', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Hoa Lư' WHERE ID='B61A'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P61A')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P61A', N'Chi cục HQ Chơn Thành', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chơn Thành' WHERE ID='P61A'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P61A')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P61A', N'Chi cục HQ Chơn Thành', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chơn Thành' WHERE ID='P61A'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B61B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B61B', N'Chi cục HQ CK Hoàng Diệu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Hoàng Diệu' WHERE ID='B61B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B61B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B61B', N'Chi cục HQ CK Hoàng Diệu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Hoàng Diệu' WHERE ID='B61B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B59D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B59D', N'Chi cục HQ Hòa Trung', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Hòa Trung' WHERE ID='B59D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C59B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C59B', N'Chi cục HQ CK Cảng Năm Căn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Năm Căn' WHERE ID='C59B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C54B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C54B', N'Chi cục HQ CK Cảng Cần Thơ', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Cần Thơ' WHERE ID='C54B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C54D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C54D', N'Chi cục HQ CK Vĩnh Long', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Vĩnh Long' WHERE ID='C54D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P54H')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P54H', N'Chi cục HQ Tây Đô', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Tây Đô' WHERE ID='P54H'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P54K')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P54K', N'Chi cục HQ Sóc Trăng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Sóc Trăng' WHERE ID='P54K'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B111')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B111', N'Chi cục HQ CK Tà Lùng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Tà Lùng' WHERE ID='B111'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B112')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B112', N'Chi cục HQ CK Tà Lùng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Tà Lùng' WHERE ID='B112'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B11E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B11E', N'Chi cục HQ CK Trà Lĩnh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Trà Lĩnh' WHERE ID='B11E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B11F')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B11F', N'Chi cục HQ CK Sóc Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Sóc Giang' WHERE ID='B11F'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B11H')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B11H', N'Chi cục HQ CK Pò Peo', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Pò Peo' WHERE ID='B11H'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='G111')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'G111', N'Chi cục HQ CK Bí Hà', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Bí Hà' WHERE ID='G111'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='G112')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'G112', N'Chi cục HQ CK Bí Hà', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Bí Hà' WHERE ID='G112'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P11K')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P11K', N'Chi cục HQ Bắc Kạn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bắc Kạn' WHERE ID='P11K'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='A34B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'A34B', N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng' WHERE ID='A34B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='A34B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'A34B', N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng' WHERE ID='A34B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C34C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C34C', N'Chi cục HQ Quản lý hàng đầu tư - gia công', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE ID='C34C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C34E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C34E', N'Chi cục HQ CK Cảng Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Đà Nẵng' WHERE ID='C34E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N34G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N34G', N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu' WHERE ID='N34G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N34H')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N34H', N'Chi cục HQ KCN Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Đà Nẵng' WHERE ID='N34H'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B401')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B401', N'Chi cục HQ CK BupRăng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK BupRăng' WHERE ID='B401'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B40C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B40C', N'Chi cục HQ Buôn Mê Thuột', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Buôn Mê Thuột' WHERE ID='B40C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D401')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D401', N'Chi cục HQ Đà Lạt', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Đà Lạt' WHERE ID='D401'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B121')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B121', N'Chi cục HQ CK Quốc tế Tây Trang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Tây Trang' WHERE ID='B121'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B122')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B122', N'Chi cục HQ CK Quốc tế Tây Trang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Tây Trang' WHERE ID='B122'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B12E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B12E', N'Chi cục HQ CK Lóng Sập', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Lóng Sập' WHERE ID='B12E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B12I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B12I', N'Chi cục HQ CK Chiềng Khương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Chiềng Khương' WHERE ID='B12I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F121')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F121', N'Chi cục HQ Sơn La', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Sơn La' WHERE ID='F121'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F122')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F122', N'Chi cục HQ Sơn La', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Sơn La' WHERE ID='F122'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='H121')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'H121', N'Chi cục HQ CK Ma Lu Thàng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Ma Lu Thàng' WHERE ID='H121'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='H122')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'H122', N'Chi cục HQ CK Ma Lu Thàng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Ma Lu Thàng' WHERE ID='H122'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D471')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D471', N'Chi cục HQ Long Thành', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Long Thành' WHERE ID='D471'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D472')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D472', N'Chi cục HQ Long Thành', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Long Thành' WHERE ID='D472'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D473')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D473', N'Chi cục HQ Long Thành', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Long Thành' WHERE ID='D473'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='I471')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'I471', N'Chi cục HQ Long Bình Tân', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Long Bình Tân' WHERE ID='I471'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='I472')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'I472', N'Chi cục HQ Long Bình Tân', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Long Bình Tân' WHERE ID='I472'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N47B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N47B', N'Chi cục HQ Biên Hoà', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Biên Hoà' WHERE ID='N47B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N47B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N47B', N'Chi cục HQ Biên Hoà', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Biên Hoà' WHERE ID='N47B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N47F')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N47F', N'Chi cục HQ Thống Nhất', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Thống Nhất' WHERE ID='N47F'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N47G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N47G', N'Chi cục HQ Nhơn Trạch', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Nhơn Trạch' WHERE ID='N47G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N47M')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N47M', N'Chi cục HQ QL KCN Bình Thuận', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ QL KCN Bình Thuận' WHERE ID='N47M'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='X47E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'X47E', N'Chi cục HQ KCX Long Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCX Long Bình' WHERE ID='X47E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B49B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B49B', N'Chi cục HQ CK Thường Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Thường Phước' WHERE ID='B49B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B49E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B49E', N'Chi cục HQ Sở Thượng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Sở Thượng' WHERE ID='B49E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B49F')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B49F', N'Chi cục HQ Thông Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Thông Bình' WHERE ID='B49F'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B49G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B49G', N'Chi cục HQ CK Dinh Bà', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Dinh Bà' WHERE ID='B49G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C491')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C491', N'Chi cục HQ CK Cảng Đồng Tháp', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Đồng Tháp' WHERE ID='C491'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C492')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C492', N'Chi cục HQ CK Cảng Đồng Tháp', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Đồng Tháp' WHERE ID='C492'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B381')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B381', N'Chi cục HQ CK Lệ Thanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Lệ Thanh' WHERE ID='B381'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B382')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B382', N'Chi cục HQ CK Lệ Thanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Lệ Thanh' WHERE ID='B382'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B38C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B38C', N'Chi cục HQ CK Bờ Y', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Bờ Y' WHERE ID='B38C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P38D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P38D', N'Chi cục HQ Kon Tum', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Kon Tum' WHERE ID='P38D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B10B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B10B', N'Chi cục HQ CK Thanh Thủy', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Thanh Thủy' WHERE ID='B10B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B10C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B10C', N'Chi cục HQ CK Xín Mần', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Xín Mần' WHERE ID='B10C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B10D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B10D', N'Chi cục HQ CK Phó Bảng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Phó Bảng' WHERE ID='B10D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B10F')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B10F', N'Chi cục HQ CK Săm Pun', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Săm Pun' WHERE ID='B10F'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B10I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B10I', N'Chi cục HQ Tuyên Quang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Tuyên Quang' WHERE ID='B10I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B011')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B011', N'Chi cục HQ CK Sân bay quốc tế Nội Bài', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE ID='B011'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B013')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B013', N'Chi cục HQ CK Sân bay quốc tế Nội Bài', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE ID='B013'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B016')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B016', N'Chi cục HQ CK Sân bay quốc tế Nội Bài', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE ID='B016'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B015')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B015', N'Chi cục HQ CK Sân bay quốc tế Nội Bài', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE ID='B015'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B01T')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B01T', N'Chi cục HQ Yên Bái', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Yên Bái' WHERE ID='B01T'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='E011')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'E011', N'Chi cục HQ Bắc Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bắc Hà Nội' WHERE ID='E011'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='E012')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'E012', N'Chi cục HQ Bắc Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bắc Hà Nội' WHERE ID='E012'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='I01K')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'I01K', N'Chi cục HQ Gia Thụy', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Gia Thụy' WHERE ID='I01K'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='M011')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'M011', N'Chi cục HQ Hà Tây', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Hà Tây' WHERE ID='M011'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='M012')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'M012', N'Chi cục HQ Hà Tây', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Hà Tây' WHERE ID='M012'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N01V')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N01V', N'Chi cục HQ KCN Bắc Thăng Long', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Bắc Thăng Long' WHERE ID='N01V'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P01J')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P01J', N'Chi cục HQ Phú Thọ', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Phú Thọ' WHERE ID='P01J'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P01L')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P01L', N'Chi cục HQ Quản lý hàng đầu tư - gia công', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE ID='P01L'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P01R')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P01R', N'Chi cục HQ Vĩnh Phúc', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Vĩnh Phúc' WHERE ID='P01R'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D01D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D01D', N'Chi cục HQ chuyển phát nhanh HN', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ chuyển phát nhanh HN' WHERE ID='D01D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D01D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D01D', N'Chi cục HQ chuyển phát nhanh HN', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ chuyển phát nhanh HN' WHERE ID='D01D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='S01I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'S01I', N'Chi cục HQ Ga đường sắt quốc tế Yên Viên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Ga đường sắt quốc tế Yên Viên' WHERE ID='S01I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P01Q')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P01Q', N'Chi cục HQ Hòa Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Hòa Bình' WHERE ID='P01Q'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B30B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B30B', N'Chi cục HQ CK Quốc tế Cầu Treo', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Cầu Treo' WHERE ID='B30B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B30E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B30E', N'Chi cục HQ Hồng Lĩnh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Hồng Lĩnh' WHERE ID='B30E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B30I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B30I', N'Chi cục HQ khu kinh tế CK Cầu Treo', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ khu kinh tế CK Cầu Treo' WHERE ID='B30I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C30C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C30C', N'Chi cục HQ CK Cảng Xuân Hải', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Xuân Hải' WHERE ID='C30C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F301')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F301', N'Chi cục HQ CK Cảng Vũng Áng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Vũng Áng' WHERE ID='F301'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F302')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F302', N'Chi cục HQ CK Cảng Vũng Áng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Vũng Áng' WHERE ID='F302'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C03C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C03C', N'Chi cục HQ CK cảng Hải Phòng KV I', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK cảng Hải Phòng KV I' WHERE ID='C03C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C03D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C03D', N'Chi cục HQ Thái Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Thái Bình' WHERE ID='C03D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C03E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C03E', N'Chi cục HQ CK cảng Hải Phòng KV II', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK cảng Hải Phòng KV II' WHERE ID='C03E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='E03E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'E03E', N'Chi cục HQ CK Cảng Đình Vũ', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Đình Vũ' WHERE ID='E03E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N03K')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N03K', N'Chi cục HQ KCX và KCN', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCX và KCN' WHERE ID='N03K'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P03A')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P03A', N'Chi cục HQ Quản lý hàng đầu tư - gia công', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE ID='P03A'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P03A')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P03A', N'Chi cục HQ Quản lý hàng đầu tư - gia công', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE ID='P03A'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P03J')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P03J', N'Chi cục HQ Hải Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Hải Dương' WHERE ID='P03J'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P03L')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P03L', N'Chi cục HQ Hưng Yên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Hưng Yên' WHERE ID='P03L'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='T03G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'T03G', N'Chi cục HQ CK cảng Hải Phòng KV III', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK cảng Hải Phòng KV III' WHERE ID='T03G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='T03G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'T03G', N'Chi cục HQ CK cảng Hải Phòng KV III', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK cảng Hải Phòng KV III' WHERE ID='T03G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B41H')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B41H', N'Chi cục HQ Ninh Thuận', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Ninh Thuận' WHERE ID='B41H'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C41B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C41B', N'Chi cục HQ CK Cảng Nha Trang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Nha Trang' WHERE ID='C41B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C41C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C41C', N'Chi cục HQ CK Cảng Cam Ranh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Cam Ranh' WHERE ID='C41C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='A41B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'A41B', N'Chi cục HQ CK sân bay quốc tế Cam Ranh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK sân bay quốc tế Cam Ranh' WHERE ID='A41B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P41E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P41E', N'Chi cục HQ Vân Phong', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Vân Phong' WHERE ID='P41E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B53C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B53C', N'Chi cục HQ CK Quốc Tế Hà Tiên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc Tế Hà Tiên' WHERE ID='B53C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B53K')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B53K', N'Chi cục HQ CK Giang Thành', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Giang Thành' WHERE ID='B53K'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C53D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C53D', N'Chi cục HQ CK Cảng Hòn Chông', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Hòn Chông' WHERE ID='C53D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C53H')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C53H', N'Chi cục HQ Phú Quốc', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Phú Quốc' WHERE ID='C53H'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15B', N'Chi cục HQ CK Hữu Nghị', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Hữu Nghị' WHERE ID='B15B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15B', N'Chi cục HQ CK Hữu Nghị', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Hữu Nghị' WHERE ID='B15B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15B', N'Chi cục HQ CK Hữu Nghị', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Hữu Nghị' WHERE ID='B15B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15C', N'Chi cục HQ CK Chi Ma', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Chi Ma' WHERE ID='B15C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15C', N'Chi cục HQ CK Chi Ma', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Chi Ma' WHERE ID='B15C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15D', N'Chi cục HQ Cốc Nam', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Cốc Nam' WHERE ID='B15D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15E', N'Chi cục HQ Tân Thanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Tân Thanh' WHERE ID='B15E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15E', N'Chi cục HQ Tân Thanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Tân Thanh' WHERE ID='B15E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15E', N'Chi cục HQ Tân Thanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Tân Thanh' WHERE ID='B15E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B15E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B15E', N'Chi cục HQ Tân Thanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Tân Thanh' WHERE ID='B15E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='S15I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'S15I', N'Chi cục HQ Ga Đồng Đăng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Ga Đồng Đăng' WHERE ID='S15I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B13B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B13B', N'Chi cục HQ CK Quốc tế Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Lào Cai' WHERE ID='B13B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B13B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B13B', N'Chi cục HQ CK Quốc tế Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Lào Cai' WHERE ID='B13B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B13C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B13C', N'Chi cục HQ CK Mường Khương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Mường Khương' WHERE ID='B13C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B13D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B13D', N'Chi cục HQ CK Bát Xát', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Bát Xát' WHERE ID='B13D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='G131')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'G131', N'Chi cục HQ Đường sắt LVQT Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Đường sắt LVQT Lào Cai' WHERE ID='G131'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='G132')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'G132', N'Chi cục HQ Đường sắt LVQT Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Đường sắt LVQT Lào Cai' WHERE ID='G132'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B48C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B48C', N'Chi cục HQ CK Mỹ Quý Tây', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Mỹ Quý Tây' WHERE ID='B48C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B48D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B48D', N'Chi cục HQ CK Quốc tế Bình Hiệp', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Bình Hiệp' WHERE ID='B48D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B48E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B48E', N'Chi cục HQ Hưng Điền', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Hưng Điền' WHERE ID='B48E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B48I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B48I', N'Chi cục HQ Đức Hòa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Đức Hòa' WHERE ID='B48I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C48G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C48G', N'Chi cục HQ CK Cảng Mỹ Tho', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Mỹ Tho' WHERE ID='C48G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C48G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C48G', N'Chi cục HQ CK Cảng Mỹ Tho', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Mỹ Tho' WHERE ID='C48G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F481')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F481', N'Chi cục HQ Bến Lức', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bến Lức' WHERE ID='F481'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F482')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F482', N'Chi cục HQ Bến Lức', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bến Lức' WHERE ID='F482'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B29B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B29B', N'Chi cục HQ CK Quốc tế Nậm Cắn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Nậm Cắn' WHERE ID='B29B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B29H')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B29H', N'Chi cục HQ CK Thanh Thủy', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Thanh Thủy' WHERE ID='B29H'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C29C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C29C', N'Chi cục HQ CK Cảng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng' WHERE ID='C29C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P29F')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P29F', N'Chi cục HQ Vinh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Vinh' WHERE ID='P29F'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B31B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B31B', N'Chi cục HQ CK Cha Lo', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cha Lo' WHERE ID='B31B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B31F')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B31F', N'Chi cục HQ CK Cà Roòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cà Roòng' WHERE ID='B31F'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D311')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D311', N'Chi cục HQ CK Cảng Hòn La', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Hòn La' WHERE ID='D311'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D312')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D312', N'Chi cục HQ CK Cảng Hòn La', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Hòn La' WHERE ID='D312'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D313')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D313', N'Chi cục HQ CK Cảng Hòn La', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Hòn La' WHERE ID='D313'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B60D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B60D', N'Chi cục HQ CK Nam Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Nam Giang' WHERE ID='B60D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C601')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C601', N'Chi cục HQ KCN Điện Nam - Điện Ngọc', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Điện Nam - Điện Ngọc' WHERE ID='C601'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C602')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C602', N'Chi cục HQ KCN Điện Nam - Điện Ngọc', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Điện Nam - Điện Ngọc' WHERE ID='C602'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C60B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C60B', N'Chi cục HQ CK Cảng Kỳ Hà', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Kỳ Hà' WHERE ID='C60B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C35B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C35B', N'Chi cục HQ CK Cảng Dung Quất', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Dung Quất' WHERE ID='C35B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N35C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N35C', N'Chi cục HQ các KCN Quảng Ngãi', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ các KCN Quảng Ngãi' WHERE ID='N35C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B201')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B201', N'Chi cục HQ CK Móng Cái', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Móng Cái' WHERE ID='B201'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B202')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B202', N'Chi cục HQ CK Móng Cái', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Móng Cái' WHERE ID='B202'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B20C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B20C', N'Chi cục HQ CK Hoành Mô', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Hoành Mô' WHERE ID='B20C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B20D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B20D', N'Chi cục HQ Bắc Phong Sinh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Bắc Phong Sinh' WHERE ID='B20D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C20D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C20D', N'Chi cục HQ CK Cảng Cái Lân', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Cái Lân' WHERE ID='C20D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C20E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C20E', N'Chi cục HQ CK Cảng Vạn Gia', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Vạn Gia' WHERE ID='C20E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C20F')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C20F', N'Chi cục HQ CK Cảng Hòn Gai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Hòn Gai' WHERE ID='C20F'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C20G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C20G', N'Chi cục HQ CK Cảng Cẩm Phả', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Cẩm Phả' WHERE ID='C20G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B32B')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B32B', N'Chi cục HQ CK Lao Bảo', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Lao Bảo' WHERE ID='B32B'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B32C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B32C', N'Chi cục HQ CK La Lay', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK La Lay' WHERE ID='B32C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B32D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B32D', N'Chi cục HQ Khu thương mại Lao Bảo', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Khu thương mại Lao Bảo' WHERE ID='B32D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C32D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C32D', N'Chi cục HQ CK Cảng Cửa Việt', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Cửa Việt' WHERE ID='C32D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='V32G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'V32G', N'Đội Kiẻm soát HQ Quảng Trị', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Đội Kiẻm soát HQ Quảng Trị' WHERE ID='V32G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B451')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B451', N'Chi cục HQ CK Mộc Bài', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Mộc Bài' WHERE ID='B451'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B452')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B452', N'Chi cục HQ CK Mộc Bài', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Mộc Bài' WHERE ID='B452'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B45D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B45D', N'Chi cục HQ Phước Tân', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Phước Tân' WHERE ID='B45D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B45E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B45E', N'Chi cục HQ CK Kà Tum', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Kà Tum' WHERE ID='B45E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C451')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C451', N'Chi cục HQ CK Xa Mát', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Xa Mát' WHERE ID='C451'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C452')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C452', N'Chi cục HQ CK Xa Mát', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Xa Mát' WHERE ID='C452'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F451')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F451', N'Chi cục HQ KCN Trảng Bàng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Trảng Bàng' WHERE ID='F451'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F452')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F452', N'Chi cục HQ KCN Trảng Bàng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCN Trảng Bàng' WHERE ID='F452'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B271')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B271', N'Chi cục HQ CK Quốc tế Na Mèo', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Na Mèo' WHERE ID='B271'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B272')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B272', N'Chi cục HQ CK Quốc tế Na Mèo', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Quốc tế Na Mèo' WHERE ID='B272'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F271')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F271', N'Chi cục HQ CK Cảng Thanh Hóa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Thanh Hóa' WHERE ID='F271'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F272')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F272', N'Chi cục HQ CK Cảng Nghi Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Nghi Sơn' WHERE ID='F272'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='N28J')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'N28J', N'Chi cục HQ Quản lý các KCN Hà Nam', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý các KCN Hà Nam' WHERE ID='N28J'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P28C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P28C', N'Chi cục HQ Ninh Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Ninh Bình' WHERE ID='P28C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P28C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P28C', N'Chi cục HQ Ninh Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Ninh Bình' WHERE ID='P28C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P28E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P28E', N'Chi cục HQ Nam Định', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Nam Định' WHERE ID='P28E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B33A')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B33A', N'Chi cục HQ CK A Đớt', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK A Đớt' WHERE ID='B33A'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B33A')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B33A', N'Chi cục HQ CK A Đớt', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK A Đớt' WHERE ID='B33A'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C33C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C33C', N'Chi cục HQ CK Cảng Thuận An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Thuận An' WHERE ID='C33C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C33F')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C33F', N'Chi cục HQ CK Cảng Chân Mây', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Chân Mây' WHERE ID='C33F'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P33D')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P33D', N'Chi cục HQ Thủy An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Thủy An' WHERE ID='P33D'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='D02S')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'D02S', N'Chi cục HQ Chuyển phát nhanh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Chuyển phát nhanh' WHERE ID='D02S'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C02V')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C02V', N'Chi cục HQ CK Cảng Hiệp Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Hiệp Phước' WHERE ID='C02V'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C02V')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C02V', N'Chi cục HQ CK Cảng Hiệp Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Hiệp Phước' WHERE ID='C02V'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C02I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C02I', N'Chi cục HQ CK Cảng Sài Gòn KV I', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV I' WHERE ID='C02I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C02I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C02I', N'Chi cục HQ CK Cảng Sài Gòn KV I', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV I' WHERE ID='C02I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C02I')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C02I', N'Chi cục HQ CK Cảng Sài Gòn KV I', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV I' WHERE ID='C02I'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C02C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C02C', N'Chi cục HQ CK Cảng Sài Gòn KV II', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV II' WHERE ID='C02C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C02C')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C02C', N'Chi cục HQ CK Cảng Sài Gòn KV II', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV II' WHERE ID='C02C'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='H021')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'H021', N'Chi cục HQ CK Cảng Sài Gòn KV III', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV III' WHERE ID='H021'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='H022')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'H022', N'Chi cục HQ CK Cảng Sài Gòn KV III', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV III' WHERE ID='H022'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='H023')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'H023', N'Chi cục HQ CK Cảng Sài Gòn KV III', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV III' WHERE ID='H023'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='I02K')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'I02K', N'Chi cục HQ CK Cảng Sài Gòn KV IV', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV IV' WHERE ID='I02K'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='I02K')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'I02K', N'Chi cục HQ CK Cảng Sài Gòn KV IV', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Cảng Sài Gòn KV IV' WHERE ID='I02K'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='B021')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'B021', N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất' WHERE ID='B021'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='C02X')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'C02X', N'Chi cục HQ CK Tân Cảng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ CK Tân Cảng' WHERE ID='C02X'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F021')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F021', N'Chi cục HQ KCX Linh Trung', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCX Linh Trung' WHERE ID='F021'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F021')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F021', N'Chi cục HQ KCX Linh Trung', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCX Linh Trung' WHERE ID='F021'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F022')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F022', N'Chi cục HQ KCX Linh Trung', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCX Linh Trung' WHERE ID='F022'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='F023')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'F023', N'Chi cục HQ KCX Linh Trung', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCX Linh Trung' WHERE ID='F023'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='X02E')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'X02E', N'Chi cục HQ KCX Tân Thuận', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ KCX Tân Thuận' WHERE ID='X02E'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P02G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P02G', N'Chi cục HQ Quản lý hàng đầu tư', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý hàng đầu tư' WHERE ID='P02G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P02G')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P02G', N'Chi cục HQ Quản lý hàng đầu tư', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý hàng đầu tư' WHERE ID='P02G'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P02J')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P02J', N'Chi cục HQ Quản lý hàng gia công', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý hàng gia công' WHERE ID='P02J'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='P02J')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'P02J', N'Chi cục HQ Quản lý hàng gia công', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Chi cục HQ Quản lý hàng gia công' WHERE ID='P02J'
END

IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z50Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z50Z', N'Cục Hải Quan An Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan An Giang' WHERE ID='Z50Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z50Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z50Z', N'Cục Hải Quan An Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan An Giang' WHERE ID='Z50Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z50Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z50Z', N'Cục Hải Quan An Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan An Giang' WHERE ID='Z50Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z50Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z50Z', N'Cục Hải Quan An Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan An Giang' WHERE ID='Z50Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z50Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z50Z', N'Cục Hải Quan An Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan An Giang' WHERE ID='Z50Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z50Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z50Z', N'Cục Hải Quan An Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan An Giang' WHERE ID='Z50Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z51Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z51Z', N'Cục Hải Quan Bà Rịa - Vũng Tàu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bà Rịa - Vũng Tàu' WHERE ID='Z51Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z51Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z51Z', N'Cục Hải Quan Bà Rịa - Vũng Tàu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bà Rịa - Vũng Tàu' WHERE ID='Z51Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z51Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z51Z', N'Cục Hải Quan Bà Rịa - Vũng Tàu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bà Rịa - Vũng Tàu' WHERE ID='Z51Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z51Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z51Z', N'Cục Hải Quan Bà Rịa - Vũng Tàu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bà Rịa - Vũng Tàu' WHERE ID='Z51Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z51Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z51Z', N'Cục Hải Quan Bà Rịa - Vũng Tàu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bà Rịa - Vũng Tàu' WHERE ID='Z51Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z51Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z51Z', N'Cục Hải Quan Bà Rịa - Vũng Tàu', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bà Rịa - Vũng Tàu' WHERE ID='Z51Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z18Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z18Z', N'Cục Hải Quan Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bắc Ninh' WHERE ID='Z18Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z18Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z18Z', N'Cục Hải Quan Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bắc Ninh' WHERE ID='Z18Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z18Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z18Z', N'Cục Hải Quan Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bắc Ninh' WHERE ID='Z18Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z18Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z18Z', N'Cục Hải Quan Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bắc Ninh' WHERE ID='Z18Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z18Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z18Z', N'Cục Hải Quan Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bắc Ninh' WHERE ID='Z18Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z18Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z18Z', N'Cục Hải Quan Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bắc Ninh' WHERE ID='Z18Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z18Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z18Z', N'Cục Hải Quan Bắc Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bắc Ninh' WHERE ID='Z18Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z37Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z37Z', N'Cục Hải Quan Bình Định', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Định' WHERE ID='Z37Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z37Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z37Z', N'Cục Hải Quan Bình Định', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Định' WHERE ID='Z37Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z43Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z43Z', N'Cục Hải Quan Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Dương' WHERE ID='Z43Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z43Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z43Z', N'Cục Hải Quan Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Dương' WHERE ID='Z43Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z43Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z43Z', N'Cục Hải Quan Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Dương' WHERE ID='Z43Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z43Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z43Z', N'Cục Hải Quan Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Dương' WHERE ID='Z43Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z43Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z43Z', N'Cục Hải Quan Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Dương' WHERE ID='Z43Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z43Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z43Z', N'Cục Hải Quan Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Dương' WHERE ID='Z43Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z43Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z43Z', N'Cục Hải Quan Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Dương' WHERE ID='Z43Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z43Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z43Z', N'Cục Hải Quan Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Dương' WHERE ID='Z43Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z43Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z43Z', N'Cục Hải Quan Bình Dương', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Dương' WHERE ID='Z43Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z61Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z61Z', N'Cục Hải Quan Bình Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Phước' WHERE ID='Z61Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z61Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z61Z', N'Cục Hải Quan Bình Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Phước' WHERE ID='Z61Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z61Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z61Z', N'Cục Hải Quan Bình Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Phước' WHERE ID='Z61Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z61Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z61Z', N'Cục Hải Quan Bình Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Phước' WHERE ID='Z61Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z61Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z61Z', N'Cục Hải Quan Bình Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Phước' WHERE ID='Z61Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z61Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z61Z', N'Cục Hải Quan Bình Phước', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Bình Phước' WHERE ID='Z61Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z59Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z59Z', N'Cục Hải Quan Cà Mau', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cà Mau' WHERE ID='Z59Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z59Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z59Z', N'Cục Hải Quan Cà Mau', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cà Mau' WHERE ID='Z59Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z54Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z54Z', N'Cục Hải Quan Cần Thơ', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cần Thơ' WHERE ID='Z54Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z54Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z54Z', N'Cục Hải Quan Cần Thơ', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cần Thơ' WHERE ID='Z54Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z54Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z54Z', N'Cục Hải Quan Cần Thơ', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cần Thơ' WHERE ID='Z54Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z54Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z54Z', N'Cục Hải Quan Cần Thơ', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cần Thơ' WHERE ID='Z54Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z11Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z11Z', N'Cục Hải Quan Cao Bằng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cao Bằng' WHERE ID='Z11Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z11Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z11Z', N'Cục Hải Quan Cao Bằng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cao Bằng' WHERE ID='Z11Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z11Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z11Z', N'Cục Hải Quan Cao Bằng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cao Bằng' WHERE ID='Z11Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z11Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z11Z', N'Cục Hải Quan Cao Bằng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cao Bằng' WHERE ID='Z11Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z11Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z11Z', N'Cục Hải Quan Cao Bằng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cao Bằng' WHERE ID='Z11Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z11Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z11Z', N'Cục Hải Quan Cao Bằng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cao Bằng' WHERE ID='Z11Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z11Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z11Z', N'Cục Hải Quan Cao Bằng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cao Bằng' WHERE ID='Z11Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z11Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z11Z', N'Cục Hải Quan Cao Bằng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Cao Bằng' WHERE ID='Z11Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z34Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z34Z', N'Cục Hải Quan Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đà Nẵng' WHERE ID='Z34Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z34Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z34Z', N'Cục Hải Quan Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đà Nẵng' WHERE ID='Z34Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z34Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z34Z', N'Cục Hải Quan Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đà Nẵng' WHERE ID='Z34Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z34Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z34Z', N'Cục Hải Quan Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đà Nẵng' WHERE ID='Z34Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z34Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z34Z', N'Cục Hải Quan Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đà Nẵng' WHERE ID='Z34Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z34Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z34Z', N'Cục Hải Quan Đà Nẵng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đà Nẵng' WHERE ID='Z34Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z40Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z40Z', N'Cục Hải Quan Đắk Lắk', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đắk Lắk' WHERE ID='Z40Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z40Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z40Z', N'Cục Hải Quan Đắk Lắk', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đắk Lắk' WHERE ID='Z40Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z40Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z40Z', N'Cục Hải Quan Đắk Lắk', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đắk Lắk' WHERE ID='Z40Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z12Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z12Z', N'Cục Hải Quan Điện Biên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Điện Biên' WHERE ID='Z12Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z12Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z12Z', N'Cục Hải Quan Điện Biên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Điện Biên' WHERE ID='Z12Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z12Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z12Z', N'Cục Hải Quan Điện Biên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Điện Biên' WHERE ID='Z12Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z12Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z12Z', N'Cục Hải Quan Điện Biên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Điện Biên' WHERE ID='Z12Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z12Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z12Z', N'Cục Hải Quan Điện Biên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Điện Biên' WHERE ID='Z12Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z12Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z12Z', N'Cục Hải Quan Điện Biên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Điện Biên' WHERE ID='Z12Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z12Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z12Z', N'Cục Hải Quan Điện Biên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Điện Biên' WHERE ID='Z12Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z12Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z12Z', N'Cục Hải Quan Điện Biên', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Điện Biên' WHERE ID='Z12Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z47Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z47Z', N'Cục Hải Quan Đồng Nai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Nai' WHERE ID='Z47Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z49Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z49Z', N'Cục Hải Quan Đồng Tháp', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Tháp' WHERE ID='Z49Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z49Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z49Z', N'Cục Hải Quan Đồng Tháp', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Tháp' WHERE ID='Z49Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z49Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z49Z', N'Cục Hải Quan Đồng Tháp', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Tháp' WHERE ID='Z49Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z49Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z49Z', N'Cục Hải Quan Đồng Tháp', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Tháp' WHERE ID='Z49Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z49Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z49Z', N'Cục Hải Quan Đồng Tháp', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Tháp' WHERE ID='Z49Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z49Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z49Z', N'Cục Hải Quan Đồng Tháp', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Đồng Tháp' WHERE ID='Z49Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z38Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z38Z', N'Cục Hải Quan Gia Lai - Kon Tum', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Gia Lai - Kon Tum' WHERE ID='Z38Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z38Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z38Z', N'Cục Hải Quan Gia Lai - Kon Tum', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Gia Lai - Kon Tum' WHERE ID='Z38Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z38Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z38Z', N'Cục Hải Quan Gia Lai - Kon Tum', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Gia Lai - Kon Tum' WHERE ID='Z38Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z38Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z38Z', N'Cục Hải Quan Gia Lai - Kon Tum', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Gia Lai - Kon Tum' WHERE ID='Z38Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z10Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z10Z', N'Cục Hải Quan Hà Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Giang' WHERE ID='Z10Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z10Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z10Z', N'Cục Hải Quan Hà Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Giang' WHERE ID='Z10Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z10Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z10Z', N'Cục Hải Quan Hà Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Giang' WHERE ID='Z10Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z10Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z10Z', N'Cục Hải Quan Hà Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Giang' WHERE ID='Z10Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z10Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z10Z', N'Cục Hải Quan Hà Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Giang' WHERE ID='Z10Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z01Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z01Z', N'Cục Hải Quan Hà Nội', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nội' WHERE ID='Z01Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z30Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z30Z', N'Cục Hải Quan Hà Tĩnh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Tĩnh' WHERE ID='Z30Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z30Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z30Z', N'Cục Hải Quan Hà Tĩnh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Tĩnh' WHERE ID='Z30Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z30Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z30Z', N'Cục Hải Quan Hà Tĩnh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Tĩnh' WHERE ID='Z30Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z30Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z30Z', N'Cục Hải Quan Hà Tĩnh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Tĩnh' WHERE ID='Z30Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z30Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z30Z', N'Cục Hải Quan Hà Tĩnh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Tĩnh' WHERE ID='Z30Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z30Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z30Z', N'Cục Hải Quan Hà Tĩnh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Tĩnh' WHERE ID='Z30Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z03Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z03Z', N'Cục Hải Quan Hải Phòng', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hải Phòng' WHERE ID='Z03Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z41Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z41Z', N'Cục Hải Quan Khánh Hòa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Khánh Hòa' WHERE ID='Z41Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z41Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z41Z', N'Cục Hải Quan Khánh Hòa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Khánh Hòa' WHERE ID='Z41Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z41Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z41Z', N'Cục Hải Quan Khánh Hòa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Khánh Hòa' WHERE ID='Z41Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z41Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z41Z', N'Cục Hải Quan Khánh Hòa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Khánh Hòa' WHERE ID='Z41Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z41Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z41Z', N'Cục Hải Quan Khánh Hòa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Khánh Hòa' WHERE ID='Z41Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z53Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z53Z', N'Cục Hải Quan Kiên Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Kiên Giang' WHERE ID='Z53Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z53Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z53Z', N'Cục Hải Quan Kiên Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Kiên Giang' WHERE ID='Z53Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z53Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z53Z', N'Cục Hải Quan Kiên Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Kiên Giang' WHERE ID='Z53Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z53Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z53Z', N'Cục Hải Quan Kiên Giang', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Kiên Giang' WHERE ID='Z53Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z15Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z15Z', N'Cục Hải Quan Lạng Sơn', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lạng Sơn' WHERE ID='Z15Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z13Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z13Z', N'Cục Hải Quan Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lào Cai' WHERE ID='Z13Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z13Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z13Z', N'Cục Hải Quan Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lào Cai' WHERE ID='Z13Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z13Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z13Z', N'Cục Hải Quan Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lào Cai' WHERE ID='Z13Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z13Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z13Z', N'Cục Hải Quan Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lào Cai' WHERE ID='Z13Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z13Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z13Z', N'Cục Hải Quan Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lào Cai' WHERE ID='Z13Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z13Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z13Z', N'Cục Hải Quan Lào Cai', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Lào Cai' WHERE ID='Z13Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z48Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z48Z', N'Cục Hải Quan Long An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Long An' WHERE ID='Z48Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z48Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z48Z', N'Cục Hải Quan Long An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Long An' WHERE ID='Z48Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z48Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z48Z', N'Cục Hải Quan Long An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Long An' WHERE ID='Z48Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z48Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z48Z', N'Cục Hải Quan Long An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Long An' WHERE ID='Z48Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z48Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z48Z', N'Cục Hải Quan Long An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Long An' WHERE ID='Z48Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z48Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z48Z', N'Cục Hải Quan Long An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Long An' WHERE ID='Z48Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z48Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z48Z', N'Cục Hải Quan Long An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Long An' WHERE ID='Z48Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z48Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z48Z', N'Cục Hải Quan Long An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Long An' WHERE ID='Z48Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z29Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z29Z', N'Cục Hải Quan Nghệ An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Nghệ An' WHERE ID='Z29Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z29Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z29Z', N'Cục Hải Quan Nghệ An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Nghệ An' WHERE ID='Z29Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z29Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z29Z', N'Cục Hải Quan Nghệ An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Nghệ An' WHERE ID='Z29Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z29Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z29Z', N'Cục Hải Quan Nghệ An', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Nghệ An' WHERE ID='Z29Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z31Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z31Z', N'Cục Hải Quan Quảng Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Bình' WHERE ID='Z31Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z31Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z31Z', N'Cục Hải Quan Quảng Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Bình' WHERE ID='Z31Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z31Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z31Z', N'Cục Hải Quan Quảng Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Bình' WHERE ID='Z31Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z31Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z31Z', N'Cục Hải Quan Quảng Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Bình' WHERE ID='Z31Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z31Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z31Z', N'Cục Hải Quan Quảng Bình', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Bình' WHERE ID='Z31Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z60Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z60Z', N'Cục Hải Quan Quảng Nam', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Nam' WHERE ID='Z60Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z60Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z60Z', N'Cục Hải Quan Quảng Nam', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Nam' WHERE ID='Z60Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z60Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z60Z', N'Cục Hải Quan Quảng Nam', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Nam' WHERE ID='Z60Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z60Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z60Z', N'Cục Hải Quan Quảng Nam', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Nam' WHERE ID='Z60Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z35Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z35Z', N'Cục Hải Quan Quảng Ngãi', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ngãi' WHERE ID='Z35Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z35Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z35Z', N'Cục Hải Quan Quảng Ngãi', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ngãi' WHERE ID='Z35Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z20Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z20Z', N'Cục Hải Quan Quảng Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ninh' WHERE ID='Z20Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z20Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z20Z', N'Cục Hải Quan Quảng Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ninh' WHERE ID='Z20Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z20Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z20Z', N'Cục Hải Quan Quảng Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ninh' WHERE ID='Z20Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z20Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z20Z', N'Cục Hải Quan Quảng Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ninh' WHERE ID='Z20Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z20Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z20Z', N'Cục Hải Quan Quảng Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ninh' WHERE ID='Z20Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z20Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z20Z', N'Cục Hải Quan Quảng Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ninh' WHERE ID='Z20Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z20Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z20Z', N'Cục Hải Quan Quảng Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ninh' WHERE ID='Z20Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z20Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z20Z', N'Cục Hải Quan Quảng Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Ninh' WHERE ID='Z20Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z32Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z32Z', N'Cục Hải Quan Quảng Trị', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Trị' WHERE ID='Z32Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z32Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z32Z', N'Cục Hải Quan Quảng Trị', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Trị' WHERE ID='Z32Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z32Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z32Z', N'Cục Hải Quan Quảng Trị', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Trị' WHERE ID='Z32Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z32Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z32Z', N'Cục Hải Quan Quảng Trị', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Trị' WHERE ID='Z32Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z32Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z32Z', N'Cục Hải Quan Quảng Trị', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Quảng Trị' WHERE ID='Z32Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z45Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z45Z', N'Cục Hải Quan Tây Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Tây Ninh' WHERE ID='Z45Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z45Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z45Z', N'Cục Hải Quan Tây Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Tây Ninh' WHERE ID='Z45Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z45Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z45Z', N'Cục Hải Quan Tây Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Tây Ninh' WHERE ID='Z45Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z45Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z45Z', N'Cục Hải Quan Tây Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Tây Ninh' WHERE ID='Z45Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z45Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z45Z', N'Cục Hải Quan Tây Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Tây Ninh' WHERE ID='Z45Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z45Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z45Z', N'Cục Hải Quan Tây Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Tây Ninh' WHERE ID='Z45Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z45Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z45Z', N'Cục Hải Quan Tây Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Tây Ninh' WHERE ID='Z45Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z45Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z45Z', N'Cục Hải Quan Tây Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Tây Ninh' WHERE ID='Z45Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z27Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z27Z', N'Cục Hải Quan Thanh Hóa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Thanh Hóa' WHERE ID='Z27Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z27Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z27Z', N'Cục Hải Quan Thanh Hóa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Thanh Hóa' WHERE ID='Z27Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z27Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z27Z', N'Cục Hải Quan Thanh Hóa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Thanh Hóa' WHERE ID='Z27Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z27Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z27Z', N'Cục Hải Quan Thanh Hóa', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Thanh Hóa' WHERE ID='Z27Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z28Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z28Z', N'Cục Hải Quan Hà Nam Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nam Ninh' WHERE ID='Z28Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z28Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z28Z', N'Cục Hải Quan Hà Nam Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nam Ninh' WHERE ID='Z28Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z28Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z28Z', N'Cục Hải Quan Hà Nam Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nam Ninh' WHERE ID='Z28Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z28Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z28Z', N'Cục Hải Quan Hà Nam Ninh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Hà Nam Ninh' WHERE ID='Z28Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z33Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z33Z', N'Cục Hải Quan Thừa Thiên - Huế', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Thừa Thiên - Huế' WHERE ID='Z33Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z33Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z33Z', N'Cục Hải Quan Thừa Thiên - Huế', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Thừa Thiên - Huế' WHERE ID='Z33Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z33Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z33Z', N'Cục Hải Quan Thừa Thiên - Huế', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Thừa Thiên - Huế' WHERE ID='Z33Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z33Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z33Z', N'Cục Hải Quan Thừa Thiên - Huế', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Thừa Thiên - Huế' WHERE ID='Z33Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z33Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z33Z', N'Cục Hải Quan Thừa Thiên - Huế', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan Thừa Thiên - Huế' WHERE ID='Z33Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END
IF NOT EXISTS(SELECT * FROM dbo.t_HaiQuan_DonViHaiQuan WHERE ID='Z02Z')
BEGIN
    INSERT INTO dbo.t_HaiQuan_DonViHaiQuan ( ID, Ten, DateCreated, DateModified ) VALUES  ( 'Z02Z', N'Cục Hải Quan TP Hồ Chí Minh', GETDATE(),GETDATE())
END
ELSE
BEGIN
    UPDATE t_HaiQuan_DonViHaiQuan SET Ten=N'Cục Hải Quan TP Hồ Chí Minh' WHERE ID='Z02Z'
END

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '21.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('21.3',GETDATE(), N'CẬP NHẬT BẢNG MÃ CHI CỤC HẢI QUAN')
END