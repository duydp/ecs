namespace Company.Interface.Report.VNACCS
{
    partial class BanChiThiSuaDoiCuaHaiQuan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuThiTHHetHan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiCaNhanToChuc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaHieuPhuongThucVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiHangHoa = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoiHanTaiNhapTaiXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBoPhanXuLyToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSoHangHoaDaiDienToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoToKhaiChiaNho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoNhanhToKhaiChiaNho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioThayDoiDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayThayDoiDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl1111 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenCoQuanHaiQuanTiepNhanToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiKiemTra = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiDauTien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiTamNhapTaiXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTChiThiHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaychithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoidungchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblPhanLoaiChiThiHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel32,
            this.xrLabel31,
            this.xrLabel10,
            this.lblBieuThiTHHetHan,
            this.lblPhanLoaiCaNhanToChuc,
            this.lblMaHieuPhuongThucVanChuyen,
            this.lblMaPhanLoaiHangHoa,
            this.lblThoiHanTaiNhapTaiXuat,
            this.lblMaBoPhanXuLyToKhai,
            this.lblMaSoHangHoaDaiDienToKhai,
            this.lblTongSoToKhaiChiaNho,
            this.lblSoNhanhToKhaiChiaNho,
            this.lblGioThayDoiDangKy,
            this.lblNgayThayDoiDangKy,
            this.xrLabel21,
            this.xrLabel20,
            this.lbl1111,
            this.lblGioDangKy,
            this.xrLabel16,
            this.lblMaLoaiHinh,
            this.lblTenCoQuanHaiQuanTiepNhanToKhai,
            this.xrLabel14,
            this.lblNgayDangKy,
            this.lblMaPhanLoaiKiemTra,
            this.lblSoToKhaiDauTien,
            this.xrLabel9,
            this.lblSoToKhaiTamNhapTaiXuat,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.lblSoToKhai,
            this.xrLabel1,
            this.xrLine1});
            this.TopMargin.HeightF = 131.2916F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 44.54165F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(96.88F, 10F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Số tờ khai";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(732.9999F, 84.54167F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(13F, 10F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "-";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(532.705F, 44.54165F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(10.21F, 10F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "/";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(493.7567F, 44.54165F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "-";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblBieuThiTHHetHan
            // 
            this.lblBieuThiTHHetHan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblBieuThiTHHetHan.LocationFloat = new DevExpress.Utils.PointFloat(746F, 84.54167F);
            this.lblBieuThiTHHetHan.Name = "lblBieuThiTHHetHan";
            this.lblBieuThiTHHetHan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuThiTHHetHan.SizeF = new System.Drawing.SizeF(16F, 10F);
            this.lblBieuThiTHHetHan.StylePriority.UseFont = false;
            this.lblBieuThiTHHetHan.StylePriority.UseTextAlignment = false;
            this.lblBieuThiTHHetHan.Text = "X";
            this.lblBieuThiTHHetHan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiCaNhanToChuc
            // 
            this.lblPhanLoaiCaNhanToChuc.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiCaNhanToChuc.LocationFloat = new DevExpress.Utils.PointFloat(411.7416F, 64.54166F);
            this.lblPhanLoaiCaNhanToChuc.Name = "lblPhanLoaiCaNhanToChuc";
            this.lblPhanLoaiCaNhanToChuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiCaNhanToChuc.SizeF = new System.Drawing.SizeF(33F, 10F);
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseFont = false;
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiCaNhanToChuc.Text = "[ X ]";
            this.lblPhanLoaiCaNhanToChuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaHieuPhuongThucVanChuyen
            // 
            this.lblMaHieuPhuongThucVanChuyen.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaHieuPhuongThucVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(395.1916F, 64.54166F);
            this.lblMaHieuPhuongThucVanChuyen.Name = "lblMaHieuPhuongThucVanChuyen";
            this.lblMaHieuPhuongThucVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHieuPhuongThucVanChuyen.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseFont = false;
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblMaHieuPhuongThucVanChuyen.Text = "X";
            this.lblMaHieuPhuongThucVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiHangHoa
            // 
            this.lblMaPhanLoaiHangHoa.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaPhanLoaiHangHoa.LocationFloat = new DevExpress.Utils.PointFloat(378.6416F, 64.54166F);
            this.lblMaPhanLoaiHangHoa.Name = "lblMaPhanLoaiHangHoa";
            this.lblMaPhanLoaiHangHoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiHangHoa.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaPhanLoaiHangHoa.StylePriority.UseFont = false;
            this.lblMaPhanLoaiHangHoa.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiHangHoa.Text = "X";
            this.lblMaPhanLoaiHangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThoiHanTaiNhapTaiXuat
            // 
            this.lblThoiHanTaiNhapTaiXuat.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblThoiHanTaiNhapTaiXuat.LocationFloat = new DevExpress.Utils.PointFloat(657.4984F, 84.54167F);
            this.lblThoiHanTaiNhapTaiXuat.Name = "lblThoiHanTaiNhapTaiXuat";
            this.lblThoiHanTaiNhapTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoiHanTaiNhapTaiXuat.SizeF = new System.Drawing.SizeF(75F, 10F);
            this.lblThoiHanTaiNhapTaiXuat.StylePriority.UseFont = false;
            this.lblThoiHanTaiNhapTaiXuat.StylePriority.UseTextAlignment = false;
            this.lblThoiHanTaiNhapTaiXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaBoPhanXuLyToKhai
            // 
            this.lblMaBoPhanXuLyToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaBoPhanXuLyToKhai.LocationFloat = new DevExpress.Utils.PointFloat(657.5032F, 74.54166F);
            this.lblMaBoPhanXuLyToKhai.Name = "lblMaBoPhanXuLyToKhai";
            this.lblMaBoPhanXuLyToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBoPhanXuLyToKhai.SizeF = new System.Drawing.SizeF(23.96F, 10F);
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseFont = false;
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseTextAlignment = false;
            this.lblMaBoPhanXuLyToKhai.Text = "XE";
            this.lblMaBoPhanXuLyToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSoHangHoaDaiDienToKhai
            // 
            this.lblMaSoHangHoaDaiDienToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSoHangHoaDaiDienToKhai.LocationFloat = new DevExpress.Utils.PointFloat(657.5034F, 64.54166F);
            this.lblMaSoHangHoaDaiDienToKhai.Name = "lblMaSoHangHoaDaiDienToKhai";
            this.lblMaSoHangHoaDaiDienToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSoHangHoaDaiDienToKhai.SizeF = new System.Drawing.SizeF(41.67F, 10F);
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseFont = false;
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseTextAlignment = false;
            this.lblMaSoHangHoaDaiDienToKhai.Text = "XXXE";
            this.lblMaSoHangHoaDaiDienToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoToKhaiChiaNho
            // 
            this.lblTongSoToKhaiChiaNho.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongSoToKhaiChiaNho.LocationFloat = new DevExpress.Utils.PointFloat(542.9182F, 44.54165F);
            this.lblTongSoToKhaiChiaNho.Name = "lblTongSoToKhaiChiaNho";
            this.lblTongSoToKhaiChiaNho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoToKhaiChiaNho.SizeF = new System.Drawing.SizeF(22.08F, 10F);
            this.lblTongSoToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblTongSoToKhaiChiaNho.StylePriority.UseTextAlignment = false;
            this.lblTongSoToKhaiChiaNho.Text = "NE";
            this.lblTongSoToKhaiChiaNho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoNhanhToKhaiChiaNho
            // 
            this.lblSoNhanhToKhaiChiaNho.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoNhanhToKhaiChiaNho.LocationFloat = new DevExpress.Utils.PointFloat(508.5468F, 44.54165F);
            this.lblSoNhanhToKhaiChiaNho.Name = "lblSoNhanhToKhaiChiaNho";
            this.lblSoNhanhToKhaiChiaNho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoNhanhToKhaiChiaNho.SizeF = new System.Drawing.SizeF(24.15637F, 10F);
            this.lblSoNhanhToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblSoNhanhToKhaiChiaNho.StylePriority.UseTextAlignment = false;
            this.lblSoNhanhToKhaiChiaNho.Text = "NE";
            this.lblSoNhanhToKhaiChiaNho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioThayDoiDangKy
            // 
            this.lblGioThayDoiDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioThayDoiDangKy.LocationFloat = new DevExpress.Utils.PointFloat(446.2883F, 84.54167F);
            this.lblGioThayDoiDangKy.Name = "lblGioThayDoiDangKy";
            this.lblGioThayDoiDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioThayDoiDangKy.SizeF = new System.Drawing.SizeF(60F, 10F);
            this.lblGioThayDoiDangKy.StylePriority.UseFont = false;
            this.lblGioThayDoiDangKy.StylePriority.UseTextAlignment = false;
            this.lblGioThayDoiDangKy.Text = "hh:mm:ss";
            this.lblGioThayDoiDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayThayDoiDangKy
            // 
            this.lblNgayThayDoiDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayThayDoiDangKy.LocationFloat = new DevExpress.Utils.PointFloat(366.0849F, 84.54167F);
            this.lblNgayThayDoiDangKy.Name = "lblNgayThayDoiDangKy";
            this.lblNgayThayDoiDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayThayDoiDangKy.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblNgayThayDoiDangKy.StylePriority.UseFont = false;
            this.lblNgayThayDoiDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayThayDoiDangKy.Text = "dd/MM/yyyy";
            this.lblNgayThayDoiDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(508.5383F, 84.54167F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(148.96F, 10F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(508.5433F, 74.54166F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(148.96F, 10F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Mã bộ phận xử lý tờ khai";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl1111
            // 
            this.lbl1111.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1111.LocationFloat = new DevExpress.Utils.PointFloat(452.2933F, 64.54166F);
            this.lbl1111.Name = "lbl1111";
            this.lbl1111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl1111.SizeF = new System.Drawing.SizeF(205.21F, 10F);
            this.lbl1111.StylePriority.UseFont = false;
            this.lbl1111.StylePriority.UseTextAlignment = false;
            this.lbl1111.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.lbl1111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioDangKy
            // 
            this.lblGioDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioDangKy.LocationFloat = new DevExpress.Utils.PointFloat(177.08F, 84.54167F);
            this.lblGioDangKy.Name = "lblGioDangKy";
            this.lblGioDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioDangKy.SizeF = new System.Drawing.SizeF(60F, 10F);
            this.lblGioDangKy.StylePriority.UseFont = false;
            this.lblGioDangKy.StylePriority.UseTextAlignment = false;
            this.lblGioDangKy.Text = "hh:mm:ss";
            this.lblGioDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(237.085F, 84.54167F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(129F, 10F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Ngày thay đổi đăng ký";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaLoaiHinh.LocationFloat = new DevExpress.Utils.PointFloat(343.3416F, 64.54166F);
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaLoaiHinh.SizeF = new System.Drawing.SizeF(35.3F, 10F);
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenCoQuanHaiQuanTiepNhanToKhai
            // 
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 74.54166F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Name = "lblTenCoQuanHaiQuanTiepNhanToKhai";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.SizeF = new System.Drawing.SizeF(102F, 10F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseFont = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseTextAlignment = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Text = "XXXXXXXXXE";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 64.54166F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(106.25F, 10F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Mã loại hình";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayDangKy.LocationFloat = new DevExpress.Utils.PointFloat(96.455F, 84.54167F);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblNgayDangKy.StylePriority.UseFont = false;
            this.lblNgayDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayDangKy.Text = "dd/MM/yyyy";
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiKiemTra
            // 
            this.lblMaPhanLoaiKiemTra.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaPhanLoaiKiemTra.LocationFloat = new DevExpress.Utils.PointFloat(152.71F, 64.54166F);
            this.lblMaPhanLoaiKiemTra.Name = "lblMaPhanLoaiKiemTra";
            this.lblMaPhanLoaiKiemTra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiKiemTra.SizeF = new System.Drawing.SizeF(39.59F, 10F);
            this.lblMaPhanLoaiKiemTra.StylePriority.UseFont = false;
            this.lblMaPhanLoaiKiemTra.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiKiemTra.Text = "XX E";
            this.lblMaPhanLoaiKiemTra.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiDauTien
            // 
            this.lblSoToKhaiDauTien.AutoWidth = true;
            this.lblSoToKhaiDauTien.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhaiDauTien.LocationFloat = new DevExpress.Utils.PointFloat(368.34F, 44.54165F);
            this.lblSoToKhaiDauTien.Name = "lblSoToKhaiDauTien";
            this.lblSoToKhaiDauTien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiDauTien.SizeF = new System.Drawing.SizeF(125.4168F, 10F);
            this.lblSoToKhaiDauTien.StylePriority.UseFont = false;
            this.lblSoToKhaiDauTien.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiDauTien.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiDauTien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.AutoWidth = true;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 44.54165F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(131.25F, 10F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Số tờ khai đầu tiên";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiTamNhapTaiXuat
            // 
            this.lblSoToKhaiTamNhapTaiXuat.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhaiTamNhapTaiXuat.LocationFloat = new DevExpress.Utils.PointFloat(237.085F, 54.54165F);
            this.lblSoToKhaiTamNhapTaiXuat.Name = "lblSoToKhaiTamNhapTaiXuat";
            this.lblSoToKhaiTamNhapTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiTamNhapTaiXuat.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseFont = false;
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiTamNhapTaiXuat.Text = "NNNNNNNNN1NE";
            this.lblSoToKhaiTamNhapTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(9.995015F, 84.54167F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(86.46F, 10F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Ngày đăng ký";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 74.54166F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(227.09F, 10F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 64.54166F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(142.71F, 10F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Mã phân loại kiểm tra";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 54.54165F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(227.085F, 10F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhai.LocationFloat = new DevExpress.Utils.PointFloat(106.88F, 44.54165F);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.SizeF = new System.Drawing.SizeF(130.2F, 10F);
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = "NNNNNNNNN1NE";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(250F, 12.5F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(269.7416F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Bản chỉ thị sửa đổi của Hải quan";
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(1.409944F, 94.54168F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 57.20825F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader,
            this.ReportFooter});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail1.HeightF = 35.22873F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(23.95834F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(731.04F, 35F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTChiThiHQ,
            this.lblNgaychithihaiquan1,
            this.lblTenchithihaiquan1,
            this.lblNoidungchithihaiquan1});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // lblSTTChiThiHQ
            // 
            this.lblSTTChiThiHQ.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSTTChiThiHQ.Name = "lblSTTChiThiHQ";
            this.lblSTTChiThiHQ.StylePriority.UseFont = false;
            this.lblSTTChiThiHQ.StylePriority.UseTextAlignment = false;
            this.lblSTTChiThiHQ.Text = "1";
            this.lblSTTChiThiHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTTChiThiHQ.Weight = 0.0849716401675839;
            // 
            // lblNgaychithihaiquan1
            // 
            this.lblNgaychithihaiquan1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaychithihaiquan1.Name = "lblNgaychithihaiquan1";
            this.lblNgaychithihaiquan1.StylePriority.UseFont = false;
            this.lblNgaychithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNgaychithihaiquan1.Text = "dd/MM/yyyy";
            this.lblNgaychithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaychithihaiquan1.Weight = 0.35712192295232159;
            // 
            // lblTenchithihaiquan1
            // 
            this.lblTenchithihaiquan1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenchithihaiquan1.Multiline = true;
            this.lblTenchithihaiquan1.Name = "lblTenchithihaiquan1";
            this.lblTenchithihaiquan1.StylePriority.UseFont = false;
            this.lblTenchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblTenchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
            this.lblTenchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTenchithihaiquan1.Weight = 0.83951528020254451;
            // 
            // lblNoidungchithihaiquan1
            // 
            this.lblNoidungchithihaiquan1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblNoidungchithihaiquan1.Multiline = true;
            this.lblNoidungchithihaiquan1.Name = "lblNoidungchithihaiquan1";
            this.lblNoidungchithihaiquan1.StylePriority.UseFont = false;
            this.lblNoidungchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNoidungchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWW0WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
            this.lblNoidungchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblNoidungchithihaiquan1.Weight = 1.9577792374061049;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblPhanLoaiChiThiHQ,
            this.xrTable2,
            this.xrLabel8});
            this.ReportHeader.HeightF = 24.07945F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblPhanLoaiChiThiHQ
            // 
            this.lblPhanLoaiChiThiHQ.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiChiThiHQ.LocationFloat = new DevExpress.Utils.PointFloat(175F, 0F);
            this.lblPhanLoaiChiThiHQ.Name = "lblPhanLoaiChiThiHQ";
            this.lblPhanLoaiChiThiHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiChiThiHQ.SizeF = new System.Drawing.SizeF(37.49994F, 11F);
            this.lblPhanLoaiChiThiHQ.StylePriority.UseFont = false;
            this.lblPhanLoaiChiThiHQ.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiChiThiHQ.Text = "X";
            this.lblPhanLoaiChiThiHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(42.48369F, 12.95833F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(712.52F, 11F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.6742694161155007;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Ngày";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell4.Weight = 0.37264495600631686;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Tên";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell5.Weight = 0.86900334220179032;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Nội dung";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell6.Weight = 2.0265392740732286;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(11.45833F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(163.5417F, 11F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = "Phân loại chỉ thị của Hải quan";
            // 
            // ReportFooter
            // 
            this.ReportFooter.HeightF = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // BanChiThiSuaDoiCuaHaiQuan
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(53, 27, 131, 57);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblBieuThiTHHetHan;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiCaNhanToChuc;
        private DevExpress.XtraReports.UI.XRLabel lblMaHieuPhuongThucVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiHangHoa;
        private DevExpress.XtraReports.UI.XRLabel lblThoiHanTaiNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRLabel lblMaBoPhanXuLyToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblMaSoHangHoaDaiDienToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRLabel lblSoNhanhToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRLabel lblGioThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblNgayThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lbl1111;
        private DevExpress.XtraReports.UI.XRLabel lblGioDangKy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel lblTenCoQuanHaiQuanTiepNhanToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiKiemTra;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiDauTien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiTamNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lblGioKhaiBaoNopThue;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiChiThiHQ;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTChiThiHQ;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaychithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell lblTenchithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell lblNoidungchithihaiquan1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
    }
}
