-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@TenThongTinXuat nvarchar(43),
	@MaPhanLoaiXuLy varchar(1),
	@MaCoBaoNiemPhong varchar(1),
	@TenCoBaoNiemPhong nvarchar(35),
	@CoQuanHaiQuan varchar(10),
	@SoToKhaiVC numeric(12, 0),
	@CoBaoXuatNhapKhau varchar(1),
	@NgayLapToKhai datetime,
	@MaNguoiKhai varchar(5),
	@TenNguoiKhai varchar(50),
	@DiaChiNguoiKhai nvarchar(100),
	@MaNguoiVC varchar(13),
	@TenNguoiVC nvarchar(100),
	@DiaChiNguoiVC nvarchar(100),
	@SoHopDongVC varchar(11),
	@NgayHopDongVC datetime,
	@NgayHetHanHopDongVC datetime,
	@MaPhuongTienVC varchar(2),
	@TenPhuongTieVC varchar(12),
	@MaMucDichVC varchar(3),
	@TenMucDichVC nvarchar(70),
	@LoaiHinhVanTai varchar(2),
	@TenLoaiHinhVanTai nvarchar(70),
	@MaDiaDiemXepHang varchar(7),
	@MaViTriXepHang varchar(6),
	@MaCangCuaKhauGaXepHang varchar(6),
	@MaCangXHKhongCo_HT varchar(1),
	@DiaDiemXepHang nvarchar(35),
	@NgayDenDiaDiem_XH datetime,
	@MaDiaDiemDoHang varchar(7),
	@MaViTriDoHang varchar(6),
	@MaCangCuaKhauGaDoHang varchar(6),
	@MaCangDHKhongCo_HT varchar(1),
	@DiaDiemDoHang nvarchar(35),
	@NgayDenDiaDiem_DH datetime,
	@TuyenDuongVC varchar(35),
	@LoaiBaoLanh varchar(1),
	@SoTienBaoLanh numeric(11, 0),
	@SoLuongCot_TK int,
	@SoLuongContainer int,
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh int,
	@KyHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@MaVach numeric(12, 0),
	@NgayPheDuyetVC datetime,
	@NgayDuKienBatDauVC datetime,
	@GioDuKienBatDauVC varchar(2),
	@NgayDuKienKetThucVC datetime,
	@GioDuKienKetThucVC varchar(2),
	@MaBuuChinhHQ varchar(7),
	@DiaChiBuuChinhHQ nvarchar(54),
	@TenBuuChinhHQ nvarchar(34),
	@GhiChu nvarchar(255),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
(
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@NgayDangKy,
	@TenThongTinXuat,
	@MaPhanLoaiXuLy,
	@MaCoBaoNiemPhong,
	@TenCoBaoNiemPhong,
	@CoQuanHaiQuan,
	@SoToKhaiVC,
	@CoBaoXuatNhapKhau,
	@NgayLapToKhai,
	@MaNguoiKhai,
	@TenNguoiKhai,
	@DiaChiNguoiKhai,
	@MaNguoiVC,
	@TenNguoiVC,
	@DiaChiNguoiVC,
	@SoHopDongVC,
	@NgayHopDongVC,
	@NgayHetHanHopDongVC,
	@MaPhuongTienVC,
	@TenPhuongTieVC,
	@MaMucDichVC,
	@TenMucDichVC,
	@LoaiHinhVanTai,
	@TenLoaiHinhVanTai,
	@MaDiaDiemXepHang,
	@MaViTriXepHang,
	@MaCangCuaKhauGaXepHang,
	@MaCangXHKhongCo_HT,
	@DiaDiemXepHang,
	@NgayDenDiaDiem_XH,
	@MaDiaDiemDoHang,
	@MaViTriDoHang,
	@MaCangCuaKhauGaDoHang,
	@MaCangDHKhongCo_HT,
	@DiaDiemDoHang,
	@NgayDenDiaDiem_DH,
	@TuyenDuongVC,
	@LoaiBaoLanh,
	@SoTienBaoLanh,
	@SoLuongCot_TK,
	@SoLuongContainer,
	@MaNganHangBaoLanh,
	@NamPhatHanhBaoLanh,
	@KyHieuChungTuBaoLanh,
	@SoChungTuBaoLanh,
	@MaVach,
	@NgayPheDuyetVC,
	@NgayDuKienBatDauVC,
	@GioDuKienBatDauVC,
	@NgayDuKienKetThucVC,
	@GioDuKienKetThucVC,
	@MaBuuChinhHQ,
	@DiaChiBuuChinhHQ,
	@TenBuuChinhHQ,
	@GhiChu,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@TenThongTinXuat nvarchar(43),
	@MaPhanLoaiXuLy varchar(1),
	@MaCoBaoNiemPhong varchar(1),
	@TenCoBaoNiemPhong nvarchar(35),
	@CoQuanHaiQuan varchar(10),
	@SoToKhaiVC numeric(12, 0),
	@CoBaoXuatNhapKhau varchar(1),
	@NgayLapToKhai datetime,
	@MaNguoiKhai varchar(5),
	@TenNguoiKhai varchar(50),
	@DiaChiNguoiKhai nvarchar(100),
	@MaNguoiVC varchar(13),
	@TenNguoiVC nvarchar(100),
	@DiaChiNguoiVC nvarchar(100),
	@SoHopDongVC varchar(11),
	@NgayHopDongVC datetime,
	@NgayHetHanHopDongVC datetime,
	@MaPhuongTienVC varchar(2),
	@TenPhuongTieVC varchar(12),
	@MaMucDichVC varchar(3),
	@TenMucDichVC nvarchar(70),
	@LoaiHinhVanTai varchar(2),
	@TenLoaiHinhVanTai nvarchar(70),
	@MaDiaDiemXepHang varchar(7),
	@MaViTriXepHang varchar(6),
	@MaCangCuaKhauGaXepHang varchar(6),
	@MaCangXHKhongCo_HT varchar(1),
	@DiaDiemXepHang nvarchar(35),
	@NgayDenDiaDiem_XH datetime,
	@MaDiaDiemDoHang varchar(7),
	@MaViTriDoHang varchar(6),
	@MaCangCuaKhauGaDoHang varchar(6),
	@MaCangDHKhongCo_HT varchar(1),
	@DiaDiemDoHang nvarchar(35),
	@NgayDenDiaDiem_DH datetime,
	@TuyenDuongVC varchar(35),
	@LoaiBaoLanh varchar(1),
	@SoTienBaoLanh numeric(11, 0),
	@SoLuongCot_TK int,
	@SoLuongContainer int,
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh int,
	@KyHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@MaVach numeric(12, 0),
	@NgayPheDuyetVC datetime,
	@NgayDuKienBatDauVC datetime,
	@GioDuKienBatDauVC varchar(2),
	@NgayDuKienKetThucVC datetime,
	@GioDuKienKetThucVC varchar(2),
	@MaBuuChinhHQ varchar(7),
	@DiaChiBuuChinhHQ nvarchar(54),
	@TenBuuChinhHQ nvarchar(34),
	@GhiChu nvarchar(255),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
SET
	[TKMD_ID] = @TKMD_ID,
	[NgayDangKy] = @NgayDangKy,
	[TenThongTinXuat] = @TenThongTinXuat,
	[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
	[MaCoBaoNiemPhong] = @MaCoBaoNiemPhong,
	[TenCoBaoNiemPhong] = @TenCoBaoNiemPhong,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[SoToKhaiVC] = @SoToKhaiVC,
	[CoBaoXuatNhapKhau] = @CoBaoXuatNhapKhau,
	[NgayLapToKhai] = @NgayLapToKhai,
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
	[MaNguoiVC] = @MaNguoiVC,
	[TenNguoiVC] = @TenNguoiVC,
	[DiaChiNguoiVC] = @DiaChiNguoiVC,
	[SoHopDongVC] = @SoHopDongVC,
	[NgayHopDongVC] = @NgayHopDongVC,
	[NgayHetHanHopDongVC] = @NgayHetHanHopDongVC,
	[MaPhuongTienVC] = @MaPhuongTienVC,
	[TenPhuongTieVC] = @TenPhuongTieVC,
	[MaMucDichVC] = @MaMucDichVC,
	[TenMucDichVC] = @TenMucDichVC,
	[LoaiHinhVanTai] = @LoaiHinhVanTai,
	[TenLoaiHinhVanTai] = @TenLoaiHinhVanTai,
	[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
	[MaViTriXepHang] = @MaViTriXepHang,
	[MaCangCuaKhauGaXepHang] = @MaCangCuaKhauGaXepHang,
	[MaCangXHKhongCo_HT] = @MaCangXHKhongCo_HT,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[NgayDenDiaDiem_XH] = @NgayDenDiaDiem_XH,
	[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
	[MaViTriDoHang] = @MaViTriDoHang,
	[MaCangCuaKhauGaDoHang] = @MaCangCuaKhauGaDoHang,
	[MaCangDHKhongCo_HT] = @MaCangDHKhongCo_HT,
	[DiaDiemDoHang] = @DiaDiemDoHang,
	[NgayDenDiaDiem_DH] = @NgayDenDiaDiem_DH,
	[TuyenDuongVC] = @TuyenDuongVC,
	[LoaiBaoLanh] = @LoaiBaoLanh,
	[SoTienBaoLanh] = @SoTienBaoLanh,
	[SoLuongCot_TK] = @SoLuongCot_TK,
	[SoLuongContainer] = @SoLuongContainer,
	[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
	[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
	[KyHieuChungTuBaoLanh] = @KyHieuChungTuBaoLanh,
	[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
	[MaVach] = @MaVach,
	[NgayPheDuyetVC] = @NgayPheDuyetVC,
	[NgayDuKienBatDauVC] = @NgayDuKienBatDauVC,
	[GioDuKienBatDauVC] = @GioDuKienBatDauVC,
	[NgayDuKienKetThucVC] = @NgayDuKienKetThucVC,
	[GioDuKienKetThucVC] = @GioDuKienKetThucVC,
	[MaBuuChinhHQ] = @MaBuuChinhHQ,
	[DiaChiBuuChinhHQ] = @DiaChiBuuChinhHQ,
	[TenBuuChinhHQ] = @TenBuuChinhHQ,
	[GhiChu] = @GhiChu,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@TenThongTinXuat nvarchar(43),
	@MaPhanLoaiXuLy varchar(1),
	@MaCoBaoNiemPhong varchar(1),
	@TenCoBaoNiemPhong nvarchar(35),
	@CoQuanHaiQuan varchar(10),
	@SoToKhaiVC numeric(12, 0),
	@CoBaoXuatNhapKhau varchar(1),
	@NgayLapToKhai datetime,
	@MaNguoiKhai varchar(5),
	@TenNguoiKhai varchar(50),
	@DiaChiNguoiKhai nvarchar(100),
	@MaNguoiVC varchar(13),
	@TenNguoiVC nvarchar(100),
	@DiaChiNguoiVC nvarchar(100),
	@SoHopDongVC varchar(11),
	@NgayHopDongVC datetime,
	@NgayHetHanHopDongVC datetime,
	@MaPhuongTienVC varchar(2),
	@TenPhuongTieVC varchar(12),
	@MaMucDichVC varchar(3),
	@TenMucDichVC nvarchar(70),
	@LoaiHinhVanTai varchar(2),
	@TenLoaiHinhVanTai nvarchar(70),
	@MaDiaDiemXepHang varchar(7),
	@MaViTriXepHang varchar(6),
	@MaCangCuaKhauGaXepHang varchar(6),
	@MaCangXHKhongCo_HT varchar(1),
	@DiaDiemXepHang nvarchar(35),
	@NgayDenDiaDiem_XH datetime,
	@MaDiaDiemDoHang varchar(7),
	@MaViTriDoHang varchar(6),
	@MaCangCuaKhauGaDoHang varchar(6),
	@MaCangDHKhongCo_HT varchar(1),
	@DiaDiemDoHang nvarchar(35),
	@NgayDenDiaDiem_DH datetime,
	@TuyenDuongVC varchar(35),
	@LoaiBaoLanh varchar(1),
	@SoTienBaoLanh numeric(11, 0),
	@SoLuongCot_TK int,
	@SoLuongContainer int,
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh int,
	@KyHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@MaVach numeric(12, 0),
	@NgayPheDuyetVC datetime,
	@NgayDuKienBatDauVC datetime,
	@GioDuKienBatDauVC varchar(2),
	@NgayDuKienKetThucVC datetime,
	@GioDuKienKetThucVC varchar(2),
	@MaBuuChinhHQ varchar(7),
	@DiaChiBuuChinhHQ nvarchar(54),
	@TenBuuChinhHQ nvarchar(34),
	@GhiChu nvarchar(255),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiVanChuyen] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiVanChuyen] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[NgayDangKy] = @NgayDangKy,
			[TenThongTinXuat] = @TenThongTinXuat,
			[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
			[MaCoBaoNiemPhong] = @MaCoBaoNiemPhong,
			[TenCoBaoNiemPhong] = @TenCoBaoNiemPhong,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[SoToKhaiVC] = @SoToKhaiVC,
			[CoBaoXuatNhapKhau] = @CoBaoXuatNhapKhau,
			[NgayLapToKhai] = @NgayLapToKhai,
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
			[MaNguoiVC] = @MaNguoiVC,
			[TenNguoiVC] = @TenNguoiVC,
			[DiaChiNguoiVC] = @DiaChiNguoiVC,
			[SoHopDongVC] = @SoHopDongVC,
			[NgayHopDongVC] = @NgayHopDongVC,
			[NgayHetHanHopDongVC] = @NgayHetHanHopDongVC,
			[MaPhuongTienVC] = @MaPhuongTienVC,
			[TenPhuongTieVC] = @TenPhuongTieVC,
			[MaMucDichVC] = @MaMucDichVC,
			[TenMucDichVC] = @TenMucDichVC,
			[LoaiHinhVanTai] = @LoaiHinhVanTai,
			[TenLoaiHinhVanTai] = @TenLoaiHinhVanTai,
			[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
			[MaViTriXepHang] = @MaViTriXepHang,
			[MaCangCuaKhauGaXepHang] = @MaCangCuaKhauGaXepHang,
			[MaCangXHKhongCo_HT] = @MaCangXHKhongCo_HT,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[NgayDenDiaDiem_XH] = @NgayDenDiaDiem_XH,
			[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
			[MaViTriDoHang] = @MaViTriDoHang,
			[MaCangCuaKhauGaDoHang] = @MaCangCuaKhauGaDoHang,
			[MaCangDHKhongCo_HT] = @MaCangDHKhongCo_HT,
			[DiaDiemDoHang] = @DiaDiemDoHang,
			[NgayDenDiaDiem_DH] = @NgayDenDiaDiem_DH,
			[TuyenDuongVC] = @TuyenDuongVC,
			[LoaiBaoLanh] = @LoaiBaoLanh,
			[SoTienBaoLanh] = @SoTienBaoLanh,
			[SoLuongCot_TK] = @SoLuongCot_TK,
			[SoLuongContainer] = @SoLuongContainer,
			[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
			[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
			[KyHieuChungTuBaoLanh] = @KyHieuChungTuBaoLanh,
			[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
			[MaVach] = @MaVach,
			[NgayPheDuyetVC] = @NgayPheDuyetVC,
			[NgayDuKienBatDauVC] = @NgayDuKienBatDauVC,
			[GioDuKienBatDauVC] = @GioDuKienBatDauVC,
			[NgayDuKienKetThucVC] = @NgayDuKienKetThucVC,
			[GioDuKienKetThucVC] = @GioDuKienKetThucVC,
			[MaBuuChinhHQ] = @MaBuuChinhHQ,
			[DiaChiBuuChinhHQ] = @DiaChiBuuChinhHQ,
			[TenBuuChinhHQ] = @TenBuuChinhHQ,
			[GhiChu] = @GhiChu,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
		(
			[TKMD_ID],
			[NgayDangKy],
			[TenThongTinXuat],
			[MaPhanLoaiXuLy],
			[MaCoBaoNiemPhong],
			[TenCoBaoNiemPhong],
			[CoQuanHaiQuan],
			[SoToKhaiVC],
			[CoBaoXuatNhapKhau],
			[NgayLapToKhai],
			[MaNguoiKhai],
			[TenNguoiKhai],
			[DiaChiNguoiKhai],
			[MaNguoiVC],
			[TenNguoiVC],
			[DiaChiNguoiVC],
			[SoHopDongVC],
			[NgayHopDongVC],
			[NgayHetHanHopDongVC],
			[MaPhuongTienVC],
			[TenPhuongTieVC],
			[MaMucDichVC],
			[TenMucDichVC],
			[LoaiHinhVanTai],
			[TenLoaiHinhVanTai],
			[MaDiaDiemXepHang],
			[MaViTriXepHang],
			[MaCangCuaKhauGaXepHang],
			[MaCangXHKhongCo_HT],
			[DiaDiemXepHang],
			[NgayDenDiaDiem_XH],
			[MaDiaDiemDoHang],
			[MaViTriDoHang],
			[MaCangCuaKhauGaDoHang],
			[MaCangDHKhongCo_HT],
			[DiaDiemDoHang],
			[NgayDenDiaDiem_DH],
			[TuyenDuongVC],
			[LoaiBaoLanh],
			[SoTienBaoLanh],
			[SoLuongCot_TK],
			[SoLuongContainer],
			[MaNganHangBaoLanh],
			[NamPhatHanhBaoLanh],
			[KyHieuChungTuBaoLanh],
			[SoChungTuBaoLanh],
			[MaVach],
			[NgayPheDuyetVC],
			[NgayDuKienBatDauVC],
			[GioDuKienBatDauVC],
			[NgayDuKienKetThucVC],
			[GioDuKienKetThucVC],
			[MaBuuChinhHQ],
			[DiaChiBuuChinhHQ],
			[TenBuuChinhHQ],
			[GhiChu],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@NgayDangKy,
			@TenThongTinXuat,
			@MaPhanLoaiXuLy,
			@MaCoBaoNiemPhong,
			@TenCoBaoNiemPhong,
			@CoQuanHaiQuan,
			@SoToKhaiVC,
			@CoBaoXuatNhapKhau,
			@NgayLapToKhai,
			@MaNguoiKhai,
			@TenNguoiKhai,
			@DiaChiNguoiKhai,
			@MaNguoiVC,
			@TenNguoiVC,
			@DiaChiNguoiVC,
			@SoHopDongVC,
			@NgayHopDongVC,
			@NgayHetHanHopDongVC,
			@MaPhuongTienVC,
			@TenPhuongTieVC,
			@MaMucDichVC,
			@TenMucDichVC,
			@LoaiHinhVanTai,
			@TenLoaiHinhVanTai,
			@MaDiaDiemXepHang,
			@MaViTriXepHang,
			@MaCangCuaKhauGaXepHang,
			@MaCangXHKhongCo_HT,
			@DiaDiemXepHang,
			@NgayDenDiaDiem_XH,
			@MaDiaDiemDoHang,
			@MaViTriDoHang,
			@MaCangCuaKhauGaDoHang,
			@MaCangDHKhongCo_HT,
			@DiaDiemDoHang,
			@NgayDenDiaDiem_DH,
			@TuyenDuongVC,
			@LoaiBaoLanh,
			@SoTienBaoLanh,
			@SoLuongCot_TK,
			@SoLuongContainer,
			@MaNganHangBaoLanh,
			@NamPhatHanhBaoLanh,
			@KyHieuChungTuBaoLanh,
			@SoChungTuBaoLanh,
			@MaVach,
			@NgayPheDuyetVC,
			@NgayDuKienBatDauVC,
			@GioDuKienBatDauVC,
			@NgayDuKienKetThucVC,
			@GioDuKienKetThucVC,
			@MaBuuChinhHQ,
			@DiaChiBuuChinhHQ,
			@TenBuuChinhHQ,
			@GhiChu,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiVanChuyen] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_ToKhaiVanChuyen] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]	

GO

