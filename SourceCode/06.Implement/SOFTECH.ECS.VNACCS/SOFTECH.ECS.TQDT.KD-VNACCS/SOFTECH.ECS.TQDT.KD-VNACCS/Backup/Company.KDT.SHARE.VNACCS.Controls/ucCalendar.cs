﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public partial class ucCalendar : ucBase
    {
        private DateTime dayMin = new DateTime(1900, 1, 1);
        public ucCalendar()
        {
            InitializeComponent();
        }
        public string TagName
        {
            get { return clcCombobox.Tag != null ? clcCombobox.Tag.ToString() : ""; }
            set { clcCombobox.Tag = value; }
        }
        public DateTime Value
        {
            set
            {
                if (value <= dayMin)
                {
                    this.clcCombobox.Value = dayMin;
                    this.clcCombobox.Text = "";
                }
                else
                {
                    this.clcCombobox.Value = value;
                    this.clcCombobox.Text = value.ToString();
                }
     
            }
            get
            {
                if (clcCombobox.Text == "" || clcCombobox.Value<= dayMin)
                    return dayMin;
                else
                    return this.clcCombobox.Value;
            }
        }
        private void ucCalendar_Load(object sender, EventArgs e)
        {

        }
        public bool ReadOnly
        {
            set{this.clcCombobox.ReadOnly = value;}
            get { return this.clcCombobox.ReadOnly; }
        }
    }
}
