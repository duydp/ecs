﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SXXK;
using Company.BLL.KDT.SXXK;
using Company.Interface.Report.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu.CTTT;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Threading;

namespace Company.Interface.SXXK
{
    public partial class ChungTuThanhToanForm_Modified : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public bool IsOpenFromChayThanhLy;

        private DataSet ds = new DataSet();
        private ChungTuThanhToanCollection ChungTuCollection = new ChungTuThanhToanCollection();

        public string LoaiChungTu { get; set; }
        public ChungTuThanhToanForm_Modified()
        {
            InitializeComponent();
        }
        private DataTable ChungTuThanhToanDataSource()
        {
            DataTable dtChungTuThanhToan = new DataTable("ChungTuThanhToan");

            DataColumn[] cols1 = new DataColumn[10];
            cols1[0] = new DataColumn("SoNgayChungTu", typeof(string));
            cols1[1] = new DataColumn("HinhThucThanhToan", typeof(string));
            cols1[2] = new DataColumn("TriGiaCT", typeof(decimal));
            cols1[3] = new DataColumn("ToKhaiXuat", typeof(string));
            cols1[4] = new DataColumn("NgayDangKyXuat", typeof(DateTime));
            cols1[5] = new DataColumn("GhiChuCT", typeof(string));
            cols1[6] = new DataColumn("SoToKhai", typeof(string));
            cols1[7] = new DataColumn("CanhBao", typeof(string));
            dtChungTuThanhToan.Columns.AddRange(cols1);

            return dtChungTuThanhToan;
        }
        private DataTable ToKhaiXuatDataSource()
        {
            DataTable dtToKhaiXuat = new DataTable("ToKhaiXuat");
            DataColumn[] cols = new DataColumn[14];
            cols[0] = new DataColumn("ToKhaiXuat", typeof(string));
            cols[1] = new DataColumn("NgayDangKyXuat", typeof(DateTime));
            cols[2] = new DataColumn("SoNgayHopDong", typeof(string));
            cols[3] = new DataColumn("MaHangXuat", typeof(string));
            cols[4] = new DataColumn("TriGiaHopDong", typeof(decimal));
            cols[5] = new DataColumn("TriGiaHangThucXuat", typeof(decimal));
            cols[6] = new DataColumn("GhiChu", typeof(string));
            cols[7] = new DataColumn("SoToKhai", typeof(string));

            cols[8] = new DataColumn("SoNgayChungTu", typeof(string));
            cols[9] = new DataColumn("HinhThucThanhToan", typeof(string));
            cols[10] = new DataColumn("TriGiaCT", typeof(decimal));
            cols[11] = new DataColumn("CanhBao", typeof(string)); // Cảnh báo sai lệch thông tin của chứng từ thanh toán
            cols[12] = new DataColumn("CanhBao2", typeof(CanhBaoThongTinTK)); // Cảnh báo sai thông tin đối với từng tờ khai
            cols[13] = new DataColumn("MaHaiQuan", typeof(string));
            dtToKhaiXuat.Columns.AddRange(cols);

            return dtToKhaiXuat;
        }
        private void BindingData(DataTable ToKhaiXuatdt)
        {
            try
            {
                DataTable dtChungTuTT = ChungTuThanhToanDataSource();
                DataTable dtToKhai = ToKhaiXuatDataSource();
                dtToKhai.TableName = "SoToKhaidt";
                string SoToKhai = string.Empty;
                DateTime NgayDangKyXuat = new DateTime(1, 1, 1);
                DataRow drToKhai = dtToKhai.NewRow();
                ToKhaiXuatdt.DefaultView.Sort = "ToKhaiXuat,NgayDangKyXuat";
                foreach (DataRow dr in ToKhaiXuatdt.Rows)
                {

                    if (SoToKhai != dr["ToKhaiXuat"].ToString().Trim() || NgayDangKyXuat != System.Convert.ToDateTime(dr["NgayDangKyXuat"]))
                    {
                        drToKhai = dtToKhai.NewRow();
                        drToKhai["ToKhaiXuat"] = dr["ToKhaiXuat"];
                        drToKhai["NgayDangKyXuat"] = dr["NgayDangKyXuat"];
                        drToKhai["SoNgayHopDong"] = dr["SoNgayHopDong"];
                        drToKhai["TriGiaHopDong"] = dr["TriGiaHopDong"];
                        drToKhai["TriGiaHangThucXuat"] = dr["TriGiaHangThucXuat"];
                        drToKhai["MaHangXuat"] = dr["MaHangXuat"];
                        drToKhai["GhiChu"] = dr["GhiChu"];
                        drToKhai["CanhBao2"] = dr["CanhBao2"];
                        //drToKhai["CanhBao"] += Environment.NewLine + dr["SoNgayChungTu"].ToString() + ": " + dr["CanhBao"].ToString();

                        SoToKhai = dr["ToKhaiXuat"].ToString().Trim();
                        NgayDangKyXuat = System.Convert.ToDateTime(dr["NgayDangKyXuat"]);

                        dtToKhai.Rows.Add(drToKhai);
                    }
                    if (!string.IsNullOrEmpty(dr["CanhBao"].ToString().Trim()))
                    {
                        if (!string.IsNullOrEmpty(drToKhai["CanhBao"].ToString().Trim()))
                            drToKhai["CanhBao"] += Environment.NewLine + dr["SoNgayChungTu"].ToString() + ": " + dr["CanhBao"].ToString();
                        else
                            drToKhai["CanhBao"] = dr["SoNgayChungTu"].ToString() + ": " + dr["CanhBao"].ToString();
                    }
                    drToKhai["CanhBao2"] = dr["CanhBao2"];

                    if (dr["SoNgayChungTu"] != null && !string.IsNullOrEmpty(dr["SoNgayChungTu"].ToString().Trim()))
                    {
                        DataRow drChungTu = dtChungTuTT.NewRow();

                        drChungTu["SoNgayChungTu"] = dr["SoNgayChungTu"];
                        drChungTu["HinhThucThanhToan"] = dr["HinhThucThanhToan"];
                        drChungTu["TriGiaCT"] = dr["TriGiaCT"];
                        drChungTu["ToKhaiXuat"] = dr["ToKhaiXuat"];
                        drChungTu["NgayDangKyXuat"] = dr["NgayDangKyXuat"];
                        drChungTu["GhiChuCT"] = dr["GhiChu"];
                        drChungTu["SoToKhai"] = dr["SoToKhai"];
                        drChungTu["CanhBao"] = dr["CanhBao"];
                        // drChungTu[""] = dr[""];
                        // drChungTu[""] = dr[""];

                        dtChungTuTT.Rows.Add(drChungTu);
                    }
                }

                ds.Relations.Clear();
                ds = new DataSet();
                ds.Tables.Add(dtToKhai);
                ds.Tables.Add(dtChungTuTT);
                //ds.Relations.Add("ChungTuThanhToan", new DataColumn[] { ToKhaiXuatdt.Columns["SoToKhai"] }, new DataColumn[] { dtChungTuTT.Columns["SoToKhai"]}, false);
                DataRelation relation1 = new DataRelation("ChungTuThanhToan", new DataColumn[] { dtToKhai.Columns["ToKhaiXuat"], dtToKhai.Columns["NgayDangKyXuat"] }, new DataColumn[] { dtChungTuTT.Columns["ToKhaiXuat"], dtChungTuTT.Columns["NgayDangKyXuat"] }, true);
                //DataRelation relation2 = new DataRelation("ChungTuThanhToan2", dtToKhai.Columns["NgayDangKyXuat"], dtChungTuTT.Columns["NgayDangKyXuat"], false);
                ds.Relations.Add(relation1);
                //dgList.SetDataBinding(ds, "ToKhaiXuat");
                //dgList.Refetch();


                gridControl1.DataSource = ds.Tables["SoToKhaidt"];
                gridControl1.ForceInitialize();
                //DevExpress.XtraGrid.Views.Grid.GridView gridview3 = new DevExpress.XtraGrid.Views.Grid.GridView();
                //gridControl1.LevelTree.Nodes.Add("ChungTuThanhToan", gridview3);
                gridView2.ViewCaption = "Chứng từ thanh toán";
                bandedGridView1.OptionsView.RowAutoHeight = true;
                gridView2.OptionsView.RowAutoHeight = true;
                //gridView2.PopulateColumns(ds.Tables["ChungTuThanhToan"]);
                //gridView2.Columns["SoToKhai"].Visible = false;
                gridControl1.Refresh();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }


        }
        private decimal GetTongTriGiaNT(List<HangMauDich> HMDCollection)
        {
            decimal d = 0;
            foreach (HangMauDich hmd in HMDCollection)
                d += hmd.TriGiaKB;
            return d;
        }

        public  void ChungTuThanhToanForm_Modified_Load(object sender, EventArgs e)
        {
            //Load();
            //if (this.LoaiChungTu == enumLoaiChungTuTT.Nhap)
            DoWork();
            //TaoMoiChungTuThanhToanNewNhap();
            //else
            //    TaoMoiChungTuThanhToanNewXuat();
            bandedGridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(bandedGridView1_RowStyle);
            bandedGridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(gridView1_CustomDrawCell);
        }


        private void TaoMoiChungTuThanhToan()
        {
            try
            {
                this.ChungTuCollection = new ChungTuThanhToan().SelectCollectionDynamic(" LanThanhLy = " + this.HSTL.LanThanhLy, " SoNgayChungTu asc, NgayDangKyXuat asc, ToKhaiXuat asc");

                DataTable dtToKhaiXuat = ToKhaiXuatDataSource();

                if (this.HSTL.getBKToKhaiXuat() >= 0)
                    this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].LoadChiTietBangKe();

                if (ChungTuCollection.Count == 0)
                {
                    BKToKhaiXuatCollection bkCollection = new BKToKhaiXuatCollection();
                    if (this.HSTL.getBKToKhaiXuat() >= 0)
                        bkCollection = this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion;

                    foreach (BKToKhaiXuat bk in bkCollection)
                    {
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        TKMD.SoToKhai = bk.SoToKhai;
                        TKMD.NamDangKy = bk.NamDangKy;
                        TKMD.MaLoaiHinh = bk.MaLoaiHinh;
                        TKMD.MaHaiQuan = bk.MaHaiQuan;
                        TKMD.Load();
                        TKMD.LoadHMDCollection();

                        List<CTTT_HopDongXuatKhau> listHDXK = new List<CTTT_HopDongXuatKhau>();
                        string where = string.Format("SoHopDong = '{0}'", TKMD.SoHopDong);
                        listHDXK = CTTT_HopDongXuatKhau.SelectCollectionDynamic(where, null);

                        //Hungtq update 05/03/2011. 
                        //Tim thong tin cua chung tu co so hop dong va ngay hop dong truoc do.
                        ChungTuThanhToan cttt = FindCTTT(ChungTuCollection, TKMD.SoHopDong, TKMD.NgayHopDong.ToString("dd/MM/yyyy"));

                        DataRow dr = dtToKhaiXuat.NewRow();
                        dr["ToKhaiXuat"] = bk.SoToKhai + "/" + bk.MaLoaiHinh;
                        dr["SoToKhai"] = bk.SoToKhai;
                        dr["NgayDangKyXuat"] = bk.NgayDangKy;
                        dr["SoNgayHopDong"] = TKMD.SoHopDong != "" ? TKMD.SoHopDong : "Không có hợp đồng" + ", \r\n" + "(" + TKMD.NgayHopDong.ToString("dd/MM/yy") + ")";
                        dr["TriGiaHopDong"] = cttt != null ? cttt.TriGiaHopDong : listHDXK.Count > 0 ? listHDXK[0].TriGia : 0; //Hungtq update
                        dr["TriGiaHangThucXuat"] = GetTongTriGiaNT(TKMD.HMDCollection);

                        int cnt = 0;
                        foreach (Company.BLL.SXXK.ToKhai.HangMauDich HMD in TKMD.HMDCollection)
                        {
                            cnt += 1;
                            dr["MaHangXuat"] = dr["MaHangXuat"].ToString() + HMD.MaPhu;

                            if (cnt < TKMD.HMDCollection.Count)
                                dr["MaHangXuat"] += ", \r\n";
                        }
                        dr["MaHangXuat"] = dr["MaHangXuat"].ToString().Remove(dr["MaHangXuat"].ToString().Length - 2);

                        List<CTTT_ChungTuChiTiet> ctctCollections = (List<CTTT_ChungTuChiTiet>)CTTT_ChungTuChiTiet.SelectCollectionDynamic("SoToKhai=" + bk.SoToKhai + " and Year(NgayDangKy)='" + bk.NgayDangKy.Year + "' And MaLoaiHinh = '" + bk.MaLoaiHinh + "'", "");
                        decimal tongtrigia = 0M;
                        decimal tongtrigiaPhiKhac = 0M;

                        foreach (CTTT_ChungTuChiTiet item in ctctCollections)
                        {
                            CTTT_ChungTu chungTu = CTTT_ChungTu.Load(item.IDCT);

                            dr["SoNgayChungTu"] = string.Format("{0} ({1})", chungTu.SoChungTu, chungTu.NgayChungTu.ToString("dd/MM/yyyy"));
                            dr["HinhThucThanhToan"] = chungTu.HinhThucThanhToan;
                            dr["TriGiaCT"] = chungTu.TongTriGia;

                            string ghichu = string.Empty;

                            cnt = 0;
                            List<CTTT_ChungTuChiTiet> ctctsGhichu = (List<CTTT_ChungTuChiTiet>)CTTT_ChungTuChiTiet.SelectCollectionBy_IDCT(chungTu.ID);
                            foreach (CTTT_ChungTuChiTiet itemchitiet in ctctsGhichu)
                            {
                                cnt += 1;
                                tongtrigia += itemchitiet.TriGia;
                                ghichu += string.Format("TK{0}/{1}={2}", itemchitiet.SoToKhai, itemchitiet.MaLoaiHinh, itemchitiet.TriGia.ToString("N4"));

                                if (cnt < ctctsGhichu.Count)
                                    ghichu += ", \r\n";
                            }

                            cnt = 0;
                            List<CTTT_ChiPhiKhac> chiphikhacs = (List<CTTT_ChiPhiKhac>)CTTT_ChiPhiKhac.SelectCollectionBy_IDCT(chungTu.ID);

                            if (chiphikhacs.Count > 0) //Them dau ngan cach giua thong tin cac to khai va phi khac
                                ghichu += ", \r\n";

                            foreach (CTTT_ChiPhiKhac itemChiPhiKhac in chiphikhacs)
                            {
                                cnt += 1;
                                tongtrigiaPhiKhac += itemChiPhiKhac.TriGia;
                                ghichu += string.Format("{0}={1}", Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhiChungTuThanhToan.getName(itemChiPhiKhac.MaChiPhi), itemChiPhiKhac.TriGia.ToString("N4"));

                                if (cnt < chiphikhacs.Count)
                                    ghichu += ", \r\n";
                            }
                            dr["GhiChu"] = ghichu;
                        }

                        //decimal tonggiatrikhaibao = (decimal)dr["TriGiaHangThucXuat"];
                        decimal tonggiatrikhaibao = dr["TriGiaCT"] != DBNull.Value ? (decimal)dr["TriGiaCT"] : 0;
                        string canhBao = "";
                        if (tonggiatrikhaibao > (tongtrigia - tongtrigiaPhiKhac)) //Tong tri gia chung tu - (Tong tri gia nhap cua to khai - Tong chi phi khac)
                        {
                            canhBao = string.Format("Trả cho đơn hàng  khác {0}", (tonggiatrikhaibao - tongtrigia).ToString("N4"));
                        }
                        else if (tonggiatrikhaibao < (tongtrigia - tongtrigiaPhiKhac))
                            canhBao = string.Format("Tổng trị giá bị âm {0}", (tonggiatrikhaibao - tongtrigia).ToString("N4"));

                        if (dr["GhiChu"].ToString().Trim().Length > 0 && canhBao != "")
                            dr["GhiChu"] += ", \r\n" + canhBao;
                        dr["CanhBao"] = canhBao;
                        string canhbaoTk = string.Empty;
                        dtToKhaiXuat.Rows.Add(dr);
                    }
                }
                else
                {
                    foreach (ChungTuThanhToan ct in this.ChungTuCollection)
                    {
                        string[] sArr = ct.ToKhaiXuat.Split(new string[] { "/" }, StringSplitOptions.None);

                        DataRow dr1 = dtToKhaiXuat.NewRow();
                        dr1["SoNgayHopDong"] = ct.SoNgayHopDong;
                        dr1["TriGiaHopDong"] = ct.TriGiaHopDong;
                        dr1["MaHangXuat"] = ct.MaHangXuat;
                        dr1["TriGiaHangThucXuat"] = ct.TriGiaHangThucXuat;
                        dr1["ToKhaiXuat"] = ct.ToKhaiXuat;
                        dr1["NgayDangKyXuat"] = ct.NgayDangKyXuat;
                        dr1["GhiChu"] = ct.GhiChu;
                        dr1["SoToKhai"] = sArr[0];

                        dr1["SoNgayChungTu"] = ct.SoNgayChungTu;
                        dr1["TriGiaCT"] = ct.TriGiaCT;
                        dr1["HinhThucThanhToan"] = ct.HinhThucThanhToan;
                        dr1["CanhBao"] = "";

                        dtToKhaiXuat.Rows.Add(dr1);
                    }
                }

                ds.Clear();
                if (!ds.Tables.Contains("ChungTuThanhToan"))
                {
                    DataTable dt = dtToKhaiXuat.Clone();
                    ds.Tables.Add(dt);
                }

                foreach (DataRow dr in dtToKhaiXuat.Rows)
                {
                    ds.Tables["ChungTuThanhToan"].ImportRow(dr);
                }

                //dgList.SetDataBinding(ds, "ChungTuThanhToan");
                dgList.DataSource = ds;
                dgList.DataMember = "ChungTuThanhToan";

                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình tạo chứng từ thanh toán.\r\n" + ex.Message, false);
            }
        }

        private void TaoMoiChungTuThanhToanNewXuat(object obj)
        {
            try
            {
                //this.ChungTuCollection = new ChungTuThanhToan().SelectCollectionDynamic(" LanThanhLy = " + this.HSTL.LanThanhLy, " SoNgayChungTu asc, NgayDangKyXuat asc, ToKhaiXuat asc");
                //if(HSTL == null)
                //this.HSTL = HoSoThanhLyDangKy.LoadByLanThanhLy(LanThanhLy, NamThanhLy, GlobalSettings.MA_DON_VI);
                //HSTL.LoadBKCollection();
                DataTable dtToKhaiXuat = ToKhaiXuatDataSource();

                if (this.HSTL.getBKToKhaiXuat() >= 0)
                    this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].LoadChiTietBangKe();

                if (ChungTuCollection.Count == 0)
                {
                    BKToKhaiXuatCollection bkCollection = new BKToKhaiXuatCollection();
                    if (this.HSTL.getBKToKhaiXuat() >= 0)
                        bkCollection = this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion;


                    foreach (BKToKhaiXuat bk in bkCollection)
                    {
                        //if (bk.SoToKhai == 896)
                        //{

                        //}
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        TKMD.SoToKhai = bk.SoToKhai;
                        TKMD.NamDangKy = bk.NamDangKy;
                        TKMD.MaLoaiHinh = bk.MaLoaiHinh;
                        TKMD.MaHaiQuan = bk.MaHaiQuan;
                        TKMD.Load();
                        TKMD.LoadHMDCollection();
                        string SoToKhaiTemp = TaoSoToKhai(TKMD.SoToKhai, bk.MaLoaiHinh);
                        List<CTTT_HopDongXuatKhau> listHDXK = new List<CTTT_HopDongXuatKhau>();
                        string where = string.Format("SoHopDong = '{0}'", TKMD.SoHopDong);
                        listHDXK = CTTT_HopDongXuatKhau.SelectCollectionDynamic(where, null);

                        //Hungtq update 05/03/2011. 
                        //Tim thong tin cua chung tu co so hop dong va ngay hop dong truoc do.
                        ChungTuThanhToan cttt = FindCTTT(ChungTuCollection, TKMD.SoHopDong, TKMD.NgayHopDong.ToString("dd/MM/yyyy"));

                        DataRow dr = dtToKhaiXuat.NewRow();
                        dr["ToKhaiXuat"] = SoToKhaiTemp;//bk.SoToKhai + "/" + bk.MaLoaiHinh;
                        dr["SoToKhai"] = bk.SoToKhai;
                        dr["NgayDangKyXuat"] = bk.NgayDangKy;
                        dr["SoNgayHopDong"] = TKMD.SoHopDong != "" ? TKMD.SoHopDong : TKMD.MaLoaiHinh.Contains("V") ? TKMD.GhiChu : "Không có hợp đồng" + ", \r\n" + "(" + TKMD.NgayHopDong.ToString("dd/MM/yy") + ")";
                        //Hungtq update
                        dr["TriGiaHangThucXuat"] = GetTongTriGiaNT(TKMD.HMDCollection);
                        dr["TriGiaHopDong"] = cttt != null ? cttt.TriGiaHopDong : listHDXK.Count > 0 ? listHDXK[0].TriGia : dr["TriGiaHangThucXuat"];
                        int cnt = 0;
                        foreach (Company.BLL.SXXK.ToKhai.HangMauDich HMD in TKMD.HMDCollection)
                        {
                            cnt += 1;
                            dr["MaHangXuat"] = dr["MaHangXuat"].ToString() + HMD.MaPhu.Trim().ToUpper();

                            if (cnt < TKMD.HMDCollection.Count)
                                dr["MaHangXuat"] += ", \r\n";
                        }
                        //dr["MaHangXuat"] = dr["MaHangXuat"].ToString().Remove(dr["MaHangXuat"].ToString().Length - 2);

                        List<CTTT_ChungTuChiTiet> ctctCollections = (List<CTTT_ChungTuChiTiet>)CTTT_ChungTuChiTiet.SelectCollectionDynamic("SoToKhai=" + bk.SoToKhai + " and Year(NgayDangKy)= " + bk.NgayDangKy.Year + " AND MaLoaiHinh = '" + bk.MaLoaiHinh.Trim() + "'", "");


                        decimal TongTriGiaThucXuat = System.Convert.ToDecimal(dr["TriGiaHangThucXuat"]);
                        decimal TongTriGiaDaPhanBo = 0M;
                        DataRow drChungTu = dtToKhaiXuat.NewRow();
                        if (ctctCollections != null && ctctCollections.Count > 0)
                        {
                            foreach (CTTT_ChungTuChiTiet item in ctctCollections)
                            {
                                drChungTu = dtToKhaiXuat.NewRow();
                                drChungTu["ToKhaiXuat"] = dr["ToKhaiXuat"];
                                drChungTu["SoToKhai"] = dr["SoToKhai"];
                                drChungTu["NgayDangKyXuat"] = dr["NgayDangKyXuat"];
                                drChungTu["SoNgayHopDong"] = dr["SoNgayHopDong"];
                                drChungTu["TriGiaHopDong"] = dr["TriGiaHopDong"];
                                drChungTu["TriGiaHangThucXuat"] = dr["TriGiaHangThucXuat"];
                                drChungTu["MaHangXuat"] = dr["MaHangXuat"];
                                CTTT_ChungTu chungTu = CTTT_ChungTu.Load(item.IDCT);

                                drChungTu["SoNgayChungTu"] = string.Format("{0} ({1})", chungTu.SoChungTu, chungTu.NgayChungTu.ToString("dd/MM/yyyy"));
                                drChungTu["HinhThucThanhToan"] = chungTu.HinhThucThanhToan;
                                drChungTu["TriGiaCT"] = chungTu.TongTriGia;

                                string ghichu = string.Empty;

                                cnt = 0;
                                List<CTTT_ChungTuChiTiet> ctctsGhichu = (List<CTTT_ChungTuChiTiet>)CTTT_ChungTuChiTiet.SelectCollectionBy_IDCT(chungTu.ID);
                                decimal tongtrigia = 0M;
                                decimal tongtrigiaPhiKhac = 0M;
                                foreach (CTTT_ChungTuChiTiet itemchitiet in ctctsGhichu)
                                {
                                    cnt += 1;
                                    tongtrigia += itemchitiet.TriGia;
                                    //ghichu += string.Format("TK{0}/{1}={2}", itemchitiet.SoToKhai, itemchitiet.MaLoaiHinh, itemchitiet.TriGia.ToString("N4"));
                                    ghichu += string.Format("TK{0}={1}", TaoSoToKhai(itemchitiet.SoToKhai, itemchitiet.MaLoaiHinh), itemchitiet.TriGia.ToString("N4"));
                                    if (itemchitiet.SoToKhai == bk.SoToKhai && itemchitiet.MaLoaiHinh == bk.MaLoaiHinh && itemchitiet.NgayDangKy == bk.NgayDangKy)
                                        TongTriGiaDaPhanBo += itemchitiet.TriGia;
                                    if (cnt < ctctsGhichu.Count)
                                        ghichu += ", \r\n";
                                }

                                cnt = 0;
                                List<CTTT_ChiPhiKhac> chiphikhacs = (List<CTTT_ChiPhiKhac>)CTTT_ChiPhiKhac.SelectCollectionBy_IDCT(chungTu.ID);

                                if (chiphikhacs.Count > 0) //Them dau ngan cach giua thong tin cac to khai va phi khac
                                    ghichu += ", \r\n";

                                foreach (CTTT_ChiPhiKhac itemChiPhiKhac in chiphikhacs)
                                {
                                    cnt += 1;
                                    tongtrigiaPhiKhac += itemChiPhiKhac.TriGia;
                                    if (string.IsNullOrEmpty(itemChiPhiKhac.GhiChu))
                                        ghichu += string.Format("{0} = {1}", Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhiChungTuThanhToan.getName(itemChiPhiKhac.MaChiPhi), itemChiPhiKhac.TriGia.ToString("########0.######"));
                                    else
                                        ghichu += string.Format("{0}({2}) = {1}", Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhiChungTuThanhToan.getName(itemChiPhiKhac.MaChiPhi), itemChiPhiKhac.TriGia.ToString("########0.######"), itemChiPhiKhac.GhiChu);

                                    if (cnt < chiphikhacs.Count)
                                        ghichu += ", \r\n";
                                }
                                drChungTu["GhiChu"] = ghichu;

                                decimal tonggiatrikhaibao = drChungTu["TriGiaCT"] != DBNull.Value ? (decimal)drChungTu["TriGiaCT"] : 0;
                                string canhBao = "";
                                if (tonggiatrikhaibao > (tongtrigia - tongtrigiaPhiKhac)) //Tong tri gia chung tu - (Tong tri gia nhap cua to khai - Tong chi phi khac)
                                {
                                    canhBao = string.Format("Trả cho đơn hàng khác : {0}", (tonggiatrikhaibao - (tongtrigia - tongtrigiaPhiKhac)).ToString("N4"));
                                }
                                else if (tonggiatrikhaibao < (tongtrigia - tongtrigiaPhiKhac))
                                    canhBao = string.Format("Phân bổ vượt trị giá:  {0}", (tonggiatrikhaibao - (tongtrigia - tongtrigiaPhiKhac)).ToString("N4"));

                                if (drChungTu["GhiChu"].ToString().Trim().Length > 0 && canhBao != "")
                                    drChungTu["GhiChu"] += ", \r\n" + canhBao;
                                drChungTu["CanhBao"] = canhBao;
                                dtToKhaiXuat.Rows.Add(drChungTu);
                            }
                            if (TongTriGiaDaPhanBo < TongTriGiaThucXuat)
                                drChungTu["CanhBao2"] = CanhBaoThongTinTK.PhanBoThieu;
                            else if (TongTriGiaDaPhanBo > TongTriGiaThucXuat)
                                drChungTu["CanhBao2"] = CanhBaoThongTinTK.PhanBoThua;

                            //drChungTu["CanhBao2"] = CanhBaoThongTinTK.
                        }
                        else
                        {
                            dr["CanhBao"] = "Tờ khai chưa được phân bổ chứng từ thanh toán";
                            dr["CanhBao2"] = CanhBaoThongTinTK.ChuaDuocPhanBo;
                            dtToKhaiXuat.Rows.Add(dr);
                        }

                    }
                }
                else
                {
                    foreach (ChungTuThanhToan ct in this.ChungTuCollection)
                    {
                        string[] sArr = ct.ToKhaiXuat.Split(new string[] { "/" }, StringSplitOptions.None);

                        DataRow dr1 = dtToKhaiXuat.NewRow();
                        dr1["SoNgayHopDong"] = ct.SoNgayHopDong;
                        dr1["TriGiaHopDong"] = ct.TriGiaHopDong;
                        dr1["MaHangXuat"] = ct.MaHangXuat;
                        dr1["TriGiaHangThucXuat"] = ct.TriGiaHangThucXuat;
                        dr1["ToKhaiXuat"] = ct.ToKhaiXuat;
                        dr1["NgayDangKyXuat"] = ct.NgayDangKyXuat;
                        dr1["GhiChu"] = ct.GhiChu;
                        dr1["SoToKhai"] = sArr[0];

                        dr1["SoNgayChungTu"] = ct.SoNgayChungTu;
                        dr1["TriGiaCT"] = ct.TriGiaCT;
                        dr1["HinhThucThanhToan"] = ct.HinhThucThanhToan;
                        dr1["CanhBao"] = "";

                        dtToKhaiXuat.Rows.Add(dr1);
                    }
                }
                Save(dtToKhaiXuat);
                if (this.InvokeRequired)
                {

                    this.Invoke(new MethodInvoker(delegate
                    {
                        BindingData(dtToKhaiXuat);
                    }));
                }
                else
                    BindingData(dtToKhaiXuat);
                doFinish();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                doFinish();
                ShowMessage("Có lỗi trong quá trình tạo chứng từ thanh toán.\r\n" + ex.Message, false);
            }
        }


        private int CheckToKhaiDaQuyetToan(int SoToKhai, DateTime NgayDangKy, string MaLoaiHinh, int LanThanhLyHienTai)
        {
            string where = string.Format("ToKhaiXuat = '{0}' AND CAST(NgayDangKyXuat as DATE) = CAST('{1}' As DATE) AND LanThanhLy < {2}", SoToKhai + "/" + MaLoaiHinh, NgayDangKy.ToString("yyyy-MM-dd"), LanThanhLyHienTai);
            ChungTuThanhToanCollection list = new ChungTuThanhToan().SelectCollectionDynamic(where, "LanThanhLy ASC");
            if (list != null && list.Count > 0)
            {
                return list[0].LanThanhLy;
            }
            else
                return 0;
        }
        private int CheckChungTuQuyetToan(string SoNgayChungTu, int LanThanhLyHienTai)
        {
            string where = string.Format("SoNgayChungTu = '{0}' AND LanThanhLy < {1}", SoNgayChungTu, LanThanhLyHienTai);
            ChungTuThanhToanCollection list = new ChungTuThanhToan().SelectCollectionDynamic(where, "LanThanhLy ASC");
            if (list != null && list.Count > 0)
            {
                return list[0].LanThanhLy;
            }
            else
                return 0;
        }
        private bool CheckToKhaiTrung(DataTable dt, string MaLoaiHinh, string soToKhai, string MaHaiQuan, DateTime NgayDangKy)
        {
            DataRow[] dr = dt.Select(string.Format("ToKhaiXuat = '{0}' and NgayDangKyXuat = #{1}# AND MaHaiQuan = '{2}'", soToKhai, NgayDangKy.ToString("yyyy-MM-dd HH:mm:ss"), MaHaiQuan));

            return dr.Length > 0;
        }
        LoadingFrm Loadingf;
        private void DoWork()
        {
            this.Hide();
            Loadingf = new LoadingFrm();
            Loadingf.TransparencyKey = Color.Magenta;
            Loadingf.BackColor = Color.Magenta;
            Loadingf.StartPosition = FormStartPosition.CenterScreen;
            Loadingf.ShowIcon = false;
            if (this.LoaiChungTu == enumLoaiChungTuTT.Nhap)
                ThreadPool.QueueUserWorkItem(TaoMoiChungTuThanhToanNewNhap);
            else
                ThreadPool.QueueUserWorkItem(TaoMoiChungTuThanhToanNewXuat);
            Loadingf.ShowDialog(this.Parent);

        }
        private void TaoMoiChungTuThanhToanNewNhap(object obj)
        {

            try
            {
                //this.ChungTuCollection = new ChungTuThanhToan().SelectCollectionDynamic(" LanThanhLy = " + this.HSTL.LanThanhLy, " SoNgayChungTu asc, NgayDangKyXuat asc, ToKhaiXuat asc");
                //if(HSTL == null)
                //this.HSTL = HoSoThanhLyDangKy.LoadByLanThanhLy(LanThanhLy, NamThanhLy, GlobalSettings.MA_DON_VI);
                //HSTL.LoadBKCollection();
                DataTable dtToKhaiXuat = ToKhaiXuatDataSource();

                if (this.HSTL.getBKToKhaiNhap() >= 0)
                    this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].LoadChiTietBangKe();

                if (ChungTuCollection.Count == 0)
                {
                    // BKToKhaiNhapCollection bkCollection = new BKToKhaiNhapCollection();
                    //                     if (this.HSTL.getBKToKhaiNhap() >= 0)
                    //                         bkCollection = this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].bkTKNCollection;
                    DataTable dt = new Company.BLL.KDT.SXXK.BKToKhaiNhap().getBKToKhaiNhapForCTTT(this.HSTL.LanThanhLy, GlobalSettings.SoThapPhan.MauBC01, GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "").Tables[0];
                    foreach (DataRow bk in dt.Rows)
                    {
                        int LanThanhLyTruoc = 0; //Lần thanh lý đầu tiên mà tờ khai đã được giải trình - khanhhn
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        TKMD.SoToKhai = System.Convert.ToInt32(bk["SoToKhaiNhap"]);
                        TKMD.NamDangKy = System.Convert.ToInt16(Convert.ToDateTime(bk["NgayDangKy"]).Year);
                        TKMD.MaLoaiHinh = bk["MaLoaiHinh"].ToString().Trim();
                        TKMD.MaHaiQuan = bk["MaHaiQuan"].ToString().Trim();
                        TKMD.NgayDangKy = Convert.ToDateTime(bk["NgayDangKy"]);
                        string SoToKhaiTemp = TaoSoToKhai(TKMD.SoToKhai, TKMD.MaLoaiHinh);
                        //LanThanhLyTruoc = CheckToKhaiDaQuyetToan(TKMD.SoToKhai, TKMD.NgayDangKy, TKMD.MaLoaiHinh,HSTL.LanThanhLy);
                        if (CheckToKhaiTrung(dtToKhaiXuat, TKMD.MaLoaiHinh, SoToKhaiTemp, TKMD.MaHaiQuan, Convert.ToDateTime(bk["NgayDangKy"])))
                            continue;
                        TKMD.Load();
                        TKMD.LoadHMDCollection();
                        if (TKMD.SoToKhai == 2627)
                        {

                        }
                        List<CTTT_HopDongXuatKhau> listHDXK = new List<CTTT_HopDongXuatKhau>();
                        string where = string.Format("SoHopDong = '{0}'", TKMD.SoHopDong);
                        listHDXK = CTTT_HopDongXuatKhau.SelectCollectionDynamic(where, null);

                        //Hungtq update 05/03/2011. 
                        //Tim thong tin cua chung tu co so hop dong va ngay hop dong truoc do.
                        ChungTuThanhToan cttt = FindCTTT(ChungTuCollection, TKMD.SoHopDong, TKMD.NgayHopDong.ToString("dd/MM/yyyy"));

                        DataRow dr = dtToKhaiXuat.NewRow();
                        dr["ToKhaiXuat"] = SoToKhaiTemp;//TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh;
                        dr["SoToKhai"] = TKMD.SoToKhai;
                        dr["MaHaiQuan"] = TKMD.MaHaiQuan;
                        dr["NgayDangKyXuat"] = TKMD.NgayDangKy;
                        dr["SoNgayHopDong"] = TKMD.SoHopDong != "" ? TKMD.SoHopDong : TKMD.MaLoaiHinh.Contains("V") ? TKMD.GhiChu : "Không có hợp đồng" + ", \r\n" + "(" + TKMD.NgayHopDong.ToString("dd/MM/yy") + ")";
                        dr["TriGiaHopDong"] = cttt != null ? cttt.TriGiaHopDong : listHDXK.Count > 0 ? listHDXK[0].TriGia : 0; //Hungtq update
                        dr["TriGiaHangThucXuat"] = GetTongTriGiaNT(TKMD.HMDCollection);
                        int cnt = 0;
                        //if (LanThanhLyTruoc == 0)
                        //{

                        foreach (Company.BLL.SXXK.ToKhai.HangMauDich HMD in TKMD.HMDCollection)
                        {
                            cnt += 1;
                            dr["MaHangXuat"] = dr["MaHangXuat"].ToString() + HMD.MaPhu.Trim().ToUpper();

                            if (cnt < TKMD.HMDCollection.Count)
                                dr["MaHangXuat"] += ", \r\n";
                        }
                        //dr["MaHangXuat"] = dr["MaHangXuat"].ToString().Remove(dr["MaHangXuat"].ToString().Length - 2);
                        //}
                        List<CTTT_ChungTuChiTiet> ctctCollections = (List<CTTT_ChungTuChiTiet>)CTTT_ChungTuChiTiet.SelectCollectionDynamic("SoToKhai=" + TKMD.SoToKhai + " and Year(NgayDangKy)= " + TKMD.NgayDangKy.Year + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh.Trim() + "'", "");


                        decimal TongTriGiaThucXuat = System.Convert.ToDecimal(dr["TriGiaHangThucXuat"]);
                        decimal TongTriGiaDaPhanBo = 0M;
                        DataRow drChungTu = dtToKhaiXuat.NewRow();
                        if (ctctCollections != null && ctctCollections.Count > 0)
                        {
                            foreach (CTTT_ChungTuChiTiet item in ctctCollections)
                            {
                                drChungTu = dtToKhaiXuat.NewRow();
                                drChungTu["ToKhaiXuat"] = dr["ToKhaiXuat"];
                                drChungTu["SoToKhai"] = dr["SoToKhai"];
                                drChungTu["NgayDangKyXuat"] = dr["NgayDangKyXuat"];
                                drChungTu["SoNgayHopDong"] = dr["SoNgayHopDong"];
                                drChungTu["TriGiaHopDong"] = dr["TriGiaHopDong"];
                                drChungTu["TriGiaHangThucXuat"] = dr["TriGiaHangThucXuat"];
                                drChungTu["MaHangXuat"] = dr["MaHangXuat"];
                                drChungTu["MaHaiQuan"] = dr["MaHaiQuan"];
                                CTTT_ChungTu chungTu = CTTT_ChungTu.Load(item.IDCT);

                                drChungTu["SoNgayChungTu"] = string.Format("{0} ({1})", chungTu.SoChungTu, chungTu.NgayChungTu.ToString("dd/MM/yyyy"));
                                drChungTu["HinhThucThanhToan"] = chungTu.HinhThucThanhToan;
                                drChungTu["TriGiaCT"] = chungTu.TongTriGia;
                                LanThanhLyTruoc = CheckChungTuQuyetToan(drChungTu["SoNgayChungTu"].ToString(), HSTL.LanThanhLy);
                                string ghichu = string.Empty;

                                cnt = 0;
                                List<CTTT_ChungTuChiTiet> ctctsGhichu = (List<CTTT_ChungTuChiTiet>)CTTT_ChungTuChiTiet.SelectCollectionBy_IDCT(chungTu.ID);
                                decimal tongtrigia = 0M;
                                decimal tongtrigiaPhiKhac = 0M;
                                foreach (CTTT_ChungTuChiTiet itemchitiet in ctctsGhichu)
                                {
                                    cnt += 1;
                                    tongtrigia += itemchitiet.TriGia;
                                    //ghichu += string.Format("TK{0}/{1}={2}", itemchitiet.SoToKhai, itemchitiet.MaLoaiHinh, itemchitiet.TriGia.ToString("N4"));
                                    ghichu += string.Format("TK{0}={1}", TaoSoToKhai(itemchitiet.SoToKhai, itemchitiet.MaLoaiHinh), itemchitiet.TriGia.ToString("N4"));
                                    if (itemchitiet.SoToKhai == TKMD.SoToKhai && itemchitiet.MaLoaiHinh == TKMD.MaLoaiHinh && itemchitiet.NgayDangKy == TKMD.NgayDangKy)
                                        TongTriGiaDaPhanBo += itemchitiet.TriGia;
                                    if (cnt < ctctsGhichu.Count)
                                        ghichu += ", \r\n";
                                }

                                cnt = 0;
                                List<CTTT_ChiPhiKhac> chiphikhacs = (List<CTTT_ChiPhiKhac>)CTTT_ChiPhiKhac.SelectCollectionBy_IDCT(chungTu.ID);

                                if (chiphikhacs.Count > 0) //Them dau ngan cach giua thong tin cac to khai va phi khac
                                    ghichu += ", \r\n";

                                foreach (CTTT_ChiPhiKhac itemChiPhiKhac in chiphikhacs)
                                {
                                    cnt += 1;
                                    tongtrigiaPhiKhac += itemChiPhiKhac.TriGia;
                                    if (string.IsNullOrEmpty(itemChiPhiKhac.GhiChu))
                                        ghichu += string.Format("{0} = {1}", Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhiChungTuThanhToan.getName(itemChiPhiKhac.MaChiPhi), itemChiPhiKhac.TriGia.ToString("########0.######"));
                                    else
                                        ghichu += string.Format("{0}({2}) = {1}", Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhiChungTuThanhToan.getName(itemChiPhiKhac.MaChiPhi), itemChiPhiKhac.TriGia.ToString("########0.######"), itemChiPhiKhac.GhiChu);

                                    if (cnt < chiphikhacs.Count)
                                        ghichu += ", \r\n";
                                }
                                drChungTu["GhiChu"] = ghichu;

                                decimal tonggiatrikhaibao = drChungTu["TriGiaCT"] != DBNull.Value ? (decimal)drChungTu["TriGiaCT"] : 0;
                                string canhBao = "";
                                if (tonggiatrikhaibao > (tongtrigia - tongtrigiaPhiKhac)) //Tong tri gia chung tu - (Tong tri gia nhap cua to khai - Tong chi phi khac)
                                {
                                    if (drChungTu["HinhThucThanhToan"].ToString().ToUpper() == "KHONGTT")
                                        canhBao = string.Format("Bù trừ cho đơn hàng khác : {0}", (tonggiatrikhaibao - (tongtrigia - tongtrigiaPhiKhac)).ToString("N4"));
                                    else
                                        canhBao = string.Format("Trả cho đơn hàng khác : {0}", (tonggiatrikhaibao - (tongtrigia - tongtrigiaPhiKhac)).ToString("N4"));
                                }
                                else if (tonggiatrikhaibao < (tongtrigia - tongtrigiaPhiKhac))
                                    canhBao = string.Format("Phân bổ vượt trị giá:  {0}", (tonggiatrikhaibao - (tongtrigia - tongtrigiaPhiKhac)).ToString("N4"));

                                if (drChungTu["GhiChu"].ToString().Trim().Length > 0 && canhBao != "")
                                    drChungTu["GhiChu"] += ", \r\n" + canhBao;
                                drChungTu["CanhBao"] = canhBao;
                                //if (LanThanhLyTruoc == 0)

                                if (LanThanhLyTruoc > 0)
                                {
                                    HoSoThanhLyDangKy hstlOld = HoSoThanhLyDangKy.LoadByLanThanhLy(LanThanhLyTruoc, 0, GlobalSettings.MA_DON_VI);
                                    string SoQuyetDinh = string.Empty;
                                    if (hstlOld != null)
                                    {
                                        SoQuyetDinh = hstlOld.SoQuyetDinh;
                                        if (string.IsNullOrEmpty(SoQuyetDinh))
                                            SoQuyetDinh = "(Số Hồ sơ " + hstlOld.SoHoSo + " Chưa được cập nhật số quyết định )";
                                        drChungTu["GhiChu"] += "\r\nIsExists: Chứng từ đã được giải trình ở hồ sơ thanh khoản số: " + SoQuyetDinh;
                                    }
                                    
                                }
                                dtToKhaiXuat.Rows.Add(drChungTu);

                            }
                            if (TongTriGiaDaPhanBo < TongTriGiaThucXuat)
                                drChungTu["CanhBao2"] = CanhBaoThongTinTK.PhanBoThieu;
                            else if (TongTriGiaDaPhanBo > TongTriGiaThucXuat)
                                drChungTu["CanhBao2"] = CanhBaoThongTinTK.PhanBoThua;

                            //dtToKhaiXuat.Rows.Add(drChungTu);

                            //drChungTu["CanhBao2"] = CanhBaoThongTinTK.
                        }
                        else
                        {
                            dr["CanhBao"] = "Tờ khai chưa được phân bổ chứng từ thanh toán";
                            dr["CanhBao2"] = CanhBaoThongTinTK.ChuaDuocPhanBo;
                            dtToKhaiXuat.Rows.Add(dr);
                        }

                    }
                }
                else
                {
                    foreach (ChungTuThanhToan ct in this.ChungTuCollection)
                    {
                        string[] sArr = ct.ToKhaiXuat.Split(new string[] { "/" }, StringSplitOptions.None);

                        DataRow dr1 = dtToKhaiXuat.NewRow();
                        dr1["SoNgayHopDong"] = ct.SoNgayHopDong;
                        dr1["TriGiaHopDong"] = ct.TriGiaHopDong;
                        dr1["MaHangXuat"] = ct.MaHangXuat;
                        dr1["TriGiaHangThucXuat"] = ct.TriGiaHangThucXuat;
                        dr1["ToKhaiXuat"] = ct.ToKhaiXuat;
                        dr1["NgayDangKyXuat"] = ct.NgayDangKyXuat;
                        dr1["GhiChu"] = ct.GhiChu;
                        dr1["SoToKhai"] = sArr[0];

                        dr1["SoNgayChungTu"] = ct.SoNgayChungTu;
                        dr1["TriGiaCT"] = ct.TriGiaCT;
                        dr1["HinhThucThanhToan"] = ct.HinhThucThanhToan;
                        dr1["CanhBao"] = "";

                        dtToKhaiXuat.Rows.Add(dr1);
                    }
                }
                Save(dtToKhaiXuat);
                if (this.InvokeRequired)
                {

                    this.Invoke(new MethodInvoker(delegate
                    {
                        BindingData(dtToKhaiXuat);
                    }));
                }
                else
                    BindingData(dtToKhaiXuat);
                doFinish();

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                doFinish();

                this.Invoke(new MethodInvoker(delegate
                    {
                        ShowMessage("Có lỗi trong quá trình tạo chứng từ thanh toán.\r\n" + ex.Message, false);
                    }));
                //ShowMessage("Có lỗi trong quá trình tạo chứng từ thanh toán.\r\n" + ex.Message, false);

            }
        }
        private void doFinish()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    foreach (Form f in Application.OpenForms)
                    {
                        try
                        {
                            if (f.Name == "LoadingFrm")
                                f.Close();
                        }
                        catch (Exception)
                        {
                            
                        }
                        
                    }
                    //foreach (Form f in Application.OpenForms)
                    //{
                    //    if (f.Name == "ChungTuThanhToanForm_Modified")
                    //        return;
                    //}
                    this.Show();
                }));
            }
            else
            {
                foreach (Form f in Application.OpenForms)
                {
                    try
                    {
                        if (f.Name == "LoadingFrm")
                            f.Close();
                    }
                    catch (Exception)
                    {
                        
                    }
                    
                }
                //foreach (Form f in Application.OpenForms)
                //{
                //    if (f.Name == "ChungTuThanhToanForm_Modified")
                //        return;
                //}
                this.Show();
            }

        }
        private ChungTuThanhToan FindCTTT(ChungTuThanhToanCollection listCTTT, string soHopDong, string ngayHopDong)
        {
            foreach (ChungTuThanhToan ctItem in listCTTT)
            {
                if (ctItem.SoNgayHopDong.Contains(soHopDong) && ctItem.SoNgayHopDong.Contains(ngayHopDong))
                {
                    return ctItem;
                }
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listCTTT"></param>
        /// <param name="soNgayHopDong">Bao gom: So hong va ngay hop dong</param>
        /// <returns></returns>
        private ChungTuThanhToan FindCTTT2(ChungTuThanhToanCollection listCTTT, string soNgayHopDong, string soToKhai)
        {
            foreach (ChungTuThanhToan ctItem in listCTTT)
            {
                if (ctItem.SoNgayHopDong.Equals(soNgayHopDong) && ctItem.GhiChu.ToLower().Contains("tk" + soToKhai))
                {
                    return ctItem;
                }
            }

            return null;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Save(DataTable dtChungTu)
        {
            ChungTuThanhToanCollection chungtus = new ChungTuThanhToanCollection();
            if (dtChungTu != null && dtChungTu.Rows.Count > 0)
            {
                foreach (DataRow dr2 in dtChungTu.Rows)
                {
                    ChungTuThanhToan ct = new ChungTuThanhToan();
                    ct.LanThanhLy = this.HSTL.LanThanhLy;
                    ct.SoNgayChungTu = dr2["SoNgayChungTu"].ToString();
                    ct.TriGiaCT = Convert.ToDecimal(dr2["TriGiaCT"] != System.DBNull.Value ? dr2["TriGiaCT"] : 0);
                    ct.HinhThucThanhToan = dr2["HinhThucThanhToan"].ToString();
                    ct.GhiChu = dr2["GhiChu"].ToString();

                    ct.SoNgayHopDong = dr2["SoNgayHopDong"].ToString();
                    ct.TriGiaHopDong = Convert.ToDecimal(dr2["TriGiaHopDong"]);
                    ct.MaHangXuat = dr2["MaHangXuat"].ToString();
                    ct.TriGiaHangThucXuat = Convert.ToDecimal(dr2["TriGiaHangThucXuat"]);
                    ct.ToKhaiXuat = dr2["ToKhaiXuat"].ToString();
                    ct.NgayDangKyXuat = Convert.ToDateTime(dr2["NgayDangKyXuat"]);
                    chungtus.Add(ct);
                }
                if (chungtus.Count != 0)
                {
                    new ChungTuThanhToan().DeleteCollection(this.ChungTuCollection);
                    this.ChungTuCollection.Clear();

                    new ChungTuThanhToan().InsertFull(chungtus, this.HSTL.LanThanhLy);

                    this.ChungTuCollection.AddRange(chungtus);
                    //ShowMessage("Lưu thông tin bảng chứng từ thành công.", false);
                }
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                ChungTuThanhToanCollection chungtus = new ChungTuThanhToanCollection();

                foreach (DataRow dr2 in this.ds.Tables["ChungTuThanhToan"].Rows)
                {
                    if (dr2.RowState != DataRowState.Deleted)
                    {
                        ChungTuThanhToan ct = new ChungTuThanhToan();
                        ct.LanThanhLy = this.HSTL.LanThanhLy;
                        ct.SoNgayChungTu = dr2["SoNgayChungTu"].ToString();
                        ct.TriGiaCT = Convert.ToDecimal(dr2["TriGiaCT"] != System.DBNull.Value ? dr2["TriGiaCT"] : 0);
                        ct.HinhThucThanhToan = dr2["HinhThucThanhToan"].ToString();
                        ct.GhiChu = dr2["GhiChu"].ToString();

                        ct.SoNgayHopDong = dr2["SoNgayHopDong"].ToString();
                        ct.TriGiaHopDong = Convert.ToDecimal(dr2["TriGiaHopDong"]);
                        ct.MaHangXuat = dr2["MaHangXuat"].ToString();
                        ct.TriGiaHangThucXuat = Convert.ToDecimal(dr2["TriGiaHangThucXuat"]);
                        ct.ToKhaiXuat = dr2["ToKhaiXuat"].ToString();
                        ct.NgayDangKyXuat = Convert.ToDateTime(dr2["NgayDangKyXuat"]);
                        chungtus.Add(ct);
                    }
                }

                if (chungtus.Count != 0)
                {
                    new ChungTuThanhToan().DeleteCollection(this.ChungTuCollection);
                    this.ChungTuCollection.Clear();

                    new ChungTuThanhToan().InsertFull(chungtus, this.HSTL.LanThanhLy);

                    this.ChungTuCollection.AddRange(chungtus);
                    ShowMessage("Lưu thông tin bảng chứng từ thành công.", false);
                }
                else
                {
                    ShowMessage("Không có bản ghi để lưu.", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private DataRow GetParent(string toKhaiXuat, DateTime ngayDangKyXuat)
        {
            foreach (DataRow dr in this.ds.Tables["ChungTuThanhToan"].Rows)
            {
                if (dr["SoToKhai"].ToString() == toKhaiXuat /* && Convert.ToDateTime(dr["NgayDangKyXuat"]) == ngayDangKyXuat*/)
                {
                    return dr;
                }
            }
            return null;
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            if (this.ChungTuCollection.Count == 0)
            {
                //ShowMessage("Bạn phải lưu trước khi xuất báo cáo.", false);
                MLMessages("Bạn phải lưu trước khi xuất báo cáo.", "MSG_THK37", "", false);
                return;
            }
            ChungTuThanhToanReport report = new ChungTuThanhToanReport();

            report.SoHSTK = this.HSTL.SoHoSo;
            report.LanThanhLy = this.HSTL.LanThanhLy;
            report.CTCollection = this.ChungTuCollection;
            bool inTriGiaHD = true;
            if (LoaiChungTu == enumLoaiChungTuTT.Nhap)
                inTriGiaHD = ShowMessage("Bạn có muốn in trị giá hợp đồng", true) == "Yes";
            if (inTriGiaHD)
            {
                report.BindReport(LoaiChungTu, inTriGiaHD);
                report.ShowRibbonPreview();
            }
            else
            {
                ChungTuThanhToanReport_KhongTriGiaHD report2 = new ChungTuThanhToanReport_KhongTriGiaHD();

                report2.SoHSTK = this.HSTL.SoHoSo;
                report2.LanThanhLy = this.HSTL.LanThanhLy;
                report2.CTCollection = this.ChungTuCollection;
                report2.BindReport(LoaiChungTu, false);
                report2.ShowRibbonPreview();
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                // if (ShowMessage("Bạn có muốn xóa bảng chứng từ này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa bảng chứng từ này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    new ChungTuThanhToan().DeleteDynamicTransaction("LanThanhLy = " + this.HSTL.LanThanhLy, null);
                    // ShowMessage("Xóa bảng chứng từ thành công.", false);
                    MLMessages("Xóa bảng chứng từ thành công.", "MSG_THK39", "", false);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }

        private void btnHDxuatkhau_Click(object sender, EventArgs e)
        {
            FrmQuanLyHopDongXuatKhau frm = new FrmQuanLyHopDongXuatKhau();
            frm.Show();
        }

        private void uiCommandManager_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            if (e.Command.Key == "cmdQuanLyHopDongXuatKhau")
            {
                FrmQuanLyHopDongXuatKhau frmQLHD = new FrmQuanLyHopDongXuatKhau();
                frmQLHD.Show();
            }
            else if (e.Command.Key == "cmdQuanLyChungTu")
            {
                FormQuanLyChungTuThanhToan frmQLCT = new FormQuanLyChungTuThanhToan();
                frmQLCT.HSTL = HSTL;
                frmQLCT.IsOpenFromChayThanhLy = IsOpenFromChayThanhLy;
                frmQLCT.ShowDialog(this);
            }
            else if (e.Command.Key == "cmdTaoCTTT")
            {
                //if (ShowMessage("Chứng từ thanh toán hiện tại sẽ bị xóa trước khi tạo chứng từ mới. Bạn có chắc chắn muốn xóa không?", true) == "Yes")
                //{
                //    //Xoa CTTT
                string strLoaiCT = string.Empty;
                if (this.LoaiChungTu == enumLoaiChungTuTT.Nhap)
                    strLoaiCT = " AND ToKhaiXuat like '%/N%' ";
                else
                    strLoaiCT = " AND ToKhaiXuat like '%/X%' ";
                new ChungTuThanhToan().DeleteDynamicTransaction("LanThanhLy = " + this.HSTL.LanThanhLy + strLoaiCT, null);
                ChungTuCollection = new ChungTuThanhToanCollection();
                //Tao moi CTTT
                //if (this.LoaiChungTu == enumLoaiChungTuTT.Nhap)
                DoWork();
                //TaoMoiChungTuThanhToanNewNhap(null);
                //else
                //{
                //    TaoMoiChungTuThanhToanNewXuat();
                //}
                // }
            }
            else if (e.Command.Key == "cmdXoaChungTu")
            {
                string strLoaiCT = string.Empty;
                if (this.LoaiChungTu == enumLoaiChungTuTT.Nhap)
                    strLoaiCT = " AND ToKhaiXuat like '%/N%' ";
                else
                    strLoaiCT = " AND ToKhaiXuat like '%/X%' ";
                new ChungTuThanhToan().DeleteDynamicTransaction("LanThanhLy = " + this.HSTL.LanThanhLy + strLoaiCT, null);
                ChungTuCollection = new ChungTuThanhToanCollection();
                this.BindingData(this.ChungTuThanhToanDataSource());
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                string[] str = e.Row.Cells[0].Text.Split(new string[] { "/" }, StringSplitOptions.None);
                tkmd.SoToKhai = Convert.ToInt32(str[0]);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKyXuat"].Value).Year);
                tkmd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                tkmd.MaLoaiHinh = str[1];
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog(this);
            }
        }

        private enum CanhBaoThongTinTK
        {
            ChuaDuocPhanBo,
            PhanBoThieu,
            PhanBoThua
        }

        private void bandedGridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            object canhbao = bandedGridView1.GetRowCellValue(e.RowHandle, "CanhBao2");
            if (canhbao != null && !string.IsNullOrEmpty(canhbao.ToString()))
            {

                switch ((CanhBaoThongTinTK)canhbao)
                {
                    case CanhBaoThongTinTK.PhanBoThieu:
                        e.Appearance.ForeColor = Color.Blue;
                        break;
                    case CanhBaoThongTinTK.PhanBoThua:
                        e.Appearance.ForeColor = Color.Green;
                        break;
                    case CanhBaoThongTinTK.ChuaDuocPhanBo:
                        e.Appearance.ForeColor = Color.Red;
                        break;
                }
            }

        }

        private void bandedGridView1_RowLoaded(object sender, DevExpress.XtraGrid.Views.Base.RowEventArgs e)
        {

        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {

            GridView view = sender as GridView;

            if (e.Column.VisibleIndex == 0 && view.IsMasterRowEmpty(e.RowHandle))

                (e.Cell as GridCellInfo).CellButtonRect = Rectangle.Empty;

        }
        private string TaoSoToKhai(int SoToKhai, string maLoaiHinh)
        {
            if (maLoaiHinh.Contains("E") || maLoaiHinh.Contains("B13"))
            {
                if (maLoaiHinh.Length > 3)
                    maLoaiHinh = maLoaiHinh.Substring(2);
                return Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(SoToKhai).ToString() + "/" + maLoaiHinh;
            }
            else
                return SoToKhai + "/" + maLoaiHinh;
        }
    }
}


