-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]
	@Master_ID bigint,
	@TrangThai varchar(10),
	@RTPTag varchar(64),
	@messagesTag varchar(50),
	@MessagesInputID varchar(50),
	@IndexTag varchar(50),
	@MaNghiepVu varchar(10),
	@GhiChu nvarchar(255),
	@TerminalID varchar(6),
	@CreatedTime datetime,
	@SoTiepNhan varchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_MsgPhanBo]
(
	[Master_ID],
	[TrangThai],
	[RTPTag],
	[messagesTag],
	[MessagesInputID],
	[IndexTag],
	[MaNghiepVu],
	[GhiChu],
	[TerminalID],
	[CreatedTime],
	[SoTiepNhan]
)
VALUES 
(
	@Master_ID,
	@TrangThai,
	@RTPTag,
	@messagesTag,
	@MessagesInputID,
	@IndexTag,
	@MaNghiepVu,
	@GhiChu,
	@TerminalID,
	@CreatedTime,
	@SoTiepNhan
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Update]
	@ID bigint,
	@Master_ID bigint,
	@TrangThai varchar(10),
	@RTPTag varchar(64),
	@messagesTag varchar(50),
	@MessagesInputID varchar(50),
	@IndexTag varchar(50),
	@MaNghiepVu varchar(10),
	@GhiChu nvarchar(255),
	@TerminalID varchar(6),
	@CreatedTime datetime,
	@SoTiepNhan varchar(50)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_MsgPhanBo]
SET
	[Master_ID] = @Master_ID,
	[TrangThai] = @TrangThai,
	[RTPTag] = @RTPTag,
	[messagesTag] = @messagesTag,
	[MessagesInputID] = @MessagesInputID,
	[IndexTag] = @IndexTag,
	[MaNghiepVu] = @MaNghiepVu,
	[GhiChu] = @GhiChu,
	[TerminalID] = @TerminalID,
	[CreatedTime] = @CreatedTime,
	[SoTiepNhan] = @SoTiepNhan
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@TrangThai varchar(10),
	@RTPTag varchar(64),
	@messagesTag varchar(50),
	@MessagesInputID varchar(50),
	@IndexTag varchar(50),
	@MaNghiepVu varchar(10),
	@GhiChu nvarchar(255),
	@TerminalID varchar(6),
	@CreatedTime datetime,
	@SoTiepNhan varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_MsgPhanBo] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_MsgPhanBo] 
		SET
			[Master_ID] = @Master_ID,
			[TrangThai] = @TrangThai,
			[RTPTag] = @RTPTag,
			[messagesTag] = @messagesTag,
			[MessagesInputID] = @MessagesInputID,
			[IndexTag] = @IndexTag,
			[MaNghiepVu] = @MaNghiepVu,
			[GhiChu] = @GhiChu,
			[TerminalID] = @TerminalID,
			[CreatedTime] = @CreatedTime,
			[SoTiepNhan] = @SoTiepNhan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_MsgPhanBo]
		(
			[Master_ID],
			[TrangThai],
			[RTPTag],
			[messagesTag],
			[MessagesInputID],
			[IndexTag],
			[MaNghiepVu],
			[GhiChu],
			[TerminalID],
			[CreatedTime],
			[SoTiepNhan]
		)
		VALUES 
		(
			@Master_ID,
			@TrangThai,
			@RTPTag,
			@messagesTag,
			@MessagesInputID,
			@IndexTag,
			@MaNghiepVu,
			@GhiChu,
			@TerminalID,
			@CreatedTime,
			@SoTiepNhan
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_MsgPhanBo]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_MsgPhanBo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[TrangThai],
	[RTPTag],
	[messagesTag],
	[MessagesInputID],
	[IndexTag],
	[MaNghiepVu],
	[GhiChu],
	[TerminalID],
	[CreatedTime],
	[SoTiepNhan]
FROM
	[dbo].[t_KDT_VNACCS_MsgPhanBo]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[TrangThai],
	[RTPTag],
	[messagesTag],
	[MessagesInputID],
	[IndexTag],
	[MaNghiepVu],
	[GhiChu],
	[TerminalID],
	[CreatedTime],
	[SoTiepNhan]
FROM [dbo].[t_KDT_VNACCS_MsgPhanBo] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[TrangThai],
	[RTPTag],
	[messagesTag],
	[MessagesInputID],
	[IndexTag],
	[MaNghiepVu],
	[GhiChu],
	[TerminalID],
	[CreatedTime],
	[SoTiepNhan]
FROM
	[dbo].[t_KDT_VNACCS_MsgPhanBo]	

GO

