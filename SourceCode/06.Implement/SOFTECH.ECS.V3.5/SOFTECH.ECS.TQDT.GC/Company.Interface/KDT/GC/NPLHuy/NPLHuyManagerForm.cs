﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;

namespace Company.Interface.KDT.GC
{
    public partial class NPLHuyManagerForm : BaseForm
    {
        public NPLHuyDangKy NPLHuyDangky = new NPLHuyDangKy();
        List<NPLHuyDangKy> NPLHuyDKList = new List<NPLHuyDangKy>();
        DataTable tb_NPLHuyDKList = new DataTable();
        public HopDong HD = new HopDong();
        public NPLHuyManagerForm()
        {
            InitializeComponent();
        }

      
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                int id= Convert.ToInt32(dgList.CurrentRow.Cells["ID"].Value);
                NPLHuyForm frm = new NPLHuyForm();
                //frm.NPLHuyDK = KDT_GC_CungUngDangKy.LoadFull(id);
                frm.HD = this.HD;
                frm.NPLHuyDK = NPLHuyDangKy.Load(id);
                frm.ShowDialog();
            }
        }

        private void CungUngManagerForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            if (HD.ID > 0)
                tb_NPLHuyDKList = NPLHuyDangKy.SelectDynamic("HopDong_ID = " + HD.ID, "").Tables[0];
            else
                tb_NPLHuyDKList = NPLHuyDangKy.SelectAll().Tables[0];
                //NPLHuyDKList = NPLHuyDangKy.se;

            dgList.DataSource = tb_NPLHuyDKList;
            dgList.Refetch();
        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case"cmdNPLHuy":
                    ThemBangKeCungUng();
                    break;

            }
        }
        private void ThemBangKeCungUng()
        {
            NPLHuyDangky =  new NPLHuyDangKy();
            NPLHuyDangky.HopDong_ID = HD.ID;
            NPLHuyDangky.SoHopDong = HD.SoHopDong;
            NPLHuyDangky.NgayHopDong = HD.NgayKy;
            NPLHuyDangky.NgayHetHan = HD.NgayHetHan;
            NPLHuyForm frm = new NPLHuyForm();
            frm.NPLHuyDK = NPLHuyDangky;
            frm.ShowDialog();
            LoadData();

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
             if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                 switch(trangthai)
                 {
                     case"-1":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                         break;
                     case "0":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                         break;
                     case "1":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                         break;
                     case "2":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                         break;
                 }
             }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<NPLHuyDangKy> ItemColl = new List<NPLHuyDangKy>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa bang kê NPL này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        string id = i.GetRow().Cells[1].Value.ToString();
                        NPLHuy.DeleteDynamic("Master_ID ="+id);
                        NPLHuyDangKy.DeleteNPLHuyDangKy(Int64.Parse(id));
                    }
                    LoadData();
                    dgList.Refetch();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

      
        
    }
}
