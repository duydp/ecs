﻿namespace Company.Interface
{
    partial class HopDongForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HopDongForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.nguyenTeControl1 = new Company.Interface.Controls.NguyenTeControl();
            this.label9 = new System.Windows.Forms.Label();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTongTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtThongTinKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnThuTucHQTruoc = new Janus.Windows.EditControls.UIButton();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTenDVBan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaDVBan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenDVMua = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaDVMua = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDiaDiemGiaoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ccThoiHanTT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnChonHang = new Janus.Windows.EditControls.UIButton();
            this.cmsThemHang = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmdAddHangTK = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdAddHang = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdAddExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvNgayHopDong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoHopDong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvPTTT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDiaDiemGiaoHang = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDVMua = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDVBan = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvThoiHangTT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvTongTriGia = new Company.Controls.CustomValidation.RangeValidator();
            this.cvTongTriGia = new Company.Controls.CustomValidation.CompareValidator();
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.btnKetQuaXuLy = new Janus.Windows.EditControls.UIButton();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lbTrangThai = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnKhaiBao = new Janus.Windows.EditControls.UIButton();
            this.btnLayPhanHoi = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.cmsThemHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvPTTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemGiaoHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVBan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvThoiHangTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTongTriGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTongTriGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnKhaiBao);
            this.grbMain.Controls.Add(this.btnLayPhanHoi);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.btnChonHang);
            this.grbMain.Size = new System.Drawing.Size(741, 573);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.nguyenTeControl1);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.cbDKGH);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.cbPTTT);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.txtTongTriGia);
            this.uiGroupBox2.Controls.Add(this.txtThongTinKhac);
            this.uiGroupBox2.Controls.Add(this.btnThuTucHQTruoc);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.txtTenDVBan);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.txtMaDVBan);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.txtTenDVMua);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtMaDVMua);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtDiaDiemGiaoHang);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.ccThoiHanTT);
            this.uiGroupBox2.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayHopDong);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(10, 80);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(719, 294);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // nguyenTeControl1
            // 
            this.nguyenTeControl1.BackColor = System.Drawing.Color.Transparent;
            this.nguyenTeControl1.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.nguyenTeControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nguyenTeControl1.Location = new System.Drawing.Point(137, 125);
            this.nguyenTeControl1.Ma = "";
            this.nguyenTeControl1.Name = "nguyenTeControl1";
            this.nguyenTeControl1.ReadOnly = false;
            this.nguyenTeControl1.Size = new System.Drawing.Size(399, 22);
            this.nguyenTeControl1.TabIndex = 6;
            this.nguyenTeControl1.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 129);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Đồng tiền thanh toán\r\n";
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "Name";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(399, 71);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(123, 21);
            this.cbDKGH.TabIndex = 4;
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDKGH.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(289, 76);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Điều kiện giao hàng";
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(137, 71);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(134, 21);
            this.cbPTTT.TabIndex = 3;
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbPTTT.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(125, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Phương thức thanh toán";
            // 
            // txtTongTriGia
            // 
            this.txtTongTriGia.DecimalDigits = 20;
            this.txtTongTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGia.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongTriGia.Location = new System.Drawing.Point(137, 207);
            this.txtTongTriGia.MaxLength = 15;
            this.txtTongTriGia.Name = "txtTongTriGia";
            this.txtTongTriGia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTriGia.Size = new System.Drawing.Size(385, 21);
            this.txtTongTriGia.TabIndex = 11;
            this.txtTongTriGia.Text = "0";
            this.txtTongTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTongTriGia.VisualStyleManager = this.vsmMain;
            // 
            // txtThongTinKhac
            // 
            this.txtThongTinKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThongTinKhac.Location = new System.Drawing.Point(137, 236);
            this.txtThongTinKhac.MaxLength = 255;
            this.txtThongTinKhac.Multiline = true;
            this.txtThongTinKhac.Name = "txtThongTinKhac";
            this.txtThongTinKhac.Size = new System.Drawing.Size(385, 52);
            this.txtThongTinKhac.TabIndex = 12;
            this.txtThongTinKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThongTinKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtThongTinKhac.VisualStyleManager = this.vsmMain;
            // 
            // btnThuTucHQTruoc
            // 
            this.btnThuTucHQTruoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThuTucHQTruoc.ButtonStyle = Janus.Windows.EditControls.ButtonStyle.Button;
            this.btnThuTucHQTruoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThuTucHQTruoc.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThuTucHQTruoc.Icon")));
            this.btnThuTucHQTruoc.Location = new System.Drawing.Point(544, 255);
            this.btnThuTucHQTruoc.Name = "btnThuTucHQTruoc";
            this.btnThuTucHQTruoc.Size = new System.Drawing.Size(118, 33);
            this.btnThuTucHQTruoc.TabIndex = 4;
            this.btnThuTucHQTruoc.Text = "Thủ tục HQ trước đó";
            this.btnThuTucHQTruoc.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnThuTucHQTruoc.Click += new System.EventHandler(this.btnThuTucHQTruoc_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 256);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Thông tin khác";
            // 
            // txtTenDVBan
            // 
            this.txtTenDVBan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDVBan.Location = new System.Drawing.Point(371, 180);
            this.txtTenDVBan.MaxLength = 255;
            this.txtTenDVBan.Name = "txtTenDVBan";
            this.txtTenDVBan.Size = new System.Drawing.Size(151, 21);
            this.txtTenDVBan.TabIndex = 10;
            this.txtTenDVBan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDVBan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDVBan.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(289, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Tên đơn vị bán";
            // 
            // txtMaDVBan
            // 
            this.txtMaDVBan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDVBan.Location = new System.Drawing.Point(137, 180);
            this.txtMaDVBan.MaxLength = 255;
            this.txtMaDVBan.Name = "txtMaDVBan";
            this.txtMaDVBan.Size = new System.Drawing.Size(134, 21);
            this.txtMaDVBan.TabIndex = 9;
            this.txtMaDVBan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDVBan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDVBan.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Mã đơn vị bán";
            // 
            // txtTenDVMua
            // 
            this.txtTenDVMua.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDVMua.Location = new System.Drawing.Point(371, 153);
            this.txtTenDVMua.MaxLength = 255;
            this.txtTenDVMua.Name = "txtTenDVMua";
            this.txtTenDVMua.Size = new System.Drawing.Size(151, 21);
            this.txtTenDVMua.TabIndex = 8;
            this.txtTenDVMua.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDVMua.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDVMua.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(289, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Tên đơn vị mua";
            // 
            // txtMaDVMua
            // 
            this.txtMaDVMua.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDVMua.Location = new System.Drawing.Point(137, 153);
            this.txtMaDVMua.MaxLength = 255;
            this.txtMaDVMua.Name = "txtMaDVMua";
            this.txtMaDVMua.Size = new System.Drawing.Size(134, 21);
            this.txtMaDVMua.TabIndex = 7;
            this.txtMaDVMua.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDVMua.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDVMua.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Mã đơn vị mua";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Tổng trị giá";
            // 
            // txtDiaDiemGiaoHang
            // 
            this.txtDiaDiemGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemGiaoHang.Location = new System.Drawing.Point(137, 98);
            this.txtDiaDiemGiaoHang.MaxLength = 255;
            this.txtDiaDiemGiaoHang.Name = "txtDiaDiemGiaoHang";
            this.txtDiaDiemGiaoHang.Size = new System.Drawing.Size(385, 21);
            this.txtDiaDiemGiaoHang.TabIndex = 5;
            this.txtDiaDiemGiaoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemGiaoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDiaDiemGiaoHang.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(289, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Thời hạn thanh toán";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Địa điểm giao hàng";
            // 
            // ccThoiHanTT
            // 
            // 
            // 
            // 
            this.ccThoiHanTT.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccThoiHanTT.DropDownCalendar.Name = "";
            this.ccThoiHanTT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccThoiHanTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccThoiHanTT.IsNullDate = true;
            this.ccThoiHanTT.Location = new System.Drawing.Point(399, 44);
            this.ccThoiHanTT.Name = "ccThoiHanTT";
            this.ccThoiHanTT.Nullable = true;
            this.ccThoiHanTT.NullButtonText = "Xóa";
            this.ccThoiHanTT.ShowNullButton = true;
            this.ccThoiHanTT.Size = new System.Drawing.Size(123, 21);
            this.ccThoiHanTT.TabIndex = 2;
            this.ccThoiHanTT.TodayButtonText = "Hôm nay";
            this.ccThoiHanTT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccThoiHanTT.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(137, 17);
            this.txtSoHopDong.MaxLength = 255;
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(134, 21);
            this.txtSoHopDong.TabIndex = 0;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày hợp đồng";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(8, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số hợp đồng";
            // 
            // ccNgayHopDong
            // 
            // 
            // 
            // 
            this.ccNgayHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHopDong.DropDownCalendar.Name = "";
            this.ccNgayHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHopDong.IsNullDate = true;
            this.ccNgayHopDong.Location = new System.Drawing.Point(137, 44);
            this.ccNgayHopDong.Name = "ccNgayHopDong";
            this.ccNgayHopDong.Nullable = true;
            this.ccNgayHopDong.NullButtonText = "Xóa";
            this.ccNgayHopDong.ShowNullButton = true;
            this.ccNgayHopDong.Size = new System.Drawing.Size(134, 21);
            this.ccNgayHopDong.TabIndex = 1;
            this.ccNgayHopDong.TodayButtonText = "Hôm nay";
            this.ccNgayHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDong.VisualStyleManager = this.vsmMain;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(481, 380);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 23);
            this.btnXoa.TabIndex = 5;
            this.btnXoa.Text = "Xoá hàng";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(653, 380);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnChonHang
            // 
            this.btnChonHang.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChonHang.ButtonStyle = Janus.Windows.EditControls.ButtonStyle.DropDownButton;
            this.btnChonHang.ContextMenuStrip = this.cmsThemHang;
            this.btnChonHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonHang.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChonHang.Icon")));
            this.btnChonHang.Location = new System.Drawing.Point(358, 380);
            this.btnChonHang.Name = "btnChonHang";
            this.btnChonHang.Size = new System.Drawing.Size(118, 23);
            this.btnChonHang.TabIndex = 4;
            this.btnChonHang.Text = "Thêm hàng";
            this.btnChonHang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnChonHang.VisualStyleManager = this.vsmMain;
            this.btnChonHang.Click += new System.EventHandler(this.btnChonHang_Click);
            // 
            // cmsThemHang
            // 
            this.cmsThemHang.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdAddHangTK,
            this.cmdAddHang,
            this.cmdAddExcel});
            this.cmsThemHang.Name = "cmsThemHang";
            this.cmsThemHang.Size = new System.Drawing.Size(193, 70);
            // 
            // cmdAddHangTK
            // 
            this.cmdAddHangTK.Name = "cmdAddHangTK";
            this.cmdAddHangTK.Size = new System.Drawing.Size(192, 22);
            this.cmdAddHangTK.Text = "Thêm hàng từ tờ khai";
            this.cmdAddHangTK.Click += new System.EventHandler(this.cmdAddHangTK_Click);
            // 
            // cmdAddHang
            // 
            this.cmdAddHang.Name = "cmdAddHang";
            this.cmdAddHang.Size = new System.Drawing.Size(192, 22);
            this.cmdAddHang.Text = "Thêm hàng ngoài tờ khai";
            this.cmdAddHang.Click += new System.EventHandler(this.cmdAddHang_Click);
            // 
            // cmdAddExcel
            // 
            this.cmdAddExcel.Name = "cmdAddExcel";
            this.cmdAddExcel.Size = new System.Drawing.Size(192, 22);
            this.cmdAddExcel.Text = "Thêm hàng từ excel";
            this.cmdAddExcel.Click += new System.EventHandler(this.cmdAddExcel_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(566, 380);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(82, 23);
            this.btnGhi.TabIndex = 6;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(9, 409);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(720, 152);
            this.dgList.TabIndex = 8;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.UpdatingCell += new Janus.Windows.GridEX.UpdatingCellEventHandler(this.dgList_UpdatingCell);
            this.dgList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgList_DeletingRecord);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvNgayHopDong
            // 
            this.rfvNgayHopDong.ControlToValidate = this.ccNgayHopDong;
            this.rfvNgayHopDong.ErrorMessage = "\"Ngày hợp đồng\" không được bỏ trống.";
            this.rfvNgayHopDong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayHopDong.Icon")));
            this.rfvNgayHopDong.Tag = "rfvNgayHopDong";
            // 
            // rfvSoHopDong
            // 
            this.rfvSoHopDong.ControlToValidate = this.txtSoHopDong;
            this.rfvSoHopDong.ErrorMessage = "\"Số hợp đồng\" không được để trống.";
            this.rfvSoHopDong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHopDong.Icon")));
            this.rfvSoHopDong.Tag = "rfvSoHopDong";
            // 
            // rfvPTTT
            // 
            this.rfvPTTT.ControlToValidate = this.cbPTTT;
            this.rfvPTTT.ErrorMessage = "\"Phương thức thanh toán\" không được để trống.";
            this.rfvPTTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvPTTT.Icon")));
            this.rfvPTTT.Tag = "rfvPTTT";
            // 
            // rfvDiaDiemGiaoHang
            // 
            this.rfvDiaDiemGiaoHang.ControlToValidate = this.txtDiaDiemGiaoHang;
            this.rfvDiaDiemGiaoHang.ErrorMessage = "\"Địa điểm giao hàng\" không được để trống.";
            this.rfvDiaDiemGiaoHang.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaDiemGiaoHang.Icon")));
            this.rfvDiaDiemGiaoHang.Tag = "rfvPTTT";
            // 
            // rfvTenDVMua
            // 
            this.rfvTenDVMua.ControlToValidate = this.txtTenDVMua;
            this.rfvTenDVMua.ErrorMessage = "\"Tên đơn vị mua\" không được để trống.";
            this.rfvTenDVMua.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDVMua.Icon")));
            this.rfvTenDVMua.Tag = "rfvTenDVMua";
            // 
            // rfvTenDVBan
            // 
            this.rfvTenDVBan.ControlToValidate = this.txtTenDVBan;
            this.rfvTenDVBan.ErrorMessage = "\"Tên đơn vị bán\" không được để trống.";
            this.rfvTenDVBan.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDVBan.Icon")));
            this.rfvTenDVBan.Tag = "rfvTenDVBan";
            // 
            // rfvThoiHangTT
            // 
            this.rfvThoiHangTT.ControlToValidate = this.ccThoiHanTT;
            this.rfvThoiHangTT.ErrorMessage = "\"Thời hạn thanh toán\" không được bỏ trống.";
            this.rfvThoiHangTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvThoiHangTT.Icon")));
            this.rfvThoiHangTT.Tag = "rfvThoiHangTT";
            // 
            // rvTongTriGia
            // 
            this.rvTongTriGia.ControlToValidate = this.txtTongTriGia;
            this.rvTongTriGia.ErrorMessage = "Giá trị không hợp lệ";
            this.rvTongTriGia.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTongTriGia.Icon")));
            this.rvTongTriGia.MaximumValue = "1000000000000";
            this.rvTongTriGia.MinimumValue = "0";
            this.rvTongTriGia.Tag = "rvTongTriGia";
            this.rvTongTriGia.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cvTongTriGia
            // 
            this.cvTongTriGia.ControlToValidate = this.txtTongTriGia;
            this.cvTongTriGia.ErrorMessage = "Giá trị không hợp lệ";
            this.cvTongTriGia.Icon = ((System.Drawing.Icon)(resources.GetObject("cvTongTriGia.Icon")));
            this.cvTongTriGia.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvTongTriGia.Tag = "cvTongTriGia";
            this.cvTongTriGia.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvTongTriGia.ValueToCompare = "0";
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXuLy);
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.lbTrangThai);
            this.grpTiepNhan.Controls.Add(this.label12);
            this.grpTiepNhan.Controls.Add(this.label13);
            this.grpTiepNhan.Controls.Add(this.label15);
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(9, 7);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(722, 68);
            this.grpTiepNhan.TabIndex = 0;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grpTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // btnKetQuaXuLy
            // 
            this.btnKetQuaXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKetQuaXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXuLy.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKetQuaXuLy.Icon")));
            this.btnKetQuaXuLy.Location = new System.Drawing.Point(387, 39);
            this.btnKetQuaXuLy.Name = "btnKetQuaXuLy";
            this.btnKetQuaXuLy.Size = new System.Drawing.Size(109, 23);
            this.btnKetQuaXuLy.TabIndex = 2;
            this.btnKetQuaXuLy.Text = "Kết quả xử lý";
            this.btnKetQuaXuLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKetQuaXuLy.Click += new System.EventHandler(this.btnKetQuaXuLy_Click);
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(355, 15);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(141, 21);
            this.ccNgayTiepNhan.TabIndex = 1;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(121, 15);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(142, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // lbTrangThai
            // 
            this.lbTrangThai.AutoSize = true;
            this.lbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lbTrangThai.Location = new System.Drawing.Point(118, 50);
            this.lbTrangThai.Name = "lbTrangThai";
            this.lbTrangThai.Size = new System.Drawing.Size(56, 13);
            this.lbTrangThai.TabIndex = 2;
            this.lbTrangThai.Text = "Trạng thái";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(21, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Trạng thái";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(269, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Ngày tiếp nhận";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Số tiếp nhận";
            // 
            // btnKhaiBao
            // 
            this.btnKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKhaiBao.Icon")));
            this.btnKhaiBao.Location = new System.Drawing.Point(9, 380);
            this.btnKhaiBao.Name = "btnKhaiBao";
            this.btnKhaiBao.Size = new System.Drawing.Size(88, 23);
            this.btnKhaiBao.TabIndex = 2;
            this.btnKhaiBao.Text = "Khai  báo";
            this.btnKhaiBao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKhaiBao.Click += new System.EventHandler(this.btnKhaiBao_Click);
            // 
            // btnLayPhanHoi
            // 
            this.btnLayPhanHoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLayPhanHoi.Icon")));
            this.btnLayPhanHoi.Location = new System.Drawing.Point(103, 380);
            this.btnLayPhanHoi.Name = "btnLayPhanHoi";
            this.btnLayPhanHoi.Size = new System.Drawing.Size(109, 23);
            this.btnLayPhanHoi.TabIndex = 3;
            this.btnLayPhanHoi.Text = "Lấy phản hồi";
            this.btnLayPhanHoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLayPhanHoi.Click += new System.EventHandler(this.btnLayPhanHoi_Click);
            // 
            // HopDongForm
            // 
            this.AcceptButton = this.btnGhi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(741, 573);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HopDongForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hợp đồng";
            this.Load += new System.EventHandler(this.HopDongForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.cmsThemHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvPTTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemGiaoHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVBan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvThoiHangTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTongTriGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTongTriGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayHopDong;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemGiaoHang;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDVMua;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDVMua;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDVBan;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDVBan;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtThongTinKhac;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnChonHang;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.CalendarCombo.CalendarCombo ccThoiHanTT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGia;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.EditControls.UIComboBox cbDKGH;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIComboBox cbPTTT;
        private Janus.Windows.GridEX.GridEX dgList;
        private Company.Interface.Controls.NguyenTeControl nguyenTeControl1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayHopDong;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoHopDong;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvPTTT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaDiemGiaoHang;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDVMua;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDVBan;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvThoiHangTT;
        private Company.Controls.CustomValidation.RangeValidator rvTongTriGia;
        private Company.Controls.CustomValidation.CompareValidator cvTongTriGia;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIButton btnKhaiBao;
        private Janus.Windows.EditControls.UIButton btnLayPhanHoi;
        private Janus.Windows.EditControls.UIButton btnKetQuaXuLy;
        private System.Windows.Forms.Label lbTrangThai;
        private System.Windows.Forms.ContextMenuStrip cmsThemHang;
        private System.Windows.Forms.ToolStripMenuItem cmdAddHangTK;
        private System.Windows.Forms.ToolStripMenuItem cmdAddHang;
        private System.Windows.Forms.ToolStripMenuItem cmdAddExcel;
        private Janus.Windows.EditControls.UIButton btnThuTucHQTruoc;
    }
}
