﻿using System;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.BLL.Utils;
using Company.BLL.DuLieuChuan;
using System.Data;

namespace Company.Interface.SXXK
{
    public partial class NguyenPhuLieuEditForm : BaseForm
    {                
        public NguyenPhuLieuEditForm()
        {
            InitializeComponent();
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {          
           this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            // Kiểm tra tính hợp lệ của mã HS.
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                error.SetIconPadding(txtMaHS, -8);
              //  error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                }
                else
                {
                    error.SetError(txtMaHS, "HS code is invalid");
                }
                return;
            }

            if (!cvError.IsValid) return;
                  
            BLL.SXXK.NguyenPhuLieu nplSXXK = new BLL.SXXK.NguyenPhuLieu();
            nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            nplSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            nplSXXK.Ma = txtMa.Text.Trim();
            if (nplSXXK.Load())
            {
               // ShowMessage("Nguyên phụ liệu này đã được đăng ký.", false);
                MLMessages("Nguyên phụ liệu này đã được đăng ký.", "MSG_PUB06", "", false);
                return;
            }

            nplSXXK.Ten = txtTen.Text.Trim(); ;
            nplSXXK.MaHS = txtMaHS.Text.Trim(); ;
            nplSXXK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            nplSXXK.InsertUpdate();
            this.Close();
        }
      
        private void NguyenPhuLieuEditForm_Load(object sender, EventArgs e)
        {

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;                      
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                error.SetIconPadding(txtMaHS, -8);
               // error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                }
                else
                {
                    error.SetError(txtMaHS, "HS code is invalid");
                }
            }
            else
            {
                error.SetError(txtMaHS, string.Empty);
            }            
        }

        private void txtMaHS_TextChanged(object sender, EventArgs e)
        {

        }
    }
}