﻿using System.Configuration;
using System.Windows.Forms;
using System;
using System.Xml;
using System.Collections.Generic;

namespace Company.Interface
{
    public class GlobalSettings
    {
        // Add by H.N.Khánh - 29/02/2012 
        // contents: Thêm UserID và IsRemember
        public static string UserId = "";
        public static bool IsRemember = false;


        //User name & password đồng bộ
        //User name & password đồng bộ
        public static string USERNAME_DONGBO;
        public static string PASSWOR_DONGBO;
        public static int SOTOKHAI_DONGBO;
        public static bool ISKHAIBAO = true;

        public static int ChiPhiKhac;
        public static float FontDongHang;
        public static float FontToKhai;
        public static bool WordWrap;
        public static int TriGiaNT;
        public static int DonGiaNT;
        public static string DiaChiWS;
        public static string DiaChiWS_Host;
        public static string DiaChiWS_Name;
        public static string ActiveStatus;
        public static int HanThanhKhoan;
        public static int ThongBaoThanhKhoan;
        public static string UserLog;
        public static string PassLog;
        public static int ThongBaoHetHan;
        public static decimal SoTienKhoanTKN;
        public static decimal SoTienKhoanTKX;
        public static bool Import;
        public static string NGON_NGU;
        public static string NGAYSAOLUU;
        public static string NHAC_NHO_SAO_LUU;
        public static string PathBackup;
        public static string LastBackup;
        public static bool NgayHeThong;
        public static string DiaPhuong;
        public static string TieudeNgay;
        public static string CHO_TIEP_NHAN_COLOR;
        public static string CHUA_DUYET_COLOR;
        public static string CHUA_KHAI_BAO_COLOR;
        public static string CUA_KHAU;
        public static string LOAI_HINH;
        public static string NHOM_LOAI_HINH;
        public static string NHOM_LOAI_HINH_KHAC_XUAT;
        public static string LOAI_HINH_KHAC_XUAT;
        public static string NHOM_LOAI_HINH_KHAC_NHAP;
        public static string LOAI_HINH_KHAC_NHAP;
        public static string DA_DUYET_COLOR;
        public static bool DAI_LY_TTHQ;
        public static string DKGH_MAC_DINH;
        public static string DVT_MAC_DINH;
        public static string MA_DAI_LY;
        public static string TEN_DAI_LY;
        public static string NUOC;
        public static string NGUYEN_TE_MAC_DINH;
        public static string PTTT_MAC_DINH;
        public static string PTVT_MAC_DINH;
        public static string TEN_DOI_TAC;
        public static decimal TY_GIA_USD;
        public static string CHENHLECH_THN_THX;
        public static string LoaiWS;
        public static string DATABASE_NAME;
        public static string USER;
        public static string PASS;
        public static string SERVER_NAME;
        public static string MA_CUC_HAI_QUAN;
        public static string TEN_CUC_HAI_QUAN;
        public static string MA_DON_VI;
        public static string DIA_CHI;
        public static string MA_HAI_QUAN;
        public static string TEN_HAI_QUAN;
        public static string TEN_HAI_QUAN_NGAN;
        public static string TEN_DON_VI;
        public static string PassWordDT = "";
        public static string MailDoanhNghiep;
        public static string MailHaiQuan;
        public static string MaMID;
        public static string SoDienThoaiDN;
        public static string SoFaxDN;
        public static string NguoiLienHe;
        public static string ChucVu;
        public static int MaHTS;
        public static string DIA_DIEM_DO_HANG;
        public static string TuDongTinhThue;
        public static bool iSignRemote;
        public static string userNameSign;
        public static string passwordSign;
        //phiph 23/10/2012
        public static string MienThueXNK;
        public static string TieuDeInDinhMuc;
        //Khanh 23/06/2012
        public static string TinhThueTGNT;

        public static System.Drawing.Printing.Margins MarginTKN;
        public static System.Drawing.Printing.Margins MarginTKX;
        public static System.Drawing.Printing.Margins MarginPhuLuc;
        public static System.Drawing.Printing.Margins MarginTKCT;
        public static long FileSize;
        public static string MienThueGTGT;
        public static List<string> ListTableNameSource = new List<string>();
        public static bool IsUseProxy;
        public static string Host;
        public static string Port;

        public static bool SuDungChuKySo;
        public static bool IsDaiLy { get; set; }
        public static bool IsOnlyMe = false;
        
        public static void KhoiTao_GiaTriMacDinh()
        {
            try
            {
                string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
                var settings = new ApplicationSettings();
                settings.ReadXml(settingFileName);

                NguyenPhuLieu.Ma = settings.NguyenPhuLieu[0].Ma;
                NguyenPhuLieu.Ten = settings.NguyenPhuLieu[0].Ten;
                NguyenPhuLieu.MaHS = settings.NguyenPhuLieu[0].MaHS;
                NguyenPhuLieu.DVT = settings.NguyenPhuLieu[0].DVT;

                SanPham.Ma = settings.SanPham[0].Ma;
                SanPham.Ten = settings.SanPham[0].Ten;
                SanPham.MaHS = settings.SanPham[0].MaHS;
                SanPham.DVT = settings.SanPham[0].DVT;

                DinhMuc.MaSP = settings.DinhMuc[0].MaSP;
                DinhMuc.MaNPL = settings.DinhMuc[0].MaNPL;
                DinhMuc.DinhMucSuDung = settings.DinhMuc[0].DinhMucSuDung;
                DinhMuc.DinhMucChung = settings.DinhMuc[0].DinhMucChung;
                DinhMuc.TyLeHH = settings.DinhMuc[0].TyLeHH;

                ThongTinDinhMuc.MaSP = settings.ThongTinDinhMuc[0].MaSP;
                ThongTinDinhMuc.SoDinhMuc = settings.ThongTinDinhMuc[0].SoDinhMuc;
                ThongTinDinhMuc.NgayDangKy = settings.ThongTinDinhMuc[0].NgayDangKy;
                ThongTinDinhMuc.NgayApDung = settings.ThongTinDinhMuc[0].NgayApDung;

                GiaoDien.Id = settings.GiaoDien[0].Id;

                SoThapPhan.DinhMuc = Convert.ToInt32(settings.SoThapPhan[0].DinhMuc);
                SoThapPhan.LuongNPL = Convert.ToInt32(settings.SoThapPhan[0].LuongNPL);
                SoThapPhan.LuongSP = Convert.ToInt32(settings.SoThapPhan[0].LuongSP);
                SoThapPhan.TLHH = Convert.ToInt32(settings.SoThapPhan[0].TLHH);
                SoThapPhan.SapXepTheoTK = Convert.ToInt32(settings.SoThapPhan[0].SapXepTheoTK);
                SoThapPhan.NPLKoTK = Convert.ToInt32(settings.SoThapPhan[0].NPLKoTK);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static void Luu_NPL(string ma, string ten, string maHS, string dvt)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            var settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.NguyenPhuLieu[0].Ma = ma;
            settings.NguyenPhuLieu[0].Ten = ten;
            settings.NguyenPhuLieu[0].MaHS = maHS;
            settings.NguyenPhuLieu[0].DVT = dvt;
            settings.WriteXml(settingFileName);
        }
        public static void Luu_SP(string ma, string ten, string maHS, string dvt)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            var settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.SanPham[0].Ma = ma;
            settings.SanPham[0].Ten = ten;
            settings.SanPham[0].MaHS = maHS;
            settings.SanPham[0].DVT = dvt;
            settings.WriteXml(settingFileName);
        }

        public static void Luu_DM(string maSP, string maNPL, string DMSD, string TLHH, string DMC)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            var settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.DinhMuc[0].MaSP = maSP;
            settings.DinhMuc[0].MaNPL = maNPL;
            settings.DinhMuc[0].DinhMucSuDung = DMSD;
            settings.DinhMuc[0].DinhMucChung = DMC;
            settings.DinhMuc[0].TyLeHH = TLHH;
            settings.WriteXml(settingFileName);
        }

        public static void Luu_TTDM(string maSP, string soDinhMuc, string ngayDangKy, string ngayApDung)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            var settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.ThongTinDinhMuc[0].MaSP = maSP;
            settings.ThongTinDinhMuc[0].SoDinhMuc = soDinhMuc;
            settings.ThongTinDinhMuc[0].NgayDangKy = ngayDangKy;
            settings.ThongTinDinhMuc[0].NgayApDung = ngayApDung;
            settings.WriteXml(settingFileName);
        }
        public static void Luu_GiaoDien(string id)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            var settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.GiaoDien[0].Id = id;
            settings.WriteXml(settingFileName);
        }
        public static void Luu_SoThapPhan(int dinhMuc, int luongNPL, int luongSP, int tyLeHH)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            var settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.SoThapPhan[0].DinhMuc = dinhMuc + "";
            settings.SoThapPhan[0].LuongSP = luongSP + "";
            settings.SoThapPhan[0].LuongNPL = luongNPL + "";
            settings.SoThapPhan[0].TLHH = tyLeHH + "";
            settings.WriteXml(settingFileName);
        }

        #region Nested type: NguyenPhuLieu

        public struct NguyenPhuLieu
        {
            public static string Ma;
            public static string Ten;
            public static string MaHS;
            public static string DVT;
        }

        #endregion

        #region Nested type: SanPham

        public struct SanPham
        {
            public static string Ma;
            public static string Ten;
            public static string MaHS;
            public static string DVT;
        }

        #endregion

        #region Nested type: DinhMuc

        public struct DinhMuc
        {
            public static string MaSP;
            public static string MaNPL;
            public static string DinhMucSuDung;
            public static string TyLeHH;
            public static string DinhMucChung;
        }

        #endregion

        #region Nested type: ThongTinDinhMuc

        public struct ThongTinDinhMuc
        {
            public static string MaSP;
            public static string SoDinhMuc;
            public static string NgayDangKy;
            public static string NgayApDung;
        }

        #endregion

        #region Nested type: GiaoDien

        public struct GiaoDien
        {
            public static string Id;
        }

        #endregion

        #region Nested type: SoThapPhan

        public struct SoThapPhan
        {
            public static int DinhMuc;
            public static int LuongNPL;
            public static int LuongSP;
            public static int SapXepTheoTK;
            public static int NPLKoTK;
            public static int TLHH;
            public static int TGNT;

        }

        #endregion

        public static void RefreshKey()
        {
            string tmpMaDoanhNghiep = MA_DON_VI;

            try
            {
                //xuống dòng thông tin địa chỉ
                WordWrap = bool.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("WordWrap"));
                ChiPhiKhac = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ChiPhiKhac"));
                FontDongHang = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang"));
                FontToKhai = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontToKhai"));
                ActiveStatus = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ActiveStatus");
                HanThanhKhoan = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("HanThanhKhoan"));
                ThongBaoThanhKhoan = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThongBaoThanhKhoan"));
                UserLog = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("UserLog");
                PassLog = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PassLog");
                MA_DAI_LY = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_DAI_LY");
                ThongBaoHetHan = Convert.ToInt32(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThongBaoHetHan"));
                SoTienKhoanTKN = decimal.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTienKhoanTKN"));
                SoTienKhoanTKX = decimal.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTienKhoanTKX"));
                TieudeNgay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TieudeNgay");
                Import = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Import"));
                NGON_NGU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ngonngu");
                NGAYSAOLUU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NgaySaoLuu");
                NHAC_NHO_SAO_LUU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHAC_NHO_SAO_LUU");
                PathBackup = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PathBackup");
                LastBackup = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LastBackup");
                NgayHeThong = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NgayHeThong"));
                DiaPhuong = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DiaPhuong");
                CUA_KHAU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("CUA_KHAU");
                LOAI_HINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LOAI_HINH");
                NHOM_LOAI_HINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHOM_LOAI_HINH");
                NHOM_LOAI_HINH_KHAC_XUAT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHOM_LOAI_HINH_KHAC_XUAT");
                LOAI_HINH_KHAC_XUAT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LOAI_HINH_KHAC_XUAT");
                NHOM_LOAI_HINH_KHAC_NHAP = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHOM_LOAI_HINH_KHAC_NHAP");
                LOAI_HINH_KHAC_NHAP = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LOAI_HINH_KHAC_NHAP");
                DKGH_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DKGH_MAC_DINH");
                DVT_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DVT_MAC_DINH");
                MA_DAI_LY = "";
                NUOC = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NUOC");
                NGUYEN_TE_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NGUYEN_TE_MAC_DINH");
                PTTT_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PTTT_MAC_DINH");
                PTVT_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PTVT_MAC_DINH");
                TEN_DOI_TAC = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DOI_TAC");
                TY_GIA_USD = decimal.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TY_GIA_USD"));
                CHENHLECH_THN_THX = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("CL_THN_THX");
                LoaiWS = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LoaiWS");
                DiaChiWS = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DiaChiWS");
                DATABASE_NAME = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DATABASE_NAME");
                USER = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("user");
                PASS = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("pass");
                SERVER_NAME = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ServerName");
                //MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_CUC_HAI_QUAN");
                //TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_CUC_HAI_QUAN");
                //MA_DON_VI = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_DON_VI");
                //DIA_CHI = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DIA_CHI");
                //MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_HAI_QUAN");
                //TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_HAI_QUAN");
                //TEN_HAI_QUAN_NGAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_HAI_QUAN_NGAN");
                //TEN_DON_VI = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI");
                //PassWordDT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PassWordDT");
                //MailDoanhNghiep = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MailDoanhNghiep");
                //MailHaiQuan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MailHaiQuan");
                //MaMID = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MaMID");
                MaHTS = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_HTS"));
                DIA_DIEM_DO_HANG = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DIA_DIEM_DO_HANG");

                TuDongTinhThue = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TuDongTinhThue");
                //phiph 23/10/2012
                TieuDeInDinhMuc = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TieuDeInDinhMuc");
                MienThueXNK = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MienThueXNK");
                //Khanh 23/06/2012
                TinhThueTGNT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TinhThueTGNT");
                MarginTKN = Company.KDT.SHARE.Components.Globals.GetMargin(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MarginTKN"));
                MarginTKX = Company.KDT.SHARE.Components.Globals.GetMargin(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MarginTKX"));
                MarginPhuLuc = Company.KDT.SHARE.Components.Globals.GetMargin(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MarginPhuLuc"));

                /*DATLMQ update Đọc dữ liệu từ file Config 19/01/2011.*/
                XmlDocument doc = new XmlDocument();
                string path = Company.GC.BLL.EntityBase.GetPathProgram() + "\\ConfigDoanhNghiep";
                //Hungtq update 28/01/2011.
                string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

                doc.Load(fileName);

                //HUNGTQ Updated 07/06/2011
                //Get thong tin WebService
                DiaChiWS = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS").InnerText;
                DiaChiWS_Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Host").InnerText;
                DiaChiWS_Name = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Name").InnerText;

                Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Host").InnerText;
                Port = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Port").InnerText;

                //Get thong tin Server
                SERVER_NAME = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Server").InnerText;
                //Get thong tin Database
                DATABASE_NAME = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Database").InnerText;
                //Get thong tin UserName
                USER = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "UserName").InnerText;
                //Get thong tin PassWord
                PASS = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Password").InnerText;

                //Get thông tin MaCucHQ
                MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaCucHQ").InnerText;
                //Get thông tin TenCucHQ
                TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenCucHQ").InnerText;
                //Get thông tin MaChiCucHQ
                MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaChiCucHQ").InnerText;
                //Get thông tin TenChiCucHQ
                TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenChiCucHQ").InnerText;
                //Get thông tin TenNganChiCucHQ
                TEN_HAI_QUAN_NGAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenNganChiCucHQ").InnerText;
                //Get thông tin MailHQ
                MailHaiQuan = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailHQ").InnerText;
                //Get thông tin MaDoanhNghiep
                MA_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaDoanhNghiep").InnerText;
                //Get thông tin TenDoanhNghiep
                TEN_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenDoanhNghiep").InnerText;
                //Get thông tin DiaChiDoanhNghiep
                DIA_CHI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DiaChiDoanhNghiep").InnerText;
                //Get thông tin MaMid
                MaMID = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaMID").InnerText;

                MailDoanhNghiep = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailDoanhNghiep").InnerText;
                SoDienThoaiDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai").InnerText;
                SoFaxDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax").InnerText;
                NguoiLienHe = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "NguoiLienHe").InnerText;
                ChucVu = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "ChucVu").InnerText;

                FileSize = long.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FileSize"));
                MienThueGTGT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MienThueGTGT");

                //Thong tin cau hinh proxy
                IsUseProxy = Host != "";

                TriGiaNT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TriGiaNT") != "" ? int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TriGiaNT")) : 4;
                DonGiaNT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DonGiaNT") != "" ? int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DonGiaNT")) : 6;
                SuDungChuKySo = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SuDungChuKySo"));
              

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            try
            {
                if (IsDaiLy && !string.IsNullOrEmpty(tmpMaDoanhNghiep))
                {

                    Company.Interface.DaiLy.Login lg = new Company.Interface.DaiLy.Login();
                    lg.GetValueConfig(tmpMaDoanhNghiep);
                }
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
