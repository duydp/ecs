﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    
    public class Rep1 : BasicVNACC
    {
        public PropertiesAttribute TerminalID { get; set;}
        public PropertiesAttribute AccessKey { get; set; }
        public PropertiesAttribute AutoExecuteFlag { get; set; }
        public Rep1(string TerminalID, string AccessKey)
        {
            this.TerminalID = new PropertiesAttribute(6);
            this.AccessKey = new PropertiesAttribute(16);
            this.AutoExecuteFlag = new PropertiesAttribute(1);

            this.TerminalID.SetValue(TerminalID);
            if (string.IsNullOrEmpty(AccessKey))
                throw new Exception("Bạn chưa cấu hình mật khẩu khai báo của máy (Terminal - Access key)!");
            this.AccessKey.SetValue(AccessKey);
            this.AutoExecuteFlag.SetValue("1");
        }
    }
}
