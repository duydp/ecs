﻿namespace Company.Interface.Report.VNACCS
{
    partial class ToKhaiSuaHangHoaXuatKhau1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotokhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotokhaidautien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSonhanhtokhaichianho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongsotokhaichianho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotokhaitamnhaptaixuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaphanloaikiemtra = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaloaihinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaphanloaihanghoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMahieuphuongthucvanchuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMasothuedaidien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTencoquanhaiquantiepnhantokhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNhomxulyhoso = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaydangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiodangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaythaydoidangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiothaydoidangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoihantainhapxuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBieuthitruonghophethan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaxuatkhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenxuatkhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinhXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiachinguoixuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSodtnguoixuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblManguoiuythacxuatkhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTennguoiuythacxuatkhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblManguoinhapkhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTennguoinhapkhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMabuuchinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiachi1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiachi2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiachi3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiachi4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblManuoc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadailyhaiquan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTendailyhaiquan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblManhanvienhaiquan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSovandon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoluong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadonvitinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongtrongluonghang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadonvitinhtrongluong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadiadiemluukho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTendiadiemluukho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadiadiemnhanhangcuoi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTendiadiemnhanhangcuoi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadiadiemxephang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTendiadiemxephang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaphuongtienvanchuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenphuongtienvanchuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayhangdidukien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblKyhieusohieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaihinhthuchoadon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSohoadon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaigiayphepxuatkhau1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSogiayphep1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotiepnhanhoadondientu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaigiayphepxuatkhau2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSogiayphep2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayphathanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaigiayphepxuatkhau3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSogiayphep3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongthucthanhtoan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaigiayphepxuatkhau4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSogiayphep4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadkgiahoadon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadongtienhoadon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongtrigiahoadon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaphanloaigiahoadon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaigiayphepxuatkhau5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSogiayphep5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadongtientongthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadongtientygiathue1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTygiatinhthue1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadongtientygiathue2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTygiatinhthue2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTonghesophanbothue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaphanloainhaplieutonggia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaikhongcanquydoi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNguoinopthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaxacdinhthoihannopthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloainopthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongsotienthuexuatkhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMatientetongtienthuexuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongsotienlephi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongsotienbaolanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMatientetienbaolanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongsotokhaicuatrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongsodonghangtokhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaidinhkemkhaibaodientu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSodinhkemkhaibao1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaidinhkemkhaibaodientu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSodinhkemkhaibao2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloaidinhkemkhaibaodientu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSodinhkemkhaibao3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanghichu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoquanlynoibodoanhnghiep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoquanlynguoisudung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaykhoihanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiadiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaydukiendentrungchuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaykhoihanhvanchuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiadiem2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaydukiendentrungchuyen2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaykhoihanhvanchuyen2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiadiem3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaydukiendentrungchuyen3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaykhoihanhvanchuyen3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiadiemdich = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaydukienden = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.HeightF = 850F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 22F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 10F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5,
            this.lblTenThongTinXuat});
            this.ReportHeader.HeightF = 35F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(93.30524F, 10.00001F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(631.1166F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Bản xác nhận nội dung khai bổ sung xuất khẩu (người khai Hải quan thực hiện)";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(758.9302F, 10F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable5.SizeF = new System.Drawing.SizeF(47.06976F, 21.25001F);
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoTrang});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.6;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoTrang.StylePriority.UsePadding = false;
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "1/3";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoTrang.Weight = 3;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow70,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow23,
            this.xrTableRow22,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow31,
            this.xrTableRow30,
            this.xrTableRow29,
            this.xrTableRow28,
            this.xrTableRow27,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow39,
            this.xrTableRow38,
            this.xrTableRow37,
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow51,
            this.xrTableRow50,
            this.xrTableRow52,
            this.xrTableRow54,
            this.xrTableRow53});
            this.xrTable1.SizeF = new System.Drawing.SizeF(796.0001F, 679F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell1,
            this.xrTableCell2,
            this.lblSotokhai,
            this.xrTableCell4,
            this.xrTableCell8,
            this.xrTableCell9,
            this.lblSotokhaidautien,
            this.xrTableCell11,
            this.lblSonhanhtokhaichianho,
            this.xrTableCell12,
            this.lblTongsotokhaichianho});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.Weight = 0.44000000000000006;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 0.22150664915059459;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Số tờ khai";
            this.xrTableCell1.Weight = 0.69516006837870226;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.049999999999999906;
            // 
            // lblSotokhai
            // 
            this.lblSotokhai.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSotokhai.Name = "lblSotokhai";
            this.lblSotokhai.StylePriority.UseFont = false;
            this.lblSotokhai.Text = "NNNNNNNNN1NE";
            this.lblSotokhai.Weight = 1.2358332824707037;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.13791687011718762;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "Số tờ khai đầu tiên";
            this.xrTableCell8.Weight = 1.1215624237060544;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 0.11286464691162101;
            // 
            // lblSotokhaidautien
            // 
            this.lblSotokhaidautien.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSotokhaidautien.Name = "lblSotokhaidautien";
            this.lblSotokhaidautien.StylePriority.UseFont = false;
            this.lblSotokhaidautien.Text = "XXXXXXXXX1XE";
            this.lblSotokhaidautien.Weight = 1.2274480819702147;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "-";
            this.xrTableCell11.Weight = 0.13369716644287111;
            // 
            // lblSonhanhtokhaichianho
            // 
            this.lblSonhanhtokhaichianho.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSonhanhtokhaichianho.Name = "lblSonhanhtokhaichianho";
            this.lblSonhanhtokhaichianho.StylePriority.UseFont = false;
            this.lblSonhanhtokhaichianho.Text = "NE";
            this.lblSonhanhtokhaichianho.Weight = 0.25869808197021493;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "/";
            this.xrTableCell12.Weight = 0.12328121185302754;
            // 
            // lblTongsotokhaichianho
            // 
            this.lblTongsotokhaichianho.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotokhaichianho.Name = "lblTongsotokhaichianho";
            this.lblTongsotokhaichianho.StylePriority.UseFont = false;
            this.lblTongsotokhaichianho.Text = "NE";
            this.lblTongsotokhaichianho.Weight = 2.642031823730445;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell17,
            this.lblSotokhaitamnhaptaixuat});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.Weight = 0.44000000000000006;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.22150619138694019;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrTableCell14.Weight = 1.9809938467600321;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.13791687011718773;
            // 
            // lblSotokhaitamnhaptaixuat
            // 
            this.lblSotokhaitamnhaptaixuat.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhaitamnhaptaixuat.Name = "lblSotokhaitamnhaptaixuat";
            this.lblSotokhaitamnhaptaixuat.StylePriority.UseFont = false;
            this.lblSotokhaitamnhaptaixuat.Text = "NNNNNNNNN1NE";
            this.lblSotokhaitamnhaptaixuat.Weight = 5.6195833984374763;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.lblMaphanloaikiemtra,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.lblMaloaihinh,
            this.lblMaphanloaihanghoa,
            this.lblMahieuphuongthucvanchuyen,
            this.xrTableCell21,
            this.xrTableCell18,
            this.xrTableCell22,
            this.lblMasothuedaidien});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.Weight = 0.43999999999999995;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.221505962505113;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Mã phân loại kiểm tra";
            this.xrTableCell26.Weight = 1.1741207040245552;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.2502065872112742;
            // 
            // lblMaphanloaikiemtra
            // 
            this.lblMaphanloaikiemtra.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloaikiemtra.Name = "lblMaphanloaikiemtra";
            this.lblMaphanloaikiemtra.StylePriority.UseFont = false;
            this.lblMaphanloaikiemtra.Text = "XX E";
            this.lblMaphanloaikiemtra.Weight = 0.45666698662329652;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.099999832692420576;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Text = "Mã loại hình";
            this.xrTableCell30.Weight = 0.92614586200193461;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.10244812011718735;
            // 
            // lblMaloaihinh
            // 
            this.lblMaloaihinh.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaloaihinh.Name = "lblMaloaihinh";
            this.lblMaloaihinh.StylePriority.UseFont = false;
            this.lblMaloaihinh.Text = "XXE";
            this.lblMaloaihinh.Weight = 0.34375;
            // 
            // lblMaphanloaihanghoa
            // 
            this.lblMaphanloaihanghoa.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloaihanghoa.Name = "lblMaphanloaihanghoa";
            this.lblMaphanloaihanghoa.StylePriority.UseFont = false;
            this.lblMaphanloaihanghoa.Text = "X";
            this.lblMaphanloaihanghoa.Weight = 0.20489528656005862;
            // 
            // lblMahieuphuongthucvanchuyen
            // 
            this.lblMahieuphuongthucvanchuyen.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMahieuphuongthucvanchuyen.Name = "lblMahieuphuongthucvanchuyen";
            this.lblMahieuphuongthucvanchuyen.StylePriority.UseFont = false;
            this.lblMahieuphuongthucvanchuyen.Text = "X";
            this.lblMahieuphuongthucvanchuyen.Weight = 0.23786464691162124;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Weight = 0.291081498898518;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell18.Weight = 1.8659244647220536;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Weight = 0.11020751254076912;
            // 
            // lblMasothuedaidien
            // 
            this.lblMasothuedaidien.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMasothuedaidien.Name = "lblMasothuedaidien";
            this.lblMasothuedaidien.StylePriority.UseFont = false;
            this.lblMasothuedaidien.Text = "XXXE";
            this.lblMasothuedaidien.Weight = 1.675182841892835;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.lblTencoquanhaiquantiepnhantokhai,
            this.xrTableCell41,
            this.xrTableCell47,
            this.xrTableCell23,
            this.lblNhomxulyhoso});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.44000000000000017;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.22150588621117057;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrTableCell38.Weight = 2.1189109871432947;
            // 
            // lblTencoquanhaiquantiepnhantokhai
            // 
            this.lblTencoquanhaiquantiepnhantokhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTencoquanhaiquantiepnhantokhai.Name = "lblTencoquanhaiquantiepnhantokhai";
            this.lblTencoquanhaiquantiepnhantokhai.StylePriority.UseFont = false;
            this.lblTencoquanhaiquantiepnhantokhai.Text = "XXXXXXXXXE";
            this.lblTencoquanhaiquantiepnhantokhai.Weight = 1.6771870076107311;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.29108152551845734;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Mã bộ phận xử lý tờ khai";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell47.Weight = 1.790533261869103;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.185599359123822;
            // 
            // lblNhomxulyhoso
            // 
            this.lblNhomxulyhoso.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhomxulyhoso.Name = "lblNhomxulyhoso";
            this.lblNhomxulyhoso.StylePriority.UseFont = false;
            this.lblNhomxulyhoso.Text = "XE";
            this.lblNhomxulyhoso.Weight = 1.6751822792250581;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.lblNgaydangky,
            this.xrTableCell15,
            this.lblGiodangky,
            this.xrTableCell19,
            this.xrTableCell54,
            this.xrTableCell55,
            this.lblNgaythaydoidangky,
            this.lblGiothaydoidangky});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.Weight = 0.43999999999999984;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.221505962505113;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "Ngày đăng ký";
            this.xrTableCell50.Weight = 1.4243273888274213;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.1316668607398559;
            // 
            // lblNgaydangky
            // 
            this.lblNgaydangky.Name = "lblNgaydangky";
            this.lblNgaydangky.Text = "dd/MM/yyyy";
            this.lblNgaydangky.Weight = 0.7104164944365341;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.14999983592969796;
            // 
            // lblGiodangky
            // 
            this.lblGiodangky.Name = "lblGiodangky";
            this.lblGiodangky.Text = "hh:mm:ss";
            this.lblGiodangky.Weight = 1.3796874825087764;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.29108149102965164;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Ngày thay đổi đăng ký";
            this.xrTableCell54.Weight = 1.3252201662307113;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.65091239333803363;
            // 
            // lblNgaythaydoidangky
            // 
            this.lblNgaythaydoidangky.Name = "lblNgaythaydoidangky";
            this.lblNgaythaydoidangky.Text = "dd/MM/yyyy";
            this.lblNgaythaydoidangky.Weight = 0.7948821329231;
            // 
            // lblGiothaydoidangky
            // 
            this.lblGiothaydoidangky.Name = "lblGiothaydoidangky";
            this.lblGiothaydoidangky.Text = "hh:mm:ss";
            this.lblGiothaydoidangky.Weight = 0.88030009823274147;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell5,
            this.xrTableCell7,
            this.lblThoihantainhapxuat,
            this.xrTableCell16,
            this.lblBieuthitruonghophethan,
            this.xrTableCell48});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 0.43999999999999984;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 0.221505962505113;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrTableCell5.Weight = 1.4243271599456118;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 0.13166701332772868;
            // 
            // lblThoihantainhapxuat
            // 
            this.lblThoihantainhapxuat.Name = "lblThoihantainhapxuat";
            this.lblThoihantainhapxuat.Text = "dd/MM/yyyy";
            this.lblThoihantainhapxuat.Weight = 0.7104164944365341;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "-";
            this.xrTableCell16.Weight = 0.14999998851757096;
            // 
            // lblBieuthitruonghophethan
            // 
            this.lblBieuthitruonghophethan.Name = "lblBieuthitruonghophethan";
            this.lblBieuthitruonghophethan.Text = "X";
            this.lblBieuthitruonghophethan.Weight = 1.6707692209361045;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 3.6513144670329742;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell44});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 0.44000000000000006;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 0.221506343974825;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Text = "Người xuất khẩu";
            this.xrTableCell44.Weight = 7.7384939627268121;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell62,
            this.xrTableCell61,
            this.lblMaxuatkhau});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.44000000000000006;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 0.3499999961853027;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "Mã";
            this.xrTableCell62.Weight = 0.764582670291292;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 0.099999010006606048;
            // 
            // lblMaxuatkhau
            // 
            this.lblMaxuatkhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxuatkhau.Name = "lblMaxuatkhau";
            this.lblMaxuatkhau.StylePriority.UseFont = false;
            this.lblMaxuatkhau.Text = "XXXXXXXXX1-XXE";
            this.lblMaxuatkhau.Weight = 6.7454186302184365;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.lblTenxuatkhau});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1.12;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.3499999961853027;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Text = "Tên";
            this.xrTableCell64.Weight = 0.764582670291292;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.099999620358145275;
            // 
            // lblTenxuatkhau
            // 
            this.lblTenxuatkhau.Font = new System.Drawing.Font("Arial", 8F);
            this.lblTenxuatkhau.Name = "lblTenxuatkhau";
            this.lblTenxuatkhau.StylePriority.UseFont = false;
            this.lblTenxuatkhau.StylePriority.UsePadding = false;
            this.lblTenxuatkhau.StylePriority.UseTextAlignment = false;
            this.lblTenxuatkhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenxuatkhau.Weight = 6.7454180198668974;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.lblMaBuuChinhXK});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.44000000000000017;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 0.3499999961853027;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Text = "Mã bưu chính";
            this.xrTableCell68.Weight = 0.764582670291292;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 0.099999620358145275;
            // 
            // lblMaBuuChinhXK
            // 
            this.lblMaBuuChinhXK.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhXK.Name = "lblMaBuuChinhXK";
            this.lblMaBuuChinhXK.StylePriority.UseFont = false;
            this.lblMaBuuChinhXK.Text = "XXXXXXE";
            this.lblMaBuuChinhXK.Weight = 6.7454180198668974;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.lblDiachinguoixuat});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1.12;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Weight = 0.3499999961853027;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Text = "Địa chỉ";
            this.xrTableCell72.Weight = 0.764582670291292;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 0.099999620358145275;
            // 
            // lblDiachinguoixuat
            // 
            this.lblDiachinguoixuat.Font = new System.Drawing.Font("Arial", 8F);
            this.lblDiachinguoixuat.Name = "lblDiachinguoixuat";
            this.lblDiachinguoixuat.StylePriority.UseFont = false;
            this.lblDiachinguoixuat.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiachinguoixuat.Weight = 6.7454180198668974;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.lblSodtnguoixuat});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.StylePriority.UseBorders = false;
            this.xrTableRow11.Weight = 0.43999999999999984;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 0.3499999961853027;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Text = "Số điện thoại";
            this.xrTableCell76.Weight = 0.764582670291292;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 0.099999010006606048;
            // 
            // lblSodtnguoixuat
            // 
            this.lblSodtnguoixuat.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodtnguoixuat.Name = "lblSodtnguoixuat";
            this.lblSodtnguoixuat.StylePriority.UseFont = false;
            this.lblSodtnguoixuat.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSodtnguoixuat.Weight = 6.7454186302184365;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseTextAlignment = false;
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableRow12.Weight = 0.44000000000000017;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 0.22150573362328574;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Text = "Người ủy thác xuất khẩu";
            this.xrTableCell80.Weight = 7.7384945730783512;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.xrTableCell84,
            this.xrTableCell85,
            this.lblManguoiuythacxuatkhau});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.43999999999999995;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.3499999961853027;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Text = "Mã";
            this.xrTableCell84.Weight = 0.764582670291292;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 0.1000002307096845;
            // 
            // lblManguoiuythacxuatkhau
            // 
            this.lblManguoiuythacxuatkhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManguoiuythacxuatkhau.Name = "lblManguoiuythacxuatkhau";
            this.lblManguoiuythacxuatkhau.StylePriority.UseFont = false;
            this.lblManguoiuythacxuatkhau.StylePriority.UseTextAlignment = false;
            this.lblManguoiuythacxuatkhau.Text = "XXXXXXXXX1-XXE";
            this.lblManguoiuythacxuatkhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.lblManguoiuythacxuatkhau.Weight = 6.7454174095153583;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell89,
            this.lblTennguoiuythacxuatkhau});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1.1199999999999997;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 0.3499999961853027;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Text = "Tên";
            this.xrTableCell88.Weight = 0.764582670291292;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 0.1000002307096845;
            // 
            // lblTennguoiuythacxuatkhau
            // 
            this.lblTennguoiuythacxuatkhau.Font = new System.Drawing.Font("Arial", 8F);
            this.lblTennguoiuythacxuatkhau.Name = "lblTennguoiuythacxuatkhau";
            this.lblTennguoiuythacxuatkhau.StylePriority.UseFont = false;
            this.lblTennguoiuythacxuatkhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTennguoiuythacxuatkhau.Weight = 6.7454174095153583;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 0.44000000000000017;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 0.22150573362328574;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Text = "Người nhập khẩu";
            this.xrTableCell82.Weight = 7.7384945730783512;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95,
            this.lblManguoinhapkhau});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.44000000000000039;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 0.3499999961853027;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Text = "Mã";
            this.xrTableCell94.Weight = 0.61666672134399414;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.24791617965698237;
            // 
            // lblManguoinhapkhau
            // 
            this.lblManguoinhapkhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManguoinhapkhau.Name = "lblManguoinhapkhau";
            this.lblManguoinhapkhau.StylePriority.UseFont = false;
            this.lblManguoinhapkhau.Text = "XXXXXXXXX1-XXE";
            this.lblManguoinhapkhau.Weight = 6.7454174095153583;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell99,
            this.lblTennguoinhapkhau});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.44000000000000017;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 0.3499999961853027;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Text = "Tên";
            this.xrTableCell98.Weight = 0.61666672134399414;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Weight = 0.24791617965698237;
            // 
            // lblTennguoinhapkhau
            // 
            this.lblTennguoinhapkhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTennguoinhapkhau.Name = "lblTennguoinhapkhau";
            this.lblTennguoinhapkhau.StylePriority.UseFont = false;
            this.lblTennguoinhapkhau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTennguoinhapkhau.Weight = 6.7454174095153583;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell103,
            this.lblMabuuchinh});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.44000000000000017;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 0.3499999961853027;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Text = "Mã bưu chính";
            this.xrTableCell102.Weight = 0.76458235377374728;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Weight = 0.10000054722722929;
            // 
            // lblMabuuchinh
            // 
            this.lblMabuuchinh.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMabuuchinh.Name = "lblMabuuchinh";
            this.lblMabuuchinh.StylePriority.UseFont = false;
            this.lblMabuuchinh.Text = "XXXXXXXXE";
            this.lblMabuuchinh.Weight = 6.7454174095153583;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.lblDiachi1,
            this.lblDiachi2});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.43999999999999995;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Weight = 0.3499999961853027;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Text = "Địa chỉ";
            this.xrTableCell106.Weight = 0.61666672134399414;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 0.24791648483275197;
            // 
            // lblDiachi1
            // 
            this.lblDiachi1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachi1.Name = "lblDiachi1";
            this.lblDiachi1.StylePriority.UseFont = false;
            this.lblDiachi1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiachi1.Weight = 3.2977082481384397;
            // 
            // lblDiachi2
            // 
            this.lblDiachi2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachi2.Name = "lblDiachi2";
            this.lblDiachi2.StylePriority.UseFont = false;
            this.lblDiachi2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiachi2.Weight = 3.4477088562011486;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell111,
            this.lblDiachi3,
            this.lblDiachi4});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.43999999999999995;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 0.3499999961853027;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 0.61666672134399414;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Weight = 0.24791648483275197;
            // 
            // lblDiachi3
            // 
            this.lblDiachi3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachi3.Name = "lblDiachi3";
            this.lblDiachi3.StylePriority.UseFont = false;
            this.lblDiachi3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiachi3.Weight = 3.2977082481384397;
            // 
            // lblDiachi4
            // 
            this.lblDiachi4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachi4.Name = "lblDiachi4";
            this.lblDiachi4.StylePriority.UseFont = false;
            this.lblDiachi4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiachi4.Weight = 3.4477088562011486;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell115,
            this.lblManuoc});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.43999999999999995;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.3499999961853027;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Text = "Mã nước";
            this.xrTableCell114.Weight = 0.61666672134399414;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Weight = 0.24791495895390392;
            // 
            // lblManuoc
            // 
            this.lblManuoc.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManuoc.Name = "lblManuoc";
            this.lblManuoc.StylePriority.UseFont = false;
            this.lblManuoc.Text = "XE";
            this.lblManuoc.Weight = 6.7454186302184365;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell123,
            this.lblMadailyhaiquan,
            this.lblTendailyhaiquan,
            this.xrTableCell129,
            this.lblManhanvienhaiquan});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 0.44000000000000061;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Weight = 0.22150565732934335;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "Đại lý Hải quan";
            this.xrTableCell122.Weight = 0.89307693285331191;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Weight = 0.099999315182366988;
            // 
            // lblMadailyhaiquan
            // 
            this.lblMadailyhaiquan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadailyhaiquan.Name = "lblMadailyhaiquan";
            this.lblMadailyhaiquan.StylePriority.UseFont = false;
            this.lblMadailyhaiquan.Text = "XXXXE";
            this.lblMadailyhaiquan.Weight = 0.56291841310257151;
            // 
            // lblTendailyhaiquan
            // 
            this.lblTendailyhaiquan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTendailyhaiquan.Name = "lblTendailyhaiquan";
            this.lblTendailyhaiquan.StylePriority.UseFont = false;
            this.lblTendailyhaiquan.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblTendailyhaiquan.Weight = 4.3217184792161216;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Text = "Mã nhân viên Hải quan";
            this.xrTableCell129.Weight = 1.2289648342132571;
            // 
            // lblManhanvienhaiquan
            // 
            this.lblManhanvienhaiquan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManhanvienhaiquan.Name = "lblManhanvienhaiquan";
            this.lblManhanvienhaiquan.StylePriority.UseFont = false;
            this.lblManhanvienhaiquan.Text = "XXXXE";
            this.lblManhanvienhaiquan.Weight = 0.63181667480466408;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell28,
            this.xrTableCell32,
            this.lblSovandon});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.44000000000000061;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.22150565732934335;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Text = "Số vận đơn";
            this.xrTableCell28.Weight = 0.89307693285331191;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 0.66291856206899424;
            // 
            // lblSovandon
            // 
            this.lblSovandon.Name = "lblSovandon";
            this.lblSovandon.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSovandon.Weight = 6.1824991544499879;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.lblSoluong,
            this.xrTableCell42,
            this.lblMadonvitinh});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.44000000000000061;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Weight = 0.22150565732934335;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Text = "Số lượng";
            this.xrTableCell35.Weight = 0.89307693285331191;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 0.66291764654175567;
            // 
            // lblSoluong
            // 
            this.lblSoluong.Name = "lblSoluong";
            this.lblSoluong.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoluong.StylePriority.UsePadding = false;
            this.lblSoluong.StylePriority.UseTextAlignment = false;
            this.lblSoluong.Text = "12,345,678";
            this.lblSoluong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoluong.Weight = 1.123436268974765;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Weight = 0.14999994886414658;
            // 
            // lblMadonvitinh
            // 
            this.lblMadonvitinh.Name = "lblMadonvitinh";
            this.lblMadonvitinh.Text = "XXE";
            this.lblMadonvitinh.Weight = 4.9090638521383143;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell52,
            this.xrTableCell53,
            this.lblTongtrongluonghang,
            this.xrTableCell58,
            this.lblMadonvitinhtrongluong});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.44000000000000061;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Weight = 0.22150565732934335;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Text = "Tổng trọng lượng hàng (Gross)";
            this.xrTableCell53.Weight = 1.6914115732407571;
            // 
            // lblTongtrongluonghang
            // 
            this.lblTongtrongluonghang.Name = "lblTongtrongluonghang";
            this.lblTongtrongluonghang.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongtrongluonghang.StylePriority.UsePadding = false;
            this.lblTongtrongluonghang.StylePriority.UseTextAlignment = false;
            this.lblTongtrongluonghang.Text = "1,234,567,890";
            this.lblTongtrongluonghang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongtrongluonghang.Weight = 0.9880192751290755;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Weight = 0.14999994886414658;
            // 
            // lblMadonvitinhtrongluong
            // 
            this.lblMadonvitinhtrongluong.Name = "lblMadonvitinhtrongluong";
            this.lblMadonvitinhtrongluong.Text = "XXE";
            this.lblMadonvitinhtrongluong.Weight = 4.9090638521383143;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.xrTableCell66,
            this.xrTableCell70,
            this.lblMadiadiemluukho,
            this.xrTableCell78,
            this.lblTendiadiemluukho});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.44000000000000061;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 0.22150565732934335;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Text = "Địa điểm lưu kho";
            this.xrTableCell66.Weight = 1.5559953290242081;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 0.42499904898266322;
            // 
            // lblMadiadiemluukho
            // 
            this.lblMadiadiemluukho.Name = "lblMadiadiemluukho";
            this.lblMadiadiemluukho.Text = "XXXE";
            this.lblMadiadiemluukho.Weight = 0.6984364703629613;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.14999994886414658;
            // 
            // lblTendiadiemluukho
            // 
            this.lblTendiadiemluukho.Name = "lblTendiadiemluukho";
            this.lblTendiadiemluukho.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTendiadiemluukho.Weight = 4.9090638521383143;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.xrTableCell137,
            this.xrTableCell138,
            this.lblMadiadiemnhanhangcuoi,
            this.xrTableCell140,
            this.lblTendiadiemnhanhangcuoi});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.44000000000000061;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Weight = 0.22150565732934335;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Text = "Địa điểm nhận hàng cuối cùng";
            this.xrTableCell137.Weight = 1.5559953290242081;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Weight = 0.42499904898266322;
            // 
            // lblMadiadiemnhanhangcuoi
            // 
            this.lblMadiadiemnhanhangcuoi.Name = "lblMadiadiemnhanhangcuoi";
            this.lblMadiadiemnhanhangcuoi.Text = "XXXXE";
            this.lblMadiadiemnhanhangcuoi.Weight = 0.6984364703629613;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Weight = 0.14999994886414658;
            // 
            // lblTendiadiemnhanhangcuoi
            // 
            this.lblTendiadiemnhanhangcuoi.Name = "lblTendiadiemnhanhangcuoi";
            this.lblTendiadiemnhanhangcuoi.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTendiadiemnhanhangcuoi.Weight = 4.9090638521383143;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell130,
            this.xrTableCell131,
            this.xrTableCell132,
            this.lblMadiadiemxephang,
            this.xrTableCell134,
            this.lblTendiadiemxephang});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.44000000000000061;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Weight = 0.22150565732934335;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Text = "Địa điểm xếp hàng";
            this.xrTableCell131.Weight = 1.5559953290242081;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Weight = 0.42499904898266322;
            // 
            // lblMadiadiemxephang
            // 
            this.lblMadiadiemxephang.Name = "lblMadiadiemxephang";
            this.lblMadiadiemxephang.Text = "XXXXXE";
            this.lblMadiadiemxephang.Weight = 0.6984364703629613;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Weight = 0.14999994886414658;
            // 
            // lblTendiadiemxephang
            // 
            this.lblTendiadiemxephang.Name = "lblTendiadiemxephang";
            this.lblTendiadiemxephang.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTendiadiemxephang.Weight = 4.9090638521383143;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell120,
            this.xrTableCell124,
            this.xrTableCell125,
            this.lblMaphuongtienvanchuyen,
            this.xrTableCell127,
            this.lblTenphuongtienvanchuyen});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.44000000000000061;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Weight = 0.22150565732934335;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Text = "Phương tiện vận chuyển dự kiến";
            this.xrTableCell124.Weight = 1.6914117258286303;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 0.289582652178241;
            // 
            // lblMaphuongtienvanchuyen
            // 
            this.lblMaphuongtienvanchuyen.Name = "lblMaphuongtienvanchuyen";
            this.lblMaphuongtienvanchuyen.Text = "XXXXXXXXE";
            this.lblMaphuongtienvanchuyen.Weight = 0.92614579480233428;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Weight = 0.10244811258841319;
            // 
            // lblTenphuongtienvanchuyen
            // 
            this.lblTenphuongtienvanchuyen.Name = "lblTenphuongtienvanchuyen";
            this.lblTenphuongtienvanchuyen.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTenphuongtienvanchuyen.Weight = 4.7289063639746747;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell108,
            this.xrTableCell112,
            this.xrTableCell116,
            this.lblNgayhangdidukien,
            this.xrTableCell118,
            this.xrTableCell119});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.44000000000000061;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Weight = 0.22150565732934335;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Text = "Ngày hàng đi dự kiến";
            this.xrTableCell112.Weight = 1.5559953290242081;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Weight = 0.42499904898266322;
            // 
            // lblNgayhangdidukien
            // 
            this.lblNgayhangdidukien.Name = "lblNgayhangdidukien";
            this.lblNgayhangdidukien.Text = "dd/MM/yyyy";
            this.lblNgayhangdidukien.Weight = 0.6984364703629613;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 0.14999994886414658;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Weight = 4.9090638521383143;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.lblKyhieusohieu});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.StylePriority.UseBorders = false;
            this.xrTableRow27.Weight = 1.120000000000001;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 0.22150565732934335;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Text = "Ký hiệu và số hiệu";
            this.xrTableCell91.Weight = 1.5559953290242081;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.42499904898266322;
            // 
            // lblKyhieusohieu
            // 
            this.lblKyhieusohieu.Name = "lblKyhieusohieu";
            this.lblKyhieusohieu.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            this.lblKyhieusohieu.Weight = 5.757500271365422;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell96,
            this.xrTableCell100,
            this.xrTableCell142,
            this.xrTableCell145,
            this.xrTableCell144,
            this.lblPhanloaihinhthuchoadon,
            this.xrTableCell146,
            this.lblSohoadon});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 0.4400000000000005;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Weight = 0.22150565732934335;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Text = "Giấy phép xuất khẩu";
            this.xrTableCell100.Weight = 2.6794308117513652;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.Weight = 0.14999997701309742;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Text = "Số hóa đơn";
            this.xrTableCell145.Weight = 0.61999993304369849;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Weight = 0.10880240568202781;
            // 
            // lblPhanloaihinhthuchoadon
            // 
            this.lblPhanloaihinhthuchoadon.Name = "lblPhanloaihinhthuchoadon";
            this.lblPhanloaihinhthuchoadon.Text = "X";
            this.lblPhanloaihinhthuchoadon.Weight = 0.19999997915252113;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Text = "-";
            this.xrTableCell146.Weight = 0.18179769547598398;
            // 
            // lblSohoadon
            // 
            this.lblSohoadon.Name = "lblSohoadon";
            this.lblSohoadon.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSohoadon.Weight = 3.7984638472535992;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148,
            this.xrTableCell159,
            this.lblPhanloaigiayphepxuatkhau1,
            this.lblSogiayphep1,
            this.xrTableCell150,
            this.xrTableCell151,
            this.xrTableCell152,
            this.lblSotiepnhanhoadondientu});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.4400000000000005;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 0.47499985277038881;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Text = "1";
            this.xrTableCell159.Weight = 0.1792864051795369;
            // 
            // lblPhanloaigiayphepxuatkhau1
            // 
            this.lblPhanloaigiayphepxuatkhau1.Name = "lblPhanloaigiayphepxuatkhau1";
            this.lblPhanloaigiayphepxuatkhau1.Text = "XXXE";
            this.lblPhanloaigiayphepxuatkhau1.Weight = 0.46029612631548367;
            // 
            // lblSogiayphep1
            // 
            this.lblSogiayphep1.Name = "lblSogiayphep1";
            this.lblSogiayphep1.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSogiayphep1.Weight = 1.7863540848152992;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBorders = false;
            this.xrTableCell150.Weight = 0.14999967183735127;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Text = "Số tiếp nhận hóa đơn điện tử";
            this.xrTableCell151.Weight = 1.5909387711412837;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Weight = 0.16041685593219235;
            // 
            // lblSotiepnhanhoadondientu
            // 
            this.lblSotiepnhanhoadondientu.Name = "lblSotiepnhanhoadondientu";
            this.lblSotiepnhanhoadondientu.Text = "NNNNNNNNN1NE";
            this.lblSotiepnhanhoadondientu.Weight = 3.1577085387101009;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell154,
            this.lblPhanloaigiayphepxuatkhau2,
            this.lblSogiayphep2,
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell162,
            this.lblNgayphathanh});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.4400000000000005;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Weight = 0.47499985277038881;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Text = "2";
            this.xrTableCell154.Weight = 0.1792864051795369;
            // 
            // lblPhanloaigiayphepxuatkhau2
            // 
            this.lblPhanloaigiayphepxuatkhau2.Name = "lblPhanloaigiayphepxuatkhau2";
            this.lblPhanloaigiayphepxuatkhau2.Text = "XXXE";
            this.lblPhanloaigiayphepxuatkhau2.Weight = 0.46029612631548367;
            // 
            // lblSogiayphep2
            // 
            this.lblSogiayphep2.Name = "lblSogiayphep2";
            this.lblSogiayphep2.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSogiayphep2.Weight = 1.7863540848152992;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.Weight = 0.14999967183735127;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Text = "Ngày phát hành";
            this.xrTableCell161.Weight = 1.5909387711412837;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.16041685593219235;
            // 
            // lblNgayphathanh
            // 
            this.lblNgayphathanh.Name = "lblNgayphathanh";
            this.lblNgayphathanh.Text = "dd/MM/yyyy";
            this.lblNgayphathanh.Weight = 3.1577085387101009;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.xrTableCell165,
            this.lblPhanloaigiayphepxuatkhau3,
            this.lblSogiayphep3,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.lblPhuongthucthanhtoan});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 0.4400000000000005;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Weight = 0.47499985277038881;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Text = "3";
            this.xrTableCell165.Weight = 0.1792864051795369;
            // 
            // lblPhanloaigiayphepxuatkhau3
            // 
            this.lblPhanloaigiayphepxuatkhau3.Name = "lblPhanloaigiayphepxuatkhau3";
            this.lblPhanloaigiayphepxuatkhau3.Text = "XXXE";
            this.lblPhanloaigiayphepxuatkhau3.Weight = 0.46029612631548367;
            // 
            // lblSogiayphep3
            // 
            this.lblSogiayphep3.Name = "lblSogiayphep3";
            this.lblSogiayphep3.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSogiayphep3.Weight = 1.7863540848152992;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseBorders = false;
            this.xrTableCell168.Weight = 0.14999967183735127;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Text = "Phương thức thanh toán";
            this.xrTableCell169.Weight = 1.5909387711412837;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 0.16041685593219235;
            // 
            // lblPhuongthucthanhtoan
            // 
            this.lblPhuongthucthanhtoan.Name = "lblPhuongthucthanhtoan";
            this.lblPhuongthucthanhtoan.Text = "XXXXXXE";
            this.lblPhuongthucthanhtoan.Weight = 3.1577085387101009;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell188,
            this.xrTableCell189,
            this.lblPhanloaigiayphepxuatkhau4,
            this.lblSogiayphep4,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194,
            this.lblMadkgiahoadon,
            this.xrTableCell197,
            this.lblMadongtienhoadon,
            this.xrTableCell196,
            this.lblTongtrigiahoadon,
            this.xrTableCell200,
            this.lblMaphanloaigiahoadon});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 0.4400000000000005;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Weight = 0.47499985277038881;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Text = "4";
            this.xrTableCell189.Weight = 0.1792864051795369;
            // 
            // lblPhanloaigiayphepxuatkhau4
            // 
            this.lblPhanloaigiayphepxuatkhau4.Name = "lblPhanloaigiayphepxuatkhau4";
            this.lblPhanloaigiayphepxuatkhau4.Text = "XXXE";
            this.lblPhanloaigiayphepxuatkhau4.Weight = 0.46029612631548367;
            // 
            // lblSogiayphep4
            // 
            this.lblSogiayphep4.Name = "lblSogiayphep4";
            this.lblSogiayphep4.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSogiayphep4.Weight = 1.7863540848152992;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.StylePriority.UseBorders = false;
            this.xrTableCell192.Weight = 0.14999967183735125;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Text = "Tổng trị giá hóa đơn";
            this.xrTableCell193.Weight = 1.0299998859657396;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Weight = 0.080600504623416391;
            // 
            // lblMadkgiahoadon
            // 
            this.lblMadkgiahoadon.Name = "lblMadkgiahoadon";
            this.lblMadkgiahoadon.Text = "XXE";
            this.lblMadkgiahoadon.Weight = 0.35075494659282358;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Text = "-";
            this.xrTableCell197.Weight = 0.12958342123795741;
            // 
            // lblMadongtienhoadon
            // 
            this.lblMadongtienhoadon.Name = "lblMadongtienhoadon";
            this.lblMadongtienhoadon.Text = "XXE";
            this.lblMadongtienhoadon.Weight = 0.37121652261247806;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Text = "-";
            this.xrTableCell196.Weight = 0.14999998383504615;
            // 
            // lblTongtrigiahoadon
            // 
            this.lblTongtrigiahoadon.Name = "lblTongtrigiahoadon";
            this.lblTongtrigiahoadon.StylePriority.UseTextAlignment = false;
            this.lblTongtrigiahoadon.Text = "12,345,678,901,234,567,890";
            this.lblTongtrigiahoadon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongtrigiahoadon.Weight = 1.5495680947675758;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Text = "-";
            this.xrTableCell200.Weight = 0.149999964761562;
            // 
            // lblMaphanloaigiahoadon
            // 
            this.lblMaphanloaigiahoadon.Name = "lblMaphanloaigiahoadon";
            this.lblMaphanloaigiahoadon.Text = "X";
            this.lblMaphanloaigiahoadon.Weight = 1.0973408413869781;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180,
            this.xrTableCell181,
            this.lblPhanloaigiayphepxuatkhau5,
            this.lblSogiayphep5,
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186,
            this.lblMadongtientongthue,
            this.xrTableCell202,
            this.lblTongthue,
            this.xrTableCell187});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.4400000000000005;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Weight = 0.47499985277038881;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Text = "5";
            this.xrTableCell181.Weight = 0.1792864051795369;
            // 
            // lblPhanloaigiayphepxuatkhau5
            // 
            this.lblPhanloaigiayphepxuatkhau5.Name = "lblPhanloaigiayphepxuatkhau5";
            this.lblPhanloaigiayphepxuatkhau5.Text = "XXXE";
            this.lblPhanloaigiayphepxuatkhau5.Weight = 0.46029612631548367;
            // 
            // lblSogiayphep5
            // 
            this.lblSogiayphep5.Name = "lblSogiayphep5";
            this.lblSogiayphep5.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSogiayphep5.Weight = 1.7863540848152992;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.Weight = 0.14999967183735127;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Text = "Tổng trị giá tính thuế";
            this.xrTableCell185.Weight = 1.4613550457862696;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Weight = 0.1295837295897721;
            // 
            // lblMadongtientongthue
            // 
            this.lblMadongtientongthue.Name = "lblMadongtientongthue";
            this.lblMadongtientongthue.Text = "XXE";
            this.lblMadongtientongthue.Weight = 0.37121635200981046;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Text = "-";
            this.xrTableCell202.Weight = 0.14999994674676764;
            // 
            // lblTongthue
            // 
            this.lblTongthue.Name = "lblTongthue";
            this.lblTongthue.StylePriority.UseTextAlignment = false;
            this.lblTongthue.Text = "12,345,678,901,234,567,890";
            this.lblTongthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongthue.Weight = 1.5495676571361303;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 1.247341434514827;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell172,
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178,
            this.lblMadongtientygiathue1,
            this.xrTableCell216,
            this.lblTygiatinhthue1,
            this.lblMadongtientygiathue2,
            this.xrTableCell215,
            this.lblTygiatinhthue2});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.4400000000000005;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Weight = 0.47499985277038881;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Weight = 0.1792864051795369;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Weight = 0.46029612631548367;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Weight = 1.7863540848152992;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.Weight = 0.14999967183735125;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Text = "Tỷ giá tính thuế";
            this.xrTableCell177.Weight = 0.96666729741266433;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Weight = 0.14393309317649139;
            // 
            // lblMadongtientygiathue1
            // 
            this.lblMadongtientygiathue1.Name = "lblMadongtientygiathue1";
            this.lblMadongtientygiathue1.Text = "XXE";
            this.lblMadongtientygiathue1.Weight = 0.35075500487196565;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Weight = 0.12958313513569553;
            // 
            // lblTygiatinhthue1
            // 
            this.lblTygiatinhthue1.Name = "lblTygiatinhthue1";
            this.lblTygiatinhthue1.Text = "123456789";
            this.lblTygiatinhthue1.Weight = 0.99203191916225553;
            // 
            // lblMadongtientygiathue2
            // 
            this.lblMadongtientygiathue2.Name = "lblMadongtientygiathue2";
            this.lblMadongtientygiathue2.Text = "XXE";
            this.lblMadongtientygiathue2.Weight = 0.36070310371469338;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Weight = 0.10460877276898417;
            // 
            // lblTygiatinhthue2
            // 
            this.lblTygiatinhthue2.Name = "lblTygiatinhthue2";
            this.lblTygiatinhthue2.Text = "123456789";
            this.lblTygiatinhthue2.Weight = 1.8607818395408271;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell205,
            this.xrTableCell206,
            this.xrTableCell207,
            this.xrTableCell208,
            this.xrTableCell209,
            this.xrTableCell210,
            this.xrTableCell211,
            this.lblTonghesophanbothue,
            this.xrTableCell218,
            this.lblMaphanloainhaplieutonggia});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.StylePriority.UseBorders = false;
            this.xrTableRow40.Weight = 0.4400000000000005;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Weight = 0.47499985277038881;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Weight = 0.1792864051795369;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Weight = 0.46029612631548367;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Weight = 1.7863540848152992;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.StylePriority.UseBorders = false;
            this.xrTableCell209.Weight = 0.14999967183735127;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Text = "Tổng hệ số phân bổ trị giá tính thuế";
            this.xrTableCell210.Weight = 1.9621559221200486;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Weight = 0.14999876630812975;
            // 
            // lblTonghesophanbothue
            // 
            this.lblTonghesophanbothue.Name = "lblTonghesophanbothue";
            this.lblTonghesophanbothue.StylePriority.UseTextAlignment = false;
            this.lblTonghesophanbothue.Text = "12,345,678,901,234,567,890";
            this.lblTonghesophanbothue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTonghesophanbothue.Weight = 1.5495676571361303;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Text = "-";
            this.xrTableCell218.Weight = 0.15000055709825988;
            // 
            // lblMaphanloainhaplieutonggia
            // 
            this.lblMaphanloainhaplieutonggia.Name = "lblMaphanloainhaplieutonggia";
            this.lblMaphanloainhaplieutonggia.Text = "X";
            this.lblMaphanloainhaplieutonggia.Weight = 1.0973412631210084;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell220,
            this.xrTableCell221,
            this.xrTableCell222,
            this.lblPhanloaikhongcanquydoi,
            this.xrTableCell223,
            this.xrTableCell224,
            this.lblNguoinopthue,
            this.xrTableCell226,
            this.xrTableCell227,
            this.lblMaxacdinhthoihannopthue,
            this.xrTableCell232,
            this.xrTableCell228,
            this.lblPhanloainopthue});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 0.4400000000000005;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Weight = 0.22150659193006611;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Text = "Phân loại không cần quy đổi VND";
            this.xrTableCell221.Weight = 1.69141035035589;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Weight = 0.099999989608512238;
            // 
            // lblPhanloaikhongcanquydoi
            // 
            this.lblPhanloaikhongcanquydoi.Name = "lblPhanloaikhongcanquydoi";
            this.lblPhanloaikhongcanquydoi.Text = "X";
            this.lblPhanloaikhongcanquydoi.Weight = 0.32749935258902324;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Weight = 0.1475007465482534;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Text = "Người nộp thuế";
            this.xrTableCell224.Weight = 0.85947915122391527;
            // 
            // lblNguoinopthue
            // 
            this.lblNguoinopthue.Name = "lblNguoinopthue";
            this.lblNguoinopthue.Text = "X";
            this.lblNguoinopthue.Weight = 0.43234305609273344;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Weight = 0.19999937091840758;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Text = "Mã xác định thời hạn nộp thuế";
            this.xrTableCell227.Weight = 1.5245868860787488;
            // 
            // lblMaxacdinhthoihannopthue
            // 
            this.lblMaxacdinhthoihannopthue.Name = "lblMaxacdinhthoihannopthue";
            this.lblMaxacdinhthoihannopthue.Weight = 0.49028434951676958;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Weight = 0.10460874362941319;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Text = "Phân loại nộp thuế";
            this.xrTableCell228.Weight = 1.019999873871827;
            // 
            // lblPhanloainopthue
            // 
            this.lblPhanloainopthue.Name = "lblPhanloainopthue";
            this.lblPhanloainopthue.Text = "X";
            this.lblPhanloainopthue.Weight = 0.84078184433807746;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233,
            this.xrTableCell234,
            this.xrTableCell235,
            this.lblTongsotienthuexuatkhau,
            this.xrTableCell237,
            this.lblMatientetongtienthuexuat,
            this.xrTableCell239,
            this.xrTableCell240,
            this.xrTableCell241,
            this.xrTableCell244,
            this.lblTongsotienlephi,
            this.xrTableCell242,
            this.xrTableCell245});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 0.4400000000000005;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Weight = 0.22150659193006611;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Text = "Tổng số tiền thuế xuất khẩu";
            this.xrTableCell234.Weight = 1.69141035035589;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Weight = 0.099999989608512238;
            // 
            // lblTongsotienthuexuatkhau
            // 
            this.lblTongsotienthuexuatkhau.Name = "lblTongsotienthuexuatkhau";
            this.lblTongsotienthuexuatkhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongsotienthuexuatkhau.StylePriority.UsePadding = false;
            this.lblTongsotienthuexuatkhau.StylePriority.UseTextAlignment = false;
            this.lblTongsotienthuexuatkhau.Text = "12,345,678,901";
            this.lblTongsotienthuexuatkhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongsotienthuexuatkhau.Weight = 0.88801961293470622;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Weight = 0.14999937297033472;
            // 
            // lblMatientetongtienthuexuat
            // 
            this.lblMatientetongtienthuexuat.Name = "lblMatientetongtienthuexuat";
            this.lblMatientetongtienthuexuat.Text = "XXE";
            this.lblMatientetongtienthuexuat.Weight = 0.29646026445615103;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Weight = 0.43234305609273344;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Weight = 0.19999937091840758;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Text = "Tổng số tiền lệ phí";
            this.xrTableCell241.Weight = 1.0333528116249839;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Weight = 0.14999933639527519;
            // 
            // lblTongsotienlephi
            // 
            this.lblTongsotienlephi.Name = "lblTongsotienlephi";
            this.lblTongsotienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongsotienlephi.StylePriority.UsePadding = false;
            this.lblTongsotienlephi.StylePriority.UseTextAlignment = false;
            this.lblTongsotienlephi.Text = "12,345,678,901";
            this.lblTongsotienlephi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongsotienlephi.Weight = 0.83151878451689254;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.Weight = 0.10460812427052346;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Text = "VND";
            this.xrTableCell245.Weight = 1.8607826406271613;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell248,
            this.lblTongsotienbaolanh,
            this.xrTableCell250,
            this.lblMatientetienbaolanh,
            this.xrTableCell252,
            this.xrTableCell253});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.StylePriority.UseBorders = false;
            this.xrTableRow43.Weight = 0.4400000000000005;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Weight = 0.22150659193006611;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Text = "Số tiền bảo lãnh";
            this.xrTableCell247.Weight = 1.69141035035589;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Weight = 0.099999989608512238;
            // 
            // lblTongsotienbaolanh
            // 
            this.lblTongsotienbaolanh.Name = "lblTongsotienbaolanh";
            this.lblTongsotienbaolanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongsotienbaolanh.StylePriority.UsePadding = false;
            this.lblTongsotienbaolanh.StylePriority.UseTextAlignment = false;
            this.lblTongsotienbaolanh.Text = "12,345,678,901";
            this.lblTongsotienbaolanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongsotienbaolanh.Weight = 0.88801961293470622;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.Weight = 0.14999937297033472;
            // 
            // lblMatientetienbaolanh
            // 
            this.lblMatientetienbaolanh.Name = "lblMatientetienbaolanh";
            this.lblMatientetienbaolanh.Text = "XXE";
            this.lblMatientetienbaolanh.Weight = 0.29646026445615103;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Weight = 0.43234305609273344;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Weight = 4.1802610683532446;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell254,
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell257,
            this.xrTableCell258,
            this.lblTongsotokhaicuatrang,
            this.xrTableCell271,
            this.lblTongsodonghangtokhai});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.StylePriority.UseBorders = false;
            this.xrTableRow44.Weight = 0.4400000000000005;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Weight = 0.22150659193006611;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Weight = 1.69141035035589;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Weight = 1.434479053907181;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Text = "Tổng số trang của tờ khai";
            this.xrTableCell257.Weight = 1.2944789168492379;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Weight = 0.16041715741865115;
            // 
            // lblTongsotokhaicuatrang
            // 
            this.lblTongsotokhaicuatrang.Name = "lblTongsotokhaicuatrang";
            this.lblTongsotokhaicuatrang.Text = "NE";
            this.lblTongsotokhaicuatrang.Weight = 0.599999925342167;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.Text = "Tổng số dòng hàng của tờ khai";
            this.xrTableCell271.Weight = 1.7169263849729832;
            // 
            // lblTongsodonghangtokhai
            // 
            this.lblTongsodonghangtokhai.Name = "lblTongsodonghangtokhai";
            this.lblTongsodonghangtokhai.Text = "NE";
            this.lblTongsodonghangtokhai.Weight = 0.8407819259254623;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell262,
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell265,
            this.lblPhanloaidinhkemkhaibaodientu1,
            this.xrTableCell267,
            this.lblSodinhkemkhaibao1,
            this.xrTableCell274,
            this.xrTableCell273,
            this.lblPhanloaidinhkemkhaibaodientu2,
            this.xrTableCell275,
            this.lblSodinhkemkhaibao2,
            this.xrTableCell277,
            this.xrTableCell276,
            this.lblPhanloaidinhkemkhaibaodientu3,
            this.xrTableCell279,
            this.lblSodinhkemkhaibao3});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.StylePriority.UseBorders = false;
            this.xrTableRow45.Weight = 0.4400000000000005;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Weight = 0.22150659193006611;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.Text = "Số đính kèm khai báo điện tử";
            this.xrTableCell263.Weight = 1.69141035035589;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.Weight = 0.099999989608512238;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.Text = "1";
            this.xrTableCell265.Weight = 0.18958280348378853;
            // 
            // lblPhanloaidinhkemkhaibaodientu1
            // 
            this.lblPhanloaidinhkemkhaibaodientu1.Name = "lblPhanloaidinhkemkhaibaodientu1";
            this.lblPhanloaidinhkemkhaibaodientu1.Text = "XXE";
            this.lblPhanloaidinhkemkhaibaodientu1.Weight = 0.28541760082923417;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseTextAlignment = false;
            this.xrTableCell267.Text = "-";
            this.xrTableCell267.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell267.Weight = 0.14999955739644655;
            // 
            // lblSodinhkemkhaibao1
            // 
            this.lblSodinhkemkhaibao1.Name = "lblSodinhkemkhaibao1";
            this.lblSodinhkemkhaibao1.Text = "NNNNNNNNN1NE";
            this.lblSodinhkemkhaibao1.Weight = 1.1418223447444562;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.Weight = 0.19999936827168396;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.Text = "2";
            this.xrTableCell273.Weight = 0.18959997066207668;
            // 
            // lblPhanloaidinhkemkhaibaodientu2
            // 
            this.lblPhanloaidinhkemkhaibaodientu2.Name = "lblPhanloaidinhkemkhaibaodientu2";
            this.lblPhanloaidinhkemkhaibaodientu2.Text = "XXE";
            this.lblPhanloaidinhkemkhaibaodientu2.Weight = 0.28999989358308809;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.StylePriority.UseTextAlignment = false;
            this.xrTableCell275.Text = "-";
            this.xrTableCell275.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell275.Weight = 0.14999990965848875;
            // 
            // lblSodinhkemkhaibao2
            // 
            this.lblSodinhkemkhaibao2.Name = "lblSodinhkemkhaibao2";
            this.lblSodinhkemkhaibao2.Text = "NNNNNNNNN1NE";
            this.lblSodinhkemkhaibao2.Weight = 1.2834163054374628;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.Weight = 0.10185546698826886;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.Text = "3";
            this.xrTableCell276.Weight = 0.17999998012564528;
            // 
            // lblPhanloaidinhkemkhaibaodientu3
            // 
            this.lblPhanloaidinhkemkhaibaodientu3.Name = "lblPhanloaidinhkemkhaibaodientu3";
            this.lblPhanloaidinhkemkhaibaodientu3.Text = "XXE";
            this.lblPhanloaidinhkemkhaibaodientu3.Weight = 0.32055842573205529;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.Text = "-";
            this.xrTableCell279.Weight = 0.14999999271010733;
            // 
            // lblSodinhkemkhaibao3
            // 
            this.lblSodinhkemkhaibao3.Name = "lblSodinhkemkhaibao3";
            this.lblSodinhkemkhaibao3.Text = "NNNNNNNNN1NE";
            this.lblSodinhkemkhaibao3.Weight = 1.3148317551843678;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell281,
            this.xrTableCell282,
            this.xrTableCell283,
            this.lblPhanghichu});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1.1200000000000014;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.Weight = 0.22150659193006611;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Text = "Phần ghi chú";
            this.xrTableCell282.Weight = 1.69141035035589;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.Weight = 0.099999989608512238;
            // 
            // lblPhanghichu
            // 
            this.lblPhanghichu.Name = "lblPhanghichu";
            this.lblPhanghichu.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblPhanghichu.Weight = 5.94708337480717;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell285,
            this.xrTableCell286,
            this.lblSoquanlynoibodoanhnghiep,
            this.xrTableCell291,
            this.xrTableCell289,
            this.xrTableCell292,
            this.lblSoquanlynguoisudung});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.StylePriority.UseBorders = false;
            this.xrTableRow47.Weight = 0.44000000000000061;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.Weight = 0.22150659193006611;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Text = "Số quản lý của nội bộ doanh nghiệp";
            this.xrTableCell286.Weight = 1.980993141811779;
            // 
            // lblSoquanlynoibodoanhnghiep
            // 
            this.lblSoquanlynoibodoanhnghiep.Name = "lblSoquanlynoibodoanhnghiep";
            this.lblSoquanlynoibodoanhnghiep.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoquanlynoibodoanhnghiep.Weight = 1.9590369496227249;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.Weight = 0.081536514082587219;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.Text = "Số quản lý người sử dụng";
            this.xrTableCell289.Weight = 1.3908339253889328;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Weight = 0.099999988517571067;
            // 
            // lblSoquanlynguoisudung
            // 
            this.lblSoquanlynguoisudung.Name = "lblSoquanlynguoisudung";
            this.lblSoquanlynguoisudung.Text = "XXXXE";
            this.lblSoquanlynguoisudung.Weight = 2.2260931953479774;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell293,
            this.xrTableCell294,
            this.xrTableCell295,
            this.xrTableCell300});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 0.44000000000000061;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.Weight = 0.22150659193006611;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.Text = "Mục thông báo của Hải quan";
            this.xrTableCell294.Weight = 1.69141035035589;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.Weight = 0.099999989608512238;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.Weight = 5.9470833748071712;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell296,
            this.xrTableCell297,
            this.xrTableCell298,
            this.lblNgaykhoihanh});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 0.44000000000000061;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.Weight = 0.47499990999084135;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.Text = "Thời hạn cho phép vận chuyển bảo thuế (khởi hành)";
            this.xrTableCell297.Weight = 2.75609406599495;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.Weight = 0.11630186761947289;
            // 
            // lblNgaykhoihanh
            // 
            this.lblNgaykhoihanh.Name = "lblNgaykhoihanh";
            this.lblNgaykhoihanh.Text = "dd/MM/yyyy";
            this.lblNgaykhoihanh.Weight = 4.6126044630963765;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell305,
            this.xrTableCell306,
            this.xrTableCell307,
            this.xrTableCell316,
            this.xrTableCell315,
            this.xrTableCell317,
            this.xrTableCell314,
            this.xrTableCell318,
            this.xrTableCell308});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 0.44000000000000061;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.Weight = 0.22150659193006611;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.Weight = 1.69141035035589;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.Weight = 0.099999989608512238;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.Weight = 0.62499989162119018;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell315.StylePriority.UsePadding = false;
            this.xrTableCell315.StylePriority.UseTextAlignment = false;
            this.xrTableCell315.Text = "Địa điểm";
            this.xrTableCell315.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell315.Weight = 0.86177095208060261;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell317.StylePriority.UsePadding = false;
            this.xrTableCell317.StylePriority.UseTextAlignment = false;
            this.xrTableCell317.Text = "Ngày đến";
            this.xrTableCell317.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell317.Weight = 0.8089976732281482;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.Weight = 0.1506536326111062;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell318.StylePriority.UsePadding = false;
            this.xrTableCell318.StylePriority.UseTextAlignment = false;
            this.xrTableCell318.Text = "Ngày khởi hành";
            this.xrTableCell318.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell318.Weight = 0.85862905486582164;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.Weight = 2.6420321704003022;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell301,
            this.xrTableCell302,
            this.xrTableCell303,
            this.xrTableCell311,
            this.lblDiadiem,
            this.lblNgaydukiendentrungchuyen,
            this.xrTableCell309,
            this.lblNgaykhoihanhvanchuyen,
            this.xrTableCell304});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 0.44000000000000061;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.Weight = 0.47499990999084135;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.Text = "Thông tin trung chuyển";
            this.xrTableCell302.Weight = 1.865416296562302;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.Weight = 0.14750105226947025;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.Text = "1";
            this.xrTableCell311.Weight = 0.14999952654607662;
            // 
            // lblDiadiem
            // 
            this.lblDiadiem.Name = "lblDiadiem";
            this.lblDiadiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblDiadiem.StylePriority.UsePadding = false;
            this.lblDiadiem.StylePriority.UseTextAlignment = false;
            this.lblDiadiem.Text = "XXXE";
            this.lblDiadiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblDiadiem.Weight = 0.86177099022757087;
            // 
            // lblNgaydukiendentrungchuyen
            // 
            this.lblNgaydukiendentrungchuyen.Name = "lblNgaydukiendentrungchuyen";
            this.lblNgaydukiendentrungchuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblNgaydukiendentrungchuyen.StylePriority.UsePadding = false;
            this.lblNgaydukiendentrungchuyen.StylePriority.UseTextAlignment = false;
            this.lblNgaydukiendentrungchuyen.Text = "dd/MM/yyyy";
            this.lblNgaydukiendentrungchuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaydukiendentrungchuyen.Weight = 0.80899843616751377;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell309.StylePriority.UsePadding = false;
            this.xrTableCell309.StylePriority.UseTextAlignment = false;
            this.xrTableCell309.Text = "~";
            this.xrTableCell309.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell309.Weight = 0.15065271708386741;
            // 
            // lblNgaykhoihanhvanchuyen
            // 
            this.lblNgaykhoihanhvanchuyen.Name = "lblNgaykhoihanhvanchuyen";
            this.lblNgaykhoihanhvanchuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblNgaykhoihanhvanchuyen.StylePriority.UsePadding = false;
            this.lblNgaykhoihanhvanchuyen.StylePriority.UseTextAlignment = false;
            this.lblNgaykhoihanhvanchuyen.Text = "dd/MM/yyyy";
            this.lblNgaykhoihanhvanchuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaykhoihanhvanchuyen.Weight = 0.85862806304464612;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.Weight = 2.6420333148093507;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell319,
            this.xrTableCell320,
            this.xrTableCell321,
            this.xrTableCell322,
            this.lblDiadiem2,
            this.lblNgaydukiendentrungchuyen2,
            this.xrTableCell325,
            this.lblNgaykhoihanhvanchuyen2,
            this.xrTableCell327});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 0.44000000000000061;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.Weight = 0.47499990999084135;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.Weight = 1.865416296562302;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.Weight = 0.14750105226947025;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.Text = "2";
            this.xrTableCell322.Weight = 0.14999952654607662;
            // 
            // lblDiadiem2
            // 
            this.lblDiadiem2.Name = "lblDiadiem2";
            this.lblDiadiem2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblDiadiem2.StylePriority.UsePadding = false;
            this.lblDiadiem2.StylePriority.UseTextAlignment = false;
            this.lblDiadiem2.Text = "XXXE";
            this.lblDiadiem2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblDiadiem2.Weight = 0.86177099022757087;
            // 
            // lblNgaydukiendentrungchuyen2
            // 
            this.lblNgaydukiendentrungchuyen2.Name = "lblNgaydukiendentrungchuyen2";
            this.lblNgaydukiendentrungchuyen2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblNgaydukiendentrungchuyen2.StylePriority.UsePadding = false;
            this.lblNgaydukiendentrungchuyen2.StylePriority.UseTextAlignment = false;
            this.lblNgaydukiendentrungchuyen2.Text = "dd/MM/yyyy";
            this.lblNgaydukiendentrungchuyen2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaydukiendentrungchuyen2.Weight = 0.80899843616751377;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell325.StylePriority.UsePadding = false;
            this.xrTableCell325.StylePriority.UseTextAlignment = false;
            this.xrTableCell325.Text = "~";
            this.xrTableCell325.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell325.Weight = 0.15065271708386741;
            // 
            // lblNgaykhoihanhvanchuyen2
            // 
            this.lblNgaykhoihanhvanchuyen2.Name = "lblNgaykhoihanhvanchuyen2";
            this.lblNgaykhoihanhvanchuyen2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblNgaykhoihanhvanchuyen2.StylePriority.UsePadding = false;
            this.lblNgaykhoihanhvanchuyen2.StylePriority.UseTextAlignment = false;
            this.lblNgaykhoihanhvanchuyen2.Text = "dd/MM/yyyy";
            this.lblNgaykhoihanhvanchuyen2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaykhoihanhvanchuyen2.Weight = 0.85862806304464612;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.Weight = 2.6420333148093507;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell337,
            this.xrTableCell338,
            this.xrTableCell339,
            this.xrTableCell340,
            this.lblDiadiem3,
            this.lblNgaydukiendentrungchuyen3,
            this.xrTableCell343,
            this.lblNgaykhoihanhvanchuyen3,
            this.xrTableCell345});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 0.44000000000000061;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.Weight = 0.47499990999084135;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.Weight = 1.865416296562302;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.Weight = 0.14750105226947025;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.Text = "3";
            this.xrTableCell340.Weight = 0.14999952654607662;
            // 
            // lblDiadiem3
            // 
            this.lblDiadiem3.Name = "lblDiadiem3";
            this.lblDiadiem3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblDiadiem3.StylePriority.UsePadding = false;
            this.lblDiadiem3.StylePriority.UseTextAlignment = false;
            this.lblDiadiem3.Text = "XXXE";
            this.lblDiadiem3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblDiadiem3.Weight = 0.86177099022757087;
            // 
            // lblNgaydukiendentrungchuyen3
            // 
            this.lblNgaydukiendentrungchuyen3.Name = "lblNgaydukiendentrungchuyen3";
            this.lblNgaydukiendentrungchuyen3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblNgaydukiendentrungchuyen3.StylePriority.UsePadding = false;
            this.lblNgaydukiendentrungchuyen3.StylePriority.UseTextAlignment = false;
            this.lblNgaydukiendentrungchuyen3.Text = "dd/MM/yyyy";
            this.lblNgaydukiendentrungchuyen3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaydukiendentrungchuyen3.Weight = 0.80899843616751377;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell343.StylePriority.UsePadding = false;
            this.xrTableCell343.StylePriority.UseTextAlignment = false;
            this.xrTableCell343.Text = "~";
            this.xrTableCell343.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell343.Weight = 0.15065271708386741;
            // 
            // lblNgaykhoihanhvanchuyen3
            // 
            this.lblNgaykhoihanhvanchuyen3.Name = "lblNgaykhoihanhvanchuyen3";
            this.lblNgaykhoihanhvanchuyen3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblNgaykhoihanhvanchuyen3.StylePriority.UsePadding = false;
            this.lblNgaykhoihanhvanchuyen3.StylePriority.UseTextAlignment = false;
            this.lblNgaykhoihanhvanchuyen3.Text = "dd/MM/yyyy";
            this.lblNgaykhoihanhvanchuyen3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaykhoihanhvanchuyen3.Weight = 0.85862806304464612;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.Weight = 2.6420333148093507;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell328,
            this.xrTableCell329,
            this.xrTableCell331,
            this.lblDiadiemdich,
            this.lblNgaydukienden,
            this.xrTableCell334,
            this.xrTableCell335,
            this.xrTableCell336});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 0.44000000000000061;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.Weight = 0.47499990999084135;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.Text = "Địa điểm đích cho vận chuyển bảo thuế";
            this.xrTableCell329.Weight = 2.0129167373893386;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.Weight = 0.15000013798851025;
            // 
            // lblDiadiemdich
            // 
            this.lblDiadiemdich.Name = "lblDiadiemdich";
            this.lblDiadiemdich.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblDiadiemdich.StylePriority.UsePadding = false;
            this.lblDiadiemdich.StylePriority.UseTextAlignment = false;
            this.lblDiadiemdich.Text = "XXXE";
            this.lblDiadiemdich.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblDiadiemdich.Weight = 0.86177099022757087;
            // 
            // lblNgaydukienden
            // 
            this.lblNgaydukienden.Name = "lblNgaydukienden";
            this.lblNgaydukienden.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblNgaydukienden.StylePriority.UsePadding = false;
            this.lblNgaydukienden.StylePriority.UseTextAlignment = false;
            this.lblNgaydukienden.Text = "dd/MM/yyyy";
            this.lblNgaydukienden.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaydukienden.Weight = 0.80899843616751377;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.Weight = 0.15065271708386741;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.Weight = 0.85862806304464612;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.Weight = 2.6420333148093507;
            // 
            // ToKhaiSuaHangHoaXuatKhau1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.Margins = new System.Drawing.Printing.Margins(16, 18, 22, 10);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTrang;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        internal DevExpress.XtraReports.UI.XRTable xrTable1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell lblSotokhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell lblSotokhaidautien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell lblSonhanhtokhaichianho;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell lblTongsotokhaichianho;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblSotokhaitamnhaptaixuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell lblMaphanloaikiemtra;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell lblMaloaihinh;
        private DevExpress.XtraReports.UI.XRTableCell lblMaphanloaihanghoa;
        private DevExpress.XtraReports.UI.XRTableCell lblMahieuphuongthucvanchuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell lblMasothuedaidien;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell lblTencoquanhaiquantiepnhantokhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell lblNhomxulyhoso;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaydangky;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblGiodangky;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaythaydoidangky;
        private DevExpress.XtraReports.UI.XRTableCell lblGiothaydoidangky;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell lblThoihantainhapxuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell lblBieuthitruonghophethan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell lblMaxuatkhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell lblTenxuatkhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell lblDiachinguoixuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell lblSodtnguoixuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblManguoiuythacxuatkhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell lblTennguoiuythacxuatkhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell lblManguoinhapkhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell lblTennguoinhapkhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell lblMabuuchinh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell lblDiachi1;
        private DevExpress.XtraReports.UI.XRTableCell lblDiachi2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell lblDiachi3;
        private DevExpress.XtraReports.UI.XRTableCell lblDiachi4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell lblManuoc;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell lblMadailyhaiquan;
        private DevExpress.XtraReports.UI.XRTableCell lblTendailyhaiquan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell lblManhanvienhaiquan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell lblSovandon;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell lblSoluong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell lblMadonvitinh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell lblTongtrongluonghang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell lblMadonvitinhtrongluong;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell lblMadiadiemluukho;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell lblTendiadiemluukho;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell lblMadiadiemnhanhangcuoi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell lblTendiadiemnhanhangcuoi;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell lblMadiadiemxephang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell lblTendiadiemxephang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell lblMaphuongtienvanchuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell lblTenphuongtienvanchuyen;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayhangdidukien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell lblKyhieusohieu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaihinhthuchoadon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell lblSohoadon;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaigiayphepxuatkhau1;
        private DevExpress.XtraReports.UI.XRTableCell lblSogiayphep1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell lblSotiepnhanhoadondientu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaigiayphepxuatkhau2;
        private DevExpress.XtraReports.UI.XRTableCell lblSogiayphep2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayphathanh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaigiayphepxuatkhau3;
        private DevExpress.XtraReports.UI.XRTableCell lblSogiayphep3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongthucthanhtoan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaigiayphepxuatkhau4;
        private DevExpress.XtraReports.UI.XRTableCell lblSogiayphep4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell lblMadkgiahoadon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell lblMadongtienhoadon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell lblTongtrigiahoadon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell lblMaphanloaigiahoadon;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaigiayphepxuatkhau5;
        private DevExpress.XtraReports.UI.XRTableCell lblSogiayphep5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell lblMadongtientongthue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell lblTongthue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell lblMadongtientygiathue1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell lblTygiatinhthue1;
        private DevExpress.XtraReports.UI.XRTableCell lblMadongtientygiathue2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell lblTygiatinhthue2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell lblTonghesophanbothue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell lblMaphanloainhaplieutonggia;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaikhongcanquydoi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoinopthue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell lblMaxacdinhthoihannopthue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloainopthue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell lblTongsotienthuexuatkhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableCell lblMatientetongtienthuexuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell lblTongsotienlephi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell lblTongsotienbaolanh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell lblMatientetienbaolanh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell lblTongsotokhaicuatrang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell lblTongsodonghangtokhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaidinhkemkhaibaodientu1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell lblSodinhkemkhaibao1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaidinhkemkhaibaodientu2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell lblSodinhkemkhaibao2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloaidinhkemkhaibaodientu3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell lblSodinhkemkhaibao3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanghichu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell lblSoquanlynoibodoanhnghiep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell lblSoquanlynguoisudung;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaykhoihanh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell lblDiadiem;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaydukiendentrungchuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaykhoihanhvanchuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableCell lblDiadiem2;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaydukiendentrungchuyen2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaykhoihanhvanchuyen2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableCell lblDiadiem3;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaydukiendentrungchuyen3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaykhoihanhvanchuyen3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell lblDiadiemdich;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaydukienden;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
    }
}
