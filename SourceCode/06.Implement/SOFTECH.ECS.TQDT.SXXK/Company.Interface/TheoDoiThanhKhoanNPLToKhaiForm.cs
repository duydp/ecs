﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.KDT.SXXK;

namespace Company.Interface
{
    public partial class TheoDoiThanhKhoanNPLToKhaiForm : BaseForm
    {
        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTon NPL;
        public TheoDoiThanhKhoanNPLToKhaiForm()
        {
            InitializeComponent();
        }

        private void TheoDoiThanhKhoanToKhaiForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["TonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["TonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            this.Text = "Theo dõi thanh khoản tờ khai số " + NPL.SoToKhai + "/" + NPL.MaLoaiHinh.Trim() + "/" + NPL.NamDangKy.ToString("dd-MM-yyyy");
            NPLNhapTonCollection col = new NPLNhapTon().SelectCollectionDynamic("SoToKhai =" + this.NPL.SoToKhai + " AND MaLoaiHinh = '" + NPL.MaLoaiHinh + "' AND NamDangKy =" + NPL.NamDangKy + " AND MaNPL = '" + NPL.MaNPL + "' AND TonCuoi < TonDau AND LanThanhLy IN (SELECT LanThanhLy FROM t_KDT_SXXK_HoSoThanhLyDangKy) AND MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'", "TonCuoi DESC");
            dgList.DataSource = col;
        }
    }
}