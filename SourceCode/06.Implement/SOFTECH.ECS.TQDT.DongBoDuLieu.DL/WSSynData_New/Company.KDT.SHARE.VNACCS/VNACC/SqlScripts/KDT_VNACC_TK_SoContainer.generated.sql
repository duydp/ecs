-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Insert]
	@Master_id bigint,
	@SoTT int,
	@SoContainer varchar(12),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_SoContainer]
(
	[Master_id],
	[SoTT],
	[SoContainer],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@Master_id,
	@SoTT,
	@SoContainer,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Update]
	@ID bigint,
	@Master_id bigint,
	@SoTT int,
	@SoContainer varchar(12),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_SoContainer]
SET
	[Master_id] = @Master_id,
	[SoTT] = @SoTT,
	[SoContainer] = @SoContainer,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@SoTT int,
	@SoContainer varchar(12),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_SoContainer] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_SoContainer] 
		SET
			[Master_id] = @Master_id,
			[SoTT] = @SoTT,
			[SoContainer] = @SoContainer,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_SoContainer]
		(
			[Master_id],
			[SoTT],
			[SoContainer],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@Master_id,
			@SoTT,
			@SoContainer,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_SoContainer]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_SoContainer] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoTT],
	[SoContainer],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_SoContainer]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[SoTT],
	[SoContainer],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_SoContainer] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoTT],
	[SoContainer],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_SoContainer]	

GO

