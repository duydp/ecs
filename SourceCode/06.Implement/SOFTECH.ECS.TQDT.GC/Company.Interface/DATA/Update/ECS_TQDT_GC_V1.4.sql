/*
Run this script on:

        ecsteam.ECS_TQDT_GC_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_GC

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 11/03/2011 6:39:39 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HCT_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HCT_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC05TT74]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[p_GC_BC05TT74] 
	-- Add the parameters for the stored procedure here
	@HopDong_ID BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
/*ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ma) AS STT*/

SELECT    ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ma) AS STT, t_GC_NguyenPhuLieu.Ten AS TenNPL, t_GC_NguyenPhuLieu.Ma AS MaNPL, t_GC_SanPham.Ten AS TenSP, t_GC_SanPham.Ma AS MaSP, 
                      t_HaiQuan_DonViTinh.Ten AS DVT, SUM(t_GC_SanPham.SoLuongDaXuat) AS SoLuongDaXuat, t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) 
                      / 100 AS DinhMucGomTiLeHH, t_GC_SanPham.SoLuongDaXuat * (t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) / 100) AS LuongSuDung, 
                      SUM(t_GC_NguyenPhuLieu.SoLuongDaDung) AS TongSoLuongDaDung, t_GC_DinhMuc.TyLeHaoHut, t_GC_DinhMuc.DinhMucSuDung
FROM         t_GC_NguyenPhuLieu INNER JOIN
                      t_KDT_GC_HopDong ON t_GC_NguyenPhuLieu.HopDong_ID = t_KDT_GC_HopDong.ID INNER JOIN
                      t_GC_DinhMuc ON t_KDT_GC_HopDong.ID = t_GC_DinhMuc.HopDong_ID AND t_GC_NguyenPhuLieu.Ma = t_GC_DinhMuc.MaNguyenPhuLieu INNER JOIN
                      t_GC_SanPham ON t_KDT_GC_HopDong.ID = t_GC_SanPham.HopDong_ID AND t_GC_DinhMuc.MaSanPham = t_GC_SanPham.Ma INNER JOIN
                      t_HaiQuan_DonViTinh ON t_GC_SanPham.DVT_ID = t_HaiQuan_DonViTinh.ID
WHERE     (t_GC_NguyenPhuLieu.HopDong_ID = @HopDong_ID)
GROUP BY t_KDT_GC_HopDong.MaHaiQuan, t_KDT_GC_HopDong.MaDoanhNghiep, t_GC_NguyenPhuLieu.Ten, t_GC_NguyenPhuLieu.Ma, t_GC_SanPham.Ten, 
                      t_GC_SanPham.Ma, t_HaiQuan_DonViTinh.Ten, t_GC_SanPham.SoLuongDaXuat * (t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) / 100), 
                      t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) / 100, t_GC_DinhMuc.TyLeHaoHut, t_GC_DinhMuc.DinhMucSuDung
HAVING      (SUM(t_GC_SanPham.SoLuongDaXuat) > 0) AND (t_KDT_GC_HopDong.MaHaiQuan = @MaHaiQuan) AND (t_KDT_GC_HopDong.MaDoanhNghiep = @MaDoanhNghiep)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC09HSTK_GC_TT117]'
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117] 
	-- Add the parameters for the stored procedure here
	@IDHopDong BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT    ROW_NUMBER() OVER (ORDER BY BangKe.SoNgayToKhai) AS 'STT', BangKe.SoNgayToKhai, BangKe.TenHang, BangKe.SoLuong, BangKe.TriGia, t_KDT_GC_HopDong.SoHopDong + ', ' + CONVERT(NVARCHAR(20), 
                      t_KDT_GC_HopDong.NgayDangKy, 103) + ', ' + CONVERT(NVARCHAR(20), t_KDT_GC_HopDong.NgayHetHan, 103) AS HdPhuLuc, BangKe.CuaKhau, 
                      BangKe.SoNgayBL, BangKe.IDHopDong, BangKe.MaHaiQuan, BangKe.MaDoanhNghiep
FROM         (SELECT     CONVERT(NVARCHAR(20), t_KDT_ToKhaiMauDich.SoToKhai) + ', ' + CONVERT(NVARCHAR(20), t_KDT_ToKhaiMauDich.NgayDangKy, 103) 
                                              AS SoNgayToKhai, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, t_KDT_HangMauDich.TriGiaKB AS TriGia, 
                                              t_HaiQuan_CuaKhau.Ten AS CuaKhau, 
                                              CASE PTVT_ID WHEN '001' THEN '' WHEN '005' THEN '' ELSE CASE t_KDT_ToKhaiMauDich.NgayVanDon WHEN '1900-1-1' THEN t_KDT_ToKhaiMauDich.SoVanDon
                                               ELSE t_KDT_ToKhaiMauDich.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_KDT_ToKhaiMauDich.NgayVanDon, 103) END END AS SoNgayBL, 
                                              t_KDT_ToKhaiMauDich.IDHopDong, t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_ToKhaiMauDich.MaDoanhNghiep
                       FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                              t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN
                                              t_HaiQuan_CuaKhau ON t_KDT_ToKhaiMauDich.CuaKhau_ID = t_HaiQuan_CuaKhau.ID
                       WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND 
                                              (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND 
                                              (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep)
                       UNION
                       SELECT     CONVERT(NVARCHAR(20), t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) + ', ' + CONVERT(NVARCHAR(20), t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 103) 
                                             AS SoNgayToKhai, t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_HangChuyenTiep.TriGia, '' AS CuaKhau, 
                                             '' AS SoNgayBL, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan AS MaHaiQuan, 
                                             t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                       FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                             t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                       WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE '%X' OR
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                             (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep)) 
                      AS BangKe INNER JOIN
                      t_KDT_GC_HopDong ON BangKe.IDHopDong = t_KDT_GC_HopDong.ID
WHERE     (t_KDT_GC_HopDong.ID = @IDHopDong)

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC07HSTK_GC_TT117]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================

CREATE PROCEDURE [dbo].[p_GC_BC07HSTK_GC_TT117] 
	-- Add the parameters for the stored procedure here
	@HD_ID BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;  
	 --  ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',
	 
--DECLARE @HD_ID BIGINT; DECLARE @MaHaiQuan CHAR(6); DECLARE @MaDoanhNghiep VARCHAR(14)		
--SET @HD_ID=492; SET @MaDoanhNghiep='0400101556'; SET @MaHaiQuan='C34C';
 
 -- LanNT 11HT-MC2010
--Tập hợp những mã loại hình  nhập và xuất tờ khai
--Tờ khai nhập

SELECT  ROW_NUMBER() OVER (ORDER BY ThietBi.TenHang) AS 'STT',   ThietBi.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ThietBi.SoLuongTamNhap, ThietBi.SoLuongTaiXuat, 
                      SUM(ABS(ThietBi.SoLuongTamNhap - ThietBi.SoLuongTaiXuat)) AS ConLai
FROM         (SELECT     t_KDT_HangMauDich.ID, t_KDT_HangMauDich.TKMD_ID AS ID_ToKhai, t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, 
                                              t_KDT_HangMauDich.DVT_ID AS DVT, t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.NgayDangKy, 
                                              t_KDT_HangMauDich.SoLuong AS SoLuongTamNhap, 0 AS SoLuongTaiXuat, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.MaHaiQuan, 
                                              t_KDT_ToKhaiMauDich.MaDoanhNghiep
                       FROM          t_KDT_HangMauDich INNER JOIN
                                              t_KDT_ToKhaiMauDich ON t_KDT_HangMauDich.TKMD_ID = t_KDT_ToKhaiMauDich.ID
                       WHERE      (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'T') AND (t_KDT_ToKhaiMauDich.IDHopDong = @HD_ID) AND (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%NGC%')
                       UNION
                       SELECT     t_KDT_GC_HangChuyenTiep.ID, t_KDT_GC_HangChuyenTiep.Master_ID AS ID_ToKhai, t_KDT_GC_HangChuyenTiep.MaHang, 
                                             t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.ID_DVT AS DVT, t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, 
                                             t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 0 AS SoLuongTamNhap, t_KDT_GC_HangChuyenTiep.SoLuong AS SoLuongTaiXuat, 
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan AS MaHaiQuan, 
                                             t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                       FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                             t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                       WHERE     (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @HD_ID) AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%TBN%' OR
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%NGC20%' OR
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%XGC20%')) AS ThietBi INNER JOIN
                      t_HaiQuan_DonViTinh ON ThietBi.DVT = t_HaiQuan_DonViTinh.ID
GROUP BY ThietBi.TenHang, t_HaiQuan_DonViTinh.Ten, ThietBi.SoLuongTamNhap, ThietBi.SoLuongTaiXuat, ThietBi.MaHaiQuan, ThietBi.MaDoanhNghiep
HAVING      (ThietBi.MaHaiQuan = @MaHaiQuan) AND (ThietBi.MaDoanhNghiep = @MaDoanhNghiep)
ORDER BY ThietBi.TenHang

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HMD_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HMD_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_NPL]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_NPL]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_SP_Xuat_HD]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SP_Xuat_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC01HSTK_GC_TT117_Tong]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================

CREATE PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT117_Tong] 
	-- Add the parameters for the stored procedure here
	@IDHopDong BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;  
	 --  ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',
--DECLARE @IDHopDong BIGINT;DECLARE @MaHaiQuan CHAR(6);DECLARE @MaDoanhNghiep VARCHAR(14)	;	SET @IDHopDong=710;SET @MaDoanhNghiep = '0400101556';SET @MaHaiQuan='C34C'
SELECT    ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ma) AS 'STT', t_KDT_GC_HopDong.ID, t_KDT_GC_HopDong.MaDoanhNghiep, t_KDT_GC_HopDong.MaHaiQuan, '-' AS SoToKhai, '-' AS NgayDangKy, 
                      t_GC_NguyenPhuLieu.Ten AS TenNguyenPhuLieu, t_GC_NguyenPhuLieu.Ma AS MaNguyenPhuLieu, SUM(ToKhai.SoLuong) AS TongLuong, 
                      t_HaiQuan_DonViTinh.Ten AS DVT, '-' AS SoLuong
FROM         t_KDT_GC_HopDong INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_HangMauDich.MaPhu AS MaNguyenPhuLieu, t_KDT_HangMauDich.SoLuong
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'NGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N')
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, 
                                                  t_KDT_GC_HangChuyenTiep.MaHang AS MaNguyenPhuLieu, t_KDT_GC_HangChuyenTiep.SoLuong
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHPLN' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPN' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'NGC18' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'NGC19')) AS ToKhai ON t_KDT_GC_HopDong.ID = ToKhai.IDHopDong AND 
                      t_KDT_GC_HopDong.MaHaiQuan = ToKhai.MaHaiQuan INNER JOIN
                      t_GC_NguyenPhuLieu ON ToKhai.MaNguyenPhuLieu = t_GC_NguyenPhuLieu.Ma AND ToKhai.IDHopDong = t_GC_NguyenPhuLieu.HopDong_ID INNER JOIN
                      t_HaiQuan_DonViTinh ON t_GC_NguyenPhuLieu.DVT_ID = t_HaiQuan_DonViTinh.ID
GROUP BY t_KDT_GC_HopDong.ID, t_KDT_GC_HopDong.MaDoanhNghiep, t_KDT_GC_HopDong.MaHaiQuan, t_GC_NguyenPhuLieu.Ten, t_GC_NguyenPhuLieu.Ma, 
                      t_HaiQuan_DonViTinh.Ten
HAVING      (t_KDT_GC_HopDong.ID = @IDHopDong) AND (t_KDT_GC_HopDong.MaDoanhNghiep = @MaDoanhNghiep) AND (t_KDT_GC_HopDong.MaHaiQuan = @MaHaiQuan)
ORDER BY t_GC_NguyenPhuLieu.Ma, SoToKhai

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC01HSTK_GC_TT117]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================

CREATE PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT117] 
	-- Add the parameters for the stored procedure here
	@IDHopDong BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--	SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',
SELECT    ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ma) AS 'STT', t_KDT_GC_HopDong.ID, t_KDT_GC_HopDong.MaDoanhNghiep, t_KDT_GC_HopDong.MaHaiQuan, ToKhai.SoToKhai, ToKhai.NgayDangKy, 
                      t_GC_NguyenPhuLieu.Ten AS TenNguyenPhuLieu, t_GC_NguyenPhuLieu.Ma AS MaNguyenPhuLieu, ToKhai.SoLuong, t_HaiQuan_DonViTinh.Ten AS DVT
FROM         t_KDT_GC_HopDong INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_HangMauDich.MaPhu AS MaNguyenPhuLieu, t_KDT_HangMauDich.SoLuong
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'NGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N')
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, 
                                                  t_KDT_GC_HangChuyenTiep.MaHang AS MaNguyenPhuLieu, t_KDT_GC_HangChuyenTiep.SoLuong
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHPLN' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPN' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'NGC18' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'NGC19')) AS ToKhai ON t_KDT_GC_HopDong.ID = ToKhai.IDHopDong AND 
                      t_KDT_GC_HopDong.MaHaiQuan = ToKhai.MaHaiQuan INNER JOIN
                      t_GC_NguyenPhuLieu ON ToKhai.MaNguyenPhuLieu = t_GC_NguyenPhuLieu.Ma AND ToKhai.IDHopDong = t_GC_NguyenPhuLieu.HopDong_ID INNER JOIN
                      t_HaiQuan_DonViTinh ON t_GC_NguyenPhuLieu.DVT_ID = t_HaiQuan_DonViTinh.ID
WHERE     (t_KDT_GC_HopDong.ID = @IDHopDong) AND (t_KDT_GC_HopDong.MaDoanhNghiep = @MaDoanhNghiep) AND (t_KDT_GC_HopDong.MaHaiQuan = @MaHaiQuan)
ORDER BY  t_GC_NguyenPhuLieu.Ma, ToKhai.SoToKhai

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC02HSTK_GC_TT117]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================

CREATE PROCEDURE [dbo].[p_GC_BC02HSTK_GC_TT117] 
	-- Add the parameters for the stored procedure here
	@IDHopDong BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- p_GC_BC02HSTK_GC_TT117 710, 'C34C','0400101556'
--	SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',

SELECT DISTINCT 
                  ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',    ToKhai.SoToKhai, ToKhai.NgayDangKy, v_KDT_SP_Xuat_HD.MaPhu AS MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, 
                      '-' AS TongCong
FROM         v_KDT_SP_Xuat_HD INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
                                                   t_KDT_ToKhaiMauDich.MaDoanhNghiep
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S')
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, t_KDT_GC_HangChuyenTiep.MaHang, 
                                                  t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19')) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong AND 
                      v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang INNER JOIN
                      t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID
WHERE     (v_KDT_SP_Xuat_HD.IDHopDong = @IDHopDong) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan)
ORDER BY v_KDT_SP_Xuat_HD.MaPhu

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC02HSTK_GC_TT117_Tong]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================

CREATE PROCEDURE [dbo].[p_GC_BC02HSTK_GC_TT117_Tong] 
	-- Add the parameters for the stored procedure here
	@IDHopDong BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- p_GC_BC02HSTK_GC_TT117_Tong 710, 'C34C','0400101556'
--	SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',

SELECT DISTINCT 
                  ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',    '-' AS SoToKhai, '-' AS NgayDangKy, v_KDT_SP_Xuat_HD.MaPhu AS MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, '-' AS SoLuong, 
                      SUM(ToKhai.SoLuong) AS TongCong
FROM         v_KDT_SP_Xuat_HD INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
                                                   t_KDT_ToKhaiMauDich.MaDoanhNghiep
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S')
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, t_KDT_GC_HangChuyenTiep.MaHang, 
                                                  t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19')) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong AND 
                      v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang INNER JOIN
                      t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID
WHERE     (v_KDT_SP_Xuat_HD.IDHopDong = @IDHopDong) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan)
GROUP BY v_KDT_SP_Xuat_HD.MaPhu, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten
ORDER BY MaHang

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC03HSTK_GC_TT117]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================

CREATE PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117] 
	-- Add the parameters for the stored procedure here
	@IDHopDong BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- p_GC_BC02HSTK_GC_TT117 710, 'C34C','0400101556'
--	SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);	 set @IDHopDong=  397;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';



SELECT   ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT',  t_View_KDT_GC_NPL.IDHopDong, ToKhai.SoToKhai, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, '-' AS TongCong
FROM         t_View_KDT_GC_NPL INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND 
                                                   (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep)
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang, 
                                                  t_KDT_GC_HangChuyenTiep.SoLuong
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHPLX' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC18') AND (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep)) AS ToKhai ON t_View_KDT_GC_NPL.IDHopDong = ToKhai.IDHopDong AND 
                      t_View_KDT_GC_NPL.MaPhu = ToKhai.MaHang INNER JOIN
                      t_HaiQuan_DonViTinh ON t_View_KDT_GC_NPL.ID_DVT = t_HaiQuan_DonViTinh.ID
WHERE     (t_View_KDT_GC_NPL.IDHopDong = @IDHopDong)
ORDER BY ToKhai.MaHang
						  
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC03HSTK_GC_TT117_Tong]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================

CREATE PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117_Tong] 
	-- Add the parameters for the stored procedure here
	@IDHopDong BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- p_GC_BC02HSTK_GC_TT117 710, 'C34C','0400101556'
--	SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);	 set @IDHopDong=  397;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';


SELECT   ROW_NUMBER() OVER (ORDER BY  t_View_KDT_GC_NPL.IDHopDong) AS 'STT',  t_View_KDT_GC_NPL.IDHopDong, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, SUM(ToKhai.SoLuong) AS TongCong, '-' AS SoLuong, 
                      '-' AS SoToKhai
FROM         t_View_KDT_GC_NPL INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND 
                                                   (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep)
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang, 
                                                  t_KDT_GC_HangChuyenTiep.SoLuong
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHPLX' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC18') AND (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep)) AS ToKhai ON t_View_KDT_GC_NPL.IDHopDong = ToKhai.IDHopDong AND 
                      t_View_KDT_GC_NPL.MaPhu = ToKhai.MaHang INNER JOIN
                      t_HaiQuan_DonViTinh ON t_View_KDT_GC_NPL.ID_DVT = t_HaiQuan_DonViTinh.ID
GROUP BY t_View_KDT_GC_NPL.IDHopDong, ToKhai.MaHang, ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten
HAVING      (t_View_KDT_GC_NPL.IDHopDong = @IDHopDong)ORDER BY ToKhai.MaHang

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_Cuc] on [dbo].[t_HaiQuan_Cuc]'
GO
ALTER TABLE [dbo].[t_HaiQuan_Cuc] ADD CONSTRAINT [PK_t_HaiQuan_Cuc] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

UPDATE dbo.t_HaiQuan_Version SET [Version] = '1.4', [Date] = GETDATE()