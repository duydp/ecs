﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class VNACC_HangMauDichXuatForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public KDT_VNACC_HangMauDich HMD = null;
        public List<KDT_VNACC_HangMauDich> ListHMD = new List<KDT_VNACC_HangMauDich>();
        DataTable dtHS = new DataTable();

        public VNACC_HangMauDichXuatForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.EDA.ToString());
        }
        private void GetHangMauDich()
        {
            HMD.MaSoHang = ctrMaSoHang.Code;
            HMD.MaQuanLy = txtMaQuanLy.Text;
            HMD.TenHang = txtTenHang.Text;
            HMD.ThueSuat = Convert.ToDecimal(txtThueSuat.Value);
            HMD.ThueSuatTuyetDoi = Convert.ToDecimal(txtThueSuatTuyetDoi.Value);
            HMD.MaDVTTuyetDoi = txtMaDVTTuyetDoi.Text;
            HMD.MaTTTuyetDoi = ctrMaTTTuyetDoi.Code;
            HMD.MaMienGiamThue = txtMaMienGiamThue.Text;
            HMD.SoTienGiamThue = Convert.ToDecimal(txtSoTienMienGiam.Value);
            HMD.SoLuong1 = Convert.ToDecimal(txtSoLuong1.Value);
            HMD.DVTLuong1 = ctrDVTLuong1.Code;
            HMD.SoLuong2 = Convert.ToDecimal(txtSoLuong2.Value);
            HMD.DVTLuong2 = ctrDVTLuong2.Code;
            HMD.TriGiaHoaDon = Convert.ToDecimal(txtTriGiaHoaDon.Value);
            HMD.DonGiaHoaDon = Convert.ToDecimal(txtDonGiaHoaDon.Value);
            HMD.DVTDonGia = txtDVTDonGia.Text;
            HMD.MaTTDonGia = ctrMaTTDonGia.Code;
            HMD.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Value);
            HMD.MaTTTriGiaTinhThue = ctrMaTTTriGiaTinhThue.Code;
            HMD.SoTTDongHangTKTNTX = txtSoTTDongHangTKTNTX.Text;
            HMD.SoDMMienThue = Convert.ToDecimal(txtSoDMMienThue.Value);
            HMD.SoDongDMMienThue = txtSoDongDMMienThue.Text;
            HMD.MaVanBanPhapQuyKhac1 = txtMaVanBan1.Text;
            HMD.MaVanBanPhapQuyKhac2 = txtMaVanBan2.Text;
            HMD.MaVanBanPhapQuyKhac3 = txtMaVanBan3.Text;
            HMD.MaVanBanPhapQuyKhac4 = txtMaVanBan4.Text;
            HMD.MaVanBanPhapQuyKhac5 = txtMaVanBan5.Text;

        }

        private void SetHangMauDich()
        {
            ctrMaSoHang.Code = HMD.MaSoHang;
            txtMaQuanLy.Text = HMD.MaQuanLy;
            txtTenHang.Text = HMD.TenHang;
            txtThueSuat.Text = HMD.ThueSuat.ToString();
            txtThueSuatTuyetDoi.Text = HMD.ThueSuatTuyetDoi.ToString();
            txtMaDVTTuyetDoi.Text = HMD.MaDVTTuyetDoi;
            ctrMaTTTuyetDoi.Code = HMD.MaTTTuyetDoi;
            txtMaMienGiamThue.Text = HMD.MaMienGiamThue;
            txtSoTienMienGiam.Text = HMD.SoTienGiamThue.ToString();
            txtSoLuong1.Text = HMD.SoLuong1.ToString();
            ctrDVTLuong2.Code = HMD.DVTLuong1;
            txtSoLuong2.Text = HMD.SoLuong2.ToString();
            ctrDVTLuong2.Code = HMD.DVTLuong2;
            txtTriGiaHoaDon.Text = HMD.TriGiaHoaDon.ToString();
            txtDonGiaHoaDon.Text = HMD.DonGiaHoaDon.ToString();
            txtDVTDonGia.Text = HMD.DVTDonGia;
            ctrMaTTDonGia.Code = HMD.MaTTDonGia;
            txtTriGiaTinhThue.Text = HMD.TriGiaTinhThue.ToString();
            ctrMaTTTriGiaTinhThue.Code = HMD.MaTTTriGiaTinhThue;
            txtSoTTDongHangTKTNTX.Text = HMD.SoTTDongHangTKTNTX;
            txtSoDongDMMienThue.Text = HMD.SoDMMienThue.ToString();
            txtSoDMMienThue.Text = HMD.SoDongDMMienThue;
            txtMaVanBan1.Text = HMD.MaVanBanPhapQuyKhac1;
            txtMaVanBan2.Text = HMD.MaVanBanPhapQuyKhac2;
            txtMaVanBan3.Text = HMD.MaVanBanPhapQuyKhac3;
            txtMaVanBan4.Text = HMD.MaVanBanPhapQuyKhac4;
            txtMaVanBan5.Text = HMD.MaVanBanPhapQuyKhac5;

            txtTriGiaTinhThueS.Text = HMD.TriGiaTinhThueS.ToString();
            ctrMaTTTriGiaTinhThueS.Code = HMD.MaTTTriGiaTinhThueS;
            txtDonGiaTinhThue.Text = HMD.DonGiaTinhThue.ToString();
            txtDV_SL_TrongDonGiaTinhThue.Text = HMD.DV_SL_TrongDonGiaTinhThue;
            ctrMaTTSoTienThueXuatKhau.Code = HMD.MaTTSoTienThueXuatKhau;
            txtSoTienThue.Text = HMD.SoTienThue.ToString();
            //txtThueSuatThueXuatKhau.Text = HMD.ThueSuatThue;
            txtPhanLoai_TS_ThueXuatKhau.Text = HMD.PhanLoaiThueSuatThue;
            txtDieuKhoanMienGiam.Text = HMD.DieuKhoanMienGiam;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_HangMauDich> ItemColl = new List<KDT_VNACC_HangMauDich>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_HangMauDich)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangMauDich item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKMD.HangCollection.Remove(item);
                    }

                    grList.DataSource = TKMD.HangCollection;
                    grList.Refetch();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {

                bool isAddNew = false;
                if (HMD == null)
                {
                    HMD = new KDT_VNACC_HangMauDich();
                    isAddNew = true;
                }
                GetHangMauDich();
                if (isAddNew)
                    TKMD.HangCollection.Add(HMD);
                grList.DataSource = TKMD.HangCollection;
                grList.Refetch();
                HMD = new KDT_VNACC_HangMauDich();
                SetHangMauDich();
                HMD = null;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void VNACC_HangMauDichXuatForm_Load(object sender, EventArgs e)
        {
            SetIDControl();
            grList.DataSource = TKMD.HangCollection;

        }

        private void grList_DoubleClick(object sender, EventArgs e)
        {
            if (grList.GetRows().Length < 0) return;
            HMD = (KDT_VNACC_HangMauDich)grList.CurrentRow.DataRow;
            SetHangMauDich();
        }
        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                ctrMaSoHang.TagName = "CMD"; //Mã số hàng hóa
                txtMaQuanLy.Tag = "COC"; //Mã quản lý riêng
                txtThueSuat.Tag = "TXA"; //Thuế suất
                txtThueSuatTuyetDoi.Tag = "TXB"; //Mức thuế tuyệt đối
                txtMaDVTTuyetDoi.Tag = "TXC"; //Mã đơn vị tính thuế tuyệt đối
                ctrMaTTTuyetDoi.TagName = "TXD"; //Mã đồng tiền của mức thuế tuyệt đối
                txtTenHang.Tag = "CMN"; //Mô tả hàng hóa
                txtMaMienGiamThue.Tag = "RE"; //Mã miễn / Giảm / Không chịu thuế xuất khẩu
                txtSoTienMienGiam.Tag = "REG"; //Số tiền giảm thuế xuất khẩu
                txtSoLuong1.Tag = "QN1"; //Số lượng (1)
                ctrDVTLuong1.TagName = "QT1"; //Mã đơn vị tính (1)
                txtSoLuong2.Tag = "QN2"; //Số lượng (2)
                ctrDVTLuong2.TagName = "QT2"; //Mã đơn vị tính (2)
                txtTriGiaHoaDon.Tag = "BPR"; //Trị giá hóa đơn
                ctrMaTTTriGiaTinhThue.TagName = "BPC"; //Mã đồng tiền của giá tính thuế
                txtTriGiaTinhThue.Tag = "DPR"; //Trị giá tính thuế
                txtDonGiaHoaDon.Tag = "UPR"; //Đơn giá hóa đơn
                ctrMaTTDonGia.TagName = "UPC"; //Mã đồng tiền của đơn giá hóa đơn
                txtDVTDonGia.Tag = "TSC"; //Đơn vị của đơn giá hóa đơn và số lượng
                txtSoTTDongHangTKTNTX.Tag = "TDL"; //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
                txtSoDMMienThue.Tag = "TXN"; //Danh mục miễn thuế xuất khẩu
                txtSoDongDMMienThue.Tag = "TXR"; //Số dòng tương ứng trong danh mục miễn thuế
                txtMaVanBan1.Tag = "OL_"; //Mã văn bản pháp quy khác
                txtMaVanBan2.Tag = "OL_"; //Mã văn bản pháp quy khác
                txtMaVanBan3.Tag = "OL_"; //Mã văn bản pháp quy khác
                txtMaVanBan4.Tag = "OL_"; //Mã văn bản pháp quy khác
                txtMaVanBan5.Tag = "OL_"; //Mã văn bản pháp quy khác


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }
    }
}
