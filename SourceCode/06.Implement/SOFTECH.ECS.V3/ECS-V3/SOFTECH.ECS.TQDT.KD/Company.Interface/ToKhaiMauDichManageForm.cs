﻿using System;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.KD.BLL.KDT.SXXK;
using System.Xml.Serialization;
using System.Xml;
using System.Data;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.QuanLyChungTu;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using Infragistics.Excel;
using PopupControl;
using Company.Interface.Controls;
using System.Collections.Generic;
namespace Company.Interface
{
    public partial class ToKhaiMauDichManageForm : BaseForm
    {
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        private ToKhaiMauDichCollection tmpCollection = new ToKhaiMauDichCollection();
        private static long tkmdID = 0;
        private string msgInfor = string.Empty;
        private ToKhaiMauDich TKMD = null;
        public string nhomLoaiHinh = "";

        //Popup toolTip;
        ToKhaiInfo toKhaiInfo;
        public ToKhaiMauDichManageForm()
        {
            InitializeComponent();

            //toolTip = new Popup(toKhaiInfo = new ToKhaiInfo());
            //toolTip.AutoClose = false;
            //toolTip.FocusOnOpen = false;
            //toolTip.ShowingAnimation = toolTip.HidingAnimation = PopupAnimations.Blend;
        }

        private void setCommandStatus()
        {
            try
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                DongBoDuLieu.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = InheritableBoolean.False;
                if (Convert.ToInt32(status) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                {
                    dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                    dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    cmdCancel.Enabled = InheritableBoolean.True;
                    cmdSend.Enabled = InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    //print.Enabled = false;
                    cmdXuatDuLieuChoPhongKhai.Enabled = InheritableBoolean.False;
                    cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                    mnuCSDaDuyet.Enabled = true;
                    mnuSuaToKhai.Enabled = false;
                    mnuHuyToKhai.Enabled = false;
                    cmdDelete.Enabled = InheritableBoolean.False;

                }
                else if (Convert.ToInt32(status) == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                    dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdSend.Enabled = InheritableBoolean.False;
                    cmdSingleDownload.Enabled = InheritableBoolean.False;
                    cmdCancel.Enabled = InheritableBoolean.False;
                    cmdXuatDuLieuChoPhongKhai.Enabled = InheritableBoolean.False;
                    cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                    mnuCSDaDuyet.Enabled = false;
                    mnuSuaToKhai.Enabled = true;
                    mnuHuyToKhai.Enabled = true;
                    //print.Enabled = true;          
                    //btnXoa.Enabled = false;
                    cmdDelete.Enabled = InheritableBoolean.False;
                }
                else if (Convert.ToInt32(status) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                    dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                    cmdSend.Enabled = InheritableBoolean.True;
                    cmdSingleDownload.Enabled = InheritableBoolean.False;
                    cmdCancel.Enabled = InheritableBoolean.False;
                    cmdXuatDuLieuChoPhongKhai.Enabled = InheritableBoolean.True;
                    cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                    mnuCSDaDuyet.Enabled = true;
                    //print.Enabled = false;
                    mnuSuaToKhai.Enabled = false;
                    mnuHuyToKhai.Enabled = false;
                    cmdDelete.Enabled = InheritableBoolean.True;
                }
                else if (Convert.ToInt32(status) == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                    dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                    cmdSend.Enabled = InheritableBoolean.True;
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    cmdCancel.Enabled = InheritableBoolean.True;
                    cmdXuatDuLieuChoPhongKhai.Enabled = InheritableBoolean.False;
                    cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                    mnuCSDaDuyet.Enabled = false;
                    mnuSuaToKhai.Enabled = false;
                    mnuHuyToKhai.Enabled = false;
                    cmdDelete.Enabled = InheritableBoolean.True;
                }
                else if (Convert.ToInt32(status) == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                {
                    mnuSuaToKhai.Enabled = false;
                    mnuHuyToKhai.Enabled = false;
                    cmdDelete.Enabled = InheritableBoolean.True;
                }
                else if (Convert.ToInt32(status) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                {
                    mnuSuaToKhai.Enabled = false;
                    mnuHuyToKhai.Enabled = true;
                    cmdDelete.Enabled = InheritableBoolean.False;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //cbStatus.SelectedValueChanged += new EventHandler(cbStatus_SelectedValueChanged);
                //cboPhanLuong.SelectedValueChanged -= new EventHandler(cboPhanLuong_SelectedValueChanged);

                //chkDate.Checked = false;
                //cbStatus.SelectedIndex = 0;
                //cbUserKB.SelectedIndex = 0;
                //this.setDataToCboPhanLuong();
                //this.setDataToCboNguoiKB();
                //cboPhanLuong.SelectedIndex = 0;

                dgList.RootTable.RowHeaderWidth = 70;

                if (MainForm.versionHD == 0)
                {
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)))
                    {
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        this.dgList.ContextMenuStrip = null;
                    }
                }
                setCommandStatus();

                //Update by Hungtq, 17/10/2011. Cap nhat vi tri cot.
                dgList.RootTable.Columns["SoTiepNhan"].Position = 2;
                dgList.RootTable.Columns["NgayTiepNhan"].Position = 3;
                dgList.RootTable.Columns["SoToKhai"].Position = 4;
                dgList.RootTable.Columns["NgayDangKy"].Position = 5;
                dgList.RootTable.Columns["SoHopDong"].Position = 6;
                dgList.RootTable.Columns["TrangThaiXuLy"].Position = 7;
                dgList.RootTable.Columns["PhanLuong"].Position = 8;

                dgList.RootTable.Columns["NgayDangKy"].Width = 120;

                //cbStatus.SelectedValueChanged += new EventHandler(cbStatus_SelectedValueChanged);
                //cboPhanLuong.SelectedValueChanged += new EventHandler(cboPhanLuong_SelectedValueChanged);

                saveFileDialog1.InitialDirectory = Application.StartupPath;
                openFileDialog1.InitialDirectory = Application.StartupPath;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void setDataToCboNguoiKB()
        {
            DataTable dt = new Company.QuanTri.User().SelectAll().Tables[0];
            DataRow dr = dt.NewRow();
            dr["USER_NAME"] = -1;
            dr["HO_TEN"] = "--Tất cả--";
            dt.Rows.InsertAt(dr, 0);
            //cbUserKB.DataSource = dt;
            //cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            //cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {

        }
        private ToKhaiMauDich getTKMDByID(long id)
        {
            foreach (ToKhaiMauDich tk in this.tkmdCollection)
            {
                if (tk.ID == id) return tk;
            }
            return null;
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
                {
                    //long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    tkmdID = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                    f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
                    f.TKMD = this.getTKMDByID(tkmdID);

                    f.TKMD.LoadChungTuHaiQuan();

                    int tt = f.TKMD.TrangThaiXuLy;
                    f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                    f.ShowDialog(this);
                    if (tt != f.TKMD.TrangThaiXuLy)
                        timToKhai1_Search(null, null);
                }
                else
                {
                    //long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    tkmdID = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                    f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
                    f.TKMD = this.getTKMDByID(tkmdID);

                    f.TKMD.LoadChungTuHaiQuan();

                    f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                    f.ShowDialog(this);
                    f.Close();
                }
            }
        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                string maLH = e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim();
                e.Row.Cells["MaLoaiHinh"].Text = maLH + " - " + this.LoaiHinhMauDich_GetName(maLH);
                if (e.Row.Cells["NgayTiepNhan"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }

                DateTime dtNgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                if (dtNgayDangKy.Year <= 1900)
                    e.Row.Cells["NgayDangKy"].Text = "";

                #region Begin TrangThaiXuLy
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Information has not yet sent";

                        }
                        break;
                    case 0:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for approval";

                        }
                        break;
                    case 1:
                        {
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Approved";

                            }

                        }
                        break;
                    case 2:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not Approved";

                        }
                        break;
                    case 5:
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Sửa tờ khai";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Edit";
                        break;
                    case 10:
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Deleted";
                        break;
                    case 11:
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for delete";
                        break;
                }
                #endregion End TrangThaiXuLy

                #region Begin PhanLuong
                if (e.Row.Cells["PhanLuong"].Value != null && e.Row.Cells["PhanLuong"].Value.ToString() != "")
                {
                    switch (Convert.ToInt32(e.Row.Cells["PhanLuong"].Value))
                    {
                        case 1:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["PhanLuong"].Text = "Luồng xanh";
                            else
                                e.Row.Cells["PhanLuong"].Text = "Green";
                            break;
                        case 2:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["PhanLuong"].Text = "Luồng vàng";
                            else
                                e.Row.Cells["PhanLuong"].Text = "Yellow";
                            break;
                        case 3:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["PhanLuong"].Text = "Luồng đỏ";
                            else
                                e.Row.Cells["PhanLuong"].Text = "Red";
                            break;
                    }

                    e.Row.Cells["HuongDan"].ToolTipText = e.Row.Cells["HuongDan"].Text;
                }
                #endregion End PhanLuong
            }
        }

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "SaoChep":
                    this.SaoChepToKhaiMD();
                    break;
                case "SaoChepALL":
                    this.SaoChepALLHang();
                    break;
                case "SaoChepToKhaiHang":
                    this.SaoChepToKhaiMD();
                    break;
                case "Export":
                    btnExportExcel_Click(null, null);
                    // this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "cmdXuatDuLieuChoPhongKhai":
                    XuatDuLieuChoPhongKhai();
                    break;
                case "cmdCSDaDuyet":
                    ChuyenTrangThai();
                    break;
                case "cmdHistory":
                    btnKetQuaXuLy_Click(null, null);
                    break;
                case "cmdDelete":
                    XoaToKhai();
                    break;
            }
        }

        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        tkmdColl.Add((ToKhaiMauDich)grItem.GetRow().DataRow);
                    }
                }

                for (int i = 0; i < tkmdColl.Count; i++)
                {
                    string msg = "Bạn có muốn chuyển trạng thái của tờ khai được chọn sang đã duyệt không?";
                    msg += "\n\nSố thứ tự tờ khai: " + tkmdColl[i].ID.ToString();
                    msg += "\n----------------------";
                    tkmdColl[i].LoadHMDCollection();
                    msg += "\nCó " + tkmdColl[i].HMDCollection.Count.ToString() + " sản phẩm đăng ký";
                    if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                    {
                        ChuyenTrangThaiTK obj = new ChuyenTrangThaiTK(tkmdColl[i]);
                        obj.ShowDialog(this);
                    }
                }
                timToKhai1_Search(null, null);
            }
            else
            {
                MLMessages("Không có dữ liệu được chọn!", "MSG_WRN11", "", false);
            }
        }

        private void XuatDuLieuChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                MLMessages("Chưa chọn danh sách tờ khai", "MSG_WRN11", "", false);
                return;
            }
            try
            {
                ToKhaiMauDichCollection col = new ToKhaiMauDichCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sotokhai = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmdSelected = (ToKhaiMauDich)i.GetRow().DataRow;
                            tkmdSelected.LoadHMDCollection();
                            col.Add(tkmdSelected);
                            sotokhai++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    MLMessages("Xuất ra file thành công " + sotokhai + " tờ khai.", "MSG_EXC05", sotokhai.ToString(), false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private string checkDataImport(ToKhaiMauDichCollection collection)
        {
            string st = "";
            foreach (ToKhaiMauDich tkmd in collection)
            {
                ToKhaiMauDich tkmdInDatabase = new ToKhaiMauDich();
                tkmdInDatabase.ID = (tkmd.ID);
                tkmdInDatabase.Load();
                if (tkmdInDatabase != null)
                {
                    if (tkmdInDatabase.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        st += "Danh sách có ID=" + tkmd.ID + " đã được duyệt.\n";
                    }
                    else
                    {
                        tmpCollection.Add(tkmd);
                    }
                }
                else
                {
                    if (tkmd.ID > 0)
                        tkmd.ID = 0;
                    tmpCollection.Add(tkmd);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                tmpCollection.Clear();
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    ToKhaiMauDichCollection tkmdCollection = (ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(tkmdCollection);
                    if (st != "")
                    {
                        string msg = setText("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "invalid inport value. Do you want to continue and skip approval information");
                        if (ShowMessage(msg, true) == "Yes")
                        {
                            ToKhaiMauDich.DongBoDuLieuPhongKhai(tmpCollection);
                            MLMessages("Import thành công", "MSG_WRN34", "", false);
                        }
                    }
                    else
                    {
                        ToKhaiMauDich.DongBoDuLieuPhongKhai(tmpCollection);
                        MLMessages("Import thành công", "MSG_WRN34", "", false);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                MLMessages("Chưa chọn danh sách tờ khai", "MSG_WRN11", "", false);
                return;
            }
            try
            {
                ToKhaiMauDichCollection col = new ToKhaiMauDichCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmdSelected = (ToKhaiMauDich)i.GetRow().DataRow;
                            tkmdSelected.LoadHMDCollection();
                            col.Add(tkmdSelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }

        }

        private void SaoChepALLHang()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            foreach (HangMauDich hmd in tkmd.HMDCollection)
            {
                hmd.ID = 0;
            }
            tkmd.ID = 0;
            tkmd.SoToKhai = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.GUIDSTR = Guid.NewGuid().ToString();
            tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
            tkmd.PhanLuong = string.Empty;
            tkmd.HUONGDAN = string.Empty;

            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;
            f.TKMD.HMDCollection = tkmd.HMDCollection;
            f.Show();
        }

        private void SaoChepALLHangChungtu()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadTKTGCollection();
            tkmd.LoadToKhaiTriGiaPP23();
            tkmd.LoadCO();
            tkmd.LoadListGiayNopTiens();

            //tkmd.VanTaiDon.ContainerCollection();
            foreach (HangMauDich hmd in tkmd.HMDCollection)
            {
                hmd.ID = 0;
            }
            //foreach (HangTriGia htg in tkmd.h)
            //{
            //    htg.ID = 0;
            //}                 
            tkmd.SoToKhai = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
            tkmd.PhanLuong = string.Empty;
            tkmd.HUONGDAN = string.Empty;
            tkmd.GUIDSTR = Guid.NewGuid().ToString();
            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f._bNew = false;

            f.pTKMD_ID = tkmd.ID;
            tkmd.ID = 0;
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;
            f.TKMD.HMDCollection = tkmd.HMDCollection;
            //
            //Copy CO :

            f.TKMD.ListCO = tkmd.ListCO;

            //Copy TKTG PP1 :

            f.TKMD.TKTGCollection = tkmd.TKTGCollection;

            //Copy TKTG PP23 :

            f.TKMD.TKTGPP23Collection = tkmd.TKTGPP23Collection;

            f.Show();
        }

        private void SaoChepCO()
        {

        }

        private void SaoChepToKhaiMD()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)) return;

            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.SoToKhai = 0;
            tkmd.ID = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.GUIDSTR = Guid.NewGuid().ToString();
            tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
            tkmd.PhanLuong = string.Empty;
            tkmd.HUONGDAN = string.Empty;

            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;
            f.Show();
        }

        private void saoChepTK_Click(object sender, EventArgs e)
        {
            SaoChepToKhaiMD();
        }

        private void saoChepHH_Click(object sender, EventArgs e)
        {
            SaoChepALLHang();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (MLMessages("Bạn có muốn xóa các thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "TK";
                            sendXML.master_id = tkmd.ID;
                            if (sendXML.Load())
                            {
                                MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan. Bạn không thể xóa.", "MSG_STN09", Convert.ToString(i.Position), false);
                                //if (st == "Yes")
                                //    tkmd.Delete();

                                e.Cancel = true;
                                continue;
                            }
                            else
                            {
                                //Load thong tin chung tu kem
                                tkmd.LoadChungTuKem();

                                //Xoa chung tu kem
                                for (int j = 0; j < tkmd.listCTDK.Count; j++)
                                {
                                    Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.DeleteBy_TKMDID(tkmd.ID);
                                }

                                tkmd.Delete();
                            }
                        }
                    }
                }
                else
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void mnuCSDaDuyet_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangThai();
        }

        private void ToKhaiMenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;

            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();



            // To khai tri gia :
            // ToKhaiTriGia tktg = new ToKhaiTriGia();
            // tktg.ID = ((ToKhaiTriGia)dgList.GetRow().DataRow).ID;
            // tktg.Load();
            // tktg.LoadHTGCollection();
            //end to khai tri gia 

            switch (tkmd.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewForm f = new ReportViewForm();
                    f.TKMD = tkmd;
                    f.ShowDialog(this);
                    break;

                case "X":
                    ReportViewTKXForm f1 = new ReportViewTKXForm();
                    f1.TKMD = tkmd;
                    f1.ShowDialog(this);
                    break;
            }
        }

        private void PhieuTNMenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "TỜ KHAI";
            Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    ToKhaiMauDich tkDangKySelected = (ToKhaiMauDich)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = tkDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = tkDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    phieuTNForm.MaHaiQuan = tkDangKySelected.MaHaiQuan;
                    j++;
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();
        }

        private void ToKhaiA4MenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            switch (tkmd.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNA4Form f = new ReportViewTKNA4Form();
                    f.TKMD = tkmd;
                    f.ShowDialog(this);
                    break;

                case "X":
                    ReportViewTKXA4Form f1 = new ReportViewTKXA4Form();
                    f1.TKMD = tkmd;
                    f1.ShowDialog(this);
                    break;
            }
        }

        private void mnuToKhaiChungTu_Click(object sender, EventArgs e)
        {
            this.SaoChepALLHangChungtu();
        }

        private void ToKhaiHQDTMenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            HangMauDich hangmd = new HangMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            if (tkmd.MaLoaiHinh.StartsWith("N"))
            {
                ReportViewTKNTQDTForm f = new ReportViewTKNTQDTForm();
                f.TKMD = tkmd;
                f.ShowDialog(this);
            }
            else
            {
                ReportViewTKXTQDTForm f = new ReportViewTKXTQDTForm();
                f.TKMD = tkmd;
                f.ShowDialog(this);
            }
            //ReportViewTKXTQDTForm
        }

        private void mnuInBKContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            Company.Interface.Report.SXXK.BangkeSoContainer bkCon = new Company.Interface.Report.SXXK.BangkeSoContainer();
            bkCon.tkmd = tkmd;
            bkCon.BindReport();
            bkCon.ShowPreview();
        }

        private void mnuNhanHuongDan_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            if (tkmd == null)
                tkmd = new ToKhaiMauDich();
            if (tkmd.HUONGDAN != "")
                ShowMessageTQDT("Nội dung hướng dẫn làm thủ tục của Hải quan : " + tkmd.HUONGDAN.ToString(), false);
            else
                ShowMessageTQDT("Chưa có hướng dẫn làm thủ tục ", false);

        }
        private void DeleteMsgSend()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
            sendXML.master_id = TKMD.ID;
            sendXML.Delete();
        }
        private void mnuSuaToKhai_Click(object sender, System.EventArgs e)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;

            string msg = "Bạn có muốn chuyển trạng thái của tờ khai đã duyệt sang tờ khai sửa không?";
            msg += "\n\nSố tờ khai: " + tkmd.SoToKhai.ToString();
            msg += "\n----------------------";
            msg += "\nCó " + tkmd.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
            {
                long id = tkmd.ID;
                tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                if (tkmd.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    Company.KDT.SHARE.Components.Globals.SaveMessage("", tkmd.ID, tkmd.GUIDSTR, MessageTypes.ToKhaiNhap, MessageFunctions.SuaToKhai, "Thay đổi trạng thái", "Thay đổi sang trạng thái sửa tờ khai");
                else
                    Company.KDT.SHARE.Components.Globals.SaveMessage("", tkmd.ID, tkmd.GUIDSTR, MessageTypes.ToKhaiXuat, MessageFunctions.SuaToKhai, "Thay đổi trạng thái", "Thay đổi sang trạng thái sửa tờ khai");
                tkmd.Update();
                //Update by Khanhhn
                // Fix lỗi: TKMD null nên hàm DeleteMsgSend() lỗi
                TKMD = tkmd;
                DeleteMsgSend();
                ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
                f.TKMD = this.getTKMDByID(id);
                f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog(this);
            }

        }

        private void mnuHuyToKhai_Click(object sender, EventArgs e)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            tkmd.LoadHMDCollection();

            string msg = "Bạn có muốn hủy tờ khai đã duyệt này không?";
            msg += "\n\nSố tờ khai: " + tkmd.SoToKhai.ToString();
            msg += "\n----------------------";
            msg += "\nCó " + tkmd.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    HuyToKhaiForm f = new HuyToKhaiForm();
                    f.TKMD = tkmd;
                    f.ShowDialog(this);
                }
            }
            else
            {
                HuyToKhaiForm f = new HuyToKhaiForm();
                f.TKMD = tkmd;
                f.ShowDialog(this);
            }

        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //datlmq 06102010: Bo sung them phan kiem tra to khai khong phe duyet
            //Lay thong tin to khai mau dich duoc chon tren luoi
            foreach (ToolStripItem item in contextMenuStrip1.Items)
            {
                item.Enabled = false;
            }
            if (dgList.SelectedItems.Count > 0)
            {
                mniInToKhai.Enabled = true;

                ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        ToKhaiMauDich obj = (ToKhaiMauDich)grItem.GetRow().DataRow;

                        switch (obj.TrangThaiXuLy)
                        {
                            case -1: //Chưa khai báo
                                {
                                    mnuKhaiBao.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    mnuCSDaDuyet.Enabled = true;
                                    mnuNhanDuLieu.Enabled = true;
                                    break;
                                }
                            case 0: //Chờ duyệt
                                {
                                    mnuNhanDuLieu.Enabled = true;
                                    mnuHuyKhaiBao.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    mnuCSDaDuyet.Enabled = true;
                                    break;
                                }
                            case 1: //Đã duyệt
                                {
                                    mnuNhanDuLieu.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    mnuSuaToKhai.Enabled = true;
                                    mnuHuyToKhai.Enabled = true;
                                    break;
                                }
                            case 2: //Không phê duyệt
                                {
                                    mnuKhaiBao.Enabled = (obj.SoToKhai == 0); //Nếu TK đã có SỐ TỜ KHAI => Ẩn Khai báo. Không khai báo mới lại.
                                    mnuNhanDuLieu.Enabled = true;
                                    SaoChepCha.Enabled = true;

                                    mnuSuaToKhai.Enabled = (obj.SoToKhai > 0);
                                    mnuHuyToKhai.Enabled = (obj.SoToKhai > 0);
                                    break;
                                }
                            case 11: //Chờ Hủy
                                {
                                    if (obj.SoToKhai > 0)
                                        mnuHuyToKhai.Enabled = true;
                                    mnuNhanDuLieu.Enabled = true;
                                    break;
                                }
                            case 5: //Sửa tờ khai
                                {
                                    mnuKhaiBao.Enabled = true;
                                    mnuNhanDuLieu.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    break;
                                }
                            case 10://Đã hủy.
                                {
                                    mnuKhaiBao.Enabled = false;
                                    mnuNhanDuLieu.Enabled = false;
                                    SaoChepCha.Enabled = false;
                                    break;
                                }
                            case -4:
                                {
                                    mnuNhanDuLieu.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    break;
                                }
                            default:
                                break;
                        }


                        ////Hien thij menu 'Chuyen trang thai' neu to khai da co So tiep nhan.
                        ////mnuCSDaDuyet.Visible = mnuCSDaDuyet.Enabled = (obj.SoTiepNhan > 0 && obj.SoToKhai == 0);
                        //mnuCSDaDuyet.Visible = mnuCSDaDuyet.Enabled = (obj.TrangThaiXuLy == -1 || obj.TrangThaiXuLy == 0);
                        //mnuHuyKhaiBao.Enabled = false;

                        //if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                        //{
                        //    mnuHuyToKhai.Enabled = false;
                        //    mnuSuaToKhai.Enabled = false;
                        //}
                        //else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                        //{
                        //    if (obj.SoToKhai > 0)
                        //    {
                        //        mnuSuaToKhai.Enabled = true;
                        //        mnuHuyToKhai.Enabled = true;
                        //    }
                        //    else
                        //    {
                        //        mnuSuaToKhai.Enabled = false;
                        //        mnuHuyToKhai.Enabled = false;
                        //    }
                        //}
                        //else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                        //{
                        //    mnuSuaToKhai.Enabled = false;
                        //    mnuHuyToKhai.Enabled = true;
                        //}
                    }
                }
            }
        }

        //DATLMQ bổ sung cho phép khai báo ở theo dõi tờ khai 17/01/2011
        private void mnuKhaiBao_Click(object sender, EventArgs e)
        {
            if (this.ShowMessage("Bạn có muốn khai báo thông tin tờ khai này không?", true) == "Yes")
            {
                if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                {
                    SendV3();
                }
                else
                    KhaiBaoToKhai();
            }
        }

        private void KhaiBaoToKhai()
        {
            ////Kiểm tra tờ khai đã được khai báo chưa: DATLMQ 17/01/2011
            //MsgSend sendXML = new MsgSend();
            //sendXML.LoaiHS = "TK";
            //sendXML.master_id = tkmdID;
            //if (sendXML.Load())
            //{
            //    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Nhận phản hồi] để lấy thông tin phản hồi từ hải quan.", "MSG_SEN03", "", false);
            //    //Hiện thị menu Nhận dữ liệu: DATLMQ 17/01/2011
            //    mnuKhaiBao.Enabled = false;
            //    mnuNhanDuLieu.Enabled = true;
            //    return;
            //}

            //string password = string.Empty;
            //this.Cursor = Cursors.Default;
            //WSForm wsForm = new WSForm();
            //try
            //{
            //    if (GlobalSettings.PassWordDT == "")
            //    {
            //        wsForm.ShowDialog(this);
            //        if (!wsForm.IsReady) return;
            //    }
            //    password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
            //    this.Cursor = Cursors.WaitCursor;
            //    //Khai báo sửa tờ khai
            //    bool thanhcong = false;
            //    //Lấy thông tin tờ khai được chọn để khai báo: DATLMQ 17/01/2011
            //    tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            //    ToKhaiMauDich TKMD = this.getTKMDByID(tkmdID);
            //    if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            //    {
            //        ////Nếu HQ đã duyệt phân luồng thì phải lấy thông tin phân luồng về trước khi khai báo sửa
            //        //this.ShowMessage("Kiểm tra nhận dữ liệu phân luồng về trước khi khai báo sửa.", false);
            //        //this.LayPhanHoiDuyetHoacPhanLuong(password);
            //        thanhcong = TKMD.WSKhaiBaoToKhaiSua(password);
            //    }
            //    else
            //    {
            //        if (TKMD.SoToKhai != 0)
            //        {
            //            string msg = "Tờ khai này đã có số tờ khai, không thể khai báo mới tờ khai.\r\nBạn hãy chuyển tờ khai sang trạng thái khác.";
            //            ShowMessage(msg, false);
            //            return;
            //        }

            //        thanhcong = TKMD.WSKhaiBaoToKhai(password);
            //    }
            //    if (thanhcong)
            //    {
            //        this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin. Bấm nút [Nhận phản hồi] để lấy thông tin phản hồi từ hải quan.", false);
            //        mnuKhaiBao.Enabled = false;
            //        mnuNhanDuLieu.Enabled = true;
            //        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            //        mnuHuyKhaiBao.Enabled = false;
            //    }
            //    //Lưu tờ khai vào Quản lý message khai báo: DATLMQ 17/01/2011
            //    sendXML = new MsgSend();
            //    sendXML.LoaiHS = "TK";
            //    sendXML.master_id = tkmdID;
            //    sendXML.func = 1;
            //    sendXML.InsertUpdate();

            //    this.Cursor = Cursors.Default;
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    {
            //        #region FPTService
            //        string[] msg = ex.Message.Split('|');
            //        if (msg.Length == 2)
            //        {
            //            if (msg[1] == "DOTNET_LEVEL")
            //            {
            //                //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
            //                ShowMessageTQDT("Lỗi kết nối", "Không kết nối được với hệ thống hải quan.", false);
            //                return;
            //            }
            //            else
            //            {
            //                ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan: " + msg[0], false);
            //            }
            //        }
            //        else
            //        {
            //            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
            //                ShowMessageTQDT("Thông báo từ hệ thống ECS ", "Thông tin lỗi: " + ex.Message, false);
            //            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
            //            else
            //            {
            //                GlobalSettings.PassWordDT = "";
            //                ShowMessageTQDT("Thông tin lỗi: " + ex.Message, false);
            //                //MLMessages(ex.Message, "MSG_WRN35", "", false);
            //            }
            //            sendXML.Delete();
            //            setCommandStatus();
            //        }
            //        #endregion FPTService
            //    }

            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();

            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}

        }

        private void mnuNhanDuLieu_Click(object sender, EventArgs e)
        {

            tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            TKMD = this.getTKMDByID(tkmdID);
            FeedBackV3();

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            XoaToKhai();
        }

        private void XoaToKhai()
        {
            if (this.tkmdCollection.Count <= 0) return;

            try
            {
                if (MLMessages("Bạn có muốn xóa các thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "TK";
                            sendXML.master_id = tkmd.ID;
                            if (sendXML.Load())
                            {
                                MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan. Bạn không thể xóa.", "MSG_STN09", Convert.ToString(i.Position), false);
                                //if (st == "Yes")
                                //    tkmd.Delete();

                                continue;
                            }
                            else
                            {
                                //Load thong tin chung tu kem
                                tkmd.LoadChungTuKem();

                                //Xoa chung tu kem
                                for (int j = 0; j < tkmd.listCTDK.Count; j++)
                                {
                                    Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.DeleteBy_TKMDID(tkmd.ID);
                                }

                                tkmd.Delete();
                            }
                        }
                    }

                    timToKhai1_Search(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() == null) return;

            ToKhaiMauDich tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = tkmd.ID;
            bool isToKhaiNhap = tkmd.MaLoaiHinh.StartsWith("N");
            form.DeclarationIssuer = isToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT;
            form.ShowDialog(this);
        }

        private void mnuHuyKhaiBao_Click(object sender, EventArgs e)
        {
            //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
            //{
            CancelV3();
            //}
            //else
            //    HuyKhaiBao();
        }

        private void dtTuNgay_ValueChanged(object sender, EventArgs e)
        {
            //DateTime tuNgay = dtTuNgay.Value;
            //DateTime denNgay = DateTime.Now;
            //if (tuNgay > denNgay)
            //{
            //    ShowMessage("Có lỗi: Khoảng thời gian không hợp lệ.\t\nVui lòng chọn lại khoảng thời gian thích hợp", false);
            //    return;
            //}
        }

        private void dtDenNgay_ValueChanged(object sender, EventArgs e)
        {
            //DateTime tuNgay = dtTuNgay.Value;
            //DateTime denNgay = dtDenNgay.Value;
            //if (tuNgay > denNgay)
            //{
            //    ShowMessage("Có lỗi: Khoảng thời gian không hợp lệ.\t\nVui lòng chọn lại khoảng thời gian thích hợp", false);
            //    return;
            //}
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            int stt = 1;
            //Lấy đường dẫn file gốc
            string destFile = "";
            string fileName = "Danh_Sach_TK.xls";
            string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            try
            {
                Workbook workBook = Workbook.Load(sourcePath);
                Worksheet workSheet = workBook.Worksheets[0];

                int row = 2;
                //int col = 0;
                foreach (ToKhaiMauDich TKMD in tkmdCollection)
                {
                    workSheet.Rows[row].Cells[0].Value = stt;
                    workSheet.Rows[row].Cells[0].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[0].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[1].Value = TKMD.ID.ToString();
                    workSheet.Rows[row].Cells[1].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[1].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[2].Value = TKMD.SoTiepNhan.ToString();
                    workSheet.Rows[row].Cells[2].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[2].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[3].Value = TKMD.NgayTiepNhan.ToShortDateString();
                    workSheet.Rows[row].Cells[3].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[3].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[4].Value = TKMD.SoToKhai.ToString();
                    workSheet.Rows[row].Cells[4].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[4].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[5].Value = TKMD.NgayDangKy.ToShortDateString();
                    workSheet.Rows[row].Cells[5].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[5].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[6].Value = TKMD.SoHopDong;
                    workSheet.Rows[row].Cells[6].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[6].CellFormat.Font.Height = 10 * 20;
                    workSheet.Rows[row].Cells[5].CellFormat.Alignment = HorizontalCellAlignment.Fill;
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        workSheet.Rows[row].Cells[7].Value = "VN";
                        workSheet.Rows[row].Cells[7].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[row].Cells[7].CellFormat.Font.Height = 10 * 20;

                        workSheet.Rows[row].Cells[8].Value = TKMD.NuocXK_ID;
                        workSheet.Rows[row].Cells[8].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[row].Cells[8].CellFormat.Font.Height = 10 * 20;
                    }
                    else
                    {
                        workSheet.Rows[row].Cells[7].Value = TKMD.NuocNK_ID;
                        workSheet.Rows[row].Cells[7].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[row].Cells[7].CellFormat.Font.Height = 10 * 20;

                        workSheet.Rows[row].Cells[8].Value = "VN";
                        workSheet.Rows[row].Cells[8].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[row].Cells[8].CellFormat.Font.Height = 10 * 20;
                    }

                    //workSheet.Rows[row].Cells[9].Value = cbStatus.SelectedItem.Text.Trim();
                    workSheet.Rows[row].Cells[9].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[9].CellFormat.Font.Height = 10 * 20;

                    string phanluong = "";
                    if (TKMD.PhanLuong.Equals(Company.KDT.SHARE.Components.TrangThaiPhanLuong.LUONG_XANH))
                        phanluong = "Luồng xanh";
                    else if (TKMD.PhanLuong.Equals(Company.KDT.SHARE.Components.TrangThaiPhanLuong.LUONG_VANG))
                        phanluong = "Luồng vàng";
                    else
                        phanluong = "Luồng đỏ";
                    workSheet.Rows[row].Cells[10].Value = phanluong;
                    workSheet.Rows[row].Cells[10].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[10].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[11].Value = TKMD.HUONGDAN;
                    workSheet.Rows[row].Cells[11].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[11].CellFormat.Font.Height = 10 * 20;
                    //TODO:Cao Hữu Tú: updated 08-09-2011
                    //contents: format lại nội dung của ô nếu nội dung quá dài thì xuống hàng mới
                    workSheet.Rows[row].Cells[11].CellFormat.VerticalAlignment = VerticalCellAlignment.Justify;


                    row++;
                    stt++;
                }

                //Chọn đường dẫn file cần lưu
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                string dateNow = DateTime.Now.ToShortDateString().Replace("/", "");
                //if (chkDate.Checked)
                //    saveFileDialog1.FileName = "Danh_Sach_TK_TuNgay_" + dtTuNgay.Value.ToString("yyyy-MM-dd") + "_DenNgay_" + dtDenNgay.Value.ToString("yyyy-MM-dd");
                //else
                //    saveFileDialog1.FileName = "Danh_Sach_TK_" + System.DateTime.Now.ToString("yyyy-MM-dd");

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    destFile = saveFileDialog1.FileName;
                    //Ghi nội dung file gốc vào file cần lưu
                    try
                    {
                        byte[] sourceFile = Globals.ReadFile(sourcePath);
                        Globals.WriteFile(destFile, sourceFile);
                        workBook.Save(destFile);
                        ShowMessage("Lưu tệp tin thành công", false);
                        System.Diagnostics.Process.Start(destFile);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi: Không thể xuất Excel.\t\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }

        private void chkDate_CheckedChanged(object sender, EventArgs e)
        {
            //if (chkDate.Checked)
            //{
            //    lblTuNgay.Enabled = true;
            //    dtTuNgay.Enabled = true;
            //    lblDenNgay.Enabled = true;
            //    dtDenNgay.Enabled = true;
            //    Export.Enabled = InheritableBoolean.True;
            //}
            //else
            //{
            //    lblTuNgay.Enabled = false;
            //    dtTuNgay.Enabled = false;
            //    lblDenNgay.Enabled = false;
            //    dtDenNgay.Enabled = false;
            //    Export.Enabled = InheritableBoolean.False;
            //}
        }

        #region V3
        FeedBackContent feedbackContent = null;
        private void SendV3()
        {
            try
            {

                tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                sendXML.master_id = tkmdID;

                if (sendXML.Load())
                {
                    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Nhận phản hồi] để lấy thông tin phản hồi từ hải quan.", "MSG_SEN03", "", false);
                    mnuKhaiBao.Enabled = false;
                    mnuNhanDuLieu.Enabled = true;
                    return;
                }

                TKMD = this.getTKMDByID(tkmdID);
                //if (GlobalSettings.IsDaiLy && !GlobalSettings.ISKHAIBAO && TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                //    this.ShowMessage("Đại lý chưa đồng bộ tờ khai cho doanh nghiệp. Số tờ khai tối đa đại lý được khai là: " + GlobalSettings.SOTOKHAI_DONGBO.ToString(), false);
                //    return;
                //}
                #region Load data
                if (this.TKMD.HMDCollection == null || this.TKMD.HMDCollection.Count == 0)
                    this.TKMD.LoadHMDCollection();

                if (this.TKMD.ChungTuTKCollection == null || this.TKMD.ChungTuTKCollection.Count == 0)
                    this.TKMD.LoadChungTuTKCollection();

                if (this.TKMD.TKTGCollection == null || this.TKMD.TKTGCollection.Count == 0)
                    this.TKMD.LoadTKTGCollection();

                if (this.TKMD.TKTGPP23Collection == null || this.TKMD.TKTGPP23Collection.Count == 0)
                    this.TKMD.LoadToKhaiTriGiaPP23();

                this.TKMD.LoadChungTuHaiQuan();

                if (this.TKMD.ListCO == null || this.TKMD.ListCO.Count == 0)
                    this.TKMD.LoadCO();
                if (this.TKMD.GiayNopTiens == null || this.TKMD.GiayNopTiens.Count == 0)
                    this.TKMD.LoadListGiayNopTiens();
                #endregion Load data
                if (TKMD.SoToKhai != 0 && TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET && (!string.IsNullOrEmpty(TKMD.LyDoSua)
                    || TKMD.NoiDungDieuChinhTKCollection.Count > 0))
                {
                    ShowMessage("Chưa nhập lý do sửa hoặc chưa sửa thông tin của tờ khai.\r\nSau khi sửa hãy lưu lại thông tin tờ khai", false);
                    return;
                }
                //string s = Helpers.Serializer(TKMD);
                //byte[] arrItem = Helpers.Object2ByteArray(TKMD);
                ObjectSend msgSend = SingleMessage.SendMessageV3(TKMD);



                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                bool isSend = dlgSendForm.DoSend(msgSend);
                //Lưu ý phải để dòng code lưu message sau kết quả trả về.
                dlgSendForm.Message.XmlSaveMessage(TKMD.ID, TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET ? MessageTitle.KhaiBaoToKhai : MessageTitle.KhaiBaoSuaTK);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                    sendXML.master_id = TKMD.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    TKMD.Update();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        private void FeedBackV3()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBackMessageV3(TKMD);

                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) setCommandStatus();// timToKhai1_Search(null, null);
                }
            }
            timToKhai1_Search(null, null);
        }
        private void CancelV3()
        {
            tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            TKMD = this.getTKMDByID(tkmdID);
            ObjectSend msgSend = SingleMessage.CancelMessageV3(TKMD);


            SendMessageForm dlgSendForm = new SendMessageForm();
            dlgSendForm.Send += SendMessage;
            bool isSend = dlgSendForm.DoSend(msgSend);
            dlgSendForm.Message.XmlSaveMessage(TKMD.ID, MessageTitle.HuyKhaiBaoToKhai);
            if (isSend) FeedBackV3();

        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        /// <summary>
        /// Xử lý message tờ khai trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ToKhaiSendHandler(TKMD, ref msgInfor, sender, e);
        }
        #endregion

        int status = -1;

        private void timToKhai1_Search(object sender, Company.Interface.Controls.StringEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string where = "1 = 1";
                string maHQ = ctrDonViHaiQuan.Ma;
                if (string.IsNullOrEmpty(maHQ)) maHQ = GlobalSettings.MA_HAI_QUAN;
                where += string.Format(" AND MaHaiQuan = '{0}' and MaDoanhNghiep='{1}'", maHQ, GlobalSettings.MA_DON_VI);
                // Thực hiện tìm kiếm.            
                if (e != null)
                {
                    where += e.String;
                    status = e.Status;
                    // Nếu status==-1(Chưa khai báo) => lấy hết
                    //if (e.Status > -1)
                    //    where += " AND YEAR(NgayTiepNhan) = " + DateTime.Now.Year;

                }
                else
                {
                    where += " and trangthaixuly=" + status;

                    //if (status > -1)
                    //    where += " AND YEAR(NgayTiepNhan) = " + DateTime.Now.Year;

                    if (timToKhai1.cboPhanLuong.SelectedValue != null && timToKhai1.cboPhanLuong.SelectedIndex != 0)
                        where += " AND PhanLuong=" + timToKhai1.cboPhanLuong.SelectedValue;
                    if (!string.IsNullOrEmpty(timToKhai1.txtSoTiepNhan.Text))
                    {
                        where += " AND SoTiepNhan like '%" + timToKhai1.txtSoTiepNhan.Value + "%'";
                    }
                    if (timToKhai1.txtSoToKhai.TextLength > 0)
                    {
                        where += " AND SoToKhai like '%" + timToKhai1.txtSoToKhai.Text + "%'";
                    }
                }
                this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "ID desc");
                dgList.DataSource = this.tkmdCollection;

                this.setCommandStatus();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            //GridEXRow rowSelect = dgList.GetRow();
            //if (rowSelect == null || rowSelect.DataRow == null) return;
            //ToKhaiMauDich tkmd = (ToKhaiMauDich)rowSelect.DataRow;
            //if (tkmd != null)
            //{
            //    tkmd.LoadChungTuHaiQuan();
            //    tkmd.LoadHMDCollection();
            //    //toolTip.Close();
            //    toKhaiInfo.ToKhai = tkmd;

            //    //toolTip.Show(dgList, new System.Drawing.Point(MousePosition.X - 150, MousePosition.Y - 150));
            //}
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            timToKhai1_Search(null, null);
        }

        // In thông tin tờ khai. Updaetd by Hungtq, 24/09/2012.
        #region IN TO KHAI - CONTEXT MENNU

        /// <summary>
        /// Lấy thông tin tờ khai mậu dịch đang chọn
        /// </summary>
        /// <returns></returns>
        private Company.KD.BLL.KDT.ToKhaiMauDich GetTKMDSelected()
        {
            GridEXRow rowSelect = dgList.GetRow();

            if (rowSelect == null || rowSelect.DataRow == null)
                return new Company.KD.BLL.KDT.ToKhaiMauDich();

            Company.KD.BLL.KDT.ToKhaiMauDich tkmd = (Company.KD.BLL.KDT.ToKhaiMauDich)rowSelect.DataRow;

            if (tkmd != null)
            {
                tkmd.LoadChungTuHaiQuan();
                tkmd.LoadHMDCollection();

                if (tkmd.TKTGCollection == null || tkmd.TKTGCollection.Count == 0)
                    tkmd.LoadTKTGCollection();
            }

            return tkmd;
        }

        /// <summary>
        /// In tờ khai thông quan điện tử
        /// </summary>
        private void InToKhaiTQDT()
        {
            try
            {
                this.TKMD = GetTKMDSelected();

                if (TKMD.ID == 0)
                    return;

                switch (TKMD.MaLoaiHinh.Substring(0, 1).ToUpper())
                {
                    case "N":
                        ReportViewTKNTQDTForm f = new ReportViewTKNTQDTForm();
                        f.TKMD = TKMD;
                        f.ShowDialog(this);
                        break;

                    case "X":
                        ReportViewTKXTQDTForm f1 = new ReportViewTKXTQDTForm();
                        f1.TKMD = TKMD;
                        f1.ShowDialog(this);
                        break;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// In tờ khai thông quan điện tử theo Thông tư 196
        /// </summary>
        private void InToKhaiTQDT_TT15()
        {
            try
            {
                this.TKMD = GetTKMDSelected();

                if (this.TKMD.HMDCollection.Count == 0) return;
                if (this.TKMD.ID == 0)
                {
                    MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);
                    return;
                }
                if (this.TKMD.TrangThaiXuLy != 1)
                {
                    if (MLMessages("Tờ khai chưa được duyệt, bạn có muốn in hay không?", "MSG_PRI02", "", true) != "Yes") return;
                }
                switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
                {
                    case "N":
                        ReportViewTKNTQDTFormTT196 f = new ReportViewTKNTQDTFormTT196();
                        f.TKMD = this.TKMD;
                        f.ShowDialog(this);
                        break;

                    case "X":
                        ReportViewTKXTQDTFormTT196 f1 = new ReportViewTKXTQDTFormTT196();
                        f1.TKMD = this.TKMD;
                        f1.ShowDialog(this);
                        break;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// In tờ khai điện tử sửa đổi bổ sung
        /// </summary>
        private void InToKhaiTQDTSuaDoiBoSung()
        {
            try
            {
                this.TKMD = GetTKMDSelected();

                InToKhaiSuaDoiBoSungForm frmInToKhaiSuaDoiBoSung = new InToKhaiSuaDoiBoSungForm();
                InToKhaiSuaDoiBoSungForm.TKMD = TKMD;
                frmInToKhaiSuaDoiBoSung.ShowDialog(this);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// In bảng kê Container
        /// </summary>
        private void InBangKeContainer()
        {
            try
            {
                this.TKMD = GetTKMDSelected();
                if (TKMD.ID == 0)
                    return;
                Company.Interface.Report.SXXK.BangkeSoContainer f = new Company.Interface.Report.SXXK.BangkeSoContainer();
                f.tkmd = TKMD;
                f.BindReport();
                f.ShowPreview();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// In ấn định thuế tờ khai
        /// </summary>
        /// <param name="tkmdID"></param>
        /// <param name="guidstr"></param>
        private void InAnDinhThue()
        {
            try
            {
                this.TKMD = GetTKMDSelected();
                if (TKMD.ID == 0)
                    return;

                Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue anDinhThue = new Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue();
                List<Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue> dsADT = (List<Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                if (dsADT.Count > 0)
                {
                    anDinhThue = dsADT[0];
                    anDinhThue.LoadChiTiet();

                    Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                    anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                    anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                    anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                    anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue, 0);
                }
                else
                {
                    //Hungtq updated 21/02/2012.
                    //Không có thông tin Ấn định thuế hoặc Có thông tin ấn định thuế từ kết quả Hải quan trả về nhưng chưa lưu được xuống database.
                    Company.Interface.Globals.AnDinhThue_InsertFromXML(this.TKMD);

                    //InAnDinhThue
                    dsADT = (List<Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                    if (dsADT.Count > 0)
                    {
                        anDinhThue = dsADT[0];
                        anDinhThue.LoadChiTiet();

                        Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                        anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                        anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                        anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                        anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue, 0);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// In tờ khai trị giá PP1
        /// </summary>
        private void InToKhaiTriGiaPP1()
        {
            try
            {
                if (this.TKMD.TKTGCollection.Count > 0)
                {
                    ToKhaiTriGia TKTG = this.TKMD.TKTGCollection[0];

                    ReportViewTKTGA4Form f1 = new ReportViewTKTGA4Form();
                    f1.TKTG = TKTG;
                    f1.TKMD = this.TKMD;
                    f1.ShowDialog(this);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #endregion

        private void mniInToKhaiTQDT_Click(object sender, EventArgs e)
        {
            InToKhaiTQDT();
        }

        private void mniInToKhaiTQDT_TT15_Click(object sender, EventArgs e)
        {
            InToKhaiTQDT_TT15();
        }

        private void mniInToKhaiSuaDoiBoSung_Click(object sender, EventArgs e)
        {
            InToKhaiTQDTSuaDoiBoSung();
        }

        private void mniInToKhaiTriGiaPP1_Click(object sender, EventArgs e)
        {
            InToKhaiTriGiaPP1();
        }

        private void mniInBangKeContainer_Click(object sender, EventArgs e)
        {
            InBangKeContainer();
        }

        private void mniInAnDinhThue_Click(object sender, EventArgs e)
        {
            InAnDinhThue();
        }
    }
}