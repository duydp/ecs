﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_BaoCaoQuyetToan_TT39 : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public int TrangThaiXuLy { set; get; }
		public long SoTN { set; get; }
		public DateTime NgayTN { set; get; }
		public DateTime NgayBatDauBC { set; get; }
		public DateTime NgayKetThucBC { set; get; }
		public int LoaiBC { set; get; }
		public int LoaiSua { set; get; }
		public string MaHQ { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string GuidStr { set; get; }
		public string GhiChuKhac { set; get; }
        public List<KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail> HopDongCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_BaoCaoQuyetToan_TT39> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_BaoCaoQuyetToan_TT39> collection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39>();
			while (reader.Read())
			{
				KDT_VNACCS_BaoCaoQuyetToan_TT39 entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTN"))) entity.SoTN = reader.GetInt64(reader.GetOrdinal("SoTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTN"))) entity.NgayTN = reader.GetDateTime(reader.GetOrdinal("NgayTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDauBC"))) entity.NgayBatDauBC = reader.GetDateTime(reader.GetOrdinal("NgayBatDauBC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThucBC"))) entity.NgayKetThucBC = reader.GetDateTime(reader.GetOrdinal("NgayKetThucBC"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiBC"))) entity.LoaiBC = reader.GetInt32(reader.GetOrdinal("LoaiBC"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiSua"))) entity.LoaiSua = reader.GetInt32(reader.GetOrdinal("LoaiSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuKhac"))) entity.GhiChuKhac = reader.GetString(reader.GetOrdinal("GhiChuKhac"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_BaoCaoQuyetToan_TT39> collection, long id)
        {
            foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39 item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_TT39 VALUES(@TrangThaiXuLy, @SoTN, @NgayTN, @NgayBatDauBC, @NgayKetThucBC, @LoaiBC, @LoaiSua, @MaHQ, @MaDoanhNghiep, @GuidStr, @GhiChuKhac)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_TT39 SET TrangThaiXuLy = @TrangThaiXuLy, SoTN = @SoTN, NgayTN = @NgayTN, NgayBatDauBC = @NgayBatDauBC, NgayKetThucBC = @NgayKetThucBC, LoaiBC = @LoaiBC, LoaiSua = @LoaiSua, MaHQ = @MaHQ, MaDoanhNghiep = @MaDoanhNghiep, GuidStr = @GuidStr, GhiChuKhac = @GhiChuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_TT39 WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDauBC", SqlDbType.DateTime, "NgayBatDauBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThucBC", SqlDbType.DateTime, "NgayKetThucBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBC", SqlDbType.Int, "LoaiBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiSua", SqlDbType.Int, "LoaiSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDauBC", SqlDbType.DateTime, "NgayBatDauBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThucBC", SqlDbType.DateTime, "NgayKetThucBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBC", SqlDbType.Int, "LoaiBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiSua", SqlDbType.Int, "LoaiSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_TT39 VALUES(@TrangThaiXuLy, @SoTN, @NgayTN, @NgayBatDauBC, @NgayKetThucBC, @LoaiBC, @LoaiSua, @MaHQ, @MaDoanhNghiep, @GuidStr, @GhiChuKhac)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_TT39 SET TrangThaiXuLy = @TrangThaiXuLy, SoTN = @SoTN, NgayTN = @NgayTN, NgayBatDauBC = @NgayBatDauBC, NgayKetThucBC = @NgayKetThucBC, LoaiBC = @LoaiBC, LoaiSua = @LoaiSua, MaHQ = @MaHQ, MaDoanhNghiep = @MaDoanhNghiep, GuidStr = @GuidStr, GhiChuKhac = @GhiChuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_TT39 WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDauBC", SqlDbType.DateTime, "NgayBatDauBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThucBC", SqlDbType.DateTime, "NgayKetThucBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBC", SqlDbType.Int, "LoaiBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiSua", SqlDbType.Int, "LoaiSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDauBC", SqlDbType.DateTime, "NgayBatDauBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThucBC", SqlDbType.DateTime, "NgayKetThucBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBC", SqlDbType.Int, "LoaiBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiSua", SqlDbType.Int, "LoaiSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_BaoCaoQuyetToan_TT39 Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_BaoCaoQuyetToan_TT39> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_BaoCaoQuyetToan_TT39> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_BaoCaoQuyetToan_TT39> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_BaoCaoQuyetToan_TT39(int trangThaiXuLy, long soTN, DateTime ngayTN, DateTime ngayBatDauBC, DateTime ngayKetThucBC, int loaiBC, int loaiSua, string maHQ, string maDoanhNghiep, string guidStr, string ghiChuKhac)
		{
			KDT_VNACCS_BaoCaoQuyetToan_TT39 entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39();	
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.NgayBatDauBC = ngayBatDauBC;
			entity.NgayKetThucBC = ngayKetThucBC;
			entity.LoaiBC = loaiBC;
			entity.LoaiSua = loaiSua;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GuidStr = guidStr;
			entity.GhiChuKhac = ghiChuKhac;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@NgayBatDauBC", SqlDbType.DateTime, NgayBatDauBC.Year <= 1753 ? DBNull.Value : (object) NgayBatDauBC);
			db.AddInParameter(dbCommand, "@NgayKetThucBC", SqlDbType.DateTime, NgayKetThucBC.Year <= 1753 ? DBNull.Value : (object) NgayKetThucBC);
			db.AddInParameter(dbCommand, "@LoaiBC", SqlDbType.Int, LoaiBC);
			db.AddInParameter(dbCommand, "@LoaiSua", SqlDbType.Int, LoaiSua);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_BaoCaoQuyetToan_TT39> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39 item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_BaoCaoQuyetToan_TT39(long id, int trangThaiXuLy, long soTN, DateTime ngayTN, DateTime ngayBatDauBC, DateTime ngayKetThucBC, int loaiBC, int loaiSua, string maHQ, string maDoanhNghiep, string guidStr, string ghiChuKhac)
		{
			KDT_VNACCS_BaoCaoQuyetToan_TT39 entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39();			
			entity.ID = id;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.NgayBatDauBC = ngayBatDauBC;
			entity.NgayKetThucBC = ngayKetThucBC;
			entity.LoaiBC = loaiBC;
			entity.LoaiSua = loaiSua;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GuidStr = guidStr;
			entity.GhiChuKhac = ghiChuKhac;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_BaoCaoQuyetToan_TT39_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@NgayBatDauBC", SqlDbType.DateTime, NgayBatDauBC.Year <= 1753 ? DBNull.Value : (object) NgayBatDauBC);
			db.AddInParameter(dbCommand, "@NgayKetThucBC", SqlDbType.DateTime, NgayKetThucBC.Year <= 1753 ? DBNull.Value : (object) NgayKetThucBC);
			db.AddInParameter(dbCommand, "@LoaiBC", SqlDbType.Int, LoaiBC);
			db.AddInParameter(dbCommand, "@LoaiSua", SqlDbType.Int, LoaiSua);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_TT39> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39 item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_BaoCaoQuyetToan_TT39(long id, int trangThaiXuLy, long soTN, DateTime ngayTN, DateTime ngayBatDauBC, DateTime ngayKetThucBC, int loaiBC, int loaiSua, string maHQ, string maDoanhNghiep, string guidStr, string ghiChuKhac)
		{
			KDT_VNACCS_BaoCaoQuyetToan_TT39 entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39();			
			entity.ID = id;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.NgayBatDauBC = ngayBatDauBC;
			entity.NgayKetThucBC = ngayKetThucBC;
			entity.LoaiBC = loaiBC;
			entity.LoaiSua = loaiSua;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GuidStr = guidStr;
			entity.GhiChuKhac = ghiChuKhac;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@NgayBatDauBC", SqlDbType.DateTime, NgayBatDauBC.Year <= 1753 ? DBNull.Value : (object) NgayBatDauBC);
			db.AddInParameter(dbCommand, "@NgayKetThucBC", SqlDbType.DateTime, NgayKetThucBC.Year <= 1753 ? DBNull.Value : (object) NgayKetThucBC);
			db.AddInParameter(dbCommand, "@LoaiBC", SqlDbType.Int, LoaiBC);
			db.AddInParameter(dbCommand, "@LoaiSua", SqlDbType.Int, LoaiSua);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_TT39> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39 item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_BaoCaoQuyetToan_TT39(long id)
		{
			KDT_VNACCS_BaoCaoQuyetToan_TT39 entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_BaoCaoQuyetToan_TT39> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39 item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = -1;
                        this.ID = this.Insert();
                    }
                    else
                    {
                        this.Update();
                    }

                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in HopDongCollection)
                    {
                        item.GoodItem_ID = this.ID;
                        item.InsertUpdateFull();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    
                }
         }

        public void DeleteFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in HopDongCollection)
                    {
                        foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail itemDetail in item.HangHoaCollection)
                        {
                            itemDetail.Delete(transaction);
                        }
                        item.Delete(transaction);
                    }
                    this.Delete(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
	}	
}