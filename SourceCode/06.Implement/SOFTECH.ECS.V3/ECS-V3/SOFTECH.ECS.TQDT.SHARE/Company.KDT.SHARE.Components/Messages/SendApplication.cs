﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Ứng dụng gửi phía doanh nghiệp
    /// </summary>
    [XmlRoot("SendApplication")]
    public class SendApplication
    {
        /// <summary>
        /// Tên phần mềm
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }
        /// <summary>
        /// Phiên bản phần mềm
        /// </summary>
        [XmlElement("version")]
        public string Version { get; set; }
        /// <summary>
        /// Tên công ty
        /// </summary>
        [XmlElement("companyName")]
        public string CompanyName { get; set; }
        /// <summary>
        /// Mã công ty
        /// </summary>
        [XmlElement("companyIdentity")]
        public string CompanyIdentity { get; set; }

        /// <summary>
        /// Ngày giờ biên soạn message yyyy-MM-dd hh:mm:ss
        /// DateTime[Lấy phản hồi tờ khai nhập,Xuất]
        /// </summary>
        [XmlElement("createMessageIssue")]
        public string CreateMessageIssue { get; set; }

        /// <summary>
        /// Chữ ký xác thực phần mềm
        /// </summary>
        [XmlElement("Signature")]
        public MessageSignature MessageSignature { get; set; }
    }
}
