namespace DongBo.Controls
{
    partial class ucConfigDN
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaDN = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateBatDau = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateKetThuc = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboxPhanHe = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtServerSource = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUserSource = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPassSource = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPassTarget = new System.Windows.Forms.TextBox();
            this.txtUserTarget = new System.Windows.Forms.TextBox();
            this.txtServerTarget = new System.Windows.Forms.TextBox();
            this.lblCSDL = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pnlGC = new System.Windows.Forms.Panel();
            this.cboDatabaseTarget = new System.Windows.Forms.ComboBox();
            this.btnConnect2 = new System.Windows.Forms.Button();
            this.pnlSXXK = new System.Windows.Forms.Panel();
            this.chkTK = new System.Windows.Forms.CheckBox();
            this.chkDM = new System.Windows.Forms.CheckBox();
            this.chkSP = new System.Windows.Forms.CheckBox();
            this.chkNPL = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCopyDestinate = new System.Windows.Forms.Button();
            this.btnSwitch = new System.Windows.Forms.Button();
            this.btnCopySource = new System.Windows.Forms.Button();
            this.btnConnect1 = new System.Windows.Forms.Button();
            this.chkDaDuyet = new System.Windows.Forms.CheckBox();
            this.chkCompareData = new System.Windows.Forms.CheckBox();
            this.cboDatabaseSource = new System.Windows.Forms.ComboBox();
            this.chkLayToKhaiSua = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.chkToKhaiMauDich = new System.Windows.Forms.CheckBox();
            this.pnlBottom.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.pnlGC.SuspendLayout();
            this.pnlSXXK.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.label6);
            this.pnlBottom.Location = new System.Drawing.Point(5, 356);
            this.pnlBottom.Size = new System.Drawing.Size(716, 49);
            this.pnlBottom.TabIndex = 2;
            // 
            // pnlTop
            // 
            this.pnlTop.Size = new System.Drawing.Size(716, 28);
            this.pnlTop.TabIndex = 0;
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.button1);
            this.pnlContent.Controls.Add(this.cboDatabaseSource);
            this.pnlContent.Controls.Add(this.chkToKhaiMauDich);
            this.pnlContent.Controls.Add(this.chkCompareData);
            this.pnlContent.Controls.Add(this.chkLayToKhaiSua);
            this.pnlContent.Controls.Add(this.chkDaDuyet);
            this.pnlContent.Controls.Add(this.btnConnect2);
            this.pnlContent.Controls.Add(this.pnlGC);
            this.pnlContent.Controls.Add(this.panel1);
            this.pnlContent.Controls.Add(this.btnConnect1);
            this.pnlContent.Controls.Add(this.cboxPhanHe);
            this.pnlContent.Controls.Add(this.dateKetThuc);
            this.pnlContent.Controls.Add(this.dateBatDau);
            this.pnlContent.Controls.Add(this.txtPassSource);
            this.pnlContent.Controls.Add(this.txtUserSource);
            this.pnlContent.Controls.Add(this.txtServerSource);
            this.pnlContent.Controls.Add(this.txtMaDN);
            this.pnlContent.Controls.Add(this.label10);
            this.pnlContent.Controls.Add(this.label4);
            this.pnlContent.Controls.Add(this.label9);
            this.pnlContent.Controls.Add(this.label3);
            this.pnlContent.Controls.Add(this.label8);
            this.pnlContent.Controls.Add(this.label2);
            this.pnlContent.Controls.Add(this.label7);
            this.pnlContent.Controls.Add(this.label5);
            this.pnlContent.Controls.Add(this.label1);
            this.pnlContent.Controls.Add(this.pnlSXXK);
            this.pnlContent.Size = new System.Drawing.Size(716, 328);
            this.pnlContent.TabIndex = 1;
            // 
            // lblCaption
            // 
            this.lblCaption.Size = new System.Drawing.Size(301, 22);
            this.lblCaption.Text = "Cấu hình thông số đồng bộ dữ liệu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã doanh nghiệp:";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(131, 13);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(136, 21);
            this.txtMaDN.TabIndex = 0;
            this.txtMaDN.TextChanged += new System.EventHandler(this.txtMaDN_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(128, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngày bắt đầu:";
            // 
            // dateBatDau
            // 
            this.dateBatDau.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateBatDau.Location = new System.Drawing.Point(131, 74);
            this.dateBatDau.Name = "dateBatDau";
            this.dateBatDau.Size = new System.Drawing.Size(136, 21);
            this.dateBatDau.TabIndex = 2;
            this.dateBatDau.ValueChanged += new System.EventHandler(this.dateBatDau_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(229, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Đồng bộ dữ liệu trong khoảng thời gian:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(283, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ngày kết thúc:";
            // 
            // dateKetThuc
            // 
            this.dateKetThuc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateKetThuc.Location = new System.Drawing.Point(286, 74);
            this.dateKetThuc.Name = "dateKetThuc";
            this.dateKetThuc.Size = new System.Drawing.Size(136, 21);
            this.dateKetThuc.TabIndex = 4;
            this.dateKetThuc.ValueChanged += new System.EventHandler(this.dateKetThuc_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(39, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Chọn phân hệ:";
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(5, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(706, 39);
            this.label6.TabIndex = 0;
            this.label6.Text = "Hãy chọn chính xác khoảng thời gian của dữ liệu mà doanh nghiệp muốn đồng bộ về";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboxPhanHe
            // 
            this.cboxPhanHe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxPhanHe.FormattingEnabled = true;
            this.cboxPhanHe.Items.AddRange(new object[] {
            "Phân hệ GC",
            "Phân hệ SXXK"});
            this.cboxPhanHe.Location = new System.Drawing.Point(131, 110);
            this.cboxPhanHe.Name = "cboxPhanHe";
            this.cboxPhanHe.Size = new System.Drawing.Size(136, 21);
            this.cboxPhanHe.TabIndex = 5;
            this.cboxPhanHe.SelectedIndexChanged += new System.EventHandler(this.cboxPhanHe_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Địa chỉ máy nguồn :";
            // 
            // txtServerSource
            // 
            this.txtServerSource.Location = new System.Drawing.Point(131, 137);
            this.txtServerSource.Name = "txtServerSource";
            this.txtServerSource.Size = new System.Drawing.Size(136, 21);
            this.txtServerSource.TabIndex = 6;
            this.txtServerSource.TextChanged += new System.EventHandler(this.txtSQLAddress_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(43, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tên truy cập:";
            // 
            // txtUserSource
            // 
            this.txtUserSource.Location = new System.Drawing.Point(131, 164);
            this.txtUserSource.Name = "txtUserSource";
            this.txtUserSource.Size = new System.Drawing.Size(136, 21);
            this.txtUserSource.TabIndex = 7;
            this.txtUserSource.TextChanged += new System.EventHandler(this.txtSQLUser_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(61, 194);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Mật khẩu:";
            // 
            // txtPassSource
            // 
            this.txtPassSource.Location = new System.Drawing.Point(131, 191);
            this.txtPassSource.Name = "txtPassSource";
            this.txtPassSource.PasswordChar = '*';
            this.txtPassSource.Size = new System.Drawing.Size(136, 21);
            this.txtPassSource.TabIndex = 8;
            this.txtPassSource.TextChanged += new System.EventHandler(this.txtSQLPass_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(62, 221);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tên CSDL:";
            // 
            // txtPassTarget
            // 
            this.txtPassTarget.Location = new System.Drawing.Point(115, 76);
            this.txtPassTarget.Name = "txtPassTarget";
            this.txtPassTarget.PasswordChar = '*';
            this.txtPassTarget.Size = new System.Drawing.Size(136, 21);
            this.txtPassTarget.TabIndex = 2;
            this.txtPassTarget.TextChanged += new System.EventHandler(this.txtMKD_TextChanged);
            // 
            // txtUserTarget
            // 
            this.txtUserTarget.Location = new System.Drawing.Point(115, 49);
            this.txtUserTarget.Name = "txtUserTarget";
            this.txtUserTarget.Size = new System.Drawing.Size(136, 21);
            this.txtUserTarget.TabIndex = 1;
            this.txtUserTarget.TextChanged += new System.EventHandler(this.txtTCD_TextChanged);
            // 
            // txtServerTarget
            // 
            this.txtServerTarget.Location = new System.Drawing.Point(115, 22);
            this.txtServerTarget.Name = "txtServerTarget";
            this.txtServerTarget.Size = new System.Drawing.Size(136, 21);
            this.txtServerTarget.TabIndex = 0;
            this.txtServerTarget.TextChanged += new System.EventHandler(this.txtMCD_TextChanged);
            // 
            // lblCSDL
            // 
            this.lblCSDL.AutoSize = true;
            this.lblCSDL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCSDL.Location = new System.Drawing.Point(47, 106);
            this.lblCSDL.Name = "lblCSDL";
            this.lblCSDL.Size = new System.Drawing.Size(62, 13);
            this.lblCSDL.TabIndex = 6;
            this.lblCSDL.Text = "Tên CSDL:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(46, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Mật khẩu:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(28, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Tên truy cập:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(7, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "Địa chỉ máy đích:";
            // 
            // pnlGC
            // 
            this.pnlGC.Controls.Add(this.cboDatabaseTarget);
            this.pnlGC.Controls.Add(this.label14);
            this.pnlGC.Controls.Add(this.label13);
            this.pnlGC.Controls.Add(this.txtPassTarget);
            this.pnlGC.Controls.Add(this.label12);
            this.pnlGC.Controls.Add(this.txtUserTarget);
            this.pnlGC.Controls.Add(this.lblCSDL);
            this.pnlGC.Controls.Add(this.txtServerTarget);
            this.pnlGC.Location = new System.Drawing.Point(415, 108);
            this.pnlGC.Name = "pnlGC";
            this.pnlGC.Size = new System.Drawing.Size(261, 131);
            this.pnlGC.TabIndex = 14;
            // 
            // cboDatabaseTarget
            // 
            this.cboDatabaseTarget.FormattingEnabled = true;
            this.cboDatabaseTarget.Location = new System.Drawing.Point(115, 103);
            this.cboDatabaseTarget.Name = "cboDatabaseTarget";
            this.cboDatabaseTarget.Size = new System.Drawing.Size(136, 21);
            this.cboDatabaseTarget.TabIndex = 3;
            this.cboDatabaseTarget.SelectedIndexChanged += new System.EventHandler(this.cboDatabaseTarget_SelectedIndexChanged);
            this.cboDatabaseTarget.DropDownClosed += new System.EventHandler(this.cboDatabaseTarget_DropDownClosed);
            this.cboDatabaseTarget.DropDown += new System.EventHandler(this.cboDatabaseTarget_DropDown);
            // 
            // btnConnect2
            // 
            this.btnConnect2.Image = global::DongBo.Properties.Resources.disconnect;
            this.btnConnect2.Location = new System.Drawing.Point(447, 291);
            this.btnConnect2.Name = "btnConnect2";
            this.btnConnect2.Size = new System.Drawing.Size(75, 23);
            this.btnConnect2.TabIndex = 17;
            this.btnConnect2.Text = "Kết nối";
            this.btnConnect2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConnect2.UseVisualStyleBackColor = true;
            this.btnConnect2.Click += new System.EventHandler(this.btnConnect2_Click);
            // 
            // pnlSXXK
            // 
            this.pnlSXXK.Controls.Add(this.chkTK);
            this.pnlSXXK.Controls.Add(this.chkDM);
            this.pnlSXXK.Controls.Add(this.chkSP);
            this.pnlSXXK.Controls.Add(this.chkNPL);
            this.pnlSXXK.Location = new System.Drawing.Point(286, 108);
            this.pnlSXXK.Name = "pnlSXXK";
            this.pnlSXXK.Size = new System.Drawing.Size(124, 131);
            this.pnlSXXK.TabIndex = 15;
            // 
            // chkTK
            // 
            this.chkTK.AutoSize = true;
            this.chkTK.Checked = true;
            this.chkTK.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTK.Location = new System.Drawing.Point(20, 101);
            this.chkTK.Name = "chkTK";
            this.chkTK.Size = new System.Drawing.Size(60, 17);
            this.chkTK.TabIndex = 0;
            this.chkTK.Text = "Tờ khai";
            this.chkTK.UseVisualStyleBackColor = true;
            this.chkTK.CheckedChanged += new System.EventHandler(this.chkTK_CheckedChanged);
            // 
            // chkDM
            // 
            this.chkDM.AutoSize = true;
            this.chkDM.Checked = true;
            this.chkDM.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDM.Location = new System.Drawing.Point(20, 76);
            this.chkDM.Name = "chkDM";
            this.chkDM.Size = new System.Drawing.Size(71, 17);
            this.chkDM.TabIndex = 0;
            this.chkDM.Text = "Định mức";
            this.chkDM.UseVisualStyleBackColor = true;
            this.chkDM.CheckedChanged += new System.EventHandler(this.chkDM_CheckedChanged);
            // 
            // chkSP
            // 
            this.chkSP.AutoSize = true;
            this.chkSP.Checked = true;
            this.chkSP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSP.Location = new System.Drawing.Point(20, 51);
            this.chkSP.Name = "chkSP";
            this.chkSP.Size = new System.Drawing.Size(73, 17);
            this.chkSP.TabIndex = 0;
            this.chkSP.Text = "Sản phẩm";
            this.chkSP.UseVisualStyleBackColor = true;
            this.chkSP.CheckedChanged += new System.EventHandler(this.chkSP_CheckedChanged);
            // 
            // chkNPL
            // 
            this.chkNPL.AutoSize = true;
            this.chkNPL.Checked = true;
            this.chkNPL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNPL.Location = new System.Drawing.Point(20, 26);
            this.chkNPL.Name = "chkNPL";
            this.chkNPL.Size = new System.Drawing.Size(103, 17);
            this.chkNPL.TabIndex = 0;
            this.chkNPL.Text = "Nguyên phụ liệu";
            this.chkNPL.UseVisualStyleBackColor = true;
            this.chkNPL.CheckedChanged += new System.EventHandler(this.chkNPL_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCopyDestinate);
            this.panel1.Controls.Add(this.btnSwitch);
            this.panel1.Controls.Add(this.btnCopySource);
            this.panel1.Location = new System.Drawing.Point(210, 289);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 27);
            this.panel1.TabIndex = 17;
            // 
            // btnCopyDestinate
            // 
            this.btnCopyDestinate.AutoSize = true;
            this.btnCopyDestinate.Image = global::DongBo.Properties.Resources.back;
            this.btnCopyDestinate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCopyDestinate.Location = new System.Drawing.Point(151, 2);
            this.btnCopyDestinate.Name = "btnCopyDestinate";
            this.btnCopyDestinate.Size = new System.Drawing.Size(79, 23);
            this.btnCopyDestinate.TabIndex = 2;
            this.btnCopyDestinate.Text = "Sao chép";
            this.btnCopyDestinate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCopyDestinate.UseVisualStyleBackColor = true;
            this.btnCopyDestinate.Click += new System.EventHandler(this.btnCopyDestinate_Click);
            // 
            // btnSwitch
            // 
            this.btnSwitch.AutoSize = true;
            this.btnSwitch.Image = global::DongBo.Properties.Resources.arrow_refresh;
            this.btnSwitch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSwitch.Location = new System.Drawing.Point(81, 2);
            this.btnSwitch.Name = "btnSwitch";
            this.btnSwitch.Size = new System.Drawing.Size(70, 23);
            this.btnSwitch.TabIndex = 1;
            this.btnSwitch.Text = "Chuyển";
            this.btnSwitch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSwitch.UseVisualStyleBackColor = true;
            this.btnSwitch.Click += new System.EventHandler(this.btnSwitch_Click);
            // 
            // btnCopySource
            // 
            this.btnCopySource.AutoSize = true;
            this.btnCopySource.Image = global::DongBo.Properties.Resources.next;
            this.btnCopySource.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCopySource.Location = new System.Drawing.Point(2, 2);
            this.btnCopySource.Name = "btnCopySource";
            this.btnCopySource.Size = new System.Drawing.Size(79, 23);
            this.btnCopySource.TabIndex = 0;
            this.btnCopySource.Text = "Sao chép";
            this.btnCopySource.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCopySource.UseVisualStyleBackColor = true;
            this.btnCopySource.Click += new System.EventHandler(this.btnCopySource_Click);
            // 
            // btnConnect1
            // 
            this.btnConnect1.Image = global::DongBo.Properties.Resources.disconnect;
            this.btnConnect1.Location = new System.Drawing.Point(129, 291);
            this.btnConnect1.Name = "btnConnect1";
            this.btnConnect1.Size = new System.Drawing.Size(75, 23);
            this.btnConnect1.TabIndex = 16;
            this.btnConnect1.Text = "Kết nối";
            this.btnConnect1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConnect1.UseVisualStyleBackColor = true;
            this.btnConnect1.Click += new System.EventHandler(this.btnConnect1_Click);
            // 
            // chkDaDuyet
            // 
            this.chkDaDuyet.AutoSize = true;
            this.chkDaDuyet.Checked = true;
            this.chkDaDuyet.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDaDuyet.Location = new System.Drawing.Point(131, 245);
            this.chkDaDuyet.Name = "chkDaDuyet";
            this.chkDaDuyet.Size = new System.Drawing.Size(154, 17);
            this.chkDaDuyet.TabIndex = 14;
            this.chkDaDuyet.Text = "Chỉ lấy thông tin đã duyệt.";
            this.chkDaDuyet.UseVisualStyleBackColor = true;
            this.chkDaDuyet.CheckedChanged += new System.EventHandler(this.chkDaDuyet_CheckedChanged);
            // 
            // chkCompareData
            // 
            this.chkCompareData.AutoSize = true;
            this.chkCompareData.Checked = true;
            this.chkCompareData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCompareData.Location = new System.Drawing.Point(286, 245);
            this.chkCompareData.Name = "chkCompareData";
            this.chkCompareData.Size = new System.Drawing.Size(165, 17);
            this.chkCompareData.TabIndex = 15;
            this.chkCompareData.Text = "So sánh dữ liệu nguồn - đích.";
            this.chkCompareData.UseVisualStyleBackColor = true;
            this.chkCompareData.CheckedChanged += new System.EventHandler(this.chkCompareData_CheckedChanged);
            // 
            // cboDatabaseSource
            // 
            this.cboDatabaseSource.FormattingEnabled = true;
            this.cboDatabaseSource.Items.AddRange(new object[] {
            "(Làm mới)"});
            this.cboDatabaseSource.Location = new System.Drawing.Point(130, 218);
            this.cboDatabaseSource.Name = "cboDatabaseSource";
            this.cboDatabaseSource.Size = new System.Drawing.Size(137, 21);
            this.cboDatabaseSource.TabIndex = 9;
            this.cboDatabaseSource.SelectedIndexChanged += new System.EventHandler(this.cboDatabaseSource_SelectedIndexChanged);
            this.cboDatabaseSource.DropDownClosed += new System.EventHandler(this.cboDatabaseSource_DropDownClosed);
            this.cboDatabaseSource.DropDown += new System.EventHandler(this.cboDatabaseSource_DropDown);
            // 
            // chkLayToKhaiSua
            // 
            this.chkLayToKhaiSua.AutoSize = true;
            this.chkLayToKhaiSua.Checked = true;
            this.chkLayToKhaiSua.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLayToKhaiSua.Location = new System.Drawing.Point(130, 268);
            this.chkLayToKhaiSua.Name = "chkLayToKhaiSua";
            this.chkLayToKhaiSua.Size = new System.Drawing.Size(284, 17);
            this.chkLayToKhaiSua.TabIndex = 14;
            this.chkLayToKhaiSua.Text = "Chỉ lấy danh sách tờ khai có sửa đổi bổ sung thông tin";
            this.chkLayToKhaiSua.UseVisualStyleBackColor = true;
            this.chkLayToKhaiSua.CheckedChanged += new System.EventHandler(this.chkLayToKhaiSua_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(273, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 23);
            this.button1.TabIndex = 18;
            this.button1.Text = "Tạo tài khoản";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkToKhaiMauDich
            // 
            this.chkToKhaiMauDich.AutoSize = true;
            this.chkToKhaiMauDich.Checked = true;
            this.chkToKhaiMauDich.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkToKhaiMauDich.Location = new System.Drawing.Point(425, 268);
            this.chkToKhaiMauDich.Name = "chkToKhaiMauDich";
            this.chkToKhaiMauDich.Size = new System.Drawing.Size(267, 17);
            this.chkToKhaiMauDich.TabIndex = 15;
            this.chkToKhaiMauDich.Text = "Update tờ khai mậu dịch và các chứng từ đính kèm";
            this.chkToKhaiMauDich.UseVisualStyleBackColor = true;
            this.chkToKhaiMauDich.CheckedChanged += new System.EventHandler(this.chkToKhaiMauDich_CheckedChanged);
            // 
            // ucConfigDN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ucConfigDN";
            this.Size = new System.Drawing.Size(726, 405);
            this.Load += new System.EventHandler(this.ucConfigDN_Load);
            this.pnlBottom.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.pnlGC.ResumeLayout(false);
            this.pnlGC.PerformLayout();
            this.pnlSXXK.ResumeLayout(false);
            this.pnlSXXK.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtMaDN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateBatDau;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateKetThuc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboxPhanHe;
        private System.Windows.Forms.TextBox txtPassSource;
        private System.Windows.Forms.TextBox txtUserSource;
        private System.Windows.Forms.TextBox txtServerSource;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPassTarget;
        private System.Windows.Forms.TextBox txtUserTarget;
        private System.Windows.Forms.TextBox txtServerTarget;
        private System.Windows.Forms.Label lblCSDL;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel pnlGC;
        private System.Windows.Forms.Panel pnlSXXK;
        private System.Windows.Forms.CheckBox chkTK;
        private System.Windows.Forms.CheckBox chkDM;
        private System.Windows.Forms.CheckBox chkSP;
        private System.Windows.Forms.CheckBox chkNPL;
        private System.Windows.Forms.Button btnConnect1;
        private System.Windows.Forms.Button btnConnect2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCopySource;
        private System.Windows.Forms.Button btnCopyDestinate;
        private System.Windows.Forms.Button btnSwitch;
        private System.Windows.Forms.CheckBox chkDaDuyet;
        private System.Windows.Forms.CheckBox chkCompareData;
        private System.Windows.Forms.ComboBox cboDatabaseTarget;
        private System.Windows.Forms.ComboBox cboDatabaseSource;
        private System.Windows.Forms.CheckBox chkLayToKhaiSua;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkToKhaiMauDich;
    }
}
