﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using System.Data;
using Company.KDT.SHARE.VNACCS.Maper;
using System.Threading;

namespace TestVNACC
{
    public partial class TestTaiHeThongVNACCS : Form
    {

        DataTable dtTam = new DataTable("BanKhaiBaoTam");
        DataTable dtChinhThuc = new DataTable("BanKhaiBaoChinhThuc");

        public TestTaiHeThongVNACCS()
        {
            InitializeComponent();
            GlobalVNACC.PathConfig = Application.StartupPath + "\\Config";

            Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalID = "E1379C";
            Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalAccessKey = "WM75EO96N942GTKY";
            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_ID = "001";
            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma = "D4101";
            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Pass = "TGWDB9AX";

            dtTam = ReadXml(dtTam);
            dtChinhThuc = ReadXml(dtChinhThuc);
        }
     
        private DataTable KhoiTaoTable(DataTable dt)
        {
            //dt = new DataTable();
            //dt = new DataTable();
            DataColumn column1 = new DataColumn("SoToKhai", typeof(decimal));
            dt.Columns.Add(column1);
            DataColumn column12 = new DataColumn("NgayKhaiBao", typeof(DateTime));
            dt.Columns.Add(column12);
            if (dt.TableName.Contains("ChinhThuc"))
            {
                DataColumn column23 = new DataColumn("PhanLuong", typeof(string));
                dt.Columns.Add(column23);
            }
            return dt;
        }


        private void SendMessEdi(string msg,string bou)
        {
            try
            {
                byte[] postData = UTF8Encoding.UTF8.GetBytes(msg);
                string boundary = bou;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://ediconn.vnaccs.customs.gov.vn/test");
                boundary = boundary.Substring(2, boundary.Length - 2);
                httpWebRequest.ContentType = "multipart/signed; micalg=\"sha1\";boundary=\"" + boundary + "\"; protocol=\"application/x-pkcs7-signature\"";

                httpWebRequest.Method = "POST";
                httpWebRequest.KeepAlive = false;
                httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
                httpWebRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                httpWebRequest.UserAgent = "SOFTECH";
                httpWebRequest.ProtocolVersion = HttpVersion.Version11;
                httpWebRequest.ContentLength = postData.Length;
                // httpWebRequest.Expect = "100-continue";
                //httpWebRequest.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), httpWebRequest);
                //LoadingProcess();
                Stream memStream = new System.IO.MemoryStream();
                memStream.Write(postData, 0, postData.Length);


                httpWebRequest.ContentLength = memStream.Length;
                Stream requestStream = httpWebRequest.GetRequestStream();
                memStream.Position = 0;
                byte[] tempBuffer = new byte[memStream.Length];
                memStream.Read(tempBuffer, 0, tempBuffer.Length);
                memStream.Close();
                requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                requestStream.Close();
                try
                {
                    WebResponse webResponse = httpWebRequest.GetResponse();
                    Stream stream = webResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    string var = reader.ReadToEnd();
                    if (var != EnumThongBao.KetThucPhanHoi)
                    {
                        //MsgLog.SaveMessages(var, this.messagesSend.Header.MaNghiepVu.GetValue().ToString() + " Return", 0, EnumThongBao.ReturnMess, "", this.messagesSend.Header.MessagesTag.GetValue().ToString(), this.messagesSend.Header.InputMessagesID.GetValue().ToString(), this.messagesSend.Header.IndexTag.GetValue().ToString());
                        ResponseVNACCS feedback = new ResponseVNACCS(var);
                        string msgFeedBack = feedback.GetError();
                        if (feedback.Error == null || feedback.Error.Count == 0)
                        {
                            KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                            ProcessMessages.GetDataResult_TKMD(feedback.ResponseData, string.Empty, TKMD);
                            AddRowDtTam((decimal)TKMD.SoToKhai, TKMD.NgayDangKy);
                            EDC IDC = VNACCMaperFromObject.EDCMapper(TKMD.SoToKhai, string.Empty);
                            MessagesSend msgIDC = MessagesSend.Load<EDC>(IDC, TKMD.InputMessageID);
                            SendMessEdiIDC(HelperVNACCS.BuildEdiMessages(msgIDC).ToString());
                        }
                    }
                    //return var;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //msgFeedBack = ex.Message;
                this.DialogResult = DialogResult.Cancel;
            }
        }



        private void SendMessEdiIDC(string msg)
        {
            try
            {
                MIME mess = new MIME();
                System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = null;
                x509Certificate2 = Company.KDT.SHARE.Components.Cryptography.GetStoreX509Certificate2New(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName);
                mess = HelperVNACCS.GetMIME(msg, x509Certificate2);
                string boundary = mess.boundary;

                byte[] postData = UTF8Encoding.UTF8.GetBytes(mess.GetMessages().ToString());
                //string boundary = editBox1.Text.Trim();
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://ediconn.vnaccs.customs.gov.vn/test");
                boundary = boundary.Substring(2, boundary.Length - 2);
                httpWebRequest.ContentType = "multipart/signed; micalg=\"sha1\";boundary=\"" + boundary + "\"; protocol=\"application/x-pkcs7-signature\"";

                httpWebRequest.Method = "POST";
                httpWebRequest.KeepAlive = false;
                httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
                httpWebRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                httpWebRequest.UserAgent = "SOFTECH";
                httpWebRequest.ProtocolVersion = HttpVersion.Version11;
                httpWebRequest.ContentLength = postData.Length;
                // httpWebRequest.Expect = "100-continue";
                //httpWebRequest.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), httpWebRequest);
                //LoadingProcess();
                Stream memStream = new System.IO.MemoryStream();
                memStream.Write(postData, 0, postData.Length);


                httpWebRequest.ContentLength = memStream.Length;
                Stream requestStream = httpWebRequest.GetRequestStream();
                memStream.Position = 0;
                byte[] tempBuffer = new byte[memStream.Length];
                memStream.Read(tempBuffer, 0, tempBuffer.Length);
                memStream.Close();
                requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                requestStream.Close();
                try
                {
                    WebResponse webResponse = httpWebRequest.GetResponse();
                    Stream stream = webResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    string var = reader.ReadToEnd();
                    if (var != EnumThongBao.KetThucPhanHoi)
                    {
                        //MsgLog.SaveMessages(var, this.messagesSend.Header.MaNghiepVu.GetValue().ToString() + " Return", 0, EnumThongBao.ReturnMess, "", this.messagesSend.Header.MessagesTag.GetValue().ToString(), this.messagesSend.Header.InputMessagesID.GetValue().ToString(), this.messagesSend.Header.IndexTag.GetValue().ToString());
                        ResponseVNACCS feedback = new ResponseVNACCS(var);
                        string msgFeedBack = feedback.GetError();
                        if (feedback.Error == null && feedback.Error.Count == 0)
                        {
                            
                        }
                    }
                    //return var;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //msgFeedBack = ex.Message;
                this.DialogResult = DialogResult.Cancel;
            }
        }




        public void writeXml(DataTable dt)
        {
            try
            {

                //System.Xml.Serialization.XmlSerializer writer =
                  //  new System.Xml.Serialization.XmlSerializer(typeof(SizeFont));
                System.IO.FileInfo f = new System.IO.FileInfo(Application.StartupPath + "\\" + dt.TableName + ".xml");
                //if (f.Exists) f.Delete();
                //else
                {
                    if (!f.Directory.Exists)
                        f.Directory.Create();
                    System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath + "\\" + dt.TableName + ".xml");
                    dt.WriteXml(file);
                    //writer.Serialize(file, this);
                    file.Close();
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public DataTable ReadXml(DataTable dt)
        {
            try
            {
                System.IO.FileInfo info = new System.IO.FileInfo(Application.StartupPath + "\\" + dt.TableName + ".xml");
                if (info.Exists)
                {
                    System.IO.StreamReader file = new System.IO.StreamReader(Application.StartupPath + "\\" + dt.TableName + ".xml");
                    dt.ReadXml(file);
                    file.Close();
                    return dt;
                }
                else
                    return KhoiTaoTable(dt);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return KhoiTaoTable(dt);
            }
        }

        private void AddRowDtTam(decimal SoToKhai, DateTime NgayKhaiBao)
        {
            dtTam.Rows.Add(SoToKhai, NgayKhaiBao);
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                    {
                        gridBanTam.DataSource = dtTam;
                        gridBanTam.Refetch();
                    }));
            }
            else
            {
                gridBanTam.DataSource = dtTam;
                gridBanTam.Refetch();
            }
        }
        private void AddRowDtChinhThuc(long SoToKhai, DateTime NgayKhaiBao,string PhanLuong)
        {
            dtChinhThuc.Rows.Add(SoToKhai, NgayKhaiBao,PhanLuong);
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    gridBanChinhThuc.DataSource = dtTam;
                    gridBanChinhThuc.Refetch();
                }));
            }
            else
            {
                gridBanChinhThuc.DataSource = dtTam;
                gridBanChinhThuc.Refetch();
            }
        }


        private void DoWork(object obj)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                    {
                        string msgSend = richTextBox1.Text.Trim();
                        SendMessEdi(msgSend,editBox1.Text.Trim());
                        SoLuongDangThucHien--;
                    }));
            }
        }
        int SoLuongDangThucHien = 0;
        int SoLanDaThucHien = 0;
        private void uiButton1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            SoLanDaThucHien = 0;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int SoLanThucHien = System.Convert.ToInt16(numericEditBox1.Value);
            int SoLuong = 5;
            while (SoLuongDangThucHien < SoLuong && SoLanDaThucHien < SoLanThucHien)
            {
                SoLanDaThucHien++;
                SoLuongDangThucHien++;
                ThreadPool.QueueUserWorkItem(DoWork);
            }
            if (SoLanDaThucHien == SoLanThucHien)
                timer1.Enabled = false;
        }
        private void setError(string mserror)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                    {
                        lblStatus.Text = mserror;
                        lblStatus.ForeColor = Color.Red;
                    }));
            }
        }

        private void btnExportXMLTam_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                        {
                            writeXml(dtTam);
                            MessageBox.Show("Thanh cong");
                        }));
                }
                else
                {
                    writeXml(dtTam);
                    MessageBox.Show("Thanh cong");
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }



    }
}
