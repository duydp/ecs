-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Insert]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Update]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Delete]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Load]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_ThuVienCauHoi_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Insert]
	@LinhVuc_ID varchar(50),
	@Phanloai int,
	@TieuDe nvarchar(250),
	@NoiDungCauHoi nvarchar(max),
	@TraLoi nvarchar(max),
	@NgayTao datetime,
	@NgayCapNhat datetime,
	@HienThi bit,
	@GhiChu nvarchar(255),
	@MaCauHoi int OUTPUT
AS
INSERT INTO [dbo].[t_GopY_ThuVienCauHoi]
(
	[LinhVuc_ID],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[TraLoi],
	[NgayTao],
	[NgayCapNhat],
	[HienThi],
	[GhiChu]
)
VALUES 
(
	@LinhVuc_ID,
	@Phanloai,
	@TieuDe,
	@NoiDungCauHoi,
	@TraLoi,
	@NgayTao,
	@NgayCapNhat,
	@HienThi,
	@GhiChu
)

SET @MaCauHoi = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Update]
	@MaCauHoi int,
	@LinhVuc_ID varchar(50),
	@Phanloai int,
	@TieuDe nvarchar(250),
	@NoiDungCauHoi nvarchar(max),
	@TraLoi nvarchar(max),
	@NgayTao datetime,
	@NgayCapNhat datetime,
	@HienThi bit,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_GopY_ThuVienCauHoi]
SET
	[LinhVuc_ID] = @LinhVuc_ID,
	[Phanloai] = @Phanloai,
	[TieuDe] = @TieuDe,
	[NoiDungCauHoi] = @NoiDungCauHoi,
	[TraLoi] = @TraLoi,
	[NgayTao] = @NgayTao,
	[NgayCapNhat] = @NgayCapNhat,
	[HienThi] = @HienThi,
	[GhiChu] = @GhiChu
WHERE
	[MaCauHoi] = @MaCauHoi

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_InsertUpdate]
	@MaCauHoi int,
	@LinhVuc_ID varchar(50),
	@Phanloai int,
	@TieuDe nvarchar(250),
	@NoiDungCauHoi nvarchar(max),
	@TraLoi nvarchar(max),
	@NgayTao datetime,
	@NgayCapNhat datetime,
	@HienThi bit,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [MaCauHoi] FROM [dbo].[t_GopY_ThuVienCauHoi] WHERE [MaCauHoi] = @MaCauHoi)
	BEGIN
		UPDATE
			[dbo].[t_GopY_ThuVienCauHoi] 
		SET
			[LinhVuc_ID] = @LinhVuc_ID,
			[Phanloai] = @Phanloai,
			[TieuDe] = @TieuDe,
			[NoiDungCauHoi] = @NoiDungCauHoi,
			[TraLoi] = @TraLoi,
			[NgayTao] = @NgayTao,
			[NgayCapNhat] = @NgayCapNhat,
			[HienThi] = @HienThi,
			[GhiChu] = @GhiChu
		WHERE
			[MaCauHoi] = @MaCauHoi
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_GopY_ThuVienCauHoi]
		(
			[LinhVuc_ID],
			[Phanloai],
			[TieuDe],
			[NoiDungCauHoi],
			[TraLoi],
			[NgayTao],
			[NgayCapNhat],
			[HienThi],
			[GhiChu]
		)
		VALUES 
		(
			@LinhVuc_ID,
			@Phanloai,
			@TieuDe,
			@NoiDungCauHoi,
			@TraLoi,
			@NgayTao,
			@NgayCapNhat,
			@HienThi,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Delete]
	@MaCauHoi int
AS

DELETE FROM 
	[dbo].[t_GopY_ThuVienCauHoi]
WHERE
	[MaCauHoi] = @MaCauHoi

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GopY_ThuVienCauHoi] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_Load]
	@MaCauHoi int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaCauHoi],
	[LinhVuc_ID],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[TraLoi],
	[NgayTao],
	[NgayCapNhat],
	[HienThi],
	[GhiChu]
FROM
	[dbo].[t_GopY_ThuVienCauHoi]
WHERE
	[MaCauHoi] = @MaCauHoi
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaCauHoi],
	[LinhVuc_ID],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[TraLoi],
	[NgayTao],
	[NgayCapNhat],
	[HienThi],
	[GhiChu]
FROM [dbo].[t_GopY_ThuVienCauHoi] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_ThuVienCauHoi_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_ThuVienCauHoi_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaCauHoi],
	[LinhVuc_ID],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[TraLoi],
	[NgayTao],
	[NgayCapNhat],
	[HienThi],
	[GhiChu]
FROM
	[dbo].[t_GopY_ThuVienCauHoi]	

GO

