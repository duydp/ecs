﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class DeliveryDestination
    {
        /// <summary>
        /// địa điểm giao hàng
        /// </summary>
        [XmlElement("line")]
        public string Line { get; set; }
        /// <summary>
        /// Thời điểm giao hàng,bắt buộc,an..10
        /// </summary>
        [XmlElement("time")]
        public string Time { get; set; }
    }
}
