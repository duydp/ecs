﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class Cuc
    {
        public static List<Cuc> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<Cuc> collection = new List<Cuc>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                Cuc entity = new Cuc();
                entity.ID = dataSet.Tables[0].Rows[i]["ID"].ToString();
                entity.Ten = dataSet.Tables[0].Rows[i]["Ten"].ToString();
                entity.IPService = dataSet.Tables[0].Rows[i]["IPService"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["IPService"].ToString() : "";
                entity.ServicePathV1 = dataSet.Tables[0].Rows[i]["ServicePathV1"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ServicePathV1"].ToString() : "";
                entity.ServicePathV2 = dataSet.Tables[0].Rows[i]["ServicePathV2"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ServicePathV2"].ToString() : "";
                entity.ServicePathV3 = dataSet.Tables[0].Rows[i]["ServicePathV3"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ServicePathV3"].ToString() : "";
                entity.IPServiceCKS = dataSet.Tables[0].Rows[i]["IPServiceCKS"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["IPServiceCKS"].ToString() : "";
                entity.ServicePathCKS = dataSet.Tables[0].Rows[i]["ServicePathCKS"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ServicePathCKS"].ToString() : "";
                entity.IsDefault = dataSet.Tables[0].Rows[i]["IsDefault"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["IsDefault"].ToString() : (Globals.VersionSend == "3.00" ? "V3" : "V2");
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;
                collection.Add(entity);
            }

            return collection;
        }
    }
}
