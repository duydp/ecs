using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using System.Threading;
using Company.Interface.Report.SXXK;

namespace Company.Interface.SXXK
{
    public partial class ChayLaiThanhLyForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        private int LanThanhLy;
        private bool Change = true;
        private bool KhoiTao = true;
        private bool KetThuc = false;
        public ChayLaiThanhLyForm()
        {
            InitializeComponent();
        }

        private void ChayThanhLyForm_Load_1(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync(this.HSTL);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (this.KetThuc)
            {
                timer1.Enabled = false;
                Thread.Sleep(2000);
                this.Close();
            }
            if (this.Change)
            {
                if (this.KhoiTao)
                {
                    DataRow dr = dtProcess.NewRow();
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        dr["TenProcess"] = "Đang chạy thanh khoản hồ sơ lần thanh lý thứ " + this.LanThanhLy + " ...";
                        dr["TrangThai"] = "Đang thực hiện";
                    }
                    else
                    {
                        dr["TenProcess"] = "Running the Liquidation File in progress the time" + this.LanThanhLy + " ...";
                        dr["TrangThai"] = "Excuting…";
                    }
                    dtProcess.Rows.Add(dr);
                    this.KhoiTao = false;
                    Thread.Sleep(500);
                }
                else
                {

                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        dtProcess.Rows[dtProcess.Rows.Count - 1]["TrangThai"] = "Hoàn thành";
                    }
                    else
                    {
                        dtProcess.Rows[dtProcess.Rows.Count - 1]["TrangThai"] = "Finish";
                    }
                    this.Change = false;
                    this.KhoiTao = true;
                }

            }

        }

        private void ChayThanhLyForm_Shown(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Change = true;
            DataTable dt = this.HSTL.GetDanhSachHSTLByUserName1().Tables[0];
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    this.KhoiTao = true;
                    HoSoThanhLyDangKy HSTLDK = new HoSoThanhLyDangKy();
                    HSTLDK.ID = Convert.ToInt64(dr["Id"]);
                    HSTLDK.Load();
                    this.LanThanhLy = HSTLDK.LanThanhLy;
                    HSTLDK.LoadBKCollection();
                    int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
                    if (HSTLDK.SoTiepNhan == 0)
                        HSTLDK.ChayThanhLyNgayHoanThanhXuat(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK, GlobalSettings.ChayToKhaiNKD, GlobalSettings.AmTKTiep, chenhLech, GlobalSettings.ToKhaiKoTK);
                    else
                        HSTLDK.ChayThanhKhoanDungTKXGC(GlobalSettings.SoThapPhan.LuongNPL);

                    HSTLDK.DongHoSo();
                    Thread.Sleep(300);
                    this.Change = true;

                }
                this.HSTL.TrangThaiThanhKhoan = 401;
            }
            catch (Exception ex)
            {
                timer1.Enabled = false;
                ShowMessage(" " + ex.Message, false);
                this.Close();
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.KetThuc = true;
        }

    }
}