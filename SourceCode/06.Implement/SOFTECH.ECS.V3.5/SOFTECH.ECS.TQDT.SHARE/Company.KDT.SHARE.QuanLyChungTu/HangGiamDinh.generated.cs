using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class HangGiamDinh : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HMD_ID { set; get; }
		public long ChungThu_ID { set; get; }
		public string MaPhu { set; get; }
		public string TenHang { set; get; }
		public string MaHS { set; get; }
		public string NuocXX_ID { set; get; }
		public decimal SoLuong { set; get; }
		public string DVT_ID { set; get; }
		public string SoVanTaiDon { set; get; }
		public DateTime NgayVanDon { set; get; }
		public bool TinhTrangContainer { set; get; }
		public string SoHieuContainer { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<HangGiamDinh> ConvertToCollection(IDataReader reader)
		{
			IList<HangGiamDinh> collection = new List<HangGiamDinh>();
			while (reader.Read())
			{
				HangGiamDinh entity = new HangGiamDinh();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HMD_ID"))) entity.HMD_ID = reader.GetInt64(reader.GetOrdinal("HMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungThu_ID"))) entity.ChungThu_ID = reader.GetInt64(reader.GetOrdinal("ChungThu_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanTaiDon"))) entity.SoVanTaiDon = reader.GetString(reader.GetOrdinal("SoVanTaiDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhTrangContainer"))) entity.TinhTrangContainer = reader.GetBoolean(reader.GetOrdinal("TinhTrangContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuContainer"))) entity.SoHieuContainer = reader.GetString(reader.GetOrdinal("SoHieuContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<HangGiamDinh> collection, long id)
        {
            foreach (HangGiamDinh item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_ChungThuGiamDinh_HangGiamDinh VALUES(@HMD_ID, @ChungThu_ID, @MaPhu, @TenHang, @MaHS, @NuocXX_ID, @SoLuong, @DVT_ID, @SoVanTaiDon, @NgayVanDon, @TinhTrangContainer, @SoHieuContainer, @GhiChu)";
            string update = "UPDATE t_KDT_ChungThuGiamDinh_HangGiamDinh SET HMD_ID = @HMD_ID, ChungThu_ID = @ChungThu_ID, MaPhu = @MaPhu, TenHang = @TenHang, MaHS = @MaHS, NuocXX_ID = @NuocXX_ID, SoLuong = @SoLuong, DVT_ID = @DVT_ID, SoVanTaiDon = @SoVanTaiDon, NgayVanDon = @NgayVanDon, TinhTrangContainer = @TinhTrangContainer, SoHieuContainer = @SoHieuContainer, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ChungThuGiamDinh_HangGiamDinh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChungThu_ID", SqlDbType.BigInt, "ChungThu_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.VarChar, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanTaiDon", SqlDbType.VarChar, "SoVanTaiDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrangContainer", SqlDbType.Bit, "TinhTrangContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuContainer", SqlDbType.VarChar, "SoHieuContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChungThu_ID", SqlDbType.BigInt, "ChungThu_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.VarChar, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanTaiDon", SqlDbType.VarChar, "SoVanTaiDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrangContainer", SqlDbType.Bit, "TinhTrangContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuContainer", SqlDbType.VarChar, "SoHieuContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_ChungThuGiamDinh_HangGiamDinh VALUES(@HMD_ID, @ChungThu_ID, @MaPhu, @TenHang, @MaHS, @NuocXX_ID, @SoLuong, @DVT_ID, @SoVanTaiDon, @NgayVanDon, @TinhTrangContainer, @SoHieuContainer, @GhiChu)";
            string update = "UPDATE t_KDT_ChungThuGiamDinh_HangGiamDinh SET HMD_ID = @HMD_ID, ChungThu_ID = @ChungThu_ID, MaPhu = @MaPhu, TenHang = @TenHang, MaHS = @MaHS, NuocXX_ID = @NuocXX_ID, SoLuong = @SoLuong, DVT_ID = @DVT_ID, SoVanTaiDon = @SoVanTaiDon, NgayVanDon = @NgayVanDon, TinhTrangContainer = @TinhTrangContainer, SoHieuContainer = @SoHieuContainer, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ChungThuGiamDinh_HangGiamDinh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChungThu_ID", SqlDbType.BigInt, "ChungThu_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.VarChar, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanTaiDon", SqlDbType.VarChar, "SoVanTaiDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrangContainer", SqlDbType.Bit, "TinhTrangContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuContainer", SqlDbType.VarChar, "SoHieuContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChungThu_ID", SqlDbType.BigInt, "ChungThu_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.VarChar, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanTaiDon", SqlDbType.VarChar, "SoVanTaiDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrangContainer", SqlDbType.Bit, "TinhTrangContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuContainer", SqlDbType.VarChar, "SoHieuContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HangGiamDinh Load(long id)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<HangGiamDinh> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<HangGiamDinh> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<HangGiamDinh> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<HangGiamDinh> SelectCollectionBy_ChungThu_ID(long chungThu_ID)
		{
            IDataReader reader = SelectReaderBy_ChungThu_ID(chungThu_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		public static IList<HangGiamDinh> SelectCollectionBy_HMD_ID(long hMD_ID)
		{
            IDataReader reader = SelectReaderBy_HMD_ID(hMD_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ChungThu_ID(long chungThu_ID)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_ChungThu_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungThu_ID", SqlDbType.BigInt, chungThu_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------
		public static DataSet SelectBy_HMD_ID(long hMD_ID)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_HMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, hMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_ChungThu_ID(long chungThu_ID)
		{
			const string spName = "p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_ChungThu_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungThu_ID", SqlDbType.BigInt, chungThu_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		public static IDataReader SelectReaderBy_HMD_ID(long hMD_ID)
		{
			const string spName = "p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_HMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, hMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHangGiamDinh(long hMD_ID, long chungThu_ID, string maPhu, string tenHang, string maHS, string nuocXX_ID, decimal soLuong, string dVT_ID, string soVanTaiDon, DateTime ngayVanDon, bool tinhTrangContainer, string soHieuContainer, string ghiChu)
		{
			HangGiamDinh entity = new HangGiamDinh();	
			entity.HMD_ID = hMD_ID;
			entity.ChungThu_ID = chungThu_ID;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.NuocXX_ID = nuocXX_ID;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.SoVanTaiDon = soVanTaiDon;
			entity.NgayVanDon = ngayVanDon;
			entity.TinhTrangContainer = tinhTrangContainer;
			entity.SoHieuContainer = soHieuContainer;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@ChungThu_ID", SqlDbType.BigInt, ChungThu_ID);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.VarChar, NuocXX_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@SoVanTaiDon", SqlDbType.VarChar, SoVanTaiDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@TinhTrangContainer", SqlDbType.Bit, TinhTrangContainer);
			db.AddInParameter(dbCommand, "@SoHieuContainer", SqlDbType.VarChar, SoHieuContainer);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HangGiamDinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangGiamDinh item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHangGiamDinh(long id, long hMD_ID, long chungThu_ID, string maPhu, string tenHang, string maHS, string nuocXX_ID, decimal soLuong, string dVT_ID, string soVanTaiDon, DateTime ngayVanDon, bool tinhTrangContainer, string soHieuContainer, string ghiChu)
		{
			HangGiamDinh entity = new HangGiamDinh();			
			entity.ID = id;
			entity.HMD_ID = hMD_ID;
			entity.ChungThu_ID = chungThu_ID;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.NuocXX_ID = nuocXX_ID;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.SoVanTaiDon = soVanTaiDon;
			entity.NgayVanDon = ngayVanDon;
			entity.TinhTrangContainer = tinhTrangContainer;
			entity.SoHieuContainer = soHieuContainer;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_ChungThuGiamDinh_HangGiamDinh_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@ChungThu_ID", SqlDbType.BigInt, ChungThu_ID);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.VarChar, NuocXX_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@SoVanTaiDon", SqlDbType.VarChar, SoVanTaiDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@TinhTrangContainer", SqlDbType.Bit, TinhTrangContainer);
			db.AddInParameter(dbCommand, "@SoHieuContainer", SqlDbType.VarChar, SoHieuContainer);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HangGiamDinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangGiamDinh item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHangGiamDinh(long id, long hMD_ID, long chungThu_ID, string maPhu, string tenHang, string maHS, string nuocXX_ID, decimal soLuong, string dVT_ID, string soVanTaiDon, DateTime ngayVanDon, bool tinhTrangContainer, string soHieuContainer, string ghiChu)
		{
			HangGiamDinh entity = new HangGiamDinh();			
			entity.ID = id;
			entity.HMD_ID = hMD_ID;
			entity.ChungThu_ID = chungThu_ID;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.NuocXX_ID = nuocXX_ID;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.SoVanTaiDon = soVanTaiDon;
			entity.NgayVanDon = ngayVanDon;
			entity.TinhTrangContainer = tinhTrangContainer;
			entity.SoHieuContainer = soHieuContainer;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@ChungThu_ID", SqlDbType.BigInt, ChungThu_ID);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.VarChar, NuocXX_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@SoVanTaiDon", SqlDbType.VarChar, SoVanTaiDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@TinhTrangContainer", SqlDbType.Bit, TinhTrangContainer);
			db.AddInParameter(dbCommand, "@SoHieuContainer", SqlDbType.VarChar, SoHieuContainer);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HangGiamDinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangGiamDinh item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHangGiamDinh(long id)
		{
			HangGiamDinh entity = new HangGiamDinh();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_ChungThu_ID(long chungThu_ID)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_ChungThu_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungThu_ID", SqlDbType.BigInt, chungThu_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		public static int DeleteBy_HMD_ID(long hMD_ID)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_HMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, hMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HangGiamDinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangGiamDinh item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}