﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_ToKhaiDelete : BaseForm
    {
        public List<KDT_VNACC_ToKhaiMauDich> ListTKMDThu = new List<KDT_VNACC_ToKhaiMauDich>();
        public List<KDT_VNACC_ToKhaiMauDich> ListTKMDChinhthuc = new List<KDT_VNACC_ToKhaiMauDich>();
        DataSet ds = new DataSet();
        public bool IsShowChonToKhai = false;
        public KDT_VNACC_ToKhaiMauDich TKMDDuocChon = null;
        public VNACC_ToKhaiDelete()
        {
            InitializeComponent();

            this.Text = "Theo dõi tờ khai";
        }

        private void VNACC_ToKhaiManager_Load(object sender, EventArgs e)
        {

            LoadData();
        }
        private void LoadData()
        {
            try
            {
                string whereTKThu = " CreatedTime < '2014-04-01') or sotokhai=0 ";
                string whereTKChiThuc = " CreatedTime >= '2014-04-01') and  sotokhai <> 0  ";
                this.Cursor = Cursors.WaitCursor;
                ds = VNACC_Category_Common.SelectDynamic("ReferenceDB in ('E001','E002')", null);
                ListTKMDThu.Clear();
                ListTKMDChinhthuc.Clear();
                ListTKMDThu = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamicBy(whereTKThu, "ID");
                grList.DropDowns["drdMaLoaiHinh"].DataSource = ds.Tables[0];
                grList.DataSource = ListTKMDThu;
                grList.Refresh();
                ListTKMDChinhthuc = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamicBy(whereTKChiThuc, "ID");
                grListTKChinhThuc.DropDowns["drdMaLoaiHinh"].DataSource = ds.Tables[0];
                grListTKChinhThuc.DataSource = ListTKMDChinhthuc;
                grListTKChinhThuc.Refresh();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;

            }



        }
        private KDT_VNACC_ToKhaiMauDich getTKMDID(long id)
        {
            List<KDT_VNACC_ToKhaiMauDich> ListTKMD = KDT_VNACC_ToKhaiMauDich.SelectCollectionAll();
            foreach (KDT_VNACC_ToKhaiMauDich TKMD in ListTKMD)
            {

                if (TKMD.ID == id)
                {
                    string where = "TKMD_ID=" + TKMD.ID;
                    //Hang mau dich Collection
                    List<KDT_VNACC_HangMauDich> listHang = new List<KDT_VNACC_HangMauDich>();
                    listHang = KDT_VNACC_HangMauDich.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_HangMauDich hang in listHang)
                    {
                        List<KDT_VNACC_HangMauDich_ThueThuKhac> listThueThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                        listThueThuKhac = KDT_VNACC_HangMauDich_ThueThuKhac.SelectCollectionDynamic("Master_id=" + hang.ID, "ID");
                        foreach (KDT_VNACC_HangMauDich_ThueThuKhac thue in listThueThuKhac)
                        {
                            hang.ThueThuKhacCollection.Add(thue);
                        }
                        TKMD.HangCollection.Add(hang);
                    }
                    // giay phep Collection
                    List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
                    listGiayPhep = KDT_VNACC_TK_GiayPhep.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_GiayPhep item in listGiayPhep)
                    {
                        TKMD.GiayPhepCollection.Add(item);
                    }
                    // so dien tu dinh kem
                    List<KDT_VNACC_TK_DinhKemDienTu> listDinhKiem = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    listDinhKiem = KDT_VNACC_TK_DinhKemDienTu.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_DinhKemDienTu item in listDinhKiem)
                    {
                        TKMD.DinhKemCollection.Add(item);
                    }
                    // trung chuyển collection KDT_VNACC_TK_TrungChuyen
                    List<KDT_VNACC_TK_TrungChuyen> listTC = new List<KDT_VNACC_TK_TrungChuyen>();
                    listTC = KDT_VNACC_TK_TrungChuyen.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_TrungChuyen item in listTC)
                    {
                        TKMD.TrungChuyenCollection.Add(item);
                    }
                    //Chi thi Hai quan collection
                    List<KDT_VNACC_ChiThiHaiQuan> listChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
                    string whereChiThiHQ = string.Format("Master_ID = {0} and LoaiThongTin = 'TK'", TKMD.ID);
                    listChiThi = KDT_VNACC_ChiThiHaiQuan.SelectCollectionDynamic(whereChiThiHQ, "");
                    foreach (KDT_VNACC_ChiThiHaiQuan item in listChiThi)
                    {
                        TKMD.ChiThiHQCollection.Add(item);
                    }
                    // Van don Collection
                    List<KDT_VNACC_TK_SoVanDon> listVanDon = new List<KDT_VNACC_TK_SoVanDon>();
                    listVanDon = KDT_VNACC_TK_SoVanDon.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_SoVanDon item in listVanDon)
                    {
                        TKMD.VanDonCollection.Add(item);
                    }
                    //Tri gia Khoan Dieu Chinh
                    List<KDT_VNACC_TK_KhoanDieuChinh> listDieuChinh = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                    listDieuChinh = KDT_VNACC_TK_KhoanDieuChinh.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_KhoanDieuChinh item in listDieuChinh)
                    {
                        TKMD.KhoanDCCollection.Add(item);
                    }

                    List<KDT_VNACC_TK_Container> listContainer = new List<KDT_VNACC_TK_Container>();
                    listContainer = KDT_VNACC_TK_Container.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_Container item in listContainer)
                    {
                        List<KDT_VNACC_TK_SoContainer> listSoCont = new List<KDT_VNACC_TK_SoContainer>();
                        listSoCont = KDT_VNACC_TK_SoContainer.SelectCollectionDynamic("Master_id=" + item.ID, "ID");
                        foreach (KDT_VNACC_TK_SoContainer soCont in listSoCont)
                        {
                            item.SoContainerCollection.Add(soCont);
                        }
                        TKMD.ContainerTKMD = item;
                    }
                    List<KDT_VNACC_TK_SoContainer> listSoContainer = new List<KDT_VNACC_TK_SoContainer>();
                    listSoContainer = KDT_VNACC_TK_SoContainer.SelectCollectionDynamic("Master_ID =" + TKMD.ID, "ID");
                    //foreach (KDT_VNACC_TK_SoContainer item in listSoContainer)
                    //{
                    //    TKMD.SoContainerCollection.Add(item);
                    //}
                    List<KDT_VNACC_TK_PhanHoi_SacThue> listSacThue = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                    listSacThue = KDT_VNACC_TK_PhanHoi_SacThue.SelectCollectionDynamic("Master_ID =" + TKMD.ID, "ID");
                    foreach (KDT_VNACC_TK_PhanHoi_SacThue item in listSacThue)
                    {
                        TKMD.SacThueCollection.Add(item);
                    }
                    List<KDT_VNACC_TK_PhanHoi_TyGia> listTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    listTyGia = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionDynamic("Master_ID =" + TKMD.ID, "ID");
                    foreach (KDT_VNACC_TK_PhanHoi_TyGia item in listTyGia)
                    {
                        TKMD.TyGiaCollection.Add(item);
                    }
                    return TKMD;
                }
            }
            return null;
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;

                    int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                    TKMD = TKMD.LoadToKhai(id);
                    if (IsShowChonToKhai)
                    {
                        TKMDDuocChon = TKMD;
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                        return;
                    }
                    string loaiHinh = "";
                    DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                    DataRow dr = ds.Tables[0].Rows[0];
                    loaiHinh = dr["ReferenceDB"].ToString();

                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
                    {
                        VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }
                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
                    {
                        VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }
                    btnTimKiem_Click(null, null);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                }
                finally { Cursor = Cursors.Default; }
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {


        }

        private void cbbTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }
        private void CopyToKhai(KDT_VNACC_ToKhaiMauDich tkmd, bool isCopyHangHoa)
        {
            KDT_VNACC_ToKhaiMauDich tkmdcopy = new KDT_VNACC_ToKhaiMauDich();
            HelperVNACCS.UpdateObject<KDT_VNACC_ToKhaiMauDich>(tkmdcopy, tkmd, false);
            tkmdcopy.NgayDangKy = new DateTime(1900, 1, 1);
            tkmdcopy.SoToKhai = 0;
            tkmdcopy.SoNhanhToKhai = 0;
            tkmdcopy.SoToKhaiDauTien = "";
            tkmdcopy.MaPhanLoaiKiemTra = string.Empty;
            tkmdcopy.AnHangCollection = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
            tkmdcopy.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
            tkmdcopy.SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
            tkmdcopy.TrangThaiXuLy = "0";
            tkmdcopy.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
            tkmdcopy.InputMessageID = string.Empty;
            tkmdcopy.MessageTag = string.Empty;
            tkmdcopy.IndexTag = string.Empty;
            if (!isCopyHangHoa)
            {
                tkmdcopy.HangCollection = new List<KDT_VNACC_HangMauDich>();
            }

            DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + tkmdcopy.MaLoaiHinh + "'", "ID");
            DataRow dr = ds.Tables[0].Rows[0];
            string loaiHinh = dr["ReferenceDB"].ToString();
            if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
            {
                VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                //f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                f.TKMD = tkmdcopy;
                f.ShowDialog();
            }
            if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
            {
                VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                //f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                f.TKMD = tkmdcopy;
                f.ShowDialog();
            }
            btnTimKiem_Click(null, null);
        }

        private void copyNộiDungTờKhaiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (grList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(grList.SelectedItems[0].GetRow().Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                    TKMD = TKMD.LoadToKhai(id);
                    CopyToKhai(TKMD, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }
        }

        private void copyNộiDungTờKhaiHàngHóaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (grList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(grList.SelectedItems[0].GetRow().Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                    TKMD = TKMD.LoadToKhai(id);
                    CopyToKhai(TKMD, true);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                if (ShowMessage("Bạn có muốn xóa tờ khai này không?", true) == "Yes")
                {
                    GridEXRow[] listRow = grList.GetCheckedRows();
                    foreach (GridEXRow item in listRow)
                    {
                        if (item.RowType == RowType.Record)
                        {
                            int id = System.Convert.ToInt32(item.Cells["ID"].Value.ToString());
                            KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                            TKMD.DeleteFull(id);
                        }
                    }
                    ShowMessage("Xóa tờ khai thành công.", false);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" Lỗi: Không thể xóa tờ khai. ", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            LoadData();


        }



        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
                {
                    string pl = e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString();
                    if (pl == "1")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Xanh";
                    else if (pl == "2")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Vàng";
                    else if (pl == "3")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Đỏ";
                    else
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Chưa phân luồng";
                }
            }
        }


        private void grListTKChinhThuc_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;

                    int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                    TKMD = TKMD.LoadToKhai(id);
                    if (IsShowChonToKhai)
                    {
                        TKMDDuocChon = TKMD;
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                        return;
                    }
                    string loaiHinh = "";
                    DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                    DataRow dr = ds.Tables[0].Rows[0];
                    loaiHinh = dr["ReferenceDB"].ToString();

                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
                    {
                        VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }
                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
                    {
                        VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }
                    LoadData();
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                }
                finally { Cursor = Cursors.Default; }
            }
        }




    }
}
