﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using Company.KDT.SHARE.Components;
using Company.BLL.SXXK;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using System.Data;
using Company.Interface.SXXK;

namespace Company.Interface.KDT.SXXK.DNCX
{
    public partial class NguyenPhuLieuSendForm_CX : BaseFormHaveGuidPanel
    {
        public  HangDuaVaoDangKy nplDangKy = new HangDuaVaoDangKy() { DanhSachHangDuaVao = new List<HangDuaVao>() };
        public HangDuaVao NPLDetail;
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        private string xmlCurrent = "";
        public bool isAdd = true;
        public bool isEdit = false;
        public bool isCancel = false;
        public NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
        //-----------------------------------------------------------------------------------------
        public NguyenPhuLieuSendForm_CX()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtMa.Focus();
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            //cmbMucDich.DataSource = MucDichSuDung.SelectAll().Tables[0];
            //cmbMucDich.DisplayMember = "TenLoaiMucDich";
            //cmbMucDich.ValueMember = "ID";
        }

        //-----------------------------------------------------------------------------------------
        private void NguyenPhuLieuSendForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.khoitao_DuLieuChuan();
                if (nplDangKy.ID==0)
                {
                    if (isEdit)
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else if (isCancel)
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    else
                    {
                        if (nplDangKy.ID == 0)
                        {
                            nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;   
                        }
                    }
                }
                else
                {
                    nplDangKy.loadHang();
                }
                SetCommandStatus();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = this.nplDangKy.DanhSachHangDuaVao;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetCommandStatus()
        {
            if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                btnUpdate.Enabled = false;
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False; ;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnUpdate.Enabled = true;
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                lblTrangThai.Text = nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                btnUpdate.Enabled = false;
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                btnUpdate.Enabled = false;
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnUpdate.Enabled = true;
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnUpdate.Enabled = true;
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnUpdate.Enabled = false;
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Chờ duyệt", "Not declared");
                this.OpenType = OpenFormType.View;
            }


        }
        //-----------------------------------------------------------------------------------------
  

     
        /// <summary>
        /// Thêm mới nguyên phụ liệu.
        /// </summary>
        private void Add()
        {
            try
            {
                NguyenPhuLieuEditForm_CX f = new NguyenPhuLieuEditForm_CX();
                f.OpenType = OpenFormType.Insert;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                f.NPLCollection = nplDangKy.DanhSachHangDuaVao;
                f.nplDangKy = nplDangKy;
                f.ShowDialog(this);

                if (f.NPLDetail != null)
                {
                    this.nplDangKy.DanhSachHangDuaVao.Add(f.NPLDetail);
                    dgList.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void Save()
        {
            try
            {
                if (this.nplDangKy.DanhSachHangDuaVao.Count == 0)
                {
                    MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể cập nhật dữ liệu.", "MSG_SAV11", "", false);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                // Master.
                this.nplDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (this.nplDangKy.ID == 0)
                {
                    this.nplDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    this.nplDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.nplDangKy.NgayTiepNhan = new DateTime(1900, 1, 1);
                    //add lannt
                    this.nplDangKy.GUIDSTR = Guid.NewGuid().ToString();
                }
                if (this.nplDangKy.InsertUpdateFull())
                {
                    this.Cursor = Cursors.Default;
                    SetCommandStatus();
                    #region Lưu log thao tác
                    string where = "1 = 1";
                    where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", nplDangKy.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.NguyenPhuLieu);
                    List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                    if (listLog.Count > 0)
                    {
                        long idLog = listLog[0].IDLog;
                        string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                        long idDK = listLog[0].ID_DK;
                        string guidstr = listLog[0].GUIDSTR_DK;
                        string userKhaiBao = listLog[0].UserNameKhaiBao;
                        DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                        string userSuaDoi = GlobalSettings.UserLog;
                        DateTime ngaySuaDoi = DateTime.Now;
                        string ghiChu = listLog[0].GhiChu;
                        bool isDelete = listLog[0].IsDelete;
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                    userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                    }
                    else
                    {
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                        log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.NguyenPhuLieu;
                        log.ID_DK = nplDangKy.ID;
                        log.GUIDSTR_DK = "";
                        log.UserNameKhaiBao = GlobalSettings.UserLog;
                        log.NgayKhaiBao = DateTime.Now;
                        log.UserNameSuaDoi = GlobalSettings.UserLog;
                        log.NgaySuaDoi = DateTime.Now;
                        log.GhiChu = "";
                        log.IsDelete = false;
                        log.Insert();
                    }
                    #endregion

                    MLMessages("Lưu thành công!", "MSG_SAV02", "", false);

                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else
                {
                    this.Cursor = Cursors.Default;
                    MLMessages("Lưu không thành công!", "MSG_SAV01", "", false);

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void Edit()
        {
            try
            {
                string msg = "";
                msg += "================================";
                msg += "\nNguyên phụ liệu này đã khai báo đến Hải Quan";
                msg += "\n\nBạn có muốn chuyển trạng thái sửa không ?";
                msg += "\nNguyên phụ liêu có số tiếp nhận : " + nplDangKy.SoTiepNhan.ToString();
                msg += "=================================";
                if (ShowMessage(msg, true) == "Yes")
                {
                    nplDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    nplDangKy.InsertUpdate();
                    SetCommandStatus();
                    Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.DeleteFull(nplDangKy, nplDangKy.MaDoanhNghiep, nplDangKy.MaHaiQuan);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        //-----------------------------------------------------------------------------------------
        private void updateRowOnGrid(string maNPL)
        {
            try
            {
                GridEXRow[] jrows = dgList.GetRows();
                foreach (GridEXRow row in jrows)
                {
                    if (row.Cells["Ma"].Value.ToString().Equals(maNPL))
                    {
                        row.BeginEdit();
                        row.Cells["IsExistOnServer"].Value = 1;
                        row.EndEdit();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------
        private void updateRowsOnGrid(IEnumerable<string> collection)
        {
            try
            {
                GridEXRow[] jrows = dgList.GetRows();
                foreach (GridEXRow row in jrows)
                {
                    row.BeginEdit();
                    row.Cells["IsExistOnServer"].Value = 0;
                    row.EndEdit();
                }

                foreach (string s in collection)
                {
                    this.updateRowOnGrid(s);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        #endregion


        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                e.Row.Cells["MucDich"].Text = Company.KDT.SHARE.Components.DuLieuChuan.MucDichSuDung.GetName(e.Row.Cells["MucDich"].Value.ToString());
                switch (e.Row.Cells["LoaiNPL"].Text)
                {
                    case "1":
                        e.Row.Cells["LoaiNPL"].Text = "Nguyên phụ liệu";
                        break;
                    case "3":
                        e.Row.Cells["LoaiNPL"].Text = "Thiết bị";
                        break;
                    case "4":
                        e.Row.Cells["LoaiNPL"].Text = "Hãng mẫu";
                        break;
                    case "2":
                        e.Row.Cells["LoaiNPL"].Text = "Sản phẩm";
                        break;
                    default:
                        break;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                #region Comment Code 
                //GridEXSelectedItemCollection items = dgList.SelectedItems;
                //foreach (GridEXSelectedItem i in items)
                //{

                //    if (e.Row.RowType == RowType.Record)
                //    {
                //        NguyenPhuLieuEditForm_CX f = new NguyenPhuLieuEditForm_CX();
                //        f.NPLDetail = (HangDuaVao)e.Row.DataRow;
                //        f.OpenType = OpenFormType.Edit;
                //        f.nplDangKy = nplDangKy;
                //        f.ShowDialog(this);
                //        if (f.NPLDetail != null)
                //        {
                //            this.nplDangKy.DanhSachHangDuaVao.Insert(i.Position, f.NPLDetail);
                //        }

                //    }
                //}
                #endregion
                if (e.Row.RowType == RowType.Record)
                {
                    NPLDetail = new HangDuaVao();
                    NPLDetail = (HangDuaVao)e.Row.DataRow;
                    txtMa.Text = this.NPLDetail.Ma;
                    txtTen.Text = this.NPLDetail.Ten;
                    txtMaHS.Text = this.NPLDetail.MaHS;
                    cbDonViTinh.SelectedValue = this.NPLDetail.DVT_ID;
                    cmbMucDich.SelectedValue = this.NPLDetail.MucDich;
                    cmbLoaiHang.SelectedValue = this.NPLDetail.LoaiNPL;
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdSend":
                    this.SendV5(false);
                    break;
                case "cmdEdit":
                    this.SendV5(false);
                    break;
                case "cmdAddExcel":
                    this.AddNguyenPhuLieuFromExcel();
                    break;
                case "cmdCancel":
                    if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN KHAI BÁO HỦY NGUYÊN PHỤ LIỆU NÀY ĐẾN HQ KHÔNG ? ", true) == "Yes")
                    {
                        nplDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        nplDangKy.Update();
                        SetCommandStatus();
                        this.SendV5(true);
                    }
                    break;
                case "XacNhan":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    this.FeedBackV5();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdSuaNPL":
                    this.Edit();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_CX_HangDuaVaoDangKy", "", Convert.ToInt32(nplDangKy.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (this.nplDangKy.SoTiepNhan == 0) return;
                Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
                phieuTN.phieu = "NGUYÊN PHỤ LIỆU";
                phieuTN.soTN = this.nplDangKy.SoTiepNhan.ToString();
                phieuTN.ngayTN = this.nplDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
                phieuTN.maHaiQuan = nplDangKy.MaHaiQuan;
                phieuTN.BindReport();
                phieuTN.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void AddNguyenPhuLieuFromExcel()
        {
            try
            {
                HangDuaVaoReadExcelForm f = new HangDuaVaoReadExcelForm();
                f.NPLCollection = this.nplDangKy.DanhSachHangDuaVao;
                f.nplDangky = nplDangKy;
                f.ShowDialog(this);
                this.nplDangKy.DanhSachHangDuaVao = f.NPLCollection;
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void XoaNguyenPhuLieu(GridEXSelectedItemCollection items, List<HangDuaVao> nguyenphulieus)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            List<HangDuaVao> npls = new List<HangDuaVao>();
            List<GridEXSelectedItem> itemRemove = new List<GridEXSelectedItem>();
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangDuaVao npl = (HangDuaVao)i.GetRow().DataRow;
                    npls.Add(npl);
                }
            }
            try
            {
                if (ShowMessage(" DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    foreach (HangDuaVao item in npls)
                    {
                        if (item.ID > 0)
                        {
                            try
                            {
                                // XÓA NPL ĐÃ ĐĂNG KÝ
                                Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                NPL.Ma = item.Ma;
                                NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                NPL.Delete();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                            item.Delete();   
                        }
                        nguyenphulieus.Remove(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Delete()
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                XoaNguyenPhuLieu(items, nplDangKy.DanhSachHangDuaVao);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            Delete();
        }
        #region EDIT DUYDP 07/09/2017
        private void SendV5(bool IsCancel)
        {

            if (nplDangKy.ID == 0)
            {
                this.ShowMessage("DOANH NGHIỆP HÃY LƯU THÔNG TIN TRƯỚC KHI KHAI BÁO", false);
                return;
            }
            try
            {
                if (nplDangKy.DanhSachHangDuaVao.Count == 0)
                {
                    MLMessages("DANH SÁCH NGUYÊN PHỤ LIỆU RỖNG.\nKHÔNG THỂ THỰC HIỆN KHAI BÁO THÔNG TIN ĐẾN HẢI QUAN.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }
                nplDangKy.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.SXXK_SanPham npl = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_DNCX_HangDuaVao(nplDangKy, "", 561, nplDangKy.TrangThaiXuLy == 5 ? true : false,IsCancel, GlobalSettings.TEN_DON_VI);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = nplDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(nplDangKy.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(nplDangKy.MaHaiQuan).Trim() : nplDangKy.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.DNCX_HANG_VAO,
                                    Function = npl.Function,
                                    Reference = nplDangKy.GUIDSTR,
                                }
                                ,
                                npl);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoNguyenPhuLieu);
                    XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdAdd.Enabled = cmdSave.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    btnDelete.Enabled = false;
                    MsgSend sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
                    sendXML.master_id = nplDangKy.ID;
                    sendXML.func = 1;
                   // sendXML.msg = msgSend;
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            try
                            {
                                // XÓA NPL ĐÃ ĐĂNG KÝ
                                foreach (HangDuaVao item in nplDangKy.DanhSachHangDuaVao)
                                {
                                    Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                    NPL.Ma = item.Ma;
                                    NPL.MaHaiQuan = nplDangKy.MaHaiQuan;
                                    NPL.MaDoanhNghiep = nplDangKy.MaDoanhNghiep;
                                    NPL.Delete();   
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        else
                        {
                            Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.TransgferHDVToSXXK(nplDangKy);
                        }
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        FeedBackV5();
                        SetCommandStatus();
                    }
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);

            }


            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.HangDuaVaoCX(nplDangKy, ref msgInfor, e);
        }
        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
            sendXML.master_id = nplDangKy.ID;
            if (!sendXML.Load())
            {
                ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = nplDangKy.GUIDSTR;

                Company.KDT.SHARE.Components.SubjectBase subjectBase = new Company.KDT.SHARE.Components.SubjectBase()
                {
                    Issuer = Company.KDT.SHARE.Components.DeclarationIssuer.DNCX_HANG_VAO,
                    Reference = reference,
                    Function = Company.KDT.SHARE.Components.DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.DNCX_HANG_VAO,
                };
                subjectBase.Type = DeclarationIssuer.DNCX_HANG_VAO;
                ObjectSend msgSend = new ObjectSend(
                                            new Company.KDT.SHARE.Components.NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = nplDangKy.MaDoanhNghiep
                                            },
                                              new Company.KDT.SHARE.Components.NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(nplDangKy.MaHaiQuan.Trim()),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(nplDangKy.MaHaiQuan).Trim() : nplDangKy.MaHaiQuan
                                                  //Identity = nplDangKy.MaHaiQuan.Trim()
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            try
                            {
                                // XÓA NPL ĐÃ ĐĂNG KÝ
                                foreach (HangDuaVao item in nplDangKy.DanhSachHangDuaVao)
                                {
                                    Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                    NPL.Ma = item.Ma;
                                    NPL.MaHaiQuan = nplDangKy.MaHaiQuan;
                                    NPL.MaDoanhNghiep = nplDangKy.MaDoanhNghiep;
                                    NPL.Delete();
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        else
                        {
                            Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.TransgferHDVToSXXK(nplDangKy);
                        }
                        nplDangKy.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        nplDangKy.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                    }
                }

            }
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = nplDangKy.ID;
                form.DeclarationIssuer = DeclarationIssuer.DNCX_HANG_VAO;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        #endregion

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, "MÃ SẢN PHẨM", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, "TÊN SẢN PHẨM", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHS, errorProvider, "MÃ SỐ HÀNG HÓA", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbDonViTinh, errorProvider, "ĐƠN VỊ TÍNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cmbMucDich, errorProvider, "MỤC ĐÍCH SỬ DỤNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cmbLoaiHang, errorProvider, "LOẠI HÀNG ĐƯA VÀO", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if(!ValidateForm(false))
                return;
            if (checkNPLExit(txtMa.Text.Trim()))
            {
                MLMessages("NGUYÊN PHỤ LIỆU NÀY ĐÃ ĐƯỢC KHAI BÁO TRÊN LƯỚI.", "MSG_PUB10", "", false);
                return;
            }
            this.NPLDetail = new HangDuaVao();
            this.NPLDetail.Ma = txtMa.Text.Trim();
            this.NPLDetail.Ten = txtTen.Text.Trim();
            this.NPLDetail.MaHS = txtMaHS.Text.Trim();
            this.NPLDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            this.NPLDetail.MucDich = cmbMucDich.SelectedItem == null ? "01" : cmbMucDich.SelectedValue.ToString();
            this.NPLDetail.LoaiNPL = cmbLoaiHang.SelectedItem == null ? "1" : cmbLoaiHang.SelectedValue.ToString();
            if (!isEdit || !isCancel)
            {
                if (isAdd)
                    nplDangKy.DanhSachHangDuaVao.Add(NPLDetail);
            }
            else
            {
                Company.BLL.SXXK.NguyenPhuLieuCollection NPLCollection = Company.BLL.SXXK.NguyenPhuLieu.SelectCollectionDynamicBy(" MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'", "");
                foreach (Company.BLL.SXXK.NguyenPhuLieu item in NPLCollection)
                {
                    if (item.Ma == NPLDetail.Ma)
                    {
                        isAdd = false; 
                        errorProvider.SetError(txtMa, "NPL NÀY CHƯA ĐƯỢC ĐĂNG KÝ CHỈ NHỮNG NPL ĐÃ ĐĂNG KÝ MỚI ĐƯỢC PHÉP KHAI BÁO SỬA HOẶC HỦY .");
                        break;
                    }
                }
                if (isAdd)
                    nplDangKy.DanhSachHangDuaVao.Add(NPLDetail);
            }
            isAdd = true;
            BindData();
            txtMa.Text = String.Empty;
            txtTen.Text = String.Empty;
        }      
        private bool checkNPLExit(string maNPL)
        {
            if (nplDangKy.DanhSachHangDuaVao != null)
            {
                foreach (HangDuaVao npl in nplDangKy.DanhSachHangDuaVao)
                {
                    if (npl.Ma == maNPL) return true;
                }

            }
            return false;
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            NPLDetail = new HangDuaVao();
                            NPLDetail = (HangDuaVao)i.GetRow().DataRow;
                            txtMa.Text = this.NPLDetail.Ma;
                            txtTen.Text = this.NPLDetail.Ten;
                            txtMaHS.Text = this.NPLDetail.MaHS;
                            cbDonViTinh.SelectedValue = this.NPLDetail.DVT_ID;
                            cmbMucDich.SelectedValue = this.NPLDetail.MucDich;
                            cmbLoaiHang.SelectedValue = this.NPLDetail.LoaiNPL;
                            isAdd = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.isBrowser = true ;
                this.NPLRegistedForm_CX.ShowDialog(this);
                bool isExits = false;
                if (this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    if (nplDangKy.DanhSachHangDuaVao.Count >= 1)
                    {
                        isExits = false;
                        foreach (HangDuaVao item in nplDangKy.DanhSachHangDuaVao)
                        {
                            if (this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma == item.Ma)
                            {
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            txtMa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                            txtTen.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                            cmbMucDich.SelectedValue = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MucDich;
                            cmbLoaiHang.SelectedValue = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.LoaiNPL;
                        }
                    }
                    else
                    {
                        txtMa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                        txtTen.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                        cmbMucDich.SelectedValue = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MucDich;
                        cmbLoaiHang.SelectedValue = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.LoaiNPL;
                    }
                }
                else if (this.NPLRegistedForm_CX.NPLCollectionSelected.Count >= 1)
                {
                    List<HangDuaVao> NPLSelectCollection = new List<HangDuaVao>();
                    isExits = false;
                    foreach (HangDuaVao item in NPLRegistedForm_CX.NPLCollectionSelected)
                    {
                        if (nplDangKy.DanhSachHangDuaVao.Count >= 1)
                        {
                            isExits = false;
                            foreach (HangDuaVao ite in nplDangKy.DanhSachHangDuaVao)
                            {
                                if (item.Ma == ite.Ma)
                                {
                                    isExits = true;
                                    break;
                                }
                            }
                            if (!isExits)
                            {
                                this.NPLDetail = new HangDuaVao();
                                this.NPLDetail.Ma = txtMa.Text.Trim();
                                this.NPLDetail.Ten = txtTen.Text.Trim();
                                this.NPLDetail.MaHS = txtMaHS.Text.Trim();
                                this.NPLDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                                this.NPLDetail.MucDich = cmbMucDich.SelectedItem == null ? "01" : cmbMucDich.SelectedValue.ToString();
                                this.NPLDetail.LoaiNPL = cmbLoaiHang.SelectedItem == null ? "1" : cmbLoaiHang.SelectedValue.ToString();
                                NPLSelectCollection.Add(NPLDetail);
                            }
                        }
                        else
                        {
                            this.NPLDetail = new HangDuaVao();
                            this.NPLDetail.Ma = txtMa.Text.Trim();
                            this.NPLDetail.Ten = txtTen.Text.Trim();
                            this.NPLDetail.MaHS = txtMaHS.Text.Trim();
                            this.NPLDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                            this.NPLDetail.MucDich = cmbMucDich.SelectedItem == null ? "01" : cmbMucDich.SelectedValue.ToString();
                            this.NPLDetail.LoaiNPL = cmbLoaiHang.SelectedItem == null ? "1" : cmbLoaiHang.SelectedValue.ToString();
                            nplDangKy.DanhSachHangDuaVao.Add(NPLDetail);
                        }
                    }
                    foreach (HangDuaVao item in NPLSelectCollection)
                    {
                        nplDangKy.DanhSachHangDuaVao.Add(item);
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {
            try
            {
                //Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                //npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                //npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //npl.Ma = txtMa.Text;
                //if (npl.Load())
                //{
                //    txtMa.Text = npl.Ma;
                //    cbDonViTinh.SelectedValue = DVT_VNACC(npl.DVT_ID.PadRight(3));
                //    txtTen.Text = npl.Ten;
                //    txtMaHS.Text = npl.MaHS;
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                //npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                //npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //npl.Ma = txtMa.Text;
                //if (npl.Load())
                //{
                //    txtMa.Text = npl.Ma;
                //    cbDonViTinh.SelectedValue = DVT_VNACC(npl.DVT_ID.PadRight(3));
                //    txtTen.Text = npl.Ten;
                //    txtMaHS.Text = npl.MaHS;
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH NGUYÊN PHỤ LIỆU KHAI BÁO.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
