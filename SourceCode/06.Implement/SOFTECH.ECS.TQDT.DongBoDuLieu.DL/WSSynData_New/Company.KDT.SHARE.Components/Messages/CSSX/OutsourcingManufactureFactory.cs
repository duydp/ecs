﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class OutsourcingManufactureFactory
    {
        /// <summary>
        /// Tên doanh nghiệp
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Mã doanh nghiệp
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; }

        /// <summary>
        /// Địa chỉ chi nhánh
        /// </summary>
        [XmlElement("address")]
        public string Address { get; set; }


        /// Hợp đồng thuê gia công lại 
        /// </summary>
        [XmlElement("ContractDocuments")]
        public ContractDocuments ContractDocuments { get; set; }

        /// Thông tin Cơ sở sản xuất (CSSX)
        /// </summary>
        [XmlElement("ManufactureFactory")]
        public List<OurManufactureFactory> OutManufactureFactory { get; set; }
    }
}
