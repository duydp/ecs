﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.Send;
using System.Runtime.InteropServices;
using System.Threading;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;

namespace Company.Interface
{
    public partial class SendmsgVNACCFrm : BaseForm
    {
        public string msgSend;
        public ResponseVNACCS feedback;
        MessagesSend messagesSend;
        public bool result;
        public string inputMSGID;
        public SendmsgVNACCFrm(MessagesSend MessagesSend)
        {
            try
            {
                InitializeComponent();
                if (MessagesSend != null)
                {
                    this.messagesSend = MessagesSend;
                    this.msgSend = HelperVNACCS.BuildEdiMessages(MessagesSend).ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
                this.Close();
            }
            
        }

        public bool isRep { get; set; }
        public bool isSend { get; set; }

        #region     Send messages
        public string msgFeedBack;
        public void setCaption(string str)
        {
            if(this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblStatus.Text = str; }));
            }
            else
                lblStatus.Text = str;
        }
        private byte[] postData;
        public void SendMessEdi(object obj)
        {
            try
            {
        
            setCaption("Đang ngừng phản hồi tự động");
            while(GlobalVNACC.isResponding)
            {
                Thread.Sleep(500);
                GlobalVNACC.isStopRespone = true;
            }

            setCaption("đính kèm chữ ký số vào messages");
            System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = null;
             if (Company.KDT.SHARE.Components.Globals.IsSignature)
             {
                 try
                 {
                     if (!string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName))
                     {
                         x509Certificate2 = Company.KDT.SHARE.Components.Cryptography.GetStoreX509Certificate2(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName);
                     }
                     else
                     {
                         msgFeedBack = "Chưa cấu hình chữ ký số";
                         this.DialogResult = DialogResult.Cancel;
                         return;
                     }
                 }
                 catch (System.Exception ex)
                 {
                     msgFeedBack = "Lỗi load chữ ký số: " + ex.Message;
                     this.DialogResult = DialogResult.Cancel;
                     return;
                 }

             }
             else
             {
                 msgFeedBack = "Chưa cấu hình chữ ký số";
                 this.DialogResult = DialogResult.Cancel;
             }
             if (x509Certificate2 == null)
             {
                 msgFeedBack = "Không tìm thấy chữ ký số ";
                 this.DialogResult = DialogResult.Cancel;
                 return;
             }
            
            MIME mess = HelperVNACCS.GetMIME(msgSend, x509Certificate2);
            Thread.Sleep(500);
            setCaption("Đóng gói messages VNACCS EDI");
            string boundary = mess.boundary;
            //boundary = mime.Boundary;
            //mess = mime.GetMime();
            //mess = mime.GetEntireBody();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(GlobalVNACC.Url);
   
            if (string.IsNullOrEmpty(boundary))
            {
                msgFeedBack = "Boundary bị rỗng";
                this.DialogResult = DialogResult.Cancel;
                return;
            }
            postData = UTF8Encoding.UTF8.GetBytes(mess.GetMessages().ToString());
            boundary = boundary.Substring(2, boundary.Length - 2);
            httpWebRequest.ContentType = "multipart/signed; micalg = \"sha1\";boundary=\"" + boundary + "\";protocol=\"application/x-pkcs7-signature\"";
            httpWebRequest.Method = "POST";
            httpWebRequest.KeepAlive = false;
            httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
            httpWebRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
            httpWebRequest.UserAgent = "SOFTECH";
            httpWebRequest.ProtocolVersion = HttpVersion.Version10;
            httpWebRequest.ContentLength = postData.Length;
            httpWebRequest.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), httpWebRequest);
            //LoadingProcess();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                msgFeedBack = ex.Message;
                this.DialogResult = DialogResult.Cancel;
            }
        }
        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                setCaption("Đang gửi dữ liệu...");
                Thread.Sleep(500);
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
                
                // End the operation
                Stream postStream = request.EndGetRequestStream(asynchronousResult);

                //Console.WriteLine("Please enter the input data to be posted:");
                //string postData = Console.ReadLine();

                // Convert the string into a byte array. 
                // byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Write to the request stream.
                postStream.Write(postData, 0, postData.Length);
                postStream.Close();

                // Start the asynchronous operation to get the response
                request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
            }
            catch (Exception e)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate 
                        {
                            this.DialogResult = DialogResult.Cancel;
                            this.msgFeedBack = e.Message;
                        }));
                }
                else
                {
                    this.msgFeedBack = e.Message;
                    this.DialogResult = DialogResult.Cancel;
                }
                
            }

        }

        private void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                setCaption("Đang nhận dữ liệu...");
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

                // End the operation
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse,Encoding.UTF8);
                string responseString = streamRead.ReadToEnd();
                Company.KDT.SHARE.VNACCS.HelperVNACCS.SaveFileEdi(Application.StartupPath, new StringBuilder(responseString), "Return" + DateTime.Now.ToString("yyyyMMddhhmmss"));
//                 if (InvokeRequired)
//                 {
//                     this.Invoke(new MethodInvoker(delegate { txtResult.Text = responseString; UnLoadingProcess(); }));
//                 }
//                 else
//                     textBox2.Text = responseString;
                //Console.WriteLine(responseString);
                // Close the stream object
                streamResponse.Close();
                streamRead.Close();

                // Release the HttpWebResponse
                response.Close();
                //allDone.Set();
                //msgFeedBack = responseString;
                MsgLog.SaveMessages(responseString, this.messagesSend.Header.MaNghiepVu.GetValue().ToString() + " Return", 0, EnumThongBao.ReturnMess, "", this.messagesSend.Header.MessagesTag.GetValue().ToString(), this.messagesSend.Header.InputMessagesID.GetValue().ToString(), this.messagesSend.Header.IndexTag.GetValue().ToString());
                feedback = new ResponseVNACCS(responseString);
                msgFeedBack = feedback.GetError();
                if (feedback.Error != null && feedback.Error.Count > 0)
                {
                    this.DialogResult = DialogResult.No;
                    Helper.Controls.ErrorMessageBoxForm_VNACC.ShowMessage(messagesSend.Header.MaNghiepVu.GetValue().ToString(),feedback.Error, false, Helper.Controls.MessageType.Error);
                    
                }
                else
                {
                    if (isRep)
                    {
                        SetGif(false);
                        ucRespone1.DoWork(null);
                    }
                    else
                        this.DialogResult = DialogResult.Yes;
                }
                
            }
            catch (Exception e)
            {
                msgFeedBack = e.Message;
                this.DialogResult = DialogResult.Cancel;
            }
            

        }
        #endregion

        private void SendVNACCFrm_Load(object sender, EventArgs e)
        {
            //progressBar1.MarqueeAnimationSpeed = 50;
            ucRespone1.InputMsgID = this.inputMSGID;
            ucRespone1.bindingThongBao();
            ucRespone1.UcEvent += new EventHandler<Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs>(ucRespone1_UcEvent);
            ucRespone1.HandlerID += new EventHandler<Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs>(ucRespone1_HandlerID);
            if (isSend)
            {
                grbSend.Visible = true;
               
                ThreadPool.QueueUserWorkItem(SendMessEdi);
            }
            else
                grbSend.Visible = false;

            if (isRep)
                grbRequest.Visible = true;
            else
                grbRequest.Visible = false;
            if (!grbSend.Visible)
            {
                grbRequest.Dock = DockStyle.Fill;
                ControlBox = true;
            }
            else if (!grbRequest.Visible)
            {
                this.Size = grbSend.Size;
                this.Height = grbSend.Size.Height + 40;
                
                grbSend.Dock = DockStyle.Fill;
            }
            SetGif(isSend);
            
        }
        void ucRespone1_HandlerID(object sender, Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs e)
        {
            ProcessReport.ShowReport(Convert.ToInt64(e.Data), e.Message);
        }
        void ucRespone1_UcEvent(object sender, Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs e)
        {
            setCaption(e.Message);
            if (e.isFinish)
                setFormClose(e.isError);
            result = !e.isError;
        }
        private void SetGif(bool send)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate 
                    {
                        pbSend.Visible = send;
                        pbRequest.Visible = !send;
                    }));
            }
            else
            {
                pbSend.Visible = send;
                pbRequest.Visible = !send;
            }
        }
        private void setFormClose(bool isError)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                    {
                        this.ControlBox = true;
                        if (isError)
                        {
                            pbRequest.Visible = false;
                            pbSend.Visible = false;
                        }
                        else
                        {
                            grbSend.Visible = false;
                            grbRequest.Dock = DockStyle.Fill;
                        }
                        ControlBox = true;
                    }
                ));
            }
        }
        
    }
   
}
