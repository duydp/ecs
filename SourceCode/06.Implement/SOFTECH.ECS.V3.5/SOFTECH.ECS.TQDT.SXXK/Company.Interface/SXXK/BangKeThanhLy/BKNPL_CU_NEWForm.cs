using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Logger;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BKNPL_CU_NEWForm : BaseForm
    {
        //public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        private int lanThanhLy = 0;
        public BKNPL_CU_NEWForm(int lanthanhly)
        {
            InitializeComponent();
            this.lanThanhLy = lanthanhly;
        }
        private DataTable GetDataSource()
        {
            try
            {
                string sql = "SELECT t_KDT_SXXK_BCXuatNhapTon.MaSP,t_KDT_SXXK_BCXuatNhapTon.MaNPL,t_KDT_SXXK_BCXuatNhapTon.MaLoaiHinhNhap,SUM(LuongNPLSuDung) AS LuongTuCungUng,t_KDT_SXXK_BCXuatNhapTon.TenDVT_NPL,t_SXXK_DinhMucMaHang.Invoid_HD,t_KDT_SXXK_BCXuatNhapTon.NgayDangKyNhap AS NgayChungTu  FROM dbo.t_KDT_SXXK_BCXuatNhapTon JOIN t_SXXK_DinhMucMaHang ON t_SXXK_DinhMucMaHang.MaSP = t_KDT_SXXK_BCXuatNhapTon.MaSP AND t_SXXK_DinhMucMaHang.MaNPL = t_KDT_SXXK_BCXuatNhapTon.MaNPL   WHERE t_KDT_SXXK_BCXuatNhapTon.MaLoaiHinhNhap LIKE 'NPLCU' AND t_KDT_SXXK_BCXuatNhapTon.LanThanhLy = {0} GROUP BY t_KDT_SXXK_BCXuatNhapTon.MaSP,t_KDT_SXXK_BCXuatNhapTon.MaNPL,MaLoaiHinhNhap,TenDVT_NPL,NgayDangKyNhap,Invoid_HD";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(string.Format(sql,lanThanhLy));
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        

        private void BKNPL_CU_NEWForm_Load(object sender, EventArgs e)
        {
            dgList2.DataSource = GetDataSource();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}