﻿using System;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.KD.BLL.KDT.SXXK;
using System.Xml.Serialization;
using System.Xml;
using System.Data;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface
{
    public partial class ToKhaiMauDichManageForm : BaseForm
    {
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        private ToKhaiMauDichCollection tmpCollection = new ToKhaiMauDichCollection();
        private static long tkmdID = 0;
        private ToKhaiMauDich TKMD = null;
        public string nhomLoaiHinh = "";
        public ToKhaiMauDichManageForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        //Lypt create date 20/01/2010
        private void setDataToCboPhanLuong()
        {
            DataTable dtPL = new DataTable();
            dtPL.Columns.Add("ID", typeof(System.String));
            dtPL.Columns.Add("GhiChu", typeof(System.String));
            DataRow drNull = dtPL.NewRow();
            drNull["ID"] = -1;
            drNull["GhiChu"] = "--Tất cả--";
            dtPL.Rows.Add(drNull);
            for (int i = 1; i <= 3; i++)
            {
                DataRow dr = dtPL.NewRow();
                dr["ID"] = i.ToString();
                if (i == 1)
                    dr["GhiChu"] = "Luồng xanh";
                if (i == 2)
                    dr["GhiChu"] = "Luồng vàng";
                if (i == 3)
                    dr["GhiChu"] = "Luồng đỏ";
                dtPL.Rows.Add(dr);
            }
            cboPhanLuong.DataSource = dtPL;
            cboPhanLuong.DisplayMember = dtPL.Columns["GhiChu"].ToString();
            cboPhanLuong.ValueMember = dtPL.Columns["ID"].ToString();

        }
        //-----------------------------------------------------------------------------------------
        private void setCommandStatus()
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            DongBoDuLieu.Enabled = InheritableBoolean.True;
            cmdCancel.Enabled = InheritableBoolean.False;
            if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSingleDownload.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = InheritableBoolean.True;
                cmdSend.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                //print.Enabled = false;
                cmdXuatDuLieuChoPhongKhai.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = true;
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = false;
                btnXoa.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.False;
                cmdXuatDuLieuChoPhongKhai.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                mnuCSDaDuyet.Enabled = false;
                mnuSuaToKhai.Enabled = true;
                mnuHuyToKhai.Enabled = true;
                //print.Enabled = true;          
                btnXoa.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.False;
                cmdXuatDuLieuChoPhongKhai.Enabled = InheritableBoolean.True;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = true;
                //print.Enabled = false;
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = false;
                btnXoa.Enabled = true;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = InheritableBoolean.True;
                cmdXuatDuLieuChoPhongKhai.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                mnuCSDaDuyet.Enabled = false;
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = false;
                btnXoa.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
            {
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = false;
                btnXoa.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
            {
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = true;
                btnXoa.Enabled = false;
            }
        }

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                txtNamTiepNhan.Text = string.Empty;
                txtNamTiepNhan.Value = 0;
                txtNamTiepNhan.Enabled = false;
                txtSoTiepNhan.Value = 0;
                txtSoTiepNhan.Text = string.Empty;
                txtSoTiepNhan.Enabled = false;
            }
            else
            {
                txtNamTiepNhan.Value = DateTime.Today.Year;
                txtNamTiepNhan.Enabled = true;
                txtSoTiepNhan.Enabled = true;
            }
            this.search();
        }

        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                cbStatus.SelectedIndex = 0;
                this.setDataToCboPhanLuong();
                cboPhanLuong.SelectedIndex = 0;
                if (MainForm.versionHD == 0)
                {
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
                    {
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        this.dgList.ContextMenuStrip = null;
                    }
                }
                this.search();
                setCommandStatus();
                saveFileDialog1.InitialDirectory = Application.StartupPath;
                openFileDialog1.InitialDirectory = Application.StartupPath;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {

        }
        private ToKhaiMauDich getTKMDByID(long id)
        {
            foreach (ToKhaiMauDich tk in this.tkmdCollection)
            {
                if (tk.ID == id) return tk;
            }
            return null;
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
                {
                    //long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    tkmdID = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                    f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
                    f.TKMD = this.getTKMDByID(tkmdID);

                    f.TKMD.LoadChungTuHaiQuan();

                    int tt = f.TKMD.TrangThaiXuLy;
                    f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                    f.ShowDialog();
                    if (tt != f.TKMD.TrangThaiXuLy)
                        search();
                    try
                    {
                        f.Dispose();
                    }
                    catch { }
                }
                else
                {
                    //long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    tkmdID = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                    f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
                    f.TKMD = this.getTKMDByID(tkmdID);

                    f.TKMD.LoadChungTuHaiQuan();

                    f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                    f.ShowDialog();
                    try
                    {
                        f.Dispose();
                    }
                    catch { }
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.

            string where = "1 = 1";
            where += string.Format(" AND MaHaiQuan = '{0}' and MaDoanhNghiep='{1}'", ctrDonViHaiQuan.Ma, GlobalSettings.MA_DON_VI);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan = " + txtSoTiepNhan.Value;
            }

            if (Convert.ToInt32(cbStatus.SelectedValue) != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                }
            }
            if (cbStatus.SelectedValue != null)
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;
            if (cboPhanLuong.SelectedValue != null && cboPhanLuong.SelectedIndex != 0)
                where += " AND PhanLuong=" + cboPhanLuong.SelectedValue;
            //if (this.nhomLoaiHinh != "") where += " AND MaLoaiHinh LIKE '%" + this.nhomLoaiHinh + "%'";
            // Thực hiện tìm kiếm.            
            this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.tkmdCollection;

            this.setCommandStatus();

            //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetName(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                if (e.Row.Cells["NgayTiepNhan"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }

                if (e.Row.Cells["NgayDangKy"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayDangKy"].Text = "";
                }

                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Chưa khai báo", "Not declared yet ");
                        break;
                    case 0:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ duyệt", " Wait to approve");
                        break;
                    case 1:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã duyệt", "Aprroved ");
                        break;
                    case 2:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Không phê duyệt", "Not approved");
                        break;
                    case 5:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Sửa tờ khai", "Edit");
                        break;
                }
                if (e.Row.Cells["PhanLuong"].Value == "1")
                    e.Row.Cells["PhanLuong"].Text = "Luồng Xanh";
                else if (e.Row.Cells["PhanLuong"].Value == "2")
                    e.Row.Cells["PhanLuong"].Text = "Luồng Vàng";
                else if (e.Row.Cells["PhanLuong"].Value == "3")
                    e.Row.Cells["PhanLuong"].Text = "Luồng Đỏ";
                else
                    e.Row.Cells["PhanLuong"].Text = "Chưa phân luồng";


            }
        }

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "SaoChep":
                    this.SaoChepToKhaiMD();
                    break;
                case "SaoChepALL":
                    this.SaoChepALLHang();
                    break;
                case "SaoChepToKhaiHang":
                    this.SaoChepToKhaiMD();
                    break;
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "cmdXuatDuLieuChoPhongKhai":
                    XuatDuLieuChoPhongKhai();
                    break;
                case "cmdCSDaDuyet":
                    ChuyenTrangThai();
                    break;
            }
        }

        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        tkmdColl.Add((ToKhaiMauDich)grItem.GetRow().DataRow);
                    }
                }

                for (int i = 0; i < tkmdColl.Count; i++)
                {
                    string msg = "Bạn có muốn chuyển trạng thái của tờ khai được chọn sang đã duyệt không?";
                    msg += "\n\nSố thứ tự tờ khai: " + tkmdColl[i].ID.ToString();
                    msg += "\n----------------------";
                    tkmdColl[i].LoadHMDCollection();
                    msg += "\nCó " + tkmdColl[i].HMDCollection.Count.ToString() + " sản phẩm đăng ký";
                    if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                    {
                        ChuyenTrangThaiTK obj = new ChuyenTrangThaiTK(tkmdColl[i]);
                        obj.ShowDialog();
                    }
                }
                this.search();
            }
            else
            {
                MLMessages("Không có dữ liệu được chọn!", "MSG_WRN11", "", false);
            }
        }

        private void XuatDuLieuChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                MLMessages("Chưa chọn danh sách tờ khai", "MSG_WRN11", "", false);
                return;
            }
            try
            {
                ToKhaiMauDichCollection col = new ToKhaiMauDichCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sotokhai = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmdSelected = (ToKhaiMauDich)i.GetRow().DataRow;
                            tkmdSelected.LoadHMDCollection();
                            col.Add(tkmdSelected);
                            sotokhai++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    MLMessages("Xuất ra file thành công " + sotokhai + " tờ khai.", "MSG_EXC05", sotokhai.ToString(), false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private string checkDataImport(ToKhaiMauDichCollection collection)
        {
            string st = "";
            foreach (ToKhaiMauDich tkmd in collection)
            {
                ToKhaiMauDich tkmdInDatabase = new ToKhaiMauDich();
                tkmdInDatabase.ID = (tkmd.ID);
                tkmdInDatabase.Load();
                if (tkmdInDatabase != null)
                {
                    if (tkmdInDatabase.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        st += "Danh sách có ID=" + tkmd.ID + " đã được duyệt.\n";
                    }
                    else
                    {
                        tmpCollection.Add(tkmd);
                    }
                }
                else
                {
                    if (tkmd.ID > 0)
                        tkmd.ID = 0;
                    tmpCollection.Add(tkmd);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                tmpCollection.Clear();
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    ToKhaiMauDichCollection tkmdCollection = (ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(tkmdCollection);
                    if (st != "")
                    {
                        string msg = setText("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "invalid inport value. Do you want to continue and skip approval information");
                        if (ShowMessage(msg, true) == "Yes")
                        {
                            ToKhaiMauDich.DongBoDuLieuPhongKhai(tmpCollection);
                            MLMessages("Import thành công", "MSG_WRN34", "", false);
                        }
                    }
                    else
                    {
                        ToKhaiMauDich.DongBoDuLieuPhongKhai(tmpCollection);
                        MLMessages("Import thành công", "MSG_WRN34", "", false);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                MLMessages("Chưa chọn danh sách tờ khai", "MSG_WRN11", "", false);
                return;
            }
            try
            {
                ToKhaiMauDichCollection col = new ToKhaiMauDichCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmdSelected = (ToKhaiMauDich)i.GetRow().DataRow;
                            tkmdSelected.LoadHMDCollection();
                            col.Add(tkmdSelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }

        }

        private void SaoChepALLHang()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            foreach (HangMauDich hmd in tkmd.HMDCollection)
            {
                hmd.ID = 0;
            }
            tkmd.ID = 0;
            tkmd.SoToKhai = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.GUIDSTR = Guid.NewGuid().ToString();
            tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
            tkmd.PhanLuong = string.Empty;

            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;
            f.TKMD.HMDCollection = tkmd.HMDCollection;
            f.Show();
        }

        private void SaoChepALLHangChungtu()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadTKTGCollection();
            tkmd.LoadToKhaiTriGiaPP23();
            tkmd.LoadCO();
            //tkmd.VanTaiDon.ContainerCollection();
            foreach (HangMauDich hmd in tkmd.HMDCollection)
            {
                hmd.ID = 0;
            }
            //foreach (HangTriGia htg in tkmd.h)
            //{
            //    htg.ID = 0;
            //}                 
            tkmd.SoToKhai = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
            tkmd.PhanLuong = string.Empty;
            tkmd.GUIDSTR = Guid.NewGuid().ToString();
            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f._bNew = false;

            f.pTKMD_ID = tkmd.ID;
            tkmd.ID = 0;
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;
            f.TKMD.HMDCollection = tkmd.HMDCollection;
            //
            //Copy CO :

            f.TKMD.ListCO = tkmd.ListCO;

            //Copy TKTG PP1 :

            f.TKMD.TKTGCollection = tkmd.TKTGCollection;

            //Copy TKTG PP23 :

            f.TKMD.TKTGPP23Collection = tkmd.TKTGPP23Collection;

            f.Show();
        }

        private void SaoChepCO()
        {

        }

        private void SaoChepToKhaiMD()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)) return;

            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.SoToKhai = 0;
            tkmd.ID = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.GUIDSTR = Guid.NewGuid().ToString();
            tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
            tkmd.PhanLuong = string.Empty;

            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;
            f.Show();
        }

        private void saoChepTK_Click(object sender, EventArgs e)
        {
            SaoChepToKhaiMD();
        }

        private void saoChepHH_Click(object sender, EventArgs e)
        {
            SaoChepALLHang();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (MLMessages("Bạn có muốn xóa các thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "TK";
                            sendXML.master_id = tkmd.ID;
                            if (sendXML.Load())
                            {
                                MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan. Bạn không thể xóa.", "MSG_STN09", Convert.ToString(i.Position), false);
                                //if (st == "Yes")
                                //    tkmd.Delete();

                                e.Cancel = true;
                                continue;
                            }
                            else
                            {
                                //Load thong tin chung tu kem
                                tkmd.LoadChungTuKem();

                                //Xoa chung tu kem
                                for (int j = 0; j < tkmd.listCTDK.Count; j++)
                                {
                                    Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.DeleteBy_TKMDID(tkmd.ID);
                                }

                                tkmd.Delete();
                            }
                        }
                    }
                }
                else
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void mnuCSDaDuyet_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangThai();
        }

        private void ToKhaiMenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;

            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();



            // To khai tri gia :
            // ToKhaiTriGia tktg = new ToKhaiTriGia();
            // tktg.ID = ((ToKhaiTriGia)dgList.GetRow().DataRow).ID;
            // tktg.Load();
            // tktg.LoadHTGCollection();
            //end to khai tri gia 

            switch (tkmd.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewForm f = new ReportViewForm();
                    f.TKMD = tkmd;
                    f.ShowDialog();
                    break;

                case "X":
                    ReportViewTKXForm f1 = new ReportViewTKXForm();
                    f1.TKMD = tkmd;
                    f1.ShowDialog();
                    break;
            }
        }

        private void PhieuTNMenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "TỜ KHAI";
            Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    ToKhaiMauDich tkDangKySelected = (ToKhaiMauDich)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = tkDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = tkDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    phieuTNForm.MaHaiQuan = tkDangKySelected.MaHaiQuan;
                    j++;
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();
        }

        private void ToKhaiA4MenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            switch (tkmd.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNA4Form f = new ReportViewTKNA4Form();
                    f.TKMD = tkmd;
                    f.ShowDialog();
                    break;

                case "X":
                    ReportViewTKXA4Form f1 = new ReportViewTKXA4Form();
                    f1.TKMD = tkmd;
                    f1.ShowDialog();
                    break;
            }
        }

        private void mnuToKhaiChungTu_Click(object sender, EventArgs e)
        {
            this.SaoChepALLHangChungtu();
        }

        private void ToKhaiHQDTMenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            HangMauDich hangmd = new HangMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            if (tkmd.MaLoaiHinh.StartsWith("N"))
            {
                ReportViewTKNTQDTForm f = new ReportViewTKNTQDTForm();
                f.TKMD = tkmd;
                f.ShowDialog();
            }
            else
            {
                ReportViewTKXTQDTForm f = new ReportViewTKXTQDTForm();
                f.TKMD = tkmd;
                f.ShowDialog();
            }
            //ReportViewTKXTQDTForm
        }

        private void cboPhanLuong_SelectedValueChanged(object sender, EventArgs e)
        {
            this.search();
        }

        private void mnuInBKContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            Company.Interface.Report.SXXK.BangkeSoContainer bkCon = new Company.Interface.Report.SXXK.BangkeSoContainer();
            bkCon.tkmd = tkmd;
            bkCon.BindReport();
            bkCon.ShowPreview();
        }

        private void mnuNhanHuongDan_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            if (tkmd == null)
                tkmd = new ToKhaiMauDich();
            if (tkmd.HUONGDAN != "")
                ShowMessageTQDT("Nội dung hướng dẫn làm thủ tục của Hải quan : " + tkmd.HUONGDAN.ToString(), false);
            else
                ShowMessageTQDT("Chưa có hướng dẫn làm thủ tục ", false);

        }

        private void mnuSuaToKhai_Click(object sender, System.EventArgs e)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;

            string msg = "Bạn có muốn chuyển trạng thái của tờ khai đã duyệt sang tờ khai sửa không?";
            msg += "\n\nSố tờ khai: " + tkmd.SoToKhai.ToString();
            msg += "\n----------------------";
            msg += "\nCó " + tkmd.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
            {
                long id = tkmd.ID;
                tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                tkmd.Update();
                ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
                f.TKMD = this.getTKMDByID(id);
                f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }

        }

        private void mnuHuyToKhai_Click(object sender, EventArgs e)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            tkmd.LoadHMDCollection();

            string msg = "Bạn có muốn hủy tờ khai đã duyệt này không?";
            msg += "\n\nSố tờ khai: " + tkmd.SoToKhai.ToString();
            msg += "\n----------------------";
            msg += "\nCó " + tkmd.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    HuyToKhaiForm f = new HuyToKhaiForm();
                    f.TKMD = tkmd;
                    f.ShowDialog();
                }
            }
            else
            {
                HuyToKhaiForm f = new HuyToKhaiForm();
                f.TKMD = tkmd;
                f.ShowDialog();
            }

        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //datlmq 06102010: Bo sung them phan kiem tra to khai khong phe duyet
            //Lay thong tin to khai mau dich duoc chon tren luoi
            foreach (ToolStripItem item in contextMenuStrip1.Items)
            {
                item.Enabled = false;
            }
            if (dgList.SelectedItems.Count > 0)
            {
                ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        ToKhaiMauDich obj = (ToKhaiMauDich)grItem.GetRow().DataRow;

                        switch (obj.TrangThaiXuLy)
                        {
                            case -1: //Chưa khai báo
                                {
                                    mnuKhaiBao.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    mnuCSDaDuyet.Enabled = true;
                                    mnuNhanDuLieu.Enabled = true;
                                    break;
                                }
                            case 0: //Chờ duyệt
                                {
                                    mnuNhanDuLieu.Enabled = true;
                                    mnuHuyKhaiBao.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    mnuCSDaDuyet.Enabled = true;
                                    break;
                                }
                            case 1: //Đã duyệt
                                {
                                    mnuNhanDuLieu.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    mnuSuaToKhai.Enabled = true;
                                    mnuHuyToKhai.Enabled = true;
                                    break;
                                }
                            case 2: //Không phê duyệt
                                {
                                    mnuKhaiBao.Enabled = (obj.SoToKhai == 0); //Nếu TK đã có SỐ TỜ KHAI => Ẩn Khai báo. Không khai báo mới lại.
                                    mnuNhanDuLieu.Enabled = true;
                                    SaoChepCha.Enabled = true;

                                    mnuSuaToKhai.Enabled = (obj.SoToKhai > 0);
                                    mnuHuyToKhai.Enabled = (obj.SoToKhai > 0);
                                    break;
                                }
                            case 11: //Chờ Hủy
                                {
                                    if(obj.SoToKhai>0)
                                    mnuHuyToKhai.Enabled = true;
                                    mnuNhanDuLieu.Enabled = true;
                                    break;
                                }
                            case 5: //Sửa tờ khai
                                {
                                    mnuKhaiBao.Enabled = true;
                                    mnuNhanDuLieu.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    break;
                                }
                            case 10://Đã hủy.
                                {
                                    mnuKhaiBao.Enabled = true;
                                    mnuNhanDuLieu.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    break;
                                }
                            case -4:
                                {
                                    mnuNhanDuLieu.Enabled = true;
                                    SaoChepCha.Enabled = true;
                                    break;
                                }
                            default:
                                break;
                        }


                        ////Hien thij menu 'Chuyen trang thai' neu to khai da co So tiep nhan.
                        ////mnuCSDaDuyet.Visible = mnuCSDaDuyet.Enabled = (obj.SoTiepNhan > 0 && obj.SoToKhai == 0);
                        //mnuCSDaDuyet.Visible = mnuCSDaDuyet.Enabled = (obj.TrangThaiXuLy == -1 || obj.TrangThaiXuLy == 0);
                        //mnuHuyKhaiBao.Enabled = false;

                        //if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                        //{
                        //    mnuHuyToKhai.Enabled = false;
                        //    mnuSuaToKhai.Enabled = false;
                        //}
                        //else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                        //{
                        //    if (obj.SoToKhai > 0)
                        //    {
                        //        mnuSuaToKhai.Enabled = true;
                        //        mnuHuyToKhai.Enabled = true;
                        //    }
                        //    else
                        //    {
                        //        mnuSuaToKhai.Enabled = false;
                        //        mnuHuyToKhai.Enabled = false;
                        //    }
                        //}
                        //else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                        //{
                        //    mnuSuaToKhai.Enabled = false;
                        //    mnuHuyToKhai.Enabled = true;
                        //}
                    }
                }
            }
        }

        //DATLMQ bổ sung cho phép khai báo ở theo dõi tờ khai 17/01/2011
        private void mnuKhaiBao_Click(object sender, EventArgs e)
        {
            if (this.ShowMessage("Bạn có muốn khai báo thông tin tờ khai này không?", true) == "Yes")
            {
                if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                {
                    SendV3();
                }
                else
                    KhaiBaoToKhai();
            }
        }

        private void KhaiBaoToKhai()
        {
            //Kiểm tra tờ khai đã được khai báo chưa: DATLMQ 17/01/2011
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = tkmdID;
            if (sendXML.Load())
            {
                MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Nhận phản hồi] để lấy thông tin phản hồi từ hải quan.", "MSG_SEN03", "", false);
                //Hiện thị menu Nhận dữ liệu: DATLMQ 17/01/2011
                mnuKhaiBao.Enabled = false;
                mnuNhanDuLieu.Enabled = true;
                return;
            }

            string password = string.Empty;
            this.Cursor = Cursors.Default;
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                //Khai báo sửa tờ khai
                bool thanhcong = false;
                //Lấy thông tin tờ khai được chọn để khai báo: DATLMQ 17/01/2011
                tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
                ToKhaiMauDich TKMD = this.getTKMDByID(tkmdID);
                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                {
                    ////Nếu HQ đã duyệt phân luồng thì phải lấy thông tin phân luồng về trước khi khai báo sửa
                    //this.ShowMessage("Kiểm tra nhận dữ liệu phân luồng về trước khi khai báo sửa.", false);
                    //this.LayPhanHoiDuyetHoacPhanLuong(password);
                    thanhcong = TKMD.WSKhaiBaoToKhaiSua(password);
                }
                else
                {
                    if (TKMD.SoToKhai != 0)
                    {
                        string msg = "Tờ khai này đã có số tờ khai, không thể khai báo mới tờ khai.\r\nBạn hãy chuyển tờ khai sang trạng thái khác.";
                        ShowMessage(msg, false);
                        return;
                    }

                    thanhcong = TKMD.WSKhaiBaoToKhai(password);
                }
                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin. Bấm nút [Nhận phản hồi] để lấy thông tin phản hồi từ hải quan.", false);
                    mnuKhaiBao.Enabled = false;
                    mnuNhanDuLieu.Enabled = true;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    mnuHuyKhaiBao.Enabled = false;
                }
                //Lưu tờ khai vào Quản lý message khai báo: DATLMQ 17/01/2011
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmdID;
                sendXML.func = 1;
                sendXML.InsertUpdate();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            ShowMessageTQDT("Lỗi kết nối", "Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan: " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            ShowMessageTQDT("Thông báo từ hệ thống ECS ", "Thông tin lỗi: " + ex.Message, false);
                        //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessageTQDT("Thông tin lỗi: " + ex.Message, false);
                            //MLMessages(ex.Message, "MSG_WRN35", "", false);
                        }
                        sendXML.Delete();
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        //DATLMQ bổ sung Nhận dữ liệu ở Theo dõi Tờ khai 18/01/2011
        private void mnuNhanDuLieu_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
            {
                FeedBackV3();
            }
            else
                LayThongTinPhanHoi();
        }

        private void LayThongTinPhanHoi()
        {
            string password = string.Empty;
            WSForm form = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                form.ShowDialog(this);
                if (!form.IsReady) return;
            }
            password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : form.txtMatKhau.Text.Trim();
            tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            ToKhaiMauDich TKMD = this.getTKMDByID(tkmdID);

            //Luu trang thai tam thoi
            int tt = TKMD.TrangThaiXuLy;

            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                this.LayPhanHoiKhaiBao(password);
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                this.LayPhanHoiDuyetHoacPhanLuong(password);
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                this.LayPhanHoiHuyKhaiBao(password);
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                this.LayPhanHoiDuyetHoacPhanLuong(password);
            }

            //Cap nhat lai trang thai
            if (tt != TKMD.TrangThaiXuLy)
                search();
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "TK";
                tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
                ToKhaiMauDich TKMD = this.getTKMDByID(tkmdID);
                sendXML.master_id = tkmdID;
                sendXML.Load();
                bool thanhcong = TKMD.WSLayPhanHoi(password);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }
                }
                else
                {
                    string message = "Đăng ký thành công.\nSố tiếp nhận: " + TKMD.SoTiepNhan.ToString();
                    //string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoThanhCong);
                    this.ShowMessage(message, false);
                }
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            ShowMessage("Không kết nối được với hệ thống Hải quan.", false);
                            return;
                        }
                        else
                        {
                            ShowMessage("Thông báo trả về từ hệ thống Hải quan:\r\n" + msg[0], false);

                            // Thông tin không hợp lệ trả về.
                            try
                            {
                                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                                ToKhaiMauDich TKMD = this.getTKMDByID(tkmdID);
                                message.ItemID = TKMD.ID;
                                message.ReferenceID = new Guid(TKMD.GUIDSTR);
                                message.MessageFrom = this.MaHaiQuan.Trim();
                                message.MessageTo = this.MaDoanhNghiep;
                                message.MessageType = Company.KDT.SHARE.Components.MessageTypes.ThongTin;
                                message.MessageFunction = Company.KDT.SHARE.Components.MessageFunctions.HuyKhaiBao;
                                message.TieuDeThongBao = Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan;
                                message.NoiDungThongBao = msg[0];
                                message.CreatedTime = DateTime.Now;
                                message.Insert();
                            }
                            catch { }
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                            sendXML.Delete();
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoiDuyetHoacPhanLuong(string password)
        {
        StartInvoke:
            MsgSend sendXML = new MsgSend();
            try
            {
                Company.KDT.SHARE.Components.Message message = new KDT.SHARE.Components.Message();
                tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
                ToKhaiMauDich TKMD = this.getTKMDByID(tkmdID);
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                bool thanhcong = TKMD.WSLayPhanHoi(password);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }
                }
                else
                {
                    string msg;
                    if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        msg = Company.KDT.SHARE.Components.Message.LayThongDiep(TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiSuaDuocDuyet);
                        if (msg == "")
                            msg = Company.KDT.SHARE.Components.Message.LayThongDiep(TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo);
                        if (TKMD.PhanLuong != "")
                        {
                            msg = Company.KDT.SHARE.Components.Message.LayThongDiep(TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocPhanLuong);
                        }
                    }
                    else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        msg = Company.KDT.SHARE.Components.Message.LayThongDiep(TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        if (TKMD.PhanLuong == "")
                        {
                            TKMD.SoTiepNhan = 0;
                            TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            TKMD.PhanLuong = "";
                            this.Update();
                        }
                        else
                        {
                            TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                            this.Update();
                        }
                    }
                    //DATLMQ bổ sung nhận Số TN từ hệ thống HQ 16/12/2010
                    else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                    {
                        //msg = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoThanhCong);
                        msg = "Đăng ký thành công!\nSố tiếp nhận: " + TKMD.SoTiepNhan.ToString();
                    }
                    else
                    {
                        msg = "Chưa có phản hồi từ hệ thống Hải quan";
                    }
                    this.ShowMessage(msg, false);
                }
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                #region FPTService
                string[] msg = ex.Message.Split('|');
                if (msg.Length == 2)
                {
                    if (msg[1] == "DOTNET_LEVEL")
                    {
                        ShowMessageTQDT("Lỗi kết nối", "Không kết nối được với hệ thống hải quan.", false);
                        return;
                    }
                    else
                    {
                        ShowMessage("Thông báo trả về từ hệ thống Hải quan:\r\n" + msg[0], false);
                        sendXML.Delete();
                        setCommandStatus();
                    }
                }
                else
                {
                    if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                    {
                        ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                        sendXML.Delete();
                        setCommandStatus();
                    }
                    else
                    {
                        GlobalSettings.PassWordDT = "";
                        ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                    }
                }
                #endregion FPTService

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoiHuyKhaiBao(string password)
        {
        StartInvoke:
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "TK";
                tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
                ToKhaiMauDich TKMD = this.getTKMDByID(tkmdID);
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                bool thanhcong = TKMD.WSLayThongBaoHuy(password);
                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }
                }
                else
                {
                    //string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoThanhCong);
                    string message = "Hủy thông tin thành công!";
                    this.ShowMessage(message, false);
                }
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            ShowMessage("Không kết nối được với hệ thống Hải quan.", false);
                            return;
                        }
                        else
                        {
                            ShowMessage("Thông báo trả về từ hệ thống Hải quan:\r\n" + msg[0], false);
                            // Thông tin không hợp lệ trả về.
                            try
                            {
                                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                                tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
                                ToKhaiMauDich TKMD = this.getTKMDByID(tkmdID);
                                message.ItemID = TKMD.ID;
                                message.ReferenceID = new Guid(TKMD.GUIDSTR);
                                message.MessageFrom = this.MaHaiQuan.Trim();
                                message.MessageTo = this.MaDoanhNghiep;
                                message.MessageType = Company.KDT.SHARE.Components.MessageTypes.ThongTin;
                                message.MessageFunction = Company.KDT.SHARE.Components.MessageFunctions.HuyKhaiBao;
                                message.TieuDeThongBao = Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan;
                                message.NoiDungThongBao = msg[0];
                                message.CreatedTime = DateTime.Now;
                                message.Insert();
                            }
                            catch { }
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void HuyKhaiBao()
        {
            try
            {
                string password = string.Empty;

                tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
                ToKhaiMauDich TKMD = this.getTKMDByID(tkmdID);

                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                bool thanhcong = TKMD.WSHuyKhaiBao(password);

                //this.LayPhanHoiKhaiBao(password);
                //LayPhanHoi(password);

                this.LayThongTinPhanHoi();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            MLMessages("Thông báo trả về từ hệ thống Hải quan: " + msg[0], "MSG_SEN09", msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            XoaToKhai();
        }

        private void XoaToKhai()
        {
            if (this.tkmdCollection.Count <= 0) return;

            try
            {
                if (MLMessages("Bạn có muốn xóa các thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "TK";
                            sendXML.master_id = tkmd.ID;
                            if (sendXML.Load())
                            {
                                MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan. Bạn không thể xóa.", "MSG_STN09", Convert.ToString(i.Position), false);
                                //if (st == "Yes")
                                //    tkmd.Delete();

                                continue;
                            }
                            else
                            {
                                //Load thong tin chung tu kem
                                tkmd.LoadChungTuKem();

                                //Xoa chung tu kem
                                for (int j = 0; j < tkmd.listCTDK.Count; j++)
                                {
                                    Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.DeleteBy_TKMDID(tkmd.ID);
                                }

                                tkmd.Delete();
                            }
                        }
                    }

                    this.search();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() == null) return;

            ToKhaiMauDich tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = tkmd.ID;
            form.ShowDialog(this);
        }

        private void mnuHuyKhaiBao_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
            {
                CancelV3();
            }
            else
                HuyKhaiBao();
        }
        #region V3
        private void SendV3()
        {
            try
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                sendXML.master_id = tkmdID;
                if (sendXML.Load())
                {
                    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Nhận phản hồi] để lấy thông tin phản hồi từ hải quan.", "MSG_SEN03", "", false);
                    mnuKhaiBao.Enabled = false;
                    mnuNhanDuLieu.Enabled = true;
                    return;
                }
                tkmdID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
                TKMD = this.getTKMDByID(tkmdID);
                TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                #region Load data
                if (this.TKMD.HMDCollection == null || this.TKMD.HMDCollection.Count == 0)
                    this.TKMD.LoadHMDCollection();

                if (this.TKMD.ChungTuTKCollection == null || this.TKMD.ChungTuTKCollection.Count == 0)
                    this.TKMD.LoadChungTuTKCollection();

                if (this.TKMD.TKTGCollection == null || this.TKMD.TKTGCollection.Count == 0)
                    this.TKMD.LoadTKTGCollection();

                if (this.TKMD.TKTGPP23Collection == null || this.TKMD.TKTGPP23Collection.Count == 0)
                    this.TKMD.LoadToKhaiTriGiaPP23();

                this.TKMD.LoadChungTuHaiQuan();

                if (this.TKMD.ListCO == null || this.TKMD.ListCO.Count == 0)
                    this.TKMD.LoadCO();
                #endregion Load data

                string msgSend = SingleMessage.SendMessageV3(TKMD);
                msgSend.XmlSaveMessage(TKMD.ID, MessageTitle.KhaiBaoToKhai);

                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                dlgSendForm.DoSend(msgSend);

                if (TKMD.TrangThaiXuLy != 2)
                {
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                    sendXML.master_id = TKMD.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();                   
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        private void FeedBackV3()
        {
            string msgSend = SingleMessage.FeedBackMessageV3(TKMD);

            SendMessageForm dlgSendForm = new SendMessageForm();
            dlgSendForm.Send += SendMessage;
            dlgSendForm.DoSend(msgSend);

        }
        private void CancelV3()
        {

            string msgSend = SingleMessage.CancelMessageV3(TKMD);
            msgSend.XmlSaveMessage(TKMD.ID, MessageTitle.HuyKhaiBaoToKhai);

            SendMessageForm dlgSendForm = new SendMessageForm();
            dlgSendForm.Send += SendMessage;
            dlgSendForm.DoSend(msgSend);


        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    FeedBackContent feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformation.Content.Text;
                    switch (feedbackContent.AdditionalInformation.Statement.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                MsgSend sendXML = new MsgSend();
                                sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                                sendXML.master_id = TKMD.ID;
                                sendXML.Delete();
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                this.ShowMessageTQDT(noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            this.ShowMessage(noidung, false);
                            //noidung += "\r\nBạn có muốn tiếp tục lấy phản hồi không?";
                            //string ret = this.ShowMessage(noidung, false);
                            //if (ret == "Yes")
                            //{
                            //    FeedBackV3();
                            //}
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {

                                string[] vals = noidung.Split('/');

                                TKMD.SoTiepNhan = long.Parse(vals[0].Trim());
                                TKMD.NamDK = int.Parse(vals[1].Trim());
                                TKMD.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance.Trim(), "yyyy-MM-dd HH:mm:ss", null);

                                noidung = string.Format("Số tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nLoại hình: {3}\r\nHải quan: {4}", new object[] {
                                    TKMD.SoTiepNhan, TKMD.NgayTiepNhan.ToString("dd-MM-yyyy HH:mm:ss"), TKMD.NamDK, TKMD.MaLoaiHinh, TKMD.MaHaiQuan });
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, noidung);
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                                {
                                    noidung = "Tờ khai được cấp số tiếp nhận\r\n" + noidung;
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                }
                                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                {
                                    noidung = "Tờ khai được cấp số chờ hủy khao báo\r\n" + noidung;
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }

                                this.ShowMessageTQDT(noidung, false);
                                break;
                            }
                        case DeclarationFunction.CAP_SO_TO_KHAI:
                            e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocCapSo, noidung);
                            break;
                        case DeclarationFunction.THONG_QUAN:
                            {
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO || TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    if (TKMD.SoTiepNhan.ToString() == feedbackContent.CustomsReference)
                                    {
                                        MsgSend sendXML = new MsgSend();
                                        sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                                        sendXML.master_id = TKMD.ID;
                                        sendXML.Delete();
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                        noidung = "Tờ khai được chấp nhận hủy khai báo\r\n" + noidung;
                                        e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.HQHuyKhaiBaoToKhai, noidung);
                                        this.ShowMessage(noidung, false);
                                    }
                                }
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocPhanLuong, noidung);
                            }
                            break;
                        default:
                            break;
                    }
                    TKMD.Update();
                    //if (this.TKMD.SoTiepNhan > 0)
                    //    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    //SetCommandStatus();
                }
                else
                {
                    //MsgSend sendXML = new MsgSend();
                    //sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                    //sendXML.master_id = TKMD.ID;
                    //sendXML.Delete();
                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }
        #endregion
    }
}