﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using System.IO;
using System.Xml;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class FrmHelpVideo : BaseForm
    {
        public FrmHelpVideo()
        {
            InitializeComponent();
        }
        public void EditHTML(string tieude, string link)
        {
            //string dinhvi_link = "[Link]";
            //string dinhvi_tieude = "[TieuDe]";
            //string dong = null; /* dòng chứa nội dung được đọc vào*/


            //string pathread = Application.StartupPath + "\\Helps\\ecs.html";
            //string pathwriter = Application.StartupPath + "\\Helps\\help.html";
            //string browserUrl = pathwriter;
            //using (StreamReader reader = new StreamReader(@"" + pathread))
            //{
            //    using (StreamWriter writer = new StreamWriter(@"" + pathwriter))
            //    {
            //        while ((dong = reader.ReadLine()) != null)
            //        {
            //            if (String.Compare(dong, dinhvi_tieude) == 0)
            //            {
            //                dong = tieude;
            //            }
            //            if (String.Compare(dong, dinhvi_link) == 0)
            //            {
            //                dong = link;
            //            }
            //            writer.WriteLine(dong);
            //        }
            //    }
            //}
            //if (!browserUrl.StartsWith("file:///"))
            //{
            //    browserUrl = "file:///" + browserUrl;
            //}
            try
            {
                webBrowser1.Navigate(new Uri(link));
            }
            catch
            {
                MessageBox.Show("Invalid Url.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public void ReadXML()
        {

            XmlDocument loadDoc = new XmlDocument();
         //   loadDoc.Load(Application.StartupPath + "\\Helps\\Help.xml");
            loadDoc.Load("http://ecs.softech.cloud/AutoUpdate/Help/Help.xml");
            //string Home = loadDoc.SelectSingleNode("/Root/home").Attributes["url"].InnerText;
            //webBrowser1.Navigate(Home);
            
            foreach (XmlNode favNodeParent in loadDoc.SelectNodes("/Root/ParentNode"))
            {


                foreach (XmlNode favNodeMain in favNodeParent)
                {
                    TreeListNode empNode = tlHelp.AppendNode(null, null);
                    if (favNodeMain.Name == "ChildNodeMain")
                    {
                        empNode.SetValue("name", favNodeMain.InnerText);
                        empNode.ImageIndex = 0;
                        empNode.SelectImageIndex = 1;
                        //empNode.StateImageIndex = 2;
                        // empNode.SetValue("value", favNodeMain["name"].InnerText);.SelectNodes("/ParentNode/ChildNode")
                    }
                    foreach (XmlNode favNode in favNodeParent)
                    {
                        if (favNode.Name != "ChildNodeMain")
                        {
                            TreeListNode empNodeChild = tlHelp.AppendNode(null, empNode);
                            empNodeChild.SetValue("name", favNode.Attributes["name"].InnerText);
                            empNodeChild.SetValue("value", favNode.Attributes["url"].InnerText);
                            empNodeChild.ImageIndex = 2;
                            empNodeChild.SelectImageIndex = 2;
                            //empNodeChild.StateImageIndex = 2;
                        }
                    }
                    break;
                }

            }
        }


     

        private void FrmHelpVideo_Load(object sender, EventArgs e)
        {
            try
            {
                ReadXML();

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }



        }

        private void tlHelp_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;


                //column1 = treeList1.FocusedNode.GetValue(1).ToString();
                string TieuDe = tlHelp.FocusedNode.GetValue(0).ToString();
                string LinkPath = tlHelp.FocusedNode.GetValue(1).ToString();

                EditHTML(TieuDe, LinkPath);

               

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

    }
}