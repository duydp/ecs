using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Windows.Forms;
using System.IO;
namespace Company.GC.BLL
{
    public class EntityBase
    {

        protected SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

      
        public void SetDabaseMoi(string nameDatabase)
        {
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameDatabase);
        }
        public static string GetPathProram()
        {
            FileInfo fileInfo = new FileInfo(Application.ExecutablePath);
            return fileInfo.DirectoryName;
        }
    }
}