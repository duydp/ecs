using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class AMC : BasicVNACC
    {
        public static int TongSoByte { get; set; }
        public PropertiesAttribute SYN { get; set; }


        public AMC()
        {
            SYN = new PropertiesAttribute(12, typeof(int));

            TongSoByte = 14;
        }

    }
    public partial class EnumGroupID
    {
    }

}
