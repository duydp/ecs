﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.GoodsItem;

namespace Company.KDT.SHARE.Components
{
    public class GoodsMeasure
    {

        /// <summary>
        ///  Lượng tạm nhập
        /// </summary>
        [XmlElement("quantityTempImport")]
        public string QuantityTempImport { get; set; }

        /// <summary>
        ///  Lượng tái xuất
        /// </summary>
        [XmlElement("quantityReExport")]
        public string QuantityReExport { get; set; }

        /// <summary>
        ///  tỉ lệ chuyển đổi
        /// </summary>
        [XmlElement("conversionRate")]
        public string ConversionRate { get; set; }

        ///// <summary>
        ///// Lượng còn lại
        ///// </summary>
        //[XmlElement("quantityExcess")]
        //public decimal QuantityExcess { get; set; }


        /// <summary>
        /// Số lượng(double) n..14,3
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }
        /// <summary>
        /// Khối lượng- trọng lượng n..14,3
        /// </summary>
        [XmlElement("grossMass")]
        public string GrossMass { get; set; }
        /// <summary>
        /// Mã đơn vị tính an..3
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set; }
        [XmlElement("tariff")]
        public string Tariff { get; set; }

        /// <summary>
        /// Tổng trị giá của hàng trong vận đơn hàng không
        /// </summary>
        [XmlElement("customsValue")]
        public string CustomsValue { get; set; }

        /// <summary>
        /// Trọng lượng tịnh 
        /// </summary>
        [XmlElement("netMass")]
        public string NetMass { get; set; }

        /// <summary>
        /// Thông tin lượng
        /// </summary>
        [XmlElement("QuantityForward")]
        public QuantityForward QuantityForward { get; set; }

        /// <summary>
        /// Trọng lượng tịnh 
        /// </summary>
        [XmlElement("unitPrice")]
        public string UnitPrice { get; set; }

        #region Dành cho thanh khoản GC
        /// <summary>
        /// Luong nhap
        /// </summary>
        [XmlElement("quantityImport")]
        public string quantityImport { get; set; }

        /// <summary>
        /// Luong Xuat 
        /// </summary>
        [XmlElement("quantityExport")]
        public string quantityExport { get; set; }


        /// <summary>
        /// Luong Cung ung
        /// </summary>
        [XmlElement("quantitySupply")]
        public string quantitySupply { get; set; }

        /// <summary>
        /// Luong Du
        /// </summary>
        [XmlElement("quantityExcess")]
        public string quantityExcess { get; set; }

        // /// <summary>
        ///// Luong Xuat tra, chuyen tiep
        ///// </summary>
        //[XmlElement("quantityReExport")]
        //public string QuantityReExport { get; set; }


        /// <summary>
        /// Thuế suất XNK
        /// </summary>
        [XmlElement("tax")]
        public string Tax { get; set; }

        [XmlElement("content")]
        public string Content { get; set; }
        #endregion

        #region Danh cho CX
        /// <summary>
        /// Số lượng tồn kho sổ sách
        /// </summary>
        [XmlElement("quantity1")]
        public string Quantity1 { get; set; }

        /// <summary>
        /// Số lượng tồn kho thực tế
        /// </summary>
        [XmlElement("quantity2")]
        public string Quantity2 { get; set; }
        #endregion
        #region Phiếu kho

        /// <summary>
        /// Số lượng dự kiến nhập
        /// </summary>
        [XmlElement("docQuantity")]
        public string DocQuantity { get; set; }

        /// <summary>
        /// Số lượng thực nhập
        /// </summary>
        [XmlElement("actualQuantity")]
        public string ActualQuantity { get; set; }

        /// <summary>
        /// Trị giá VNĐ
        /// </summary>
        [XmlElement("statisticalValue")]
        public string StatisticalValue { get; set; }

        #endregion
    }
}
