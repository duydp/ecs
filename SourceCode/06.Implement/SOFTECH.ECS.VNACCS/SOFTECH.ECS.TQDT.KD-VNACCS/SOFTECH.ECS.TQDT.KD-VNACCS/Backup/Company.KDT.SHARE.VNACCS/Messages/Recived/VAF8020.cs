using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAF8020 : BasicVNACC
    {
       
        public void LoadVAF8020(string strResult)
        {
            try
            {

                this.GetObject<VAF8020>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAF8020, false, VAF8020.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAF8020.TongSoByte);
               
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }

}
