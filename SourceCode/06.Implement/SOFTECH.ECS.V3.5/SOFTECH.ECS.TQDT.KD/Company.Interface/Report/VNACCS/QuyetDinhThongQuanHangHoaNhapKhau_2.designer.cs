namespace Company.Interface.Report.VNACCS
{
    partial class QuyetDinhThongQuanHangHoaNhapKhau_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoQuanLyNSD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoQuanLyNoiBoDN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanGhiChu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDinhKemKhaiBaoDienTu3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDinhKemKhaiBaoDienTu2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDinhKemKhaiBaoDienTu1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuThiTHHetHan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiCaNhanToChuc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaHieuPhuongThucVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiHangHoa = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoiHanTaiNhapTaiXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBoPhanXuLyToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSoHangHoaDaiDienToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoToKhaiChiaNho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoNhanhToKhaiChiaNho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioThayDoiDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayThayDoiDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl1111 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenCoQuanHaiQuanTiepNhanToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiKiemTra = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiDauTien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiTamNhapTaiXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.lblHanNopThue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThueAnHan1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTieuDe = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThueAnHan1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThueAnHan2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHanNopThue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThueAnHan2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHanNopThue3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThueAnHan3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThueAnHan3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThueAnHan4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHanNopThue4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThueAnHan4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHanNopThue5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThueAnHan5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThueAnHan5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThueAnHan6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHanNopThue6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThueAnHan6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKhoiHanhVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel100 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemTrungChuyen1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDuKienDenDDTC1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKhoiHanh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemTrungChuyen2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel110 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDuKienDenDDTC2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKhoiHanh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel113 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemTrungChuyen3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDuKienDenDDTC3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKhoiHanh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemDichVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDuKienDen = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTChiThiHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaychithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoidungchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblPhanLoaiChiThiHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.lblPhanLoaiThamTra = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoNgayMongDoi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenTruongDvHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayCapPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHoanThanhKT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayPheQuyet = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHoanThanhKiemTraBP = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioCapPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioHoanThanhKT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioPheQuyet = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioHoanThanhKiemTraBP = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHanNopThueVAT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThueAnHanVAT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThueAnHanVAT = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine3,
            this.lblSoQuanLyNSD,
            this.xrLabel33,
            this.lblSoQuanLyNoiBoDN,
            this.xrLabel29,
            this.lblPhanGhiChu,
            this.xrLabel27,
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3,
            this.xrLabel25,
            this.xrLabel23,
            this.lblSoDinhKemKhaiBaoDienTu3,
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2,
            this.xrLabel18,
            this.xrLabel17,
            this.lblSoDinhKemKhaiBaoDienTu2,
            this.xrLabel2,
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1,
            this.xrLabel11,
            this.xrLabel12,
            this.lblSoDinhKemKhaiBaoDienTu1,
            this.xrLine2});
            this.Detail.HeightF = 53.12503F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 48.00002F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // lblSoQuanLyNSD
            // 
            this.lblSoQuanLyNSD.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoQuanLyNSD.LocationFloat = new DevExpress.Utils.PointFloat(586.2867F, 38.00001F);
            this.lblSoQuanLyNSD.Name = "lblSoQuanLyNSD";
            this.lblSoQuanLyNSD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoQuanLyNSD.SizeF = new System.Drawing.SizeF(51.0033F, 10.00001F);
            this.lblSoQuanLyNSD.StylePriority.UseFont = false;
            this.lblSoQuanLyNSD.StylePriority.UseTextAlignment = false;
            this.lblSoQuanLyNSD.Text = "XXXE";
            this.lblSoQuanLyNSD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(427.0817F, 38.00001F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(159.205F, 10.00001F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "Số quản lý người sử dụng";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoQuanLyNoiBoDN
            // 
            this.lblSoQuanLyNoiBoDN.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoQuanLyNoiBoDN.LocationFloat = new DevExpress.Utils.PointFloat(209.7884F, 38.00001F);
            this.lblSoQuanLyNoiBoDN.Name = "lblSoQuanLyNoiBoDN";
            this.lblSoQuanLyNoiBoDN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoQuanLyNoiBoDN.SizeF = new System.Drawing.SizeF(176.7099F, 10.00001F);
            this.lblSoQuanLyNoiBoDN.StylePriority.UseFont = false;
            this.lblSoQuanLyNoiBoDN.StylePriority.UseTextAlignment = false;
            this.lblSoQuanLyNoiBoDN.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoQuanLyNoiBoDN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 38.00001F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(199.7883F, 10.00001F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "Số quản lý của nội bộ doanh nghiệp";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanGhiChu
            // 
            this.lblPhanGhiChu.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanGhiChu.LocationFloat = new DevExpress.Utils.PointFloat(106.88F, 12.00002F);
            this.lblPhanGhiChu.Multiline = true;
            this.lblPhanGhiChu.Name = "lblPhanGhiChu";
            this.lblPhanGhiChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanGhiChu.SizeF = new System.Drawing.SizeF(505.2F, 26F);
            this.lblPhanGhiChu.StylePriority.UseFont = false;
            this.lblPhanGhiChu.StylePriority.UseTextAlignment = false;
            this.lblPhanGhiChu.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblPhanGhiChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(9.995015F, 12.00002F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(96.88499F, 10.00001F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Phần ghi chú";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiDinhKemKhaiBaoDienTu3
            // 
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.LocationFloat = new DevExpress.Utils.PointFloat(582.91F, 0F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.Name = "lblPhanLoaiDinhKemKhaiBaoDienTu3";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.SizeF = new System.Drawing.SizeF(39.59F, 10F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.StylePriority.UseFont = false;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.Text = "XXE";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(567.5F, 0F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "3";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(622.5F, 0F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "-";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoDinhKemKhaiBaoDienTu3
            // 
            this.lblSoDinhKemKhaiBaoDienTu3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDinhKemKhaiBaoDienTu3.LocationFloat = new DevExpress.Utils.PointFloat(637.29F, 0F);
            this.lblSoDinhKemKhaiBaoDienTu3.Name = "lblSoDinhKemKhaiBaoDienTu3";
            this.lblSoDinhKemKhaiBaoDienTu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDinhKemKhaiBaoDienTu3.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblSoDinhKemKhaiBaoDienTu3.StylePriority.UseFont = false;
            this.lblSoDinhKemKhaiBaoDienTu3.StylePriority.UseTextAlignment = false;
            this.lblSoDinhKemKhaiBaoDienTu3.Text = "NNNNNNNNN1NE";
            this.lblSoDinhKemKhaiBaoDienTu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoDinhKemKhaiBaoDienTu3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblPhanLoaiDinhKemKhaiBaoDienTu2
            // 
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.LocationFloat = new DevExpress.Utils.PointFloat(387.4916F, 0F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.Name = "lblPhanLoaiDinhKemKhaiBaoDienTu2";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.SizeF = new System.Drawing.SizeF(39.59F, 10F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.StylePriority.UseFont = false;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.Text = "XXE";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(371.4983F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "2";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(427.0816F, 0F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "-";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoDinhKemKhaiBaoDienTu2
            // 
            this.lblSoDinhKemKhaiBaoDienTu2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDinhKemKhaiBaoDienTu2.LocationFloat = new DevExpress.Utils.PointFloat(441.8716F, 0F);
            this.lblSoDinhKemKhaiBaoDienTu2.Name = "lblSoDinhKemKhaiBaoDienTu2";
            this.lblSoDinhKemKhaiBaoDienTu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDinhKemKhaiBaoDienTu2.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblSoDinhKemKhaiBaoDienTu2.StylePriority.UseFont = false;
            this.lblSoDinhKemKhaiBaoDienTu2.StylePriority.UseTextAlignment = false;
            this.lblSoDinhKemKhaiBaoDienTu2.Text = "NNNNNNNNN1NE";
            this.lblSoDinhKemKhaiBaoDienTu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoDinhKemKhaiBaoDienTu2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(166.655F, 10.00001F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Số đính kèm khai báo điện tử";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiDinhKemKhaiBaoDienTu1
            // 
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.LocationFloat = new DevExpress.Utils.PointFloat(192.08F, 0F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.Name = "lblPhanLoaiDinhKemKhaiBaoDienTu1";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.SizeF = new System.Drawing.SizeF(39.59F, 10F);
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.StylePriority.UseFont = false;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.Text = "XXE";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(177.08F, 0F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "1";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(231.67F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "-";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoDinhKemKhaiBaoDienTu1
            // 
            this.lblSoDinhKemKhaiBaoDienTu1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDinhKemKhaiBaoDienTu1.LocationFloat = new DevExpress.Utils.PointFloat(246.46F, 0F);
            this.lblSoDinhKemKhaiBaoDienTu1.Name = "lblSoDinhKemKhaiBaoDienTu1";
            this.lblSoDinhKemKhaiBaoDienTu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDinhKemKhaiBaoDienTu1.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblSoDinhKemKhaiBaoDienTu1.StylePriority.UseFont = false;
            this.lblSoDinhKemKhaiBaoDienTu1.StylePriority.UseTextAlignment = false;
            this.lblSoDinhKemKhaiBaoDienTu1.Text = "NNNNNNNNN1NE";
            this.lblSoDinhKemKhaiBaoDienTu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoDinhKemKhaiBaoDienTu1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(2.000014F, 10.00001F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSoTrang,
            this.xrLabel3,
            this.xrLabel32,
            this.xrLabel31,
            this.xrLabel10,
            this.lblBieuThiTHHetHan,
            this.lblPhanLoaiCaNhanToChuc,
            this.lblMaHieuPhuongThucVanChuyen,
            this.lblMaPhanLoaiHangHoa,
            this.lblThoiHanTaiNhapTaiXuat,
            this.lblMaBoPhanXuLyToKhai,
            this.lblMaSoHangHoaDaiDienToKhai,
            this.lblTongSoToKhaiChiaNho,
            this.lblSoNhanhToKhaiChiaNho,
            this.lblGioThayDoiDangKy,
            this.lblNgayThayDoiDangKy,
            this.xrLabel21,
            this.xrLabel20,
            this.lbl1111,
            this.lblGioDangKy,
            this.xrLabel16,
            this.lblMaLoaiHinh,
            this.lblTenCoQuanHaiQuanTiepNhanToKhai,
            this.xrLabel14,
            this.lblNgayDangKy,
            this.lblMaPhanLoaiKiemTra,
            this.lblSoToKhaiDauTien,
            this.xrLabel9,
            this.lblSoToKhaiTamNhapTaiXuat,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.lblSoToKhai,
            this.xrLabel1,
            this.xrLine1});
            this.TopMargin.HeightF = 131.2916F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(700F, 18F);
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTrang.SizeF = new System.Drawing.SizeF(30F, 11F);
            this.lblSoTrang.StylePriority.UseFont = false;
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "N/N";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 44.54165F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(96.88F, 10F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Số tờ khai";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(732.9999F, 84.54167F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(13F, 10F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "-";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(532.705F, 44.54165F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(10.21F, 10F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "/";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(493.7567F, 44.54165F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "-";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblBieuThiTHHetHan
            // 
            this.lblBieuThiTHHetHan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblBieuThiTHHetHan.LocationFloat = new DevExpress.Utils.PointFloat(746F, 84.54167F);
            this.lblBieuThiTHHetHan.Name = "lblBieuThiTHHetHan";
            this.lblBieuThiTHHetHan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuThiTHHetHan.SizeF = new System.Drawing.SizeF(16F, 10F);
            this.lblBieuThiTHHetHan.StylePriority.UseFont = false;
            this.lblBieuThiTHHetHan.StylePriority.UseTextAlignment = false;
            this.lblBieuThiTHHetHan.Text = "X";
            this.lblBieuThiTHHetHan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiCaNhanToChuc
            // 
            this.lblPhanLoaiCaNhanToChuc.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiCaNhanToChuc.LocationFloat = new DevExpress.Utils.PointFloat(411.7416F, 64.54166F);
            this.lblPhanLoaiCaNhanToChuc.Name = "lblPhanLoaiCaNhanToChuc";
            this.lblPhanLoaiCaNhanToChuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiCaNhanToChuc.SizeF = new System.Drawing.SizeF(33F, 10F);
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseFont = false;
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiCaNhanToChuc.Text = "[ X ]";
            this.lblPhanLoaiCaNhanToChuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaHieuPhuongThucVanChuyen
            // 
            this.lblMaHieuPhuongThucVanChuyen.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaHieuPhuongThucVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(395.1916F, 64.54166F);
            this.lblMaHieuPhuongThucVanChuyen.Name = "lblMaHieuPhuongThucVanChuyen";
            this.lblMaHieuPhuongThucVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHieuPhuongThucVanChuyen.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseFont = false;
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblMaHieuPhuongThucVanChuyen.Text = "X";
            this.lblMaHieuPhuongThucVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiHangHoa
            // 
            this.lblMaPhanLoaiHangHoa.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaPhanLoaiHangHoa.LocationFloat = new DevExpress.Utils.PointFloat(378.6416F, 64.54166F);
            this.lblMaPhanLoaiHangHoa.Name = "lblMaPhanLoaiHangHoa";
            this.lblMaPhanLoaiHangHoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiHangHoa.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaPhanLoaiHangHoa.StylePriority.UseFont = false;
            this.lblMaPhanLoaiHangHoa.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiHangHoa.Text = "X";
            this.lblMaPhanLoaiHangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThoiHanTaiNhapTaiXuat
            // 
            this.lblThoiHanTaiNhapTaiXuat.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblThoiHanTaiNhapTaiXuat.LocationFloat = new DevExpress.Utils.PointFloat(657.4984F, 84.54167F);
            this.lblThoiHanTaiNhapTaiXuat.Name = "lblThoiHanTaiNhapTaiXuat";
            this.lblThoiHanTaiNhapTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoiHanTaiNhapTaiXuat.SizeF = new System.Drawing.SizeF(75F, 10F);
            this.lblThoiHanTaiNhapTaiXuat.StylePriority.UseFont = false;
            this.lblThoiHanTaiNhapTaiXuat.StylePriority.UseTextAlignment = false;
            this.lblThoiHanTaiNhapTaiXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaBoPhanXuLyToKhai
            // 
            this.lblMaBoPhanXuLyToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaBoPhanXuLyToKhai.LocationFloat = new DevExpress.Utils.PointFloat(657.5032F, 74.54166F);
            this.lblMaBoPhanXuLyToKhai.Name = "lblMaBoPhanXuLyToKhai";
            this.lblMaBoPhanXuLyToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBoPhanXuLyToKhai.SizeF = new System.Drawing.SizeF(23.96F, 10F);
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseFont = false;
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseTextAlignment = false;
            this.lblMaBoPhanXuLyToKhai.Text = "XE";
            this.lblMaBoPhanXuLyToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSoHangHoaDaiDienToKhai
            // 
            this.lblMaSoHangHoaDaiDienToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSoHangHoaDaiDienToKhai.LocationFloat = new DevExpress.Utils.PointFloat(657.5034F, 64.54166F);
            this.lblMaSoHangHoaDaiDienToKhai.Name = "lblMaSoHangHoaDaiDienToKhai";
            this.lblMaSoHangHoaDaiDienToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSoHangHoaDaiDienToKhai.SizeF = new System.Drawing.SizeF(41.67F, 10F);
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseFont = false;
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseTextAlignment = false;
            this.lblMaSoHangHoaDaiDienToKhai.Text = "XXXE";
            this.lblMaSoHangHoaDaiDienToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoToKhaiChiaNho
            // 
            this.lblTongSoToKhaiChiaNho.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongSoToKhaiChiaNho.LocationFloat = new DevExpress.Utils.PointFloat(542.9182F, 44.54165F);
            this.lblTongSoToKhaiChiaNho.Name = "lblTongSoToKhaiChiaNho";
            this.lblTongSoToKhaiChiaNho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoToKhaiChiaNho.SizeF = new System.Drawing.SizeF(22.08F, 10F);
            this.lblTongSoToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblTongSoToKhaiChiaNho.StylePriority.UseTextAlignment = false;
            this.lblTongSoToKhaiChiaNho.Text = "NE";
            this.lblTongSoToKhaiChiaNho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTongSoToKhaiChiaNho.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoNhanhToKhaiChiaNho
            // 
            this.lblSoNhanhToKhaiChiaNho.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoNhanhToKhaiChiaNho.LocationFloat = new DevExpress.Utils.PointFloat(508.5468F, 44.54165F);
            this.lblSoNhanhToKhaiChiaNho.Name = "lblSoNhanhToKhaiChiaNho";
            this.lblSoNhanhToKhaiChiaNho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoNhanhToKhaiChiaNho.SizeF = new System.Drawing.SizeF(24.15637F, 10F);
            this.lblSoNhanhToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblSoNhanhToKhaiChiaNho.StylePriority.UseTextAlignment = false;
            this.lblSoNhanhToKhaiChiaNho.Text = "NE";
            this.lblSoNhanhToKhaiChiaNho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoNhanhToKhaiChiaNho.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblGioThayDoiDangKy
            // 
            this.lblGioThayDoiDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioThayDoiDangKy.LocationFloat = new DevExpress.Utils.PointFloat(446.2883F, 84.54167F);
            this.lblGioThayDoiDangKy.Name = "lblGioThayDoiDangKy";
            this.lblGioThayDoiDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioThayDoiDangKy.SizeF = new System.Drawing.SizeF(60F, 10F);
            this.lblGioThayDoiDangKy.StylePriority.UseFont = false;
            this.lblGioThayDoiDangKy.StylePriority.UseTextAlignment = false;
            this.lblGioThayDoiDangKy.Text = "hh:mm:ss";
            this.lblGioThayDoiDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayThayDoiDangKy
            // 
            this.lblNgayThayDoiDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayThayDoiDangKy.LocationFloat = new DevExpress.Utils.PointFloat(366.0849F, 84.54167F);
            this.lblNgayThayDoiDangKy.Name = "lblNgayThayDoiDangKy";
            this.lblNgayThayDoiDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayThayDoiDangKy.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblNgayThayDoiDangKy.StylePriority.UseFont = false;
            this.lblNgayThayDoiDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayThayDoiDangKy.Text = "dd/MM/yyyy";
            this.lblNgayThayDoiDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(508.5383F, 84.54167F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(148.96F, 10F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(508.5433F, 74.54166F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(148.96F, 10F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Mã bộ phận xử lý tờ khai";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl1111
            // 
            this.lbl1111.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1111.LocationFloat = new DevExpress.Utils.PointFloat(452.2933F, 64.54166F);
            this.lbl1111.Name = "lbl1111";
            this.lbl1111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl1111.SizeF = new System.Drawing.SizeF(205.21F, 10F);
            this.lbl1111.StylePriority.UseFont = false;
            this.lbl1111.StylePriority.UseTextAlignment = false;
            this.lbl1111.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.lbl1111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioDangKy
            // 
            this.lblGioDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioDangKy.LocationFloat = new DevExpress.Utils.PointFloat(177.08F, 84.54167F);
            this.lblGioDangKy.Name = "lblGioDangKy";
            this.lblGioDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioDangKy.SizeF = new System.Drawing.SizeF(60F, 10F);
            this.lblGioDangKy.StylePriority.UseFont = false;
            this.lblGioDangKy.StylePriority.UseTextAlignment = false;
            this.lblGioDangKy.Text = "hh:mm:ss";
            this.lblGioDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(237.085F, 84.54167F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(129F, 10F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Ngày thay đổi đăng ký";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaLoaiHinh.LocationFloat = new DevExpress.Utils.PointFloat(343.3416F, 64.54166F);
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaLoaiHinh.SizeF = new System.Drawing.SizeF(35.3F, 10F);
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenCoQuanHaiQuanTiepNhanToKhai
            // 
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 74.54166F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Name = "lblTenCoQuanHaiQuanTiepNhanToKhai";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.SizeF = new System.Drawing.SizeF(102F, 10F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseFont = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseTextAlignment = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Text = "XXXXXXXXXE";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 64.54166F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(106.25F, 10F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Mã loại hình";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayDangKy.LocationFloat = new DevExpress.Utils.PointFloat(96.455F, 84.54167F);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblNgayDangKy.StylePriority.UseFont = false;
            this.lblNgayDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayDangKy.Text = "dd/MM/yyyy";
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiKiemTra
            // 
            this.lblMaPhanLoaiKiemTra.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaPhanLoaiKiemTra.LocationFloat = new DevExpress.Utils.PointFloat(152.71F, 64.54166F);
            this.lblMaPhanLoaiKiemTra.Name = "lblMaPhanLoaiKiemTra";
            this.lblMaPhanLoaiKiemTra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiKiemTra.SizeF = new System.Drawing.SizeF(39.59F, 10F);
            this.lblMaPhanLoaiKiemTra.StylePriority.UseFont = false;
            this.lblMaPhanLoaiKiemTra.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiKiemTra.Text = "XX E";
            this.lblMaPhanLoaiKiemTra.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiDauTien
            // 
            this.lblSoToKhaiDauTien.AutoWidth = true;
            this.lblSoToKhaiDauTien.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhaiDauTien.LocationFloat = new DevExpress.Utils.PointFloat(368.34F, 44.54165F);
            this.lblSoToKhaiDauTien.Name = "lblSoToKhaiDauTien";
            this.lblSoToKhaiDauTien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiDauTien.SizeF = new System.Drawing.SizeF(125.4168F, 10F);
            this.lblSoToKhaiDauTien.StylePriority.UseFont = false;
            this.lblSoToKhaiDauTien.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiDauTien.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiDauTien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoToKhaiDauTien.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel9
            // 
            this.xrLabel9.AutoWidth = true;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 44.54165F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(131.25F, 10F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Số tờ khai đầu tiên";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiTamNhapTaiXuat
            // 
            this.lblSoToKhaiTamNhapTaiXuat.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhaiTamNhapTaiXuat.LocationFloat = new DevExpress.Utils.PointFloat(237.085F, 54.54165F);
            this.lblSoToKhaiTamNhapTaiXuat.Name = "lblSoToKhaiTamNhapTaiXuat";
            this.lblSoToKhaiTamNhapTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiTamNhapTaiXuat.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseFont = false;
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiTamNhapTaiXuat.Text = "NNNNNNNNN1NE";
            this.lblSoToKhaiTamNhapTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoToKhaiTamNhapTaiXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(9.995015F, 84.54167F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(86.46F, 10F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Ngày đăng ký";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 74.54166F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(227.09F, 10F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 64.54166F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(142.71F, 10F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Mã phân loại kiểm tra";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 54.54165F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(227.085F, 10F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhai.LocationFloat = new DevExpress.Utils.PointFloat(106.88F, 44.54165F);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.SizeF = new System.Drawing.SizeF(130.2F, 10F);
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = "NNNNNNNNN1NE";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(175F, 18.5F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(409.29F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Tờ khai hàng hóa nhập khẩu (thông quan)";
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(1.409944F, 94.54168F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 57.20825F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHanNopThue1
            // 
            this.lblHanNopThue1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblHanNopThue1.LocationFloat = new DevExpress.Utils.PointFloat(190.07F, 131.2132F);
            this.lblHanNopThue1.Name = "lblHanNopThue1";
            this.lblHanNopThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanNopThue1.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblHanNopThue1.StylePriority.UseFont = false;
            this.lblHanNopThue1.StylePriority.UseTextAlignment = false;
            this.lblHanNopThue1.Text = "dd/MM/yyyy";
            this.lblHanNopThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSacThueAnHan1
            // 
            this.lblMaSacThueAnHan1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSacThueAnHan1.LocationFloat = new DevExpress.Utils.PointFloat(68.53658F, 131.2132F);
            this.lblMaSacThueAnHan1.Name = "lblMaSacThueAnHan1";
            this.lblMaSacThueAnHan1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThueAnHan1.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaSacThueAnHan1.StylePriority.UseFont = false;
            this.lblMaSacThueAnHan1.StylePriority.UseTextAlignment = false;
            this.lblMaSacThueAnHan1.Text = "X";
            this.lblMaSacThueAnHan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine5
            // 
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(1.463318F, 0F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // xrLabel78
            // 
            this.xrLabel78.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(11.45833F, 2F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(199.7883F, 10.00001F);
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "Mục thông báo của Hải quan";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(532.695F, 121.2133F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(213.2949F, 9.999985F);
            this.xrLabel79.StylePriority.UseFont = false;
            this.xrLabel79.StylePriority.UseTextAlignment = false;
            this.xrLabel79.Text = "Dành cho VAT hàng hóa đặc biệt";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(46.66154F, 108.4064F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(163.1117F, 10F);
            this.xrLabel80.StylePriority.UseFont = false;
            this.xrLabel80.StylePriority.UseTextAlignment = false;
            this.xrLabel80.Text = "Tổng số tiền thuế chậm nộp";
            this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTieuDe
            // 
            this.lblTieuDe.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTieuDe.LocationFloat = new DevExpress.Utils.PointFloat(46.66159F, 121.2133F);
            this.lblTieuDe.Name = "lblTieuDe";
            this.lblTieuDe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTieuDe.SizeF = new System.Drawing.SizeF(303.3117F, 10F);
            this.lblTieuDe.StylePriority.UseFont = false;
            this.lblTieuDe.StylePriority.UseTextAlignment = false;
            this.lblTieuDe.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWE";
            this.lblTieuDe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenSacThueAnHan1
            // 
            this.lblTenSacThueAnHan1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenSacThueAnHan1.LocationFloat = new DevExpress.Utils.PointFloat(85.08656F, 131.2132F);
            this.lblTenSacThueAnHan1.Name = "lblTenSacThueAnHan1";
            this.lblTenSacThueAnHan1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThueAnHan1.SizeF = new System.Drawing.SizeF(104.9834F, 10F);
            this.lblTenSacThueAnHan1.StylePriority.UseFont = false;
            this.lblTenSacThueAnHan1.StylePriority.UseTextAlignment = false;
            this.lblTenSacThueAnHan1.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSacThueAnHan2
            // 
            this.lblMaSacThueAnHan2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSacThueAnHan2.LocationFloat = new DevExpress.Utils.PointFloat(271.1199F, 131.2132F);
            this.lblMaSacThueAnHan2.Name = "lblMaSacThueAnHan2";
            this.lblMaSacThueAnHan2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThueAnHan2.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaSacThueAnHan2.StylePriority.UseFont = false;
            this.lblMaSacThueAnHan2.StylePriority.UseTextAlignment = false;
            this.lblMaSacThueAnHan2.Text = "X";
            this.lblMaSacThueAnHan2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHanNopThue2
            // 
            this.lblHanNopThue2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblHanNopThue2.LocationFloat = new DevExpress.Utils.PointFloat(392.6532F, 131.2132F);
            this.lblHanNopThue2.Name = "lblHanNopThue2";
            this.lblHanNopThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanNopThue2.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblHanNopThue2.StylePriority.UseFont = false;
            this.lblHanNopThue2.StylePriority.UseTextAlignment = false;
            this.lblHanNopThue2.Text = "dd/MM/yyyy";
            this.lblHanNopThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenSacThueAnHan2
            // 
            this.lblTenSacThueAnHan2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenSacThueAnHan2.LocationFloat = new DevExpress.Utils.PointFloat(287.6698F, 131.2132F);
            this.lblTenSacThueAnHan2.Name = "lblTenSacThueAnHan2";
            this.lblTenSacThueAnHan2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThueAnHan2.SizeF = new System.Drawing.SizeF(104.9834F, 10F);
            this.lblTenSacThueAnHan2.StylePriority.UseFont = false;
            this.lblTenSacThueAnHan2.StylePriority.UseTextAlignment = false;
            this.lblTenSacThueAnHan2.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHanNopThue3
            // 
            this.lblHanNopThue3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblHanNopThue3.LocationFloat = new DevExpress.Utils.PointFloat(190.07F, 141.2132F);
            this.lblHanNopThue3.Name = "lblHanNopThue3";
            this.lblHanNopThue3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanNopThue3.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblHanNopThue3.StylePriority.UseFont = false;
            this.lblHanNopThue3.StylePriority.UseTextAlignment = false;
            this.lblHanNopThue3.Text = "dd/MM/yyyy";
            this.lblHanNopThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSacThueAnHan3
            // 
            this.lblMaSacThueAnHan3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSacThueAnHan3.LocationFloat = new DevExpress.Utils.PointFloat(68.53658F, 141.2132F);
            this.lblMaSacThueAnHan3.Name = "lblMaSacThueAnHan3";
            this.lblMaSacThueAnHan3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThueAnHan3.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaSacThueAnHan3.StylePriority.UseFont = false;
            this.lblMaSacThueAnHan3.StylePriority.UseTextAlignment = false;
            this.lblMaSacThueAnHan3.Text = "X";
            this.lblMaSacThueAnHan3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenSacThueAnHan3
            // 
            this.lblTenSacThueAnHan3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenSacThueAnHan3.LocationFloat = new DevExpress.Utils.PointFloat(85.08656F, 141.2132F);
            this.lblTenSacThueAnHan3.Name = "lblTenSacThueAnHan3";
            this.lblTenSacThueAnHan3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThueAnHan3.SizeF = new System.Drawing.SizeF(104.9834F, 10F);
            this.lblTenSacThueAnHan3.StylePriority.UseFont = false;
            this.lblTenSacThueAnHan3.StylePriority.UseTextAlignment = false;
            this.lblTenSacThueAnHan3.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSacThueAnHan4
            // 
            this.lblMaSacThueAnHan4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSacThueAnHan4.LocationFloat = new DevExpress.Utils.PointFloat(271.1199F, 141.2132F);
            this.lblMaSacThueAnHan4.Name = "lblMaSacThueAnHan4";
            this.lblMaSacThueAnHan4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThueAnHan4.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaSacThueAnHan4.StylePriority.UseFont = false;
            this.lblMaSacThueAnHan4.StylePriority.UseTextAlignment = false;
            this.lblMaSacThueAnHan4.Text = "X";
            this.lblMaSacThueAnHan4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHanNopThue4
            // 
            this.lblHanNopThue4.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblHanNopThue4.LocationFloat = new DevExpress.Utils.PointFloat(392.6532F, 141.2132F);
            this.lblHanNopThue4.Name = "lblHanNopThue4";
            this.lblHanNopThue4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanNopThue4.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblHanNopThue4.StylePriority.UseFont = false;
            this.lblHanNopThue4.StylePriority.UseTextAlignment = false;
            this.lblHanNopThue4.Text = "dd/MM/yyyy";
            this.lblHanNopThue4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenSacThueAnHan4
            // 
            this.lblTenSacThueAnHan4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenSacThueAnHan4.LocationFloat = new DevExpress.Utils.PointFloat(287.6698F, 141.2132F);
            this.lblTenSacThueAnHan4.Name = "lblTenSacThueAnHan4";
            this.lblTenSacThueAnHan4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThueAnHan4.SizeF = new System.Drawing.SizeF(104.9834F, 10F);
            this.lblTenSacThueAnHan4.StylePriority.UseFont = false;
            this.lblTenSacThueAnHan4.StylePriority.UseTextAlignment = false;
            this.lblTenSacThueAnHan4.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHanNopThue5
            // 
            this.lblHanNopThue5.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblHanNopThue5.LocationFloat = new DevExpress.Utils.PointFloat(190.07F, 151.2132F);
            this.lblHanNopThue5.Name = "lblHanNopThue5";
            this.lblHanNopThue5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanNopThue5.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblHanNopThue5.StylePriority.UseFont = false;
            this.lblHanNopThue5.StylePriority.UseTextAlignment = false;
            this.lblHanNopThue5.Text = "dd/MM/yyyy";
            this.lblHanNopThue5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSacThueAnHan5
            // 
            this.lblMaSacThueAnHan5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSacThueAnHan5.LocationFloat = new DevExpress.Utils.PointFloat(68.53658F, 151.2132F);
            this.lblMaSacThueAnHan5.Name = "lblMaSacThueAnHan5";
            this.lblMaSacThueAnHan5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThueAnHan5.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaSacThueAnHan5.StylePriority.UseFont = false;
            this.lblMaSacThueAnHan5.StylePriority.UseTextAlignment = false;
            this.lblMaSacThueAnHan5.Text = "X";
            this.lblMaSacThueAnHan5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenSacThueAnHan5
            // 
            this.lblTenSacThueAnHan5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenSacThueAnHan5.LocationFloat = new DevExpress.Utils.PointFloat(85.08656F, 151.2132F);
            this.lblTenSacThueAnHan5.Name = "lblTenSacThueAnHan5";
            this.lblTenSacThueAnHan5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThueAnHan5.SizeF = new System.Drawing.SizeF(104.9834F, 10F);
            this.lblTenSacThueAnHan5.StylePriority.UseFont = false;
            this.lblTenSacThueAnHan5.StylePriority.UseTextAlignment = false;
            this.lblTenSacThueAnHan5.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSacThueAnHan6
            // 
            this.lblMaSacThueAnHan6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSacThueAnHan6.LocationFloat = new DevExpress.Utils.PointFloat(271.1199F, 151.2132F);
            this.lblMaSacThueAnHan6.Name = "lblMaSacThueAnHan6";
            this.lblMaSacThueAnHan6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThueAnHan6.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaSacThueAnHan6.StylePriority.UseFont = false;
            this.lblMaSacThueAnHan6.StylePriority.UseTextAlignment = false;
            this.lblMaSacThueAnHan6.Text = "X";
            this.lblMaSacThueAnHan6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHanNopThue6
            // 
            this.lblHanNopThue6.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblHanNopThue6.LocationFloat = new DevExpress.Utils.PointFloat(392.6532F, 151.2132F);
            this.lblHanNopThue6.Name = "lblHanNopThue6";
            this.lblHanNopThue6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanNopThue6.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblHanNopThue6.StylePriority.UseFont = false;
            this.lblHanNopThue6.StylePriority.UseTextAlignment = false;
            this.lblHanNopThue6.Text = "dd/MM/yyyy";
            this.lblHanNopThue6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenSacThueAnHan6
            // 
            this.lblTenSacThueAnHan6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenSacThueAnHan6.LocationFloat = new DevExpress.Utils.PointFloat(287.6698F, 151.2132F);
            this.lblTenSacThueAnHan6.Name = "lblTenSacThueAnHan6";
            this.lblTenSacThueAnHan6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThueAnHan6.SizeF = new System.Drawing.SizeF(104.9834F, 10F);
            this.lblTenSacThueAnHan6.StylePriority.UseFont = false;
            this.lblTenSacThueAnHan6.StylePriority.UseTextAlignment = false;
            this.lblTenSacThueAnHan6.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel98
            // 
            this.xrLabel98.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(46.66154F, 161.2132F);
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(296.6684F, 10F);
            this.xrLabel98.StylePriority.UseFont = false;
            this.xrLabel98.StylePriority.UseTextAlignment = false;
            this.xrLabel98.Text = "Thời hạn cho phép vận chuyển bảo thuế (khởi hành)";
            this.xrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayKhoiHanhVanChuyen
            // 
            this.lblNgayKhoiHanhVanChuyen.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayKhoiHanhVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(343.3299F, 161.2132F);
            this.lblNgayKhoiHanhVanChuyen.Name = "lblNgayKhoiHanhVanChuyen";
            this.lblNgayKhoiHanhVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayKhoiHanhVanChuyen.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblNgayKhoiHanhVanChuyen.StylePriority.UseFont = false;
            this.lblNgayKhoiHanhVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblNgayKhoiHanhVanChuyen.Text = "dd/MM/yyyy";
            this.lblNgayKhoiHanhVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel100
            // 
            this.xrLabel100.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel100.LocationFloat = new DevExpress.Utils.PointFloat(46.66666F, 181.2132F);
            this.xrLabel100.Name = "xrLabel100";
            this.xrLabel100.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel100.SizeF = new System.Drawing.SizeF(163.1117F, 10F);
            this.xrLabel100.StylePriority.UseFont = false;
            this.xrLabel100.StylePriority.UseTextAlignment = false;
            this.xrLabel100.Text = "Thông tin trung chuyển";
            this.xrLabel100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel101
            // 
            this.xrLabel101.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(252.08F, 171.2132F);
            this.xrLabel101.Name = "xrLabel101";
            this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel101.SizeF = new System.Drawing.SizeF(83.72679F, 10F);
            this.xrLabel101.StylePriority.UseFont = false;
            this.xrLabel101.StylePriority.UseTextAlignment = false;
            this.xrLabel101.Text = "Địa điểm";
            this.xrLabel101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel102
            // 
            this.xrLabel102.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(335.8068F, 171.2132F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(83.72679F, 10F);
            this.xrLabel102.StylePriority.UseFont = false;
            this.xrLabel102.StylePriority.UseTextAlignment = false;
            this.xrLabel102.Text = "Ngày đến";
            this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel103
            // 
            this.xrLabel103.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(442.0717F, 171.2132F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(89.21329F, 10F);
            this.xrLabel103.StylePriority.UseFont = false;
            this.xrLabel103.StylePriority.UseTextAlignment = false;
            this.xrLabel103.Text = "Ngày khởi hành";
            this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel104
            // 
            this.xrLabel104.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(224.7783F, 181.2132F);
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(27.30156F, 10F);
            this.xrLabel104.StylePriority.UseFont = false;
            this.xrLabel104.StylePriority.UseTextAlignment = false;
            this.xrLabel104.Text = "1";
            this.xrLabel104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblDiaDiemTrungChuyen1
            // 
            this.lblDiaDiemTrungChuyen1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaDiemTrungChuyen1.LocationFloat = new DevExpress.Utils.PointFloat(252.08F, 181.2132F);
            this.lblDiaDiemTrungChuyen1.Name = "lblDiaDiemTrungChuyen1";
            this.lblDiaDiemTrungChuyen1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemTrungChuyen1.SizeF = new System.Drawing.SizeF(83.72679F, 10F);
            this.lblDiaDiemTrungChuyen1.StylePriority.UseFont = false;
            this.lblDiaDiemTrungChuyen1.StylePriority.UseTextAlignment = false;
            this.lblDiaDiemTrungChuyen1.Text = "XXXXXXE";
            this.lblDiaDiemTrungChuyen1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayDuKienDenDDTC1
            // 
            this.lblNgayDuKienDenDDTC1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayDuKienDenDDTC1.LocationFloat = new DevExpress.Utils.PointFloat(335.8068F, 181.2132F);
            this.lblNgayDuKienDenDDTC1.Name = "lblNgayDuKienDenDDTC1";
            this.lblNgayDuKienDenDDTC1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDuKienDenDDTC1.SizeF = new System.Drawing.SizeF(83.72681F, 10F);
            this.lblNgayDuKienDenDDTC1.StylePriority.UseFont = false;
            this.lblNgayDuKienDenDDTC1.StylePriority.UseTextAlignment = false;
            this.lblNgayDuKienDenDDTC1.Text = "dd/MM/yyyy";
            this.lblNgayDuKienDenDDTC1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayKhoiHanh1
            // 
            this.lblNgayKhoiHanh1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayKhoiHanh1.LocationFloat = new DevExpress.Utils.PointFloat(442.0717F, 181.2132F);
            this.lblNgayKhoiHanh1.Name = "lblNgayKhoiHanh1";
            this.lblNgayKhoiHanh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayKhoiHanh1.SizeF = new System.Drawing.SizeF(89.21329F, 10F);
            this.lblNgayKhoiHanh1.StylePriority.UseFont = false;
            this.lblNgayKhoiHanh1.StylePriority.UseTextAlignment = false;
            this.lblNgayKhoiHanh1.Text = "dd/MM/yyyy";
            this.lblNgayKhoiHanh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel108
            // 
            this.xrLabel108.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(419.5336F, 181.2132F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(22.53806F, 10F);
            this.xrLabel108.StylePriority.UseFont = false;
            this.xrLabel108.StylePriority.UseTextAlignment = false;
            this.xrLabel108.Text = "~";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblDiaDiemTrungChuyen2
            // 
            this.lblDiaDiemTrungChuyen2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaDiemTrungChuyen2.LocationFloat = new DevExpress.Utils.PointFloat(252.08F, 191.2131F);
            this.lblDiaDiemTrungChuyen2.Name = "lblDiaDiemTrungChuyen2";
            this.lblDiaDiemTrungChuyen2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemTrungChuyen2.SizeF = new System.Drawing.SizeF(83.72679F, 10F);
            this.lblDiaDiemTrungChuyen2.StylePriority.UseFont = false;
            this.lblDiaDiemTrungChuyen2.StylePriority.UseTextAlignment = false;
            this.lblDiaDiemTrungChuyen2.Text = "XXXXXXE";
            this.lblDiaDiemTrungChuyen2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel110
            // 
            this.xrLabel110.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel110.LocationFloat = new DevExpress.Utils.PointFloat(224.7783F, 191.2131F);
            this.xrLabel110.Name = "xrLabel110";
            this.xrLabel110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel110.SizeF = new System.Drawing.SizeF(27.30156F, 10F);
            this.xrLabel110.StylePriority.UseFont = false;
            this.xrLabel110.StylePriority.UseTextAlignment = false;
            this.xrLabel110.Text = "2";
            this.xrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayDuKienDenDDTC2
            // 
            this.lblNgayDuKienDenDDTC2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayDuKienDenDDTC2.LocationFloat = new DevExpress.Utils.PointFloat(335.8068F, 191.2131F);
            this.lblNgayDuKienDenDDTC2.Name = "lblNgayDuKienDenDDTC2";
            this.lblNgayDuKienDenDDTC2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDuKienDenDDTC2.SizeF = new System.Drawing.SizeF(83.72681F, 10F);
            this.lblNgayDuKienDenDDTC2.StylePriority.UseFont = false;
            this.lblNgayDuKienDenDDTC2.StylePriority.UseTextAlignment = false;
            this.lblNgayDuKienDenDDTC2.Text = "dd/MM/yyyy";
            this.lblNgayDuKienDenDDTC2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayKhoiHanh2
            // 
            this.lblNgayKhoiHanh2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayKhoiHanh2.LocationFloat = new DevExpress.Utils.PointFloat(442.0717F, 191.2131F);
            this.lblNgayKhoiHanh2.Name = "lblNgayKhoiHanh2";
            this.lblNgayKhoiHanh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayKhoiHanh2.SizeF = new System.Drawing.SizeF(89.21329F, 10F);
            this.lblNgayKhoiHanh2.StylePriority.UseFont = false;
            this.lblNgayKhoiHanh2.StylePriority.UseTextAlignment = false;
            this.lblNgayKhoiHanh2.Text = "dd/MM/yyyy";
            this.lblNgayKhoiHanh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel113
            // 
            this.xrLabel113.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel113.LocationFloat = new DevExpress.Utils.PointFloat(419.5336F, 191.2131F);
            this.xrLabel113.Name = "xrLabel113";
            this.xrLabel113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel113.SizeF = new System.Drawing.SizeF(22.53806F, 10F);
            this.xrLabel113.StylePriority.UseFont = false;
            this.xrLabel113.StylePriority.UseTextAlignment = false;
            this.xrLabel113.Text = "~";
            this.xrLabel113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblDiaDiemTrungChuyen3
            // 
            this.lblDiaDiemTrungChuyen3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaDiemTrungChuyen3.LocationFloat = new DevExpress.Utils.PointFloat(252.08F, 201.2131F);
            this.lblDiaDiemTrungChuyen3.Name = "lblDiaDiemTrungChuyen3";
            this.lblDiaDiemTrungChuyen3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemTrungChuyen3.SizeF = new System.Drawing.SizeF(83.72679F, 10F);
            this.lblDiaDiemTrungChuyen3.StylePriority.UseFont = false;
            this.lblDiaDiemTrungChuyen3.StylePriority.UseTextAlignment = false;
            this.lblDiaDiemTrungChuyen3.Text = "XXXXXXE";
            this.lblDiaDiemTrungChuyen3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel115
            // 
            this.xrLabel115.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(224.7783F, 201.2131F);
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(27.30156F, 10F);
            this.xrLabel115.StylePriority.UseFont = false;
            this.xrLabel115.StylePriority.UseTextAlignment = false;
            this.xrLabel115.Text = "3";
            this.xrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayDuKienDenDDTC3
            // 
            this.lblNgayDuKienDenDDTC3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayDuKienDenDDTC3.LocationFloat = new DevExpress.Utils.PointFloat(335.8068F, 201.2131F);
            this.lblNgayDuKienDenDDTC3.Name = "lblNgayDuKienDenDDTC3";
            this.lblNgayDuKienDenDDTC3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDuKienDenDDTC3.SizeF = new System.Drawing.SizeF(83.72681F, 10F);
            this.lblNgayDuKienDenDDTC3.StylePriority.UseFont = false;
            this.lblNgayDuKienDenDDTC3.StylePriority.UseTextAlignment = false;
            this.lblNgayDuKienDenDDTC3.Text = "dd/MM/yyyy";
            this.lblNgayDuKienDenDDTC3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayKhoiHanh3
            // 
            this.lblNgayKhoiHanh3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayKhoiHanh3.LocationFloat = new DevExpress.Utils.PointFloat(442.0717F, 201.2131F);
            this.lblNgayKhoiHanh3.Name = "lblNgayKhoiHanh3";
            this.lblNgayKhoiHanh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayKhoiHanh3.SizeF = new System.Drawing.SizeF(89.21329F, 10F);
            this.lblNgayKhoiHanh3.StylePriority.UseFont = false;
            this.lblNgayKhoiHanh3.StylePriority.UseTextAlignment = false;
            this.lblNgayKhoiHanh3.Text = "dd/MM/yyyy";
            this.lblNgayKhoiHanh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel118
            // 
            this.xrLabel118.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(419.5336F, 201.2131F);
            this.xrLabel118.Name = "xrLabel118";
            this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel118.SizeF = new System.Drawing.SizeF(22.53806F, 10F);
            this.xrLabel118.StylePriority.UseFont = false;
            this.xrLabel118.StylePriority.UseTextAlignment = false;
            this.xrLabel118.Text = "~";
            this.xrLabel118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel119
            // 
            this.xrLabel119.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(46.66668F, 211.2131F);
            this.xrLabel119.Name = "xrLabel119";
            this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel119.SizeF = new System.Drawing.SizeF(205.4133F, 10.00003F);
            this.xrLabel119.StylePriority.UseFont = false;
            this.xrLabel119.StylePriority.UseTextAlignment = false;
            this.xrLabel119.Text = "Địa điểm đích cho vận chuyển bảo thuế";
            this.xrLabel119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaDiemDichVanChuyen
            // 
            this.lblDiaDiemDichVanChuyen.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaDiemDichVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(252.08F, 211.2131F);
            this.lblDiaDiemDichVanChuyen.Name = "lblDiaDiemDichVanChuyen";
            this.lblDiaDiemDichVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemDichVanChuyen.SizeF = new System.Drawing.SizeF(83.72679F, 10F);
            this.lblDiaDiemDichVanChuyen.StylePriority.UseFont = false;
            this.lblDiaDiemDichVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblDiaDiemDichVanChuyen.Text = "XXXXXXE";
            this.lblDiaDiemDichVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayDuKienDen
            // 
            this.lblNgayDuKienDen.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayDuKienDen.LocationFloat = new DevExpress.Utils.PointFloat(335.8068F, 211.2131F);
            this.lblNgayDuKienDen.Name = "lblNgayDuKienDen";
            this.lblNgayDuKienDen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDuKienDen.SizeF = new System.Drawing.SizeF(83.72681F, 10F);
            this.lblNgayDuKienDen.StylePriority.UseFont = false;
            this.lblNgayDuKienDen.StylePriority.UseTextAlignment = false;
            this.lblNgayDuKienDen.Text = "dd/MM/yyyy";
            this.lblNgayDuKienDen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader,
            this.ReportFooter});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail1.HeightF = 35.22873F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(23.95834F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(731.04F, 35F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTChiThiHQ,
            this.lblNgaychithihaiquan1,
            this.lblTenchithihaiquan1,
            this.lblNoidungchithihaiquan1});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // lblSTTChiThiHQ
            // 
            this.lblSTTChiThiHQ.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSTTChiThiHQ.Name = "lblSTTChiThiHQ";
            this.lblSTTChiThiHQ.StylePriority.UseFont = false;
            this.lblSTTChiThiHQ.StylePriority.UseTextAlignment = false;
            this.lblSTTChiThiHQ.Text = "1";
            this.lblSTTChiThiHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTTChiThiHQ.Weight = 0.0849716401675839;
            // 
            // lblNgaychithihaiquan1
            // 
            this.lblNgaychithihaiquan1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaychithihaiquan1.Name = "lblNgaychithihaiquan1";
            this.lblNgaychithihaiquan1.StylePriority.UseFont = false;
            this.lblNgaychithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNgaychithihaiquan1.Text = "dd/MM/yyyy";
            this.lblNgaychithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaychithihaiquan1.Weight = 0.35712192295232159;
            // 
            // lblTenchithihaiquan1
            // 
            this.lblTenchithihaiquan1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenchithihaiquan1.Multiline = true;
            this.lblTenchithihaiquan1.Name = "lblTenchithihaiquan1";
            this.lblTenchithihaiquan1.StylePriority.UseFont = false;
            this.lblTenchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblTenchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
            this.lblTenchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTenchithihaiquan1.Weight = 0.83951528020254451;
            // 
            // lblNoidungchithihaiquan1
            // 
            this.lblNoidungchithihaiquan1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblNoidungchithihaiquan1.Multiline = true;
            this.lblNoidungchithihaiquan1.Name = "lblNoidungchithihaiquan1";
            this.lblNoidungchithihaiquan1.StylePriority.UseFont = false;
            this.lblNoidungchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNoidungchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWW0WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
            this.lblNoidungchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblNoidungchithihaiquan1.Weight = 1.9577792374061049;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblPhanLoaiChiThiHQ,
            this.xrTable2,
            this.xrLabel8});
            this.ReportHeader.HeightF = 24.07945F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblPhanLoaiChiThiHQ
            // 
            this.lblPhanLoaiChiThiHQ.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiChiThiHQ.LocationFloat = new DevExpress.Utils.PointFloat(175F, 0F);
            this.lblPhanLoaiChiThiHQ.Name = "lblPhanLoaiChiThiHQ";
            this.lblPhanLoaiChiThiHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiChiThiHQ.SizeF = new System.Drawing.SizeF(37.49994F, 11F);
            this.lblPhanLoaiChiThiHQ.StylePriority.UseFont = false;
            this.lblPhanLoaiChiThiHQ.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiChiThiHQ.Text = "X";
            this.lblPhanLoaiChiThiHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(42.48369F, 12.95833F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(712.52F, 11F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.6742694161155007;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Ngày";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell4.Weight = 0.37264495600631686;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Tên";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell5.Weight = 0.86900334220179032;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Nội dung";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell6.Weight = 2.0265392740732286;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(11.45833F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(163.5417F, 11F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = "Phân loại chỉ thị của Hải quan";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblPhanLoaiThamTra,
            this.lblSoNgayMongDoi,
            this.lblTenTruongDvHQ,
            this.xrLabel43,
            this.xrLabel42,
            this.xrLabel41,
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel38,
            this.xrLabel37,
            this.lblNgayCapPhep,
            this.lblNgayHoanThanhKT,
            this.lblNgayPheQuyet,
            this.lblNgayHoanThanhKiemTraBP,
            this.lblGioCapPhep,
            this.lblGioHoanThanhKT,
            this.lblGioPheQuyet,
            this.lblGioHoanThanhKiemTraBP,
            this.lblHanNopThueVAT,
            this.lblMaSacThueAnHanVAT,
            this.lblTenSacThueAnHanVAT,
            this.xrLabel78,
            this.lblHanNopThue1,
            this.lblMaSacThueAnHan1,
            this.lblNgayDuKienDen,
            this.xrLabel79,
            this.xrLabel80,
            this.lblTieuDe,
            this.lblTenSacThueAnHan1,
            this.lblMaSacThueAnHan2,
            this.lblHanNopThue2,
            this.lblTenSacThueAnHan2,
            this.lblHanNopThue3,
            this.lblMaSacThueAnHan3,
            this.lblTenSacThueAnHan3,
            this.lblMaSacThueAnHan4,
            this.lblHanNopThue4,
            this.lblTenSacThueAnHan4,
            this.lblHanNopThue5,
            this.lblMaSacThueAnHan5,
            this.lblTenSacThueAnHan5,
            this.lblMaSacThueAnHan6,
            this.lblHanNopThue6,
            this.lblTenSacThueAnHan6,
            this.xrLabel98,
            this.lblNgayKhoiHanhVanChuyen,
            this.xrLabel100,
            this.xrLabel101,
            this.xrLabel102,
            this.xrLabel103,
            this.xrLabel104,
            this.lblDiaDiemTrungChuyen1,
            this.lblNgayDuKienDenDDTC1,
            this.lblNgayKhoiHanh1,
            this.xrLabel108,
            this.lblDiaDiemTrungChuyen2,
            this.xrLabel110,
            this.lblNgayDuKienDenDDTC2,
            this.lblNgayKhoiHanh2,
            this.xrLabel113,
            this.lblDiaDiemTrungChuyen3,
            this.xrLabel115,
            this.lblNgayDuKienDenDDTC3,
            this.lblNgayKhoiHanh3,
            this.xrLabel118,
            this.xrLabel119,
            this.lblDiaDiemDichVanChuyen,
            this.xrLine5});
            this.ReportFooter.HeightF = 319.9107F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // lblPhanLoaiThamTra
            // 
            this.lblPhanLoaiThamTra.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblPhanLoaiThamTra.LocationFloat = new DevExpress.Utils.PointFloat(296.6216F, 55.12271F);
            this.lblPhanLoaiThamTra.Name = "lblPhanLoaiThamTra";
            this.lblPhanLoaiThamTra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiThamTra.SizeF = new System.Drawing.SizeF(25.285F, 9.999992F);
            this.lblPhanLoaiThamTra.StylePriority.UseFont = false;
            this.lblPhanLoaiThamTra.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiThamTra.Text = "XE";
            this.lblPhanLoaiThamTra.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoNgayMongDoi
            // 
            this.lblSoNgayMongDoi.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblSoNgayMongDoi.LocationFloat = new DevExpress.Utils.PointFloat(295.4419F, 92.6227F);
            this.lblSoNgayMongDoi.Name = "lblSoNgayMongDoi";
            this.lblSoNgayMongDoi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoNgayMongDoi.SizeF = new System.Drawing.SizeF(25.285F, 9.999992F);
            this.lblSoNgayMongDoi.StylePriority.UseFont = false;
            this.lblSoNgayMongDoi.StylePriority.UseTextAlignment = false;
            this.lblSoNgayMongDoi.Text = "NE";
            this.lblSoNgayMongDoi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoNgayMongDoi.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenTruongDvHQ
            // 
            this.lblTenTruongDvHQ.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenTruongDvHQ.LocationFloat = new DevExpress.Utils.PointFloat(295.1013F, 17.74999F);
            this.lblTenTruongDvHQ.Name = "lblTenTruongDvHQ";
            this.lblTenTruongDvHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenTruongDvHQ.SizeF = new System.Drawing.SizeF(406.593F, 10.00001F);
            this.lblTenTruongDvHQ.StylePriority.UseFont = false;
            this.lblTenTruongDvHQ.StylePriority.UseTextAlignment = false;
            this.lblTenTruongDvHQ.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWW3WWWWWE";
            this.lblTenTruongDvHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(46.67162F, 17.74997F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(163.1117F, 10F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "Trưởng đơn vị Hải quan";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(47.00215F, 43.87273F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(163.1117F, 10F);
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.StylePriority.UseTextAlignment = false;
            this.xrLabel42.Text = "Ngày hoàn thành kiểm tra";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(47.00215F, 92.62273F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(239.545F, 10.00001F);
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.StylePriority.UseTextAlignment = false;
            this.xrLabel41.Text = "Số ngày mong đợi đến khi cấp phép nhập khẩu";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(47.00215F, 80.12271F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(163.1117F, 10F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "Ngày hoàn thành kiểm tra BP";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(47.00215F, 67.62271F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(163.1117F, 10F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "Ngày phê duyệt BP";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(47.00215F, 55.12271F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(190.4184F, 9.999989F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "Phân loại thẩm tra sau thông quan";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(47.01218F, 31.37273F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(163.1117F, 10F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "Ngày cấp phép";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayCapPhep
            // 
            this.lblNgayCapPhep.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayCapPhep.LocationFloat = new DevExpress.Utils.PointFloat(295.4419F, 31.37276F);
            this.lblNgayCapPhep.Name = "lblNgayCapPhep";
            this.lblNgayCapPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayCapPhep.SizeF = new System.Drawing.SizeF(70.39148F, 10.00003F);
            this.lblNgayCapPhep.StylePriority.UseFont = false;
            this.lblNgayCapPhep.StylePriority.UseTextAlignment = false;
            this.lblNgayCapPhep.Text = "dd/MM/yyyy";
            this.lblNgayCapPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayHoanThanhKT
            // 
            this.lblNgayHoanThanhKT.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayHoanThanhKT.LocationFloat = new DevExpress.Utils.PointFloat(295.4419F, 43.87276F);
            this.lblNgayHoanThanhKT.Name = "lblNgayHoanThanhKT";
            this.lblNgayHoanThanhKT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHoanThanhKT.SizeF = new System.Drawing.SizeF(70.39148F, 10.00003F);
            this.lblNgayHoanThanhKT.StylePriority.UseFont = false;
            this.lblNgayHoanThanhKT.StylePriority.UseTextAlignment = false;
            this.lblNgayHoanThanhKT.Text = "dd/MM/yyyy";
            this.lblNgayHoanThanhKT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayPheQuyet
            // 
            this.lblNgayPheQuyet.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayPheQuyet.LocationFloat = new DevExpress.Utils.PointFloat(295.4419F, 67.62271F);
            this.lblNgayPheQuyet.Name = "lblNgayPheQuyet";
            this.lblNgayPheQuyet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayPheQuyet.SizeF = new System.Drawing.SizeF(70.39148F, 10.00003F);
            this.lblNgayPheQuyet.StylePriority.UseFont = false;
            this.lblNgayPheQuyet.StylePriority.UseTextAlignment = false;
            this.lblNgayPheQuyet.Text = "dd/MM/yyyy";
            this.lblNgayPheQuyet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayHoanThanhKiemTraBP
            // 
            this.lblNgayHoanThanhKiemTraBP.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayHoanThanhKiemTraBP.LocationFloat = new DevExpress.Utils.PointFloat(295.4419F, 80.12271F);
            this.lblNgayHoanThanhKiemTraBP.Name = "lblNgayHoanThanhKiemTraBP";
            this.lblNgayHoanThanhKiemTraBP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHoanThanhKiemTraBP.SizeF = new System.Drawing.SizeF(70.39148F, 10.00003F);
            this.lblNgayHoanThanhKiemTraBP.StylePriority.UseFont = false;
            this.lblNgayHoanThanhKiemTraBP.StylePriority.UseTextAlignment = false;
            this.lblNgayHoanThanhKiemTraBP.Text = "dd/MM/yyyy";
            this.lblNgayHoanThanhKiemTraBP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioCapPhep
            // 
            this.lblGioCapPhep.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioCapPhep.LocationFloat = new DevExpress.Utils.PointFloat(365.8334F, 31.37276F);
            this.lblGioCapPhep.Name = "lblGioCapPhep";
            this.lblGioCapPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioCapPhep.SizeF = new System.Drawing.SizeF(57.69653F, 9.999974F);
            this.lblGioCapPhep.StylePriority.UseFont = false;
            this.lblGioCapPhep.StylePriority.UseTextAlignment = false;
            this.lblGioCapPhep.Text = "hh:mm:ss";
            this.lblGioCapPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioHoanThanhKT
            // 
            this.lblGioHoanThanhKT.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioHoanThanhKT.LocationFloat = new DevExpress.Utils.PointFloat(365.8334F, 43.87276F);
            this.lblGioHoanThanhKT.Name = "lblGioHoanThanhKT";
            this.lblGioHoanThanhKT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioHoanThanhKT.SizeF = new System.Drawing.SizeF(57.69653F, 9.999974F);
            this.lblGioHoanThanhKT.StylePriority.UseFont = false;
            this.lblGioHoanThanhKT.StylePriority.UseTextAlignment = false;
            this.lblGioHoanThanhKT.Text = "hh:mm:ss";
            this.lblGioHoanThanhKT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioPheQuyet
            // 
            this.lblGioPheQuyet.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioPheQuyet.LocationFloat = new DevExpress.Utils.PointFloat(365.8334F, 67.62271F);
            this.lblGioPheQuyet.Name = "lblGioPheQuyet";
            this.lblGioPheQuyet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioPheQuyet.SizeF = new System.Drawing.SizeF(57.69653F, 9.999974F);
            this.lblGioPheQuyet.StylePriority.UseFont = false;
            this.lblGioPheQuyet.StylePriority.UseTextAlignment = false;
            this.lblGioPheQuyet.Text = "hh:mm:ss";
            this.lblGioPheQuyet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioHoanThanhKiemTraBP
            // 
            this.lblGioHoanThanhKiemTraBP.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioHoanThanhKiemTraBP.LocationFloat = new DevExpress.Utils.PointFloat(365.8334F, 80.12271F);
            this.lblGioHoanThanhKiemTraBP.Name = "lblGioHoanThanhKiemTraBP";
            this.lblGioHoanThanhKiemTraBP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioHoanThanhKiemTraBP.SizeF = new System.Drawing.SizeF(57.69653F, 9.999974F);
            this.lblGioHoanThanhKiemTraBP.StylePriority.UseFont = false;
            this.lblGioHoanThanhKiemTraBP.StylePriority.UseTextAlignment = false;
            this.lblGioHoanThanhKiemTraBP.Text = "hh:mm:ss";
            this.lblGioHoanThanhKiemTraBP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHanNopThueVAT
            // 
            this.lblHanNopThueVAT.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblHanNopThueVAT.LocationFloat = new DevExpress.Utils.PointFloat(657.4933F, 131.2133F);
            this.lblHanNopThueVAT.Name = "lblHanNopThueVAT";
            this.lblHanNopThueVAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHanNopThueVAT.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblHanNopThueVAT.StylePriority.UseFont = false;
            this.lblHanNopThueVAT.StylePriority.UseTextAlignment = false;
            this.lblHanNopThueVAT.Text = "dd/MM/yyyy";
            this.lblHanNopThueVAT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSacThueAnHanVAT
            // 
            this.lblMaSacThueAnHanVAT.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblMaSacThueAnHanVAT.LocationFloat = new DevExpress.Utils.PointFloat(526.62F, 131.2132F);
            this.lblMaSacThueAnHanVAT.Name = "lblMaSacThueAnHanVAT";
            this.lblMaSacThueAnHanVAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThueAnHanVAT.SizeF = new System.Drawing.SizeF(16.28503F, 9.999985F);
            this.lblMaSacThueAnHanVAT.StylePriority.UseFont = false;
            this.lblMaSacThueAnHanVAT.StylePriority.UseTextAlignment = false;
            this.lblMaSacThueAnHanVAT.Text = "X";
            this.lblMaSacThueAnHanVAT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenSacThueAnHanVAT
            // 
            this.lblTenSacThueAnHanVAT.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblTenSacThueAnHanVAT.LocationFloat = new DevExpress.Utils.PointFloat(542.9082F, 131.2132F);
            this.lblTenSacThueAnHanVAT.Name = "lblTenSacThueAnHanVAT";
            this.lblTenSacThueAnHanVAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThueAnHanVAT.SizeF = new System.Drawing.SizeF(114.5801F, 9.999985F);
            this.lblTenSacThueAnHanVAT.StylePriority.UseFont = false;
            this.lblTenSacThueAnHanVAT.StylePriority.UseTextAlignment = false;
            this.lblTenSacThueAnHanVAT.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHanVAT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // QuyetDinhThongQuanHangHoaNhapKhau_2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(53, 27, 131, 57);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblBieuThiTHHetHan;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiCaNhanToChuc;
        private DevExpress.XtraReports.UI.XRLabel lblMaHieuPhuongThucVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiHangHoa;
        private DevExpress.XtraReports.UI.XRLabel lblThoiHanTaiNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRLabel lblMaBoPhanXuLyToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblMaSoHangHoaDaiDienToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRLabel lblSoNhanhToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRLabel lblGioThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblNgayThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lbl1111;
        private DevExpress.XtraReports.UI.XRLabel lblGioDangKy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel lblTenCoQuanHaiQuanTiepNhanToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiKiemTra;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiDauTien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiTamNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiDinhKemKhaiBaoDienTu3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel lblSoDinhKemKhaiBaoDienTu3;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiDinhKemKhaiBaoDienTu2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel lblSoDinhKemKhaiBaoDienTu2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiDinhKemKhaiBaoDienTu1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblSoDinhKemKhaiBaoDienTu1;
        private DevExpress.XtraReports.UI.XRLabel lblSoQuanLyNSD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel lblSoQuanLyNoiBoDN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel lblPhanGhiChu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lblHanNopThue1;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThueAnHan1;
        private DevExpress.XtraReports.UI.XRLabel lblGioKhaiBaoNopThue;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel lblTieuDe;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThueAnHan1;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThueAnHan2;
        private DevExpress.XtraReports.UI.XRLabel lblHanNopThue2;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThueAnHan2;
        private DevExpress.XtraReports.UI.XRLabel lblHanNopThue3;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThueAnHan3;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThueAnHan3;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThueAnHan4;
        private DevExpress.XtraReports.UI.XRLabel lblHanNopThue4;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThueAnHan4;
        private DevExpress.XtraReports.UI.XRLabel lblHanNopThue5;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThueAnHan5;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThueAnHan5;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThueAnHan6;
        private DevExpress.XtraReports.UI.XRLabel lblHanNopThue6;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThueAnHan6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKhoiHanhVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel xrLabel100;
        private DevExpress.XtraReports.UI.XRLabel xrLabel101;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemTrungChuyen1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDuKienDenDDTC1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKhoiHanh1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemTrungChuyen2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel110;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDuKienDenDDTC2;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKhoiHanh2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel113;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemTrungChuyen3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel115;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDuKienDenDDTC3;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKhoiHanh3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel118;
        private DevExpress.XtraReports.UI.XRLabel xrLabel119;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemDichVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDuKienDen;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiChiThiHQ;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTChiThiHQ;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaychithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell lblTenchithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell lblNoidungchithihaiquan1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThueAnHanVAT;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThueAnHanVAT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel lblNgayCapPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHoanThanhKT;
        private DevExpress.XtraReports.UI.XRLabel lblNgayPheQuyet;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHoanThanhKiemTraBP;
        private DevExpress.XtraReports.UI.XRLabel lblGioCapPhep;
        private DevExpress.XtraReports.UI.XRLabel lblGioHoanThanhKT;
        private DevExpress.XtraReports.UI.XRLabel lblGioPheQuyet;
        private DevExpress.XtraReports.UI.XRLabel lblGioHoanThanhKiemTraBP;
        private DevExpress.XtraReports.UI.XRLabel lblHanNopThueVAT;
        private DevExpress.XtraReports.UI.XRLabel lblSoNgayMongDoi;
        private DevExpress.XtraReports.UI.XRLabel lblTenTruongDvHQ;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiThamTra;
        private DevExpress.XtraReports.UI.XRLabel lblSoTrang;
    }
}
