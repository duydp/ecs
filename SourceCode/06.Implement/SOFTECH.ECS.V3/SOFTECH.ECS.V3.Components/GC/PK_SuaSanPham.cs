﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using System.Linq;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class PK_SuaSanPham
    {
        /// <summary>
        /// ConfigProduct
        /// </summary>
        /// <param name="Product">XElement</param>
        /// <param name="SP">SanPham</param>
        /// <returns>XElement</returns>
        public static XElement ConfigProduct(XElement Product, SanPham SP)
        {
            if (Product != null && Product.Elements().Count() > 0 && SP != null)
            {
                DonViTinh DVT = DonViTinh.Load(SP.DVT_ID);
                foreach (XElement element in Product.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodePhuKien.preIdentification.ToUpper()))
                        element.SetValue("");
                    if (element.Name.ToString().ToUpper().Equals(NodePhuKien.Commodity.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.description.ToUpper()))
                                elementChild.SetValue(SP.Ten);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.identification.ToUpper()))
                                elementChild.SetValue(SP.Ma);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.tariffClassification.ToUpper()))
                                elementChild.SetValue(SP.MaHS);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.productGroup.ToUpper()))
                                elementChild.SetValue(SP.NhomSanPham_ID);
                        }
                    }
                    if (DVT != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.GoodsMeasure.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.measureUnit.ToUpper()))
                                    elementChild.SetValue(DVT.Ten);
                            }
                        }
                    }
                }
            }
            return Product;
        }
        /// <summary>
        /// ConfigPK_SuaSanPham
        /// </summary> 
        /// <returns>string</returns>
        public static string ConfigPK_SuaSanPham(PhuKienDangKy PKDK, int issuer, int function, int status)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/GC/PK_SuaSanPham.xml");
                HopDong HD = HopDong.Load(PKDK.HopDong_ID);
                if (HD != null && docXML != null)
                {
                    IList<SanPham> lSP = SanPham.SelectCollectionBy_HopDong_ID(HD.ID);
                    XElement Root = docXML.Root;
                    PhuKienChung.ConfigPhuKien(Root, PKDK, issuer, function, status);

                    XElement AdditionalInformation = Root.Element(NodePhuKien.AdditionalInformation);
                    XElement Content = AdditionalInformation.Element(NodePhuKien.Content);
                    XElement Product = Content.Element(NodePhuKien.Product);
                    if (Root != null && Root.Elements().Count() > 0)
                    {
                        foreach (XElement element in Root.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.AdditionalInformation.ToUpper()))
                            {
                                AdditionalInformation = element;
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.Content.ToUpper()))
                                    {
                                        Content = elementChild;
                                        foreach (XElement Child in elementChild.Elements())
                                        {
                                            if (Child.Name.ToString().ToUpper().Equals(NodePhuKien.Material.ToUpper()))
                                                Product = Child;
                                        }
                                    }
                                }
                            }
                        }
                        if (lSP != null)
                        {
                            if (lSP.Count == 1)
                            {
                                foreach (XElement element in Product.Elements())
                                {
                                    ConfigProduct(Product, lSP[0]);
                                }
                            }
                            else if (lSP.Count > 1)
                            {
                                XElement TempProduct = new XElement(Product);
                                ConfigProduct(Product, lSP[0]);
                                // Add new Product
                                for (int i = 1; i < lSP.Count; i++)
                                {
                                    XElement NewProduct = new XElement(TempProduct);
                                    Content.Add(ConfigProduct(NewProduct, lSP[i]));
                                }
                            }
                        }
                        docXML.Save(@"D:\NewPK_SuaSanPham.xml");
                        return docXML.ToString();
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }
        }
    }
}
