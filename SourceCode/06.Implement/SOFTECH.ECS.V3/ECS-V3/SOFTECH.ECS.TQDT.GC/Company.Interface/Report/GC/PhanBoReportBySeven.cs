using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class PhanBoReportBySeven : DevExpress.XtraReports.UI.XtraReport
    {
        public int ToSo;
        private int STT = 0;
        public DataTable dt = new DataTable();
        public bool First = false;
        public bool Last = false;
        public PhanBoReportBySeven()
        {
            InitializeComponent();
        }
        public void BindReport(ToKhaiMauDich TKMD)
        {
            ReportHeader.Visible = this.First;
            ReportFooter.Visible = this.Last;
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = this.dt;
            lblToSo.Text = this.ToSo + "";
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            string maHaiQuan = TKMD.MaHaiQuan;
            if (maHaiQuan.Contains("N60C")) maHaiQuan = "ĐNĐN";
            if (TKMD.SoToKhai > 0)
                lblTieuDe.Text = "BẢNG KÊ ĐỊNH MỨC TIÊU HAO NGUYÊN VẬT LIỆU NHẬP KHẨU TỜ KHAI: " + TKMD.SoToKhai + "/" + LoaiHinhMauDich.SelectTenVTByMa(TKMD.MaLoaiHinh) + "/" + maHaiQuan +"/";
            else
                lblTieuDe.Text = "BẢNG KÊ ĐỊNH MỨC TIÊU HAO NGUYÊN VẬT LIỆU NHẬP KHẨU TỜ KHAI:           /" + LoaiHinhMauDich.SelectTenVTByMa(TKMD.MaLoaiHinh) + "/" + maHaiQuan +"/";
            if (TKMD.NgayDangKy.Year > 1900)
                lblTieuDe.Text += TKMD.NgayDangKy.ToString("dd-MM-yyyy");
            else if (TKMD.NgayTiepNhan.Year > 1900)
                lblTieuDe.Text += TKMD.NgayTiepNhan.ToString("dd-MM-yyyy");


            
            lblMaNPL.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".MaNPL");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TenNPL");
            lblDVT.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DVT");
            lblDinhMuc1.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc0","{0:n"+ GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc2.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc1", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc3.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc2", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc4.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc3", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc5.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc4", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");

            lblLuongSD1.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD0", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD2.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD1", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD3.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD2", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD4.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD3", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD5.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD4", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

            lblTLHH1.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH0", "{0:n0}");
            lblTLHH2.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH1", "{0:n0}");
            lblTLHH3.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH2", "{0:n0}");
            lblTLHH4.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH3", "{0:n0}");
            lblTLHH5.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH4", "{0:n0}");

            lblSanPham1.Text = dt.Columns["DinhMuc0"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD0"].Caption;
            if (dt.Columns["DinhMuc1"].Caption != " ")
                lblSanPham2.Text = dt.Columns["DinhMuc1"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD1"].Caption;
            else
                lblSanPham2.Text = "";
            if (dt.Columns["DinhMuc2"].Caption != " ") 
                lblSanPham3.Text = dt.Columns["DinhMuc2"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD2"].Caption;
            else
                lblSanPham3.Text = "";
            if (dt.Columns["DinhMuc3"].Caption != " ") 
                lblSanPham4.Text = dt.Columns["DinhMuc3"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD3"].Caption;
            else lblSanPham4.Text = "";
            if (dt.Columns["DinhMuc4"].Caption != " ")
                lblSanPham5.Text = dt.Columns["DinhMuc4"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD4"].Caption;
            else lblSanPham5.Text = "";
            xrLabel2.Text = GlobalSettings.TieudeNgay;

        }
        public void BindReport(ToKhaiChuyenTiep TKCT)
        {
            ReportHeader.Visible = this.First;
            ReportFooter.Visible = this.Last;
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = this.dt;
            lblToSo.Text = this.ToSo + "";
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            string maHaiQuan = TKCT.MaHaiQuanTiepNhan;
            if (maHaiQuan.Contains("N60C")) maHaiQuan = "ĐNĐN";
            if (TKCT.SoToKhai > 0)
                lblTieuDe.Text = "BẢNG KÊ ĐỊNH MỨC TIÊU HAO NGUYÊN VẬT LIỆU NHẬP KHẨU TỜ KHAI: " + TKCT.SoToKhai + "/" + LoaiHinhMauDich.SelectTenVTByMa(TKCT.MaLoaiHinh) + "/" + maHaiQuan + "/";
            else
                lblTieuDe.Text = "BẢNG KÊ ĐỊNH MỨC TIÊU HAO NGUYÊN VẬT LIỆU NHẬP KHẨU TỜ KHAI:          /" + LoaiHinhMauDich.SelectTenVTByMa(TKCT.MaLoaiHinh) + "/" + maHaiQuan + "/";
            if (TKCT.NgayDangKy.Year > 1900)
                lblTieuDe.Text += TKCT.NgayDangKy.ToString("dd-MM-yyyy");
            else if (TKCT.NgayTiepNhan.Year > 1900)
                lblTieuDe.Text += TKCT.NgayTiepNhan.ToString("dd-MM-yyyy");



            lblMaNPL.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".MaNPL");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TenNPL");
            lblDVT.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DVT");
            lblDinhMuc1.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc0", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc2.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc1", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc3.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc2", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc4.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc3", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc5.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc4", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");

            lblLuongSD1.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD0", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD2.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD1", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD3.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD2", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD4.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD3", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD5.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD4", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

            lblTLHH1.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH0", "{0:n0}");
            lblTLHH2.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH1", "{0:n0}");
            lblTLHH3.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH2", "{0:n0}");
            lblTLHH4.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH3", "{0:n0}");
            lblTLHH5.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TLHH4", "{0:n0}");

            lblSanPham1.Text = dt.Columns["DinhMuc0"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD0"].Caption;
            if (dt.Columns["DinhMuc1"].Caption != " ")
                lblSanPham2.Text = dt.Columns["DinhMuc1"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD1"].Caption;
            else
                lblSanPham2.Text = "";
            if (dt.Columns["DinhMuc2"].Caption != " ")
                lblSanPham3.Text = dt.Columns["DinhMuc2"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD2"].Caption;
            else
                lblSanPham3.Text = "";
            if (dt.Columns["DinhMuc3"].Caption != " ")
                lblSanPham4.Text = dt.Columns["DinhMuc3"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD3"].Caption;
            else lblSanPham4.Text = "";
            if (dt.Columns["DinhMuc4"].Caption != " ")
                lblSanPham5.Text = dt.Columns["DinhMuc4"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD4"].Caption;
            else lblSanPham5.Text = "";


        }


        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString(); ;
        }

        private void PhanBoReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT =0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

       
        private void lblDinhMuc5_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc4") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc4")) == 0) lblDinhMuc5.Text = "-";
        }


       
        private void lblLuongSD5_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc4") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc4")) == 0) lblLuongSD5.Text = "-";
        }


        private void lblDinhMuc1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc0") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc0")) == 0) lblDinhMuc1.Text = "-";
        }
        private void lblDinhMuc2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc1") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc1")) == 0) lblDinhMuc2.Text = "-";
        }
        private void lblDinhMuc3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc2") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc2")) == 0) lblDinhMuc3.Text = "-";
        }
        private void lblDinhMuc4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc3") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc3")) == 0) lblDinhMuc4.Text = "-";
        }

        private void lblLuongSD1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc0") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc0")) == 0) lblLuongSD1.Text = "-";
        }
        private void lblLuongSD2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc1") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc1")) == 0) lblLuongSD2.Text = "-";
        }
        private void lblLuongSD3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc2") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc2")) == 0) lblLuongSD3.Text = "-";
        }
        private void lblLuongSD4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc3") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc3")) == 0) lblLuongSD4.Text = "-";
        }

        private void lblTLHH1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc0") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc0")) == 0) lblTLHH1.Text = "-";
        }
        private void lblTLHH2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc1") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc1")) == 0) lblTLHH2.Text = "-";
        }
        private void lblTLHH3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc2") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc2")) == 0) lblTLHH3.Text = "-";
        }
        private void lblTLHH4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc3") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc3")) == 0) lblTLHH4.Text = "-";
        }
        private void lblTLHH5_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc4") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc4")) == 0) lblTLHH5.Text = "-";
        }
        private void ReportFooter_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
    }
}
