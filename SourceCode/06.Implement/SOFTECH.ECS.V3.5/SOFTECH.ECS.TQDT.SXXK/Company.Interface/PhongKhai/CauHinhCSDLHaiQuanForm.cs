﻿using System;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using System.Threading;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Xml;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface.PhongKhai
{
    public partial class CauHinhCSDLHaiQuanForm : BaseForm
    {

        public CauHinhCSDLHaiQuanForm()
        {
            InitializeComponent();
        }
        public bool Change = false;
        private void SendForm_Load(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            string stECS_SXXK = config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString;
            string stSXXK= config.ConnectionStrings.ConnectionStrings["SXXK"].ConnectionString;
            string stKhaiDT = config.ConnectionStrings.ConnectionStrings["KhaiDT"].ConnectionString;
            string[] arrStrECS_SXXK = SpilitConnectionString(stECS_SXXK);
            string[] arrStrSXXK = SpilitConnectionString(stSXXK);
            string[] arrStrKhaiDT = SpilitConnectionString(stKhaiDT);

            txtServerECS_SXXK.Text = arrStrECS_SXXK[0];
            txtDatabaseECS_SXXK.Text = arrStrECS_SXXK[1];
            txtUserECS_SXXK.Text = arrStrECS_SXXK[2];
            txtPassECS_SXXK.Text = arrStrECS_SXXK[3];

            txtServerSXXK.Text = arrStrSXXK[0];
            txtDatabaseSXXK.Text = arrStrSXXK[1];
            txtUserSXXK.Text = arrStrSXXK[2];
            txtPassSXXK.Text = arrStrSXXK[3];

            txtServerKhaiDT.Text = arrStrKhaiDT[0];
            txtDatabaseKhaiDT.Text = arrStrKhaiDT[1];
            txtUserKhaiDT.Text = arrStrKhaiDT[2];
            txtPassKhaiDT.Text = arrStrKhaiDT[3];

        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Change = false;
            this.Close();
        }
        private string[] SpilitConnectionString(string connectionString)
        {
            string[] arrStr = connectionString.Split(new char[1] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            arrStr[0] = arrStr[0].Replace("Server=", "");
            arrStr[1] = arrStr[1].Replace("Database=", "");
            arrStr[2] = arrStr[2].Replace("Uid=", "");
            arrStr[3] = arrStr[3].Replace("Pwd=", "");
            return arrStr;
        }
        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            containerValidator1.Validate();
            if (!containerValidator1.IsValid)
                return;

            string stECS_SXXK = "Server=" + txtServerECS_SXXK.Text.Trim() + ";Database=" + txtDatabaseECS_SXXK.Text  + ";Uid=" + txtUserECS_SXXK.Text + ";Pwd=" + txtPassECS_SXXK.Text;
            string stSXXK = "Server=" + txtServerSXXK.Text.Trim() + ";Database=" + txtDatabaseSXXK.Text + ";Uid=" + txtUserSXXK.Text + ";Pwd=" + txtPassSXXK.Text;
            string stKhaiDT = "Server=" + txtServerKhaiDT.Text.Trim() + ";Database=" + txtDatabaseKhaiDT.Text + ";Uid=" + txtUserKhaiDT.Text + ";Pwd=" + txtPassKhaiDT.Text;
            string provider = "RsaProtectedConfigurationProvider"; 
            
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = stECS_SXXK;
            config.ConnectionStrings.ConnectionStrings["SXXK"].ConnectionString = stSXXK;
            config.ConnectionStrings.ConnectionStrings["KhaiDT"].ConnectionString = stKhaiDT;

            ConfigurationSection section = config.ConnectionStrings;
            if ((section.SectionInformation.IsProtected == false) &&
                (section.ElementInformation.IsLocked == false))
            {
                // Save the encrypted section.
                section.SectionInformation.ProtectSection(provider); 
                section.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Modified);
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(section.SectionInformation.Name);
            this.Change = true;
            this.Close();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            containerValidator1.ContainerToValidate = this.grpECS_SXXK;
            containerValidator1.Validate();
            if (!containerValidator1.IsValid) return;
            string stECS_SXXK = "Server=" + txtServerECS_SXXK.Text.Trim() + ";Database=" + txtDatabaseECS_SXXK.Text + ";Uid=" + txtUserECS_SXXK.Text + ";Pwd=" + txtPassECS_SXXK.Text;

            SqlConnection con = new SqlConnection(stECS_SXXK);
            try
            {
                con.Open();
                ShowMessage("Kết nối đến Database '" + txtDatabaseECS_SXXK.Text + "' thành công.", false);

            }
            catch
            {
                ShowMessage("Kết nối đến Database '" + txtDatabaseECS_SXXK.Text + "' không thành công.", false);

            }
            finally
            {
                containerValidator1.ContainerToValidate = this;
            }
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            containerValidator1.ContainerToValidate = this.grpSXXK;
            containerValidator1.Validate();
            if (!containerValidator1.IsValid) return;

            string stSXXK = "Server=" + txtServerSXXK.Text.Trim() + ";Database=" + txtDatabaseSXXK.Text + ";Uid=" + txtUserSXXK.Text + ";Pwd=" + txtPassSXXK.Text;

            SqlConnection con = new SqlConnection(stSXXK);
            try
            {
                con.Open();
                ShowMessage("Kết nối đến Database '" + txtDatabaseSXXK.Text + "' thành công.", false);

            }
            catch
            {
                ShowMessage("Kết nối đến Database '" + txtDatabaseSXXK.Text + "' không thành công.", false);

            }
            finally
            {
                containerValidator1.ContainerToValidate = this;
            }
        }

        private void uiButton5_Click(object sender, EventArgs e)
        {
            containerValidator1.ContainerToValidate = this.grpKhaiDT;
            containerValidator1.Validate();
            if (!containerValidator1.IsValid) return;

            string stKhaiDT = "Server=" + txtServerKhaiDT.Text.Trim() + ";Database=" + txtDatabaseKhaiDT.Text + ";Uid=" + txtUserKhaiDT.Text + ";Pwd=" + txtPassKhaiDT.Text;

            SqlConnection con = new SqlConnection(stKhaiDT);
            try
            {
                con.Open();
                ShowMessage("Kết nối đến Database '" + txtDatabaseKhaiDT.Text + "' thành công.", false);

            }
            catch
            {
                ShowMessage("Kết nối đến Database '" + txtDatabaseKhaiDT.Text + "' không thành công.", false);
            }
            finally
            {
                containerValidator1.ContainerToValidate = this;
            }
        }


        

        

       

        
    }
}