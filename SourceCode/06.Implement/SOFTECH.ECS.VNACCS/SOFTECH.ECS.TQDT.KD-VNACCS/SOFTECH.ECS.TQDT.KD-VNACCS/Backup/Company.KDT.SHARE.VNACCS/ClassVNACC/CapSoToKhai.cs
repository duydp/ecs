﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.VNACCS
{
	/// <summary>
	///		The data structure representation of the 't_VNACCS_CapSoToKhai' table.
	/// </summary>
	
	public partial class CapSoToKhai
	{
        public static CapSoToKhai CapSoToKhaiV5(decimal SoTKVNACC, KDT_VNACC_ToKhaiMauDich tkmdV)
        {

            //string spName = "Select top 1 * from t_VNACCS_CapSoToKhai where SoTKVNACCS = " + SoTKVNACC;
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            //IDataReader reader = dbCommand.ExecuteReader();
            //IList<CapSoToKhai> list = ConvertToCollection(reader);
            CapSoToKhai capsos = GetFromTKMDVNACC(SoTKVNACC);
            if (capsos == null)
            {
                capsos = new CapSoToKhai();
                capsos.SoTK = SoTKMax() + 1;
            }
            capsos.MaLoaiHinh = tkmdV.MaLoaiHinh;
            capsos.SoNhanhTK = (int)tkmdV.SoNhanhToKhai;
            capsos.SoTKDauTien = Convert.ToDecimal(string.IsNullOrEmpty(tkmdV.SoToKhaiDauTien) ? "0" : tkmdV.SoToKhaiDauTien);
            capsos.SoTKTNTX = tkmdV.SoToKhaiTNTX;
            capsos.SoTKVNACCS = tkmdV.SoToKhai;
            capsos.NamDangKy = tkmdV.NgayDangKy.Year;
            capsos.SoTKVNACCSFull = SoTKVNACC;
            try
            {
                capsos.InsertUpdate();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            
            return capsos;
        }
        public static CapSoToKhai CapSoToKhaiGiay(decimal SoTKVNACC,string maLoaiHinh, string soTKGiay, decimal SoTK_KyThuat,DateTime ngayDangKy)
        {

            
            CapSoToKhai capsos = GetFromTKMDVNACC(SoTKVNACC);
            if (capsos == null)
            {
                capsos = new CapSoToKhai();
                capsos.SoTK = SoTKMax() + 1;
            }
            capsos.MaLoaiHinh = maLoaiHinh;
            capsos.SoNhanhTK = 0;
            capsos.SoTKDauTien = 0;
            capsos.SoTKTNTX = 0;
            capsos.SoTKVNACCS = SoTK_KyThuat;
            capsos.NamDangKy = ngayDangKy.Year;
            capsos.SoTKVNACCSFull = capsos.SoTK;
            capsos.SoTKGiay = soTKGiay;
            try
            {
                capsos.InsertUpdate();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }

            return capsos;
        }
        public static int SoTKMax()
        {
            string spName = "Select Max(SoTK) from t_VNACCS_CapSoToKhai";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString())   )
                return 0;
            else
                return (int)obj;
        }
        public static decimal GetSoTKVNACCS(int SoTKMD)
        {
            string spName = "Select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = " + SoTKMD;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return 0;
            else
                return (decimal)obj;
        }
        public static List<int> GetSoTKByVNACCS(decimal SoTKVNACCS)
        {
            string spName = "Select SoTK from t_VNACCS_CapSoToKhai where SoTKVNACCS like '%" + SoTKVNACCS + "%'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader  reader = db.ExecuteReader(dbCommand);
            List<int> listSoTK = new List<int>();
            while (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) listSoTK.Add(reader.GetInt32(reader.GetOrdinal("SoTK")));
            }
            return listSoTK;
            //if (obj == null || string.IsNullOrEmpty(obj.ToString()))
            //    return 0;
            //else
            //    return (int)obj;
        }
        public static string ConvertMaLoaiHinh(string MaLoaiHinhVNACC)
        {
            string spName = "Select Count(*) from t_VNACC_Category_Common where ReferenceDB = 'E001' and Code = '" + MaLoaiHinhVNACC + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);


            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null)
                return "XV" + MaLoaiHinhVNACC;
            else
            {
                if (((int)obj) == 1)
                    return "NV" + MaLoaiHinhVNACC;
                else
                    return "XV" + MaLoaiHinhVNACC;
            }



        }

        public static CapSoToKhai GetFromTKMD_TKGiay(string soTKGiay)
        {
            string spName = "Select top 1 * from t_VNACCS_CapSoToKhai where SoTKGiay = '" + soTKGiay+"'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<CapSoToKhai> list = ConvertToCollection(reader);
            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            else
                return null;
        }
        public static CapSoToKhai GetFromTKMDVNACC(decimal SoToKhai)
        {
            string spName = "Select top 1 * from t_VNACCS_CapSoToKhai where SoTKVNACCSFull = " + SoToKhai;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<CapSoToKhai> list = ConvertToCollection(reader);
            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            else
                return null;
        }
        public static CapSoToKhai GetFromTKMDVNACCS(decimal SoToKhai)
        {
            string spName = "Select top 1 * from t_VNACCS_CapSoToKhai where SoTKVNACCS = " + SoToKhai;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<CapSoToKhai> list = ConvertToCollection(reader);
            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            else
                return null;
        }
        public static CapSoToKhai GetFromTKMD(int SoToKhai, string MaLoaiHinh)
        {

            CapSoToKhai capso = CapSoToKhai.Load(SoToKhai, MaLoaiHinh);
            if (capso == null || capso.SoTK == 0)
                return null;
            else
                return capso;
//             string spName = "Select top 1 * from t_VNACCS_CapSoToKhai where SoTK = " + SoToKhai;
//             SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
//             SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
// 
//             IDataReader reader = dbCommand.ExecuteReader();
//             IList<CapSoToKhai> list = ConvertToCollection(reader);
//             if (list != null && list.Count > 0)
//             {
//                 return list[0];
//             }
//             else
//                 return null;

        }
        public static CapSoToKhai GetFromTKMD(int soTK, string maLoaiHinh, SqlTransaction trans, SqlDatabase db)
        {
            const string spName = "[dbo].[p_VNACCS_CapSoToKhai_Load]";
            if (db == null)
                db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, soTK);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, maLoaiHinh);

            IDataReader reader = trans == null ? db.ExecuteReader(dbCommand) : db.ExecuteReader(dbCommand,trans);
            IList<CapSoToKhai> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }
        public int InsertUpdate(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_VNACCS_CapSoToKhai_InsertUpdate";
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
            db.AddInParameter(dbCommand, "@SoTKVNACCS", SqlDbType.Decimal, SoTKVNACCS);
            db.AddInParameter(dbCommand, "@SoTKDauTien", SqlDbType.Decimal, SoTKDauTien);
            db.AddInParameter(dbCommand, "@SoNhanhTK", SqlDbType.Int, SoNhanhTK);
            db.AddInParameter(dbCommand, "@TongSoTKChiaNho", SqlDbType.Int, TongSoTKChiaNho);
            db.AddInParameter(dbCommand, "@SoTKTNTX", SqlDbType.Decimal, SoTKTNTX);
            db.AddInParameter(dbCommand, "@Temp1", SqlDbType.NVarChar, Temp1);
            db.AddInParameter(dbCommand, "@SoTKGiay", SqlDbType.VarChar, SoTKGiay);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
	}	
}

