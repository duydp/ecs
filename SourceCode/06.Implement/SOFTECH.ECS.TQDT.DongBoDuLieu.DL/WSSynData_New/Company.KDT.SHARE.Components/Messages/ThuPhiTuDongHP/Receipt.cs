﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class Receipt
    {
        /// <summary>
        /// Mẫu biên lai thu
        /// </summary>
       [XmlElement("invForm")]
       public string InvForm { get; set; }

        /// <summary>
        /// Số serial biên lai thu
        /// 
        /// </summary>
       [XmlElement("invSeries")]
       public string InvSeries { get; set; }

        /// <summary>
        /// Quyển biên lai thu
        /// </summary>
       [XmlElement("invNumber")]
       public string InvNumber { get; set; }

        /// <summary>
       /// Số biên lai thu
        /// </summary>
       [XmlElement("receiptNo")]
       public string ReceiptNo { get; set; }

        /// <summary>
        /// Ngày biên lai thu
        /// </summary>
       [XmlElement("receiptDate")]
       public string ReceiptDate { get; set; }

        /// <summary>
        /// Trạng thái biên lai
       /// I: Phát hành; C: Đã hủy
        /// </summary>
       [XmlElement("receiptStatus")]
       public string ReceiptStatus { get; set; }

        /// <summary>
       /// Tổng số tiền trên biên lai
        /// </summary>
       [XmlElement("totalAmount")]
       public string TotalAmount { get; set; }
    }
}
