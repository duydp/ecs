IF OBJECT_ID('t_KDT_GC_HopDong_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_HopDong_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_HopDong_Log]
(
[ID] [bigint]  NULL ,
[SoTiepNhan] [bigint] NOT NULL,
[TrangThaiXuLy] [int] NOT NULL,
[SoHopDong] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDaiLy] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayKy] [datetime] NOT NULL,
[NgayDangKy] [datetime] NOT NULL,
[NgayHetHan] [datetime] NOT NULL,
[NgayGiaHan] [datetime] NULL,
[NuocThue_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NguyenTe_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayTiepNhan] [datetime] NULL,
[DonViDoiTac] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChiDoiTac] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CanBoTheoDoi] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CanBoDuyet] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrangThaiThanhKhoan] [int] NULL,
[GUIDSTR] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeXuatKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LyDoSua] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionStatus] [smallint] NULL,
[GuidReference] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamTN] [int] NULL,
[HUONGDAN] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLuong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HuongdanPL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChiDoanhNghiep] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonViDoiTac] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDoanhNghiep] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChu] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PTTT_ID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongTriGiaSP] [float] NULL,
[TongTriGiaTienCong] [float] NULL,
[IsGiaCongNguoc] [int] NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_GC_HopDong_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_HopDong_Log
GO

CREATE TRIGGER trg_t_KDT_GC_HopDong_Log
ON dbo.t_KDT_GC_HopDong
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_HopDong_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_GC_HopDong_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_HopDong_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_GC_NhomSanPham_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_NhomSanPham_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_NhomSanPham_Log]
(
[HopDong_ID] [bigint] NULL,
[MaSanPham] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuong] [numeric] (18, 5) NOT NULL,
[GiaGiaCong] [numeric] (18, 5) NOT NULL,
[STTHang] [int] NOT NULL,
[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGia] [float] NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_GC_NhomSanPham_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_NhomSanPham_Log
GO

CREATE TRIGGER trg_t_KDT_GC_NhomSanPham_Log
ON dbo.t_KDT_GC_NhomSanPham
AFTER INSERT,UPDATE, DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_NhomSanPham_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_GC_NhomSanPham_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_NhomSanPham_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_GC_NhomSanPham_Log') IS NOT NULL
DROP TABLE dbo.t_GC_NhomSanPham_Log
GO
CREATE TABLE [dbo].[t_GC_NhomSanPham_Log]
(
[HopDong_ID] [bigint] NOT NULL,
[MaSanPham] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuong] [numeric] (18, 5) NOT NULL,
[GiaGiaCong] [money] NOT NULL,
[STTHang] [int] NOT NULL,
[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrangThai] [int] NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_GC_NhomSanPham_Log') IS NOT NULL
DROP TRIGGER trg_t_GC_NhomSanPham_Log
GO

CREATE TRIGGER trg_t_GC_NhomSanPham_Log
ON dbo.t_GC_NhomSanPham
AFTER INSERT,UPDATE, DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_NhomSanPham_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_GC_NhomSanPham_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_NhomSanPham_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_GC_SanPham_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_SanPham_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_SanPham_Log]
(
[HopDong_ID] [bigint]  NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 6) NOT NULL,
[NhomSanPham_ID] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[STTHang] [int] NOT NULL,
[DonGia] [numeric] (18, 5) NULL ,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_GC_SanPham_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_SanPham_Log
GO

CREATE TRIGGER trg_t_KDT_GC_SanPham_Log
ON dbo.t_KDT_GC_SanPham
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_SanPham_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_GC_SanPham_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_SanPham_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_GC_SanPham_Log') IS NOT NULL
DROP TABLE dbo.t_GC_SanPham_Log
GO
CREATE TABLE [dbo].[t_GC_SanPham_Log]
(
[HopDong_ID] [bigint] NOT NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 5) NOT NULL,
[SoLuongDaXuat] [numeric] (18, 5) NULL,
[NhomSanPham_ID] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[STTHang] [int] NOT NULL,
[TrangThai] [int] NOT NULL,
[DonGia] [numeric] (18, 5) NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_GC_SanPham_Log') IS NOT NULL
DROP TRIGGER trg_t_GC_SanPham_Log
GO

CREATE TRIGGER trg_t_GC_SanPham_Log
ON dbo.t_GC_SanPham
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_SanPham_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_GC_SanPham_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_SanPham_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
GO
IF OBJECT_ID('t_KDT_GC_NguyenPhuLieu_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_NguyenPhuLieu_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_NguyenPhuLieu_Log]
(
[HopDong_ID] [bigint]  NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 6) NOT NULL,
[STTHang] [int] NOT NULL,
[DonGia] [numeric] (18, 5) NULL ,
[GhiChu] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_GC_NguyenPhuLieu_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_NguyenPhuLieu_Log
GO

CREATE TRIGGER trg_t_KDT_GC_NguyenPhuLieu_Log
ON dbo.t_KDT_GC_NguyenPhuLieu
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_GC_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_GC_NguyenPhuLieu_Log') IS NOT NULL
DROP TABLE dbo.t_GC_NguyenPhuLieu_Log
GO
CREATE TABLE [dbo].[t_GC_NguyenPhuLieu_Log]
(
[HopDong_ID] [bigint] NOT NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 6) NOT NULL,
[SoLuongDaNhap] [numeric] (18, 6) NULL,
[SoLuongDaDung] [numeric] (18, 6) NULL,
[SoLuongCungUng] [numeric] (18, 6) NULL,
[TongNhuCau] [numeric] (18, 6) NULL,
[STTHang] [int] NOT NULL,
[TrangThai] [int] NOT NULL,
[DonGia] [numeric] (18, 5) NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_GC_NguyenPhuLieu_Log') IS NOT NULL
DROP TRIGGER trg_t_GC_NguyenPhuLieu_Log
GO

CREATE TRIGGER trg_t_GC_NguyenPhuLieu_Log
ON dbo.t_GC_NguyenPhuLieu
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_GC_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

GO
IF OBJECT_ID('t_KDT_GC_HangMau_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_HangMau_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_HangMau_Log]
(
[HopDong_ID] [bigint]  NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 5) NOT NULL,
[STTHang] [int] NOT NULL,
[GhiChu] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_GC_HangMau_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_HangMau_Log
GO

CREATE TRIGGER trg_t_KDT_GC_HangMau_Log
ON dbo.t_KDT_GC_HangMau
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_HangMau_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_GC_HangMau_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_HangMau_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_GC_HangMau_Log') IS NOT NULL
DROP TABLE dbo.t_GC_HangMau_Log
GO
CREATE TABLE [dbo].[t_GC_HangMau_Log]
(
[HopDong_ID] [bigint] NOT NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 6) NOT NULL,
[SoLuongDaNhap] [numeric] (18, 6) NULL,
[SoLuongDaDung] [numeric] (18, 6) NULL,
[SoLuongCungUng] [numeric] (18, 6) NULL,
[TongNhuCau] [numeric] (18, 6) NULL,
[STTHang] [int] NOT NULL,
[TrangThai] [int] NOT NULL,
[DonGia] [numeric] (18, 5) NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_GC_HangMau_Log') IS NOT NULL
DROP TRIGGER trg_t_GC_HangMau_Log
GO

CREATE TRIGGER trg_t_GC_HangMau_Log
ON dbo.t_GC_HangMau
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_HangMau_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_GC_HangMau_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_HangMau_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_GC_DinhMucDangKy_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_DinhMucDangKy_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_DinhMucDangKy_Log]
(
[ID] [bigint]  NULL,
[SoTiepNhan] [bigint] NOT NULL,
[TrangThaiXuLy] [int] NOT NULL,
[NgayTiepNhan] [datetime] NULL,
[ID_HopDong] [bigint] NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUIDSTR] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeXuatKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LyDoSua] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionStatus] [int] NULL,
[GuidReference] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamTN] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HUONGDAN] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLuong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Huongdan_PL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiDinhMuc] [int] NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_GC_DinhMucDangKy_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_DinhMucDangKy_Log
GO

CREATE TRIGGER trg_t_KDT_GC_DinhMucDangKy_Log
ON dbo.t_KDT_GC_DinhMucDangKy
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_DinhMucDangKy_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_GC_DinhMucDangKy_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_DinhMucDangKy_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_GC_DinhMuc_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_DinhMuc_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_DinhMuc_Log]
(
[ID] [bigint]  NULL ,
[MaSanPham] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNguyenPhuLieu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DinhMucSuDung] [numeric] (18, 8) NOT NULL,
[TyLeHaoHut] [numeric] (18, 8) NOT NULL,
[GhiChu] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STTHang] [int] NOT NULL,
[Master_ID] [bigint] NOT NULL,
[NPL_TuCungUng] [numeric] (18, 8) NOT NULL,
[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNPL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDDSX] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_GC_DinhMuc_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_DinhMuc_Log
GO

CREATE TRIGGER trg_t_KDT_GC_DinhMuc_Log
ON dbo.t_KDT_GC_DinhMuc
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_DinhMuc_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_GC_DinhMuc_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_DinhMuc_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_GC_DinhMuc_Log') IS NOT NULL
DROP TABLE dbo.t_GC_DinhMuc_Log
GO
CREATE TABLE [dbo].[t_GC_DinhMuc_Log]
(
[HopDong_ID] [bigint] NOT NULL,
[MaSanPham] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNguyenPhuLieu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DinhMucSuDung] [numeric] (18, 8) NOT NULL,
[TyLeHaoHut] [numeric] (18, 8) NOT NULL,
[STTHang] [int] NOT NULL,
[NPL_TuCungUng] [numeric] (18, 6) NOT NULL,
[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNPL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_GC_DinhMuc_Log') IS NOT NULL
DROP TRIGGER trg_t_GC_DinhMuc_Log
GO
CREATE TRIGGER trg_t_GC_DinhMuc_Log
ON dbo.t_GC_DinhMuc
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_DinhMuc_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_GC_DinhMuc_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_DinhMuc_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_GC_PhuKienDangKy_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_PhuKienDangKy_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_PhuKienDangKy_Log]
(
[SoTiepNhan] [bigint] NULL,
[NgayTiepNhan] [datetime] NULL,
[TrangThaiXuLy] [int] NOT NULL,
[NgayPhuKien] [datetime] NOT NULL,
[NguoiDuyet] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VanBanChoPhep] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GhiChu] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoPhuKien] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HopDong_ID] [bigint] NOT NULL,
[ID] [bigint] NOT NULL ,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUIDSTR] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeXuatKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LyDoSua] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionStatus] [int] NULL,
[GuidReference] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamDK] [smallint] NULL,
[HUONGDAN] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLuong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HuongdanPL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

GO
IF OBJECT_ID('trg_t_KDT_GC_PhuKienDangKy_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_PhuKienDangKy_Log
GO
CREATE TRIGGER trg_t_KDT_GC_PhuKienDangKy_Log
ON dbo.t_KDT_GC_PhuKienDangKy
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_PhuKienDangKy_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_GC_PhuKienDangKy_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_GC_PhuKienDangKy_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END

GO
IF OBJECT_ID('t_KDT_GC_HangPhuKien_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_HangPhuKien_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_HangPhuKien_Log]
(
[ID] [bigint] NOT NULL,
[MaHang] [varchar] (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenHang] [nvarchar] (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ThongTinCu] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (18, 5) NOT NULL,
[DonGia] [float] NOT NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGia] [float] NOT NULL,
[NguyenTe_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STTHang] [int] NOT NULL,
[TinhTrang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Master_ID] [bigint] NOT NULL,
[NhomSP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_GC_HangPhuKien_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_HangPhuKien_Log
GO
CREATE TRIGGER trg_t_KDT_GC_HangPhuKien_Log
ON dbo.t_KDT_GC_HangPhuKien
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
    		INSERT INTO t_KDT_GC_HangPhuKien_Log
			SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
	   		INSERT INTO t_KDT_GC_HangPhuKien_Log
			SELECT *,GETDATE(),'Updated' FROM Inserted     
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
    		INSERT INTO t_KDT_GC_HangPhuKien_Log
			SELECT *,GETDATE(),'Deleted' FROM Deleted 
	END
END
GO


GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '28.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('28.1',GETDATE(), N'CẬP NHẬT GHI LOG  HỢP ĐỒNG - ĐỊNH MỨC - PHỤ KIỆN')
END