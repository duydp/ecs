using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_SXXK_BKNPLCungUng 
	{
        private List<KDT_SXXK_BKNPLCungUng_Detail> _ListNPL_Detail = new List<KDT_SXXK_BKNPLCungUng_Detail>();

        public List<KDT_SXXK_BKNPLCungUng_Detail> Npl_Detail_List
        {
            set { this._ListNPL_Detail = value; }
            get { return this._ListNPL_Detail; }
        }

        public static KDT_SXXK_BKNPLCungUng LoadBy(int lanthanhly,string soToKhai , string masanPham, int soTT,long Master_id)
        {
            string where = string.Format(" LanThanhLy= {0} and SoToKhaiXuat='{1}' and MaSanPham='{2}' and SoTTDongHang= {3} and Master_ID= {4} ", lanthanhly, soToKhai, masanPham, soTT, Master_id);
            List<KDT_SXXK_BKNPLCungUng> list = KDT_SXXK_BKNPLCungUng.SelectCollectionDynamic(where, "");
            try
            {
                if (list.Count > 0)
                {
                    KDT_SXXK_BKNPLCungUng BKNPL = new KDT_SXXK_BKNPLCungUng();
                    BKNPL = list[0];
                    List<KDT_SXXK_BKNPLCungUng_Detail> BKDetail = KDT_SXXK_BKNPLCungUng_Detail.SelectCollectionDynamic("Master_id = " + BKNPL.ID, null);
                    foreach (KDT_SXXK_BKNPLCungUng_Detail item in BKDetail)
                    {
                        BKNPL.Npl_Detail_List.Add(item);
                    }
                    return BKNPL;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                return null;
            }
        
        }
        public static KDT_SXXK_BKNPLCungUng LoadID(long id)
        {
           
            List<KDT_SXXK_BKNPLCungUng> list = KDT_SXXK_BKNPLCungUng.SelectCollectionDynamic("id = "+id, "");
            try
            {
                if (list.Count > 0)
                {
                    KDT_SXXK_BKNPLCungUng BKNPL = new KDT_SXXK_BKNPLCungUng();
                    BKNPL = list[0];
                    List<KDT_SXXK_BKNPLCungUng_Detail> BKDetail = KDT_SXXK_BKNPLCungUng_Detail.SelectCollectionDynamic("Master_id = " + BKNPL.ID, null);
                    foreach (KDT_SXXK_BKNPLCungUng_Detail item in BKDetail)
                    {
                        BKNPL.Npl_Detail_List.Add(item);
                    }
                    return BKNPL;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public bool InsertUpdatFull()
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update(transaction);
                    foreach (KDT_SXXK_BKNPLCungUng_Detail item in this.Npl_Detail_List)
                    {
                        item.Master_id = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                    throw;
                }
            }
            return ret;
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            KDT_SXXK_BKNPLCungUng_Detail.DeleteDynamic("Master_ID = " + id);
            this.Delete();
        }
        
	}	
}