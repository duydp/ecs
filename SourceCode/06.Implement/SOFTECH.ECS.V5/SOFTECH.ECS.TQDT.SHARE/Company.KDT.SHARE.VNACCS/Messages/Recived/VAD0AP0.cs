using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAD0AP0 : BasicVNACC
    {
        public List<VAD0AP0_HANG> HangMD { get; set; }
        public void LoadVAD0AP0(string strResult)
        {
            try
            {

                this.GetObject<VAD0AP0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD0AP0, false, VAD0AP0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD0AP0.TongSoByte);
                while (true)
                {
                    VAD0AP0_HANG vad1agHang = new VAD0AP0_HANG();
                    vad1agHang.GetObject<VAD0AP0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD0AP0_HANG", false, VAD0AP0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAD0AP0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD0AP0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD0AP0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAD0AP0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }


    }

}
