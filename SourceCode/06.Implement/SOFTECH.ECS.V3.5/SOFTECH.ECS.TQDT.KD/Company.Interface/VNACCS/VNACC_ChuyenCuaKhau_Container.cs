﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{

    public partial class VNACC_ChuyenCuaKhau_Container : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiVanChuyen TKVC;
        public KDT_VNACC_TKVC_Container Cont = null;
        bool isAddNew = false;
        public VNACC_ChuyenCuaKhau_Container()
        {

            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclarationOnTransportation.OLA.ToString());
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_TKVC_Container> ItemColl = new List<KDT_VNACC_TKVC_Container>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_TKVC_Container)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_TKVC_Container item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKVC.ContainerCollection.Remove(item);
                    }

                    grList.DataSource = TKVC.ContainerCollection;
                    try { grList.Refetch(); }
                    catch { grList.Refresh(); }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
       
        private void btnLuu_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;


                if (Cont == null)
                {
                    Cont = new KDT_VNACC_TKVC_Container();
                    isAddNew = true;
                }
                GetContainer();
                if (isAddNew)
                    TKVC.ContainerCollection.Add(Cont);
                grList.DataSource = TKVC.ContainerCollection;
                grList.Refetch();
                Cont = new KDT_VNACC_TKVC_Container();
                //SetHangMD();
                Cont = null;
                // txtSoLuong.Text = "0";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        private void GetContainer()
        {
            try
            {
                Cont.SoHieuContainer = txtSoHieuContainer.Text;
                Cont.SoDongHangTrenTK = txtSoDongHangTrenTK.Text;
                Cont.SoSeal1 = txtSoSeal1.Text;
                Cont.SoSeal2 = txtSoSeal2.Text;
                Cont.SoSeal3 = txtSoSeal3.Text;
                Cont.SoSeal4 = txtSoSeal4.Text;
                Cont.SoSeal5 = txtSoSeal5.Text;
                Cont.SoSeal6 = txtSoSeal6.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void SetContainer()
        {
            try
            {
                txtSoHieuContainer.Text = Cont.SoHieuContainer;
                txtSoDongHangTrenTK.Text = Cont.SoDongHangTrenTK;
                txtSoSeal1.Text = Cont.SoSeal1;
                txtSoSeal2.Text = Cont.SoSeal2;
                txtSoSeal3.Text = Cont.SoSeal3;
                txtSoSeal4.Text = Cont.SoSeal4;
                txtSoSeal5.Text = Cont.SoSeal5;
                txtSoSeal6.Text = Cont.SoSeal6;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        private void VNACC_ChuyenCuaKhau_Container_Load(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                SetIDControl();
                grList.DataSource = TKVC.ContainerCollection;
                if (TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
                {
                    btnLuu.Enabled = false;
                    btnXoa.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }

        private void grList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    try
                    {
                        Cursor = Cursors.WaitCursor;

                        Cont = (KDT_VNACC_TKVC_Container)grList.CurrentRow.DataRow;
                        SetContainer();
                        isAddNew = false;

                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                    }
                    finally { Cursor = Cursors.Default; }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private bool SetIDControl()
        {
            bool isValid = true;
            try
            {
                
                txtSoHieuContainer.Tag = "CNO";
                txtSoDongHangTrenTK.Tag = "RNO";
                txtSoSeal1.Tag = "SE_";
                txtSoSeal2.Tag = "SE_";
                txtSoSeal3.Tag = "SE_";
                txtSoSeal4.Tag = "SE_";
                txtSoSeal5.Tag = "SE_";
                txtSoSeal6.Tag = "SE_";

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }
    }
}
