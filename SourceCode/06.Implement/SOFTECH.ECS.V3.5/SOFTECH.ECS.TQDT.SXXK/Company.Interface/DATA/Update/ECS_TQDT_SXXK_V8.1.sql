
/****** Object:  Table [dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]    Script Date: 04/06/2013 08:31:00 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NPL_ID] [bigint] NOT NULL,
	[Ma] [varchar](50) NOT NULL,
	[NPLChinh] [bit] NOT NULL,
 CONSTRAINT [PK_t_KDT_SXXK_NguyenPhuLieuBoSung] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung] ADD  CONSTRAINT [DF_t_KDT_SXXK_NguyenPhuLieuBoSung_NPLChinh]  DEFAULT ((0)) FOR [NPLChinh]

END

GO



/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectAll]    Script Date: 04/06/2013 08:32:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectDynamic]    Script Date: 04/06/2013 08:32:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectBy_NPL_ID]    Script Date: 04/06/2013 08:32:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectBy_NPL_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectBy_NPL_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Load]    Script Date: 04/06/2013 08:32:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]    Script Date: 04/06/2013 08:32:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteBy_NPL_ID]    Script Date: 04/06/2013 08:32:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteBy_NPL_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteBy_NPL_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Delete]    Script Date: 04/06/2013 08:32:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_InsertUpdate]    Script Date: 04/06/2013 08:32:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Update]    Script Date: 04/06/2013 08:32:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Insert]    Script Date: 04/06/2013 08:32:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Insert]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectAll]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[NPL_ID],
	[Ma],
	[NPLChinh]
FROM
	[dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectDynamic]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[NPL_ID],
	[Ma],
	[NPLChinh]
FROM [dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectBy_NPL_ID]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectBy_NPL_ID]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_SelectBy_NPL_ID]
	@NPL_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[NPL_ID],
	[Ma],
	[NPLChinh]
FROM
	[dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]
WHERE
	[NPL_ID] = @NPL_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Load]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Load]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[NPL_ID],
	[Ma],
	[NPLChinh]
FROM
	[dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteBy_NPL_ID]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteBy_NPL_ID]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_DeleteBy_NPL_ID]
	@NPL_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]
WHERE
	[NPL_ID] = @NPL_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Delete]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Delete]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_InsertUpdate]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_InsertUpdate]
	@ID bigint,
	@NPL_ID bigint,
	@Ma varchar(50),
	@NPLChinh bit
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung] 
		SET
			[NPL_ID] = @NPL_ID,
			[Ma] = @Ma,
			[NPLChinh] = @NPLChinh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]
		(
			[NPL_ID],
			[Ma],
			[NPLChinh]
		)
		VALUES 
		(
			@NPL_ID,
			@Ma,
			@NPLChinh
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Update]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Update]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Update]
	@ID bigint,
	@NPL_ID bigint,
	@Ma varchar(50),
	@NPLChinh bit
AS

UPDATE
	[dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]
SET
	[NPL_ID] = @NPL_ID,
	[Ma] = @Ma,
	[NPLChinh] = @NPLChinh
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Insert]    Script Date: 04/06/2013 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Insert]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuBoSung_Insert]
	@NPL_ID bigint,
	@Ma varchar(50),
	@NPLChinh bit,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_NguyenPhuLieuBoSung]
(
	[NPL_ID],
	[Ma],
	[NPLChinh]
)
VALUES 
(
	@NPL_ID,
	@Ma,
	@NPLChinh
)

SET @ID = SCOPE_IDENTITY()


GO



/****** Object:  Table [dbo].[t_SXXK_NguyenPhuLieuBoSung]    Script Date: 04/06/2013 08:33:38 ******/
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_SXXK_NguyenPhuLieuBoSung]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[t_SXXK_NguyenPhuLieuBoSung](
	[MaHaiQuan] [char](6) NOT NULL,
	[MaDoanhNghiep] [varchar](14) NOT NULL,
	[Ma] [varchar](30) NOT NULL,
	[NPLChinh] [bit] NOT NULL,
 CONSTRAINT [PK_t_SXXK_NguyenPhuLieuBoSung] PRIMARY KEY CLUSTERED 
(
	[MaHaiQuan] ASC,
	[MaDoanhNghiep] ASC,
	[Ma] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[t_SXXK_NguyenPhuLieuBoSung] ADD  CONSTRAINT [DF_t_SXXK_NguyenPhuLieuBoSung_NPLChinh]  DEFAULT ((0)) FOR [NPLChinh]

END

GO



/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectDynamic]    Script Date: 04/06/2013 08:34:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectAll]    Script Date: 04/06/2013 08:34:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_Load]    Script Date: 04/06/2013 08:34:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieuBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]    Script Date: 04/06/2013 08:34:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_Delete]    Script Date: 04/06/2013 08:34:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieuBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_InsertUpdate]    Script Date: 04/06/2013 08:34:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieuBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_Update]    Script Date: 04/06/2013 08:34:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieuBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_Insert]    Script Date: 04/06/2013 08:34:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieuBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_Insert]
GO


/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectDynamic]    Script Date: 04/06/2013 08:34:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[NPLChinh]
FROM [dbo].[t_SXXK_NguyenPhuLieuBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectAll]    Script Date: 04/06/2013 08:34:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[NPLChinh]
FROM
	[dbo].[t_SXXK_NguyenPhuLieuBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_Load]    Script Date: 04/06/2013 08:34:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieuBoSung_Load]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_Load]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[NPLChinh]
FROM
	[dbo].[t_SXXK_NguyenPhuLieuBoSung]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma

GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]    Script Date: 04/06/2013 08:34:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_NguyenPhuLieuBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_Delete]    Script Date: 04/06/2013 08:34:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieuBoSung_Delete]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_Delete]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30)
AS

DELETE FROM 
	[dbo].[t_SXXK_NguyenPhuLieuBoSung]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma


GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_InsertUpdate]    Script Date: 04/06/2013 08:34:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieuBoSung_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_InsertUpdate]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@NPLChinh bit
AS
IF EXISTS(SELECT [MaHaiQuan], [MaDoanhNghiep], [Ma] FROM [dbo].[t_SXXK_NguyenPhuLieuBoSung] WHERE [MaHaiQuan] = @MaHaiQuan AND [MaDoanhNghiep] = @MaDoanhNghiep AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_NguyenPhuLieuBoSung] 
		SET
			[NPLChinh] = @NPLChinh
		WHERE
			[MaHaiQuan] = @MaHaiQuan
			AND [MaDoanhNghiep] = @MaDoanhNghiep
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_NguyenPhuLieuBoSung]
	(
			[MaHaiQuan],
			[MaDoanhNghiep],
			[Ma],
			[NPLChinh]
	)
	VALUES
	(
			@MaHaiQuan,
			@MaDoanhNghiep,
			@Ma,
			@NPLChinh
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_Update]    Script Date: 04/06/2013 08:34:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieuBoSung_Update]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_Update]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@NPLChinh bit
AS

UPDATE
	[dbo].[t_SXXK_NguyenPhuLieuBoSung]
SET
	[NPLChinh] = @NPLChinh
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma


GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_NguyenPhuLieuBoSung_Insert]    Script Date: 04/06/2013 08:34:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieuBoSung_Insert]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieuBoSung_Insert]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@NPLChinh bit
AS
INSERT INTO [dbo].[t_SXXK_NguyenPhuLieuBoSung]
(
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[NPLChinh]
)
VALUES
(
	@MaHaiQuan,
	@MaDoanhNghiep,
	@Ma,
	@NPLChinh
)


GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '8.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('8.1',GETDATE(), N'Cap nhat NPL bo sung')
END