﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class PreviousCustomsDocument
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("number")]
        public string Number { get; set; }
    }
}
