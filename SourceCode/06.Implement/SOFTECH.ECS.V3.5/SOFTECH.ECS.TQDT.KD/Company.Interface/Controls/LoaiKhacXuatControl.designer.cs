﻿namespace Company.Interface.Controls
{
    partial class LoaiHinhKhacXuatControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout cbTen_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoaiHinhKhacXuatControl));
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbTen = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.ds = new System.Data.DataSet();
            this.dtLoaiHinhMauDich = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.cbTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMa
            // 
            this.txtMa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa.Location = new System.Drawing.Point(0, 0);
            this.txtMa.Name = "txtMa";
            this.txtMa.ReadOnly = true;
            this.txtMa.Size = new System.Drawing.Size(174, 21);
            this.txtMa.TabIndex = 0;
            this.txtMa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMa.Leave += new System.EventHandler(this.txtMaLoaiHinh_Leave);
            // 
            // cbTen
            // 
            this.cbTen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTen.ComboStyle = Janus.Windows.GridEX.ComboStyle.DropDownList;
            this.cbTen.DataMember = "LoaiHinhMauDich";
            this.cbTen.DataSource = this.ds;
            cbTen_DesignTimeLayout.LayoutString = resources.GetString("cbTen_DesignTimeLayout.LayoutString");
            this.cbTen.DesignTimeLayout = cbTen_DesignTimeLayout;
            this.cbTen.DisplayMember = "Ten";
            this.cbTen.Location = new System.Drawing.Point(0, 27);
            this.cbTen.Name = "cbTen";
            this.cbTen.SelectedIndex = -1;
            this.cbTen.SelectedItem = null;
            this.cbTen.Size = new System.Drawing.Size(174, 21);
            this.cbTen.TabIndex = 1;
            this.cbTen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbTen.ValueMember = "ID";
            this.cbTen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbTen.ValueChanged += new System.EventHandler(this.cbLoaiHinhMauDich_ValueChanged);
            // 
            // ds
            // 
            this.ds.DataSetName = "DuLieuChuan";
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtLoaiHinhMauDich});
            // 
            // dtLoaiHinhMauDich
            // 
            this.dtLoaiHinhMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2});
            this.dtLoaiHinhMauDich.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "ID"}, true)});
            this.dtLoaiHinhMauDich.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
            this.dtLoaiHinhMauDich.TableName = "LoaiHinhMauDich";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "ID";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "Ten";
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.cbTen;
            this.rfvTen.ErrorMessage = "\"Loại hình mậu dịch\" không được bỏ trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            // 
            // LoaiHinhKhacXuatControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.cbTen);
            this.Controls.Add(this.txtMa);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "LoaiHinhKhacXuatControl";
            this.Size = new System.Drawing.Size(174, 49);
            ((System.ComponentModel.ISupportInitialize)(this.cbTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtMa;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbTen;
        private System.Data.DataSet ds;
        private System.Data.DataTable dtLoaiHinhMauDich;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
    }
}