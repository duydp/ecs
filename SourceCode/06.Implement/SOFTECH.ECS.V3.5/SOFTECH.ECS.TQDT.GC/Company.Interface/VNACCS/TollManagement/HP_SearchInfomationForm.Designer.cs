﻿namespace Company.Interface.VNACCS.TollManagement
{
    partial class HP_SearchInfomationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HP_SearchInfomationForm));
            this.grbHangRoi = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbbLoaiHangHoa = new Janus.Windows.EditControls.UIComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSoBienLai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTongTien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cbbTrangThai = new Janus.Windows.EditControls.UIComboBox();
            this.cbbChoPhep = new Janus.Windows.EditControls.UIComboBox();
            this.cbbNhomLoaiHinh = new Janus.Windows.EditControls.UIComboBox();
            this.ctrMaPhuongThucVT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaLoaiHinh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDDLuuKho = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayToKhai = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayBienLai = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoBienLaiThu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtQuyenBienLai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoSerial = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMauBienLai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbHangRoi)).BeginInit();
            this.grbHangRoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 728), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 728);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 704);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 704);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.grbHangRoi);
            this.grbMain.Size = new System.Drawing.Size(1166, 728);
            // 
            // grbHangRoi
            // 
            this.grbHangRoi.BackColor = System.Drawing.Color.Transparent;
            this.grbHangRoi.Controls.Add(this.btnSearch);
            this.grbHangRoi.Controls.Add(this.label8);
            this.grbHangRoi.Controls.Add(this.label5);
            this.grbHangRoi.Controls.Add(this.cbbLoaiHangHoa);
            this.grbHangRoi.Controls.Add(this.label6);
            this.grbHangRoi.Controls.Add(this.txtSoBienLai);
            this.grbHangRoi.Controls.Add(this.txtSoContainer);
            this.grbHangRoi.Controls.Add(this.label29);
            this.grbHangRoi.Controls.Add(this.txtSoVanDon);
            this.grbHangRoi.Controls.Add(this.editBox1);
            this.grbHangRoi.Controls.Add(this.label9);
            this.grbHangRoi.Dock = System.Windows.Forms.DockStyle.Top;
            this.grbHangRoi.Location = new System.Drawing.Point(0, 0);
            this.grbHangRoi.Name = "grbHangRoi";
            this.grbHangRoi.Size = new System.Drawing.Size(1166, 96);
            this.grbHangRoi.TabIndex = 0;
            this.grbHangRoi.Text = "Thông tin tìm kiếm";
            this.grbHangRoi.VisualStyleManager = this.vsmMain;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(679, 56);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(172, 23);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "Tra cứu";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(307, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Số biên lai :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(307, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số vận đơn :";
            // 
            // cbbLoaiHangHoa
            // 
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Hàng container";
            uiComboBoxItem8.Value = "100";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Hàng lỏng, hàng rời";
            uiComboBoxItem9.Value = "101";
            this.cbbLoaiHangHoa.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem8,
            uiComboBoxItem9});
            this.cbbLoaiHangHoa.Location = new System.Drawing.Point(96, 22);
            this.cbbLoaiHangHoa.Name = "cbbLoaiHangHoa";
            this.cbbLoaiHangHoa.Size = new System.Drawing.Size(177, 21);
            this.cbbLoaiHangHoa.TabIndex = 1;
            this.cbbLoaiHangHoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(583, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số tờ khai  :";
            // 
            // txtSoBienLai
            // 
            this.txtSoBienLai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoBienLai.Location = new System.Drawing.Point(381, 22);
            this.txtSoBienLai.Name = "txtSoBienLai";
            this.txtSoBienLai.Size = new System.Drawing.Size(177, 21);
            this.txtSoBienLai.TabIndex = 2;
            this.txtSoBienLai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoContainer
            // 
            this.txtSoContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoContainer.Location = new System.Drawing.Point(96, 58);
            this.txtSoContainer.Name = "txtSoContainer";
            this.txtSoContainer.Size = new System.Drawing.Size(177, 21);
            this.txtSoContainer.TabIndex = 4;
            this.txtSoContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(10, 62);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(74, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Số container :";
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon.Location = new System.Drawing.Point(381, 58);
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(177, 21);
            this.txtSoVanDon.TabIndex = 5;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // editBox1
            // 
            this.editBox1.BackColor = System.Drawing.Color.White;
            this.editBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox1.Location = new System.Drawing.Point(680, 21);
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(171, 22);
            this.editBox1.TabIndex = 3;
            this.editBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Loại hàng hóa :";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 682);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1166, 46);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1074, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 27;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtTongTien);
            this.uiGroupBox3.Controls.Add(this.cbbTrangThai);
            this.uiGroupBox3.Controls.Add(this.cbbChoPhep);
            this.uiGroupBox3.Controls.Add(this.cbbNhomLoaiHinh);
            this.uiGroupBox3.Controls.Add(this.ctrMaPhuongThucVT);
            this.uiGroupBox3.Controls.Add(this.ctrMaLoaiHinh);
            this.uiGroupBox3.Controls.Add(this.ctrMaDDLuuKho);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox3.Controls.Add(this.clcNgayToKhai);
            this.uiGroupBox3.Controls.Add(this.clcNgayBienLai);
            this.uiGroupBox3.Controls.Add(this.txtDiaChi);
            this.uiGroupBox3.Controls.Add(this.txtGhiChu);
            this.uiGroupBox3.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox3.Controls.Add(this.label24);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.label13);
            this.uiGroupBox3.Controls.Add(this.txtSoBienLaiThu);
            this.uiGroupBox3.Controls.Add(this.txtQuyenBienLai);
            this.uiGroupBox3.Controls.Add(this.txtSoSerial);
            this.uiGroupBox3.Controls.Add(this.txtMauBienLai);
            this.uiGroupBox3.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox3.Controls.Add(this.label12);
            this.uiGroupBox3.Controls.Add(this.label15);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.label23);
            this.uiGroupBox3.Controls.Add(this.label22);
            this.uiGroupBox3.Controls.Add(this.label14);
            this.uiGroupBox3.Controls.Add(this.label20);
            this.uiGroupBox3.Controls.Add(this.label19);
            this.uiGroupBox3.Controls.Add(this.label18);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 96);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1166, 356);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Kết quả tìm kiếm";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtTongTien
            // 
            this.txtTongTien.DecimalDigits = 20;
            this.txtTongTien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTien.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongTien.Location = new System.Drawing.Point(129, 112);
            this.txtTongTien.MaxLength = 15;
            this.txtTongTien.Name = "txtTongTien";
            this.txtTongTien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTien.Size = new System.Drawing.Size(288, 21);
            this.txtTongTien.TabIndex = 13;
            this.txtTongTien.Text = "0";
            this.txtTongTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cbbTrangThai
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "I : Phát hành";
            uiComboBoxItem1.Value = "I";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "C : Đã hủy";
            uiComboBoxItem2.Value = "C";
            this.cbbTrangThai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbTrangThai.Location = new System.Drawing.Point(900, 73);
            this.cbbTrangThai.Name = "cbbTrangThai";
            this.cbbTrangThai.ReadOnly = true;
            this.cbbTrangThai.Size = new System.Drawing.Size(202, 21);
            this.cbbTrangThai.TabIndex = 12;
            this.cbbTrangThai.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbChoPhep
            // 
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "0 : Chưa đủ điều kiện";
            uiComboBoxItem3.Value = "0";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "1 : Đã đủ điều kiện lấy hàng";
            uiComboBoxItem4.Value = "1";
            this.cbbChoPhep.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbbChoPhep.Location = new System.Drawing.Point(129, 312);
            this.cbbChoPhep.Name = "cbbChoPhep";
            this.cbbChoPhep.ReadOnly = true;
            this.cbbChoPhep.Size = new System.Drawing.Size(200, 21);
            this.cbbChoPhep.TabIndex = 23;
            this.cbbChoPhep.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbChoPhep.SelectedIndexChanged += new System.EventHandler(this.cbbChoPhep_SelectedIndexChanged);
            // 
            // cbbNhomLoaiHinh
            // 
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Hàng tạm nhập, tái xuất, hàng chuyển khẩu, hàng gửi kho ngoại quan";
            uiComboBoxItem5.Value = "TF001";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Đối với hàng quá cảnh";
            uiComboBoxItem6.Value = "TF002";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Đối với hàng hóa nhập khẩu, hàng hóa xuất khẩu";
            uiComboBoxItem7.Value = "TF003";
            this.cbbNhomLoaiHinh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7});
            this.cbbNhomLoaiHinh.Location = new System.Drawing.Point(129, 272);
            this.cbbNhomLoaiHinh.Name = "cbbNhomLoaiHinh";
            this.cbbNhomLoaiHinh.ReadOnly = true;
            this.cbbNhomLoaiHinh.Size = new System.Drawing.Size(200, 21);
            this.cbbNhomLoaiHinh.TabIndex = 20;
            this.cbbNhomLoaiHinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // ctrMaPhuongThucVT
            // 
            this.ctrMaPhuongThucVT.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaPhuongThucVT.Appearance.Options.UseBackColor = true;
            this.ctrMaPhuongThucVT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E005;
            this.ctrMaPhuongThucVT.Code = "";
            this.ctrMaPhuongThucVT.ColorControl = System.Drawing.SystemColors.Info;
            this.ctrMaPhuongThucVT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhuongThucVT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhuongThucVT.IsOnlyWarning = false;
            this.ctrMaPhuongThucVT.IsValidate = true;
            this.ctrMaPhuongThucVT.Location = new System.Drawing.Point(562, 272);
            this.ctrMaPhuongThucVT.Name = "ctrMaPhuongThucVT";
            this.ctrMaPhuongThucVT.Name_VN = "";
            this.ctrMaPhuongThucVT.SetOnlyWarning = false;
            this.ctrMaPhuongThucVT.SetValidate = false;
            this.ctrMaPhuongThucVT.ShowColumnCode = true;
            this.ctrMaPhuongThucVT.ShowColumnName = true;
            this.ctrMaPhuongThucVT.Size = new System.Drawing.Size(211, 21);
            this.ctrMaPhuongThucVT.TabIndex = 21;
            this.ctrMaPhuongThucVT.TagName = "";
            this.ctrMaPhuongThucVT.Where = null;
            this.ctrMaPhuongThucVT.WhereCondition = "";
            // 
            // ctrMaLoaiHinh
            // 
            this.ctrMaLoaiHinh.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaLoaiHinh.Appearance.Options.UseBackColor = true;
            this.ctrMaLoaiHinh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ctrMaLoaiHinh.Code = "";
            this.ctrMaLoaiHinh.ColorControl = System.Drawing.SystemColors.Info;
            this.ctrMaLoaiHinh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaLoaiHinh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaLoaiHinh.IsOnlyWarning = false;
            this.ctrMaLoaiHinh.IsValidate = true;
            this.ctrMaLoaiHinh.Location = new System.Drawing.Point(900, 234);
            this.ctrMaLoaiHinh.Name = "ctrMaLoaiHinh";
            this.ctrMaLoaiHinh.Name_VN = "";
            this.ctrMaLoaiHinh.SetOnlyWarning = false;
            this.ctrMaLoaiHinh.SetValidate = false;
            this.ctrMaLoaiHinh.ShowColumnCode = true;
            this.ctrMaLoaiHinh.ShowColumnName = true;
            this.ctrMaLoaiHinh.Size = new System.Drawing.Size(202, 21);
            this.ctrMaLoaiHinh.TabIndex = 19;
            this.ctrMaLoaiHinh.TagName = "";
            this.ctrMaLoaiHinh.Where = null;
            this.ctrMaLoaiHinh.WhereCondition = "";
            // 
            // ctrMaDDLuuKho
            // 
            this.ctrMaDDLuuKho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDDLuuKho.Appearance.Options.UseBackColor = true;
            this.ctrMaDDLuuKho.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDDLuuKho.Code = "";
            this.ctrMaDDLuuKho.ColorControl = System.Drawing.SystemColors.Info;
            this.ctrMaDDLuuKho.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDDLuuKho.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDDLuuKho.IsOnlyWarning = false;
            this.ctrMaDDLuuKho.IsValidate = true;
            this.ctrMaDDLuuKho.Location = new System.Drawing.Point(900, 272);
            this.ctrMaDDLuuKho.Name = "ctrMaDDLuuKho";
            this.ctrMaDDLuuKho.Name_VN = "";
            this.ctrMaDDLuuKho.SetOnlyWarning = false;
            this.ctrMaDDLuuKho.SetValidate = false;
            this.ctrMaDDLuuKho.ShowColumnCode = true;
            this.ctrMaDDLuuKho.ShowColumnName = false;
            this.ctrMaDDLuuKho.Size = new System.Drawing.Size(96, 21);
            this.ctrMaDDLuuKho.TabIndex = 22;
            this.ctrMaDDLuuKho.TagName = "";
            this.ctrMaDDLuuKho.Where = null;
            this.ctrMaDDLuuKho.WhereCondition = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã doanh nghiệp :";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.BackColor = System.Drawing.Color.White;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(129, 233);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.ReadOnly = true;
            this.txtSoToKhai.Size = new System.Drawing.Size(200, 22);
            this.txtSoToKhai.TabIndex = 17;
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayToKhai
            // 
            this.clcNgayToKhai.AutoSize = false;
            this.clcNgayToKhai.BackColor = System.Drawing.Color.White;
            this.clcNgayToKhai.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.clcNgayToKhai.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayToKhai.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcNgayToKhai.DropDownCalendar.Name = "";
            this.clcNgayToKhai.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayToKhai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayToKhai.Location = new System.Drawing.Point(562, 233);
            this.clcNgayToKhai.Name = "clcNgayToKhai";
            this.clcNgayToKhai.Nullable = true;
            this.clcNgayToKhai.NullButtonText = "Xóa";
            this.clcNgayToKhai.ReadOnly = true;
            this.clcNgayToKhai.ShowNullButton = true;
            this.clcNgayToKhai.Size = new System.Drawing.Size(167, 22);
            this.clcNgayToKhai.TabIndex = 18;
            this.clcNgayToKhai.TodayButtonText = "Hôm nay";
            this.clcNgayToKhai.Value = new System.DateTime(2019, 6, 12, 0, 0, 0, 0);
            this.clcNgayToKhai.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayBienLai
            // 
            this.clcNgayBienLai.AutoSize = false;
            this.clcNgayBienLai.BackColor = System.Drawing.Color.White;
            this.clcNgayBienLai.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.clcNgayBienLai.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayBienLai.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcNgayBienLai.DropDownCalendar.Name = "";
            this.clcNgayBienLai.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayBienLai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayBienLai.Location = new System.Drawing.Point(562, 72);
            this.clcNgayBienLai.Name = "clcNgayBienLai";
            this.clcNgayBienLai.Nullable = true;
            this.clcNgayBienLai.NullButtonText = "Xóa";
            this.clcNgayBienLai.ReadOnly = true;
            this.clcNgayBienLai.ShowNullButton = true;
            this.clcNgayBienLai.Size = new System.Drawing.Size(167, 22);
            this.clcNgayBienLai.TabIndex = 11;
            this.clcNgayBienLai.TodayButtonText = "Hôm nay";
            this.clcNgayBienLai.Value = new System.DateTime(2019, 6, 12, 0, 0, 0, 0);
            this.clcNgayBienLai.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.BackColor = System.Drawing.Color.White;
            this.txtDiaChi.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(129, 192);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.ReadOnly = true;
            this.txtDiaChi.Size = new System.Drawing.Size(973, 22);
            this.txtDiaChi.TabIndex = 16;
            this.txtDiaChi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(562, 311);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.ReadOnly = true;
            this.txtGhiChu.Size = new System.Drawing.Size(540, 22);
            this.txtGhiChu.TabIndex = 24;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.BackColor = System.Drawing.Color.White;
            this.txtTenDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(562, 151);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.ReadOnly = true;
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(540, 22);
            this.txtTenDoanhNghiep.TabIndex = 15;
            this.txtTenDoanhNghiep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(433, 315);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(102, 14);
            this.label24.TabIndex = 0;
            this.label24.Text = "Diễn giải chi tiết :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 315);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "Cho phép lấy hàng :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(14, 275);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "Nhóm loại hình :";
            // 
            // txtSoBienLaiThu
            // 
            this.txtSoBienLaiThu.BackColor = System.Drawing.Color.White;
            this.txtSoBienLaiThu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoBienLaiThu.Location = new System.Drawing.Point(129, 72);
            this.txtSoBienLaiThu.Name = "txtSoBienLaiThu";
            this.txtSoBienLaiThu.ReadOnly = true;
            this.txtSoBienLaiThu.Size = new System.Drawing.Size(288, 22);
            this.txtSoBienLaiThu.TabIndex = 10;
            this.txtSoBienLaiThu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoBienLaiThu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtQuyenBienLai
            // 
            this.txtQuyenBienLai.BackColor = System.Drawing.Color.White;
            this.txtQuyenBienLai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuyenBienLai.Location = new System.Drawing.Point(900, 32);
            this.txtQuyenBienLai.Name = "txtQuyenBienLai";
            this.txtQuyenBienLai.ReadOnly = true;
            this.txtQuyenBienLai.Size = new System.Drawing.Size(202, 22);
            this.txtQuyenBienLai.TabIndex = 9;
            this.txtQuyenBienLai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtQuyenBienLai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoSerial
            // 
            this.txtSoSerial.BackColor = System.Drawing.Color.White;
            this.txtSoSerial.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoSerial.Location = new System.Drawing.Point(562, 32);
            this.txtSoSerial.Name = "txtSoSerial";
            this.txtSoSerial.ReadOnly = true;
            this.txtSoSerial.Size = new System.Drawing.Size(211, 22);
            this.txtSoSerial.TabIndex = 8;
            this.txtSoSerial.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoSerial.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMauBienLai
            // 
            this.txtMauBienLai.BackColor = System.Drawing.Color.White;
            this.txtMauBienLai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauBienLai.Location = new System.Drawing.Point(129, 32);
            this.txtMauBienLai.Name = "txtMauBienLai";
            this.txtMauBienLai.ReadOnly = true;
            this.txtMauBienLai.Size = new System.Drawing.Size(288, 22);
            this.txtMauBienLai.TabIndex = 7;
            this.txtMauBienLai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMauBienLai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.BackColor = System.Drawing.Color.White;
            this.txtMaDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(129, 151);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.ReadOnly = true;
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(296, 22);
            this.txtMaDoanhNghiep.TabIndex = 14;
            this.txtMaDoanhNghiep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(433, 275);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 14);
            this.label12.TabIndex = 0;
            this.label12.Text = "Mã loại PTVC :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(779, 275);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 14);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mã địa điểm :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(779, 237);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 14);
            this.label11.TabIndex = 0;
            this.label11.Text = "Loại hình :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(779, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(112, 14);
            this.label23.TabIndex = 0;
            this.label23.Text = "Trạng thái biên lai :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(779, 36);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(92, 14);
            this.label22.TabIndex = 0;
            this.label22.Text = "Quyển biên lai :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(433, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 14);
            this.label14.TabIndex = 0;
            this.label14.Text = "Ngày biên lai thu :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(433, 36);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(125, 14);
            this.label20.TabIndex = 0;
            this.label20.Text = "Số Serial biên lai thu :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(14, 116);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 14);
            this.label19.TabIndex = 0;
            this.label19.Text = "Tổng tiền :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(14, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 14);
            this.label18.TabIndex = 0;
            this.label18.Text = "Số biên lai thu :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(14, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 14);
            this.label17.TabIndex = 0;
            this.label17.Text = "Mẫu biên lai thu :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(433, 237);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ngày tờ khai :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(14, 237);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Số tờ khai :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(14, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 14);
            this.label10.TabIndex = 0;
            this.label10.Text = "Địa chỉ :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(433, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên doanh nghiệp :";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 452);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1166, 230);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Danh mục khoản mục phí";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(3, 17);
            this.dgList.Margin = new System.Windows.Forms.Padding(0);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.Size = new System.Drawing.Size(1160, 210);
            this.dgList.TabIndex = 16;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // HP_SearchInfomationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1372, 734);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HP_SearchInfomationForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tra cứu biên lai";
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbHangRoi)).EndInit();
            this.grbHangRoi.ResumeLayout(false);
            this.grbHangRoi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIComboBox cbbChoPhep;
        private Janus.Windows.EditControls.UIComboBox cbbNhomLoaiHinh;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhuongThucVT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaLoaiHinh;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDDLuuKho;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayToKhai;

        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox grbHangRoi;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHangHoa;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoBienLai;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoContainer;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoBienLaiThu;
        private Janus.Windows.GridEX.EditControls.EditBox txtMauBienLai;
        private Janus.Windows.EditControls.UIComboBox cbbTrangThai;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayBienLai;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuyenBienLai;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSerial;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTien;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.EditControls.UIButton btnClose;

    }
}