using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAF5110 : BasicVNACC
    {
        public List<VAF5110_HANG> HangMD { get; set; }
        public void LoadVAF5110(string strResult)
        {
            try
            {

                this.GetObject<VAF5110>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAF5110, false, VAF5110.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAF5110.TongSoByte);
                while (true)
                {
                    VAF5110_HANG vad1agHang = new VAF5110_HANG();
                    vad1agHang.GetObject<VAF5110_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAF5110_HANG", false, VAF5110_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAF5110_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAF5110_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAF5110_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAF5110_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }

}
