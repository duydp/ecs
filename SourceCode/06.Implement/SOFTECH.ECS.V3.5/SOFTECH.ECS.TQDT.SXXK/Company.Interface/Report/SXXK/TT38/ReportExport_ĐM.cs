﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using Company.GC.BLL.KDT.GC;
using Company.BLL.KDT.SXXK;
namespace Company.Interface.Report.SXXK.TT38
{
    public partial class ReportExport_ĐM : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportExport_ĐM()
        {
            InitializeComponent();
        }
        public void BindReport(List<DinhMuc> DinhMucCollection, DateTime DateFrom, DateTime DateTo)
        {
            int k = 1;
            foreach (DinhMuc item in DinhMucCollection)
            {
                item.STT = k;
                item.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(item.DVT_ID);
                item.DVT = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(item.DVT);
                item.DinhMucSuDung = item.DinhMucSuDung;
                k++;
            }
            DetailReport.DataSource = DinhMucCollection;
            lblTuNgay.Text = DateFrom.ToString("dd/MM/yyyy");
            lblDenNgay.Text = DateTo.ToString("dd/MM/yyyy");
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            txtTenHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "TenSP");
            txtMaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSanPham");
            txtDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
            txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MaNguyenPhuLieu");
            txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
            txtDVTNPL.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
            txtLuongNLVTTT.DataBindings.Add("Text", DetailReport.DataSource, "DinhMucSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtTLHH.DataBindings.Add("Text", DetailReport.DataSource, "TyLeHaoHut", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "GhiChu");
        }
    }
}
