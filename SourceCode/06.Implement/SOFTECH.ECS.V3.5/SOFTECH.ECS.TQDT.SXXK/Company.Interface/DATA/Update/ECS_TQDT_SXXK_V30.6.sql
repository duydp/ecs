----------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NamDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	Case when MaLoaiHinh like ''%V%'' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = LoaiVanDon) else SoHopDong  end AS SoHopDong,
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[Xuat_NPL_SP],
	[ThanhLy],
	[NGAY_THN_THX],
	[MaDonViUT],
	[TrangThaiThanhKhoan],
	[ChungTu],
	[PhanLuong],
	[NgayHoanThanh],
	[TrangThai],
	[TrongLuongNet],
	[SoTienKhoan],
	[GhiChu],
	[HesoNhan]
FROM [dbo].[t_SXXK_ToKhaiMauDich] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

Go

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.6',GETDATE(), N' Cập nhật các [p_SXXK_ToKhaiMauDich_SelectDynamic]')
END	

