﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_HoaDon_PhanLoaiForm : BaseForm
    {
        public KDT_VNACC_HoaDon hoaDon;
        public KDT_VNACC_PhanLoaiHoaDon phanLoaiHD = null;
        public VNACC_HoaDon_PhanLoaiForm()
        {
            InitializeComponent();
        }

        private void VNACC_HoaDon_PhanLoaiForm_Load(object sender, EventArgs e)
        {
            grdChungTu.DataSource = hoaDon.PhanLoaiCollection;
        }


        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                 this.Cursor = Cursors.WaitCursor;

                bool isAddNew = false;
                if (phanLoaiHD == null)
                {
                    phanLoaiHD = new KDT_VNACC_PhanLoaiHoaDon();
                    isAddNew = true;
                }
                GetPhanLoai(phanLoaiHD);
                hoaDon.PhanLoaiCollection.Add(phanLoaiHD);
                grdChungTu.DataSource = hoaDon.PhanLoaiCollection;
                grdChungTu.Refetch();
                phanLoaiHD = new KDT_VNACC_PhanLoaiHoaDon();
                SetPhanLoai(phanLoaiHD);
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        private void GetPhanLoai(KDT_VNACC_PhanLoaiHoaDon phanLoai)
        {
            phanLoai.MaPhaLoai = txtMaPhanLoai.Text;
            phanLoai.So = txtSoChungTu.Text;
            phanLoai.NgayThangNam = clcNgayChungTu.Value;

        }
        private void SetPhanLoai(KDT_VNACC_PhanLoaiHoaDon phanLoai)
        {
            txtMaPhanLoai.Text = phanLoai.MaPhaLoai;
            txtSoChungTu.Text = phanLoai.So;
            clcNgayChungTu.Value = (phanLoai.NgayThangNam.Year > 1900) ? phanLoai.NgayThangNam : DateTime.Today;

        }

        private void grdChungTu_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = grdChungTu.SelectedItems;
           // List<KDT_VNACC_PhanLoaiHoaDon> hangColl = new List<KDT_VNACC_PhanLoaiHoaDon>();
            if (grdChungTu.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            phanLoaiHD = (KDT_VNACC_PhanLoaiHoaDon)items[0].GetRow().DataRow;
            SetPhanLoai(phanLoaiHD);
        }

        private void grdChungTu_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
                {
                this.Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = grdChungTu.SelectedItems;
                List<KDT_VNACC_PhanLoaiHoaDon> hangColl = new List<KDT_VNACC_PhanLoaiHoaDon>();
                if (grdChungTu.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa chứng từ này này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hangColl.Add((KDT_VNACC_PhanLoaiHoaDon)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_PhanLoaiHoaDon hmd in hangColl)
                    {
                        if (hmd.ID > 0)
                            hmd.Delete();
                        hoaDon.PhanLoaiCollection.Remove(hmd);
                    }

                    grdChungTu.DataSource = hoaDon.PhanLoaiCollection;
                    grdChungTu.Refetch();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

    }
}
