﻿using System;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using System.Threading;
using System.Data;
//using SQLDMO;
using System.Reflection;
using System.Configuration;

namespace Company.Interface
{
    public partial class BackUpAndReStoreForm : BaseForm
    {
        public bool isBackUp = true;
        public static DateTime lastBackUp = new DateTime(1900, 01, 01);
        private string DATABASE_NAME = "";
        //SQLDMO.BackupClass backupClass;
        //SQLDMO.RestoreClass restoreClass = new SQLDMO.RestoreClass();

        public BackUpAndReStoreForm()
        {
            InitializeComponent();
        }

        private void BackUpAndReStoreForm_Load(object sender, EventArgs e)
        {
            if (!GlobalSettings.PathBackup.Equals(""))
                txtPath.Text = GlobalSettings.PathBackup.Trim();
            else
                txtPath.Text = @"D:\SOFTECH\DATABASE\BACKUP";

            DATABASE_NAME = GlobalSettings.DATABASE_NAME + "_" + DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString("00") + DateTime.Today.Day.ToString("00") + ".BAK";
            editBox1.Text = DATABASE_NAME;

            if (GlobalSettings.NGAYSAOLUU == "")
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;

            string nhacNhoSaoLuu = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHAC_NHO_SAO_LUU");

            if (GlobalSettings.NGON_NGU == "0")
            {
                if (isBackUp)
                {
                    uiComboBox1.SelectedValue = nhacNhoSaoLuu != null || nhacNhoSaoLuu != "" ? nhacNhoSaoLuu : "7";
                    //uiButton1.Text = "Sao lưu";
                }
                //else
                //{
                //    label2.Visible = label3.Visible = label4.Visible = uiComboBox1.Visible = false;
                //    uiButton1.Text = "Phục hồi";
                //}
            }
            else
            {
                if (isBackUp)
                {
                    uiComboBox1.SelectedValue = nhacNhoSaoLuu != null || nhacNhoSaoLuu != "" ? nhacNhoSaoLuu : "7";
                    //uiButton1.Text = "Backup";
                }
                //else
                //{
                //    label2.Visible = label3.Visible = label4.Visible = uiComboBox1.Visible = false;
                //    uiButton1.Text = "Restore";
                //}

            }
            uiCheckBox1.Checked = nhacNhoSaoLuu != null || nhacNhoSaoLuu != "" ? true : false;

            uiCheckBox1_CheckedChanged(null, EventArgs.Empty);

            //backupClass = new SQLDMO.BackupClass();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                #region Old_Code
                //SQLDMO.SQLServer2Class server = new SQLDMO.SQLServer2Class();
                //server.Connect(GlobalSettings.SERVER_NAME, GlobalSettings.USER, GlobalSettings.PASS);
                //if (isBackUp)
                //{
                //    backupClass.Action = SQLDMO.SQLDMO_BACKUP_TYPE.SQLDMOBackup_Database;
                //    backupClass.Database = GlobalSettings.DATABASE_NAME;
                //    //backupClass.Files = editBox1.Text.Trim();
                //    SaveFileDialog saveFileDlg = new SaveFileDialog();
                //    saveFileDlg.Filter = "BAK files (*.BAK)|*.txt|All files (*.*)|*.*";
                //    saveFileDlg.RestoreDirectory = true;
                //    if (saveFileDlg.ShowDialog() == DialogResult.OK)
                //    {
                //        backupClass.Files = saveFileDlg.FileName + editBox1.Text.Trim();
                //        backupClass.BackupSetDescription = "Full BackUp";
                //        backupClass.BackupSetName = "CUONGNH";
                //        backupClass.SQLBackup(server);
                //    }
                //backupClass.BackupSetDescription = "Full BackUp";
                //backupClass.BackupSetName = "CUONGNH";
                //backupClass.SQLBackup(server);
                //if (uiCheckBox1.Checked)
                //{
                //    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                //    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                //}
                //else
                //{
                //    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                //    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                //}
                //lastBackUp = DateTime.Now;
                //GlobalSettings.RefreshKey();

                //BackupLog.addToLog(GlobalSettings.DATABASE_NAME, editBox1.Text.Trim(), DateTime.Now);
                //ShowMessage("Sao lưu dữ liệu thành công.", false);
                //}
                //else
                //{
                //restoreClass.Action = SQLDMO.SQLDMO_RESTORE_TYPE.SQLDMORestore_Database;
                //restoreClass.Database = GlobalSettings.DATABASE_NAME;
                //restoreClass.Files = editBox1.Text.Trim();
                //restoreClass.SQLRestore(server);
                //ShowMessage("Phục hồi dữ liệu thành công.", false);
                //}
                #endregion

                //bool ok = true;
                //string serverName = GlobalSettings.SERVER_NAME.Split('\\')[0].Trim();

                if (txtPath.Text.Equals("") || editBox1.Text.Equals(""))
                    ShowMessage("Đường dẫn hoặc tên file không được để trống", false);
                else
                {
                    #region Backup SQL 2008
                    //ServerConnection srvCon = new ServerConnection(GlobalSettings.SERVER_NAME);
                    //srvCon.LoginSecure = false;
                    //srvCon.Login = GlobalSettings.USER;
                    //srvCon.Password = GlobalSettings.PASS;
                    //Server srvSql = new Server(srvCon);

                    //SaveFileDialog saveBackupDialog = new SaveFileDialog();
                    //saveBackupDialog.Filter = "BAK files (*.BAK)|*.BAK|All files (*.*)|*.*";
                    //saveBackupDialog.FileName = txtPath.Text + "\\" + editBox1.Text;

                    ////saveBackupDialog.RestoreDirectory = true;
                    ////if (saveBackupDialog.ShowDialog() == DialogResult.OK)
                    ////{
                    //Backup backupDatabase = new Backup();
                    //backupDatabase.Action = BackupActionType.Database;
                    //backupDatabase.Database = GlobalSettings.DATABASE_NAME;

                    //BackupDeviceItem bkdevice = new BackupDeviceItem(saveBackupDialog.FileName, DeviceType.File);
                    //backupDatabase.Devices.Add(bkdevice);
                    //backupDatabase.Incremental = false;
                    //backupDatabase.Initialize = true;
                    //backupDatabase.SqlBackup(srvSql);
                    //}
                    //else
                    //ok = false;
                    #endregion

                    //SQLDMO.SQLServer2Class server = new SQLDMO.SQLServer2Class();
                    //server.Connect(GlobalSettings.SERVER_NAME, GlobalSettings.USER, GlobalSettings.PASS);
                    if (isBackUp)
                    {
                        //backupClass.Action = SQLDMO.SQLDMO_BACKUP_TYPE.SQLDMOBackup_Database;
                        //backupClass.Database = GlobalSettings.DATABASE_NAME;
                        //SaveFileDialog saveFileDlg = new SaveFileDialog();
                        //saveFileDlg.Filter = "BAK files (*.BAK)|*.txt|All files (*.*)|*.*";
                        //saveFileDlg.FileName = txtPath.Text.Trim() + "\\" + editBox1.Text.Trim();
                        //saveFileDlg.RestoreDirectory = true;

                        //if (saveFileDlg.ShowDialog() == DialogResult.OK)
                        //{
                        //backupClass.Files = saveFileDlg.FileName;
                        //backupClass.BackupSetDescription = "Full BackUp";
                        //backupClass.BackupSetName = "ECS_TQDT_SXXK";
                        //backupClass.SQLBackup(server);
                        //}

                        string path = txtPath.Text.Trim().Substring(txtPath.Text.Trim().Length - 1, 1) == "\\" ? txtPath.Text.Trim().Remove(txtPath.Text.Trim().Length - 1, 1) : txtPath.Text.Trim();
                        path += "\\" + editBox1.Text.Trim();

                        if (BackupDatabase(path))
                            ShowMessage("Sao lưu dữ liệu thành công.", false);
                    }

                    if (uiCheckBox1.Checked)
                    {
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                    }
                    else
                    {
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                    }

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("LastBackup", DateTime.Now.ToString("dd/MM/yyyy"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PathBackup", txtPath.Text.Trim());
                    this.Cursor = Cursors.Default;
                    //if (ok)

                    GlobalSettings.RefreshKey();
                    //server.DisConnect();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                string st = "";
                if (isBackUp)
                    st = "Lỗi khi sao lưu dữ liệu.";
                else
                    st = "Lỗi khi phục hồi dữ liệu.";
                ShowMessage(st + " " + ex.Message, false);

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void editBox1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void uiCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (uiCheckBox1.Checked == false)
            {
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;
            }
            else
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = true;
        }

        private bool BackupDatabase(string fileName)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string strConnString = "server=" + GlobalSettings.SERVER_NAME;
                strConnString += ";User Id=" + GlobalSettings.USER + ";Password=" + GlobalSettings.PASS;

                Company.KDT.SHARE.Components.SQL.InitializeServer(strConnString);

                Microsoft.SqlServer.Management.Smo.BackupDevice backupDevice = new Microsoft.SqlServer.Management.Smo.BackupDevice();

                //backupDevice.Parent = Company.KDT.SHARE.Components.SQL.server;
                backupDevice.Name = DATABASE_NAME;
                backupDevice.PhysicalLocation = fileName;
                backupDevice.BackupDeviceType = Microsoft.SqlServer.Management.Smo.BackupDeviceType.Disk;
                backupDevice.Create();

                return true;
            }
            catch (Exception e1)
            {
                Globals.ShowMessage("Kết nối không thành công. Bạn hãy kiểm tra lại thông tin cấu hình.\r\n\nChi tiết: " + e1.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(e1);

                return false;
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                FrmFolderBrowse frm = new FrmFolderBrowse(GlobalSettings.USER, GlobalSettings.PASS, GlobalSettings.SERVER_NAME);
                frm.ShowDialog();
                txtPath.Text = FrmFolderBrowse.pathSelected;
            }
            catch (Exception e1)
            {
                Globals.ShowMessage("Kết nối không thành công. Bạn hãy kiểm tra lại thông tin cấu hình.\r\n\nChi tiết: " + e1.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(e1);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (uiCheckBox1.Checked)
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                }
                else
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                }

                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PathBackup", txtPath.Text.Trim());

                ShowMessage("Lưu cấu hình thành công.", false);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

    }
}
