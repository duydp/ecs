using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class DinhMuc
	{
		#region Private members.
		
		protected long _ID;
		protected string _MaSanPham = string.Empty;
		protected string _MaNguyenPhuLieu = string.Empty;
		protected string _DVT_ID = string.Empty;
		protected decimal _DinhMucSuDung;
		protected decimal _TyLeHaoHut;
		protected string _GhiChu = string.Empty;
		protected int _STTHang;
		protected long _Master_ID;
        protected string _TenNPL = "";
        protected bool _IsFromVietNam = false;
		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		
		public string MaSanPham
		{
			set {this._MaSanPham = value;}
			get {return this._MaSanPham;}
		}
		
		public string MaNguyenPhuLieu
		{
			set {this._MaNguyenPhuLieu = value;}
			get {return this._MaNguyenPhuLieu;}
		}
		
		public string DVT_ID
		{
			set {this._DVT_ID = value;}
			get {return this._DVT_ID;}
		}
		
		public decimal DinhMucSuDung
		{
			set {this._DinhMucSuDung = value;}
			get {return this._DinhMucSuDung;}
		}
		
		public decimal TyLeHaoHut
		{
			set {this._TyLeHaoHut = value;}
			get {return this._TyLeHaoHut;}
		}
		
		public string GhiChu
		{
			set {this._GhiChu = value;}
			get {return this._GhiChu;}
		}
		
		public int STTHang
		{
			set {this._STTHang = value;}
			get {return this._STTHang;}
		}
		
		public long Master_ID
		{
			set {this._Master_ID = value;}
			get {return this._Master_ID;}
		}
        public string TenNPL
        {
            set { this._TenNPL = value; }
            get { return this._TenNPL; }
        }

        public bool IsFromVietNam { get { return _IsFromVietNam; } set { _IsFromVietNam = value; } }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static DinhMuc Load(long iD)
		{
			string spName = "[dbo].p_KDT_SXXK_DinhMuc_Load";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
			DinhMuc entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            
			if (reader.Read())
			{
				entity = new DinhMuc();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
                
			}
			reader.Close();
			dbCommand.Connection.Close();
			
			return entity;
		}


        public static bool checkDinhMucExit(string masp, long master_ID,string mahq,string madv)
        {
             SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
             string sql = "select dm.id " +
                        "from t_KDT_SXXK_DinhMuc dm inner join t_KDT_SXXK_DinhMucDangKy dmdk on " +
                        "dm.Master_ID=dmdk.ID " +
                        "where dm.MaSanPham=@MaSP and dm.Master_ID<>@Master_ID and dmdk.TrangThaiXuLy<>1 " +
                        " and dmdk.MaHaiQuan=@MaHaiQuan and dmdk.MaDoanhNghiep=@MaDoanhNghiep";
             SqlCommand dbCommand = (SqlCommand) db.GetSqlStringCommand(sql);
             db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
             db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahq);
             db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
             db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, masp);
             object o = db.ExecuteScalar(dbCommand);
             if (o == null)
                 return false;
             return true;
        }
        public static long GetIDDinhMucExit(string masp, long master_ID, string mahq, string madv)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "select dmdk.ID " +
                       "from t_KDT_SXXK_DinhMuc dm inner join t_KDT_SXXK_DinhMucDangKy dmdk on " +
                       "dm.Master_ID=dmdk.ID " +
                       "where dm.MaSanPham=@MaSP and dm.Master_ID<>@Master_ID and dmdk.TrangThaiXuLy<>1 " +
                       " and dmdk.MaHaiQuan=@MaHaiQuan and dmdk.MaDoanhNghiep=@MaDoanhNghiep";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahq);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, masp);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return 0;
            return Convert.ToInt64(o);
        }

        public static DinhMuc GetDinhMucByMaSP(string maSP, string mahq, string madv)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "select * " +
                       "from t_KDT_SXXK_DinhMuc dm inner join t_KDT_SXXK_DinhMucDangKy dmdk on " +
                       "dm.Master_ID=dmdk.ID " +
                       "where dm.MaSanPham=@MaSP " +
                       " and dmdk.MaHaiQuan=@MaHaiQuan and dmdk.MaDoanhNghiep=@MaDoanhNghiep";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahq);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, maSP);
            DinhMuc entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }

        public static DinhMuc GetDinhMucByMaSP_NPL(string maSP, string maNPL, string mahq, string madv)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "select * " +
                       "from t_KDT_SXXK_DinhMuc dm inner join t_KDT_SXXK_DinhMucDangKy dmdk on " +
                       "dm.Master_ID=dmdk.ID " +
                       "where dm.MaSanPham=@MaSP " +
                       "and dm.MaNguyenPhuLieu=@maNPL" +
                       " and dmdk.MaHaiQuan=@MaHaiQuan and dmdk.MaDoanhNghiep=@MaDoanhNghiep";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahq);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@maNPL", SqlDbType.VarChar, maNPL);
            DinhMuc entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Master_ID(long master_ID)
		{
			string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		

		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderBy_Master_ID(long master_ID)
		{
			string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            return db.ExecuteReader(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------

		public static DinhMucCollection SelectCollectionBy_Master_ID(long master_ID)
		{
			string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);
						
            IDataReader reader = db.ExecuteReader(dbCommand);
			DinhMucCollection collection = new DinhMucCollection();			
			while (reader.Read())
			{
				DinhMuc entity = new DinhMuc();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
                

                collection.Add(entity);
			}
			reader.Close();
			dbCommand.Connection.Close();
			return collection;			
			
		}
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public static IDataReader SelectReaderAll()
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DinhMucCollection SelectCollectionAll()
		{
			DinhMucCollection collection = new DinhMucCollection();

			IDataReader reader = SelectReaderAll();
			while (reader.Read())
			{
				DinhMuc entity = new DinhMuc();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "[dbo].p_KDT_SXXK_DinhMuc_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static DinhMucCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			DinhMucCollection collection = new DinhMucCollection();

			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				DinhMuc entity = new DinhMuc();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long Insert(string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_ID)
		{
			DinhMuc entity = new DinhMuc();			
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DVT_ID = dVT_ID;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.GhiChu = ghiChu;
			entity.STTHang = sTTHang;
			entity.Master_ID = master_ID;
			
			long returnID = entity.Insert();
						
			return returnID;
		}
		
		public bool Insert(DinhMucCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "[dbo].p_KDT_SXXK_DinhMuc_Insert";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this._GhiChu);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            db.AddInParameter(dbCommand, "@IsFromVietNam", SqlDbType.Bit, this._IsFromVietNam);
            
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_DinhMuc_InsertUpdate";	
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this._GhiChu);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            db.AddInParameter(dbCommand, "@IsFromVietNam", SqlDbType.Bit, this._IsFromVietNam);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(DinhMucCollection collection)
        {
            bool ret;			
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int Update(long iD, string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_ID, bool isFromVietNam)
		{
			DinhMuc entity = new DinhMuc();			
			entity.ID = iD;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DVT_ID = dVT_ID;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.GhiChu = ghiChu;
			entity.STTHang = sTTHang;
			entity.Master_ID = master_ID;
            entity.IsFromVietNam = isFromVietNam;
			return entity.Update();
		}
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public bool Update(DinhMucCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        if (item.UpdateTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "[dbo].p_KDT_SXXK_DinhMuc_Update";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, this._DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, this._TyLeHaoHut);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, this._GhiChu);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            db.AddInParameter(dbCommand, "@IsFromVietNam", SqlDbType.Bit, this._IsFromVietNam);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int Delete(long iD)
		{
			DinhMuc entity = new DinhMuc();
			entity.ID = iD;
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public bool DeleteCollection(DinhMucCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (DinhMuc item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}

		//---------------------------------------------------------------------------------------------

		public static void DeleteCollection(DinhMucCollection collection, SqlTransaction transaction)
		{			
            foreach (DinhMuc item in collection)
            {
                item.DeleteTransaction(transaction);
            }			
		}

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "[dbo].p_KDT_SXXK_DinhMuc_Delete";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		#endregion
	}	
}