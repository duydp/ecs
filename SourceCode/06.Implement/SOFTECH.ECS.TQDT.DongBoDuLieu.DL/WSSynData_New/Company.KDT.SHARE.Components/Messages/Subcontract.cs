﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class Subcontract
    {
       /// <summary>
       /// 
       /// </summary>
       [XmlElement("reference")]
       public string Reference { get; set; }
       /// <summary>
       /// 
       /// </summary>
       [XmlElement("issue")]
       public string Issue { get; set; }
       /// <summary>
       /// 
       /// </summary>
       [XmlElement("decription")]
       public string Decription { get; set; }
    }
}
