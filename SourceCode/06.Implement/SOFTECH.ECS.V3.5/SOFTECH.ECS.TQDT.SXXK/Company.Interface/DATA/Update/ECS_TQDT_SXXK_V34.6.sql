IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TransportEquipment]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_TransportEquipment]
GO
CREATE TABLE t_KDT_VNACCS_TransportEquipment
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
TKMD_ID BIGINT NOT NULL,
TenNhaVanChuyen NVARCHAR(255) NOT NULL,
MaNhaVanChuyen NVARCHAR(17) NOT NULL,
DiaChiNhaVanChuyen NVARCHAR(255),
CoBaoXNK NVARCHAR(1) NOT NULL,
MaLoaiHinhVanChuyen NVARCHAR(6) NOT NULL,
MaHQGiamSat NVARCHAR(6) NOT NULL,
MaPTVC NVARCHAR(2),
MaMucDichVC NVARCHAR(3),
NgayBatDauVC DATETIME,
NgayKetThucVC DATETIME,
NgayHopDongVC DATETIME,
SoHopDongVC NVARCHAR(10),
NgayHetHanHDVC DATETIME,
MaDiaDiemXepHang NVARCHAR(7) NOT NULL,
TenDiaDiemXepHang NVARCHAR(255),
MaViTriXepHang NVARCHAR(7),
MaCangCuaKhauGaXepHang NVARCHAR(7),
MaDiaDiemDoHang NVARCHAR(7) NOT NULL,
TenDiaDiemDoHang NVARCHAR(255),
MaViTriDoHang NVARCHAR(7),
MaCangCuaKhauGaDoHang NVARCHAR(7),
GhiChuKhac NVARCHAR(200),
TuyenDuong NVARCHAR(35) NOT NULL,
SoDienThoaiHQ NVARCHAR(20),
SoFaxHQ NVARCHAR(20),
TenDaiDienDN  NVARCHAR(255) NOT NULL,
ThoiGianVC NVARCHAR(50) NOT NULL,
SoKMVC NUMERIC(5) NOT NULL,
GuidStr VARCHAR(MAX)
)
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TransportEquipment_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_TransportEquipment_Details]
GO
CREATE TABLE t_KDT_VNACCS_TransportEquipment_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TransportEquipment_ID BIGINT NOT NULL FOREIGN KEY REFERENCES t_KDT_VNACCS_TransportEquipment(ID),
SoContainer NVARCHAR(35) NOT NULL,
SoVanDon NVARCHAR(35) NOT NULL,
SoSeal NVARCHAR(35) NOT NULL,
SoSealHQ NVARCHAR(35),
LoaiContainer NVARCHAR(3) NOT NULL
)

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@TenNhaVanChuyen nvarchar(255),
	@MaNhaVanChuyen nvarchar(17),
	@DiaChiNhaVanChuyen nvarchar(255),
	@CoBaoXNK nvarchar(1),
	@MaLoaiHinhVanChuyen nvarchar(6),
	@MaHQGiamSat nvarchar(6),
	@MaPTVC nvarchar(2),
	@MaMucDichVC nvarchar(3),
	@NgayBatDauVC datetime,
	@NgayKetThucVC datetime,
	@NgayHopDongVC datetime,
	@SoHopDongVC nvarchar(10),
	@NgayHetHanHDVC datetime,
	@MaDiaDiemXepHang nvarchar(7),
	@TenDiaDiemXepHang nvarchar(255),
	@MaViTriXepHang nvarchar(7),
	@MaCangCuaKhauGaXepHang nvarchar(7),
	@MaDiaDiemDoHang nvarchar(7),
	@TenDiaDiemDoHang nvarchar(255),
	@MaViTriDoHang nvarchar(7),
	@MaCangCuaKhauGaDoHang nvarchar(7),
	@GhiChuKhac nvarchar(200),
	@TuyenDuong nvarchar(35),
	@SoDienThoaiHQ nvarchar(20),
	@SoFaxHQ nvarchar(20),
	@TenDaiDienDN nvarchar(255),
	@ThoiGianVC nvarchar(50),
	@SoKMVC numeric(5, 0),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TransportEquipment]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[TenNhaVanChuyen],
	[MaNhaVanChuyen],
	[DiaChiNhaVanChuyen],
	[CoBaoXNK],
	[MaLoaiHinhVanChuyen],
	[MaHQGiamSat],
	[MaPTVC],
	[MaMucDichVC],
	[NgayBatDauVC],
	[NgayKetThucVC],
	[NgayHopDongVC],
	[SoHopDongVC],
	[NgayHetHanHDVC],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaDiaDiemDoHang],
	[TenDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[GhiChuKhac],
	[TuyenDuong],
	[SoDienThoaiHQ],
	[SoFaxHQ],
	[TenDaiDienDN],
	[ThoiGianVC],
	[SoKMVC],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TKMD_ID,
	@TenNhaVanChuyen,
	@MaNhaVanChuyen,
	@DiaChiNhaVanChuyen,
	@CoBaoXNK,
	@MaLoaiHinhVanChuyen,
	@MaHQGiamSat,
	@MaPTVC,
	@MaMucDichVC,
	@NgayBatDauVC,
	@NgayKetThucVC,
	@NgayHopDongVC,
	@SoHopDongVC,
	@NgayHetHanHDVC,
	@MaDiaDiemXepHang,
	@TenDiaDiemXepHang,
	@MaViTriXepHang,
	@MaCangCuaKhauGaXepHang,
	@MaDiaDiemDoHang,
	@TenDiaDiemDoHang,
	@MaViTriDoHang,
	@MaCangCuaKhauGaDoHang,
	@GhiChuKhac,
	@TuyenDuong,
	@SoDienThoaiHQ,
	@SoFaxHQ,
	@TenDaiDienDN,
	@ThoiGianVC,
	@SoKMVC,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@TenNhaVanChuyen nvarchar(255),
	@MaNhaVanChuyen nvarchar(17),
	@DiaChiNhaVanChuyen nvarchar(255),
	@CoBaoXNK nvarchar(1),
	@MaLoaiHinhVanChuyen nvarchar(6),
	@MaHQGiamSat nvarchar(6),
	@MaPTVC nvarchar(2),
	@MaMucDichVC nvarchar(3),
	@NgayBatDauVC datetime,
	@NgayKetThucVC datetime,
	@NgayHopDongVC datetime,
	@SoHopDongVC nvarchar(10),
	@NgayHetHanHDVC datetime,
	@MaDiaDiemXepHang nvarchar(7),
	@TenDiaDiemXepHang nvarchar(255),
	@MaViTriXepHang nvarchar(7),
	@MaCangCuaKhauGaXepHang nvarchar(7),
	@MaDiaDiemDoHang nvarchar(7),
	@TenDiaDiemDoHang nvarchar(255),
	@MaViTriDoHang nvarchar(7),
	@MaCangCuaKhauGaDoHang nvarchar(7),
	@GhiChuKhac nvarchar(200),
	@TuyenDuong nvarchar(35),
	@SoDienThoaiHQ nvarchar(20),
	@SoFaxHQ nvarchar(20),
	@TenDaiDienDN nvarchar(255),
	@ThoiGianVC nvarchar(50),
	@SoKMVC numeric(5, 0),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TransportEquipment]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[TenNhaVanChuyen] = @TenNhaVanChuyen,
	[MaNhaVanChuyen] = @MaNhaVanChuyen,
	[DiaChiNhaVanChuyen] = @DiaChiNhaVanChuyen,
	[CoBaoXNK] = @CoBaoXNK,
	[MaLoaiHinhVanChuyen] = @MaLoaiHinhVanChuyen,
	[MaHQGiamSat] = @MaHQGiamSat,
	[MaPTVC] = @MaPTVC,
	[MaMucDichVC] = @MaMucDichVC,
	[NgayBatDauVC] = @NgayBatDauVC,
	[NgayKetThucVC] = @NgayKetThucVC,
	[NgayHopDongVC] = @NgayHopDongVC,
	[SoHopDongVC] = @SoHopDongVC,
	[NgayHetHanHDVC] = @NgayHetHanHDVC,
	[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
	[TenDiaDiemXepHang] = @TenDiaDiemXepHang,
	[MaViTriXepHang] = @MaViTriXepHang,
	[MaCangCuaKhauGaXepHang] = @MaCangCuaKhauGaXepHang,
	[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
	[TenDiaDiemDoHang] = @TenDiaDiemDoHang,
	[MaViTriDoHang] = @MaViTriDoHang,
	[MaCangCuaKhauGaDoHang] = @MaCangCuaKhauGaDoHang,
	[GhiChuKhac] = @GhiChuKhac,
	[TuyenDuong] = @TuyenDuong,
	[SoDienThoaiHQ] = @SoDienThoaiHQ,
	[SoFaxHQ] = @SoFaxHQ,
	[TenDaiDienDN] = @TenDaiDienDN,
	[ThoiGianVC] = @ThoiGianVC,
	[SoKMVC] = @SoKMVC,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@TenNhaVanChuyen nvarchar(255),
	@MaNhaVanChuyen nvarchar(17),
	@DiaChiNhaVanChuyen nvarchar(255),
	@CoBaoXNK nvarchar(1),
	@MaLoaiHinhVanChuyen nvarchar(6),
	@MaHQGiamSat nvarchar(6),
	@MaPTVC nvarchar(2),
	@MaMucDichVC nvarchar(3),
	@NgayBatDauVC datetime,
	@NgayKetThucVC datetime,
	@NgayHopDongVC datetime,
	@SoHopDongVC nvarchar(10),
	@NgayHetHanHDVC datetime,
	@MaDiaDiemXepHang nvarchar(7),
	@TenDiaDiemXepHang nvarchar(255),
	@MaViTriXepHang nvarchar(7),
	@MaCangCuaKhauGaXepHang nvarchar(7),
	@MaDiaDiemDoHang nvarchar(7),
	@TenDiaDiemDoHang nvarchar(255),
	@MaViTriDoHang nvarchar(7),
	@MaCangCuaKhauGaDoHang nvarchar(7),
	@GhiChuKhac nvarchar(200),
	@TuyenDuong nvarchar(35),
	@SoDienThoaiHQ nvarchar(20),
	@SoFaxHQ nvarchar(20),
	@TenDaiDienDN nvarchar(255),
	@ThoiGianVC nvarchar(50),
	@SoKMVC numeric(5, 0),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TransportEquipment] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TransportEquipment] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[TenNhaVanChuyen] = @TenNhaVanChuyen,
			[MaNhaVanChuyen] = @MaNhaVanChuyen,
			[DiaChiNhaVanChuyen] = @DiaChiNhaVanChuyen,
			[CoBaoXNK] = @CoBaoXNK,
			[MaLoaiHinhVanChuyen] = @MaLoaiHinhVanChuyen,
			[MaHQGiamSat] = @MaHQGiamSat,
			[MaPTVC] = @MaPTVC,
			[MaMucDichVC] = @MaMucDichVC,
			[NgayBatDauVC] = @NgayBatDauVC,
			[NgayKetThucVC] = @NgayKetThucVC,
			[NgayHopDongVC] = @NgayHopDongVC,
			[SoHopDongVC] = @SoHopDongVC,
			[NgayHetHanHDVC] = @NgayHetHanHDVC,
			[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
			[TenDiaDiemXepHang] = @TenDiaDiemXepHang,
			[MaViTriXepHang] = @MaViTriXepHang,
			[MaCangCuaKhauGaXepHang] = @MaCangCuaKhauGaXepHang,
			[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
			[TenDiaDiemDoHang] = @TenDiaDiemDoHang,
			[MaViTriDoHang] = @MaViTriDoHang,
			[MaCangCuaKhauGaDoHang] = @MaCangCuaKhauGaDoHang,
			[GhiChuKhac] = @GhiChuKhac,
			[TuyenDuong] = @TuyenDuong,
			[SoDienThoaiHQ] = @SoDienThoaiHQ,
			[SoFaxHQ] = @SoFaxHQ,
			[TenDaiDienDN] = @TenDaiDienDN,
			[ThoiGianVC] = @ThoiGianVC,
			[SoKMVC] = @SoKMVC,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TransportEquipment]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TKMD_ID],
			[TenNhaVanChuyen],
			[MaNhaVanChuyen],
			[DiaChiNhaVanChuyen],
			[CoBaoXNK],
			[MaLoaiHinhVanChuyen],
			[MaHQGiamSat],
			[MaPTVC],
			[MaMucDichVC],
			[NgayBatDauVC],
			[NgayKetThucVC],
			[NgayHopDongVC],
			[SoHopDongVC],
			[NgayHetHanHDVC],
			[MaDiaDiemXepHang],
			[TenDiaDiemXepHang],
			[MaViTriXepHang],
			[MaCangCuaKhauGaXepHang],
			[MaDiaDiemDoHang],
			[TenDiaDiemDoHang],
			[MaViTriDoHang],
			[MaCangCuaKhauGaDoHang],
			[GhiChuKhac],
			[TuyenDuong],
			[SoDienThoaiHQ],
			[SoFaxHQ],
			[TenDaiDienDN],
			[ThoiGianVC],
			[SoKMVC],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TKMD_ID,
			@TenNhaVanChuyen,
			@MaNhaVanChuyen,
			@DiaChiNhaVanChuyen,
			@CoBaoXNK,
			@MaLoaiHinhVanChuyen,
			@MaHQGiamSat,
			@MaPTVC,
			@MaMucDichVC,
			@NgayBatDauVC,
			@NgayKetThucVC,
			@NgayHopDongVC,
			@SoHopDongVC,
			@NgayHetHanHDVC,
			@MaDiaDiemXepHang,
			@TenDiaDiemXepHang,
			@MaViTriXepHang,
			@MaCangCuaKhauGaXepHang,
			@MaDiaDiemDoHang,
			@TenDiaDiemDoHang,
			@MaViTriDoHang,
			@MaCangCuaKhauGaDoHang,
			@GhiChuKhac,
			@TuyenDuong,
			@SoDienThoaiHQ,
			@SoFaxHQ,
			@TenDaiDienDN,
			@ThoiGianVC,
			@SoKMVC,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TransportEquipment]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TransportEquipment] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[TenNhaVanChuyen],
	[MaNhaVanChuyen],
	[DiaChiNhaVanChuyen],
	[CoBaoXNK],
	[MaLoaiHinhVanChuyen],
	[MaHQGiamSat],
	[MaPTVC],
	[MaMucDichVC],
	[NgayBatDauVC],
	[NgayKetThucVC],
	[NgayHopDongVC],
	[SoHopDongVC],
	[NgayHetHanHDVC],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaDiaDiemDoHang],
	[TenDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[GhiChuKhac],
	[TuyenDuong],
	[SoDienThoaiHQ],
	[SoFaxHQ],
	[TenDaiDienDN],
	[ThoiGianVC],
	[SoKMVC],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_TransportEquipment]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[TenNhaVanChuyen],
	[MaNhaVanChuyen],
	[DiaChiNhaVanChuyen],
	[CoBaoXNK],
	[MaLoaiHinhVanChuyen],
	[MaHQGiamSat],
	[MaPTVC],
	[MaMucDichVC],
	[NgayBatDauVC],
	[NgayKetThucVC],
	[NgayHopDongVC],
	[SoHopDongVC],
	[NgayHetHanHDVC],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaDiaDiemDoHang],
	[TenDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[GhiChuKhac],
	[TuyenDuong],
	[SoDienThoaiHQ],
	[SoFaxHQ],
	[TenDaiDienDN],
	[ThoiGianVC],
	[SoKMVC],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_TransportEquipment] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_SelectAll]




































AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[TenNhaVanChuyen],
	[MaNhaVanChuyen],
	[DiaChiNhaVanChuyen],
	[CoBaoXNK],
	[MaLoaiHinhVanChuyen],
	[MaHQGiamSat],
	[MaPTVC],
	[MaMucDichVC],
	[NgayBatDauVC],
	[NgayKetThucVC],
	[NgayHopDongVC],
	[SoHopDongVC],
	[NgayHetHanHDVC],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaDiaDiemDoHang],
	[TenDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[GhiChuKhac],
	[TuyenDuong],
	[SoDienThoaiHQ],
	[SoFaxHQ],
	[TenDaiDienDN],
	[ThoiGianVC],
	[SoKMVC],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_TransportEquipment]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectBy_TransportEquipment_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectBy_TransportEquipment_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteBy_TransportEquipment_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteBy_TransportEquipment_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Insert]
	@TransportEquipment_ID bigint,
	@SoContainer nvarchar(35),
	@SoVanDon nvarchar(35),
	@SoSeal nvarchar(35),
	@SoSealHQ nvarchar(35),
	@LoaiContainer nvarchar(3),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TransportEquipment_Details]
(
	[TransportEquipment_ID],
	[SoContainer],
	[SoVanDon],
	[SoSeal],
	[SoSealHQ],
	[LoaiContainer]
)
VALUES 
(
	@TransportEquipment_ID,
	@SoContainer,
	@SoVanDon,
	@SoSeal,
	@SoSealHQ,
	@LoaiContainer
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Update]
	@ID bigint,
	@TransportEquipment_ID bigint,
	@SoContainer nvarchar(35),
	@SoVanDon nvarchar(35),
	@SoSeal nvarchar(35),
	@SoSealHQ nvarchar(35),
	@LoaiContainer nvarchar(3)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TransportEquipment_Details]
SET
	[TransportEquipment_ID] = @TransportEquipment_ID,
	[SoContainer] = @SoContainer,
	[SoVanDon] = @SoVanDon,
	[SoSeal] = @SoSeal,
	[SoSealHQ] = @SoSealHQ,
	[LoaiContainer] = @LoaiContainer
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_InsertUpdate]
	@ID bigint,
	@TransportEquipment_ID bigint,
	@SoContainer nvarchar(35),
	@SoVanDon nvarchar(35),
	@SoSeal nvarchar(35),
	@SoSealHQ nvarchar(35),
	@LoaiContainer nvarchar(3)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TransportEquipment_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TransportEquipment_Details] 
		SET
			[TransportEquipment_ID] = @TransportEquipment_ID,
			[SoContainer] = @SoContainer,
			[SoVanDon] = @SoVanDon,
			[SoSeal] = @SoSeal,
			[SoSealHQ] = @SoSealHQ,
			[LoaiContainer] = @LoaiContainer
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TransportEquipment_Details]
		(
			[TransportEquipment_ID],
			[SoContainer],
			[SoVanDon],
			[SoSeal],
			[SoSealHQ],
			[LoaiContainer]
		)
		VALUES 
		(
			@TransportEquipment_ID,
			@SoContainer,
			@SoVanDon,
			@SoSeal,
			@SoSealHQ,
			@LoaiContainer
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TransportEquipment_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteBy_TransportEquipment_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteBy_TransportEquipment_ID]
	@TransportEquipment_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_TransportEquipment_Details]
WHERE
	[TransportEquipment_ID] = @TransportEquipment_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TransportEquipment_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TransportEquipment_ID],
	[SoContainer],
	[SoVanDon],
	[SoSeal],
	[SoSealHQ],
	[LoaiContainer]
FROM
	[dbo].[t_KDT_VNACCS_TransportEquipment_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectBy_TransportEquipment_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectBy_TransportEquipment_ID]
	@TransportEquipment_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TransportEquipment_ID],
	[SoContainer],
	[SoVanDon],
	[SoSeal],
	[SoSealHQ],
	[LoaiContainer]
FROM
	[dbo].[t_KDT_VNACCS_TransportEquipment_Details]
WHERE
	[TransportEquipment_ID] = @TransportEquipment_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TransportEquipment_ID],
	[SoContainer],
	[SoVanDon],
	[SoSeal],
	[SoSealHQ],
	[LoaiContainer]
FROM [dbo].[t_KDT_VNACCS_TransportEquipment_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 13, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TransportEquipment_ID],
	[SoContainer],
	[SoVanDon],
	[SoSeal],
	[SoSealHQ],
	[LoaiContainer]
FROM
	[dbo].[t_KDT_VNACCS_TransportEquipment_Details]	

GO

 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '34.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('34.6',GETDATE(), N'CẬP NHẬT KHAI BÁO BÁO TỜ KHAI VẬN CHUYỂN ĐỘC LẬP ĐỦ ĐIỀU KIỆN QUA KVGS')
END