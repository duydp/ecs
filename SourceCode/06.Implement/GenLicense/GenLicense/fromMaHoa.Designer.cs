﻿namespace GenLicense
{
    partial class formMaHoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtChuoiMaHoa = new System.Windows.Forms.TextBox();
            this.txtChuoiNhap = new System.Windows.Forms.TextBox();
            this.btnSinhMa = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDoc = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtChuoiMaHoa);
            this.groupBox1.Controls.Add(this.txtChuoiNhap);
            this.groupBox1.Controls.Add(this.btnDoc);
            this.groupBox1.Controls.Add(this.btnSinhMa);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(303, 181);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin mã hóa";
            // 
            // txtChuoiMaHoa
            // 
            this.txtChuoiMaHoa.Location = new System.Drawing.Point(109, 51);
            this.txtChuoiMaHoa.Name = "txtChuoiMaHoa";
            this.txtChuoiMaHoa.Size = new System.Drawing.Size(167, 20);
            this.txtChuoiMaHoa.TabIndex = 2;
            // 
            // txtChuoiNhap
            // 
            this.txtChuoiNhap.Location = new System.Drawing.Point(109, 28);
            this.txtChuoiNhap.Name = "txtChuoiNhap";
            this.txtChuoiNhap.Size = new System.Drawing.Size(90, 20);
            this.txtChuoiNhap.TabIndex = 2;
            // 
            // btnSinhMa
            // 
            this.btnSinhMa.Location = new System.Drawing.Point(109, 77);
            this.btnSinhMa.Name = "btnSinhMa";
            this.btnSinhMa.Size = new System.Drawing.Size(61, 23);
            this.btnSinhMa.TabIndex = 1;
            this.btnSinhMa.Text = "Mã hóa";
            this.btnSinhMa.UseVisualStyleBackColor = true;
            this.btnSinhMa.Click += new System.EventHandler(this.btnSinhMa_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 58);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Chuỗi mã hóa";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 35);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chuỗi nhập";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnDoc
            // 
            this.btnDoc.Location = new System.Drawing.Point(176, 77);
            this.btnDoc.Name = "btnDoc";
            this.btnDoc.Size = new System.Drawing.Size(61, 23);
            this.btnDoc.TabIndex = 1;
            this.btnDoc.Text = "Đọc";
            this.btnDoc.UseVisualStyleBackColor = true;
            this.btnDoc.Click += new System.EventHandler(this.btnDoc_Click);
            // 
            // formMaHoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 181);
            this.Controls.Add(this.groupBox1);
            this.Name = "formMaHoa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mã hóa thông tin";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtChuoiMaHoa;
        private System.Windows.Forms.TextBox txtChuoiNhap;
        private System.Windows.Forms.Button btnSinhMa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDoc;
    }
}

