﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.BLL.SXXK.ToKhai;

namespace DongBoDuLieu.BLL.SXXK
{
    public class TK
    {
        private static void fixDB(string MaDN)
        {
            string spName = "p_FixDb";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDN);
            db.ExecuteNonQuery(dbCommand);
        }
        public static DataSet GetALLDanhSachToKhai(string MaHaiQuan, string MaDN, bool isPhongKhai, int NamDangKy, string matKhau)
        {
            string query = "SELECT  *  from DTOKHAIMD WHERE MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDN + "' ";
            if (NamDangKy > 0)
                query += " and NAMDK >=" + NamDangKy;
            query += " ORDER BY NGAY_DK";
            DataSet ds = null;
            if (!isPhongKhai)
                ds = WebServiceDB.getDataBySQL(query.ToString(), MaHaiQuan, "SXXK", MaDN, matKhau);
            else
                ds = WebServiceDB.getDataBySQLDatabase(query, MaHaiQuan, "SXXK");
            return ds;
        }

        //HUNGTQ Update 25/10/2010
        public static DataSet GetALLDanhSachToKhai(string MaHaiQuan, string MaDN, bool isPhongKhai, DateTime tuNgay, DateTime denNgay, string matKhau)
        {
            string query = "SELECT  *  from DTOKHAIMD WHERE MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDN + "' ";
            if (tuNgay > DateTime.MinValue)
                query += " and NGAY_DK >= '" + tuNgay + "' AND NGAY_DK <= '" + denNgay + "'";
            query += " ORDER BY NGAY_DK";
            DataSet ds = null;
            if (!isPhongKhai)
                ds = WebServiceDB.getDataBySQL(query.ToString(), MaHaiQuan, "SXXK", MaDN, matKhau);
            else
                ds = WebServiceDB.getDataBySQLDatabase(query, MaHaiQuan, "SXXK");
            return ds;
        }

        public static DataSet GetALLDanhSachToKhaiLocalSimple(string MaHaiQuan, string MaDN, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = " SELECT sxxk.SoToKhai AS SoTK, sxxk.MaLoaiHinh AS Ma_LH, sxxk.MaHaiQuan AS Ma_HQ, sxxk.NamDangKy AS NamDK, " +
                            " sxxk.NgayDangKy AS Ngay_DK, sxxk.MaDoanhNghiep AS Ma_DV, " +
                            " sxxk.MaDaiLyTTHQ, sxxk.TenDaiLyTTHQ, sxxk.MaDonViUT AS Ma_DVUT, sxxk.TenDonViDoiTac AS DV_DT, " +
                            " sxxk.PTVT_ID AS Ma_PTVT, " +
                            " sxxk.SoHieuPTVT AS Ten_PTVT, " +
                            " sxxk.NgayDenPTVT AS NgayDen, sxxk.SoVanDon AS Van_Don, sxxk.CuaKhau_ID AS Ma_CK, " +
                            " sxxk.DiaDiemXepHang AS CangNN, " +
                            " sxxk.NgayGiayPhep AS Ngay_GP, sxxk.SoGiayPhep AS So_GP, sxxk.NgayHetHanGiayPhep AS Ngay_HHGP, " +
                            " sxxk.SoHopDong AS So_HD, sxxk.TyGiaUSD AS TyGia_USD, sxxk.NgayHopDong AS Ngay_HD, sxxk.NgayHetHanHopDong AS Ngay_HHHD, " +
                            " sxxk.NuocXK_ID AS Nuoc_XK, sxxk.NuocNK_ID AS Nuoc_NK, sxxk.DKGH_ID AS Ma_GH, sxxk.SoHang AS SoHang, sxxk.PTTT_ID AS Ma_PTTT, " +
                            " sxxk.NguyenTe_ID AS Ma_NT, sxxk.TyGiaTinhThue AS TyGia_VND, sxxk.LePhiHaiQuan AS LePhi_HQ, sxxk.ChungTu AS GiayTo, " +
                            " sxxk.TenChuHang AS TenCH, sxxk.PhiBaoHiem AS Phi_BH, sxxk.PhiVanChuyen AS Phi_VC, '' AS Ngay_TH, '' AS User_TH, " +
                            " sxxk.TongTriGiaKhaiBao AS TongTGKB, sxxk.TongTriGiaTinhThue AS TongTGTT, sxxk.TrongLuong AS Tr_luong, sxxk.SoKien AS So_Kien, " +
                            " sxxk.SoContainer20 AS So_Container, sxxk.SoContainer40 AS So_Container40, sxxk.SoHoaDonThuongMai AS So_HDTM," +
                            " sxxk.NgayHoaDonThuongMai AS Ngay_HDTM, sxxk.NgayVanDon AS Ngay_VanDon, sxxk.SoLuongPLTK AS So_PLTK, '' AS Ma_MID, " +
                            " sxxk.TrangThai AS THANH_LY, sxxk.TrangThaiThanhKhoan AS TTTK, sxxk.Xuat_NPL_SP AS XUAT_NPL_SP, sxxk.NgayHoanThanh AS NGAY_HOANTHANH " +
                            " from dbo.t_SXXK_ToKhaiMauDich sxxk" +
                            " WHERE sxxk.MaHaiQuan = '" + MaHaiQuan + "' AND sxxk.MaDoanhNghiep = '" + MaDN + "' ";
            if (tuNgay > DateTime.MinValue)
                query += " and sxxk.NgayDangKy >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND sxxk.NgayDangKy <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.SoToKhai , " +
                        " temp.NamDangKy , " +
                        " temp.MaLoaiHinh , " +
                        " temp.MaHaiQuan , " +
                        " temp.MaDoanhNghiep " +
                        "  FROM   [" + databaseNameTarget + "].dbo.t_SXXK_ToKhaiMauDich temp " +
                        "  WHERE  temp.SoToKhai = sxxk.SoToKhai " +
                        " AND YEAR(temp.NgayDangKy) = YEAR(sxxk.NgayDangKy) " +
                        " AND temp.MaLoaiHinh = sxxk.MaLoaiHinh " +
                        " AND temp.MaHaiQuan = sxxk.MaHaiQuan " +
                        " AND temp.MaDoanhNghiep = sxxk.MaDoanhNghiep ) ";
            query += " ORDER BY sxxk.NgayDangKy";
            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);
            return ds;
        }

        public static DataSet GetALLDanhSachToKhaiLocal(string MaHaiQuan, string MaDN, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = " SELECT sxxk.SoToKhai AS SoTK, sxxk.MaLoaiHinh AS Ma_LH, sxxk.MaHaiQuan AS Ma_HQ, sxxk.NamDangKy AS NamDK, " +
                            " sxxk.NgayDangKy AS Ngay_DK, sxxk.MaDoanhNghiep AS Ma_DV, " +
                            " sxxk.MaDaiLyTTHQ, sxxk.TenDaiLyTTHQ, sxxk.MaDonViUT AS Ma_DVUT, sxxk.TenDonViDoiTac AS DV_DT, " +
                            " sxxk.PTVT_ID AS Ma_PTVT, " +
                            " sxxk.SoHieuPTVT AS Ten_PTVT, " +
                            " sxxk.NgayDenPTVT AS NgayDen, sxxk.SoVanDon AS Van_Don, sxxk.CuaKhau_ID AS Ma_CK, " +
                            " sxxk.DiaDiemXepHang AS CangNN, " +
                            " sxxk.NgayGiayPhep AS Ngay_GP, sxxk.SoGiayPhep AS So_GP, sxxk.NgayHetHanGiayPhep AS Ngay_HHGP, " +
                            " sxxk.SoHopDong AS So_HD, sxxk.TyGiaUSD AS TyGia_USD, sxxk.NgayHopDong AS Ngay_HD, sxxk.NgayHetHanHopDong AS Ngay_HHHD, " +
                            " sxxk.NuocXK_ID AS Nuoc_XK, sxxk.NuocNK_ID AS Nuoc_NK, sxxk.DKGH_ID AS Ma_GH, sxxk.SoHang AS SoHang, sxxk.PTTT_ID AS Ma_PTTT, " +
                            " sxxk.NguyenTe_ID AS Ma_NT, sxxk.TyGiaTinhThue AS TyGia_VND, sxxk.LePhiHaiQuan AS LePhi_HQ, sxxk.ChungTu AS GiayTo, " +
                            " sxxk.TenChuHang AS TenCH, sxxk.PhiBaoHiem AS Phi_BH, sxxk.PhiVanChuyen AS Phi_VC, '' AS Ngay_TH, '' AS User_TH, " +
                            " sxxk.TongTriGiaKhaiBao AS TongTGKB, sxxk.TongTriGiaTinhThue AS TongTGTT, sxxk.TrongLuong AS Tr_luong, sxxk.SoKien AS So_Kien, " +
                            " sxxk.SoContainer20 AS So_Container, sxxk.SoContainer40 AS So_Container40, sxxk.SoHoaDonThuongMai AS So_HDTM," +
                            " sxxk.NgayHoaDonThuongMai AS Ngay_HDTM, sxxk.NgayVanDon AS Ngay_VanDon, sxxk.SoLuongPLTK AS So_PLTK, '' AS Ma_MID, " +
                            " sxxk.TrangThai AS THANH_LY, sxxk.TrangThaiThanhKhoan AS TTTK, sxxk.Xuat_NPL_SP AS XUAT_NPL_SP, sxxk.NgayHoanThanh AS NGAY_HOANTHANH " +
                            " from dbo.t_SXXK_ToKhaiMauDich sxxk LEFT JOIN dbo.t_KDT_ToKhaiMauDich kdt" +
                            " ON sxxk.NgayDangKy = kdt.NgayDangKy" +
                            " AND sxxk.MaDoanhNghiep = kdt.MaDoanhNghiep" +
                            " AND sxxk.MaHaiQuan = kdt.MaHaiQuan" +
                            " AND sxxk.MaLoaiHinh = kdt.MaLoaiHinh" +
                            " AND sxxk.SoToKhai = kdt.SoToKhai" +
                            " WHERE sxxk.MaHaiQuan = '" + MaHaiQuan + "' AND sxxk.MaDoanhNghiep = '" + MaDN + "' ";
            if (trangThaiDaDuyet)
                query += "  AND kdt.TrangThaiXuLy = 1 ";
            if (tuNgay > DateTime.MinValue)
                query += " and sxxk.NgayDangKy >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND sxxk.NgayDangKy <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.SoToKhai , " +
                        " temp.NamDangKy , " +
                        " temp.MaLoaiHinh , " +
                        " temp.MaHaiQuan , " +
                        " temp.MaDoanhNghiep " +
                        "  FROM   [" + databaseNameTarget + "].dbo.t_SXXK_ToKhaiMauDich temp " +
                        "  WHERE  temp.SoToKhai = kdt.SoToKhai " +
                        " AND YEAR(temp.NgayDangKy) = YEAR(kdt.NgayDangKy) " +
                        " AND temp.MaLoaiHinh = kdt.MaLoaiHinh " +
                        " AND temp.MaHaiQuan = kdt.MaHaiQuan " +
                        " AND temp.MaDoanhNghiep = kdt.MaDoanhNghiep ) ";
            query += " ORDER BY sxxk.NgayDangKy";
            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);
            return ds;
        }

        public static DataSet GetALLDanhSachToKhaiLocalTQDT(string MaHaiQuan, string MaDN, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = " SELECT  kdt.GUIDSTR  , " +
                            " kdt.SoTiepNhan as SoTN, kdt.NgayTiepNhan as NgayTN, kdt.TrangThaiXuLy,  " +
                            "  kdt.PhanLuong  , " +
                            "  kdt.HUONGDAN  , " +
                            "  kdt.SoToKhai AS SoTK  , " +
                            "  kdt.MaLoaiHinh AS Ma_LH  , " +
                            "  kdt.MaHaiQuan AS Ma_HQ  , " +
                            "  ISNULL(kdt.NamDK, YEAR(kdt.NgayDangKy)) AS NamDK , " +
                            "  kdt.NgayDangKy AS Ngay_DK  , " +
                            "  kdt.MaDoanhNghiep AS Ma_DV  , " +
                            "  kdt.MaDaiLyTTHQ  , " +
                            "  kdt.TenDaiLyTTHQ  , " +
                            "  kdt.MaDonViUT AS Ma_DVUT  , " +
                            "  kdt.TenDonViDoiTac AS DV_DT  , " +
                            "  kdt.PTVT_ID AS Ma_PTVT  , " +
                            "  kdt.SoHieuPTVT AS Ten_PTVT  , " +
                            "  kdt.NgayDenPTVT AS NgayDen  , " +
                            "  kdt.SoVanDon AS Van_Don  , " +
                            "  kdt.CuaKhau_ID AS Ma_CK  , " +
                            "  kdt.DiaDiemXepHang AS CangNN  , " +
                            "  kdt.NgayGiayPhep AS Ngay_GP  , " +
                            "  kdt.SoGiayPhep AS So_GP  , " +
                            "  kdt.NgayHetHanGiayPhep AS Ngay_HHGP  , " +
                            "  kdt.SoHopDong AS So_HD  , " +
                            "  kdt.TyGiaUSD AS TyGia_USD  , " +
                            "  kdt.NgayHopDong AS Ngay_HD  , " +
                            "  kdt.NgayHetHanHopDong AS Ngay_HHHD  , " +
                            "  kdt.NuocXK_ID AS Nuoc_XK  , " +
                            "  kdt.NuocNK_ID AS Nuoc_NK  , " +
                            "  kdt.DKGH_ID AS Ma_GH  , " +
                            "  kdt.SoHang AS SoHang  , " +
                            "  kdt.PTTT_ID AS Ma_PTTT  , " +
                            "  kdt.NguyenTe_ID AS Ma_NT  , " +
                            "  kdt.TyGiaTinhThue AS TyGia_VND  , " +
                            "  kdt.LePhiHaiQuan AS LePhi_HQ  , " +
                            "  kdt.GiayTo AS GiayTo  , " +
                            "  kdt.TenChuHang AS TenCH  , " +
                            "  kdt.PhiBaoHiem AS Phi_BH  , " +
                            "  kdt.PhiVanChuyen AS Phi_VC  , " +
                            "  '' AS Ngay_TH  , " +
                            "  '' AS User_TH  , " +
                            "  kdt.TongTriGiaKhaiBao AS TongTGKB  , " +
                            "  kdt.TongTriGiaTinhThue AS TongTGTT  , " +
                            "  kdt.TrongLuong AS Tr_luong  , " +
                            "  kdt.SoKien AS So_Kien  , " +
                            "  kdt.SoContainer20 AS So_Container  , " +
                            "  kdt.SoContainer40 AS So_Container40  , " +
                            "  kdt.SoHoaDonThuongMai AS So_HDTM  , " +
                            "  kdt.NgayHoaDonThuongMai AS Ngay_HDTM  , " +
                            "  kdt.NgayVanDon AS Ngay_VanDon  , " +
                            "  kdt.SoLuongPLTK AS So_PLTK  , " +
                            "  '' AS Ma_MID  , " +
                            "  sxxk.TrangThai AS THANH_LY  , " +
                            "  sxxk.TrangThaiThanhKhoan AS TTTK  , " +
                            "  kdt.LoaiHangHoa AS XUAT_NPL_SP  , " +
                            "  sxxk.NgayHoanThanh AS NGAY_HOANTHANH  " +
                            " FROM    dbo.t_KDT_ToKhaiMauDich kdt  " +
                            "  LEFT JOIN dbo.t_SXXK_ToKhaiMauDich sxxk ON sxxk.NgayDangKy = kdt.NgayDangKy  " +
                            "    AND sxxk.MaDoanhNghiep = kdt.MaDoanhNghiep  " +
                            "    AND sxxk.MaHaiQuan = kdt.MaHaiQuan  " +
                            "    AND sxxk.MaLoaiHinh = kdt.MaLoaiHinh  " +
                            "    AND sxxk.SoToKhai = kdt.SoToKhai  " +
                            " WHERE   kdt.MaHaiQuan = '" + MaHaiQuan + "' " +
                            "  AND kdt.MaDoanhNghiep = '" + MaDN + "' ";
            if (trangThaiDaDuyet)
                query += "  AND kdt.TrangThaiXuLy = 1 ";
            if (tuNgay > DateTime.MinValue && trangThaiDaDuyet == true)
                query += " and kdt.NgayDangKy >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND kdt.NgayDangKy <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            else if (trangThaiDaDuyet == false)
                query += " and kdt.NgayTiepNhan >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND kdt.NgayTiepNhan <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.SoToKhai , " +
                        " temp.NamDK , " +
                        " temp.MaLoaiHinh , " +
                        " temp.MaHaiQuan , " +
                        " temp.MaDoanhNghiep " +
                        "  FROM   [" + databaseNameTarget + "].dbo.t_KDT_ToKhaiMauDich temp " +
                        "  WHERE  temp.SoToKhai = kdt.SoToKhai " +
                        " AND YEAR(temp.NgayDangKy) = YEAR(kdt.NgayDangKy) " +
                        " AND temp.MaLoaiHinh = kdt.MaLoaiHinh " +
                        " AND temp.MaHaiQuan = kdt.MaHaiQuan " +
                        " AND temp.MaDoanhNghiep = kdt.MaDoanhNghiep ) ";
            if (trangThaiDaDuyet)
                query += " ORDER BY kdt.NgayDangKy";
            else
                query += " ORDER BY kdt.NgayTiepNhan";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);
            return ds;
        }

        public static DataSet GetALLDanhSachToKhaiLocalTQDT_ChiLayToKhaiSua(string MaHaiQuan, string MaDN, DateTime tuNgay, DateTime denNgay)
        {
            string query = " SELECT  kdt.GUIDSTR  , " +
                            " kdt.SoTiepNhan as SoTN, kdt.NgayTiepNhan as NgayTN, kdt.TrangThaiXuLy,  " +
                            "  kdt.PhanLuong  , " +
                            "  kdt.HUONGDAN  , " +
                            "  kdt.SoToKhai AS SoTK  , " +
                            "  kdt.MaLoaiHinh AS Ma_LH  , " +
                            "  kdt.MaHaiQuan AS Ma_HQ  , " +
                            "  ISNULL(kdt.NamDK, YEAR(kdt.NgayDangKy)) AS NamDK , " +
                            "  kdt.NgayDangKy AS Ngay_DK  , " +
                            "  kdt.MaDoanhNghiep AS Ma_DV  , " +
                            "  kdt.MaDaiLyTTHQ  , " +
                            "  kdt.TenDaiLyTTHQ  , " +
                            "  kdt.MaDonViUT AS Ma_DVUT  , " +
                            "  kdt.TenDonViDoiTac AS DV_DT  , " +
                            "  kdt.PTVT_ID AS Ma_PTVT  , " +
                            "  kdt.SoHieuPTVT AS Ten_PTVT  , " +
                            "  kdt.NgayDenPTVT AS NgayDen  , " +
                            "  kdt.SoVanDon AS Van_Don  , " +
                            "  kdt.CuaKhau_ID AS Ma_CK  , " +
                            "  kdt.DiaDiemXepHang AS CangNN  , " +
                            "  kdt.NgayGiayPhep AS Ngay_GP  , " +
                            "  kdt.SoGiayPhep AS So_GP  , " +
                            "  kdt.NgayHetHanGiayPhep AS Ngay_HHGP  , " +
                            "  kdt.SoHopDong AS So_HD  , " +
                            "  kdt.TyGiaUSD AS TyGia_USD  , " +
                            "  kdt.NgayHopDong AS Ngay_HD  , " +
                            "  kdt.NgayHetHanHopDong AS Ngay_HHHD  , " +
                            "  kdt.NuocXK_ID AS Nuoc_XK  , " +
                            "  kdt.NuocNK_ID AS Nuoc_NK  , " +
                            "  kdt.DKGH_ID AS Ma_GH  , " +
                            "  kdt.SoHang AS SoHang  , " +
                            "  kdt.PTTT_ID AS Ma_PTTT  , " +
                            "  kdt.NguyenTe_ID AS Ma_NT  , " +
                            "  kdt.TyGiaTinhThue AS TyGia_VND  , " +
                            "  kdt.LePhiHaiQuan AS LePhi_HQ  , " +
                            "  kdt.GiayTo AS GiayTo  , " +
                            "  kdt.TenChuHang AS TenCH  , " +
                            "  kdt.PhiBaoHiem AS Phi_BH  , " +
                            "  kdt.PhiVanChuyen AS Phi_VC  , " +
                            "  '' AS Ngay_TH  , " +
                            "  '' AS User_TH  , " +
                            "  kdt.TongTriGiaKhaiBao AS TongTGKB  , " +
                            "  kdt.TongTriGiaTinhThue AS TongTGTT  , " +
                            "  kdt.TrongLuong AS Tr_luong  , " +
                            "  kdt.SoKien AS So_Kien  , " +
                            "  kdt.SoContainer20 AS So_Container  , " +
                            "  kdt.SoContainer40 AS So_Container40  , " +
                            "  kdt.SoHoaDonThuongMai AS So_HDTM  , " +
                            "  kdt.NgayHoaDonThuongMai AS Ngay_HDTM  , " +
                            "  kdt.NgayVanDon AS Ngay_VanDon  , " +
                            "  kdt.SoLuongPLTK AS So_PLTK  , " +
                            "  '' AS Ma_MID  , " +
                            "  sxxk.TrangThai AS THANH_LY  , " +
                            "  sxxk.TrangThaiThanhKhoan AS TTTK  , " +
                            "  kdt.LoaiHangHoa AS XUAT_NPL_SP  , " +
                            "  sxxk.NgayHoanThanh AS NGAY_HOANTHANH  " +
                            " FROM    dbo.t_KDT_ToKhaiMauDich kdt  " +
                            "  LEFT JOIN dbo.t_SXXK_ToKhaiMauDich sxxk ON sxxk.NgayDangKy = kdt.NgayDangKy  " +
                            "    AND sxxk.MaDoanhNghiep = kdt.MaDoanhNghiep  " +
                            "    AND sxxk.MaHaiQuan = kdt.MaHaiQuan  " +
                            "    AND sxxk.MaLoaiHinh = kdt.MaLoaiHinh  " +
                            "    AND sxxk.SoToKhai = kdt.SoToKhai  " +
                            " WHERE   kdt.MaHaiQuan = '" + MaHaiQuan + "' " +
                            "  AND kdt.MaDoanhNghiep = '" + MaDN + "' ";

            if (tuNgay > DateTime.MinValue)
                query += " and kdt.NgayDangKy >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND kdt.NgayDangKy <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";

            query += " AND EXISTS ( SELECT ( SELECT COUNT(*) FROM [dbo].[t_KDT_SXXK_NPLNhapTon] WHERE TonCuoi < TonDau AND LanThanhLy IN ( SELECT LanThanhLy FROM t_KDT_SXXK_HoSoThanhLyDangKy WHERE TrangThaiThanhKhoan = 401 ) AND MaDoanhNghiep = '" + MaDN + "' AND MaLoaiHinh LIKE 'N%' AND SoToKhai = a.SoToKhai AND NamDangKy = YEAR(a.NgayDangKy) ) AS DongHoSoTHanhKhoan , a.SoToKhai , a.NgayDangKy , a.MaLoaiHinh , a.MaPhu , a.SoLuong , a.ThueXNK , b.SoLuong_SXXK , b.ThueXNK_SXXK , 'GapDoiSoLuong' = b.SoLuong_SXXK / a.SoLuong FROM ( SELECT tk.SoToKhai , tk.NgayDangKy , tk.MaLoaiHinh , h.MaPhu , h.SoLuong , h.ThueXNK FROM dbo.t_KDT_ToKhaiMauDich tk INNER JOIN dbo.t_KDT_HangMauDich h ON tk.ID = h.TKMD_ID WHERE tk.MaLoaiHinh LIKE 'N%' AND tk.TrangThaiXuLy = 1 AND tk.MaHaiQuan = '" + MaHaiQuan + "' AND tk.MaDoanhNghiep = '" + MaDN + "' ) a INNER JOIN ( SELECT tk.SoToKhai , tk.NgayDangKy , tk.MaLoaiHinh , h.MaPhu , h.SoLuong AS SoLuong_SXXK , h.ThueXNK AS ThueXNK_SXXK FROM dbo.t_sxxk_ToKhaiMauDich tk INNER JOIN dbo.t_sxxk_HangMauDich h ON tk.MaHaiQuan = h.MaHaiQuan AND tk.SoToKhai = h.SoToKhai AND tk.MaLoaiHinh = h.MaLoaiHinh AND tk.NamDangKy = h.NamDangKy WHERE tk.MaLoaiHinh LIKE 'N%' AND tk.MaHaiQuan = '" + MaHaiQuan + "' AND tk.MaDoanhNghiep = '" + MaDN + "' ) b ON a.MaLoaiHinh = b.MaLoaiHinh AND a.NgayDangKy = b.NgayDangKy AND a.SoToKhai = b.SoToKhai AND a.MaPhu = b.MaPhu WHERE ( a.SoLuong <> b.SoLuong_SXXK OR a.ThueXNK <> b.ThueXNK_SXXK ) AND ( SELECT COUNT(*) FROM [dbo].[t_KDT_SXXK_NPLNhapTon] WHERE TonCuoi < TonDau AND LanThanhLy IN ( SELECT LanThanhLy FROM t_KDT_SXXK_HoSoThanhLyDangKy WHERE TrangThaiThanhKhoan = 401 ) AND MaDoanhNghiep = '" + MaDN + "' AND MaLoaiHinh LIKE 'N%' AND SoToKhai = a.SoToKhai AND NamDangKy = YEAR(a.NgayDangKy) ) = 0 AND a.SoToKhai= kdt.SoToKhai AND a.MaLoaiHinh=kdt.MaLoaiHinh AND YEAR(a.NgayDangKy)=YEAR(kdt.NgayDangKy))  ";

            query += " ORDER BY kdt.NgayDangKy";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);
            return ds;
        }

        public DataSet GetDanhSachToKhai(string MaHaiQuan, string MaDN, string matKhau, params string[] ToKhai)
        {
            string query = "SELECT  *  from DTOKHAIMD WHERE MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDN + "'";
            query += " ORDER BY NGAY_DK";
            DataSet ds = null;
            ds = WebServiceDB.getDataBySQL(query.ToString(), MaHaiQuan, "SXXK", MaDN, matKhau);
            return ds;
        }
        public static DataSet GetALLDanhSachToKhaiByNgay(string MaHaiQuan, string MaDN, bool isPhongKhai, DateTime ngayBatDau, DateTime ngayKetThuc, string matKhau)
        {
            string query = "SELECT  *  from DTOKHAIMD WHERE MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDN + "' ";
            query += " and NgayDK >=" + ngayBatDau.ToString("MM/dd/yyyy") + " and NgayDK <=" + ngayKetThuc.ToString("MM/dd/yyyy");
            query += " ORDER BY NGAY_DK";
            DataSet ds = null;
            if (!isPhongKhai)
                ds = WebServiceDB.getDataBySQL(query.ToString(), MaHaiQuan, "SXXK", MaDN, matKhau);
            else
                ds = WebServiceDB.getDataBySQLDatabase(query, MaHaiQuan, "SXXK");
            return ds;
        }
        /// <summary>
        /// Ghi de tat ca cac du lieu cu
        /// </summary>
        /// <param name="mahaiquan"></param>
        /// <param name="madv"></param>
        /// <param name="ds"></param>
        /// <param name="dsHang"></param>
        /// <returns></returns>
        public static void InsertGhiDeTatCaDuLieu(string mahaiquan, string madv, DataSet ds, DataSet dsHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DeleteToKhaiKhongThamGiaThanhKhoan(mahaiquan, madv, transaction);
                    if (ds != null)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            ToKhaiMauDich tkmd = new ToKhaiMauDich();
                            tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                            tkmd.MaDaiLyTTHQ = Convert.ToString(row["MA_DVKT"]);
                            tkmd.MaHaiQuan = mahaiquan;
                            //tkmd.TenDaiLyTTHQ = Convert.ToString(row[0]["TenDLTTHQ"]);
                            tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                            tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                            tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                            tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                            tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                            tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;
                            if (tkmd.Load(transaction))
                                continue;
                            try
                            {
                                tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                            }
                            catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                            if (row["Ngay_GP"].ToString() != "")
                                tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                            try
                            {
                                if (row["Ngay_HHGP"].ToString() != "")
                                    tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                            }
                            catch (Exception exx) { }
                            tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                            if (row["Ngay_HD"].ToString() != "")
                                tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                            if (row["Ngay_HHHD"].ToString() != "")
                                tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                            tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                            if (row["Ngay_HDTM"].ToString() != "")
                                tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                            tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                            tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                            if (row["NgayDen"].ToString() != "")
                                tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                            tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                            if (row["Ngay_VanDon"].ToString() != "")
                                tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                            tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                            tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                            tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                            tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                            tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                            tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                            tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                            tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                            tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                            if (row["SoHang"].ToString() != "")
                                tkmd.SoHang = Convert.ToInt16(row["SoHang"].ToString());
                            if (row["So_PLTK"].ToString() != "")
                                tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                            tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                            if (row["So_Container"].ToString() != "")
                                tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                            if (row["So_Container40"].ToString() != "")
                                tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"].ToString());
                            if (row["So_Kien"].ToString() != "")
                                tkmd.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                            if (row["Tr_Luong"].ToString() != "")
                                tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                            if (row["TongTGKB"].ToString() != "")
                                tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"].ToString());
                            if (row["TongTGTT"].ToString() != "")
                                tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                            //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);
                            tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);
                            if (row["LePhi_HQ"].ToString() != "")
                                tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"].ToString());
                            if (row["Phi_BH"].ToString() != "")
                                tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                            if (row["Phi_VC"].ToString() != "")
                                tkmd.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                            tkmd.ThanhLy = row["THANH_LY"].ToString();
                            tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                            tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                            tkmd.ChungTu = row["GIAYTO"].ToString();
                            if (row["NGAY_HOANTHANH"].ToString() != "")
                                tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());

                            tkmd.InsertTransaction(transaction);
                            DataRow[] rowCol = dsHang.Tables[0].Select("SOTK=" + tkmd.SoToKhai + " and NAMDK=" + tkmd.NamDangKy + " and Ma_LH='" + tkmd.MaLoaiHinh + "' and MA_HQ='" + tkmd.MaHaiQuan + "'");
                            foreach (DataRow rowHMD in rowCol)
                            {
                                HangMauDich hmd = new HangMauDich();
                                hmd.MaHaiQuan = rowHMD["MA_HQ"].ToString();
                                hmd.MaLoaiHinh = rowHMD["MA_LH"].ToString();
                                hmd.NamDangKy = (short)Convert.ToInt32(rowHMD["NAMDK"].ToString());
                                hmd.SoToKhai = Convert.ToInt32(rowHMD["SOTK"].ToString());
                                hmd.MaPhu = rowHMD["MA_NPL_SP"].ToString();
                                hmd.TenHang = rowHMD["Ten_Hang"].ToString();
                                hmd.MaHS = rowHMD["MA_HANGKB"].ToString();
                                //if (hmd.MaHS.Length < 10)
                                //{
                                //    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                                //        hmd.MaHS += "0";
                                //}
                                hmd.DVT_ID = rowHMD["Ma_DVT"].ToString();
                                hmd.NuocXX_ID = rowHMD["Nuoc_XX"].ToString();
                                hmd.SoLuong = Convert.ToDecimal(rowHMD["Luong"]);
                                try
                                {
                                    hmd.DonGiaKB = Convert.ToDecimal(rowHMD["DGia_KB"]);
                                }
                                catch (Exception exx) { }
                                try
                                {
                                    hmd.DonGiaTT = Convert.ToDecimal(rowHMD["DGia_TT"]);
                                }
                                catch (Exception exx) { }
                                hmd.TriGiaKB = Convert.ToDecimal(rowHMD["TriGia_KB"]);
                                hmd.TriGiaTT = Convert.ToDecimal(rowHMD["TriGia_TT"]);
                                hmd.TriGiaKB_VND = Convert.ToDecimal(rowHMD["TGKB_VND"]);
                                if (rowHMD["TS_XNK"].ToString() != "")
                                    hmd.ThueSuatXNK = Convert.ToDecimal(rowHMD["TS_XNK"]);
                                if (rowHMD["TS_TTDB"].ToString() != "")
                                    hmd.ThueSuatTTDB = Convert.ToDecimal(rowHMD["TS_TTDB"]);
                                if (rowHMD["TS_VAT"].ToString() != "")
                                    hmd.ThueSuatGTGT = Convert.ToDecimal(rowHMD["TS_VAT"]);
                                if (rowHMD["Thue_XNK"].ToString() != "")
                                    hmd.ThueXNK = Convert.ToDecimal(rowHMD["Thue_XNK"]);
                                if (rowHMD["Thue_TTDB"].ToString() != "")
                                    hmd.ThueTTDB = Convert.ToDecimal(rowHMD["Thue_TTDB"]);
                                if (rowHMD["Thue_VAT"].ToString() != "")
                                    hmd.ThueGTGT = Convert.ToDecimal(rowHMD["Thue_VAT"]);
                                if (rowHMD["Phu_Thu"].ToString() != "")
                                    hmd.PhuThu = Convert.ToDecimal(rowHMD["Phu_Thu"]);
                                if (rowHMD["MienThue"].ToString() != "")
                                    hmd.MienThue = Convert.ToByte(rowHMD["MienThue"]);
                                if (rowHMD["TyLe_ThuKhac"].ToString() != "")
                                    hmd.TyLeThuKhac = Convert.ToDecimal(rowHMD["TyLe_ThuKhac"]);
                                if (rowHMD["TriGia_ThuKhac"].ToString() != "")
                                    hmd.TriGiaThuKhac = Convert.ToDecimal(rowHMD["TriGia_ThuKhac"]);
                                hmd.SoThuTuHang = Convert.ToInt16(rowHMD["STTHANG"]);
                                hmd.InsertTransaction(transaction);
                            }
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            fixDB(madv);
        }

        public static void InsertGhiDeTatCaDuLieu(string mahaiquan, string madv, DataSet ds, DataSet dsHang, string connectionString)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DeleteToKhaiKhongThamGiaThanhKhoan(mahaiquan, madv, transaction);
                    if (ds != null)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            ToKhaiMauDich tkmd = new ToKhaiMauDich();
                            tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                            tkmd.MaDaiLyTTHQ = Convert.ToString(row["MA_DVKT"]);
                            tkmd.MaHaiQuan = mahaiquan;
                            //tkmd.TenDaiLyTTHQ = Convert.ToString(row[0]["TenDLTTHQ"]);
                            tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                            tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                            tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                            tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                            tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                            tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;
                            if (tkmd.Load(transaction))
                                continue;
                            try
                            {
                                tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                            }
                            catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                            if (row["Ngay_GP"].ToString() != "")
                                tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                            try
                            {
                                if (row["Ngay_HHGP"].ToString() != "")
                                    tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                            }
                            catch (Exception exx) { }
                            tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                            if (row["Ngay_HD"].ToString() != "")
                                tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                            if (row["Ngay_HHHD"].ToString() != "")
                                tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                            tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                            if (row["Ngay_HDTM"].ToString() != "")
                                tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                            tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                            tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                            if (row["NgayDen"].ToString() != "")
                                tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                            tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                            if (row["Ngay_VanDon"].ToString() != "")
                                tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                            tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                            tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                            tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                            tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                            tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                            tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                            tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                            tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                            tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                            if (row["SoHang"].ToString() != "")
                                tkmd.SoHang = Convert.ToInt16(row["SoHang"].ToString());
                            if (row["So_PLTK"].ToString() != "")
                                tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                            tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                            if (row["So_Container"].ToString() != "")
                                tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                            if (row["So_Container40"].ToString() != "")
                                tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"].ToString());
                            if (row["So_Kien"].ToString() != "")
                                tkmd.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                            if (row["Tr_Luong"].ToString() != "")
                                tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                            if (row["TongTGKB"].ToString() != "")
                                tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"].ToString());
                            if (row["TongTGTT"].ToString() != "")
                                tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                            //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);
                            tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);
                            if (row["LePhi_HQ"].ToString() != "")
                                tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"].ToString());
                            if (row["Phi_BH"].ToString() != "")
                                tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                            if (row["Phi_VC"].ToString() != "")
                                tkmd.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                            tkmd.ThanhLy = row["THANH_LY"].ToString();
                            tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                            tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                            tkmd.ChungTu = row["GIAYTO"].ToString();
                            if (row["NGAY_HOANTHANH"].ToString() != "")
                                tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());

                            tkmd.InsertTransaction(transaction);
                            DataRow[] rowCol = dsHang.Tables[0].Select("SOTK=" + tkmd.SoToKhai + " and NAMDK=" + tkmd.NamDangKy + " and Ma_LH='" + tkmd.MaLoaiHinh + "' and MA_HQ='" + tkmd.MaHaiQuan + "'");
                            foreach (DataRow rowHMD in rowCol)
                            {
                                HangMauDich hmd = new HangMauDich();
                                hmd.MaHaiQuan = rowHMD["MA_HQ"].ToString();
                                hmd.MaLoaiHinh = rowHMD["MA_LH"].ToString();
                                hmd.NamDangKy = (short)Convert.ToInt32(rowHMD["NAMDK"].ToString());
                                hmd.SoToKhai = Convert.ToInt32(rowHMD["SOTK"].ToString());
                                hmd.MaPhu = rowHMD["MA_NPL_SP"].ToString();
                                hmd.TenHang = rowHMD["Ten_Hang"].ToString();
                                hmd.MaHS = rowHMD["MA_HANGKB"].ToString();
                                //if (hmd.MaHS.Length < 10)
                                //{
                                //    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                                //        hmd.MaHS += "0";
                                //}
                                hmd.DVT_ID = rowHMD["Ma_DVT"].ToString();
                                hmd.NuocXX_ID = rowHMD["Nuoc_XX"].ToString();
                                hmd.SoLuong = Convert.ToDecimal(rowHMD["Luong"]);
                                try
                                {
                                    hmd.DonGiaKB = Convert.ToDecimal(rowHMD["DGia_KB"]);
                                }
                                catch (Exception exx) { }
                                try
                                {
                                    hmd.DonGiaTT = Convert.ToDecimal(rowHMD["DGia_TT"]);
                                }
                                catch (Exception exx) { }
                                hmd.TriGiaKB = Convert.ToDecimal(rowHMD["TriGia_KB"]);
                                hmd.TriGiaTT = Convert.ToDecimal(rowHMD["TriGia_TT"]);
                                hmd.TriGiaKB_VND = Convert.ToDecimal(rowHMD["TGKB_VND"]);
                                if (rowHMD["TS_XNK"].ToString() != "")
                                    hmd.ThueSuatXNK = Convert.ToDecimal(rowHMD["TS_XNK"]);
                                if (rowHMD["TS_TTDB"].ToString() != "")
                                    hmd.ThueSuatTTDB = Convert.ToDecimal(rowHMD["TS_TTDB"]);
                                if (rowHMD["TS_VAT"].ToString() != "")
                                    hmd.ThueSuatGTGT = Convert.ToDecimal(rowHMD["TS_VAT"]);
                                if (rowHMD["Thue_XNK"].ToString() != "")
                                    hmd.ThueXNK = Convert.ToDecimal(rowHMD["Thue_XNK"]);
                                if (rowHMD["Thue_TTDB"].ToString() != "")
                                    hmd.ThueTTDB = Convert.ToDecimal(rowHMD["Thue_TTDB"]);
                                if (rowHMD["Thue_VAT"].ToString() != "")
                                    hmd.ThueGTGT = Convert.ToDecimal(rowHMD["Thue_VAT"]);
                                if (rowHMD["Phu_Thu"].ToString() != "")
                                    hmd.PhuThu = Convert.ToDecimal(rowHMD["Phu_Thu"]);
                                if (rowHMD["MienThue"].ToString() != "")
                                    hmd.MienThue = Convert.ToByte(rowHMD["MienThue"]);
                                if (rowHMD["TyLe_ThuKhac"].ToString() != "")
                                    hmd.TyLeThuKhac = Convert.ToDecimal(rowHMD["TyLe_ThuKhac"]);
                                if (rowHMD["TriGia_ThuKhac"].ToString() != "")
                                    hmd.TriGiaThuKhac = Convert.ToDecimal(rowHMD["TriGia_ThuKhac"]);
                                hmd.SoThuTuHang = Convert.ToInt16(rowHMD["STTHANG"]);
                                hmd.InsertTransaction(transaction);
                            }
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            fixDB(madv);
        }

        public static void InsertGhiDeTatCaDuLieuTQDT(string mahaiquan, string madv, DataSet ds, DataSet dsHang, string connectionString)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DeleteToKhaiKhongThamGiaThanhKhoan(mahaiquan, madv, transaction);
                    if (ds != null)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            ToKhaiMauDich tkmd = new ToKhaiMauDich();
                            tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                            tkmd.MaDaiLyTTHQ = Convert.ToString(row["MA_DVKT"]);
                            tkmd.MaHaiQuan = mahaiquan;
                            //tkmd.TenDaiLyTTHQ = Convert.ToString(row[0]["TenDLTTHQ"]);
                            tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                            tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                            tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                            tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                            tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                            tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;
                            if (tkmd.Load(transaction))
                                continue;
                            try
                            {
                                tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                            }
                            catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                            if (row["Ngay_GP"].ToString() != "")
                                tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                            try
                            {
                                if (row["Ngay_HHGP"].ToString() != "")
                                    tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                            }
                            catch (Exception exx) { }
                            tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                            if (row["Ngay_HD"].ToString() != "")
                                tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                            if (row["Ngay_HHHD"].ToString() != "")
                                tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                            tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                            if (row["Ngay_HDTM"].ToString() != "")
                                tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                            tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                            tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                            if (row["NgayDen"].ToString() != "")
                                tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                            tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                            if (row["Ngay_VanDon"].ToString() != "")
                                tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                            tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                            tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                            tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                            tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                            tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                            tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                            tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                            tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                            tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                            if (row["SoHang"].ToString() != "")
                                tkmd.SoHang = Convert.ToInt16(row["SoHang"].ToString());
                            if (row["So_PLTK"].ToString() != "")
                                tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                            tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                            if (row["So_Container"].ToString() != "")
                                tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                            if (row["So_Container40"].ToString() != "")
                                tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"].ToString());
                            if (row["So_Kien"].ToString() != "")
                                tkmd.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                            if (row["Tr_Luong"].ToString() != "")
                                tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                            if (row["TongTGKB"].ToString() != "")
                                tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"].ToString());
                            if (row["TongTGTT"].ToString() != "")
                                tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                            //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);
                            tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);
                            if (row["LePhi_HQ"].ToString() != "")
                                tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"].ToString());
                            if (row["Phi_BH"].ToString() != "")
                                tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                            if (row["Phi_VC"].ToString() != "")
                                tkmd.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                            tkmd.ThanhLy = row["THANH_LY"].ToString();
                            tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                            tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                            tkmd.ChungTu = row["GIAYTO"].ToString();
                            if (row["NGAY_HOANTHANH"].ToString() != "")
                                tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());

                            tkmd.InsertTransaction(transaction);
                            DataRow[] rowCol = dsHang.Tables[0].Select("SOTK=" + tkmd.SoToKhai + " and NAMDK=" + tkmd.NamDangKy + " and Ma_LH='" + tkmd.MaLoaiHinh + "' and MA_HQ='" + tkmd.MaHaiQuan + "'");
                            foreach (DataRow rowHMD in rowCol)
                            {
                                HangMauDich hmd = new HangMauDich();
                                hmd.MaHaiQuan = rowHMD["MA_HQ"].ToString();
                                hmd.MaLoaiHinh = rowHMD["MA_LH"].ToString();
                                hmd.NamDangKy = (short)Convert.ToInt32(rowHMD["NAMDK"].ToString());
                                hmd.SoToKhai = Convert.ToInt32(rowHMD["SOTK"].ToString());
                                hmd.MaPhu = rowHMD["MA_NPL_SP"].ToString();
                                hmd.TenHang = rowHMD["Ten_Hang"].ToString();
                                hmd.MaHS = rowHMD["MA_HANGKB"].ToString();
                                //if (hmd.MaHS.Length < 10)
                                //{
                                //    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                                //        hmd.MaHS += "0";
                                //}
                                hmd.DVT_ID = rowHMD["Ma_DVT"].ToString();
                                hmd.NuocXX_ID = rowHMD["Nuoc_XX"].ToString();
                                hmd.SoLuong = Convert.ToDecimal(rowHMD["Luong"]);
                                try
                                {
                                    hmd.DonGiaKB = Convert.ToDecimal(rowHMD["DGia_KB"]);
                                }
                                catch (Exception exx) { }
                                try
                                {
                                    hmd.DonGiaTT = Convert.ToDecimal(rowHMD["DGia_TT"]);
                                }
                                catch (Exception exx) { }
                                hmd.TriGiaKB = Convert.ToDecimal(rowHMD["TriGia_KB"]);
                                hmd.TriGiaTT = Convert.ToDecimal(rowHMD["TriGia_TT"]);
                                hmd.TriGiaKB_VND = Convert.ToDecimal(rowHMD["TGKB_VND"]);
                                if (rowHMD["TS_XNK"].ToString() != "")
                                    hmd.ThueSuatXNK = Convert.ToDecimal(rowHMD["TS_XNK"]);
                                if (rowHMD["TS_TTDB"].ToString() != "")
                                    hmd.ThueSuatTTDB = Convert.ToDecimal(rowHMD["TS_TTDB"]);
                                if (rowHMD["TS_VAT"].ToString() != "")
                                    hmd.ThueSuatGTGT = Convert.ToDecimal(rowHMD["TS_VAT"]);
                                if (rowHMD["Thue_XNK"].ToString() != "")
                                    hmd.ThueXNK = Convert.ToDecimal(rowHMD["Thue_XNK"]);
                                if (rowHMD["Thue_TTDB"].ToString() != "")
                                    hmd.ThueTTDB = Convert.ToDecimal(rowHMD["Thue_TTDB"]);
                                if (rowHMD["Thue_VAT"].ToString() != "")
                                    hmd.ThueGTGT = Convert.ToDecimal(rowHMD["Thue_VAT"]);
                                if (rowHMD["Phu_Thu"].ToString() != "")
                                    hmd.PhuThu = Convert.ToDecimal(rowHMD["Phu_Thu"]);
                                if (rowHMD["MienThue"].ToString() != "")
                                    hmd.MienThue = Convert.ToByte(rowHMD["MienThue"]);
                                if (rowHMD["TyLe_ThuKhac"].ToString() != "")
                                    hmd.TyLeThuKhac = Convert.ToDecimal(rowHMD["TyLe_ThuKhac"]);
                                if (rowHMD["TriGia_ThuKhac"].ToString() != "")
                                    hmd.TriGiaThuKhac = Convert.ToDecimal(rowHMD["TriGia_ThuKhac"]);
                                hmd.SoThuTuHang = Convert.ToInt16(rowHMD["STTHANG"]);
                                hmd.InsertTransaction(transaction);
                            }
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            fixDB(madv);
        }

        /// <summary>
        /// Bo qua nhuwng du lieu da co tren may
        /// </summary>
        /// <param name="mahaiquan"></param>
        /// <param name="madv"></param>
        /// <param name="ds"></param>
        /// <param name="dsHang"></param>
        /// <returns></returns>
        public static void InsertDuLieuMoi(string mahaiquan, string madv, DataSet ds, DataSet dsHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (ds != null)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            ToKhaiMauDich tkmd = new ToKhaiMauDich();
                            tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                            tkmd.MaDaiLyTTHQ = Convert.ToString(row["MA_DVKT"]);
                            tkmd.MaHaiQuan = mahaiquan;
                            //tkmd.TenDaiLyTTHQ = Convert.ToString(row[0]["TenDLTTHQ"]);
                            tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                            tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                            tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                            tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                            tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                            tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;
                            if (tkmd.Load(transaction))
                                continue;
                            try
                            {
                                tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                            }
                            catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                            if (row["Ngay_GP"].ToString() != "")
                                tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                            try
                            {
                                if (row["Ngay_HHGP"].ToString() != "")
                                    tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                            }
                            catch (Exception exx) { }
                            tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                            if (row["Ngay_HD"].ToString() != "")
                                tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                            if (row["Ngay_HHHD"].ToString() != "")
                                tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                            tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                            if (row["Ngay_HDTM"].ToString() != "")
                                tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                            tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                            tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                            if (row["NgayDen"].ToString() != "")
                                tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                            tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                            if (row["Ngay_VanDon"].ToString() != "")
                                tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                            tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                            tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                            tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                            tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                            tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                            tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                            tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                            tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                            tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                            if (row["SoHang"].ToString() != "")
                                tkmd.SoHang = Convert.ToInt16(row["SoHang"].ToString());
                            if (row["So_PLTK"].ToString() != "")
                                tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                            tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                            if (row["So_Container"].ToString() != "")
                                tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                            if (row["So_Container40"].ToString() != "")
                                tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"].ToString());
                            if (row["So_Kien"].ToString() != "")
                                tkmd.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                            if (row["Tr_Luong"].ToString() != "")
                                tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                            if (row["TongTGKB"].ToString() != "")
                                tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"].ToString());
                            if (row["TongTGTT"].ToString() != "")
                                tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                            //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);
                            tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);
                            if (row["LePhi_HQ"].ToString() != "")
                                tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"].ToString());
                            if (row["Phi_BH"].ToString() != "")
                                tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                            if (row["Phi_VC"].ToString() != "")
                                tkmd.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                            tkmd.ThanhLy = row["THANH_LY"].ToString();
                            tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                            tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                            tkmd.ChungTu = row["GIAYTO"].ToString();
                            if (row["NGAY_HOANTHANH"].ToString() != "")
                                tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());

                            tkmd.InsertTransaction(transaction);
                            DataRow[] rowCol = dsHang.Tables[0].Select("SOTK=" + tkmd.SoToKhai + " and NAMDK=" + tkmd.NamDangKy + " and Ma_LH='" + tkmd.MaLoaiHinh + "' and MA_HQ='" + tkmd.MaHaiQuan + "'");
                            foreach (DataRow rowHMD in rowCol)
                            {
                                HangMauDich hmd = new HangMauDich();
                                hmd.MaHaiQuan = rowHMD["MA_HQ"].ToString();
                                hmd.MaLoaiHinh = rowHMD["MA_LH"].ToString();
                                hmd.NamDangKy = (short)Convert.ToInt32(rowHMD["NAMDK"].ToString());
                                hmd.SoToKhai = Convert.ToInt32(rowHMD["SOTK"].ToString());
                                hmd.MaPhu = rowHMD["MA_NPL_SP"].ToString();
                                hmd.TenHang = rowHMD["Ten_Hang"].ToString();
                                hmd.MaHS = rowHMD["MA_HANGKB"].ToString();
                                //if (hmd.MaHS.Length < 10)
                                //{
                                //    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                                //        hmd.MaHS += "0";
                                //}
                                hmd.DVT_ID = rowHMD["Ma_DVT"].ToString();
                                hmd.NuocXX_ID = rowHMD["Nuoc_XX"].ToString();
                                hmd.SoLuong = Convert.ToDecimal(rowHMD["Luong"]);
                                try
                                {
                                    hmd.DonGiaKB = Convert.ToDecimal(rowHMD["DGia_KB"]);
                                }
                                catch (Exception exx) { }
                                try
                                {
                                    hmd.DonGiaTT = Convert.ToDecimal(rowHMD["DGia_TT"]);
                                }
                                catch (Exception exx) { }
                                hmd.TriGiaKB = Convert.ToDecimal(rowHMD["TriGia_KB"]);
                                hmd.TriGiaTT = Convert.ToDecimal(rowHMD["TriGia_TT"]);
                                hmd.TriGiaKB_VND = Convert.ToDecimal(rowHMD["TGKB_VND"]);
                                if (rowHMD["TS_XNK"].ToString() != "")
                                    hmd.ThueSuatXNK = Convert.ToDecimal(rowHMD["TS_XNK"]);
                                if (rowHMD["TS_TTDB"].ToString() != "")
                                    hmd.ThueSuatTTDB = Convert.ToDecimal(rowHMD["TS_TTDB"]);
                                if (rowHMD["TS_VAT"].ToString() != "")
                                    hmd.ThueSuatGTGT = Convert.ToDecimal(rowHMD["TS_VAT"]);
                                if (rowHMD["Thue_XNK"].ToString() != "")
                                    hmd.ThueXNK = Convert.ToDecimal(rowHMD["Thue_XNK"]);
                                if (rowHMD["Thue_TTDB"].ToString() != "")
                                    hmd.ThueTTDB = Convert.ToDecimal(rowHMD["Thue_TTDB"]);
                                if (rowHMD["Thue_VAT"].ToString() != "")
                                    hmd.ThueGTGT = Convert.ToDecimal(rowHMD["Thue_VAT"]);
                                if (rowHMD["Phu_Thu"].ToString() != "")
                                    hmd.PhuThu = Convert.ToDecimal(rowHMD["Phu_Thu"]);
                                if (rowHMD["MienThue"].ToString() != "")
                                    hmd.MienThue = Convert.ToByte(rowHMD["MienThue"]);
                                if (rowHMD["TyLe_ThuKhac"].ToString() != "")
                                    hmd.TyLeThuKhac = Convert.ToDecimal(rowHMD["TyLe_ThuKhac"]);
                                if (rowHMD["TriGia_ThuKhac"].ToString() != "")
                                    hmd.TriGiaThuKhac = Convert.ToDecimal(rowHMD["TriGia_ThuKhac"]);
                                hmd.SoThuTuHang = Convert.ToInt16(rowHMD["STTHANG"]);
                                hmd.InsertTransaction(transaction);
                            }
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            fixDB(madv);
        }

        public static void InsertDuLieuMoi(string mahaiquan, string madv, DataSet ds, DataSet dsHang, string connectionString)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (ds != null)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            ToKhaiMauDich tkmd = new ToKhaiMauDich();
                            tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                            tkmd.MaDaiLyTTHQ = Convert.ToString(row["MaDaiLyTTHQ"]);
                            tkmd.MaHaiQuan = mahaiquan;
                            //tkmd.TenDaiLyTTHQ = Convert.ToString(row[0]["TenDaiLyTTHQ"]);
                            tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                            tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                            tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                            tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                            tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                            tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;
                            if (tkmd.Load(transaction, db))
                                continue;
                            try
                            {
                                tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                            }
                            catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                            if (row["Ngay_GP"].ToString() != "")
                                tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                            try
                            {
                                if (row["Ngay_HHGP"].ToString() != "")
                                    tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                            }
                            catch (Exception exx) { }
                            tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                            if (row["Ngay_HD"].ToString() != "")
                                tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                            if (row["Ngay_HHHD"].ToString() != "")
                                tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                            tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                            if (row["Ngay_HDTM"].ToString() != "")
                                tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                            tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                            tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                            if (row["NgayDen"].ToString() != "")
                                tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                            tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                            if (row["Ngay_VanDon"].ToString() != "")
                                tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                            tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                            tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                            tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                            tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                            tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                            tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                            tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                            tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                            tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                            if (row["SoHang"].ToString() != "")
                                tkmd.SoHang = Convert.ToInt16(row["SoHang"].ToString());
                            if (row["So_PLTK"].ToString() != "")
                                tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                            tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                            if (row["So_Container"].ToString() != "")
                                tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                            if (row["So_Container40"].ToString() != "")
                                tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"].ToString());
                            if (row["So_Kien"].ToString() != "")
                                tkmd.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                            if (row["Tr_Luong"].ToString() != "")
                                tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                            if (row["TongTGKB"].ToString() != "")
                                tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"].ToString());
                            if (row["TongTGTT"].ToString() != "")
                                tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                            //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);
                            tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);
                            if (row["LePhi_HQ"].ToString() != "")
                                tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"].ToString());
                            if (row["Phi_BH"].ToString() != "")
                                tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                            if (row["Phi_VC"].ToString() != "")
                                tkmd.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                            tkmd.ThanhLy = row["THANH_LY"].ToString();
                            tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                            tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                            tkmd.ChungTu = row["GIAYTO"].ToString();
                            if (row["NGAY_HOANTHANH"].ToString() != "")
                                tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());

                            tkmd.InsertTransaction(transaction, db);

                            DataRow[] rowCol = dsHang.Tables[0].Select("SOTK=" + tkmd.SoToKhai + " and NAMDK=" + tkmd.NamDangKy + " and Ma_LH='" + tkmd.MaLoaiHinh + "' and MA_HQ='" + tkmd.MaHaiQuan + "'");
                            foreach (DataRow rowHMD in rowCol)
                            {
                                HangMauDich hmd = new HangMauDich();
                                hmd.MaHaiQuan = rowHMD["MA_HQ"].ToString();
                                hmd.MaLoaiHinh = rowHMD["MA_LH"].ToString();
                                hmd.NamDangKy = (short)Convert.ToInt32(rowHMD["NAMDK"].ToString());
                                hmd.SoToKhai = Convert.ToInt32(rowHMD["SOTK"].ToString());
                                hmd.MaPhu = rowHMD["MA_NPL_SP"].ToString();
                                hmd.TenHang = rowHMD["Ten_Hang"].ToString();
                                hmd.MaHS = rowHMD["MA_HANGKB"].ToString();
                                //if (hmd.MaHS.Length < 10)
                                //{
                                //    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                                //        hmd.MaHS += "0";
                                //}
                                hmd.DVT_ID = rowHMD["Ma_DVT"].ToString();
                                hmd.NuocXX_ID = rowHMD["Nuoc_XX"].ToString();
                                hmd.SoLuong = Convert.ToDecimal(rowHMD["Luong"]);
                                try
                                {
                                    hmd.DonGiaKB = Convert.ToDecimal(rowHMD["DGia_KB"]);
                                }
                                catch (Exception exx) { }
                                try
                                {
                                    hmd.DonGiaTT = Convert.ToDecimal(rowHMD["DGia_TT"]);
                                }
                                catch (Exception exx) { }
                                hmd.TriGiaKB = Convert.ToDecimal(rowHMD["TriGia_KB"]);
                                hmd.TriGiaTT = Convert.ToDecimal(rowHMD["TriGia_TT"]);
                                hmd.TriGiaKB_VND = Convert.ToDecimal(rowHMD["TGKB_VND"]);
                                if (rowHMD["TS_XNK"].ToString() != "")
                                    hmd.ThueSuatXNK = Convert.ToDecimal(rowHMD["TS_XNK"]);
                                if (rowHMD["TS_TTDB"].ToString() != "")
                                    hmd.ThueSuatTTDB = Convert.ToDecimal(rowHMD["TS_TTDB"]);
                                if (rowHMD["TS_VAT"].ToString() != "")
                                    hmd.ThueSuatGTGT = Convert.ToDecimal(rowHMD["TS_VAT"]);
                                if (rowHMD["Thue_XNK"].ToString() != "")
                                    hmd.ThueXNK = Convert.ToDecimal(rowHMD["Thue_XNK"]);
                                if (rowHMD["Thue_TTDB"].ToString() != "")
                                    hmd.ThueTTDB = Convert.ToDecimal(rowHMD["Thue_TTDB"]);
                                if (rowHMD["Thue_VAT"].ToString() != "")
                                    hmd.ThueGTGT = Convert.ToDecimal(rowHMD["Thue_VAT"]);
                                if (rowHMD["Phu_Thu"].ToString() != "")
                                    hmd.PhuThu = Convert.ToDecimal(rowHMD["Phu_Thu"]);
                                if (rowHMD["MienThue"].ToString() != "")
                                    hmd.MienThue = Convert.ToByte(rowHMD["MienThue"]);
                                if (rowHMD["TyLe_ThuKhac"].ToString() != "")
                                    hmd.TyLeThuKhac = Convert.ToDecimal(rowHMD["TyLe_ThuKhac"]);
                                if (rowHMD["TriGia_ThuKhac"].ToString() != "")
                                    hmd.TriGiaThuKhac = Convert.ToDecimal(rowHMD["TriGia_ThuKhac"]);
                                hmd.SoThuTuHang = Convert.ToInt16(rowHMD["STTHANG"]);

                                hmd.InsertTransaction(transaction, db);
                            }
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            fixDB(madv);
        }

        private static void DeleteToKhaiKhongThamGiaThanhKhoan(string MaHaiQuan, string MaDoanhNghiep, SqlTransaction transaction)
        {
            string sql = "delete FROM t_SXXK_ToKhaiMauDich  " +
                        "WHERE rtrim(MaHaiQuan)+RTRIM(MaLoaiHinh)+cast(SoToKhai AS VARCHAR)+CAST(NamDangKy AS VARCHAR) " +
                        "NOT IN  " +
                        "(" +
                            "SELECT 	rtrim(tksbkn.MaHaiQuan)+RTRIM(tksbkn.MaLoaiHinh)+cast(tksbkn.SoToKhai AS VARCHAR)+CAST(tksbkn.NamDangKy AS VARCHAR) " +
                            "FROM t_KDT_SXXK_BKToKhaiNhap tksbkn " +
                            "UNION  " +
                         "   SELECT 	rtrim(tksbkx.MaHaiQuan)+RTRIM(tksbkx.MaLoaiHinh)+cast(tksbkx.SoToKhai AS VARCHAR)+CAST(tksbkx.NamDangKy AS VARCHAR) " +
                         "   FROM t_KDT_SXXK_BKToKhaiXuat tksbkx  " +
                        ") and MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.ExecuteNonQuery(dbCommand, transaction);

            sql = "delete FROM t_SXXK_ThanhLy_NPLNhapTon  " +
                        "WHERE rtrim(MaHaiQuan)+RTRIM(MaLoaiHinh)+cast(SoToKhai AS VARCHAR)+CAST(NamDangKy AS VARCHAR) " +
                        "NOT IN  " +
                        "(" +
                            "SELECT 	rtrim(tksbkn.MaHaiQuan)+RTRIM(tksbkn.MaLoaiHinh)+cast(tksbkn.SoToKhai AS VARCHAR)+CAST(tksbkn.NamDangKy AS VARCHAR) " +
                            "FROM t_KDT_SXXK_BKToKhaiNhap tksbkn " +
                            "UNION  " +
                         "   SELECT 	rtrim(tksbkx.MaHaiQuan)+RTRIM(tksbkx.MaLoaiHinh)+cast(tksbkx.SoToKhai AS VARCHAR)+CAST(tksbkx.NamDangKy AS VARCHAR) " +
                         "   FROM t_KDT_SXXK_BKToKhaiXuat tksbkx  " +
                        ") and MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan";
            dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            dbCommand.Parameters.Clear();
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.ExecuteNonQuery(dbCommand, transaction);
        }

        public static void InsertDuLieuTQDT(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, DataSet dsHang, string connectionString)
        {
            try
            {
                Company.BLL.KDT.ToKhaiMauDichCollection kdtTKMDCollection = new Company.BLL.KDT.ToKhaiMauDichCollection();

                //Create collection DinhMucDangKy
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();

                    //Kiem tra neu TKMD co GUID ID thi kiem tra ton tai To khai tren Database Target dua tren GUID.
                    if (row["GUIDSTR"].ToString().Length != 0)
                        tkmd.Load(row["GUIDSTR"].ToString());
                    //Kiem tra ton tai To khai tren Database Source dua tren cap khoa: this._MaHaiQuan, this._MaDoanhNghiep, this._SoToKhai, this._NamDK, this._MaLoaiHinh, this._TrangThaiXuLy 
                    else
                        tkmd.Load(MaHaiQuan, MaDoanhNghiep, Convert.ToInt32(row["SoTK"]), Convert.ToInt32(row["NamDK"]), row["Ma_LH"].ToString());

                    if (tkmd.NamDK == 0)
                    {
                        tkmd.NamDK = tkmd.NgayDangKy.Year;
                    }

                    if (tkmd != null && tkmd.ID != 0)
                    {
                        //Load Hang mau dich cua To khai.
                        tkmd.LoadHMDCollection();

                        //Load Chung tu kem lien quan cua To khai.
                        tkmd.LoadChungTuHaiQuan();

                        //Load KetQuaXuLy lien quan cua To khai.
                        tkmd.LoadKetQuaXuLy();

                        //Load HuyToKhai lien quan cua To khai.
                        tkmd.LoadHuyToKhai();

                        //Load MsgSend lien quan cua To khai.
                        tkmd.LoadMsgSend();

                        kdtTKMDCollection.Add(tkmd);
                    }
                }

                //Insert data 
                int cnt = 0;
                foreach (Company.BLL.KDT.ToKhaiMauDich item in kdtTKMDCollection)
                {
                    cnt += 1;
                    item.InsertUpdateFull(connectionString, cnt);
                    
                    //Chuyen trang thai to khai vao Dang ky
                    item.TransgferDataToSXXK(connectionString);

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw new Exception(ex.Message); }
        }

        public static void InsertDuLieuKTX(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, DataSet dsHang, string connectionString)
        {
            try
            {
                SqlDatabase dbTarget = new SqlDatabase(connectionString);

                Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection sxxkTKMDCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();

                //Create collection
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmd = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();

                    tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                    tkmd.MaDaiLyTTHQ = ""; // Convert.ToString(row["MaDaiLyTTHQ"]);
                    tkmd.TenDaiLyTTHQ = ""; // Convert.ToString(row["TenDaiLyTTHQ"]);
                    tkmd.MaHaiQuan = MaHaiQuan;
                    tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                    tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                    tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                    tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                    tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                    tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;

                    if (tkmd.Load(null, dbTarget))
                        continue;

                    try
                    {
                        tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                    }
                    catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                    if (row["Ngay_GP"].ToString() != "")
                        tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                    try
                    {
                        if (row["Ngay_HHGP"].ToString() != "")
                            tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                    }
                    catch (Exception exx) { }
                    tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                    if (row["Ngay_HD"].ToString() != "")
                        tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                    if (row["Ngay_HHHD"].ToString() != "")
                        tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                    tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                    if (row["Ngay_HDTM"].ToString() != "")
                        tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                    tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                    tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                    if (row["NgayDen"].ToString() != "")
                        tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                    tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                    if (row["Ngay_VanDon"].ToString() != "")
                        tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                    tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                    tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                    tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                    tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                    tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                    tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                    tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"]);
                    tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                    tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                    if (row["SoHang"].ToString() != "")
                        tkmd.SoHang = Convert.ToInt16(row["SoHang"]);
                    if (row["So_PLTK"].ToString() != "")
                        tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"]);
                    tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                    if (row["So_Container"].ToString() != "")
                        tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"]);
                    if (row["So_Container40"].ToString() != "")
                        tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"]);
                    if (row["So_Kien"].ToString() != "")
                        tkmd.SoKien = Convert.ToDecimal(row["So_Kien"]);
                    if (row["Tr_Luong"].ToString() != "")
                        tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"]);
                    if (row["TongTGKB"].ToString() != "")
                        tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"]);
                    if (row["TongTGTT"].ToString() != "")
                        tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                    //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);
                    tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);
                    if (row["LePhi_HQ"].ToString() != "")
                        tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"]);
                    if (row["Phi_BH"].ToString() != "")
                        tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"]);
                    if (row["Phi_VC"].ToString() != "")
                        tkmd.PhiVanChuyen = System.Convert.ToDecimal(row["Phi_VC"]);
                    tkmd.ThanhLy = row["THANH_LY"].ToString();
                    tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                    tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                    tkmd.ChungTu = row["GIAYTO"].ToString();
                    if (row["NGAY_HOANTHANH"].ToString() != "")
                        tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());

                    //Hang Mau dich cua To khai
                    DataRow[] rowCol = dsHang.Tables[0].Select("SOTK=" + tkmd.SoToKhai + " and NAMDK=" + tkmd.NamDangKy + " and Ma_LH='" + tkmd.MaLoaiHinh + "' and MA_HQ='" + tkmd.MaHaiQuan + "'");
                    foreach (DataRow rowHMD in rowCol)
                    {
                        Company.BLL.SXXK.ToKhai.HangMauDich hmd = new Company.BLL.SXXK.ToKhai.HangMauDich();

                        hmd.MaHaiQuan = rowHMD["MA_HQ"].ToString();
                        hmd.MaLoaiHinh = rowHMD["MA_LH"].ToString();
                        hmd.NamDangKy = (short)Convert.ToInt32(rowHMD["NAMDK"].ToString());
                        hmd.SoToKhai = Convert.ToInt32(rowHMD["SOTK"].ToString());
                        hmd.MaPhu = rowHMD["MA_NPL_SP"].ToString();
                        hmd.TenHang = rowHMD["Ten_Hang"].ToString();
                        hmd.MaHS = rowHMD["MA_HANGKB"].ToString();
                        //if (hmd.MaHS.Length < 10)
                        //{
                        //    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                        //        hmd.MaHS += "0";
                        //}
                        hmd.DVT_ID = rowHMD["Ma_DVT"].ToString();
                        hmd.NuocXX_ID = rowHMD["Nuoc_XX"].ToString();
                        hmd.SoLuong = Convert.ToDecimal(rowHMD["Luong"]);
                        try
                        {
                            hmd.DonGiaKB = Convert.ToDecimal(rowHMD["DGia_KB"]);
                        }
                        catch (Exception exx) { Logger.LocalLogger.Instance().WriteMessage(exx); }
                        try
                        {
                            hmd.DonGiaTT = Convert.ToDecimal(rowHMD["DGia_TT"]);
                        }
                        catch (Exception exx) { Logger.LocalLogger.Instance().WriteMessage(exx); }

                        hmd.TriGiaKB = Convert.ToDecimal(rowHMD["TriGia_KB"]);
                        hmd.TriGiaTT = Convert.ToDecimal(rowHMD["TriGia_TT"]);
                        hmd.TriGiaKB_VND = Convert.ToDecimal(rowHMD["TGKB_VND"]);
                        if (rowHMD["TS_XNK"].ToString() != "")
                            hmd.ThueSuatXNK = Convert.ToDecimal(rowHMD["TS_XNK"]);
                        if (rowHMD["TS_TTDB"].ToString() != "")
                            hmd.ThueSuatTTDB = Convert.ToDecimal(rowHMD["TS_TTDB"]);
                        if (rowHMD["TS_VAT"].ToString() != "")
                            hmd.ThueSuatGTGT = Convert.ToDecimal(rowHMD["TS_VAT"]);
                        if (rowHMD["Thue_XNK"].ToString() != "")
                            hmd.ThueXNK = Convert.ToDecimal(rowHMD["Thue_XNK"]);
                        if (rowHMD["Thue_TTDB"].ToString() != "")
                            hmd.ThueTTDB = Convert.ToDecimal(rowHMD["Thue_TTDB"]);
                        if (rowHMD["Thue_VAT"].ToString() != "")
                            hmd.ThueGTGT = Convert.ToDecimal(rowHMD["Thue_VAT"]);
                        if (rowHMD["Phu_Thu"].ToString() != "")
                            hmd.PhuThu = Convert.ToDecimal(rowHMD["Phu_Thu"]);
                        if (rowHMD["MienThue"].ToString() != "")
                            hmd.MienThue = Convert.ToByte(rowHMD["MienThue"]);
                        if (rowHMD["TyLe_ThuKhac"].ToString() != "")
                            hmd.TyLeThuKhac = Convert.ToDecimal(rowHMD["TyLe_ThuKhac"]);
                        if (rowHMD["TriGia_ThuKhac"].ToString() != "")
                            hmd.TriGiaThuKhac = Convert.ToDecimal(rowHMD["TriGia_ThuKhac"]);
                        hmd.SoThuTuHang = Convert.ToInt16(rowHMD["STTHANG"]);

                        //Add Hang vao To khai
                        tkmd.HMDCollection.Add(hmd);
                    }

                    //Add To Khai vao Collection
                    sxxkTKMDCollection.Add(tkmd);

                }

                //Insert data
                int cnt = 0;
                foreach (Company.BLL.SXXK.ToKhai.ToKhaiMauDich item in sxxkTKMDCollection)
                {
                    cnt += 1;
                    item.InsertUpdateFull(connectionString, cnt);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw new Exception(ex.Message); }
        }


        public static DataSet GetKDTToKhaiMauDich(DateTime Fromday, string ConnectionStringTarget)
        {
            string _fromdate = Fromday.ToString("yyyy-MM-dd");
            string sql = @"SELECT * FROM dbo.t_KDT_ToKhaiMauDich 
                           WHERE CONVERT(DATE,NgayDangKy,102) > CONVERT(DATE,'" + _fromdate + "',102)";
            Database db = new SqlDatabase(ConnectionStringTarget);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            
            return db.ExecuteDataSet(dbCommand);
        }

        public static DataSet getToKhaiTQDTFromHQ(string sql ,string MaDoanhNghiep, string MaHQ, DateTime FromDay, DateTime ToDay)
        {
//            string _fromDate = FromDay.ToString("yyyy-MM-dd");
//            string sql = @"SELECT TOP 5 TKMD.TKID,TKMD.SOTN,TKMD.SoTKChinhThuc,TKMD.NgayDKChinhThuc,TKMD.Reference,TKMD.Ma_DV FROM dbo.Temp_DToKhaiMD AS TKMD
//WHERE  TKMD.MA_DV = '0900277558001'";
            return WebServiceDB.getDataBySQL(sql, "P33D", "SXXK", "0900277558001", "0900277558001");
        }
    }
}
