﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KD.BLL;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT;

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class HopDongForm : Company.Interface.BaseForm
    {

        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        public HopDongThuongMai HopDongTM = new HopDongThuongMai();
        public HopDongForm()
        {
            InitializeComponent();
        }

        private bool checkSoHopDong(string soHopDong)
        {
            foreach (HopDongThuongMai HDTM in TKMD.HopDongThuongMaiCollection)
            {
                if (HDTM.SoHopDongTM.Trim().ToUpper() == soHopDong.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HopDongThuongMaiDetail> HopDongThuongMaiDetailCollection = new List<HopDongThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongThuongMaiDetail hdtmdtmp = new HopDongThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HopDongThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HopDongThuongMaiDetail hdtmtmp in HopDongThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HopDongThuongMaiDetail hdtmd in HopDongTM.ListHangMDOfHopDong)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HopDongTM.ListHangMDOfHopDong.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
        private void BindData()
        {
            Company.KD.BLL.KDT.HangMauDich hmd = new Company.KD.BLL.KDT.HangMauDich();
            hmd.TKMD_ID = TKMD.ID;

            dgList.DataSource = HopDongTM.ConvertListToDataSet(hmd.SelectBy_TKMD_ID().Tables[0]);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (HopDongTM.ListHangMDOfHopDong.Count == 0)
            {
                ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                return;
            }


            TKMD.HopDongThuongMaiCollection.Remove(HopDongTM);

            if (checkSoHopDong(txtSoHopDong.Text))
            {
                if (HopDongTM.ID > 0)
                    TKMD.HopDongThuongMaiCollection.Add(HopDongTM);
                ShowMessage("Số hợp đồng này đã tồn tại.", false);
                return;
            }
            HopDongTM.DiaDiemGiaoHang = txtDiaDiemGiaoHang.Text.Trim();
            HopDongTM.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
            HopDongTM.MaDonViBan = txtMaDVBan.Text.Trim();
            HopDongTM.MaDonViMua = txtMaDVMua.Text.Trim();
            HopDongTM.NgayHopDongTM = ccNgayHopDong.Value;
            HopDongTM.NguyenTe_ID = nguyenTeControl1.Ma;
            HopDongTM.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
            HopDongTM.SoHopDongTM = txtSoHopDong.Text.Trim();
            HopDongTM.TenDonViBan = txtTenDVBan.Text.Trim();
            HopDongTM.TenDonViMua = txtTenDVMua.Text.Trim();
            HopDongTM.ThoiHanThanhToan = ccThoiHanTT.Value;
            HopDongTM.TongTriGia = Convert.ToDecimal(txtTongTriGia.Text);
            HopDongTM.ThongTinKhac = txtThongTinKhac.Text.Trim();
            HopDongTM.TKMD_ID = TKMD.ID;
            if (isKhaiBoSung)
                HopDongTM.LoaiKB = 1;
            else
                HopDongTM.LoaiKB = 0;
            HopDongTM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            GridEXRow[] rowCollection = dgList.GetRows();
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (HopDongThuongMaiDetail item in HopDongTM.ListHangMDOfHopDong)
                {
                    if (rowview["HMD_ID"].ToString().Trim() == item.HMD_ID.ToString().Trim())
                    {
                        item.GhiChu = row.Cells["GhiChu"].Text;

                        item.MaHS = rowview["MaHS"].ToString();
                        item.MaPhu = rowview["MaPhu"].ToString();
                        item.TenHang = rowview["TenHang"].ToString();
                        item.NuocXX_ID = rowview["NuocXX_ID"].ToString();
                        item.DVT_ID = rowview["DVT_ID"].ToString();
                        item.SoLuong = Convert.ToDecimal(rowview["SoLuong"]);
                        item.DonGiaKB = Convert.ToDouble(rowview["DonGiaKB"]);
                        item.TriGiaKB = Convert.ToDouble(rowview["TriGiaKB"]);
                        break;
                    }
                }
            }
            try
            {
                if (string.IsNullOrEmpty(HopDongTM.GuidStr)) HopDongTM.GuidStr = Guid.NewGuid().ToString();
                HopDongTM.InsertUpdateFull();
                TKMD.HopDongThuongMaiCollection.Add(HopDongTM);
                BindData();
                ShowMessage("Lưu thành công.", false);
                if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET && TKMD.SoToKhai > 0)
                    btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }


        }
        public HangMauDichCollection ConvertListToHangMauDichCollection(List<HopDongThuongMaiDetail> listHangHopDong, HangMauDichCollection hangCollection)
        {

            HangMauDichCollection tmp = new HangMauDichCollection();

            foreach (HopDongThuongMaiDetail item in listHangHopDong)
            {
                foreach (Company.KD.BLL.KDT.HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất hàng khi kích nút Chọn hàng
            //if (HopDongTM.ListHangMDOfHopDong.Count > 0)
            //{
            //    f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(HopDongTM.ListHangMDOfHopDong, TKMD.HMDCollection);
            //}
            f.ShowDialog();
            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (Company.KD.BLL.KDT.HangMauDich HMD in f.HMDTMPCollection)
                {
                    bool ok = false;
                    foreach (HopDongThuongMaiDetail hangHopDongDetail in HopDongTM.ListHangMDOfHopDong)
                    {
                        if (hangHopDongDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HopDongThuongMaiDetail hopDongDetail = new HopDongThuongMaiDetail();
                        hopDongDetail.HMD_ID = HMD.ID;
                        hopDongDetail.HopDongTM_ID = HopDongTM.ID;
                        hopDongDetail.MaPhu = HMD.MaPhu;
                        hopDongDetail.MaHS = HMD.MaHS;
                        hopDongDetail.TenHang = HMD.TenHang;
                        hopDongDetail.DVT_ID = HMD.DVT_ID;
                        hopDongDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hopDongDetail.SoLuong = HMD.SoLuong;
                        hopDongDetail.NuocXX_ID = HMD.NuocXX_ID;
                        hopDongDetail.DonGiaKB = Convert.ToDouble(HMD.DonGiaKB);
                        hopDongDetail.TriGiaKB = Convert.ToDouble(HMD.TriGiaKB);
                        HopDongTM.ListHangMDOfHopDong.Add(hopDongDetail);
                    }
                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void HopDongForm_Load(object sender, EventArgs e)
        {
            // an cac button truoc khi load
            btnKhaiBao.Enabled = false;
            btnLayPhanHoi.Enabled = false;

            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.DisplayMember = cbDKGH.ValueMember = "ID";
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            //DATLMQ bổ sung lấy HĐ từ TKMD 13/12/2010
            if (HopDongTM != null && HopDongTM.ID <= 0)
            {
                HopDongTM.SoHopDongTM = TKMD.SoHopDong;
                txtSoHopDong.Text = HopDongTM.SoHopDongTM;
                HopDongTM.NgayHopDongTM = TKMD.NgayHopDong;
                ccNgayHopDong.Text = HopDongTM.NgayHopDongTM.ToShortDateString();
                HopDongTM.ThoiHanThanhToan = TKMD.NgayHetHanHopDong;
                ccThoiHanTT.Value = HopDongTM.ThoiHanThanhToan;
                //DATLMQ kiểm tra loại hình tờ khai 30/12/2010
                if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    txtMaDVMua.Text = TKMD.MaDoanhNghiep;
                    txtTenDVMua.Text = TKMD.TenDoanhNghiep;
                    txtTenDVBan.Text = TKMD.TenDonViDoiTac;
                }
                else
                {
                    txtTenDVMua.Text = TKMD.TenDonViDoiTac;
                    txtMaDVBan.Text = TKMD.MaDoanhNghiep;
                    txtTenDVBan.Text = TKMD.TenDoanhNghiep;
                }
                //DATLMQ update DiaDiemGiaoHang 17/01/2011
                txtDiaDiemGiaoHang.Text = Company.KD.BLL.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
            }

            else if (HopDongTM != null && HopDongTM.ID > 0)
            {
                txtDiaDiemGiaoHang.Text = HopDongTM.DiaDiemGiaoHang;
                cbDKGH.SelectedValue = HopDongTM.DKGH_ID;
                txtMaDVBan.Text = HopDongTM.MaDonViBan;
                txtMaDVMua.Text = HopDongTM.MaDonViMua;
                ccNgayHopDong.Value = HopDongTM.NgayHopDongTM;
                nguyenTeControl1.Ma = HopDongTM.NguyenTe_ID;
                cbPTTT.SelectedValue = HopDongTM.PTTT_ID;
                txtSoHopDong.Text = HopDongTM.SoHopDongTM;
                txtTenDVBan.Text = HopDongTM.TenDonViBan;
                txtTenDVMua.Text = HopDongTM.TenDonViMua;
                ccThoiHanTT.Value = HopDongTM.ThoiHanThanhToan;
                txtTongTriGia.Text = HopDongTM.TongTriGia.ToString();
                txtThongTinKhac.Text = HopDongTM.ThongTinKhac;
                ccNgayHopDong.Text = HopDongTM.NgayHopDongTM.ToShortDateString();
                ccThoiHanTT.Text = HopDongTM.ThoiHanThanhToan.ToShortDateString();
                BindData();
            }

            if (TKMD.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (HopDongTM.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                ccNgayTiepNhan.Text = HopDongTM.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = HopDongTM.SoTiepNhan + "";
            }
            else if (TKMD.SoToKhai > 0 && int.Parse(TKMD.PhanLuong != "" ? TKMD.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            SetButtonStateHOPDONG(TKMD, isKhaiBoSung, HopDongTM);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (HopDongTM.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                bool thanhcong = HopDongTM.WSKhaiBaoBoSungHopDong(GlobalSettings.PassWordDT, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK, TKMD.ID, TKMD.MaDoanhNghiep, null, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao, TKMD.ConvertHMDKDToHangMauDich());
                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                if (this.HopDongTM.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.HopDongTM.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = HopDongTM.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HopDongTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHopDongThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.HopDongTM.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.HopDongTM.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.HopDongTM.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = HopDongTM.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HopDongTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHopDongDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HopDongTM.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    this.ShowMessage(message, false);
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }


        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOP DONG.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOPDONG(Company.KD.BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong)
        {
            if (hopdong == null)
                return false;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO
                    );

                btnXoa.Enabled = status;
                btnChonHang.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    status = true;

                    btnXoa.Enabled = status;
                    btnChonHang.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXuLy.Enabled = true;
                    //Neu hop dong chua co so tiep nhan -> phai khai bao
                    if (hopdong.SoTiepNhan == 0)
                    {
                        btnKhaiBao.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                    }
                    //Neu hop dong da co so tiep nhan -> co the lay phan hoi
                    else
                    {
                        btnKhaiBao.Enabled = false;
                        btnLayPhanHoi.Enabled = true;
                    }


                }
            }

            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (HopDongTM.GuidStr != null && HopDongTM.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(HopDongTM.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            List<HopDongThuongMaiDetail> HopDongThuongMaiDetailCollection = new List<HopDongThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongThuongMaiDetail hdtmdtmp = new HopDongThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HopDongThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HopDongThuongMaiDetail hdtmtmp in HopDongThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HopDongThuongMaiDetail hdtmd in HopDongTM.ListHangMDOfHopDong)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HopDongTM.ListHangMDOfHopDong.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            List<HopDongThuongMaiDetail> HopDongThuongMaiDetailCollection = new List<HopDongThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongThuongMaiDetail hdtmdtmp = new HopDongThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HopDongThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HopDongThuongMaiDetail hdtmtmp in HopDongThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HopDongThuongMaiDetail hdtmd in HopDongTM.ListHangMDOfHopDong)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HopDongTM.ListHangMDOfHopDong.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
    }
}

