using System;
using System.Data;
using System.Data.SqlClient;
using Company.BLL;

namespace Company.QuanTri
{
	public partial class User : EntityBase
	{
		#region Private members.
		
		protected string _USER_NAME = String.Empty;
		protected string _PASSWORD = String.Empty;
		protected string _HO_TEN = String.Empty;
		protected string _MO_TA = String.Empty;
		protected bool _isAdmin=false;
		protected long _ID;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public string USER_NAME
		{
			set {this._USER_NAME = value;}
			get {return this._USER_NAME;}
		}
		public string PASSWORD
		{
			set {this._PASSWORD = value;}
			get {return this._PASSWORD;}
		}
		public string HO_TEN
		{
			set {this._HO_TEN = value;}
			get {return this._HO_TEN;}
		}
		public string MO_TA
		{
			set {this._MO_TA = value;}
			get {return this._MO_TA;}
		}
		public bool isAdmin
		{
			set {this._isAdmin = value;}
			get {return this._isAdmin;}
		}
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_User_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("USER_NAME"))) this._USER_NAME = reader.GetString(reader.GetOrdinal("USER_NAME"));
				if (!reader.IsDBNull(reader.GetOrdinal("PASSWORD"))) this._PASSWORD = reader.GetString(reader.GetOrdinal("PASSWORD"));
				if (!reader.IsDBNull(reader.GetOrdinal("HO_TEN"))) this._HO_TEN = reader.GetString(reader.GetOrdinal("HO_TEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MO_TA"))) this._MO_TA = reader.GetString(reader.GetOrdinal("MO_TA"));
				if (!reader.IsDBNull(reader.GetOrdinal("isAdmin"))) this._isAdmin = reader.GetBoolean(reader.GetOrdinal("isAdmin"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		


		public DataSet SelectAll()
        {
            string spName = "p_User_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_User_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_User_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_User_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public UserCollection SelectCollectionAll()
		{
			UserCollection collection = new UserCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				User entity = new User();
				
				if (!reader.IsDBNull(reader.GetOrdinal("USER_NAME"))) entity.USER_NAME = reader.GetString(reader.GetOrdinal("USER_NAME"));
				if (!reader.IsDBNull(reader.GetOrdinal("PASSWORD"))) entity.PASSWORD = reader.GetString(reader.GetOrdinal("PASSWORD"));
				if (!reader.IsDBNull(reader.GetOrdinal("HO_TEN"))) entity.HO_TEN = reader.GetString(reader.GetOrdinal("HO_TEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MO_TA"))) entity.MO_TA = reader.GetString(reader.GetOrdinal("MO_TA"));
				if (!reader.IsDBNull(reader.GetOrdinal("isAdmin"))) entity.isAdmin = reader.GetBoolean(reader.GetOrdinal("isAdmin"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public UserCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			UserCollection collection = new UserCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				User entity = new User();
				
				if (!reader.IsDBNull(reader.GetOrdinal("USER_NAME"))) entity.USER_NAME = reader.GetString(reader.GetOrdinal("USER_NAME"));
				if (!reader.IsDBNull(reader.GetOrdinal("PASSWORD"))) entity.PASSWORD = reader.GetString(reader.GetOrdinal("PASSWORD"));
				if (!reader.IsDBNull(reader.GetOrdinal("HO_TEN"))) entity.HO_TEN = reader.GetString(reader.GetOrdinal("HO_TEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MO_TA"))) entity.MO_TA = reader.GetString(reader.GetOrdinal("MO_TA"));
				if (!reader.IsDBNull(reader.GetOrdinal("isAdmin"))) entity.isAdmin = reader.GetBoolean(reader.GetOrdinal("isAdmin"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_User_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, this._USER_NAME);
			this.db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, this._PASSWORD);
			this.db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, this._HO_TEN);
			this.db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, this._MO_TA);
			this.db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, this._isAdmin);
			this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			
			if (transaction != null)
			{
				this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(UserCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (User item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, UserCollection collection)
        {
            foreach (User item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_User_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, this._USER_NAME);
			this.db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, this._PASSWORD);
			this.db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, this._HO_TEN);
			this.db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, this._MO_TA);
			this.db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, this._isAdmin);
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(UserCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (User item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_User_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, this._USER_NAME);
			this.db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, this._PASSWORD);
			this.db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, this._HO_TEN);
			this.db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, this._MO_TA);
			this.db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, this._isAdmin);
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(UserCollection collection, SqlTransaction transaction)
        {
            foreach (User item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_User_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(UserCollection collection, SqlTransaction transaction)
        {
            foreach (User item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(UserCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (User item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}

        //---------------------------------------------------------------------------------------------

       
		#endregion
	}	
}