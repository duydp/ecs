﻿namespace Company.Interface
{
    partial class ChonThuMucDaiLyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChonThuMucDaiLyForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.txtThuMuc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnChon = new Janus.Windows.EditControls.UIButton();
            this.btnXuat = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.ThuMucDaiLy = new System.Windows.Forms.FolderBrowserDialog();
            this.cbStatus = new Janus.Windows.EditControls.UIComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.cbStatus);
            this.grbMain.Controls.Add(this.btnXuat);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnChon);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.txtThuMuc);
            this.grbMain.Size = new System.Drawing.Size(385, 105);
            // 
            // txtThuMuc
            // 
            this.txtThuMuc.Location = new System.Drawing.Point(15, 31);
            this.txtThuMuc.Name = "txtThuMuc";
            this.txtThuMuc.Size = new System.Drawing.Size(333, 21);
            this.txtThuMuc.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chọn thư mục chứa các file nhập từ đại lý";
            // 
            // btnChon
            // 
            this.btnChon.Location = new System.Drawing.Point(354, 31);
            this.btnChon.Name = "btnChon";
            this.btnChon.Size = new System.Drawing.Size(19, 21);
            this.btnChon.TabIndex = 2;
            this.btnChon.Text = "...";
            this.btnChon.VisualStyleManager = this.vsmMain;
            this.btnChon.Click += new System.EventHandler(this.btnChon_Click);
            // 
            // btnXuat
            // 
            this.btnXuat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuat.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXuat.Icon")));
            this.btnXuat.Location = new System.Drawing.Point(201, 68);
            this.btnXuat.Name = "btnXuat";
            this.btnXuat.Size = new System.Drawing.Size(86, 25);
            this.btnXuat.TabIndex = 3;
            this.btnXuat.Text = "Thực hiện";
            this.btnXuat.VisualStyleManager = this.vsmMain;
            this.btnXuat.Click += new System.EventHandler(this.btnXuat_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(293, 68);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 25);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cbStatus
            // 
            this.cbStatus.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Ghi đè";
            uiComboBoxItem1.Value = -1;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.FormatStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Dữ liệu mới";
            uiComboBoxItem2.Value = 0;
            this.cbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbStatus.Location = new System.Drawing.Point(15, 70);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(159, 21);
            this.cbStatus.TabIndex = 273;
            this.cbStatus.VisualStyleManager = this.vsmMain;
            // 
            // ChonThuMucDaiLyForm
            // 
            this.AcceptButton = this.btnXuat;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(385, 105);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(393, 139);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(393, 139);
            this.Name = "ChonThuMucDaiLyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chọn đường dẫn";
            this.Load += new System.EventHandler(this.ChonThuMucDaiLyForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnChon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtThuMuc;
        private Janus.Windows.EditControls.UIButton btnXuat;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.FolderBrowserDialog ThuMucDaiLy;
        private Janus.Windows.EditControls.UIComboBox cbStatus;
    }
}