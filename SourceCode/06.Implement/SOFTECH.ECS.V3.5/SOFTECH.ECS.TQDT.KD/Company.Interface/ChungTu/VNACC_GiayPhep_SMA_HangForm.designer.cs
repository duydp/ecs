﻿namespace Company.Interface
{
    partial class VNACC_GiayPhep_SMA_HangForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_GiayPhep_SMA_HangForm));
            Janus.Windows.GridEX.GridEXLayout grdHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucDonViTinhLuongTonKyTruoc = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuongTonKhoDenNgay = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongSo = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtHuHao = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongSoXuatTrongKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txtLuongNhapTrongKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtLuongTonKhoKyTruoc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtQuanHuyenDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtQuanHuyenCongTyCungCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtQuanHuyenCongTySanXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtQuanHuyenXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuChiTiet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoNhaTenDuongDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoNhaTenDuongCongTyCungCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoNhaTenDuongCongTySanXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoNhaTenDuongXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTinhThanhPhoDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTinhThanhPhoCongTyCungCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTinhThanhPhoCongTySanXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTinhThanhPhoXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongXaDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongXaCongTyCungCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongXaCongTySanXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongXaXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucMaQuocGiaDonViUyThac = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucMaQuocGiaCongTyCungCap = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucMaQuocGiaCongTySanXuat = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucMaQuocGiaXuatKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTenDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtTenCongTyCungCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtTenCongTySanXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhCongTyCungCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenThuongNhanXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhCongTySanXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.dtHanDung = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ucDonViTinhSoLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucDonViTinhKhoiLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoDangKy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTieuChuanChatLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtHoatChat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCongDung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTenHoatChatGayNghien = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaSoHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtGiaBanLeVND = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtGiaBanBuonVND = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGiaNhapKhauVND = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTongSoKhoiLuongHoatChatGayNghien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TongSoKhoiLuongHoatChatGayNghien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTenThuocQuyCachDongGoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.grdHang = new Janus.Windows.GridEX.GridEX();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMaSoHangHoa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaSoHangHoa)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 559), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 559);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 535);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 535);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.splitContainer1);
            this.grbMain.Size = new System.Drawing.Size(702, 559);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhLuongTonKyTruoc);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongTonKhoDenNgay);
            this.uiGroupBox1.Controls.Add(this.txtTongSo);
            this.uiGroupBox1.Controls.Add(this.label45);
            this.uiGroupBox1.Controls.Add(this.label43);
            this.uiGroupBox1.Controls.Add(this.txtHuHao);
            this.uiGroupBox1.Controls.Add(this.txtTongSoXuatTrongKy);
            this.uiGroupBox1.Controls.Add(this.label47);
            this.uiGroupBox1.Controls.Add(this.label46);
            this.uiGroupBox1.Controls.Add(this.label44);
            this.uiGroupBox1.Controls.Add(this.txtLuongNhapTrongKy);
            this.uiGroupBox1.Controls.Add(this.label42);
            this.uiGroupBox1.Controls.Add(this.txtLuongTonKhoKyTruoc);
            this.uiGroupBox1.Controls.Add(this.label41);
            this.uiGroupBox1.Controls.Add(this.label40);
            this.uiGroupBox1.Controls.Add(this.label33);
            this.uiGroupBox1.Controls.Add(this.label26);
            this.uiGroupBox1.Controls.Add(this.label21);
            this.uiGroupBox1.Controls.Add(this.label39);
            this.uiGroupBox1.Controls.Add(this.label32);
            this.uiGroupBox1.Controls.Add(this.label25);
            this.uiGroupBox1.Controls.Add(this.label22);
            this.uiGroupBox1.Controls.Add(this.label38);
            this.uiGroupBox1.Controls.Add(this.label31);
            this.uiGroupBox1.Controls.Add(this.label24);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.label37);
            this.uiGroupBox1.Controls.Add(this.label30);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.label16);
            this.uiGroupBox1.Controls.Add(this.txtQuanHuyenDonViUyThac);
            this.uiGroupBox1.Controls.Add(this.txtQuanHuyenCongTyCungCap);
            this.uiGroupBox1.Controls.Add(this.txtQuanHuyenCongTySanXuat);
            this.uiGroupBox1.Controls.Add(this.txtQuanHuyenXuatKhau);
            this.uiGroupBox1.Controls.Add(this.txtGhiChuChiTiet);
            this.uiGroupBox1.Controls.Add(this.txtSoNhaTenDuongDonViUyThac);
            this.uiGroupBox1.Controls.Add(this.txtSoNhaTenDuongCongTyCungCap);
            this.uiGroupBox1.Controls.Add(this.txtSoNhaTenDuongCongTySanXuat);
            this.uiGroupBox1.Controls.Add(this.txtSoNhaTenDuongXuatKhau);
            this.uiGroupBox1.Controls.Add(this.txtTinhThanhPhoDonViUyThac);
            this.uiGroupBox1.Controls.Add(this.txtTinhThanhPhoCongTyCungCap);
            this.uiGroupBox1.Controls.Add(this.txtTinhThanhPhoCongTySanXuat);
            this.uiGroupBox1.Controls.Add(this.txtTinhThanhPhoXuatKhau);
            this.uiGroupBox1.Controls.Add(this.txtPhuongXaDonViUyThac);
            this.uiGroupBox1.Controls.Add(this.txtPhuongXaCongTyCungCap);
            this.uiGroupBox1.Controls.Add(this.txtPhuongXaCongTySanXuat);
            this.uiGroupBox1.Controls.Add(this.txtPhuongXaXuatKhau);
            this.uiGroupBox1.Controls.Add(this.ucMaQuocGiaDonViUyThac);
            this.uiGroupBox1.Controls.Add(this.ucMaQuocGiaCongTyCungCap);
            this.uiGroupBox1.Controls.Add(this.ucMaQuocGiaCongTySanXuat);
            this.uiGroupBox1.Controls.Add(this.ucMaQuocGiaXuatKhau);
            this.uiGroupBox1.Controls.Add(this.txtTenDonViUyThac);
            this.uiGroupBox1.Controls.Add(this.label36);
            this.uiGroupBox1.Controls.Add(this.txtTenCongTyCungCap);
            this.uiGroupBox1.Controls.Add(this.label29);
            this.uiGroupBox1.Controls.Add(this.txtTenCongTySanXuat);
            this.uiGroupBox1.Controls.Add(this.txtMaBuuChinhDonViUyThac);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.txtMaBuuChinhCongTyCungCap);
            this.uiGroupBox1.Controls.Add(this.txtTenThuongNhanXuatKhau);
            this.uiGroupBox1.Controls.Add(this.label35);
            this.uiGroupBox1.Controls.Add(this.txtMaBuuChinhCongTySanXuat);
            this.uiGroupBox1.Controls.Add(this.label28);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.label34);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.txtMaBuuChinhXuatKhau);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.label55);
            this.uiGroupBox1.Controls.Add(this.label57);
            this.uiGroupBox1.Controls.Add(this.label58);
            this.uiGroupBox1.Controls.Add(this.dtHanDung);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhSoLuong);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhKhoiLuong);
            this.uiGroupBox1.Controls.Add(this.txtSoDangKy);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.txtTieuChuanChatLuong);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.txtHoatChat);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtCongDung);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.txtTenHoatChatGayNghien);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtMaSoHangHoa);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtSoLuong);
            this.uiGroupBox1.Controls.Add(this.txtGiaBanLeVND);
            this.uiGroupBox1.Controls.Add(this.txtGiaBanBuonVND);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.txtGiaNhapKhauVND);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.txtTongSoKhoiLuongHoatChatGayNghien);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.TongSoKhoiLuongHoatChatGayNghien);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.txtTenThuocQuyCachDongGoi);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(696, 374);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ucDonViTinhLuongTonKyTruoc
            // 
            this.ucDonViTinhLuongTonKyTruoc.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDonViTinhLuongTonKyTruoc.Appearance.Options.UseBackColor = true;
            this.ucDonViTinhLuongTonKyTruoc.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ucDonViTinhLuongTonKyTruoc.Code = "";
            this.ucDonViTinhLuongTonKyTruoc.ColorControl = System.Drawing.Color.Empty;
            this.ucDonViTinhLuongTonKyTruoc.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhLuongTonKyTruoc.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhLuongTonKyTruoc.IsOnlyWarning = false;
            this.ucDonViTinhLuongTonKyTruoc.IsValidate = true;
            this.ucDonViTinhLuongTonKyTruoc.Location = new System.Drawing.Point(273, 758);
            this.ucDonViTinhLuongTonKyTruoc.Name = "ucDonViTinhLuongTonKyTruoc";
            this.ucDonViTinhLuongTonKyTruoc.Name_VN = "";
            this.ucDonViTinhLuongTonKyTruoc.SetOnlyWarning = false;
            this.ucDonViTinhLuongTonKyTruoc.SetValidate = false;
            this.ucDonViTinhLuongTonKyTruoc.ShowColumnCode = true;
            this.ucDonViTinhLuongTonKyTruoc.ShowColumnName = false;
            this.ucDonViTinhLuongTonKyTruoc.Size = new System.Drawing.Size(71, 26);
            this.ucDonViTinhLuongTonKyTruoc.TabIndex = 45;
            this.ucDonViTinhLuongTonKyTruoc.TagName = "";
            this.ucDonViTinhLuongTonKyTruoc.Where = null;
            this.ucDonViTinhLuongTonKyTruoc.WhereCondition = "";
            this.ucDonViTinhLuongTonKyTruoc.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtSoLuongTonKhoDenNgay
            // 
            this.txtSoLuongTonKhoDenNgay.DecimalDigits = 0;
            this.txtSoLuongTonKhoDenNgay.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongTonKhoDenNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongTonKhoDenNgay.Location = new System.Drawing.Point(488, 815);
            this.txtSoLuongTonKhoDenNgay.MaxLength = 8;
            this.txtSoLuongTonKhoDenNgay.Name = "txtSoLuongTonKhoDenNgay";
            this.txtSoLuongTonKhoDenNgay.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongTonKhoDenNgay.Size = new System.Drawing.Size(184, 21);
            this.txtSoLuongTonKhoDenNgay.TabIndex = 49;
            this.txtSoLuongTonKhoDenNgay.Text = "0";
            this.txtSoLuongTonKhoDenNgay.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongTonKhoDenNgay.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongTonKhoDenNgay.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongSo
            // 
            this.txtTongSo.DecimalDigits = 0;
            this.txtTongSo.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSo.Location = new System.Drawing.Point(488, 788);
            this.txtTongSo.MaxLength = 9;
            this.txtTongSo.Name = "txtTongSo";
            this.txtTongSo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSo.Size = new System.Drawing.Size(184, 21);
            this.txtTongSo.TabIndex = 47;
            this.txtTongSo.Text = "0";
            this.txtTongSo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSo.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(352, 820);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 13);
            this.label45.TabIndex = 52;
            this.label45.Text = "Số lượng tồn kho đến ngày";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(352, 793);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(45, 13);
            this.label43.TabIndex = 52;
            this.label43.Text = "Tổng số";
            // 
            // txtHuHao
            // 
            this.txtHuHao.DecimalDigits = 0;
            this.txtHuHao.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtHuHao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHuHao.Location = new System.Drawing.Point(146, 842);
            this.txtHuHao.MaxLength = 8;
            this.txtHuHao.Name = "txtHuHao";
            this.txtHuHao.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtHuHao.Size = new System.Drawing.Size(198, 21);
            this.txtHuHao.TabIndex = 50;
            this.txtHuHao.Text = "0";
            this.txtHuHao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtHuHao.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtHuHao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongSoXuatTrongKy
            // 
            this.txtTongSoXuatTrongKy.DecimalDigits = 0;
            this.txtTongSoXuatTrongKy.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSoXuatTrongKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoXuatTrongKy.Location = new System.Drawing.Point(146, 815);
            this.txtTongSoXuatTrongKy.MaxLength = 9;
            this.txtTongSoXuatTrongKy.Name = "txtTongSoXuatTrongKy";
            this.txtTongSoXuatTrongKy.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoXuatTrongKy.Size = new System.Drawing.Size(198, 21);
            this.txtTongSoXuatTrongKy.TabIndex = 48;
            this.txtTongSoXuatTrongKy.Text = "0";
            this.txtTongSoXuatTrongKy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoXuatTrongKy.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoXuatTrongKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(12, 873);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(77, 13);
            this.label47.TabIndex = 52;
            this.label47.Text = "Ghi chú chi tiết";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(12, 847);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(42, 13);
            this.label46.TabIndex = 52;
            this.label46.Text = "Hư hao";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(12, 820);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(113, 13);
            this.label44.TabIndex = 52;
            this.label44.Text = "Tổng số xuất trong kỳ";
            // 
            // txtLuongNhapTrongKy
            // 
            this.txtLuongNhapTrongKy.DecimalDigits = 0;
            this.txtLuongNhapTrongKy.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtLuongNhapTrongKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongNhapTrongKy.Location = new System.Drawing.Point(146, 788);
            this.txtLuongNhapTrongKy.MaxLength = 8;
            this.txtLuongNhapTrongKy.Name = "txtLuongNhapTrongKy";
            this.txtLuongNhapTrongKy.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLuongNhapTrongKy.Size = new System.Drawing.Size(198, 21);
            this.txtLuongNhapTrongKy.TabIndex = 46;
            this.txtLuongNhapTrongKy.Text = "0";
            this.txtLuongNhapTrongKy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLuongNhapTrongKy.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLuongNhapTrongKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(12, 793);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(107, 13);
            this.label42.TabIndex = 52;
            this.label42.Text = "Lượng nhập trong kỳ";
            // 
            // txtLuongTonKhoKyTruoc
            // 
            this.txtLuongTonKhoKyTruoc.DecimalDigits = 0;
            this.txtLuongTonKhoKyTruoc.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtLuongTonKhoKyTruoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongTonKhoKyTruoc.Location = new System.Drawing.Point(146, 761);
            this.txtLuongTonKhoKyTruoc.MaxLength = 8;
            this.txtLuongTonKhoKyTruoc.Name = "txtLuongTonKhoKyTruoc";
            this.txtLuongTonKhoKyTruoc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLuongTonKhoKyTruoc.Size = new System.Drawing.Size(121, 21);
            this.txtLuongTonKhoKyTruoc.TabIndex = 44;
            this.txtLuongTonKhoKyTruoc.Text = "0";
            this.txtLuongTonKhoKyTruoc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLuongTonKhoKyTruoc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLuongTonKhoKyTruoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLuongTonKhoKyTruoc.VisualStyleManager = this.vsmMain;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(12, 766);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(119, 13);
            this.label41.TabIndex = 52;
            this.label41.Text = "Lượng tồn kho kỳ trước";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(37, 738);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(70, 13);
            this.label40.TabIndex = 48;
            this.label40.Text = "Quận, huyện";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(37, 630);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(70, 13);
            this.label33.TabIndex = 48;
            this.label33.Text = "Quận, huyện";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(37, 522);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 13);
            this.label26.TabIndex = 48;
            this.label26.Text = "Quận, huyện";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(37, 414);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(70, 13);
            this.label21.TabIndex = 48;
            this.label21.Text = "Quận, huyện";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(357, 738);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(83, 13);
            this.label39.TabIndex = 50;
            this.label39.Text = "Tỉnh, thành phố";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(357, 630);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(83, 13);
            this.label32.TabIndex = 50;
            this.label32.Text = "Tỉnh, thành phố";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(357, 522);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 13);
            this.label25.TabIndex = 50;
            this.label25.Text = "Tỉnh, thành phố";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(357, 414);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(83, 13);
            this.label22.TabIndex = 50;
            this.label22.Text = "Tỉnh, thành phố";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(360, 711);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(63, 13);
            this.label38.TabIndex = 46;
            this.label38.Text = "Phường, xã";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(360, 603);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(63, 13);
            this.label31.TabIndex = 46;
            this.label31.Text = "Phường, xã";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(360, 495);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 13);
            this.label24.TabIndex = 46;
            this.label24.Text = "Phường, xã";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(360, 387);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 13);
            this.label15.TabIndex = 46;
            this.label15.Text = "Phường, xã";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(37, 711);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(97, 13);
            this.label37.TabIndex = 44;
            this.label37.Text = "Số nhà, tên đường";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(37, 603);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(97, 13);
            this.label30.TabIndex = 44;
            this.label30.Text = "Số nhà, tên đường";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(37, 495);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(97, 13);
            this.label23.TabIndex = 44;
            this.label23.Text = "Số nhà, tên đường";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(37, 387);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "Số nhà, tên đường";
            // 
            // txtQuanHuyenDonViUyThac
            // 
            this.txtQuanHuyenDonViUyThac.Location = new System.Drawing.Point(146, 734);
            this.txtQuanHuyenDonViUyThac.Name = "txtQuanHuyenDonViUyThac";
            this.txtQuanHuyenDonViUyThac.Size = new System.Drawing.Size(198, 21);
            this.txtQuanHuyenDonViUyThac.TabIndex = 42;
            // 
            // txtQuanHuyenCongTyCungCap
            // 
            this.txtQuanHuyenCongTyCungCap.Location = new System.Drawing.Point(146, 626);
            this.txtQuanHuyenCongTyCungCap.Name = "txtQuanHuyenCongTyCungCap";
            this.txtQuanHuyenCongTyCungCap.Size = new System.Drawing.Size(198, 21);
            this.txtQuanHuyenCongTyCungCap.TabIndex = 35;
            // 
            // txtQuanHuyenCongTySanXuat
            // 
            this.txtQuanHuyenCongTySanXuat.Location = new System.Drawing.Point(146, 518);
            this.txtQuanHuyenCongTySanXuat.Name = "txtQuanHuyenCongTySanXuat";
            this.txtQuanHuyenCongTySanXuat.Size = new System.Drawing.Size(198, 21);
            this.txtQuanHuyenCongTySanXuat.TabIndex = 28;
            // 
            // txtQuanHuyenXuatKhau
            // 
            this.txtQuanHuyenXuatKhau.Location = new System.Drawing.Point(146, 410);
            this.txtQuanHuyenXuatKhau.Name = "txtQuanHuyenXuatKhau";
            this.txtQuanHuyenXuatKhau.Size = new System.Drawing.Size(198, 21);
            this.txtQuanHuyenXuatKhau.TabIndex = 21;
            // 
            // txtGhiChuChiTiet
            // 
            this.txtGhiChuChiTiet.Location = new System.Drawing.Point(146, 869);
            this.txtGhiChuChiTiet.Name = "txtGhiChuChiTiet";
            this.txtGhiChuChiTiet.Size = new System.Drawing.Size(526, 21);
            this.txtGhiChuChiTiet.TabIndex = 51;
            // 
            // txtSoNhaTenDuongDonViUyThac
            // 
            this.txtSoNhaTenDuongDonViUyThac.Location = new System.Drawing.Point(146, 707);
            this.txtSoNhaTenDuongDonViUyThac.Name = "txtSoNhaTenDuongDonViUyThac";
            this.txtSoNhaTenDuongDonViUyThac.Size = new System.Drawing.Size(198, 21);
            this.txtSoNhaTenDuongDonViUyThac.TabIndex = 40;
            // 
            // txtSoNhaTenDuongCongTyCungCap
            // 
            this.txtSoNhaTenDuongCongTyCungCap.Location = new System.Drawing.Point(146, 599);
            this.txtSoNhaTenDuongCongTyCungCap.Name = "txtSoNhaTenDuongCongTyCungCap";
            this.txtSoNhaTenDuongCongTyCungCap.Size = new System.Drawing.Size(198, 21);
            this.txtSoNhaTenDuongCongTyCungCap.TabIndex = 33;
            // 
            // txtSoNhaTenDuongCongTySanXuat
            // 
            this.txtSoNhaTenDuongCongTySanXuat.Location = new System.Drawing.Point(146, 491);
            this.txtSoNhaTenDuongCongTySanXuat.Name = "txtSoNhaTenDuongCongTySanXuat";
            this.txtSoNhaTenDuongCongTySanXuat.Size = new System.Drawing.Size(198, 21);
            this.txtSoNhaTenDuongCongTySanXuat.TabIndex = 26;
            // 
            // txtSoNhaTenDuongXuatKhau
            // 
            this.txtSoNhaTenDuongXuatKhau.Location = new System.Drawing.Point(146, 383);
            this.txtSoNhaTenDuongXuatKhau.Name = "txtSoNhaTenDuongXuatKhau";
            this.txtSoNhaTenDuongXuatKhau.Size = new System.Drawing.Size(198, 21);
            this.txtSoNhaTenDuongXuatKhau.TabIndex = 19;
            // 
            // txtTinhThanhPhoDonViUyThac
            // 
            this.txtTinhThanhPhoDonViUyThac.Location = new System.Drawing.Point(442, 734);
            this.txtTinhThanhPhoDonViUyThac.Name = "txtTinhThanhPhoDonViUyThac";
            this.txtTinhThanhPhoDonViUyThac.Size = new System.Drawing.Size(230, 21);
            this.txtTinhThanhPhoDonViUyThac.TabIndex = 43;
            // 
            // txtTinhThanhPhoCongTyCungCap
            // 
            this.txtTinhThanhPhoCongTyCungCap.Location = new System.Drawing.Point(442, 626);
            this.txtTinhThanhPhoCongTyCungCap.Name = "txtTinhThanhPhoCongTyCungCap";
            this.txtTinhThanhPhoCongTyCungCap.Size = new System.Drawing.Size(230, 21);
            this.txtTinhThanhPhoCongTyCungCap.TabIndex = 36;
            // 
            // txtTinhThanhPhoCongTySanXuat
            // 
            this.txtTinhThanhPhoCongTySanXuat.Location = new System.Drawing.Point(442, 518);
            this.txtTinhThanhPhoCongTySanXuat.Name = "txtTinhThanhPhoCongTySanXuat";
            this.txtTinhThanhPhoCongTySanXuat.Size = new System.Drawing.Size(230, 21);
            this.txtTinhThanhPhoCongTySanXuat.TabIndex = 29;
            // 
            // txtTinhThanhPhoXuatKhau
            // 
            this.txtTinhThanhPhoXuatKhau.Location = new System.Drawing.Point(442, 410);
            this.txtTinhThanhPhoXuatKhau.Name = "txtTinhThanhPhoXuatKhau";
            this.txtTinhThanhPhoXuatKhau.Size = new System.Drawing.Size(230, 21);
            this.txtTinhThanhPhoXuatKhau.TabIndex = 22;
            // 
            // txtPhuongXaDonViUyThac
            // 
            this.txtPhuongXaDonViUyThac.Location = new System.Drawing.Point(442, 707);
            this.txtPhuongXaDonViUyThac.Name = "txtPhuongXaDonViUyThac";
            this.txtPhuongXaDonViUyThac.Size = new System.Drawing.Size(230, 21);
            this.txtPhuongXaDonViUyThac.TabIndex = 41;
            // 
            // txtPhuongXaCongTyCungCap
            // 
            this.txtPhuongXaCongTyCungCap.Location = new System.Drawing.Point(442, 599);
            this.txtPhuongXaCongTyCungCap.Name = "txtPhuongXaCongTyCungCap";
            this.txtPhuongXaCongTyCungCap.Size = new System.Drawing.Size(230, 21);
            this.txtPhuongXaCongTyCungCap.TabIndex = 34;
            // 
            // txtPhuongXaCongTySanXuat
            // 
            this.txtPhuongXaCongTySanXuat.Location = new System.Drawing.Point(442, 491);
            this.txtPhuongXaCongTySanXuat.Name = "txtPhuongXaCongTySanXuat";
            this.txtPhuongXaCongTySanXuat.Size = new System.Drawing.Size(230, 21);
            this.txtPhuongXaCongTySanXuat.TabIndex = 27;
            // 
            // txtPhuongXaXuatKhau
            // 
            this.txtPhuongXaXuatKhau.Location = new System.Drawing.Point(442, 383);
            this.txtPhuongXaXuatKhau.Name = "txtPhuongXaXuatKhau";
            this.txtPhuongXaXuatKhau.Size = new System.Drawing.Size(230, 21);
            this.txtPhuongXaXuatKhau.TabIndex = 20;
            // 
            // ucMaQuocGiaDonViUyThac
            // 
            this.ucMaQuocGiaDonViUyThac.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaQuocGiaDonViUyThac.Appearance.Options.UseBackColor = true;
            this.ucMaQuocGiaDonViUyThac.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaQuocGiaDonViUyThac.Code = "";
            this.ucMaQuocGiaDonViUyThac.ColorControl = System.Drawing.Color.Empty;
            this.ucMaQuocGiaDonViUyThac.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaQuocGiaDonViUyThac.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaQuocGiaDonViUyThac.IsOnlyWarning = false;
            this.ucMaQuocGiaDonViUyThac.IsValidate = true;
            this.ucMaQuocGiaDonViUyThac.Location = new System.Drawing.Point(442, 677);
            this.ucMaQuocGiaDonViUyThac.Name = "ucMaQuocGiaDonViUyThac";
            this.ucMaQuocGiaDonViUyThac.Name_VN = "";
            this.ucMaQuocGiaDonViUyThac.SetOnlyWarning = false;
            this.ucMaQuocGiaDonViUyThac.SetValidate = false;
            this.ucMaQuocGiaDonViUyThac.ShowColumnCode = true;
            this.ucMaQuocGiaDonViUyThac.ShowColumnName = false;
            this.ucMaQuocGiaDonViUyThac.Size = new System.Drawing.Size(121, 26);
            this.ucMaQuocGiaDonViUyThac.TabIndex = 39;
            this.ucMaQuocGiaDonViUyThac.TagName = "";
            this.ucMaQuocGiaDonViUyThac.Where = null;
            this.ucMaQuocGiaDonViUyThac.WhereCondition = "";
            this.ucMaQuocGiaDonViUyThac.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucMaQuocGiaCongTyCungCap
            // 
            this.ucMaQuocGiaCongTyCungCap.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaQuocGiaCongTyCungCap.Appearance.Options.UseBackColor = true;
            this.ucMaQuocGiaCongTyCungCap.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaQuocGiaCongTyCungCap.Code = "";
            this.ucMaQuocGiaCongTyCungCap.ColorControl = System.Drawing.Color.Empty;
            this.ucMaQuocGiaCongTyCungCap.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaQuocGiaCongTyCungCap.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaQuocGiaCongTyCungCap.IsOnlyWarning = false;
            this.ucMaQuocGiaCongTyCungCap.IsValidate = true;
            this.ucMaQuocGiaCongTyCungCap.Location = new System.Drawing.Point(442, 569);
            this.ucMaQuocGiaCongTyCungCap.Name = "ucMaQuocGiaCongTyCungCap";
            this.ucMaQuocGiaCongTyCungCap.Name_VN = "";
            this.ucMaQuocGiaCongTyCungCap.SetOnlyWarning = false;
            this.ucMaQuocGiaCongTyCungCap.SetValidate = false;
            this.ucMaQuocGiaCongTyCungCap.ShowColumnCode = true;
            this.ucMaQuocGiaCongTyCungCap.ShowColumnName = false;
            this.ucMaQuocGiaCongTyCungCap.Size = new System.Drawing.Size(121, 26);
            this.ucMaQuocGiaCongTyCungCap.TabIndex = 32;
            this.ucMaQuocGiaCongTyCungCap.TagName = "";
            this.ucMaQuocGiaCongTyCungCap.Where = null;
            this.ucMaQuocGiaCongTyCungCap.WhereCondition = "";
            this.ucMaQuocGiaCongTyCungCap.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucMaQuocGiaCongTySanXuat
            // 
            this.ucMaQuocGiaCongTySanXuat.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaQuocGiaCongTySanXuat.Appearance.Options.UseBackColor = true;
            this.ucMaQuocGiaCongTySanXuat.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaQuocGiaCongTySanXuat.Code = "";
            this.ucMaQuocGiaCongTySanXuat.ColorControl = System.Drawing.Color.Empty;
            this.ucMaQuocGiaCongTySanXuat.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaQuocGiaCongTySanXuat.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaQuocGiaCongTySanXuat.IsOnlyWarning = false;
            this.ucMaQuocGiaCongTySanXuat.IsValidate = true;
            this.ucMaQuocGiaCongTySanXuat.Location = new System.Drawing.Point(442, 461);
            this.ucMaQuocGiaCongTySanXuat.Name = "ucMaQuocGiaCongTySanXuat";
            this.ucMaQuocGiaCongTySanXuat.Name_VN = "";
            this.ucMaQuocGiaCongTySanXuat.SetOnlyWarning = false;
            this.ucMaQuocGiaCongTySanXuat.SetValidate = false;
            this.ucMaQuocGiaCongTySanXuat.ShowColumnCode = true;
            this.ucMaQuocGiaCongTySanXuat.ShowColumnName = false;
            this.ucMaQuocGiaCongTySanXuat.Size = new System.Drawing.Size(121, 26);
            this.ucMaQuocGiaCongTySanXuat.TabIndex = 25;
            this.ucMaQuocGiaCongTySanXuat.TagName = "";
            this.ucMaQuocGiaCongTySanXuat.Where = null;
            this.ucMaQuocGiaCongTySanXuat.WhereCondition = "";
            this.ucMaQuocGiaCongTySanXuat.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucMaQuocGiaXuatKhau
            // 
            this.ucMaQuocGiaXuatKhau.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaQuocGiaXuatKhau.Appearance.Options.UseBackColor = true;
            this.ucMaQuocGiaXuatKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaQuocGiaXuatKhau.Code = "";
            this.ucMaQuocGiaXuatKhau.ColorControl = System.Drawing.Color.Empty;
            this.ucMaQuocGiaXuatKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaQuocGiaXuatKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaQuocGiaXuatKhau.IsOnlyWarning = false;
            this.ucMaQuocGiaXuatKhau.IsValidate = true;
            this.ucMaQuocGiaXuatKhau.Location = new System.Drawing.Point(442, 353);
            this.ucMaQuocGiaXuatKhau.Name = "ucMaQuocGiaXuatKhau";
            this.ucMaQuocGiaXuatKhau.Name_VN = "";
            this.ucMaQuocGiaXuatKhau.SetOnlyWarning = false;
            this.ucMaQuocGiaXuatKhau.SetValidate = false;
            this.ucMaQuocGiaXuatKhau.ShowColumnCode = true;
            this.ucMaQuocGiaXuatKhau.ShowColumnName = false;
            this.ucMaQuocGiaXuatKhau.Size = new System.Drawing.Size(121, 26);
            this.ucMaQuocGiaXuatKhau.TabIndex = 18;
            this.ucMaQuocGiaXuatKhau.TagName = "";
            this.ucMaQuocGiaXuatKhau.Where = null;
            this.ucMaQuocGiaXuatKhau.WhereCondition = "";
            this.ucMaQuocGiaXuatKhau.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtTenDonViUyThac
            // 
            this.txtTenDonViUyThac.Location = new System.Drawing.Point(146, 653);
            this.txtTenDonViUyThac.Name = "txtTenDonViUyThac";
            this.txtTenDonViUyThac.Size = new System.Drawing.Size(526, 21);
            this.txtTenDonViUyThac.TabIndex = 37;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(10, 658);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(84, 13);
            this.label36.TabIndex = 36;
            this.label36.Text = "Công ty ủy thác";
            // 
            // txtTenCongTyCungCap
            // 
            this.txtTenCongTyCungCap.Location = new System.Drawing.Point(146, 545);
            this.txtTenCongTyCungCap.Name = "txtTenCongTyCungCap";
            this.txtTenCongTyCungCap.Size = new System.Drawing.Size(526, 21);
            this.txtTenCongTyCungCap.TabIndex = 30;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(10, 550);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(91, 13);
            this.label29.TabIndex = 36;
            this.label29.Text = "Công ty cung cấp";
            // 
            // txtTenCongTySanXuat
            // 
            this.txtTenCongTySanXuat.Location = new System.Drawing.Point(146, 437);
            this.txtTenCongTySanXuat.Name = "txtTenCongTySanXuat";
            this.txtTenCongTySanXuat.Size = new System.Drawing.Size(526, 21);
            this.txtTenCongTySanXuat.TabIndex = 23;
            // 
            // txtMaBuuChinhDonViUyThac
            // 
            this.txtMaBuuChinhDonViUyThac.Location = new System.Drawing.Point(146, 680);
            this.txtMaBuuChinhDonViUyThac.Name = "txtMaBuuChinhDonViUyThac";
            this.txtMaBuuChinhDonViUyThac.Size = new System.Drawing.Size(198, 21);
            this.txtMaBuuChinhDonViUyThac.TabIndex = 38;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 442);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(90, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Công ty sản xuất";
            // 
            // txtMaBuuChinhCongTyCungCap
            // 
            this.txtMaBuuChinhCongTyCungCap.Location = new System.Drawing.Point(146, 572);
            this.txtMaBuuChinhCongTyCungCap.Name = "txtMaBuuChinhCongTyCungCap";
            this.txtMaBuuChinhCongTyCungCap.Size = new System.Drawing.Size(198, 21);
            this.txtMaBuuChinhCongTyCungCap.TabIndex = 31;
            // 
            // txtTenThuongNhanXuatKhau
            // 
            this.txtTenThuongNhanXuatKhau.Location = new System.Drawing.Point(146, 329);
            this.txtTenThuongNhanXuatKhau.Name = "txtTenThuongNhanXuatKhau";
            this.txtTenThuongNhanXuatKhau.Size = new System.Drawing.Size(526, 21);
            this.txtTenThuongNhanXuatKhau.TabIndex = 16;
            this.txtTenThuongNhanXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(37, 685);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(71, 13);
            this.label35.TabIndex = 38;
            this.label35.Text = "Mã bưu chính";
            // 
            // txtMaBuuChinhCongTySanXuat
            // 
            this.txtMaBuuChinhCongTySanXuat.Location = new System.Drawing.Point(146, 464);
            this.txtMaBuuChinhCongTySanXuat.Name = "txtMaBuuChinhCongTySanXuat";
            this.txtMaBuuChinhCongTySanXuat.Size = new System.Drawing.Size(198, 21);
            this.txtMaBuuChinhCongTySanXuat.TabIndex = 24;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(37, 577);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 13);
            this.label28.TabIndex = 38;
            this.label28.Text = "Mã bưu chính";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(10, 334);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 13);
            this.label20.TabIndex = 36;
            this.label20.Text = "Công ty xuất khẩu";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(361, 685);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(64, 13);
            this.label34.TabIndex = 42;
            this.label34.Text = "Mã quốc gia";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(37, 469);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 38;
            this.label18.Text = "Mã bưu chính";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(361, 577);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 13);
            this.label27.TabIndex = 42;
            this.label27.Text = "Mã quốc gia";
            // 
            // txtMaBuuChinhXuatKhau
            // 
            this.txtMaBuuChinhXuatKhau.Location = new System.Drawing.Point(146, 356);
            this.txtMaBuuChinhXuatKhau.Name = "txtMaBuuChinhXuatKhau";
            this.txtMaBuuChinhXuatKhau.Size = new System.Drawing.Size(198, 21);
            this.txtMaBuuChinhXuatKhau.TabIndex = 17;
            this.txtMaBuuChinhXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(361, 469);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 42;
            this.label17.Text = "Mã quốc gia";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(37, 361);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(71, 13);
            this.label55.TabIndex = 38;
            this.label55.Text = "Mã bưu chính";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(361, 361);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(64, 13);
            this.label57.TabIndex = 42;
            this.label57.Text = "Mã quốc gia";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Location = new System.Drawing.Point(647, 910);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(16, 13);
            this.label58.TabIndex = 35;
            this.label58.Text = "   ";
            // 
            // dtHanDung
            // 
            // 
            // 
            // 
            this.dtHanDung.DropDownCalendar.Name = "";
            this.dtHanDung.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtHanDung.Location = new System.Drawing.Point(444, 140);
            this.dtHanDung.Name = "dtHanDung";
            this.dtHanDung.Size = new System.Drawing.Size(94, 21);
            this.dtHanDung.TabIndex = 5;
            this.dtHanDung.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtHanDung.VisualStyleManager = this.vsmMain;
            // 
            // ucDonViTinhSoLuong
            // 
            this.ucDonViTinhSoLuong.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDonViTinhSoLuong.Appearance.Options.UseBackColor = true;
            this.ucDonViTinhSoLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ucDonViTinhSoLuong.Code = "";
            this.ucDonViTinhSoLuong.ColorControl = System.Drawing.Color.Empty;
            this.ucDonViTinhSoLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhSoLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhSoLuong.IsOnlyWarning = false;
            this.ucDonViTinhSoLuong.IsValidate = true;
            this.ucDonViTinhSoLuong.Location = new System.Drawing.Point(273, 164);
            this.ucDonViTinhSoLuong.Name = "ucDonViTinhSoLuong";
            this.ucDonViTinhSoLuong.Name_VN = "";
            this.ucDonViTinhSoLuong.SetOnlyWarning = false;
            this.ucDonViTinhSoLuong.SetValidate = false;
            this.ucDonViTinhSoLuong.ShowColumnCode = true;
            this.ucDonViTinhSoLuong.ShowColumnName = false;
            this.ucDonViTinhSoLuong.Size = new System.Drawing.Size(71, 26);
            this.ucDonViTinhSoLuong.TabIndex = 7;
            this.ucDonViTinhSoLuong.TagName = "";
            this.ucDonViTinhSoLuong.Where = null;
            this.ucDonViTinhSoLuong.WhereCondition = "";
            this.ucDonViTinhSoLuong.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucDonViTinhKhoiLuong
            // 
            this.ucDonViTinhKhoiLuong.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDonViTinhKhoiLuong.Appearance.Options.UseBackColor = true;
            this.ucDonViTinhKhoiLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucDonViTinhKhoiLuong.Code = "";
            this.ucDonViTinhKhoiLuong.ColorControl = System.Drawing.Color.Empty;
            this.ucDonViTinhKhoiLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhKhoiLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhKhoiLuong.IsOnlyWarning = false;
            this.ucDonViTinhKhoiLuong.IsValidate = true;
            this.ucDonViTinhKhoiLuong.Location = new System.Drawing.Point(273, 245);
            this.ucDonViTinhKhoiLuong.Name = "ucDonViTinhKhoiLuong";
            this.ucDonViTinhKhoiLuong.Name_VN = "";
            this.ucDonViTinhKhoiLuong.SetOnlyWarning = false;
            this.ucDonViTinhKhoiLuong.SetValidate = false;
            this.ucDonViTinhKhoiLuong.ShowColumnCode = true;
            this.ucDonViTinhKhoiLuong.ShowColumnName = false;
            this.ucDonViTinhKhoiLuong.Size = new System.Drawing.Size(71, 26);
            this.ucDonViTinhKhoiLuong.TabIndex = 11;
            this.ucDonViTinhKhoiLuong.TagName = "";
            this.ucDonViTinhKhoiLuong.Where = null;
            this.ucDonViTinhKhoiLuong.WhereCondition = "";
            this.ucDonViTinhKhoiLuong.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtSoDangKy
            // 
            this.txtSoDangKy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDangKy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDangKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDangKy.Location = new System.Drawing.Point(146, 140);
            this.txtSoDangKy.MaxLength = 12;
            this.txtSoDangKy.Name = "txtSoDangKy";
            this.txtSoDangKy.Size = new System.Drawing.Size(228, 21);
            this.txtSoDangKy.TabIndex = 4;
            this.txtSoDangKy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDangKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoDangKy.VisualStyleManager = this.vsmMain;
            this.txtSoDangKy.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(384, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Hạn dùng";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 145);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Số đăng ký";
            // 
            // txtTieuChuanChatLuong
            // 
            this.txtTieuChuanChatLuong.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTieuChuanChatLuong.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTieuChuanChatLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTieuChuanChatLuong.Location = new System.Drawing.Point(146, 113);
            this.txtTieuChuanChatLuong.MaxLength = 12;
            this.txtTieuChuanChatLuong.Name = "txtTieuChuanChatLuong";
            this.txtTieuChuanChatLuong.Size = new System.Drawing.Size(228, 21);
            this.txtTieuChuanChatLuong.TabIndex = 3;
            this.txtTieuChuanChatLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTieuChuanChatLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTieuChuanChatLuong.VisualStyleManager = this.vsmMain;
            this.txtTieuChuanChatLuong.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Tiêu chuẩn";
            // 
            // txtHoatChat
            // 
            this.txtHoatChat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtHoatChat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtHoatChat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoatChat.Location = new System.Drawing.Point(444, 86);
            this.txtHoatChat.MaxLength = 12;
            this.txtHoatChat.Name = "txtHoatChat";
            this.txtHoatChat.Size = new System.Drawing.Size(228, 21);
            this.txtHoatChat.TabIndex = 2;
            this.txtHoatChat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtHoatChat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtHoatChat.VisualStyleManager = this.vsmMain;
            this.txtHoatChat.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(384, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Hoạt chất";
            // 
            // txtCongDung
            // 
            this.txtCongDung.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCongDung.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCongDung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCongDung.Location = new System.Drawing.Point(146, 221);
            this.txtCongDung.MaxLength = 12;
            this.txtCongDung.Name = "txtCongDung";
            this.txtCongDung.Size = new System.Drawing.Size(526, 21);
            this.txtCongDung.TabIndex = 9;
            this.txtCongDung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCongDung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtCongDung.VisualStyleManager = this.vsmMain;
            this.txtCongDung.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 226);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Công dụng";
            // 
            // txtTenHoatChatGayNghien
            // 
            this.txtTenHoatChatGayNghien.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenHoatChatGayNghien.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenHoatChatGayNghien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHoatChatGayNghien.Location = new System.Drawing.Point(146, 194);
            this.txtTenHoatChatGayNghien.MaxLength = 12;
            this.txtTenHoatChatGayNghien.Name = "txtTenHoatChatGayNghien";
            this.txtTenHoatChatGayNghien.Size = new System.Drawing.Size(526, 21);
            this.txtTenHoatChatGayNghien.TabIndex = 8;
            this.txtTenHoatChatGayNghien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHoatChatGayNghien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHoatChatGayNghien.VisualStyleManager = this.vsmMain;
            this.txtTenHoatChatGayNghien.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tên hoạt chất gây nghiện";
            // 
            // txtMaSoHangHoa
            // 
            this.txtMaSoHangHoa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSoHangHoa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSoHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSoHangHoa.Location = new System.Drawing.Point(146, 86);
            this.txtMaSoHangHoa.MaxLength = 12;
            this.txtMaSoHangHoa.Name = "txtMaSoHangHoa";
            this.txtMaSoHangHoa.Size = new System.Drawing.Size(228, 21);
            this.txtMaSoHangHoa.TabIndex = 1;
            this.txtMaSoHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSoHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSoHangHoa.VisualStyleManager = this.vsmMain;
            this.txtMaSoHangHoa.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Mã số hàng hóa";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 0;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.Location = new System.Drawing.Point(146, 167);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(121, 21);
            this.txtSoLuong.TabIndex = 6;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuong.VisualStyleManager = this.vsmMain;
            // 
            // txtGiaBanLeVND
            // 
            this.txtGiaBanLeVND.DecimalDigits = 3;
            this.txtGiaBanLeVND.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtGiaBanLeVND.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaBanLeVND.Location = new System.Drawing.Point(474, 299);
            this.txtGiaBanLeVND.MaxLength = 19;
            this.txtGiaBanLeVND.Name = "txtGiaBanLeVND";
            this.txtGiaBanLeVND.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtGiaBanLeVND.Size = new System.Drawing.Size(198, 21);
            this.txtGiaBanLeVND.TabIndex = 15;
            this.txtGiaBanLeVND.Text = "0.000";
            this.txtGiaBanLeVND.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGiaBanLeVND.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtGiaBanLeVND.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGiaBanBuonVND
            // 
            this.txtGiaBanBuonVND.DecimalDigits = 3;
            this.txtGiaBanBuonVND.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtGiaBanBuonVND.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaBanBuonVND.Location = new System.Drawing.Point(146, 302);
            this.txtGiaBanBuonVND.MaxLength = 19;
            this.txtGiaBanBuonVND.Name = "txtGiaBanBuonVND";
            this.txtGiaBanBuonVND.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtGiaBanBuonVND.Size = new System.Drawing.Size(198, 21);
            this.txtGiaBanBuonVND.TabIndex = 14;
            this.txtGiaBanBuonVND.Text = "0.000";
            this.txtGiaBanBuonVND.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGiaBanBuonVND.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtGiaBanBuonVND.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(361, 304);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Giá bán lẻ (VNĐ)";
            // 
            // txtGiaNhapKhauVND
            // 
            this.txtGiaNhapKhauVND.DecimalDigits = 3;
            this.txtGiaNhapKhauVND.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtGiaNhapKhauVND.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaNhapKhauVND.Location = new System.Drawing.Point(474, 272);
            this.txtGiaNhapKhauVND.MaxLength = 19;
            this.txtGiaNhapKhauVND.Name = "txtGiaNhapKhauVND";
            this.txtGiaNhapKhauVND.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtGiaNhapKhauVND.Size = new System.Drawing.Size(198, 21);
            this.txtGiaNhapKhauVND.TabIndex = 13;
            this.txtGiaNhapKhauVND.Text = "0.000";
            this.txtGiaNhapKhauVND.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGiaNhapKhauVND.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtGiaNhapKhauVND.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(12, 304);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(102, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Giá bán buôn (VNĐ)";
            // 
            // txtTongSoKhoiLuongHoatChatGayNghien
            // 
            this.txtTongSoKhoiLuongHoatChatGayNghien.DecimalDigits = 3;
            this.txtTongSoKhoiLuongHoatChatGayNghien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSoKhoiLuongHoatChatGayNghien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoKhoiLuongHoatChatGayNghien.Location = new System.Drawing.Point(146, 275);
            this.txtTongSoKhoiLuongHoatChatGayNghien.MaxLength = 10;
            this.txtTongSoKhoiLuongHoatChatGayNghien.Name = "txtTongSoKhoiLuongHoatChatGayNghien";
            this.txtTongSoKhoiLuongHoatChatGayNghien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoKhoiLuongHoatChatGayNghien.Size = new System.Drawing.Size(198, 21);
            this.txtTongSoKhoiLuongHoatChatGayNghien.TabIndex = 12;
            this.txtTongSoKhoiLuongHoatChatGayNghien.Text = "0.000";
            this.txtTongSoKhoiLuongHoatChatGayNghien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoKhoiLuongHoatChatGayNghien.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTongSoKhoiLuongHoatChatGayNghien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongSoKhoiLuongHoatChatGayNghien.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(361, 280);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Giá nhập khẩu (VNĐ)";
            // 
            // TongSoKhoiLuongHoatChatGayNghien
            // 
            this.TongSoKhoiLuongHoatChatGayNghien.DecimalDigits = 3;
            this.TongSoKhoiLuongHoatChatGayNghien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.TongSoKhoiLuongHoatChatGayNghien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TongSoKhoiLuongHoatChatGayNghien.Location = new System.Drawing.Point(146, 248);
            this.TongSoKhoiLuongHoatChatGayNghien.MaxLength = 15;
            this.TongSoKhoiLuongHoatChatGayNghien.Name = "TongSoKhoiLuongHoatChatGayNghien";
            this.TongSoKhoiLuongHoatChatGayNghien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TongSoKhoiLuongHoatChatGayNghien.Size = new System.Drawing.Size(121, 21);
            this.TongSoKhoiLuongHoatChatGayNghien.TabIndex = 10;
            this.TongSoKhoiLuongHoatChatGayNghien.Text = "0.000";
            this.TongSoKhoiLuongHoatChatGayNghien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.TongSoKhoiLuongHoatChatGayNghien.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.TongSoKhoiLuongHoatChatGayNghien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.TongSoKhoiLuongHoatChatGayNghien.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 280);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Tổng số khối lượng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 172);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Số lượng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 253);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Tổng số khối lượng";
            // 
            // txtTenThuocQuyCachDongGoi
            // 
            this.txtTenThuocQuyCachDongGoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenThuocQuyCachDongGoi.Location = new System.Drawing.Point(12, 35);
            this.txtTenThuocQuyCachDongGoi.MaxLength = 255;
            this.txtTenThuocQuyCachDongGoi.Multiline = true;
            this.txtTenThuocQuyCachDongGoi.Name = "txtTenThuocQuyCachDongGoi";
            this.txtTenThuocQuyCachDongGoi.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTenThuocQuyCachDongGoi.Size = new System.Drawing.Size(660, 45);
            this.txtTenThuocQuyCachDongGoi.TabIndex = 0;
            this.txtTenThuocQuyCachDongGoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenThuocQuyCachDongGoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenThuocQuyCachDongGoi.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(278, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên thuốc, hàm lượng, dạng bào chế, quy cách đóng gói";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(606, 384);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 54;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGhi.Location = new System.Drawing.Point(439, 384);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 52;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(523, 384);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 53;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // grdHang
            // 
            this.grdHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHang.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.ColumnAutoResize = true;
            grdHang_DesignTimeLayout.LayoutString = resources.GetString("grdHang_DesignTimeLayout.LayoutString");
            this.grdHang.DesignTimeLayout = grdHang_DesignTimeLayout;
            this.grdHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdHang.GroupByBoxVisible = false;
            this.grdHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.Location = new System.Drawing.Point(0, 0);
            this.grdHang.Name = "grdHang";
            this.grdHang.RecordNavigator = true;
            this.grdHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdHang.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdHang.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdHang.Size = new System.Drawing.Size(702, 139);
            this.grdHang.TabIndex = 0;
            this.grdHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdHang.VisualStyleManager = this.vsmMain;
            this.grdHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdHang_RowDoubleClick);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvMaSoHangHoa
            // 
            this.rfvMaSoHangHoa.ControlToValidate = this.txtMaSoHangHoa;
            this.rfvMaSoHangHoa.ErrorMessage = "\"Mã số hàng hóa\" không được để trống.";
            this.rfvMaSoHangHoa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaSoHangHoa.Icon")));
            this.rfvMaSoHangHoa.Tag = "rfvMaSoHangHoa";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.btnGhi);
            this.splitContainer1.Panel1.Controls.Add(this.uiGroupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.btnClose);
            this.splitContainer1.Panel1.Controls.Add(this.btnXoa);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.grdHang);
            this.splitContainer1.Size = new System.Drawing.Size(702, 559);
            this.splitContainer1.SplitterDistance = 416;
            this.splitContainer1.TabIndex = 0;
            // 
            // VNACC_GiayPhep_SMA_HangForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 565);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_GiayPhep_SMA_HangForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hàng hóa";
            this.Load += new System.EventHandler(this.VNACC_GiayPhep_SMA_HangForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaSoHangHoa)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grdHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox TongSoKhoiLuongHoatChatGayNghien;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenThuocQuyCachDongGoi;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSoHangHoa;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaSoHangHoa;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhKhoiLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhSoLuong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtHoatChat;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.CalendarCombo.CalendarCombo dtHanDung;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDangKy;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtTieuChuanChatLuong;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHoatChatGayNghien;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtCongDung;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoKhoiLuongHoatChatGayNghien;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtGiaNhapKhauVND;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtGiaBanLeVND;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtGiaBanBuonVND;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label58;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaQuocGiaXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenThuongNhanXuatKhau;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhXuatKhau;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuanHuyenXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoNhaTenDuongXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinhThanhPhoXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuongXaXuatKhau;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuanHuyenDonViUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuanHuyenCongTyCungCap;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuanHuyenCongTySanXuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoNhaTenDuongDonViUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoNhaTenDuongCongTyCungCap;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoNhaTenDuongCongTySanXuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinhThanhPhoDonViUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinhThanhPhoCongTyCungCap;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinhThanhPhoCongTySanXuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuongXaDonViUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuongXaCongTyCungCap;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuongXaCongTySanXuat;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaQuocGiaDonViUyThac;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaQuocGiaCongTyCungCap;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaQuocGiaCongTySanXuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonViUyThac;
        private System.Windows.Forms.Label label36;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCongTyCungCap;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCongTySanXuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDonViUyThac;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhCongTyCungCap;
        private System.Windows.Forms.Label label35;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhCongTySanXuat;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label17;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhLuongTonKyTruoc;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSo;
        private System.Windows.Forms.Label label43;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongNhapTrongKy;
        private System.Windows.Forms.Label label42;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongTonKhoKyTruoc;
        private System.Windows.Forms.Label label41;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongTonKhoDenNgay;
        private System.Windows.Forms.Label label45;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtHuHao;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoXuatTrongKy;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label44;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuChiTiet;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}