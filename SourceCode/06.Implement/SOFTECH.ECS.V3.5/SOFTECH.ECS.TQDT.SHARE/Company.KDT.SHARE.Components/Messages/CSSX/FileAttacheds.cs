﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
    public class FileAttacheds
    {
        /// <summary>
        /// Đường dẫn file
        /// </summary>
      [XmlElement("AttachedFile")]
        public List<FileAttached> File { get; set; }
    }
}
