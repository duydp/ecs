﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.Interface.SXXK;
using System.IO;
using System.Diagnostics;

using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.Interface.Report.GC;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.SXXK;
using System.Data.SqlClient;
using Company.GC.BLL.GC;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.GC;
namespace Company.Interface
{
    public partial class XuatNhapTonForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public HoSoQuyetToan HS = new HoSoQuyetToan();
        public HoSoQuyetToan_DSHopDong DSHD= new HoSoQuyetToan_DSHopDong();
        private DataTable tb = new DataTable();
        private DataTable tbNPL_SP_DM = new DataTable();
        private DataTable tb_M15 = new DataTable();
        public List<HopDong> list_HopDong = new List<HopDong>();
        public string ListHD = "";
        public string ListID_HopDong = "";
        public bool isDaQuyetToan = false;
        int totalRows = 0;
        int index = 0;
        public string ngayDauKy;
        public string ngayCuoiKy;
        public DateTime NgayDauKy;
        public DateTime NgayCuoiKy;

        public string NPL;
        public string NPLTemp;
        public decimal LuongNhap = 0;
        public decimal LuongXuat = 0;
        public decimal LuongTon = 0;
        public decimal TriGiaNhap = 0;
        public decimal TriGiaXuat = 0;
        public decimal TriGiaTon = 0;
        public DataTable dtTotal = new DataTable();
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP();
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
        public System.IO.FileInfo fin;
        public System.IO.FileStream fs;
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public long size;
        public byte[] data;
        public XuatNhapTonForm()
        {
            InitializeComponent();

        }
        public bool isUpdate = false;
        public bool isXuLy1HD = false;
        public bool IsReadExcel = false;
        public DataTable tb_Mau15 = new DataTable();
        public DataTable dt = new DataTable();
        #region XỬ LÝ QUYẾT TOÁN HỢP ĐỒNG
        public DataSet GetTotalNPLImport(long HopDong_ID, int NamQuyetToan)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet dsNPLToTalImport = new DataSet();
            try
            {
                SqlCommand cmd = (SqlCommand)db.GetStoredProcCommand("p_GC_XuatNhapTon_SP_DM_NPL_TT38");
                db.AddInParameter(cmd, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
                db.AddInParameter(cmd, "@NamQuyetToan", SqlDbType.Int, NamQuyetToan);
                dsNPLToTalImport = db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return dsNPLToTalImport;
        }
        public DataSet GetTotalNPLExport(long HopDong_ID, int NamQuyetToan)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet dsNPLToTalImport = new DataSet();
            try
            {
                SqlCommand cmd = (SqlCommand)db.GetStoredProcCommand("p_GC_XuatNhapTon_SP_DM_NPL_TT38");
                db.AddInParameter(cmd, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
                db.AddInParameter(cmd, "@NamQuyetToan", SqlDbType.Int, NamQuyetToan);
                dsNPLToTalImport = db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return dsNPLToTalImport;
        }
        #endregion
        public DataSet GetDM_SP_NPL()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();
            //ngayDauKy = dtpNgayDauKy.Value.ToString("yyyy-MM-dd");
            //ngayCuoiKy = dtpNgayCuoiKy.Value.ToString("yyyy-MM-dd");
            //ngayDauKy = ngayDauKy + " 00:00:00.00";
            //ngayCuoiKy = ngayCuoiKy + " 23:59:59.999";
            //Lấy danh sách SP chưa có định mức
            #region Comment SQL
            //try
            //{
//                string sql = "         SELECT DISTINCT                                                                                                                           " +
//"                 ToKhai.IDHopDong as HopDong_ID ,                                                                                                                " +
//"                 ToKhai.SoHopDong ,                                                                                                                " +
//"                 CONVERT(VARCHAR(50), ( CASE WHEN ToKhai.MaLoaiHinh LIKE '%V%'                                                                     " +
//"                                             THEN ( SELECT TOP 1                                                                                   " +
//"                                                             SoTKVNACCS                                                                            " +
//"                                                    FROM     t_VNACCS_CapSoToKhai                                                                  " +
//"                                                    WHERE    SoTK = ToKhai.SoToKhai                                                                " +
//"                                                  )                                                                                                " +
//"                                             ELSE ToKhai.SoToKhai                                                                                  " +
//"                                        END )) AS  SoToKhai ,                                                                                       " +
//"                 ToKhai.MaLoaiHinh ,                                                                                                               " +
//"                 ToKhai.NgayDangKy ,                                                                                                               " +
//"                 v_KDT_SP_Xuat_HD.MaPhu AS MaHang ,                                                                                                " +
//"                 v_KDT_SP_Xuat_HD.SoThuTuHang,                                                                                                " +
//"                 ToKhai.TenHang ,                                                                                                                  " +
//                    //"                 --t_HaiQuan_DonViTinh.Ten AS DVT ,                                                                                                "+
//"                 ToKhai.SoLuong ,                                                                                                                  " +
//"                 DM.MaNguyenPhuLieu ,                                                                                                              " +
//"                 CONVERT(DECIMAL(16, 6), DM.DinhMucSuDung) AS DinhMucSuDung ,                                                                      " +
//"                 CONVERT(DECIMAL(16, 6), DM.TyLeHaoHut) AS TyLeHaoHut ,                                                                            " +
//"                 CONVERT(DECIMAL(24, 6), ToKhai.SoLuong * DM.DinhMucSuDung) AS LuongNPLChuaHH ,                                                    " +
//"                 ToKhai.SoLuong *( DM.DinhMucSuDung/100 * DM.TyLeHaoHut + DM.DinhMucSuDung) AS LuongNPLCoHH                                                                        " +
//"         FROM    v_KDT_SP_Xuat_HD                                                                                                                  " +
//"                 INNER JOIN ( SELECT t_KDT_ToKhaiMauDich.SoToKhai ,                                                                                " +
//"                                     t_KDT_ToKhaiMauDich.MaLoaiHinh ,                                                                              " +
//"                                     t_KDT_ToKhaiMauDich.NgayDangKy ,                                                                              " +
//"                                     t_KDT_ToKhaiMauDich.IDHopDong ,                                                                               " +
//"                                     t_KDT_ToKhaiMauDich.SoHopDong ,                                                                               " +
//"                                     t_KDT_ToKhaiMauDich.MaHaiQuan ,                                                                               " +
//"                                     t_KDT_HangMauDich.MaPhu AS MaHang ,                                                                           " +
//"                                     t_KDT_HangMauDich.TenHang ,                                                                                   " +
//"                                     t_KDT_HangMauDich.SoLuong ,                                                                                   " +
//"                                     t_KDT_HangMauDich.SoThuTuHang ,                                                                                   " +
//"                                     t_KDT_ToKhaiMauDich.MaDoanhNghiep                                                                             " +
//"                              FROM   t_KDT_ToKhaiMauDich                                                                                           " +
//"                                     INNER JOIN t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID                            " +
//"                              WHERE  ( t_KDT_ToKhaiMauDich.TrangThaiXuLy IN (0, 1 ))                                                                     " +
//"                                     AND ( t_KDT_ToKhaiMauDich.IDHopDong in (" + ListID_HopDong + ") )                                                                   " +
//"                                     AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' ) AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh NOT LIKE 'XVB13' )                                                              " +
//"                                     AND ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' )                                                                 " +
////"                                     AND t_KDT_ToKhaiMauDich.NgayDangKy BETWEEN '"+ngayDauKy+"' AND '"+ngayCuoiKy+"'" +
//"                              UNION ALL                                                                                                                " +
//"                              SELECT t_KDT_GC_ToKhaiChuyenTiep.SoToKhai ,                                                                          " +
//"                                     t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,                                                                        " +
//"                                     t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,                                                                        " +
//"                                     t_KDT_GC_ToKhaiChuyenTiep.IDHopDong ,                                                                         " +
//"                                     t_KDT_GC_ToKhaiChuyenTiep.SoHopDongDV AS SoHopDong ,                                                          " +
//"                                     t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan ,                                                                 " +
//"                                     t_KDT_GC_HangChuyenTiep.MaHang ,                                                                              " +
//"                                     t_KDT_GC_HangChuyenTiep.TenHang ,                                                                             " +
//"                                     t_KDT_GC_HangChuyenTiep.SoLuong ,                                                                             " +
//"                                     t_KDT_GC_HangChuyenTiep.SoThuTuHang ,                                                                             " +
//"                                     t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep                                                                       " +
//"                              FROM   t_KDT_GC_ToKhaiChuyenTiep                                                                                     " +
//"                                     INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID        " +
//"                              WHERE  ( t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy IN (0, 1 ))                                                               " +
//"                                     AND ( t_KDT_GC_ToKhaiChuyenTiep.IDHopDong in (" + ListID_HopDong + ") )                                                             " +
//"                                     AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX'                                                          " +
//"                                           OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19'                                                       " +
//"                                           OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XVE54'                                                       " +
//"                                         )                                                                                                         " +
////"                                       AND t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy BETWEEN '"+ngayDauKy+"' AND '"+ngayCuoiKy+"'" +
//"                            ) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong                                                           " +
//"                                           AND v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang                                                              " +
//"                                           AND v_KDT_SP_Xuat_HD.SoThuTuHang = ToKhai.SoThuTuHang                                                              " +
//"                 INNER JOIN t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID                                                " +
//"                 INNER JOIN t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID                                          " +
//"                 INNER JOIN dbo.t_GC_DinhMuc AS DM ON ToKhai.MaHang = DM.MaSanPham                                                                 " +
//"                                                      AND ToKhai.IDHopDong = DM.HopDong_ID                                                         " +
//"         WHERE   ( v_KDT_SP_Xuat_HD.IDHopDong in (" + ListID_HopDong + ") )                                                                                              " +
//                    //"                 AND ( ToKhai.MaDoanhNghiep = @MaDoanhNghiep )                                                                                   "+
//                    //"                 AND ( ToKhai.MaHaiQuan = @MaHaiQuan )                                                                                           "+
//                    //"                 --AND ToKhai.NgayDangKy >= @TuNgay                                                                                                "+
//                    //"                 --AND ToKhai.NgayDangKy <= @DenNgay                                                                                               "+
//" ORDER BY        ToKhai.IDHopDong ,                                                                                                                " +
//"                 MaNguyenPhuLieu ,                                                                                                                 " +
//"                 ToKhai.NgayDangKy ,                                                                                                               " +
//"                 MaHang                                                                                                                            ";

                //SqlCommand cmd = (SqlCommand)db.GetSqlStringCommand(sql);
                //db.AddInParameter(cmd, "@IDHopDong", SqlDbType.BigInt, this.HD.ID);
                //db.AddInParameter(cmd, "@MaHaiQuan", SqlDbType.Char, GlobalSettings.MA_HAI_QUAN);
                //db.AddInParameter(cmd, "@MaDoanhNghiep", SqlDbType.NVarChar, GlobalSettings.MA_DON_VI);
                //db.AddInParameter(cmd, "@TuNgay", SqlDbType.DateTime, dtpTuNgay.Value);
                //db.AddInParameter(cmd, "@DenNgay", SqlDbType.DateTime, dtpDenNgay.Value);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            #endregion
            try
            {
              SqlCommand cmd = (SqlCommand)db.GetStoredProcCommand("p_GC_XuatNhapTon_SP_DM_NPL_TT38");
                db.AddInParameter(cmd, "@HopDong_ID", SqlDbType.NVarChar, ListID_HopDong);
                cmd.CommandTimeout = 60000;
                ds = db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return ds;

        }

        public DataSet GetXNTFromThanhKhoan()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();
            //ngayDauKy = dtpNgayDauKy.Value.ToString("yyyy-MM-dd");
            //ngayCuoiKy = dtpNgayCuoiKy.Value.ToString("yyyy-MM-dd");
            //ngayDauKy = ngayDauKy + " 00:00:00.00";
            //ngayCuoiKy = ngayCuoiKy + " 23:59:59.999";
            //Lấy danh sách SP chưa có định mức
           #region Comment SQL
//            try
//            {
//                if (ListHD != "")
//                {

//                    string sql = " SELECT  0 AS STT ,																										" +
//"         152 AS SoTaiKhoan ,                                                                                              " +
//"         n.OldHD_ID AS [HopDong_ID] ,                                                                                     " +
//"         hd.SoHopDong ,                                                                                                   " +
//"         t.SoToKhai ,                                                                                                     " +
//"         t.SoToKhaiVnacc ,                                                                                                " +
//"         t.NgayDangKy ,                                                                                                   " +
//"         t.MaLoaiHinh ,                                                                                                   " +
//"         [Ma] ,                                                                                                           " +
//"         [Ten] ,                                                                                                          " +
//"         '' AS [MaHS] ,                                                                                                   " +
//"         n.DVT AS DVT_ID ,                                                                                                " +
//"         TongLuongNK AS [SoLuongDangKy] ,                                                                                 " +
//"         TongLuongNK AS [SoLuongDaNhap] ,                                                                                 " +
//"         TongLuongXK AS [SoLuongDaDung] ,                                                                                 " +
//"         TongLuongCU AS [SoLuongCungUng] ,                                                                                " +
//"         ( TongLuongNK + TongLuongCU ) - TongLuongXK AS LuongTon ,                                                        " +
//"         t.SoLuong ,                                                                                                      " +
//"         t.SoLuong AS LuongSD_TK,                                                                                        " +
//"         t.SoLuong AS LuongTon_TK ,                                                                                       " +
//"         CASE WHEN t.TyGiaTT>0 THEN CONVERT(DECIMAL(24, 6), ( t.DonGiaTT / t.TyGiaTT )) ELSE 0 END AS DonGiaKB ,          " +
//"         CONVERT(DECIMAL(24, 6), t.DonGiaTT) AS DonGiaTT ,                                                                " +
//"         CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaTT ,                                                                " +
//"         CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaSuDung ,                                                            " +
//"         CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TonTriGia ,                                                               " +
//"         t.TyGiaTT ,                                                                                                      " +
//"         t.MaDVT                                                                                                          " +
//" FROM    [dbo].[t_GC_ThanhKhoanHDGC] n                                                                                    " +
//"         JOIN ( " +
//" SELECT   dk.HopDong_ID AS IDHopDong ,                                                " +
//"                         dk.SoTiepNhan AS SoToKhai ,                                  " +
//"                         dk.SoTiepNhan AS SoToKhaiVnacc ,                             " +
//"                         dk.NgayTiepNhan AS NgayDangKy ,                              " +
//"                         'NLHUY' AS MaLoaiHinh ,                                      " +
//"                         cu.MaNPL AS MaPhu ,                                          " +
//"                         cu.TenNPL AS TenHang ,                                       " +
//"                         cu.LuongNPLHuy AS SoLuong ,                                  " +
//"                         1 AS DonGiaTT ,                                              " +
//"                         1 AS TriGiaTT ,                                              " +
//"                         1 AS TyGiaTT ,                                               " +
//"                         'VND' AS MaDVT                                               " +
//"                FROM     t_KDT_GC_NPLHuyDangKy AS dk                                  " +
//"                         JOIN dbo.t_KDT_GC_NPLHuy AS cu ON dk.ID = cu.Master_ID       " +
//"                WHERE    dk.SoHopDong IN ( " + ListHD + " )                          " +
//" UNION ALL " +
//" SELECT   dk.HopDong_ID AS IDHopDong ,                                                       " +
//"                         dk.SoTiepNhan AS SoToKhai ,                                         " +
//"                         dk.SoTiepNhan AS SoToKhaiVnacc ,                                    " +
//"                         dk.NgayTiepNhan AS NgayDangKy ,                                     " +
//"                         'NPLCU' AS MaLoaiHinh ,                                             " +
//"                         cu.MaNPL AS MaPhu ,                                                 " +
//"                         cu.TenNPL AS TenHang ,                                              " +
//"                         cu.LuongCungUng AS SoLuong ,                                        " +
//"                         1 AS DonGiaTT ,                                                     " +
//"                         1 AS TriGiaTT ,                                                     " +
//"                         1 AS TyGiaTT ,                                                      " +
//"                         'VND' AS MaDVT                                                      " +
//"                FROM     t_KDT_GC_CungUngDangKy AS dk                                        " +
//"                         JOIN dbo.t_KDT_GC_CungUng AS cu ON dk.ID = cu.Master_ID             " +
//"                WHERE    dk.SoHopDong IN ( " + ListHD + " )                                 " +
//"                   UNION ALL " +
//                    " SELECT   tkmd.IDHopDong ,                                                                                 " +
//"                         tkmd.SoToKhai ,                                                                                  " +
//"                         tkmd.Huongdan_PL AS SoToKhaiVnacc ,                                                              " +
//"                         tkmd.NgayDangKy ,                                                                                " +
//"                         tkmd.MaLoaiHinh ,                                                                                " +
//"                         hmd.MaHang AS MaPhu ,                                                                            " +
//"                         hmd.TenHang ,                                                                                    " +
//"                         hmd.SoLuong ,                                                                                    " +
//"                         hmd.DonGiaTT ,                                                                                   " +
//"                         hmd.TriGiaTT ,                                                                                   " +
//"                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
//"                              THEN ( SELECT TOP 1 g.TyGiaTinhThue                                                              " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE 0                                                                                      " +
//"                         END AS TyGiaTT ,                                                                                 " +
//"                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
//"                              THEN ( SELECT TOP 1  g.MaTTTyGiaTinhThue                                                          " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)                           " +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE ''                                                                                     " +
//"                         END AS MaDVT                                                                                     " +
//"                FROM     t_KDT_GC_HangChuyenTiep hmd                                                                      " +
//"                         INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tkmd ON hmd.Master_ID = tkmd.ID                             " +
//"                WHERE    SoHopDongDV IN ( " + ListHD + " )                                                                " +
//"                         AND maloaihinh LIKE '%NV%'                                                                       " +
//"                         AND LoaiHangHoa = 'N'                                                                            " +
//"                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
////"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'                  " +
//"                UNION ALL                                                                                                     " +
//"                SELECT   tkmd.IDHopDong ,                                                                                 " +
//"                         tkmd.SoToKhai ,                                                                                  " +
//"                         tkmd.Huongdan_PL AS SoToKhaiVnacc ,                                                              " +
//"                         tkmd.NgayDangKy ,                                                                                " +
//"                         tkmd.MaLoaiHinh ,                                                                                " +
//"                         hmd.MaHang AS MaPhu ,                                                                            " +
//"                         hmd.TenHang ,                                                                                    " +
//"                         hmd.SoLuong ,                                                                                    " +
//"                         hmd.DonGiaTT ,                                                                                   " +
//"                         hmd.TriGiaTT ,                                                                                   " +
//"                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
//"                              THEN ( SELECT TOP 1  g.TyGiaTinhThue                                                              " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE 0                                                                                      " +
//"                         END AS TyGiaTT ,                                                                                 " +
//"                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
//"                              THEN ( SELECT TOP 1 g.MaTTTyGiaTinhThue                                                          " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE ''                                                                                     " +
//"                         END AS MaDVT                                                                                     " +
//"                FROM     t_KDT_GC_HangChuyenTiep hmd                                                                      " +
//"                         INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tkmd ON hmd.Master_ID = tkmd.ID                             " +
//"                WHERE    SoHopDongDV IN ( " + ListHD + " )                                                                " +
//"                         AND maloaihinh LIKE '%XV%'                                                                       " +
//"                         AND LoaiHangHoa = 'N'                                                                            " +
//"                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
////"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'                  " +
//"                UNION ALL                                                                                                     " +
//"                SELECT   tkmd.IDHopDong ,                                                                                 " +
//"                         tkmd.SoToKhai ,                                                                                  " +
//"                         tkmd.LoaiVanDon AS SoToKhaiVnacc ,                                                               " +
//"                         tkmd.NgayDangKy ,                                                                                " +
//"                         tkmd.MaLoaiHinh ,                                                                                " +
//"                         hmd.MaPhu ,                                                                                      " +
//"                         hmd.TenHang ,                                                                                    " +
//"                         hmd.SoLuong ,                                                                                    " +
//"                         hmd.DonGiaTT ,                                                                                   " +
//"                         hmd.TriGiaTT ,                                                                                   " +
//"                         CASE WHEN tkmd.LoaiVanDon <> ''                                                                  " +
//"                              THEN ( SELECT TOP 1 g.TyGiaTinhThue                                                              " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE 0                                                                                      " +
//"                         END AS TyGiaTT ,                                                                                 " +
//"                         CASE WHEN tkmd.LoaiVanDon <> ''                                                                  " +
//"                              THEN ( SELECT TOP 1  g.MaTTTyGiaTinhThue                                                          " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE ''                                                                                     " +
//"                         END AS MaDVT                                                                                     " +
//"                FROM     t_KDT_HangMauDich hmd                                                                            " +
//"                         INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID                                     " +
//"                WHERE    SoHopDong IN ( " + ListHD + " )                                                                  " +
//"                         AND MaLoaiHinh LIKE '%NV%'                                                                       " +
//"                         AND LoaiHangHoa = 'N'                                                                            " +
//"                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
////"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'          " +
//"                UNION ALL                                                                                                     " +
//"                SELECT tkmd.IDHopDong," +
//                "		tkmd.SoToKhai," +
//                "		CONVERT(VARCHAR(18), tkmd.LoaiVanDon) AS SoToKhaiVnacc ," +
//                "		tkmd.NgayDangKy," +
//                "		tkmd.MaLoaiHinh," +
//                "		dbo.t_GC_DinhMuc.MaNguyenPhuLieu AS MaPhu," +
//                "		dbo.t_GC_DinhMuc.TenNPL AS TenHang," +
//                "		hmd.SoLuong * ( dbo.t_GC_DinhMuc.DinhMucSuDung/ 100 * dbo.t_GC_DinhMuc.TyLeHaoHut+ dbo.t_GC_DinhMuc.DinhMucSuDung ) AS SoLuong, " +
//                "		hmd.DonGiaTT, " +
//                "	    hmd.TriGiaTT, " +
//                "		tkmd.TyGiaTinhThue AS TyGiaTT, " +
//                "		tkmd.NguyenTe_ID AS MaDVT	" +
//            "	FROM    dbo.t_KDT_ToKhaiMauDich tkmd " +
//            "	INNER JOIN dbo.t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
//            "	INNER JOIN dbo.t_GC_DinhMuc ON dbo.t_GC_DinhMuc.MaSanPham = hmd.MaPhu " +
//"                  AND dbo.t_GC_DinhMuc.HopDong_ID = tkmd.IDHopDong " +
//"	INNER JOIN dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID " +
//"                        AND dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma " +
//              " WHERE     SoHopDong IN (" + ListHD + ") " +
//"                         AND MaLoaiHinh LIKE '%XV%'                                                                       " +
//"                         AND LoaiHangHoa = 'S'                                                                            " +
//"                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
////"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'          " +
//"              ) t ON n.OldHD_ID = t.IDHopDong                                                                             " +
//"                     AND n.Ma = t.MaPhu                                                                                   " +
//"         JOIN t_kdt_gc_hopdong hd ON hd.ID = n.OldHD_ID                                                                   " +
//" WHERE   SoHopDong IN ( " + ListHD + " )                                                                                  " +
//" UNION ALL                                                                                                                    " +
//" SELECT  0 AS STT ,                                                                                                       " +
//"         155 AS SoTaiKhoan ,                                                                                              " +
//"         [HopDong_ID] ,                                                                                                   " +
//"         hd.SoHopDong ,                                                                                                   " +
//"         t.SoToKhai ,                                                                                                     " +
//"         t.SoToKhaiVnacc ,                                                                                                " +
//"         t.NgayDangKy ,                                                                                                   " +
//"         t.MaLoaiHinh ,                                                                                                   " +
//"         [Ma] ,                                                                                                           " +
//"         [Ten] ,                                                                                                          " +
//"         n.[MaHS] ,                                                                                                       " +
//"         CASE WHEN DVT_ID <> '' THEN ( SELECT    Ten                                                                      " +
//"                                       FROM      t_HaiQuan_DonViTinh                                                      " +
//"                                       WHERE     ID = n.DVT_ID                                                            " +
//"                                     )                                                                                    " +
//"              ELSE ''                                                                                                     " +
//"         END AS DVT_ID ,                                                                                                  " +
//"         [SoLuongDangKy] ,                                                                                                " +
//"         SoLuongDaXuat AS [SoLuongDaNhap] ,                                                                               " +
//"         SoLuongDaXuat AS [SoLuongDaDung] ,                                                                               " +
//"         0 AS [SoLuongCungUng] ,                                                                                          " +
//"         0 AS LuongTon ,                                                                                                  " +
//"         t.SoLuong ,                                                                                                      " +
//"         t.SoLuong AS LuongSD_TK ,                                                                                        " +
//"         0 AS LuongTon_TK ,                                                                                               " +
//"         CASE WHEN t.TyGiaTT>0 THEN CONVERT(DECIMAL(24, 6), ( t.DonGiaTT / t.TyGiaTT )) ELSE 0 END AS DonGiaKB ,                                                " +
//"         CONVERT(DECIMAL(24, 6), t.DonGiaTT) AS DonGiaTT ,                                                                " +
//"         CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaTT ,                                                                " +
//"         CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaSuDung ,                                                            " +
//"         0 AS TonTriGia ,                                                                                                 " +
//"         t.TyGiaTT ,                                                                                                      " +
//"         t.MaDVT                                                                                                          " +
//" FROM    [dbo].[t_GC_SanPham] n                                                                                           " +
//"         JOIN ( SELECT   tkmd.IDHopDong ,                                                                                 " +
//"                         tkmd.SoToKhai ,                                                                                  " +
//"                         tkmd.Huongdan_PL AS SoToKhaiVnacc ,                                                              " +
//"                         tkmd.NgayDangKy ,                                                                                " +
//"                         tkmd.MaLoaiHinh ,                                                                                " +
//"                         hmd.MaHang AS MaPhu ,                                                                            " +
//"                         hmd.TenHang ,                                                                                    " +
//"                         hmd.SoLuong ,                                                                                    " +
//"                         hmd.DonGiaTT ,                                                                                   " +
//"                         hmd.TriGiaTT ,                                                                                   " +
//"                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
//"                              THEN ( SELECT TOP 1  g.TyGiaTinhThue                                                              " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE 0                                                                                      " +
//"                         END AS TyGiaTT ,                                                                                 " +
//"                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
//"                              THEN ( SELECT TOP 1 g.MaTTTyGiaTinhThue                                                          " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE ''                                                                                     " +
//"                         END AS MaDVT                                                                                     " +
//"                FROM     t_KDT_GC_HangChuyenTiep hmd                                                                      " +
//"                         INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tkmd ON hmd.Master_ID = tkmd.ID                             " +
//"                WHERE    SoHopDongDV IN ( " + ListHD + " )                                                                " +
//"                         AND maloaihinh LIKE '%V%'                                                                       " +
//"                         AND LoaiHangHoa = 'S'                                                                            " +
//"                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
////"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'          " +
//"                UNION ALL                                                                                                     " +
//"                SELECT   tkmd.IDHopDong ,                                                                                 " +
//"                         tkmd.SoToKhai ,                                                                                  " +
//"                         tkmd.LoaiVanDon AS SoToKhaiVnacc ,                                                               " +
//"                         tkmd.NgayDangKy ,                                                                                " +
//"                         tkmd.MaLoaiHinh ,                                                                                " +
//"                         hmd.MaPhu ,                                                                                      " +
//"                         hmd.TenHang ,                                                                                    " +
//"                         hmd.SoLuong ,                                                                                    " +
//"                         hmd.DonGiaTT ,                                                                                   " +
//"                         hmd.TriGiaTT ,                                                                                   " +
//"                         CASE WHEN tkmd.LoaiVanDon <> ''                                                                  " +
//"                              THEN ( SELECT TOP 1 g.TyGiaTinhThue                                                              " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai) " +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE 0                                                                                      " +
//"                         END AS TyGiaTT ,                                                                                 " +
//"                         CASE WHEN tkmd.LoaiVanDon <> ''                                                                  " +
//"                              THEN ( SELECT TOP 1  g.MaTTTyGiaTinhThue                                                          " +
//"                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
//"                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
//"                                                               ID                                                         " +
//"                                                           FROM                                                           " +
//"                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
//"                                                           WHERE                                                          " +
//"                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
//"                                                         )                                                                " +
//"                                   )                                                                                      " +
//"                              ELSE ''                                                                                     " +
//"                         END AS MaDVT                                                                                     " +
//"                FROM     t_KDT_HangMauDich hmd                                                                            " +
//"                         INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID                                     " +
//"                WHERE    SoHopDong IN ( " + ListHD + " )                                                                  " +
//"                         AND (maloaihinh LIKE '%XVB13%' OR maloaihinh LIKE '%NVA31%')                                                                       " +
//"                         AND LoaiHangHoa = 'S'                                                                            " +
//"                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
////"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'          " +
//"                UNION ALL                                                                                                     " +
//"                SELECT   tkmd.IDHopDong ,                                                                                 " +
//"                         tkmd.SoToKhai ,                                                                                  " +
//"                         CONVERT(VARCHAR(18), tkmd.LoaiVanDon) AS SoToKhaiVnacc ,                                           " +
//"                         tkmd.NgayDangKy ,                                                                                " +
//"                         tkmd.MaLoaiHinh ,                                                                                " +
//"                         hmd.MaPhu ,                                                                                      " +
//"                         hmd.TenHang ,                                                                                    " +
//"                         hmd.SoLuong ,                                                                                    " +
//"                         hmd.DonGiaTT ,                                                                                   " +
//"                         hmd.TriGiaTT ,                                                                                   " +
//"                         tkmd.TyGiaTinhThue AS TyGiaTT ,                                                                       " +
//"                         tkmd.NguyenTe_ID AS MaDVT                                                                                   " +
//"                FROM     t_KDT_HangMauDich hmd                                                                            " +
//"                         INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID                                     " +
//"                WHERE    SoHopDong IN ( " + ListHD + " )                                                                  " +
//"                         AND maloaihinh  LIKE '%V%'                                                                    " +
//"                         AND LoaiHangHoa = 'S'                                                                            " +
//"                         AND TrangThaiXuLy = 1                                                                            " +
////"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'          " +
//                "				UNION ALL " +
//                "SELECT  " +
//                "		tkmd.IDHopDong," +
//                "		tkmd.SoToKhai," +
//                "		CONVERT(VARCHAR(18), tkmd.LoaiVanDon) AS SoToKhaiVnacc ," +
//                "		tkmd.NgayDangKy," +
//                "		tkmd.MaLoaiHinh," +
//                "		dbo.t_GC_DinhMuc.MaNguyenPhuLieu AS MaPhu," +
//                "		dbo.t_GC_DinhMuc.TenNPL AS TenHang," +
//                "		hmd.SoLuong * ( dbo.t_GC_DinhMuc.DinhMucSuDung/ 100 * dbo.t_GC_DinhMuc.TyLeHaoHut+ dbo.t_GC_DinhMuc.DinhMucSuDung ) AS SoLuong, " +
//                "		hmd.DonGiaTT, " +
//                "		hmd.TriGiaTT, " +
//                "		tkmd.TyGiaTinhThue AS TyGiaTT, " +
//                "		tkmd.NguyenTe_ID AS MaDVT	" +
//            "	FROM    dbo.t_KDT_ToKhaiMauDich tkmd " +
//            "	INNER JOIN dbo.t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
//            "	INNER JOIN dbo.t_GC_DinhMuc ON dbo.t_GC_DinhMuc.MaSanPham = hmd.MaPhu " +
//                        "                  AND dbo.t_GC_DinhMuc.HopDong_ID = tkmd.IDHopDong " +
//            "	INNER JOIN dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID " +
//                        "                        AND dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma " +
//              " WHERE    SoHopDong IN (" + ListHD + ") " +
//              "         AND ( MaLoaiHinh LIKE '%XVB13%' " +
//              "               OR MaLoaiHinh LIKE '%NVA31%' " +
//              "             ) " +
//              "         AND LoaiHangHoa = 'S' " +
//              "         AND tkmd.TrangThaiXuLy = 1 " +
////              "         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'          " +
//"              ) t ON n.HopDong_ID = t.IDHopDong                                                                           " +
//"                     AND n.Ma = t.MaPhu                                                                                   " +
//"         JOIN t_kdt_gc_hopdong hd ON hd.ID = n.HopDong_ID                                                                 " +
//" WHERE   SoHopDong IN ( " + ListHD + " )                                                                                  " +
//" ORDER BY SoTaiKhoan ASC ,                                                                                                " +
//"         HopDong_ID ,                                                                                                     " +
//"         n.Ma ,                                                                                                           " +
//"         NgayDangKy                                                                                                       ";
//                  SqlCommand cmd = (SqlCommand)db.GetSqlStringCommand(sql);

//                    //DbCommand cmd = db.GetStoredProcCommand("p_GC_XuatNhapTon");
//                    //db.AddInParameter(cmd, "@ListHD", DbType.String, ListHD);
//                    cmd.CommandTimeout = 60000;
//                    ds = db.ExecuteDataSet(cmd);
//                }
//                else
//                {
//                    DbCommand cmd = db.GetStoredProcCommand("p_GC_XuatNhapTon_TT38");
//                    db.AddInParameter(cmd, "@HopDong_ID", DbType.String, ListID_HopDong);
//                    cmd.CommandTimeout = 60000;
//                    ds = db.ExecuteDataSet(cmd);
//                }

//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
//            }
          #endregion
            try
            {
                DbCommand cmd = db.GetStoredProcCommand("p_GC_XuatNhapTon_TT38");
                db.AddInParameter(cmd, "@HopDong_ID", DbType.String, ListID_HopDong);
                cmd.CommandTimeout = 60000;
                ds = db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return ds;

        }
        public DataSet GetXNTFromQuyetToan(string list_HD)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();
            try
            {
                DbCommand cmd = db.GetStoredProcCommand("p_GC_XuatNhapTon_TT38");
                db.AddInParameter(cmd, "@HopDong_ID", DbType.String, list_HD);
                cmd.CommandTimeout = 60000;
                ds = db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return ds;

        }

        private void loadHS()
        {
            if (isUpdate)
            {
                txtNamQT.Text = HS.NamQT.ToString();
                //txtID.Text = HS.ID.ToString();
                //txtSoHoSo.Text = HS.SoHoSo;
                txtGhiChu.Text = HS.GhiChu;
                dateNgayTN.Value = HS.NgayTao;
            }
            else
            {
                txtNamQT.Text = DateTime.Now.Year.ToString(); ;
                //txtID.Text = "";
                //txtSoHoSo.Text = "";
                txtGhiChu.Text = "";
                dateNgayTN.Value = DateTime.Now;
            }
        }
        private void XuatNhapTonForm_Load(object sender, EventArgs e)
        {
            if (isXuLy1HD)
            {
                txtNamQT.Text = (DateTime.Now.Year - 1).ToString();
                cbbLoaiHinh.SelectedValue = 1;
                cbbDVT.SelectedValue = 2;
                uiTabPage4.Enabled = false;
                btnThemHD.Enabled = false;
                btnGetXNTFromThanhKhoan.PerformClick();
            }
            else
            {
                if (goodItem.ID != 0)
                {
                    SetGoodItem();
                    BindDataGoodItem();
                    BindDataHD();
                    btnGetXNTFromThanhKhoan.PerformClick();
                }
                else
                {
                    txtNamQT.Text = (DateTime.Now.Year -1).ToString();
                    cbbLoaiHinh.SelectedValue = 1;
                    cbbDVT.SelectedValue = 2;
                }
            }

            if (ListHD != "")
            {
                dgList.RootTable.Columns["SoHopDong"].Visible = true;
                btnInBaoCao.Enabled = false;
                btnXuatMau15_TriGia.Enabled = false;
                btnBCMau15_TongTriGia.Enabled = false;
            }
            else
            {
                dgList.RootTable.Columns["SoHopDong"].Visible = true;
                btnInBaoCao.Enabled = true;
                btnXuatMau15_TriGia.Enabled = true;
                btnXuLyXNT_Mau15.Enabled = false;
            }
            if (ListID_HopDong == "")
            {
                ListID_HopDong = HD.ID.ToString();
            }
            btnBCMau15_TongTriGia.Enabled = true;
            btnXuatMau15_TriGia.Enabled = true;
            btnInBaoCao.Enabled = true;
            btnXuLyXNT_Mau15.Enabled = true;
            btnMau15_Luong.Enabled = true;
            btnMau15_TongLuong.Enabled = true;

            if (isDaQuyetToan)
            {
                btnGetXNTFromThanhKhoan.Text = "BƯỚC 2 : LẤY DỮ LIỆU";
                btnDone.Enabled = false;
                btnBCMau15_TongTriGia.Enabled = true;
                btnXuatMau15_TriGia.Enabled = true;
                btnInBaoCao.Enabled = true;
                btnXuLyXNT_Mau15.Enabled = false;
                btnMau15_Luong.Enabled = true;
                btnMau15_TongLuong.Enabled = true;
                btnDone.Enabled = false;
                btnGetXNTFromThanhKhoan.PerformClick();
            }
            dtpNgayDauKy.Value = new DateTime(DateTime.Now.Year - 1, 01, 01);
            dtpNgayCuoiKy.Value = new DateTime(DateTime.Now.Year - 1, 12, 31);
            dgList.HideColumnsWhenGrouped = InheritableBoolean.True;

        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "XuatNhapTon_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
            ////-----------------------
            //dgList.Tables[0].Columns["SoToKhaiVNACC"].Visible = true;
            ////dgList.Tables[0].Columns["SoToKhai"].Visible = true;
            //dgList.Tables[0].Columns["NgayDangKy"].Visible = true;
            //dgList.Tables[0].Columns["MaLoaiHinh"].Visible = true;
            //GridEXGroup customG = dgList.Tables[0].Groups[0].Clone();
            //dgList.Tables[0].Groups.Clear();
            //SaveFileDialog sfNPL = new SaveFileDialog();
            //sfNPL.Filter = "Excel files| *.xls";
            //sfNPL.ShowDialog(this);
            //if (sfNPL.FileName != "")
            //{
            //    gridEXExporter1.GridEX = dgList;
            //    Stream str = sfNPL.OpenFile();
            //    gridEXExporter1.Export(str);
            //    str.Close();
            //    dgList.Tables[0].Groups.Add(customG);
            //    dgList.Tables[0].Columns["SoToKhaiVNACC"].Visible = false;
            //    dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
            //    dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;

            //    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
            //    {
            //        Process.Start(sfNPL.FileName);
            //    }
            //}
            //else
            //{
            //    dgList.Tables[0].Columns["SoToKhaiVNACC"].Visible = false;
            //    dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
            //    dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;
            //    dgList.Tables[0].Groups.Add(customG);
            //}
        }

        private void btnInBaoCao_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                NhapXuatTon_TT38 xnt = new NhapXuatTon_TT38();
                xnt.HD = this.HD;
                xnt.tb = tb;
                xnt.BindReport("");
                xnt.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }


        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXuatMau15_Click(object sender, EventArgs e)
        {

        }

        private void btnXuLyXNT_Mau15_Click(object sender, EventArgs e)
        {

            btnBCMau15_TongTriGia.Enabled = false;
            lblXuLy.Text = "Đang xử lý...";
            progressBar1.Value = 0;
            lblPercen.Text = "0%";
            btnXuLyXNT_Mau15.Enabled = false;
            if (backgroundWorker1.IsBusy)
                backgroundWorker1.CancelAsync();
            backgroundWorker1.RunWorkerAsync();

            dgList.DataSource = tb;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (lblXuLy.Text == "Đã xử lý xuất nhập tồn")
            {
                //foreach (DataRow dr in tb.Rows)
                //{
                //    if(dr[""])
                //}
            }
            else
            {
                #region Tính Lương tồn
                DataTable temp = tb.DefaultView.ToTable(true, "SoTaiKhoan", "SoHopDong", "Ma", "SoLuongDaNhap", "SoLuongCungUng", "SoLuongDaDung", "LuongTon");

                //DataTable tbTKN = tb.DefaultView.ToTable(true,);
                totalRows = temp.Select("SoTaiKhoan='152'").Length;
                progressBar1.Minimum = 0;
                try
                {
                    progressBar1.Maximum = 100;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

                backgroundWorker1.WorkerReportsProgress = true;
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    //decimal tongNhap = 0;
                    decimal tongSuDung = 0;
                    //decimal tongTon = 0;
                    #region Tính toán
                    foreach (DataRow dr in temp.Select())
                    {
                        //string ma1 = dr["Ma"].ToString();
                        //tongNhap = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                        tongSuDung = Convert.ToDecimal(dr["SoLuongDaDung"].ToString());
                        //tongTon = Convert.ToDecimal(dr["LuongTon"].ToString());

                        decimal luongTK = 0;
                        //decimal luongSD = 0;
                        //decimal luongTon = 0;
                        decimal triGiaTK = 0;
                        //decimal triGiaSD = 0;
                        //decimal triGiaTon = 0;
                        decimal donGia = 0;
                        //decimal tyGia = 0;
                        if (dr["Ma"].ToString() == "PL05")
                        {

                        }
                        foreach (DataRow dr1 in tb.Select("SoTaiKhoan='" + dr["SoTaiKhoan"].ToString() + "' and Ma='" + dr["Ma"].ToString() + "' and SoHopDong = '" + dr["SoHopDong"].ToString() + "'", "NgayDangKy"))
                        {

                            if (dr["Ma"].ToString() == "PL05")
                            {

                            }
                            luongTK = Convert.ToDecimal(dr1["SoLuong"].ToString());
                            //luongSD = Convert.ToDecimal(dr1["LuongSD_TK"].ToString());
                            //luongTon = Convert.ToDecimal(dr1["LuongTon_TK"].ToString());
                            triGiaTK = Convert.ToDecimal(dr1["TriGiaTT"].ToString());
                            //try
                            //{
                            //    triGiaSD = Convert.ToDecimal(dr1["TriGiaSuDung"].ToString());
                            //}
                            //catch (Exception ex)
                            //{
                            //}
                            //triGiaTon = Convert.ToDecimal(dr1["TonTriGia"].ToString());
                            donGia = Convert.ToDecimal(dr1["DonGiaTT"].ToString());
                            //try
                            //{
                            //    tyGia = dr1["TyGiaTT"] != DBNull.Value ? Convert.ToDecimal(dr1["TyGiaTT"].ToString()) : 0;
                            //}
                            //catch (Exception ex)
                            //{

                            //    //throw;
                            //}

                            if (dr1["SoTaiKhoan"].ToString() == "152" && dr1["MaLoaiHinh"].ToString().Contains("N"))
                            {
                                #region Tài khoản 152
                                try
                                {
                                    #region Tính lượng vs trị giá
                                    if (tongSuDung >= luongTK)
                                    {
                                        dr1["LuongSD_TK"] = luongTK;
                                        dr1["LuongTon_TK"] = 0;
                                        try
                                        {
                                            dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                            dr1["TriGiaSuDung"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }
                                        dr1["TonTriGia"] = 0;
                                        tongSuDung = tongSuDung - luongTK;
                                    }
                                    else if (tongSuDung > 0 && tongSuDung < luongTK)
                                    {
                                        dr1["LuongSD_TK"] = tongSuDung;
                                        dr1["LuongTon_TK"] = luongTK - tongSuDung;

                                        try
                                        {
                                            dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                            dr1["TriGiaSuDung"] = Math.Round(tongSuDung * donGia, 0).ToString();
                                            dr1["TonTriGia"] = Math.Round(triGiaTK - (tongSuDung * donGia), 0).ToString();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }
                                        tongSuDung = 0;
                                    }
                                    else
                                    {
                                        dr1["LuongSD_TK"] = 0;
                                        dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                        dr1["TriGiaSuDung"] = 0;
                                        dr1["TonTriGia"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                    }

                                    #endregion

                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                                #endregion Tài khoản 152

                            }
                            else if (dr1["SoTaiKhoan"].ToString() == "152" && dr1["MaLoaiHinh"].ToString().Contains("X"))
                            {

                                #region Tài Khoản 152 chuyển tiếp
                                try
                                {
                                    #region Tính lượng vs trị giá

                                    dr1["SoLuong"] = luongTK;
                                    dr1["LuongSD_TK"] = luongTK;
                                    dr1["LuongTon_TK"] = 0;
                                    dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                    dr1["TriGiaSuDung"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                    dr1["TonTriGia"] = 0;
                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                                #endregion Tài Khoản chuyển tiếp
                            }
                        }

                        index++;
                        try
                        {
                            int i = (index * 100) / totalRows;
                            backgroundWorker1.ReportProgress(i);
                        }
                        catch (Exception)
                        {

                        }

                    }
                    #endregion
                }

                #endregion
            }

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {

                progressBar1.Value = e.ProgressPercentage;
                lblPercen.Text = e.ProgressPercentage.ToString() + " %";
                lblPercen.Refresh();
                lblXuLy.Text = "Đang xử lý...(" + index + "/" + totalRows + ")";
                lblXuLy.Refresh();
            }
            catch (Exception)
            {

                //    throw;
            }

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblXuLy.Text = "Đã xử lý xuất nhập tồn";
            btnXuLyXNT_Mau15.Enabled = false;
            btnXuatMau15_TriGia.Enabled = true;
            btnInBaoCao.Enabled = true;
            btnBCMau15_TongTriGia.Enabled = true;
            btnMau15_TongLuong.Enabled = true;
            btnMau15_Luong.Enabled = true;
        }

        private void btnBCMau15Tong_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Mau15_TT38_Tong xnt = new Mau15_TT38_Tong();
                //DataTable temp = tb.DefaultView.ToTable(true, "SoHopDong", "Ma", "SoLuongDaNhap", "SoLuongCungUng", "SoLuongDaDung", "LuongTon");
                //backgroundWorker1.RunWorkerAsync();
                xnt.HD = this.HD;
                //xnt.NamXuLy = int.Parse(numNamXuLy.Value.ToString());
                xnt.NgayDauKy = dtpNgayDauKy.Value;
                xnt.NgayCuoiKy=dtpNgayCuoiKy.Value;
                xnt.tb_Mau15 = tb;
                if (isDaQuyetToan)
                    xnt.BindReport(dt, true);
                else
                    xnt.BindReport(dt, false);
                xnt.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }



        private void btnGetXNTFromThanhKhoan_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (isDaQuyetToan)
                {
                    //tb = HopDong.SelectDynamic("HopDong_ID in (" + ListID_HopDong + ")", "HopDong_ID").Tables[0];
                    tb = t_GC_QuyetToan_Mau15.SelectDynamic("HopDong_ID in (" + ListID_HopDong + ")", "HopDong_ID").Tables[0];
                    tbNPL_SP_DM = NPLQuyetToan.SelectDynamic("HopDong_ID in (" + ListID_HopDong + ")", "HopDong_ID").Tables[0];
                    lblXuLy.Text = "Đã xử lý";
                }
                else
                {
                    tb = this.GetXNTFromThanhKhoan().Tables[0];
                    tbNPL_SP_DM = this.GetDM_SP_NPL().Tables[0];
                }
                dgList.DataSource = tb;
                dgDinhMuc_SanPham.DataSource = tbNPL_SP_DM;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if (isDaQuyetToan)
                    btnXuLyXNT_Mau15.Enabled = false;
                else
                    btnXuLyXNT_Mau15.Enabled = true;
            }
            if (tbNPL_SP_DM.Columns.Count >=1)
            {
                ProcessDataReport(tbNPL_SP_DM, isDaQuyetToan);
                btnXuLyXNT_Mau15.Enabled = false;
                //ShowMessage("Xử lý thành công", false); 
            }
        }
        public void ProcessDataReport(DataTable tb_DM_SP_NPL, bool isDaQuyetToan)
        {
            NgayDauKy = dtpNgayDauKy.Value;
            NgayCuoiKy = dtpNgayCuoiKy.Value; 
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            DataTable dtTable = new DataTable();
            tb_Mau15 = tb;
            #region Xử lý tổng
            DataTable tbNPL = tb_Mau15.DefaultView.ToTable(true, "SoTaiKhoan", "HopDong_ID", "SoHopDong", "Ma", "Ten", "SoLuongDaNhap", "SoLuongDaDung", "SoLuongCungUng", "LuongTon");
            int stt = 1;
            //return;
            dtTable = tb_Mau15.Clone();
            if (!dtTable.Columns.Contains("LuongNhap_DauKy"))
            {
                DataColumn dc1 = new DataColumn("LuongNhap_DauKy");
                dc1.DataType = typeof(System.Decimal);
                DataColumn dc2 = new DataColumn("LuongNhap_TrongKy");
                dc2.DataType = typeof(System.Decimal);
                DataColumn dc3 = new DataColumn("LuongXuat_DauKy");
                dc3.DataType = typeof(System.Decimal);
                DataColumn dc4 = new DataColumn("LuongXuat_TrongKy");
                dc4.DataType = typeof(System.Decimal);
                DataColumn dc5 = new DataColumn("LuongTonCuoi");
                dc5.DataType = typeof(System.Decimal);
                DataColumn dc6 = new DataColumn("TriGiaNhap_DauKy");
                dc6.DataType = typeof(System.Decimal);
                DataColumn dc7 = new DataColumn("TriGiaNhap_TrongKy");
                dc7.DataType = typeof(System.Decimal);
                DataColumn dc8 = new DataColumn("TriGiaXuat_DauKy");
                dc8.DataType = typeof(System.Decimal);
                DataColumn dc9 = new DataColumn("TriGiaXuat_TrongKy");
                dc9.DataType = typeof(System.Decimal);
                DataColumn dc10 = new DataColumn("TriGiaTonCuoi");
                dc10.DataType = typeof(System.Decimal);
                DataColumn dc11 = new DataColumn("TriGiaTonDau");
                dc11.DataType = typeof(System.Decimal);
                DataColumn dc12 = new DataColumn("LuongTonDau");
                dc12.DataType = typeof(System.Decimal);
                //minhnd thêm CU và hủy
                DataColumn dc13 = new DataColumn("LuongCU_DauKy");
                dc13.DataType = typeof(System.Decimal);
                DataColumn dc14 = new DataColumn("LuongCU_TrongKy");
                dc14.DataType = typeof(System.Decimal);
                DataColumn dc15 = new DataColumn("LuongNPLHuy");
                dc15.DataType = typeof(System.Decimal);
                dtTable.Columns.Add(dc1);
                dtTable.Columns.Add(dc2);
                dtTable.Columns.Add(dc3);
                dtTable.Columns.Add(dc4);
                dtTable.Columns.Add(dc5);
                dtTable.Columns.Add(dc6);
                dtTable.Columns.Add(dc7);
                dtTable.Columns.Add(dc8);
                dtTable.Columns.Add(dc9);
                dtTable.Columns.Add(dc10);
                dtTable.Columns.Add(dc11);
                dtTable.Columns.Add(dc12);
                dtTable.Columns.Add(dc13);
                dtTable.Columns.Add(dc14);
                dtTable.Columns.Add(dc15);
            }
            foreach (DataRow dr in tbNPL.Rows)
            {
                decimal luongNK_DauKy = 0;
                decimal luongNK_TrongKy = 0;
                //decimal luongNK_DauKyTong = 0;
                //decimal luongNK_TrongKyTong = 0;
                decimal triGiaNK_DauKy = 0;
                decimal triGiaNK_TrongKy = 0;
                decimal luongXK_DauKy = 0;
                decimal luongXK_DauKyTong = 0;
                decimal luongXK_TrongKy = 0;
                decimal luongXK_TrongKyTong = 0;
                decimal triGiaXK_DauKy = 0;
                decimal triGiaXK_TrongKy = 0;
                //decimal luongSD = 0;
                decimal luongTon = 0;
                decimal luongTonDau = 0;
                decimal triGiaTonDau = 0;
                decimal triGiaTon = 0;
                //minhnd thêm lượng xuất trả
                decimal luongXTDauKy = 0;
                decimal triGiaXTDauKy = 0;
                decimal luongXTTrongKy = 0;
                decimal triGiaXTTrongKy = 0;
                //minhnd thêm lượng xuất trả
                //minhnd thêm lượng cung ứng và hủy
                decimal luongCUDauKy = 0;
                //decimal triGiaCUDauKy = 0;
                decimal luongCUTrongKy = 0;
                //decimal triGiaCUTrongKy = 0;
                //
                decimal luongNPLHuy = 0;
                //minhnd thêm lượng cung ứng và hủy
                decimal luongxuatDauCopy = 0;
                decimal luongxuatTrongCopy = 0;
                string DVT = "";
                try
                {
                    //Tính NPL xuất theo kỳ | tờ khai xuất định mức mã hàng
                    //Chưa + lương chuyển tiếp và xuất trả của các tờ khai xuất
                    foreach (DataRow drNPL in tb_DM_SP_NPL.Rows)
                    {

                        if (dr["Ma"].ToString() == drNPL["MaNguyenPhuLieu"].ToString())
                        {
                            try
                            {
                                int nam = int.Parse(drNPL["NgayDangKy"].ToString().Substring(6, 4));
                                int thang = int.Parse(drNPL["NgayDangKy"].ToString().Substring(3, 2));
                                int ngay = int.Parse(drNPL["NgayDangKy"].ToString().Substring(0, 2));
                                if (thang == 12 && ngay == 31)
                                {

                                }
                                //duydp
                                DateTime dateTK = new DateTime(nam, thang, ngay);

                                if (dateTK < NgayDauKy)
                                {
                                    try
                                    {
                                        luongXK_DauKyTong += Convert.ToDecimal(drNPL["LuongNPLCoHH"].ToString());

                                    }
                                    catch (Exception ex)
                                    {

                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                    }

                                }
                                else if (dateTK <= NgayCuoiKy)
                                {
                                    luongXK_TrongKyTong += Convert.ToDecimal(drNPL["LuongNPLCoHH"].ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }

                        }
                        else
                            continue;
                    }
                    //Giữ lại lượng xuất trong ky
                    luongxuatTrongCopy = luongXK_TrongKyTong;
                    luongxuatDauCopy = luongXK_DauKyTong;
                    //Tính NPL Nhập
                    foreach (DataRow drNPL in tb_Mau15.Rows)
                    {
                        if (dr["Ma"].ToString() == drNPL["Ma"].ToString())
                        {
                            try
                            {
                                int nam = int.Parse(drNPL["NgayDangKy"].ToString().Substring(6, 4));
                                int thang = int.Parse(drNPL["NgayDangKy"].ToString().Substring(3, 2));
                                int ngay = int.Parse(drNPL["NgayDangKy"].ToString().Substring(0, 2));
                                DVT = drNPL["DVT_ID"].ToString();
                                DateTime dateTK = new DateTime(nam, thang, ngay);
                                //Các tờ khai xuất chuyển tiếp NPL
                                if (drNPL["SoTaiKhoan"].ToString().Contains("152") && drNPL["MaLoaiHinh"].ToString().Contains("X"))
                                {
                                    // duydp
                                    if (dateTK < NgayDauKy)
                                    {
                                        luongXTDauKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                        triGiaXTDauKy += Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                    }
                                    else if (dateTK <= NgayCuoiKy)
                                    {

                                        triGiaXTTrongKy += Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                        luongXTTrongKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                    }
                                    else
                                    {

                                    }


                                }
                                else if (drNPL["SoTaiKhoan"].ToString().Contains("155") && drNPL["MaLoaiHinh"].ToString().Contains("N"))
                                {
                                    try
                                    {
                                        if (dateTK < NgayDauKy)
                                        {
                                            luongNK_DauKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            triGiaNK_DauKy += Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            //luongXK_TrongKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            //triGiaXK_TrongKy += Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            drNPL["LuongNhap_DauKy"] = luongNK_DauKy;
                                            drNPL["TriGiaNhap_DauKy"] = triGiaNK_DauKy;
                                        }
                                        else if (dateTK <= NgayCuoiKy)
                                        {
                                            luongNK_TrongKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            triGiaNK_TrongKy += Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            //luongXK_TrongKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            //triGiaXK_TrongKy += Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            drNPL["LuongNhap_TrongKy"] = luongNK_TrongKy;
                                            drNPL["TriGiaNhap_TrongKy"] = triGiaNK_TrongKy;
                                            luongTonDau += 0;
                                            triGiaTonDau += 0;
                                            luongTon += 0;
                                            triGiaTon += 0;
                                        }
                                        else
                                        {

                                        }


                                    }
                                    catch (Exception ex)
                                    {

                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                    }
                                }
                                else if (drNPL["SoTaiKhoan"].ToString().Contains("155") && drNPL["MaLoaiHinh"].ToString().Contains("X"))
                                {
                                    try
                                    {
                                        if (dateTK < NgayDauKy)
                                        {
                                            luongXK_DauKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            triGiaXK_DauKy += Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                        }
                                        else if (dateTK <= NgayCuoiKy)
                                        {
                                            luongXK_TrongKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            triGiaXK_TrongKy += Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            luongTonDau += 0;
                                            triGiaTonDau += 0;
                                            luongTon += 0;
                                            triGiaTon += 0;
                                        }
                                        else
                                        {
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                    }
                                }
                                //+ vào lượng NPL hủy
                                else if (drNPL["SoTaiKhoan"].ToString().Contains("152") && drNPL["MaLoaiHinh"].ToString().Contains("NLHUY"))
                                {
                                    //Nếu hủy trong kỳ thì mới cộng
                                    if (dateTK <= NgayDauKy)
                                    {
                                        //luongNPLHuy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                    }
                                    else if (dateTK <= NgayDauKy)
                                    {
                                        luongNPLHuy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                    }
                                    else
                                    {
                                    }
                                }
                                //Cung ứng ko + trị giá
                                else if (drNPL["SoTaiKhoan"].ToString().Contains("152") && drNPL["MaLoaiHinh"].ToString().Contains("NPLCU"))
                                {
                                    // duydp
                                    if (dateTK < NgayDauKy)
                                    {

                                        try
                                        {
                                            luongCUDauKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                        }
                                        catch (Exception ex)
                                        {

                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                    else if (dateTK <= NgayCuoiKy)
                                    {
                                        luongCUTrongKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                    }
                                }
                                else
                                {
                                    //Tính lượng NPL xuất các kỳ
                                    if (dateTK < NgayDauKy)
                                    {

                                        try
                                        {
                                            luongNK_DauKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            triGiaNK_DauKy += Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            if (luongXK_DauKyTong >= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString()))
                                            {
                                                luongXK_DauKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                                luongXK_DauKyTong -= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());

                                                //luongXK_DauKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                                triGiaXK_DauKy += Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                                luongTonDau += Convert.ToDecimal(drNPL["LuongTon_TK"].ToString());
                                                triGiaTonDau += Convert.ToDecimal(drNPL["TonTriGia"].ToString());
                                            }
                                            else if (luongXK_DauKyTong == 0 && luongXK_TrongKyTong != 0)
                                            {
                                                //test

                                                luongXK_DauKy += 0;
                                                triGiaXK_DauKy += 0;
                                                //+ qua xuất trong kỳ
                                                luongXK_TrongKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                                luongXK_TrongKyTong -= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                                //luongXK_DauKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                                triGiaXK_TrongKy += Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());

                                                luongTonDau += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                                triGiaTonDau += Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            }

                                        }
                                        catch (Exception ex)
                                        {

                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                    // duydp
                                    else if (dateTK <= NgayCuoiKy)
                                    {
                                        if (drNPL["Ma"].ToString() == "DCS")
                                        {

                                        }
                                        luongNK_TrongKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                        triGiaNK_TrongKy += Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                        if (luongXK_TrongKyTong >= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString()))
                                        {
                                            luongXK_TrongKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                            luongXK_TrongKyTong -= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());

                                            //luongXK_DauKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                            triGiaXK_TrongKy += Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                            luongTon += Convert.ToDecimal(drNPL["LuongTon_TK"].ToString());
                                            triGiaTon += Convert.ToDecimal(drNPL["TonTriGia"].ToString());
                                        }
                                        else
                                        {
                                            //minhnd kiểm tra lại
                                            decimal c = Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                            luongXK_TrongKy += c;
                                            decimal a = Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                            triGiaXK_TrongKy += a;

                                            luongTon += 0;
                                            triGiaTon += 0;
                                        }

                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }

                        }
                        else
                            continue;
                    }
                    if (dr["Ma"].ToString().Contains("19"))
                    {

                    }
                    DataRow drF = dtTable.NewRow();
                    if (isDaQuyetToan)
                        drF["ID"] = stt;
                    else
                        drF["STT"] = stt;
                    drF["HopDong_ID"] = dr["HopDong_ID"].ToString();
                    drF["SoHopDong"] = dr["SoHopDong"].ToString();
                    drF["SoTaiKhoan"] = dr["SoTaiKhoan"].ToString();
                    drF["Ma"] = dr["Ma"].ToString();
                    drF["Ten"] = dr["Ten"].ToString();
                    drF["DVT_ID"] = DVT;
                    drF["SoLuongDaNhap"] = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                    drF["SoLuongCungUng"] = Convert.ToDecimal(dr["SoLuongCungUng"].ToString());
                    drF["SoLuongDaDung"] = Convert.ToDecimal(dr["SoLuongDaDung"].ToString());
                    drF["LuongTon"] = Convert.ToDecimal(dr["LuongTon"].ToString());
                    drF["SoToKhai"] = 0;
                    drF["SoToKhaiVnacc"] = 0;
                    drF["MaLoaiHinh"] = "";
                    drF["SoLuong"] = 0;
                    drF["LuongSD_TK"] = 0;
                    drF["LuongTon_TK"] = 0;

                    decimal _luongNhapDauKy = 0;
                    _luongNhapDauKy = luongNK_DauKy + luongCUDauKy;
                    drF["LuongNhap_DauKy"] = _luongNhapDauKy;
                    decimal _luongNhapTrongKy = 0;
                    _luongNhapTrongKy = luongNK_TrongKy + luongCUTrongKy;
                    drF["LuongNhap_TrongKy"] = _luongNhapTrongKy;

                    decimal _luongXuatDauKy = 0;
                    _luongXuatDauKy = luongxuatDauCopy + luongXTDauKy;
                    decimal _luongXuatTrongKy = 0;
                    // lượng hủy + vào xuất trong kỳ
                    _luongXuatTrongKy = luongxuatTrongCopy + luongXTTrongKy + luongNPLHuy;
                    if (dr["SoTaiKhoan"].ToString() == "155")
                    {
                        drF["LuongXuat_DauKy"] = luongXK_DauKy;
                        drF["LuongXuat_TrongKy"] = luongXK_TrongKy;
                    }
                    else
                    {

                        drF["LuongXuat_DauKy"] = _luongXuatDauKy;
                        //+ lương hủy vào xuất trong kỳ

                        drF["LuongXuat_TrongKy"] = _luongXuatTrongKy;
                    }

                    //Tính lại lượng tồn
                    decimal _luongTonDau = 0;
                    _luongTonDau = _luongNhapDauKy - _luongXuatDauKy;
                    decimal _luongTonCuoi = 0;
                    _luongTonCuoi = (_luongTonDau + _luongNhapTrongKy) - (_luongXuatTrongKy);
                    if (dr["SoTaiKhoan"].ToString() == "155")
                    {
                        drF["LuongTonCuoi"] = 0;
                        drF["LuongTonDau"] = 0;
                        drF["LuongXuat_TrongKy"] = luongXK_TrongKy;
                        drF["LuongNhap_TrongKy"] = luongXK_TrongKy;

                    }
                    else
                    {

                        drF["LuongTonCuoi"] = _luongTonCuoi;
                        drF["LuongTonDau"] = _luongTonDau;
                    }
                    //Tính lại lượng tồn
                    decimal _triGiaXuatDauKy = 0;
                    _triGiaXuatDauKy = triGiaXK_DauKy + triGiaXTDauKy;
                    decimal _triGiaTonDauKy = 0;
                    //bỏ + trị giá xuất chuyển tiếp
                    _triGiaTonDauKy = triGiaNK_DauKy - (triGiaXK_DauKy + triGiaXTDauKy);
                    if (dr["SoTaiKhoan"].ToString() == "155")
                    {
                        drF["TriGiaTonDau"] = 0;
                        drF["TriGiaNhap_DauKy"] = triGiaNK_DauKy;
                        drF["TriGiaNhap_TrongKy"] = triGiaXK_TrongKy;
                        drF["TriGiaXuat_TrongKy"] = triGiaXK_TrongKy;
                        drF["TriGiaXuat_DauKy"] = triGiaNK_DauKy;
                        drF["TriGiaTonCuoi"] = 0;
                    }
                    else
                    {
                        drF["TriGiaNhap_DauKy"] = triGiaNK_DauKy;
                        drF["TriGiaXuat_DauKy"] = _triGiaXuatDauKy;
                        drF["TriGiaTonDau"] = _triGiaTonDauKy;

                        drF["TriGiaNhap_TrongKy"] = triGiaNK_TrongKy;
                        //Nếu có lượng hủy thì hủy luôn trị giá. trị giá đưa vào trị giá xuất trong kỳ
                        decimal _triGiaXuatTrongKy = 0;
                        //Bỏ + trị giá xuất chuyển tiếp trong kỳ
                        _triGiaXuatTrongKy = (triGiaXK_TrongKy + triGiaXTTrongKy);
                        decimal _triGiaTonCuoiKy = 0;
                        _triGiaTonCuoiKy = (_triGiaTonDauKy + triGiaNK_TrongKy) - _triGiaXuatTrongKy;
                        if (Math.Round(Convert.ToDecimal(drF["LuongTonCuoi"].ToString()), 2) == 0)
                        {
                            drF["TriGiaTonCuoi"] = 0;
                            drF["TriGiaXuat_TrongKy"] = _triGiaXuatTrongKy + _triGiaTonCuoiKy;//(triGiaXK_TrongKy + triGiaXTTrongKy) + ((triGiaNK_DauKy - (triGiaXK_DauKy + triGiaXTDauKy)) + triGiaNK_TrongKy) - (triGiaXK_TrongKy + triGiaXTTrongKy);
                        }
                        else
                        {
                            drF["TriGiaTonCuoi"] = _triGiaTonCuoiKy;//((triGiaNK_DauKy - (triGiaXK_DauKy + triGiaXTDauKy)) + triGiaNK_TrongKy) - (triGiaXK_TrongKy);
                            drF["TriGiaXuat_TrongKy"] = _triGiaXuatTrongKy;
                        }

                        //+ vào xuất trong kỳ

                    }

                    stt++;
                    dtTable.Rows.Add(drF);

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //throw;
                }

            }
            #endregion Xử lý tổng

            dt = dtTable.DefaultView.ToTable(true, "SoTaiKhoan", "Ten", "Ma", "DVT_ID", "LuongTonDau", "LuongNhap_TrongKy", "LuongXuat_TrongKy", "LuongTonCuoi", "TriGiaTonDau", "TriGiaNhap_TrongKy", "TriGiaXuat_TrongKy", "TriGiaTonCuoi");
            dtTotal = dt;
            GetDataGoodItem();
            BindDataGoodItem();

        }
        private void toolStripGridDM_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "NPLTheoDM_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgDinhMuc_SanPham;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void btnMau15_TongLuong_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Mau15_TT38_Tong xnt = new Mau15_TT38_Tong();
                xnt.HD = this.HD;
                xnt.NgayDauKy = dtpNgayDauKy.Value;
                xnt.NgayCuoiKy = dtpNgayCuoiKy.Value;
                xnt.isLuong = true;
                xnt.tb_Mau15 = tb;
                if (isDaQuyetToan)
                    xnt.BindReport(dt, true);
                else
                    xnt.BindReport(dt, false);
                xnt.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnMau15_Luong_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Mau15_TT38_ChiTiet xnt = new Mau15_TT38_ChiTiet();
                //DataTable temp = tb.DefaultView.ToTable(true, "SoHopDong", "Ma", "SoLuongDaNhap", "SoLuongCungUng", "SoLuongDaDung", "LuongTon");
                //backgroundWorker1.RunWorkerAsync();
                xnt.NgayDauKy = dtpNgayDauKy.Value;
                xnt.NgayCuoiKy = dtpNgayCuoiKy.Value;
                xnt.isLuong = true;
                Mau15_TT38_ChiTiet.tb_Mau15Chuan = tb;
                if (isDaQuyetToan)
                    xnt.BindReport(tbNPL_SP_DM, true);
                else
                    xnt.BindReport(tbNPL_SP_DM, false);
                xnt.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void saveNPLQuyetToan()
        {
            try
            {
                foreach (DataRow dr in tbNPL_SP_DM.Rows)
                {
                    NPLQuyetToan npl = new NPLQuyetToan();
                    npl.HopDong_ID = int.Parse(dr["HopDong_ID"].ToString());
                    npl.SoHopDong = dr["SoHopDong"].ToString();
                    npl.SoToKhai = Convert.ToDecimal(dr["SoToKhai"].ToString());
                    npl.MaLoaiHinh = dr["MaLoaiHinh"].ToString();
                    npl.NgayDangKy = Convert.ToDateTime(dr["NgayDangKy"].ToString());
                    npl.MaHang = dr["MaHang"].ToString();
                    npl.TenHang = dr["TenHang"].ToString();
                    npl.SoLuong = Convert.ToDecimal(dr["SoLuong"].ToString());
                    npl.MaNguyenPhuLieu = dr["MaNguyenPhuLieu"].ToString();
                    npl.DinhMucSuDung = Convert.ToDecimal(dr["DinhMucSuDung"].ToString());
                    npl.TyLeHaoHut = Convert.ToDecimal(dr["TyLeHaoHut"].ToString());
                    npl.LuongNPLChuaHH = Convert.ToDecimal(dr["LuongNPLChuaHH"].ToString());
                    npl.LuongNPLCoHH = Convert.ToDecimal(dr["LuongNPLCoHH"].ToString());
                    npl.Insert();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void btnDone_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == false)
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    try
                    {
                        HopDong.DeleteQuyetToan(ListID_HopDong);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    try
                    {
                        HopDong.ResetQuyetToan(ListID_HopDong);
                    }
                    catch (Exception ex)
                    {

                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                    try
                    {
                        foreach (DataRow dr in tb.Rows)
                        {
                            t_GC_QuyetToan_Mau15 qt = new t_GC_QuyetToan_Mau15();
                            qt.SoTaiKhoan = int.Parse(dr["SoTaiKhoan"].ToString());
                            qt.HopDong_ID = int.Parse(dr["HopDong_ID"].ToString());
                            qt.SoHopDong = dr["SoHopDong"].ToString();
                            qt.SoToKhai = int.Parse(dr["SoToKhai"].ToString());
                            qt.SoToKhaiVnacc = Convert.ToDecimal(dr["SoToKhaiVnacc"].ToString());
                            try
                            {
                                qt.NgayDangKy = Convert.ToDateTime(dr["NgayDangKy"].ToString());
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                            qt.MaLoaiHinh = dr["MaLoaiHinh"].ToString();
                            qt.Ma = dr["Ma"].ToString();
                            qt.Ten = dr["Ten"].ToString();
                            qt.MaHS = dr["MaHS"].ToString();
                            qt.DVT_ID = dr["DVT_ID"].ToString();
                            qt.SoLuongDangKy = Convert.ToDecimal(dr["SoLuongDangKy"].ToString());
                            qt.SoLuongDaNhap = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                            qt.SoLuongDaDung = Convert.ToDecimal(dr["SoLuongDaDung"].ToString());
                            qt.SoLuongCungUng = Convert.ToDecimal(dr["SoLuongCungUng"].ToString());
                            qt.LuongTon = Convert.ToDecimal(dr["LuongTon"].ToString());
                            qt.SoLuong = Convert.ToDecimal(dr["SoLuong"].ToString());
                            qt.LuongSD_TK = Convert.ToDecimal(dr["LuongSD_TK"].ToString());
                            qt.LuongTon_TK = Convert.ToDecimal(dr["LuongTon_TK"].ToString());
                            qt.DonGiaKB = Convert.ToDecimal(dr["DonGiaKB"].ToString());
                            qt.DonGiaTT = Convert.ToDecimal(dr["DonGiaTT"].ToString());
                            qt.TriGiaTT = Convert.ToDecimal(dr["TriGiaTT"].ToString());
                            qt.TriGiaSuDung = Convert.ToDecimal(dr["TriGiaSuDung"].ToString());
                            qt.TonTriGia = Convert.ToDecimal(dr["TonTriGia"].ToString());
                            try
                            {
                                qt.TyGiaTT = Convert.ToDecimal(dr["TyGiaTT"].ToString());
                            }
                            catch (Exception ex)
                            {
                                if (qt.DonGiaKB != 0)
                                {
                                    qt.TyGiaTT = qt.DonGiaTT / qt.DonGiaKB;
                                }
                                else
                                {
                                    qt.TyGiaTT = 0;
                                }

                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }

                            qt.MaDVT = dr["MaDVT"].ToString();
                            qt.InsertUpdate();
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                    //Lưu NPL quyết toán
                    NPLQuyetToan.DeleteDynamic("HopDong_ID in (" + ListID_HopDong + ")");
                    saveNPLQuyetToan();
                    HopDong.UpdateQuyetToan(ListID_HopDong);
                    ShowMessage("Lưu thành công", false);     
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
            else
            {
                ShowMessage("Bạn chưa xử lý quyết toán", false);     
                //MessageBox.Show("Bạn chưa xử lý quyết toán", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
        public void BindDataHD()
        {
            try
            {
                if (isDaQuyetToan)
                {
                    DSHD = HoSoQuyetToan_DSHopDong.Load(goodItem.ID);
                    list_HopDong = HopDong.SelectCollectionDynamic("ID IN (" + DSHD.SoHopDong + ")", null);
                    ListID_HopDong = DSHD.SoHopDong;
                }
                dgListHD.Refresh();
                dgListHD.DataSource = list_HopDong;
                dgListHD.Refetch();

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void SaveGoodItem()
        {
            try
            {
                DSHD = new HoSoQuyetToan_DSHopDong();
                DSHD.Master_ID = Convert.ToInt32(goodItem.ID);
                DSHD.NgayKy = goodItem.NgayTN;
                DSHD.SoHopDong = ListID_HopDong;
                DSHD.InsertUpdate();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void loadListHD()
        {
            DataTable temp = new DataTable();
            try
            {
                temp = HoSoQuyetToan_DSHopDong.SelectDynamic("Master_ID = " + HD.ID, "ID").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            dgListHD.DataSource = temp;
            int i = 0;
            foreach (DataRow dr in temp.Rows)
            {
                i++;
                if (i != temp.Rows.Count)
                {
                    ListID_HopDong += "" + dr["HopDong_ID"].ToString() + ",";
                    ListHD += "'" + dr[1].ToString() + "',";
                }
                else
                {
                    ListID_HopDong += "" + dr["HopDong_ID"].ToString() + "";
                    ListHD += "'" + dr[1].ToString() + "'";
                }
            }

        }
        private void btnThemHD_Click(object sender, EventArgs e)
        {
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.isTaoHoSo = true;
            f.isQuyetToan = true;
            f.ShowDialog();
            if (f.list_HopDong.Count != 0)
            {
                list_HopDong = f.list_HopDong;
                foreach (HopDong hd in f.list_HopDong)
                {
                    if (ListID_HopDong!="0")
                    {
                        ListID_HopDong += "0," + hd.ID + ",";
                    }
                    else
                    {
                        ListID_HopDong += hd.ID + ",";
                    }
                }
                ListID_HopDong = ListID_HopDong.Substring(0, ListID_HopDong.Length - 1);
                //save();
                //foreach (HopDong hd in f.list_HopDong)
                //{
                //    DataTable temp = (DataTable)dgListHD.DataSource;
                //    foreach (DataRow dr in temp.Rows)
                //    {


                //        int id = int.Parse(dr["HopDong_ID"].ToString());
                //        if (id == hd.ID)
                //        {
                //            break;
                //        }
                //        else
                //        {
                //            try
                //            {
                //                HoSoQuyetToan_DSHopDong dshd = new HoSoQuyetToan_DSHopDong();
                //                dshd.HopDong_ID = hd.ID;
                //                dshd.Master_ID = Convert.ToInt32(hd.ID);
                //                dshd.NgayKy = hd.NgayKy;
                //                dshd.NgayHetHan = hd.NgayHetHan;
                //                dshd.SoHopDong = hd.SoHopDong;
                //                dshd.Insert();
                //            }
                //            catch (Exception ex)
                //            {
                //                Logger.LocalLogger.Instance().WriteMessage(ex);
                //                //throw;
                //            }

                //        }

                //    }
                //    if (dgListHD.GetRows().Length == 0)
                //    {
                //        try
                //        {
                //            HoSoQuyetToan_DSHopDong dshd = new HoSoQuyetToan_DSHopDong();
                //            dshd.HopDong_ID = hd.ID;
                //            dshd.Master_ID = Convert.ToInt32(hd.ID);
                //            dshd.NgayKy = hd.NgayKy;
                //            dshd.NgayHetHan = hd.NgayHetHan;
                //            dshd.SoHopDong = hd.SoHopDong;
                //            dshd.Insert();
                //        }
                //        catch (Exception ex)
                //        {
                //            Logger.LocalLogger.Instance().WriteMessage(ex);
                //            //throw;
                //        }
                //    }
                //}
                //loadListHD();
                BindDataHD();
                isDaQuyetToan = false;
                //isDaQuyetToan = true;
                btnGetXNTFromThanhKhoan.PerformClick();
            }
        }
        private void save()
        {
            //if (txtID.Text.Length > 0)
            //{
            //    //HS.ID = int.Parse(txtID.Text);
            //    //HS.SoHoSo = txtSoHoSo.Text;
            //    //HS.NgayTao = dtpNgayTao.Value;
            //    HS.NamQT = int.Parse(txtNamQT.Text);
            //    HS.GhiChu = txtGhiChu.Text;
            //    HS.Update();
            //}
            //else
            //{
            //    //HS.SoHoSo = txtSoHoSo.Text;
            //    //HS.NgayTao = dtpNgayTao.Value;
            //    HS.NamQT = int.Parse(txtNamQT.Text);
            //    HS.GhiChu = txtGhiChu.Text;
            //    int i = HS.Insert();
            //    HS = HoSoQuyetToan.Load(i);
            //    isUpdate = true;
            //}
            loadHS();
        }
        private void btnLuuHS_Click(object sender, EventArgs e)
        {
            save();
            MessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void toolStripMenuXuatEXHopDong_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "TỔNG HỢP " + uiTab1.SelectedTab.Text + "_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        if (uiTab1.SelectedTab.Name=="uiTabPage3")
                        {
                            gridEXExporter1.GridEX = dgListTotal;
                        }
                        if (uiTab1.SelectedTab.Name=="uiTabPage4")
                        {
                            gridEXExporter1.GridEX = dgListHD;                            
                        }
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void dgListHD_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                int id = int.Parse(dgListHD.CurrentRow.Cells["ID"].Text);
                HD= (HopDong)e.Row.DataRow;
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    //HoSoQuyetToan_DSHopDong.DeleteDynamic("ID = " + id);
                    ListID_HopDong.Replace(HD.ID.ToString(),"0");
                    list_HopDong.Remove(HD);
                    BindDataHD();
                    btnGetXNTFromThanhKhoan.PerformClick();
                    ShowMessage("Xóa thành công hợp đồng.", false);
                    //MessageBox.Show("Xóa thành công hợp đồng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                BindDataHD();
                btnGetXNTFromThanhKhoan.PerformClick();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }


        }

        private void dgListHD_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    string list_ID = "";
                    int i = 1;
                    foreach (GridEXSelectedItem item in dgListHD.SelectedItems)
                    {

                        GridEXRow dr = item.GetRow();
                        string id = dr.Cells["ID"].Text;
                        HD = (HopDong)item.GetRow().DataRow;
                        list_HopDong.Remove(HD);
                        ListID_HopDong.Replace(HD.ID.ToString(), "0");
                        if (i == dgListHD.SelectedItems.Count)
                        {
                            list_ID += id;
                        }
                        else
                        {
                            list_ID += id + ",";
                        }
                        i++;
                    }
                    BindDataHD();
                    btnGetXNTFromThanhKhoan.PerformClick();
                    //HoSoQuyetToan_DSHopDong.DeleteDynamic("ID in(" + list_ID + ")");
                    //MessageBox.Show("Xóa thành công hợp đồng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ShowMessage("Xóa thành công hợp đồng.", false);
                }
                BindDataHD();
                btnGetXNTFromThanhKhoan.PerformClick();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send("Send");
                    break;
                case "cmdSendEdit":
                    Send("Edit");
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                default:
                    break;
            }
        }
        public void Save()
        {
            if (String.IsNullOrEmpty(lblFileName.Text))
            {
                ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                return;
            }
            if (!ValidateForm(false))
                return;
            if (!IsReadExcel)
            {
                if (dtTotal.Rows.Count < 1)
                {
                    ShowMessage("Bạn chưa xử lý báo cáo ", false);
                    return;
                }
            }
            else
            {
                if (goodItem.GoodItemsCollection.Count < 1)
                {
                    ShowMessage("Không có số liệu báo cáo đọc từ file Excel . Vui lòng kiểm tra và đọc lại File excel ", false);
                    return;
                }
            }
            GetGoodItem();
            goodItem.InsertUpdateFull();
            SaveGoodItem();
            BindDataGoodItem();
            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            ShowMessage("Lưu thành công", false);
        }
        public void BindDataGoodItem()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = goodItem.GoodItemsCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void GetGoodItem()
        {
            goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            goodItem.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            goodItem.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
            goodItem.NamQuyetToan = Convert.ToDecimal(txtNamQT.Text);
            goodItem.LoaiHinhBaoCao = Convert.ToDecimal(cbbLoaiHinh.SelectedValue.ToString());
            goodItem.DVTBaoCao = Convert.ToDecimal(cbbDVT.SelectedValue.ToString());
            goodItem.GhiChuKhac = txtGhiChu.Text.ToString();
            if (!IsReadExcel)
            {
                GetDataGoodItem();
            }
        }
        public void SetGoodItem()
        {
            string TrangThai = goodItem.TrangThaiXuLy.ToString();
            switch (TrangThai)
            {
                case "0":
                    lblTrangThai.Text = "Chờ duyệt";
                    break;
                case "-1":
                    lblTrangThai.Text = "Chưa khai báo";
                    break;
                case "1":
                    lblTrangThai.Text = "Đã duyệt";
                    break;
                case "2":
                    lblTrangThai.Text = "Không phê duyệt";
                    break;

            }
            txtSoTiepNhan.Text = goodItem.SoTN.ToString();
            dateNgayTN.Value = goodItem.NgayTN;
            txtNamQT.Text = goodItem.NamQuyetToan.ToString();
            cbbDVT.SelectedValue = goodItem.DVTBaoCao.ToString();
            cbbLoaiHinh.SelectedValue = goodItem.LoaiHinhBaoCao.ToString();
            txtGhiChu.Text = goodItem.GhiChuKhac;
            lblFileName.Text = goodItem.FileName;
            goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectCollectionDynamic("GoodItem_ID = " + goodItem.ID, " ");
        }
        public void GetDataGoodItem()
        {
            goodItem.GoodItemsCollection.Clear();
            //Xử lý nguyên phụ liệu và sản phẩm không có dữ liệu Xuất - Nhập - Tồn 
           // Xóa dòng NPL và SP này để tránh gử báo cáo lên HQ bị lỗi .
            for (int i = dtTotal.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dtTotal.Rows[i];
                if (Convert.ToDecimal(dr["LuongTonDau"].ToString()) == 0 && Convert.ToDecimal(dr["LuongNhap_TrongKy"].ToString()) == 0 && Convert.ToDecimal(dr["LuongXuat_TrongKy"].ToString()) == 0 && Convert.ToDecimal(dr["LuongTonCuoi"]) == 0)
                    dr.Delete();
            }
            DataRow[] drNPLSP = new DataRow[dtTotal.Rows.Count];
            dtTotal.Rows.CopyTo(drNPLSP, 0);
            for (int i = 0; i < dtTotal.Rows.Count; i++)
            {
                goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
                goodItemDetail.STT = i + 1;
                if (drNPLSP[i]["SoTaiKhoan"].ToString() == "152")
                {
                    goodItemDetail.LoaiHangHoa = 1;
                }
                else
                {
                    goodItemDetail.LoaiHangHoa = 2;
                }
                goodItemDetail.TaiKhoan = 1;
                goodItemDetail.TenHangHoa = drNPLSP[i]["Ten"].ToString();
                goodItemDetail.MaHangHoa = drNPLSP[i]["Ma"].ToString();
                goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(drNPLSP[i]["DVT_ID"].ToString()));
                if (cbbDVT.SelectedValue.ToString() == "1")
                {
                    goodItemDetail.TonDauKy = Convert.ToDecimal(drNPLSP[i]["TriGiaTonDau"].ToString());
                    goodItemDetail.NhapTrongKy = Convert.ToDecimal(drNPLSP[i]["TriGiaNhap_TrongKy"].ToString());
                    goodItemDetail.XuatTrongKy = Convert.ToDecimal(drNPLSP[i]["TriGiaXuat_TrongKy"].ToString());
                    goodItemDetail.TonCuoiKy = Convert.ToDecimal(drNPLSP[i]["TriGiaTonCuoi"].ToString());
                }
                else
                {
                    goodItemDetail.TonDauKy = Convert.ToDecimal(drNPLSP[i]["LuongTonDau"].ToString());
                    goodItemDetail.NhapTrongKy = Convert.ToDecimal(drNPLSP[i]["LuongNhap_TrongKy"].ToString());
                    goodItemDetail.XuatTrongKy = Convert.ToDecimal(drNPLSP[i]["LuongXuat_TrongKy"].ToString());
                    goodItemDetail.TonCuoiKy = Convert.ToDecimal(drNPLSP[i]["LuongTonCuoi"].ToString());
                }
                goodItemDetail.GhiChu = "";//dr["GHICHU"].ToString();
                goodItem.GoodItemsCollection.Add(goodItemDetail);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtNamQT, errorProvider, "Năm quyết toán", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHinh, errorProvider, "Loại hình báo cáo", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbDVT, errorProvider, "Đơn vị tính báo cáo", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void Send(string Status)
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (goodItem.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    goodItem = KDT_VNACCS_BaoCaoQuyetToan_NPLSP.Load(goodItem.ID);
                    goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectCollectionDynamic("GoodItem_ID = " + goodItem.ID, " ");
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                sendXML.master_id = goodItem.ID;

                if (sendXML.Load())
                {
                    if (Status == "Send")
                    {
                        ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    goodItem.GuidStr = Guid.NewGuid().ToString();
                    GoodsItems_VNACCS goodsItem_VNACCS = new GoodsItems_VNACCS();
                    goodsItem_VNACCS = Mapper_V4.ToDataTransferGoodsItem(goodItem, goodItemDetail, GlobalSettings.DIA_CHI, GlobalSettings.TEN_DON_VI, Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = goodItem.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ)),
                              Identity = goodItem.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = goodsItem_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = goodItem.GuidStr,
                          },
                          goodsItem_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem);
                        cmdFeedback.Enabled = cmdFeedback2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                        sendXML.master_id = goodItem.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        goodItem.Update();
                        Feedback();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = goodItem.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.GoodsItem,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.GoodsItem,
                };
                subjectBase.Type = DeclarationIssuer.GoodsItem;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = goodItem.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ.Trim())),
                                                  Identity = goodItem.MaHQ
                                              }, subjectBase, null);
                if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ));
                    msgSend.To.Identity = goodItem.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
        }
        public void Result()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = goodItem.ID;
            form.DeclarationIssuer = DeclarationIssuer.GoodsItem;
            form.ShowDialog(this);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.GoodItemSendHandler(goodItem, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenFileDialog = new OpenFileDialog();

            OpenFileDialog.FileName = "";
            OpenFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                    + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                    + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

            OpenFileDialog.Multiselect = false;
            if (OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.FileInfo fin = new System.IO.FileInfo(OpenFileDialog.FileName);
                if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                  && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                  && fin.Extension.ToUpper() != ".gif".ToUpper()
                  && fin.Extension.ToUpper() != ".tiff".ToUpper()
                  && fin.Extension.ToUpper() != ".txt".ToUpper()
                  && fin.Extension.ToUpper() != ".xml".ToUpper()
                  && fin.Extension.ToUpper() != ".xsl".ToUpper()
                  && fin.Extension.ToUpper() != ".csv".ToUpper()
                  && fin.Extension.ToUpper() != ".doc".ToUpper()
                  && fin.Extension.ToUpper() != ".mdb".ToUpper()
                  && fin.Extension.ToUpper() != ".pdf".ToUpper()
                  && fin.Extension.ToUpper() != ".ppt".ToUpper()
                  && fin.Extension.ToUpper() != ".xls".ToUpper())
                {
                    ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                }
                else
                {
                    System.IO.FileStream fs = new System.IO.FileStream(OpenFileDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    size = 0;
                    size = fs.Length;
                    data = new byte[fs.Length];
                    fs.Read(data, 0, data.Length);
                    filebase64 = System.Convert.ToBase64String(data);
                    lblFileName.Text = fin.Name;
                    goodItem.FileName = fin.Name;
                    goodItem.Content = filebase64;
                }

            }
        }

        private void uiTab1_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            switch (uiTab1.SelectedTab.Name)
            {
                case "uiTabPage3": 
                    BindDataGoodItem();
                    break;
                case "uiTabPage4":
                    BindDataHD();
                    break;
                default:
                    break;
            }
        }

        private void ReadExcelFile_Click(object sender, EventArgs e)
        {
            goodItem.GoodItemsCollection.Clear();
            IsReadExcel = true;
            ReadExcelFormNPL f = new ReadExcelFormNPL();
            f.goodItem = goodItem;
            f.ShowDialog(this);
            BindDataGoodItem();
        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "BÁO CÁO QUYẾT TOÁN HỢP ĐỒNG_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgListTotal;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

    }
}