﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class BillOfLading
    {
        /// <summary>
        /// Số vận đơn
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày vận đơn
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Nước phát hành 
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Địa điểm chuyển tải / quá cảnh
        /// </summary>
        [XmlElement("transitLocation")]
        public string TransitLocation { get; set; }

        /// <summary>
        /// Loại vận đơn
        /// </summary>
        [XmlElement("category")]
        public string Category { get; set; }

        /// <summary>
        /// AttachedFile
        /// </summary>
        [XmlElement("AttachedFile")]
        public Company.KDT.SHARE.Components.AttachedFile AttachedFile { get; set; }
    }
}
