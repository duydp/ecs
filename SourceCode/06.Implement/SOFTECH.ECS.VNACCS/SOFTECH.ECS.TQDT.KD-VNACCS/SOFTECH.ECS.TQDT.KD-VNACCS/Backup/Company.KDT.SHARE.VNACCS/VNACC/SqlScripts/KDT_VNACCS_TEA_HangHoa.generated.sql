-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]
	@Master_ID bigint,
	@MoTaHangHoa nvarchar(200),
	@SoLuongDangKyMT numeric(19, 4),
	@DVTSoLuongDangKyMT varchar(4),
	@SoLuongDaSuDung numeric(19, 4),
	@DVTSoLuongDaSuDung varchar(4),
	@TriGia numeric(24, 4),
	@TriGiaDuKien numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TEA_HangHoa]
(
	[Master_ID],
	[MoTaHangHoa],
	[SoLuongDangKyMT],
	[DVTSoLuongDangKyMT],
	[SoLuongDaSuDung],
	[DVTSoLuongDaSuDung],
	[TriGia],
	[TriGiaDuKien],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@Master_ID,
	@MoTaHangHoa,
	@SoLuongDangKyMT,
	@DVTSoLuongDangKyMT,
	@SoLuongDaSuDung,
	@DVTSoLuongDaSuDung,
	@TriGia,
	@TriGiaDuKien,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]
	@ID bigint,
	@Master_ID bigint,
	@MoTaHangHoa nvarchar(200),
	@SoLuongDangKyMT numeric(19, 4),
	@DVTSoLuongDangKyMT varchar(4),
	@SoLuongDaSuDung numeric(19, 4),
	@DVTSoLuongDaSuDung varchar(4),
	@TriGia numeric(24, 4),
	@TriGiaDuKien numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TEA_HangHoa]
SET
	[Master_ID] = @Master_ID,
	[MoTaHangHoa] = @MoTaHangHoa,
	[SoLuongDangKyMT] = @SoLuongDangKyMT,
	[DVTSoLuongDangKyMT] = @DVTSoLuongDangKyMT,
	[SoLuongDaSuDung] = @SoLuongDaSuDung,
	[DVTSoLuongDaSuDung] = @DVTSoLuongDaSuDung,
	[TriGia] = @TriGia,
	[TriGiaDuKien] = @TriGiaDuKien,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MoTaHangHoa nvarchar(200),
	@SoLuongDangKyMT numeric(19, 4),
	@DVTSoLuongDangKyMT varchar(4),
	@SoLuongDaSuDung numeric(19, 4),
	@DVTSoLuongDaSuDung varchar(4),
	@TriGia numeric(24, 4),
	@TriGiaDuKien numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TEA_HangHoa] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TEA_HangHoa] 
		SET
			[Master_ID] = @Master_ID,
			[MoTaHangHoa] = @MoTaHangHoa,
			[SoLuongDangKyMT] = @SoLuongDangKyMT,
			[DVTSoLuongDangKyMT] = @DVTSoLuongDangKyMT,
			[SoLuongDaSuDung] = @SoLuongDaSuDung,
			[DVTSoLuongDaSuDung] = @DVTSoLuongDaSuDung,
			[TriGia] = @TriGia,
			[TriGiaDuKien] = @TriGiaDuKien,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TEA_HangHoa]
		(
			[Master_ID],
			[MoTaHangHoa],
			[SoLuongDangKyMT],
			[DVTSoLuongDangKyMT],
			[SoLuongDaSuDung],
			[DVTSoLuongDaSuDung],
			[TriGia],
			[TriGiaDuKien],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@Master_ID,
			@MoTaHangHoa,
			@SoLuongDangKyMT,
			@DVTSoLuongDangKyMT,
			@SoLuongDaSuDung,
			@DVTSoLuongDaSuDung,
			@TriGia,
			@TriGiaDuKien,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TEA_HangHoa]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TEA_HangHoa] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MoTaHangHoa],
	[SoLuongDangKyMT],
	[DVTSoLuongDangKyMT],
	[SoLuongDaSuDung],
	[DVTSoLuongDaSuDung],
	[TriGia],
	[TriGiaDuKien],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEA_HangHoa]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MoTaHangHoa],
	[SoLuongDangKyMT],
	[DVTSoLuongDangKyMT],
	[SoLuongDaSuDung],
	[DVTSoLuongDaSuDung],
	[TriGia],
	[TriGiaDuKien],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TEA_HangHoa] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MoTaHangHoa],
	[SoLuongDangKyMT],
	[DVTSoLuongDangKyMT],
	[SoLuongDaSuDung],
	[DVTSoLuongDaSuDung],
	[TriGia],
	[TriGiaDuKien],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEA_HangHoa]	

GO

