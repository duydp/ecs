DELETE FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A'
-------------------------------------BẢNG MÃ ĐỘI CHI CỤC HẢI QUAN DÙNG TRONG VNACCS-------------------------------------------------
IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '50BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Tịnh Biên' WHERE TableID='A014A' AND CustomsCode = '50BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BB','00',N'Chi cục HQ CK Tịnh Biên','I')
	INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BB','00',N'Chi cục HQ CK Tịnh Biên','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '50BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Vĩnh Hội Đông' WHERE TableID='A014A' AND CustomsCode = '50BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BC','00',N'Chi cục HQ Vĩnh Hội Đông','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BC','00',N'Chi cục HQ Vĩnh Hội Đông','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '50BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Vĩnh Xương' WHERE TableID='A014A' AND CustomsCode = '50BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BD','00',N'Chi cục HQ CK Vĩnh Xương','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BD','00',N'Chi cục HQ CK Vĩnh Xương','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '50BJ' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Bắc Đai' WHERE TableID='A014A' AND CustomsCode = '50BJ' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BJ','00',N'Chi cục HQ Bắc Đai','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BJ','00',N'Chi cục HQ Bắc Đai','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '50BK' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Khánh Bình' WHERE TableID='A014A' AND CustomsCode = '50BK' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BK','00',N'Chi cục HQ Khánh Bình','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50BK','00',N'Chi cục HQ Khánh Bình','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '50CE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Cảng Mỹ Thới' WHERE TableID='A014A' AND CustomsCode = '50CE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50CE','00',N'Chi cục HQ Cảng Mỹ Thới','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','50CE','00',N'Chi cục HQ Cảng Mỹ Thới','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '51BE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Cảng Cát Lở' WHERE TableID='A014A' AND CustomsCode = '51BE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51BE','00',N'Chi cục HQ Cảng Cát Lở','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51BE','00',N'Chi cục HQ Cảng Cát Lở','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '51C1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK Kho ngoại quan' WHERE TableID='A014A' AND CustomsCode = '51C1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51C1','00',N'Đội Thủ tục hàng hóa XNK Kho ngoại quan','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51C1','00',N'Đội Thủ tục hàng hóa XNK Kho ngoại quan','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '51C2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục SP-PSA' WHERE TableID='A014A' AND CustomsCode = '51C2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51C2','00',N'Đội Thủ tục SP-PSA','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51C2','00',N'Đội Thủ tục SP-PSA','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '51CB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu' WHERE TableID='A014A' AND CustomsCode = '51CB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51CB','00',N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51CB','00',N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '51CH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Côn Đảo' WHERE TableID='A014A' AND CustomsCode = '51CH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51CH','00',N'Chi cục HQ Côn Đảo','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51CH','00',N'Chi cục HQ Côn Đảo','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '51CI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK cảng Cái Mép' WHERE TableID='A014A' AND CustomsCode = '51CI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51CI','00',N'Chi cục HQ CK cảng Cái Mép','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','51CI','00',N'Chi cục HQ CK cảng Cái Mép','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '18A1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ quản lý KCN Yên Phong' WHERE TableID='A014A' AND CustomsCode = '18A1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18A1','00',N'Đội TTHQ quản lý KCN Yên Phong','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18A1','00',N'Đội TTHQ quản lý KCN Yên Phong','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '18A2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ quản lý KCN Quế Võ' WHERE TableID='A014A' AND CustomsCode = '18A2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18A2','00',N'Đội TTHQ quản lý KCN Quế Võ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18A2','00',N'Đội TTHQ quản lý KCN Quế Võ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '18A3' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '18A3' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18A3','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18A3','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '18B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '18B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18B1','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18B1','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '18B2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục KCN Yên Bình' WHERE TableID='A014A' AND CustomsCode = '18B2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18B2','00',N'Đội Thủ tục KCN Yên Bình','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18B2','00',N'Đội Thủ tục KCN Yên Bình','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '18BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý các KCN Bắc Giang' WHERE TableID='A014A' AND CustomsCode = '18BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18BC','00',N'Chi cục HQ Quản lý các KCN Bắc Giang','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18BC','00',N'Chi cục HQ Quản lý các KCN Bắc Giang','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '18ID' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Cảng nội địa Tiên Sơn' WHERE TableID='A014A' AND CustomsCode = '18ID' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18ID','00',N'Chi cục HQ Cảng nội địa Tiên Sơn','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','18ID','00',N'Chi cục HQ Cảng nội địa Tiên Sơn','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '37CB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Qui Nhơn' WHERE TableID='A014A' AND CustomsCode = '37CB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','37CB','00',N'Chi cục HQ CK Cảng Qui Nhơn','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','37CB','00',N'Chi cục HQ CK Cảng Qui Nhơn','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '37TC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Phú Yên' WHERE TableID='A014A' AND CustomsCode = '37TC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','37TC','00',N'Chi cục HQ Phú Yên','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','37TC','00',N'Chi cục HQ Phú Yên','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '43CN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng tổng hợp Bình Dương' WHERE TableID='A014A' AND CustomsCode = '43CN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43CN','00',N'Chi cục HQ CK Cảng tổng hợp Bình Dương','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43CN','00',N'Chi cục HQ CK Cảng tổng hợp Bình Dương','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '43IH' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Sóng Thần' WHERE TableID='A014A' AND CustomsCode = '43IH' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43IH','02',N'Chi cục HQ Sóng Thần','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43IH','02',N'Chi cục HQ Sóng Thần','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '43K1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ - HQ Mỹ Phước' WHERE TableID='A014A' AND CustomsCode = '43K1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43K1','00',N'Đội Nghiệp vụ - HQ Mỹ Phước','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43K1','00',N'Đội Nghiệp vụ - HQ Mỹ Phước','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '43K2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TT Khu liên hợp - HQ Mỹ Phước' WHERE TableID='A014A' AND CustomsCode = '43K2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43K2','00',N'Đội TT Khu liên hợp - HQ Mỹ Phước','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43K2','00',N'Đội TT Khu liên hợp - HQ Mỹ Phước','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '43K3' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TT Tân Định - HQ Mỹ Phước' WHERE TableID='A014A' AND CustomsCode = '43K3' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43K3','00',N'Đội TT Tân Định - HQ Mỹ Phước','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43K3','00',N'Đội TT Tân Định - HQ Mỹ Phước','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '43ND' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Sóng Thần' WHERE TableID='A014A' AND CustomsCode = '43ND' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43ND','00',N'Chi cục HQ KCN Sóng Thần','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43ND','00',N'Chi cục HQ KCN Sóng Thần','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '43NF' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Việt Nam - Singapore' WHERE TableID='A014A' AND CustomsCode = '43NF' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43NF','00',N'Chi cục HQ KCN Việt Nam - Singapore','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43NF','00',N'Chi cục HQ KCN Việt Nam - Singapore','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '43NG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Việt Hương' WHERE TableID='A014A' AND CustomsCode = '43NG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43NG','00',N'Chi cục HQ KCN Việt Hương','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43NG','00',N'Chi cục HQ KCN Việt Hương','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '43PB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN' WHERE TableID='A014A' AND CustomsCode = '43PB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43PB','00',N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','43PB','00',N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '61BA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Tổng hợp' WHERE TableID='A014A' AND CustomsCode = '61BA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61BA','00',N'Đội Nghiệp vụ Tổng hợp','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61BA','00',N'Đội Nghiệp vụ Tổng hợp','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '61BA' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ CK Tà Vát' WHERE TableID='A014A' AND CustomsCode = '61BA' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61BA','01',N'Đội TTHQ CK Tà Vát','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61BA','01',N'Đội TTHQ CK Tà Vát','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '61PA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '61PA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61PA','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61PA','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '61PA' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ 2' WHERE TableID='A014A' AND CustomsCode = '61PA' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61PA','01',N'Đội Nghiệp vụ 2','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61PA','01',N'Đội Nghiệp vụ 2','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '61BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Tổng hợp' WHERE TableID='A014A' AND CustomsCode = '61BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61BB','00',N'Đội Nghiệp vụ Tổng hợp','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61BB','00',N'Đội Nghiệp vụ Tổng hợp','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '61BB' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiêp vụ CK Tân Tiến' WHERE TableID='A014A' AND CustomsCode = '61BB' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61BB','01',N'Đội Nghiêp vụ CK Tân Tiến','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','61BB','01',N'Đội Nghiêp vụ CK Tân Tiến','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '59BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Hòa Trung' WHERE TableID='A014A' AND CustomsCode = '59BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','59BD','00',N'Chi cục HQ Hòa Trung','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','59BD','00',N'Chi cục HQ Hòa Trung','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '59CB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Năm Căn' WHERE TableID='A014A' AND CustomsCode = '59CB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','59CB','00',N'Chi cục HQ CK Cảng Năm Căn','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','59CB','00',N'Chi cục HQ CK Cảng Năm Căn','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '54CB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cần Thơ' WHERE TableID='A014A' AND CustomsCode = '54CB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','54CB','00',N'Chi cục HQ CK Cảng Cần Thơ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','54CB','00',N'Chi cục HQ CK Cảng Cần Thơ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '54CD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Vĩnh Long' WHERE TableID='A014A' AND CustomsCode = '54CD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','54CD','00',N'Chi cục HQ CK Vĩnh Long','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','54CD','00',N'Chi cục HQ CK Vĩnh Long','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '54PH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Tây Đô' WHERE TableID='A014A' AND CustomsCode = '54PH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','54PH','00',N'Chi cục HQ Tây Đô','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','54PH','00',N'Chi cục HQ Tây Đô','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '54PK' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Sóc Trăng' WHERE TableID='A014A' AND CustomsCode = '54PK' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','54PK','00',N'Chi cục HQ Sóc Trăng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','54PK','00',N'Chi cục HQ Sóc Trăng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '11B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Tà Lùng' WHERE TableID='A014A' AND CustomsCode = '11B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11B1','00',N'Chi cục HQ CK Tà Lùng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11B1','00',N'Chi cục HQ CK Tà Lùng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '11B2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội NV số 2 Nà Lạn' WHERE TableID='A014A' AND CustomsCode = '11B2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11B2','00',N'Đội NV số 2 Nà Lạn','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11B2','00',N'Đội NV số 2 Nà Lạn','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '11BE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Trà Lĩnh' WHERE TableID='A014A' AND CustomsCode = '11BE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11BE','00',N'Chi cục HQ CK Trà Lĩnh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11BE','00',N'Chi cục HQ CK Trà Lĩnh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '11BF' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Sóc Giang' WHERE TableID='A014A' AND CustomsCode = '11BF' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11BF','00',N'Chi cục HQ CK Sóc Giang','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11BF','00',N'Chi cục HQ CK Sóc Giang','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '11BH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Pò Peo' WHERE TableID='A014A' AND CustomsCode = '11BH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11BH','00',N'Chi cục HQ CK Pò Peo','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11BH','00',N'Chi cục HQ CK Pò Peo','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '11G1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Bí Hà' WHERE TableID='A014A' AND CustomsCode = '11G1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11G1','00',N'Chi cục HQ CK Bí Hà','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11G1','00',N'Chi cục HQ CK Bí Hà','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '11G2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội NV Lý Vạn' WHERE TableID='A014A' AND CustomsCode = '11G2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11G2','00',N'Đội NV Lý Vạn','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11G2','00',N'Đội NV Lý Vạn','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '11PK' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Bắc Kạn' WHERE TableID='A014A' AND CustomsCode = '11PK' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11PK','00',N'Chi cục HQ Bắc Kạn','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','11PK','00',N'Chi cục HQ Bắc Kạn','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '34AB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng' WHERE TableID='A014A' AND CustomsCode = '34AB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34AB','00',N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34AB','00',N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '34AB' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Bưu phẩm bưu kiện' WHERE TableID='A014A' AND CustomsCode = '34AB' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34AB','01',N'Đội Bưu phẩm bưu kiện','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34AB','01',N'Đội Bưu phẩm bưu kiện','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '34CC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A014A' AND CustomsCode = '34CC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34CC','00',N'Chi cục HQ Quản lý hàng đầu tư - gia công','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34CC','00',N'Chi cục HQ Quản lý hàng đầu tư - gia công','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '34CE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Đà Nẵng' WHERE TableID='A014A' AND CustomsCode = '34CE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34CE','00',N'Chi cục HQ CK Cảng Đà Nẵng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34CE','00',N'Chi cục HQ CK Cảng Đà Nẵng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '34NG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu' WHERE TableID='A014A' AND CustomsCode = '34NG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34NG','00',N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34NG','00',N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '34NH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Đà Nẵng' WHERE TableID='A014A' AND CustomsCode = '34NH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34NH','00',N'Chi cục HQ KCN Đà Nẵng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','34NH','00',N'Chi cục HQ KCN Đà Nẵng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '40B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK BupRăng' WHERE TableID='A014A' AND CustomsCode = '40B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','40B1','00',N'Chi cục HQ CK BupRăng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','40B1','00',N'Chi cục HQ CK BupRăng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '40BC' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Buôn Mê Thuột' WHERE TableID='A014A' AND CustomsCode = '40BC' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','40BC','01',N'Chi cục HQ Buôn Mê Thuột','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','40BC','01',N'Chi cục HQ Buôn Mê Thuột','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '40D1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Đà Lạt' WHERE TableID='A014A' AND CustomsCode = '40D1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','40D1','00',N'Chi cục HQ Đà Lạt','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','40D1','00',N'Chi cục HQ Đà Lạt','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '12B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '12B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12B1','00',N'Đội nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12B1','00',N'Đội nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '12B2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục Huổi Puốc' WHERE TableID='A014A' AND CustomsCode = '12B2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12B2','00',N'Đội Thủ tục Huổi Puốc','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12B2','00',N'Đội Thủ tục Huổi Puốc','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '12BE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Lóng Sập' WHERE TableID='A014A' AND CustomsCode = '12BE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12BE','00',N'Chi cục HQ CK Lóng Sập','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12BE','00',N'Chi cục HQ CK Lóng Sập','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '12BI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Chiềng Khương' WHERE TableID='A014A' AND CustomsCode = '12BI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12BI','00',N'Chi cục HQ CK Chiềng Khương','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12BI','00',N'Chi cục HQ CK Chiềng Khương','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '12F1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'HQ Thị xã Sơn La' WHERE TableID='A014A' AND CustomsCode = '12F1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12F1','00',N'HQ Thị xã Sơn La','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12F1','00',N'HQ Thị xã Sơn La','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '12F2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ HQCK Nà Cài' WHERE TableID='A014A' AND CustomsCode = '12F2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12F2','00',N'Đội Nghiệp vụ HQCK Nà Cài','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12F2','00',N'Đội Nghiệp vụ HQCK Nà Cài','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '12H1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '12H1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12H1','00',N'Đội nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12H1','00',N'Đội nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '12H2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Hải quan Pô Tô' WHERE TableID='A014A' AND CustomsCode = '12H2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12H2','00',N'Đội Hải quan Pô Tô','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','12H2','00',N'Đội Hải quan Pô Tô','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47D1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '47D1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47D1','00',N'Đội nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47D1','00',N'Đội nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47D2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ 2' WHERE TableID='A014A' AND CustomsCode = '47D2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47D2','00',N'Đội nghiệp vụ 2','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47D2','00',N'Đội nghiệp vụ 2','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47D3' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ 3' WHERE TableID='A014A' AND CustomsCode = '47D3' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47D3','00',N'Đội nghiệp vụ 3','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47D3','00',N'Đội nghiệp vụ 3','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47I1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '47I1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47I1','00',N'Đội nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47I1','00',N'Đội nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47I2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ 2' WHERE TableID='A014A' AND CustomsCode = '47I2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47I2','00',N'Đội nghiệp vụ 2','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47I2','00',N'Đội nghiệp vụ 2','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47NB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục tàu' WHERE TableID='A014A' AND CustomsCode = '47NB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NB','00',N'Đội Thủ tục tàu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NB','00',N'Đội Thủ tục tàu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47NB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Biên Hoà' WHERE TableID='A014A' AND CustomsCode = '47NB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NB','00',N'Chi cục HQ Biên Hoà','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NB','00',N'Chi cục HQ Biên Hoà','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47NF' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Thống Nhất' WHERE TableID='A014A' AND CustomsCode = '47NF' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NF','00',N'Chi cục HQ Thống Nhất','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NF','00',N'Chi cục HQ Thống Nhất','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47NG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Nhơn Trạch' WHERE TableID='A014A' AND CustomsCode = '47NG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NG','00',N'Chi cục HQ Nhơn Trạch','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NG','00',N'Chi cục HQ Nhơn Trạch','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47NM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ QL KCN Bình Thuận' WHERE TableID='A014A' AND CustomsCode = '47NM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NM','00',N'Chi cục HQ QL KCN Bình Thuận','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47NM','00',N'Chi cục HQ QL KCN Bình Thuận','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '47XE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCX Long Bình' WHERE TableID='A014A' AND CustomsCode = '47XE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47XE','00',N'Chi cục HQ KCX Long Bình','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','47XE','00',N'Chi cục HQ KCX Long Bình','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '49BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Thường Phước' WHERE TableID='A014A' AND CustomsCode = '49BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49BB','00',N'Chi cục HQ CK Thường Phước','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49BB','00',N'Chi cục HQ CK Thường Phước','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '49BE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Sở Thượng' WHERE TableID='A014A' AND CustomsCode = '49BE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49BE','00',N'Chi cục HQ Sở Thượng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49BE','00',N'Chi cục HQ Sở Thượng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '49BF' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Thông Bình' WHERE TableID='A014A' AND CustomsCode = '49BF' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49BF','00',N'Chi cục HQ Thông Bình','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49BF','00',N'Chi cục HQ Thông Bình','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '49BG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Dinh Bà' WHERE TableID='A014A' AND CustomsCode = '49BG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49BG','00',N'Chi cục HQ CK Dinh Bà','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49BG','00',N'Chi cục HQ CK Dinh Bà','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '49C1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Đồng Tháp - KV Cao Lãnh' WHERE TableID='A014A' AND CustomsCode = '49C1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49C1','00',N'Chi cục HQ CK Cảng Đồng Tháp - KV Cao Lãnh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49C1','00',N'Chi cục HQ CK Cảng Đồng Tháp - KV Cao Lãnh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '49C2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Đồng Tháp - KV Sa Đéc' WHERE TableID='A014A' AND CustomsCode = '49C2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49C2','00',N'Chi cục HQ CK Cảng Đồng Tháp - KV Sa Đéc','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','49C2','00',N'Chi cục HQ CK Cảng Đồng Tháp - KV Sa Đéc','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '38B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ tổng hợp' WHERE TableID='A014A' AND CustomsCode = '38B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','38B1','00',N'Đội Nghiệp vụ tổng hợp','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','38B1','00',N'Đội Nghiệp vụ tổng hợp','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '38B2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục' WHERE TableID='A014A' AND CustomsCode = '38B2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','38B2','00',N'Đội Thủ tục','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','38B2','00',N'Đội Thủ tục','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '38BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Bờ Y' WHERE TableID='A014A' AND CustomsCode = '38BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','38BC','00',N'Chi cục HQ CK Bờ Y','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','38BC','00',N'Chi cục HQ CK Bờ Y','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '38PD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Kon Tum' WHERE TableID='A014A' AND CustomsCode = '38PD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','38PD','00',N'Chi cục HQ Kon Tum','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','38PD','00',N'Chi cục HQ Kon Tum','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '10BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Thanh Thủy' WHERE TableID='A014A' AND CustomsCode = '10BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BB','00',N'Chi cục HQ CK Thanh Thủy','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BB','00',N'Chi cục HQ CK Thanh Thủy','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '10BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Xín Mần' WHERE TableID='A014A' AND CustomsCode = '10BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BC','00',N'Chi cục HQ CK Xín Mần','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BC','00',N'Chi cục HQ CK Xín Mần','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '10BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Phó Bảng' WHERE TableID='A014A' AND CustomsCode = '10BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BD','00',N'Chi cục HQ CK Phó Bảng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BD','00',N'Chi cục HQ CK Phó Bảng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '10BF' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Săm Pun' WHERE TableID='A014A' AND CustomsCode = '10BF' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BF','00',N'Chi cục HQ CK Săm Pun','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BF','00',N'Chi cục HQ CK Săm Pun','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '10BI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Tuyên Quang' WHERE TableID='A014A' AND CustomsCode = '10BI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BI','00',N'Chi cục HQ Tuyên Quang','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','10BI','00',N'Chi cục HQ Tuyên Quang','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa xuất' WHERE TableID='A014A' AND CustomsCode = '01B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01B1','00',N'Đội Thủ tục hàng hóa xuất','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01B1','00',N'Đội Thủ tục hàng hóa xuất','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01B3' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa nhập' WHERE TableID='A014A' AND CustomsCode = '01B3' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01B3','00',N'Đội Thủ tục hàng hóa nhập','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01B3','00',N'Đội Thủ tục hàng hóa nhập','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01B6' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hành lý nhập khẩu' WHERE TableID='A014A' AND CustomsCode = '01B6' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01B6','00',N'Đội thủ tục hành lý nhập khẩu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01B6','00',N'Đội thủ tục hành lý nhập khẩu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01B5' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hành lý xuất khẩu' WHERE TableID='A014A' AND CustomsCode = '01B5' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01B5','00',N'Đội thủ tục hành lý xuất khẩu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01B5','00',N'Đội thủ tục hành lý xuất khẩu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01BT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Yên Bái' WHERE TableID='A014A' AND CustomsCode = '01BT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01BT','00',N'Chi cục HQ Yên Bái','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01BT','00',N'Chi cục HQ Yên Bái','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01E1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '01E1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01E1','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01E1','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01E2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Hàng không' WHERE TableID='A014A' AND CustomsCode = '01E2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01E2','00',N'Đội Hàng không','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01E2','00',N'Đội Hàng không','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01IK' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Gia Thụy' WHERE TableID='A014A' AND CustomsCode = '01IK' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01IK','00',N'Chi cục HQ Gia Thụy','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01IK','00',N'Chi cục HQ Gia Thụy','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01M1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ Hà Đông' WHERE TableID='A014A' AND CustomsCode = '01M1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01M1','00',N'Đội TTHQ Hà Đông','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01M1','00',N'Đội TTHQ Hà Đông','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01M2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ Khu CNC Hòa Lạc' WHERE TableID='A014A' AND CustomsCode = '01M2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01M2','00',N'Đội TTHQ Khu CNC Hòa Lạc','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01M2','00',N'Đội TTHQ Khu CNC Hòa Lạc','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01NV' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Bắc Thăng Long' WHERE TableID='A014A' AND CustomsCode = '01NV' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01NV','00',N'Chi cục HQ KCN Bắc Thăng Long','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01NV','00',N'Chi cục HQ KCN Bắc Thăng Long','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01PJ' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Phú Thọ' WHERE TableID='A014A' AND CustomsCode = '01PJ' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01PJ','00',N'Chi cục HQ Phú Thọ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01PJ','00',N'Chi cục HQ Phú Thọ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01PL' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A014A' AND CustomsCode = '01PL' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01PL','00',N'Chi cục HQ Quản lý hàng đầu tư - gia công','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01PL','00',N'Chi cục HQ Quản lý hàng đầu tư - gia công','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01PR' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Vĩnh Phúc' WHERE TableID='A014A' AND CustomsCode = '01PR' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01PR','00',N'Chi cục HQ Vĩnh Phúc','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01PR','00',N'Chi cục HQ Vĩnh Phúc','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01DD' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục XNK 1' WHERE TableID='A014A' AND CustomsCode = '01DD' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01DD','01',N'Đội thủ tục XNK 1','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01DD','01',N'Đội thủ tục XNK 1','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01DD' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục XNK 2' WHERE TableID='A014A' AND CustomsCode = '01DD' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01DD','02',N'Đội thủ tục XNK 2','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01DD','02',N'Đội thủ tục XNK 2','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01SI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Ga đường sắt quốc tế Yên Viên' WHERE TableID='A014A' AND CustomsCode = '01SI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01SI','00',N'Chi cục HQ Ga đường sắt quốc tế Yên Viên','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01SI','00',N'Chi cục HQ Ga đường sắt quốc tế Yên Viên','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '01PQ' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Hòa Bình' WHERE TableID='A014A' AND CustomsCode = '01PQ' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01PQ','00',N'Chi cục HQ Hòa Bình','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','01PQ','00',N'Chi cục HQ Hòa Bình','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '30BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc tế Cầu Treo' WHERE TableID='A014A' AND CustomsCode = '30BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30BB','00',N'Chi cục HQ CK Quốc tế Cầu Treo','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30BB','00',N'Chi cục HQ CK Quốc tế Cầu Treo','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '30BE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Hồng Lĩnh' WHERE TableID='A014A' AND CustomsCode = '30BE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30BE','00',N'Chi cục HQ Hồng Lĩnh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30BE','00',N'Chi cục HQ Hồng Lĩnh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '30BI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ khu kinh tế CK Cầu Treo' WHERE TableID='A014A' AND CustomsCode = '30BI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30BI','00',N'Chi cục HQ khu kinh tế CK Cầu Treo','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30BI','00',N'Chi cục HQ khu kinh tế CK Cầu Treo','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '30CC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Xuân Hải' WHERE TableID='A014A' AND CustomsCode = '30CC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30CC','00',N'Chi cục HQ CK Cảng Xuân Hải','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30CC','00',N'Chi cục HQ CK Cảng Xuân Hải','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '30F1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Vũng Áng' WHERE TableID='A014A' AND CustomsCode = '30F1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30F1','00',N'Chi cục HQ CK Cảng Vũng Áng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30F1','00',N'Chi cục HQ CK Cảng Vũng Áng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '30F2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ cảng Sơn Dương' WHERE TableID='A014A' AND CustomsCode = '30F2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30F2','00',N'Đội Nghiệp vụ cảng Sơn Dương','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','30F2','00',N'Đội Nghiệp vụ cảng Sơn Dương','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03CC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK' WHERE TableID='A014A' AND CustomsCode = '03CC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03CC','00',N'Đội Thủ tục hàng hóa XNK','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03CC','00',N'Đội Thủ tục hàng hóa XNK','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03CD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '03CD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03CD','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03CD','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03CE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK' WHERE TableID='A014A' AND CustomsCode = '03CE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03CE','00',N'Đội Thủ tục hàng hóa XNK','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03CE','00',N'Đội Thủ tục hàng hóa XNK','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03EE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK' WHERE TableID='A014A' AND CustomsCode = '03EE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03EE','00',N'Đội Thủ tục hàng hóa XNK','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03EE','00',N'Đội Thủ tục hàng hóa XNK','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03NK' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '03NK' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03NK','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03NK','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03PA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng gia công' WHERE TableID='A014A' AND CustomsCode = '03PA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03PA','00',N'Đội Thủ tục hàng gia công','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03PA','00',N'Đội Thủ tục hàng gia công','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03PA' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng đầu tư' WHERE TableID='A014A' AND CustomsCode = '03PA' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03PA','01',N'Đội Thủ tục hàng đầu tư','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03PA','01',N'Đội Thủ tục hàng đầu tư','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03PJ' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '03PJ' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03PJ','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03PJ','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03PL' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '03PL' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03PL','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03PL','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03TG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK' WHERE TableID='A014A' AND CustomsCode = '03TG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03TG','00',N'Đội Thủ tục hàng hóa XNK','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03TG','00',N'Đội Thủ tục hàng hóa XNK','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '03TG' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội GS tàu, kho bãi và KSHQ' WHERE TableID='A014A' AND CustomsCode = '03TG' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03TG','01',N'Đội GS tàu, kho bãi và KSHQ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','03TG','01',N'Đội GS tàu, kho bãi và KSHQ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '41BH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Ninh Thuận' WHERE TableID='A014A' AND CustomsCode = '41BH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41BH','00',N'Chi cục HQ Ninh Thuận','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41BH','00',N'Chi cục HQ Ninh Thuận','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '41CB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Nha Trang' WHERE TableID='A014A' AND CustomsCode = '41CB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41CB','00',N'Chi cục HQ CK Cảng Nha Trang','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41CB','00',N'Chi cục HQ CK Cảng Nha Trang','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '41CC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cam Ranh' WHERE TableID='A014A' AND CustomsCode = '41CC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41CC','00',N'Chi cục HQ CK Cảng Cam Ranh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41CC','00',N'Chi cục HQ CK Cảng Cam Ranh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '41AB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK sân bay quốc tế Cam Ranh' WHERE TableID='A014A' AND CustomsCode = '41AB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41AB','00',N'Chi cục HQ CK sân bay quốc tế Cam Ranh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41AB','00',N'Chi cục HQ CK sân bay quốc tế Cam Ranh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '41PE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Vân Phong' WHERE TableID='A014A' AND CustomsCode = '41PE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41PE','00',N'Chi cục HQ Vân Phong','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','41PE','00',N'Chi cục HQ Vân Phong','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '53BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc Tế Hà Tiên' WHERE TableID='A014A' AND CustomsCode = '53BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','53BC','00',N'Chi cục HQ CK Quốc Tế Hà Tiên','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','53BC','00',N'Chi cục HQ CK Quốc Tế Hà Tiên','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '53BK' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Giang Thành' WHERE TableID='A014A' AND CustomsCode = '53BK' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','53BK','00',N'Chi cục HQ CK Giang Thành','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','53BK','00',N'Chi cục HQ CK Giang Thành','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '53CD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Hòn Chông' WHERE TableID='A014A' AND CustomsCode = '53CD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','53CD','00',N'Chi cục HQ CK Cảng Hòn Chông','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','53CD','00',N'Chi cục HQ CK Cảng Hòn Chông','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '53CH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Phú Quốc' WHERE TableID='A014A' AND CustomsCode = '53CH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','53CH','00',N'Chi cục HQ Phú Quốc','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','53CH','00',N'Chi cục HQ Phú Quốc','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Hữu Nghị' WHERE TableID='A014A' AND CustomsCode = '15BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BB','00',N'Đội Nghiệp vụ Hữu Nghị','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BB','00',N'Đội Nghiệp vụ Hữu Nghị','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BB' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ Co Sâu' WHERE TableID='A014A' AND CustomsCode = '15BB' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BB','01',N'Đội nghiệp vụ Co Sâu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BB','01',N'Đội nghiệp vụ Co Sâu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BB' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Pò Nhùng' WHERE TableID='A014A' AND CustomsCode = '15BB' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BB','02',N'Đội Nghiệp vụ Pò Nhùng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BB','02',N'Đội Nghiệp vụ Pò Nhùng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Chi Ma' WHERE TableID='A014A' AND CustomsCode = '15BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BC','00',N'Chi cục HQ CK Chi Ma','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BC','00',N'Chi cục HQ CK Chi Ma','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BC' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ Bản Chắt - Chi cục HQ CK Chi Ma' WHERE TableID='A014A' AND CustomsCode = '15BC' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BC','01',N'Đội nghiệp vụ Bản Chắt - Chi cục HQ CK Chi Ma','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BC','01',N'Đội nghiệp vụ Bản Chắt - Chi cục HQ CK Chi Ma','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Cốc Nam' WHERE TableID='A014A' AND CustomsCode = '15BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BD','00',N'Chi cục HQ Cốc Nam','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BD','00',N'Chi cục HQ Cốc Nam','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BE' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Na Hình' WHERE TableID='A014A' AND CustomsCode = '15BE' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BE','01',N'Đội Nghiệp vụ Na Hình','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BE','01',N'Đội Nghiệp vụ Na Hình','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BE' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Nà Nưa' WHERE TableID='A014A' AND CustomsCode = '15BE' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BE','02',N'Đội Nghiệp vụ Nà Nưa','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BE','02',N'Đội Nghiệp vụ Nà Nưa','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BE' AND CustomsSubSectionCode='03') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Bình Nghi' WHERE TableID='A014A' AND CustomsCode = '15BE' AND CustomsSubSectionCode='03' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BE','03',N'Đội Nghiệp vụ Bình Nghi','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BE','03',N'Đội Nghiệp vụ Bình Nghi','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15BE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Tân Thanh' WHERE TableID='A014A' AND CustomsCode = '15BE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BE','00',N'Đội Nghiệp vụ Tân Thanh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15BE','00',N'Đội Nghiệp vụ Tân Thanh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '15SI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Ga Đồng Đăng' WHERE TableID='A014A' AND CustomsCode = '15SI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15SI','00',N'Chi cục HQ Ga Đồng Đăng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','15SI','00',N'Chi cục HQ Ga Đồng Đăng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '13BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục HH XNK 1' WHERE TableID='A014A' AND CustomsCode = '13BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13BB','00',N'Đội Thủ tục HH XNK 1','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13BB','00',N'Đội Thủ tục HH XNK 1','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '13BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục HH XNK 2' WHERE TableID='A014A' AND CustomsCode = '13BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13BB','00',N'Đội Thủ tục HH XNK 2','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13BB','00',N'Đội Thủ tục HH XNK 2','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '13BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Mường Khương' WHERE TableID='A014A' AND CustomsCode = '13BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13BC','00',N'Chi cục HQ CK Mường Khương','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13BC','00',N'Chi cục HQ CK Mường Khương','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '13BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Bát Xát' WHERE TableID='A014A' AND CustomsCode = '13BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13BD','00',N'Chi cục HQ CK Bát Xát','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13BD','00',N'Chi cục HQ CK Bát Xát','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '13G1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '13G1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13G1','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13G1','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '13G2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'ICD Vinalines' WHERE TableID='A014A' AND CustomsCode = '13G2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13G2','00',N'ICD Vinalines','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','13G2','00',N'ICD Vinalines','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '48BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Mỹ Quý Tây' WHERE TableID='A014A' AND CustomsCode = '48BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48BC','00',N'Chi cục HQ CK Mỹ Quý Tây','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48BC','00',N'Chi cục HQ CK Mỹ Quý Tây','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '48BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc tế Bình Hiệp' WHERE TableID='A014A' AND CustomsCode = '48BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48BD','00',N'Chi cục HQ CK Quốc tế Bình Hiệp','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48BD','00',N'Chi cục HQ CK Quốc tế Bình Hiệp','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '48BE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Hưng Điền' WHERE TableID='A014A' AND CustomsCode = '48BE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48BE','00',N'Chi cục HQ Hưng Điền','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48BE','00',N'Chi cục HQ Hưng Điền','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '48BI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Đức Hòa' WHERE TableID='A014A' AND CustomsCode = '48BI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48BI','00',N'Chi cục HQ Đức Hòa','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48BI','00',N'Chi cục HQ Đức Hòa','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '48CG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '48CG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48CG','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48CG','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '48CG' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ Bến Tre' WHERE TableID='A014A' AND CustomsCode = '48CG' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48CG','02',N'Đội nghiệp vụ Bến Tre','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48CG','02',N'Đội nghiệp vụ Bến Tre','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '48F1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ KCN Long Hậu' WHERE TableID='A014A' AND CustomsCode = '48F1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48F1','00',N'Đội Nghiệp vụ KCN Long Hậu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48F1','00',N'Đội Nghiệp vụ KCN Long Hậu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '48F2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục- Chi cục HQ Bến Lức' WHERE TableID='A014A' AND CustomsCode = '48F2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48F2','00',N'Đội Thủ tục- Chi cục HQ Bến Lức','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','48F2','00',N'Đội Thủ tục- Chi cục HQ Bến Lức','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '29BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc tế Nậm Cắn' WHERE TableID='A014A' AND CustomsCode = '29BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','29BB','00',N'Chi cục HQ CK Quốc tế Nậm Cắn','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','29BB','00',N'Chi cục HQ CK Quốc tế Nậm Cắn','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '29BH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Thanh Thủy' WHERE TableID='A014A' AND CustomsCode = '29BH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','29BH','00',N'Chi cục HQ CK Thanh Thủy','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','29BH','00',N'Chi cục HQ CK Thanh Thủy','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '29CC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng' WHERE TableID='A014A' AND CustomsCode = '29CC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','29CC','00',N'Chi cục HQ CK Cảng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','29CC','00',N'Chi cục HQ CK Cảng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '29PF' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Vinh' WHERE TableID='A014A' AND CustomsCode = '29PF' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','29PF','00',N'Chi cục HQ Vinh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','29PF','00',N'Chi cục HQ Vinh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '31BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cha Lo' WHERE TableID='A014A' AND CustomsCode = '31BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31BB','00',N'Chi cục HQ CK Cha Lo','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31BB','00',N'Chi cục HQ CK Cha Lo','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '31BF' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cà Roòng' WHERE TableID='A014A' AND CustomsCode = '31BF' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31BF','00',N'Chi cục HQ CK Cà Roòng','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31BF','00',N'Chi cục HQ CK Cà Roòng','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '31D1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Cảng Hòn La' WHERE TableID='A014A' AND CustomsCode = '31D1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31D1','00',N'Đội Nghiệp vụ Cảng Hòn La','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31D1','00',N'Đội Nghiệp vụ Cảng Hòn La','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '31D2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Đồng Hới' WHERE TableID='A014A' AND CustomsCode = '31D2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31D2','00',N'Đội Nghiệp vụ Đồng Hới','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31D2','00',N'Đội Nghiệp vụ Đồng Hới','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '31D3' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Cảng Gianh' WHERE TableID='A014A' AND CustomsCode = '31D3' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31D3','00',N'Đội Nghiệp vụ Cảng Gianh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','31D3','00',N'Đội Nghiệp vụ Cảng Gianh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '60BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Nam Giang' WHERE TableID='A014A' AND CustomsCode = '60BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','60BD','00',N'Chi cục HQ CK Nam Giang','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','60BD','00',N'Chi cục HQ CK Nam Giang','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '60C1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '60C1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','60C1','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','60C1','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '60C2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Tây Giang' WHERE TableID='A014A' AND CustomsCode = '60C2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','60C2','00',N'Đội Tây Giang','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','60C2','00',N'Đội Tây Giang','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '60CB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Kỳ Hà' WHERE TableID='A014A' AND CustomsCode = '60CB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','60CB','00',N'Chi cục HQ CK Cảng Kỳ Hà','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','60CB','00',N'Chi cục HQ CK Cảng Kỳ Hà','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '35CB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Dung Quất' WHERE TableID='A014A' AND CustomsCode = '35CB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','35CB','00',N'Chi cục HQ CK Cảng Dung Quất','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','35CB','00',N'Chi cục HQ CK Cảng Dung Quất','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '35NC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ các KCN Quảng Ngãi' WHERE TableID='A014A' AND CustomsCode = '35NC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','35NC','00',N'Chi cục HQ các KCN Quảng Ngãi','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','35NC','00',N'Chi cục HQ các KCN Quảng Ngãi','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '20B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'HQ Cửa khẩu Bắc Luân' WHERE TableID='A014A' AND CustomsCode = '20B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20B1','00',N'HQ Cửa khẩu Bắc Luân','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20B1','00',N'HQ Cửa khẩu Bắc Luân','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '20B2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'HQ Cửa khẩu Ka Long' WHERE TableID='A014A' AND CustomsCode = '20B2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20B2','00',N'HQ Cửa khẩu Ka Long','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20B2','00',N'HQ Cửa khẩu Ka Long','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '20BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Hoành Mô' WHERE TableID='A014A' AND CustomsCode = '20BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20BC','00',N'Chi cục HQ CK Hoành Mô','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20BC','00',N'Chi cục HQ CK Hoành Mô','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '20BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Bắc Phong Sinh' WHERE TableID='A014A' AND CustomsCode = '20BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20BD','00',N'Chi cục HQ Bắc Phong Sinh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20BD','00',N'Chi cục HQ Bắc Phong Sinh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '20CD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cái Lân' WHERE TableID='A014A' AND CustomsCode = '20CD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20CD','00',N'Chi cục HQ CK Cảng Cái Lân','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20CD','00',N'Chi cục HQ CK Cảng Cái Lân','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '20CE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Vạn Gia' WHERE TableID='A014A' AND CustomsCode = '20CE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20CE','00',N'Chi cục HQ CK Cảng Vạn Gia','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20CE','00',N'Chi cục HQ CK Cảng Vạn Gia','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '20CF' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Hòn Gai' WHERE TableID='A014A' AND CustomsCode = '20CF' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20CF','00',N'Chi cục HQ CK Cảng Hòn Gai','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20CF','00',N'Chi cục HQ CK Cảng Hòn Gai','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '20CG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cẩm Phả' WHERE TableID='A014A' AND CustomsCode = '20CG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20CG','00',N'Chi cục HQ CK Cảng Cẩm Phả','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','20CG','00',N'Chi cục HQ CK Cảng Cẩm Phả','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '32BB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ -Tổng hợp' WHERE TableID='A014A' AND CustomsCode = '32BB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32BB','00',N'Đội Nghiệp vụ -Tổng hợp','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32BB','00',N'Đội Nghiệp vụ -Tổng hợp','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '32BC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK La Lay' WHERE TableID='A014A' AND CustomsCode = '32BC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32BC','00',N'Chi cục HQ CK La Lay','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32BC','00',N'Chi cục HQ CK La Lay','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '32BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ -Tổng hợp' WHERE TableID='A014A' AND CustomsCode = '32BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32BD','00',N'Đội Nghiệp vụ -Tổng hợp','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32BD','00',N'Đội Nghiệp vụ -Tổng hợp','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '32CD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cửa Việt' WHERE TableID='A014A' AND CustomsCode = '32CD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32CD','00',N'Chi cục HQ CK Cảng Cửa Việt','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32CD','00',N'Chi cục HQ CK Cảng Cửa Việt','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '32VG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Kiẻm soát HQ Quảng Trị' WHERE TableID='A014A' AND CustomsCode = '32VG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32VG','00',N'Đội Kiẻm soát HQ Quảng Trị','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','32VG','00',N'Đội Kiẻm soát HQ Quảng Trị','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '45B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '45B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45B1','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45B1','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '45B2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Quản lý Khu TM - CN Mộc Bài' WHERE TableID='A014A' AND CustomsCode = '45B2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45B2','00',N'Đội Quản lý Khu TM - CN Mộc Bài','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45B2','00',N'Đội Quản lý Khu TM - CN Mộc Bài','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '45BD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Phước Tân' WHERE TableID='A014A' AND CustomsCode = '45BD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45BD','00',N'Chi cục HQ Phước Tân','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45BD','00',N'Chi cục HQ Phước Tân','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '45BE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Kà Tum' WHERE TableID='A014A' AND CustomsCode = '45BE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45BE','00',N'Chi cục HQ CK Kà Tum','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45BE','00',N'Chi cục HQ CK Kà Tum','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '45C1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '45C1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45C1','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45C1','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '45C2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục Hải quan Chàng Riệc' WHERE TableID='A014A' AND CustomsCode = '45C2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45C2','00',N'Đội thủ tục Hải quan Chàng Riệc','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45C2','00',N'Đội thủ tục Hải quan Chàng Riệc','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '45F1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '45F1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45F1','00',N'Đội Nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45F1','00',N'Đội Nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '45F2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ KCN Phước Đông' WHERE TableID='A014A' AND CustomsCode = '45F2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45F2','00',N'Đội TTHQ KCN Phước Đông','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','45F2','00',N'Đội TTHQ KCN Phước Đông','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '27B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc tế Na Mèo' WHERE TableID='A014A' AND CustomsCode = '27B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','27B1','00',N'Chi cục HQ CK Quốc tế Na Mèo','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','27B1','00',N'Chi cục HQ CK Quốc tế Na Mèo','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '27B2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ CK Tén Tằn' WHERE TableID='A014A' AND CustomsCode = '27B2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','27B2','00',N'Đội TTHQ CK Tén Tằn','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','27B2','00',N'Đội TTHQ CK Tén Tằn','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '27F1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Thanh Hóa' WHERE TableID='A014A' AND CustomsCode = '27F1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','27F1','00',N'Chi cục HQ CK Cảng Thanh Hóa','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','27F1','00',N'Chi cục HQ CK Cảng Thanh Hóa','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '27F2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Nghi Sơn' WHERE TableID='A014A' AND CustomsCode = '27F2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','27F2','00',N'Chi cục HQ CK Cảng Nghi Sơn','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','27F2','00',N'Chi cục HQ CK Cảng Nghi Sơn','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '28NJ' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý các KCN Hà Nam' WHERE TableID='A014A' AND CustomsCode = '28NJ' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','28NJ','00',N'Chi cục HQ Quản lý các KCN Hà Nam','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','28NJ','00',N'Chi cục HQ Quản lý các KCN Hà Nam','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '28PC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiêp vụ' WHERE TableID='A014A' AND CustomsCode = '28PC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','28PC','00',N'Đội Nghiêp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','28PC','00',N'Đội Nghiêp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '28PC' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'HQ cảng Ninh Phúc' WHERE TableID='A014A' AND CustomsCode = '28PC' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','28PC','02',N'HQ cảng Ninh Phúc','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','28PC','02',N'HQ cảng Ninh Phúc','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '28PE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Nam Định' WHERE TableID='A014A' AND CustomsCode = '28PE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','28PE','00',N'Chi cục HQ Nam Định','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','28PE','00',N'Chi cục HQ Nam Định','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '33BA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ - Chi cục HQ CK A Đớt' WHERE TableID='A014A' AND CustomsCode = '33BA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33BA','00',N'Đội Nghiệp vụ - Chi cục HQ CK A Đớt','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33BA','00',N'Đội Nghiệp vụ - Chi cục HQ CK A Đớt','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '33BA' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Hồng Vân' WHERE TableID='A014A' AND CustomsCode = '33BA' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33BA','01',N'Đội Nghiệp vụ Hồng Vân','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33BA','01',N'Đội Nghiệp vụ Hồng Vân','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '33CC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Thuận An' WHERE TableID='A014A' AND CustomsCode = '33CC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33CC','00',N'Chi cục HQ CK Cảng Thuận An','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33CC','00',N'Chi cục HQ CK Cảng Thuận An','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '33CF' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Chân Mây' WHERE TableID='A014A' AND CustomsCode = '33CF' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33CF','00',N'Chi cục HQ CK Cảng Chân Mây','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33CF','00',N'Chi cục HQ CK Cảng Chân Mây','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '33PD' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Thủy An' WHERE TableID='A014A' AND CustomsCode = '33PD' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33PD','02',N'Chi cục HQ Thủy An','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','33PD','02',N'Chi cục HQ Thủy An','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 1' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','01',N'Đội Thủ tục hàng hóa XNK 1','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','01',N'Đội Thủ tục hàng hóa XNK 1','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 2' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','02',N'Đội Thủ tục hàng hóa XNK 2','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','02',N'Đội Thủ tục hàng hóa XNK 2','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='03') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 3' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='03' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','03',N'Đội Thủ tục hàng hóa XNK 3','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','03',N'Đội Thủ tục hàng hóa XNK 3','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='04') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 4' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='04' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','04',N'Đội Thủ tục hàng hóa XNK 4','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','04',N'Đội Thủ tục hàng hóa XNK 4','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A1') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội DHL' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A1' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A1',N'Chi cục HQ CPN - Đội DHL','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A1',N'Chi cục HQ CPN - Đội DHL','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A2') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội FEDEX' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A2' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A2',N'Chi cục HQ CPN - Đội FEDEX','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A2',N'Chi cục HQ CPN - Đội FEDEX','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A3') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội UPS' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A3' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A3',N'Chi cục HQ CPN - Đội UPS','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A3',N'Chi cục HQ CPN - Đội UPS','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A4') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội TNT' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A4' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A4',N'Chi cục HQ CPN - Đội TNT','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A4',N'Chi cục HQ CPN - Đội TNT','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A5') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội EMS' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A5' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A5',N'Chi cục HQ CPN - Đội EMS','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A5',N'Chi cục HQ CPN - Đội EMS','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A6') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội THANTOC' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A6' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A6',N'Chi cục HQ CPN - Đội THANTOC','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A6',N'Chi cục HQ CPN - Đội THANTOC','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A7') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội KERRY' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A7' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A7',N'Chi cục HQ CPN - Đội KERRY','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A7',N'Chi cục HQ CPN - Đội KERRY','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A8') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội HOPNHAT' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A8' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A8',N'Chi cục HQ CPN - Đội HOPNHAT','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A8',N'Chi cục HQ CPN - Đội HOPNHAT','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A9') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội SCE' WHERE TableID='A014A' AND CustomsCode = '02DS' AND CustomsSubSectionCode='A9' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A9',N'Chi cục HQ CPN - Đội SCE','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02DS','A9',N'Chi cục HQ CPN - Đội SCE','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02CV' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa nhập khẩu' WHERE TableID='A014A' AND CustomsCode = '02CV' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CV','01',N'Đội Thủ tục hàng hóa nhập khẩu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CV','01',N'Đội Thủ tục hàng hóa nhập khẩu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02CV' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa xuất khẩu' WHERE TableID='A014A' AND CustomsCode = '02CV' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CV','02',N'Đội Thủ tục hàng hóa xuất khẩu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CV','02',N'Đội Thủ tục hàng hóa xuất khẩu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02CI' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hàng hóa Nhập khẩu' WHERE TableID='A014A' AND CustomsCode = '02CI' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CI','01',N'Đội thủ tục hàng hóa Nhập khẩu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CI','01',N'Đội thủ tục hàng hóa Nhập khẩu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02CI' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hàng hóa Xuất khẩu' WHERE TableID='A014A' AND CustomsCode = '02CI' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CI','02',N'Đội thủ tục hàng hóa Xuất khẩu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CI','02',N'Đội thủ tục hàng hóa Xuất khẩu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02CI' AND CustomsSubSectionCode='03') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Giám sát' WHERE TableID='A014A' AND CustomsCode = '02CI' AND CustomsSubSectionCode='03' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CI','03',N'Đội Giám sát','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CI','03',N'Đội Giám sát','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02CC' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hàng hóa XNK' WHERE TableID='A014A' AND CustomsCode = '02CC' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CC','01',N'Đội thủ tục hàng hóa XNK','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CC','01',N'Đội thủ tục hàng hóa XNK','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02CC' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Giám sát' WHERE TableID='A014A' AND CustomsCode = '02CC' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CC','02',N'Đội Giám sát','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CC','02',N'Đội Giám sát','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02H1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 1 (cảng Bến Nghé)' WHERE TableID='A014A' AND CustomsCode = '02H1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02H1','00',N'Đội Thủ tục hàng hóa XNK 1 (cảng Bến Nghé)','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02H1','00',N'Đội Thủ tục hàng hóa XNK 1 (cảng Bến Nghé)','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02H2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục và Giám sát Xăng dầu (kho Xăng dầu)' WHERE TableID='A014A' AND CustomsCode = '02H2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02H2','00',N'Đội thủ tục và Giám sát Xăng dầu (kho Xăng dầu)','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02H2','00',N'Đội thủ tục và Giám sát Xăng dầu (kho Xăng dầu)','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02H3' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 2 (cảng VICT)' WHERE TableID='A014A' AND CustomsCode = '02H3' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02H3','00',N'Đội Thủ tục hàng hóa XNK 2 (cảng VICT)','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02H3','00',N'Đội Thủ tục hàng hóa XNK 2 (cảng VICT)','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02IK' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa Nhập khẩu' WHERE TableID='A014A' AND CustomsCode = '02IK' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02IK','01',N'Đội Thủ tục hàng hóa Nhập khẩu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02IK','01',N'Đội Thủ tục hàng hóa Nhập khẩu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02IK' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa Xuất khẩu' WHERE TableID='A014A' AND CustomsCode = '02IK' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02IK','02',N'Đội Thủ tục hàng hóa Xuất khẩu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02IK','02',N'Đội Thủ tục hàng hóa Xuất khẩu','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02B1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK – TCS' WHERE TableID='A014A' AND CustomsCode = '02B1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02B1','00',N'Đội Thủ tục hàng hóa XNK – TCS','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02B1','00',N'Đội Thủ tục hàng hóa XNK – TCS','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02CX' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK' WHERE TableID='A014A' AND CustomsCode = '02CX' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CX','00',N'Đội Thủ tục hàng hóa XNK','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02CX','00',N'Đội Thủ tục hàng hóa XNK','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02F1' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ 1 (Linh Trung)' WHERE TableID='A014A' AND CustomsCode = '02F1' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02F1','00',N'Đội Nghiệp vụ 1 (Linh Trung)','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02F1','00',N'Đội Nghiệp vụ 1 (Linh Trung)','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02F1' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Giám sát' WHERE TableID='A014A' AND CustomsCode = '02F1' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02F1','01',N'Đội Giám sát','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02F1','01',N'Đội Giám sát','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02F2' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ 2 (Linh Trung 2)' WHERE TableID='A014A' AND CustomsCode = '02F2' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02F2','00',N'Đội Nghiệp vụ 2 (Linh Trung 2)','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02F2','00',N'Đội Nghiệp vụ 2 (Linh Trung 2)','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02F3' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục Khu Công nghệ cao' WHERE TableID='A014A' AND CustomsCode = '02F3' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02F3','00',N'Đội Thủ tục Khu Công nghệ cao','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02F3','00',N'Đội Thủ tục Khu Công nghệ cao','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02XE' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = '02XE' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02XE','00',N'Đội nghiệp vụ','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02XE','00',N'Đội nghiệp vụ','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02PG' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng Đầu tư và Kinh doanh' WHERE TableID='A014A' AND CustomsCode = '02PG' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02PG','01',N'Đội Thủ tục hàng Đầu tư và Kinh doanh','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02PG','01',N'Đội Thủ tục hàng Đầu tư và Kinh doanh','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02PG' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng Sản xuất xuất khẩu và Gia công' WHERE TableID='A014A' AND CustomsCode = '02PG' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02PG','02',N'Đội Thủ tục hàng Sản xuất xuất khẩu và Gia công','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02PG','02',N'Đội Thủ tục hàng Sản xuất xuất khẩu và Gia công','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02PJ' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng Gia công' WHERE TableID='A014A' AND CustomsCode = '02PJ' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02PJ','01',N'Đội Thủ tục hàng Gia công','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02PJ','01',N'Đội Thủ tục hàng Gia công','E')
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = '02PJ' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục Hàng sản xuất xuất khẩu' WHERE TableID='A014A' AND CustomsCode = '02PJ' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02PJ','02',N'Đội Thủ tục Hàng sản xuất xuất khẩu','I')
 INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name,ImportExportClassification) VALUES('A014A','02PJ','02',N'Đội Thủ tục Hàng sản xuất xuất khẩu','E')
END


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '33.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('33.5',GETDATE(), N'BẢNG MÃ ĐỘI CHI CỤC HẢI QUAN DÙNG TRONG VNACCS')
END