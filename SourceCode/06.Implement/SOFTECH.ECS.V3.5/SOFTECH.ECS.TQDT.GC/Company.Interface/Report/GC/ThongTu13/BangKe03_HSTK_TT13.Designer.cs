namespace Company.Interface.Report.GC
{
    partial class BangKe03_HSTK_TT13
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblToKhaiNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguyenLieuVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNLVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuonghang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKi2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKi1 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lbl17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblbenthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAddNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAddThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbllimittimefirst = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDateFirst = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMathang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblhopsoHDGC = new DevExpress.XtraReports.UI.XRLabel();
            this.lblbennhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl4 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.HeightF = 20F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1058F, 20F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT,
            this.lblToKhaiNhap,
            this.lblTenNguyenLieuVT,
            this.lblMaNLVT,
            this.lblDVT,
            this.lblLuonghang,
            this.lblTongLuong,
            this.lblGhiChu});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // lblSTT
            // 
            this.lblSTT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTT.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTT.Weight = 0.0651890482398957;
            // 
            // lblToKhaiNhap
            // 
            this.lblToKhaiNhap.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblToKhaiNhap.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblToKhaiNhap.Name = "lblToKhaiNhap";
            this.lblToKhaiNhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblToKhaiNhap.StylePriority.UseTextAlignment = false;
            this.lblToKhaiNhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblToKhaiNhap.Weight = 0.15254237288135591;
            // 
            // lblTenNguyenLieuVT
            // 
            this.lblTenNguyenLieuVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenNguyenLieuVT.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenNguyenLieuVT.Name = "lblTenNguyenLieuVT";
            this.lblTenNguyenLieuVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNguyenLieuVT.StylePriority.UseTextAlignment = false;
            this.lblTenNguyenLieuVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTenNguyenLieuVT.Weight = 0.2607561929595828;
            // 
            // lblMaNLVT
            // 
            this.lblMaNLVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaNLVT.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaNLVT.Name = "lblMaNLVT";
            this.lblMaNLVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNLVT.StylePriority.UseTextAlignment = false;
            this.lblMaNLVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMaNLVT.Weight = 0.2385919165580182;
            // 
            // lblDVT
            // 
            this.lblDVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVT.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.StylePriority.UseBorders = false;
            this.lblDVT.StylePriority.UseFont = false;
            this.lblDVT.Weight = 0.13037809647979143;
            // 
            // lblLuonghang
            // 
            this.lblLuonghang.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuonghang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblLuonghang.Name = "lblLuonghang";
            this.lblLuonghang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuonghang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuonghang.Weight = 0.16297262059973924;
            // 
            // lblTongLuong
            // 
            this.lblTongLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongLuong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongLuong.Name = "lblTongLuong";
            this.lblTongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongLuong.Weight = 0.19556738344911179;
            // 
            // lblGhiChu
            // 
            this.lblGhiChu.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblGhiChu.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblGhiChu.Name = "lblGhiChu";
            this.lblGhiChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGhiChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblGhiChu.Weight = 0.17340262958869787;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel74,
            this.xrLabel75,
            this.xrLabel76,
            this.xrLabel77});
            this.ReportFooter.HeightF = 157F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel74
            // 
            this.xrLabel74.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(308F, 25F);
            this.xrLabel74.Text = "Công chức Hải quan kiểm tra, đối chiếu";
            // 
            // xrLabel75
            // 
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(450F, 42F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(266F, 25F);
            this.xrLabel75.Text = "Giám đốc doanh nghiệp";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(0F, 17F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(308F, 25F);
            this.xrLabel76.Text = "Đà Nẵng, ngày ... tháng ... năm......";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(450F, 17F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(266F, 25F);
            this.xrLabel77.Text = "Đà Nẵng, ngày ... tháng ... năm......";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayKi
            // 
            this.lblNgayKi.BackColor = System.Drawing.Color.Transparent;
            this.lblNgayKi.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayKi.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKi.LocationFloat = new DevExpress.Utils.PointFloat(817F, 33F);
            this.lblNgayKi.Name = "lblNgayKi";
            this.lblNgayKi.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblNgayKi.SizeF = new System.Drawing.SizeF(250F, 25F);
            this.lblNgayKi.Text = "Ngày ... tháng ... năm......";
            this.lblNgayKi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayKi2
            // 
            this.lblNgayKi2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKi2.LocationFloat = new DevExpress.Utils.PointFloat(817F, 78.00001F);
            this.lblNgayKi2.Name = "lblNgayKi2";
            this.lblNgayKi2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayKi2.SizeF = new System.Drawing.SizeF(250F, 20F);
            this.lblNgayKi2.Text = "(Ký tên, đóng dấu)";
            this.lblNgayKi2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayKi1
            // 
            this.lblNgayKi1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKi1.LocationFloat = new DevExpress.Utils.PointFloat(817F, 58F);
            this.lblNgayKi1.Name = "lblNgayKi1";
            this.lblNgayKi1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayKi1.SizeF = new System.Drawing.SizeF(250F, 20F);
            this.lblNgayKi1.Text = "Đại diện theo pháp luật của thương nhân";
            this.lblNgayKi1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayKi1,
            this.lblNgayKi2,
            this.lblNgayKi});
            this.ReportFooter1.HeightF = 117F;
            this.ReportFooter1.Name = "ReportFooter1";
            this.ReportFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ReportFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl17,
            this.lbl16,
            this.lblbenthue,
            this.xrLabel17,
            this.lbl2,
            this.lbl9,
            this.xrLabel32,
            this.xrLabel35,
            this.lbl14,
            this.lblSoLuong,
            this.lbl12,
            this.lblAddNhan,
            this.lbl11,
            this.lblAddThue,
            this.lbl10,
            this.lblChiCucHQ,
            this.lbl6,
            this.lbl13,
            this.lbllimittimefirst,
            this.lblDateFirst,
            this.lbl8,
            this.lblMathang,
            this.lblhopsoHDGC,
            this.lblbennhan,
            this.lbl15,
            this.lbl5,
            this.lbl1,
            this.lbl3,
            this.lbl4});
            this.ReportHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.ReportHeader.HeightF = 218F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ReportHeader.StylePriority.UseFont = false;
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl17
            // 
            this.lbl17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl17.LocationFloat = new DevExpress.Utils.PointFloat(842F, 0F);
            this.lbl17.Name = "lbl17";
            this.lbl17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl17.SizeF = new System.Drawing.SizeF(209F, 17F);
            this.lbl17.Text = "Mẫu: 03/HSTK-GC/2014, Khổ A4";
            this.lbl17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl16
            // 
            this.lbl16.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl16.LocationFloat = new DevExpress.Utils.PointFloat(875F, 67F);
            this.lbl16.Name = "lbl16";
            this.lbl16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl16.SizeF = new System.Drawing.SizeF(75F, 17F);
            this.lbl16.StylePriority.UseTextAlignment = false;
            this.lbl16.Text = "Trang số: ";
            this.lbl16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblbenthue
            // 
            this.lblbenthue.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbenthue.LocationFloat = new DevExpress.Utils.PointFloat(125F, 126F);
            this.lblbenthue.Name = "lblbenthue";
            this.lblbenthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblbenthue.SizeF = new System.Drawing.SizeF(358F, 20F);
            this.lblbenthue.Tag = "Bên thuê GC";
            this.lblbenthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblbenthue.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(192F, 106F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(291F, 20F);
            this.xrLabel17.Tag = "Số phụ lục";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel17.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lbl2
            // 
            this.lbl2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.LocationFloat = new DevExpress.Utils.PointFloat(8F, 106F);
            this.lbl2.Name = "lbl2";
            this.lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl2.SizeF = new System.Drawing.SizeF(184F, 20F);
            this.lbl2.Text = "Phụ lục hợp đồng gia công số :";
            this.lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl9
            // 
            this.lbl9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl9.LocationFloat = new DevExpress.Utils.PointFloat(483F, 106F);
            this.lbl9.Name = "lbl9";
            this.lbl9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl9.SizeF = new System.Drawing.SizeF(42F, 20F);
            this.lbl9.Text = "Ngày:";
            this.lbl9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(525F, 106F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(191F, 20F);
            this.xrLabel32.Tag = "Ngày phụ lục";
            this.xrLabel32.Text = " ";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel32.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(775F, 106F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(283F, 20F);
            this.xrLabel35.Tag = "Hạn phụ lục";
            this.xrLabel35.Text = " ";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel35.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lbl14
            // 
            this.lbl14.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl14.LocationFloat = new DevExpress.Utils.PointFloat(717F, 106F);
            this.lbl14.Name = "lbl14";
            this.lbl14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl14.SizeF = new System.Drawing.SizeF(58F, 20F);
            this.lbl14.Text = "Thời hạn";
            this.lbl14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong.LocationFloat = new DevExpress.Utils.PointFloat(617F, 166F);
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuong.SizeF = new System.Drawing.SizeF(443F, 20F);
            this.lblSoLuong.Tag = "Lượng hàng";
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoLuong.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lbl12
            // 
            this.lbl12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl12.LocationFloat = new DevExpress.Utils.PointFloat(483F, 166F);
            this.lbl12.Name = "lbl12";
            this.lbl12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl12.SizeF = new System.Drawing.SizeF(133F, 20F);
            this.lbl12.Text = "Lượng hàng gia công:";
            this.lbl12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAddNhan
            // 
            this.lblAddNhan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddNhan.LocationFloat = new DevExpress.Utils.PointFloat(542F, 146F);
            this.lblAddNhan.Name = "lblAddNhan";
            this.lblAddNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAddNhan.SizeF = new System.Drawing.SizeF(517F, 20F);
            this.lblAddNhan.Tag = "ĐC bên nhận";
            this.lblAddNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAddNhan.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lbl11
            // 
            this.lbl11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl11.LocationFloat = new DevExpress.Utils.PointFloat(483F, 146F);
            this.lbl11.Name = "lbl11";
            this.lbl11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl11.SizeF = new System.Drawing.SizeF(59F, 20F);
            this.lbl11.Text = "Địa chỉ :";
            this.lbl11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAddThue
            // 
            this.lblAddThue.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddThue.LocationFloat = new DevExpress.Utils.PointFloat(542F, 126F);
            this.lblAddThue.Name = "lblAddThue";
            this.lblAddThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAddThue.SizeF = new System.Drawing.SizeF(517F, 20F);
            this.lblAddThue.Tag = "ĐC bên thuê";
            this.lblAddThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAddThue.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lbl10
            // 
            this.lbl10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl10.LocationFloat = new DevExpress.Utils.PointFloat(483F, 126F);
            this.lbl10.Name = "lbl10";
            this.lbl10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl10.SizeF = new System.Drawing.SizeF(59F, 20F);
            this.lbl10.Text = "Địa chỉ :";
            this.lbl10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblChiCucHQ
            // 
            this.lblChiCucHQ.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChiCucHQ.LocationFloat = new DevExpress.Utils.PointFloat(192F, 186F);
            this.lblChiCucHQ.Name = "lblChiCucHQ";
            this.lblChiCucHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQ.SizeF = new System.Drawing.SizeF(867F, 20F);
            this.lblChiCucHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl6
            // 
            this.lbl6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6.LocationFloat = new DevExpress.Utils.PointFloat(8F, 186F);
            this.lbl6.Name = "lbl6";
            this.lbl6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl6.SizeF = new System.Drawing.SizeF(184F, 20F);
            this.lbl6.Text = "Đơn vị Hải quan làm thủ tục : ";
            this.lbl6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl13
            // 
            this.lbl13.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl13.LocationFloat = new DevExpress.Utils.PointFloat(717F, 86F);
            this.lbl13.Name = "lbl13";
            this.lbl13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl13.SizeF = new System.Drawing.SizeF(58F, 20F);
            this.lbl13.Text = "Thời hạn";
            this.lbl13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbllimittimefirst
            // 
            this.lbllimittimefirst.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllimittimefirst.LocationFloat = new DevExpress.Utils.PointFloat(775F, 86F);
            this.lbllimittimefirst.Name = "lbllimittimefirst";
            this.lbllimittimefirst.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbllimittimefirst.SizeF = new System.Drawing.SizeF(283F, 20F);
            this.lbllimittimefirst.Tag = "Thời hạn HĐ";
            this.lbllimittimefirst.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbllimittimefirst.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lblDateFirst
            // 
            this.lblDateFirst.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateFirst.LocationFloat = new DevExpress.Utils.PointFloat(525F, 86F);
            this.lblDateFirst.Name = "lblDateFirst";
            this.lblDateFirst.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDateFirst.SizeF = new System.Drawing.SizeF(191F, 20F);
            this.lblDateFirst.Tag = "Ngày HĐ";
            this.lblDateFirst.Text = " ";
            this.lblDateFirst.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDateFirst.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lbl8
            // 
            this.lbl8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8.LocationFloat = new DevExpress.Utils.PointFloat(483F, 86F);
            this.lbl8.Name = "lbl8";
            this.lbl8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl8.SizeF = new System.Drawing.SizeF(42F, 20F);
            this.lbl8.Text = "Ngày:";
            this.lbl8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblMathang
            // 
            this.lblMathang.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMathang.LocationFloat = new DevExpress.Utils.PointFloat(133F, 166F);
            this.lblMathang.Name = "lblMathang";
            this.lblMathang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMathang.SizeF = new System.Drawing.SizeF(350F, 20F);
            this.lblMathang.Tag = "Mặt hàng GC";
            this.lblMathang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMathang.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lblhopsoHDGC
            // 
            this.lblhopsoHDGC.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblhopsoHDGC.LocationFloat = new DevExpress.Utils.PointFloat(150F, 86F);
            this.lblhopsoHDGC.Name = "lblhopsoHDGC";
            this.lblhopsoHDGC.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblhopsoHDGC.SizeF = new System.Drawing.SizeF(333F, 20F);
            this.lblhopsoHDGC.Tag = "Số hợp đồng";
            this.lblhopsoHDGC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblhopsoHDGC.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lblbennhan
            // 
            this.lblbennhan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbennhan.LocationFloat = new DevExpress.Utils.PointFloat(133F, 146F);
            this.lblbennhan.Name = "lblbennhan";
            this.lblbennhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblbennhan.SizeF = new System.Drawing.SizeF(350F, 20F);
            this.lblbennhan.Tag = "Bên nhận GC";
            this.lblbennhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblbennhan.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lbl15
            // 
            this.lbl15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl15.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold);
            this.lbl15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 17F);
            this.lbl15.Name = "lbl15";
            this.lbl15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl15.SizeF = new System.Drawing.SizeF(1083F, 50F);
            this.lbl15.Text = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ XUẤT TRẢ RA NƯỚC NGOÀI VÀ CHUYỂN SANG HỢP ĐỒNG " +
                "GIA CÔNG KHÁC TRONG KHI ĐANG THỰC HIỆN HỢP ĐỒNG GIA CÔNG";
            this.lbl15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl5
            // 
            this.lbl5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.LocationFloat = new DevExpress.Utils.PointFloat(8F, 166F);
            this.lbl5.Name = "lbl5";
            this.lbl5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl5.SizeF = new System.Drawing.SizeF(125F, 20F);
            this.lbl5.Text = "Mặt hàng gia công :";
            this.lbl5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl1
            // 
            this.lbl1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.LocationFloat = new DevExpress.Utils.PointFloat(8F, 86F);
            this.lbl1.Name = "lbl1";
            this.lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl1.SizeF = new System.Drawing.SizeF(142F, 20F);
            this.lbl1.Text = "Hợp đồng gia công số :";
            this.lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl3
            // 
            this.lbl3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.LocationFloat = new DevExpress.Utils.PointFloat(8F, 126F);
            this.lbl3.Name = "lbl3";
            this.lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl3.SizeF = new System.Drawing.SizeF(117F, 20F);
            this.lbl3.Text = "Bên thuê gia công :";
            this.lbl3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl4
            // 
            this.lbl4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4.LocationFloat = new DevExpress.Utils.PointFloat(8F, 146F);
            this.lbl4.Name = "lbl4";
            this.lbl4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl4.SizeF = new System.Drawing.SizeF(125F, 20F);
            this.lbl4.Text = "Bên nhận gia công :";
            this.lbl4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel2,
            this.xrLabel12,
            this.xrLabel15,
            this.xrLabel4,
            this.xrLabel5,
            this.xrLabel6,
            this.xrLabel7,
            this.xrLabel8,
            this.xrTable2});
            this.PageHeader.HeightF = 87F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.Azure;
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(50F, 67F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.Text = "Số TT";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.Azure;
            this.xrLabel2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(50F, 0F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(117F, 67F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.Text = "Số tờ khai";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.Azure;
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(167F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(891F, 25F);
            this.xrLabel12.Text = "Nguyên liệu, vật tư";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BackColor = System.Drawing.Color.Azure;
            this.xrLabel15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(167F, 25F);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(200F, 42F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.Text = "Tên nguyên phụ liệu";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.BackColor = System.Drawing.Color.Azure;
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(367F, 25F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(183F, 42F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.Text = "Mã NPL";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BackColor = System.Drawing.Color.Azure;
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(550F, 25F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(100F, 42F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.Text = "Đơn vị tính";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.BackColor = System.Drawing.Color.Azure;
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(650F, 25F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(125F, 42F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.Text = "Lượng hàng từng tờ khai";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.BackColor = System.Drawing.Color.Azure;
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(775F, 25F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(150F, 42F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.Text = "Tổng lượng";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.BackColor = System.Drawing.Color.Azure;
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(925F, 25F);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(133F, 42F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.Text = "Ghi chú";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 67F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1058F, 20F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "(1)";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.0651890482398957;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "(2)";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.15254237288135591;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "(3)";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.2607561929595828;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "(4)";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 0.2385919165580182;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "(5)";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.13037809647979143;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "(6)";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.16297262059973924;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "(7)";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.195567224296162;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "(8)";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.17340278874164766;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 20F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 25F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // BangKe03_HSTK_TT13
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportFooter1,
            this.ReportHeader,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(69, 15, 20, 25);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKi1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKi2;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKi;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel lbl17;
        private DevExpress.XtraReports.UI.XRLabel lbl16;
        private DevExpress.XtraReports.UI.XRLabel lblbenthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel lbl2;
        private DevExpress.XtraReports.UI.XRLabel lbl9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel lbl14;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuong;
        private DevExpress.XtraReports.UI.XRLabel lbl12;
        private DevExpress.XtraReports.UI.XRLabel lblAddNhan;
        private DevExpress.XtraReports.UI.XRLabel lbl11;
        private DevExpress.XtraReports.UI.XRLabel lblAddThue;
        private DevExpress.XtraReports.UI.XRLabel lbl10;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQ;
        private DevExpress.XtraReports.UI.XRLabel lbl6;
        private DevExpress.XtraReports.UI.XRLabel lbl13;
        private DevExpress.XtraReports.UI.XRLabel lbllimittimefirst;
        private DevExpress.XtraReports.UI.XRLabel lblDateFirst;
        private DevExpress.XtraReports.UI.XRLabel lbl8;
        private DevExpress.XtraReports.UI.XRLabel lblMathang;
        public DevExpress.XtraReports.UI.XRLabel lblhopsoHDGC;
        private DevExpress.XtraReports.UI.XRLabel lblbennhan;
        private DevExpress.XtraReports.UI.XRLabel lbl15;
        private DevExpress.XtraReports.UI.XRLabel lbl5;
        private DevExpress.XtraReports.UI.XRLabel lbl1;
        private DevExpress.XtraReports.UI.XRLabel lbl3;
        private DevExpress.XtraReports.UI.XRLabel lbl4;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT;
        private DevExpress.XtraReports.UI.XRTableCell lblToKhaiNhap;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguyenLieuVT;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNLVT;
        private DevExpress.XtraReports.UI.XRTableCell lblDVT;
        private DevExpress.XtraReports.UI.XRTableCell lblLuonghang;
        private DevExpress.XtraReports.UI.XRTableCell lblTongLuong;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChu;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
    }
}
