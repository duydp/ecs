﻿using System;
using System.Windows.Forms;
using Company.QuanTri;
using System.Data.SqlClient;
using ServerRemote;

namespace Company.Interface
{
    public partial class Login : Form
    {
        private MessageBoxControl _msgBox;

        public Login()
        {
            InitializeComponent();
        }

        private void UiButton2Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void UiButton3Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                    string msg = "Đăng nhập không thành công.";
                    MessageBoxControl.ShowMessage(msg);
                    MainForm.isLoginSuccess = false;
                }
                else
                {

                    MainForm.isLoginSuccess = true;
                    Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MainForm.isLoginSuccess = false;
                if (MessageBoxControl.ShowMessageConfirm("Lỗi kết nối CSDL. Bạn có muốn cấu hình kết nối CSDL không?") == "Yes")
                {
                    var f = new ThietLapThongSoKBForm();
                    f.ShowDialog();
                }
                return;
            }
            
        }


        private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var f = new ThietLapThongSoKBForm();
            f.ShowDialog();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtMatKhau.Text = txtUser.Text = string.Empty;
            txtUser.Focus();
        }
    }
}