﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    public class ReduceTax
    {
        /// <summary>
        /// So van ban mien/giam thue
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }
        /// <summary>
        /// Thue suat truoc khi giam
        /// </summary>
        [XmlElement("tax")]
        public string tax { get; set; }
        /// <summary>
        /// Ty le giam
        /// </summary>
        [XmlElement("redureValue")]
        public string redureValue { get; set; }
        
    }
}
