--Update HS 8 so HangMauDich
UPDATE dbo.t_KDT_HangMauDich SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_KDT_HangGiayPhepDetail SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_KDT_HoaDonThuongMaiDetail SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_KDT_HopDongThuongMaiDetail SET MaHS = LEFT(MaHS, 8)

GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.3', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.3', getdate(), null)
	end