using System.Data;
using System.Data.Common;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class LoaiPhieuChuyenTiep : BaseClass
    {
        public static DataTable SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_LoaiPhieuChuyenTiep --ORDER BY TenPhieuChuyenTiep";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static DataTable SelectBy_ID(string ID)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_LoaiPhieuChuyenTiep WHERE ID LIKE '%{0}%'", ID);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static string GetName(string ID)
        {
            //string query = string.Format("SELECT Ten FROM t_HaiQuan_LoaiPhieuChuyenTiep WHERE ID = '{0}'", ID);
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //return db.ExecuteScalar(dbCommand).ToString();


            try
            {
                string query = string.Format("[ID] = '{0}'", ID);

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["LoaiPhieuChuyenTiep"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["Ten"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ID, ex); }

            return string.Empty;
        }
    }
}