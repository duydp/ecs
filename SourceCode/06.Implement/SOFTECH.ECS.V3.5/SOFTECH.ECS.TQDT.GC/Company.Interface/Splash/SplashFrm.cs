using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;

namespace Company.Interface.Splash
{
    public partial class SplashFrm : DevExpress.XtraEditors.XtraForm
    {
        public SplashFrm()
        {
            InitializeComponent();
            /*lblStatus.Text = _Status;*/
        }

        private static Thread _splashThread;
        private static SplashFrm _splashForm;
        public static string _Status = "Chương trình đang thực thi, xin vui lòng đợi trong giây lát...";
        public static void ShowSplash(string status)
        {
//             if (_splashThread == null)
//             {
                // show the form in a new thread   
            _Status = status;
                _splashThread = new Thread(new ThreadStart(DoShowSplash));
            
                _splashThread.IsBackground = true;
                _splashThread.Start();
            /*}*/
        }
        private static void DoShowSplash()
        {
            if (_splashForm == null)
            {
                _splashForm = new SplashFrm();
                //_splashForm.lblStatus.Text = _Status;
                _splashForm.StartPosition = FormStartPosition.CenterScreen;
                _splashForm.TopMost = true;
                
            }
            //_splashForm.Show();
            // create a new message pump on this thread (started from ShowSplash)       
            Application.Run(_splashForm);
        }
        public static void CloseSplash()
        {
            // Need to call on the thread that launched this splash       
            if (_splashForm.InvokeRequired)
                _splashForm.Invoke(new MethodInvoker(CloseSplash));
            else
            {
                _splashForm.Dispose();
                _splashForm = null;
                Application.ExitThread();
                
                
            }
        }

        private void SplashFrm_Load(object sender, EventArgs e)
        {
            //this.lblTrangThai.Text = _Status;
            this.lblStatus.Text = _Status;

        }

    }
}