﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
#endif
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.Interface.Report.VNACCS;
#if KD_V4
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
#endif
#if SXXK_V4
using Company.Interface.Report.VNACCS;
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
#endif

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class CapSoDinhDanhHangHoaForm : BaseFormHaveGuidPanel
    {
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public KDT_VNACCS_CapSoDinhDanh capSoDinhDanh = new KDT_VNACCS_CapSoDinhDanh();
        public String Caption;
        public bool IsChange;
        public CapSoDinhDanhHangHoaForm()
        {
            InitializeComponent();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (capSoDinhDanh.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || capSoDinhDanh.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || capSoDinhDanh.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send();
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdPrint":
                    Print();
                    break;
                    
            }
        }
        private void Print()
        {
            try
            {
                DinhDanhHangHoa f = new DinhDanhHangHoa();
                f.capSoDinhDanh = capSoDinhDanh;
                f.BindReport();
                f.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }
        private void Send()
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ","Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (capSoDinhDanh.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ","Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    capSoDinhDanh = KDT_VNACCS_CapSoDinhDanh.Load(capSoDinhDanh.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.CapSoDinhDanh;
                sendXML.master_id = capSoDinhDanh.ID;

                if (sendXML.Load())
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ","Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    capSoDinhDanh.GuidStr = Guid.NewGuid().ToString();
                    CapSoDinhDanh_VNACCS capSoDinhDanh_VNACCS = new CapSoDinhDanh_VNACCS();
                    capSoDinhDanh_VNACCS = Mapper_V4.ToDataTransferCapSoDinhDanh(capSoDinhDanh, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = capSoDinhDanh.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(capSoDinhDanh.MaHQ)),
                              Identity = capSoDinhDanh.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = capSoDinhDanh_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = capSoDinhDanh.GuidStr,
                          },
                          capSoDinhDanh_VNACCS
                        );
                    capSoDinhDanh.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)// Chỉ lưu message khi kết quả phản hồi khác không chấp nhận
                    {
                        sendForm.Message.XmlSaveMessage(capSoDinhDanh.ID, MessageTitle.RegisterDinhDanh);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.CapSoDinhDanh;
                        sendXML.master_id = capSoDinhDanh.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(capSoDinhDanh_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            capSoDinhDanh.Update();
                            txtSoTiepNhan.Text = capSoDinhDanh.SoTiepNhan.ToString();
                            ccNgayTiepNhan.Value = capSoDinhDanh.NgayTiepNhan;
                            txtSoDDHH.Text = capSoDinhDanh.SoDinhDanh;
                            clcNgayCap.Value = capSoDinhDanh.NgayCap;
                            lblTrangThai.Text = "Đã duyệt";
                            ShowMessageTQDT(msgInfor, false);
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            capSoDinhDanh.Update();
                            txtSoTiepNhan.Text = capSoDinhDanh.SoTiepNhan.ToString();
                            ccNgayTiepNhan.Value = capSoDinhDanh.NgayTiepNhan;
                            txtSoDDHH.Text = capSoDinhDanh.SoDinhDanh;
                            clcNgayCap.Value = capSoDinhDanh.NgayCap;
                            lblTrangThai.Text = "Đã cấp số định danh";
                            ShowMessageTQDT(msgInfor, false);
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhDanhHangHoaSendHandler(capSoDinhDanh, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(cbbLoaiDoiTuong, errorProvider, "LOẠI ĐỐI TƯỢNG XIN CẤP SỐ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHH, errorProvider, "LOẠI THÔNG TIN HÀNG HÓA", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void Set()
        {
            try
            {
                txtMaHQ.Text = capSoDinhDanh.MaHQ;
                txtMaDoanhNghiep.Text = capSoDinhDanh.MaDoanhNghiep;
                txtSoTiepNhan.Text = capSoDinhDanh.SoTiepNhan.ToString();
                ccNgayTiepNhan.Value = capSoDinhDanh.NgayTiepNhan;

                txtSoDDHH.Text = capSoDinhDanh.SoDinhDanh;
                clcNgayCap.Value = capSoDinhDanh.NgayCap;

                cbbLoaiDoiTuong.TextChanged -= new EventHandler(txt_TextChanged);
                cbbLoaiDoiTuong.SelectedValue = capSoDinhDanh.LoaiDoiTuong;
                cbbLoaiDoiTuong.TextChanged += new EventHandler(txt_TextChanged);

                cbbLoaiHH.TextChanged -= new EventHandler(txt_TextChanged);
                cbbLoaiHH.SelectedValue = capSoDinhDanh.LoaiTTHH;
                cbbLoaiHH.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (capSoDinhDanh==null)
                {
                    capSoDinhDanh = new KDT_VNACCS_CapSoDinhDanh();
                }
                capSoDinhDanh.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text.ToString());
                capSoDinhDanh.NgayTiepNhan = ccNgayTiepNhan.Value;
                capSoDinhDanh.NgayCap = clcNgayCap.Value;
                capSoDinhDanh.MaDoanhNghiep = txtMaDoanhNghiep.Text;
                capSoDinhDanh.MaHQ = txtMaHQ.Text;
                capSoDinhDanh.LoaiDoiTuong = Convert.ToInt32(cbbLoaiDoiTuong.SelectedValue.ToString());
                capSoDinhDanh.LoaiTTHH = Convert.ToInt32(cbbLoaiHH.SelectedValue.ToString());
                if (capSoDinhDanh.ID==0)
                {
                    capSoDinhDanh.ID = capSoDinhDanh.Insert();  
                }
                else
                {
                    capSoDinhDanh.Update();
                }
                ShowMessageTQDT("Lưu thành công ",false);
                this.SetChange(false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.CapSoDinhDanh;
            sendXML.master_id = capSoDinhDanh.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                return;
            }
            while (isFeedBack)
            {
                string reference = capSoDinhDanh.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.DinhDanh,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.DinhDanh,
                };
                subjectBase.Type = DeclarationIssuer.DinhDanh;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = capSoDinhDanh.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(capSoDinhDanh.MaHQ.Trim())),
                                                  Identity = capSoDinhDanh.MaHQ
                                              }, subjectBase, null);
                if (capSoDinhDanh.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(capSoDinhDanh.MaHQ));
                    msgSend.To.Identity = capSoDinhDanh.MaHQ;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                    {
                        txtSoTiepNhan.Text = capSoDinhDanh.SoTiepNhan.ToString();
                        ccNgayTiepNhan.Value = capSoDinhDanh.NgayTiepNhan;
                        txtSoDDHH.Text = capSoDinhDanh.SoDinhDanh;
                        clcNgayCap.Value = capSoDinhDanh.NgayCap;
                        lblTrangThai.Text = "Đã cấp số tiếp nhận";
                        ShowMessageTQDT(msgInfor, false);
                        Feedback();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN )
                    {
                        txtSoTiepNhan.Text = capSoDinhDanh.SoTiepNhan.ToString();
                        ccNgayTiepNhan.Value = capSoDinhDanh.NgayTiepNhan;
                        txtSoDDHH.Text = capSoDinhDanh.SoDinhDanh;
                        clcNgayCap.Value = capSoDinhDanh.NgayCap;
                        lblTrangThai.Text = "Đã cấp số định danh";

                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }
        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = capSoDinhDanh.ID;
                form.DeclarationIssuer = DeclarationIssuer.DinhDanh;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void CapSoDinhDanhHangHoaForm_Load(object sender, EventArgs e)//Load Mã DN và Mã HQ VNACCS ngay từ đầu
        {
            txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            txtMaHQ.Text = GlobalSettings.MA_HAI_QUAN_VNACCS;
            Caption = this.Text;
            if (capSoDinhDanh.ID !=0)
            {
                Set();
            }
        }

        private void linkHDSD_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=60mzWC9lUug");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void CapSoDinhDanhHangHoaForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Khai báo số định danh hàng hoá có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.Save();
                }
            }
        }
    }
}
