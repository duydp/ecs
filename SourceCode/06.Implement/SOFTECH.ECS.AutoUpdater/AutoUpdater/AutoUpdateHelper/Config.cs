/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   Ê¥µîÆïÊ¿£¨Knights Warrior£© 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace ECS.AutoUpdate
{
    public class Config
    {
        #region The private fields
        private bool enabled;
        private string serverUrl = string.Empty;
        private UpdateFileList updateFileList = new UpdateFileList();
        private string classUpdateSql = string.Empty;
        private string classUnzip = string.Empty;
        private string title = string.Empty;
        private string dateRelease;
        #endregion

        #region The public property
        public string ProxyHost { get; set; }
        public string ProxyPost { get; set; }
        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }
        public string ServerUrl
        {
            get { return serverUrl; }
            set { serverUrl = value; }
        }
        public UpdateFileList UpdateFileList
        {
            get { return updateFileList; }
            set { updateFileList = value; }
        }
        public string ClassUpdateSql
        {
            get { return classUpdateSql; }
            set { classUpdateSql = value; }
        }
        public string ClassUnZip
        {
            get { return classUnzip; }
            set { classUnzip = value; }
        }
        public string TitleUpdate { get; set; }
        public string Description { get; set; }
        public string DateRelease
        {
            get { return dateRelease; }
            set { dateRelease = value; }
        }
        #endregion
        public static Config Install
        {
            get
            {
                return _install;
            }
        }
        #region The public method
        static Config _install;

        public static Config LoadConfig(string file)
        {
            if (!System.IO.File.Exists(file)) throw new NotSupportedException("Tệp tin Config không tồn tại");
            XmlSerializer xs = new XmlSerializer(typeof(Config));
            StreamReader sr = new StreamReader(file);
            _install = xs.Deserialize(sr) as Config;
            sr.Close();

            return _install;
        }
        public void SaveConfig(string file)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Config));
            StreamWriter sw = new StreamWriter(file);
            xs.Serialize(sw, this);
            sw.Close();
        }
        public void Save()
        {
            SaveConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.FILENAME));
        }
        #endregion
    }

}
