﻿
namespace Utils
{
    public class ColumnExcel
    {
        public static string SheetName;

        public static char CodeNo;
        public static char CodeColumn;
        public static char CodeElementNameJP;
        public static char CodeElementNameEN;
        public static char CodeElementNameVN;
        public static char CodeID;
        public static char CodeAttribute;
        public static char CodeDigit;
        public static char CodeRepetition1;
        public static char CodeCondition1;
        public static char CodeCondition2;
        public static char CodeCondition3;
        public static char CodeCode;
        public static char CodeFormat;
        public static char CodeReferenceDB;
        public static char CodeInputConditionJP;
        public static char CodeInputConditionEN;

        public static int IntNo;
        public static int IntColumn;
        public static int IntElementNameJP;
        public static int IntElementNameEN;
        public static int IntElementNameVN;
        public static int IntID;
        public static int IntAttribute;
        public static int IntDigit;
        public static int IntRepetition1;
        public static int IntCondition1;
        public static int IntCondition2;
        public static int IntCondition3;
        public static int IntCode;
        public static int IntFormat;
        public static int IntReferenceDB;
        public static int IntInputConditionJP;
        public static int IntInputConditionEN;
    }
}
