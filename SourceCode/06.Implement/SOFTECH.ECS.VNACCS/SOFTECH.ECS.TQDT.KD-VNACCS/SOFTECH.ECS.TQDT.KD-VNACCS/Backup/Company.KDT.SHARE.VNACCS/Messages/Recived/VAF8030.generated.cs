using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAF8030 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute CSM { get; set; }
public PropertiesAttribute CBN { get; set; }
public PropertiesAttribute SPN { get; set; }
public PropertiesAttribute IEN { get; set; }
public PropertiesAttribute IEC { get; set; }
public PropertiesAttribute PCD { get; set; }
public PropertiesAttribute ADB { get; set; }
public PropertiesAttribute TEL { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute DDC { get; set; }
public PropertiesAttribute DKC { get; set; }
public PropertiesAttribute TAN { get; set; }
public PropertiesAttribute TNM { get; set; }
public PropertiesAttribute DOP { get; set; }

public VAF8030()
        {
CSM = new PropertiesAttribute(210, typeof(string));
CBN = new PropertiesAttribute(210, typeof(string));
SPN = new PropertiesAttribute(12, typeof(int));
IEN = new PropertiesAttribute(300, typeof(string));
IEC = new PropertiesAttribute(13, typeof(string));
PCD = new PropertiesAttribute(7, typeof(string));
ADB = new PropertiesAttribute(300, typeof(string));
TEL = new PropertiesAttribute(20, typeof(string));
ICN = new PropertiesAttribute(12, typeof(int));
DDC = new PropertiesAttribute(8, typeof(DateTime));
DKC = new PropertiesAttribute(3, typeof(string));
TAN = new PropertiesAttribute(15, typeof(string));
TNM = new PropertiesAttribute(210, typeof(string));
DOP = new PropertiesAttribute(8, typeof(DateTime));
TongSoByte = 1356;
}

}public partial class EnumGroupID
    {
}

}
