﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class CauHinhToKhaiForm : Company.Interface.BaseForm
    {
        public CauHinhToKhaiForm()
        {
            InitializeComponent();
        }
        public Label lblPhuongThucTT { get { return _lblPhuongThucTT; } }
        public List<Label> Listlable = new List<Label>();

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (Label lable in Listlable)
            {
                lable.ForeColor = lable.ForeColor == Color.Black ? Color.Red : Color.Black;
            }
        }
        private void khoitao_DuLieuChuan()
        {
            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            txtTenDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            //txtTyGiaUSD.Value = GlobalSettings.TY_GIA_USD;
            cbMaHTS.SelectedIndex = GlobalSettings.MaHTS;
            ctrNuocXK.Ma = GlobalSettings.NUOC;
            ctrNuocNK.Ma = GlobalSettings.NUOCNK;
            ctrCuaKhauXuatHang.Ma = GlobalSettings.CUA_KHAU;
            ctrDiaDiemDoHang.Ma = GlobalSettings.DIA_DIEM_DO_HANG;

            ctrCuaKhauXuat.Ma = GlobalSettings.CUA_KHAU_XUAT_HANG;


            txtMienThue1.Text = GlobalSettings.TieuDeInDinhMuc;
            txtMienThue2.Text = GlobalSettings.MienThueGTGT;
            cbTinhThue.SelectedValue = GlobalSettings.TuDongTinhThue;

            cbPTTinhThue.SelectedValue = GlobalSettings.TinhThueTGNT;
            
            //phiph
                      
            ctrLoaiHinhMD_Nhap.Ma = GlobalSettings.LoaihinhMD_Nhap;
            
            ctrLoaiHinhMD_Xuat.Nhom = "XSX";
            ctrLoaiHinhMD_Xuat.loadData();
            ctrLoaiHinhMD_Xuat.Ma = GlobalSettings.LoaihinhMD_Xuat;
           
        }
        private void CauHinhToKhaiForm_Load(object sender, EventArgs e)
        {
            
            khoitao_DuLieuChuan();
            if (Listlable.Count > 0)
            {
                timer1.Interval = 500;
                timer1.Start();
            }

            
           
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            GlobalSettings.RefreshKey();
            //Hungtq 14/01/2011. Luu cau hinh
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DOI_TAC", txtTenDoiTac.Text, GlobalSettings.TEN_DOI_TAC);
            GlobalSettings.TEN_DOI_TAC = txtTenDoiTac.Text;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PTTT_MAC_DINH", cbPTTT.SelectedValue, GlobalSettings.PTTT_MAC_DINH);
            GlobalSettings.PTTT_MAC_DINH = cbPTTT.SelectedValue.ToString();
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PTVT_MAC_DINH", cbPTVT.SelectedValue, GlobalSettings.PTVT_MAC_DINH);
            GlobalSettings.PTVT_MAC_DINH = cbPTVT.SelectedValue.ToString();
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DKGH_MAC_DINH", cbDKGH.SelectedValue, GlobalSettings.DKGH_MAC_DINH);
            GlobalSettings.DKGH_MAC_DINH = cbDKGH.SelectedValue.ToString();
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CUA_KHAU", ctrCuaKhauXuatHang.Ma, GlobalSettings.CUA_KHAU);
            GlobalSettings.CUA_KHAU = ctrCuaKhauXuatHang.Ma;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DIA_DIEM_DO_HANG", ctrDiaDiemDoHang.Ma,GlobalSettings.DIA_DIEM_DO_HANG);
            GlobalSettings.DIA_DIEM_DO_HANG = ctrDiaDiemDoHang.Ma;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CUA_KHAU_XUAT_HANG", ctrCuaKhauXuat.Ma, GlobalSettings.CUA_KHAU_XUAT_HANG);
            GlobalSettings.CUA_KHAU_XUAT_HANG = ctrCuaKhauXuat.Ma;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NGUYEN_TE_MAC_DINH", ctrNguyenTe.Ma,GlobalSettings.NGUYEN_TE_MAC_DINH);
            GlobalSettings.NGUYEN_TE_MAC_DINH = ctrNguyenTe.Ma;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NUOC", ctrNuocXK.Ma,GlobalSettings.NUOC);
            GlobalSettings.NUOC = ctrNuocXK.Ma;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NUOCNK", ctrNuocNK.Ma, GlobalSettings.NUOCNK);
            GlobalSettings.NUOCNK = ctrNuocNK.Ma;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_HTS", cbMaHTS.SelectedValue,GlobalSettings.MaHTS);
            GlobalSettings.MaHTS = Convert.ToInt32(cbMaHTS.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieuDeInDinhMuc", txtMienThue1.Text.Trim(),GlobalSettings.TieuDeInDinhMuc);
            GlobalSettings.TieuDeInDinhMuc=txtMienThue1.Text.Trim();
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TuDongTinhThue", cbTinhThue.SelectedValue,GlobalSettings.TuDongTinhThue);
            GlobalSettings.TuDongTinhThue = cbTinhThue.SelectedValue.ToString();
            //KhanhHN 22/06/2012 
            // Cấu hình tính thuế dựa trên Đơn giá/ trị giá
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TinhThueTGNT", cbPTTinhThue.SelectedValue, GlobalSettings.TinhThueTGNT);
            GlobalSettings.TinhThueTGNT = cbPTTinhThue.SelectedValue.ToString();

            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MienThueGTGT", txtMienThue2.Text.Trim(),GlobalSettings.MienThueGTGT);
            GlobalSettings.MienThueGTGT=txtMienThue2.Text.Trim();
            //Phi 12/08/2013
            GlobalSettings.LoaihinhMD_Nhap = ctrLoaiHinhMD_Nhap.Ma;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("LoaiHinhMD_Nhap", ctrLoaiHinhMD_Nhap.Ma, GlobalSettings.LoaihinhMD_Nhap);
            GlobalSettings.LoaihinhMD_Xuat = ctrLoaiHinhMD_Xuat.Ma;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("LoaiHinhMD_Xuat", ctrLoaiHinhMD_Xuat.Ma, GlobalSettings.LoaihinhMD_Xuat);
            
            ShowMessage("Lưu cấu hình thành công.", false);
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

 

      


    }
}

