using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.Interface.SXXK.BangKe;
using Janus.Windows.GridEX;
using Janus.Windows.EditControls;
using Company.BLL.KDT;
using Company.BLL;
using Company.Interface.Report.SXXK;
using System.IO;
using Company.Interface.Report;

namespace Company.Interface.SXXK
{
    public partial class CapNhatHoSoThanhLyForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        private string xmlCurrent = "";
        public CapNhatHoSoThanhLyForm()
        {
            InitializeComponent();
        }
        private void setCommandStatus()
        {
            if (HSTL.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Luu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (GlobalSettings.NGON_NGU == "0")
                    label8.Text = "Đã duyệt";
                else
                    label8.Text = "Approved";
                btnSendBK.Enabled = false;
                btnHuyBK.Enabled = false;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false; ;
            }
            else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnSendBK.Enabled = false;
                btnHuyBK.Enabled = false;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false; ;
            }
            else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                if (GlobalSettings.NGON_NGU == "0")
                    label8.Text = "Chờ duyệt";
                else
                    label8.Text = "Wait for Approval";

                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnSendBK.Enabled = true;
                btnHuyBK.Enabled = true;
                sendBK1.Enabled = true;
                huyBangKe1.Enabled = true;
            }
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "HSTL";
            sendXML.master_id = HSTL.ID;
            if (sendXML.Load())
            {

                if (GlobalSettings.NGON_NGU == "0")
                    label8.Text = "Đang chờ xác nhận hồ sơ ";
                else
                    label8.Text = "Waiting for confirmation";

                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnSendBK.Enabled = false; ;
                btnHuyBK.Enabled = false;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false;
            }
        }
        private void CapNhatHoSoThanhLyForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            ccNgayThanhLy.ReadOnly = false;
            txtNgayTiepNhan.ReadOnly = true;
            txtSoHSTL.Value = this.HSTL.SoHoSo;
            txtLanThanhLy.Value = this.HSTL.LanThanhLy;
            ccNgayThanhLy.Value = this.HSTL.NgayBatDau;
            refreshTrangThai();
            dgList.DataSource = this.HSTL.BKCollection;
            bindCombobox();
            if (GlobalSettings.NGON_NGU == "1")
            {
                this.SetCombo();
            }
            this.txtSoTiepNhan.Text = HSTL.SoTiepNhan.ToString();
            if (this.HSTL.NgayTiepNhan != new DateTime(1900, 1, 1))
                this.txtNgayTiepNhan.Text = HSTL.NgayTiepNhan.ToString("dd/MM/yyyy");
            if (HSTL.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Luu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (GlobalSettings.NGON_NGU == "0")
                    label8.Text = "Đã duyệt";
                else
                    label8.Text = "Approved";

                btnSendBK.Enabled = false;
                btnHuyBK.Enabled = false;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false; ;
            }
            else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnSendBK.Enabled = false;
                btnHuyBK.Enabled = false;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false; ;
            }
            else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                if (GlobalSettings.NGON_NGU == "0")
                    label8.Text = "Chờ duyệt";
                else
                    label8.Text = "Wait for Approval";

                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnSendBK.Enabled = true;
                btnHuyBK.Enabled = true;
                sendBK1.Enabled = true;
                huyBangKe1.Enabled = true;
            }
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "HSTL";
            sendXML.master_id = HSTL.ID;
            if (sendXML.Load())
            {
                if (GlobalSettings.NGON_NGU == "0")
                    label8.Text = "Đang chờ xác nhận hồ sơ ";
                else
                    label8.Text = "Waiting for confirmation";

                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnSendBK.Enabled = false; ;
                btnHuyBK.Enabled = false;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false;
            }
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleThanhKhoan.ThanhKhoan)))
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                uiCommandBar1.Visible = false;
            }
            if (this.IsMdiChild) cmdFixAndRun1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            else cmdFixAndRun1.Visible = Janus.Windows.UI.InheritableBoolean.True;
        }
        private void SetCombo()
        {
            cbbBangKe.Items.Clear();
            cbbBangKe.Items.Add("BK03-List of Unliquidated Raw Materials", (object)"DTLNPLCHUATL");
            cbbBangKe.Items.Add("BK04-List of Destroyed and Presented Raw Materials", (object)"DTLNPLXH");
            cbbBangKe.Items.Add("BK05-List of Re-Exported Raw Materials", (object)"DTLNPLTX");
            cbbBangKe.Items.Add("BK06-List of Raw Materials of Import Trade Customs Declaration Forms", (object)"DTLNPLNKD");
            cbbBangKe.Items.Add("BK07-List of Raw Materials Begged to Pay Domestic Consumption Tax", (object)"DTLNPLNT");
            cbbBangKe.Items.Add("BK08-List of Raw Materials Exported under Export Processing Type", (object)"DTLNPLXGC");
            cbbBangKe.Items.Add("BK09-List of Raw Materials Temporary Prepaid Tax", (object)"DTLCHITIETNT");
            cbbBangKe.Items.Add("BK10-List of Self-Supply Raw Materials", (object)"DTLNPLTCU");
            cbbBangKe.Items.Sort();
            cbbBangKe.SelectedIndex = 0;
        }
        private void refreshTrangThai()
        {

            if (this.HSTL.TrangThaiThanhKhoan == 0)
            {
                cmdRollback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThanhLy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXuatBaoCao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCanDoiNX1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFixAndRun1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThaiTK.Text = "Đang nhập liệu";
                else
                    lblTrangThaiTK.Text = "Inputing data";
                txtSoHSTL.ReadOnly = false;
                ccNgayThanhLy.ReadOnly = false;
                uiButton1.Enabled = uiButton2.Enabled = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            else if (this.HSTL.TrangThaiThanhKhoan > 0 && this.HSTL.TrangThaiThanhKhoan < 400)
            {
                cmdRollback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThanhLy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXuatBaoCao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCanDoiNX1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFixAndRun1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThaiTK.Text = "Đang chạy thanh khoản";
                else
                    lblTrangThaiTK.Text = "Inputing data";


                txtSoHSTL.ReadOnly = false;
                ccNgayThanhLy.ReadOnly = false;
                uiButton1.Enabled = uiButton2.Enabled = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            else if (this.HSTL.TrangThaiThanhKhoan == 400)
            {
                cmdRollback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThanhLy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXuatBaoCao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCanDoiNX1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFixAndRun1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThaiTK.Text = "Đã chạy thanh khoản";
                else
                    lblTrangThaiTK.Text = "Liquidated";

                txtSoHSTL.ReadOnly = false;
                ccNgayThanhLy.ReadOnly = false;
                uiButton1.Enabled = uiButton2.Enabled = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            else if (this.HSTL.TrangThaiThanhKhoan == 401)
            {
                cmdRollback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThanhLy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXuatBaoCao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCanDoiNX1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFixAndRun1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThaiTK.Text = "Đã đóng hồ sơ";
                else
                    lblTrangThaiTK.Text = "Closed Liquidatation";


                txtSoHSTL.ReadOnly = true;
                ccNgayThanhLy.ReadOnly = true;
                uiButton1.Enabled = uiButton2.Enabled = false;
                dgList.AllowDelete = InheritableBoolean.False;
            }
            if (this.HSTL.LanThanhLy < this.HSTL.GetLanThanhLyMoiNhatByUserName(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN))
            {
                if (GlobalSettings.NGON_NGU == "0")
                    ThanhLy1.Text = "Chạy lại thanh khoản và đóng hồ sơ";
                else
                    ThanhLy1.Text = "Closed Liquidatation";

                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }

        }
        private void bindComboboxDelete(string maBangKe)
        {
            switch (maBangKe)
            {
                case "DTLTKN":
                    break;
                case "DTLTKX":
                    break;
                case "DTLNPLCHUATL":
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK03 - Bảng kê nguyên phụ liệu chưa thanh lý", (object)"DTLNPLCHUATL");
                    else
                        cbbBangKe.Items.Add("BK03-List of Unliquidated Raw Materials", (object)"DTLNPLCHUATL");

                    break;
                case "DTLNPLXH":
                    //cbbBangKe.Items.Add("BK04 - Bảng kê nguyên phụ liệu hủy, biếu, tặng", (object)"DTLNPLXH");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK04 - Bảng kê nguyên phụ liệu hủy, biếu, tặng", (object)"DTLNPLXH");
                    else
                        cbbBangKe.Items.Add("BK04 - List of Destroyed and Presented Raw Materials", (object)"DTLNPLXH");
                    break;
                case "DTLNPLTX":
                    //cbbBangKe.Items.Add("BK05 - Bảng kê nguyên phụ liệu tái xuất", (object)"DTLNPLTX");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK05 - Bảng kê nguyên phụ liệu tái xuất", (object)"DTLNPLTX");
                    else
                        cbbBangKe.Items.Add("BK05 - BK05-List of Re-Exported Raw Materials", (object)"DTLNPLTX");
                    break;
                case "DTLNPLNKD":
                    // cbbBangKe.Items.Add("BK06 - Bảng kê nguyên phụ liệu sử dụng tờ khai NKD", (object)"DTLNPLNKD");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK06 - Bảng kê nguyên phụ liệu sử dụng tờ khai NKD", (object)"DTLNPLNKD");
                    else
                        cbbBangKe.Items.Add("BK06 - List of Raw Materials of Import Trade Customs Declaration Forms", (object)"DTLNPLNKD");
                    break;
                case "DTLNPLNT":
                    //cbbBangKe.Items.Add("BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa", (object)"DTLNPLNT");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa", (object)"DTLNPLNT");
                    else
                        cbbBangKe.Items.Add("BK07 - List of Raw Materials Begged to Pay Domestic Consumption Tax", (object)"DTLNPLNT");
                    break;
                case "DTLNPLXGC":
                    //cbbBangKe.Items.Add("BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC", (object)"DTLNPLXGC");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC", (object)"DTLNPLXGC");
                    else
                        cbbBangKe.Items.Add("BK08 - List of Raw Materials Exported under Export Processing Type", (object)"DTLNPLXGC");
                    break;
                case "DTLCHITIETNT":
                    //cbbBangKe.Items.Add("BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế", (object)"DTLCHITIETNT");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế", (object)"DTLCHITIETNT");
                    else
                        cbbBangKe.Items.Add("BK09 - List of Raw Materials Temporary Prepaid Tax", (object)"DTLCHITIETNT");
                    break;
                default:
                    //cbbBangKe.Items.Add("BK10 - Bảng kê nguyên phụ liệu tự cung ứng", (object)"DTLNPLTCU");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK10 - Bảng kê nguyên phụ liệu tự cung ứng", (object)"DTLNPLTCU");
                    else
                        cbbBangKe.Items.Add("BK10 - List of Self-Supply Raw Materials", (object)"DTLNPLTCU");
                    break;
            }
            cbbBangKe.Items.Sort();
            cbbBangKe.SelectedValue = cbbBangKe.Items[0].Value;
        }
        private void bindCombobox()
        {
            foreach (BangKeHoSoThanhLy bk in this.HSTL.BKCollection)
            {
                switch (bk.MaBangKe)
                {
                    case "DTLTKN":
                        if (cbbBangKe.Items[(object)"DTLTKN"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLTKN"]);
                        break;
                    case "DTLTKX":
                        if (cbbBangKe.Items[(object)"DTLTKX"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLTKX"]);
                        break;
                    case "DTLNPLCHUATL":
                        if (cbbBangKe.Items[(object)"DTLNPLCHUATL"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLCHUATL"]);
                        break;
                    case "DTLNPLXH":
                        if (cbbBangKe.Items[(object)"DTLNPLXH"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLXH"]);
                        break;
                    case "DTLNPLTX":
                        if (cbbBangKe.Items[(object)"DTLNPLTX"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLTX"]);
                        break;
                    case "DTLNPLNKD":
                        if (cbbBangKe.Items[(object)"DTLNPLNKD"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLNKD"]);
                        break;
                    case "DTLNPLNT":
                        if (cbbBangKe.Items[(object)"DTLNPLNT"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLNT"]);
                        break;
                    case "DTLNPLXGC":
                        if (cbbBangKe.Items[(object)"DTLNPLXGC"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLXGC"]);
                        break;
                    case "DTLCHITIETNT":
                        if (cbbBangKe.Items[(object)"DTLCHITIETNT"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLCHITIETNT"]);
                        break;
                    default:
                        if (cbbBangKe.Items[(object)"DTLNPLTCU"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLTCU"]);
                        break;
                }

            }
            cbbBangKe.Items.Sort();
            cbbBangKe.SelectedValue = cbbBangKe.Items[0].Value;
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            switch (e.Row.Cells["MaBangKe"].Text)
            {
                case "DTLTKN":
                    BK01Form bk01 = new BK01Form();
                    int i = this.HSTL.getBKToKhaiNhap();
                    this.HSTL.BKCollection[i].LoadChiTietBangKe();
                    bk01.bkTKNCollection = this.HSTL.BKCollection[i].bkTKNCollection;
                    bk01.HSTL = this.HSTL;
                    bk01.ShowDialog();
                    break;
                case "DTLTKX":
                    BK02Form bk02 = new BK02Form();
                    this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].LoadChiTietBangKe();
                    bk02.bkTKXCollection = this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion;
                    bk02.HSTL = this.HSTL;
                    bk02.ShowDialog();
                    break;
                case "DTLNPLCHUATL":
                    BK03Form bk03 = new BK03Form();
                    this.HSTL.BKCollection[this.HSTL.getBKNPLChuaThanhLY()].LoadChiTietBangKe();
                    bk03.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLChuaThanhLY()].bkNPLCTLCollection;
                    bk03.HSTL = this.HSTL;
                    bk03.ShowDialog();
                    break;
                case "DTLNPLXH":
                    BK04Form bk04 = new BK04Form();
                    this.HSTL.BKCollection[this.HSTL.getBKNPLXinHuy()].LoadChiTietBangKe();
                    bk04.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLXinHuy()].bkNPLXHCollection;
                    bk04.HSTL = this.HSTL;
                    bk04.ShowDialog();
                    break;
                case "DTLNPLTX":
                    BK05Form bk05 = new BK05Form();
                    this.HSTL.BKCollection[this.HSTL.getBKNPLTaiXuat()].LoadChiTietBangKe();
                    bk05.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLTaiXuat()].bkNPLTXCollection;
                    bk05.HSTL = this.HSTL;
                    bk05.ShowDialog();
                    break;
                case "DTLNPLNKD":
                    BK06Form bk06 = new BK06Form();
                    this.HSTL.BKCollection[this.HSTL.getBKNPLNhapKinhDoanh()].LoadChiTietBangKe();
                    bk06.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLNhapKinhDoanh()].bkNPLNKDCollection;
                    bk06.HSTL = this.HSTL;
                    bk06.ShowDialog();
                    break;
                case "DTLNPLNT":
                    BK07Form bk07 = new BK07Form();
                    this.HSTL.BKCollection[this.HSTL.getBKNPLNopThue()].LoadChiTietBangKe();
                    bk07.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLNopThue()].bkNPLNTCollection;
                    bk07.HSTL = this.HSTL;
                    bk07.ShowDialog();
                    break;
                case "DTLNPLXGC":
                    BK08Form bk08 = new BK08Form();
                    this.HSTL.BKCollection[this.HSTL.getBKNPLXuatGiaCong()].LoadChiTietBangKe();
                    bk08.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLXuatGiaCong()].bkNPLXGCCollection;
                    bk08.HSTL = this.HSTL;
                    bk08.ShowDialog();
                    break;
                case "DTLCHITIETNT":
                    BK09Form bk09 = new BK09Form();
                    this.HSTL.BKCollection[this.HSTL.getBKNPLTamNopThue()].LoadChiTietBangKe();
                    bk09.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLTamNopThue()].bkNPLTNTCollection;
                    bk09.HSTL = this.HSTL;
                    bk09.ShowDialog();
                    break;
                default:
                    BK10Form bk10 = new BK10Form();
                    this.HSTL.BKCollection[this.HSTL.getBKNPLTuCungUng()].LoadChiTietBangKe();
                    bk10.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLTuCungUng()].bkNPLTCUCollection;
                    bk10.HSTL = this.HSTL;
                    bk10.ShowDialog();
                    break;
            }
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            switch (e.Row.Cells["MaBangKe"].Text)
            {
                case "DTLTKN":
                    //e.Row.Cells["TenBangKe"].Text = "BK01 - Bảng kê tờ khai nhập khẩu thanh lý";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK01 - Bảng kê tờ khai nhập khẩu thanh lý";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK01 - List of Unliquidated  import declaration materials";
                    break;
                case "DTLTKX":
                    //e.Row.Cells["TenBangKe"].Text = "BK02 - Bảng kê tờ khai xuất khẩu thanh lý";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK02 - Bảng kê tờ khai xuất khẩu thanh lý";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK02 - List of Unliquidated  export declaration materials";
                    break;
                case "DTLNPLCHUATL":
                    //e.Row.Cells["TenBangKe"].Text = "BK03 - Bảng kê nguyên phụ liệu chưa thanh lý";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK03 - Bảng kê nguyên phụ liệu chưa thanh lý";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK03 - List of Unliquidated Raw Materials";
                    break;
                case "DTLNPLXH":
                    //e.Row.Cells["TenBangKe"].Text = "BK04 - Bảng kê nguyên phụ liệu hủy, biếu, tặng";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK04 - Bảng kê nguyên phụ liệu hủy, biếu, tặng";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK04 - List of Destroyed and Presented Raw Materials";
                    break;
                case "DTLNPLTX":
                    //e.Row.Cells["TenBangKe"].Text = "BK05 - Bảng kê nguyên phụ liệu tái xuất";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK05 - Bảng kê nguyên phụ liệu tái xuất";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK05 - List of Re-Exported Raw Materials";
                    break;
                case "DTLNPLNKD":
                    // e.Row.Cells["TenBangKe"].Text = "BK06 - Bảng kê nguyên phụ liệu xuất sử dụng tờ khai NKD";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK06 - Bảng kê nguyên phụ liệu xuất sử dụng tờ khai NKD";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK06 - List of Raw Materials of Import Trade Customs Declaration Forms";
                    break;
                case "DTLNPLNT":
                    //e.Row.Cells["TenBangKe"].Text = "BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK07 - List of Raw Materials Begged to Pay Domestic Consumption Tax";
                    break;
                case "DTLNPLXGC":
                    //e.Row.Cells["TenBangKe"].Text = "BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK08 - List of Raw Materials Exported under Export Processing Type";
                    break;
                case "DTLCHITIETNT":
                    //e.Row.Cells["TenBangKe"].Text = "BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK09 - List of Raw Materials Temporary Prepaid Tax";
                    break;
                default:
                    //e.Row.Cells["TenBangKe"].Text = "BK10 - Bảng kê nguyên phụ liệu tự cung ứng";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK10 - Bảng kê nguyên phụ liệu tự cung ứng";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK10 - List of Self-Supply Raw Materials";
                    break;
            }
            switch (e.Row.Cells["TrangThaiXL"].Value.ToString())
            {
                case "1":
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TrangThaiXL"].Text = "Đã khai báo";
                    else
                        e.Row.Cells["TrangThaiXL"].Text = "Declarated";

                    break;
                case "0":
                    // e.Row.Cells["TrangThaiXL"].Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TrangThaiXL"].Text = "Chưa khai báo";
                    else
                        e.Row.Cells["TrangThaiXL"].Text = "Not declarated yet";
                    break;
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            switch (cbbBangKe.SelectedValue.ToString())
            {
                case "DTLTKN":
                    break;
                case "DTLTKX":
                    break;
                case "DTLNPLCHUATL":
                    BK03Form bk03 = new BK03Form();
                    bk03.HSTL = this.HSTL;
                    bk03.ShowDialog();
                    this.HSTL = bk03.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLXH":
                    BK04Form bk04 = new BK04Form();
                    bk04.HSTL = this.HSTL;
                    bk04.ShowDialog();
                    this.HSTL = bk04.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLTX":
                    BK05Form bk05 = new BK05Form();
                    bk05.HSTL = this.HSTL;
                    bk05.ShowDialog();
                    this.HSTL = bk05.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLNKD":
                    BK06Form bk06 = new BK06Form();
                    bk06.HSTL = this.HSTL;
                    bk06.ShowDialog();
                    this.HSTL = bk06.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLNT":
                    BK07Form bk07 = new BK07Form();
                    bk07.HSTL = this.HSTL;
                    bk07.ShowDialog();
                    this.HSTL = bk07.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLXGC":
                    BK08Form bk08 = new BK08Form();
                    bk08.HSTL = this.HSTL;
                    bk08.ShowDialog();
                    this.HSTL = bk08.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLCHITIETNT":
                    BK09Form bk09 = new BK09Form();
                    bk09.HSTL = this.HSTL;
                    bk09.ShowDialog();
                    this.HSTL = bk09.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                default:
                    BK10Form bk10 = new BK10Form();
                    bk10.HSTL = this.HSTL;
                    bk10.ShowDialog();
                    this.HSTL = bk10.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            //Nhận dữ liệu HSTL từ hải quan
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {

            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaBangKe"].Text == "DTLTKN" || e.Row.Cells["MaBangKe"].Text == "DTLTKX")
                {
                    ShowMessage(setText("Bảng kê BK01 và BK02 không xóa được.", "BK01,BK02 list can not delete?"), false);
                    e.Cancel = true;
                    return;
                }
                else
                {
                    if (ShowMessage(setText("Bạn có muốn xóa bảng kê này không?", "Do you want to delete this list ?"), true) != "Yes")
                    // if (MLMessages("Bạn có muốn xóa bảng kê này không?", "MSG_DEL01", "", true) != "Yes")
                    {
                        e.Cancel = true;
                        return;
                    }
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.ID = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    bk.Delete();
                    bindComboboxDelete(e.Row.Cells["MaBangKe"].Text);

                }
            }
        }
        private void refreshSTTHang()
        {
            for (int i = 0; i < this.HSTL.BKCollection.Count; i++)
            {
                this.HSTL.BKCollection[i].STTHang = i + 1;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.HSTL.NgayBatDau = ccNgayThanhLy.Value;
            this.HSTL.SoHoSo = Convert.ToInt32(txtSoHSTL.Value);
            this.HSTL.Update();
            refreshSTTHang();
            bool r = new BangKeHoSoThanhLy().InsertUpdate(this.HSTL.BKCollection);
            if (r)
                //ShowMessage("Cập nhật hồ sơ thanh lý thành công.", false);
                MLMessages("Lưu hồ sơ thanh lý thành công.", "MSG_SAV02", "", false);
            else
                //ShowMessage("Cập nhật không thành công.", false);
                MLMessages("Lưu hồ sơ thanh lý không thành công.", "MSG_SAV01", "", false);
        }
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "HSTL";
                sendXML.master_id = HSTL.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = HSTL.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    //ShowMessage("Chưa có phản hồi từ hải quan.", false);
                    MLMessages("Chưa có phản hồi từ hải quan.", "MSG_THK88", "", false);

                    return;
                }
                if (sendXML.func == 1)
                {
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HSTL.SoTiepNhan, false);
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HSTL.SoTiepNhan, "MSG_THK20", "" + HSTL.SoTiepNhan, false);
                    txtSoTiepNhan.Text = HSTL.SoTiepNhan.ToString();
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        label8.Text = "Chờ duyệt";
                    }
                    else
                    {
                        label8.Text = "Wait for approval";
                    }
                }
                else if (sendXML.func == 3)
                {
                    //ShowMessage("Hủy khai báo thành công", false);
                    MLMessages("Hủy khai báo thành công.", "MSG_THK21", "", false);
                    txtSoTiepNhan.Text = "0";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        label8.Text = "Chưa khai báo";
                    }
                    else
                    {
                        label8.Text = "Not yet declarated";
                    }

                }
                //else if (sendXML.func == 2)
                //{
                //    if (HSTL.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                //    {
                //        this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                //    }
                //    else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                //    {
                //        this.ShowMessage("Hải quan chưa duyệt danh sách này", false);
                //    }
                //    else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                //    {
                //        this.ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                //    }
                //}
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Có lỗi do hệ thống phía hải quan.", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = HSTL.ID;
                                //hd.LoaiToKhai = LoaiToKhai.HO_SO_THANH_KHOAN;
                                //hd.TrangThai = HSTL.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //hd.PassWord = pass;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            if (sendXML.func == 1)
                            {
                                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            }
                            else if (sendXML.func == 3)
                            {
                                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            }

                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void cmdSend_Click(object sender, EventArgs e)
        {
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                xmlCurrent = HSTL.WSSendHoSoThanhLy(wsForm.txtMatKhau.Text.Trim());
                this.Cursor = Cursors.Default;
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = "HSTL";
                sendXML.master_id = HSTL.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                LayPhanHoi(wsForm.txtMatKhau.Text.Trim());
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = HSTL.ID;
                                //hd.LoaiToKhai = LoaiToKhai.HO_SO_THANH_KHOAN;
                                //hd.TrangThai = HSTL.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.KHAI_BAO;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {

                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo hồ sơ thanh lý. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoiBangKe(string pass)
        {
            MsgSend sendXML = new MsgSend();
            int i = 0;
            this.Cursor = Cursors.WaitCursor;
            string st = "";
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                ++i;
                try
                {
                    sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                    switch (sendXML.LoaiHS)
                    {
                        case "DTLTKN":
                            st = "Danh sách tờ khai nhập";
                            break;
                        case "DTLTKX":
                            st = "Danh sách tờ khai xuất";
                            break;
                        case "DTLNPLCHUATL":
                            st = "Danh sách nguyên phụ liệu chưa thanh lý";
                            break;
                        case "DTLNPLXH":
                            break;
                        case "DTLNPLTX":
                            st = "Danh sách nguyên phụ liệu tái xuất";
                            break;
                        case "DTLNPLNKD":
                            st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                            break;
                        case "DTLNPLNT":
                            st = "Danh sách nguyên phụ liệu nộp thuế";
                            break;
                        case "DTLNPLXGC":
                            st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                            break;
                        case "DTLCHITIETNT":
                            st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                            break;
                    }
                    sendXML.master_id = HSTL.ID;
                    if (!sendXML.Load())
                    {
                        continue;
                    }
                    xmlCurrent = HSTL.LayPhanHoiBangKe(pass, sendXML.msg);
                    if (xmlCurrent != "")
                        --i;
                    else
                        sendXML.Delete();
                }
                catch (Exception ex)
                {
                    --i;
                    this.Cursor = Cursors.Default;
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                                {
                                }
                                else
                                    break;
                            }
                            else
                            {
                                ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                                sendXML.Delete();
                            }
                        }
                        else
                        {

                            ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                        }
                        #endregion FPTService
                    }
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
            }
            if (i > 0)
                //  ShowMessage("Thực hiện thành công " + i.ToString() + " bảng kê", false);
                MLMessages("Thực hiện thành công " + i.ToString() + " bảng kê", "MSG_THK23", "", false);
            else
                //  ShowMessage("Chưa có phản hồi từ hải quan", false);
                MLMessages("Chưa có phản hồi từ hải quan.", "MSG_THK88", "", false);
            this.HSTL.LoadBKCollection();
            dgList.DataSource = HSTL.BKCollection;
            this.Cursor = Cursors.Default;
        }
        private void LayPhanHoiBangKe()
        {
            MsgSend sendXML = new MsgSend();
            if (dgList.GetCheckedRows().Length == 0)
            {
                // ShowMessage("Chưa chọn bảng kê để gửi.", false);
                MLMessages("Chưa chọn bảng kê để gửi.", "MSG_THK57", "", false);
                return;
            }
            if (HSTL.SoTiepNhan == 0)
            {
                // ShowMessage("Chưa đăng ký số hồ sơ thanh lý.", false);
                MLMessages("Chưa đăng ký số hồ sơ thanh lý.", "MSG_THK58", "", false);
                return;
            }
            //Kiểm tra xem có bảng kê nào đã gửi nhưng chưa có phản hồi

            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            int i = 0;
            string st = "";
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                ++i;
                try
                {
                    sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                    switch (sendXML.LoaiHS)
                    {
                        case "DTLTKN":
                            st = "Danh sách tờ khai nhập";
                            break;
                        case "DTLTKX":
                            st = "Danh sách tờ khai xuất";
                            break;
                        case "DTLNPLCHUATL":
                            st = "Danh sách nguyên phụ liệu chưa thanh lý";
                            break;
                        case "DTLNPLXH":
                            break;
                        case "DTLNPLTX":
                            st = "Danh sách nguyên phụ liệu tái xuất";
                            break;
                        case "DTLNPLNKD":
                            st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                            break;
                        case "DTLNPLNT":
                            st = "Danh sách nguyên phụ liệu nộp thuế";
                            break;
                        case "DTLNPLXGC":
                            st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                            break;
                        case "DTLCHITIETNT":
                            st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                            break;
                    }
                    sendXML.master_id = HSTL.ID;
                    if (!sendXML.Load())
                    {
                        continue;
                    }
                    xmlCurrent = HSTL.LayPhanHoiBangKe(wsForm.txtMatKhau.Text, sendXML.msg);
                    if (xmlCurrent != "")
                        --i;
                    else
                        sendXML.Delete();
                }
                catch (Exception ex)
                {
                    --i;
                    this.Cursor = Cursors.Default;
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                                {
                                }
                                else
                                    break;
                            }
                            else
                            {
                                ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                                sendXML.Delete();
                            }
                        }
                        else
                        {

                            ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                        }
                        #endregion FPTService
                    }
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
            }
            if (i > 0)
                // ShowMessage("Thực hiện thành công " + i.ToString() + " bảng kê", false);
                MLMessages("Thực hiện thành công " + i.ToString() + " bảng kê", "MSG_THK23", "", false);
            else
                // ShowMessage("Chưa có phản hồi từ hải quan", false);
                MLMessages("Chưa có phản hồi từ hải quan.", "MSG_THK88", "", false);
            this.HSTL.LoadBKCollection();
            dgList.DataSource = HSTL.BKCollection;
            this.Cursor = Cursors.Default;
        }
        private void LayPhanHoiMotBangKe(string mabangke, string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = mabangke;
                sendXML.master_id = HSTL.ID;
                if (!sendXML.Load())
                {
                    // ShowMessage("Bảng kê này chưa gửi thông tin tới hải quan.", false);
                    MLMessages("Bảng kê này chưa gửi thông tin tới hải quan.", "MSG_THK89", "", false);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = HSTL.LayPhanHoiBangKe(pass, sendXML.msg);
                if (xmlCurrent != "")
                {
                    //ShowMessage("Chưa có phản hồi từ hải quan.", false);
                    MLMessages("Chưa có phản hồi từ hải quan.", "MSG_THK88", "", false);
                    return;
                }
                if (sendXML.func == 1)
                {
                    //ShowMessage("Bảng kê đã được khai báo.", false);        
                    MLMessages("Bảng kê đã được khai báo.", "MSG_THK90", "", false);
                }
                else if (sendXML.func == 3)
                {
                    //ShowMessage("Hủy bảng kê thành công", false);
                    MLMessages("Hủy bảng kê thành công", "MSG_THK62", "", false);
                }
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công.\nCó lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                            {
                            }
                            else
                                return;
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {

                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            this.HSTL.LoadBKCollection();
            dgList.DataSource = HSTL.BKCollection;
            this.Cursor = Cursors.Default;
        }
        private void LayPhanHoiHoSo()
        {
            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            try
            {
                sendXML.LoaiHS = "HSTL";
                sendXML.master_id = HSTL.ID;
                if (!sendXML.Load())
                {
                    // ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_SEN03", "", false);
                    return;
                }
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = HSTL.LayPhanHoi(wsForm.txtMatKhau.Text.Trim(), sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    ShowMessage("Chưa có phản hồi từ hải quan.", false);
                    return;
                }
                if (sendXML.func == 1)
                {
                    ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HSTL.SoTiepNhan, false);
                    txtSoTiepNhan.Text = HSTL.SoTiepNhan.ToString();
                    label8.Text = "Chờ duyệt";
                }
                else if (sendXML.func == 3)
                {
                    ShowMessage("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "0";
                    label8.Text = "Chưa khai báo";

                }
                else if (sendXML.func == 2)
                {
                    if (HSTL.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                    }
                    else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        this.ShowMessage("Hải quan chưa duyệt danh sách này", false);
                    }
                    else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        this.ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
                this.HSTL.LoadBKCollection();
                dgList.DataSource = HSTL.BKCollection;
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = HSTL.ID;
                                //hd.LoaiToKhai = LoaiToKhai.HO_SO_THANH_KHOAN;
                                //hd.TrangThai = HSTL.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //hd.PassWord = wsForm.txtMatKhau.Text.Trim();
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            if (sendXML.func == 1)
                            {
                                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            }
                            else if (sendXML.func == 3)
                            {
                                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            }
                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void XacNhanThongTin()
        {

            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "HSTL";
            sendXML.master_id = HSTL.ID;
            if (!sendXML.Load())
            {
                LayPhanHoiBangKe();
            }
            else
                LayPhanHoiHoSo();
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            switch (e.Command.Key)
            {
                case "Huy":
                    this.HuyHoSoKhaiBao();
                    break;
                case "XacNhan":
                    this.XacNhanThongTin();
                    break;
                case "KhaiBao":
                    this.cmdSend_Click(null, null);
                    break;
                case "Luu":
                    this.btnSave_Click(null, null);
                    break;
                case "ThanhLy":
                    if (MainForm.versionHD != 2)
                    {
                        try
                        {
                            string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                            if (st != "")
                            {
                                ShowMessage(st, false);
                                this.Cursor = Cursors.Default;
                                return;
                            }

                        }
                        catch { }
                    }
                    this.HSTL.Load();
                    refreshTrangThai();
                    if (this.HSTL.TrangThaiThanhKhoan == 401)
                    {
                        ShowMessage("Hồ sơ đã đóng, bạn không thể chạy thanh khoản.", false);
                        break;
                    }
                    this.ThanhLy();
                    break;
                case "cmdThanhKhoan":
                    if (MainForm.versionHD != 2)
                    {
                        try
                        {
                            string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                            if (st != "")
                            {
                                ShowMessage(st, false);
                                this.Cursor = Cursors.Default;
                                return;
                            }

                        }
                        catch { }
                    }
                    this.HSTL.Load();
                    refreshTrangThai();
                    if (this.HSTL.TrangThaiThanhKhoan == 401)
                    {
                        ShowMessage("Hồ sơ đã đóng, bạn không thể chạy thanh khoản.", false);
                        break;
                    }
                    this.ThanhLy();
                    break;
                case "cmdFixAndRun":
                    try
                    {
                        string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                        if (st != "")
                        {
                            ShowMessage(st, false);
                            this.Cursor = Cursors.Default;
                            return;
                        }

                    }
                    catch { }
                    this.HSTL.Load();
                    refreshTrangThai();
                    if (this.HSTL.TrangThaiThanhKhoan == 401)
                    {
                        ShowMessage("Hồ sơ đã đóng, bạn không thể fix dữ liệu và chạy thanh khoản.", false);
                        break;
                    }
                    this.FixDuLieuVaThanhLy();
                    break;
                case "cmdBCNPLXuatNhapTon":
                    this.ShowBKNPLXuatNhapKhau();
                    break;
                case "cmdBCThueXNK":
                    this.ShowBKThueXNK();
                    break;
                case "cmdBKToKhaiXuat":
                    this.ShowBKToKhaiXuat();
                    break;
                case "cmdBKToKhaiNhap":
                    this.ShowBKToKhaiNhap();
                    break;
                case "cmdPrint":
                    this.ShowBCThongTu59();
                    break;
                case "cmdTT79KCX":
                    this.ShowBCThongTu79KCX();
                    break;
                case "cmdRollback":
                    try
                    {

                        // if (ShowMessage("Bạn có muốn rollback không, khi rollback thì dữ liệu tồn sẽ thay đổi.", true) == "Yes")
                        if (MLMessages("Bạn có muốn rollback không, khi rollback thì dữ liệu tồn sẽ thay đổi.", "MSG_THK91", "", true) == "Yes")
                        {

                            this.HSTL.RollBackNhieuHSTK();
                            this.HSTL.Load();
                            refreshTrangThai();
                            //ShowMessage("Hồ sơ đã rollback thành công.", false);
                            MLMessages("Hồ sơ đã rollback thành công.", "MSG_THK92", "", false);
                        }
                    }
                    catch (Exception ex)
                    {
                        //ShowMessage("Hồ sơ rollback không thành công.\n Lỗi :" + ex.Message, false);
                        MLMessages("Hồ sơ rollback không thành công.\n Lỗi :" + ex.Message, "MSG_THK93", "", false);
                    }
                    break;
                case "cmdClose":
                    DongHSTLForm f1 = new DongHSTLForm();
                    f1.HSTL = this.HSTL;
                    f1.ShowDialog();
                    this.HSTL = f1.HSTL;
                    refreshTrangThai();
                    txtSoHSTL.Value = this.HSTL.SoHoSo;
                    ccNgayThanhLy.Value = this.HSTL.NgayBatDau;
                    this.HSTL.Update();
                    break;
                case "cmdCanDoiNX":
                    CanDoiNhapXuatForm fff = new CanDoiNhapXuatForm();
                    fff.HSTL = this.HSTL;
                    fff.Show();
                    break;
                case "cmdChungTuThanhToan":
                    ShowChungTuThanhToanForm();
                    break;
                case "cmdXuatBaoCao":
                    ShowBCThongTu929();
                    break;
                case "cmdBKTKNKNL":
                    ShowBangKeToKhaiNhapKhauNPL();
                    break;
                case "cmdBKTKXKSP":
                    ShowBangKeToKhaiXuatKhauSP();
                    break;
                case "cmdBCNLSXXK":
                    ShowBaoCaoNPLSXXK();
                    break;
                case "cmdBCXNTNL":
                    ShowBaoCaoXuatNhapTonNPL();
                    break;
                case "cmdBCTTNLNK":
                    ShowBaoCaoTinhThueNPLNhapKhau();
                    break;
            }
            this.Cursor = Cursors.Default;
        }

        private void ShowBaoCaoTinhThueNPLNhapKhau()
        {

        }

        private void ShowBaoCaoXuatNhapTonNPL()
        {

        }

        private void ShowBaoCaoNPLSXXK()
        {
            BCNPLSXXK_929 BCNPLSXXK = new BCNPLSXXK_929();
            BCNPLSXXK.LanThanhLy = this.HSTL.LanThanhLy;
            BCNPLSXXK.SoHoSo = this.HSTL.SoHoSo;
            BCNPLSXXK.BindReport("");
            BCNPLSXXK.ShowPreview();
        }

        private void ShowBangKeToKhaiXuatKhauSP()
        {
            BangKeToKhaiXuat_929 BKToKhaiXuatKhau = new BangKeToKhaiXuat_929();
            BKToKhaiXuatKhau.SoHoSo = this.HSTL.SoHoSo;
            BKToKhaiXuatKhau.BindReport(this.HSTL.LanThanhLy);
            BKToKhaiXuatKhau.ShowPreview();
        }

        private void ShowBangKeToKhaiNhapKhauNPL()
        {
            BangKeToKhaiNhap_929 BKToKhaiNhapKhau = new BangKeToKhaiNhap_929();
            BKToKhaiNhapKhau.LanThanhLy = this.HSTL.LanThanhLy;
            BKToKhaiNhapKhau.SoHoSo = this.HSTL.SoHoSo;
            BKToKhaiNhapKhau.BindReport("");
            BKToKhaiNhapKhau.ShowPreview();
        }

        private void ShowBCThongTu929()
        {
            if (GlobalSettings.SoThapPhan.TachLam2 == 1)
            {
                ChonToKhaiNhapForm1 ChonToKhai = new ChonToKhaiNhapForm1();
                ChonToKhai.HSTL = this.HSTL;
                ChonToKhai.ShowDialog();
                PreviewFormBC929 XuatDoi = new PreviewFormBC929();
                XuatDoi.LanThanhLy = this.HSTL.LanThanhLy;
                XuatDoi.SoHoSo = this.HSTL.SoHoSo;
                XuatDoi.TKNHoanThueCollection = ChonToKhai.TKNHoanThueCollection;
                XuatDoi.TKNKhongThuCollection = ChonToKhai.TKNKhongThuCollection;
                XuatDoi.Show();
            }
            else
            {
                PreviewForm929 XuatDon = new PreviewForm929();
                XuatDon.LanThanhLy = this.HSTL.LanThanhLy;
                XuatDon.SoHoSo = this.HSTL.SoHoSo;
                XuatDon.Show();
            }
        }

        private void ShowChungTuThanhToanForm()
        {
            ChungTuThanhToanForm f = new ChungTuThanhToanForm();
            f.HSTL = this.HSTL;
            f.Show();
        }

        private void ShowBCThongTu59()
        {
            if (GlobalSettings.SoThapPhan.TachLam2 == 1)
            {
                ChonToKhaiNhapForm1 f = new ChonToKhaiNhapForm1();
                f.HSTL = this.HSTL;
                f.ShowDialog();
                PreviewFormITG f1 = new PreviewFormITG();
                f1.LanThanhLy = this.HSTL.LanThanhLy;
                f1.SoHoSo = this.HSTL.SoHoSo;
                f1.TKNHoanThueCollection = f.TKNHoanThueCollection;
                f1.TKNKhongThuCollection = f.TKNKhongThuCollection;
                f1.Show();
            }
            else
            {
                PreviewForm f = new PreviewForm();
                f.LanThanhLy = this.HSTL.LanThanhLy;
                f.SoHoSo = this.HSTL.SoHoSo;
                f.Show();
            }
        }
        private void ShowBCThongTu79KCX()
        {

            PreviewForm_KCX f = new PreviewForm_KCX();
            f.LanThanhLy = this.HSTL.LanThanhLy;
            f.SoHoSoString = HSTL.getSoHoSo(this.HSTL.ID);
            f.SoHoSo = this.HSTL.SoHoSo;

            foreach (BangKeHoSoThanhLy bk in BangKeHoSoThanhLy.SelectCollectionBy_MaterID(this.HSTL.ID))
            {
                if (bk.MaBangKe.Contains("TKN"))
                {
                    f.BangKeNhapID = bk.ID;
                    break;
                }
            }
            f.Show();

        }

        private void ShowBKToKhaiNhap()
        {
            Company.Interface.Report.SXXK.BangKeToKhaiNhap bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiNhap();
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport("");
            bc1.ShowPreview();
        }

        private void ShowBKToKhaiXuat()
        {
            //Company.Interface.Report.SXXK.BangKeToKhaiXuat bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiXuat();
            //Hungtq update 16/02/2011.
            Company.Interface.Report.SXXK.BangKeToKhaiXuat_TT194 bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiXuat_TT194();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport(this.HSTL.LanThanhLy);
            bc1.ShowPreview();
        }

        private void ShowBKThueXNK()
        {
            //Company.Interface.Report.SXXK.BCThueXNK bc1 = new Company.Interface.Report.SXXK.BCThueXNK();
            //Hungtq update 16/02/2011.
            Company.Interface.Report.SXXK.BCThueXNK_TT194 bc1 = new Company.Interface.Report.SXXK.BCThueXNK_TT194();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.BindReport("");
            bc1.ShowPreview();
        }

        private void ShowBKNPLXuatNhapKhau()
        {
            //BCNPLXuatNhapTon bc = new BCNPLXuatNhapTon();
            //Hungtq update 16/02/2011.
            BCNPLXuatNhapTon_TT194 bc = new BCNPLXuatNhapTon_TT194();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.BindReport("");
            bc.ShowPreview();
        }

        private void FixDuLieuVaThanhLy()
        {

            DataSet ds = this.HSTL.GetDSSPChuaCoDM();
            if (ds.Tables[0].Rows.Count > 0)
            {
                //   ShowMessage("Có " + ds.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", false);
                MLMessages("Có " + ds.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", "MSG_THK94", "", false);
                SanPhamChuaCoDMForm f = new SanPhamChuaCoDMForm();
                f.ds = ds;
                f.ShowDialog();
                this.HSTL.TrangThaiThanhKhoan = 100;
                refreshTrangThai();
            }
            else
            {
                if (this.HSTL.LanThanhLy < this.HSTL.GetLanThanhLyMoiNhatByUserName(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN))
                {
                    try
                    {
                        this.HSTL.FixDuLieuThanhKhoan();
                        ChayLaiThanhLyForm f = new ChayLaiThanhLyForm();
                        f.HSTL = this.HSTL;
                        f.ShowDialog();
                        this.HSTL = f.HSTL;
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi: " + ex.Message, false);
                        return;
                    }
                }
                else
                {
                    try
                    {
                        this.HSTL.FixDuLieuThanhKhoan();
                        ChayThanhLyForm f = new ChayThanhLyForm();
                        f.HSTL = this.HSTL;
                        f.ShowDialog();
                        this.HSTL = f.HSTL;
                        this.HSTL.Update();
                    }
                    catch (Exception ex1)
                    {
                        ShowMessage("Lỗi: " + ex1.Message, false);
                        return;
                    }

                }
                refreshTrangThai();
            }
        }
        private void ThanhLy()
        {
            int count = HSTL.CheckHSTKTruocDoDaDong();
            if (count > 0)
            {
                ShowMessage("Có " + count + " bộ hồ sơ trước đó chưa đóng, không thể sửa bộ hiện tại.", false);
                return;
            }
            DataSet ds = this.HSTL.GetDSSPChuaCoDM();
            if (ds.Tables[0].Rows.Count > 0)
            {
                //   ShowMessage("Có " + ds.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", false);
                MLMessages("Có " + ds.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", "MSG_THK94", "", false);
                SanPhamChuaCoDMForm f = new SanPhamChuaCoDMForm();
                f.ds = ds;
                f.ShowDialog();
                this.HSTL.TrangThaiThanhKhoan = 100;
                refreshTrangThai();
            }
            else
            {
                //try
                //{
                if (this.HSTL.LanThanhLy < this.HSTL.GetLanThanhLyMoiNhatByUserName(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN))
                {
                    ChayLaiThanhLyForm f = new ChayLaiThanhLyForm();
                    f.HSTL = this.HSTL;
                    f.ShowDialog();
                    //this.HSTL.ChayThanhLy(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK);
                    //ShowMessage("Chạy thanh khoản thành công.", false);
                    this.HSTL = f.HSTL;
                }
                else
                {
                    ChayThanhLyForm f = new ChayThanhLyForm();
                    f.HSTL = this.HSTL;
                    f.ShowDialog();
                    //this.HSTL.ChayThanhLy(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK);
                    //ShowMessage("Chạy thanh khoản thành công.", false);
                    //this.HSTL = f.HSTL;
                    this.HSTL.Update();
                }
                //}
                //catch (Exception ex)
                //{
                //    ShowMessage("Lỗi: " + ex.Message, false);
                //}
                //finally
                //{
                //    this.Cursor = Cursors.Default;
                //}
                refreshTrangThai();
            }
        }
        private void HuyHoSoKhaiBao()
        {
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                xmlCurrent = HSTL.HuyKhaiBaoHSTL1(wsForm.txtMatKhau.Text.Trim());
                this.Cursor = Cursors.Default;

                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = "HSTL";
                sendXML.master_id = HSTL.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnSendBK.Enabled = false; ;
                btnHuyBK.Enabled = false;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false;
                LayPhanHoi(wsForm.txtMatKhau.Text.Trim());
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = HSTL.ID;
                                //hd.LoaiToKhai = LoaiToKhai.HO_SO_THANH_KHOAN;
                                //hd.TrangThai = HSTL.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy khai báo hồ sơ thanh lý . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.HSTL.TrangThaiThanhKhoan == 401)
                {
                    // ShowMessage("Hồ sơ đã đóng bạn không được xoá.", false);
                    MLMessages("Hồ sơ đã đóng bạn không được xoá.", "MSG_THK95", "", false);
                    return;
                }
                if (ShowMessage(setText("Bạn có muốn xóa hồ sơ thanh khoản không?", "Do you want to delete this liquidation document ?"), true) == "Yes")
                //  if (MLMessages("Bạn có muốn xóa hồ sơ thanh khoản không?", "MSG_DEL01", "", true) == "Yes")
                {
                    this.HSTL.Delete();
                    // if (ShowMessage("Bạn có muốn tạo mới hồ sơ không?", true) == "Yes")
                    if (MLMessages("Bạn có muốn tạo mới hồ sơ không?", "MSG_THK96", "", true) == "Yes")
                    {
                        Form[] forms = this.ParentForm.MdiChildren;
                        for (int i = 0; i < forms.Length; i++)
                        {
                            if (forms[i].Name.ToString().Equals("TaoHoSoThanhLyForm"))
                            {
                                forms[i].Activate();
                                return;
                            }
                        }

                        TaoHoSoThanhLyForm f = new TaoHoSoThanhLyForm();
                        f.MdiParent = this.ParentForm;
                        f.Show();
                    }
                    this.Close();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnSendBK_Click(object sender, EventArgs e)
        {
            if (dgList.GetCheckedRows().Length == 0)
            {
                ShowMessage("Chưa chọn bảng kê để gửi.", false);
                return;
            }
            if (HSTL.SoTiepNhan == 0)
            {
                ShowMessage("Chưa đăng ký số hồ sơ thanh lý.", false);
                return;
            }
            //Kiểm tra xem có bảng kê nào đã gửi nhưng chưa có phản hồi
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                sendXML.master_id = HSTL.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đang chờ phản hồi.", false);
                    return;
                }
                else
                {
                    if (row.Cells["trangthaixl"].Value.ToString() == "1")
                    {
                        ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đã được khai báo.", false);
                        return;
                    }
                }
            }
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            string st = "";
            int i = 0;

            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                ++i;
                try
                {
                    switch (row.Cells["mabangke"].Text.Trim())
                    {
                        case "DTLTKN":
                            xmlCurrent = HSTL.WSSendBKToKhaiNhap1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách tờ khai nhập";
                            break;
                        case "DTLTKX":
                            xmlCurrent = HSTL.WSSendBKToKhaiXuat1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách tờ khai xuất";
                            break;
                        case "DTLNPLCHUATL":
                            xmlCurrent = HSTL.WSSendBKNPLChuaThanhLy1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu chưa thanh lý";
                            break;
                        case "DTLNPLXH":
                            xmlCurrent = HSTL.WSSendBKNPLHuy1(wsForm.txtMatKhau.Text.Trim());
                            break;
                        case "DTLNPLTX":
                            xmlCurrent = HSTL.WSSendBKNPLTaiXuat1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu tái xuất";
                            break;
                        case "DTLNPLNKD":
                            xmlCurrent = HSTL.WSSendBKNPLNhapKinhDoanh1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                            break;
                        case "DTLNPLNT":
                            xmlCurrent = HSTL.WSSendBKNPLNopThue1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu nộp thuế";
                            break;
                        case "DTLNPLXGC":
                            xmlCurrent = HSTL.WSSendBKNPLXuatGiaCong1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                            break;
                        case "DTLCHITIETNT":
                            xmlCurrent = HSTL.WSSendBKNPLTamNopThue1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                            break;
                    }
                    if (xmlCurrent != "")
                    {
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                        sendXML.master_id = HSTL.ID;
                        sendXML.msg = xmlCurrent;
                        sendXML.func = 1;
                        xmlCurrent = "";
                        sendXML.InsertUpdate();
                    }
                }
                catch (Exception ex)
                {
                    --i;
                    this.Cursor = Cursors.Default;
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                                {
                                }
                                else
                                    break;
                            }
                            else
                            {
                                ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {

                            ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                        }
                        #endregion FPTService
                    }
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
            }
            if (i > 0)
            {
                LayPhanHoiBangKe(wsForm.txtMatKhau.Text.Trim());
            }
            else
                ShowMessage("Không gửi thông tin bảng kê được.", false);
            this.HSTL.LoadBKCollection();
            dgList.DataSource = this.HSTL.BKCollection;
            this.Cursor = Cursors.Default;
        }

        private void btnHuyBK_Click(object sender, EventArgs e)
        {
            if (dgList.GetCheckedRows().Length == 0)
            {
                ShowMessage("Chưa chọn bảng kê để gửi.", false);
                return;
            }
            if (HSTL.SoTiepNhan == 0)
            {
                ShowMessage("Chưa đăng ký số hồ sơ thanh lý.", false);
                return;
            }
            //Kiểm tra xem có bảng kê nào đã gửi nhưng chưa có phản hồi
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                sendXML.master_id = HSTL.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đang chờ phản hồi.", false);
                    return;
                }
                else
                {
                    if (row.Cells["trangthaixl"].Value.ToString() == "0")
                    {
                        ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " chưa được khai báo.", false);
                        return;
                    }
                }
            }
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            string st = "";
            int i = 0;

            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                ++i;
                try
                {
                    switch (row.Cells["mabangke"].Text.Trim())
                    {
                        case "DTLTKN":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachTKN1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách tờ khai nhập";
                            break;
                        case "DTLTKX":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachTKX1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách tờ khai xuất";
                            break;
                        case "DTLNPLCHUATL":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLChuaThanhLy1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu chưa thanh lý";
                            break;
                        case "DTLNPLXH":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLXinHuy1(wsForm.txtMatKhau.Text.Trim());
                            break;
                        case "DTLNPLTX":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLTaiXuat1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu tái xuất";
                            break;
                        case "DTLNPLNKD":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLNKD1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                            break;
                        case "DTLNPLNT":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLNopThue1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu nộp thuế";
                            break;
                        case "DTLNPLXGC":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLXuatGiaCong1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                            break;
                        case "DTLCHITIETNT":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLTamNopThue1(wsForm.txtMatKhau.Text.Trim());
                            st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                            break;
                    }
                    if (xmlCurrent != "")
                    {
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                        sendXML.master_id = HSTL.ID;
                        sendXML.msg = xmlCurrent;
                        sendXML.func = 3;
                        xmlCurrent = "";
                        sendXML.InsertUpdate();
                    }
                }
                catch (Exception ex)
                {
                    --i;
                    this.Cursor = Cursors.Default;
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                if (ShowMessage("Hủy khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn hủy tiếp những bảng kê còn lại không?", true) == "Yes")
                                {
                                }
                                else
                                    break;
                            }
                            else
                            {
                                ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {

                            ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                        }
                        #endregion FPTService
                    }
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi hủy khai báo bảng kê hồ sơ thanh lý. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
            }
            if (i > 0)
            {
                LayPhanHoiBangKe(wsForm.txtMatKhau.Text.Trim());
            }
            else
                ShowMessage("Chưa gửi thông tin hủy bảng kê tới hải quan được.\nHãy thực hiện lại.", false);
            this.Cursor = Cursors.Default;
        }

        private void sendBK1_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
            sendXML.master_id = HSTL.ID;
            if (sendXML.Load())
            {
                ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đang chờ phản hồi.", false);
                return;
            }
            else
            {
                if (row.Cells["trangthaixl"].Value.ToString() == "1")
                {
                    ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đã được khai báo.", false);
                    return;
                }
            }
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            string st = "";
            this.Cursor = Cursors.WaitCursor;
            try
            {
                switch (row.Cells["mabangke"].Text.Trim())
                {
                    case "DTLTKN":
                        xmlCurrent = HSTL.WSSendBKToKhaiNhap1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách tờ khai nhập";
                        break;
                    case "DTLTKX":
                        xmlCurrent = HSTL.WSSendBKToKhaiXuat1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách tờ khai xuất";
                        break;
                    case "DTLNPLCHUATL":
                        xmlCurrent = HSTL.WSSendBKNPLChuaThanhLy1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu chưa thanh lý";
                        break;
                    case "DTLNPLXH":
                        xmlCurrent = HSTL.WSSendBKNPLHuy1(wsForm.txtMatKhau.Text.Trim());
                        break;
                    case "DTLNPLTX":
                        xmlCurrent = HSTL.WSSendBKNPLTaiXuat1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu tái xuất";
                        break;
                    case "DTLNPLNKD":
                        xmlCurrent = HSTL.WSSendBKNPLNhapKinhDoanh1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                        break;
                    case "DTLNPLNT":
                        xmlCurrent = HSTL.WSSendBKNPLNopThue1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu nộp thuế";
                        break;
                    case "DTLNPLXGC":
                        xmlCurrent = HSTL.WSSendBKNPLXuatGiaCong1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                        break;
                    case "DTLCHITIETNT":
                        xmlCurrent = HSTL.WSSendBKNPLTamNopThue1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                        break;
                }
                if (xmlCurrent != "")
                {
                    sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                    sendXML.master_id = HSTL.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 1;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                            {
                            }
                            else
                                return;
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {

                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            LayPhanHoiMotBangKe(sendXML.LoaiHS, wsForm.txtMatKhau.Text.Trim());
            this.Cursor = Cursors.Default;
        }

        private void huyBangKe1_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
            sendXML.master_id = HSTL.ID;
            if (sendXML.Load())
            {
                ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đang chờ phản hồi.", false);
                return;
            }
            else
            {
                if (row.Cells["trangthaixl"].Value.ToString() == "0")
                {
                    ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " chưa được khai báo.", false);
                    return;
                }
            }
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            string st = "";
            try
            {
                switch (row.Cells["mabangke"].Text.Trim())
                {
                    case "DTLTKN":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachTKN1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách tờ khai nhập";
                        break;
                    case "DTLTKX":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachTKX1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách tờ khai xuất";
                        break;
                    case "DTLNPLCHUATL":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLChuaThanhLy1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu chưa thanh lý";
                        break;
                    case "DTLNPLXH":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLXinHuy1(wsForm.txtMatKhau.Text.Trim());
                        break;
                    case "DTLNPLTX":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLTaiXuat1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu tái xuất";
                        break;
                    case "DTLNPLNKD":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLNKD1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                        break;
                    case "DTLNPLNT":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLNopThue1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu nộp thuế";
                        break;
                    case "DTLNPLXGC":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLXuatGiaCong1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                        break;
                    case "DTLCHITIETNT":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLTamNopThue1(wsForm.txtMatKhau.Text.Trim());
                        st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                        break;
                }
                if (xmlCurrent != "")
                {
                    sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                    sendXML.master_id = HSTL.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                            {
                            }
                            else
                                return;
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {

                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            LayPhanHoiMotBangKe(sendXML.LoaiHS, wsForm.txtMatKhau.Text.Trim());
            this.Cursor = Cursors.Default;
        }

        private void xacNhanThongTin1_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
            sendXML.master_id = HSTL.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Bảng kê này chưa có gửi thông tin tới hải quan.", false);
                return;
            }
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            LayPhanHoiMotBangKe(sendXML.LoaiHS, wsForm.txtMatKhau.Text.Trim());
            this.Cursor = Cursors.Default;
        }

        private void uiCommandBar1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void CapNhatHoSoThanhLyForm_Activated(object sender, EventArgs e)
        {
            //this.HSTL.Load();
            //this.refreshTrangThai();
        }

        private void txtSoHSTL_Click(object sender, EventArgs e)
        {

        }

        private void btnXoaBK_Click(object sender, EventArgs e)
        {
            BangKeHoSoThanhLyCollection bkColl = new BangKeHoSoThanhLyCollection();
            if (dgList.GetCheckedRows().Length < 1)
            {
                ShowMessage(setText("Chưa có bảng kê nào được chọn  để xóa .", "Please select list to delete before?"), false);
                return;
            }
            if (ShowMessage(setText("Bạn có muốn xóa bảng kê này không?", "Do you want to delete this list ?"), true) != "Yes")
            {
                return;
            }
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                BangKeHoSoThanhLy bk = (BangKeHoSoThanhLy)row.DataRow;
                if (row.RowType == RowType.Record)
                {
                    if (bk.MaBangKe == "DTLTKN" || bk.MaBangKe == "DTLTKX")
                    {
                        ShowMessage(setText("Bảng kê BK01 và BK02 không xóa được, sẽ bỏ qua", "BK01,BK02 list can not delete, so skip?"), false);
                    }
                    else
                    {
                        if (bk.ID > 0)
                        {
                            bk.Delete();

                        }
                        bkColl.Add(bk);


                    }
                }
            }
            foreach (BangKeHoSoThanhLy bkDelete in bkColl)
            {
                this.HSTL.BKCollection.Remove(bkDelete);
                bindComboboxDelete(bkDelete.MaBangKe);
            }
            dgList.DataSource = this.HSTL.BKCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            bindCombobox();

        }
    }
}