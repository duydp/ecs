﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
//using Company.GC.BLL.SXXK;
namespace Company.Interface.KDT.GC
{
    public partial class ReadExcelHCTForm : BaseForm
    {
        public ToKhaiChuyenTiep TKCT;
        public Decimal TiGiaTT;
        public char LoaiHH;
        public string NhomLoaiHinh;
        public decimal TyGiaTT;
       
        public ReadExcelHCTForm()
        {
            InitializeComponent();
        }

        private void editBox8_TextChanged(object sender, EventArgs e)
        {

        }
        //Hien thi Dialog box 
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        private int checkNPLExit(string maNPL)
        {
            for (int i = 0; i < this.TKCT.HCTCollection.Count; i++)
            {
                if (this.TKCT.HCTCollection[i].MaHang.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }

        private void txtTriGiaColumn_TextChanged(object sender, EventArgs e)
        {

        }


        private bool EmptyCheck(string st)
        {
            if (st == "") return true;
            return false;
        }
        private bool CheckNumber(decimal so)
        {
            if (so < 0) return true;
            return false;
        }
        private bool isSoLuongLimited(decimal so)
        {
            if (so < 0 || so > 10000000000000) return true;
            return false;
        }
        private bool isDonGiaLimited(decimal  so)
        {
            if (so < 0 || so > 10000000000000) return true;
            return false;
        }
        private bool CheckNameDVT(string varcheck)
        {
            DataTable dt = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;
        }
        private bool CheckNuocXX(string varcheck)
        {
            DataTable dt = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.SelectAll().Tables[0];
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;

        }
        private bool CheckNguyenTe(string varcheck)
        {
            DataTable dt = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.SelectAll().Tables[0];
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                error.SetError(txtRow,setText("Dòng bắt đầu phải lớn hơn 0","This value must be greater than 0 "));
                error.SetIconPadding(txtRow, 8);
                return;

            }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {

                ShowMessage(ex.Message+" ====", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                return;
            }
            //editing :
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int maHangCol = ConvertCharToInt(maHangColumn);

            char soLuongColumn = Convert.ToChar(txtSoLuongColumn.Text.Trim());
            int soLuongCol = ConvertCharToInt(soLuongColumn);

            char donGiaColumn = Convert.ToChar(txtDonGiaColumn.Text.Trim());
            int donGiaCol = ConvertCharToInt(donGiaColumn);

            

            char xuatXuColumn = Convert.ToChar(txtXuatXuColumn.Text.Trim());
            int xuatXuCol = ConvertCharToInt(xuatXuColumn);

            //char tiGiaTTColumn= Convert.ToChar(txtTiGiaTT.Text );
            //int tiGiaTTCol = ConvertCharToInt(tiGiaTTColumn);
            //decimal dongiaTT = Convert.ToDecimal(donGiaCol * TiGiaTT);
            //decimal trigiaNT = Convert.ToDecimal(donGiaCol * soLuongCol);
            //decimal trigiaTT = Convert.ToDecimal(trigiaNT * TiGiaTT);            

            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        //NguyenPhuLieu npl = new NguyenPhuLieu();                       
                        HangChuyenTiep hct = new HangChuyenTiep();
                        //EmptyCheck(Convert.ToString(wsr.Cells[TinhTrangCol].Value.ToString()))
                        //if (EmptyCheck(Convert.ToString(wsr.Cells[maHangCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[xuatXuCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[dVTCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[soLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHSCol].Value)) || isDonGiaLimited(Convert.ToDecimal(wsr.Cells[donGiaCol].Value)) || isSoLuongLimited(Convert.ToDecimal(wsr.Cells[soLuongCol].Value)) || CheckNuocXX(Convert.ToString(wsr.Cells[xuatXuCol].Value.ToString())) || CheckNameDVT(Convert.ToString(wsr.Cells[dVTCol].Value)))
                        //{
                        //    ShowMessage(" Dữ liệu tài dòng " + (wsr.Index + 1) + " sai, được bỏ qua", false);
                        //    continue;
                        //}
                        hct.MaHang = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        if (hct.MaHang.Trim() == "") continue;
                        if (LoaiHH == 'N')
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hct.MaHang;
                            npl.HopDong_ID = TKCT.IDHopDong;
                            //temp
                            if (!npl.Load())
                            {
                                showMsg("MSG_240245", hct.MaHang);
                                //ShowMessage("Mặt hàng : " + hct.TenHang + " chưa khai báo trong hợp đồng nên sẽ bị bỏ qua.", false);
                                continue;
                            }
                            hct.TenHang = npl.Ten;
                            hct.MaHS = npl.MaHS;
                            hct.ID_DVT = npl.DVT_ID;
                        }
                        else if (LoaiHH == 'S')
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hct.MaHang;
                            sp.HopDong_ID = TKCT.IDHopDong;
                            if (!sp.Load())
                            {
                                showMsg("MSG_240245", hct.MaHang);
                                //ShowMessage("Mặt hàng : " + hct.TenHang + " chưa khai báo trong hợp đồng nên sẽ bị bỏ qua.", false);
                                continue;
                            }
                            hct.TenHang = sp.Ten;
                            hct.MaHS = sp.MaHS;
                            hct.ID_DVT = sp.DVT_ID;
                        }
                        else if (LoaiHH=='T')
                        {
                            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            tb.Ma = hct.MaHang;
                            tb.HopDong_ID = TKCT.IDHopDong;
                            if (!tb.Load())
                            {
                                //ShowMessage("Mặt hàng : " + hct.TenHang + " chưa khai báo trong hợp đồng nên sẽ bị bỏ qua.", false);
                                showMsg("MSG_240245", hct.MaHang);
                                continue;
                            }
                            hct.TenHang = tb.Ten;
                            hct.MaHS = tb.MaHS;
                            hct.ID_DVT = tb.DVT_ID;
                        }
                        else if (LoaiHH == 'H') // Hàng mẫu
                        {
                            Company.GC.BLL.KDT.GC.HangMau hm = HangMau.Load( TKCT.IDHopDong,hct.MaHang);
                            
                            
                            if (hm == null)
                            {
                                //ShowMessage("Mặt hàng : " + hct.TenHang + " chưa khai báo trong hợp đồng nên sẽ bị bỏ qua.", false);
                                showMsg("MSG_240245", hct.MaHang);
                                continue;
                            }
                            hct.TenHang = hm.Ten;
                            hct.MaHS = hm.MaHS;
                            hct.ID_DVT = hm.DVT_ID;
                        }
                        hct.SoLuong = Convert.ToDecimal(wsr.Cells[soLuongCol].Value);
                        hct.DonGia = Convert.ToDecimal(wsr.Cells[donGiaCol].Value);                        
                        hct.ID_NuocXX = Convert.ToString(wsr.Cells[xuatXuCol].Value).Trim();     
                   
                        hct.TriGia = hct.DonGia * hct.SoLuong;
                        hct.TriGiaKB_VND = hct.SoLuong * hct.DonGia* TyGiaTT;
                        hct.TriGiaTT= hct.TriGiaKB_VND;
                      
                        int i = checkNPLExit(hct.MaHang);
                        if (i >= 0)
                        {
                            int dong = wsr.Index + 1;
                            //if (ShowMessage("Sản phẩm có mã \"" + hct.MaHang + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế sản phẩm trên lưới bằng sản phẩm này?", true) == "Yes")
                            if (showMsg("MSG_0203013", new String[] { hct.MaHang, dong.ToString()}, true) == "Yes")
                                this.TKCT.HCTCollection[i] = hct;

                        }
                        else this.TKCT.HCTCollection.Add(hct);


                    }
                    catch
                    {
                        //if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        if (showMsg("MSG_0203014", wsr.Index + 1, true) == "Yes")
                        {
                            this.TKCT.HCTCollection.Clear();
                            return;
                        }
                    }
                }
            }
            this.Close();

        }
  
        private void ReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void lblLinkExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GC("HangHoa_CT");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}