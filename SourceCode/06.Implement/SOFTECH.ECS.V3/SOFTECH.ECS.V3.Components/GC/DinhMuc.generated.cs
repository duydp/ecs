﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class DinhMuc
    {
        #region Properties.

        public long ID { set; get; }
        public string MaSanPham { set; get; }
        public string MaNguyenPhuLieu { set; get; }
        public string DVT_ID { set; get; }
        public decimal DinhMucSuDung { set; get; }
        public decimal TyLeHaoHut { set; get; }
        public string GhiChu { set; get; }
        public int STTHang { set; get; }
        public long Master_ID { set; get; }
        public decimal NPL_TuCungUng { set; get; }
        public string TenSanPham { set; get; }
        public string TenNPL { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<DinhMuc> ConvertToCollection(IDataReader reader)
        {
            IList<DinhMuc> collection = new List<DinhMuc>();
            while (reader.Read())
            {
                DinhMuc entity = new DinhMuc();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) entity.NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static DinhMuc Load(long id)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<DinhMuc> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<DinhMuc> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<DinhMuc> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<DinhMuc> SelectCollectionBy_Master_ID(long master_ID)
        {
            IDataReader reader = SelectReaderBy_Master_ID(master_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_Master_ID(long master_ID)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_SelectBy_Master_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_Master_ID(long master_ID)
        {
            const string spName = "p_KDT_GC_DinhMuc_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertDinhMuc(string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_ID, decimal nPL_TuCungUng, string tenSanPham, string tenNPL)
        {
            DinhMuc entity = new DinhMuc();
            entity.MaSanPham = maSanPham;
            entity.MaNguyenPhuLieu = maNguyenPhuLieu;
            entity.DVT_ID = dVT_ID;
            entity.DinhMucSuDung = dinhMucSuDung;
            entity.TyLeHaoHut = tyLeHaoHut;
            entity.GhiChu = ghiChu;
            entity.STTHang = sTTHang;
            entity.Master_ID = master_ID;
            entity.NPL_TuCungUng = nPL_TuCungUng;
            entity.TenSanPham = tenSanPham;
            entity.TenNPL = tenNPL;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
            db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<DinhMuc> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (DinhMuc item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateDinhMuc(long id, string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_ID, decimal nPL_TuCungUng, string tenSanPham, string tenNPL)
        {
            DinhMuc entity = new DinhMuc();
            entity.ID = id;
            entity.MaSanPham = maSanPham;
            entity.MaNguyenPhuLieu = maNguyenPhuLieu;
            entity.DVT_ID = dVT_ID;
            entity.DinhMucSuDung = dinhMucSuDung;
            entity.TyLeHaoHut = tyLeHaoHut;
            entity.GhiChu = ghiChu;
            entity.STTHang = sTTHang;
            entity.Master_ID = master_ID;
            entity.NPL_TuCungUng = nPL_TuCungUng;
            entity.TenSanPham = tenSanPham;
            entity.TenNPL = tenNPL;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_GC_DinhMuc_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
            db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<DinhMuc> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (DinhMuc item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateDinhMuc(long id, string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_ID, decimal nPL_TuCungUng, string tenSanPham, string tenNPL)
        {
            DinhMuc entity = new DinhMuc();
            entity.ID = id;
            entity.MaSanPham = maSanPham;
            entity.MaNguyenPhuLieu = maNguyenPhuLieu;
            entity.DVT_ID = dVT_ID;
            entity.DinhMucSuDung = dinhMucSuDung;
            entity.TyLeHaoHut = tyLeHaoHut;
            entity.GhiChu = ghiChu;
            entity.STTHang = sTTHang;
            entity.Master_ID = master_ID;
            entity.NPL_TuCungUng = nPL_TuCungUng;
            entity.TenSanPham = tenSanPham;
            entity.TenNPL = tenNPL;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
            db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<DinhMuc> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (DinhMuc item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteDinhMuc(long id)
        {
            DinhMuc entity = new DinhMuc();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_Master_ID(long master_ID)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_DeleteBy_Master_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMuc_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<DinhMuc> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (DinhMuc item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}