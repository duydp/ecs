﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;

namespace Company.Interface
{
    public partial class CauHinhToKhaiForm : BaseForm
    {
        public CauHinhToKhaiForm()
        {
            InitializeComponent();
        }
        private void khoitao_DuLieuChuan()
        {
            //Hungtq 22/12/2010.

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll();
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll();
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll();
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            txtTenDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            //txtTyGiaUSD.Value =  GlobalSettings.TY_GIA_USD;
            cbMaHTS.SelectedIndex = GlobalSettings.MaHTS;

            ctrCuaKhauXuatHang.Ma = GlobalSettings.CUA_KHAU;
            ctrDiaDiemDoHang.Ma = GlobalSettings.DIA_DIEM_DO_HANG;

            txtTieuDeDM.Text = GlobalSettings.TieuDeInDinhMuc;
            cbTinhThue.SelectedValue = GlobalSettings.TuDongTinhThue;
        }
        private void CauHinhToKhaiForm_Load(object sender, EventArgs e)
        {
            khoitao_DuLieuChuan();

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //Hungtq 22/12/2010. Luu cau hinh
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DOI_TAC", txtTenDoiTac.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PTTT_MAC_DINH", cbPTTT.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PTVT_MAC_DINH", cbPTVT.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DKGH_MAC_DINH", cbDKGH.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CUA_KHAU", ctrCuaKhauXuatHang.Ma);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DIA_DIEM_DO_HANG", ctrDiaDiemDoHang.Ma);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NGUYEN_TE_MAC_DINH", ctrNguyenTe.Ma);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NUOC", ctrNuocXK.Ma);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_HTS", cbMaHTS.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieuDeInDinhMuc", txtTieuDeDM.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TuDongTinhThue", cbTinhThue.SelectedValue);
            GlobalSettings.RefreshKey();

            showMsg("MSG_0203043");
            //ShowMessage("Lưu cấu hình thành công.", false);
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}

