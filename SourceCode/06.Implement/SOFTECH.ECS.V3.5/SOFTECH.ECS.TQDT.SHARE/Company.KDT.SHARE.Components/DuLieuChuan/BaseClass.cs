using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.IO;
using System.Windows.Forms;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class BaseClass
    {
        protected static SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

        public static string GetPathProgram()
        {
            FileInfo fileInfo = new FileInfo(Application.ExecutablePath);
            return fileInfo.DirectoryName;
        }
    }
}