﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Company.BLL.Utils;
using System.Globalization;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;

namespace Company.BLL.KDT.SXXK
{
    public partial class HoSoThanhLyDangKy
    {
        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuanTiepNhan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;

            return doc.InnerXml;
        }
        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.GetElementsByTagName("function")[0].InnerText = "5";
            string kq = "";
            int i = 0;
            for (i = 1; i <= 3; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                }
            }

            if (i > 3)
                return doc.InnerXml;
            if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
            {
                XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.HOSOTHANHLY)
                {
                    #region Lấy số tiếp nhận của hồ sơ thanh lý

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        XmlNode dataGet = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        this.SoTiepNhan = Convert.ToInt64(dataGet.Attributes["SO_HS"].Value);
                        this.NgayTiepNhan = DateTime.Today;
                        this.NamTiepNhan = (short)this.NgayTiepNhan.Year;
                        this.TrangThaiXuLy = 0;
                        this.Update();
                    }
                    #endregion Lấy số tiếp nhận của hồ sơ thanh lý
                }
                else if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.HOSOTHANHLY)
                {
                    #region Hủy khai báo hồ sơ thanh lý

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        this.SoTiepNhan = 0;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.NgayTiepNhan = new DateTime(1900, 1, 1);
                        this.Update();
                        try
                        {
                            BKCollection[this.getBKToKhaiNhap()].TrangThaiXL = 0;
                            BKCollection[this.getBKToKhaiNhap()].Update();                                                       
                        }
                        catch { }
                        try
                        {
                            BKCollection[this.getBKNPLChuaThanhLY()].TrangThaiXL = 0;
                            BKCollection[this.getBKNPLChuaThanhLY()].Update();                       
                        }
                        catch { }
                        try
                        {
                            BKCollection[this.getBKNPLXinHuy()].TrangThaiXL = 0;
                            BKCollection[this.getBKNPLXinHuy()].Update();
                        }
                        catch { }
                        try
                        {
                            BKCollection[this.getBKNPLNopThue()].TrangThaiXL = 0;
                            BKCollection[this.getBKNPLNopThue()].Update();
                        }
                        catch { }
                        try
                        {
                            BKCollection[this.getBKNPLTaiXuat()].TrangThaiXL = 0;
                            BKCollection[this.getBKNPLTaiXuat()].Update();
                        }
                        catch { }
                        try
                        {
                            BKCollection[this.getBKNPLTamNopThue()].TrangThaiXL = 0;
                            BKCollection[this.getBKNPLTamNopThue()].Update();
                        }
                        catch { }
                        try
                        {
                            BKCollection[this.getBKNPLNhapKinhDoanh()].TrangThaiXL = 0;
                            BKCollection[this.getBKNPLNhapKinhDoanh()].Update();
                        }
                        catch { }
                        try
                        {
                            BKCollection[this.getBKNPLXuatGiaCong()].TrangThaiXL = 0;
                            BKCollection[this.getBKNPLXuatGiaCong()].Update();
                        }
                        catch { }
                        try
                        {
                            BKCollection[this.getBKToKhaiXuat()].TrangThaiXL = 0;
                            BKCollection[this.getBKToKhaiXuat()].Update();
                        }
                        catch { }
                        
                    }

                    #endregion Hủy khai báo hồ sơ thanh lý
                }
                //else if (nodeTrangthai.Attributes["TRA_LOI"].Value == LAY_THONG_TIN.SP)
                //{
                //    #region Nhận trạng thái hồ sơ thanh lý
                //    XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                //    if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "DA_XU_LY")
                //    {
                //        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                //        //TransgferDataToSXXK();
                //    }
                //    else if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "TU_CHOI")
                //    {
                //        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                //        this.Update();
                //    }
                //    #endregion Nhận trạng thái hồ sơ thanh lý
                //}
            }
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
            {
                XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                string errorSt = "";
                if (stMucLoi == "XML_LEVEL")
                    errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                else if (stMucLoi == "DATA_LEVEL")
                    errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                else if (stMucLoi == "SERVICE_LEVEL")
                    errorSt = "Lỗi do Web service trả về ";
                else if (stMucLoi == "DOTNET_LEVEL")
                    errorSt = "Lỗi do hệ thống của hải quan ";
                throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
            }
            return "";

        }
        #region Khai báo hồ sơ thanh lý và bảng kê
        public string WSSendHoSoThanhLy(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));

            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\DANG_KY_TL_HO_SO.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";            
        }

        public string WSSendBKToKhaiNhap1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));
            
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_DSTKN_DK.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/DSTKN");
            this.BKCollection[this.getBKToKhaiNhap()].LoadChiTietBangKe();
            foreach (BKToKhaiNhap tkn in this.BKCollection[this.getBKToKhaiNhap()].bkTKNCollection)
            {
                XmlElement tk = doc.CreateElement("TO_KHAI_NK");

                XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
                loaiHinh.Value = tkn.MaLoaiHinh.ToString();

                XmlAttribute soTK = doc.CreateAttribute("SO_TK");
                soTK.Value = tkn.SoToKhai.ToString();

                XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
                namDK.Value = tkn.NamDangKy.ToString();

                tk.Attributes.Append(soTK);
                tk.Attributes.Append(loaiHinh);
                tk.Attributes.Append(namDK);
                dsTK.AppendChild(tk);
            }

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";          
        }
        public string WSSendBKToKhaiXuat1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));

            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_DSTKX_DK.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/DSTKX");
            this.BKCollection[this.getBKToKhaiXuat()].LoadChiTietBangKe();
            foreach (BKToKhaiXuat tkn in this.BKCollection[this.getBKToKhaiXuat()].bkTKXColletion)
            {
                XmlElement tk = doc.CreateElement("TO_KHAI_XK");

                XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
                loaiHinh.Value = tkn.MaLoaiHinh.ToString();

                XmlAttribute soTK = doc.CreateAttribute("SO_TK");
                soTK.Value = tkn.SoToKhai.ToString();

                XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
                namDK.Value = tkn.NamDangKy.ToString();

                XmlAttribute mahaiquan = doc.CreateAttribute("MA_HQ");
                mahaiquan.Value = tkn.MaHaiQuan;

                XmlAttribute ngayTX = doc.CreateAttribute("NGAY_THX");
                ngayTX.Value = tkn.NgayThucXuat.ToString("yyyy-MM-dd");

                tk.Attributes.Append(soTK);
                tk.Attributes.Append(loaiHinh);
                tk.Attributes.Append(namDK);
                tk.Attributes.Append(mahaiquan);
                tk.Attributes.Append(ngayTX);
                dsTK.AppendChild(tk);
            }
            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";     
        }
        public string WSSendBKNPLChuaThanhLy1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));
        
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_CTL_DK.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_CHUA_TL");
            this.BKCollection[this.getBKNPLChuaThanhLY()].LoadChiTietBangKe();
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            foreach (BKNPLChuaThanhLy bkCTL in this.BKCollection[this.getBKNPLChuaThanhLY()].bkNPLCTLCollection)
            {
                XmlElement tk = doc.CreateElement("TKN_TL");

                XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
                loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

                XmlAttribute soTK = doc.CreateAttribute("SO_TK");
                soTK.Value = bkCTL.SoToKhai.ToString();

                XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
                namDK.Value = bkCTL.NamDangKy.ToString();
                tk.Attributes.Append(soTK);
                tk.Attributes.Append(loaiHinh);
                tk.Attributes.Append(namDK);
                //tao 
                XmlElement tk_Hang = doc.CreateElement("TKN_TL.HANG");

                XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
                maNPL.Value = bkCTL.MaNPL.ToString();

                XmlAttribute LuongCTL = doc.CreateAttribute("LUONG_CTL");
                LuongCTL.Value = bkCTL.Luong.ToString(f);
                tk_Hang.Attributes.Append(maNPL);
                tk_Hang.Attributes.Append(LuongCTL);

                //lay tat ca cac nut TKN_TL cua dsTK kiem tra
                bool ok = true;
                XmlNode nodeOLD = null;
                foreach (XmlNode node in doc.GetElementsByTagName("TKN_TL"))
                {
                    if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
                    {
                        ok = false;
                        nodeOLD = node;
                        break;
                    }
                }
                if (ok)
                    dsTK.AppendChild(tk);
                else
                    tk = (XmlElement)nodeOLD;
                tk.AppendChild(tk_Hang);
            }
            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";     
        }
        public string WSSendBKNPLNopThue1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));
        
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_NT_DK.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_NOP_THUE");
            this.BKCollection[this.getBKNPLNopThue()].LoadChiTietBangKe();
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            foreach (BKNPLNopThueTieuThuNoiDia bkCTL in this.BKCollection[this.getBKNPLNopThue()].bkNPLNTCollection)
            {
                XmlElement tk = doc.CreateElement("TKN_NT");

                XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
                loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

                XmlAttribute soTK = doc.CreateAttribute("SO_TK");
                soTK.Value = bkCTL.SoToKhai.ToString();

                XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
                namDK.Value = bkCTL.NamDangKy.ToString();
                tk.Attributes.Append(soTK);
                tk.Attributes.Append(loaiHinh);
                tk.Attributes.Append(namDK);
                //tao 
                XmlElement tk_Hang = doc.CreateElement("TKN_NT.NPL_NT");

                XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
                maNPL.Value = bkCTL.MaNPL.ToString();

                XmlAttribute LuongCTL = doc.CreateAttribute("LUONG_NT");
                LuongCTL.Value = bkCTL.LuongNopThue.ToString(f);
                tk_Hang.Attributes.Append(maNPL);
                tk_Hang.Attributes.Append(LuongCTL);

                //lay tat ca cac nut TKN_TL cua dsTK kiem tra
                bool ok = true;
                XmlNode nodeOLD = null;
                foreach (XmlNode node in doc.GetElementsByTagName("TKN_NT"))
                {
                    if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
                    {
                        ok = false;
                        nodeOLD = node;
                        break;
                    }
                }
                if (ok)
                    dsTK.AppendChild(tk);
                else
                    tk = (XmlElement)nodeOLD;
                tk.AppendChild(tk_Hang);
            }
            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";  
        }
        public string WSSendBKNPLHuy1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));

            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_XH_DK.xml");

            //luu thong tin gui
            #region Thong tin chung
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();
            #endregion Thong Tin chung

            #region Load NPL
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_HUY");
            this.BKCollection[this.getBKNPLXinHuy()].LoadChiTietBangKe();
            foreach (BKNPLXinHuy bkCTL in this.BKCollection[this.getBKNPLXinHuy()].bkNPLXHCollection)
            {
                XmlElement tk = doc.CreateElement("TKN_H");

                XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
                loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

                XmlAttribute soTK = doc.CreateAttribute("SO_TK");
                soTK.Value = bkCTL.SoToKhai.ToString();

                XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
                namDK.Value = bkCTL.NamDangKy.ToString();
                tk.Attributes.Append(soTK);
                tk.Attributes.Append(loaiHinh);
                tk.Attributes.Append(namDK);
                //tao 
                XmlElement tk_Hang = doc.CreateElement("TKN_H.NPL_H");

                XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
                maNPL.Value = bkCTL.MaNPL.ToString();

                tk_Hang.Attributes.Append(maNPL);

                //lay tat ca cac nut TKN_TL cua dsTK kiem tra
                bool ok = true;
                XmlNode nodeOLD = null;
                foreach (XmlNode node in doc.GetElementsByTagName("TKN_H"))
                {
                    if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
                    {
                        ok = false;
                        nodeOLD = node;
                        break;
                    }
                }
                if (ok)
                    dsTK.AppendChild(tk);
                else
                    tk = (XmlElement)nodeOLD;
                tk.AppendChild(tk_Hang);
            }
            #endregion Load NPL

            #region Load Thong tin huy

            foreach (BKNPLXinHuy bkCTL in this.BKCollection[this.getBKNPLXinHuy()].bkNPLXHCollection)
            {
                XmlElement tk = doc.CreateElement("TT_HUY");

                XmlAttribute bbHuy = doc.CreateAttribute("BB_HUY");
                bbHuy.Value = bkCTL.BienBanHuy.ToString();

                XmlAttribute NgayHuy = doc.CreateAttribute("NGAY_HUY");
                NgayHuy.Value = bkCTL.NgayHuy.ToString("yyyy-MM-dd");

                XmlAttribute LUONG_HUY = doc.CreateAttribute("LUONG_HUY");
                LUONG_HUY.Value = bkCTL.LuongHuy.ToString(f);
                tk.Attributes.Append(bbHuy);
                tk.Attributes.Append(NgayHuy);
                tk.Attributes.Append(LUONG_HUY);

                //lay tat ca cac nut TKN_H.NPL_H cua dsTK kiem tra

                foreach (XmlNode node in doc.GetElementsByTagName("TKN_H.NPL_H"))
                {
                    if (node.Attributes["MA_NPL"].Value == bkCTL.MaNPL)
                    {
                        node.AppendChild(tk);
                        break;
                    }
                }
            }

            #endregion Load Thong tin huy
            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";  
        }
        public string WSSendBKNPLTamNopThue1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));

            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_CHI_TIET_NT_DK.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/CHI_TIET_NT");
            this.BKCollection[this.getBKNPLTamNopThue()].LoadChiTietBangKe();
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            foreach (BKNPLTamNopThue bkCTL in this.BKCollection[this.getBKNPLTamNopThue()].bkNPLTNTCollection)
            {
                XmlElement tk = doc.CreateElement("TKN");

                XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
                loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

                XmlAttribute soTK = doc.CreateAttribute("SO_TK");
                soTK.Value = bkCTL.SoToKhai.ToString();

                XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
                namDK.Value = bkCTL.NamDangKy.ToString();

                XmlAttribute mahq = doc.CreateAttribute("MA_HQ");
                mahq.Value = bkCTL.MaHaiQuan;
                tk.Attributes.Append(soTK);
                tk.Attributes.Append(loaiHinh);
                tk.Attributes.Append(namDK);
                tk.Attributes.Append(mahq);
                //tao 
                XmlElement tk_Hang = doc.CreateElement("TKN.NPL");

                XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
                maNPL.Value = bkCTL.MaNPL.ToString();

                XmlAttribute NOP_NK = doc.CreateAttribute("NOP_NK");
                NOP_NK.Value = bkCTL.Nop_NK.ToString(f);

                XmlAttribute NOP_VAT = doc.CreateAttribute("NOP_VAT");
                NOP_VAT.Value = bkCTL.Nop_VAT.ToString(f);

                XmlAttribute NOP_TTDB = doc.CreateAttribute("NOP_TTDB");
                NOP_TTDB.Value = bkCTL.Nop_TTDB.ToString(f);

                XmlAttribute NOP_CLGIA = doc.CreateAttribute("NOP_CLGIA");
                NOP_CLGIA.Value = bkCTL.Nop_CLGia.ToString(f);

                XmlAttribute ngayNT = doc.CreateAttribute("NGAY_NT");
                ngayNT.Value = bkCTL.NgayNopThue.ToString("yyyy-MM-dd");

                tk_Hang.Attributes.Append(maNPL);
                tk_Hang.Attributes.Append(NOP_NK);
                tk_Hang.Attributes.Append(NOP_VAT);
                tk_Hang.Attributes.Append(NOP_TTDB);
                tk_Hang.Attributes.Append(NOP_CLGIA);
                tk_Hang.Attributes.Append(ngayNT);

                //lay tat ca cac nut TKN_TL cua dsTK kiem tra
                bool ok = true;
                XmlNode nodeOLD = null;
                foreach (XmlNode node in doc.GetElementsByTagName("TKN"))
                {
                    if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
                    {
                        ok = false;
                        nodeOLD = node;
                        break;
                    }
                }
                if (ok)
                    dsTK.AppendChild(tk);
                else
                    tk = (XmlElement)nodeOLD;
                tk.AppendChild(tk_Hang);
            }
            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";        
        }
        public string WSSendBKNPLNhapKinhDoanh1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));
            
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_NKD_DK.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_NKD");
            this.BKCollection[this.getBKNPLNhapKinhDoanh()].LoadChiTietBangKe();

            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            #region Tạo thông tin  về tờ khai nhập
            foreach (BKNPLXuatSuDungNKD bkCTL in this.BKCollection[this.getBKNPLNhapKinhDoanh()].bkNPLNKDCollection)
            {
                XmlElement tkNKD = doc.CreateElement("TKNKD_TL");

                XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
                loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

                XmlAttribute soTK = doc.CreateAttribute("SO_TK");
                soTK.Value = bkCTL.SoToKhai.ToString();

                XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
                namDK.Value = bkCTL.NamDangKy.ToString();

                tkNKD.Attributes.Append(soTK);
                tkNKD.Attributes.Append(loaiHinh);
                tkNKD.Attributes.Append(namDK);

                //tao 
                XmlElement tk_HangNKD = doc.CreateElement("TKNKD_TL.HANG");

                XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
                maNPL.Value = bkCTL.MaNPL.ToString();

                tk_HangNKD.Attributes.Append(maNPL);

                //lay tat ca cac nut TKN_TL cua dsTK kiem tra
                bool ok = true;
                XmlNode nodeOLD = null;
                foreach (XmlNode node in doc.GetElementsByTagName("TKNKD_TL"))
                {
                    if (node.Attributes["MA_LH"].Value == tkNKD.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tkNKD.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tkNKD.Attributes["NAM_DK"].Value)
                    {
                        ok = false;
                        nodeOLD = node;
                        break;
                    }
                }
                if (ok)//neu to khai nay chua co trong danh sach thi dua vao dong thoi dua luong hang vao
                {
                    dsTK.AppendChild(tkNKD);
                    tkNKD.AppendChild(tk_HangNKD);
                }
                else
                {
                    tkNKD = (XmlElement)nodeOLD;//neu to khai nay da co thi kiem tra hang nay da co chua
                    ok = true;
                    foreach (XmlNode node in tkNKD.ChildNodes)
                    {
                        if (node.Attributes["MA_NPL"].Value == tk_HangNKD.Attributes["MA_NPL"].Value)
                        {
                            ok = false;
                            tk_HangNKD = (XmlElement)node;
                            break;
                        }
                    }
                    if (ok)
                        tkNKD.AppendChild(tk_HangNKD);
                }

            #endregion Tạo thông tin  về tờ khai nhập
                #region Tạo thông tin  về tờ khai xuất

                //thong tin to khai xuat
                XmlElement tkXuat = doc.CreateElement("TKNKD_TL.TKX_SD");

                XmlAttribute loaiHinhX = doc.CreateAttribute("MA_LH");
                loaiHinhX.Value = bkCTL.MaLoaiHinhXuat.ToString();

                XmlAttribute soTKX = doc.CreateAttribute("SO_TK");
                soTKX.Value = bkCTL.SoToKhaiXuat.ToString();

                XmlAttribute namDKX = doc.CreateAttribute("NAM_DK");
                namDKX.Value = bkCTL.NamDangKyXuat.ToString();

                XmlAttribute mahqX = doc.CreateAttribute("MA_HQ");
                mahqX.Value = bkCTL.MaLoaiHinhXuat.ToString();

                tkXuat.Attributes.Append(soTKX);
                tkXuat.Attributes.Append(loaiHinhX);
                tkXuat.Attributes.Append(namDKX);
                tkXuat.Attributes.Append(mahqX);

                //thong tin hang xuat
                XmlElement tkHangXuat = doc.CreateElement("TKNKD_TL.TKX_SD");

                XmlAttribute masp = doc.CreateAttribute("MA_SP");
                masp.Value = bkCTL.MaSP.ToString();

                XmlAttribute luong = doc.CreateAttribute("LUONG_SD");
                luong.Value = bkCTL.LuongSuDung.ToString(f);

                tkHangXuat.Attributes.Append(masp);
                tkHangXuat.Attributes.Append(luong);
                ok = true;
                foreach (XmlNode nodeConNPL in tk_HangNKD.ChildNodes)
                {
                    if (nodeConNPL.Attributes["MA_LH"].Value == tkXuat.Attributes["MA_LH"].Value && nodeConNPL.Attributes["SO_TK"].Value == tkXuat.Attributes["SO_TK"].Value && nodeConNPL.Attributes["NAM_DK"].Value == tkXuat.Attributes["NAM_DK"].Value
                        && nodeConNPL.Attributes["MA_HQ"].Value == tkXuat.Attributes["MA_HQ"].Value)
                    {
                        ok = false;
                        tkXuat = (XmlElement)nodeConNPL;
                        break;
                    }
                }
                if (ok)
                    tk_HangNKD.AppendChild(tkXuat);
                ok = true;
                foreach (XmlNode nodeConNPL in tkXuat.ChildNodes)
                {
                    if (nodeConNPL.Attributes["MA_SP"].Value == tkHangXuat.Attributes["MA_SP"].Value)
                    {
                        ok = false;
                        tkHangXuat = (XmlElement)nodeConNPL;
                        break;
                    }
                }
                if (ok)
                    tkXuat.AppendChild(tkHangXuat);


            #endregion Tạo thông tin  về tờ khai xuất


                XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
                XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

                Content.AppendChild(root);
                     
            }
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";  
        }

        public string WSSendBKNPLXuatGiaCong1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_NKD_DK.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_XGC");
            this.BKCollection[this.getBKNPLNhapKinhDoanh()].LoadChiTietBangKe();

            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
           
            foreach (BKNPLXuatGiaCong bkCTL in this.BKCollection[this.getBKNPLXuatGiaCong()].bkNPLXGCCollection)
            {
                #region Tạo thông tin  về tờ khai nhập
                XmlElement tkNKD = doc.CreateElement("TKN_XGC");

                XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
                loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

                XmlAttribute soTK = doc.CreateAttribute("SO_TK");
                soTK.Value = bkCTL.SoToKhai.ToString();

                XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
                namDK.Value = bkCTL.NamDangKy.ToString();

                tkNKD.Attributes.Append(soTK);
                tkNKD.Attributes.Append(loaiHinh);
                tkNKD.Attributes.Append(namDK);

                //tao 
                XmlElement tk_HangNKD = doc.CreateElement("TKN_XGC.NPL_XGC");

                XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
                maNPL.Value = bkCTL.MaNPL.ToString();

                tk_HangNKD.Attributes.Append(maNPL);

                //lay tat ca cac nut TKN_TL cua dsTK kiem tra
                bool ok = true;
                XmlNode nodeOLD = null;
                foreach (XmlNode node in doc.GetElementsByTagName("TKN_XGC"))
                {
                    if (node.Attributes["MA_LH"].Value == tkNKD.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tkNKD.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tkNKD.Attributes["NAM_DK"].Value)
                    {
                        ok = false;
                        nodeOLD = node;
                        break;
                    }
                }
                if (ok)//neu to khai nay chua co trong danh sach thi dua vao dong thoi dua luong hang vao
                {
                    dsTK.AppendChild(tkNKD);
                    tkNKD.AppendChild(tk_HangNKD);
                }
                else
                {
                    tkNKD = (XmlElement)nodeOLD;//neu to khai nay da co thi kiem tra hang nay da co chua
                    ok = true;
                    foreach (XmlNode node in tkNKD.ChildNodes)
                    {
                        if (node.Attributes["MA_NPL"].Value == tk_HangNKD.Attributes["MA_NPL"].Value)
                        {
                            ok = false;
                            tk_HangNKD = (XmlElement)node;
                            break;
                        }
                    }
                    if (ok)
                        tkNKD.AppendChild(tk_HangNKD);
                }

                #endregion Tạo thông tin  về tờ khai nhập

                #region Tạo thông tin  về tờ khai xuất

                //thong tin to khai xuat
                XmlElement tkXuat = doc.CreateElement("TKN_XGC.NPL_XGC.TK_XGC");

                XmlAttribute loaiHinhX = doc.CreateAttribute("MA_LH");
                loaiHinhX.Value = bkCTL.MaLoaiHinhXuat.ToString();

                XmlAttribute soTKX = doc.CreateAttribute("SO_TK");
                soTKX.Value = bkCTL.SoToKhaiXuat.ToString();

                XmlAttribute namDKX = doc.CreateAttribute("NAM_DK");
                namDKX.Value = bkCTL.NamDangKyXuat.ToString();

                XmlAttribute mahqX = doc.CreateAttribute("MA_HQ");
                mahqX.Value = bkCTL.MaLoaiHinhXuat.ToString();


                XmlAttribute luong = doc.CreateAttribute("LUONG_XGC");
                luong.Value = bkCTL.LuongXuat.ToString(f);

                tkXuat.Attributes.Append(soTKX);
                tkXuat.Attributes.Append(loaiHinhX);
                tkXuat.Attributes.Append(namDKX);
                tkXuat.Attributes.Append(mahqX);
                tkXuat.Attributes.Append(luong);

                //thong tin hang xuat

                ok = true;
                foreach (XmlNode nodeConNPL in tk_HangNKD.ChildNodes)
                {
                    if (nodeConNPL.Attributes["MA_LH"].Value == tkXuat.Attributes["MA_LH"].Value && nodeConNPL.Attributes["SO_TK"].Value == tkXuat.Attributes["SO_TK"].Value && nodeConNPL.Attributes["NAM_DK"].Value == tkXuat.Attributes["NAM_DK"].Value
                        && nodeConNPL.Attributes["MA_HQ"].Value == tkXuat.Attributes["MA_HQ"].Value)
                    {
                        ok = false;
                        tkXuat = (XmlElement)nodeConNPL;
                        break;
                    }
                }
                if (ok)
                    tk_HangNKD.AppendChild(tkXuat);


                #endregion Tạo thông tin  về tờ khai xuất


             
            }
            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";        
        }

        public string WSSendBKNPLTaiXuat1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.KhaiBao));
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_TX_DK.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_TAI_XUAT");
            this.BKCollection[this.getBKNPLTaiXuat()].LoadChiTietBangKe();

            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            #region Tạo thông tin  về tờ khai nhập
            foreach (BKNPLTaiXuat bkCTL in this.BKCollection[this.getBKNPLTaiXuat()].bkNPLTXCollection)
            {
                XmlElement tkNKD = doc.CreateElement("TKN_TX");

                XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
                loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

                XmlAttribute soTK = doc.CreateAttribute("SO_TK");
                soTK.Value = bkCTL.SoToKhai.ToString();

                XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
                namDK.Value = bkCTL.NamDangKy.ToString();

                tkNKD.Attributes.Append(soTK);
                tkNKD.Attributes.Append(loaiHinh);
                tkNKD.Attributes.Append(namDK);

                //tao 
                XmlElement tk_HangNKD = doc.CreateElement("TKN_TX.NPL_TX");

                XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
                maNPL.Value = bkCTL.MaNPL.ToString();

                tk_HangNKD.Attributes.Append(maNPL);

                //lay tat ca cac nut TKN_TL cua dsTK kiem tra
                bool ok = true;
                XmlNode nodeOLD = null;
                foreach (XmlNode node in doc.GetElementsByTagName("TKN_TX"))
                {
                    if (node.Attributes["MA_LH"].Value == tkNKD.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tkNKD.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tkNKD.Attributes["NAM_DK"].Value)
                    {
                        ok = false;
                        nodeOLD = node;
                        break;
                    }
                }
                if (ok)//neu to khai nay chua co trong danh sach thi dua vao dong thoi dua luong hang vao
                {
                    dsTK.AppendChild(tkNKD);
                    tkNKD.AppendChild(tk_HangNKD);
                }
                else
                {
                    tkNKD = (XmlElement)nodeOLD;//neu to khai nay da co thi kiem tra hang nay da co chua
                    ok = true;
                    foreach (XmlNode node in tkNKD.ChildNodes)
                    {
                        if (node.Attributes["MA_NPL"].Value == tk_HangNKD.Attributes["MA_NPL"].Value)
                        {
                            ok = false;
                            tk_HangNKD = (XmlElement)node;
                            break;
                        }
                    }
                    if (ok)
                        tkNKD.AppendChild(tk_HangNKD);
                }

            #endregion Tạo thông tin  về tờ khai nhập
                #region Tạo thông tin  về tờ khai xuất

                //thong tin to khai xuat
                XmlElement tkXuat = doc.CreateElement("TKN_TX.NPL_TX.TK_TX");

                XmlAttribute loaiHinhX = doc.CreateAttribute("MA_LH");
                loaiHinhX.Value = bkCTL.MaLoaiHinhXuat.ToString();

                XmlAttribute soTKX = doc.CreateAttribute("SO_TK");
                soTKX.Value = bkCTL.SoToKhaiXuat.ToString();

                XmlAttribute namDKX = doc.CreateAttribute("NAM_DK");
                namDKX.Value = bkCTL.NamDangKyXuat.ToString();

                XmlAttribute mahqX = doc.CreateAttribute("MA_HQ");
                mahqX.Value = bkCTL.MaHaiQuanXuat.ToString();


                XmlAttribute luong = doc.CreateAttribute("LUONG_TX");
                luong.Value =Convert.ToUInt32(bkCTL.LuongTaiXuat).ToString();

                tkXuat.Attributes.Append(soTKX);
                tkXuat.Attributes.Append(loaiHinhX);
                tkXuat.Attributes.Append(namDKX);
                tkXuat.Attributes.Append(mahqX);
                tkXuat.Attributes.Append(luong);

                //thong tin hang xuat

                ok = true;
                foreach (XmlNode nodeConNPL in tk_HangNKD.ChildNodes)
                {
                    if (nodeConNPL.Attributes["MA_LH"].Value == tkXuat.Attributes["MA_LH"].Value && nodeConNPL.Attributes["SO_TK"].Value == tkXuat.Attributes["SO_TK"].Value && nodeConNPL.Attributes["NAM_DK"].Value == tkXuat.Attributes["NAM_DK"].Value
                        && nodeConNPL.Attributes["MA_HQ"].Value == tkXuat.Attributes["MA_HQ"].Value)
                    {
                        ok = false;
                        tkXuat = (XmlElement)nodeConNPL;
                        break;
                    }
                }
                if (ok)
                    tk_HangNKD.AppendChild(tkXuat);


                #endregion Tạo thông tin  về tờ khai xuất


                
            }
            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }
        #endregion Khai báo hồ sơ thanh lý và bảng kê

        #region Hủy hồ sơ thanh lý và bảng kê

        #region  Hủy thông tin duyệt hồ sơ thanh lý

        public string HuyKhaiBaoHSTL1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));
          
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\HUY_DANG_KY_TL_HO_SO.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();
          
            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string HuyKhaiBaoDanhSachTKN1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));

            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_DSTKN_HUY.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string HuyKhaiBaoDanhSachTKX1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));
          
           
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_DSTKX_HUY.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();
            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string HuyKhaiBaoDanhSachNPLChuaThanhLy1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));
            
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_CTL_HUY.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string HuyKhaiBaoDanhSachNPLNKD1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));
          
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_NKD_HUY.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string HuyKhaiBaoDanhSachNPLNopThue1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));

            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_NT_HUY.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string HuyKhaiBaoDanhSachNPLTamNopThue1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));

            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_CHI_TIET_NT_HUY.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string HuyKhaiBaoDanhSachNPLTaiXuat1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_TX_HUY.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string HuyKhaiBaoDanhSachNPLXuatGiaCong1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_XGC_HUY.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string HuyKhaiBaoDanhSachNPLXinHuy1(string pass)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docMSG = new XmlDocument();
            docMSG.LoadXml(ConfigPhongBi(MessgaseType.HoSoThanhKhoanSXXK, MessgaseFunction.HuyKhaiBao));
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path+"\\FPTService\\SXXK_TL_NPL_XH_HUY.xml");

            //luu thong tin gui
            XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            hqNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

            XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            dv.Attributes["TEN_DV"].Value = "";

            XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
            dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
            dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

            XmlNode root = docMSG.ImportNode(doc.SelectSingleNode("Root"), true);
            XmlNode Content = docMSG.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            string kq = "";
            try
            {
                kq = kdt.Send(docMSG.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return docMSG.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        #endregion  Hủy thông tin duyệt hồ sơ thanh lý

        #endregion Hủy hồ sơ thanh lý và bảng kê
        public string LayPhanHoiBangKe(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.GetElementsByTagName("function")[0].InnerText = "5";
            string kq = "";
            int i = 0;
            for (i = 1; i <= 3; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                }
            }

            if (i > 3)
                return doc.InnerXml;
            if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
            {
                XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                #region Phản hồi gửi bảng kê
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.BK_TOKHAINHAP)
                {
                    #region phản hồi ds tờ khai nhập

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {                     
                        BKCollection[this.getBKToKhaiNhap()].TrangThaiXL = 1;
                        BKCollection[this.getBKToKhaiNhap()].Update();
                    }
                    #endregion phản hồi ds tờ khai nhập
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.BK_TOKHAIXUAT)
                {
                    #region phản hồi ds tờ khai xuất

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKToKhaiXuat()].TrangThaiXL = 1;
                        BKCollection[this.getBKToKhaiXuat()].Update();
                    }
                    #endregion phản hồi ds tờ khai xuất
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.BK_XUAT_GC)
                {
                    #region phản hồi ds npl xuất gia công

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLXuatGiaCong()].TrangThaiXL = 1;
                        BKCollection[this.getBKNPLXuatGiaCong()].Update();
                    }
                    #endregion phản hồi ds npl xuất gia công
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.BK_TL_NKD)
                {
                    #region phản hồi ds npl xuất kinh doanh

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLNhapKinhDoanh()].TrangThaiXL = 1;
                        BKCollection[this.getBKNPLNhapKinhDoanh()].Update();
                    }
                    #endregion phản hồi ds npl xuất kinh doanh
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.BK_TAM_NOP_THUE)
                {
                    #region phản hồi ds npl tạm nộp thuế

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLTamNopThue()].TrangThaiXL = 1;
                        BKCollection[this.getBKNPLTamNopThue()].Update();
                    }
                    #endregion phản hồi ds npl tạm nộp thuế
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.BK_TAI_XUAT)
                {
                    #region phản hồi ds npl tái xuất

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLTaiXuat()].TrangThaiXL = 1;
                        BKCollection[this.getBKNPLTaiXuat()].Update();
                    }
                    #endregion phản hồi ds npl tạm nộp thuế
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.BK_NOP_THUE)
                {
                    #region phản hồi ds npl nộp thuế

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLNopThue()].TrangThaiXL = 1;
                        BKCollection[this.getBKNPLNopThue()].Update();
                    }
                    #endregion phản hồi ds npl nộp thuế
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.BK_HUY)
                {
                    #region phản hồi ds npl hủy

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLXinHuy()].TrangThaiXL = 1;
                        BKCollection[this.getBKNPLXinHuy()].Update();
                    }
                    #endregion phản hồi ds npl hủy
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.BK_CHUA_THANH_LY)
                {
                    #region phản hồi ds npl chưa thanh lý

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLChuaThanhLY()].TrangThaiXL = 1;
                        BKCollection[this.getBKNPLChuaThanhLY()].Update();
                    }
                    #endregion phản hồi ds npl chưa thanh lý
                }
                #endregion Phản hồi gửi bảng kê

                #region Phản hồi hủy bảng kê
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.BK_TOKHAINHAP)
                {
                    #region phản hồi ds tờ khai nhập

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKToKhaiNhap()].TrangThaiXL = 0;
                        BKCollection[this.getBKToKhaiNhap()].Update();
                    }
                    #endregion phản hồi ds tờ khai nhập
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.BK_TOKHAIXUAT)
                {
                    #region phản hồi ds tờ khai xuất

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKToKhaiXuat()].TrangThaiXL = 0;
                        BKCollection[this.getBKToKhaiXuat()].Update();
                    }
                    #endregion phản hồi ds tờ khai xuất
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.BK_XUAT_GC)
                {
                    #region phản hồi ds npl xuất gia công

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLXuatGiaCong()].TrangThaiXL = 0;
                        BKCollection[this.getBKNPLXuatGiaCong()].Update();
                    }
                    #endregion phản hồi ds npl xuất gia công
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.BK_TL_NKD)
                {
                    #region phản hồi ds npl xuất kinh doanh

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLNhapKinhDoanh()].TrangThaiXL = 0;
                        BKCollection[this.getBKNPLNhapKinhDoanh()].Update();
                    }
                    #endregion phản hồi ds npl xuất kinh doanh
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.BK_TAM_NOP_THUE)
                {
                    #region phản hồi ds npl tạm nộp thuế

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLTamNopThue()].TrangThaiXL = 0;
                        BKCollection[this.getBKNPLTamNopThue()].Update();
                    }
                    #endregion phản hồi ds npl tạm nộp thuế
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.BK_TAI_XUAT)
                {
                    #region phản hồi ds npl tái xuất

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLTaiXuat()].TrangThaiXL = 0;
                        BKCollection[this.getBKNPLTaiXuat()].Update();
                    }
                    #endregion phản hồi ds npl tạm nộp thuế
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.BK_NOP_THUE)
                {
                    #region phản hồi ds npl nộp thuế

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLNopThue()].TrangThaiXL = 0;
                        BKCollection[this.getBKNPLNopThue()].Update();
                    }
                    #endregion phản hồi ds npl nộp thuế
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.BK_HUY)
                {
                    #region phản hồi ds npl hủy

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLXinHuy()].TrangThaiXL = 0;
                        BKCollection[this.getBKNPLXinHuy()].Update();
                    }
                    #endregion phản hồi ds npl hủy
                }
                if (nodeTrangthai.Attributes["TRA_LOI"].Value ==THONG_TIN_HUY.BK_CHUA_THANH_LY)
                {
                    #region phản hồi ds npl chưa thanh lý

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        BKCollection[this.getBKNPLChuaThanhLY()].TrangThaiXL = 0;
                        BKCollection[this.getBKNPLChuaThanhLY()].Update();
                    }
                    #endregion phản hồi ds npl chưa thanh lý
                }
                #endregion Phản hồi hủy bảng kê

            }
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
            {
                XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                string errorSt = "";
                if (stMucLoi == "XML_LEVEL")
                    errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                else if (stMucLoi == "DATA_LEVEL")
                    errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                else if (stMucLoi == "SERVICE_LEVEL")
                    errorSt = "Lỗi do Web service trả về ";
                else if (stMucLoi == "DOTNET_LEVEL")
                    errorSt = "Lỗi do hệ thống của hải quan ";
                throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
            }
            return "";

        }
    }
}
