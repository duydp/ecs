﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
using Janus.Windows.GridEX;
using Company.GC.BLL;
using Company.KDT.SHARE.Components;

namespace Company.Interface.QuanTri
{
    public partial class QuanLyPhanQuyen : BaseForm
    {
        private GROUPS group = new GROUPS();
        private GROUPSCollection collectionGroup = new GROUPSCollection();
        private ROLECollection collectionRole = new ROLECollection();
        private ROLE role = new ROLE();
        private int ModuleCurrent = 0;
        private long GroupCurrent = 0;
        public QuanLyPhanQuyen()
        {
            InitializeComponent();
        }

        private void QuanLyPhanQuyen_Load(object sender, EventArgs e)
        {
            collectionGroup = group.SelectCollectionAll();
            dgListNhom.DataSource = collectionGroup;
            if (collectionGroup.Count > 0)
            {
                GroupCurrent = collectionGroup[0].MA_NHOM;
                lblNhomPhanQuyen.Text = setText("Phân quyền cho nhóm : ", "Permission for group:     ") + collectionGroup[0].TEN_NHOM;
            }
            cbModule.SelectedIndex=0;
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Permission)))
            {                
                TopRebar1.Visible = false;
            }     
        }
        private void BindDataRole()
        {
            string where = "";
            ModuleCurrent = Convert.ToInt32(cbModule.SelectedValue);
            if (ModuleCurrent > 0)
            {
                where = "ID_MODULE=" + ModuleCurrent;
                collectionRole = role.SelectCollectionDynamic(where, "");
            }
            else
                collectionRole = role.SelectCollectionAll();
            GROUP_ROLE gr = new GROUP_ROLE();
            gr.GROUP_ID = GroupCurrent;
            
            GROUP_ROLECollection grCollection = gr.SelectCollectionBy_GROUP_IDAndModule(ModuleCurrent);
            foreach (GROUP_ROLE grEntity in grCollection)
            {
                foreach (ROLE r in collectionRole)
                {
                    if (r.ID == grEntity.ID_ROLE)
                    {
                        r.Check = true;
                        break;
                    }
                }
            }
            dgListRole.DataSource = collectionRole;
        }
        private void cbModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GroupCurrent > 0)
            {
                BindDataRole();
            }
        }

        private void dgListRole_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["ID_MODULE"].Value.ToString() == "1")
                    e.Row.Cells["ID_MODULE"].Text =setText( "Hợp đồng","Contract");
                else if (e.Row.Cells["ID_MODULE"].Value.ToString() == "2")
                    e.Row.Cells["ID_MODULE"].Text = setText("Định mức", "Norm");
                else if (e.Row.Cells["ID_MODULE"].Value.ToString() == "3")
                    e.Row.Cells["ID_MODULE"].Text = setText("Phụ kiện", "Accessory");
                else if (e.Row.Cells["ID_MODULE"].Value.ToString() == "4")
                    e.Row.Cells["ID_MODULE"].Text = setText("Tờ khai có HĐ", "Declaration with contract");
                else if (e.Row.Cells["ID_MODULE"].Value.ToString() == "5")
                    e.Row.Cells["ID_MODULE"].Text = setText("Tờ khai GCCT", "Transitional outsourcing declaration");
                else if (e.Row.Cells["ID_MODULE"].Value.ToString() == "6")
                    e.Row.Cells["ID_MODULE"].Text = setText("Tờ khai không HĐ", "Declaration without contract");
                else if (e.Row.Cells["ID_MODULE"].Value.ToString() == "7")
                    e.Row.Cells["ID_MODULE"].Text = setText("NPL cung ứng", "Self-supplied Material");
                else if (e.Row.Cells["ID_MODULE"].Value.ToString() == "8")
                    e.Row.Cells["ID_MODULE"].Text = setText("Quản trị hệ thống","Administrator");
                if (e.Row.Cells["Check"].Value.ToString() == "True")
                {
                    e.Row.CheckState = RowCheckState.Checked;
                }
                else
                    e.Row.CheckState = RowCheckState.Unchecked;
            }
        }

        private void dgListNhom_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GroupCurrent = Convert.ToInt64(e.Row.Cells["Ma_Nhom"].Value);
            lblNhomPhanQuyen.Text = setText("Phân quyền cho nhóm : ", "Permission for group:     ") + e.Row.Cells["TEN_NHOM"].Text;
            BindDataRole();
        }

        private void uiCommandManager1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                GROUP_ROLECollection grCollection = new GROUP_ROLECollection();
                foreach (GridEXRow row in dgListRole.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {
                        ROLE r = (ROLE)row.DataRow;
                        GROUP_ROLE gr = new GROUP_ROLE();
                        gr.GROUP_ID = GroupCurrent;
                        gr.ID_ROLE = r.ID;
                        grCollection.Add(gr);
                    }
                }
                GROUP_ROLE grUpdate = new GROUP_ROLE();
                grUpdate.GROUP_ID = GroupCurrent;
                grUpdate.InsertUpdateFull(grCollection);
                ShowMessage(setText("Cập nhật thành công","Save successfully "), false);
            }
            catch (Exception ex)
            {
                ShowMessage(setText("Lỗi : ","Error:  ")+ex.Message, false);
            }
        }
    }
}

