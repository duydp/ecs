GO

ALTER TABLE t_KDT_VNACCS_StorageAreasProduction_AttachedFiles
ALTER COLUMN FileName [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteBy_StorageAreasProduction_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Insert]
	@StorageAreasProduction_ID bigint,
	@FileName nvarchar(255),
	@FileSize bigint,
	@Content nvarchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
(
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@FileName,
	@FileSize,
	@Content
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@FileName nvarchar(255),
	@FileSize bigint,
	@Content nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[FileName] = @FileName,
	[FileSize] = @FileSize,
	[Content] = @Content
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@FileName nvarchar(255),
	@FileSize bigint,
	@Content nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[FileName] = @FileName,
			[FileSize] = @FileSize,
			[Content] = @Content
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
		(
			[StorageAreasProduction_ID],
			[FileName],
			[FileSize],
			[Content]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@FileName,
			@FileSize,
			@Content
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.5',GETDATE(), N'CẬP NHẬT PROCDEDURE FILE ĐÍNH KÈM CSSX')
END