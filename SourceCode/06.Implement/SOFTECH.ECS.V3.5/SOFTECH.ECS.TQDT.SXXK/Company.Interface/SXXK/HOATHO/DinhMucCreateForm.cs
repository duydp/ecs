﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucCreateForm : BaseForm
    {
        public DinhMucDangKy dmDangKy = new DinhMucDangKy();// { TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO };

        //-----------------------------------------------------------------------------------------
        public DinhMucCreateForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }

        //-----------------------------------------------------------------------------------------
        private void DinhMucSendForm_Load(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>


        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới định mức.
        /// </summary>
        private void add()
        {
            dmDangKy = new DinhMucDangKy();

            DinhMucEditForm f = new DinhMucEditForm();
            f.dmDangKy = dmDangKy;
            f.WindowState = FormWindowState.Normal;
            f.ShowDialog(this);
            try
            {
                dgList.DataSource = this.dmDangKy.DMCollection;

                dgList.Refetch();
            }
            catch { dgList.Refresh(); }

        }

        //-----------------------------------------------------------------------------------------
        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {


        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void save()
        {

        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdImportExcel":
                    this.ShowExcelForm();
                    break;
                case "SaoChep":
                    this.SaoChepDinhMuc();
                    break;
                case "InDM":
                    this.InDinhMuc();
                    break;
                case "Dinhmuc":
                    this.InDinhMuc();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdSuaDM":
                    //this.ChuyenTrangThaiSua();
                    break;
            }
        }
        private void inPhieuTN()
        {
            if (this.dmDangKy.SoTiepNhan == 0)
            {
                return;
            }
            Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
            phieuTN.phieu = "ĐỊNH MỨC";
            phieuTN.soTN = this.dmDangKy.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.dmDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void InDinhMuc()
        {
            if (dmDangKy.ID == 0)
            {
                ShowMessage("Lưu thông tin trước ghi in.", false);
                return;
            }
            Report.ReportViewDinhMucForm f = new Company.Interface.Report.ReportViewDinhMucForm();
            f.DMDangKy = this.dmDangKy;
            f.ShowDialog(this);
        }
        private void ShowExcelForm()
        {
            ImportKDTDMForm importExcel = new ImportKDTDMForm();
            importExcel.dmDangKy = dmDangKy;
            importExcel.ShowDialog(this);
            try
            {
                dgList.DataSource = dmDangKy.DMCollection;
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }
        private void SaoChepDinhMuc()
        {
            Company.Interface.SXXK.DinhMucRegistedForm f = new Company.Interface.SXXK.DinhMucRegistedForm();

        }


        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MLMessages("Bạn có muốn xóa định mức này không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
                        // Thực hiện xóa trong CSDL.
                        if (dmSelected.ID > 0)
                        {
                            dmSelected.Delete();
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void XoaDinhMuc(GridEXSelectedItemCollection items)
        {


        }



        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = dmDangKy.ID;
            form.DeclarationIssuer = DeclarationIssuer.SXXK_DINH_MUC;
            form.ShowDialog(this);
        }




    }
}

    
