﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages;

namespace Company.KDT.SHARE.Components
{
    public class GoodsItem
    {
        /// <summary>
        /// Số thứ tự n..5
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }
        /// <summary>
        /// Trị giá tính thuế n..16
        /// </summary>
        [XmlElement("statisticalValue")]
        public string StatisticalValue { get; set; }
        /// <summary>
        /// Nguyên tệ a3
        /// </summary>
        [XmlElement("CurrencyExchange")]
        public CurrencyExchange CurrencyExchange { get; set; }
        /// <summary>
        /// Thông tin kiện
        /// </summary>
        [XmlElement("ConsignmentItemPackaging")]
        public Packaging ConsignmentItemPackaging { get; set; }
        /// <summary>
        /// Chi Tiết hàng, Thuế
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
        /// <summary>
        /// Lượng cấp phép
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }
        /// <summary>
        /// Ghi chú khác
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        /// Xuất xứ
        /// </summary>
        [XmlElement("Origin")]
        public Origin Origin { get; set; }
        /// <summary>
        /// Hóa đơn thương mại
        /// </summary>
        [XmlElement("Invoice")]
        public Invoice Invoice { get; set; }


        #region Giấy đăng ký kiểm tra
        /// <summary>
        /// chung tu kem theo
        /// </summary>
        [XmlElement("AdditionalDocument")]
        public AdditionalDocument AdditionalDocument { get; set; }

        /// <summary>
        /// dia diem kiem tra
        /// </summary>
        [XmlElement("Examination")]
        public Examination Examination { get; set; }


        /// <summary>
        /// Co quan kiem tra
        /// </summary>
        [XmlElement("Examiner")]
        public NameBase Examiner { get; set; }
        #endregion

        #region Chứng thư giám định
        /// <summary>
        /// Van don cua hang giam dinh
        /// </summary>
        [XmlElement("BillOfLading")]
        public BillOfLading BillOfLading { get; set; }

        /// <summary>
        /// container hang giam dinh
        /// </summary>
        [XmlElement("EquipmentIdentification")]
        public EquipmentIdentification EquipmentIdentification { get; set; }

        #endregion

    }
}
