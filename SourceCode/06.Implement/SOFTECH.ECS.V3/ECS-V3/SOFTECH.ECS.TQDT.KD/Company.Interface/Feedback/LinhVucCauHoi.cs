using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Company.KDT.SHARE.Components.Feedback.Components;
using Janus.Windows.GridEX;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Columns;

namespace Company.Interface.Feedback
{
    public class LinhVucEventArg : EventArgs
    {
        public int ID { get; set; }
        public string Ten { get; set;   }
        public LinhVucEventArg(int id)
        {
            ID = id;
        }
    }
    public partial class LinhVucCauHoi : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];
        public event EventHandler<LinhVucEventArg> LoadCauHoiFromLinhVuc;
        private void Onclick(LinhVucEventArg e)
        {
            if (LoadCauHoiFromLinhVuc != null)
                LoadCauHoiFromLinhVuc(this, e);
        }

        public LinhVucCauHoi()
        {
            InitializeComponent();
        }



        private void LinhVucCauHoi_Load(object sender, EventArgs e)
        {

            LoadData();

        }
        private DataRow[] GetChilds(DataTable dtSource, int parentID)
        {
            if (dtSource == null)
                return null;

            DataRow[] ret = dtSource.Select("ParentID=" + parentID, "Thutu");
            return ret;
        }
        private void FillTreeList(DataRow[] rows, TreeListNode parent)
        {

            foreach (DataRow item in rows)
            {
                TreeListNode node = trlLinhVuc.AppendNode(new object[] { item["Ten"].ToString() }, parent, item);

                DataRow[] rows1 = GetChilds(dt, (int)item["ID"]);
                if (rows1.Length > 0)
                {
                    FillTreeList(rows1, node);
                }
            }
        }
        public void LoadData()
        {
            dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];

            DataRow[] drItems = GetChilds(dt, 0);
            if (drItems != null && drItems.Length > 0)
            {
                TreeListNode parentNode = trlLinhVuc.AppendNode(new object[] { "Danh sách lĩnh vực câu hỏi" }, null, null);

                FillTreeList(drItems, parentNode);
                parentNode.ExpandAll();
            }
        }

        private void trlLinhVuc_AfterFocusNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            if (e.Node.Tag != null)
            {
                DataRow dr = (DataRow)e.Node.Tag;
                //CauHoiUserControl cauhoi = new CauHoiUserControl();
                int ID = Convert.ToInt32(dr[0].ToString());
                //if (LoadCauHoiFromLinhVuc != null)
                //   LoadCauHoiFromLinhVuc(this, new LinhVucEventArg(ID));
                this.Onclick(new LinhVucEventArg(ID));
                //cauhoi.CauHoiUserControl_Load(null,null);
                //cauhoi.DataBinding(cauhoi.ID);
            }
            else
            {
                int ID = 0;
                this.Onclick(new LinhVucEventArg(ID));
            }

        }


    }
}
