﻿namespace Company.Interface
{
    partial class VNACC_GiayPhep_SAA_ThamTraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_GiayPhep_SAA_ThamTraForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucTinhTrangThamTra = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucPhanLoaiTraCuu = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucPhuongThucXuLyCuoiCung = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.dtGioGiaoViec = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayGiaoViec = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayKhaiBao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnTimKiem = new Janus.Windows.EditControls.UIButton();
            this.txtTongSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.txtMaNhanVienKiemTra = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNoiDungGiaoViec = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNguoiGiaoVIec = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSoDonXinCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaDonViCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTinhTrangThamTraTimKiem = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grdHang = new Janus.Windows.GridEX.GridEX();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMaSoHangHoa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaSoHangHoa)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 468), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 468);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 444);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 444);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grdHang);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(768, 468);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ucTinhTrangThamTra);
            this.uiGroupBox1.Controls.Add(this.ucPhanLoaiTraCuu);
            this.uiGroupBox1.Controls.Add(this.ucPhuongThucXuLyCuoiCung);
            this.uiGroupBox1.Controls.Add(this.dtGioGiaoViec);
            this.uiGroupBox1.Controls.Add(this.dtNgayGiaoViec);
            this.uiGroupBox1.Controls.Add(this.dtNgayKhaiBao);
            this.uiGroupBox1.Controls.Add(this.btnTimKiem);
            this.uiGroupBox1.Controls.Add(this.txtTongSoToKhai);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.txtMaNhanVienKiemTra);
            this.uiGroupBox1.Controls.Add(this.txtTenNguoiKhai);
            this.uiGroupBox1.Controls.Add(this.txtNoiDungGiaoViec);
            this.uiGroupBox1.Controls.Add(this.txtMaNguoiKhai);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.txtNguoiGiaoVIec);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.txtSoDonXinCapPhep);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtMaDonViCapPhep);
            this.uiGroupBox1.Controls.Add(this.txtTinhTrangThamTraTimKiem);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(768, 243);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ucTinhTrangThamTra
            // 
            this.ucTinhTrangThamTra.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucTinhTrangThamTra.Appearance.Options.UseBackColor = true;
            this.ucTinhTrangThamTra.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E030;
            this.ucTinhTrangThamTra.Code = "";
            this.ucTinhTrangThamTra.ColorControl = System.Drawing.Color.Empty;
            this.ucTinhTrangThamTra.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucTinhTrangThamTra.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucTinhTrangThamTra.IsOnlyWarning = false;
            this.ucTinhTrangThamTra.IsValidate = true;
            this.ucTinhTrangThamTra.Location = new System.Drawing.Point(204, 54);
            this.ucTinhTrangThamTra.Name = "ucTinhTrangThamTra";
            this.ucTinhTrangThamTra.Name_VN = "";
            this.ucTinhTrangThamTra.SetOnlyWarning = false;
            this.ucTinhTrangThamTra.SetValidate = false;
            this.ucTinhTrangThamTra.ShowColumnCode = true;
            this.ucTinhTrangThamTra.ShowColumnName = true;
            this.ucTinhTrangThamTra.Size = new System.Drawing.Size(49, 26);
            this.ucTinhTrangThamTra.TabIndex = 4;
            this.ucTinhTrangThamTra.TagName = "";
            this.ucTinhTrangThamTra.Where = null;
            this.ucTinhTrangThamTra.WhereCondition = "";
            this.ucTinhTrangThamTra.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucPhanLoaiTraCuu
            // 
            this.ucPhanLoaiTraCuu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucPhanLoaiTraCuu.Appearance.Options.UseBackColor = true;
            this.ucPhanLoaiTraCuu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E026;
            this.ucPhanLoaiTraCuu.Code = "";
            this.ucPhanLoaiTraCuu.ColorControl = System.Drawing.Color.Empty;
            this.ucPhanLoaiTraCuu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucPhanLoaiTraCuu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucPhanLoaiTraCuu.IsOnlyWarning = false;
            this.ucPhanLoaiTraCuu.IsValidate = true;
            this.ucPhanLoaiTraCuu.Location = new System.Drawing.Point(433, 54);
            this.ucPhanLoaiTraCuu.Name = "ucPhanLoaiTraCuu";
            this.ucPhanLoaiTraCuu.Name_VN = "";
            this.ucPhanLoaiTraCuu.SetOnlyWarning = false;
            this.ucPhanLoaiTraCuu.SetValidate = false;
            this.ucPhanLoaiTraCuu.ShowColumnCode = true;
            this.ucPhanLoaiTraCuu.ShowColumnName = true;
            this.ucPhanLoaiTraCuu.Size = new System.Drawing.Size(49, 26);
            this.ucPhanLoaiTraCuu.TabIndex = 5;
            this.ucPhanLoaiTraCuu.TagName = "";
            this.ucPhanLoaiTraCuu.Where = null;
            this.ucPhanLoaiTraCuu.WhereCondition = "";
            this.ucPhanLoaiTraCuu.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucPhuongThucXuLyCuoiCung
            // 
            this.ucPhuongThucXuLyCuoiCung.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucPhuongThucXuLyCuoiCung.Appearance.Options.UseBackColor = true;
            this.ucPhuongThucXuLyCuoiCung.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E031;
            this.ucPhuongThucXuLyCuoiCung.Code = "";
            this.ucPhuongThucXuLyCuoiCung.ColorControl = System.Drawing.Color.Empty;
            this.ucPhuongThucXuLyCuoiCung.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucPhuongThucXuLyCuoiCung.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucPhuongThucXuLyCuoiCung.IsOnlyWarning = false;
            this.ucPhuongThucXuLyCuoiCung.IsValidate = true;
            this.ucPhuongThucXuLyCuoiCung.Location = new System.Drawing.Point(645, 54);
            this.ucPhuongThucXuLyCuoiCung.Name = "ucPhuongThucXuLyCuoiCung";
            this.ucPhuongThucXuLyCuoiCung.Name_VN = "";
            this.ucPhuongThucXuLyCuoiCung.SetOnlyWarning = false;
            this.ucPhuongThucXuLyCuoiCung.SetValidate = false;
            this.ucPhuongThucXuLyCuoiCung.ShowColumnCode = true;
            this.ucPhuongThucXuLyCuoiCung.ShowColumnName = true;
            this.ucPhuongThucXuLyCuoiCung.Size = new System.Drawing.Size(49, 26);
            this.ucPhuongThucXuLyCuoiCung.TabIndex = 6;
            this.ucPhuongThucXuLyCuoiCung.TagName = "";
            this.ucPhuongThucXuLyCuoiCung.Where = null;
            this.ucPhuongThucXuLyCuoiCung.WhereCondition = "";
            this.ucPhuongThucXuLyCuoiCung.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // dtGioGiaoViec
            // 
            this.dtGioGiaoViec.CustomFormat = "hh:mm tt";
            this.dtGioGiaoViec.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtGioGiaoViec.DropDownCalendar.Name = "";
            this.dtGioGiaoViec.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtGioGiaoViec.Location = new System.Drawing.Point(265, 138);
            this.dtGioGiaoViec.Name = "dtGioGiaoViec";
            this.dtGioGiaoViec.Size = new System.Drawing.Size(74, 21);
            this.dtGioGiaoViec.TabIndex = 13;
            this.dtGioGiaoViec.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayGiaoViec
            // 
            // 
            // 
            // 
            this.dtNgayGiaoViec.DropDownCalendar.Name = "";
            this.dtNgayGiaoViec.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayGiaoViec.Location = new System.Drawing.Point(120, 138);
            this.dtNgayGiaoViec.Name = "dtNgayGiaoViec";
            this.dtNgayGiaoViec.Size = new System.Drawing.Size(133, 21);
            this.dtNgayGiaoViec.TabIndex = 12;
            this.dtNgayGiaoViec.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayKhaiBao
            // 
            // 
            // 
            // 
            this.dtNgayKhaiBao.DropDownCalendar.Name = "";
            this.dtNgayKhaiBao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayKhaiBao.Location = new System.Drawing.Point(574, 84);
            this.dtNgayKhaiBao.Name = "dtNgayKhaiBao";
            this.dtNgayKhaiBao.Size = new System.Drawing.Size(120, 21);
            this.dtNgayKhaiBao.TabIndex = 9;
            this.dtNgayKhaiBao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayKhaiBao.VisualStyleManager = this.vsmMain;
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Image = ((System.Drawing.Image)(resources.GetObject("btnTimKiem.Image")));
            this.btnTimKiem.ImageIndex = 4;
            this.btnTimKiem.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTimKiem.Location = new System.Drawing.Point(671, 13);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(90, 23);
            this.btnTimKiem.TabIndex = 3;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTimKiem.VisualStyleManager = this.vsmMain;
            // 
            // txtTongSoToKhai
            // 
            this.txtTongSoToKhai.DecimalDigits = 0;
            this.txtTongSoToKhai.Enabled = false;
            this.txtTongSoToKhai.Location = new System.Drawing.Point(574, 15);
            this.txtTongSoToKhai.Name = "txtTongSoToKhai";
            this.txtTongSoToKhai.Size = new System.Drawing.Size(91, 21);
            this.txtTongSoToKhai.TabIndex = 2;
            this.txtTongSoToKhai.Text = "0";
            this.txtTongSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(287, 215);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 18;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Enabled = false;
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGhi.Location = new System.Drawing.Point(120, 215);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 16;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Enabled = false;
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(204, 215);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 17;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // txtMaNhanVienKiemTra
            // 
            this.txtMaNhanVienKiemTra.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNhanVienKiemTra.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNhanVienKiemTra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNhanVienKiemTra.Location = new System.Drawing.Point(401, 84);
            this.txtMaNhanVienKiemTra.Name = "txtMaNhanVienKiemTra";
            this.txtMaNhanVienKiemTra.Size = new System.Drawing.Size(81, 21);
            this.txtMaNhanVienKiemTra.TabIndex = 8;
            this.txtMaNhanVienKiemTra.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNhanVienKiemTra.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNguoiKhai
            // 
            this.txtTenNguoiKhai.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenNguoiKhai.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenNguoiKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiKhai.Location = new System.Drawing.Point(265, 111);
            this.txtTenNguoiKhai.Name = "txtTenNguoiKhai";
            this.txtTenNguoiKhai.Size = new System.Drawing.Size(496, 21);
            this.txtTenNguoiKhai.TabIndex = 11;
            this.txtTenNguoiKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNoiDungGiaoViec
            // 
            this.txtNoiDungGiaoViec.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNoiDungGiaoViec.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNoiDungGiaoViec.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDungGiaoViec.Location = new System.Drawing.Point(120, 165);
            this.txtNoiDungGiaoViec.Multiline = true;
            this.txtNoiDungGiaoViec.Name = "txtNoiDungGiaoViec";
            this.txtNoiDungGiaoViec.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNoiDungGiaoViec.Size = new System.Drawing.Size(617, 44);
            this.txtNoiDungGiaoViec.TabIndex = 15;
            this.txtNoiDungGiaoViec.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNoiDungGiaoViec.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNguoiKhai
            // 
            this.txtMaNguoiKhai.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNguoiKhai.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNguoiKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiKhai.Location = new System.Drawing.Point(120, 111);
            this.txtMaNguoiKhai.Name = "txtMaNguoiKhai";
            this.txtMaNguoiKhai.Size = new System.Drawing.Size(133, 21);
            this.txtMaNguoiKhai.TabIndex = 10;
            this.txtMaNguoiKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 170);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Nội dung giao việc";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Ngày giao việc";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Người khai";
            // 
            // txtNguoiGiaoVIec
            // 
            this.txtNguoiGiaoVIec.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNguoiGiaoVIec.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNguoiGiaoVIec.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiGiaoVIec.Location = new System.Drawing.Point(456, 138);
            this.txtNguoiGiaoVIec.Name = "txtNguoiGiaoVIec";
            this.txtNguoiGiaoVIec.Size = new System.Drawing.Size(115, 21);
            this.txtNguoiGiaoVIec.TabIndex = 14;
            this.txtNguoiGiaoVIec.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiGiaoVIec.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(370, 143);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Người giao việc";
            // 
            // txtSoDonXinCapPhep
            // 
            this.txtSoDonXinCapPhep.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDonXinCapPhep.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDonXinCapPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDonXinCapPhep.Location = new System.Drawing.Point(120, 84);
            this.txtSoDonXinCapPhep.Name = "txtSoDonXinCapPhep";
            this.txtSoDonXinCapPhep.Size = new System.Drawing.Size(133, 21);
            this.txtSoDonXinCapPhep.TabIndex = 7;
            this.txtSoDonXinCapPhep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDonXinCapPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Số đơn xin cấp phép";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(283, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Mã nhân viên kiểm tra";
            // 
            // txtMaDonViCapPhep
            // 
            this.txtMaDonViCapPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonViCapPhep.Location = new System.Drawing.Point(120, 15);
            this.txtMaDonViCapPhep.Name = "txtMaDonViCapPhep";
            this.txtMaDonViCapPhep.Size = new System.Drawing.Size(133, 21);
            this.txtMaDonViCapPhep.TabIndex = 0;
            this.txtMaDonViCapPhep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonViCapPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTinhTrangThamTraTimKiem
            // 
            this.txtTinhTrangThamTraTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTinhTrangThamTraTimKiem.Location = new System.Drawing.Point(456, 15);
            this.txtTinhTrangThamTraTimKiem.MaxLength = 255;
            this.txtTinhTrangThamTraTimKiem.Name = "txtTinhTrangThamTraTimKiem";
            this.txtTinhTrangThamTraTimKiem.Size = new System.Drawing.Size(26, 21);
            this.txtTinhTrangThamTraTimKiem.TabIndex = 1;
            this.txtTinhTrangThamTraTimKiem.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTinhTrangThamTraTimKiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(283, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Phân loại tra cứu";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Mã đơn vị cấp phép";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(495, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Phương thức xử lý cuối cùng";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(493, 89);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ngày khai báo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(488, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tổng số tờ khai";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(259, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(191, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Thông tin tình trạng thẩm tra/ kiểm tra";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(191, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Thông tin tình trạng thẩm tra/ kiểm tra";
            // 
            // grdHang
            // 
            this.grdHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHang.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.ColumnAutoResize = true;
            grdHang_DesignTimeLayout.LayoutString = resources.GetString("grdHang_DesignTimeLayout.LayoutString");
            this.grdHang.DesignTimeLayout = grdHang_DesignTimeLayout;
            this.grdHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdHang.GroupByBoxVisible = false;
            this.grdHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.Location = new System.Drawing.Point(0, 243);
            this.grdHang.Name = "grdHang";
            this.grdHang.RecordNavigator = true;
            this.grdHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdHang.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdHang.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdHang.Size = new System.Drawing.Size(768, 225);
            this.grdHang.TabIndex = 1;
            this.grdHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdHang.VisualStyleManager = this.vsmMain;
            this.grdHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdHang_RowDoubleClick);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvMaSoHangHoa
            // 
            this.rfvMaSoHangHoa.ControlToValidate = this.txtSoDonXinCapPhep;
            this.rfvMaSoHangHoa.ErrorMessage = "\"Mã số hàng hóa\" không được để trống.";
            this.rfvMaSoHangHoa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaSoHangHoa.Icon")));
            this.rfvMaSoHangHoa.Tag = "rfvMaSoHangHoa";
            // 
            // VNACC_GiayPhep_SAA_ThamTraForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 474);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_GiayPhep_SAA_ThamTraForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin tình trạng thẩm tra/ kiểm tra";
            this.Load += new System.EventHandler(this.VNACC_GiayPhep_SAA_ThamTraForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaSoHangHoa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grdHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDonXinCapPhep;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaSoHangHoa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoToKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonViCapPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinhTrangThamTraTimKiem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIButton btnTimKiem;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayGiaoViec;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayKhaiBao;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNhanVienKiemTra;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiKhai;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.CalendarCombo.CalendarCombo dtGioGiaoViec;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiGiaoVIec;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDungGiaoViec;
        private System.Windows.Forms.Label label12;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucPhuongThucXuLyCuoiCung;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucTinhTrangThamTra;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucPhanLoaiTraCuu;
        private System.Windows.Forms.Label label13;
    }
}