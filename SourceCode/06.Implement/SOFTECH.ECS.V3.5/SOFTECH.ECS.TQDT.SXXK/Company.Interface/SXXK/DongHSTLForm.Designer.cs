namespace Company.Interface.SXXK
{
    partial class DongHSTLForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DongHSTLForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayKetThuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayQD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoQD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHoSo = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiButton1);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(475, 139);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ccNgayKetThuc);
            this.uiGroupBox1.Controls.Add(this.ccNgayQD);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.txtSoQD);
            this.uiGroupBox1.Controls.Add(this.txtSoHoSo);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(431, 90);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Nhập thông tin đóng hồ sơ";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayKetThuc
            // 
            this.ccNgayKetThuc.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayKetThuc.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayKetThuc.DropDownCalendar.Name = "";
            this.ccNgayKetThuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKetThuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKetThuc.Location = new System.Drawing.Point(326, 24);
            this.ccNgayKetThuc.Name = "ccNgayKetThuc";
            this.ccNgayKetThuc.Nullable = true;
            this.ccNgayKetThuc.NullButtonText = "Xóa";
            this.ccNgayKetThuc.ShowNullButton = true;
            this.ccNgayKetThuc.Size = new System.Drawing.Size(82, 21);
            this.ccNgayKetThuc.TabIndex = 3;
            this.ccNgayKetThuc.TodayButtonText = "Hôm nay";
            this.ccNgayKetThuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKetThuc.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayQD
            // 
            this.ccNgayQD.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayQD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayQD.DropDownCalendar.Name = "";
            this.ccNgayQD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayQD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayQD.Location = new System.Drawing.Point(326, 54);
            this.ccNgayQD.Name = "ccNgayQD";
            this.ccNgayQD.Nullable = true;
            this.ccNgayQD.NullButtonText = "Xóa";
            this.ccNgayQD.ShowNullButton = true;
            this.ccNgayQD.Size = new System.Drawing.Size(82, 21);
            this.ccNgayQD.TabIndex = 7;
            this.ccNgayQD.TodayButtonText = "Hôm nay";
            this.ccNgayQD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayQD.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(246, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ngày QĐ đóng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(246, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ngày kết thúc";
            // 
            // txtSoQD
            // 
            this.txtSoQD.Location = new System.Drawing.Point(90, 54);
            this.txtSoQD.Name = "txtSoQD";
            this.txtSoQD.Size = new System.Drawing.Size(100, 21);
            this.txtSoQD.TabIndex = 5;
            this.txtSoQD.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHoSo
            // 
            this.txtSoHoSo.DecimalDigits = 0;
            this.txtSoHoSo.Location = new System.Drawing.Point(90, 24);
            this.txtSoHoSo.Name = "txtSoHoSo";
            this.txtSoHoSo.Size = new System.Drawing.Size(55, 21);
            this.txtSoHoSo.TabIndex = 1;
            this.txtSoHoSo.Text = "0";
            this.txtSoHoSo.Value = 0;
            this.txtSoHoSo.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoHoSo.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Số QĐ đóng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số hồ sơ";
            // 
            // uiButton1
            // 
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(144, 110);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(158, 23);
            this.uiButton1.TabIndex = 1;
            this.uiButton1.Text = "Thực hiện đóng hồ sơ";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // DongHSTLForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 139);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "DongHSTLForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Đóng hồ sơ thanh khoản";
            this.Load += new System.EventHandler(this.DongHSTLForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQD;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoHoSo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKetThuc;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayQD;
        private Janus.Windows.EditControls.UIButton uiButton1;
    }
}