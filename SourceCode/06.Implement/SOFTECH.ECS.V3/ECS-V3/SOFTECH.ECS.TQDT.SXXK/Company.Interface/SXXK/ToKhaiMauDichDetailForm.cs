﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using Company.Interface.Report;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
//using Company.Interface.Reports;

namespace Company.Interface.SXXK
{
    public partial class ToKhaiMauDichDetailForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public string NhomLoaiHinh = string.Empty;

        public ToKhaiMauDichDetailForm()
        {
            InitializeComponent();
        }

        private void loadTKMDData()
        {
            ctrDonViHaiQuan.Ma = this.TKMD.MaHaiQuan;
            txtSoLuongPLTK.Value = this.TKMD.SoLuongPLTK;
            txtSotk.Text = this.TKMD.SoToKhai.ToString();
            cbNgayDangky.Text = this.TKMD.NgayDangKy.ToShortDateString();
            if (this.TKMD.NGAY_THN_THX.Year > 1900)
                cbNgayThucNX.Text = this.TKMD.NGAY_THN_THX.ToString("dd/MM/yyyy HH:mm");
            else
                cbNgayThucNX.Text = "";
            if (this.TKMD.NgayHoanThanh.Year > 1900)
                cboDateNgayHT.Text = this.TKMD.NgayHoanThanh.ToString("dd/MM/yyyy HH:mm");
            else
                cboDateNgayHT.Text = "";

            switch (this.TKMD.ThanhLy)
            {
                case "":
                    //lblTrangThai.Text = "Chưa thanh khoản";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa thanh khoản";
                    }
                    else
                    {
                        lblTrangThai.Text = "Not yet liquidated";
                    }
                    break;
                case "H":
                    //lblTrangThai.Text = "Đã thanh khoản hết";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã thanh khoản hết";
                    }
                    else
                    {
                        lblTrangThai.Text = "Đã thanh khoản hết";
                    }
                    break;
                case "L":
                    //lblTrangThai.Text = "Thanh khoản một phần";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Thanh khoản một phần";
                    }
                    else
                    {
                        lblTrangThai.Text = "Partial liquidation";
                    }
                    break;
            }
            // Doanh nghiệp.
            txtMaDonVi.Text = this.TKMD.MaDoanhNghiep;
            //txtTenDonVi.Text = this.TKMD.TenDoanhNghiep;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.Text = this.TKMD.TenDonViDoiTac;

            // Đại lý TTHQ.
            txtMaDaiLy.Text = this.TKMD.MaDaiLyTTHQ;
            txtTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;

            txtMaDonViUyThac.Text = this.TKMD.MaDonViUT;
            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Ma = this.TKMD.MaLoaiHinh;

            // Phương tiện vận tải.
            cbPTVT.SelectedValue = this.TKMD.PTVT_ID;
            txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
            ccNgayDen.Text = this.TKMD.NgayDenPTVT.ToShortDateString();

            // Nước.
            if (this.TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;
            else
                ctrNuocXuatKhau.Ma = this.TKMD.NuocNK_ID;

            // ĐKGH.
            cbDKGH.SelectedValue = this.TKMD.DKGH_ID;

            // PTTT.
            cbPTTT.SelectedValue = this.TKMD.PTTT_ID;

            // Giấy phép.
            txtSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep.Year > 1900) ccNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToShortDateString();
            if (this.TKMD.NgayHetHanGiayPhep.Year > 1900) ccNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToShortDateString();
            if (this.TKMD.SoGiayPhep.Length > 0) chkGiayPhep.Checked = true;

            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.Text = this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai.Year > 1900) ccNgayHDTM.Text = this.TKMD.NgayHoaDonThuongMai.ToShortDateString();
            if (this.TKMD.SoHoaDonThuongMai.Length > 0) chkHoaDonThuongMai.Checked = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;

            // Nguyên tệ.
            ctrNguyenTe.Ma = this.TKMD.NguyenTe_ID;
            txtTyGiaTinhThue.Value = this.TKMD.TyGiaTinhThue;
            txtTyGiaUSD.Value = this.TKMD.TyGiaUSD;

            // Hợp đồng.
            txtSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong.Year > 1900) ccNgayHopDong.Text = this.TKMD.NgayHopDong.ToShortDateString();
            if (this.TKMD.NgayHetHanHopDong.Year > 1900) ccNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToShortDateString();
            if (this.TKMD.SoHopDong.Length > 0) chkHopDong.Checked = true;

            // Vận tải đơn.
            txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
            if (this.TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToShortDateString();
            if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
            if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

            // Tên chủ hàng.
            txtTenChuHang.Text = this.TKMD.TenChuHang;

            // Container 20.
            txtSoContainer20.Value = this.TKMD.SoContainer20;

            // Container 40.
            txtSoContainer40.Value = this.TKMD.SoContainer40;

            // Số kiện hàng.
            txtSoKien.Value = this.TKMD.SoKien;

            // Trọng lượng.
            txtTrongLuong.Value = this.TKMD.TrongLuong;

            // Lệ phí HQ.
            txtLePhiHQ.Value = this.TKMD.LePhiHaiQuan;

            // Phí BH.
            txtPhiBaoHiem.Value = this.TKMD.PhiBaoHiem;
            txtPhiKhac.Value = this.TKMD.PhiKhac;
            txtTrongLuongTinh.Value = this.TKMD.TrongLuongNet;
            // Phí VC.
            txtPhiVanChuyen.Value = this.TKMD.PhiVanChuyen;
            //chung tu
            txtChungTuTK.Text = this.TKMD.ChungTu;
            if (this.TKMD.Xuat_NPL_SP == "S")
            {
                radSP.Checked = true;
                radNPL.Checked = false;
                radTB.Checked = false;
            }
            else if (this.TKMD.Xuat_NPL_SP == "N")
            {
                radSP.Checked = false;
                radNPL.Checked = true;
                radTB.Checked = false;
            }
            else if (this.TKMD.Xuat_NPL_SP == "T")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = true;
            }

            this.TKMD.LoadHMDCollection();
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();
        }

        private void khoitao_DuLieuChuan()
        {
            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Nhom = this.NhomLoaiHinh;
            ctrLoaiHinhMauDich.Ma = this.NhomLoaiHinh + "01";

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;
        }

        private void khoitao_GiaoDienToKhai()
        {
            this.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 3);
            string loaiToKhai = this.NhomLoaiHinh.Substring(0, 1);
            if (loaiToKhai == "N")
            {
                //Updated by Hungtq, 05/09/2012.
                dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

                // Tiêu đề.
                if (GlobalSettings.NGON_NGU == "0")
                {
                    this.Text = "Tờ khai nhập khẩu (" + this.NhomLoaiHinh + ")";
                }
                else
                {
                    this.Text = "Import declaration (" + this.NhomLoaiHinh + ")";
                }

            }
            else
            {
                //Updated by Hungtq, 05/09/2012.
                dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;

                // Tiêu đề.
                if (GlobalSettings.NGON_NGU == "0")
                {
                    this.Text = "Tờ khai xuất khẩu (" + this.NhomLoaiHinh + ")";
                }
                else
                {
                    this.Text = "Export declaration (" + this.NhomLoaiHinh + ")";
                }

                this.uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                // Người XK / Người NK.
                //grbNguoiNK.Text = "Người xuất khẩu";
                //grbNguoiXK.Text = "Người nhập khẩu";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    grbNguoiNK.Text = "Người xuất khẩu";
                    grbNguoiXK.Text = "Người nhập khẩu";
                }
                else
                {
                    grbNguoiNK.Text = "Importer";
                    grbNguoiXK.Text = "Exporter";
                }

                // Phương tiện vận tải.
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = false;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = false;

                // Vận tải đơn.
                chkVanTaiDon.Visible = false;
                grbVanTaiDon.Visible = false;
                if (GlobalSettings.NGON_NGU == "0")
                {
                    grbNuocXK.Text = "Nước nhập khẩu";
                    grbDiaDiemDoHang.Text = "Cửa khẩu xuất hàng";
                }
                else
                {
                    grbNuocXK.Text = "Import country";
                    grbDiaDiemDoHang.Text = "Export country";
                }
                // Nước XK.
                //grbNuocXK.Text = "Nước nhập khẩu";

                // Địa điểm xếp hàng / Địa điểm dỡ hàng.
                //grbDiaDiemDoHang.Text = "Địa điểm xếp hàng";
                chkDiaDiemXepHang.Visible = grbDiaDiemXepHang.Visible = false;
            }

            if (this.NhomLoaiHinh == "NSX" || this.NhomLoaiHinh == "XSX")
            {
                if (this.NhomLoaiHinh == "NSX")
                {
                    grpLoaiHangHoa.Visible = false;
                    radNPL.Checked = true;
                    radSP.Checked = false;
                    radTB.Checked = false;

                }
                else
                {
                    radTB.Visible = false;
                }
            }
            else
                grpLoaiHangHoa.Visible = false;

        }

        //-----------------------------------------------------------------------------------------

        private void ToKhaiNhapKhauSXXK_Form_Load(object sender, EventArgs e)
        {

            //ccNgayDen.Value = DateTime.Today;
            this.loadTKMDData();
            TKMD.Load();
            //if (TKMD.MaLoaiHinh.StartsWith("N"))
            //    cmdPhanBo.Visible = Janus.Windows.UI.InheritableBoolean.False;
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDienToKhai();
            // Người nhập khẩu / Người xuất khẩu.
            txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
            txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;


            this.ViewTKMD();
            this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);


            //txtTenDonViDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
            {
                uiCommandBar1.Visible = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
        }

        private void ViewTKMD()
        {
            ctrDonViHaiQuan.ReadOnly = true;
            txtSotk.ReadOnly = true;
            // Doanh nghiệp.
            txtMaDonVi.ReadOnly = true;
            txtTenDonVi.ReadOnly = true;


        }

        //-----------------------------------------------------------------------------------------
        private void chkHoaDonThuongMai_CheckedChanged(object sender, EventArgs e)
        {
            grbHoaDonThuongMai.Enabled = chkHoaDonThuongMai.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkGiayPhep_CheckedChanged(object sender, EventArgs e)
        {
            gbGiayPhep.Enabled = chkGiayPhep.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHopDong_CheckedChanged(object sender, EventArgs e)
        {
            grbHopDong.Enabled = chkHopDong.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkVanTaiDon_CheckedChanged(object sender, EventArgs e)
        {
            grbVanTaiDon.Enabled = chkVanTaiDon.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkDiaDiemXepHang_CheckedChanged(object sender, EventArgs e)
        {
            grbDiaDiemXepHang.Enabled = chkDiaDiemXepHang.Checked;
        }
        //-----------------------------------------------------------------------------------------    

        private void tinhLaiThue()
        {
            this.TKMD.TongTriGiaKhaiBao = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                // Tính lại thuế.
                hmd.TinhThue(Convert.ToDecimal(txtTyGiaTinhThue.Value));
                // Tổng trị giá khai báo.
                this.TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
            }
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();
        }
        //-----------------------------------------------------------------------------------------

        private void inToKhai()
        {
            //ReportViewerForm f = new ReportViewerForm();
            //f.TKMD = this.TKMD;
            //f.ShowDialog(this);
        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdTinhLaiThue":
                    this.tinhLaiThue();
                    break;
                case "cmdPrint":
                    this.inToKhai();
                    break;


            }
        }

        //-----------------------------------------------------------------------------------------
        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            //switch (this.NhomLoaiHinh)
            //{
            //    case "NGC":
            //        HopDongRegistedForm f = new HopDongRegistedForm();
            //        f.HopDongSelected.MaHaiQuan = ctrDonViHaiQuan.Ma;
            //        f.IsBrowseForm = true;
            //        f.ShowDialog(this);
            //        if (f.HopDongSelected.SoHopDong != "")
            //        {
            //            this.HDGC = f.HopDongSelected;
            //            txtSoHopDong.Text = f.HopDongSelected.SoHopDong;
            //            ccNgayHopDong.Value = f.HopDongSelected.NgayKy;
            //            ccNgayHHHopDong.Value = f.HopDongSelected.NgayHetHan;
            //        }
            //        break;
            //    case "XGC":
            //        HopDongRegistedForm f1 = new HopDongRegistedForm();
            //        f1.HopDongSelected.MaHaiQuan = ctrDonViHaiQuan.Ma;
            //        f1.IsBrowseForm = true;
            //        f1.ShowDialog(this);     
            //        if (f1.HopDongSelected.SoHopDong != "")
            //        {
            //            this.HDGC = f1.HopDongSelected;
            //            txtSoHopDong.Text = f1.HopDongSelected.SoHopDong;
            //            ccNgayHopDong.Value = f1.HopDongSelected.NgayKy;
            //            ccNgayHHHopDong.Value = f1.HopDongSelected.NgayHetHan;
            //        }
            //        break;
            //}
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangMauDichRegisterForm f = new HangMauDichRegisterForm();
                    f.HMD = (HangMauDich)i.GetRow().DataRow;
                    if (radNPL.Checked) this.TKMD.Xuat_NPL_SP = "N";
                    if (radSP.Checked) this.TKMD.Xuat_NPL_SP = "S";
                    if (radTB.Checked) this.TKMD.Xuat_NPL_SP = "T";
                    f.LoaiHangHoa = this.TKMD.Xuat_NPL_SP;
                    f.NhomLoaiHinh = this.NhomLoaiHinh;
                    f.MaHaiQuan = this.TKMD.MaHaiQuan;
                    f.MaNguyenTe = ctrNguyenTe.Ma;
                    f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
                    f.HMDCollection = this.TKMD.HMDCollection;
                    f.TKMD = TKMD;
                    f.create = false;
                    f.ShowDialog(this);
                    this.TKMD.HMDCollection = f.HMDCollection;
                    try
                    {
                        dgList.DataSource = TKMD.HMDCollection;
                        dgList.Refetch();
                    }
                    catch
                    {
                        dgList.Refresh();
                    }
                }
                break;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text.ToString());
        }
        public void Save()
        {
            bool valid = false;
            cvError.ContainerToValidate = this.txtTenDonViDoiTac;
            cvError.Validate();
            bool valid1 = cvError.IsValid;
            cvError.ContainerToValidate = this.grbNguyenTe;
            cvError.Validate();
            bool valid2 = cvError.IsValid;
            valid = valid1 && valid2;

            if (valid)
            {

                if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
                {
                    //epError.SetError(txtTyGiaTinhThue, "Tỷ giá tính thuế không hợp lệ.");
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        epError.SetError(txtTyGiaTinhThue, "Tỷ giá tính thuế không hợp lệ.");
                    }
                    else
                    {
                        epError.SetError(txtTyGiaTinhThue, "Tax rate is not valid.");

                    }
                    epError.SetIconPadding(txtTyGiaTinhThue, -8);
                    return;

                }
                this.TKMD.MaHaiQuan = ctrDonViHaiQuan.Ma;

                this.TKMD.SoHang = (short)this.TKMD.HMDCollection.Count;
                this.TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);

                // Doanh nghiệp.
                this.TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                // Đơn vị đối tác.
                this.TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;



                this.TKMD.MaDonViUT = txtMaDonViUyThac.Text;

                // Đại lý TTHQ.
                this.TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                this.TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                //Ngay thuc nhap xuat 
                if (cbNgayThucNX.Text.Trim().Length > 0)
                    this.TKMD.NGAY_THN_THX = cbNgayThucNX.Value;
                else
                    this.TKMD.NGAY_THN_THX = new DateTime(1900, 1, 1);

                //Ngay thuc hoan thanh :
                if (cboDateNgayHT.Text.Trim().Length > 0)
                    this.TKMD.NgayHoanThanh = cboDateNgayHT.Value;
                else
                    this.TKMD.NgayHoanThanh = new DateTime(1900, 1, 1);
                // Loại hình mậu dịch.

                // Giấy phép.
                if (chkGiayPhep.Checked)
                {
                    this.TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                    this.TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
                    this.TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;
                }
                else
                {
                    this.TKMD.SoGiayPhep = "";
                    this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }

                // 7. Hợp đồng.
                if (chkHopDong.Checked)
                {
                    this.TKMD.SoHopDong = txtSoHopDong.Text;
                    // Ngày HD.
                    if (ccNgayHopDong.IsNullDate)
                        this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHopDong = ccNgayHopDong.Value;
                    // Ngày hết hạn HD.
                    if (ccNgayHHHopDong.IsNullDate)
                        this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;
                }
                else
                {
                    this.TKMD.SoHopDong = "";
                    this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                }

                // Hóa đơn thương mại.
                if (chkHoaDonThuongMai.Checked)
                {
                    this.TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                    if (ccNgayHDTM.IsNullDate)
                        this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;
                }
                else
                {
                    this.TKMD.SoHoaDonThuongMai = "";
                    this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                }

                // Phương tiện vận tải.
                this.TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                this.TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                this.TKMD.NgayDenPTVT = ccNgayDen.Value;

                // Vận tải đơn.
                if (chkVanTaiDon.Checked)
                {
                    this.TKMD.SoVanDon = txtSoVanTaiDon.Text;
                    if (ccNgayVanTaiDon.IsNullDate)
                        this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayVanDon = ccNgayVanTaiDon.Value;
                }
                else
                {
                    this.TKMD.SoVanDon = "";
                    this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                }

                // Nước.
                if (this.TKMD.MaLoaiHinh.Contains("NSX"))
                {
                    this.TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                    this.TKMD.NuocNK_ID = "VN".PadRight(3);
                }
                else
                {
                    this.TKMD.NuocXK_ID = "VN".PadRight(3);
                    this.TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
                }

                // Địa điểm xếp hàng.
                if (chkDiaDiemXepHang.Checked)
                    this.TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                else
                    this.TKMD.DiaDiemXepHang = "";

                // Địa điểm dỡ hàng.
                this.TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                // Điều kiện giao hàng.
                this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                // Đồng tiền thanh toán.
                this.TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                // Phương thức thanh toán.
                this.TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                //
                this.TKMD.TenChuHang = txtTenChuHang.Text;
                this.TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                this.TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                this.TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                this.TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                // Tổng trị giá khai báo (Chưa tính).
                this.TKMD.TongTriGiaKhaiBao = 0;
                //

                this.TKMD.LoaiToKhaiGiaCong = "NPL";
                //
                this.TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);

                this.TKMD.NgayDangKy = cbNgayDangky.Value;
                this.TKMD.NgayHoanThanh = cboDateNgayHT.Value;
                this.TinhTongTriGiaKhaiBao();
                try
                {
                    //this.TKMD.InsertUpdate();    
                    this.TKMD.Update();
                    // ShowMessage("Lưu thông tin tờ khai thành công.", false);
                    MLMessages("Cập nhật thành công!", "MSG_SAV02", "", false);
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Source, false);
                }
            }
        }

        private void TinhTongTriGiaKhaiBao()
        {
            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                this.TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
                this.TKMD.TongTriGiaTinhThue += hmd.TriGiaTT;
            }
        }


        private void cmMain_CommandClick_1(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            if (e.Command.Key == "ThemHang")
            {
                HangMauDichRegisterForm f = new HangMauDichRegisterForm();
                f.HMD = new HangMauDich();
                if (radNPL.Checked) this.TKMD.Xuat_NPL_SP = "N";
                if (radSP.Checked) this.TKMD.Xuat_NPL_SP = "S";
                if (radTB.Checked) this.TKMD.Xuat_NPL_SP = "T";
                f.LoaiHangHoa = this.TKMD.Xuat_NPL_SP;
                f.NhomLoaiHinh = this.NhomLoaiHinh;
                f.MaHaiQuan = this.TKMD.MaHaiQuan;
                f.MaNguyenTe = ctrNguyenTe.Ma;
                f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
                f.HMDCollection = this.TKMD.HMDCollection;
                f.TKMD = TKMD;
                f.create = true;
                f.ShowDialog(this);
                this.TKMD.HMDCollection = f.HMDCollection;
                try
                {
                    dgList.DataSource = TKMD.HMDCollection;
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
            else if (e.Command.Key == "cmdSave")
            {
                Save();
            }
            else if (e.Command.Key == "cmdPhanBo")
            {
                try
                {
                    ToKhaiMauDich TKMDXuat = new ToKhaiMauDich();
                    TKMD.LoadHMDCollection();
                    if (TKMD.MaLoaiHinh.StartsWith("X") && TKMD.TrangThai == 0)
                    {
                        TKMD.LoadPhanBo();
                        // if (ShowMessage("Bạn có muốn xóa phân bổ của tờ khai này không?", true) != "Yes")
                        if (MLMessages("Bạn có muốn xóa phân bổ của tờ khai này không?", "MSG_DEL01", "", true) != "Yes")
                            return;
                    }
                    else if (TKMD.MaLoaiHinh.StartsWith("X") && TKMD.TrangThai == 1)
                    {
                        // ShowMessage("Tờ khai này chưa được phân bổ.", false);MSG_THK70
                        MLMessages("Tờ khai này chưa được phân bổ.", "MSG_THK70", "", false);
                        return;
                    }
                    else if (TKMD.MaLoaiHinh.StartsWith("X") && TKMD.TrangThai == 2)
                    {
                        // ShowMessage("Tờ khai này không được phân bổ.", false);
                        MLMessages("Tờ khai này không được phân bổ.", "MSG_THK70", "", false);
                        return;
                    }
                    else if (TKMD.MaLoaiHinh.StartsWith("N"))
                    {
                        PhanBoToKhaiXuat pbTKX = PhanBoToKhaiNhap.SelectToKhaiXuatDuocPhanBoDauTienChoToKhaiNhap(GlobalSettings.MA_HAI_QUAN, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NamDangKy);
                        if (pbTKX == null)
                        {
                            //ShowMessage("Tờ khai này chưa được phân bổ.", false);
                            MLMessages("Tờ khai này chưa được phân bổ.", "MSG_THK70", "", false);
                            return;
                        }
                        TKMDXuat.SoToKhai = pbTKX.SoToKhaiXuat;
                        TKMDXuat.NamDangKy = pbTKX.NamDangKyXuat;
                        TKMDXuat.MaHaiQuan = pbTKX.MaHaiQuanXuat;
                        TKMDXuat.MaLoaiHinh = pbTKX.MaLoaiHinhXuat;
                        TKMDXuat.Load();
                        TKMDXuat.LoadHMDCollection();
                        TKMDXuat.LoadPhanBo();
                        // if (ShowMessage("Nếu chỉnh sửa tờ khai nhập này bạn sẽ phải phân bổ lại từ tờ khai xuất : " + TKMDXuat.SoToKhai+ ". Bạn có muốn tiếp tục không?", true) != "Yes")
                        if (MLMessages("Nếu chỉnh sửa tờ khai nhập này bạn sẽ phải phân bổ lại từ tờ khai xuất : " + TKMDXuat.SoToKhai + ". Bạn có muốn tiếp tục không?", "MSG_TK01", "", true) != "Yes")
                            return;
                    }


                    if (TKMD.MaLoaiHinh.StartsWith("X") && TKMD.TrangThai == 0)
                        TKMD.DeletePhanBo();
                    else if (TKMD.MaLoaiHinh.StartsWith("N"))
                    {
                        TKMDXuat.DeletePhanBo();
                    }
                    //  ShowMessage("Xóa phân bổ tờ khai thành công.", false);
                    MLMessages("Thực hiện thành công.", "MSG_TK02", "", false);
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                }
            }
            else if (e.Command.Key == "cmdInCV")
            {
                XemCVXNK obj = new XemCVXNK(this.TKMD);
                obj.Show();
            }
            else if (e.Command.Key == "cmdCVXNK")
            {
                XemCVXNK obj = new XemCVXNK(this.TKMD);
                obj.Show();
            }
            else if (e.Command.Key == "cmdGiayDeNghi")
            {
                XemGDN obj = new XemGDN(this.TKMD);
                obj.Show();
            }
            else if (e.Command.Key == "cmdCamKet")
            {
                XemCKNNPL obj = new XemCKNNPL(this.TKMD);
                obj.Show();
            }
            else if (e.Command.Key == "cmdKiemTraHo")
            {
                XemKTH obj = new XemKTH(this.TKMD);
                obj.Show();
            }
            else if (e.Command.Key == "cmdChuyenCK")
            {
                XemCCK obj = new XemCCK(this.TKMD);
                obj.Show();
            }
            else if (e.Command.Key == "cmdPrint")
            {
                switch (this.TKMD.MaLoaiHinh.Substring(0, 3))
                {
                    case "NSX":
                        ReportViewTKNSXXKForm f = new ReportViewTKNSXXKForm();
                        f.TKMD = this.TKMD;
                        f.Show();
                        break;

                    case "XSX":
                        ReportViewTKXSXXKForm f1 = new ReportViewTKXSXXKForm();
                        f1.TKMD = this.TKMD;
                        f1.Show();
                        break;
                }
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
            {

                if (TKMD.TrangThai == 0 && TKMD.MaLoaiHinh.StartsWith("X"))
                {
                    // ShowMessage("Tờ khai xuất này đã được phân bổ. Không thể xóa hàng được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", false);
                    MLMessages("Tờ khai xuất này đã được phân bổ. Không thể xóa hàng được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", "MSG_TK01", "", false);
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    PhanBoToKhaiXuat pbTKX = PhanBoToKhaiNhap.SelectToKhaiXuatDuocPhanBoDauTienChoToKhaiNhap(GlobalSettings.MA_HAI_QUAN, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NamDangKy);
                    if (pbTKX != null)
                    {
                        // ShowMessage("Tờ khai nhập này đã được phân bổ. Không thể xóa hàng được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", false);
                        MLMessages("Tờ khai nhập này đã được phân bổ. Không thể xóa hàng được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", "MSG_TK01", "", false);
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                        try
                        {
                            hmd.DeleteHangTransaction();
                        }
                        catch (Exception ex)
                        {
                            e.Cancel = true;
                            ShowMessage("" + ex.Message, false);
                        }

                    }
                }
            }
            else
                e.Cancel = true;
        }
    }
}