using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamReadExcelForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public SanPhamReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private int checkSPExit(string maSP)
        {
            for (int i = 0; i < this.HD.SPCollection.Count; i++)
            {
                if (this.HD.SPCollection[i].Ma == maSP) return i;
            }
            return -1;
        }
        private bool checkExitNhomSP(string nhomsp)
        {
            foreach (NhomSanPham nhom in HD.NhomSPCollection)
            {
                if (nhom.MaSanPham.Trim().ToUpper() == nhomsp.Trim().ToUpper())
                {
                    return true;
                }
            }
            return false;
        }
        private bool EmptyCheck(string st)
        {
            if (st == "") return true;
            return false;
        }
        private bool CheckNumber(decimal so)
        {
            if (so < 0) return true;
            return false;
        }
        private bool CheckNameDVT(string varcheck)
        {
            DataTable dt = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if(!cvError.IsValid)return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            //if (beginRow < 0)
            //{
            //    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
            //    error.SetIconPadding(txtRow, -8);
            //    return;

            //}
            //else
            //{
            //    error.SetError(txtRow, null);
            //}
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                showMsg("MSG_0203008");
                //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                return;
            }
            try
            {
               ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn ;
            int maHangCol = 0;
            char tenHangColumn ;
            int tenHangCol = 0;
            char maHSColumn ;
            int maHSCol = 0;
            char dvtColumn;
            int dvtCol = 0;
            char SoLuongColumn ;
            int SoLuongCol = 0;
            char LoaiSPColumn ;
            int LoaiSPCol = 0;

            char DonGiaColumn ;
            int DonGiaCol = 0;

            try{
                 maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
                 maHangCol = ConvertCharToInt(maHangColumn);
                 tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
                 tenHangCol = ConvertCharToInt(tenHangColumn);
                 maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
                 maHSCol = ConvertCharToInt(maHSColumn);
                 dvtColumn = Convert.ToChar(txtDVTColumn.Text);
                 dvtCol = ConvertCharToInt(dvtColumn);
                 SoLuongColumn = Convert.ToChar(txtSoLuong.Text);
                 SoLuongCol = ConvertCharToInt(SoLuongColumn);
                 LoaiSPColumn = Convert.ToChar(txtLoaiSPGC.Text);
                 LoaiSPCol = ConvertCharToInt(LoaiSPColumn);

                 DonGiaColumn = Convert.ToChar(txtDonGia.Text);
                 DonGiaCol = ConvertCharToInt(DonGiaColumn);
            }
            catch { return; }
            int j=0;
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    j=wsr.Index + 1;
                    try
                    {
                        SanPham sp = new SanPham();
                        //if (CheckNumber(Convert.ToDecimal(wsr.Cells[SoLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHangCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[SoLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHSCol].Value)) || CheckNameDVT(Convert.ToString(wsr.Cells[dvtCol].Value)))
                        //{
                        //    ShowMessage(" Dữ liệu tài dòng " + (wsr.Index + 1) + " sai, được bỏ qua", false);
                        //    continue;
                        //}
                        sp.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        sp.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                        sp.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                        if (sp.MaHS == "")
                            sp.MaHS = "X";
                        try
                        {
                            sp.DVT_ID = DonViTinh_GetID(Convert.ToString(wsr.Cells[dvtCol].Value));
                        }
                        catch
                        {
                            showMsg("MSG_0203010", Convert.ToString(wsr.Cells[dvtCol].Value));
                            //MLMessages("Đơn vị tính : " + Convert.ToString(wsr.Cells[dvtCol].Value) + " không có trong hệ thống.","MSG_WRN06",Convert.ToString(wsr.Cells[dvtCol].Value), false);
                            return;
                        }
                        sp.SoLuongDangKy = Convert.ToDecimal(wsr.Cells[SoLuongCol].Value);
                        sp.DonGia = Convert.ToDecimal(wsr.Cells[DonGiaCol].Value);
                        sp.NhomSanPham_ID = Convert.ToString(wsr.Cells[LoaiSPCol].Value).Trim();
                        try
                        {
                            string tenLoaiSP = NhomSanPham_GetName(sp.NhomSanPham_ID);
                            if (!checkExitNhomSP(sp.NhomSanPham_ID))
                            {
                                showMsg("MSG_0203023", tenLoaiSP);
                                //MLMessages("Loại sản phẩm gia công : " + tenLoaiSP + " chưa được khai báo trong hợp đồng.", "MSG_STN05", tenLoaiSP, false);
                                return;
                            }
                        }
                        catch
                        {
                            if(showMsg("MSG_0203012", true) == "Yes")
                            //if (MLMessages("Loại sản phẩm gia công này không có trong hệ thống. Bạn có muốn tiếp tục không ?","MSG_EXC06","", true) == "Yes")
                            {
                                return;
                            }
                            else
                                continue;

                        }
                        if (checkSPExit(sp.Ma)>=0)
                        {
                            int dong = wsr.Index + 1;
                            if (showMsg("MSG_0203013", new String[] { sp.Ma, dong.ToString() } , true) == "Yes")
                            //if (MLMessages("Sản phẩm có mã \"" + sp.Ma + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế sản phẩm trên lưới bằng sản phẩm này?", "MSG_EXC07", j.ToString(), true) == "Yes")
                                this.HD.SPCollection[checkSPExit(sp.Ma)] = sp;

                        }
                        else this.HD.SPCollection.Add(sp);
                    }
                    catch
                    {
                        if (showMsg("MSG_0203014", wsr.Index + 1, true) != "Yes")
                        //if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        {
                            break;
                        }


                    }
                }
            }
            this.Close();
        }

        private void SanPhamReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void lblLinkExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GC("SP_HD");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}