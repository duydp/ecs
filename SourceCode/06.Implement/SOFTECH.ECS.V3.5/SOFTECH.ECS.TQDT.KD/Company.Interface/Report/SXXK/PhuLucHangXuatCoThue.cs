using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Collections.Generic;

namespace Company.Interface.Report.SXXK
{
    public partial class PhuLucHangXuatCoThue : DevExpress.XtraReports.UI.XtraReport
    {
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public Company.Interface.Report.ReportViewTKXTQDTForm report;
        public int SoToKhai;
        public DateTime NgayDangKy;
        public ToKhaiMauDich TKMD;
        public PhuLucHangXuatCoThue()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            xrLabel3.Text = this.TKMD.MaLoaiHinh.Substring(1, 2);
            this.PrintingSystem.ShowMarginsWarning = false;
            double tongTriGiaNT = 0;
            double tongThueXNK = 0;
            double tongThueGTGT = 0;
            double tongTriGiaThuKhac = 0;
            //lblMaHaiQuan.Text = GlobalSettings.MA_HAI_QUAN;
            if(NgayDangKy > new DateTime(1900,1,1))
                lblNgayDangKy.Text = NgayDangKy.ToString("dd/MM/yyyy");
            if(SoToKhai!=0)
                lblSoToKhai.Text = SoToKhai + "";
            int limit = this.HMDCollection.Count;
            if (this.HMDCollection.Count > 9) limit = 9;
            for (int i = 0; i < limit; i++)
            {
                XRControl control = new XRControl();
                HangMauDich hmd = this.HMDCollection[i];
                control = this.xrTable2.Rows[i].Controls["TriGiaTT" + (i + 1)];
                control.Text = hmd.TriGiaTT.ToString("N0");

                control = this.xrTable2.Rows[i].Controls["ThueSuatXNK" + (i + 1)];
                control.Text = hmd.ThueSuatXNK.ToString("N0");

                control = this.xrTable2.Rows[i].Controls["TienThueXNK" + (i + 1)];
                control.Text = hmd.ThueXNK.ToString("N0");

                control = this.xrTable2.Rows[i].Controls["TyLeThuKhac" + (i + 1)];
                control.Text = hmd.TyLeThuKhac.ToString("N0");

                control = this.xrTable2.Rows[i].Controls["TriGiaThuKhac" + (i + 1)];
                control.Text = hmd.TriGiaThuKhac.ToString("N0");
                tongThueXNK += hmd.ThueXNK;
                tongTriGiaThuKhac += hmd.TriGiaThuKhac;
            }
            lblTongTienThueXNK.Text = tongTriGiaNT.ToString("N2");
            lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
            lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
            lblTongTienThueBangSo.Text = this.TinhTongThueHMD().ToString("N0");
            string s = Company.KD.BLL.Utils.VNCurrency.ToString(this.TinhTongThueHMD()).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);
            lblTongTienThueBangChu.Text = s.Replace("  ", " ");
        }
        public void setVisibleImage(bool t)
        {
            xrPictureBox1.Visible = t;
        }
        public double TinhTongThueHMD()
        {
            double tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                tong = tong + hmd.ThueXNK + hmd.TriGiaThuKhac;
            return tong;
        }
    }
}
