﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuManageForm : BaseForm
    {
        /// <summary>
        /// Dùng cho kết quả tìm kiếm.
        /// </summary>
        private NguyenPhuLieuDangKyCollection nplDangKyCollection = new NguyenPhuLieuDangKyCollection();
        private NguyenPhuLieuDangKyCollection tmpCollection = new NguyenPhuLieuDangKyCollection();
        /// <summary>
        /// Thông tin nguyên phụ liệu đang dược chọn.
        /// </summary>
        private readonly NguyenPhuLieuDangKy currentNPLDangKy = new NguyenPhuLieuDangKy();
        NguyenPhuLieuDangKy npldk = new NguyenPhuLieuDangKy();
        private string xmlCurrent = "";

        public NguyenPhuLieuManageForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        private void NguyenPhuLieuManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                saveFileDialog1.InitialDirectory = Application.StartupPath;
                cbStatus.SelectedIndex = 0;

                //An nut Xac nhan
                LaySoTiepNhan.Visible = InheritableBoolean.False;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            setCommandStatus();
            saveFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        //-----------------------------------------------------------------------------------------

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
            where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
            }

            if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                }
            }

            where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

            // Thực hiện tìm kiếm.            
            this.nplDangKyCollection = NguyenPhuLieuDangKy.SelectCollectionDynamic(where, "");
            dgList.DataSource = this.nplDangKyCollection;

            this.setCommandStatus();

            this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }

        //-----------------------------------------------------------------------------------------

        private void setCommandStatus()
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            DongBo.Enabled = InheritableBoolean.True;
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = false;
                btnSuaNPL.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                LaySoTiepNhan.Enabled = InheritableBoolean.False;
                cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = false;
                btnSuaNPL.Enabled = true;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = false;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = false;
                cmdSend.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.True;
                btnDelete.Enabled = true;
                btnSuaNPL.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdXoa.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = false;
                btnSuaNPL.Enabled = true;
            }
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.KhaiDienTu)))
                {
                    cmdSend.Visible = InheritableBoolean.False;
                    cmdSingleDownload.Visible = InheritableBoolean.False;
                    cmdCancel.Visible = InheritableBoolean.False;
                    cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    LaySoTiepNhan.Visible = InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    DongBo.Visible = InheritableBoolean.False;
                    btnDelete.Visible = false;
                }
            }
        }


        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not declared yet";

                        }
                        break;
                    case 0:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for approval";

                        }
                        break;
                    case 1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Approved";

                        }
                        break;
                    case 2:
                        // e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not Approved";

                        }
                        break;





                }

                //TODO: Cao Hữu Tú updated:15-09-2011
                //Contents: bổ sung cột mã Nguyên phụ liệu vào Grid Danh sách nguyên phụ liệu


                Company.BLL.KDT.SXXK.NguyenPhuLieuCollection NguyenphulieuCollection = new NguyenPhuLieuCollection();
                int ValueIdCell = Int32.Parse(e.Row.Cells["ID"].Value.ToString());
                string TatCaMaNPL = "";

                NguyenphulieuCollection = Company.BLL.KDT.SXXK.NguyenPhuLieu.SelectCollectionBy_Master_ID(ValueIdCell);
                Company.BLL.KDT.SXXK.NguyenPhuLieu entityNguyenPhuLieu = new NguyenPhuLieu();

                if (NguyenphulieuCollection.Count > 0)
                {


                    //lấy mã đầu tiên trong NguyenPhuLieuCollection đưa vào Cột mã nguyên phụ liệu
                    entityNguyenPhuLieu = NguyenphulieuCollection[0];
                    e.Row.Cells["MaNguyenPhuLieu"].Text = entityNguyenPhuLieu.Ma;

                    //lấy mã Nguyên phụ liệu bắt đầu từ phần tử thứ 2 đưa vào 
                    foreach (Company.BLL.KDT.SXXK.NguyenPhuLieu EntityNPL in NguyenphulieuCollection)
                    {

                        TatCaMaNPL = TatCaMaNPL + EntityNPL.Ma + "\n";

                    }
                    e.Row.Cells["MaNguyenPhuLieu"].ToolTipText = TatCaMaNPL;


                }

            }
        }

        //-----------------------------------------------------------------------------------------

        private void download()
        {
            NguyenPhuLieuDangKy nplDK = new NguyenPhuLieuDangKy();
            string password = "";
            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            if (dgList.GetRow() != null)
            {
                nplDK = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = nplDK.ID;
                if (!sendXML.Load())
                {
                    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    //MLMessages("Nguyên phụ liệu chưa được gửi thông tin đến hải quan.\nHãy chọn chức năng 'Khai báo' cho nguyên phụ liệu này.","MSG_SEN03", "", false);
                    //return;
                }
            }
            else
            {
                //ShowMessage("Chưa chọn thông tin nhận trạng thái.", false);
                MLMessages("Chưa chọn thông tin nhận trạng thái.", "MSG_REC01", "", false);
                return;
            }
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                //xmlCurrent = nplDK.WSRequestXML(password);
                //DATLMQ comment theo chuan moi cua TNTT 25/11/2010
                //xmlCurrent = nplDK.TQDTWSLayPhanHoi(password);
                this.Cursor = Cursors.Default;
                sendXML = new MsgSend();
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = nplDK.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Nhận dữ liệu không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = nplDK.ID;
                                //hd.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                                //hd.TrangThai = nplDK.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                                //hd.PassWord = password;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong nhận thông tin : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                                sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSingleDownload":

                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    {
                        LayPhanHoiV3();
                    }
                    else
                    {
                        this.download();
                    }
                    break;
                case "cmdCancel":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    {
                        cancelV3();
                    }
                    else
                    {
                        this.cancel();
                    }
                    break;
                case "cmdSend":
                    if (this.ShowMessage("Bạn có muốn khai báo thông tin tờ khai này không?", true) == "Yes")
                    {
                        if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        {
                            SendV3();
                        }
                        else
                        {
                            this.send();
                        }
                    }
                    break;
                case "LaySoTiepNhan":
                    this.LaySoTiepNhanDT();
                    break;
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "cmdXoa":
                    this.Delete();
                    break;
                case "cmdCSDaDuyet":
                    this.ChuyenTrangThai();
                    break;
                case "cmdXuatNPLChoPhongKhai":
                    XuatNPLChoPhongKhai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdMessage":
                    break;
            }
        }

        private void SuaNPLDaDuyet()
        {
            NguyenPhuLieuDangKy npldk = new NguyenPhuLieuDangKy();
            npldk = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
            npldk.LoadNPLCollection();
            if (NguyenPhuLieuDangKySUA.SelectCollectionBy_IDNPLDK(npldk.ID).Count == 0)
            {
                string msg = "Bạn có muốn chuyển sang trạng thái Sửa nguyên phụ liệu không?";
                msg += "\n\nSố tiếp nhận: " + npldk.SoTiepNhan.ToString();
                msg += "\n----------------------";
                msg += "\nCó " + npldk.NPLCollection.Count.ToString() + " nguyên phụ liệu đăng ký đã được duyệt";
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    NguyenPhuLieuSuaForm f = new NguyenPhuLieuSuaForm();
                    f.nplDangKy = npldk;
                    f.masterId = npldk.ID;
                    f.npldkSUA = new NguyenPhuLieuDangKySUA();
                    f.ShowDialog();
                }
            }
            else
            {
                NguyenPhuLieuSuaListForm listForm = new NguyenPhuLieuSuaListForm();
                listForm.npldk = npldk;
                listForm.ShowDialog();
            }
        }

        private void inPhieuTN()
        {
            if (dgList.GetRows().Length < 1) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "NGUYÊN PHỤ LIỆU";
            Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = nplDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = nplDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();

        }
        private void XuatNPLChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách nguyên phụ liệu cần xuất.", false);
                MLMessages("Chưa chọn danh sách nguyên phụ liệu cần xuất.", "MSG_REC02", "", false);
                return;
            }
            try
            {
                NguyenPhuLieuDangKyCollection col = new NguyenPhuLieuDangKyCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(NguyenPhuLieuDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sonpl = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            NguyenPhuLieuDangKy nplSelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                            nplSelected.LoadNPLCollection();
                            col.Add(nplSelected);
                            sonpl++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    // ShowMessage("Xuất thành công " + sonpl + " nguyên phụ liệu.", false);
                    MLMessages("Xuất thành công " + sonpl + " nguyên phụ liệu.", "MSG_NPL02", "" + sonpl, false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }

        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                NguyenPhuLieuDangKyCollection nplDKcoll = new NguyenPhuLieuDangKyCollection();
                //GridEXSelectedItemCollection a = dgList.SelectedItems;
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    nplDKcoll.Add((NguyenPhuLieuDangKy)grItem.GetRow().DataRow);
                }

                for (int i = 0; i < nplDKcoll.Count; i++)
                {
                    string msg = "";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        msg = "Bạn có muốn chuyển trạng thái của danh sách được chọn sang đã duyệt không?";
                        msg += "\n\nSố thứ tự danh sách: " + nplDKcoll[i].ID.ToString();
                        //msg += "\n----------------------";
                        nplDKcoll[i].LoadNPLCollection();
                        msg += "\nCó " + nplDKcoll[i].NPLCollection.Count.ToString() + " nguyên phụ liệu đăng ký";
                        msg += "\nCác mã nguyên phụ liệu: ";
                    }
                    else
                    {
                        msg = "Do you want to change status of selected list into Approved?";
                        msg += "\n\n Order number of this list: " + nplDKcoll[i].ID.ToString();
                        //msg += "\n----------------------";
                        nplDKcoll[i].LoadNPLCollection();
                        msg += "\nThere are " + nplDKcoll[i].NPLCollection.Count.ToString() + "registered material(s)";
                        msg += "\nMaterial(s) code: ";

                    }

                    for (int j = 0; j < nplDKcoll[i].NPLCollection.Count; j++)
                    {
                        msg += nplDKcoll[i].NPLCollection[j].Ma.ToString();
                        if (j == nplDKcoll[i].NPLCollection.Count - 1)
                        {
                            msg += ".";
                        }
                        else
                        {
                            msg += ", ";

                        }
                    }
                    if (ShowMessage(msg, true) == "Yes")
                    {
                        if (nplDKcoll[i].TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            nplDKcoll[i].NgayTiepNhan = DateTime.Now;
                        }
                        string fmtMsg = string.Format("Thông tin trước khi chuyển-->GUIDSTR:{0},Mã hải quan: {1},Mã doanh nghiệp: {2}," +
                                                      "ID: {3},Năm đăng ký:{4},Ngày tiếp nhận:{5},Số tiếp nhận:{6}",
                                                   new object[]{
                                                       nplDKcoll[i].GUIDSTR,
                                                       nplDKcoll[i].MaHaiQuan,                                                       
                                                       nplDKcoll[i].MaDoanhNghiep,
                                                       nplDKcoll[i].ID,
                                                       nplDKcoll[i].NamDK,
                                                       nplDKcoll[i].NgayTiepNhan,                                                       
                                                       nplDKcoll[i].SoTiepNhan});
                        Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty, nplDKcoll[i].ID, nplDKcoll[i].GUIDSTR,
                        Company.KDT.SHARE.Components.MessageTypes.DanhMucNguyenPhuLieu,
                        Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                        Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay, fmtMsg);

                        nplDKcoll[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        nplDKcoll[i].TransgferDataToSXXK();
                    }
                }
                this.search();
            }
            else
            {
                //ShowMessage("Chưa có dữ liệu được chọn", false);
                MLMessages("Chưa có dữ liệu được chọn", "MSG_REC02", "", false);
            }
        }
        public void XoaNguyenPhuLieuDangKy(GridEXSelectedItemCollection items)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            MsgSend sendXML = new MsgSend();

            List<NguyenPhuLieuDangKy> itemsDelete = new List<NguyenPhuLieuDangKy>();

            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    NguyenPhuLieuDangKy npldk = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                    sendXML.LoaiHS = "NPL";
                    sendXML.master_id = npldk.ID;
                    if (!sendXML.Load())
                    {
                        msgWarning += string.Format(" ID ={0} [Chấp nhận xóa]\r\n", npldk.ID);
                        itemsDelete.Add(npldk);
                    }
                    else
                    {
                        msgWarning += string.Format(" ID ={0} [Không thể xóa]\r\n", npldk.ID);
                    }
                }
            }
            msgWarning += "* Ghi chú: Những nguyên phụ liệu [Không thể xóa] vì đã gửi thông tin đến hải quan\r\n";
            if (itemsDelete.Count == 0)
            {
                Globals.ShowMessage(msgWarning, false);
                return;
            }
            else
            {
                msgWarning += "Bạn đồng ý thực hiện không?";
                if (Globals.ShowMessage(msgWarning, true) != "Yes") return;
            }
            try
            {
                msgWarning = string.Empty;
                foreach (NguyenPhuLieuDangKy item in itemsDelete)
                {
                    if (item.CloneToDB(null))
                        item.Delete();
                    else msgWarning += string.Format("ID={0} [thực hiện không thành công]", item.ID);
                }
                if (msgWarning != string.Empty)

                    Globals.ShowMessage(msgWarning, false);
            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Delete()
        {

            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaNguyenPhuLieuDangKy(items);
            search();
            #region Code cũ
            //try
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    if (items.Count <= 0) return;
            //    // if (ShowMessage("Bạn có muốn xóa danh sách nguyên phụ liệu này không?", true) == "Yes")
            //    if (MLMessages("Bạn có muốn xóa danh sách nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        foreach (GridEXSelectedItem i in items)
            //        {
            //            if (i.RowType == RowType.Record)
            //            {
            //                NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
            //                MsgSend sendXML = new MsgSend();
            //                sendXML.LoaiHS = "NPL";
            //                sendXML.master_id = nplDangKySelected.ID;
            //                if (sendXML.Load())
            //                {
            //                    if (nplDangKySelected.SoTiepNhan != 0)
            //                        //  ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
            //                        MLMessages("Danh sách thứ " + i.Position + 1 + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_NPL03", "" + i.Position + 1 + "", false);
            //                }
            //                else
            //                {
            //                    if (nplDangKySelected.ID > 0)
            //                    {
            //                        nplDangKySelected.CloneToDB(null);
            //                        nplDangKySelected.Delete();
            //                    }
            //                }

            //                if (nplDangKySelected.SoTiepNhan == 0 && nplDangKySelected.ID > 0)
            //                    nplDangKySelected.Delete();

            //                //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
            //                #region Log
            //                try
            //                {
            //                    string where = "1 = 1";
            //                    where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", nplDangKySelected.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.NguyenPhuLieu);
            //                    List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
            //                    if (listLog.Count > 0)
            //                    {
            //                        long idLog = listLog[0].IDLog;
            //                        string loaiKhaiBao = listLog[0].LoaiKhaiBao;
            //                        long idDK = listLog[0].ID_DK;
            //                        string guidstr = listLog[0].GUIDSTR_DK;
            //                        string userKhaiBao = listLog[0].UserNameKhaiBao;
            //                        DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
            //                        string userSuaDoi = GlobalSettings.UserLog;
            //                        DateTime ngaySuaDoi = DateTime.Now;
            //                        string ghiChu = listLog[0].GhiChu;
            //                        bool isDelete = true;
            //                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
            //                                                                                    userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
            //                    }
            //                }
            //                catch (Exception ex)
            //                {
            //                    ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
            //                    return;
            //                }
            //                #endregion
            //            }
            //        }
            //        search();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage(ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            #endregion Code cũ

        }
        private string checkDataHangImport(NguyenPhuLieuDangKy nplDangky)
        {
            string st = "";
            foreach (NguyenPhuLieu npl in nplDangky.NPLCollection)
            {
                BLL.SXXK.NguyenPhuLieu nplDaDuyet = new Company.BLL.SXXK.NguyenPhuLieu();
                nplDaDuyet.Ma = npl.Ma;
                nplDaDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                nplDaDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (nplDaDuyet.Load())
                {
                    if (npl.STTHang == 1)
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st = "Danh sách có ID='" + nplDangky.ID + "'\n";

                        }
                        else
                        {
                            st = "List has ID='" + nplDangky.ID + "'\n";

                        }
                    }
                    if (GlobalSettings.NGON_NGU == "0")
                    {

                        st += "Nguyên phụ liệu có mã '" + npl.Ma + "' đã có trong hệ thống.\n";
                    }
                    else
                    {

                        st += "Material has code '" + npl.Ma + "' already exist in system.\n";
                    }
                }
            }
            return st;
        }
        private string checkDataImport(NguyenPhuLieuDangKyCollection collection)
        {
            string st = "";
            foreach (NguyenPhuLieuDangKy nplDangky in collection)
            {
                NguyenPhuLieuDangKy nplInDatabase = NguyenPhuLieuDangKy.Load(nplDangky.ID);
                if (nplInDatabase != null)
                {
                    if (nplInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        //  st += "Danh sách có ID=" + nplDangky.ID + " đã được duyệt.\n";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st += "Danh sách có ID=" + nplDangky.ID + " đã được duyệt.\n";
                        }
                        else
                        {
                            st += "List has ID=" + nplDangky.ID + " already approved.\n";

                        }
                    }
                    else
                    {
                        string tmp = checkDataHangImport(nplDangky);
                        st += tmp;
                        if (tmp == "")
                            tmpCollection.Add(nplDangky);
                    }
                }
                else
                {
                    if (nplInDatabase.ID > 0)
                        nplInDatabase.ID = 0;
                    tmpCollection.Add(nplDangky);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    tmpCollection.Clear();
                    XmlSerializer serializer = new XmlSerializer(typeof(NguyenPhuLieuDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    NguyenPhuLieuDangKyCollection nplDKCollection = (NguyenPhuLieuDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(nplDKCollection);
                    if (st != "")
                    {
                        // if (ShowMessage("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", true) == "Yes")
                        if (MLMessages("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "MSG_PUB04", "", true) == "Yes")
                        {
                            NguyenPhuLieuDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                            //  ShowMessage("Import thành công", false);
                            MLMessages("Import thành công", "MSG_PUB02", "", false);
                        }
                    }
                    else
                    {
                        NguyenPhuLieuDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                        // ShowMessage("Import thành công", false);
                        MLMessages("Import thành công", "MSG_PUB02", "", false);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("" + ex.Message, false);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách nguyên phụ liệu",false);
                //Message("MSG_REC02", "", false);
                MLMessages("Chưa chọn danh sách nguyên phụ liệu", "MSG_REC02", "", false);
                return;
            }
            try
            {
                NguyenPhuLieuDangKyCollection col = new NguyenPhuLieuDangKyCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(NguyenPhuLieuDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                            nplDangKySelected.LoadNPLCollection();
                            col.Add(nplDangKySelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }

        }
        private void LaySoTiepNhanDT()
        {
            NguyenPhuLieuDangKy nplDangKy = new NguyenPhuLieuDangKy();
            MsgSend sendXML = new MsgSend();
            string password = "";
            if (dgList.GetRow() != null)
            {
                nplDangKy = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = nplDangKy.ID;
                if (!sendXML.Load())
                {
                    // ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    //Message("MSG_SEN03", "", false);
                    MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_STN01", "", false);
                    return;
                }
            }
            else
            {
                MLMessages("Chưa chọn thông tin để xác nhận thông tin.", "MSG_REC02", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                xmlCurrent = nplDangKy.LayPhanHoi(password, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    //  string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);

                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }

                if (sendXML.func == 1)
                {
                    // ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + nplDangKy.SoTiepNhan, false);
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + nplDangKy.SoTiepNhan, "MSG_SEN05", "" + nplDangKy.SoTiepNhan, false);
                    this.search();
                }
                else if (sendXML.func == 3)
                {
                    // ShowMessage("Đã hủy danh sách nguyên phụ liệu này", false);
                    MLMessages("Đã hủy danh sách nguyên phụ liệu này", "MSG_SEN06", "", false);
                    this.search();
                }
                else if (sendXML.func == 2)
                {
                    if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {


                        // ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN07", "", false);

                        //try
                        //{
                        //    nplDangKy.TransgferDataToSXXKNguyenPhuLieuTon();
                        //}
                        //catch (Exception ex)
                        //{
                        //    ShowMessage("Có lỗi khi cập nhật vào danh sách nguyên phụ liệu tồn : "+ex.Message, false);
                        //}
                        this.search();
                    }
                    else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        //  ShowMessage("Hải quan chưa xử lý!", false); 
                        MLMessages("Hải quan chưa xử lý!", "MSG_SEN08", "", false);
                    }
                    else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_STN09", "", false);
                        this.search();
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = nplDangKy.ID;
                                //hd.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                                //hd.TrangThai = nplDangKy.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //hd.PassWord = password;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                                sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoi(string pass)
        {
            NguyenPhuLieuDangKy nplDangKy = new NguyenPhuLieuDangKy();
            MsgSend sendXML = new MsgSend();
            try
            {
                if (dgList.GetRow() != null)
                {
                    nplDangKy = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
                    sendXML.LoaiHS = "NPL";
                    sendXML.master_id = nplDangKy.ID;
                    sendXML.Load();
                    this.Cursor = Cursors.WaitCursor;

                    //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 25/11/2010
                    //Tao XML Header
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(Globals.ConfigPhongBiPhanHoi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, nplDangKy.GUIDSTR));

                    //Tao Body XML
                    XmlDocument docNPL = new XmlDocument();
                    string path = EntityBase.GetPathProram();
                    docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                    XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                    //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
                    root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                    root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                    root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                    root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                    //this.GUIDSTR = (System.Guid.NewGuid().ToString()); ;
                    //this.Update();
                    root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = nplDangKy.GUIDSTR.Trim();

                    XmlNode Content = xml.GetElementsByTagName("Content")[0];
                    Content.AppendChild(root);

                    // xmlCurrent = nplDangKy.LayPhanHoi(pass, sendXML.msg);
                    xmlCurrent = nplDangKy.TQDTLayPhanHoi(pass, xml.InnerXml);
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        //string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                        string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                        if (kq == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        return;
                    }

                    if (sendXML.func == 1)
                    {
                        // ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + nplDangKy.SoTiepNhan, false);
                        MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + nplDangKy.SoTiepNhan, "MSG_SEN05", "" + nplDangKy.SoTiepNhan, false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        //ShowMessage("Đã hủy danh sách nguyên phụ liệu này", false);
                        MLMessages("Đã hủy danh sách nguyên phụ liệu này", "MSG_SEN06", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            // ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                            MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN18", "", false);
                            //try
                            //{
                            //    nplDangKy.TransgferDataToSXXKNguyenPhuLieuTon();
                            //}
                            //catch (Exception ex)
                            //{
                            //    ShowMessage("Có lỗi khi cập nhật vào danh sách nguyên phụ liệu tồn : " + ex.Message, false);
                            //}
                            this.search();
                        }
                        else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            //ShowMessage("Hải quan chưa xử lý!", false);
                            MLMessages("Hải quan chưa xử lý!", "MSG_SEN08", "", false);
                        }
                        else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            // ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                            MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN9", "", false);
                            this.search();
                        }
                        else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO && !(nplDangKy.HUONGDAN.Equals("")))
                        {
                            MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN9", "", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = nplDangKy.ID;
                                //hd.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                                //hd.TrangThai = nplDangKy.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //hd.PassWord = pass;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);

                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                NguyenPhuLieuDangKyDetailForm f = new NguyenPhuLieuDangKyDetailForm();
                f.NPLDangKy = (NguyenPhuLieuDangKy)e.Row.DataRow;
                int ttxl = f.NPLDangKy.TrangThaiXuLy;
                if (f.NPLDangKy.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                {
                    f.OpenType = OpenFormType.Edit;
                }
                else
                {
                    f.OpenType = OpenFormType.View;
                }
                //neu la ws của hải quan thì khi chờ duỵet ko cho sửa
                if (f.NPLDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    f.OpenType = OpenFormType.View;
                }
                f.ShowDialog(this);
                if (ttxl != f.NPLDangKy.TrangThaiXuLy) this.search();
            }
        }

        /// <summary>
        /// Hủy thông tin đã đăng ký.
        /// </summary>
        private void cancel()
        {
            NguyenPhuLieuDangKy nplDangKy;
            MsgSend sendXML = new MsgSend();
            string password = "";
            if (dgList.GetRow() != null)
            {
                nplDangKy = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = nplDangKy.ID;
                if (sendXML.Load())
                {
                    //  ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                    //return;
                }
            }
            else
            {
                //ShowMessage("Chưa chọn thông tin để hủy.", false);
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = nplDangKy.WSCancelXML(password);
                this.Cursor = Cursors.Default;

                sendXML = new MsgSend();
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = nplDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Hủy thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = nplDangKy.ID;
                                //hd.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                                //hd.TrangThai = nplDangKy.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                                //hd.PassWord = password;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                                sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                // luu vao file loi
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi Hủy danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            NguyenPhuLieuDangKy nplDangKy = new NguyenPhuLieuDangKy();
            string password = "";
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null)
            {
                nplDangKy = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = nplDangKy.ID;
                if (sendXML.Load())
                {
                    //ShowMessage("Nguyên phụ liệu đã gửi thông tin đến hải quan.\nHãy chọn chức năng Lấy phản hồi cho nguyên phụ liệu này.", false);
                    MLMessages("Nguyên phụ liệu đã gửi thông tin đến hải quan nhưng chưa có thông tin phản hồi.\nHãy chọn chức năng 'Nhận dữ liệu' cho nguyên phụ liệu này.", "MSG_SEN03", "", false);
                    //LaySoTiepNhan.Enabled = InheritableBoolean.True;
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    return;
                    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                    //return;
                }
            }
            else
            {
                // ShowMessage("Chưa chọn thông tin để gửi.", false);
                MLMessages("Chưa chọn thông tin để gửi.", "MSG_PUB01", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                nplDangKy.LoadNPLCollection();
                // Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu npl = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_NPL(nplDangKy);
                // string stringdemo = Company.KDT.SHARE.Components.Helpers.Serializer(npl);

                if (nplDangKy.NPLCollection.Count == 0)
                {
                    // ShowMessage("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }

                string[] danhsachDaDangKy = new string[0];

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                xmlCurrent = nplDangKy.WSSendXML(password);
                this.Cursor = Cursors.Default;

                sendXML = new MsgSend();
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = nplDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                #region FPTService
                string[] msg = ex.Message.Split('|');
                if (msg.Length == 2)
                {
                    if (msg[1] == "DOTNET_LEVEL")
                    {
                        // if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                        if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                        {
                            HangDoi hd = new HangDoi();
                            hd.ID = nplDangKy.ID;
                            hd.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                            hd.TrangThai = nplDangKy.TrangThaiXuLy;
                            hd.ChucNang = ChucNang.KHAI_BAO;
                            hd.PassWord = password;
                            MainForm.AddToQueueForm(hd);
                            MainForm.ShowQueueForm();
                        }
                    }
                    else
                    {
                        // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                        if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            GlobalSettings.PassWordDT = "";
                        }
                        else
                            sendXML.Delete();
                    }
                }
                else
                {
                    //ShowMessage("Xảy ra lỗi không xác định.", false);
                    MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                    GlobalSettings.PassWordDT = "";
                }
                #endregion FPTService

                //Lưu vào file         
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                txtNamTiepNhan.Text = string.Empty;
                txtNamTiepNhan.Value = 0;
                txtNamTiepNhan.Enabled = false;
                txtSoTiepNhan.Value = 0;
                txtSoTiepNhan.Text = string.Empty;
                txtSoTiepNhan.Enabled = false;
            }
            else
            {

                txtNamTiepNhan.Value = DateTime.Today.Year;
                txtNamTiepNhan.Enabled = true;
                txtSoTiepNhan.Enabled = true;
            }
            this.search();
        }

        private void NguyenPhuLieuManageForm_Shown(object sender, EventArgs e)
        {
            this.search();
        }


        private void dgList_DeletingRecord(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
            #region code cũ
            //try
            //{
            //    // if (ShowMessage("Bạn có muốn xóa danh sách nguyên phụ liệu này không?", true) == "Yes")
            //    if (MLMessages("Bạn có muốn xóa danh sách nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        GridEXSelectedItemCollection items = dgList.SelectedItems;
            //        foreach (GridEXSelectedItem i in items)
            //        {
            //            if (i.RowType == RowType.Record)
            //            {
            //                NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
            //                MsgSend sendXML = new MsgSend();
            //                sendXML.LoaiHS = "NPL";
            //                sendXML.master_id = nplDangKySelected.ID;
            //                if (sendXML.Load())
            //                {
            //                    // ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);                                
            //                    MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_NPL03", "" + i.Position + "", false);
            //                }
            //                else
            //                {
            //                    if (nplDangKySelected.ID > 0)
            //                    {
            //                        nplDangKySelected.Delete();
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage(ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            #endregion code cũ

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void btnSuaNPL_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null)
            {
                this.SuaNPLDaDuyet();
            }
            else
            {
                ShowMessage("Chưa chọn thông tin để sửa.", false);
                return;
            }
        }
        private void uiMessage_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;

            NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)dgList.CurrentRow.DataRow;
            Globals.KetQuaXuLyChung(nplDangKySelected.ID);
        }
        #region Send V3 Create by LANNT
        private void SendV3()
        {
            npldk = new NguyenPhuLieuDangKy();

            if (dgList.GetRow() != null)
                npldk = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
            else
                return;
            try
            {
                if (npldk.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                npldk.LoadNPLCollection();
                if (npldk.NPLCollection.Count == 0)
                {
                    // ShowMessage("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }

                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = npldk.ID;
                if (sendXML.Load())
                {
                    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    //NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                else
                {
                     npldk.GUIDSTR = Guid.NewGuid().ToString();
                    Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu npl = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_NPL(npldk, false);
                    string msgSend = Company.KDT.SHARE.Components.Helpers.BuildSend(
                                   new Company.KDT.SHARE.Components.NameBase()
                                   {
                                       Name = GlobalSettings.TEN_DON_VI,
                                       Identity = npldk.MaDoanhNghiep
                                   }
                                     , new Company.KDT.SHARE.Components.NameBase()
                                     {
                                         Name = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(npldk.MaHaiQuan),
                                         Identity = npldk.MaHaiQuan
                                     }
                                  ,
                                    new Company.KDT.SHARE.Components.SubjectBase()
                                    {
                                        Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL,
                                        Function = Company.KDT.SHARE.Components.DeclarationFunction.KHAI_BAO,
                                        Reference = npldk.GUIDSTR,
                                    }
                                    ,
                                    npl, null, true);
                    npldk.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    sendForm.DoSend(msgSend);
                    Company.KDT.SHARE.Components.Globals.XmlSaveMessage(msgSend, npldk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoNguyenPhuLieu);
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = "NPL";
                    sendXML.master_id = npldk.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();

                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {

                if (e.Error == null)
                {

                    Company.KDT.SHARE.Components.FeedBackContent feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string content = feedbackContent.AdditionalInformation.Content.Text;
                    switch (feedbackContent.AdditionalInformation.Statement.Trim())
                    {
                        case Company.KDT.SHARE.Components.DeclarationFunction.KHONG_CHAP_NHAN:
                            this.ShowMessageTQDT(feedbackContent.AdditionalInformation.Content.Text, false);
                            npldk.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.CHUA_XU_LY:
                            this.ShowMessageTQDT(feedbackContent.AdditionalInformation.Content.Text, false);
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.CAP_SO_TIEP_NHAN:

                            string[] ketqua = feedbackContent.AdditionalInformation.Content.Text.Split('/');
                            string noidung = "\nSố Tiếp Nhận: " + ketqua[0] + "\nNăm Ðăng Ký: " + ketqua[1];
                            if ((npldk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO))
                            {
                                noidung = "Ðã Tiếp Nhận Hủy Khai Báo." + noidung;
                                npldk.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                            }
                            else if (npldk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                            {
                                noidung = "Ðã Tiếp Nhận Khai Báo" + noidung;
                                npldk.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                            }
                            npldk.SoTiepNhan = long.Parse(ketqua[0].Trim());
                            npldk.NamDK = short.Parse(ketqua[1].Trim());
                            npldk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, npldk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiNguyenPhuLieu, noidung);
                            this.ShowMessageTQDT(noidung, false);
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.THONG_QUAN:
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, npldk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, feedbackContent.AdditionalInformation.Content.Text);
                            if (npldk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                            {
                                this.ShowMessageTQDT("Ðã Duyệt Khai Báo", false);
                                npldk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            }
                            else if (npldk.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                            {
                                this.ShowMessageTQDT("Chấp Nhận Hủy Khai Báo", false);
                                npldk.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY;
                            }
                            break;
                        default:
                            Company.KDT.SHARE.Components.Globals.SaveMessage(e.FeedBackMessage, npldk.ID, "Error");
                            break;

                    }
                    npldk.Update();
                }
                else
                {
                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }
        private void LayPhanHoiV3()
        {
            if (dgList.GetRow() != null)
                npldk = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
            else
                return;
            string reference = npldk.GUIDSTR;

            Company.KDT.SHARE.Components.SubjectBase subjectBase = new Company.KDT.SHARE.Components.SubjectBase()
            {
                Issuer = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL,
                Reference = reference,
                Function = Company.KDT.SHARE.Components.DeclarationFunction.HOI_TRANG_THAI,
                Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL,

            };

            string msgSend = Company.KDT.SHARE.Components.Helpers.BuildFeedBack(
                                        new Company.KDT.SHARE.Components.NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = npldk.MaDoanhNghiep
                                        },
                                          new Company.KDT.SHARE.Components.NameBase()
                                          {
                                              Name = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(npldk.MaHaiQuan.Trim()),
                                              Identity = npldk.MaHaiQuan
                                          }, subjectBase, null);
            SendMessageForm sendForm = new SendMessageForm();
            sendForm.Send += SendMessage;
            sendForm.DoSend(msgSend);

        }
        /// <summary>
        /// Hủy Thông Tin đến Hải Quan
        /// </summary>
        private void cancelV3()
        {
            npldk = new NguyenPhuLieuDangKy();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null)
            {
                npldk = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = npldk.ID;
                if (sendXML.Load())
                {
                    MLMessages("Tờ khai sửa đã gửi đến Hải quan. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                    cmdSend.Enabled = InheritableBoolean.False;
                    cmdSingleDownload.Enabled = InheritableBoolean.True;
                    return;
                }
            }
            else
            {
                //ShowMessage("Chưa chọn thông tin để hủy.", false);
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            Company.KDT.SHARE.Components.DeclarationBase npl = Company.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL, npldk.GUIDSTR, npldk.SoTiepNhan, npldk.MaHaiQuan, npldk.NgayTiepNhan);
            string msgSend = Company.KDT.SHARE.Components.Helpers.BuildSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = npldk.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(npldk.MaHaiQuan),
                                 Identity = npldk.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DANHMUC_NPL,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = npldk.GUIDSTR,
                            }
                            ,
                            npl, null, true);
            SendMessageForm sendForm = new SendMessageForm();
            npldk.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO;
            sendForm.Send += SendMessage;
            sendForm.DoSend(msgSend);
            Company.KDT.SHARE.Components.Globals.XmlSaveMessage(msgSend, npldk.ID, Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoToKhai);
            sendXML.func = 3;
            sendXML.InsertUpdate();
        }
        #endregion


    }
}