/*
Run this script on:

        192.168.72.151\sqlserver.ECS_TQDT_GC_V4_20_08_2015_THÁI_LIÊN    -  This database will be modified

to synchronize it with:

        192.168.72.151\sqlserver.ECS_TQDT_GC_V4_19_09_2015_[09_12]_THIANCO

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 19/09/2015 10:51:33 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[t_VNACCS_CapSoToKhai]'
GO
ALTER TABLE [dbo].[t_VNACCS_CapSoToKhai] DROP CONSTRAINT [IX_t_VNACCS_CapSoToKhai]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '21.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('21.6',GETDATE(), N'Fix lỗi xử lý hợp đồng. Dropping constraints')
END	