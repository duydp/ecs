﻿namespace CA_QuanLyGCN
{
    partial class QuyetDinhPhatFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.GridEX.GridEXLayout cbbDoanhNghiep_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuyetDinhPhatFrm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenCoSoBiPhat = new Janus.Windows.EditControls.UIComboBox();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.cbbDoanhNghiep = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.clcNgayPhat = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoQuyetDinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiQuyetDinhPhat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDoanhNghiep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox1.Controls.Add(this.txtTenCoSoBiPhat);
            this.uiGroupBox1.Controls.Add(this.cbbDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.clcNgayPhat);
            this.uiGroupBox1.Controls.Add(this.btnLuu);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.txtSoQuyetDinh);
            this.uiGroupBox1.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.txtNguoiQuyetDinhPhat);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(703, 189);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtTenCoSoBiPhat
            // 
            this.txtTenCoSoBiPhat.Location = new System.Drawing.Point(115, 48);
            this.txtTenCoSoBiPhat.Name = "txtTenCoSoBiPhat";
            this.txtTenCoSoBiPhat.Size = new System.Drawing.Size(564, 20);
            this.txtTenCoSoBiPhat.TabIndex = 0;
            this.txtTenCoSoBiPhat.VisualStyleManager = this.visualStyleManager1;
            // 
            // visualStyleManager1
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office2007";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme1.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme1);
            // 
            // cbbDoanhNghiep
            // 
            cbbDoanhNghiep_DesignTimeLayout.LayoutString = resources.GetString("cbbDoanhNghiep_DesignTimeLayout.LayoutString");
            this.cbbDoanhNghiep.DesignTimeLayout = cbbDoanhNghiep_DesignTimeLayout;
            this.cbbDoanhNghiep.Location = new System.Drawing.Point(115, 22);
            this.cbbDoanhNghiep.Name = "cbbDoanhNghiep";
            this.cbbDoanhNghiep.ReadOnly = true;
            this.cbbDoanhNghiep.SelectedIndex = -1;
            this.cbbDoanhNghiep.SelectedItem = null;
            this.cbbDoanhNghiep.Size = new System.Drawing.Size(94, 20);
            this.cbbDoanhNghiep.TabIndex = 4;
            this.cbbDoanhNghiep.VisualStyleManager = this.visualStyleManager1;
            // 
            // clcNgayPhat
            // 
            // 
            // 
            // 
            this.clcNgayPhat.DropDownCalendar.Name = "";
            this.clcNgayPhat.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayPhat.Location = new System.Drawing.Point(580, 132);
            this.clcNgayPhat.Name = "clcNgayPhat";
            this.clcNgayPhat.Size = new System.Drawing.Size(99, 20);
            this.clcNgayPhat.TabIndex = 4;
            this.clcNgayPhat.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayPhat.VisualStyleManager = this.visualStyleManager1;
            // 
            // btnLuu
            // 
            this.btnLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLuu.Icon")));
            this.btnLuu.Location = new System.Drawing.Point(604, 160);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(75, 23);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.VisualStyleManager = this.visualStyleManager1;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Lý do phạt";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên cơ sở bị phạt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(518, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Ngày phạt";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(215, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Mã doanh nghiệp";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(327, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Số quyết định";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Người lập quyết định";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã doanh nghiệp";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(115, 74);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(564, 52);
            this.txtGhiChu.TabIndex = 1;
            this.txtGhiChu.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtSoQuyetDinh
            // 
            this.txtSoQuyetDinh.Location = new System.Drawing.Point(406, 132);
            this.txtSoQuyetDinh.Name = "txtSoQuyetDinh";
            this.txtSoQuyetDinh.Size = new System.Drawing.Size(106, 20);
            this.txtSoQuyetDinh.TabIndex = 3;
            this.txtSoQuyetDinh.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(311, 22);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(368, 20);
            this.txtTenDoanhNghiep.TabIndex = 0;
            this.txtTenDoanhNghiep.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtNguoiQuyetDinhPhat
            // 
            this.txtNguoiQuyetDinhPhat.Location = new System.Drawing.Point(115, 132);
            this.txtNguoiQuyetDinhPhat.Name = "txtNguoiQuyetDinhPhat";
            this.txtNguoiQuyetDinhPhat.Size = new System.Drawing.Size(206, 20);
            this.txtNguoiQuyetDinhPhat.TabIndex = 2;
            this.txtNguoiQuyetDinhPhat.VisualStyleManager = this.visualStyleManager1;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // QuyetDinhPhatFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 189);
            this.Controls.Add(this.uiGroupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QuyetDinhPhatFrm";
            this.Text = "Quyết định phạt";
            this.Load += new System.EventHandler(this.QuyetDinhPhatFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDoanhNghiep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private Janus.Windows.EditControls.UIButton btnLuu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayPhat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiQuyetDinhPhat;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbDoanhNghiep;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Janus.Windows.EditControls.UIComboBox txtTenCoSoBiPhat;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQuyetDinh;
    }
}