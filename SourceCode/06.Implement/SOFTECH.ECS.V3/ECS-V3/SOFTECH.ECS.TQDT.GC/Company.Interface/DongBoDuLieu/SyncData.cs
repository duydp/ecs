﻿
using System;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
#if KD_V3
using Company.Interface.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
using Company.KD.BLL.DuLieuChuan;
#elif GC_V3
using Company.Interface.GC;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
#endif
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;
using System.Threading;
using KetQuaXuLy = Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3.KetQuaXuLy;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface.DongBoDuLieu
{
    class SyncData
    {
        ISyncData wsSyncData = WebService.SyncService();
        public event EventHandler<SyncEventArgs> SyncEventArgs;
        private void OnSynce(SyncEventArgs e)
        {
            if (SyncEventArgs != null)
            {
                SyncEventArgs(this, e);
            }
        }
        #region save Log error
        private void SaveLogError(Detail chitiet, string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = chitiet.SoToKhai;
            logError.MaDoanhNghiep = chitiet.MaDoanhNghiep;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.GUIDSTR = chitiet.GUIDSTR;
            logError.TrangThai = 2;
            logError.InsertUpdate();
        }
        private void SaveLogError(ToKhaiMauDich chitiet, string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = chitiet.SoToKhai;
            logError.MaDoanhNghiep = chitiet.MaDoanhNghiep;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.GUIDSTR = chitiet.GUIDSTR;
            logError.TrangThai = 1;
            logError.InsertUpdate();
        }
        private void SaveLogError(ToKhaiChuyenTiep chitiet, string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = chitiet.SoToKhai;
            logError.MaDoanhNghiep = chitiet.MaDoanhNghiep;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.GUIDSTR = chitiet.GUIDSTR;
            logError.TrangThai = 1;
            logError.InsertUpdate();
        }
        private void SaveLogError(string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = 0;
            logError.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.TrangThai = 0;
            logError.InsertUpdate();
        }
        #endregion


        public void GC_SendData(List<ToKhaiMauDich> tokhaiSelect, string user, string password)
        {
            try
            {
                int percent = 0;
                int total = tokhaiSelect.Count;
                int pos = 0;
                int valSend = 0;
                string msgSend = string.Empty;
                foreach (ToKhaiMauDich tkmd in tokhaiSelect)
                {
                    pos++;
                    try
                    {
                        HopDong HD = HopDong.Load(tkmd.IDHopDong);
                        if (HD == null)
                        {
                            OnSynce(new SyncEventArgs(new Exception("Không tìm thấy hợp đồng của tờ khai ")));
                            return ;
                        }
                        if (!this.GC_SendDataHopDong(HD, user, password))
                        {
                            OnSynce(new SyncEventArgs(new Exception("Dừng đồng bộ, Hợp đồng không thể gửi đi")));
                            return;
                        }

                        this.GC_SendDataPhuKien(HD, user, password);

                        
                        this.GC_SendDataDinhMuc(HD, user, password);

                        tkmd.LoadHMDCollection();
                        tkmd.LoadChungTuHaiQuan();
                        if (string.IsNullOrEmpty(tkmd.GUIDSTR))
                        {
                            tkmd.GUIDSTR = Guid.NewGuid().ToString();
                            tkmd.Update();
                        }
                        SyncDaTa syncDaTa = new SyncDaTa()
                        {
                            Type = "TKMD",
                            Declaration = new List<SyncDaTaDetail>()
                        };
                        syncDaTa.Declaration.Add(new SyncDaTaDetail
                        {
                            Issuer = user,
                            Business = tkmd.MaDoanhNghiep,
                            Reference = tkmd.GUIDSTR,
                            CustomsReference = tkmd.SoTiepNhan.ToString(),
                            Acceptance = tkmd.NgayDangKy.ToString(),
                            Number = tkmd.SoToKhai.ToString(),
                            NatureOfTransaction = tkmd.MaLoaiHinh,
                            DeclarationOffice = tkmd.MaHaiQuan,
                            StreamlineContent = tkmd.HUONGDAN,
                            Type = "GC",
                        });
                        string content = Helpers.Serializer(tkmd);
                        content = Helpers.ConvertToBase64(content);
                        syncDaTa.Content = new Content { Text = content };
                        msgSend = Helpers.Serializer(syncDaTa);
                        msgSend = wsSyncData.SendDaiLy(msgSend, user, password);
                        if (!string.IsNullOrEmpty(msgSend))
                        {
                            //Warring info
                            if (msgSend == "MK")
                            {
                                OnSynce(new SyncEventArgs(new Exception("Sai mật khẩu kết nối")));
                                GlobalSettings.USERNAME_DONGBO = string.Empty;
                                GlobalSettings.PASSWOR_DONGBO = string.Empty;
                                break;
                            }
                            else
                            { 
                                OnSynce(new SyncEventArgs(new Exception(msgSend)));
                                SaveLogError(tkmd, msgSend);
                            }

                        }
                        else
                        {
                            Status sttk = new Status();
                            sttk.GUIDSTR = tkmd.GUIDSTR;
                            sttk.MSG_STATUS = 1;
                            if (tkmd.MaLoaiHinh.Substring(0,1).ToUpper()=="N")
                                sttk.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.GC_TOKHAI_NHAP);
                            else
                                sttk.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.GC_TOKHAI_XUAT);
                            sttk.CREATE_TIME = DateTime.Now;
                            sttk.InsertUpdate();
                            valSend++;
                            string msg = string.Format("Đã gửi thông tin {0}/{1}/{2}", tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.NamDK);
                            percent = (int)((pos * 100) / total);
                            OnSynce(new SyncEventArgs(msg, percent));
                        }
                    }
                    catch (Exception ex)
                    {
                        OnSynce(new SyncEventArgs(ex));
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
                OnSynce(new SyncEventArgs(string.Format("Đã gửi được {0}/{1} tờ khai", valSend, total), (int)((pos * 100) / total)));
            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));
            }
        }
        public void GC_SendData_GCCT(List<ToKhaiChuyenTiep> tokhaiSelect, string user, string password)
        {
            try
            {
                int percent = 0;
                int total = tokhaiSelect.Count;
                int pos = 0;
                int valSend = 0;
                string msgSend = string.Empty;
                foreach (ToKhaiChuyenTiep tkct  in tokhaiSelect)
                {
                    pos++;
                    try
                    {
                        #region  Hợp đồng chính
                        HopDong HD = HopDong.Load(tkct.IDHopDong);
                        if (HD == null)
                        {
                            OnSynce(new SyncEventArgs(new Exception("Không tìm thấy hợp đồng của tờ khai ")));
                            return;
                        }
                        
                        if (!this.GC_SendDataHopDong(HD, user, password))
                        {
                            OnSynce(new SyncEventArgs(new Exception("Dừng đồng bộ, Hợp đồng không thể gửi đi")));
                            return;
                        }

                        
                        this.GC_SendDataPhuKien(HD, user, password);
                        #endregion

                        #region Hợp đồng phụ
                        HopDong HD2 = new HopDong();
                        if (tkct.ID_Relation == 0)
                        {
                            List<HopDong> _listHD = HopDong.SelectCollectionDynamic("SoHopDong = '" + tkct.SoHDKH + "'", null);
                            if (_listHD != null)
                                HD2 = _listHD[0];
                        }
                        else
                            HD2 = HopDong.Load(tkct.ID_Relation);
                        if (HD2 == null)
                        {
                            OnSynce(new SyncEventArgs(new Exception("Không tìm thấy hợp đồng của tờ khai ")));
                            return;
                        }
                        OnSynce(new SyncEventArgs("Đang Đồng bộ Hợp Đồng " + HD2.SoHopDong, percent));
                        if (!this.GC_SendDataHopDong(HD2, user, password))
                        {
                            OnSynce(new SyncEventArgs(new Exception("Dừng đồng bộ, Hợp đồng không thể gửi đi")));
                            return;
                        }
                        OnSynce(new SyncEventArgs("Đang Đồng bộ Phụ Kiện của HĐ " + HD2.SoHopDong, percent));
                        this.GC_SendDataPhuKien(HD2, user, password);

                        #endregion
                        
                        tkct.LoadHCTCollection();
                        tkct.LoadChungTuHaiQuan(string.Empty);
                        if (string.IsNullOrEmpty(tkct.GUIDSTR))
                        {
                            tkct.GUIDSTR = Guid.NewGuid().ToString();
                            tkct.Update();
                        }
                        SyncDaTa syncDaTa = new SyncDaTa()
                        {
                            Type = "TKCT",
                            Declaration = new List<SyncDaTaDetail>()
                        };
                        syncDaTa.Declaration.Add(new SyncDaTaDetail
                        {
                            Issuer = user,
                            Business = tkct.MaDoanhNghiep,
                            Reference = tkct.GUIDSTR,
                            CustomsReference = tkct.SoTiepNhan.ToString(),
                            Acceptance = tkct.NgayDangKy.ToString(),
                            Number = tkct.SoToKhai.ToString(),
                            NatureOfTransaction = tkct.MaLoaiHinh,
                            DeclarationOffice = tkct.MaHaiQuanTiepNhan,
                            StreamlineContent = tkct.HUONGDAN,
                            Type = "GC",
                        });
                        string content = Helpers.Serializer(tkct);
                        content = Helpers.ConvertToBase64(content);
                        syncDaTa.Content = new Content { Text = content };
                        msgSend = Helpers.Serializer(syncDaTa);
                        msgSend = wsSyncData.SendDaiLy(msgSend, user, password);
                        if (!string.IsNullOrEmpty(msgSend))
                        {
                            //Warring info
                            if (msgSend == "MK")
                            {
                                OnSynce(new SyncEventArgs(new Exception("Sai mật khẩu kết nối")));
                                GlobalSettings.USERNAME_DONGBO = string.Empty;
                                GlobalSettings.PASSWOR_DONGBO = string.Empty;
                                break;
                            }
                            else
                            {
                                OnSynce(new SyncEventArgs(new Exception(msgSend)));
                                SaveLogError(tkct, msgSend);
                            }

                        }
                        else
                        {
                            Status sttk = new Status();
                            sttk.GUIDSTR = tkct.GUIDSTR;
                            sttk.MSG_STATUS = 1;
                            sttk.MSG_TYPE = tkct.MaLoaiHinh.Substring(0, 1).ToUpper() == "N" ? Convert.ToInt16(DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP) : Convert.ToInt16(DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT);
                            sttk.CREATE_TIME = DateTime.Now;
                            sttk.InsertUpdate();
                            valSend++;
                            string msg = string.Format("Đã gửi thông tin {0}/{1}/{2}", tkct.SoToKhai, tkct.MaLoaiHinh, tkct.NamDK);
                            percent = (int)((pos * 100) / total);
                            OnSynce(new SyncEventArgs(msg, percent));
                        }
                    }
                    catch (Exception ex)
                    {
                        OnSynce(new SyncEventArgs(ex));
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
                OnSynce(new SyncEventArgs(string.Format("Đã gửi được {0}/{1} tờ khai", valSend, total), (int)((pos * 100) / total)));
            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));
            }
        }


        public void GC_SearchData(string user, string password, DateTime fromDate, DateTime toDate, bool timTatCa)
        {
            try
            {
                OnSynce(new SyncEventArgs("Bắt đầu tìm kiếm...", 0));

                SyncDaTa syncdata = new SyncDaTa();
                syncdata.issuer = GlobalSettings.MA_DON_VI;
                syncdata.FromDate = fromDate.ToString("yyyy-MM-dd");
                syncdata.ToDate = toDate.ToString("yyyy-MM-dd");
                syncdata.Type = "GC";
                syncdata.Status = timTatCa ? "1" : "0";
                Thread.Sleep(200);
                OnSynce(new SyncEventArgs("Truyền thông tin yêu cầu...", 0));
                string msg = Helpers.ConvertToBase64(Helpers.Serializer(syncdata));
                string msgFeedback = string.Empty;
                msgFeedback = wsSyncData.ReceivesViews(user, password, msg);

                if (msgFeedback == "MK")
                {
                    OnSynce(new SyncEventArgs(new Exception("Sai mật khẩu kết nối")));
                    GlobalSettings.USERNAME_DONGBO = string.Empty;
                    GlobalSettings.PASSWOR_DONGBO = string.Empty;
                    return;
                }
                else
                {
                    if (string.IsNullOrEmpty(msgFeedback))
                    {

                        OnSynce(new SyncEventArgs(new Exception("Không tìm thấy tờ khai")));
                        return;
                    }
                    else
                    {
                        SyncDaTa feedback = new SyncDaTa();
                        try
                        {
                            feedback = Helpers.Deserialize<SyncDaTa>(msgFeedback);
                        }
                        catch (System.Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            OnSynce(new SyncEventArgs(new Exception("Có lỗi trả về từ server: " + msgFeedback)));
                            return;
                        }
                        List<Detail> chitietCO = MapperDBDL.MapperView(feedback.Declaration);
                        OnSynce(new SyncEventArgs("Đã tải dữ liệu từ đại lý về", 100, null, chitietCO));
                    }
                }
            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));

            }
        }
        
        public void GC_GetData(string user, string password, List<Detail> detailSelect)
        {
            try
            {
                int pos = 0;
                int total = detailSelect.Count;
                int valDownload = 0;
                
                if (!this.GC_GetDataHopDong(user,password))
                {
                    OnSynce(new SyncEventArgs(new Exception("Dừng đồng bộ, Hợp đồng không thể nhận về ")));
                    return;
                }
                this.GC_GetDataPhuKien(user,password);
                
                this.GC_GetDataDinhMuc(user, password);

                foreach (Detail item in detailSelect)
                {
                    string sfmtToKhai = string.Format("{0}/{1}/{2}", item.SoToKhai, item.MaLoaiHinh, item.MaHaiQuan);
                    OnSynce(new SyncEventArgs("Đang tải " + sfmtToKhai, (int)((pos++ * 100) / total)));
                    string msgFeedback = wsSyncData.SendDoanhNghiep(item.GUIDSTR, user, password);
                    if (msgFeedback == "MK")
                    {
                        OnSynce(new SyncEventArgs(new Exception("Sai tên đăng nhập hoặc mật khẩu ")));
                        GlobalSettings.USERNAME_DONGBO = string.Empty;
                        GlobalSettings.PASSWOR_DONGBO = string.Empty;
                        return;
                    }
                    SyncDaTa xml = new SyncDaTa();

                    try
                    {
                        string content = string.Empty;
                        string error = string.Empty;
                        xml = Helpers.Deserialize<SyncDaTa>(msgFeedback);
                        if (xml.Type == "TKCT")
                        {
                            content = Helpers.ConvertFromBase64(xml.Content.Text);
                            ToKhaiChuyenTiep tk = Helpers.Deserialize<ToKhaiChuyenTiep>(content);
                            error = tk.InsertFullFromISyncDaTa();
                        } 
                        else if (xml.Type == "TKMD")
                        {
                            content = Helpers.ConvertFromBase64(xml.Content.Text);
                            ToKhaiMauDich tk = Helpers.Deserialize<ToKhaiMauDich>(content);
                            error = tk.InsertFullFromISyncDaTa();
                        }

                        if (!string.IsNullOrEmpty(error))
                        {
                            SaveLogError(item, error);
                            OnSynce(new SyncEventArgs(new Exception(string.Format("Cập nhật dữ liệu {0} không thành công!", sfmtToKhai))));
                        }
                        valDownload++;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        OnSynce(new SyncEventArgs(ex));
                    }
                }
                OnSynce(new SyncEventArgs(string.Format("Đã tải được {0}/{1} tờ khai", valDownload, total), (int)((pos * 100) / total)));

            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));
            }
        }


        #region Send Data HĐ - ĐM - PK
        private bool GC_SendDataHopDong(HopDong HD, string user, string password)
        {
            OnSynce(new SyncEventArgs("Đang Đồng bộ Hợp Đồng " + HD.SoHopDong, 10)); 
            Status sthd = Status.Load(HD.GUIDSTR);
            HD.TKCTCollection = null;
            if (sthd == null || sthd.MSG_STATUS == 0)
            {
                try
                {
                    HD.LoadCollection();
                    if (string.IsNullOrEmpty(HD.GUIDSTR))
                    {
                        HD.GUIDSTR = Guid.NewGuid().ToString();
                        HD.Update();
                    }
                    string msgHD = Helpers.Serializer(HD);
                    msgHD = Helpers.ConvertToBase64(msgHD);
                    SyncDaTa syn = new SyncDaTa();
                    syn.Content = new Content { Text = msgHD };
                    syn.Declaration = new List<SyncDaTaDetail>();
                    syn.Declaration.Add(new SyncDaTaDetail
                    {
                        Business = GlobalSettings.MA_DON_VI,
                        Reference = HD.GUIDSTR,
                        Type = "GC",
                    });
                    syn.Type = "HDGC";
                    string msgSendHD = Helpers.Serializer(syn);
                    string HDfeedback = wsSyncData.SendGC(msgSendHD, user, password);
                    if (!string.IsNullOrEmpty(HDfeedback))
                    {
                        SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng " + HD.SoHopDong + ": " + HDfeedback);
                        OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng" + HDfeedback)));
                        return false;
                    }
                    else
                    {
                        sthd = new Status();
                        sthd.GUIDSTR = HD.GUIDSTR;
                        sthd.MSG_STATUS = 1;
                        sthd.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.HOP_DONG_GIA_CONG);
                        sthd.CREATE_TIME = DateTime.Now;
                        sthd.InsertUpdate();
                        return true;
                    }
                }
                catch (System.Exception ex)
                {
                    OnSynce(new SyncEventArgs(ex));
                    SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng " + HD.SoHopDong + ": " + ex.Message);
                    return false;
                }
            }
            return true;
        }
        private bool GC_SendDataPhuKien(HopDong HD, string user, string password)
        {
            PhuKienDangKy _pkdk = new PhuKienDangKy();
            _pkdk.HopDong_ID = HD.ID;
            PhuKienDangKyCollection pkDKCO = _pkdk.SelectCollectionBy_HopDong_ID();
            for (int j = 0; j < pkDKCO.Count; j++)
            {
                OnSynce(new SyncEventArgs("Đang Đồng bộ Phụ Kiện của HĐ " + HD.SoHopDong, (int)((j+1)*100/pkDKCO.Count)));
                if (pkDKCO[j].TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    Status stpk = Status.Load(pkDKCO[j].GUIDSTR);
                    if (stpk == null || stpk.MSG_STATUS == 0)
                    {
                        try
                        {
                            pkDKCO[j].LoadCollection();
                            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien item in pkDKCO[j].PKCollection)
                            {
                                item.LoadCollection();
                            }
                            if (string.IsNullOrEmpty(pkDKCO[j].GUIDSTR))
                            {
                                pkDKCO[j].GUIDSTR = Guid.NewGuid().ToString();
                                pkDKCO[j].Update();
                            }
                            pkDKCO[j].SoHopDong = HD.SoHopDong;
                            string msgPK = Helpers.Serializer(pkDKCO[j]);
                            msgPK = Helpers.ConvertToBase64(msgPK);
                            SyncDaTa syn = new SyncDaTa();
                            syn.Content = new Content { Text = msgPK };
                            syn.Declaration = new List<SyncDaTaDetail>();
                            syn.Declaration.Add(new SyncDaTaDetail
                            {
                                Business = GlobalSettings.MA_DON_VI,
                                Reference = pkDKCO[j].GUIDSTR,
                                Type = "GC",
                            });
                            syn.Type = "PKGC";
                            string msgSendPK = Helpers.Serializer(syn);
                            string PKfeedback = wsSyncData.SendGC(msgSendPK, user, password);
                            if (!string.IsNullOrEmpty(PKfeedback))
                            {
                                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện số " + pkDKCO[j].SoPhuKien + ":" + PKfeedback);
                                return false;
                            }
                            else
                            {
                                stpk = new Status();
                                stpk.GUIDSTR = pkDKCO[j].GUIDSTR;
                                stpk.MSG_STATUS = 1;
                                stpk.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.GC_PHU_KIEN_HOP_DONG);
                                stpk.CREATE_TIME = DateTime.Now;
                                stpk.InsertUpdate();
                            }
                        }
                        catch (System.Exception ex)
                        {
                            SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện số " + pkDKCO[j].SoPhuKien + ":" + ex.Message);
                            return false;
                        }
                        Thread.Sleep(3000);
                    }
                }
            }
            return true;
        }
        private bool GC_SendDataDinhMuc(HopDong HD, string user, string password)
        {
            DinhMucDangKy dmdk = new DinhMucDangKy();
            dmdk.ID_HopDong = HD.ID;
            DinhMucDangKyCollection dmdkCo = dmdk.SelectCollectionBy_ID_HopDong();
            for (int j = 0; j < dmdkCo.Count; j++)
            {
                OnSynce(new SyncEventArgs("Đang Đồng bộ Định Mức của HĐ " + HD.SoHopDong, (int)((j+1)*100/dmdkCo.Count)));
                if (dmdkCo[j].TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    Status stdm = Status.Load(dmdkCo[j].GUIDSTR);
                    if (stdm == null || stdm.MSG_STATUS == 0)
                    {
                        try
                        {
                            dmdkCo[j].LoadCollection();
                            if (string.IsNullOrEmpty(dmdkCo[j].GUIDSTR))
                            {
                                dmdkCo[j].GUIDSTR = Guid.NewGuid().ToString();
                                dmdkCo[j].Update();
                            }
                            dmdkCo[j].SoHopDong = HD.SoHopDong;
                            string msgDM = Helpers.Serializer(dmdkCo[j]);
                            msgDM = Helpers.ConvertToBase64(msgDM);
                            SyncDaTa syn = new SyncDaTa();
                            syn.Content = new Content { Text = msgDM };
                            syn.Declaration = new List<SyncDaTaDetail>();
                            syn.Declaration.Add(new SyncDaTaDetail
                            {
                                Business = GlobalSettings.MA_DON_VI,
                                Reference = dmdkCo[j].GUIDSTR,
                                Type = "GC",
                            });
                            syn.Type = "DMGC";
                            string msgSendDM = Helpers.Serializer(syn);
                            string DMfeedback = wsSyncData.SendGC(msgSendDM, user, password);
                            if (!string.IsNullOrEmpty(DMfeedback))
                            {
                                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức số " + dmdkCo[j].ID + ":" + DMfeedback);
                                return false;
                            }
                            else
                            {
                                stdm = new Status();
                                stdm.GUIDSTR = dmdkCo[j].GUIDSTR;
                                stdm.MSG_STATUS = 1;
                                stdm.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.GC_DINH_MUC);
                                stdm.CREATE_TIME = DateTime.Now;
                                stdm.InsertUpdate();
                            }
                        }
                        catch (System.Exception ex)
                        {
                            SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức số " + dmdkCo[j].ID + ":" + ex.Message);
                            return false;
                        }
                        Thread.Sleep(3000);
                    }

                }

            }
            return true;
        }
        #endregion


        #region Get DaTa HĐ - ĐM - PK
        private bool GC_GetDataHopDong(string user, string password)
        {
            try
            {
                OnSynce(new SyncEventArgs("Đang tải hợp đồng...", 30));
                string msgFeedBackHD = wsSyncData.ReceivesGC("HDGC", user, password);
                while (!string.IsNullOrEmpty(msgFeedBackHD))
                {
                    try
                    {
                        if (msgFeedBackHD == "MK")
                        {
                            OnSynce(new SyncEventArgs(new Exception("Sai tên đăng nhập hoặc mật khẩu ")));
                            GlobalSettings.USERNAME_DONGBO = string.Empty;
                            GlobalSettings.PASSWOR_DONGBO = string.Empty;
                            return false;
                        }
                        else
                        {
                            SyncDaTa xml = Helpers.Deserialize<SyncDaTa>(msgFeedBackHD);
                            HopDong HD = Helpers.Deserialize<HopDong>(Helpers.ConvertFromBase64(xml.Content.Text));
                            string error = HD.InsertFullFromISyncDaTa();
                            if (!string.IsNullOrEmpty(error))
                            {
                                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng " + HD.SoHopDong + ": " + error);
                                OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng " + HD.SoHopDong + ": " + error)));
                                return false;
                            }
                            Thread.Sleep(3000);
                            msgFeedBackHD = wsSyncData.ReceivesGC("HDGC", user, password);

                        }
                    }
                    catch
                    {
                        SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng : " + msgFeedBackHD);
                        OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng: " + msgFeedBackHD)));
                        return false;
                    }

                }
            }
            catch (System.Exception ex)
            {
                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng : " + ex.Message);
                OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng: " + ex.Message)));
                return false;
            }
            return true;
        }
        private bool GC_GetDataDinhMuc(string user, string password)
        {
            int pos = 1;
            try
            {
                string msgFeedBackDM = wsSyncData.ReceivesGC("DMGC", user, password);
                while (!string.IsNullOrEmpty(msgFeedBackDM))
                {
                    try
                    {
                        if (pos == 99) pos = 2;
                        OnSynce(new SyncEventArgs("Đang tải định mức...", pos++));

                        if (msgFeedBackDM == "MK")
                        {
                            OnSynce(new SyncEventArgs(new Exception("Sai tên đăng nhập hoặc mật khẩu ")));
                            GlobalSettings.USERNAME_DONGBO = string.Empty;
                            GlobalSettings.PASSWOR_DONGBO = string.Empty;
                            return false;
                        }
                        else
                        {
                            SyncDaTa xml = Helpers.Deserialize<SyncDaTa>(msgFeedBackDM);
                            DinhMucDangKy Dm = Helpers.Deserialize<DinhMucDangKy>(Helpers.ConvertFromBase64(xml.Content.Text));
                            string error = Dm.InsertFullFromISyncDaTa();
                            Dm.TransferGC();
                            if (!string.IsNullOrEmpty(error))
                            {
                                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức : " + error);
                                OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu định mức: " + error)));
                                return false;
                            }
                            Thread.Sleep(3000);
                            msgFeedBackDM = wsSyncData.ReceivesGC("DMGC", user, password);
                        }
                    }
                    catch
                    {
                        SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức : " + msgFeedBackDM);
                        OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu định mức: " + msgFeedBackDM)));
                        return false;
                    }

                }
                OnSynce(new SyncEventArgs("Đang tải định mức...", 100));
            }
            catch (System.Exception ex)
            {
                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức : " + ex.Message);
                OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu định mức: " + ex.Message)));
                return false;
            }
            return true;
        }
        private bool GC_GetDataPhuKien(string user, string password)
        {
            int pos = 1;
            try
            {
                string msgFeedBackPK = wsSyncData.ReceivesGC("PKGC", user, password);
                while (!string.IsNullOrEmpty(msgFeedBackPK))
                {
                    try
                    {
                        if (pos == 99) pos = 2;
                        OnSynce(new SyncEventArgs("Đang tải phụ kiện hợp đồng...", pos++));

                        if (msgFeedBackPK == "MK")
                        {
                            OnSynce(new SyncEventArgs(new Exception("Sai tên đăng nhập hoặc mật khẩu ")));
                            GlobalSettings.USERNAME_DONGBO = string.Empty;
                            GlobalSettings.PASSWOR_DONGBO = string.Empty;
                            return false;
                        }
                        else
                        {
                            SyncDaTa xml = Helpers.Deserialize<SyncDaTa>(msgFeedBackPK);
                            PhuKienDangKy Pk = Helpers.Deserialize<PhuKienDangKy>(Helpers.ConvertFromBase64(xml.Content.Text));
                            string error = Pk.InsertFullFromISyncDaTa();
                            if (!string.IsNullOrEmpty(error))
                            {
                                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện " + Pk.SoPhuKien + ": " + error);
                                OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện " + Pk.SoPhuKien + ": " + error)));
                                return false;
                            }
                            Thread.Sleep(3000);
                            msgFeedBackPK = wsSyncData.ReceivesGC("PKGC", user, password);
                        }
                    }
                    catch
                    {
                        SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện Hơp đồng : " + msgFeedBackPK);
                        OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu phụ kiện Hơp đồng: " + msgFeedBackPK)));
                        return false;
                    }

                }
                OnSynce(new SyncEventArgs("Đang tải phụ kiện hợp đồng...", 100));
            }
            catch (System.Exception ex)
            {
                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng : " + ex.Message);
                OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu Hơp đồng: " + ex.Message)));
                return false;
            }
            return true;
        }
        #endregion

    }
}
