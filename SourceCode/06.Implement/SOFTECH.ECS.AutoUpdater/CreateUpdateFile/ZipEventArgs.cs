﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CreateUpdateFile
{
    public class ZipEventArgs : EventArgs
    {
        public string FileName { get; private set; }
        public long FullSize { get; private set; }
        public int Percent { get; private set; }
        public string XmlContent { get; private set; }        
        public ZipEventArgs(string filename, long size, int percent, string xmlContent)
        {
            FileName = filename;
            FullSize = size;
            Percent = percent;
            XmlContent = xmlContent;
        }
    }
}
