﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.Messages.Send;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class TEA : BasicVNACC
    {
        public List<TEA_HANG> listHang { get; set; }

        public StringBuilder BuilEdiMessagesTEA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<TEA>(StrBuild, true, GlobalVNACC.PathConfig, EnumNghiepVu.TEA);
            foreach (TEA_HANG item in listHang)
            {
                item.BuildEdiMessages<TEA_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "TEA_HANG");
            }
            return str;
        }

    }
}
