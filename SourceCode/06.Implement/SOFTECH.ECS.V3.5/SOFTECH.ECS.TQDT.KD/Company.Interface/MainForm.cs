﻿using System;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.Interface.SXXK;
using Janus.Windows.ExplorerBar;
using Company.KD.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.LookAndFeel;
using Company.KD.BLL.KDT.SXXK;
using Company.Interface.Report;
using Company.Interface.DanhMucChuan;
using System.IO;
using System.Xml.Serialization;
using Company.QuanTri;
using Company.Interface.QuanTri;
using System.Globalization;
using System.Threading;
using System.Text;
using System.Security.Cryptography;
using System.Xml;
using Company.Interface.PhongKhai;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Configuration;
using Company.Interface.DongBoDuLieu;
using Company.KDT.SHARE.VNACCS;
using Company.Interface.VNACCS.PhieuKho;
using Company.Interface.VNACCS.Vouchers;
using Company.Interface.VNACCS;
using IWshRuntimeLibrary;
using System.Drawing;
using Company.Interface.ProdInfo;
using Company.Interface.VNACCS.TollManagement;

namespace Company.Interface
{
    public partial class MainForm : BaseForm
    {
        public static int flag = 0;
        public static bool isLoginSuccess = false;
        public static bool isUseSettingVNACC = true;
        WebBrowser wbManin = new WebBrowser();
        System.Windows.Forms.HtmlDocument docHTML;
        private HtmlAgilityPack.HtmlDocument htmlAgilityPackDoc = new HtmlAgilityPack.HtmlDocument();
        public static int versionHD = 0;
        public static int typeLogin = 0;
        int soLanLayTyGia = 0;
        private FrmQuerySQLUpdate frmQuery;
        private FrmDaiLy frmDaiLy;
        public ThongBaoFormVNACCS frmThongBao = new ThongBaoFormVNACCS();
        public TrangThaiPhanHoiForm frmTrangThaiPhanHoi = new TrangThaiPhanHoiForm();
        private FrmDongBoDuLieu_VNACCS DBDLForm_VNACCS;
        private Company.Interface.VNACCS.QuanLyMessageForm quanLyMsg = new Company.Interface.VNACCS.QuanLyMessageForm();
        //0 la ban doanh nghiep // 1 ban dai ly // 2 la ban phong khai
        #region SXXK
        private static QueueForm queueForm = new QueueForm();
        // Nguyên phụ liệu.       
        private NguyenPhuLieuRegistedForm nplRegistedForm;
        // Sản phẩm.
        //private SanPhamForm spForm;        
        private SanPhamRegistedForm spRegistedForm;

        //bao cao-thong ke
        private BaocaoThongkeTK thongkeTKForm;
        private BaocaoTriGiaXK bcTriGiaXKForm;
        private BaoCaoThongKeNuoc_Hang thongkeKNForm;

        ToKhaiMauDichManageForm tkmdManageForm;
        private ToKhaiMauDichRegisterForm tkmdRegisterForm;
        ToKhaiMauDichForm tkmdForm;

        #endregion

        #region VNACC
        //Hoa Don
        private VNACC_HoaDonForm khaiBaoHoaDon;
        private VNACC_GiayPhepForm_SEA khaiBaoGiayPhep;
        private VNACC_GiayPhepManageForm giayPhepManage;
        private VNACC_HoaDonManageForm hoaDonManage;
        //to khai tri gia thap
        private VNACC_TKNhapTriGiaForm toKhaiNhapTriGia;
        private VNACC_TKXuatTriGiaForm toKhaiXuatTriGia;
        private VNACC_TKTriGiaManager toKhaiTriGiaManager;
        //To khai VNACC
        private VNACC_ToKhaiMauDichXuatForm toKhaiXuat;
        private VNACC_ToKhaiMauDichNhapForm toKhaiNhap;
        private VNACC_ToKhaiManager toKhaiManager;
        // To khai van chuyen
        private VNACC_ChuyenCuaKhau toKhaiVanChuyen;
        private VNACC_ChuyenCuaKhau_ManagerForm theoDoiTKVanChuyen;

        // Dang ky hang miem thue TEA
        private VNACC_TEA_ManagerForm TEAManager;
        //Dang ky hang Tam nhap tai xuat
        private VNACC_TIA_ManagerForm TIAManager;
        #endregion

        //-----------------------------------------------------------------------------------------------

        #region Du lieu Chuan
        private MaHSForm maHSForm;
        private PTTTForm ptttForm;
        private PTVTForm ptvtForm;
        private DonViTinhForm dvtForm;
        private DonViHaiQuanForm dvhqForm;
        private CuaKhauForm ckForm;
        private NguyenTeForm ntForm;
        private DKGHForm dkghForm;
        private NuocForm nuocForm;
        private NhomCuaKhauForm nhomCuaKhauForm;
        private DonViDoiTacForm dvdtForm;
        private SyncDataForm dongBoForm;
        #endregion

        #region Quản trị
        public static ECSPrincipal EcsQuanTri;
        QuanLyNguoiDung NguoiDung;
        QuanLyNhomNguoiDung NhomNguoiDung;
        #endregion Quản trị

        #region Hien thi ngay thang nam, gio
        private System.Windows.Forms.Timer timerDateTime;

        private void SetTimerDateTimeStatus()
        {
            timerDateTime = new System.Windows.Forms.Timer();
            timerDateTime.Interval = 1000;
            timerDateTime.Tick += new EventHandler(timerDateTime_Tick);

            timerDateTime.Enabled = true;
            timerDateTime.Start();
            statusBar.Panels["DateTime"].AutoSize = StatusBarPanelAutoSize.Contents;

            statusBar.ImageList = ilSmall;
            statusBar.Panels["DateTime"].ImageIndex = 65;

            timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = 1000;
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Enabled = true;
            timer1.Start();
            statusBar.Panels["Support"].AutoSize = StatusBarPanelAutoSize.Spring;

        }

        void timerDateTime_Tick(object sender, EventArgs e)
        {
            statusBar.Panels["DateTime"].ToolTipText = statusBar.Panels["DateTime"].Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt");
        }

        #endregion

        QuanLyMessage quanlyMess;
        private void khoitao_GiaTriMacDinh()
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();

            try
            {
                statusBar.Panels["DoanhNghiep"].AutoSize = StatusBarPanelAutoSize.Spring;
                statusBar.Panels["HaiQuan"].AutoSize = StatusBarPanelAutoSize.Contents;
                statusBar.Panels["HaiQuan"].Width = 250;
                statusBar.Panels["Service"].AutoSize = StatusBarPanelAutoSize.None;
                statusBar.Panels["Service"].Width = 250;
                statusBar.Panels["Terminal"].AutoSize = StatusBarPanelAutoSize.Contents;

                if (versionHD == 0)
                {
                    string statusStr1 = setText(string.Format("Mã/ tên người khai HQ: {0}/ {1}", ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.HO_TEN) + " - " + string.Format("Mã/ tên Doanh nghiệp: {0}/ {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI), "User: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Company : {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI));
                    //string statusStr2 = setText(string.Format("Hải quan tiếp nhận: {0}/ {1}.", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN), string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN));
                    statusBar.Panels["HaiQuan"].Text = setText(string.Format("Hải quan tiếp nhận: {0}", GlobalSettings.MA_HAI_QUAN), string.Format("Customs : {0}", GlobalSettings.MA_HAI_QUAN));
                    string statusStr3 = string.Format("Service khai báo: {0}", GlobalSettings.DiaChiWS);
                    statusBar.Panels["DoanhNghiep"].ToolTipText = statusBar.Panels["DoanhNghiep"].Text = statusStr1;
                    statusBar.Panels["HaiQuan"].ToolTipText = setText(string.Format("Hải quan tiếp nhận: {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN), string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN));
                    statusBar.Panels["Service"].ToolTipText = statusBar.Panels["Service"].Text = statusStr3;
                    statusBar.Panels["Terminal"].ToolTipText = statusBar.Panels["Terminal"].Text = string.Format("Terminal ID: {0}", GlobalVNACC.TerminalID != null && GlobalVNACC.TerminalID != "" ? GlobalVNACC.TerminalID : "?");
                }
                else
                {
                    string diaChiKhaibao = "http://" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(GlobalSettings.MA_DON_VI, "DIA_CHI_HQ").Value_Config.ToString() +
                                        "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(GlobalSettings.MA_DON_VI, "TEN_DICH_VU").Value_Config.ToString();
                    //this.Text = "Hệ thống hỗ trợ khai báo từ xa loại hình Kinh Doanh - Đầu Tư version " + Application.ProductVersion;
                    statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                    //statusBar.Panels["HaiQuan"].Text = statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Hải quan tiếp nhận: {0}/ {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                    statusBar.Panels["HaiQuan"].Text = string.Format("Hải quan tiếp nhận: {0}", GlobalSettings.MA_HAI_QUAN);
                    statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Hải quan tiếp nhận: {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                    statusBar.Panels["Service"].Text = statusBar.Panels["Service"].ToolTipText = string.Format("Service khai báo: {0}", GlobalSettings.DiaChiWS);
                    statusBar.Panels["Terminal"].ToolTipText = statusBar.Panels["Terminal"].Text = string.Format("Terminal ID: {0}", GlobalVNACC.TerminalID != null && GlobalVNACC.TerminalID != "" ? GlobalVNACC.TerminalID : "?");
                }

                statusBar.Panels["Terminal"].FormatStyle.ForeColor = GlobalVNACC.TerminalID != null && GlobalVNACC.TerminalID != "" ? System.Drawing.Color.Blue : System.Drawing.Color.Red;

                //TODO: Hungtq updaetd 7/2/2013. Khoitao_giatriMacDinh()
                string VersionSend = "V?";
                bool CKS = false;

                CKS = Company.KDT.SHARE.Components.Globals.IsSignOnLan || Company.KDT.SHARE.Components.Globals.IsSignRemote || Company.KDT.SHARE.Components.Globals.IsSignature;
                if (versionHD == 0)
                {
                    if (Company.KDT.SHARE.Components.Globals.IsKTX)
                        VersionSend = string.Format("V{0}", 1);
                    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                        VersionSend = string.Format("V{0}", 2);
                    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00" && GlobalSettings.SendV4 == false)
                        VersionSend = string.Format("V{0}", 3);
                    else if (GlobalSettings.SendV4) //Mac dinh V4
                        VersionSend = string.Format("V{0}", 4);
                    if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS) //Mac dinh V4
                        VersionSend = string.Format("V{0}", 5);

                }
                else
                {
                    string MaDN = GlobalSettings.MA_DON_VI;
                    VersionSend = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "SendV4").Value_Config.ToString();
                    if (VersionSend == "True")
                    {
                        VersionSend = "V4";
                        if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS) //khai báo VNACCS
                            VersionSend = string.Format("V{0}", 5);
                    }
                    else
                        VersionSend = "V3";

                }
                statusBar.Panels["Version"].ToolTipText = statusBar.Panels["Version"].Text = VersionSend;
                statusBar.Panels["Version"].FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
                statusBar.ImageList = ilSmall;
                statusBar.Panels["CKS"].ToolTipText = (CKS ? "CKS" : "");
                statusBar.Panels["CKS"].Text = "";
                statusBar.Panels["CKS"].ImageIndex = (CKS ? 60 : -1);

                //statusBar.Panels["DoanhNghiep"].ImageIndex = 37;
                statusBar.Panels["HaiQuan"].ImageIndex = 61;
                statusBar.Panels["Terminal"].ImageIndex = 62;
                //statusBar.Panels["Service"].ImageIndex = 63;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        public static void LoadECSPrincipal(long id)
        {
            EcsQuanTri = new ECSPrincipal(id);
        }
        public static void ShowQueueForm()
        {
            queueForm.Show();
        }
        public static void AddToQueueForm(HangDoi hd)
        {
            queueForm.HDCollection.Add(hd);
            queueForm.RefreshQueue();
        }
        public MainForm()
        {
            //try
            //{
            //    Company.KDT.SHARE.Components.Globals.CreateShortcut(
            //        AppDomain.CurrentDomain.BaseDirectory + "SOFTECH.ECS.TQDT.KD.exe",
            //        Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\SOFTECH.ECS.TQDT.KD v5.0 - VNACCS.lnk",
            //        "", null, "", AppDomain.CurrentDomain.BaseDirectory, "");
            //}

            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage("Lỗi tạo tự động shortcut chương trình.", ex);
            //}

            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            wbManin.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbManin_DocumentCompleted);
            //this.FormClosing += new FormClosingEventHandler(MainForm_FormClosing);
            //
            // TODO: Add any constructor code after InitializeComponent call
            //

            //TODO: HUNGTQ updated 02/08/2012.
            CreateQuerySQLCommand();

            SetTimerDateTimeStatus();
        }
        void wbManin_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                //docHTML = wbManin.Document;
                //HtmlElement itemTyGia = null;
                //foreach (HtmlElement item in docHTML.GetElementsByTagName("div"))
                //{
                //    if (item.GetAttribute("id") == "wp_ds26")
                //    {
                //        itemTyGia = item;
                //        break;
                //    }
                //}
                //if (itemTyGia == null)
                //{
                //    MainForm.flag = 0;
                //    return;
                //}
                //int i = 1;
                //string stUSD = "";
                //try
                //{
                //    foreach (HtmlElement itemTD in docHTML.GetElementsByTagName("TD"))
                //    {
                //        if (itemTD.InnerText != null)
                //        {

                //        }
                //        if (itemTD.InnerText != null && itemTD.InnerText.IndexOf("1 USD=") > -1)
                //        {
                //            stUSD = itemTD.InnerText.Trim();
                //        }
                //    }
                //    if (stUSD != "")
                //    {
                //        stUSD = stUSD.Substring(6).Trim();
                //        stUSD = stUSD.Substring(0, stUSD.Length - 3).Trim();
                //        Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.Update(null, "USD", Convert.ToDecimal(stUSD));

                //    }
                //}
                //catch { }
                //XmlDocument doc = new XmlDocument();
                //XmlElement root = doc.CreateElement("Root");
                //doc.AppendChild(root);
                //foreach (HtmlElement itemTD in itemTyGia.GetElementsByTagName("TR"))
                //{
                //    if (i == 1)
                //    {
                //        i++;
                //        continue;
                //    }
                //    HtmlElementCollection collection = itemTD.GetElementsByTagName("TD");
                //    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                //    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                //    itemMaNT.InnerText = collection[1].InnerText;

                //    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                //    itemTenNT.InnerText = collection[2].InnerText;

                //    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                //    itemTyNT.InnerText = collection[3].InnerText;

                //    itemNT.AppendChild(itemMaNT);
                //    itemNT.AppendChild(itemTenNT);
                //    itemNT.AppendChild(itemTyNT);
                //    root.AppendChild(itemNT);
                //}
                //Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.UpdateTyGia(root);
                //doc.Save("TyGia.xml");
                //timer1.Enabled = false;
            }
            catch { MainForm.flag = 0; }
            //timer1.Enabled = false;
        }
        private void ShowLoginForm()
        {
            // this.Hide();
        }
        private void LoadDongBoDuLieuDN()
        {
            LoadDongBoDuLieuForm dongboDNForm = new LoadDongBoDuLieuForm();
            dongboDNForm.ShowDialog(this);
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                bool installCASuccess = Company.KDT.SHARE.VNACCS.ImportCertNet.InstallCA();
                string cer = installCASuccess ? " - Certified" : "- Not certified";

                //Hungtq complemented 14/12/2010
                //timer1.Enabled = false; //Disable timer get Exchange Rate
                string strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                string dateRelease = Company.KDT.SHARE.Components.DownloadUpdate.GetDateRelease();
                this.Text += " - Build " + strVersion + (dateRelease != "" ? string.Format(" ({0})", dateRelease) : "") + cer;
                this.Text += " - SĐT HỖ TRỢ : (0236) 3.840.888 . ĐỊA CHỈ EMAIL : ecs@softech.vn ";
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Application.StartupPath + "\\Config.xml");
                    XmlNode node = doc.SelectSingleNode("Root/Type");
                    MainForm.versionHD = Convert.ToInt32(node.InnerText);
                    // Đại lý.
                    if (MainForm.versionHD == 2)
                    {
                        cmdXuat.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        cmdNhap.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                    {
                        cmdXuat.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        cmdNhap.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    if (ConfigurationManager.AppSettings["NgonNgu"] == "0")
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecs-kinhdoanh.png");
                    }
                }
                catch { }
                GlobalSettings.IsDaiLy = MainForm.versionHD == 1 ? true : false;

                //try
                //{
                //if (Properties.Settings.Default.NgonNgu == "1")
                //{
                //    //this.BackgroundImage = System.Drawing.Image.FromFile("BG-KDDT.jpg");
                //}
                //else
                //{
                //    //this.BackgroundImage = System.Drawing.Image.FromFile("BG-KDDT.jpg");
                //}

                this.BackgroundImageLayout = ImageLayout.Stretch;
                //}
                //catch { }
                this.Hide();

                if (versionHD == 0)
                {
                    GlobalSettings.ISKHAIBAO = true;
                    Login login = new Login();
                    login.ShowDialog(this);
                }
                else if (versionHD == 1)
                {
                    ThongTinDaiLy DLinfo = new ThongTinDaiLy();
                    try
                    {

                        DLinfo = Company.KDT.SHARE.Components.HeThongPhongKhai.GetThongTinDL();
                        if (DLinfo == null)
                        {
                            Company.Interface.TTDaiLy.FrmInfo finfo = new Company.Interface.TTDaiLy.FrmInfo();
                            finfo.ShowDialog(this);
                            while (!finfo.isSuccess)
                            {
                                if (ShowMessage("Bạn chưa nhập thông tin đại lý. Vui lòng nhập đầy đủ thông tin đại lý khai báo trước khi sử dụng chương trình. Xin cảm ơn!\r\n\r\n Nhập lại thông tin đại lý ?", true) == "Yes")
                                {
                                    finfo = new Company.Interface.TTDaiLy.FrmInfo();
                                    finfo.ShowDialog(this);
                                }
                                else
                                {
                                    Application.ExitThread();
                                    Application.Exit();
                                    return;
                                }
                            }
                        }
                        else
                        {
                            GlobalSettings.MA_DAI_LY = DLinfo.MaDL;
                            GlobalSettings.TEN_DAI_LY = DLinfo.TenDL;

                        }
                    }

                    catch (System.Exception ex)
                    {
                        MessageBox.Show("Không kết nối được với cơ sở dữ liệu. " + ex.Message);
                        ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
                        f.ShowDialog(this);
                    }
                    Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                    login.ShowDialog(this);
                }
                else if (versionHD == 2)
                {
                    Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                    login.ShowDialog(this);
                }
                else if (versionHD == 3)
                {
                    Login login = new Login();
                    login.ShowDialog(this);
                    //DongBoDuLieu.Visible = DongBoDuLieu1.Visible = Janus.Windows.UI.InheritableBoolean.True;
                }
                if (isLoginSuccess)
                {
                    if (GlobalSettings.IsDaiLy)
                    {
                        ThongTinDaiLy DLinfo = Company.KDT.SHARE.Components.HeThongPhongKhai.GetThongTinDL();
                        if (DLinfo != null)
                        {
                            GlobalSettings.MA_DAI_LY = DLinfo.MaDL;
                            GlobalSettings.TEN_DAI_LY = DLinfo.TenDL;
                        }
                    }
                    this.Show();

                    //Hungtq updated 20/12/2011. Cau hinh Ecs Express
                    Express();

                    this.khoitao_GiaTriMacDinh();

                    //Hungtq updated 11/12/2012.
                    Company.KDT.SHARE.Components.Globals.KhoiTao_DanhMucChuanHQ();

                    if (versionHD == 0)
                    {
                        if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                        {
                            QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                            ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                        if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)))
                        {
                            cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                    }
                    else if (versionHD == 2)
                    {
                        switch (typeLogin)
                        {
                            case 1: // User Da Cau Hinh
                                this.Show();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 2: // User Chua Cau Hinh
                                this.Show();
                                DangKyForm dangKyForm = new DangKyForm();
                                dangKyForm.ShowDialog(this);
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 3:// Admin
                                this.Hide();
                                CreatAccountForm fAdminForm = new CreatAccountForm();
                                fAdminForm.ShowDialog(this);
                                this.LoginUserKhac();
                                break;
                        }
                    }

                    vsmMain.DefaultColorScheme = GlobalSettings.GiaoDien.Id;
                    if (GlobalSettings.GiaoDien.Id == "Office2007")
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    }

                    if (MainForm.versionHD != 2)
                    {
                        this.requestActivate();

                        int dayInt = 7;
                        int dateTrial = KeySecurity.Active.Install.License.KeyInfo.TrialDays;
                        //try
                        //{
                        //    string day = new Company.KDT.SHARE.Components.Utils.License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ExpiredDate"));
                        //    dayInt = int.Parse(day);
                        //}
                        //catch (Exception) { }
                        if (!string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Key))
                        {
                            cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            if (dayInt > dateTrial)
                                ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn SOFTECH để tiếp tục sử dụng", false);
                            //CultureInfo en = new CultureInfo("en-US");
                            //for (int k = 0; k < dayInt; k++)
                            //{
                            //    //Kiem tra Culture, neu la vi-VN -> chuyen doi ve cung 1 chuan en-US.
                            //    string dateString = DateTime.Now.Date.ToString("MM/dd/yyyy");

                            //    if (DateTime.Parse(dateString, en).AddDays(k) == DateTime.Parse(Program.lic.dayExpires, en).Date)
                            //    {
                            //        int ngayConLai = k + 1;
                            //        ShowMessage("Phần mềm còn " + ngayConLai + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn SOFTECH để tiếp tục sử dụng", false);
                            //        break;
                            //    }
                            //}
                            this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");
                        }
                        else
                        {
                            this.Text += setText(" - Bản dùng thử.", " - Trial.");
                        }
                    }
                    if (GlobalSettings.MA_DON_VI == "" || GlobalSettings.MA_DON_VI == "?")
                    {
                        ShowThietLapThongTinDNAndHQ();
                    }

                    //Hungtq complemented 14/12/2010
                    //timer1.Enabled = true; //Enable timer get Exchange Rate

                    backgroundWorker1.RunWorkerAsync();

                    //Sao luu du lieu
                    if (GlobalSettings.NGAYSAOLUU != null && GlobalSettings.NGAYSAOLUU != "")
                    {
                        string st = GlobalSettings.NGAYSAOLUU;
                        DateTime time = Convert.ToDateTime(GlobalSettings.NGAYSAOLUU);
                        int NHAC_NHO_SAO_LUU = Convert.ToInt32(GlobalSettings.NHAC_NHO_SAO_LUU);
                        TimeSpan time1 = DateTime.Today.Subtract(time);
                        int ngay = NHAC_NHO_SAO_LUU - time1.Days;
                        if (ngay <= 0)
                            ShowBackupAndRestore(true);
                    }
                    else
                        ShowBackupAndRestore(true);

                    if (!(GlobalSettings.LastBackup.Equals("")) && !(GlobalSettings.NHAC_NHO_SAO_LUU.Equals("")))
                        if (Convert.ToDateTime(GlobalSettings.LastBackup).Add(TimeSpan.Parse(GlobalSettings.NHAC_NHO_SAO_LUU)) == DateTime.Today)
                        {
                            ShowBackupAndRestore(true);
                        }

                    frmTrangThaiPhanHoi.Show();
                }
                else
                    Application.Exit();
                
                //}
                //else
                //{
                //    ShowMessage("Chương trình đã hết hạn sử dụng. \nLiên hệ với Softech để được cung cấp phiên bản chính thức. \nSố điện thoại: (0511)3.810535 hoặc FAX: (0511)3.810278.\nXin cám ơn quí khách hàng đã sử dụng chương trình của chúng tôi.", false);
                //    Application.Exit();
                //}
                if(!GlobalSettings.THONGBAOVNACC)
                {
                    FrmThongBaoVNACCS frmThongBaoVNACCS = new FrmThongBaoVNACCS();
                    frmThongBaoVNACCS.ShowDialog();
                }
                CreateShorcut();

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void requestActivate()
        {

            string sfmtMsg = "{0}\nVui lòng liên hệ với Công ty cổ phẩn SOFTECH để tiếp tục sử dụng";
            bool isShowActive = false;
            //if (KeySecurity.Active.Install.License.Status == KeySecurity.LicenseStatus.Lock)
            //{
            //    if (!string.IsNullOrEmpty(KeySecurity.Active.Install.License.Message))
            //    {
            //        InfoOnline f = new InfoOnline();
            //        f.SetRTF(KeySecurity.Active.Install.License.Message);
            //        f.Show();
            //    }
            //} 
            if (KeySecurity.Active.Install.License.Status == KeySecurity.LicenseStatus.Lock)
            {
                sfmtMsg = string.Format(sfmtMsg, KeySecurity.Active.Install.License.Message);
                isShowActive = true;
            }
            else if (KeySecurity.Active.Install.License.KeyInfo == null)
            {
                sfmtMsg = string.Format(sfmtMsg, "Bạn chưa đăng ký sử dụng phần mềm.");
                isShowActive = true;
            }
            //else if (Helpers.GetMD5Value(Security.Active.Install.License.KeyInfo.ProductId) != "ECS_TQDT_KD")
            //{
            //    sfmtMsg = string.Format(sfmtMsg, "Mã sản phẩm không hợp lệ.");
            //    isShowActive = true;
            //}
            else if (KeySecurity.Active.Install.License.KeyInfo.TrialDays == 0)
            {
                sfmtMsg = string.Format(sfmtMsg, "Đã hết hạn dùng phần mềm");
                isShowActive = true;
            }

            if (isShowActive)
            {
                if (ShowMessage(sfmtMsg, true) == "Yes")
                {
                    ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
                    if (obj.ShowDialog(this) == DialogResult.OK)
                    {
                        ShowMessage("Kích hoạt thành công. Chương trình sẽ tự khởi động lại", false);
                        Application.Restart();
                    }
                    else
                    {
                        ShowMessage("Kích hoạt không thành công, chương trình sẽ tự đóng", false);
                        Application.Exit();
                    }

                }
                else
                    Application.Exit();
            }
            if (KeySecurity.Active.Install.License.KeyInfo.IsShow
                && !string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Notice))
            {
                InfoOnline inf = new InfoOnline();
                inf.SetRTF(KeySecurity.Active.Install.License.KeyInfo.Notice);
                inf.ShowDialog();
            }
        }

        private void show_ThongKeKimNgachNuoc_MatHang()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongKeKimNgachNuoc_MatHang"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            thongkeKNForm = new Company.Interface.BaoCaoThongKeNuoc_Hang();
            thongkeKNForm.MdiParent = this;
            thongkeKNForm.Show();
        }
        //---------------------------------------------------------------------------------------

        private void show_TriGiaXK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TriGiaXK"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            bcTriGiaXKForm = new Company.Interface.BaocaoTriGiaXK();
            bcTriGiaXKForm.MdiParent = this;
            bcTriGiaXKForm.Show();
        }
        //---------------------------------------------------------------------------------------

        private void show_ThongKeTK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongKeTK"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            thongkeTKForm = new Company.Interface.BaocaoThongkeTK();
            thongkeTKForm.MdiParent = this;
            thongkeTKForm.Show();
        }
        //---------------------------------------------------------------------------------------
        private void show_ToKhaiSXXKDangKy(string nhomLH)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new ToKhaiMauDichRegisterForm();
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.NhomLoaiHinh = nhomLH;
            //   tkmdRegisterForm.Text = "Tờ khai "+nhomLH+" đã đăng ký";
            tkmdRegisterForm.Show();
        }

        private void show_ToKhaiSXXKManage(string NhomLH)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm" + NhomLH))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            //     tkmdManageForm.Name+=NhomLH;
            tkmdManageForm.nhomLoaiHinh = NhomLH;
            tkmdManageForm.MdiParent = this;
            //  tkmdManageForm.Text = "Theo dõi tờ khai "+NhomLH;
            tkmdManageForm.Show();
        }

        //-----------------------------------------------------------------------------------------------
        private void show_NguyenPhuLieuRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenPhuLieuRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplRegistedForm = new NguyenPhuLieuRegistedForm();
            nplRegistedForm.MdiParent = this;
            nplRegistedForm.Show();
        }

        ////-----------------------------------------------------------------------------------------------     

        //-----------------------------------------------------------------------------------------------
        private void show_SanPhamRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SanPhamRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            spRegistedForm = new SanPhamRegistedForm();
            spRegistedForm.MdiParent = this;
            spRegistedForm.Show();
        }

        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiMauDichForm(string nhomLoaiHinh)
        {
            if (MainForm.versionHD == 0)
            {
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)))
                {
                    ShowMessage("Bạn không có quyền thực hiện chức năng này.", false);
                    return;
                }
            }
            tkmdForm = new ToKhaiMauDichForm();
            tkmdForm.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            //   tkmdForm.Name = nhomLoaiHinh;
            tkmdForm.NhomLoaiHinh = nhomLoaiHinh;
            tkmdForm.MdiParent = this;
            tkmdForm.Show();
        }

        //-----------------------------------------------------------------------------------------

        private void show_ToKhaiMauDichManageForm(string nhomloaihinh)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.nhomLoaiHinh = nhomloaihinh;
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Show();
        }

        private void show_ToKhaiMauDichRegisterForm(string nhomLH)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm" + nhomLH))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new ToKhaiMauDichRegisterForm();
            tkmdRegisterForm.Name = "ToKhaiMauDichRegisterForm" + nhomLH;
            tkmdRegisterForm.NhomLoaiHinh = nhomLH;
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.Show();
        }

        private void pnlMain_SelectedPanelChanged(object sender, Janus.Windows.UI.Dock.PanelActionEventArgs e)
        {

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (queueForm.HDCollection.Count > 0)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(HangDoiCollection));
                FileStream fs = new FileStream("HangDoi.xml", FileMode.Create);
                serializer.Serialize(fs, queueForm.HDCollection);
            }
            queueForm.Dispose();
            var form = Application.OpenForms["TrangThaiPhanHoiForm"] as TrangThaiPhanHoiForm;
            if (form != null) form.Dispose();
            Application.Exit();
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //Hungtq, 21/11/2013.
                case "cmdHDSDCKS":
                    HDSD_CKS();
                    break;

                case "cmdNhomCuaKhau":
                    ShowNhomCuaKhauForm();
                    break;
                case "cmdCauHinhChuKySo":
                    ChuKySo();
                    break;
                case "cmdThoat":
                    this.Close();
                    break;
                case "DongBoDuLieu":
                    this.ShowDongBoDuLieuForm();
                    break;
                case "cmd2007":
                    if (cmd20071.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    GlobalSettings.Luu_GiaoDien("Office2007");
                    UpdateStyleForAllForm("Office2007");
                    //this.UpdateStyles();

                    break;
                case "cmd2003":
                    if (cmd20031.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    GlobalSettings.Luu_GiaoDien("Office2003");
                    UpdateStyleForAllForm("Office2003");
                    //this.UpdateStyles();
                    break;
                case "cmdVN":
                    if (ConfigurationManager.AppSettings["NgonNgu"] == "1")
                    {
                        if (MLMessages("Chương trình sẽ khởi động lại và chuyển giao diện sang tiếng Việt,hãy lưu lại các công việc đang làm.\nBạn muốn chuyển giao diện không?", "MSG_0203092", "", true) == "Yes")
                        {
                            ConfigurationManager.AppSettings["NgonNgu"] = "0";
                            System.Xml.XmlDocument newCfg = new System.Xml.XmlDocument();
                            newCfg.Load("SOFTECH.ECS.TQDT.KD.exe.config");
                            XmlNode newNode = newCfg.SelectSingleNode("configuration/userSettings/Company.Interface.Properties.Settings/setting[@name='NgonNgu']");
                            if (newNode != null) newNode.InnerText = ConfigurationManager.AppSettings["NgonNgu"];
                            newCfg.Save("SOFTECH.ECS.TQDT.KD.exe.config");
                            //Properties.Settings.Default.NgonNgu = "0";
                            //Properties.Settings.Default.Save();
                            CultureInfo culture = new CultureInfo("vi-VN");
                            //Thread.CurrentThread.CurrentCulture = culture;
                            //Thread.CurrentThread.CurrentUICulture = culture;
                            Application.Restart();
                        }

                    }
                    break;
                case "cmdEng":
                    if (ConfigurationManager.AppSettings["NgonNgu"] == "0")
                    {
                        ConfigurationManager.AppSettings["NgonNgu"] = "1";
                        System.Xml.XmlDocument newCfg = new System.Xml.XmlDocument();
                        newCfg.Load("SOFTECH.ECS.TQDT.KD.exe.config");
                        XmlNode newNode = newCfg.SelectSingleNode("configuration/userSettings/Company.Interface.Properties.Settings/setting[@name='NgonNgu']");
                        if (newNode != null) newNode.InnerText = ConfigurationManager.AppSettings["NgonNgu"];
                        newCfg.Save("SOFTECH.ECS.TQDT.KD.exe.config");
                        //Properties.Settings.Default.Save();
                        CultureInfo culture = new CultureInfo("en-US");
                        this.InitCulture("en-US", "Company.Interface.LanguageResource.Language");
                        Form[] forms = this.MdiChildren;
                        for (int i = 0; i < forms.Length; i++)
                        {
                            ((BaseForm)forms[i]).InitCulture("en-US", "Company.Interface.LanguageResource.Language");
                        }

                        if (ConfigurationManager.AppSettings["NgonNgu"] == "0")
                        {
                            this.BackgroundImage = System.Drawing.Image.FromFile("ecs-kinhdoanh.png");
                        }
                        this.BackgroundImageLayout = ImageLayout.Stretch;

                    }
                    break;
                case "cmdHelp":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\Help.chm");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                    }
                    break;
                case "cmdHDSDVNACCS":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\VNACCS_TaiLieuHuongDanSuDung.pdf");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                    }
                    break;
                case "cmdHelpVideo":
                    try
                    {
                        this.Cursor = Cursors.WaitCursor;
                        FrmHelpVideo frm = new FrmHelpVideo();
                        frm.Show();
                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        this.Cursor = Cursors.Default;
                    }

                    break;
                case "cmdBieuThueXNK2018":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\BIEU THUE XNK2018 - Update.xlsx");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                    }
                    break;  
                case "cmdMaHS":
                    ShowMaHSForm();
                    break;
                //Cảng trong nước
                case "cmdBerth":
                    ShowBerthForm();
                    break;
                //Địa điểm lưu kho hàng chờ thông quan
                case "cmdCargo":
                    ShowCargoForm();
                    break;
                //Địa điểm xếp hàng 
                case "cmdCityUNLOCODE":
                    ShowCityUNLOCODEForm();
                    break;
                //Tổng hợp danh mục
                case "cmdCommon":
                    ShowCommonForm();
                    break;
                //Container size
                case "cmdContainerSize":
                    ShowContainerSizeForm();
                    break;
                // Đội thủ tục HQ
                case "cmdCustomsSubSection":
                    ShowCustomsSubSectionForm();
                    break;
                // Mã kiểm dịch động vật
                case "cmdOGAUser":
                    ShowOGAUserForm();
                    break;
                //ĐVT Lượng kiện
                case "cmdPackagesUnit":
                    ShowPackagesUnitForm();
                    break;
                // Kho ngoại quan
                case "cmdStations":
                    ShowStationsForm();
                    break;
                // Biểu thuế XNK
                case "cmdTaxClassificationCode":
                    ShowTaxClassificationCodeForm();
                    break;
                case "cmdHaiQuan":
                    ShowDVHQForm();
                    break;
                case "cmdNuoc":
                    ShowNuocForm();
                    break;
                case "cmdNguyenTe":
                    ShowNguyenTeForm();
                    break;
                case "cmdPTTT":
                    ShowPTTTForm();
                    break;
                case "cmdPTVT":
                    ShowPTVTForm();
                    break;
                case "cmdDKGH":
                    ShowDKGHForm();
                    break;
                case "cmdDVT":
                    ShowDVTForm();
                    break;
                case "cmdCuaKhau":
                    ShowCuaKhauForm();
                    break;
                case "cmdRestore":
                    ShowRestoreForm();
                    break;
                case "cmdBackUp":
                    ShowBackupAndRestore(true);
                    break;
                case "ThongSoKetNoi":
                    ShowThietLapThongSoKetNoi();
                    break;
                case "TLThongTinDNHQ":
                    ShowThietLapThongTinDNAndHQ();
                    break;
                case "cmdThietLapIn":
                    ShowThietLapInBaoCao();
                    break;
                case "cmdCauHinhToKhai":
                    ShowCauHinhToKhai();
                    break;
                case "QuanLyNguoiDung":
                    ShowQuanLyNguoiDung();
                    break;
                case "QuanLyNhom":
                    ShowQuanLyNhomNguoiDung();
                    break;
                case "LoginUser":
                    LoginUserKhac();
                    break;
                case "cmdChangePass":
                    ChangePassForm();
                    break;
                case "MaHS":
                    TraCuuMaHSForm();
                    break;
                case "DonViDoiTac":
                    DonViDoiTacForm();
                    break;
                case "cmdAutoUpdate":
                    AutoUpdate("MSG");
                    break;
                case "cmdXuatToKhaiDauTu":
                    XuatToKhaiChoPhongKhai();
                    break;
                case "cmdXuatToKhaiKinhDoanh":
                    XuatToKhaiChoPhongKhai();
                    break;
                case "cmdAbout":
                    this.dispInfo();
                    break;
                case "cmdActivate":
                    this.dispActivate();
                    break;
                case "cmdCloseMe":
                    this.doCloseMe();
                    break;
                case "cmdCloseAllButMe":
                    this.doCloseAllButMe();
                    break;
                case "cmdCloseAll":
                    this.doCloseAll();
                    break;
                case "cmdNhapToKhaiDauTu":
                    NhapToKhaiTuDoanhNghiep();
                    break;

                case "cmdNhapToKhaiKinhDoanh":
                    NhapToKhaiTuDoanhNghiep();
                    break;
                case "QuanLyMess":
                    WSForm2 wsform2 = new WSForm2();
                    wsform2.ShowDialog(this);
                    if (WSForm2.IsSuccess == true)
                    {
                        ShowQuanLyMess();
                    }
                    break;
                case "cmdThietLapCHDN":
                    ShowThietLapform();
                    break;
                case "cmdLog":
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\Error.log");
                    break;
                case "cmdTimer":
                    new FrmCauHinhThoiGian().ShowDialog(this);
                    break;
                //phi
                case "cmdDaiLy":
                    ShowDaiLy();
                    break;
                case"cmdThongBaoVNACCS":
                    FrmThongBaoVNACCS frmVNACCS = new FrmThongBaoVNACCS();
                    frmVNACCS.Show();
                    break;
                case "cmdSignFile":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\PDFSignedDigitalSetup.msi");
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                case "cmdHelpSignFile":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\HelpSignFile.pdf");
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                    // Đồng bộ dữ liệu
                case "cmdSyncData":
                    ShowDongBoDuLieuForm();
                    break;
                case "cmdUpdateCategoryOnline":
                    UpdateCategoryOnline();
                    break;
                case "cmdFeedbackAll":
                    FeedBackAll();
                    break;
                case "cmdImportExcelTKMDVNACCS":
                    ImportExcelTKMDVNACCS();
                    break;
                case "cmdPerformanceDatabase":
                    PerformanceDatabase();
                    break;                    
            }

            //TODO: HUNGTQ updated 02/08/2012.
            cmdQuerySQL(e);

            //TODO: HUNGTQ updated 16/08/2012.
            cmdBoSungCommand(e);
        }

        private void PerformanceDatabase()
        {
            PerformanceDatabaseForm f = new PerformanceDatabaseForm();
            f.Show(this);
        }

        private void ImportExcelTKMDVNACCS()
        {
            try
            {
                ReadExcelTKMDFormVNACCS f = new ReadExcelTKMDFormVNACCS();
                f.Show(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void FeedBackAll()
        {
            //frmTrangThaiPhanHoi.Show();
            var mainForm = Application.OpenForms["MainForm"] as MainForm;
            //mainForm.ShowFormThongBao(null, null);
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { mainForm.frmThongBao.Show(); }));

            }
            else
                mainForm.frmThongBao.Show();
        }
        private void UpdateCategoryOnline()
        {
            try
            {
                ProcessUpdateCategoryOnline f = new ProcessUpdateCategoryOnline();
                f.Show(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ShowTaxClassificationCodeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_TaxClassificationCode"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_TaxClassificationCode f = new Company.Interface.VNACCS.Category.VNACC_Category_TaxClassificationCode();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowStationsForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Stations"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Stations f = new Company.Interface.VNACCS.Category.VNACC_Category_Stations();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowPackagesUnitForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_PackagesUnit"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_PackagesUnit f = new Company.Interface.VNACCS.Category.VNACC_Category_PackagesUnit();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowOGAUserForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_OGAUser"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_OGAUser f = new Company.Interface.VNACCS.Category.VNACC_Category_OGAUser();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCustomsSubSectionForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CustomsSubSection"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CustomsSubSection f = new Company.Interface.VNACCS.Category.VNACC_Category_CustomsSubSection();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowContainerSizeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_ContainerSize"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_ContainerSize f = new Company.Interface.VNACCS.Category.VNACC_Category_ContainerSize();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCommonForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Common"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Common f = new Company.Interface.VNACCS.Category.VNACC_Category_Common();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCityUNLOCODEForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CityUNLOCODE"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CityUNLOCODE f = new Company.Interface.VNACCS.Category.VNACC_Category_CityUNLOCODE();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCargoForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Cargo"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Cargo f = new Company.Interface.VNACCS.Category.VNACC_Category_Cargo();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowBerthForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Berth"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Berth f = new Company.Interface.VNACCS.Category.VNACC_Category_Berth();
            f.MdiParent = this;
            f.Show();
        }
        private void HDSD_CKS()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Company.KDT.SHARE.Components.Globals.HDSD_CKS();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        #region Query SQL

        //TODO: HUNGTQ updated 02/08/2012.

        /// <summary>
        ///  Bo sung dong lenh: CreateQuerySQLCommand(); vao ham khoi dung MainForm()
        /// </summary>
        private void CreateQuerySQLCommand()
        {
            try
            {
                Janus.Windows.UI.CommandBars.UICommand mnuFormSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuFormSQL");
                //Janus.Windows.UI.CommandBars.UICommand mnuMiniSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuMiniSQL");
                // 
                // mnuFormSQL
                // 
                mnuFormSQL.ImageIndex = 42;
                mnuFormSQL.Key = "mnuFormSQL";
                mnuFormSQL.Name = "mnuFormSQL";
                mnuFormSQL.Text = "Form SQL";
                // 
                // mnuMiniSQL
                // 
                //mnuMiniSQL.ImageIndex = 42;
                //mnuMiniSQL.Key = "mnuMiniSQL";
                //mnuMiniSQL.Name = "mnuMiniSQL";
                //mnuMiniSQL.Text = "MiniSQL";

                // 
                // mnuQuerySQL
                // 
                cmdQuery.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                    mnuFormSQL});
                //mnuMiniSQL});
                cmdQuery.ImageIndex = 42;
                cmdQuery.Key = "cmdQuery";
                cmdQuery.Name = "cmdQuery";
                cmdQuery.Text = "Thực hiện truy vấn SQL";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage(ex.Message, false);
            }
        }

        /// <summary>
        /// Bo sung dong lenh: cmdQuerySQL(e); vao cuoi dong cua ham cmMain_CommandClick(...)
        /// </summary>
        /// <param name="e"></param>
        private void cmdQuerySQL(Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "mnuFormSQL":
                    FormSQL();
                    break;
                case "mnuMiniSQL":
                    MiniSQL();
                    break;
            }
        }

        private void FormSQL()
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("FrmQuerySQLUpdate"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                frmQuery = new FrmQuerySQLUpdate();
                frmQuery.MdiParent = this;
                frmQuery.Show();
            }
        }

        private void MiniSQL()
        {
            //WSForm2 login2 = new WSForm2();
            //login2.ShowDialog(this);
            //if (WSForm2.IsSuccess == true)
            //{
            //    string fileName = Application.StartupPath + "\\MiniSQL\\MiniSqlQuery.exe";
            //    if (System.IO.File.Exists(fileName))
            //    {

            //        MiniSqlQuery.Core.DbConnectionDefinition conn = new MiniSqlQuery.Core.DbConnectionDefinition();
            //        conn.ConnectionString = "Data Source=" + GlobalSettings.SERVER_NAME + ";Initial Catalog=" + GlobalSettings.DATABASE_NAME +
            //                                ";User ID=" + GlobalSettings.USER + ";Password=" + GlobalSettings.PASS;
            //        conn.Name = GlobalSettings.DATABASE_NAME;
            //        System.Diagnostics.Process.Start(fileName);
            //    }
            //}
        }

        #endregion

        #region Bo sung command

        //TODO: HUNGTQ updated 16/08/2012.

        /// <summary>
        /// Bo sung dong lenh: cmdBoSungCommand(e); vao cuoi dong cua ham cmMain_CommandClick(...)
        /// </summary>
        /// <param name="e"></param>
        private void cmdBoSungCommand(Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.OpenCommandBieuThue(e.Command.Key);

                if (e.Command.Key == "cmdGetCategoryOnline")
                {
                    string msg = Company.KDT.SHARE.Components.Globals.UpdateCategoryOnline();

                    if (msg != "")
                    {
                        if (msg.Contains("E|"))
                            ShowMessage("Có lỗi trong quá trình thực hiện cập nhật danh mục trực tuyến:\r\n" + msg.Replace("E|", ""), true, true, "");
                        else
                            ShowMessage(msg, false);
                    }
                    else
                        ShowMessage("Không có bản ghi nào được cập nhật mới. Thông tin danh mục trên hệ thống hiện tại là mới nhất.", false);
                }
                else if (e.Command.Key == "cmdGopY")
                {
                }
                else if (e.Command.Key == "cmdTeamview")
                {
                    string path = Application.StartupPath + "\\Support\\TeamViewerQS_vi-ckq.exe";

                    System.Diagnostics.Process.Start(path);
                }
                else if (e.Command.Key == "cmdCapNhatHS8Auto")
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Bạn có chắc chắn muốn chuyển mã HS danh mục Nguyên phụ liệu, Sản phẩm thành 8 số không?");
                    sb.AppendLine();
                    sb.AppendLine("Quy tắc chuyển mã:");
                    sb.AppendLine("   - Chương trình sẽ tự động cắt bỏ 2 số '00' bên phải của mã HS, chỉ lấy 8 số.");
                    sb.AppendLine();
                    sb.AppendLine("Vì vậy mã HS sau chuyển đổi có thể không đúng với danh mục chuẩn mã HS 8 số, bạn phải kiểm tra lại danh mục mã HS vừa chuyển đổi.");

                    if (ShowMessageTQDT("Chuyên đổi mã Biểu thuế (HS)", sb.ToString(), true) == "Yes")
                    {
                        if (Company.KDT.SHARE.Components.Globals.ConvertHS8Auto(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "KD") == true)
                        {
                            ShowMessage("Đã thực hiện thành công.", false);
                        }
                    }
                }
                else if (e.Command.Key == "cmdCapNhatHS8SoManual")
                {
                    HS08SO.ChuyenMaHS08So f = new Company.Interface.HS08SO.ChuyenMaHS08So();
                    f.ShowDialog();
                }
                else if (e.Command.Key == "cmdImageResize")
                {
                    Cursor = Cursors.WaitCursor;

                    string msg = Company.KDT.SHARE.Components.Globals.DownloadToolImageResize();

                    if (msg.Length != 0)
                        ShowMessage(msg, false);
                }
                else if (e.Command.Key == "cmdImageResizeHelp")
                {
                    Cursor = Cursors.WaitCursor;

                    string msg = Company.KDT.SHARE.Components.Globals.HDSDImageResize();

                    if (msg.Length != 0)
                        ShowMessage(msg, false);
                }
                else if (e.Command.Key == "cmdUpdateDatabase")
                {
                    UpdateNewDataVersion();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }

        private void UpdateNewDataVersion()
        {
            try
            {
                DialogResult result = Helper.Controls.UpdateDatabaseSQLForm.Instance(true).ShowDialog();

                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdDataVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != "" ? dataVersion : "?"));

                //TODO: Reload lai du lieu sau khi Nang cap du lieu moi. Hungtq, 21/11/2013.
                if (result != DialogResult.No && result != DialogResult.None && result != DialogResult.Cancel)
                    Company.KDT.SHARE.VNACCS.Controls.ucBase.InitialData(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
        }

        #endregion

        private void ChuKySo()
        {
            FrmCauHinhChuKySo cks = new FrmCauHinhChuKySo();
            cks.MaDoanhNghiep = "";
            cks.Show(this);

            //TODO: Hungtq updated, 7/2/2013. ChuKySo(): Reload sau khi dong form cau hinh CKS
            khoitao_GiaTriMacDinh();
        }

        private void ShowThietLapform()
        {
            //Classes.clsBien.boolShow = true;
            DangKyForm dangkyForm = new DangKyForm();
            dangkyForm.ShowDialog(this);

        }
        private void ShowQuanLyMess()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyMessage"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            quanlyMess = new QuanLyMessage();
            quanlyMess.MdiParent = this;
            quanlyMess.Show();
        }
        private void NhapToKhaiTuDoanhNghiep()
        {
            ToKhaiMauDichCollection TKMDCollection = new ToKhaiMauDichCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKMDCollection = (ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();

                    if (TKMDCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiMauDich TKMD in TKMDCollection)
                        {
                            try
                            {
                                TKMD.InsertFull();
                                i++;
                            }
                            catch { }
                        }
                        if (i > 0)
                            ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }
        private void dispActivate()
        {
            ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
            obj.Show();
        }

        private void dispInfo()
        {
            ProdInfo.frmInfo obj = new Company.Interface.ProdInfo.frmInfo();
            obj.ShowDialog(this);
        }

        private void XuatToKhaiChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ToKhaiMauDichManageForm f = new ToKhaiMauDichManageForm();

            f.MdiParent = this;
            f.Show();
        }
        //private void AutoUpdate()
        //{
        //    if (ShowMessage(setText("Bạn có muốn cập nhật chương trình mới nhất không?", "Do you want to update new licence"), true) == "Yes")
        //    {
        //        System.Diagnostics.Process.Start(Application.StartupPath + "\\AutoUpdate\\ECS_AutoUpdate.exe");
        //        Application.Exit();
        //    }

        //}
        private void DonViDoiTacForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DonViDoiTacForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dvdtForm = new DonViDoiTacForm();
            dvdtForm.MdiParent = this;
            dvdtForm.Show();
        }
        private void TraCuuMaHSForm()
        {
            //HaiQuan.HS.HSMainForm f = new HaiQuan.HS.HSMainForm();
            //f.Show();
            ShowMaHSForm();
        }
        private void ChangePassForm()
        {
            ChangePassForm f = new ChangePassForm();
            f.ShowDialog(this);
        }
        private void LoginUserKhac()
        {
            this.Hide();
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
            MainForm.EcsQuanTri = null;
            isLoginSuccess = false;
            if (versionHD == 0)
            {
                Login login = new Login();
                login.ShowDialog(this);
            }
            else if (versionHD == 1)
            {
                Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                login.ShowDialog(this);
            }
            else if (versionHD == 2)
            {
                Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                login.ShowDialog(this);
            }
            if (isLoginSuccess)
            {
                //statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = "Người dùng: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                this.Show();
                if (versionHD == 0)
                {
                    if (MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    else
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)))
                    {
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else if (versionHD == 1)
                {
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                }
                else if (versionHD == 2)
                {
                    switch (typeLogin)
                    {
                        case 1: // User Da Cau Hinh
                            this.Show();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 2: // User Chua Cau Hinh
                            this.Show();
                            DangKyForm dangKyForm = new DangKyForm();
                            dangKyForm.ShowDialog(this);
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 3:// Admin
                            this.Hide();
                            CreatAccountForm fAdminForm = new CreatAccountForm();
                            fAdminForm.ShowDialog(this);
                            this.ShowLoginForm();
                            break;
                    }
                }
                khoitao_GiaTriMacDinh();
            }
            else
                Application.Exit();
        }
        private void ShowQuanLyNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            NguoiDung = new QuanLyNguoiDung();
            NguoiDung.MdiParent = this;
            NguoiDung.Show();
        }
        private void ShowQuanLyNhomNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNhomNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            NhomNguoiDung = new QuanLyNhomNguoiDung();
            NhomNguoiDung.MdiParent = this;
            NhomNguoiDung.Show();
        }
        private void ShowCauHinhToKhai()
        {
            CauHinhToKhaiForm f = new CauHinhToKhaiForm();
            f.ShowDialog(this);
        }

        private void ShowThietLapInBaoCao()
        {
            CauHinhInForm f = new CauHinhInForm();
            f.ShowDialog(this);
        }
        private void ShowRestoreForm()
        {
            //RestoreForm f = new RestoreForm();
            //f.ShowDialog(this);
        }
        private void ShowThietLapThongTinDNAndHQ()
        {

            CultureInfo cultureCurrent = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            CultureInfo cultureVN = new CultureInfo("vi-VN");
            ThongTinDNAndHQForm f = new ThongTinDNAndHQForm();
            f.ShowDialog(this);
            khoitao_GiaTriMacDinh();
            if (!cultureCurrent.Equals(Thread.CurrentThread.CurrentCulture) && Thread.CurrentThread.CurrentCulture.Equals(cultureVN))
            {
                Application.Restart();
            }
            else if (!cultureCurrent.Equals(Thread.CurrentThread.CurrentCulture))
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    ((BaseForm)forms[i]).InitCulture(Thread.CurrentThread.CurrentCulture.Name, "Company.Interface.LanguageResource.ResourceEN");
                }
                this.InitCulture(Thread.CurrentThread.CurrentCulture.Name, "Company.Interface.LanguageResource.ResourceEN");
            }
        }
        private void ShowThietLapThongSoKetNoi()
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog(this);

            //Test webservice
            string msgError = "";
            bool webServiceConnection = Company.KDT.SHARE.Components.Globals.ServiceExists(GlobalSettings.DiaChiWS, false, out msgError);
            if (webServiceConnection)
                statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Blue;
            else
                statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Red;

        }
        private void ShowBackupAndRestore(bool isBackUp)
        {
            BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            f.isBackUp = isBackUp;
            f.ShowDialog(this);
        }
        private void ShowCuaKhauForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_BorderGate"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_BorderGate f = new Company.Interface.VNACCS.Category.VNACC_Category_BorderGate();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("CuaKhauForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //ckForm = new CuaKhauForm();
            //ckForm.MdiParent = this;
            //ckForm.Show();
        }

        private void ShowNhomCuaKhauForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NhomCuaKhau"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            nhomCuaKhauForm = new NhomCuaKhauForm();
            nhomCuaKhauForm.MdiParent = this;
            nhomCuaKhauForm.Show();
        }

        private void ShowDVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_QuantityUnit"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_QuantityUnit f = new Company.Interface.VNACCS.Category.VNACC_Category_QuantityUnit();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("DonViTinhForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //dvtForm = new DonViTinhForm();
            //dvtForm.MdiParent = this;
            //dvtForm.Show();
        }

        private void ShowDKGHForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DKGHForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dkghForm = new DKGHForm();
            dkghForm.MdiParent = this;
            dkghForm.Show();
        }
        private void ShowPTVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_TransportMeans"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_TransportMeans f = new Company.Interface.VNACCS.Category.VNACC_Category_TransportMeans();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("PTVTForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //ptvtForm = new PTVTForm();
            //ptvtForm.MdiParent = this;
            //ptvtForm.Show();
        }
        private void ShowPTTTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PTTTForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ptttForm = new PTTTForm();
            ptttForm.MdiParent = this;
            ptttForm.Show();
        }

        private void ShowNguyenTeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CurrencyExchange"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CurrencyExchange f = new Company.Interface.VNACCS.Category.VNACC_Category_CurrencyExchange();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("NguyenTeForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //ntForm = new NguyenTeForm();
            //ntForm.MdiParent = this;
            //ntForm.Show();
        }

        private void ShowNuocForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NuocForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            nuocForm = new NuocForm();
            nuocForm.MdiParent = this;
            nuocForm.Show();
        }

        private void ShowDVHQForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CustomsOffice"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CustomsOffice f = new Company.Interface.VNACCS.Category.VNACC_Category_CustomsOffice();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("DonViHaiQuanForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //dvhqForm = new DonViHaiQuanForm();
            //dvhqForm.MdiParent = this;
            //dvhqForm.Show();
        }

        private void ShowMaHSForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_HSCode"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_HSCode f = new Company.Interface.VNACCS.Category.VNACC_Category_HSCode();
            f.MdiParent = this;
            f.Show();
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("MaHSForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //maHSForm = new MaHSForm();
            //maHSForm.MdiParent = this;
            //maHSForm.Show();
        }
        private void ShowDongBoDuLieuForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("FrmDongBoDuLieu_VNACCS"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DBDLForm_VNACCS = new FrmDongBoDuLieu_VNACCS();
            DBDLForm_VNACCS.MdiParent = this;
            DBDLForm_VNACCS.Show();
        }
        private void DongBoDuLieuDN()
        {

        }
        private void UpdateStyleForAllForm(string style)
        {
            this.vsmMain.DefaultColorScheme = style;
            queueForm.vsmMain.DefaultColorScheme = style;

            if (tkmdForm != null)
                tkmdForm.vsmMain.DefaultColorScheme = style;
            if (tkmdManageForm != null)
                tkmdManageForm.vsmMain.DefaultColorScheme = style;
            if (tkmdRegisterForm != null)
                tkmdRegisterForm.vsmMain.DefaultColorScheme = style;
            if (maHSForm != null)
                maHSForm.vsmMain.DefaultColorScheme = style;
            if (ptttForm != null)
                ptttForm.vsmMain.DefaultColorScheme = style;
            if (ptvtForm != null)
                ptvtForm.vsmMain.DefaultColorScheme = style;
            if (dvtForm != null)
                dvtForm.vsmMain.DefaultColorScheme = style;
            if (dvhqForm != null)
                dvhqForm.vsmMain.DefaultColorScheme = style;
            if (ckForm != null)
                ckForm.vsmMain.DefaultColorScheme = style;
            if (ntForm != null)
                ntForm.vsmMain.DefaultColorScheme = style;
            if (dkghForm != null)
                dkghForm.vsmMain.DefaultColorScheme = style;
            if (nuocForm != null)
                nuocForm.vsmMain.DefaultColorScheme = style;

        }
        private void cmbMenu_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void pmMain_MdiTabMouseDown(object sender, Janus.Windows.UI.Dock.MdiTabMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                e.Tab.Form.Activate();
                if (this.MdiChildren.Length == 1)
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                mnuRightClick.Show(this, e.X + pmMain.MdiTabGroups[0].Location.X, e.Y + 6);
            }
        }

        private void doCloseMe()
        {
            Form form = pmMain.MdiTabGroups[0].SelectedTab.Form;
            form.Close();
        }

        private void doCloseAllButMe()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i] != pmMain.MdiTabGroups[0].SelectedTab.Form)
                {
                    forms[i].Close();
                }
            }
        }

        private void doCloseAll()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
        }

        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();


        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdDataVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != "" ? dataVersion : "?"));

                long sotk = ToKhaiMauDich.SelectCountSoTK(GlobalSettings.MA_DON_VI);
                if ((sotk % 2 == 0 && sotk > 0) || (sotk % 10 == 0 && sotk >= 10))
                {
                    string loaihinh = "ECSKD";
                    if (versionHD == 1)
                        loaihinh = "ECSDLKD";
                    if (versionHD != 2)
                        WebServiceConnection.sendThongTinDN(GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, "SoTK=" + sotk, loaihinh);
                }
                string error = KeySecurity.Active.Install.ECS_updateInfo(UpdateInfo(sotk.ToString()));
                if (!string.IsNullOrEmpty(error))
                {
                    Logger.LocalLogger.Instance().WriteMessage("Lỗi Update thông tin ECS ", new Exception(error));
                }

                //Hungtq updated 23/05/2013.
                Company.KDT.SHARE.Components.Globals.KhoiTao_DanhMucChuanHQ_MaHS();

                //Test webservice
                string msgError = "";
                bool webServiceConnection = Company.KDT.SHARE.Components.Globals.ServiceExists(GlobalSettings.DiaChiWS, false, out msgError);
                if (webServiceConnection)
                    statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Blue;
                else
                    statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Red;


                //Cap nhat phan mem
                AutoUpdate(string.Empty);

                if (!e.Cancel)
                {
                    //Hungtq complemented 14/12/2010
                    if (Company.KDT.SHARE.Components.Globals.GetTyGia() == true)
                    {
                        //timer1.Enabled = false;
                        MainForm.flag = 1;
                    }
                    else
                    {
                        MainForm.flag = 0;
                        soLanLayTyGia = 0;
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //private void ShowDaiLy()
        //{
        //    FrmDaiLy f = new FrmDaiLy();
        //    f.ShowDialog(this);
        //}
        private void ShowDaiLy()
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("FrmDaiLy"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                frmDaiLy = new FrmDaiLy();
                frmDaiLy.MdiParent = this;
                frmDaiLy.Show();
            }
        }

        #region AutoUpdate ONLINE
        private void CreateShorcutAutoUpdate()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + "AUTO UPDATE " + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = AppDomain.CurrentDomain.BaseDirectory + "\\AppAutoUpdate.exe";
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
        private void CreateShorcut()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
        private void AutoUpdate(string args)
        {
            try
            {
#if KD_V4       
                CreateShorcutAutoUpdate();
                using (System.Data.SqlClient.SqlConnection cnn = new System.Data.SqlClient.SqlConnection())
                {
                    cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS });
                    cnn.Open();

                    System.Data.SqlClient.SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand();
                    sqlCmd.Connection = cnn;

                    string cmdText = "DELETE  FROM dbo.t_HaiQuan_Version WHERE Version IN ('30.9','31.0','31.1','31.2','31.3','31.4','31.5','31.6','31.7','31.8','31.9','32.0','32.1','32.2','32.3')";
                    sqlCmd.CommandText = cmdText;
                    sqlCmd.ExecuteNonQuery();
                    cnn.Close();
                }
                    DirectoryInfo dInfo = new DirectoryInfo(System.Windows.Forms.Application.StartupPath);
                    System.Security.AccessControl.DirectorySecurity dSecurity = dInfo.GetAccessControl();
                    dSecurity.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule("everyone", System.Security.AccessControl.FileSystemRights.FullControl, System.Security.AccessControl.InheritanceFlags.ObjectInherit | System.Security.AccessControl.InheritanceFlags.ContainerInherit, System.Security.AccessControl.PropagationFlags.InheritOnly, System.Security.AccessControl.AccessControlType.Allow));
                    dInfo.SetAccessControl(dSecurity);
                    List<string> fileNames = new List<string>();
                    fileNames.Add("ECS_TQDT_KD_V30.9.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.0.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.1.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.2.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.3.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.4.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.5.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.6.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.7.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.8.sql");
                    fileNames.Add("ECS_TQDT_KD_V31.9.sql");
                    fileNames.Add("ECS_TQDT_KD_V32.0.sql");
                    fileNames.Add("ECS_TQDT_KD_V32.1.sql");
                    fileNames.Add("ECS_TQDT_KD_V32.2.sql");
                    fileNames.Add("ECS_TQDT_KD_V32.3.sql");
                    foreach (var item in fileNames)
                    {
                        string path = System.Windows.Forms.Application.StartupPath + "\\DATA\\Update\\" + item;
                        try
                        {
                            System.IO.File.Delete(path);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }

#endif
                //Kiem tra phien ban du lieu truoc khi cap nhat phien ban chuong trinh moi
                if (Helper.Controls.UpdateDatabaseSQLForm.Instance(true).IsHasNewDataVersion)
                {
                    UpdateNewDataVersion();
                }

                Company.KDT.SHARE.Components.DownloadUpdate dl = new DownloadUpdate(args);
                dl.DoDownload();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #region TẠO COMMAND TOOLBAR

        private delegate void CreateCommandCallback(string lastestVersion);
        private void CreateCommand(string lastestVersion)
        {
            try
            {
                this.cmbMenu.CommandManager = this.cmMain;
                this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                    this.cmdNewVersion
                });

                if (!this.cmMain.Commands.Contains("cmdNewVersion"))
                    this.cmMain.Commands.Add(this.cmdNewVersion);

                // 
                // cmdNewVersion
                // 
                this.cmdNewVersion.Key = "cmdNewVersion";
                this.cmdNewVersion.Name = "cmdNewVersion";
                this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;
                this.cmdNewVersion.TextAlignment = Janus.Windows.UI.CommandBars.ContentAlignment.MiddleCenter;
                this.cmdNewVersion.TextImageRelation = Janus.Windows.UI.CommandBars.TextImageRelation.ImageBeforeText;
                this.cmdNewVersion.ImageReplaceableColor = System.Drawing.Color.Pink;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //HUNGTQ, Update 11/06/2010.

        private Janus.Windows.UI.CommandBars.UICommand cmdNewVersion;

        /// <summary>
        /// Tạo command chứng từ đính kèm bổ sung.
        /// </summary>
        private void CreateCommandBosung(string lastestVersion)
        {
            #region Initialize
            //Tao nut command tren toolbar.
            this.cmdNewVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdNewVersion");

            //this.cmbMenu.CommandManager = this.cmMain;
            //this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            //    this.cmdNewVersion
            //});

            //if (!this.cmMain.Commands.Contains("cmdNewVersion"))
            //    this.cmMain.Commands.Add(this.cmdNewVersion);

            //// 
            //// cmdNewVersion
            //// 
            //this.cmdNewVersion.Key = "cmdNewVersion";
            //this.cmdNewVersion.Name = "cmdNewVersion";
            //this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;

            // Invoke the method that updates the form's label
            this.Invoke(new CreateCommandCallback(this.CreateCommand), new object[] { lastestVersion });

            #endregion

            cmdNewVersion.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdNewVersion_Click);

        }

        private void cmdNewVersion_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

            switch (e.Command.Key)
            {
                case "cmdNewVersion":
                    AutoUpdate("MSG");
                    break;
            }

        }

        #endregion

        #endregion

        #region Begin ECS Express

        private void Express()
        {
            try
            {
                string val = KeySecurity.KeyCode.Decrypt(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Ep"));

                if (!string.IsNullOrEmpty(val) && bool.Parse(val) == true)
                {
                    SetExpress(false);
                }
                else
                    SetExpress(true);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void SetExpress(bool visible)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Janus.Windows.UI.InheritableBoolean status = visible == true ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

                /*Menu He thong*/
                //cmdThietLapCHDN.Visible = status;           //Thiet lap thong tin doanh nghiep
                cmdNhapXuat.Visible = status;
                cmdXuat.Visible = status; //Xuat du lieu cho phong khai
                cmdNhap.Visible = status;                 //Nhap du lieu doanh nghiep
                cmdRestore.Visible = status;                //Phuc hoi du lieu

                /*Menu Giao dien*/
                cmdEng.Visible = status;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        #endregion End ECS Express

        #region Cập nhật thông tin

        private string UpdateInfo(string soTK)
        {

            Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer info = new Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer();
            #region Tạo thông tin update
            if (GlobalSettings.IsDaiLy)
            {
                string ListDoanhNghiep = Company.KDT.SHARE.Components.HeThongPhongKhai.GetDoanhNghiep();
                ThongTinDaiLy DLinfo = new ThongTinDaiLy();
                DLinfo = Company.KDT.SHARE.Components.HeThongPhongKhai.GetThongTinDL();
                info.Id = DLinfo.MaDL;
                info.Name = DLinfo.TenDL;
                info.Address = DLinfo.DiaChiDL;
                info.Phone = DLinfo.SoDienThoaiDL;
                info.Contact_Person = DLinfo.NguoiLienHeDL;
                info.Email = DLinfo.EmailDL;
                info.Id_Customs = GlobalSettings.MA_HAI_QUAN;
                info.IP_Customs = GlobalSettings.DiaChiWS;
                info.ServerName = GlobalSettings.SERVER_NAME;
                info.Data_Name = GlobalSettings.DATABASE_NAME;
                info.Data_User = GlobalSettings.USER;
                info.Data_Pass = GlobalSettings.PASS;
                info.Data_Version = Company.KDT.SHARE.Components.Version.GetVersion();
                info.App_Version = Application.ProductVersion;
                info.LastCheck = DateTime.Now;
                info.temp1 = ListDoanhNghiep;
                info.MachineCode = KeySecurity.KeyCode.ProcessInfo();
                info.Product_ID = "ECS_TQDT_KD_V3";
                info.RecordCount = soTK;
            }
            else
            {
                string SignRemote = string.Empty; // Thông tin của doanh nghiệp sử dụng chữ kí số
                if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
                {
                    SignRemote = "Sữ dụng chữ kí số online";
                    SignRemote += ". User: " + Company.KDT.SHARE.Components.Globals.UserNameSignRemote;
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                {
                    SignRemote = "Sử dụng chũ kí số qua mạng nội bộ";
                    SignRemote += ". Database chữ kí số: " + Company.KDT.SHARE.Components.Globals.DataSignLan;
                }
                info.Id = GlobalSettings.MA_DON_VI;
                info.Name = GlobalSettings.TEN_DON_VI;
                info.Address = GlobalSettings.DIA_CHI;
                info.Phone = GlobalSettings.SoDienThoaiDN;
                info.Contact_Person = GlobalSettings.NguoiLienHe;
                info.Email = GlobalSettings.MailDoanhNghiep;
                info.Id_Customs = GlobalSettings.MA_HAI_QUAN;
                info.IP_Customs = GlobalSettings.DiaChiWS;
                info.ServerName = GlobalSettings.SERVER_NAME;
                info.Data_Name = GlobalSettings.DATABASE_NAME;
                info.Data_User = GlobalSettings.USER;
                info.Data_Pass = GlobalSettings.PASS;
                info.Data_Version = Company.KDT.SHARE.Components.Version.GetVersion();
                info.App_Version = Application.ProductVersion;
                info.LastCheck = DateTime.Now;
                info.temp1 = SignRemote;
                info.MachineCode = KeySecurity.KeyCode.ProcessInfo();
                info.Product_ID = "ECS_TQDT_KD_V3";
                info.RecordCount = soTK;
            }
            #endregion
            return Helpers.Serializer(info);
        }
        #endregion

        private void statusBar_PanelClick(object sender, Janus.Windows.UI.StatusBar.StatusBarEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                switch (e.Panel.Key)
                {
                    case "DoanhNghiep":
                    case "HaiQuan":
                    case "Terminal":
                    case "Service":
                    case "Version":
                        ShowThietLapThongTinDNAndHQ();
                        break;
                    case "CKS":
                        ChuKySo();
                        break;
                    case "Support":
                        Support();
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { this.Cursor = Cursors.Default; }

        }

        private void Support()
        {
            frmSendSupport f = new frmSendSupport();
            f.ShowDialog(this);
        }

        private void statusBar_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void statusBar_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void LoadThongBao()
        {

        }

        public void ShowFormThongBao(object sender, EventArgs e)
        {
            if (!frmThongBao.Visible)
            {
                frmThongBao.TopMost = true;
                frmThongBao.BringToFront();
                frmThongBao.Visible = true;
            }
        }

        private void uiPanel2_MouseLeave(object sender, EventArgs e)
        {

        }

        private void explorerBarTQDT_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //hang hoa
                case "HangHoaNhap":
                    this.show_NguyenPhuLieuRegistedForm();
                    break;

                // SP đã đăng ký.
                case "HangHoaXuat":
                    this.show_SanPhamRegistedForm();
                    break;

                // Tờ khai mậu dịch kinh doanh.
                case "tkNhapKhau":
                    this.show_ToKhaiMauDichForm("NKD");
                    break;
                case "tkXuatKhau":
                    this.show_ToKhaiMauDichForm("XKD");
                    break;

                case "TheoDoiTKSXXK":
                    show_ToKhaiSXXKManage("KD");
                    break;
                case "ToKhaiSXXKDangKy":
                    show_ToKhaiSXXKDangKy("KD");
                    break;

                // Tờ khai mậu dịch đầu tư
                case "tkNhapDT":
                    this.show_ToKhaiMauDichForm("NDT");
                    break;
                case "tkXuatDT":
                    this.show_ToKhaiMauDichForm("XDT");
                    break;

                case "tkTheoDoiDT":
                    show_ToKhaiSXXKManage("DT");
                    break;
                case "tkDTDangKy":
                    show_ToKhaiSXXKDangKy("DT");
                    break;
                case "ThongKeTK":
                    show_ThongKeTK();
                    break;
                case "TriGiaXK":
                    show_TriGiaXK();
                    break;
                case "ThongKeKimNgachNuoc_MatHang":
                    show_ThongKeKimNgachNuoc_MatHang();
                    break;
            }
        }

        private void explorerBarVNACCS_TKMD_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //To khai VNACC
                case "ToKhaiNhap":
                    this.showToKhaiNhap();
                    break;
                case "ToKhaiXuat":
                    this.showToKhaiXuat();
                    break;
                case "TheoDoiToKhai":
                    this.showTheoDoiToKhai();
                    break;
                case "TheoDoiChungTu":
                    this.showTheoDoiChungTuToKhai();
                    break;        

                // To khai tri gia thap
                case "ToKhaiNhapTriGiaThap":
                    this.showToKhaiNhapTriGia();
                    break;
                case "ToKhaiXuatTriGiaThap":
                    this.showToKhaiXuatTriGia();
                    break;
                case "TheoDoiTKTG":
                    this.showTheoDoiToKhaiTriGia();
                    break;
                // to khai van chuyen
                case "TKVanChuyen":
                    this.showToKhaiVanChuyen();
                    break;
                case "TheoDoiTKVanChuyen":
                    this.showTheoDoiTKVanChuyen();
                    break;
                // Dang ky hang mien thue
                case "KhaiBaoTEA":
                    this.showTEA();
                    break;
                case "TheoDoiTEA":
                    this.showManagerTEA();
                    break;

                // Dang ky hang mien thue
                case "KhaiBaoTIA":
                    this.showTIA();
                    break;
                case "TheoDoiTIA":
                    this.showManagerTIA();
                    break;

                //To khai sua bo sung thue cua hang hoa
                case "KhaiBao_TK_KhaiBoSung_ThueHangHoa":
                    this.show_VNACC_TKBoSung_ThueHangHoaForm();
                    break;
                case "TheoDoi_TK_KhaiBoSung_ThueHangHoa":
                    this.show_VNACC_TKBoSung_ThueHangHoaManageForm();
                    break;

                //Chung tu thanh toan
                case "KhaiBao_CTTT_IAS":
                    this.Show_VNACC_CTTT_IAS();
                    break;
                case "KhaiBao_CTTT_IBA":
                    this.Show_VNACC_CTTT_IBA();
                    break;
                case "TheoDoi_ChungTuThanhToan":
                    this.Show_VNACC_CTTTManageForm();
                    break;

                //Chung tu dinh kem
                case "KhaiBao_ChungTuKem_MSB":
                    this.show_VNACC_ChungTuKem_MSBForm();
                    break;
                case "KhaiBao_ChungTuKem_HYS":
                    this.show_VNACC_ChungTuKem_HYSForm();
                    break;
                case "TheoDoi_ChungTuKem":
                    this.show_VNACC_ChungTuKemManageForm();
                    break;
                //Khai báo hàng vào kho CFS
                case "KhaiBao_CFS":
                    this.PhieuKhoRegister();
                    break;
                case "TheoDoi_CFS":
                    this.PhieKhoManager();
                    break;
                case "cmdSignFile":
                    this.show_VNACC_SignFile();
                    break;
                //Đinh danh hàng hóa
                case "cmdKhaiBaoDinhDanh":
                    this.showKhaiBao_DinhDanhHH();
                    break;
                case "cmdTheoDoiDinhDanh":
                    this.showTheoDoiDinhDanhHH();
                    break;
                //Tách vận đơn
                case "cmdKhaiBaoTachVanDon":
                    this.showKhaiBao_TachVanDon();
                    break;
                case "cmdTheoDoiTachVanDon":
                    this.showTheoDoiTachVanDon();
                    break;
                //Quản lý thu phí
                case "cmdHangContainer":
                    this.showToKhaiNP_HangContainer();
                    break;
                case "cmdHangRoi":
                    this.showToKhaiNP_HangRoi();
                    break;
                case "cmdDanhSachTKNP":
                    this.showDanhSachTKNP();
                    break;
                case "cmdTraCuuBL":
                    this.showTraCuuBL();
                    break;
                case "cmdRegisterInformation":
                    this.showRegisterInformation();
                    break;
                case "cmdRegisterManagement":
                    this.showRegisterManagement();
                    break;
                case "cmdTheoDoiMessage":
                    this.showTheoDoiMessage();
                    break;
            }
        }

        private void showTheoDoiMessage()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyMessage"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            QuanLyMessage f = new QuanLyMessage();
            f.MdiParent = this;
            f.Show();
        }
        private void showRegisterManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("RegisterManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            RegisterManagementForm f = new RegisterManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showRegisterInformation()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_Register_Information"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_Register_Information f = new HP_Register_Information();
            f.MdiParent = this;
            f.Show();
        }

        private void showTraCuuBL()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_SearchInfomationForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_SearchInfomationForm f = new HP_SearchInfomationForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showDanhSachTKNP()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_ToKhaiNopPhiManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_ToKhaiNopPhiManagementForm f = new HP_ToKhaiNopPhiManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showToKhaiNP_HangRoi()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_ToKhaiNopPhiForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_ToKhaiNopPhiForm f = new HP_ToKhaiNopPhiForm();
            f.IsContainer = false;
            f.MdiParent = this;
            f.Show();
        }

        private void showToKhaiNP_HangContainer()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_ToKhaiNopPhiForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_ToKhaiNopPhiForm f = new HP_ToKhaiNopPhiForm();
            f.IsContainer = true;
            f.MdiParent = this;
            f.Show();
        }
        private void showTheoDoiTachVanDon()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_BillOfLadingManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_BillOfLadingManagementForm f = new VNACC_BillOfLadingManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showKhaiBao_TachVanDon()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_BillOfLadingForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_BillOfLadingForm f = new VNACC_BillOfLadingForm();
            f.MdiParent = this;
            f.Show();
        }
        private void showTheoDoiDinhDanhHH()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhDanhHangHoaManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhDanhHangHoaManagerForm f = new DinhDanhHangHoaManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showKhaiBao_DinhDanhHH()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CapSoDinhDanhHangHoaForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            CapSoDinhDanhHangHoaForm f = new CapSoDinhDanhHangHoaForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_SignFile()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SignDigitalDocumentForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            SignDigitalDocumentForm f = new SignDigitalDocumentForm();
            f.MdiParent = this;
            f.Show();
        }
        private void PhieuKhoRegister()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhieuKho"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            PhieuKho f = new PhieuKho();
            f.MdiParent = this;
            f.Show();
        }

        private void PhieKhoManager()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhieuKhoManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            PhieuKhoManager f = new PhieuKhoManager();
            f.MdiParent = this;
            f.Show();
        }

        private void explorerBarVNACCS_GiayPhep_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Giay phep SEA
                case "KhaiBao_GiayPhep_SEA":
                    this.show_VNACC_GiayPhepForm_SEA();
                    break;
                case "TheoDoi_GiayPhep_SEA":
                    this.show_VNACC_GiayPhepManageForm_SEA();
                    break;

                //Giay phep SFA
                case "KhaiBao_GiayPhep_SFA":
                    this.show_VNACC_GiayPhepForm_SFA();
                    break;
                case "TheoDoi_GiayPhep_SFA":
                    this.show_VNACC_GiayPhepManageForm_SFA();
                    break;

                //Giay phep SAA
                case "KhaiBao_GiayPhep_SAA":
                    this.show_VNACC_GiayPhepForm_SAA();
                    break;
                case "TheoDoi_GiayPhep_SAA":
                    this.show_VNACC_GiayPhepManageForm_SAA();
                    break;

                //Giay phep SMA
                case "KhaiBao_GiayPhep_SMA":
                    this.show_VNACC_GiayPhepForm_SMA();
                    break;
                case "TheoDoi_GiayPhep_SMA":
                    this.show_VNACC_GiayPhepManageForm_SMA();
                    break;
            }
        }

        private void explorerBarVNACCS_HoaDon_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Hoa don
                case "KhaiBaoHoaDon":
                    this.show_VNACC_HoaDonForm();
                    break;
                case "TheoDoiHoaDon":
                    this.show_VNACC_HoaDonManageForm();
                    break;
            }
        }

        #region Khai bao sua doi/ bo sung thue hang hoa

        private void show_VNACC_TKBoSung_ThueHangHoaManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TKBoSung_ThueHangHoaManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_TKBoSung_ThueHangHoaManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_TKBoSung_ThueHangHoaForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("VNACC_TKBoSung_ThueHangHoaForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            BaseForm f = new VNACC_TKBoSung_ThueHangHoaForm();
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        #region VNACC Chung tu kem
        private void show_VNACC_ChungTuKemManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuKemManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuKemManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_ChungTuKem_MSBForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("VNACC_ChungTuDinhKemForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            BaseForm f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.MSB);
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_ChungTuKem_HYSForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("VNACC_ChungTuDinhKemForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            BaseForm f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.HYS);
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        // Hoa don
        #region Hoa Don
        private void show_VNACC_HoaDonForm()
        {
                       
            khaiBaoHoaDon = new VNACC_HoaDonForm();
            khaiBaoHoaDon.MdiParent = this;
            khaiBaoHoaDon.Show();
        }
        private void show_VNACC_HoaDonManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_HoaDonManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hoaDonManage = new VNACC_HoaDonManageForm();
            hoaDonManage.MdiParent = this;
            hoaDonManage.Show();
        }
        #endregion

        //giay phep
        #region Giay Phep
        private void show_VNACC_GiayPhepForm_SEA()
        {
            
            khaiBaoGiayPhep = new VNACC_GiayPhepForm_SEA();
            khaiBaoGiayPhep.MdiParent = this;
            khaiBaoGiayPhep.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SEA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            giayPhepManage = new VNACC_GiayPhepManageForm();
            giayPhepManage.MdiParent = this;
            giayPhepManage.Show();
        }

        private void show_VNACC_GiayPhepForm_SFA()
        {
          
            BaseForm f = new VNACC_GiayPhepForm_SFA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SFA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm_SFA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepManageForm_SFA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepForm_SAA()
        {
            
            BaseForm f = new VNACC_GiayPhepForm_SAA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SAA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm_SAA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepManageForm_SAA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepForm_SMA()
        {
           
            BaseForm f = new VNACC_GiayPhepForm_SMA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SMA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm_SMA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepManageForm_SMA();
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        #region Chung tu thanh toan

        private void Show_VNACC_CTTT_IAS()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuThanhToanForm_IAS"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuThanhToanForm_IAS();
            f.MdiParent = this;
            f.Show();
        }

        private void Show_VNACC_CTTT_IBA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuThanhToanForm_IBA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuThanhToanForm_IBA();
            f.MdiParent = this;
            f.Show();
        }

        private void Show_VNACC_CTTTManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuThanhToanManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuThanhToanManageForm();
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        #region To khai VNACC
        private void showToKhaiNhap()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("ToKhaiNhap"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            toKhaiNhap = new VNACC_ToKhaiMauDichNhapForm();
            toKhaiNhap.MdiParent = this;
            toKhaiNhap.Show();
        }

        private void showToKhaiXuat()
        {

            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("ToKhaiXuat"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            toKhaiXuat = new VNACC_ToKhaiMauDichXuatForm();
            toKhaiXuat.MdiParent = this;
            toKhaiXuat.Show();
        }

        private void showTheoDoiToKhai()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ToKhaiManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiManager = new VNACC_ToKhaiManager();
            toKhaiManager.MdiParent = this;
            toKhaiManager.Show();
        }
        private void showTheoDoiChungTuToKhai()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_VouchersToKhaiManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_VouchersToKhaiManager toKhaiManager = new VNACC_VouchersToKhaiManager();
            toKhaiManager.MdiParent = this;
            toKhaiManager.Show();

        }
        private void showTEA()
        {

            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("KhaiBaoTEA"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            //TEAForm = new VNACC_TEAForm();
            //TEAForm.MdiParent = this;
            //TEAForm.Show();
            VNACC_TEAForm f = new VNACC_TEAForm();
            f.ShowDialog();
        }
        private void showManagerTEA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TEA_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TEAManager = new VNACC_TEA_ManagerForm();
            TEAManager.MdiParent = this;
            TEAManager.Show();
        }
        private void showTIA()
        {
            VNACC_TIAForm f = new VNACC_TIAForm();
            f.ShowDialog();
        }
        private void showManagerTIA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TIA_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TIAManager = new VNACC_TIA_ManagerForm();
            TIAManager.MdiParent = this;
            TIAManager.Show();
        }
        #endregion

        #region To khai tri gia thap
        private void showToKhaiNhapTriGia()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiNhapTriGiaThap"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiNhapTriGia = new VNACC_TKNhapTriGiaForm();
            toKhaiNhapTriGia.MdiParent = this;
            toKhaiNhapTriGia.Show();
        }
        private void showToKhaiXuatTriGia()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiXuat"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiXuatTriGia = new VNACC_TKXuatTriGiaForm();
            toKhaiXuatTriGia.MdiParent = this;
            toKhaiXuatTriGia.Show();
        }
        private void showTheoDoiToKhaiTriGia()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TKTriGiaManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiTriGiaManager = new VNACC_TKTriGiaManager();
            toKhaiTriGiaManager.MdiParent = this;
            toKhaiTriGiaManager.Show();
        }
        #endregion

        #region To Khai Van Chuyen
        private void showToKhaiVanChuyen()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("TKVanChuyen"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            toKhaiVanChuyen = new VNACC_ChuyenCuaKhau();
            toKhaiVanChuyen.MdiParent = this;
            toKhaiVanChuyen.Show();
        }
        private void showTheoDoiTKVanChuyen()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChuyenCuaKhau_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            theoDoiTKVanChuyen = new VNACC_ChuyenCuaKhau_ManagerForm();
            theoDoiTKVanChuyen.MdiParent = this;
            theoDoiTKVanChuyen.Show();
        }
        #endregion

        private void uiPanelToKhaiMauDich_Click(object sender, EventArgs e)
        {

        }
        public void show_QuanLyMessageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyMessageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            quanLyMsg = new Company.Interface.VNACCS.QuanLyMessageForm();
            quanLyMsg.MdiParent = this;
            quanLyMsg.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            statusBar.Panels["Support"].Visible = true;
            Int32 red;
            Int32 green;
            Int32 blue;
            Random rnd = new Random();
            red = rnd.Next(0, 255);
            green = rnd.Next(0, 255);
            blue = rnd.Next(0, 255);
            Color clr = Color.FromArgb(red, green, blue);
            statusBar.Panels["Support"].Alignment = HorizontalAlignment.Center;
            statusBar.Panels["Support"].FormatStyle.ForeColor = clr;
        }


    }
}
