﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using System.Linq;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;


namespace SOFTECH.ECS.V3.Components.KD
{

    // NAMNTH
    public partial class HopDongThuongMai
    {

        /// <summary>
        /// ConfigContractItem
        /// </summary>
        /// <param name="ContractItem">XElement</param>
        /// <param name="lHDTM_detail">HopDongThuongMaiDetai</param>
        /// <returns>XElement</returns>
        public static XElement ConfigContractItem(XElement ContractItem, HopDongThuongMaiDetail lHDTM_detail)
        {
            Nuoc nuoc;
            DonViTinh dvt;


            if (ContractItem != null && ContractItem.Elements().Count()>0 && lHDTM_detail != null)
            {
                foreach (XElement element in ContractItem.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.unitPrice.ToUpper()))//Đơn giá
                        element.SetValue("");
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.statisticalValue.ToUpper()))//Trị giá
                        element.SetValue("");                  

                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.Commodity.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.description.ToUpper()))
                                elementChild.SetValue(lHDTM_detail.TenHang);// Tên hàng
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.identification.ToUpper()))
                                elementChild.SetValue("");//Mã tham chiếu hàng hóa
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.tariffClassification.ToUpper()))
                                elementChild.SetValue(lHDTM_detail.MaHS);//Mã HS

                        }
                    }
                    
                    nuoc = Nuoc.Load(lHDTM_detail.NuocXX_ID);
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.Origin.ToUpper()))
                    {
                        if (nuoc != null)
                            foreach(XElement elementChild in element.Elements())
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.originCountry.ToUpper()))
                                    elementChild.SetValue(nuoc.Ten);//Xuất xứ của hàng hóa
                    }

                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.GoodsMeasure.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.quantity.ToUpper()))
                                elementChild.SetValue(lHDTM_detail.SoLuong);//Số lượng
                            dvt = DonViTinh.Load(lHDTM_detail.DVT_ID);
                            if (dvt != null)
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.measureUnit.ToUpper()))
                                    elementChild.SetValue(dvt.Ten);//Mã đơn vị tính                        
                        }                        
                    }

                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.AdditionalInformation.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.content.ToUpper()))
                                elementChild.SetValue(lHDTM_detail.GhiChu);//Mã đơn vị tính                                            
                    }
                }
            }            

            return ContractItem;
        }

        /// <summary>
        /// ConfigContractDocument
        /// </summary>
        /// <param name="ContractDocument">XElement</param>
        /// <param name="hdtm">HopDongThuongMai</param>
        /// <returns>XElement</returns>
        public static XElement ConfigContractDocument(XElement ContractDocument,HopDongThuongMai hdtm)
        {

            PhuongThucThanhToan pttt = PhuongThucThanhToan.Load(hdtm.PTTT_ID);
            ToKhaiMauDich tkmd = ToKhaiMauDich.Load(hdtm.TKMD_ID);
            DieuKienGiaoHang dkgh = DieuKienGiaoHang.Load(hdtm.DKGH_ID);
            IList<HopDongThuongMaiDetail> lHDTM_detail = HopDongThuongMaiDetail.SelectCollectionBy_HopDongTM_ID(hdtm.ID);
            NguyenTe nguyente = NguyenTe.Load(hdtm.NguyenTe_ID);

            //các thông tin liên quan Hợp đồng thương mại
            if (ContractDocument != null && hdtm != null)
            {
                XElement ContractItem = new XElement(NodeHopDongThuongMai.ContractItem);
                foreach (XElement element in ContractDocument.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.reference.ToUpper()))
                        element.SetValue(hdtm.SoHopDongTM);//Số hợp đồng   
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.issue.ToUpper()))
                        element.SetValue(hdtm.NgayHopDongTM);//Ngày hợp đồng                                                                
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.expire.ToUpper()))
                        element.SetValue(hdtm.ThoiHanThanhToan);//Thời hạn thanh toán    
                    if (pttt != null)
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.Payment.ToUpper()))
                        {
                            foreach(XElement elementChild in element.Elements())
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.method.ToUpper()))
                                    elementChild.SetValue(pttt.GhiChu);//Phương thức thanh toán      
                        }
                            
                    if (dkgh != null)
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.TradeTerm.ToUpper()))
                        { 
                            foreach(XElement elementChild in element.Elements())
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.condition.ToUpper()))
                                    elementChild.SetValue(dkgh.MoTa);//Điều kiện giao hàng   
                        }
                            
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.totalValue.ToUpper()))
                        element.SetValue(hdtm.TongTriGia);//Tổng trị giá                                    
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.DeliveryDestination.ToUpper()))
                    { 
                        foreach(XElement elementChild in element.Elements())
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.line.ToUpper()))
                                elementChild.SetValue(hdtm.DiaDiemGiaoHang);//Địa điểm giao hàng     
                    }
                    if(nguyente!=null)
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.CurrencyExchange.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.currencyType.ToUpper()))
                                    elementChild.SetValue(nguyente.Ten);//Đồng tiền thanh toán               
                        }
                        
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.Buyer.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.name.ToUpper()))
                                elementChild.SetValue(hdtm.TenDonViMua);//Tên người mua        
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.identity.ToUpper()))
                                elementChild.SetValue(hdtm.MaDonViMua);//Ma người mua        
                        }
                        
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.Seller.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.name.ToUpper()))
                                elementChild.SetValue(hdtm.TenDonViBan);//Tên người bán        
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.identity.ToUpper()))
                                elementChild.SetValue(hdtm.MaDonViBan);//Ma người bán        
                        }
                        
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.AdditionalInformation.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.content.ToUpper()))
                                elementChild.SetValue(hdtm.ThongTinKhac);//Ghi chú khác 
                    }

                    else if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.ContractItem.ToUpper()))
                        ContractItem = element;                        
                }

                if (ContractItem != null)
                {
                    IList<HopDongThuongMaiDetail> lHDTMDT = HopDongThuongMaiDetail.SelectCollectionBy_HopDongTM_ID(hdtm.ID);
                    if (lHDTMDT != null)
                    {
                        if (lHDTMDT.Count > 1)
                        {
                            XElement TempContractItem = new XElement(ContractItem);
                            ConfigContractItem(ContractItem, lHDTMDT[0]);
                            for (int i = 1; i < lHDTMDT.Count; i++)
                            {
                                XElement NewContractItem = new XElement(TempContractItem);
                                ContractDocument.Add(ConfigContractItem(NewContractItem, lHDTMDT[i]));
                            }
                        }
                        else if (lHDTMDT.Count == 1)
                        {
                            ConfigContractItem(ContractItem, lHDTMDT[0]);
                        }
                    }
                }

            } 

            return ContractDocument;
        }

        /// <summary>
        /// ConfigHopDongThuongMai
        /// </summary>        
        /// <returns>string</returns>
        public static string ConfigHopDongThuongMai(ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            try
            {
                IList<HopDongThuongMai> lHDTM = HopDongThuongMai.SelectCollectionBy_TKMD_ID(tkmd.ID);
                if (lHDTM != null && lHDTM.Count > 0)
                {
                    XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/KD/HopDongThuongMai.xml");
                    if (tkmd != null && docXML != null)
                    {
                        XElement Root = docXML.Root;
                        if (Root != null && Root.Elements().Count() > 0)
                        {
                            XElement ContractDocuments = Root.Element(NodeHopDongThuongMai.ContractDocuments);
                            XElement ContractDocument = ContractDocuments.Element(NodeHopDongThuongMai.ContractDocument);
                            // Check Upper
                            foreach (XElement element in Root.Elements())
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.ContractDocuments.ToUpper()))
                                    ContractDocuments = element;
                            }
                            foreach (XElement element in ContractDocuments.Elements())
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeHopDongThuongMai.ContractDocument.ToUpper()))
                                    ContractDocument = element;
                            }
                            Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                            // Add ContractDocuments

                            // ContractDocument > 1
                           
                            if (lHDTM.Count == 1)
                            {
                                ConfigContractDocument(ContractDocument, lHDTM[0]);
                            }
                            else if (lHDTM.Count > 1)
                            {
                                XElement TempContractDocument = new XElement(ContractDocument);
                                ConfigContractDocument(ContractDocument, lHDTM[0]);
                                // Add new ContractDocument
                                for (int i = 1; i < lHDTM.Count; i++)
                                {
                                    XElement NewContractDocument = new XElement(TempContractDocument);
                                    ContractDocuments.Add(ConfigContractDocument(NewContractDocument, lHDTM[i]));
                                }
                            }
                           

                            //docXML.Save(@"D:\NewHDTM.xml");
                            //docXML.Save(@"../../../SOFTECH.ECS.V3.Components/XML/LoadKD/NewHopDongThuongMai.xml");
                            return docXML.ToString();
                        }

                    }
                    return "";
                }                
                return "";
            }
            catch
            {
                return "";
            }
            
        
        }
        
    }
}
