-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@LOCODE varchar(10),
	@CityCode varchar(5),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@AirportPortClassification int,
	@CityNameOrStateName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_CityUNLOCODE]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@LOCODE,
	@CityCode,
	@CountryCode,
	@NecessityIndicationOfInputName,
	@AirportPortClassification,
	@CityNameOrStateName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Update]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@LOCODE varchar(10),
	@CityCode varchar(5),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@AirportPortClassification int,
	@CityNameOrStateName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CityUNLOCODE]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[LOCODE] = @LOCODE,
	[CityCode] = @CityCode,
	[CountryCode] = @CountryCode,
	[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
	[AirportPortClassification] = @AirportPortClassification,
	[CityNameOrStateName] = @CityNameOrStateName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@LOCODE varchar(10),
	@CityCode varchar(5),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@AirportPortClassification int,
	@CityNameOrStateName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_CityUNLOCODE] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CityUNLOCODE] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[LOCODE] = @LOCODE,
			[CityCode] = @CityCode,
			[CountryCode] = @CountryCode,
			[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
			[AirportPortClassification] = @AirportPortClassification,
			[CityNameOrStateName] = @CityNameOrStateName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_CityUNLOCODE]
		(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[LOCODE],
			[CityCode],
			[CountryCode],
			[NecessityIndicationOfInputName],
			[AirportPortClassification],
			[CityNameOrStateName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@LOCODE,
			@CityCode,
			@CountryCode,
			@NecessityIndicationOfInputName,
			@AirportPortClassification,
			@CityNameOrStateName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CityUNLOCODE]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CityUNLOCODE] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CityUNLOCODE]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CityUNLOCODE] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CityUNLOCODE]	

GO

