using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data.SqlClient;
using Logger;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.GC
{
    public partial class NhapXuatTon_TT38 : DevExpress.XtraReports.UI.XtraReport
    {
        public DataTable tb = new DataTable();
        public HopDong HD = new HopDong();
        private decimal _tongTriGia = 0;
        private decimal _tongTriGiaSD = 0;
        private decimal _tongTriGiaTon = 0;
        private decimal SoLuongConLai_G_F;
        private decimal TongLuongNhap_G_F;
        private decimal TongLuongSuDung_G_F;
        private decimal TongTriGiaConLai_G_F;
        private string _maNPL;
        private int _stt = 0;
        public NhapXuatTon_TT38()
        {
            InitializeComponent();
            
        }

        public void BindReport(string where)
        {
            string MaNPL = "";
            decimal DonGiaTT = 0;
            decimal TongTriGiaNhap = 0;
            decimal TongLuongNhap = 0;
            decimal LuongXuat = 0;
            decimal DonGiaTB=0;
            foreach (DataRow dr in tb.Rows)
            {
                if (dr["Ma"].ToString()==MaNPL && dr["MaLoaiHinh"].ToString().Contains("XV"))
                {
                    if (dr["MaLoaiHinh"].ToString().Contains("XVE54"))
                    {
                        dr["TriGiaSuDung"] = 0;
                    }
                    else
                    {
                        LuongXuat = Convert.ToDecimal(dr["LuongSD_TK"]);
                        if (LuongXuat < TongLuongNhap)
                        {
                            try
                            {
                                dr["DonGiaTT"] = TongTriGiaNhap / LuongXuat;
                                dr["TriGiaSuDung"] = LuongXuat * (TongTriGiaNhap / LuongXuat);

                            }
                            catch (Exception ex)
                            {

                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }

                        }
                        else
                        {
                            try
                            {
                                dr["DonGiaTT"] = TongTriGiaNhap / TongLuongNhap;
                                dr["TriGiaSuDung"] = (LuongXuat * DonGiaTT);
                            }
                            catch (Exception ex)
                            {

                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }

                        }
                        TongLuongNhap = 0;
                        TongTriGiaNhap = 0;
                    }


                    //dr["TriGiaSuDung"] =(DonGiaTT * Convert.ToDecimal(dr["LuongSD_TK"]));
                }
                if (dr["MaLoaiHinh"].ToString().Contains("NV"))
                {
                    MaNPL = dr["Ma"].ToString();
                    DonGiaTT = Convert.ToDecimal(dr["DonGiaTT"]);
                    TongLuongNhap  += Convert.ToDecimal(dr["SoLuong"]);
                    TongTriGiaNhap += Convert.ToDecimal(dr["TriGiaSuDung"]);
                    DonGiaTB= TongTriGiaNhap/TongLuongNhap;
                    dr["LuongSD_TK"] = 0;
                    dr["TriGiaSuDung"] = 0;

                }
                else if (dr["MaLoaiHinh"].ToString().Contains("XVE54"))
                {
                    dr["TriGiaSuDung"] = 0;
                    dr["SoLuong"] = 0;
                }
                else
                {
                    dr["SoLuong"] = 0;
                    dr["TriGiaTT"] = 0;
                    MaNPL = dr["Ma"].ToString();
                }

            }
            DataTable dataSource = tb;
            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontToKhai);
            //int i = 0;
            //string maNPL = "";
            try
            {
                //foreach (DataRow dr in dataSource.Rows)
                //{
                    
                //    if (maNPL == dr["Ma"].ToString().Trim())
                //    {
                //        dr["STT"] = i;
                //    }
                //    else
                //    {
                //        maNPL = dr["Ma"].ToString().Trim();
                //        i++;
                //        dr["STT"] = i;
                //    }
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
            //#region Tính Lương tồn
            //DataTable temp = dataSource.DefaultView.ToTable(true, "Ma","SoLuongDaNhap","SoLuongCungUng","SoLuongDaDung","LuongTon");
            //string ma = "";
            //decimal tongNhap = 0;
            //decimal tongSuDung = 0;
            //decimal tongTon = 0;
            
            
            //foreach (DataRow dr in temp.Select())
            //{
            //    string ma1 = dr["Ma"].ToString();
            //    tongNhap = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
            //    tongSuDung = Convert.ToDecimal(dr["SoLuongDaDung"].ToString());
            //    tongTon = Convert.ToDecimal(dr["LuongTon"].ToString());
            //    foreach (DataRow dr1 in dataSource.Select("Ma='"+dr["Ma"].ToString()+"'","NgayDangKy"))
            //    {
            //        if (dr1["SoToKhai"].ToString() == "4266") 
            //        {

            //        }
            //        //int lenght = dataSource.Select("Ma='" + dr["Ma"].ToString() + "'", "Ma").Length;
            //        string ma2 = dr1["Ma"].ToString();
            //        try
            //        {
            //            #region Tính lượng vs trị giá
            //            decimal luongTK = Convert.ToDecimal(dr1["SoLuong"].ToString());
            //            decimal luongSD = Convert.ToDecimal(dr1["LuongSD_TK"].ToString());
            //            decimal luongTon = Convert.ToDecimal(dr1["LuongTon_TK"].ToString());
            //            //
            //            decimal triGiaTK = Convert.ToDecimal(dr1["TriGiaTT"].ToString());
            //            try
            //            {
            //                decimal triGiaSD = Convert.ToDecimal(dr1["TriGiaSuDung"].ToString());
            //            }
            //            catch (Exception ex)
            //            { }
            //            decimal triGiaTon = Convert.ToDecimal(dr1["TonTriGia"].ToString());
            //            decimal donGia = Convert.ToDecimal(dr1["DonGiaTT"].ToString());
            //            decimal tyGia = Convert.ToDecimal(dr1["TyGiaTT"].ToString());
            //            if (tongSuDung > luongTK) 
            //            {
            //                dr1["LuongSD_TK"] = luongTK;
            //                dr1["LuongTon_TK"] = 0;
            //                try
            //                {
            //                     dr1["TriGiaSuDung"] = Convert.ToInt64(Math.Round(triGiaTK,0).ToString());
            //                }
            //                catch (Exception ex)
            //                {
            //                }
            //                dr1["TonTriGia"] = 0;
            //                tongSuDung = tongSuDung - luongTK;


            //            }
            //            else if (tongSuDung > 0 && tongSuDung < luongTK)
            //            {
            //                dr1["LuongSD_TK"] = tongSuDung;
            //                dr1["LuongTon_TK"] = luongTK - tongSuDung;

            //                try
            //                {
            //                    dr1["TriGiaSuDung"] = Math.Round(tongSuDung * donGia, 0).ToString();
            //                    dr1["TonTriGia"] = Math.Round(triGiaTK - (tongSuDung * donGia), 0).ToString();
            //                }
            //                catch (Exception ex)
            //                {

            //                }

            //                tongSuDung = 0;
            //            }
            //            else 
            //            {
            //                dr1["LuongSD_TK"] = 0;
            //                dr1["TriGiaSuDung"] = 0;
            //            }

            //            //luongSD = Convert.ToDecimal(dr1["LuongSD_TK"].ToString());
            //            //luongTon = Convert.ToDecimal(dr1["LuongTon_TK"].ToString());
            //            #endregion

            //        }
            //        catch (Exception ex)
            //        {

            //           // throw;
            //        }
            //    }
            //}
            //#endregion


            DetailReport.DataSource = dataSource;
            try
            {
                this.GroupHeader3.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("SoHopDong"),
                
                });
                this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("SoTaiKhoan"),
                
                });
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("Ma"),
                new DevExpress.XtraReports.UI.GroupField("SoLuongDaNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoLuongDaDung"),
                new DevExpress.XtraReports.UI.GroupField("SoLuongCungUng"),
                new DevExpress.XtraReports.UI.GroupField("LuongTon"),
                });
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            txtSoTaiKhoan.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            txtSoHopDong.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDong");
            lbl_STT_G_H.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblMaNPL_G_H.DataBindings.Add("Text", DetailReport.DataSource, "Ma");
            lblTenNPL_G_H.DataBindings.Add("Text", DetailReport.DataSource, "Ten");

            //lblTongLuongNhap_G_F.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongDaNhap", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //lblTongLuongCU_G_H.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongCungUng", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            //lbl_TongLuongSD_G_F.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongDaDung", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //lblTongLuongTon_G_F.DataBindings.Add("Text", DetailReport.DataSource, "LuongTon", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            
            lblSoToKhai_D.DataBindings.Add("Text", DetailReport.DataSource, "SoToKhaiVnacc");
            lblNgayDangKy_D.DataBindings.Add("Text", DetailReport.DataSource, "NgayDangKy");
            txtMaLoaiHinh.DataBindings.Add("Text", DetailReport.DataSource, "MaLoaiHinh");

            lblLuongNhapTK_D.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
            lblLuongSD_D.DataBindings.Add("Text", DetailReport.DataSource, "LuongSD_TK", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
            lblLuongTonTK_D.DataBindings.Add("Text", DetailReport.DataSource, "LuongTon_TK", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
            lbl_DVT_NPL_D.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
            lblDVT_NPL_G_F.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
            lblDonGia_D.DataBindings.Add("Text", DetailReport.DataSource, "DonGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(6, true));
            lblTriGia_D.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            lblTriGiaSuDung_TK_D.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
            lblTonTriGia_D.DataBindings.Add("Text", DetailReport.DataSource, "TonTriGia", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
            lblTyGia_D.DataBindings.Add("Text", DetailReport.DataSource, "TyGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lbl_DVT_TG_D.DataBindings.Add("Text", DetailReport.DataSource, "MaDVT");

            lblTongLuongNhap_G_F.DataBindings.Add("Text", null, "SoLuong");
            lblTongLuongNhap_G_F.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:N6}");

            lbl_TongLuongSD_G_F.DataBindings.Add("Text", null, "LuongSD_TK");
            lbl_TongLuongSD_G_F.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:N6}");            

            lblTongLuongTon_G_F.DataBindings.Add("Text", null, "LuongTon_TK");
            lblTongLuongTon_G_F.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:N6}");

            lblTongTriGia_G_F.DataBindings.Add("Text", null, "TriGiaTT");
            lblTongTriGia_G_F.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:N0}");

            lblTongTriGiaSuDung_G_F.DataBindings.Add("Text", null, "TriGiaSuDung");
            lblTongTriGiaSuDung_G_F.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:N0}");

            //lblTongTonTriGia_G_F.DataBindings.Add("Text", null, "TonTriGia");
            //lblTongTonTriGia_G_F.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:N0}");

            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            
            //lblSoHopDong.Text = HD.SoHopDong;
            //lblSoHopDong.Width = 200;
            
            xrLabel5.Text = GlobalSettings.TieudeNgay;
        }

        private void NhapXuatTon_TT38_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
             
        }

        private void lblTriGia_D_AfterPrint(object sender, EventArgs e)
        {
            //_tongTriGia += Convert.ToDecimal(lblTriGia_D.Text);
        }

        private void lblTriGiaSuDung_TK_D_AfterPrint(object sender, EventArgs e)
        {
            //_tongTriGiaSD += Convert.ToDecimal(lblTriGiaSuDung_TK_D.Text);
        }

        private void lblTonTriGia_D_AfterPrint(object sender, EventArgs e)
        {
            //_tongTriGiaTon += Convert.ToDecimal(lblTonTriGia_D.Text);
        }

        private void lblTongTriGia_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblTongTriGia_G_F.Text = _tongTriGia.ToString("N0");
            //_tongTriGia = 0;
        }

        private void lblTongTriGiaSuDung_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblTongTriGiaSuDung_G_F.Text = _tongTriGiaSD.ToString("N0");
            //_tongTriGiaSD = 0;
        }

        private void lblMaNPL_G_H_AfterPrint(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_maNPL) || lblMaNPL_G_H.Text != _maNPL) 
            {
                lblMaNPL_G_H.Text = _maNPL;
            }
            
            
        }

        private void lbl_STT_G_H_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            _stt++;
            lbl_STT_G_H.Text = _stt.ToString();
        }

        private void lblTongTonTriGia_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                TongTriGiaConLai_G_F = (decimal)lblTongTriGia_G_F.Summary.GetResult() - (decimal)lblTongTriGiaSuDung_G_F.Summary.GetResult();
                lblTongTonTriGia_G_F.Text = TongTriGiaConLai_G_F.ToString("N0");
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
            //lblTongTonTriGia_G_F.Text = _tongTriGiaTon.ToString("N0");
            //_tongTriGiaTon = 0;
        }

        private void lblNgayDangKy_D_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string d = lblNgayDangKy_D.Text;
            lblNgayDangKy_D.Text = Convert.ToDateTime(d).ToString("dd/MM/yyyy");
        }

        private void txtSoHopDong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            txtSoHopDong.Text = "Hợp đồng số: " + txtSoHopDong.Text;
        }

        private void txtTenTaiKhoan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (txtSoTaiKhoan.Text == "Tài khoản: 152")
                txtTenTaiKhoan.Text = "Nguyên liệu , vật liệu nhập khẩu";
            else
            {
                txtTenTaiKhoan.Text = "Thành phẩm được sản xuất từ nguồn nhập khẩu";
                _stt = 0;
            }
        }

        private void txtSoTaiKhoan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            txtSoTaiKhoan.Text = "Tài khoản: " + txtSoTaiKhoan.Text;
        }

        private void txtMaLoaiHinh_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (txtMaLoaiHinh.Text.Contains("V"))
            //{
            //    try
            //    {
            //        txtMaLoaiHinh.Text = txtMaLoaiHinh.Text.Substring(2, 3);
            //    }
            //    catch (Exception ex)
            //    {

            //        //throw;
            //    }

            //}
        }

        private void lblLuongNhapTK_D_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (txtMaLoaiHinh.Text.Contains("N"))
            {
                //TongLuongNhap_G_F += Convert.ToDecimal(lblLuongNhapTK_D.Text);
            }
            else
            {
                lblLuongNhapTK_D.Text = "0";
            }
        }

        private void lblLuongNhapTK_D_AfterPrint(object sender, EventArgs e)
        {

        }

        private void lblLuongSD_D_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (txtMaLoaiHinh.Text.Contains("X"))
            {
                //TongLuongSuDung_G_F += Convert.ToDecimal(lblLuongSD_D.Text);
            }
            else
            {
                lblLuongSD_D.Text = "0";
            }

        }

        private void lblLuongSD_D_AfterPrint(object sender, EventArgs e)
        {

        }

        private void lblTongLuongTon_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblTongLuongTon_G_F_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    SoLuongConLai_G_F = (decimal)lblTongLuongNhap_G_F.Summary.GetResult() - (decimal)lbl_TongLuongSD_G_F.Summary.GetResult();
            //    lblTongLuongTon_G_F.Text = SoLuongConLai_G_F.ToString();
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void lblTriGia_D_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (txtMaLoaiHinh.Text.Contains("N") || txtMaLoaiHinh.Text.Contains("XVE54"))
            {
            }
            else
            {
                lblTriGia_D.Text = "0";
            }
        }

        private void lblTriGiaSuDung_TK_D_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (txtMaLoaiHinh.Text.Contains("X"))
            {
            }
            else if (txtMaLoaiHinh.Text.Contains("XVE54"))
            {
                lblTriGiaSuDung_TK_D.Text = "0";
            }
            else
            {
                lblTriGiaSuDung_TK_D.Text = "0";
            }
        }

        private void lblTongTonTriGia_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void lblTongLuongNhap_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblTongLuongNhap_G_F_AfterPrint(object sender, EventArgs e)
        {
            //lblTongLuongNhap_G_F.Text = TongLuongNhap_G_F.ToString("N6");
        }

        private void lbl_TongLuongSD_G_F_AfterPrint(object sender, EventArgs e)
        {
            //lbl_TongLuongSD_G_F.Text = TongLuongSuDung_G_F.ToString("N6");
        }

        private void lbl_TongLuongSD_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblTongTriGia_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void lblTongTriGiaSuDung_G_F_AfterPrint(object sender, EventArgs e)
        {

        }
       
       
       
    }
}
