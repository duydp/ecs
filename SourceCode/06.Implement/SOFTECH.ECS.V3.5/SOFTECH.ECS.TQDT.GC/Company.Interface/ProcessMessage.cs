﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
#if GC_V4
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho;
using System.IO;
#elif SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho;
#elif KD_V4
#endif

namespace Company.Interface
{
   public class ProcessMessage
    {
        public string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        public string sfmtDate = "yyyy-MM-dd";
        public string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";
        public KDT_ContainerDangKy ProcessMessageBSContainer(KDT_ContainerDangKy containerDangKy)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Container_VNACCS fback = null;

                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", containerDangKy.ID, DeclarationIssuer.Container);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            containerDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                containerDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                containerDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else containerDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Container_VNACCS> msg = Helpers.Deserialize<MessageSend<Container_VNACCS>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Container_VNACCS>(content);
                                            if (fback.TransportEquipments.TransportEquipment.Count >= 1)
                                            {
                                                foreach (KDT_ContainerBS containerBS in containerDangKy.ListCont)
                                                {
                                                    containerBS.Delete();
                                                }
                                                containerDangKy.ListCont.Clear();
                                                foreach (TransportEquipment items in fback.TransportEquipments.TransportEquipment)
                                                {
                                                    KDT_ContainerBS cont = new KDT_ContainerBS();
                                                    cont.Master_id = containerDangKy.ID;
                                                    cont.SoVanDon = items.BillOfLading.ToString();
                                                    cont.SoContainer = items.Container;
                                                    cont.SoSeal = items.Seal;
                                                    cont.GhiChu = items.Content;
                                                    containerDangKy.ListCont.Add(cont);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return containerDangKy;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
#if GC_V4
        public GiamSatTieuHuy ProcessMessageGSTieuHuy(GiamSatTieuHuy giamSatTieuHuy)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                GC_DNGiamSatTieuHuy fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", giamSatTieuHuy.ID, DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            giamSatTieuHuy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                giamSatTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                giamSatTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else giamSatTieuHuy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);


                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<GC_DNGiamSatTieuHuy> msg = Helpers.Deserialize<MessageSend<GC_DNGiamSatTieuHuy>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<GC_DNGiamSatTieuHuy>(content);
                                            giamSatTieuHuy.MaDoanhNghiep = fback.Agents[0].Identity.ToString();
                                            giamSatTieuHuy.HopDong_ID = HopDong.GetID(fback.ContractReference.Reference.ToString());
                                            giamSatTieuHuy.SoGiayPhep = fback.License.NumberLicense.ToString();
                                            giamSatTieuHuy.NgayGiayPhep = Convert.ToDateTime(fback.License.DateLicense);
                                            giamSatTieuHuy.NgayHetHan = Convert.ToDateTime(fback.License.ExpireDate);
                                            giamSatTieuHuy.ToChucCap = fback.License.AdminitrativeOrgan.ToString();
                                            giamSatTieuHuy.CacBenThamGia = fback.UserAttends.ToString();
                                            giamSatTieuHuy.ThoiGianTieuHuy = Convert.ToDateTime(fback.Time.ToString());
                                            giamSatTieuHuy.DiaDiemTieuHuy = fback.Location.ToString();
                                            giamSatTieuHuy.GhiChuKhac = fback.AdditionalInformationNew.Content.ToString();
                                            if (fback.Scraps.Count >= 1)
                                            {
                                                foreach (HangGSTieuHuy hangGSTieuHuy in giamSatTieuHuy.HangGSTieuHuys)
                                                {
                                                    hangGSTieuHuy.Delete();
                                                }
                                                giamSatTieuHuy.HangGSTieuHuys.Clear();
                                                foreach (Company.KDT.SHARE.Components.Scrap scrap in fback.Scraps)
                                                {
                                                    HangGSTieuHuy hangGSTieuHuy = new HangGSTieuHuy();
                                                    hangGSTieuHuy.STTHang = Convert.ToInt32(scrap.sequence.ToString());
                                                    hangGSTieuHuy.MaHang = scrap.Commodity.Identification.ToString();
                                                    hangGSTieuHuy.LoaiHang = Convert.ToInt32(scrap.Commodity.Type);
                                                    hangGSTieuHuy.MaHS = scrap.Commodity.TariffClassification.ToString();
                                                    hangGSTieuHuy.TenHang = scrap.Commodity.Description.ToString();
                                                    hangGSTieuHuy.SoLuong = Convert.ToDecimal(scrap.GoodsMeasure.Quantity.ToString());
                                                    hangGSTieuHuy.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(scrap.GoodsMeasure.RegisteredMeasureUnit.ToString());
                                                    giamSatTieuHuy.HangGSTieuHuys.Add(hangGSTieuHuy);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return giamSatTieuHuy;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public HopDong ProcessMessageHDGC(HopDong HD)
        {
            try
            {
                StreamWriter write;
                FeedBackContent feedbackContent = null;
                GC_HopDong fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", HD.ID, DeclarationIssuer.HOP_DONG_GIA_CONG);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                       if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            HD.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                HD.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                HD.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else HD.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    write = File.AppendText("Error.txt");
                                    write.WriteLine("--------------------------------");
                                    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                    write.WriteLine("Step 1");
                                    write.Flush();
                                    write.Close();

                                    MessageSend<GC_HopDong> msg = Helpers.Deserialize<MessageSend<GC_HopDong>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<GC_HopDong>(content);
                                            ContractDocument contractDocument = fback.ContractDocument;

                                            HD.SoHopDong = contractDocument.Reference.ToString();
                                            HD.NgayKy = Convert.ToDateTime(contractDocument.Issue);
                                            HD.NgayHetHan = Convert.ToDateTime(contractDocument.Expire);
                                            HD.IsGiaCongNguoc = Convert.ToInt32(contractDocument.IsInverseProcedure);

                                            HD.PTTT_ID = contractDocument.Payment.Method.ToString();

                                            HD.NguyenTe_ID = contractDocument.CurrencyExchange.CurrencyType.ToString();

                                            HD.MaDoanhNghiep = contractDocument.Importer.Identity.ToString();
                                            HD.TenDoanhNghiep = contractDocument.Importer.Name.ToString();
                                            HD.DiaChiDoanhNghiep = contractDocument.Importer.Address.ToString();

                                            HD.DonViDoiTac = contractDocument.Exporter.Identity.ToString();
                                            HD.TenDonViDoiTac = contractDocument.Exporter.Name.ToString();
                                            HD.DiaChiDoiTac = contractDocument.Exporter.Address.ToString();

                                            HD.TongTriGiaTienCong = Convert.ToDouble(contractDocument.CustomsValue.TotalPaymentValue);
                                            HD.TongTriGiaSP = Convert.ToDouble(contractDocument.CustomsValue.TotalProductValue);

                                            HD.NuocThue_ID = fback.ExportationCountry.ToString();

                                            foreach (Company.GC.BLL.KDT.GC.SanPham sanPham in HD.SPCollection)
                                            {
                                                Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                                                SP.Ma = sanPham.Ma;
                                                SP.HopDong_ID = HD.ID;
                                                SP.Delete();
                                                sanPham.Delete();
                                            }
                                            HD.SPCollection.Clear();
                                            foreach (Company.GC.BLL.KDT.GC.NguyenPhuLieu nguyenPhuLieu in HD.NPLCollection)
                                            {
                                                Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                                                NPL.Ma = nguyenPhuLieu.Ma;
                                                NPL.HopDong_ID = HD.ID;
                                                NPL.Delete();
                                                nguyenPhuLieu.Delete();
                                            }
                                            HD.NPLCollection.Clear();
                                            foreach (Company.GC.BLL.KDT.GC.ThietBi thietBi in HD.TBCollection)
                                            {
                                                Company.GC.BLL.GC.ThietBi TB = new Company.GC.BLL.GC.ThietBi();
                                                TB.Ma = thietBi.Ma;
                                                TB.HopDong_ID = HD.ID;
                                                TB.Delete();
                                                thietBi.Delete();
                                            }
                                            foreach (Company.GC.BLL.KDT.GC.HangMau hangMau in HD.HangMauCollection)
                                            {
                                                Company.GC.BLL.GC.HangMau HM = new Company.GC.BLL.GC.HangMau();
                                                HM.Ma = hangMau.Ma;
                                                HM.HopDong_ID = HD.ID;
                                                HM.Delete();
                                                hangMau.Delete();
                                            }
                                            HD.HangMauCollection.Clear();
                                            foreach (Company.GC.BLL.KDT.GC.NhomSanPham nhomSanPham in HD.NhomSPCollection)
                                            {
                                                nhomSanPham.Delete();
                                            }
                                            HD.NhomSPCollection.Clear();
                                            if (contractDocument.Items.Count >= 0)
                                            {
                                                foreach (Item items in contractDocument.Items)
                                                {
                                                    Company.GC.BLL.KDT.GC.NhomSanPham nhomSP = new Company.GC.BLL.KDT.GC.NhomSanPham();
                                                    nhomSP.MaSanPham = items.Identity.ToString();
                                                    nhomSP.TenSanPham = items.Name.ToString();
                                                    nhomSP.TriGia = Convert.ToDouble(items.ProductValue);
                                                    nhomSP.GiaGiaCong = Convert.ToDecimal(items.PaymentValue);
                                                    HD.NhomSPCollection.Add(nhomSP);
                                                }
                                            }
                                            write = File.AppendText("Error.txt");
                                            write.WriteLine("--------------------------------");
                                            write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                            write.WriteLine("Step 2 HD.NhomSPCollection : " + HD.NhomSPCollection.Count.ToString());
                                            write.Flush();
                                            write.Close();
                                            if (contractDocument.Products.Count >= 1)
                                            {
                                                foreach (Product product in contractDocument.Products)
                                                {
                                                    Company.GC.BLL.KDT.GC.SanPham sp = new Company.GC.BLL.KDT.GC.SanPham();
                                                    sp.Ten = product.Commodity.Description.ToString();
                                                    sp.Ma = product.Commodity.Identification.ToString();
                                                    sp.MaHS = product.Commodity.TariffClassification.ToString();
                                                    sp.NhomSanPham_ID = product.Commodity.ProductGroup.ToString();
                                                    sp.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(product.GoodsMeasure.MeasureUnit.ToString());
                                                    HD.SPCollection.Add(sp);
                                                }
                                            }
                                            write = File.AppendText("Error.txt");
                                            write.WriteLine("--------------------------------");
                                            write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                            write.WriteLine("Step 3 HD.SPCollection : " + HD.SPCollection.Count.ToString());
                                            write.Flush();
                                            write.Close();
                                            if (contractDocument.Materials.Count >= 1)
                                            {
                                                foreach (Product material in contractDocument.Materials)
                                                {
                                                    Company.GC.BLL.KDT.GC.NguyenPhuLieu npl = new Company.GC.BLL.KDT.GC.NguyenPhuLieu();
                                                    npl.Ten = material.Commodity.Description.ToString();
                                                    npl.Ma = material.Commodity.Identification.ToString();
                                                    npl.MaHS = material.Commodity.TariffClassification.ToString();
                                                    npl.TuCungUng = material.Commodity.Origin == "2" ? true : false;

                                                    npl.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(material.GoodsMeasure.MeasureUnit.ToString());
                                                    HD.NPLCollection.Add(npl);
                                                }
                                            }
                                            write = File.AppendText("Error.txt");
                                            write.WriteLine("--------------------------------");
                                            write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                            write.WriteLine("Step 4 HD.NPLCollection : " + HD.NPLCollection.Count.ToString());
                                            write.Flush();
                                            write.Close();
                                            if (contractDocument.Equipments.Count >= 1)
                                            {
                                                foreach (Equipment equipment in contractDocument.Equipments)
                                                {
                                                    Company.GC.BLL.KDT.GC.ThietBi tb = new Company.GC.BLL.KDT.GC.ThietBi();
                                                    tb.Ten = equipment.Commodity.Description.ToString();
                                                    tb.Ma = equipment.Commodity.Identification.ToString();
                                                    tb.MaHS = equipment.Commodity.TariffClassification.ToString();

                                                    tb.SoLuongDangKy = Convert.ToDecimal(equipment.GoodsMeasure.Quantity);
                                                    tb.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(equipment.GoodsMeasure.MeasureUnit.ToString());

                                                    tb.NuocXX_ID = equipment.Origin.OriginCountry.ToString();

                                                    tb.NguyenTe_ID = equipment.CurrencyExchange.CurrencyType.ToString();

                                                    tb.DonGia = Convert.ToDouble(equipment.CustomsValue.unitPrice);

                                                    tb.TinhTrang = equipment.Status.ToString();
                                                    HD.TBCollection.Add(tb);
                                                }
                                            }
                                            write = File.AppendText("Error.txt");
                                            write.WriteLine("--------------------------------");
                                            write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                            write.WriteLine("Step 5 HD.TBCollection : " + HD.TBCollection.Count.ToString());
                                            write.Flush();
                                            write.Close();
                                            if (contractDocument.SampleProducts.Count >= 1)
                                            {
                                                foreach (Product sampleProduct in contractDocument.SampleProducts)
                                                {
                                                    Company.GC.BLL.KDT.GC.HangMau hm = new Company.GC.BLL.KDT.GC.HangMau();
                                                    hm.Ten = sampleProduct.Commodity.Description.ToString();
                                                    hm.Ma = sampleProduct.Commodity.Identification.ToString();
                                                    hm.MaHS = sampleProduct.Commodity.TariffClassification.ToString();

                                                    hm.SoLuongDangKy = Convert.ToDecimal(sampleProduct.GoodsMeasure.Quantity);
                                                    hm.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(sampleProduct.GoodsMeasure.MeasureUnit.ToString());
                                                    HD.HangMauCollection.Add(hm);
                                                }
                                            }
                                            write = File.AppendText("Error.txt");
                                            write.WriteLine("--------------------------------");
                                            write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                            write.WriteLine("Step 6 HD.HangMauCollection : " + HD.HangMauCollection.Count.ToString());
                                            write.Flush();
                                            write.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return HD;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public string LoaiPhuKien_GetName(object id)
        {
            if (id.ToString().Trim() == "")
                return "";

            DataTable LoaiPhuKien = Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhuKien.SelectAll();
            DataRow[] rows = LoaiPhuKien.Select(string.Format("ID_LoaiPhuKien = '{0}'", id.ToString().PadRight(3)));
            return rows.Length > 0 ? rows[0][1].ToString() : "";
        }
        public PhuKienDangKy ProcessMessagePhuKien(PhuKienDangKy pkdk)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                GC_PhuKien fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", pkdk.ID, DeclarationIssuer.GC_PHU_KIEN_HOP_DONG);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);

                List<KDT_GC_NguyenPhuLieu> NPLCollection = new List<KDT_GC_NguyenPhuLieu>();
                NPLCollection = KDT_GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(pkdk.HopDong_ID);
                List<KDT_GC_SanPham> SPCollection = new List<KDT_GC_SanPham>();
                SPCollection = KDT_GC_SanPham.SelectCollectionBy_HopDong_ID(pkdk.HopDong_ID);
                List<KDT_GC_ThietBi> TBCollection = new List<KDT_GC_ThietBi>();
                TBCollection = KDT_GC_ThietBi.SelectCollectionBy_HopDong_ID(pkdk.HopDong_ID);
                List<KDT_GC_HangMau> HMCollection = new List<KDT_GC_HangMau>();
                HMCollection = KDT_GC_HangMau.SelectCollectionDynamic("HopDong_ID = "+pkdk.HopDong_ID,"");
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            pkdk.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else pkdk.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<GC_PhuKien> msg = Helpers.Deserialize<MessageSend<GC_PhuKien>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<GC_PhuKien>(content);
                                            pkdk.SoPhuKien = fback.SubContract.Reference.ToString();
                                            pkdk.GhiChu = fback.SubContract.Decription.ToString();
                                            pkdk.NgayPhuKien = Convert.ToDateTime(fback.SubContract.Issue);

                                            if (fback.AdditionalInformations.Count >= 1)
                                            {
                                                foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien it in pkdk.PKCollection)
                                                {
                                                    if (it.ID > 0)
                                                    {
                                                        it.Delete();
                                                    }                                                    
                                                }
                                                pkdk.PKCollection.Clear();

                                                foreach (AdditionalInformation additionalInformation in fback.AdditionalInformations)
                                                {
                                                    string LoaiPhuKien = additionalInformation.Statement.ToString();
                                                    switch (LoaiPhuKien)
                                                    {
                                                        #region Hủy Hợp đồng
                                                        case "101":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.MaPhuKien = "101";
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.InsertUpdate();
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Hủy đăng ký Sản phẩm
                                                        case "102":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Product product in additionalInformation.ContentPK.Declaration.Products)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                                                    HangPK.MaHang = product.Commodity.Identification.ToString();
                                                                    foreach (KDT_GC_SanPham sp in SPCollection)
                                                                    {
                                                                        if (sp.Ma == HangPK.MaHang)
                                                                        {
                                                                            HangPK.TenHang = sp.Ten;
                                                                            HangPK.NhomSP = sp.NhomSanPham_ID;
                                                                            HangPK.DVT_ID = sp.DVT_ID;
                                                                            HangPK.MaHS = sp.MaHS;
                                                                            HangPK.ThongTinCu = sp.Ma;
                                                                            break;
                                                                        }
                                                                    }
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungSP(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Hủy đăng ký Nguyên liệu
                                                        case "103":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);

                                                                foreach (Product product in additionalInformation.ContentPK.Declaration.Materials)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                                                    HangPK.MaHang = product.Commodity.Identification.ToString();
                                                                    foreach (KDT_GC_NguyenPhuLieu npl in NPLCollection)
                                                                    {
                                                                        if (npl.Ma == HangPK.MaHang)
                                                                        {
                                                                            HangPK.TenHang = npl.Ten;
                                                                            HangPK.MaHS = npl.MaHS;
                                                                            HangPK.DVT_ID = npl.DVT_ID;
                                                                            HangPK.ThongTinCu = npl.Ma;
                                                                            HangPK.TinhTrang = "1";
                                                                            break;
                                                                        }
                                                                    }
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungNPL(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Hủy đăng ký Thiết bị
                                                        case "104":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Equipment equipment in additionalInformation.ContentPK.Declaration.Equipments)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                                                    HangPK.MaHang = equipment.Commodity.Identification.ToString();
                                                                    foreach (KDT_GC_ThietBi tb in TBCollection)
                                                                    {
                                                                        if (tb.Ma == HangPK.MaHang)
                                                                        {
                                                                            HangPK.TenHang = tb.Ten;
                                                                            HangPK.MaHS = tb.MaHS;
                                                                            HangPK.ThongTinCu = tb.Ma;
                                                                            HangPK.DVT_ID = tb.DVT_ID;
                                                                            HangPK.NuocXX_ID = tb.NuocXX_ID;
                                                                            HangPK.SoLuong = Convert.ToDecimal(tb.SoLuongDangKy);
                                                                            HangPK.DonGia = Convert.ToDouble(tb.DonGia);
                                                                            HangPK.TriGia = Convert.ToDouble(tb.TriGia);
                                                                            HangPK.NguyenTe_ID = tb.NguyenTe_ID;
                                                                            HangPK.TinhTrang = tb.TinhTrang;
                                                                            break;
                                                                        }
                                                                    }
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungTB(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Hủy đăng ký Hàng mẫu
                                                        case "105":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Equipment equipment in additionalInformation.ContentPK.Declaration.SampleProducts)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                                                    HangPK.MaHang = equipment.Commodity.Identification.ToString();
                                                                    foreach (KDT_GC_HangMau hm in HMCollection)
                                                                    {
                                                                        if (hm.Ma == HangPK.MaHang)
                                                                        {
                                                                            HangPK.TenHang = hm.Ten;
                                                                            HangPK.DVT_ID = hm.DVT_ID;
                                                                            HangPK.MaHS = hm.MaHS;
                                                                            HangPK.ThongTinCu = hm.Ma;
                                                                            HangPK.SoLuong = Convert.ToDecimal(hm.SoLuongDangKy);
                                                                            break;
                                                                        }
                                                                    }
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungHM(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Gia hạn Hợp đồng
                                                        case "201":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                LoaiPK.ThongTinMoi = additionalInformation.ContentPK.Declaration.NewExpire.ToString();
                                                                LoaiPK.ThongTinCu = additionalInformation.ContentPK.Declaration.OldExpire.ToString();
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Sửa thông tin chung Hợp đồng
                                                        case "501":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.MaPhuKien = "501";
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.InsertUpdate();
                                                                DeclarationPhuKien declaration = additionalInformation.ContentPK.Declaration;
                                                                KDT_GC_PhuKien_TTHopDong TTHD = new KDT_GC_PhuKien_TTHopDong();
                                                                TTHD.LoaiPhuKien_ID = LoaiPK.ID;
                                                                TTHD.PTTT_ID = declaration.Payment.ToString();

                                                                TTHD.NguyenTe_ID = declaration.CurrencyExchange.CurrencyType.ToString();

                                                                TTHD.TenDoanhNghiep = declaration.Importer.Name.ToString();
                                                                TTHD.MaDoanhNghiep = declaration.Importer.Identity.ToString();
                                                                TTHD.DiaChiDoanhNghiep = declaration.Importer.Address.ToString();

                                                                TTHD.TenDonViDoiTac = declaration.Exporter.Name.ToString();
                                                                TTHD.DonViDoiTac = declaration.Exporter.Identity.ToString();
                                                                TTHD.DiaChiDoiTac = declaration.Exporter.Address.ToString();

                                                                TTHD.TongTriGiaTienCong = Convert.ToDouble(declaration.CustomsValue.TotalPaymentValue);
                                                                TTHD.TongTriGiaSP = Convert.ToDouble(declaration.CustomsValue.TotalProductValue);
                                                                TTHD.NuocThue_ID = declaration.ExportationCountry.ToString();
                                                                TTHD.GhiChu = declaration.AdditionalInformation.Content.ToString();
                                                                TTHD.InsertUpdate();
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Sửa Sản phẩm
                                                        case "502":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Product product in additionalInformation.ContentPK.Declaration.Products)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                                                    HangPK.ThongTinCu = product.PreIdentification.ToString();

                                                                    HangPK.MaHang = product.Commodity.Identification.ToString();
                                                                    HangPK.TenHang = product.Commodity.Description.ToString();
                                                                    HangPK.MaHS = product.Commodity.TariffClassification.ToString();
                                                                    HangPK.NhomSP = product.Commodity.ProductGroup.ToString();

                                                                    HangPK.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(product.GoodsMeasure.MeasureUnit.ToString());
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungSP(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Sửa Nguyên liệu
                                                        case "503":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);

                                                                foreach (Product product in additionalInformation.ContentPK.Declaration.Materials)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();

                                                                    HangPK.ThongTinCu = product.PreIdentification.ToString();
                                                                    HangPK.MaHang = product.Commodity.Identification.ToString();
                                                                    HangPK.TenHang = product.Commodity.Description.ToString();
                                                                    HangPK.MaHS = product.Commodity.TariffClassification.ToString();
                                                                    HangPK.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(product.GoodsMeasure.MeasureUnit.ToString());

                                                                    HangPK.TinhTrang = product.Commodity.Origin.ToString();

                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungNPL(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Sửa Thiết bị
                                                        case "504":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Equipment equipment in additionalInformation.ContentPK.Declaration.Equipments)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                                                    HangPK.ThongTinCu = equipment.PreIdentification.ToString();
                                                                    HangPK.MaHang = equipment.Commodity.Identification.ToString();
                                                                    HangPK.TenHang = equipment.Commodity.Description.ToString();
                                                                    HangPK.MaHS = equipment.Commodity.TariffClassification.ToString();
                                                                    HangPK.SoLuong = Convert.ToDecimal(equipment.GoodsMeasure.Quantity);
                                                                    HangPK.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(equipment.GoodsMeasure.MeasureUnit.ToString());
                                                                    HangPK.NguyenTe_ID = equipment.CurrencyExchange.CurrencyType.ToString();
                                                                    HangPK.NuocXX_ID = equipment.Origin.OriginCountry.ToString();
                                                                    HangPK.DonGia = Convert.ToDouble(equipment.CustomsValue.unitPrice);
                                                                    HangPK.TriGia = Convert.ToDouble(Convert.ToDouble(HangPK.SoLuong) * HangPK.DonGia);
                                                                    HangPK.TinhTrang = equipment.Status.ToString();
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungTB(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Sửa Hàng mẫu
                                                        case "505":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Equipment equipment in additionalInformation.ContentPK.Declaration.SampleProducts)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                                                    HangPK.ThongTinCu = equipment.PreIdentification.ToString();
                                                                    HangPK.MaHang = equipment.Commodity.Identification.ToString();
                                                                    HangPK.TenHang = equipment.Commodity.Description.ToString();
                                                                    HangPK.MaHS = equipment.Commodity.TariffClassification.ToString();
                                                                    HangPK.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(equipment.GoodsMeasure.MeasureUnit.ToString());
                                                                    HangPK.SoLuong = Convert.ToDecimal(equipment.GoodsMeasure.Quantity);
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungHM(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Bổ sung Sản phẩm
                                                        case "802":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Product product in additionalInformation.ContentPK.Declaration.Products)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();

                                                                    HangPK.MaHang = product.Commodity.Identification.ToString();
                                                                    HangPK.TenHang = product.Commodity.Description.ToString();
                                                                    HangPK.MaHS = product.Commodity.TariffClassification.ToString();
                                                                    HangPK.NhomSP = product.Commodity.ProductGroup.ToString();

                                                                    HangPK.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(product.GoodsMeasure.MeasureUnit.ToString());
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungSP(pkdk.ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Bổ sung Nguyên liệu
                                                        case "803":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Product product in additionalInformation.ContentPK.Declaration.Materials)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();

                                                                    HangPK.MaHang = product.Commodity.Identification.ToString();
                                                                    HangPK.TenHang = product.Commodity.Description.ToString();
                                                                    HangPK.MaHS = product.Commodity.TariffClassification.ToString();
                                                                    HangPK.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(product.GoodsMeasure.MeasureUnit.ToString());

                                                                    HangPK.TinhTrang = product.Commodity.Origin.ToString();

                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungNPL(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Bổ sung Thiết bị
                                                        case "804":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Equipment equipment in additionalInformation.ContentPK.Declaration.Equipments)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                                                    HangPK.MaHang = equipment.Commodity.Identification.ToString();
                                                                    HangPK.TenHang = equipment.Commodity.Description.ToString();
                                                                    HangPK.MaHS = equipment.Commodity.TariffClassification.ToString();
                                                                    HangPK.SoLuong = Convert.ToDecimal(equipment.GoodsMeasure.Quantity);
                                                                    HangPK.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(equipment.GoodsMeasure.MeasureUnit.ToString());
                                                                    HangPK.NguyenTe_ID = equipment.CurrencyExchange.CurrencyType.ToString();
                                                                    HangPK.NuocXX_ID = equipment.Origin.OriginCountry.ToString();
                                                                    HangPK.DonGia = Convert.ToDouble(equipment.CustomsValue.unitPrice);
                                                                    HangPK.TriGia = Convert.ToDouble(Convert.ToDouble(HangPK.SoLuong) * HangPK.DonGia);
                                                                    HangPK.TinhTrang = equipment.Status.ToString();
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungTB(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        #region Bổ sung Hàng mẫu
                                                        case "805":
                                                            {
                                                                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                                                                LoaiPK.Master_ID = pkdk.ID;
                                                                LoaiPK.MaPhuKien = LoaiPhuKien;
                                                                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                                                                foreach (Equipment equipment in additionalInformation.ContentPK.Declaration.SampleProducts)
                                                                {
                                                                    Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                                                    HangPK.MaHang = equipment.Commodity.Identification.ToString();
                                                                    HangPK.TenHang = equipment.Commodity.Description.ToString();
                                                                    HangPK.MaHS = equipment.Commodity.TariffClassification.ToString();
                                                                    HangPK.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(equipment.GoodsMeasure.MeasureUnit.ToString());
                                                                    HangPK.SoLuong = Convert.ToDecimal(equipment.GoodsMeasure.Quantity);
                                                                    LoaiPK.HPKCollection.Add(HangPK);
                                                                }
                                                                LoaiPK.InsertUpdateBoSungHM(pkdk.HopDong_ID);
                                                                pkdk.PKCollection.Add(LoaiPK);
                                                                break;
                                                            }
                                                        #endregion

                                                        default:
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return pkdk;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public DinhMucDangKy ProcessMessageDinhMuc(DinhMucDangKy dmDangKy)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.GC_DinhMuc fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", dmDangKy.ID, DeclarationIssuer.GC_DINH_MUC);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition,orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            dmDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.GC_DinhMuc> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.GC_DinhMuc>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.GC_DinhMuc>(content);
                                            dmDangKy.GUIDSTR = item.ReferenceID.ToString();
                                            dmDangKy.LoaiDinhMuc = fback.ContractReference.ProductionNormType == null ? 0 : Convert.ToInt32(fback.ContractReference.ProductionNormType);
                                            dmDangKy.NamTN = dmDangKy.NgayTiepNhan.Year.ToString();
                                            if (ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                            {
                                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                            }
                                            else
                                            {
                                                dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                            }
                                            if (fback.ProductionNorms.Count >= 1)
                                            {
                                                foreach (Company.GC.BLL.KDT.GC.DinhMuc items in dmDangKy.DMCollection)
                                                {
                                                    items.Delete(dmDangKy.ID_HopDong);
                                                }
                                                dmDangKy.DMCollection.Clear();
                                                foreach (ProductionNorm productionNorm in fback.ProductionNorms)
                                                {
                                                    int i = 1;
                                                    foreach (MaterialsNorm materialsNorm in productionNorm.MaterialsNorms)
                                                    {
                                                        Company.GC.BLL.KDT.GC.DinhMuc dm = new Company.GC.BLL.KDT.GC.DinhMuc();
                                                        dm.STT = i;
                                                        dm.HopDong_ID = dmDangKy.ID_HopDong;
                                                        dm.MaSanPham = productionNorm.Product.Commodity.Identification.ToString();
                                                        dm.TenSanPham = productionNorm.Product.Commodity.Description;
                                                        dm.MaDDSX = productionNorm.Product.Commodity.ProductCtrlNo.ToString();
                                                        dm.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(productionNorm.Product.GoodsMeasure.MeasureUnit.ToString());

                                                        dm.MaNguyenPhuLieu = materialsNorm.Material.Commodity.Identification.ToString();
                                                        dm.TenNPL = materialsNorm.Material.Commodity.Description.ToString();
                                                        dm.DinhMucSuDung = Convert.ToDecimal(materialsNorm.Norm.ToString());
                                                        dm.TyLeHaoHut = Convert.ToDecimal(materialsNorm.Loss.ToString());
                                                        dm.GhiChu = materialsNorm.Description.ToString();
                                                        dm.DVT = VNACCS_Mapper.GetCodeV4DVT(materialsNorm.Material.GoodsMeasure.MeasureUnit.ToString());

                                                        dmDangKy.DMCollection.Add(dm);
                                                        i++;
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return dmDangKy;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public KDT_VNACCS_BaoCaoQuyetToan_TT39 ProcessMessageBCQT(KDT_VNACCS_BaoCaoQuyetToan_TT39 BCQT)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", BCQT.ID, DeclarationIssuer.GoodsItemNew);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            BCQT.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                BCQT.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                BCQT.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else BCQT.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS>(content);
                                            BCQT.NgayBatDauBC = Convert.ToDateTime(fback.StartDate);
                                            BCQT.NgayKetThucBC = Convert.ToDateTime(fback.FinishDate);
                                            BCQT.LoaiBC = Convert.ToInt32(fback.Type);
                                            BCQT.LoaiSua = Convert.ToInt32(fback.UpdateType);
                                            BCQT.MaDoanhNghiep = fback.Importer.Identity.ToString();
                                            BCQT.MaHQ = fback.DeclarationOffice.ToString();
                                            BCQT.GhiChuKhac = fback.AdditionalInformation.Content.ToString();
                                            if (fback.ContractReferences.ContractReference.Count >= 1)
                                            {
                                                foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail items in BCQT.HopDongCollection)
                                                {
                                                    items.DeleteFull();
                                                }
                                                BCQT.HopDongCollection.Clear();
                                                foreach (ContractReference contractReference in fback.ContractReferences.ContractReference)
                                                {
                                                    KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail hopDong_Detail = new KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail();
                                                    hopDong_Detail.STT = contractReference.Sequence;
                                                    hopDong_Detail.SoHopDong = contractReference.Reference.ToString();
                                                    hopDong_Detail.NgayHopDong = Convert.ToDateTime(contractReference.Issue);
                                                    hopDong_Detail.MaHQ = contractReference.DeclarationOffice.ToString();
                                                    hopDong_Detail.NgayHetHan = Convert.ToDateTime(contractReference.Expire);
                                                    hopDong_Detail.GhiChuKhac = contractReference.AdditionalInformation.Content.ToString();
                                                    hopDong_Detail.ID_HopDong = HopDong.GetID(hopDong_Detail.SoHopDong);
                                                    foreach (Company.KDT.SHARE.Components.GoodsItem goodsItem in contractReference.GoodsItems.GoodsItem)
                                                    {
                                                        KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail Detail = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
                                                        Detail.STT = Convert.ToDecimal(goodsItem.Sequence);
                                                        Detail.TenHangHoa = goodsItem.DescriptionMaterial.ToString();
                                                        Detail.MaHangHoa = goodsItem.IdentificationMaterial.ToString();
                                                        Detail.DVT = goodsItem.MeasureUnitMaterial.ToString();
                                                        Detail.LuongTonDauKy = goodsItem.QuantityBeginMaterial;
                                                        Detail.LuongNhapTrongKy = goodsItem.QuantityImportMaterial;
                                                        Detail.LuongTaiXuat = goodsItem.QuantityReExportMaterial;
                                                        Detail.LuongChuyenMucDichSD = goodsItem.QuantityRePurposeMaterial;
                                                        Detail.LuongXuatKhau = goodsItem.QuantityExportProduct;
                                                        Detail.LuongXuatKhac = goodsItem.QuantityExportOther;
                                                        Detail.LuongTonCuoiKy = goodsItem.QuantityExcessMaterial;
                                                        Detail.GhiChu = goodsItem.Content;
                                                        hopDong_Detail.HangHoaCollection.Add(Detail);
                                                    }
                                                    BCQT.HopDongCollection.Add(hopDong_Detail);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return BCQT;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport ProcessMessageBCCT(Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport BCCT)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", BCCT.ID, DeclarationIssuer.TotalInventoryReport_GC);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            BCCT.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                BCCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                BCCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else BCCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS>(content);
                                            BCCT.MaHaiQuan = fback.Importer.Identity.ToString();
                                            BCCT.MaHaiQuan = fback.DeclarationOffice.ToString();
                                            BCCT.NgayChotTon = Convert.ToDateTime(fback.FinishDate);
                                            BCCT.LoaiSua = Convert.ToDecimal(fback.UpdateType);
                                            BCCT.LoaiBaoCao = Convert.ToDecimal(fback.Type);

                                            if (fback.ContractReferences.ContractReference.Count >= 1)
                                            {
                                                foreach (Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_ContractReference items in BCCT.ContractCollection)
                                                {
                                                    items.DeleteFull();
                                                }
                                                BCCT.ContractCollection.Clear();

                                                foreach (ContractReference contractReference in fback.ContractReferences.ContractReference)
                                                {
                                                    Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_ContractReference contract = new Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_ContractReference();
                                                    contract.SoHopDong = contractReference.Reference.ToString();
                                                    contract.NgayHopDong = Convert.ToDateTime(contractReference.Issue);
                                                    contract.MaHQ = contractReference.DeclarationOffice.ToString();
                                                    contract.NgayHetHan = Convert.ToDateTime(contractReference.Expire);
                                                    foreach (GoodsItem goodsItem in contractReference.GoodsItems.GoodsItem)
                                                    {
                                                        Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail Detail = new Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail();
                                                        Detail.TenHangHoa = goodsItem.Description.ToString();
                                                        Detail.MaHangHoa = goodsItem.Identification.ToString();
                                                        Detail.SoLuongTonKhoSoSach = Convert.ToDecimal(goodsItem.Quantity1);
                                                        Detail.SoLuongTonKhoThucTe = Convert.ToDecimal(goodsItem.Quantity2);
                                                        Detail.DVT = VNACCS_Mapper.GetCodeV4DVT(goodsItem.MeasureUnit.ToString());
                                                        contract.DetailCollection.Add(Detail);
                                                    }
                                                    BCCT.ContractCollection.Add(contract);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return BCCT;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public KDT_VNACCS_BaoCaoQuyetToan_MMTB ProcessMessageBCQTMMTB(KDT_VNACCS_BaoCaoQuyetToan_MMTB BCQTMMTB)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.Messages.GoodsItem.ContractReference_VNACCS fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", BCQTMMTB.ID, DeclarationIssuer.ContractReference);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            BCQTMMTB.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                BCQTMMTB.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                BCQTMMTB.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else BCQTMMTB.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.Messages.GoodsItem.ContractReference_VNACCS> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.Messages.GoodsItem.ContractReference_VNACCS>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.Messages.GoodsItem.ContractReference_VNACCS>(content);
                                            if (fback.ContractReferences.ContractReference.Count >= 1)
                                            {
                                                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail items in BCQTMMTB.GoodItemCollection)
                                                {
                                                    items.DeleteFull();
                                                }
                                                BCQTMMTB.GoodItemCollection.Clear();
                                                foreach (ContractReference contractReference in fback.ContractReferences.ContractReference)
                                                {
                                                    KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail hopDong_Detail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();
                                                    hopDong_Detail.STT = contractReference.Sequence;
                                                    hopDong_Detail.SoHopDong = contractReference.Reference.ToString();
                                                    hopDong_Detail.NgayHopDong = Convert.ToDateTime(fback.Issue);
                                                    hopDong_Detail.MaHQ = contractReference.DeclarationOffice.ToString();
                                                    hopDong_Detail.NgayHetHan = Convert.ToDateTime(contractReference.Expire);
                                                    hopDong_Detail.GhiChuKhac = contractReference.AdditionalInformation.Content;
                                                    hopDong_Detail.ID_HopDong = HopDong.GetID(hopDong_Detail.SoHopDong);
                                                    foreach (GoodsItem goodsItem in contractReference.GoodsItems.GoodsItem)
                                                    {
                                                        KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail Detail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail();
                                                        Detail.STT = Convert.ToDecimal(goodsItem.Commodity.Sequence);
                                                        Detail.TenHangHoa = goodsItem.Commodity.Description.ToString();
                                                        Detail.MaHangHoa = goodsItem.Commodity.Identification.ToString();
                                                        Detail.MaHS = Convert.ToDecimal(goodsItem.Commodity.TariffClassification.ToString());
                                                        Detail.GhiChu = goodsItem.Commodity.Content.ToString();
                                                        Detail.LuongTamNhap = Convert.ToDecimal(goodsItem.GoodsMeasure.QuantityTempImport);
                                                        Detail.LuongTaiXuat = Convert.ToDecimal(goodsItem.GoodsMeasure.QuantityReExport);
                                                        Detail.LuongChuyenTiep = goodsItem.GoodsMeasure.QuantityForward.Quantity;
                                                        Detail.SoHopDong = goodsItem.GoodsMeasure.QuantityForward.Reference.ToString();
                                                        Detail.NgayHopDong = Convert.ToDateTime(goodsItem.GoodsMeasure.QuantityForward.Issue);
                                                        Detail.MaHQ = goodsItem.GoodsMeasure.QuantityForward.DeclarationOffice.ToString();
                                                        Detail.NgayHetHan = Convert.ToDateTime(goodsItem.GoodsMeasure.QuantityForward.Expire);
                                                        Detail.LuongConLai = Convert.ToDecimal(goodsItem.GoodsMeasure.quantityExcess);
                                                        Detail.DVT = goodsItem.GoodsMeasure.MeasureUnit.ToString();
                                                        Detail.ID_HopDong = HopDong.GetID(Detail.SoHopDong);
                                                        hopDong_Detail.GoodItemDetailCollection.Add(Detail);
                                                    }
                                                    BCQTMMTB.GoodItemCollection.Add(hopDong_Detail);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return BCQTMMTB;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
#endif
#if SXXK_V4
        public NguyenPhuLieuDangKy ProcessMessageNPL(NguyenPhuLieuDangKy nplDangky)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", nplDangky.ID, DeclarationIssuer.SXXK_NPL);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            nplDangky.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                nplDangky.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                nplDangky.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else nplDangky.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                        }
                        else
                        {
                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu>>(item.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu>(content);
                                            if (fback.Product.Count >= 1)
                                            {
                                                foreach (NguyenPhuLieu items in nplDangky.NPLCollection)
                                                {
                                                    items.Delete(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                                                }
                                                nplDangky.NPLCollection.Clear();
                                                int i = 1;
                                                foreach (Product product in fback.Product)
                                                {
                                                    NguyenPhuLieu npl = new NguyenPhuLieu();
                                                    npl.STT = npl.STTHang = i;
                                                    npl.Ma = product.Commodity.Identification.ToString();
                                                    npl.Ten = product.Commodity.Description.ToString();
                                                    npl.MaHS = product.Commodity.TariffClassification.ToString();
                                                    npl.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(product.GoodsMeasure.MeasureUnit.ToString());
                                                    nplDangky.NPLCollection.Add(npl);
                                                    i++;
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return nplDangky;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public SanPhamDangKy ProcessMessageSP(SanPhamDangKy spDangky)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.SXXK_SanPham fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", spDangky.ID, DeclarationIssuer.SXXK_SP);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            spDangky.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                spDangky.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                spDangky.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else spDangky.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                        }
                        else
                        {
                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.SXXK_SanPham> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.SXXK_SanPham>>(item.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.SXXK_SanPham>(content);
                                            if (fback.Product.Count >= 1)
                                            {
                                                foreach (SanPham items in spDangky.SPCollection)
                                                {
                                                    items.Delete(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                                                }
                                                spDangky.SPCollection.Clear();
                                                int i = 1;
                                                foreach (Product product in fback.Product)
                                                {
                                                    SanPham sp = new SanPham();
                                                    sp.STTHang = i;
                                                    sp.Ma = product.Commodity.Identification.ToString();
                                                    sp.Ten = product.Commodity.Description.ToString();
                                                    sp.MaHS = product.Commodity.TariffClassification.ToString();
                                                    sp.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(product.GoodsMeasure.MeasureUnit.ToString());
                                                    spDangky.SPCollection.Add(sp);
                                                    i++;
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return spDangky;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public DinhMucDangKy ProcessMessageDinhMuc(DinhMucDangKy dmDangKy)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.SXXK_DinhMucSP fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", dmDangKy.ID, DeclarationIssuer.SXXK_DINH_MUC);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            dmDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                        }
                        else
                        {
                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.SXXK_DinhMucSP> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.SXXK_DinhMucSP>>(item.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.SXXK_DinhMucSP>(content);
                                            if (fback.ProductionNorm.Count >= 1)
                                            {
                                                foreach (DinhMuc items in dmDangKy.DMCollection)
                                                {
                                                    items.Delete();
                                                }
                                                dmDangKy.DMCollection.Clear();
                                                dmDangKy.LoaiDinhMuc = Convert.ToInt32(fback.ProductionNormType.ToString());
                                                foreach (ProductionNorm productionNorm in fback.ProductionNorm)
                                                {
                                                    int i = 1;
                                                    foreach (MaterialsNorm materialsNorm in productionNorm.MaterialsNorms)
                                                    {
                                                        DinhMuc dm = new DinhMuc();
                                                        dm.STT = i;
                                                        dm.MaSanPham = productionNorm.Product.Commodity.Identification.ToString();
                                                        dm.TenSP = productionNorm.Product.Commodity.Description;
                                                        dm.MaDinhDanhLenhSX = productionNorm.Product.Commodity.ProductCtrlNo.ToString();
                                                        dm.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(productionNorm.Product.GoodsMeasure.MeasureUnit.ToString());

                                                        dm.MaNguyenPhuLieu = materialsNorm.Material.Commodity.Identification.ToString();
                                                        dm.TenNPL = materialsNorm.Material.Commodity.Description.ToString();
                                                        dm.DinhMucSuDung = Convert.ToDecimal(materialsNorm.Norm.Replace(".",","));
                                                        dm.TyLeHaoHut = Convert.ToDecimal(materialsNorm.Loss.Replace(".", ","));
                                                        dm.GhiChu = materialsNorm.Description.ToString();
                                                        dm.DVT = VNACCS_Mapper.GetCodeV4DVT(materialsNorm.Material.GoodsMeasure.MeasureUnit.ToString());

                                                        dmDangKy.DMCollection.Add(dm);
                                                        i++;
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return dmDangKy;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public KDT_SXXK_DinhMucThucTeDangKy ProcessMessageDinhMucThucTe(KDT_SXXK_DinhMucThucTeDangKy dmDangKy)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.SXXK_DinhMucSP fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", dmDangKy.ID, DeclarationIssuer.SXXK_DINH_MUC);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            dmDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else dmDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                        }
                        else
                        {
                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.SXXK_DinhMucSP> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.SXXK_DinhMucSP>>(item.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.SXXK_DinhMucSP>(content);
                                            if (fback.ProductionNorm.Count >= 1)
                                            {
                                                foreach (KDT_SXXK_DinhMucThucTe_SP items in dmDangKy.SPCollection)
                                                {
                                                    foreach (KDT_SXXK_DinhMucThucTe_DinhMuc it in items.DMCollection)
                                                    {
                                                        it.Delete();
                                                    }
                                                    items.Delete();
                                                }
                                                dmDangKy.SPCollection.Clear();
                                                foreach (ProductionNorm productionNorm in fback.ProductionNorm)
                                                {
                                                    int i = 1;
                                                    foreach (MaterialsNorm materialsNorm in productionNorm.MaterialsNorms)
                                                    {
                                                        KDT_SXXK_DinhMucThucTe_SP sp = new KDT_SXXK_DinhMucThucTe_SP();                                                       
                                                        sp.MaSanPham = productionNorm.Product.Commodity.Identification.ToString();
                                                        sp.TenSanPham = productionNorm.Product.Commodity.Description;
                                                        sp.MaHS = productionNorm.Product.Commodity.TariffClassification.ToString();
                                                        sp.DVT_ID = VNACCS_Mapper.GetCodeV4DVT(productionNorm.Product.GoodsMeasure.MeasureUnit.ToString());
                                                        if (dmDangKy.SPCollection.Count==0)
                                                        {
                                                            dmDangKy.SPCollection.Add(sp);
                                                        }
                                                        else
                                                        {
                                                            bool isExits = false;
                                                            foreach (KDT_SXXK_DinhMucThucTe_SP entity in dmDangKy.SPCollection)
                                                            {
                                                                if (entity.MaSanPham == sp.MaSanPham)
                                                                {
                                                                    isExits = true;
                                                                }
                                                            }
                                                            if (!isExits)
                                                            {
                                                                dmDangKy.SPCollection.Add(sp);
                                                            }
                                                        }
                                                        foreach (KDT_SXXK_DinhMucThucTe_SP entity in dmDangKy.SPCollection)
                                                        {
                                                            if (sp.MaSanPham == entity.MaSanPham)
                                                            {
                                                                KDT_SXXK_DinhMucThucTe_DinhMuc dm = new KDT_SXXK_DinhMucThucTe_DinhMuc();
                                                                dm.MaSanPham = entity.MaSanPham;
                                                                dm.TenSanPham = entity.TenSanPham;
                                                                dm.DVT_SP = entity.DVT_ID;
                                                                dm.STT = i;
                                                                dm.MaNPL = materialsNorm.Material.Commodity.Identification.ToString();
                                                                dm.TenNPL = materialsNorm.Material.Commodity.Description.ToString();
                                                                dm.DinhMucSuDung = Convert.ToDecimal(materialsNorm.Norm.ToString());
                                                                dm.TyLeHaoHut = Convert.ToDecimal(materialsNorm.Loss.ToString());
                                                                dm.GhiChu = materialsNorm.Description.ToString();
                                                                dm.DVT_NPL = VNACCS_Mapper.GetCodeV4DVT(materialsNorm.Material.GoodsMeasure.MeasureUnit.ToString());
                                                                entity.DMCollection.Add(dm);
                                                                i++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return dmDangKy;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New ProcessMessageBCQT(KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New BCQT)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", BCQT.ID, DeclarationIssuer.GoodsItem_SXXK);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            BCQT.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                BCQT.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                BCQT.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else BCQT.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                        }
                        else
                        {
                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS>>(item.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS>(content);
                                            BCQT.NgayBatDauBC = Convert.ToDateTime(fback.StartDate);
                                            BCQT.NgayKetThucBC = Convert.ToDateTime(fback.FinishDate);
                                            BCQT.LoaiBaoCao = Convert.ToInt32(fback.Type);
                                            BCQT.LoaiSua = Convert.ToInt32(fback.UpdateType);
                                            BCQT.MaDoanhNghiep = fback.Importer.Identity.ToString();
                                            BCQT.MaHQ = fback.DeclarationOffice.ToString();
                                            BCQT.GhiChuKhac = fback.AdditionalInformation.Content.ToString();
                                            if (fback.GoodsItems.GoodsItem.Count >= 1)
                                            {
                                                foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New items in BCQT.GoodItemsCollection)
                                                {
                                                    items.Delete();
                                                }
                                                BCQT.GoodItemsCollection.Clear();
                                                foreach (Company.KDT.SHARE.Components.GoodsItem goodsItem in fback.GoodsItems.GoodsItem)
                                                {
                                                    KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New Detail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
                                                    Detail.STT = Convert.ToDecimal(goodsItem.Sequence);
                                                    Detail.TenHangHoa = goodsItem.DescriptionMaterial.ToString();
                                                    Detail.MaHangHoa = goodsItem.IdentificationMaterial.ToString();
                                                    Detail.DVT = goodsItem.MeasureUnitMaterial.ToString();
                                                    Detail.TonDauKy = goodsItem.QuantityBeginMaterial;
                                                    Detail.NhapTrongKy = goodsItem.QuantityImportMaterial;
                                                    Detail.TaiXuat = goodsItem.QuantityReExportMaterial;
                                                    Detail.ChuyenMDSD = goodsItem.QuantityRePurposeMaterial;
                                                    Detail.XuatTrongKy = goodsItem.QuantityExportProduct;
                                                    Detail.XuatKhac = goodsItem.QuantityExportOther;
                                                    Detail.TonCuoiKy = goodsItem.QuantityExcessMaterial;
                                                    Detail.GhiChu = goodsItem.Content;
                                                    BCQT.GoodItemsCollection.Add(Detail);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return BCQT;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public KDT_VNACCS_TotalInventoryReport ProcessMessageBCCT(KDT_VNACCS_TotalInventoryReport BCCT)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", BCCT.ID, DeclarationIssuer.TotalInventoryReport);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            BCCT.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                BCCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                BCCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else BCCT.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                        }
                        else
                        {
                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS> msg = Helpers.Deserialize<MessageSend<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS>>(item.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Company.KDT.SHARE.Components.Messages.Vouchers.GoodsItems_VNACCS>(content);
                                            BCCT.MaHaiQuan = fback.Importer.Identity.ToString();
                                            BCCT.MaHaiQuan = fback.DeclarationOffice.ToString();
                                            BCCT.NgayChotTon = Convert.ToDateTime(fback.FinishDate);
                                            BCCT.LoaiSua = Convert.ToDecimal(fback.UpdateType);
                                            BCCT.LoaiBaoCao = Convert.ToDecimal(fback.Type);
                                            if (fback.GoodsItems.GoodsItem.Count >= 1)
                                            {
                                                foreach (KDT_VNACCS_TotalInventoryReport_Detail items in BCCT.DetailCollection)
                                                {
                                                    items.Delete();
                                                }
                                                foreach (GoodsItem goodsItem in fback.GoodsItems.GoodsItem)
                                                {
                                                    KDT_VNACCS_TotalInventoryReport_Detail Detail = new KDT_VNACCS_TotalInventoryReport_Detail();
                                                    Detail.TenHangHoa = goodsItem.Description.ToString();
                                                    Detail.MaHangHoa = goodsItem.Identification.ToString();
                                                    Detail.SoLuongTonKhoSoSach = Convert.ToDecimal(goodsItem.Quantity1);
                                                    Detail.SoLuongTonKhoThucTe = Convert.ToDecimal(goodsItem.Quantity2);
                                                    Detail.DVT = goodsItem.MeasureUnit.ToString();
                                                    BCCT.DetailCollection.Add(Detail);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return BCCT;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
#endif
        public T_KDT_VNACCS_WarehouseImport ProcessMessageWarehouseImport(T_KDT_VNACCS_WarehouseImport warehouseImport)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                GC_ImportWareHouse fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", warehouseImport.ID, DeclarationIssuer.ImportWareHouse);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            warehouseImport.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                warehouseImport.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                warehouseImport.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else warehouseImport.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<GC_ImportWareHouse> msg = Helpers.Deserialize<MessageSend<GC_ImportWareHouse>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<GC_ImportWareHouse>(content);
                                            if (fback.AdditionalDocuments.AdditionalDocument.Count >= 1)
                                            {
                                                foreach (T_KDT_VNACCS_WarehouseImport_Detail Detail in warehouseImport.WarehouseImportDetailsCollection)
                                                {
                                                    foreach (T_KDT_VNACCS_WarehouseImport_GoodsDetail GoodsDetail in Detail.Collection)
                                                    {
                                                        GoodsDetail.Delete();
                                                    }
                                                    Detail.Delete();
                                                }
                                                warehouseImport.WarehouseImportDetailsCollection.Clear();

                                                warehouseImport.MaHQ = fback.DeclarationOffice.ToString();
                                                warehouseImport.NgayBatDauBC = Convert.ToDateTime(fback.StartDate);
                                                warehouseImport.NgayKetthucBC = Convert.ToDateTime(fback.FinishDate);
                                                warehouseImport.GuidStr = fback.Reference.ToString();
                                                warehouseImport.MaKho = fback.Warehouse.Identity.ToString();
                                                warehouseImport.TenKho = fback.Warehouse.Name.ToString();
                                                warehouseImport.MaDoanhNghiep = fback.Agents[0].Identity.ToString();
                                                foreach (PNK_AdditionalDocument additionalDocument in fback.AdditionalDocuments.AdditionalDocument)
                                                {
                                                    T_KDT_VNACCS_WarehouseImport_Detail Detail = new T_KDT_VNACCS_WarehouseImport_Detail();
                                                    Detail.STT = Convert.ToDecimal(additionalDocument.Sequence.ToString());
                                                    Detail.SoPhieuNhap = additionalDocument.Identification.ToString();
                                                    Detail.NgayPhieuNhap = Convert.ToDateTime(additionalDocument.Issue.ToString());
                                                    Detail.TenNguoiGiaoHang = additionalDocument.NameConsignee.ToString();
                                                    Detail.MaNguoiGiaoHang = additionalDocument.IdentityConsignor.ToString();
                                                    foreach (PNK_CustomsGoodsItem customsGoodsItem in additionalDocument.CustomsGoodsItem)
                                                    {
                                                        T_KDT_VNACCS_WarehouseImport_GoodsDetail goodsDetail = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();
                                                        warehouseImport.SoTKChungTu = customsGoodsItem.DeclarationDocument.Reference.ToString();
                                                        warehouseImport.Loai = Convert.ToDecimal(customsGoodsItem.DeclarationDocument.Type.ToString());
#if GC_V4
                                                        warehouseImport.SoHopDong = customsGoodsItem.ContractReference.Reference.ToString();
                                                        warehouseImport.NgayHopDong = Convert.ToDateTime(customsGoodsItem.ContractReference.Issue.ToString());
                                                        warehouseImport.NgayHetHanHD = Convert.ToDateTime(customsGoodsItem.ContractReference.Expire.ToString());
                                                        warehouseImport.MaHQTiepNhanHD = customsGoodsItem.ContractReference.DeclarationOffice.ToString();
#endif
                                                        goodsDetail.STT = Convert.ToDecimal(customsGoodsItem.Commodity.Sequence.ToString());
                                                        goodsDetail.TenHangHoa = customsGoodsItem.Commodity.Description.ToString();
                                                        goodsDetail.MaHangHoa = customsGoodsItem.Commodity.Identification.ToString();
                                                        goodsDetail.LoaiHangHoa = Convert.ToDecimal(customsGoodsItem.Commodity.Type.ToString());
                                                        goodsDetail.MaDinhDanhSX = customsGoodsItem.Commodity.ProductCtrlNo.ToString();
                                                        goodsDetail.NguonNhap = Convert.ToDecimal(customsGoodsItem.Commodity.Origin.ToString());
                                                        goodsDetail.SoLuongDuKienNhap = Convert.ToDecimal(customsGoodsItem.GoodsMeasure.DocQuantity.ToString());
                                                        goodsDetail.SoLuongThucNhap = Convert.ToDecimal(customsGoodsItem.GoodsMeasure.ActualQuantity.ToString());
                                                        goodsDetail.DVT = VNACCS_Mapper.GetCodeV4DVT(customsGoodsItem.GoodsMeasure.MeasureUnit.ToString());
                                                        Detail.Collection.Add(goodsDetail);
                                                    }
                                                    warehouseImport.WarehouseImportDetailsCollection.Add(Detail);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return warehouseImport;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public T_KDT_VNACCS_WarehouseExport ProcessMessageWarehouseExport(T_KDT_VNACCS_WarehouseExport warehouseExport)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                GC_ImportWareHouse fback = null;
                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", warehouseExport.ID, DeclarationIssuer.EXportWareHouse);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            warehouseExport.SoTN = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                warehouseExport.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                warehouseExport.NgayTN = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else warehouseExport.NgayTN = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);

                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.DaKhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<GC_ImportWareHouse> msg = Helpers.Deserialize<MessageSend<GC_ImportWareHouse>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<GC_ImportWareHouse>(content);
                                            if (fback.AdditionalDocuments.AdditionalDocument.Count >= 1)
                                            {
                                                foreach (T_KDT_VNACCS_WarehouseExport_Detail Detail in warehouseExport.WarehouseExportCollection)
                                                {
                                                    foreach (T_KDT_VNACCS_WarehouseExport_GoodsDetail GoodsDetail in Detail.Collection)
                                                    {
                                                        GoodsDetail.Delete();
                                                    }
                                                    Detail.Delete();
                                                }
                                                warehouseExport.WarehouseExportCollection.Clear();

                                                warehouseExport.MaHQ = fback.DeclarationOffice.ToString();
                                                warehouseExport.NgayBatDauBC = Convert.ToDateTime(fback.StartDate);
                                                warehouseExport.NgayKetthucBC = Convert.ToDateTime(fback.FinishDate);
                                                warehouseExport.GuidStr = fback.Reference.ToString();
                                                warehouseExport.MaKho = fback.Warehouse.Identity.ToString();
                                                warehouseExport.TenKho = fback.Warehouse.Name.ToString();
                                                warehouseExport.MaDoanhNghiep = fback.Agents[0].Identity.ToString();
                                                foreach (PNK_AdditionalDocument additionalDocument in fback.AdditionalDocuments.AdditionalDocument)
                                                {
                                                    T_KDT_VNACCS_WarehouseExport_Detail Detail = new T_KDT_VNACCS_WarehouseExport_Detail();
                                                    Detail.STT = Convert.ToDecimal(additionalDocument.Sequence.ToString());
                                                    Detail.SoPhieuXuat = additionalDocument.Identification.ToString();
                                                    Detail.NgayPhieuXuat = Convert.ToDateTime(additionalDocument.Issue.ToString());
                                                    Detail.TenNguoiNhanHang = additionalDocument.NameConsignee.ToString();
                                                    Detail.MaNguoiNhanHang = additionalDocument.IdentityConsignor.ToString();
                                                    foreach (PNK_CustomsGoodsItem customsGoodsItem in additionalDocument.CustomsGoodsItem)
                                                    {
                                                        T_KDT_VNACCS_WarehouseExport_GoodsDetail goodsDetail = new T_KDT_VNACCS_WarehouseExport_GoodsDetail();
                                                        warehouseExport.SoTKChungTu = customsGoodsItem.DeclarationDocument.Reference.ToString();
                                                        warehouseExport.Loai = Convert.ToDecimal(customsGoodsItem.DeclarationDocument.Type.ToString());
#if GC_V4
                                                        warehouseExport.SoHopDong = customsGoodsItem.ContractReference.Reference.ToString();
                                                        warehouseExport.NgayHopDong = Convert.ToDateTime(customsGoodsItem.ContractReference.Issue.ToString());
                                                        warehouseExport.NgayHetHanHD = Convert.ToDateTime(customsGoodsItem.ContractReference.Expire.ToString());
                                                        warehouseExport.MaHQTiepNhanHD = customsGoodsItem.ContractReference.DeclarationOffice.ToString();
#endif
                                                        goodsDetail.STT = Convert.ToDecimal(customsGoodsItem.Commodity.Sequence.ToString());
                                                        goodsDetail.TenHangHoa = customsGoodsItem.Commodity.Description.ToString();
                                                        goodsDetail.MaHangHoa = customsGoodsItem.Commodity.Identification.ToString();
                                                        goodsDetail.LoaiHangHoa = Convert.ToDecimal(customsGoodsItem.Commodity.Type.ToString());
                                                        goodsDetail.MaDinhDanhSX = customsGoodsItem.Commodity.ProductCtrlNo.ToString();
                                                        goodsDetail.MucDichSuDung = Convert.ToDecimal(customsGoodsItem.Commodity.Usage.ToString());
                                                        goodsDetail.SoLuongDuKienXuat = Convert.ToDecimal(customsGoodsItem.GoodsMeasure.DocQuantity.ToString());
                                                        goodsDetail.SoLuongThucXuat = Convert.ToDecimal(customsGoodsItem.GoodsMeasure.ActualQuantity.ToString());
                                                        goodsDetail.DVT = VNACCS_Mapper.GetCodeV4DVT(customsGoodsItem.GoodsMeasure.MeasureUnit.ToString());
                                                        Detail.Collection.Add(goodsDetail);
                                                    }
                                                    warehouseExport.WarehouseExportCollection.Add(Detail);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return warehouseExport;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
