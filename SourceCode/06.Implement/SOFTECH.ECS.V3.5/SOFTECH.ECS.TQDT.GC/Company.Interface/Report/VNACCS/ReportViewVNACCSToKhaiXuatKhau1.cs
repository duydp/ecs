﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using BarcodeLib;
namespace Company.Interface.Report.VNACCS
{
    public partial class ReportViewVNACCSToKhaiXuatKhau1 : DevExpress.XtraReports.UI.XtraReport
    {
        int count;
        public ReportViewVNACCSToKhaiXuatKhau1()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 30,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        public void BindReport(KDT_VNACC_ToKhaiMauDich tkmd)
        {
            try
            {
                KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                //lblSoTrang.Text = "1/" + Vae.K69.GetValue().ToString();
                pictureBox1.Image = GenerateBarCode(tkmd.SoToKhai.ToString());
                count = tkmd.HangCollection.Count;
                lblTenThongTinXuat.Text = "Tờ khai hàng hóa xuất khẩu (thông quan)";
                int HangHoa = tkmd.HangCollection.Count;
                if (HangHoa <= 2)
                {
                    lblSoTrang.Text = "1/" + System.Convert.ToDecimal(3).ToString();
                }
                else
                {
                    lblSoTrang.Text = "1/" + System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
                }
                lblSotokhai.Text = tkmd.SoToKhai.ToString();
                lblSotokhaidautien.Text = String.IsNullOrEmpty(tkmd.SoToKhaiDauTien) ? "" : tkmd.SoToKhaiDauTien.ToString();
                lblSonhanhtokhaichianho.Text = tkmd.SoNhanhToKhai.ToString();
                lblTongsotokhaichianho.Text = tkmd.TongSoTKChiaNho.ToString();
                lblSotokhaitamnhaptaixuat.Text = tkmd.SoToKhaiTNTX.ToString();
                lblMaphanloaikiemtra.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiKiemTra) ? "" : tkmd.MaPhanLoaiKiemTra.ToString();
                lblMaloaihinh.Text = String.IsNullOrEmpty(tkmd.MaLoaiHinh) ? "" : tkmd.MaLoaiHinh.ToString();
                lblMaphanloaihanghoa.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiHH) ? "" : tkmd.MaPhanLoaiHH.ToString();
                lblMahieuphuongthucvanchuyen.Text = String.IsNullOrEmpty(tkmd.MaPhuongThucVT) ? "" : tkmd.MaPhuongThucVT.ToString();
                lblMasothuedaidien.Text = String.IsNullOrEmpty(tkmd.MaSoThueDaiDien) ? "" : tkmd.MaSoThueDaiDien.ToString();
                lblTencoquanhaiquantiepnhantokhai.Text = String.IsNullOrEmpty(tkmd.TenCoQuanHaiQuan) ? "" : tkmd.TenCoQuanHaiQuan.ToString();
                lblNhomxulyhoso.Text = String.IsNullOrEmpty(tkmd.NhomXuLyHS) ? "" : tkmd.NhomXuLyHS.ToString();
                if
                 (tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgaydangky.Text = tkmd.NgayDangKy.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgaydangky.Text = "";
                }

                if (tkmd.NgayDangKy.Hour.ToString() != "" && tkmd.NgayDangKy.Hour.ToString() != "0")
                {
                    lblGiodangky.Text = tkmd.NgayDangKy.Hour.ToString() + ":" + tkmd.NgayDangKy.Minute.ToString() + ":" + tkmd.NgayDangKy.Second.ToString();
                }
                else
                {
                    lblGiodangky.Text = "";
                }
                if
                    (tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgaythaydoidangky.Text = tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy");

                }
                else
                {
                    lblNgaythaydoidangky.Text = "";
                }

                if (tkmd.NgayThayDoiDangKy.Hour.ToString() != "" && tkmd.NgayThayDoiDangKy.Hour.ToString() != "0")
                {
                    lblGiothaydoidangky.Text = tkmd.NgayThayDoiDangKy.Hour.ToString() + ":" + tkmd.NgayThayDoiDangKy.Minute.ToString() + ":" + tkmd.NgayThayDoiDangKy.Second.ToString();
                }
                else
                {
                    lblGiothaydoidangky.Text = "";
                }
                if
                    (tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblThoihantainhapxuat.Text = tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblThoihantainhapxuat.Text = "";
                }
                lblBieuthitruonghophethan.Text = String.IsNullOrEmpty(tkmd.BieuThiTruongHopHetHan) ? "" : tkmd.BieuThiTruongHopHetHan.ToString();
                lblMaxuatkhau.Text = String.IsNullOrEmpty(tkmd.MaDonVi) ? "" : tkmd.MaDonVi.ToString();
                lblTenxuatkhau.Text = String.IsNullOrEmpty(tkmd.TenDonVi) ? "" : tkmd.TenDonVi.ToString();
                lblMaBuuChinhXK.Text = String.IsNullOrEmpty(tkmd.MaBuuChinhDonVi) ? "" : tkmd.MaBuuChinhDonVi.ToString();
                lblDiachinguoixuat.Text = String.IsNullOrEmpty(tkmd.DiaChiDonVi) ? "" : tkmd.DiaChiDonVi.ToString();
                lblSodtnguoixuat.Text = String.IsNullOrEmpty(tkmd.SoDienThoaiDonVi) ? "" : tkmd.SoDienThoaiDonVi.ToString();
                lblManguoiuythacxuatkhau.Text = String.IsNullOrEmpty(tkmd.MaUyThac) ? "" : tkmd.MaUyThac.ToString();
                lblTennguoiuythacxuatkhau.Text = String.IsNullOrEmpty(tkmd.TenUyThac) ? "" : tkmd.TenUyThac.ToString();
                lblManguoinhapkhau.Text = String.IsNullOrEmpty(tkmd.MaDoiTac) ? "" : tkmd.MaDoiTac.ToString();
                lblTennguoinhapkhau.Text = String.IsNullOrEmpty(tkmd.TenDoiTac) ? "" : tkmd.TenDoiTac.ToString();
                lblMabuuchinh.Text = String.IsNullOrEmpty(tkmd.MaBuuChinhDoiTac) ? "" : tkmd.MaBuuChinhDoiTac.ToString();
                lblDiachi1.Text = String.IsNullOrEmpty(tkmd.DiaChiDoiTac1) ? "" : tkmd.DiaChiDoiTac1.ToString();
                lblDiachi2.Text = String.IsNullOrEmpty(tkmd.DiaChiDoiTac2) ? "" : tkmd.DiaChiDoiTac2.ToString();
                lblDiachi3.Text = String.IsNullOrEmpty(tkmd.DiaChiDoiTac3) ? "" : tkmd.DiaChiDoiTac3.ToString();
                lblDiachi4.Text = String.IsNullOrEmpty(tkmd.DiaChiDoiTac4) ? "" : tkmd.DiaChiDoiTac4.ToString();
                lblManuoc.Text = String.IsNullOrEmpty(tkmd.MaNuocDoiTac) ? "" : tkmd.MaNuocDoiTac.ToString();
                lblMadailyhaiquan.Text = String.IsNullOrEmpty(tkmd.MaDaiLyHQ) ? "" : tkmd.MaDaiLyHQ.ToString();
                lblTendailyhaiquan.Text = String.IsNullOrEmpty(tkmd.TenDaiLyHaiQuan) ? "" : tkmd.TenDaiLyHaiQuan.ToString();
                lblManhanvienhaiquan.Text = String.IsNullOrEmpty(tkmd.MaNhanVienHaiQuan) ? "" : tkmd.MaNhanVienHaiQuan.ToString();
                if (tkmd.VanDon == null)
                {
                    lblSovandon.Text = String.Empty;
                }
                else
                {
                    lblSovandon.Text = tkmd.VanDon.ToString();
                }
                lblSoluong.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(tkmd.SoLuong.ToString().Replace(".", ",")));
                lblMadonvitinh.Text = String.IsNullOrEmpty(tkmd.MaDVTSoLuong) ? "" : tkmd.MaDVTSoLuong.ToString();
                lblTongtrongluonghang.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(tkmd.TrongLuong.ToString().Replace(".", ",")));
                lblMadonvitinhtrongluong.Text = String.IsNullOrEmpty(tkmd.MaDVTTrongLuong) ? "" : tkmd.MaDVTTrongLuong.ToString();
                lblMadiadiemluukho.Text = String.IsNullOrEmpty(tkmd.MaDDLuuKho) ? "" : tkmd.MaDDLuuKho.ToString();
                lblTendiadiemluukho.Text = String.IsNullOrEmpty(tkmd.TenDDLuuKho) ? "" : tkmd.TenDDLuuKho.ToString();
                lblMadiadiemnhanhangcuoi.Text = String.IsNullOrEmpty(tkmd.MaDiaDiemDoHang) ? "" : tkmd.MaDiaDiemDoHang.ToString();
                lblTendiadiemnhanhangcuoi.Text = String.IsNullOrEmpty(tkmd.TenDiaDiemDohang) ? "" : tkmd.TenDiaDiemDohang.ToString();
                lblMadiadiemxephang.Text = String.IsNullOrEmpty(tkmd.MaDiaDiemXepHang) ? "" : tkmd.MaDiaDiemXepHang.ToString();
                lblTendiadiemxephang.Text = String.IsNullOrEmpty(tkmd.TenDiaDiemXepHang) ? "" : tkmd.TenDiaDiemXepHang.ToString();
                lblMaphuongtienvanchuyen.Text = String.IsNullOrEmpty(tkmd.MaPTVC) ? "" : tkmd.MaPTVC.ToString();
                lblTenphuongtienvanchuyen.Text = String.IsNullOrEmpty(tkmd.TenPTVC) ? "" : tkmd.TenPTVC.ToString();
                if
                (Convert.ToDateTime(tkmd.NgayKhoiHanhVC).ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayKhoiHanhVC.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayhangdidukien.Text = Convert.ToDateTime(tkmd.NgayKhoiHanhVC).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayhangdidukien.Text = "";
                }
                lblKyhieusohieu.Text = String.IsNullOrEmpty(tkmd.SoHieuKyHieu) ? "" : tkmd.SoHieuKyHieu.ToString();
                for (int i = 0; i < tkmd.GiayPhepCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:

                            lblPhanloaigiayphepxuatkhau1.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString();
                            lblSogiayphep1.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString();

                            break;
                        case 1:

                            lblPhanloaigiayphepxuatkhau2.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString();
                            lblSogiayphep2.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString();
                            break;
                        case 2:

                            lblPhanloaigiayphepxuatkhau3.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString();
                            lblSogiayphep3.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString();
                            break;
                        case 3:

                            lblPhanloaigiayphepxuatkhau4.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString();
                            lblSogiayphep4.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString();
                            break;
                        case 4:

                            lblPhanloaigiayphepxuatkhau5.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].PhanLoai) ? "" : tkmd.GiayPhepCollection[i].PhanLoai.ToString();
                            lblSogiayphep5.Text = String.IsNullOrEmpty(tkmd.GiayPhepCollection[i].SoGiayPhep) ? "" : tkmd.GiayPhepCollection[i].SoGiayPhep.ToString();
                            break;

                    }

                }
                lblPhanloaihinhthuchoadon.Text = String.IsNullOrEmpty(tkmd.SoToKhaiDauTien) ? "" : tkmd.PhanLoaiHD.ToString();

                lblSohoadon.Text = String.IsNullOrEmpty(tkmd.SoHoaDon) ? "" : tkmd.SoHoaDon.ToString();
                lblSotiepnhanhoadondientu.Text = tkmd.SoTiepNhanHD.ToString();
                if
                    (Convert.ToDateTime(tkmd.NgayPhatHanhHD).ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayPhatHanhHD.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayphathanh.Text = Convert.ToDateTime(tkmd.NgayPhatHanhHD).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayphathanh.Text = "";
                }
                lblPhuongthucthanhtoan.Text = String.IsNullOrEmpty(tkmd.PhuongThucTT) ? "" : tkmd.PhuongThucTT.ToString();
                lblMadkgiahoadon.Text = String.IsNullOrEmpty(tkmd.MaDieuKienGiaHD) ? "" : tkmd.MaDieuKienGiaHD.ToString();
                lblMadongtienhoadon.Text = String.IsNullOrEmpty(tkmd.MaTTHoaDon) ? "" : tkmd.MaTTHoaDon.ToString();
                decimal Tongtrigiahoadon = tkmd.TongTriGiaHD;//Convert.ToDecimal(tkmd.TongTriGiaHD.ToString().Replace(".", ","));
                //decimal Tygiatinhthue = Convert.ToDecimal(tkmd.TyGiaCollection[0].TyGiaTinhThue.ToString().Replace(".", ","));
                //decimal Tongthue = Tongtrigiahoadon * Tygiatinhthue;
                lblTongtrigiahoadon.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), tkmd.TongTriGiaHD);
                lblMaphanloaigiahoadon.Text = String.IsNullOrEmpty(tkmd.PhanLoaiGiaHD) ? "" : tkmd.PhanLoaiGiaHD.ToString();
                //lblMadongtientongthue.Text = Vae.FCD.GetValue().ToString();
                lblMadongtientongthue.Text = "VND";
                //lblTongthue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.FKK.GetValue().ToString().Replace(".", ",")));
                //lblTongthue.Text = String.Format("{0:N2}", Tongthue);
                for (int i = 0; i < tkmd.TyGiaCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblMadongtientygiathue1.Text = String.IsNullOrEmpty(tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue) ? "" : tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue.ToString();
                            lblTygiatinhthue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TyGiaCollection[i].TyGiaTinhThue.ToString().Replace(".", ",")));
                            break;
                        case 1:
                            lblMadongtientygiathue2.Text = String.IsNullOrEmpty(tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue) ? "" : tkmd.TyGiaCollection[i].MaTTTyGiaTinhThue.ToString();
                            lblTygiatinhthue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TyGiaCollection[i].TyGiaTinhThue.ToString().Replace(".", ",")));
                            break;


                    }

                }

                //minhnd Fix Trị giá Tờ khai
                TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(lblSotokhai.Text);
                decimal soToKhai = Convert.ToDecimal(TKMD.SoToKhai.ToString().Substring(11, 1));
                decimal triGiaHang = 0;
                TKMD.LoadFull();
                foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                {
                    triGiaHang += hmd.TriGiaHoaDon;
                }
                lblTonghesophanbothue.Text = triGiaHang.ToString("#,#.0000#;(#,#.0000#)");
                //minhnd Fix Trị giá Tờ khai
                //lblTonghesophanbothue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.TP.GetValue().ToString().Replace(".", ",")));

                lblMaphanloainhaplieutonggia.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiTongGiaCoBan) ? "" : tkmd.MaPhanLoaiTongGiaCoBan.ToString();
                lblPhanloaikhongcanquydoi.Text = String.IsNullOrEmpty(tkmd.PhanLoaiKhongQDVND) ? "" : tkmd.PhanLoaiKhongQDVND.ToString();

                for (int i = 0; i < tkmd.DinhKemCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblPhanloaidinhkemkhaibaodientu1.Text = String.IsNullOrEmpty(tkmd.DinhKemCollection[i].PhanLoai) ? "" : tkmd.DinhKemCollection[i].PhanLoai.ToString();
                            lblSodinhkemkhaibao1.Text = String.IsNullOrEmpty(tkmd.DinhKemCollection[i].PhanLoai) ? "" : tkmd.DinhKemCollection[i].SoDinhKemKhaiBaoDT.ToString();
                            break;
                        case 1:
                            lblPhanloaidinhkemkhaibaodientu2.Text = String.IsNullOrEmpty(tkmd.DinhKemCollection[i].PhanLoai) ? "" : tkmd.DinhKemCollection[i].PhanLoai.ToString();
                            lblSodinhkemkhaibao2.Text = tkmd.DinhKemCollection[i].SoDinhKemKhaiBaoDT.ToString();
                            break;
                        case 2:
                            lblPhanloaidinhkemkhaibaodientu3.Text = String.IsNullOrEmpty(tkmd.DinhKemCollection[i].PhanLoai) ? "" : tkmd.DinhKemCollection[i].PhanLoai.ToString();
                            lblSodinhkemkhaibao3.Text = tkmd.DinhKemCollection[i].SoDinhKemKhaiBaoDT.ToString();
                            break;
                    }

                }

                lblNguoinopthue.Text = String.IsNullOrEmpty(tkmd.NguoiNopThue) ? "" : tkmd.NguoiNopThue.ToString();
                lblMaxacdinhthoihannopthue.Text = String.IsNullOrEmpty(tkmd.MaXDThoiHanNopThue) ? "" : tkmd.MaXDThoiHanNopThue.ToString();
                lblPhanloainopthue.Text = String.IsNullOrEmpty(tkmd.PhanLoaiNopThue) ? "" : tkmd.PhanLoaiNopThue.ToString();
                //lblTongsotienthuexuatkhau.Text = Vae.ETA.GetValue().ToString();
                lblMatientetongtienthuexuat.Text = String.IsNullOrEmpty(tkmd.MaTTTongTienThueXuatKhau) ? "" : tkmd.MaTTTongTienThueXuatKhau.ToString();
                lblTongsotienthuexuatkhau.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TongSoTienThueXuatKhau.ToString().Replace(".", ",")));
                lblTongsotienlephi.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.TongSoTienLePhi));
                //lblTongsotienbaolanh.Text = Vae.SAM.GetValue().ToString();
                lblMatientetienbaolanh.Text = String.IsNullOrEmpty(tkmd.MaTTCuaSoTienBaoLanh) ? "" : tkmd.MaTTCuaSoTienBaoLanh.ToString();
                lblTongsotienbaolanh.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(tkmd.SoTienBaoLanh.ToString().Replace(".", ",")));
                lblTongsotokhaicuatrang.Text = tkmd.TongSoTrangCuaToKhai.ToString();
                lblTongsodonghangtokhai.Text = tkmd.TongSoDongHangCuaToKhai.ToString();
                lblPhanghichu.Text = String.IsNullOrEmpty(tkmd.GhiChu) ? "" : tkmd.GhiChu.ToString();
                lblSoquanlynoibodoanhnghiep.Text = String.IsNullOrEmpty(tkmd.SoQuanLyNoiBoDN) ? "" : tkmd.SoQuanLyNoiBoDN.ToString();
                lblSoquanlynguoisudung.Text = String.IsNullOrEmpty(tkmd.SoQuanLyNguoiSuDung) ? "" : tkmd.SoQuanLyNguoiSuDung.ToString();

                lblTentruongdvHQ.Text = String.IsNullOrEmpty(tkmd.TenCoQuanHaiQuan) ? "" : tkmd.TenCoQuanHaiQuan.ToString();
                if (tkmd.NgayHoanThanhKiemTra.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayHoanThanhKiemTra.ToString("dd/MM/yyyy") != "01/01/0001")
                    lblNgayhoanthanhktra.Text = tkmd.NgayHoanThanhKiemTra.ToString("dd/MM/yyyy");
                else
                    lblNgayhoanthanhktra.Text = "";
                if (tkmd.NgayHoanThanhKiemTra.Hour.ToString() != "" && tkmd.NgayHoanThanhKiemTra.Hour.ToString().ToUpper() != "0")
                    lblGiohoanthanhktra.Text = tkmd.NgayHoanThanhKiemTra.Hour.ToString().ToUpper() + ":" + tkmd.NgayHoanThanhKiemTra.Minute.ToString().ToUpper() + ":" + tkmd.NgayHoanThanhKiemTra.Second.ToString().ToUpper();
                else
                    lblGiohoanthanhktra.Text = "";
                if (tkmd.NgayCapPhep.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayCapPhep.ToString("dd/MM/yyyy") != "01/01/0001")
                    lblNgaycapphep.Text = tkmd.NgayCapPhep.ToString("dd/MM/yyyy");
                else
                    lblNgaycapphep.Text = "";
                if (tkmd.NgayCapPhep.Hour.ToString() != "" && tkmd.NgayCapPhep.Hour.ToString().ToUpper() != "0")
                    lblGiocapphep.Text = tkmd.NgayCapPhep.Hour.ToString().ToUpper() + ":" + tkmd.NgayCapPhep.Minute.ToString().ToUpper() + ":" + tkmd.NgayCapPhep.Second.ToString().ToUpper();
                else
                    lblGiocapphep.Text = "";
                if
                    (Convert.ToDateTime(tkmd.NgayKhoiHanhVC).ToString("dd/MM/yyyy") != "01/01/1900" && Convert.ToDateTime(tkmd.NgayKhoiHanhVC).ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgaykhoihanh.Text = Convert.ToDateTime(tkmd.NgayKhoiHanhVC).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgaykhoihanh.Text = "";
                }
                for (int i = 0; i < tkmd.TrungChuyenCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblDiadiem.Text = String.IsNullOrEmpty(tkmd.TrungChuyenCollection[i].MaDiaDiem) ? "" : tkmd.TrungChuyenCollection[i].MaDiaDiem.ToString();
                            if (Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayDen).ToString("dd/MM/yyyy") != "01/01/1900" && Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayDen).ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgaydukiendentrungchuyen.Text = Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayDen).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgaydukiendentrungchuyen.Text = "";
                            }

                            if (Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayKhoiHanh).ToString("dd/MM/yyyy") != "01/01/1900" && Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayKhoiHanh).ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgaykhoihanhvanchuyen.Text = Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayKhoiHanh).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgaykhoihanhvanchuyen.Text = "";
                            }
                            break;
                        case 1:
                            lblDiadiem2.Text = String.IsNullOrEmpty(tkmd.TrungChuyenCollection[i].MaDiaDiem) ? "" : tkmd.TrungChuyenCollection[i].MaDiaDiem.ToString();
                            if (Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayDen).ToString("dd/MM/yyyy") != "01/01/1900" && Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayDen).ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgaydukiendentrungchuyen2.Text = Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayDen).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgaydukiendentrungchuyen2.Text = "";
                            }

                            if (Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayKhoiHanh).ToString("dd/MM/yyyy") != "01/01/1900" && Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayKhoiHanh).ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgaykhoihanhvanchuyen2.Text = Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayKhoiHanh).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgaykhoihanhvanchuyen2.Text = "";
                            }
                            break;
                        case 2:
                            lblDiadiem3.Text = String.IsNullOrEmpty(tkmd.TrungChuyenCollection[i].MaDiaDiem) ? "" : tkmd.TrungChuyenCollection[i].MaDiaDiem.ToString();
                            if (Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayDen).ToString("dd/MM/yyyy") != "01/01/1900" && Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayDen).ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgaydukiendentrungchuyen3.Text = Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayDen).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgaydukiendentrungchuyen3.Text = "";
                            }

                            if (Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayKhoiHanh).ToString("dd/MM/yyyy") != "01/01/1900" && Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayKhoiHanh).ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgaykhoihanhvanchuyen3.Text = Convert.ToDateTime(tkmd.TrungChuyenCollection[i].NgayKhoiHanh).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgaykhoihanhvanchuyen3.Text = "";
                            }
                            break;


                    }

                }

                lblDiadiemdich.Text = String.IsNullOrEmpty(tkmd.DiaDiemDichVC) ? "" : tkmd.DiaDiemDichVC.ToString();
                if
                    (Convert.ToDateTime(tkmd.NgayHangDen).ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayHangDen.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgaydukienden.Text = Convert.ToDateTime(tkmd.NgayHangDen).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgaydukienden.Text = "";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            int pages;
            if (count%2 >=1)
            {
                pages = count / 2 + 1;
            }
            else
            {
                pages = count;
            }
            lblSoTrang.Text = 1 + "/" + (pages + 2);
        }
    }
}
