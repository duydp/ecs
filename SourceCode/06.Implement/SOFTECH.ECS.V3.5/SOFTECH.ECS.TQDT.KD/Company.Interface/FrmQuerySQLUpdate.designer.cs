﻿namespace Company.Interface
{
    partial class FrmQuerySQLUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmQuerySQLUpdate));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.lblRecord = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.grdResult = new System.Windows.Forms.DataGridView();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.txtMessage = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.tabQuery = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnQuery = new Janus.Windows.EditControls.UIButton();
            this.label7 = new System.Windows.Forms.Label();
            this.btnTabDelete = new Janus.Windows.EditControls.UIButton();
            this.btnCmd_Select = new Janus.Windows.EditControls.UIButton();
            this.btnTabNew = new Janus.Windows.EditControls.UIButton();
            this.bntCmd_Update = new Janus.Windows.EditControls.UIButton();
            this.btnCmd_Delete = new Janus.Windows.EditControls.UIButton();
            this.btnCmd_Insert = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNgayDangKy = new System.Windows.Forms.DateTimePicker();
            this.txtNgayTiepNhan = new System.Windows.Forms.DateTimePicker();
            this.btnUpdate = new Janus.Windows.EditControls.UIButton();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSTN = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTT_XuLy = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtReferenceid = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.rbtnSoTK = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.rbtnSoTN = new System.Windows.Forms.RadioButton();
            this.comboBoxTable = new System.Windows.Forms.ComboBox();
            this.rbtnID = new System.Windows.Forms.RadioButton();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelect = new Janus.Windows.EditControls.UIButton();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab)).BeginInit();
            this.uiTab.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdResult)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            this.tabQuery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(996, 717);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "exit.png");
            this.imageList1.Images.SetKeyName(1, "bt_play.png");
            this.imageList1.Images.SetKeyName(2, "erase.png");
            // 
            // editBox1
            // 
            this.editBox1.Location = new System.Drawing.Point(6, 40);
            this.editBox1.Multiline = true;
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(561, 109);
            this.editBox1.TabIndex = 0;
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.AutoScroll = true;
            this.uiTabPage4.Key = "tabPageGrid";
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(571, 215);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Kết quả";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.lblRecord);
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 674);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(996, 43);
            this.uiGroupBox2.TabIndex = 5;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // lblRecord
            // 
            this.lblRecord.AutoSize = true;
            this.lblRecord.Location = new System.Drawing.Point(6, 17);
            this.lblRecord.Name = "lblRecord";
            this.lblRecord.Size = new System.Drawing.Size(0, 13);
            this.lblRecord.TabIndex = 11;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnClose.ImageList = this.imageList1;
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(900, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(87, 23);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiTab);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(996, 674);
            this.uiGroupBox1.TabIndex = 6;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiTab
            // 
            this.uiTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab.Location = new System.Drawing.Point(3, 394);
            this.uiTab.Name = "uiTab";
            this.uiTab.Size = new System.Drawing.Size(990, 277);
            this.uiTab.TabIndex = 18;
            this.uiTab.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.AutoScroll = true;
            this.uiTabPage1.Controls.Add(this.grdResult);
            this.uiTabPage1.Key = "tabPageGrid";
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(988, 255);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Kết quả";
            // 
            // grdResult
            // 
            this.grdResult.AllowUserToAddRows = false;
            this.grdResult.AllowUserToDeleteRows = false;
            this.grdResult.BackgroundColor = System.Drawing.Color.White;
            this.grdResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdResult.Location = new System.Drawing.Point(0, 0);
            this.grdResult.Name = "grdResult";
            this.grdResult.Size = new System.Drawing.Size(988, 255);
            this.grdResult.TabIndex = 0;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.AutoScroll = true;
            this.uiTabPage2.Controls.Add(this.txtMessage);
            this.uiTabPage2.Key = "tabPageMessage";
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(988, 255);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thông báo";
            // 
            // txtMessage
            // 
            this.txtMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMessage.Location = new System.Drawing.Point(0, 0);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMessage.Size = new System.Drawing.Size(988, 255);
            this.txtMessage.TabIndex = 0;
            this.txtMessage.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.tabQuery);
            this.uiGroupBox7.Controls.Add(this.label8);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 197);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(990, 197);
            this.uiGroupBox7.TabIndex = 17;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // tabQuery
            // 
            this.tabQuery.Controls.Add(this.tabPage1);
            this.tabQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabQuery.Location = new System.Drawing.Point(3, 8);
            this.tabQuery.Name = "tabQuery";
            this.tabQuery.SelectedIndex = 0;
            this.tabQuery.Size = new System.Drawing.Size(984, 186);
            this.tabQuery.TabIndex = 11;
            this.tabQuery.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tabQuery_KeyUp);
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(976, 160);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cmd_SQL 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 11;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnQuery);
            this.uiGroupBox6.Controls.Add(this.label7);
            this.uiGroupBox6.Controls.Add(this.btnTabDelete);
            this.uiGroupBox6.Controls.Add(this.btnCmd_Select);
            this.uiGroupBox6.Controls.Add(this.btnTabNew);
            this.uiGroupBox6.Controls.Add(this.bntCmd_Update);
            this.uiGroupBox6.Controls.Add(this.btnCmd_Delete);
            this.uiGroupBox6.Controls.Add(this.btnCmd_Insert);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 148);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(990, 49);
            this.uiGroupBox6.TabIndex = 16;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.ImageIndex = 1;
            this.btnQuery.ImageList = this.imageList1;
            this.btnQuery.Location = new System.Drawing.Point(67, 16);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 23);
            this.btnQuery.TabIndex = 9;
            this.btnQuery.Text = "Thực hiện";
            this.btnQuery.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 11;
            // 
            // btnTabDelete
            // 
            this.btnTabDelete.ImageIndex = 1;
            this.btnTabDelete.Location = new System.Drawing.Point(35, 16);
            this.btnTabDelete.Name = "btnTabDelete";
            this.btnTabDelete.Size = new System.Drawing.Size(23, 23);
            this.btnTabDelete.TabIndex = 2;
            this.btnTabDelete.Text = "T-";
            this.btnTabDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTabDelete.Click += new System.EventHandler(this.btnTabDelete_Click);
            // 
            // btnCmd_Select
            // 
            this.btnCmd_Select.ImageIndex = 1;
            this.btnCmd_Select.Location = new System.Drawing.Point(152, 17);
            this.btnCmd_Select.Name = "btnCmd_Select";
            this.btnCmd_Select.Size = new System.Drawing.Size(60, 23);
            this.btnCmd_Select.TabIndex = 2;
            this.btnCmd_Select.Text = "SELECT";
            this.btnCmd_Select.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCmd_Select.Click += new System.EventHandler(this.btnCmd_Select_Click);
            // 
            // btnTabNew
            // 
            this.btnTabNew.ImageIndex = 1;
            this.btnTabNew.Location = new System.Drawing.Point(6, 16);
            this.btnTabNew.Name = "btnTabNew";
            this.btnTabNew.Size = new System.Drawing.Size(23, 23);
            this.btnTabNew.TabIndex = 2;
            this.btnTabNew.Text = "T+";
            this.btnTabNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTabNew.Click += new System.EventHandler(this.btnTabNew_Click);
            // 
            // bntCmd_Update
            // 
            this.bntCmd_Update.ImageIndex = 1;
            this.bntCmd_Update.Location = new System.Drawing.Point(218, 17);
            this.bntCmd_Update.Name = "bntCmd_Update";
            this.bntCmd_Update.Size = new System.Drawing.Size(54, 23);
            this.bntCmd_Update.TabIndex = 2;
            this.bntCmd_Update.Text = "UPDATE";
            this.bntCmd_Update.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.bntCmd_Update.Click += new System.EventHandler(this.bntCmd_Update_Click);
            // 
            // btnCmd_Delete
            // 
            this.btnCmd_Delete.ImageIndex = 1;
            this.btnCmd_Delete.Location = new System.Drawing.Point(344, 17);
            this.btnCmd_Delete.Name = "btnCmd_Delete";
            this.btnCmd_Delete.Size = new System.Drawing.Size(54, 23);
            this.btnCmd_Delete.TabIndex = 2;
            this.btnCmd_Delete.Text = "DELETE";
            this.btnCmd_Delete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCmd_Delete.Click += new System.EventHandler(this.btnCmd_Delete_Click);
            // 
            // btnCmd_Insert
            // 
            this.btnCmd_Insert.ImageIndex = 1;
            this.btnCmd_Insert.Location = new System.Drawing.Point(278, 17);
            this.btnCmd_Insert.Name = "btnCmd_Insert";
            this.btnCmd_Insert.Size = new System.Drawing.Size(60, 23);
            this.btnCmd_Insert.TabIndex = 2;
            this.btnCmd_Insert.Text = "INSERT";
            this.btnCmd_Insert.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCmd_Insert.Click += new System.EventHandler(this.btnCmd_Insert_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(990, 140);
            this.uiGroupBox3.TabIndex = 15;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.label2);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Controls.Add(this.txtNgayDangKy);
            this.uiGroupBox5.Controls.Add(this.txtNgayTiepNhan);
            this.uiGroupBox5.Controls.Add(this.btnUpdate);
            this.uiGroupBox5.Controls.Add(this.label12);
            this.uiGroupBox5.Controls.Add(this.txtSTN);
            this.uiGroupBox5.Controls.Add(this.label13);
            this.uiGroupBox5.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox5.Controls.Add(this.label3);
            this.uiGroupBox5.Controls.Add(this.txtTT_XuLy);
            this.uiGroupBox5.Controls.Add(this.label14);
            this.uiGroupBox5.Controls.Add(this.txtReferenceid);
            this.uiGroupBox5.Controls.Add(this.label15);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox5.Location = new System.Drawing.Point(385, 8);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(602, 129);
            this.uiGroupBox5.TabIndex = 17;
            this.uiGroupBox5.Text = "Cập nhật";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(144, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Ngày đăng ký:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 11;
            // 
            // txtNgayDangKy
            // 
            this.txtNgayDangKy.CustomFormat = "yyyy/MM/dd";
            this.txtNgayDangKy.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtNgayDangKy.Location = new System.Drawing.Point(229, 59);
            this.txtNgayDangKy.Name = "txtNgayDangKy";
            this.txtNgayDangKy.Size = new System.Drawing.Size(93, 21);
            this.txtNgayDangKy.TabIndex = 24;
            this.txtNgayDangKy.Value = new System.DateTime(2012, 7, 31, 12, 53, 7, 0);
            // 
            // txtNgayTiepNhan
            // 
            this.txtNgayTiepNhan.CustomFormat = "yyyy/MM/dd";
            this.txtNgayTiepNhan.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtNgayTiepNhan.Location = new System.Drawing.Point(229, 26);
            this.txtNgayTiepNhan.Name = "txtNgayTiepNhan";
            this.txtNgayTiepNhan.Size = new System.Drawing.Size(93, 21);
            this.txtNgayTiepNhan.TabIndex = 23;
            this.txtNgayTiepNhan.Value = new System.DateTime(2012, 7, 31, 12, 53, 7, 0);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpdate.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageIndex = 1;
            this.btnUpdate.ImageList = this.imageList1;
            this.btnUpdate.Location = new System.Drawing.Point(79, 91);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 7;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(327, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Ref:";
            // 
            // txtSTN
            // 
            this.txtSTN.Location = new System.Drawing.Point(79, 26);
            this.txtSTN.Name = "txtSTN";
            this.txtSTN.Size = new System.Drawing.Size(60, 21);
            this.txtSTN.TabIndex = 3;
            this.txtSTN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(329, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Trạng thái xử lý:";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Location = new System.Drawing.Point(79, 59);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(60, 21);
            this.txtSoToKhai.TabIndex = 3;
            this.txtSoToKhai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Số tờ khai:";
            // 
            // txtTT_XuLy
            // 
            this.txtTT_XuLy.Location = new System.Drawing.Point(422, 26);
            this.txtTT_XuLy.Name = "txtTT_XuLy";
            this.txtTT_XuLy.Size = new System.Drawing.Size(88, 21);
            this.txtTT_XuLy.TabIndex = 5;
            this.txtTT_XuLy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Số tiếp nhận:";
            // 
            // txtReferenceid
            // 
            this.txtReferenceid.BackColor = System.Drawing.Color.White;
            this.txtReferenceid.Location = new System.Drawing.Point(358, 59);
            this.txtReferenceid.Name = "txtReferenceid";
            this.txtReferenceid.Size = new System.Drawing.Size(238, 21);
            this.txtReferenceid.TabIndex = 6;
            this.txtReferenceid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(144, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Ngày tiếp nhận:";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.rbtnSoTK);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.rbtnSoTN);
            this.uiGroupBox4.Controls.Add(this.comboBoxTable);
            this.uiGroupBox4.Controls.Add(this.rbtnID);
            this.uiGroupBox4.Controls.Add(this.txtID);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.btnSelect);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(382, 129);
            this.uiGroupBox4.TabIndex = 16;
            this.uiGroupBox4.Text = "Tìm kiếm";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // rbtnSoTK
            // 
            this.rbtnSoTK.AutoSize = true;
            this.rbtnSoTK.Location = new System.Drawing.Point(157, 63);
            this.rbtnSoTK.Name = "rbtnSoTK";
            this.rbtnSoTK.Size = new System.Drawing.Size(52, 17);
            this.rbtnSoTK.TabIndex = 6;
            this.rbtnSoTK.Text = "Số TK";
            this.rbtnSoTK.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 11;
            // 
            // rbtnSoTN
            // 
            this.rbtnSoTN.AutoSize = true;
            this.rbtnSoTN.Location = new System.Drawing.Point(95, 63);
            this.rbtnSoTN.Name = "rbtnSoTN";
            this.rbtnSoTN.Size = new System.Drawing.Size(53, 17);
            this.rbtnSoTN.TabIndex = 5;
            this.rbtnSoTN.Text = "Số TN";
            this.rbtnSoTN.UseVisualStyleBackColor = true;
            // 
            // comboBoxTable
            // 
            this.comboBoxTable.FormattingEnabled = true;
            this.comboBoxTable.Location = new System.Drawing.Point(46, 29);
            this.comboBoxTable.Name = "comboBoxTable";
            this.comboBoxTable.Size = new System.Drawing.Size(323, 21);
            this.comboBoxTable.TabIndex = 0;
            // 
            // rbtnID
            // 
            this.rbtnID.AutoSize = true;
            this.rbtnID.Checked = true;
            this.rbtnID.Location = new System.Drawing.Point(45, 63);
            this.rbtnID.Name = "rbtnID";
            this.rbtnID.Size = new System.Drawing.Size(36, 17);
            this.rbtnID.TabIndex = 4;
            this.rbtnID.TabStop = true;
            this.rbtnID.Text = "ID";
            this.rbtnID.UseVisualStyleBackColor = true;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(46, 94);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(112, 21);
            this.txtID.TabIndex = 1;
            this.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            this.txtID.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtID_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Table";
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelect.Enabled = false;
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageIndex = 1;
            this.btnSelect.ImageList = this.imageList1;
            this.btnSelect.Location = new System.Drawing.Point(177, 92);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 23);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "Tìm";
            this.btnSelect.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 11;
            // 
            // FrmQuerySQLUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(996, 717);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "FrmQuerySQLUpdate";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thực hiện truy vấn SQL";
            this.Load += new System.EventHandler(this.FrmQuerySQL_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab)).EndInit();
            this.uiTab.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdResult)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            this.uiTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            this.tabQuery.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.UI.Tab.UITab uiTab;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private System.Windows.Forms.DataGridView grdResult;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMessage;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private System.Windows.Forms.TabControl tabQuery;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnQuery;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIButton btnTabDelete;
        private Janus.Windows.EditControls.UIButton btnCmd_Select;
        private Janus.Windows.EditControls.UIButton btnTabNew;
        private Janus.Windows.EditControls.UIButton bntCmd_Update;
        private Janus.Windows.EditControls.UIButton btnCmd_Delete;
        private Janus.Windows.EditControls.UIButton btnCmd_Insert;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker txtNgayDangKy;
        private System.Windows.Forms.DateTimePicker txtNgayTiepNhan;
        private Janus.Windows.EditControls.UIButton btnUpdate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSTN;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSoToKhai;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTT_XuLy;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtReferenceid;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.RadioButton rbtnSoTK;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbtnSoTN;
        private System.Windows.Forms.ComboBox comboBoxTable;
        private System.Windows.Forms.RadioButton rbtnID;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnSelect;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label lblRecord;
        private Janus.Windows.EditControls.UIButton btnClose;
    }
}