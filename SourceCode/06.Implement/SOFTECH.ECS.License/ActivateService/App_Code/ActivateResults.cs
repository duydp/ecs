using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Summary description for ActivateResults
/// </summary>
public class ActivateResults
{
    private bool value;
    private string message;
    private SerialInfo information;

    public bool Value
    {
        get { return this.value; }
        set { this.value = value; }
    }

    public string Message
    {
        get { return message; }
        set { message = value; }
    }

    public SerialInfo Information
    {
        get { return information; }
        set { information = value; }
    }

    public ActivateResults()
	{
        
	}
}
