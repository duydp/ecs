﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.Messages.Send;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class TIA : BasicVNACC
    {
        public List<TIA_HANG> listHang { get; set; }

        public StringBuilder BuilEdiMessagesTIA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<TIA>(StrBuild, true, GlobalVNACC.PathConfig, EnumNghiepVu.TIA);
            foreach (TIA_HANG item in listHang)
            {
                item.BuildEdiMessages<TIA_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "TIA_HANG");
            }
            return str;
        }

    }
}
