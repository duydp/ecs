-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_HangVanDonDetail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_HangVanDonDetail_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Insert]
	@VanDon_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@TriGiaKB float,
	@LoaiKien nvarchar(10),
	@SoHieuContainer nvarchar(50),
	@SoHieuKien nvarchar(50),
	@SoKien decimal(8, 0),
	@TrongLuong float,
	@TrongLuongTinh float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HangVanDonDetail]
(
	[VanDon_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB],
	[LoaiKien],
	[SoHieuContainer],
	[SoHieuKien],
	[SoKien],
	[TrongLuong],
	[TrongLuongTinh]
)
VALUES 
(
	@VanDon_ID,
	@MaNguyenTe,
	@MaChuyenNganh,
	@GhiChu,
	@HMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB,
	@LoaiKien,
	@SoHieuContainer,
	@SoHieuKien,
	@SoKien,
	@TrongLuong,
	@TrongLuongTinh
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Update]
	@ID bigint,
	@VanDon_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@TriGiaKB float,
	@LoaiKien nvarchar(10),
	@SoHieuContainer nvarchar(50),
	@SoHieuKien nvarchar(50),
	@SoKien decimal(8, 0),
	@TrongLuong float,
	@TrongLuongTinh float
AS

UPDATE
	[dbo].[t_KDT_HangVanDonDetail]
SET
	[VanDon_ID] = @VanDon_ID,
	[MaNguyenTe] = @MaNguyenTe,
	[MaChuyenNganh] = @MaChuyenNganh,
	[GhiChu] = @GhiChu,
	[HMD_ID] = @HMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB,
	[LoaiKien] = @LoaiKien,
	[SoHieuContainer] = @SoHieuContainer,
	[SoHieuKien] = @SoHieuKien,
	[SoKien] = @SoKien,
	[TrongLuong] = @TrongLuong,
	[TrongLuongTinh] = @TrongLuongTinh
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]
	@ID bigint,
	@VanDon_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@TriGiaKB float,
	@LoaiKien nvarchar(10),
	@SoHieuContainer nvarchar(50),
	@SoHieuKien nvarchar(50),
	@SoKien decimal(8, 0),
	@TrongLuong float,
	@TrongLuongTinh float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangVanDonDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangVanDonDetail] 
		SET
			[VanDon_ID] = @VanDon_ID,
			[MaNguyenTe] = @MaNguyenTe,
			[MaChuyenNganh] = @MaChuyenNganh,
			[GhiChu] = @GhiChu,
			[HMD_ID] = @HMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB,
			[LoaiKien] = @LoaiKien,
			[SoHieuContainer] = @SoHieuContainer,
			[SoHieuKien] = @SoHieuKien,
			[SoKien] = @SoKien,
			[TrongLuong] = @TrongLuong,
			[TrongLuongTinh] = @TrongLuongTinh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangVanDonDetail]
		(
			[VanDon_ID],
			[MaNguyenTe],
			[MaChuyenNganh],
			[GhiChu],
			[HMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB],
			[LoaiKien],
			[SoHieuContainer],
			[SoHieuKien],
			[SoKien],
			[TrongLuong],
			[TrongLuongTinh]
		)
		VALUES 
		(
			@VanDon_ID,
			@MaNguyenTe,
			@MaChuyenNganh,
			@GhiChu,
			@HMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB,
			@LoaiKien,
			@SoHieuContainer,
			@SoHieuKien,
			@SoKien,
			@TrongLuong,
			@TrongLuongTinh
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HangVanDonDetail]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HangVanDonDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB],
	[LoaiKien],
	[SoHieuContainer],
	[SoHieuKien],
	[SoKien],
	[TrongLuong],
	[TrongLuongTinh]
FROM
	[dbo].[t_KDT_HangVanDonDetail]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[VanDon_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB],
	[LoaiKien],
	[SoHieuContainer],
	[SoHieuKien],
	[SoKien],
	[TrongLuong],
	[TrongLuongTinh]
FROM [dbo].[t_KDT_HangVanDonDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangVanDonDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB],
	[LoaiKien],
	[SoHieuContainer],
	[SoHieuKien],
	[SoKien],
	[TrongLuong],
	[TrongLuongTinh]
FROM
	[dbo].[t_KDT_HangVanDonDetail]	

GO

