﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class CX_ThanhlyTaiSanCoDinh:DeclarationBase
    {
       /// <summary>
        /// Ngày thông báo thanh lý, bắt buộc,an..10,YYYY-MM-DD
       /// </summary>
       [XmlElement("time")]
       public string Time { get; set; }
       /// <summary>
       /// Thông tin hàng hóa xin thanh lý
       /// </summary>
       [XmlElement("CustomsGoodsItem")]
       public List<CustomsGoodsItem> CustomsGoodsItemL { get; set; }

    }
}
