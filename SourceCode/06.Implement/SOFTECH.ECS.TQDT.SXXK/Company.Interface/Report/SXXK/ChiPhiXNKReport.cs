using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;
using Company.BLL.SXXK.ToKhai;

namespace Company.Interface.Report.SXXK
{
    public partial class ChiPhiXNKReport : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDichCollection TKMDCollection = new ToKhaiMauDichCollection();
        private int STT = 0;
        public ChiPhiXNKReport()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = this.TKMDCollection;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            
            lblSoToKhai.DataBindings.Add("Text", this.DataSource, "SoToKhai");
            lblMaLoaiHinh.DataBindings.Add("Text", this.DataSource, "MaLoaiHinh");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, "NgayDangKy","{0:dd/MM/yyyy}");
            lblSoTienKhoan.DataBindings.Add("Text", this.DataSource, "SoTienKhoan", "{0:n0}");
            lblTongSoTienKhoan.DataBindings.Add("Text", this.DataSource, "SoTienKhoan", "{0:n0}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");
            xrLabel77.Text = GlobalSettings.TieudeNgay;
            
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            STT++;
            lblSTT.Text = STT + "";
        }
    }
}
