﻿using System;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.Interface.Report;
using System.IO;
using Infragistics.Excel;
using System.Net.Mail;
using System.Drawing;
using Company.Interface.DanhMucChuan;
using System.Linq;

using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT.SXXK;

namespace Company.Interface
{
    public partial class ToKhaiMauDichForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //
        public ToKhaiTriGia TKTGG = new ToKhaiTriGia();
        //
        public long pTKMD_ID;
        public bool _bNew = true;
        public string NhomLoaiHinh = string.Empty;
        public static string tenPTVT = "";
        private static int i;
        public bool luu = false;
        public bool isEdited = false;
        public static int soDongHang;
        string msgInfor = string.Empty;
        FeedBackContent feedbackContent = null;
        #region Biến phục vụ cho việc In tờ khai sửa đổi bổ sung
        //BEGIN: DATLMQ bổ sung các biến dùng để lưu giá trị TK cũ 16/02/2011
        private string nguoiNhapKhau = string.Empty;
        private string nguoiXuatKhau = string.Empty;
        private string nguoiUyThac = string.Empty;
        private string PTVT = string.Empty;
        private string soHieuPTVT = string.Empty;
        private DateTime ngayDenPTVT = new DateTime(1900, 1, 1);
        private string nuocXuatKhau = string.Empty;
        private string nuocNhapKhau = string.Empty;
        private string dieuKienGiaoHang = string.Empty;
        private string phuongThucThanhToan = string.Empty;
        private string soGiayPhep = string.Empty;
        private DateTime ngayGiayPhep = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanGiayPhep = new DateTime(1900, 1, 1);
        private string hoaDonTM = string.Empty;
        private DateTime ngayHoaDonTM = new DateTime(1900, 1, 1);
        private string diaDiemDoHang = string.Empty;
        private string nguyenTe = string.Empty;
        private decimal tyGiaTinhThue = 0;
        private decimal tyGiaUSD = 0;
        private string soHopDong = string.Empty;
        private DateTime ngayHopDong = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanHopDong = new DateTime(1900, 1, 1);
        private string soVanDon = string.Empty;
        private DateTime ngayVanDon = new DateTime(1900, 1, 1);
        private string diaDiemXepHang = string.Empty;
        private decimal soContainer20;
        private decimal soContainer40;
        private decimal soKienHang;
        private decimal trongLuong;
        private double trongLuongNet;
        private decimal lePhiHaiQuan;
        private decimal phiBaoHiem;
        private decimal phiVanChuyen;
        private decimal phiKhac;
        //END

        //DATLMQ bổ sung biến phục vụ việc tự động lưu nội dung sửa đổi bổ sung 04/03/2011
        //public static List<Company.KD.BLL.KDT.HangMauDich> ListHangMauDichEdit = new List<Company.KD.BLL.KDT.HangMauDich>();
        //private static Company.KD.BLL.KDT.HangMauDich hangMDTK = new Company.KD.BLL.KDT.HangMauDich();
        public static string maHS_Edit = string.Empty;
        public static string tenHang_Edit = string.Empty;
        public static string maHang_Edit = string.Empty;
        public static string xuatXu_Edit = string.Empty;
        public static decimal soLuong_Edit;
        public static string dvt_Edit = string.Empty;
        public static double dongiaNT_Edit;
        public static double trigiaNT_Edit;
        //END
        #endregion
        private ToKhaiMauDich tkmd_old = new ToKhaiMauDich();

        SendCommand _sendCommand = new SendCommand();
        public ToKhaiMauDichForm()
        {
            InitializeComponent();
        }

        private void loadTKMDData()
        {
            ctrDonViHaiQuan.Ma = Company.KDT.SHARE.Components.Globals.trimMaHaiQuan(this.TKMD.MaHaiQuan);
            txtSoLuongPLTK.Value = this.TKMD.SoLuongPLTK;

            // Doanh nghiệp.
            txtMaDonVi.Text = this.TKMD.MaDoanhNghiep;
            txtTenDonVi.Text = this.TKMD.TenDoanhNghiep;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.Text = this.TKMD.TenDonViDoiTac;
            #region
            if (TKMD.ID == 0)
            {
                txtMaDaiLy.Text = GlobalSettings.MA_DON_VI;
                txtTenDaiLy.Text = GlobalSettings.TEN_DON_VI;

                txtMaDonViUyThac.Text = GlobalSettings.MA_DON_VI;
                txtTenDonViUyThac.Text = GlobalSettings.TEN_DON_VI;
            }
            else
            {

                txtMaDaiLy.Text = this.TKMD.MaDaiLyTTHQ;
                txtTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;

                txtMaDonViUyThac.Text = this.TKMD.MaDonViUT;
                txtTenDonViUyThac.Text = this.TKMD.TenDonViUT;
            }
            #endregion


            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Ma = this.TKMD.MaLoaiHinh;

            // Phương tiện vận tải.
            cbPTVT.SelectedValue = this.TKMD.PTVT_ID;
            txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
            if (TKMD.NgayDenPTVT != null || TKMD.NgayDenPTVT.Year > 1900)
                ccNgayDen.Text = this.TKMD.NgayDenPTVT.ToShortDateString();
            else
                ccNgayDen.Text = "";
            // Nước.
            if (this.TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;
            else
                ctrNuocXuatKhau.Ma = this.TKMD.NuocNK_ID;

            // ĐKGH.
            cbDKGH.SelectedValue = this.TKMD.DKGH_ID;

            // PTTT.
            cbPTTT.SelectedValue = this.TKMD.PTTT_ID;

            // Giấy phép.
            txtSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep.Year > 1900) ccNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToShortDateString();
            else ccNgayGiayPhep.Text = "";

            if (this.TKMD.NgayHetHanGiayPhep.Year > 1900) ccNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToShortDateString();
            else ccNgayHHGiayPhep.Text = "";
            //if (this.TKMD.SoGiayPhep.Length > 0) chkGiayPhep.Checked = true;

            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.Text = this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai.Year > 1900) ccNgayHDTM.Text = this.TKMD.NgayHoaDonThuongMai.ToShortDateString();
            else ccNgayHDTM.Text = "";
            //if (this.TKMD.SoHoaDonThuongMai.Length > 0) chkHoaDonThuongMai.Checked = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;

            // Nguyên tệ.
            ctrNguyenTe.Ma = this.TKMD.NguyenTe_ID;
            txtTyGiaTinhThue.Value = this.TKMD.TyGiaTinhThue;
            txtTyGiaUSD.Value = this.TKMD.TyGiaUSD;

            // Hợp đồng.
            txtSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong.Year > 1900) ccNgayHopDong.Text = this.TKMD.NgayHopDong.ToShortDateString();
            else ccNgayHopDong.Text = "";

            if (this.TKMD.NgayHetHanHopDong.Year > 1900) ccNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToShortDateString();
            else ccNgayHHHopDong.Text = "";
            //if (this.TKMD.SoHopDong.Length > 0) chkHopDong.Checked = true;

            // Vận tải đơn.
            txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
            if (this.TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToShortDateString();
            else ccNgayVanTaiDon.Text = "";
            // if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
            //if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

            // Tên chủ hàng.
            txtTenChuHang.Text = this.TKMD.TenChuHang;
            txtChucVu.Text = this.TKMD.ChucVu;

            // Container 20.
            txtSoContainer20.Value = this.TKMD.SoContainer20;

            // Container 40.
            txtSoContainer40.Value = this.TKMD.SoContainer40;

            // Số kiện hàng.
            txtSoKien.Value = this.TKMD.SoKien;

            // Trọng lượng.
            txtTrongLuong.Value = this.TKMD.TrongLuong;

            // Lệ phí HQ.
            txtLePhiHQ.Value = this.TKMD.LePhiHaiQuan;

            // Phí BH.
            txtPhiBaoHiem.Value = this.TKMD.PhiBaoHiem;

            // Phí VC.
            txtPhiVanChuyen.Value = this.TKMD.PhiVanChuyen;

            //Phí ngân hàng
            txtPhiNganHang.Value = this.TKMD.PhiKhac;

            txtTrongLuongTinh.Value = this.TKMD.TrongLuongNet;
            // Nước / xuất nhập khẩu 
            //grbNuocXK.Text = TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "N" ? TKMD.NuocNK_ID : TKMD.NuocXK_ID;

            if (this.TKMD.ID > 0)
            {
                if (this.TKMD.HMDCollection == null || this.TKMD.HMDCollection.Count == 0)
                    this.TKMD.LoadHMDCollection();

                if (this.TKMD.ChungTuTKCollection == null || this.TKMD.ChungTuTKCollection.Count == 0)
                    this.TKMD.LoadChungTuTKCollection();

                if (this.TKMD.TKTGCollection == null || this.TKMD.TKTGCollection.Count == 0)
                    this.TKMD.LoadTKTGCollection();

                if (this.TKMD.TKTGPP23Collection == null || this.TKMD.TKTGPP23Collection.Count == 0)
                    this.TKMD.LoadToKhaiTriGiaPP23();

                this.TKMD.LoadChungTuHaiQuan();

                if (this.TKMD.ListCO == null || this.TKMD.ListCO.Count == 0)
                    this.TKMD.LoadCO();
                if (this.TKMD.GiayNopTiens == null || this.TKMD.GiayNopTiens.Count == 0)
                    this.TKMD.LoadListGiayNopTiens();
            }

            ReViewData();

            //chung tu kem
            txtChungTu.Text = this.TKMD.GiayTo;

            //so to khai 
            if (this.TKMD.SoToKhai > 0)
                txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();

            if (this.TKMD.SoTiepNhan > 0)
                txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();

            txtDeXuatKhac.Text = this.TKMD.DeXuatKhac;
            txtLyDoSua.Text = this.TKMD.LyDoSua;

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                lblTrangThai.Text = "Chờ duyệt";
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                lblTrangThai.Text = "Chưa khai báo";
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                lblTrangThai.Text = "Không phê duyệt";
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                lblTrangThai.Text = "Đã duyệt";
            }

            if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                lblPhanLuong.Text = "Luồng Xanh";
            else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                lblPhanLuong.Text = "Luồng Vàng";
            else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                lblPhanLuong.Text = "Luồng Đỏ";
            else
                lblPhanLuong.Text = "Chưa phân luồng";
        }

        private void ReViewData()
        {
            try
            {
                dgList.DataSource = this.TKMD.HMDCollection;
                gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
                dgToKhaiTriGia.DataSource = this.TKMD.TKTGCollection;

                //minhcanhdn Sửa Sao chép danh sách CO
                gridCO.DataSource = this.TKMD.ListCO;
                dglistTKTK23.DataSource = this.TKMD.TKTGPP23Collection;

                dgList.Refetch();
                gridEX1.Refetch();
                dgToKhaiTriGia.Refetch();
                gridCO.Refetch();
                dglistTKTK23.Refetch();

            }
            catch
            {
                dgList.Refresh();
                gridEX1.Refresh();
                dgToKhaiTriGia.Refresh();
                gridCO.Refresh();
                dglistTKTK23.Refresh();
            }
        }
        void ctrNguyenTe_ValueChanged(object sender, System.EventArgs e)
        {
            txtTyGiaTinhThue.Text = ctrNguyenTe.TyGia + "";
            if (ctrNguyenTe.Ma == "USD")
            {
                txtTyGiaUSD.Text = ctrNguyenTe.TyGia + "";
            }
        }
        private void khoitao_DuLieuChuan()
        {
            //Người đại diện doanh nghiệp
            txtTenChuHang.Text = GlobalSettings.NguoiLienHe;
            txtChucVu.Text = GlobalSettings.ChucVu;


            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Nhom = this.NhomLoaiHinh;
            ctrLoaiHinhMauDich.Ma = this.NhomLoaiHinh + "01";

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            if (this.NhomLoaiHinh.StartsWith("N"))
                ctrCuaKhau.Ma = GlobalSettings.DIA_DIEM_DO_HANG;
            else
                ctrCuaKhau.Ma = GlobalSettings.CUA_KHAU;

            //Số thập phân trọng lượng
            if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTPTrongLuongTK").Equals(""))
                txtTrongLuong.DecimalDigits = 2;
            else
                txtTrongLuong.DecimalDigits = Convert.ToInt32(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTPTrongLuongTK"));
            //Số thập phân Số lượng của Hàng mậu dịch
            if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTPSoLuongHMD").Equals(""))
                dgList.RootTable.Columns["SoLuong"].FormatString = "N3";
            else
                dgList.RootTable.Columns["SoLuong"].FormatString = "N" + Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTPSoLuongHMD");

            if (GlobalSettings.IsDaiLy)
            {
                txtMaDaiLy.Text = GlobalSettings.MA_DAI_LY;
                txtTenDaiLy.Text = GlobalSettings.TEN_DAI_LY;
            }

        }

        private void khoitao_GiaoDienToKhai()
        {
            if (NhomLoaiHinh.Substring(0, 1).ToUpper() == "N")
                this.Tag = this.Name + "_N";
            else
                this.Tag = this.Name + "_X";
            this.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 3);
            string loaiToKhai = this.NhomLoaiHinh.Substring(0, 1);
            dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.SoLuongHMD;
            //grbHopDong.Enabled = chkHopDong.Enabled = true;
            //chkGiayPhep.Enabled = gbGiayPhep.Enabled = true;
            if (loaiToKhai == "N")
            {
                // Tiêu đề.
                this.Text = setText("Tờ khai nhập khẩu ", "Import declaration") + "(" + this.NhomLoaiHinh + ")";
                uiGroupBox11.Visible = true;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = true;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = true;
                //chkHoaDonThuongMai.Enabled = grbHoaDonThuongMai.Enabled = true;
                // chkVanTaiDon.Enabled = grbVanTaiDon.Enabled = true;
                // chkDiaDiemXepHang.Enabled = grbDiaDiemXepHang.Enabled = true;
                //grbDiaDiemXepHang.Visible = chkDiaDiemXepHang.Visible = true;                
            }
            else
            {
                // Tiêu đề.
                this.Text = setText("Tờ khai xuất khẩu ", "Export declaration") + "(" + this.NhomLoaiHinh + ")";
                this.uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                // Người XK / Người NK.
                grbNguoiNK.Text = setText("Người xuất khẩu", "Exporter");
                grbNguoiXK.Text = setText("Người nhập khẩu", "Importer");

                // Phương tiện vận tải.              
                //uiGroupBox11.Enabled = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Visible"));
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = false;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = false;
                //chkHoaDonThuongMai.Visible =
                //grbHoaDonThuongMai.Enabled = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Visible"));
                //chkVanTaiDon.Visible =
                //ccNgayVanTaiDon.Text = DateTime.Now.ToShortDateString();
                // grbVanTaiDon.Enabled = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Visible"));
                grbVanTaiDon.Enabled = false;
                // Vận tải đơn.
                // VanDon2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                // Nước XK.
                grbNuocXK.Text = setText("Nước nhập khẩu", "Importing country");

                // Địa điểm xếp hàng / Địa điểm dỡ hàng.
                grbDiaDiemDoHang.Text = setText("Cửa khẩu xuất hàng", "Exporting gate");
                //grbDiaDiemXepHang.Enabled = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Visible"));
                // chkDiaDiemXepHang.Visible = false;
            }

            // Loại hình gia công.
            if (this.NhomLoaiHinh == "NGC" || this.NhomLoaiHinh == "XGC")
            {
                //chkHopDong.Checked = true;
                //chkHopDong.Visible = false;
                txtSoHopDong.BackColor = ccNgayHopDong.BackColor = ccNgayHHHopDong.BackColor = Color.FromArgb(255, 255, 192);
                txtSoHopDong.ButtonStyle = EditButtonStyle.Ellipsis;

            }
            else if (this.NhomLoaiHinh == "NSX" || this.NhomLoaiHinh == "XSX")
            {

            }
        }

        //-----------------------------------------------------------------------------------------

        private void ToKhaiNhapKhauSXXK_Form_Load(object sender, EventArgs e)
        {
            if (TKMD.ID == 0) TKMD.PhanLuong = "";
            #region KhanhHN add phụ vụ cho tờ khai sửa
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                tkmd_old.ID = TKMD.ID;
                tkmd_old.LoadHMDCollection();
            }
            #endregion
            // Người nhập khẩu / Người xuất khẩu.
            txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
            txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
            txtTenDonViDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            //txtTyGiaUSD.Value = GlobalSettings.TY_GIA_USD;//linhhtn mark

            ccNgayDen.Value = DateTime.Today;
            this.khoitao_DuLieuChuan();
            switch (this.OpenType)
            {
                case Company.KDT.SHARE.Components.OpenFormType.View:
                    this.loadTKMDData();
                    //this.ViewTKMD();
                    this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);
                    break;
                case Company.KDT.SHARE.Components.OpenFormType.Edit:
                    this.loadTKMDData();
                    this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);
                    break;
                case Company.KDT.SHARE.Components.OpenFormType.Insert:

                    this.TKMD.TKTGCollection = new ToKhaiTriGiaCollection();
                    dgToKhaiTriGia.DataSource = this.TKMD.TKTGCollection;
                    txtTyGiaUSD.Value = Company.KDT.SHARE.Components.Globals.GetNguyenTe("USD");//HungTQ Update 13/12/2010.
                    break;
            }


            //KD_ToKhaiNhap KD_tkn = Company.KD.BLL.DataTransferObjectMapper.Mapper.ToDataTransferToKhaiNhap(TKMD);
            //string msg = Company.KDT.SHARE.Components.Helpers.Serializer(KD_tkn);

            this.khoitao_GiaoDienToKhai();
            SetCommandStatus();
            //if (NhomLoaiHinh.IndexOf("X")==0)
            //    ToKhaiTriGia.Visible = Janus.Windows.UI.InheritableBoolean.False;
            //if (TKMD.ID > 0)
            //{
            //    MsgSend sendXML = new MsgSend();
            //    sendXML.LoaiHS = "TK";
            //    sendXML.master_id = TKMD.ID;
            //    if (sendXML.Load())
            //    {
            //        //lblTrangThai.Text = setText("Đang chờ xác nhận ","Wait to comfirm");
            //        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;//false
            //        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;//false
            //        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //        ToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.True;//false
            //    }
            //    else
            //        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //}
            if (GlobalSettings.TuDongTinhThue == "0")
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
            //{
            //dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            //TopRebar1.Visible = false;
            //}
        }

        private void ViewTKMD()
        {
            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            ChungTuKemTheo.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            ctrDonViHaiQuan.ReadOnly = true;
            txtSoLuongPLTK.ReadOnly = true;
            cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            // Doanh nghiệp.
            txtMaDonVi.ReadOnly = true;
            txtTenDonVi.ReadOnly = true;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.ReadOnly = true;

            // Đại lý TTHQ.
            txtMaDaiLy.ReadOnly = true;
            txtTenDaiLy.ReadOnly = true;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.ReadOnly = true;

            // Phương tiện vận tải.
            cbPTVT.ReadOnly = true;
            txtSoHieuPTVT.ReadOnly = true;
            ccNgayDen.ReadOnly = true;

            // Nước.
            ctrNuocXuatKhau.ReadOnly = true;

            // ĐKGH.
            cbDKGH.ReadOnly = true;

            // PTTT.
            cbPTTT.ReadOnly = true;

            // Giấy phép.
            txtSoGiayPhep.ReadOnly = true;
            ccNgayGiayPhep.ReadOnly = true;
            ccNgayHHGiayPhep.ReadOnly = true;


            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.ReadOnly = true;
            ccNgayHDTM.ReadOnly = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.ReadOnly = true;

            // Nguyên tệ.
            ctrNguyenTe.ReadOnly = true;
            txtTyGiaTinhThue.ReadOnly = true;
            txtTyGiaUSD.ReadOnly = true;

            // Hợp đồng.
            txtSoHopDong.ReadOnly = true;
            ccNgayHopDong.ReadOnly = true;
            ccNgayHHHopDong.ReadOnly = true;

            // Vận tải đơn.
            txtSoVanTaiDon.ReadOnly = true;
            ccNgayVanTaiDon.ReadOnly = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.ReadOnly = true;

            // Tên chủ hàng.
            txtTenChuHang.ReadOnly = true;
            txtChucVu.ReadOnly = true;

            // Container 20.
            txtSoContainer20.ReadOnly = true;

            // Container 40.
            txtSoContainer40.ReadOnly = true;

            // Số kiện hàng.
            txtSoKien.ReadOnly = true;

            // Trọng lượng.
            txtTrongLuong.ReadOnly = true;

            // Lệ phí HQ.
            txtLePhiHQ.ReadOnly = true;

            // Phí BH.
            txtPhiBaoHiem.ReadOnly = true;

            // Phí VC.
            txtPhiVanChuyen.ReadOnly = true;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHoaDonThuongMai_CheckedChanged(object sender, EventArgs e)
        {
            //grbHoaDonThuongMai.Enabled = chkHoaDonThuongMai.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkGiayPhep_CheckedChanged(object sender, EventArgs e)
        {
            //gbGiayPhep.Enabled = chkGiayPhep.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHopDong_CheckedChanged(object sender, EventArgs e)
        {
            //grbHopDong.Enabled = chkHopDong.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkVanTaiDon_CheckedChanged(object sender, EventArgs e)
        {
            // grbVanTaiDon.Enabled = chkVanTaiDon.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkDiaDiemXepHang_CheckedChanged(object sender, EventArgs e)
        {
            // grbDiaDiemXepHang.Enabled = chkDiaDiemXepHang.Checked;
        }
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        private void ReadExcel()
        {
            ReadExcelForm reform = new ReadExcelForm();
            this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            reform.TKMD = this.TKMD;
            reform.ShowDialog(this);
            try
            {
                dgList.DataSource = TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        //-----------------------------------------------------------------------------------------

        private void themHang()
        {
            cvError.ContainerToValidate = grbNguyenTe;
            cvError.Validate();
            if (!cvError.IsValid) return;

            HangMauDichForm f = new HangMauDichForm();
            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            f.TKMD = this.TKMD;
            f.NhomLoaiHinh = this.NhomLoaiHinh;
            f.MaHaiQuan = ctrDonViHaiQuan.Ma;
            //f.HMDCollection = this.TKMD.HMDCollection;
            f.MaNguyenTe = ctrNguyenTe.Ma;
            f.TyGiaTT = Convert.ToDouble(txtTyGiaTinhThue.Value);
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
            //this.TKMD.HMDCollection = f.HMDCollection;
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.SoLuongHMD;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            SetCommandStatus();
            this.TKMD.SoLuongPLTK = (short)Helpers.GetLoading(TKMD.HMDCollection.Count);
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin tờ khai.
        /// </summary>
        public void Save()
        {
            bool valid = false;
            //Update by Khanhhn
            #region Fix lỗi khai báo dữ liệu thiếu thông tin chứng từ
            if (this.NhomLoaiHinh.Substring(0, 1) == "N")
            {
                valid = Globals.ValidateNull(txtSoVanTaiDon, epError, "Số vận đơn");
                if (!valid)
                {
                    txtSoVanTaiDon.Focus();
                    ShowMessage("Bạn chưa nhập vận đơn", false);
                    return;
                }
            }
            else
            {
                valid = Globals.ValidateNull(txtSoHopDong, epError, "Số hợp đồng");
                valid |= Globals.ValidateNull(txtSoGiayPhep, epError, "Số giấy phép");
                if (!valid) return;
                else epError.Clear();
            }
            #endregion
            bool isKhaibaoSua = this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ? true : false;
            if (isKhaibaoSua)
            {
                if (txtLyDoSua.Text.Trim() == "")
                {
                    MLMessages("Chưa nhập lý do sửa tờ khai", "MSG_SAV06", "", false);
                    return;
                }
                try
                {
                    ToKhaiMauDich TKMD_SuaDoi = new ToKhaiMauDich();
                    NoiDungToKhai noiDungTK = new NoiDungToKhai();
                    List<NoiDungToKhai> listNoiDungTK = new List<NoiDungToKhai>();
                    NoiDungDieuChinhTKDetail noiDungEditDetail = new NoiDungDieuChinhTKDetail();
                    NoiDungDieuChinhTK noiDungEdit = new NoiDungDieuChinhTK();
                    noiDungEdit.TKMD_ID = TKMD.ID;
                    noiDungEdit.SoTK = TKMD.SoToKhai;
                    noiDungEdit.NgayDK = TKMD.NgayDangKy;
                    noiDungEdit.MaLoaiHinh = TKMD.MaLoaiHinh;
                    noiDungEdit.NgaySua = DateTime.Now;
                    i = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID);
                    if (i == 0)
                    {
                        i++;
                        noiDungEdit.SoDieuChinh = i;
                        noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        noiDungEdit.InsertUpdate();
                        luu = true;
                    }
                    else
                    {
                        if (luu == false)
                        {
                            i++;
                            noiDungEdit.SoDieuChinh = i;
                            noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            noiDungEdit.InsertUpdate();
                            luu = true;
                        }
                    }

                    #region Lưu tạm giá trị của TK cũ vào các biến tương ứng
                    //Người nhập khẩu & Người xuất khẩu
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        nguoiNhapKhau = TKMD.TenDoanhNghiep;
                        nguoiXuatKhau = TKMD.TenDonViDoiTac;
                    }
                    else
                    {
                        nguoiNhapKhau = TKMD.TenDonViDoiTac;
                        nguoiXuatKhau = TKMD.TenDoanhNghiep;
                    }
                    //Người ủy thác
                    nguoiUyThac = TKMD.TenDonViUT;
                    //PTVT
                    PTVT = TKMD.PTVT_ID;
                    tenPTVT = TKMD.SoHieuPTVT;
                    ngayDenPTVT = TKMD.NgayDenPTVT;
                    //Nước xuất khẩu & Nước nhập khẩu
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        nuocXuatKhau = TKMD.NuocXK_ID;
                    }
                    else
                    {
                        nuocNhapKhau = TKMD.NuocNK_ID;
                    }
                    //Điều kiện giao hàng
                    dieuKienGiaoHang = TKMD.DKGH_ID;
                    //Phương thức thanh toán
                    phuongThucThanhToan = TKMD.PTTT_ID;
                    //Giấy phép
                    soGiayPhep = TKMD.SoGiayPhep;
                    ngayGiayPhep = TKMD.NgayGiayPhep;
                    ngayHetHanGiayPhep = TKMD.NgayHetHanGiayPhep;
                    //Hóa đơn thương mại
                    hoaDonTM = TKMD.SoHoaDonThuongMai;
                    ngayHoaDonTM = TKMD.NgayHoaDonThuongMai;
                    //Địa điểm dở hàng & Địa điểm xếp hàng
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        diaDiemDoHang = TKMD.CuaKhau_ID;
                        diaDiemXepHang = TKMD.DiaDiemXepHang;
                    }
                    else
                    {
                        diaDiemXepHang = TKMD.CuaKhau_ID;
                        diaDiemDoHang = TKMD.DiaDiemXepHang;
                    }
                    //Nguyên tệ
                    nguyenTe = TKMD.NguyenTe_ID;
                    //Tỷ giá tính thuế
                    tyGiaTinhThue = TKMD.TyGiaTinhThue;
                    //Tỷ giá USD
                    tyGiaUSD = TKMD.TyGiaUSD;
                    //Hợp đồng
                    soHopDong = TKMD.SoHopDong;
                    ngayHopDong = TKMD.NgayHopDong;
                    ngayHetHanHopDong = TKMD.NgayHetHanHopDong;
                    //Vận tải đơn
                    soVanDon = TKMD.SoVanDon;
                    ngayVanDon = TKMD.NgayVanDon;
                    //Tổng trọng lượng: số Cont20, số Cont40, số kiện hàng
                    soContainer20 = TKMD.SoContainer20;
                    soContainer40 = TKMD.SoContainer40;
                    soKienHang = TKMD.SoKien;
                    //Trọng lượng
                    trongLuong = TKMD.TrongLuong;
                    //Trọng lượng tịnh
                    trongLuongNet = TKMD.TrongLuongNet;
                    //Lệ phí HQ
                    lePhiHaiQuan = TKMD.LePhiHaiQuan;
                    //Phí vận chuyện
                    phiVanChuyen = TKMD.PhiVanChuyen;
                    //Phí bảo hiểm
                    phiBaoHiem = TKMD.PhiBaoHiem;
                    //Phí khác
                    phiKhac = TKMD.PhiKhac;
                    #endregion

                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        #region 1. Người xuất khẩu
                        //Tên Đơn vị đối tác
                        TKMD_SuaDoi.TenDonViDoiTac = txtTenDonViDoiTac.Text;
                        if (TKMD_SuaDoi.TenDonViDoiTac != TKMD.TenDonViDoiTac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + grbNguoiXK.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = grbNguoiXK.Tag.ToString();
                                noiDungTK.Ten = "1. Người xuất khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiXuatKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiXuatKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 3. Đơn vị ủy thác
                        //Tên đơn vị Ủy thác
                        TKMD_SuaDoi.TenDonViUT = txtTenDonViUyThac.Text.Trim();
                        if (TKMD_SuaDoi.TenDonViUT != TKMD.TenDonViUT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label6.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label6.Tag.ToString();
                                noiDungTK.Ten = "3. Người ủy thác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiUyThac;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiUyThac;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 6. Hóa đơn Thương mại
                        //Số Hóa đơn Thương mại
                        TKMD_SuaDoi.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text.Trim();
                        if (TKMD_SuaDoi.SoHoaDonThuongMai != TKMD.SoHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label15.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label15.Tag.ToString();
                                noiDungTK.Ten = "6. Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + hoaDonTM;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + hoaDonTM;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hóa đơn Thương mại
                        if (ccNgayHDTM.Text.Equals(""))
                            TKMD_SuaDoi.NgayHoaDonThuongMai = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHoaDonThuongMai = Convert.ToDateTime(ccNgayHDTM.Text);
                        if (TKMD_SuaDoi.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label16.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label16.Tag.ToString();
                                noiDungTK.Ten = "6. Ngày Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 7. Giấy phép
                        //Số Giấy phép
                        TKMD_SuaDoi.SoGiayPhep = txtSoGiayPhep.Text.Trim();
                        if (TKMD_SuaDoi.SoGiayPhep != TKMD.SoGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label11.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label11.Tag.ToString();
                                noiDungTK.Ten = "7. Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soGiayPhep;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soGiayPhep;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Giấy phép
                        if (ccNgayGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayGiayPhep = Convert.ToDateTime(ccNgayGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayGiayPhep != TKMD.NgayGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label12.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label12.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hết hạn Giấy phép
                        if (ccNgayHHGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanGiayPhep = Convert.ToDateTime(ccNgayHHGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label33.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label33.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày hết hạn Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 8. Hợp đồng
                        //Số Hợp đồng
                        TKMD_SuaDoi.SoHopDong = txtSoHopDong.Text;
                        if (TKMD_SuaDoi.SoHopDong != TKMD.SoHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label13.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label13.Tag.ToString();
                                noiDungTK.Ten = "8. Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soHopDong;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soHopDong;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hợp đồng
                        if (ccNgayHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHopDong = Convert.ToDateTime(ccNgayHopDong.Text);
                        if (TKMD_SuaDoi.NgayHopDong != TKMD.NgayHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label14.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label14.Tag.ToString();
                                noiDungTK.Ten = "8. Ngày Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày hết hạn Hợp đồng
                        if (ccNgayHHHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanHopDong = Convert.ToDateTime(ccNgayHHHopDong.Text);
                        if (TKMD_SuaDoi.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label34.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label34.Tag.ToString();
                                noiDungTK.Ten = "8. Ngày hết hạn Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 9. Vận tải đơn
                        //Số vận tải đơn
                        TKMD_SuaDoi.SoVanDon = txtSoVanTaiDon.Text;
                        if (TKMD_SuaDoi.SoVanDon != TKMD.SoVanDon)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label19.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label19.Tag.ToString();
                                noiDungTK.Ten = "9. Vận tải đơn:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soVanDon;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoVanDon;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soVanDon;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoVanDon;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày vận tải đơn
                        if (ccNgayVanTaiDon.Text.Equals(""))
                            TKMD_SuaDoi.NgayVanDon = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayVanDon = Convert.ToDateTime(ccNgayVanTaiDon.Text);
                        if (TKMD_SuaDoi.NgayVanDon != TKMD.NgayVanDon)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label20.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label20.Tag.ToString();
                                noiDungTK.Ten = "9. Ngày vận tải đơn:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayVanDon.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayVanDon.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayVanDon.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayVanDon.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 10. Cảng xếp hàng
                        TKMD_SuaDoi.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                        if (TKMD_SuaDoi.DiaDiemXepHang != TKMD.DiaDiemXepHang)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + grbDiaDiemXepHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = grbDiaDiemXepHang.Tag.ToString();
                                noiDungTK.Ten = "10. Cảng xếp hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + diaDiemXepHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + diaDiemXepHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 11. Cảng dở hàng
                        TKMD_SuaDoi.CuaKhau_ID = ctrCuaKhau.Ma.Trim();
                        if (TKMD_SuaDoi.CuaKhau_ID != TKMD.CuaKhau_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + grbDiaDiemDoHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = grbDiaDiemDoHang.Tag.ToString();
                                noiDungTK.Ten = "11. Cảng dở hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + diaDiemDoHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.CuaKhau_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + diaDiemDoHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.CuaKhau_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 12. Phương tiện vận tải
                        //Loại Phương tiện vận tải
                        TKMD_SuaDoi.PTVT_ID = cbPTVT.SelectedValue.ToString().Trim();
                        if (TKMD_SuaDoi.PTVT_ID != TKMD.PTVT_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label17.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label17.Tag.ToString();
                                noiDungTK.Ten = "12. Phương tiện vận tải:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + PTVT;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PTVT_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + PTVT;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PTVT_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Số hiệu Phương tiện vận tải
                        TKMD_SuaDoi.SoHieuPTVT = txtSoHieuPTVT.Text;
                        if (TKMD_SuaDoi.SoHieuPTVT != TKMD.SoHieuPTVT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + lblSoHieuPTVT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = lblSoHieuPTVT.Tag.ToString();
                                noiDungTK.Ten = "12. Số hiệu Phương tiện vận tải:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soHieuPTVT;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHieuPTVT;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soHieuPTVT;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHieuPTVT;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày đến Phương tiện vận tải
                        if (ccNgayDen.Text.Equals(""))
                            TKMD_SuaDoi.NgayDenPTVT = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayDenPTVT = Convert.ToDateTime(ccNgayDen.Text);
                        if (TKMD_SuaDoi.NgayDenPTVT != TKMD.NgayDenPTVT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + lblNgayDenPTVT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = lblNgayDenPTVT.Tag.ToString();
                                noiDungTK.Ten = "12. Ngày đến Phương tiện vận tải:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayDenPTVT.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayDenPTVT.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayDenPTVT.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayDenPTVT.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 13. Nước xuất khẩu
                        TKMD_SuaDoi.NuocXK_ID = ctrNuocXuatKhau.Ma.Trim();
                        if (TKMD_SuaDoi.NuocXK_ID != TKMD.NuocXK_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + ctrNuocXuatKhau.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = ctrNuocXuatKhau.Tag.ToString();
                                noiDungTK.Ten = "13. Nước xuất khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nuocXuatKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NuocXK_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nuocXuatKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NuocXK_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 14. Điều kiện giao hàng
                        TKMD_SuaDoi.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
                        if (TKMD_SuaDoi.DKGH_ID != TKMD.DKGH_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbDKGH.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbDKGH.Tag.ToString();
                                noiDungTK.Ten = "14. Điều kiện giao hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + dieuKienGiaoHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + dieuKienGiaoHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 15. Phương thức thanh toán
                        TKMD_SuaDoi.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
                        if (TKMD_SuaDoi.PTTT_ID != TKMD.PTTT_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbPTTT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbPTTT.Tag.ToString();
                                noiDungTK.Ten = "15. Phương thức thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phuongThucThanhToan;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phuongThucThanhToan;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 16. Đồng tiền thanh toán
                        TKMD_SuaDoi.NguyenTe_ID = ctrNguyenTe.Ma.Trim();
                        if (TKMD_SuaDoi.NguyenTe_ID != TKMD.NguyenTe_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + ctrNguyenTe.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = ctrNguyenTe.Tag.ToString();
                                noiDungTK.Ten = "16. Đồng tiền thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguyenTe;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguyenTe;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 17. Tỷ giá tính thuế
                        TKMD_SuaDoi.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                        if (TKMD_SuaDoi.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTyGiaTinhThue.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTyGiaTinhThue.Tag.ToString();
                                noiDungTK.Ten = "17. Tỷ giá tính thuế:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + tyGiaTinhThue.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + tyGiaTinhThue.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container20
                        TKMD_SuaDoi.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                        if (TKMD_SuaDoi.SoContainer20 != TKMD.SoContainer20)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer20.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer20.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont20:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer20.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer20.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container40
                        TKMD_SuaDoi.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                        if (TKMD_SuaDoi.SoContainer40 != TKMD.SoContainer40)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer40.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer40.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont40:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer40.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer40.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số kiện hàng
                        TKMD_SuaDoi.SoKien = Convert.ToDecimal(txtSoKien.Text);
                        if (TKMD_SuaDoi.SoKien != TKMD.SoKien)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoKien.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoKien.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Tổng số kiện:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soKienHang.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soKienHang.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng
                        TKMD_SuaDoi.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);
                        if (TKMD_SuaDoi.TrongLuong != TKMD.TrongLuong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuong.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuong.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng trọng lượng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuong.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuong.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng tịnh
                        TKMD_SuaDoi.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Text);
                        if (TKMD_SuaDoi.TrongLuongNet != TKMD.TrongLuongNet)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuongTinh.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuongTinh.Tag.ToString();
                                noiDungTK.Ten = "32. Ghi chép khác: Trọng lượng tịnh:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuongNet.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuongNet.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Lệ phí hải quan
                        TKMD_SuaDoi.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                        if (TKMD_SuaDoi.LePhiHaiQuan != TKMD.LePhiHaiQuan)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtLePhiHQ.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtLePhiHQ.Tag.ToString();
                                noiDungTK.Ten = "Phí hải quan:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + lePhiHaiQuan.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + lePhiHaiQuan.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí bảo hiểm
                        TKMD_SuaDoi.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                        if (TKMD_SuaDoi.PhiBaoHiem != TKMD.PhiBaoHiem)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiBaoHiem.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiBaoHiem.Tag.ToString();
                                noiDungTK.Ten = "Phí bảo hiểm:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiBaoHiem.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiBaoHiem.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí vận chuyển
                        TKMD_SuaDoi.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                        if (TKMD_SuaDoi.PhiVanChuyen != TKMD.PhiVanChuyen)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiVanChuyen.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiVanChuyen.Tag.ToString();
                                noiDungTK.Ten = "Phí vận chuyển:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiVanChuyen.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiVanChuyen.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí khác
                        TKMD_SuaDoi.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Text);
                        if (TKMD_SuaDoi.PhiKhac != TKMD.PhiKhac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiNganHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiNganHang.Tag.ToString();
                                noiDungTK.Ten = "Phí khác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiKhac.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiKhac.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region 2. Người nhập khẩu
                        //Tên Đơn vị đối tác
                        TKMD_SuaDoi.TenDonViDoiTac = txtTenDonViDoiTac.Text;
                        if (TKMD_SuaDoi.TenDonViDoiTac != TKMD.TenDonViDoiTac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NguoiNhapKhau" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = "NguoiNhapKhau";
                                noiDungTK.Ten = "2. Người nhập khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiNhapKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiNhapKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 3. Đơn vị ủy thác
                        //Tên đơn vị Ủy thác
                        TKMD_SuaDoi.TenDonViUT = txtTenDonViUyThac.Text.Trim();
                        if (TKMD_SuaDoi.TenDonViUT != TKMD.TenDonViUT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label6.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label6.Tag.ToString();
                                noiDungTK.Ten = "3. Người ủy thác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiUyThac;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiUyThac;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 6. Giấy phép
                        //Số Giấy phép
                        TKMD_SuaDoi.SoGiayPhep = txtSoGiayPhep.Text.Trim();
                        if (TKMD_SuaDoi.SoGiayPhep != TKMD.SoGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label11.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label11.Tag.ToString();
                                noiDungTK.Ten = "6. Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soGiayPhep;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soGiayPhep;
                                    noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Giấy phép
                        if (ccNgayGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayGiayPhep = Convert.ToDateTime(ccNgayGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayGiayPhep != TKMD.NgayGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label12.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label12.Tag.ToString();
                                noiDungTK.Ten = "6. Ngày Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hết hạn Giấy phép
                        if (ccNgayHHGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanGiayPhep = Convert.ToDateTime(ccNgayHHGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label33.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label33.Tag.ToString();
                                noiDungTK.Ten = "6. Ngày hết hạn Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 7. Hợp đồng
                        //Số Hợp đồng
                        TKMD_SuaDoi.SoHopDong = txtSoHopDong.Text;
                        if (TKMD_SuaDoi.SoHopDong != TKMD.SoHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label13.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label13.Tag.ToString();
                                noiDungTK.Ten = "7. Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soHopDong;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soHopDong;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hợp đồng
                        if (ccNgayHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHopDong = Convert.ToDateTime(ccNgayHopDong.Text);
                        if (TKMD_SuaDoi.NgayHopDong != TKMD.NgayHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label14.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label14.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày hết hạn Hợp đồng
                        if (ccNgayHHHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanHopDong = Convert.ToDateTime(ccNgayHHHopDong.Text);
                        if (TKMD_SuaDoi.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label34.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label34.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày hết hạn Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 8. Hóa đơn thương mại
                        //Số Hóa đơn Thương mại
                        TKMD_SuaDoi.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text.Trim();
                        if (TKMD_SuaDoi.SoHoaDonThuongMai != TKMD.SoHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label15.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label15.Tag.ToString();
                                noiDungTK.Ten = "8. Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + hoaDonTM;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + hoaDonTM;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hóa đơn Thương mại
                        if (ccNgayHDTM.Text.Equals(""))
                            TKMD_SuaDoi.NgayHoaDonThuongMai = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHoaDonThuongMai = Convert.ToDateTime(ccNgayHDTM.Text);
                        if (TKMD_SuaDoi.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label16.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label16.Tag.ToString();
                                noiDungTK.Ten = "8. Ngày Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 9. Cảng xếp hàng
                        //TKMD_SuaDoi.DiaDiemXepHang = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(ctrCuaKhau.Ma);
                        if (TKMD_SuaDoi.DiaDiemXepHang != TKMD.DiaDiemXepHang)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "CangXepHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = "CangXepHang";
                                noiDungTK.Ten = "9. Cảng xếp hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + diaDiemXepHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + diaDiemXepHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 10. Nước nhập khẩu
                        TKMD_SuaDoi.NuocNK_ID = ctrNuocXuatKhau.Ma.Trim();
                        if (TKMD_SuaDoi.NuocNK_ID != TKMD.NuocNK_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocNK" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = "NuocNK";
                                noiDungTK.Ten = "10. Nước nhập khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nuocNhapKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NuocNK_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nuocNhapKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NuocNK_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 11. Điều kiện giao hàng
                        TKMD_SuaDoi.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
                        if (TKMD_SuaDoi.DKGH_ID != TKMD.DKGH_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbDKGH.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbDKGH.Tag.ToString();
                                noiDungTK.Ten = "11. Điều kiện giao hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + dieuKienGiaoHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + dieuKienGiaoHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 12. Phương thức thanh toán
                        TKMD_SuaDoi.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
                        if (TKMD_SuaDoi.PTTT_ID != TKMD.PTTT_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbPTTT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbPTTT.Tag.ToString();
                                noiDungTK.Ten = "12. Phương thức thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phuongThucThanhToan;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phuongThucThanhToan;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 13. Đồng tiền thanh toán
                        TKMD_SuaDoi.NguyenTe_ID = ctrNguyenTe.Ma.Trim();
                        if (TKMD_SuaDoi.NguyenTe_ID != TKMD.NguyenTe_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + ctrNguyenTe.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = ctrNguyenTe.Tag.ToString();
                                noiDungTK.Ten = "13. Đồng tiền thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguyenTe;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguyenTe;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 14. Tỷ giá tính thuế
                        TKMD_SuaDoi.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                        if (TKMD_SuaDoi.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTyGiaTinhThue.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTyGiaTinhThue.Tag.ToString();
                                noiDungTK.Ten = "14. Tỷ giá tính thuế:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + tyGiaTinhThue.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + tyGiaTinhThue.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container20
                        TKMD_SuaDoi.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                        if (TKMD_SuaDoi.SoContainer20 != TKMD.SoContainer20)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer20.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer20.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont20:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer20.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer20.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container40
                        TKMD_SuaDoi.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                        if (TKMD_SuaDoi.SoContainer40 != TKMD.SoContainer40)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer40.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer40.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont40:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer40.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer40.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số kiện hàng
                        TKMD_SuaDoi.SoKien = Convert.ToDecimal(txtSoKien.Text);
                        if (TKMD_SuaDoi.SoKien != TKMD.SoKien)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoKien.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoKien.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Tổng số kiện:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soKienHang.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soKienHang.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng
                        TKMD_SuaDoi.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);
                        if (TKMD_SuaDoi.TrongLuong != TKMD.TrongLuong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuong.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuong.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng trọng lượng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuong.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuong.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng tịnh
                        TKMD_SuaDoi.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Text);
                        if (TKMD_SuaDoi.TrongLuongNet != TKMD.TrongLuongNet)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuongTinh.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuongTinh.Tag.ToString();
                                noiDungTK.Ten = "32. Ghi chép khác: Trọng lượng tịnh:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuongNet.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuongNet.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Lệ phí hải quan
                        TKMD_SuaDoi.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                        if (TKMD_SuaDoi.LePhiHaiQuan != TKMD.LePhiHaiQuan)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtLePhiHQ.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtLePhiHQ.Tag.ToString();
                                noiDungTK.Ten = "Phí hải quan:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + lePhiHaiQuan.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + lePhiHaiQuan.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí bảo hiểm
                        TKMD_SuaDoi.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                        if (TKMD_SuaDoi.PhiBaoHiem != TKMD.PhiBaoHiem)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiBaoHiem.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiBaoHiem.Tag.ToString();
                                noiDungTK.Ten = "Phí bảo hiểm:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiBaoHiem.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiBaoHiem.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí vận chuyển
                        TKMD_SuaDoi.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                        if (TKMD_SuaDoi.PhiVanChuyen != TKMD.PhiVanChuyen)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiVanChuyen.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiVanChuyen.Tag.ToString();
                                noiDungTK.Ten = "Phí vận chuyển:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiVanChuyen.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiVanChuyen.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí khác
                        TKMD_SuaDoi.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Text);
                        if (TKMD_SuaDoi.PhiKhac != TKMD.PhiKhac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiNganHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiNganHang.Tag.ToString();
                                noiDungTK.Ten = "Phí khác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiKhac.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiKhac.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion
                    }
                    #region lưu thay đổi khi sửa hàng ở tờ khai sửa

                    tkmd_old.LoadHMDCollection();
                    foreach (Company.KD.BLL.KDT.HangMauDich item in tkmd_old.HMDCollection)
                    {
                        IEnumerable<Company.KD.BLL.KDT.HangMauDich> hangMD = from h in TKMD.HMDCollection
                                                                             where h.MaPhu == item.MaPhu
                                                                             select h;
                        if (hangMD.ToList().Count > 0)
                        {
                            this.AutoInsertHMDToTKEdit(TKMD, item, hangMD.ToList()[0]);
                        }
                        else
                        {
                            noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = "Xóa dòng hàng thứ: " + item.SoThuTuHang.ToString() + ". Và có mã: " + item.MaPhu;
                            noiDungEditDetail.NoiDungTKSua = string.Empty;
                            noiDungEditDetail.InsertUpdate();
                        }
                    }
                    foreach (Company.KD.BLL.KDT.HangMauDich item in TKMD.HMDCollection)
                    {
                        IEnumerable<Company.KD.BLL.KDT.HangMauDich> hangMD = from h in tkmd_old.HMDCollection
                                                                             where h.MaPhu == item.MaPhu
                                                                             select h;
                        if (hangMD.ToList().Count == 0)
                        {
                            noiDungEditDetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = string.Empty;
                            noiDungEditDetail.NoiDungTKSua = "Thêm dòng hàng thứ: " + item.SoThuTuHang.ToString() + ". Và có mã: " + item.MaPhu;
                            noiDungEditDetail.InsertUpdate();
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình lưu tờ khai sửa: " + ex.Message, false);
                    return;
                }
                //}
            }
            if (this.NhomLoaiHinh.Substring(0, 1) == "N")
            {
                cvError.ContainerToValidate = this;
                cvError.Validate();
                valid = cvError.IsValid;
                //if (ctrLoaiHinhMauDich.Ma.Equals("NSX04"))
                //    valid = true;
            }
            else
            {
                cvError.ContainerToValidate = this.txtTenDonViDoiTac;
                cvError.Validate();
                bool valid1 = cvError.IsValid;
                cvError.ContainerToValidate = this.grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                valid = valid1 && valid2;
            }
            valid &= Globals.ValidateNull(txtTenChuHang, epError, "Đại diện doanh nghiệp");

            if (valid)
            {
                if (this.TKMD.HMDCollection.Count == 0)
                {
                    // MessageBox.Show("Chưa nhập thông tin hàng của tờ khai", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    MLMessages("Chưa nhập thông tin hàng của tờ khai", "MSG_SAV06", "", false);
                    return;
                }
                if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
                {
                    epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "Invalid input value"));
                    epError.SetIconPadding(txtTyGiaTinhThue, -8);
                    txtTyGiaTinhThue.Focus();
                    return;
                }

                this.TKMD.SoTiepNhan = this.TKMD.SoTiepNhan;
                this.TKMD.MaHaiQuan = ctrDonViHaiQuan.Ma;
                if (!isKhaibaoSua)
                {
                    this.TKMD.NgayTiepNhan = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayDangKy = DateTime.Parse("01/01/1900");
                }
                this.TKMD.SoHang = (short)this.TKMD.HMDCollection.Count;
                //this.TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);
                this.TKMD.SoLuongPLTK = (short)Helpers.GetLoading(TKMD.HMDCollection.Count);
                // Doanh nghiệp.
                this.TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                // Đơn vị đối tác.
                this.TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


                this.TKMD.MaDonViUT = txtMaDonViUyThac.Text;
                this.TKMD.TenDonViUT = txtTenDonViUyThac.Text;

                // Đại lý TTHQ.
                this.TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                this.TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                // Đề xuất khác
                this.TKMD.DeXuatKhac = txtDeXuatKhac.Text;
                //Lý do sửa
                if (isKhaibaoSua)
                    this.TKMD.LyDoSua = txtLyDoSua.Text;
                // Loại hình mậu dịch.
                this.TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;
                if (string.IsNullOrEmpty(TKMD.GUIDSTR)) TKMD.GUIDSTR = Guid.NewGuid().ToString();
                // Giấy phép.
                if (txtSoGiayPhep.Text.Trim().Length > 0)
                {
                    this.TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                    if (!ccNgayGiayPhep.IsNullDate)
                        this.TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
                    else
                        this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    if (!ccNgayHHGiayPhep.IsNullDate)
                        this.TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;
                    else
                        this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }
                else
                {
                    this.TKMD.SoGiayPhep = "";
                    this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }

                // 7. Hợp đồng.
                if (txtSoHopDong.Text.Trim().Length > 0)
                {
                    this.TKMD.SoHopDong = txtSoHopDong.Text;
                    // Ngày HD.
                    if (ccNgayHopDong.IsNullDate)
                        this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHopDong = ccNgayHopDong.Value;
                    // Ngày hết hạn HD.
                    if (ccNgayHHHopDong.IsNullDate)
                        this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;
                }
                else
                {
                    this.TKMD.SoHopDong = "";
                    this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                }

                // Hóa đơn thương mại.
                if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                {
                    this.TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                    if (ccNgayHDTM.IsNullDate)
                        this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;
                }
                else
                {
                    this.TKMD.SoHoaDonThuongMai = "";
                    this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                }
                // Phương tiện vận tải.
                this.TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                this.TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                if (ccNgayDen.IsNullDate)
                    this.TKMD.NgayDenPTVT = DateTime.Parse("01/01/1900");
                else
                    this.TKMD.NgayDenPTVT = ccNgayDen.Value;

                // Vận tải đơn.
                if (txtSoVanTaiDon.Text.Trim().Length > 0)
                {
                    this.TKMD.SoVanDon = txtSoVanTaiDon.Text;
                    if (ccNgayVanTaiDon.IsNullDate)
                        this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayVanDon = ccNgayVanTaiDon.Value;
                }
                else
                {
                    this.TKMD.SoVanDon = "";
                    this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                }
                // Nước.
                if (this.NhomLoaiHinh.Substring(0, 1) == "N")
                {
                    this.TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                    this.TKMD.NuocNK_ID = "VN".PadRight(3);
                }
                else
                {
                    this.TKMD.NuocXK_ID = "VN".PadRight(3);
                    this.TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
                }

                // Địa điểm xếp hàng.
                //if (chkDiaDiemXepHang.Checked)
                this.TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                // else
                //    this.TKMD.DiaDiemXepHang = "";

                // Địa điểm dỡ hàng.
                this.TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                // Điều kiện giao hàng.
                this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                // Đồng tiền thanh toán.
                this.TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                // Phương thức thanh toán.
                this.TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                //
                this.TKMD.TenChuHang = txtTenChuHang.Text;
                this.TKMD.ChucVu = txtChucVu.Text;
                this.TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                this.TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                this.TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                this.TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                // Tổng trị giá khai báo (Chưa tính).
                this.TKMD.TongTriGiaKhaiBao = 0;
                //
                if (!isKhaibaoSua)
                    this.TKMD.TrangThaiXuLy = -1;
                this.TKMD.LoaiToKhaiGiaCong = "NPL";
                //
                this.TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                this.TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

                //this.tkmd.NgayGui = DateTime.Parse("01/01/1900");

                if (GlobalSettings.TuDongTinhThue == "1" && TKMD.MaLoaiHinh.Contains("N"))
                    this.tinhLaiThue();
                else
                {
                    tinhTongTriGiaKhaiBao();
                }
                this.TKMD.GiayTo = txtChungTu.Text;

                try
                {
                    if (TKMD.SoToKhai == 0)
                        TKMD.PhanLuong = "";

                    //chek them moi :  
                    if (string.IsNullOrEmpty(TKMD.GUIDSTR))
                    {
                        TKMD.GUIDSTR = Guid.NewGuid().ToString();
                    }
                    this.TKMD.InsertUpdateFull();
                    //Neu la chinh sua, cap nhat lai ID luu tam la ID cua TKMD dang mo: tranh bi copy trung du lieu cua TK cu.
                    if (_bNew == false)
                        pTKMD_ID = TKMD.ID;

                    //Lypt update date 22/01/2010
                    if (_bNew)
                    {
                        _bNew = false;
                        //Copy Van don                        
                        IList<VanDon> VanDoncollection = new List<VanDon>();
                        VanDoncollection = VanDon.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (VanDon vd in VanDoncollection)
                        {
                            long vdResult = VanDon.InsertVanDon(vd.SoVanDon, vd.NgayVanDon, vd.HangRoi, vd.SoHieuPTVT, vd.NgayDenPTVT, vd.MaHangVT, vd.TenHangVT, vd.TenPTVT, vd.QuocTichPTVT, vd.NuocXuat_ID, vd.MaNguoiNhanHang,
                                vd.TenNguoiNhanHang, vd.MaNguoiGiaoHang, vd.TenNguoiNhanHang, vd.CuaKhauNhap_ID, vd.CuaKhauXuat, vd.MaNguoiNhanHangTrungGian, vd.TenNguoiNhanHangTrungGian, vd.MaCangXepHang, vd.TenCangXepHang,
                                vd.MaCangDoHang, vd.TenCangDoHang, this.TKMD.ID, vd.DKGH_ID, vd.DiaDiemGiaoHang, vd.NoiDi, vd.SoHieuChuyenDi, vd.NgayKhoiHanh, vd.TongSoKien, vd.LoaiKien, string.Empty);
                            List<Container> ContColl = Company.KDT.SHARE.QuanLyChungTu.Container.SelectCollectionBy_VanDon_ID(vd.ID);//vd.ID
                            for (int v = 0; v < ContColl.Count; v++)
                            {
                                ContColl[v].VanDon_ID = vdResult;
                            }
                            Company.KDT.SHARE.QuanLyChungTu.Container.InsertCollection(ContColl);
                            break;
                        }
                        //copy Hop dong
                        List<HopDongThuongMai> HopDongCollection = new List<HopDongThuongMai>();
                        HopDongCollection = (List<HopDongThuongMai>)HopDongThuongMai.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (HopDongThuongMai hd in HopDongCollection)
                        {
                            hd.LoaiKB = 0;
                            hd.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            hd.NgayTiepNhan = new DateTime(1900, 1, 1);
                            hd.SoTiepNhan = 0;
                            hd.GuidStr = Guid.NewGuid().ToString();

                            long hdResult = HopDongThuongMai.InsertHopDongThuongMai(hd.SoHopDongTM, hd.NgayHopDongTM, hd.ThoiHanThanhToan, hd.NguyenTe_ID, hd.PTTT_ID, hd.DKGH_ID, hd.DiaDiemGiaoHang, hd.MaDonViMua, hd.TenDonViMua,
                                hd.MaDonViBan, hd.TenDonViBan, hd.TongTriGia, hd.ThongTinKhac, this.TKMD.ID, hd.GuidStr, hd.LoaiKB, hd.SoTiepNhan, hd.NgayTiepNhan, hd.TrangThai, hd.NamTiepNhan, hd.MaDoanhNghiep);
                            //---Hang hop dong
                            List<HopDongThuongMaiDetail> HopDongDTMColl = HopDongThuongMaiDetail.SelectCollectionBy_HopDongTM_ID(hd.ID);
                            for (int k = 0; k < HopDongDTMColl.Count; k++)
                            {
                                HopDongDTMColl[k].HopDongTM_ID = hdResult;
                            }
                            HopDongThuongMaiDetail.InsertCollection(HopDongDTMColl);
                            break;
                        }
                        //copy Giay phep
                        List<GiayPhep> GPColl = new List<GiayPhep>();
                        GPColl = GiayPhep.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (GiayPhep gp in GPColl)
                        {
                            gp.LoaiKB = 0;
                            gp.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            gp.NgayTiepNhan = new DateTime(1900, 1, 1);
                            gp.SoTiepNhan = 0;
                            gp.GuidStr = Guid.NewGuid().ToString();

                            long gpResult = GiayPhep.InsertGiayPhep(gp.SoGiayPhep, gp.NgayGiayPhep, gp.NgayHetHan, gp.NguoiCap, gp.NoiCap, gp.MaDonViDuocCap, gp.TenDonViDuocCap, gp.MaCoQuanCap, gp.TenQuanCap, gp.ThongTinKhac, gp.MaDoanhNghiep,
                                this.TKMD.ID, gp.GuidStr, gp.LoaiKB, gp.SoTiepNhan, gp.NgayTiepNhan, gp.TrangThai, gp.NamTiepNhan);
                            //--Hang Giay phep
                            List<HangGiayPhepDetail> HangGP = HangGiayPhepDetail.SelectCollectionBy_GiayPhep_ID(gp.ID);
                            for (int h = 0; h < HangGP.Count; h++)
                            {
                                HangGP[h].GiayPhep_ID = gpResult;
                            }
                            HangGiayPhepDetail.InsertCollection(HangGP);
                            break;
                        }
                        // copy Hoa don thuong mai
                        List<HoaDonThuongMai> HDTMColl = new List<HoaDonThuongMai>();
                        HDTMColl = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (HoaDonThuongMai hdtm in HDTMColl)
                        {
                            hdtm.LoaiKB = 0;
                            hdtm.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            hdtm.NgayTiepNhan = new DateTime(1900, 1, 1);
                            hdtm.SoTiepNhan = 0;
                            hdtm.GuidStr = Guid.NewGuid().ToString();

                            long hdonResult = HoaDonThuongMai.InsertHoaDonThuongMai(hdtm.SoHoaDon, hdtm.NgayHoaDon, hdtm.NguyenTe_ID, hdtm.PTTT_ID, hdtm.DKGH_ID, hdtm.MaDonViMua, hdtm.TenDonViMua, hdtm.MaDonViBan, hdtm.TenDonViBan,
                                hdtm.ThongTinKhac, this.TKMD.ID, hdtm.GuidStr, hdtm.LoaiKB, hdtm.SoTiepNhan, hdtm.NgayTiepNhan, hdtm.TrangThai, hdtm.NamTiepNhan, hdtm.MaDoanhNghiep);
                            //--Hang hoa don thuong mai
                            List<HoaDonThuongMaiDetail> HoaDonTMColl = HoaDonThuongMaiDetail.SelectCollectionBy_HoaDonTM_ID(hdtm.ID);
                            for (int d = 0; d < HoaDonTMColl.Count; d++)
                            {
                                HoaDonTMColl[d].HoaDonTM_ID = hdonResult;
                            }
                            HoaDonThuongMaiDetail.InsertCollection(HoaDonTMColl);
                            break;

                        }
                        // copy CO
                        List<CO> COColl = new List<CO>();
                        COColl = (List<CO>)CO.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (CO co in COColl)
                        {
                            co.LoaiKB = 0;
                            co.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            co.NgayTiepNhan = new DateTime(1900, 1, 1);
                            co.SoTiepNhan = 0;
                            co.GuidStr = Guid.NewGuid().ToString();

                            CO.InsertCO(co.SoCO, co.NgayCO, co.ToChucCap, co.NuocCapCO, co.MaNuocXKTrenCO, co.MaNuocNKTrenCO, co.TenDiaChiNguoiXK, co.TenDiaChiNguoiNK, co.LoaiCO,
                                co.ThongTinMoTaChiTiet, co.MaDoanhNghiep, co.NguoiKy, co.NoCo, co.ThoiHanNop, this.TKMD.ID, co.GuidStr, co.LoaiKB, co.SoTiepNhan, co.NgayTiepNhan, co.TrangThai, co.NamTiepNhan, co.NgayHetHan, co.NgayKhoiHanh,
                                co.CangXepHang, co.CangDoHang);
                        }
                        //Copy De nghi chuyen cua khau
                        List<DeNghiChuyenCuaKhau> chuyenCK = new List<DeNghiChuyenCuaKhau>();
                        chuyenCK = DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (DeNghiChuyenCuaKhau ck in chuyenCK)
                        {
                            ck.LoaiKB = 0;
                            ck.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            ck.NgayTiepNhan = new DateTime(1900, 1, 1);
                            ck.SoTiepNhan = 0;
                            ck.GuidStr = Guid.NewGuid().ToString();

                            DeNghiChuyenCuaKhau.InsertDeNghiChuyenCuaKhau(ck.ThongTinKhac, ck.MaDoanhNghiep, this.TKMD.ID, ck.GuidStr, ck.LoaiKB, ck.SoTiepNhan, ck.NgayTiepNhan, ck.TrangThai,
                                ck.NamTiepNhan, ck.SoVanDon, ck.NgayVanDon, ck.ThoiGianDen, ck.DiaDiemKiemTra, ck.TuyenDuong);
                        }

                        //Copy Chung tu kem
                        List<ChungTuKem> chungTuKem = new List<ChungTuKem>();
                        chungTuKem = ChungTuKem.SelectCollectionBy_TKMDID(pTKMD_ID);
                        long ctkID = 0;
                        foreach (ChungTuKem ctk in chungTuKem)
                        {
                            ctk.LoaiKB = "0";
                            ctk.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO.ToString();
                            ctk.NGAYTN = new DateTime(1900, 1, 1);
                            ctk.SOTN = 0;
                            ctk.GUIDSTR = Guid.NewGuid().ToString();

                            ctkID = ChungTuKem.InsertChungTuKem(ctk.SO_CT, ctk.NGAY_CT, ctk.MA_LOAI_CT, ctk.DIENGIAI, ctk.LoaiKB, ctk.TrangThaiXuLy, ctk.Tempt, ctk.MessageID, ctk.GUIDSTR, ctk.KDT_WAITING, ctk.KDT_LASTINFO, ctk.SOTN, ctk.NGAYTN, ctk.TotalSize, ctk.Phanluong, ctk.HuongDan, this.TKMD.ID);

                            ctk.LoadListCTChiTiet();
                            foreach (ChungTuKemChiTiet chiTiet in ctk.listCTChiTiet)
                            {
                                ChungTuKemChiTiet.InsertChungTuKemChiTiet(ctkID, chiTiet.FileName, chiTiet.FileSize, chiTiet.NoiDung);
                            }
                        }

                        //Copy To khai tri gia
                        try
                        {
                            ToKhaiTriGiaCollection TKTGColl = new ToKhaiTriGiaCollection();
                            ToKhaiTriGia tktg = new ToKhaiTriGia();
                            tktg.TKMD_ID = pTKMD_ID;
                            TKTGColl = tktg.SelectCollectionBy_TKMD_ID();

                            if (TKTGColl.Count > 0)
                            {
                                HangTriGia hangtg = new HangTriGia();
                                hangtg.TKTG_ID = TKTGColl[0].ID;
                                HangTriGiaCollection hangcoll = hangtg.SelectCollectionBy_TKTG_ID();
                                TKTGColl[0].TKMD_ID = this.TKMD.ID;
                                tktg.Insert(TKTGColl);
                                //--Copy HangTKTG
                                for (int tg = 0; tg < hangcoll.Count; tg++)
                                {
                                    hangcoll[tg].TKTG_ID = TKTGColl[0].ID;
                                }
                                hangtg.Insert(hangcoll);
                            }
                        }
                        catch (Exception EX)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(EX);
                            ShowMessage("Lỗi dữ liệu tờ khai trị giá,vui lòng nhập thêm tờ khai trị giá nếu có", false);
                        }
                        //Copy To khai tri gia PP23 
                        ToKhaiTriGiaPP23Collection TKTGPP23Coll = new ToKhaiTriGiaPP23Collection();
                        ToKhaiTriGiaPP23 tktgPP23 = new ToKhaiTriGiaPP23();
                        tktgPP23.TKMD_ID = pTKMD_ID;
                        TKTGPP23Coll = tktgPP23.SelectCollectionBy_TKMD_ID();
                        for (int j = 0; j < TKTGPP23Coll.Count; j++)
                        {
                            TKTGPP23Coll[j].TKMD_ID = this.TKMD.ID;
                        }
                        tktgPP23.Insert(TKTGPP23Coll);

                        if (this.TKMD.HMDCollection == null || this.TKMD.HMDCollection.Count == 0)
                            this.TKMD.LoadHMDCollection();
                        if (this.TKMD.ChungTuTKCollection == null || this.TKMD.ChungTuTKCollection.Count == 0)
                            this.TKMD.LoadChungTuTKCollection();
                        if (this.TKMD.TKTGCollection == null || this.TKMD.TKTGCollection.Count == 0)
                            this.TKMD.LoadTKTGCollection();
                        if (this.TKMD.TKTGPP23Collection == null || this.TKMD.TKTGPP23Collection.Count == 0)
                            this.TKMD.LoadToKhaiTriGiaPP23();
                        this.TKMD.LoadChungTuHaiQuan();
                        if (this.TKMD.ListCO == null || this.TKMD.ListCO.Count == 0)
                            this.TKMD.LoadCO();
                        if (this.TKMD.GiayNopTiens == null || this.TKMD.GiayNopTiens.Count == 0)
                            this.TKMD.LoadListGiayNopTiens();
                    }
                    dgToKhaiTriGia.DataSource = this.TKMD.TKTGCollection;
                    try
                    { dgToKhaiTriGia.Refetch(); }
                    catch { }
                    
                    //minhcanhdn Sửa Sao chép danh sách CO
                    gridCO.DataSource = this.TKMD.ListCO;
                    gridCO.Refetch();
                    dglistTKTK23.DataSource = this.TKMD.TKTGPP23Collection;
                    dglistTKTK23.Refetch();
                    //Het Lypt Update

                    //add extra :
                    //TKTGG.ID = TKMD.ID;
                    //TKTGG.InsertUpdateFull();
                    //
                    this.TKMD.LoadChungTuTKCollection();
                    this.TKMD.LoadHMDCollection();
                    dgList.DataSource = this.TKMD.HMDCollection;
                    dgList.Refetch();
                    gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
                    gridEX1.Refetch();
                    MLMessages("Lưu tờ khai thành công.", "MSG_SAV02", "", false);
                    SetCommandStatus();
                }
                catch (Exception ex)
                {
                    ShowMessage(setText("Dữ liệu của bạn không hợp lệ ", "Invalid input value") + ex.Message, false);
                }
            }
        }

        private void tinhLaiThue()
        {
            this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            this.TKMD.TongTriGiaKhaiBao = 0;
            double Phi = Convert.ToDouble(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            double TongTriGiaHang = 0;
            foreach (Company.KD.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                TongTriGiaHang += hmd.TriGiaKB;
                this.TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
            }
            double TriGiaTTMotDong = 0;
            if (TongTriGiaHang == 0) return;
            TriGiaTTMotDong = (1 + Phi / TongTriGiaHang) * Convert.ToDouble(TKMD.TyGiaTinhThue);
            this.TKMD.TongTriGiaTinhThue = 0;
            foreach (Company.KD.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (hmd.FOC)
                {
                    hmd.TriGiaTT = hmd.ThueXNK = hmd.ThueTTDB = hmd.ThueGTGT = hmd.TriGiaThuKhac = hmd.ThueSuatXNK = hmd.ThueSuatTTDB = hmd.ThueSuatGTGT = hmd.TyLeThuKhac = 0;
                    continue;
                }

                hmd.DonGiaTT = TriGiaTTMotDong * hmd.DonGiaKB;
                hmd.TriGiaTT = Math.Round(hmd.DonGiaTT * Convert.ToDouble(hmd.SoLuong), MidpointRounding.AwayFromZero);
                this.TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
                if (!hmd.ThueTuyetDoi)
                    hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                else
                {
                    hmd.ThueXNK = Math.Round(hmd.DonGiaTuyetDoi * Convert.ToDouble(TKMD.TyGiaTinhThue) * Convert.ToDouble(hmd.SoLuong));
                }
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                hmd.TriGiaThuKhac = hmd.TyLeThuKhac > 0 ? Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero) : hmd.TriGiaThuKhac;
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB + hmd.TriGiaThuKhac) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
            }
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void tinhTongTriGiaKhaiBao()
        {
            this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            foreach (Company.KD.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                this.TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
                this.TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
            }
        }
        //-----------------------------------------------------------------------------------------
        private void inPhieuTN()
        {
            if (this.TKMD.SoTiepNhan == 0) return;
            Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
            phieuTN.phieu = "TỜ KHAI";
            phieuTN.soTN = this.TKMD.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = TKMD.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void inToKhai()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);
                return;
            }
            if (this.TKMD.TrangThaiXuLy != 1)
            {
                if (MLMessages("Tờ khai chưa được duyệt, bạn có muốn in hay không?", "MSG_PRI02", "", true) != "Yes") return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewForm f = new ReportViewForm();
                    f.TKMD = this.TKMD;
                    f.ShowDialog(this);
                    break;

                case "X":
                    ReportViewTKXForm f1 = new ReportViewTKXForm();
                    f1.TKMD = this.TKMD;
                    f1.ShowDialog(this);
                    break;
            }

        }
        private void InToKhaiTQDT_TT15()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);
                return;
            }
            if (this.TKMD.TrangThaiXuLy != 1)
            {
                if (MLMessages("Tờ khai chưa được duyệt, bạn có muốn in hay không?", "MSG_PRI02", "", true) != "Yes") return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNTQDTFormTT196 f = new ReportViewTKNTQDTFormTT196();
                    f.TKMD = this.TKMD;
                    f.ShowDialog(this);
                    break;

                case "X":
                    ReportViewTKXTQDTFormTT196 f1 = new ReportViewTKXTQDTFormTT196();
                    f1.TKMD = this.TKMD;
                    f1.ShowDialog(this);
                    break;
            }
        }

        /// <summary>
        /// Thiết lập trạng thái các nút lệnh.
        /// </summary>
        private void SetCommandStatus()
        {
            try
            {
                //Update by Khanhhn
                // Không mở tiện ích in khi chưa nhập tờ khai
                if (TKMD.ID == 0)
                    TienIch.Enabled = TienIch1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                else
                    TienIch.Enabled = TienIch1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtLyDoSua.Enabled = false;
                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET
                    || TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                    dgToKhaiTriGia.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;

                    gridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdLayPhanHoiSoTiepNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSuaTK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdHuyTK.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        lblTrangThai.Text = "Chưa khai báo";
                        txtSoTiepNhan.Text = "";
                        lblPhanLuong.Text = "Chưa phân luồng";
                        txtSoToKhai.Text = "";
                    }
                    else
                        if (TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
                        {
                            txtLyDoSua.Enabled = true;
                        }
                        else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            lblTrangThai.Text = "Đã hủy";
                        }
                    ChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                }
                else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdLayPhanHoiSoTiepNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    dgToKhaiTriGia.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    gridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                    lblTrangThai.Text = "Đã duyệt";
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString("0");
                    lblPhanLuong.Text = "Chưa phân luồng";
                    txtSoToKhai.Text = this.TKMD.SoToKhai.ToString("0");

                    cmdToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    cmdBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSuaTK.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyTK.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    ChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdLayPhanHoiSoTiepNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    lblTrangThai.Text = "Chờ duyệt";
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString("0");
                    if (this.TKMD.SoToKhai > 0)
                        txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();
                    else
                        txtSoToKhai.Text = "";
                    //lblPhanLuong.Text = "Chưa phân luồng";
                    dgToKhaiTriGia.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    gridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSuaTK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdHuyTK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    dgList.Refetch();
                }
                else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdLayPhanHoiSoTiepNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    dgToKhaiTriGia.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    gridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                    lblTrangThai.Text = "Đã hủy";
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString("0");
                    txtSoToKhai.Text = this.TKMD.SoToKhai.ToString("0");

                    cmdToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    cmdBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSuaTK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdHuyTK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }

                if (TKMD.PhanLuong == "1")
                {
                    lblPhanLuong.ForeColor = Color.Green;
                    lblPhanLuong.Text = "Luồng Xanh";
                }
                else if (TKMD.PhanLuong == "2")
                {
                    lblPhanLuong.Text = "Luồng Vàng";
                    lblPhanLuong.ForeColor = Color.Yellow;

                }
                else if (TKMD.PhanLuong == "3")
                {
                    lblPhanLuong.Text = "Luồng Đỏ";
                    lblPhanLuong.ForeColor = Color.Red;

                }
                else
                {
                    lblPhanLuong.Text = "Chưa phân luồng";
                    lblPhanLuong.ForeColor = Color.Black;
                }

                //datlmq 07102010: Bo sung them trang thai Cho huy
                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                    cmdSuaTK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdHuyTK.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    cmdToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    lblTrangThai.Text = "Chờ duyệt";
                }
                //datlmq 07102010: Bo sung them trang thai Khong phe duyet
                else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;

                    lblTrangThai.Text = "Không phê duyệt";

                    //datlmq 05102010: Tờ khai ở trạng thái không phê duyệt:
                    //+ Nếu chưa có số tờ khai thì không cho phép sửa TK, hủy TK
                    //+ Nếu có số tờ khai thì cho phép sửa TK, hủy TK
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    if (TKMD.SoToKhai > 0)
                    {
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                        cmdSuaTK.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdHuyTK.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                        cmdToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        ChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    }
                    else
                    {
                        cmdSuaTK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdHuyTK.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                        cmdToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        ChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    }
                }
                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                //datlmq 07102010: Neu to khai da co so TK thi khong cho phep huy khai bao
                if (TKMD.SoToKhai > 0)
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;


                //Hungtq updated 24/02/2012.
                #region Cho phép xem thông tin Chứng từ kèm khi Tờ khai ở trạng thái: Chờ duyệt, Đã duyệt, Sửa tờ khai, Hủy tờ khai, Không phê duyệt, Chờ hủy tờ khai, Hủy tờ khai.

                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET
                    || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET
                    || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                    || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    )
                {
                    ChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    cmdToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }

                #endregion

                //Hien thi thong tin so luong chung tu dinh kem tren menu 'Chung tu dinh kem'
                SetStatusChungTuDinhKem(this.TKMD);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //-----------------------------------------------------------------------------------------
        private void HienThiThongDiep()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.TKMD.ID;
            bool isToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            form.DeclarationIssuer = isToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT;
            form.ShowDialog(this);
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //Hungtq, Update 24/09/2012
                case "cmdInTKTGPP1":
                    InToKhaiTriGiaPP1();
                    break;
                //Hungtq, Update 06/11/2011
                case "cmdInAnDinhThue":
                    InAnDinhThue(this.TKMD.ID, this.TKMD.GUIDSTR);
                    break;

                case "cmdKetQuaXuLy":
                    this.HienThiThongDiep();
                    break;
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdTinhLaiThue":
                    this.tinhLaiThue();
                    ShowMessage(setText("Tổng trị giá nguyên tệ là : ", "Total value in original currency: ") + this.TKMD.TongTriGiaKhaiBao.ToString("N3"), false);
                    break;
                case "cmdReadExcel":
                    this.ReadExcel();
                    break;
                case "cmdThemHang":
                    this.themHang();
                    ReViewData();
                    break;
                case "ThemHang":
                    this.themHang();
                    ReViewData();
                    break;
                case "ToKhai":
                    this.inToKhai();
                    break;
                case "cmdPrint":
                    this.inToKhai();
                    break;
                case "cmdInToKhaiTQDT_TT15":
                    this.InToKhaiTQDT_TT15();
                    break;
                case "cmdInContainer196":
                    this.InContainerTT196();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;

                //datlmq bổ sung cho phép khai báo ở mnuMain 07/12/2010
                case "TruyenDuLieu":
                    // TUNGNT: Hoi y kien truoc khi khai bao
                    //if (this.ShowMessage("Bạn có muốn khai báo thông tin tờ khai này không?", true) == "Yes")
                    //{
                    //    KhaiBaoToKhai();
                    //    checkTrangThaiXuLyTK();
                    //}
                    break;
                case "cmdSend":
                    // TUNGNT: Hoi y kien truoc khi khai bao
                    if (this.ShowMessage("Bạn có muốn khai báo thông tin tờ khai này không?", true) == "Yes")
                    {
                        //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        //{
                        SendV3();
                        //}
                        //else
                        //{
                        //    KhaiBaoToKhai();
                        //}
                        checkTrangThaiXuLyTK();
                    }
                    break;
                case "ChungTuKemTheo":
                    this.AddChungTu();
                    ReViewData();
                    break;
                case "NhanDuLieu":
                    //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    //{
                    FeedBackV3();
                    //}
                    //else
                    //{
                    //    this.LayThongTinPhanHoi();
                    //}
                    checkTrangThaiXuLyTK();
                    break;
                case "cmdXacNhanThongTin":
                    this.GetRequestionALL();
                    checkTrangThaiXuLyTK();
                    break;
                case "FileDinhKem":
                    this.GetFileDinhKem();
                    break;
                case "ToKhaiTriGia":
                    this.AddToKhaiTriGia();
                    ReViewData();
                    break;
                case "Huy":
                    //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    //{
                    CancelV3();
                    //}
                    //else
                    //    this.HuyKhaiBaoCDTQDT();
                    break;
                case "cmdToKhaiTGPP2":
                    this.AddToKhaiTriGia2();
                    ReViewData();
                    break;
                case "cmdToKhaiTGPP3":
                    this.AddToKhaiTriGia3();
                    ReViewData();
                    break;
                case "GiayPhep":
                    this.txtSoGiayPhep_ButtonClick(null, null);
                    break;
                case "HoaDon":
                    this.txtSoHoaDonThuongMai_ButtonClick(null, null);
                    break;
                case "HopDong":
                    this.txtSoHopDong_ButtonClick(null, null);
                    break;
                case "CO":
                    this.QuanLyCO("");
                    break;
                case "VanDon":
                    this.txtSoVanTaiDon_ButtonClick(null, null);
                    break;
                case "ToKhaiA4":
                    this.InToKhaiA4();
                    break;
                case "cmdExportExcel":
                    this.ExportExcel();
                    break;
                case "cmdBoSungGiayPhep":
                    this.txtSoGiayPhep_ButtonClick("1", null);
                    break;
                case "cmdBoSungHoaDon":
                    this.txtSoHoaDonThuongMai_ButtonClick("1", null);
                    break;
                case "cmdBoSungHopDong":
                    this.txtSoHopDong_ButtonClick("1", null);
                    break;
                case "cmdBoSungCO":
                    this.QuanLyCO("1");
                    break;

                case "cmdChuyenCuaKhau":
                    this.QuanLyChuyenCuaKhau("");
                    break;
                case "cmdBoSungChuyenCuaKhau":
                    this.QuanLyChuyenCuaKhau("1");
                    break;
                case "cmdHuyTK":
                    HuyTOKhaiMD();
                    break;
                case "cmdSuaTK":
                    SuaToKhaiDaDuyet();
                    break;
                case "cmdInTKTQ":
                    InToKhaiThongQuanA4();
                    break;
                case "cmdInTKDTSuaDoiBoSung":
                    InToKhaiDienTuSuaDoiBoSung();
                    break;
                case "cmdContainer":
                    InContainer();
                    break;
                case "cmdChungTuKem":
                    AddChungTuAnh("");
                    ReViewData();
                    break;
                case "cmdBoSungChungTuDinhKemDangAnh":
                    AddChungTuAnh("1");
                    ReViewData();
                    break;
                case "cmdChungTuNo":
                    AddChungTuNo();
                    break;
                case "cmdGiayNopTien":
                    ShowGiayNopTien();
                    break;
                case "cmdInToKhaiXNKTaiCho196":
                    InToKhaiXNKTaiCho196();
                    break;

            }
        }
        private void InToKhaiXNKTaiCho196()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                //ShowMessage("Bạn hãy lưu trước khi in.",false);
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 1).ToUpper())
            {
                case "N":
                    ReportViewTKNTQDT_TaiCho_FormTT196 f = new ReportViewTKNTQDT_TaiCho_FormTT196();
                    f.TKMD = this.TKMD;
                    f.Show();
                    break;

                case "X":
                    ReportViewTKXTQDT_TaiCho_FormTT196 f1 = new ReportViewTKXTQDT_TaiCho_FormTT196();
                    f1.TKMD = this.TKMD;
                    f1.Show();
                    break;
            }
        }
        /// <summary>
        /// In tờ khai trị giá PP1
        /// </summary>
        private void InToKhaiTriGiaPP1()
        {
            if (this.TKMD.TKTGCollection.Count > 0)
            {
                ToKhaiTriGia TKTG = this.TKMD.TKTGCollection[0];

                ReportViewTKTGA4Form f1 = new ReportViewTKTGA4Form();
                f1.TKTG = TKTG;
                f1.TKMD = this.TKMD;
                f1.ShowDialog(this);
            }
        }

        //datlmq bo sung ham KiemTraTrangThaiToKhai 20/09/2010
        private void checkTrangThaiXuLyTK()
        {
            NoiDungChinhSuaTKForm noiDungDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            //Nếu là tờ khai sửa đã được duyệt
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa đang chờ duyệt
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa mới
            else
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        ndDieuChinhTK.Update();
                    }
                }
            }
        }

        private void InToKhaiDienTuSuaDoiBoSung()
        {
            InToKhaiSuaDoiBoSungForm frmInToKhaiSuaDoiBoSung = new InToKhaiSuaDoiBoSungForm();
            InToKhaiSuaDoiBoSungForm.TKMD = TKMD;
            frmInToKhaiSuaDoiBoSung.ShowDialog(this);
        }

        private void fillNoiDungTKChinh(Worksheet worksheet, int row, int column)
        {
            int j = 1;
            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                //if (ndDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                NoiDungChinhSuaTKDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column].Value = ndDieuChinhTKChiTiet.NoiDungTKChinh;
                        int thuong1 = worksheet.Rows[row].Cells[column].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row, column + 10);
                            mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKChinh;
                            worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                }
                //}
            }
        }

        private void fillNoiDungTKSua(Worksheet worksheet, int row, int column)
        {
            int j = 1;
            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                //if (ndDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                NoiDungChinhSuaTKDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column + 11].Value = ndDieuChinhTKChiTiet.NoiDungTKSua;
                        int thuong1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row, column + 20);
                            mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKSua;
                            worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                }
                //}
            }
        }

        private void AddChungTuAnh(string isKhaiBoSung)
        {
            ListChungTuKemForm ctForm = new ListChungTuKemForm();
            ctForm.TKMD = this.TKMD;
            if (isKhaiBoSung != "")
                ctForm.isKhaiBoSung = true;
            ctForm.ShowDialog(this);

        }
        private void AddChungTuNo()
        {
            ListChungTuNoForm ctForm = new ListChungTuNoForm();
            ctForm.TKMD = this.TKMD;
            ctForm.ShowDialog(this);

        }
        private void ShowGiayNopTien()
        {
            ListGiayNopTienForm giayNopTienList = new ListGiayNopTienForm();
            giayNopTienList.TKMD = this.TKMD;
            giayNopTienList.ShowDialog(this);

        }
        private void HuyTOKhaiMD()
        {

            HuyToKhaiForm f = new HuyToKhaiForm();
            f.TKMD = this.TKMD;
            f.ShowDialog(this);

        }
        private void SuaToKhaiDaDuyet()
        {
            //SuaToKhaiForm f = new SuaToKhaiForm();
            //f.TKMD = this.TKMD;
            //f.ShowDialog(this);

            string msg = "Bạn có muốn chuyển trạng thái của tờ khai đã duyệt sang tờ khai sửa không?";
            msg += "\n\nSố tờ khai: " + this.TKMD.SoToKhai.ToString();
            msg += "\n----------------------";
            msg += "\nCó " + this.TKMD.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
            {
                long id = this.TKMD.ID;
                this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    Company.KDT.SHARE.Components.Globals.SaveMessage("", TKMD.ID, TKMD.GUIDSTR, MessageTypes.ToKhaiNhap, MessageFunctions.SuaToKhai, "Thay đổi trạng thái", "Thay đổi sang trạng thái sửa tờ khai");
                else
                    Company.KDT.SHARE.Components.Globals.SaveMessage("", TKMD.ID, TKMD.GUIDSTR, MessageTypes.ToKhaiXuat, MessageFunctions.SuaToKhai, "Thay đổi trạng thái", "Thay đổi sang trạng thái sửa tờ khai");
                this.TKMD.Update();
                DeleteMsgSend();
                lblTrangThai.Text = "Chưa gởi đến Hải quan";
                txtLyDoSua.Enabled = true;
                SetCommandStatus();
            }


        }

        private void ExportExcel()
        {
            if (DialogResult.OK != saveFileDialog1.ShowDialog(this))
                return;

            FileStream stream = new FileStream(saveFileDialog1.FileName, FileMode.Create);
            gridEXExporter1.Export(stream);
            stream.Flush();


        }
        private void InToKhaiA4()
        {
            if (TKMD.ID == 0)
                return;
            switch (TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNA4Form f = new ReportViewTKNA4Form();
                    f.TKMD = TKMD;
                    f.ShowDialog(this);
                    break;

                case "X":
                    ReportViewTKXA4Form f1 = new ReportViewTKXA4Form();
                    f1.TKMD = TKMD;
                    f1.ShowDialog(this);
                    break;
            }

        }
        private void InToKhaiThongQuanA4()
        {
            if (TKMD.ID == 0)
                return;
            switch (TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNTQDTForm f = new ReportViewTKNTQDTForm();
                    f.TKMD = TKMD;
                    f.ShowDialog(this);
                    break;

                case "X":
                    ReportViewTKXTQDTForm f1 = new ReportViewTKXTQDTForm();
                    f1.TKMD = TKMD;
                    f1.ShowDialog(this);
                    break;
            }

        }
        private void InContainer()
        {
            if (TKMD.ID == 0)
                return;
            Company.Interface.Report.SXXK.BangkeSoContainer f = new Company.Interface.Report.SXXK.BangkeSoContainer();
            f.tkmd = TKMD;
            f.BindReport();
            f.ShowPreview();



        }
        private void QuanLyCO(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListCOTKMDForm f = new ListCOTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog(this);
            ReViewData();

        }

        private void QuanLyChuyenCuaKhau(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListDeNghiChuyenCuaKhauTKMDForm f = new ListDeNghiChuyenCuaKhauTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }

        //private void HuyKhaiBao()
        //{
        //    MsgSend sendXML = new MsgSend();
        //    sendXML.LoaiHS = "TK";
        //    sendXML.master_id = TKMD.ID;
        //    string password = "";
        //    //if (sendXML.Load())
        //    //{
        //    //    MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.","MSG_SEN01","", false);
        //    //    return;
        //    //}
        //    try
        //    {
        //        int ttxl = TKMD.TrangThaiXuLy;
        //        string stPhanLuong = TKMD.PhanLuong;
        //        WSForm wsForm = new WSForm();
        //        if (GlobalSettings.PassWordDT == "")
        //        {
        //            wsForm.ShowDialog(this);
        //            if (!wsForm.IsReady) return;
        //        }
        //        if (GlobalSettings.PassWordDT != "")
        //            password = GlobalSettings.PassWordDT;
        //        else
        //            password = wsForm.txtMatKhau.Text.Trim();

        //        this.Cursor = Cursors.WaitCursor;
        //        xmlCurrent = TKMD.HuyKhaiBao(password);
        //        sendXML = new MsgSend();
        //        sendXML.LoaiHS = "TK"; ;
        //        sendXML.master_id = TKMD.ID;
        //        sendXML.msg = xmlCurrent;
        //        sendXML.func = 3;
        //        xmlCurrent = "";
        //        sendXML.InsertUpdate();
        //        LayPhanHoi(password);
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Cursor = Cursors.Default;

        //        {
        //            #region FPTService
        //            string[] msg = ex.Message.Split('|');
        //            if (msg.Length == 2)
        //            {
        //                if (msg[1] == "DOTNET_LEVEL")
        //                {
        //                    //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
        //                    //{
        //                    //    HangDoi hd = new HangDoi();
        //                    //    hd.ID = TKMD.ID;
        //                    //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
        //                    //    hd.TrangThai = TKMD.TrangThaiXuLy;
        //                    //    hd.ChucNang = ChucNang.KHAI_BAO;
        //                    //    hd.PassWord = password;
        //                    //    MainForm.AddToQueueForm(hd);
        //                    //    MainForm.ShowQueueForm();
        //                    //}
        //                    MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
        //                    return;
        //                }
        //                else
        //                {
        //                    MLMessages("Có lỗi trong khai báo : " + msg[0],"MSG_SEN09",msg[0], false);
        //                }
        //            }
        //            else
        //            {
        //                if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
        //                    ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
        //                else
        //                {
        //                    GlobalSettings.PassWordDT = "";
        //                    ShowMessage(ex.Message, false);
        //                }
        //                SetCommandStatus();
        //            }
        //            #endregion FPTService
        //        }

        //        StreamWriter write = File.AppendText("Error.txt");
        //        write.WriteLine("--------------------------------");
        //        write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
        //        write.WriteLine(ex.StackTrace);
        //        write.WriteLine("Lỗi là : ");
        //        write.WriteLine(ex.Message);
        //        write.WriteLine("--------------------------------");
        //        write.Flush();
        //        write.Close();
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        private void HuyKhaiBaoCDTQDT()
        {
            try
            {
                string password = string.Empty;
                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                bool thanhcong = TKMD.WSHuyKhaiBao(password);

                this.LayPhanHoiHuyKhaiBao(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            MLMessages("Thông báo: " + msg[0], "MSG_SEN09", msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage(ex.Message, false);
                        }
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void AddToKhaiTriGia()
        {
            if (this.TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin tờ khai trước khi nhập tờ khai trị giá.", "MSG_SAV09", "", false);
                return;
            }
            ToKhaiTriGiaForm f = new ToKhaiTriGiaForm();
            f.TKMD = this.TKMD;

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET || 
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog(this);
            try
            {
                dgToKhaiTriGia.Refetch();
            }
            catch
            {
                dgToKhaiTriGia.Refresh();
            }
        }

        private void AddToKhaiTriGia2()
        {
            if (this.TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin tờ khai trước khi nhập tờ khai trị giá.", "MSG_SAV09", "", false);
                return;
            }
            HangTKTGPP23Form f = new HangTKTGPP23Form();
            f.TKTG23 = new ToKhaiTriGiaPP23();
            f.TKTG23.MaToKhaiTriGia = 2;
            f.TKMD = this.TKMD;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                 TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ||
                 TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET ||
                 TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog(this);
            if (f.TKTG23.ID > 0)
            {
                TKMD.TKTGPP23Collection.Add(f.TKTG23);
                try
                {
                    dglistTKTK23.DataSource = TKMD.TKTGPP23Collection;
                    dglistTKTK23.Refetch();
                }
                catch
                {
                    dglistTKTK23.Refresh();
                }
            }

        }
        private void AddToKhaiTriGia3()
        {
            if (this.TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin tờ khai trước khi nhập tờ khai trị giá.", "MSG_SAV09", "", false);
                return;
            }
            HangTKTGPP23Form f = new HangTKTGPP23Form();
            f.TKTG23 = new ToKhaiTriGiaPP23();
            f.TKTG23.MaToKhaiTriGia = 3;
            f.TKMD = this.TKMD;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                 TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ||
                 TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET ||
                 TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog(this);
            if (f.TKTG23.ID > 0)
            {
                TKMD.TKTGPP23Collection.Add(f.TKTG23);
                try
                {
                    dglistTKTK23.DataSource = TKMD.TKTGPP23Collection;
                    dglistTKTK23.Refetch();
                }
                catch
                {
                    dglistTKTK23.Refresh();
                }
            }

        }
        private void GetFileDinhKem()
        {
            SendMailChungTuForm f = new SendMailChungTuForm();
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
        }

        private void NhanDuLieuToKhai()
        {
            try
            {
                string password = "";
                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                LayPhanHoiDuyetHoacPhanLuong(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            MLMessages("Thông báo : " + msg[0], "MSG_SEN09", msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage(ex.Message, false);
                        }
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void AddChungTu()
        {
            ChungTuForm f = new ChungTuForm();
            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            f.collection = this.TKMD.ChungTuTKCollection;
            f.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;
            f.ShowDialog(this);
            this.TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            SetCommandStatus();
        }

        /// <summary>
        /// Khai báo tờ khai
        /// </summary>
        private void KhaiBaoToKhai()
        {


            if (TKMD.ID == 0)
            {
                this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                return;
            }

            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            if (sendXML.Load())
            {
                //MLMessages("Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                //Hien thi nut Nhan du lieu
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }

            string password = string.Empty;
            this.Cursor = Cursors.Default;
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;
                //KHAI BAO SỬA TỜ KHAI
                bool thanhcong = false;
                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                {
                    ////Nếu HQ đã duyệt phân luồng thì phải lấy thông tin phân luồng về trước khi khai báo sửa
                    //this.ShowMessage("Kiểm tra nhận dữ liệu phân luồng về trước khi khai báo sửa.", false);
                    //this.LayPhanHoiDuyetHoacPhanLuong(password);
                    //thanhcong = TKMD.WSKhaiBaoToKhaiSua(password);
                }
                else
                {
                    if (TKMD.SoToKhai != 0)
                    {
                        string msg = "Tờ khai này đã có số tờ khai, không thể khai báo mới tờ khai.\r\nBạn hãy chuyển tờ khai sang trạng thái khác.";
                        ShowMessage(msg, false);
                        return;
                    }

                    //thanhcong = TKMD.WSKhaiBaoToKhai(password);
                }
                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    LayPhanHoiEnable();
                }

                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.func = 1;
                sendXML.InsertUpdate();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessageTQDT("Lỗi kết nối", "Không kết nối được với hệ thống hải quan.", false);
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                            return;
                        }
                        else
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessageTQDT("Thông báo : " + msg[0], false);
                            ShowMessage("Thông báo: " + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //DATLMQ comment ngày 29/03/2011
                            ShowMessageTQDT("Thông báo từ hệ thống ECS ", "Thông tin lỗi: " + ex.Message, false);
                        //ShowMessage("Thông báo từ hệ thống ECS\r\n", false, true, "Thông tin lỗi: " + ex.ToString() + " " + ex.StackTrace);
                        //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //DATLMQ comment ngày 29/03/2011
                            ShowMessageTQDT("Thông báo từ hệ thống ECS ", "Thông tin lỗi: " + ex.Message, false);
                            //ShowMessage("Thông báo từ hệ thống ECS\r\n", false, true, "Thông tin lỗi: " + ex.ToString() + " " + ex.StackTrace);
                            //MLMessages(ex.Message, "MSG_WRN35", "", false);
                        }
                        sendXML.Delete();
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoiEnable()
        {
            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            dgList.AllowDelete = InheritableBoolean.False;
            Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdToKhaiTriGia.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        }

        //private void LaySoTiepNhan()
        //{
        //    WSForm wsForm = new WSForm();
        //    MsgSend sendXML = new MsgSend();
        //    XmlDocument doc = new XmlDocument();
        //    XmlNode node = null;
        //    string password = "";
        //    try
        //    {                
        //        sendXML.LoaiHS = "TK";
        //        sendXML.master_id = TKMD.ID;
        //        if (!sendXML.Load())
        //        {
        //            MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN01","", false);
        //            return;
        //        }

        //        if (GlobalSettings.PassWordDT == "")
        //        {
        //            wsForm.ShowDialog(this);
        //            if (!wsForm.IsReady) return;
        //        }

        //        if (GlobalSettings.PassWordDT != "")
        //            password = GlobalSettings.PassWordDT;
        //        else
        //            password = wsForm.txtMatKhau.Text.Trim();
        //        this.Cursor = Cursors.WaitCursor;
        //        {
        //            xmlCurrent = TKMD.LayPhanHoiGC(password, sendXML.msg);                  
        //        }
        //        this.Cursor = Cursors.Default;
        //        // Thực hiện kiểm tra.  
        //        if (xmlCurrent != "")
        //        {
        //            doc.LoadXml(xmlCurrent);
        //            node = doc.SelectSingleNode("Megs");
        //            if (node == null)
        //            {
        //                string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
        //                if (kq == "Yes")
        //                {
        //                    this.Refresh();
        //                    LayPhanHoi(password);
        //                }
        //                return;
        //            }
        //        }
        //        if (sendXML.func == 1)
        //        {                   
        //           // MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKMD.SoTiepNhan,"MSG_SEN02",TKMD.SoTiepNhan.ToString(), false);e
        //            ShowMessageTQDT("Khai báo thành công !\r\n Số tiếp nhận :" + TKMD.SoTiepNhan + "Năm tiếp nhận :" + TKMD.NamDK.ToString() , false);
        //            txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString(); 
        //            lblTrangThai.Text = setText("Chờ duyệt chính thức","Wait to approve");

        //        }
        //        else if (sendXML.func == 3)
        //        {
        //           // MLMessages("Hủy khai báo thành công","MSG_CAN01","", false);tờ khai
        //            ShowMessageTQDT("Hủy tờ khai thành công !", false);
        //            txtSoTiepNhan.Text = "";
        //            lblTrangThai.Text = setText("Chưa khai báo","Not declared yet");

        //        }
        //        else if (sendXML.func == 2)
        //        {
        //            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
        //            {                                                                       
        //                string mess = "Tờ khai đã được cấp số.\nSố tờ khai : " + TKMD.SoToKhai.ToString() + ".\r\n";
        //                if (node != null)
        //                {
        //                    if (node.HasChildNodes)
        //                        mess += "Có phản hồi từ hải quan : \r\n";
        //                    foreach (XmlNode nodeCon in node.ChildNodes)
        //                    {
        //                        mess += Company.KD.BLL.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText);
        //                    }
        //                }
        //               // MLMessages(mess, "MSG_SEN11", TKMD.SoToKhai.ToString(), false);
        //                ShowMessageTQDT(mess, false);
        //                lblTrangThai.Text = setText("Đã duyệt","Approved");
        //                txtSoToKhai.Text = TKMD.SoToKhai.ToString();

        //            }
        //            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
        //            {
        //                string mess = "Tờ khai chưa được cấp số\r\n";
        //                if (node != null)
        //                {
        //                    if (node.HasChildNodes)
        //                        mess += "Có phản hồi từ hải quan : \r\n";
        //                    foreach (XmlNode nodeCon in node.ChildNodes)
        //                    {
        //                        mess += Company.KD.BLL.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText);
        //                    }
        //                }
        //              //  MLMessages(mess, "MSG_SEN04", "", false);
        //                ShowMessageTQDT(mess, false);

        //            }                    
        //        }
        //        //xoa thông tin msg nay trong database
        //        sendXML.Delete();
        //        setCommandStatus();
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Cursor = Cursors.Default;
        //        //if (loaiWS == "1")
        //        {
        //            #region FPTService
        //            string[] msg = ex.Message.Split('|');
        //            if (msg.Length == 2)
        //            {
        //                if (msg[1] == "DOTNET_LEVEL")
        //                {
        //                    //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
        //                    //{
        //                    //    HangDoi hd = new HangDoi();
        //                    //    hd.ID = TKMD.ID;
        //                    //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
        //                    //    hd.TrangThai = TKMD.TrangThaiXuLy;
        //                    //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
        //                    //    hd.PassWord = pass;
        //                    //    MainForm.AddToQueueForm(hd);
        //                    //    MainForm.ShowQueueForm();
        //                    //}
        //                    MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
        //                    return;
        //                }
        //                else
        //                {
        //                    if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
        //                    {
        //                        ShowMessage(ex.Message, false);
        //                        sendXML.Delete();
        //                        setCommandStatus();
        //                    }
        //                    else
        //                    {
        //                        GlobalSettings.PassWordDT = "";
        //                        ShowMessage(ex.Message, false);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
        //                {
        //                    ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
        //                    sendXML.Delete();
        //                    setCommandStatus();
        //                }
        //                else
        //                {
        //                    GlobalSettings.PassWordDT = "";
        //                    MLMessages(ex.Message, "MSG_WRN35","", false);
        //                }
        //            }
        //            #endregion FPTService
        //        }

        //        StreamWriter write = File.AppendText("Error.txt");
        //        write.WriteLine("--------------------------------");
        //        write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
        //        write.WriteLine(ex.StackTrace);
        //        write.WriteLine("Lỗi là : ");
        //        write.WriteLine(ex.Message);
        //        write.WriteLine("--------------------------------");
        //        write.Flush();
        //        write.Close();
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}
        private void GetRequestionALL()
        {
            //WSForm wsForm = new WSForm();
            //XmlDocument doc = new XmlDocument();
            //XmlNode node = null;
            //string password = "";
            //try
            //{
            //    if (GlobalSettings.PassWordDT == "")
            //    {
            //        wsForm.ShowDialog(this);
            //        if (!wsForm.IsReady) return;
            //    }

            //    if (GlobalSettings.PassWordDT != "")
            //        password = GlobalSettings.PassWordDT;
            //    else
            //        password = wsForm.txtMatKhau.Text.Trim();
            //    this.Cursor = Cursors.WaitCursor;
            //    {
            //        bool thanhcong = TKMD.WSLaySoTiepNhan(password);
            //    }
            //    this.Cursor = Cursors.Default;
            //    // Thực hiện kiểm tra.  
            //    if (xmlCurrent != "")
            //    {
            //        doc.LoadXml(xmlCurrent);
            //        node = doc.SelectSingleNode("Megs");
            //        if (node == null)
            //        {
            //            string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
            //            if (kq == "Yes")
            //            {
            //                this.Refresh();
            //              LayPhanHoiKhaiBao(password);
            //            }
            //            return;
            //        }
            //    }
            //    if (sendXML.func == 1)
            //    {
            //        // MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKMD.SoTiepNhan,"MSG_SEN02",TKMD.SoTiepNhan.ToString(), false);e
            //        ShowMessageTQDT("Khai báo thành công !\r\n Số tiếp nhận :" + TKMD.SoTiepNhan + "Năm tiếp nhận :" + TKMD.NamDK.ToString(), false);
            //        txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
            //        lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");

            //    }
            //    else if (sendXML.func == 3)
            //    {
            //        // MLMessages("Hủy khai báo thành công","MSG_CAN01","", false);tờ khai
            //        ShowMessageTQDT("Hủy tờ khai thành công !", false);
            //        txtSoTiepNhan.Text = "";
            //        lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");

            //    }
            //    else if (sendXML.func == 2)
            //    {
            //        if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            //        {
            //            string mess ="";
            //            if(TKMD.PhanLuong =="")
            //            {
            //                     mess = "Tờ khai đã được cấp số.\nSố tờ khai : " + TKMD.SoToKhai.ToString() + ".\r\n Tờ khai chưa được phân luồng";
            //                    if (node != null)
            //                    {
            //                        if (node.HasChildNodes)
            //                            mess += "Có phản hồi từ hải quan : \r\n";
            //                        foreach (XmlNode nodeCon in node.ChildNodes)
            //                        {
            //                            mess += Company.KD.BLL.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText);
            //                        }
            //                    }                             
            //                    ShowMessageTQDT(mess, false);
            //                    lblTrangThai.Text = setText("Đã duyệt, chưa phân luồng", "Approved");
            //                    txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            //            }
            //            else
            //            {
            //                if (TKMD.PhanLuong=="1")
            //                {
            //                    ShowMessageTQDT("Tờ khai đã được cấp số. \r\n Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n Mã loại hình :" + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n Phân luồng tờ khai : LUỒNG XANH " + "\r\n Hướng dẫn của cơ quan Hải quan :" + TKMD.HUONGDAN.ToString() , false);
            //                    lblTrangThai.Text = setText("Đã duyệt, Luồng Xanh", "Approved");
            //                    txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            //                }
            //                else if (TKMD.PhanLuong == "2")
            //                {
            //                    ShowMessageTQDT("Tờ khai đã được cấp số. \r\n Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n Mã loại hình :" + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n Phân luồng tờ khai : LUỒNG VÀNG " + "\r\n Hướng dẫn của cơ quan Hải quan :" + TKMD.HUONGDAN.ToString(), false);
            //                    lblTrangThai.Text = setText("Đã duyệt, Luồng Vàng", "Approved");
            //                    txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            //                }
            //               else  if (TKMD.PhanLuong == "3")
            //                {
            //                    ShowMessageTQDT("Tờ khai đã được cấp số. \r\n Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n Mã loại hình :" + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n Phân luồng tờ khai : LUỒNG ĐỎ " + "\r\n Hướng dẫn của cơ quan Hải quan :" + TKMD.HUONGDAN.ToString(), false);
            //                    lblTrangThai.Text = setText("Đã duyệt, Luồng đỏ", "Approved");
            //                    txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            //                }

            //            }
            //        }
            //        else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            //        {
            //            string mess = "Tờ khai chưa được cấp số\r\n";
            //            if (node != null)
            //            {
            //                if (node.HasChildNodes)
            //                    mess += "Có phản hồi từ hải quan : \r\n";
            //                foreach (XmlNode nodeCon in node.ChildNodes)
            //                {
            //                    mess += Company.KD.BLL.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText);
            //                }
            //            }
            //            //  MLMessages(mess, "MSG_SEN04", "", false);
            //            ShowMessageTQDT(mess, false);

            //        }
            //    }
            //    //xoa thông tin msg nay trong database
            //    sendXML.Delete();
            //    SetCommandStatus();
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    //if (loaiWS == "1")
            //    {
            //        #region FPTService
            //        string[] msg = ex.Message.Split('|');
            //        if (msg.Length == 2)
            //        {
            //            if (msg[1] == "DOTNET_LEVEL")
            //            {

            //                MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
            //                return;
            //            }
            //            else
            //            {
            //                if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
            //                {
            //                    ShowMessage(ex.Message, false);
            //                    sendXML.Delete();
            //                    SetCommandStatus();
            //                }
            //                else
            //                {
            //                    GlobalSettings.PassWordDT = "";
            //                    ShowMessage(ex.Message, false);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
            //            {
            //                ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
            //                sendXML.Delete();
            //                SetCommandStatus();
            //            }
            //            else
            //            {
            //                GlobalSettings.PassWordDT = "";
            //                MLMessages(ex.Message, "MSG_WRN35", "", false);
            //            }
            //        }
            //        #endregion FPTService
            //    }

            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
        }
        private void sendMailChungTu()
        {
            this.Cursor = Cursors.WaitCursor;
            if (GlobalSettings.MailDoanhNghiep == "" || GlobalSettings.MailHaiQuan == "")
            {
                if (GlobalSettings.MailDoanhNghiep == "")
                    MLMessages("Chưa cấu hình mail của doanh nghiệp", "MSG_MAL01", "", false);
                if (GlobalSettings.MailHaiQuan == "")
                    MLMessages("Chưa cấu hình mail của hải quan tiếp nhận", "MSG_MAL02", "", false);
                return;
            }
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(GlobalSettings.MailDoanhNghiep, GlobalSettings.TEN_DON_VI, System.Text.Encoding.UTF8);
            string[] mailArray = GlobalSettings.MailHaiQuan.Split(';');
            for (int k = 0; k < mailArray.Length; ++k)
                if (mailArray[k] != "")
                    mail.To.Add(mailArray[k]);
            mail.Subject = "Khai báo điện tử - " + GlobalSettings.MA_DON_VI + "- Tờ khai có số tiếp nhận : " + this.TKMD.SoTiepNhan.ToString();
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "Hải quan điện tử - Duyệt tờ khai - Chứng từ ";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;
            mail.Priority = MailPriority.High;

            int i = 0;
            foreach (ChungTu ct in TKMD.ChungTuTKCollection)
            {
                try
                {
                    if (ct.FileUpLoad != "")
                    {
                        //FileStream file = File.Open(ct.FileUpLoad, FileMode.Open);
                        Attachment att = new Attachment(ct.FileUpLoad);
                        mail.Attachments.Add(att);
                        mail.Body += "\n" + ct.TenChungTu + " ";
                        if (ct.SoBanChinh > 0)
                            mail.Body += "Số bản chính : " + ct.SoBanChinh;
                        if (ct.SoBanSao > 0)
                            mail.Body += "Số bản sao : " + ct.SoBanSao;
                        ++i;
                    }
                }
                catch
                {
                }
            }
            try
            {
                if (i > 0)
                {
                    this.Cursor = Cursors.WaitCursor;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.Credentials = new System.Net.NetworkCredential("haiquandientu@gmail.com", "haiquandientudanang");
                    smtp.Send(mail);
                    //ShowMessage("Gửi mail thành công.", false);
                    this.Cursor = Cursors.Default;
                    this.Close();
                }
                this.Cursor = Cursors.WaitCursor;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
                MLMessages("Không thể gửi chứng từ tới hải quan.", "MSG_MAL06", "", false);
            }
        }

        //private void LayPhanHoi(string pass)
        //{
        //    MsgSend sendXML = new MsgSend();
        //    XmlDocument doc = new XmlDocument();
        //    XmlNode node = null;
        //    try
        //    {
        //        sendXML.LoaiHS = "TK";
        //        sendXML.master_id = TKMD.ID;
        //        sendXML.Load();
        //        this.Cursor = Cursors.WaitCursor;

        //        //if (TKMD.TrangThaiXuLy == 1)
        //        //    TKMD.WSRequestDaDuyet(pass);
        //        //else
        //        //    
        //        xmlCurrent = TKMD.LayPhanHoiGC(pass, sendXML.msg);

        //        this.Cursor = Cursors.Default;
        //        // Thực hiện kiểm tra.  
        //        if (xmlCurrent != "")
        //        {
        //            doc.LoadXml(xmlCurrent);
        //            node = doc.SelectSingleNode("Megs");
        //            if (node == null)
        //            {
        //                string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
        //                if (kq == "Yes")
        //                {
        //                    this.Refresh();
        //                    LayPhanHoi(pass);
        //                }
        //                return;
        //            }
        //        }
        //        if (sendXML.func == 1)
        //        {
        //            MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKMD.SoTiepNhan, "MSG_SEN02", TKMD.SoTiepNhan.ToString(), false);
        //            txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
        //            lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");

        //        }
        //        else if (sendXML.func == 3)
        //        {
        //            MLMessages("Hủy khai báo thành công", "MSG_CAN01", "", false);
        //            txtSoTiepNhan.Text = "";
        //            lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
        //        }
        //        else if (sendXML.func == 2)
        //        {
        //            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
        //            {
        //                string mess = "Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString();

        //                if (node != null)
        //                {
        //                    if (node.HasChildNodes)
        //                        mess += "\nCó phản hồi từ hải quan : \r\n";
        //                    foreach (XmlNode nodeCon in node.ChildNodes)
        //                    {
        //                        mess += Company.KD.BLL.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
        //                    }
        //                }
        //                MLMessages(mess, "MSG_SEN11", TKMD.SoToKhai.ToString(), false);

        //                lblTrangThai.Text = setText("Đã duyệt", "Approved");
        //                txtSoToKhai.Text = TKMD.SoToKhai.ToString();
        //            }
        //            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
        //            {

        //                string mess = "Hải quan chưa duyệt danh sách này.\r\n";
        //                if (node != null)
        //                {
        //                    if (node.HasChildNodes)
        //                        mess += "Có phản hồi từ hải quan : \r\n";
        //                    foreach (XmlNode nodeCon in node.ChildNodes)
        //                    {
        //                        mess += Company.KD.BLL.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText);
        //                    }
        //                }
        //                MLMessages(mess, "MSG_SEN04", "", false);
        //            }
        //        }
        //        //xoa thông tin msg nay trong database
        //        sendXML.Delete();
        //        setCommandStatus();
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Cursor = Cursors.Default;
        //        //if (loaiWS == "1")
        //        {
        //            #region FPTService
        //            string[] msg = ex.Message.Split('|');
        //            if (msg.Length == 2)
        //            {
        //                if (msg[1] == "DOTNET_LEVEL")
        //                {
        //                    //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
        //                    //{
        //                    //    HangDoi hd = new HangDoi();
        //                    //    hd.ID = TKMD.ID;
        //                    //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
        //                    //    hd.TrangThai = TKMD.TrangThaiXuLy;
        //                    //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
        //                    //    hd.PassWord = pass;
        //                    //    MainForm.AddToQueueForm(hd);
        //                    //    MainForm.ShowQueueForm();
        //                    //}
        //                    MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
        //                    return;
        //                }
        //                else
        //                {
        //                    MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
        //                    sendXML.Delete();
        //                    setCommandStatus();
        //                }
        //            }
        //            else
        //            {
        //                if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
        //                {
        //                    ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
        //                    sendXML.Delete();
        //                    setCommandStatus();
        //                }
        //                else
        //                {
        //                    GlobalSettings.PassWordDT = "";
        //                    MLMessages(ex.Message, "MSG_WRN35", "", false);
        //                }
        //            }
        //            #endregion FPTService
        //        }

        //        StreamWriter write = File.AppendText("Error.txt");
        //        write.WriteLine("--------------------------------");
        //        write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
        //        write.WriteLine(ex.StackTrace);
        //        write.WriteLine("Lỗi là : ");
        //        write.WriteLine(ex.Message);
        //        write.WriteLine("--------------------------------");
        //        write.Flush();
        //        write.Close();
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        /// <summary>
        /// Lấy phản hồi sau khi khai báo. Mục đích là lấy số tiếp nhận điện tử.
        /// </summary>
        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            MsgSend sendXML = new MsgSend();

            try
            {
                //Hungtq Comment 03122010. Vi khong load Phong bi tu Template
                //bool thanhcong = TKMD.WSLaySoTiepNhan(password);
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                bool thanhcong = TKMD.WSLayPhanHoi(password);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }
                }
                else
                {
                    string message = "Đăng ký thành công.\nSố tiếp nhận: " + TKMD.SoTiepNhan.ToString();
                    //string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoThanhCong);
                    this.ShowMessage(message, false);
                }
                sendXML.Delete();
                this.SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Không kết nối được với hệ thống Hải quan.", false);
                            ShowMessage("Không kết nối được với hệ thống Hải quan.", false, true, ex.ToString() + " " + ex.StackTrace);
                            return;
                        }
                        else
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Thông báo trả về từ hệ thống Hải quan:\r\n" + msg[0], false);
                            ShowMessage("Thông báo trả về từ hệ thống Hải quan:\r\n" + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);

                            // Thông tin không hợp lệ trả về.
                            try
                            {
                                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                                message.ItemID = this.TKMD.ID;
                                message.ReferenceID = new Guid(this.TKMD.GUIDSTR);
                                message.MessageFrom = this.MaHaiQuan.Trim();
                                message.MessageTo = this.MaDoanhNghiep;
                                message.MessageType = Company.KDT.SHARE.Components.MessageTypes.ThongTin;
                                message.MessageFunction = Company.KDT.SHARE.Components.MessageFunctions.HuyKhaiBao;
                                message.TieuDeThongBao = Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan;
                                message.NoiDungThongBao = msg[0];
                                message.CreatedTime = DateTime.Now;
                                message.Insert();
                            }
                            catch { }
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                            ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n", false, true, "Thông tin lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                            ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n", false, true, "Thông tin lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                            sendXML.Delete();
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// linhhtn
        /// Lấy phản hồi sau khi hủy khai báo. Mục đích là lấy thông báo hủy từ HQ.
        /// </summary>
        private void LayPhanHoiHuyKhaiBao(string password)
        {
        StartInvoke:
            MsgSend sendXML = new MsgSend();
            try
            {
                //bool thanhcong = TKMD.WSLaySoTiepNhan(password);//
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();

                bool thanhcong = TKMD.WSLayThongBaoHuy(password);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }
                    else
                    {
                        TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                        this.Update();
                    }
                }
                else
                {
                    //string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoThanhCong);
                    string message = "Hủy khai báo tờ khai thành công!";
                    this.ShowMessage(message, false);
                }
                sendXML.Delete();
                this.SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Không kết nối được với hệ thống Hải quan.", false);
                            ShowMessage("Lỗi kết nối: Không kết nối được với Hệ thống Hải quan\r\n", false, true, "Chi tiết lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                            return;
                        }
                        else
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Thông báo trả về từ hệ thống Hải quan:\r\n" + msg[0], false);
                            ShowMessage("Thông báo trả về từ Hệ thống Hải quan:\r\n", false, true, "Chi tiết lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                            // Thông tin không hợp lệ trả về.
                            try
                            {
                                Company.KDT.SHARE.Components.Message message = new Company.KDT.SHARE.Components.Message();
                                message.ItemID = this.TKMD.ID;
                                message.ReferenceID = new Guid(this.TKMD.GUIDSTR);
                                message.MessageFrom = this.MaHaiQuan.Trim();
                                message.MessageTo = this.MaDoanhNghiep;
                                message.MessageType = Company.KDT.SHARE.Components.MessageTypes.ThongTin;
                                message.MessageFunction = Company.KDT.SHARE.Components.MessageFunctions.HuyKhaiBao;
                                message.TieuDeThongBao = Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan;
                                message.NoiDungThongBao = msg[0];
                                message.CreatedTime = DateTime.Now;
                                message.Insert();
                            }
                            catch { }
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                            ShowMessage("Thông báo trả về từ Hệ thống Hải quan:\r\n", false, true, "Chi tiết lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                            ShowMessage("Thông báo trả về từ Hệ thống Hải quan:\r\n", false, true, "Chi tiết lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Lấy các thông tin phản hồi.
        /// </summary>
        private void LayThongTinPhanHoi()
        {
            string password = string.Empty;
            WSForm form = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                form.ShowDialog(this);
                if (!form.IsReady) return;
            }
            password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : form.txtMatKhau.Text.Trim();

            if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || this.TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                this.LayPhanHoiKhaiBao(password);
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                this.LayPhanHoiDuyetHoacPhanLuong(password);
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                //this.LayPhanHoiKhaiBao(password);
                this.LayPhanHoiHuyKhaiBao(password);//LayPhanHoiHuyKhaiBao
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                this.LayPhanHoiDuyetHoacPhanLuong(password);
            }
        }

        /// <summary>
        /// Lấy thông tin phản hồi sau khi đã khai báo có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        private void LayPhanHoiDuyetHoacPhanLuong(string password)
        {
        StartInvoke:
            MsgSend sendXML = new MsgSend();
            try
            {
                Company.KDT.SHARE.Components.Message message = new KDT.SHARE.Components.Message();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();

                bool thanhcong = TKMD.WSLayPhanHoi(password);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }
                }
                else
                {
                    string msg;
                    if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        msg = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiSuaDuocDuyet);
                        if (msg == "")
                            msg = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo);
                        if (TKMD.PhanLuong != "")
                        {
                            msg = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocPhanLuong);
                        }
                    }
                    else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        msg = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        if (this.TKMD.PhanLuong == "")
                        {
                            this.TKMD.SoTiepNhan = 0;
                            this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.TKMD.PhanLuong = "";
                            this.Update();
                        }
                        else
                        {
                            this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                            this.Update();
                        }
                    }
                    //DATLMQ bổ sung nhận Số TN từ hệ thống HQ 16/12/2010
                    else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                    {
                        //msg = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoThanhCong);
                        msg = "Đăng ký thành công!\nSố tiếp nhận: " + TKMD.SoTiepNhan.ToString();
                    }
                    else
                    {
                        msg = "Chưa có phản hồi từ hệ thống Hải quan";
                    }
                    this.ShowMessage(msg, false);

                    if (TKMD.IsThongBaoThue)
                    {
                        if (ShowMessage("Hải quan có trả về Thông báo Thuế.\nBạn có muốn in không?", true).ToUpper().Equals("YES"))
                        {
                            InAnDinhThue();
                        }
                    }
                }
                sendXML.Delete();
                this.SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                #region FPTService
                string[] msg = ex.Message.Split('|');
                if (msg.Length == 2)
                {
                    if (msg[1] == "DOTNET_LEVEL")
                    {
                        //DATLMQ comment ngày 29/03/2011
                        //ShowMessageTQDT("Lỗi kết nối","Không kết nối được với hệ thống hải quan.",false );
                        ShowMessage("Lỗi kết nối: Không kết nối được với hệ thống Hải quan\r\n", false, true, "Chi tiết lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                        return;
                    }
                    else
                    {
                        //DATLMQ comment ngày 29/03/2011
                        //ShowMessage("Thông báo trả về từ hệ thống Hải quan:\r\n" + msg[0], false);
                        ShowMessage("Thông báo trả về từ hệ thống Hải quan:\r\n" + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                        sendXML.Delete();
                        SetCommandStatus();
                    }
                }
                else
                {
                    if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                    {
                        //DATLMQ comment ngày 29/03/2011
                        //ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                        ShowMessage("Thông báo trả về từ Hệ thống Hải quan:\r\n", false, true, ex.ToString() + " " + ex.StackTrace);
                        sendXML.Delete();
                        SetCommandStatus();
                    }
                    else
                    {
                        GlobalSettings.PassWordDT = "";
                        //DATLMQ comment ngày 29/03/2011
                        //ShowMessage("Khai báo không thành công!\r\nLỗi liên quan đến hệ thống Hải quan:\r\n" + ex.Message, false);
                        ShowMessage("Thông báo trả về từ Hệ thống Hải quan:\r\n", false, true, ex.ToString() + " " + ex.StackTrace);
                    }
                }
                #endregion FPTService

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void InAnDinhThue()
        {
            Globals.ExportExcelThongBaoThue(TKMD, TKMD.SoQD, TKMD.NgayQD, TKMD.NgayHetHan, TKMD.TaiKhoanKhoBac, TKMD.TenKhoBac,
                TKMD.ChuongThueXNK, TKMD.LoaiThueXNK, TKMD.KhoanThueXNK, TKMD.MucThueXNK, TKMD.TieuMucThueXNK, TKMD.SoTienThueXNK,
                TKMD.ChuongThueVAT, TKMD.LoaiThueVAT, TKMD.KhoanThueVAT, TKMD.MucThueVAT, TKMD.TieuMucThueVAT, TKMD.SoTienThueVAT,
                TKMD.ChuongThueTTDB, TKMD.LoaiThueTTDB, TKMD.KhoanThueTTDB, TKMD.MucThueTTDB, TKMD.TieuMucThueTTDB, TKMD.SoTienThueTTDB,
                TKMD.ChuongThueTVCBPG, TKMD.LoaiThueTVCBPG, TKMD.KhoanThueTVCBPG, TKMD.MucThueTVCBPG, TKMD.TieuMucThueTVCBPG, TKMD.SoTienThueTVCBPG, 0);
        }

        private void InAnDinhThue(long tkmdID, string guidstr)
        {
            try
            {
                AnDinhThue anDinhThue = new AnDinhThue();
                List<AnDinhThue> dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                if (dsADT.Count > 0)
                {
                    anDinhThue = dsADT[0];
                    anDinhThue.LoadChiTiet();

                    Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                    anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                    anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                    anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                    anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue, 0);
                }
                else
                {
                    //Hungtq updated 21/02/2012.
                    //Không có thông tin Ấn định thuế hoặc Có thông tin ấn định thuế từ kết quả Hải quan trả về nhưng chưa lưu được xuống database.
                    Company.Interface.Globals.AnDinhThue_InsertFromXML(this.TKMD);

                    //InAnDinhThue
                    dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                    if (dsADT.Count > 0)
                    {
                        anDinhThue = dsADT[0];
                        anDinhThue.LoadChiTiet();

                        Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                        anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                        anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                        anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                        anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue, 0);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //-----------------------------------------------------------------------------------------
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHopDongTKMDForm f = new ListHopDongTKMDForm();
            if (sender != null)
            {
                f.isKhaiBoSung = true;
            }
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }

        private void AutoInsertHMDToTKEdit(ToKhaiMauDich toKhaiMauDich, Company.KD.BLL.KDT.HangMauDich hangMauDichOld, Company.KD.BLL.KDT.HangMauDich HangMauDichNew)
        {
            try
            {

                //Company.KD.BLL.KDT.HangMauDich HMD_Edit = new Company.KD.BLL.KDT.HangMauDich();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
                List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail noiDungEditDetail = new Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail();
                List<Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail> nddctklist = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail>();
                if (toKhaiMauDich.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    #region 21. Mã HS
                    if (HangMauDichNew.MaHS != hangMauDichOld.MaHS)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "21. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.MaHS;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.MaHS;
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'  AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.MaHS;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.MaHS;
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 20. Tên hàng, Quy cách phẩm chất
                    if (HangMauDichNew.TenHang != hangMauDichOld.TenHang)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "20. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TenHang;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.TenHang;
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TenHang;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.TenHang;
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 20. Mã hàng
                    if (HangMauDichNew.MaPhu != hangMauDichOld.MaPhu)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "20. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.MaPhu;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.MaPhu;
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.MaPhu;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.MaPhu;
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 22. Xuất xứ
                    if (HangMauDichNew.NuocXX_ID.Trim() != hangMauDichOld.NuocXX_ID.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "22. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.NuocXX_ID;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.NuocXX_ID.Trim();
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.NuocXX_ID;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.NuocXX_ID.Trim();
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 23. Số lượng
                    if (HangMauDichNew.SoLuong != hangMauDichOld.SoLuong)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "23. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.SoLuong.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.SoLuong.ToString();
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.SoLuong.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.SoLuong.ToString();
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 24. Đơn vị tính
                    if (HangMauDichNew.DVT_ID.Trim() != hangMauDichOld.DVT_ID.Trim())
                    {
                        string DVT_old = DonViTinh.GetName(hangMauDichOld.DVT_ID.Trim());
                        string DVT_new = DonViTinh.GetName(HangMauDichNew.DVT_ID.Trim());
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "24. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + DVT_old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + DVT_new;
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + DVT_old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + DVT_new;
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 25. Đơn giá nguyên tệ
                    if (HangMauDichNew.DonGiaKB != hangMauDichOld.DonGiaKB)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "25. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TriGiaKB.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.DonGiaKB.ToString();
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TriGiaKB.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.DonGiaKB.ToString();
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 26. Trị giá nguyên tệ
                    if (HangMauDichNew.TriGiaKB != hangMauDichOld.TriGiaKB)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "26. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TriGiaKB.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.TriGiaKB.ToString();
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TriGiaKB.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.TriGiaKB.ToString();
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region 18. Mã HS
                    if (HangMauDichNew.MaHS != hangMauDichOld.MaHS)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "18. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.MaHS;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.MaHS;
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.MaHS;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.MaHS;
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Tên hàng, Quy cách phẩm chất
                    if (HangMauDichNew.TenHang != hangMauDichOld.TenHang)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "17. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TenHang;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.TenHang;
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TenHang;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.TenHang;
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Mã hàng
                    if (HangMauDichNew.MaPhu != hangMauDichOld.MaPhu)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "17. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.MaPhu;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.MaPhu;
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.MaPhu;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.MaPhu;
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 19. Xuất xứ
                    if (HangMauDichNew.NuocXX_ID.Trim() != hangMauDichOld.NuocXX_ID.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "19. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.NuocXX_ID;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.NuocXX_ID.Trim();
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.NuocXX_ID;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.NuocXX_ID.Trim();
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 20. Số lượng
                    if (HangMauDichNew.SoLuong != hangMauDichOld.SoLuong)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "20. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.SoLuong.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.SoLuong.ToString();
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.SoLuong.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.SoLuong.ToString();
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 21. Đơn vị tính
                    if (HangMauDichNew.DVT_ID.Trim() != hangMauDichOld.DVT_ID.Trim())
                    {
                        string DVT_old = DonViTinh.GetName(hangMauDichOld.DVT_ID.Trim());
                        string DVT_new = DonViTinh.GetName(HangMauDichNew.DVT_ID.Trim());
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "21. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + DVT_old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + DVT_new;
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + DVT_old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + DVT_new;
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 22. Đơn giá nguyên tệ
                    if (HangMauDichNew.DonGiaKB != hangMauDichOld.DonGiaKB)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "22. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TriGiaKB.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.DonGiaKB.ToString();
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TriGiaKB.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.DonGiaKB.ToString();
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 23. Trị giá nguyên tệ
                    if (HangMauDichNew.TriGiaKB != hangMauDichOld.TriGiaKB)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "23. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TriGiaKB.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.TriGiaKB.ToString();
                            nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                            if (nddctklist.Count > 0)
                                noiDungEditDetail = nddctklist[0];
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + hangMauDichOld.SoThuTuHang.ToString() + ": " + hangMauDichOld.TriGiaKB.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + HangMauDichNew.SoThuTuHang.ToString() + ": " + HangMauDichNew.TriGiaKB.ToString();
                                nddctklist = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.SelectCollectionDynamic("Id_DieuChinh = '" + noiDungEditDetail.Id_DieuChinh + "'   AND NoiDungTKChinh = N'" + noiDungEditDetail.NoiDungTKChinh + "'", null);
                                if (nddctklist.Count > 0)
                                    noiDungEditDetail = nddctklist[0];
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion
                }
            }
            catch
            {
                ShowMessage("Có lỗi trong quá trình ghi dữ liệu vào tờ khai sửa đổi, bổ sung", false);
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            int pos = items[0].Position;
            Company.KD.BLL.KDT.HangMauDich hangmaudich = this.TKMD.HMDCollection[pos];
            #region LanNT thay doan tren bang doan duoi
            //foreach (GridEXSelectedItem i in items)
            //{
            //    if (i.RowType == RowType.Record)
            //    {
            //        HangMauDichEditForm f = new HangMauDichEditForm();
            //        f.HMD = this.TKMD.HMDCollection[i.Position];
            //        soDongHang = i.Position + 1;
            //        f.collection = this.TKMD.HMDCollection;
            //        f.LoaiHangHoa = this.TKMD.LoaiHangHoa;
            //        f.NhomLoaiHinh = this.NhomLoaiHinh;
            //        f.MaHaiQuan = this.TKMD.MaHaiQuan;
            //        f.MaNguyenTe = ctrNguyenTe.Ma;
            //        f.TyGiaTT = Convert.ToDouble(txtTyGiaTinhThue.Value);
            //        this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            //        this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            //        this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            //        this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            //        this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();
            //        f.TKMD = this.TKMD;
            //        if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            //        else
            //            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            //        f.ShowDialog(this);
            //        if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //        {
            //            if (f.IsEdited)
            //            {
            //                if (f.HMD.TKMD_ID > 0)
            //                    f.HMD.Update();
            //                else
            //                    this.TKMD.HMDCollection[i.Position] = f.HMD;
            //            }
            //            else if (f.IsDeleted)
            //            {
            //                if (f.HMD.TKMD_ID > 0)
            //                    f.HMD.Delete();
            //                this.TKMD.HMDCollection.RemoveAt(i.Position);
            //            }
            //        }
            //        dgList.DataSource = this.TKMD.HMDCollection;
            //        try
            //        {
            //            dgList.Refetch();
            //        }
            //        catch { dgList.DataSource = this.TKMD.HMDCollection; }
            //        SetCommandStatus();
            //    }
            //    break;
            //}
            #endregion LanNT thay doan tren bang doan duoi
            HangMauDichForm frmHangMD = new HangMauDichForm(hangmaudich);
            frmHangMD.TKMD = this.TKMD;
            frmHangMD.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            frmHangMD.NhomLoaiHinh = this.NhomLoaiHinh;
            frmHangMD.MaHaiQuan = ctrDonViHaiQuan.Ma;
            frmHangMD.MaNguyenTe = ctrNguyenTe.Ma;
            frmHangMD.TyGiaTT = Convert.ToDouble(txtTyGiaTinhThue.Value);
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();
            frmHangMD.ShowDialog(this);
            txtSoLuongPLTK.Text = TKMD.SoLuongPLTK.ToString();
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (dgList.GetRows().Length < 1) return;
            if (e.Row.RowType == RowType.Record)
            {
                if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_STN10", "", false);
                    e.Cancel = true;
                    return;
                }
                if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
                {
                    Company.KD.BLL.KDT.HangMauDich hmd = (Company.KD.BLL.KDT.HangMauDich)e.Row.DataRow;
                    if (hmd.ID > 0)
                    {
                        hmd.Delete();
                    }
                    if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        this.TKMD.Load();
                        this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.TKMD.Update();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void gridEX1_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuForm f = new ChungTuForm();
            if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            ChungTu ctDetail = new ChungTu();
            ctDetail.ID = Convert.ToInt64(e.Row.Cells["ID"].Text);
            ctDetail.TenChungTu = e.Row.Cells["TenChungTu"].Text;
            ctDetail.SoBanChinh = Convert.ToInt16(e.Row.Cells["SoBanChinh"].Text);
            ctDetail.SoBanSao = Convert.ToInt16(e.Row.Cells["SoBanSao"].Text);
            ctDetail.STTHang = Convert.ToInt16(e.Row.RowIndex + 1);
            ctDetail.Master_ID = Convert.ToInt64(e.Row.Cells["Master_ID"].Text);
            ctDetail.FileUpLoad = e.Row.Cells["FileUpLoad"].Text;
            f.ctDetail = ctDetail;
            int i = 0;
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                if (ct.TenChungTu.ToUpper() == ctDetail.TenChungTu.ToUpper())
                {
                    this.TKMD.ChungTuTKCollection.RemoveAt(i);
                    break;
                }
                ++i;
            }
            f.MaLoaiHinh = this.TKMD.MaLoaiHinh;
            f.collection = this.TKMD.ChungTuTKCollection;
            f.ShowDialog(this);
            this.TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            SetCommandStatus();
        }

        private void gridEX1_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (gridEX1.GetRows().Length < 1) return;
            if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_STN10", "", false);
                e.Cancel = true;
                return;
            }
            ChungTu ctDetail = new ChungTu();
            ctDetail.ID = Convert.ToInt64(e.Row.Cells["ID"].Text);
            if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
            {
                if (ctDetail.ID > 0)
                    ctDetail.Delete();
                this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
            }
            else
                e.Cancel = true;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (dgList.GetRows().Length < 1) return;
                if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            Company.KD.BLL.KDT.HangMauDich hmd = (Company.KD.BLL.KDT.HangMauDich)i.GetRow().DataRow;
                            if (hmd.ID > 0)
                            {
                                if (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                                {
                                    new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hóa đơn thương mại, không thể xóa.", false);
                                    e.Cancel = true;
                                    return;
                                }
                                else if (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                                {
                                    new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hợp đồng thương mại, không thể xóa.", false);
                                    e.Cancel = true;
                                    return;
                                }

                                hmd.Delete();
                            }
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                new Company.Controls.KDTMessageBoxControl().ShowMessage("Xóa thông tin hàng không thành công.", false);
            }
        }

        private void radSP_CheckedChanged(object sender, EventArgs e)
        {
            this.TKMD.HMDCollection.Clear();
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void txtChungTu_ButtonClick(object sender, EventArgs e)
        {
            GiayToForm f = new GiayToForm();
            f.collection = this.TKMD.ChungTuTKCollection;
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            f.ShowDialog(this);
            this.TKMD.ChungTuTKCollection = f.collection;
            try
            {
                gridEX1.DataSource = TKMD.ChungTuTKCollection;
                gridEX1.Refetch();
            }
            catch
            {
                gridEX1.Refresh();
            }
            txtChungTu.Text = f.GiayTo;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dgToKhaiTriGia_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (dgToKhaiTriGia.GetRows().Length < 1) return;
            if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgToKhaiTriGia.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiTriGia tktg = (ToKhaiTriGia)i.GetRow().DataRow;
                        if (tktg.ID > 0)
                        {
                           // TKMD.TKTGCollection.Remove(tktg);
                            tktg.Delete();
                            
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void dgToKhaiTriGia_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ToKhaiTriGia TKTG = (ToKhaiTriGia)e.Row.DataRow;
            ToKhaiTriGiaForm f = new ToKhaiTriGiaForm();
            f.TKTG = TKTG;
            f.TKMD = this.TKMD;
            //update phiph
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                 TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ||
                 TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET ||
                 TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog(this);
            try
            {
                dgToKhaiTriGia.Refetch();
            }
            catch
            {
                dgToKhaiTriGia.Refresh();
            }
        }

        private void txtTenDonViDoiTac_ButtonClick(object sender, EventArgs e)
        {
            DonViDoiTacForm f = new DonViDoiTacForm();
            f.isBrower = true;
            f.ShowDialog(this);
            if (f.doiTac != null && f.doiTac.TenCongTy != "")
            {
                txtTenDonViDoiTac.Text = f.doiTac.TenCongTy + ". " + f.doiTac.DiaChi;
            }
        }

        private void uiPanel3_SelectedPanelChanged(object sender, Janus.Windows.UI.Dock.PanelActionEventArgs e)
        {

        }

        private void dglistTKTK23_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["MaToKhaiTriGia"].Value == "1")
                e.Row.Cells["MaToKhaiTriGia"].Text = setText("Tờ khai trị giá PP3", "Declaration under 3nd method");
            else
                e.Row.Cells["MaToKhaiTriGia"].Text = setText("Tờ khai trị giá PP2", "Declaration under 2nd method");
        }

        private void dglistTKTK23_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            HangTKTGPP23Form f = new HangTKTGPP23Form();
            f.TKTG23 = (ToKhaiTriGiaPP23)e.Row.DataRow;
            f.TKMD = this.TKMD;
            //update Phiph
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                  TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ||
                  TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET ||
                  TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog(this);
            try
            {
                dglistTKTK23.Refetch();
            }
            catch
            {
                dglistTKTK23.Refresh();
            }
        }

        private void dglistTKTK23_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (dglistTKTK23.GetRows().Length < 1) return;
            if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dglistTKTK23.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiTriGiaPP23 tktg = (ToKhaiTriGiaPP23)i.GetRow().DataRow;
                        if (tktg.ID > 0)
                        {
                            tktg.Delete();
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void txtSoVanTaiDon_ButtonClick(object sender, EventArgs e)
        {
            //if (TKMD.ID == 0)
            //{
            //    ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
            //    return;
            //}
            //DATLMQ bổ sung kiểm tra MaLoaiHinh ngày 25/03/2011
            //else if (this.TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
            //{
            VanTaiDonForm f = new VanTaiDonForm();
            f.TKMD = TKMD;
            f.ShowDialog(this);
            if (f.TKMD.VanTaiDon != null && f.TKMD.VanTaiDon.SoVanDon.Trim() != "")
            {
                //DATLMQ update SoHieuPTVT 17/01/2011
                txtSoHieuPTVT.Text = tenPTVT;
                if (TKMD.NgayDenPTVT != null || TKMD.NgayDenPTVT.Year > 1900)
                    ccNgayDen.Text = this.TKMD.NgayDenPTVT.ToShortDateString();
                else
                    ccNgayDen.Text = "";
                // Nước.
                ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;

                // ĐKGH.
                cbDKGH.SelectedValue = this.TKMD.DKGH_ID;

                // Địa điểm dỡ hàng.
                ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;

                // Vận tải đơn.
                txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
                if (this.TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToShortDateString();
                else ccNgayVanTaiDon.Text = "";
                // if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

                // Địa điểm xếp hàng.
                txtDiaDiemXepHang.Text = this.TKMD.VanTaiDon.TenCangXepHang;
                //if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

                // Container 20.
                txtSoContainer20.Value = this.TKMD.SoContainer20;

                // Container 40.
                txtSoContainer40.Value = this.TKMD.SoContainer40;
            }
        }

        private void txtSoGiayPhep_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListGiayPhepTKMDForm f = new ListGiayPhepTKMDForm();
            if (sender != null)
            {
                f.isKhaiBoSung = true;
            }
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }

        private void txtSoHoaDonThuongMai_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHoaDonThuongMaiTKMDForm f = new ListHoaDonThuongMaiTKMDForm();
            if (sender != null)
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }

        private void btnNoiDungDieuChinhTKForm_Click(object sender, EventArgs e)
        {
            NoiDungChinhSuaTKForm noidungchinhsuatk = new NoiDungChinhSuaTKForm();
            noidungchinhsuatk.TKMD = this.TKMD;
            noidungchinhsuatk.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
            noidungchinhsuatk.ShowDialog(this);
        }

        private void ctrLoaiHinhMauDich_Leave(object sender, EventArgs e)
        {
            //if (ctrLoaiHinhMauDich.Ma.Equals("NSX04"))
            //{
            //    grbVanTaiDon.Enabled = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Visible"));
            //    uiGroupBox11.Enabled = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Visible"));
            //    txtSoHieuPTVT.Visible = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Visible"));
            //    ccNgayDen.Visible = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Visible"));
            //    grbDiaDiemXepHang.Enabled = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Visible"));
            //}
        }

        private void btnNoiDungDieuChinhTKForm_Click_1(object sender, EventArgs e)
        {
            NoiDungChinhSuaTKForm noidungchinhsuatk = new NoiDungChinhSuaTKForm();
            noidungchinhsuatk.TKMD = this.TKMD;
            noidungchinhsuatk.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
            noidungchinhsuatk.ShowDialog(this);
        }

        #region Send V3 Create by LANNT
        private void FeedBackV3()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBackMessageV3(TKMD);

                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetCommandStatus();
                }
            }
            //bool isFeedBack = true;
            //int count = Company.KDT.SHARE.Components.Globals.CountSend;
            //while (isFeedBack)
            //{
            //    ObjectSend msgSend = SingleMessage.FeedBackMessageV3(TKMD);

            //    SendMessageForm dlgSendForm = new SendMessageForm();
            //    dlgSendForm.Send += SendMessage;
            //    isFeedBack = dlgSendForm.DoSend(msgSend);
            //    if (isFeedBack)
            //    {
            //        if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET && count > 0)
            //        {
            //            if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
            //            {
            //                if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
            //                    isFeedBack = false;
            //                else
            //                    isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
            //                ShowMessageTQDT(msgInfor, false);
            //            }
            //            count--;
            //        }
            //        else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            //        {
            //            ShowMessageTQDT(msgInfor, false);
            //            isFeedBack = false;
            //        }
            //        else if (!string.IsNullOrEmpty(msgInfor))
            //            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
            //        else isFeedBack = false;
            //    }
            //}


        }

        private void CancelV3()
        {

            ObjectSend msgSend = SingleMessage.CancelMessageV3(TKMD);


            SendMessageForm dlgSendForm = new SendMessageForm();

            dlgSendForm.Send += SendMessage;
            bool isSend = dlgSendForm.DoSend(msgSend, true);
            dlgSendForm.Message.XmlSaveMessage(TKMD.ID, MessageTitle.HuyKhaiBaoToKhai);
            if (isSend) FeedBackV3();

        }

        private void SendV3()
        {
            try
            {
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                //if (GlobalSettings.IsDaiLy && !GlobalSettings.ISKHAIBAO && TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                //    this.ShowMessage("Đại lý chưa đồng bộ tờ khai cho doanh nghiệp. Số tờ khai tối đa đại lý được khai là: " + GlobalSettings.SOTOKHAI_DONGBO.ToString(), false);
                //    return;
                //}

                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                sendXML.master_id = TKMD.ID;
                if (sendXML.Load())
                {
                    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }

                if (TKMD.SoToKhai != 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
                {
                    string msg = "Tờ khai này đã có số tờ khai, không thể khai báo mới tờ khai.\r\nBạn hãy chuyển tờ khai sang trạng thái khác.";
                    ShowMessage(msg, false);
                    return;
                }

                if (TKMD.SoToKhai != 0 && TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET && (string.IsNullOrEmpty(TKMD.LyDoSua)
                    || TKMD.NoiDungDieuChinhTKCollection.Count <= 0))
                {
                    ShowMessage("Chưa nhập lý do sửa hoặc chưa sửa thông tin của tờ khai.\r\nSau khi sửa hãy lưu lại thông tin tờ khai", false);
                    return;
                }


                ObjectSend msgSend = SingleMessage.SendMessageV3(TKMD);

                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;

                bool isSend = dlgSendForm.DoSend(msgSend);
                //Lưu ý phải để dòng code lưu message sau kết quả trả về.
                dlgSendForm.Message.XmlSaveMessage(TKMD.ID, TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET ? MessageTitle.KhaiBaoToKhai : MessageTitle.KhaiBaoSuaTK);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                    sendXML.master_id = TKMD.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    TKMD.Update();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);

            this.Invoke(new MethodInvoker(
                delegate
                {
                    if (this.TKMD.SoTiepNhan > 0)
                        txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    if (this.TKMD.SoToKhai > 0)
                        txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();
                    SetCommandStatus();
                }));
        }
        /// <summary>
        /// Xử lý message tờ khai trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ToKhaiSendHandler(TKMD, ref msgInfor, sender, e);
        }

        private void DeleteMsgSend()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
            sendXML.master_id = TKMD.ID;
            sendXML.Delete();
        }

        #endregion

        /// <summary>
        /// Hien thi thong tin so luong chung tu dinh kem tren menu 'Chung tu dinh kem'. 
        /// Updaetd by Hungtq, 24/09/2012.
        /// </summary>
        /// <param name="tkmd"></param>
        private void SetStatusChungTuDinhKem(Company.KD.BLL.KDT.ToKhaiMauDich tkmd)
        {
            try
            {
                this.VanDon2.ImageIndex = (tkmd.VanTaiDon != null ? 4 : -1);

                this.HopDong1.ImageIndex = (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0) == true ? 4 : -1;

                this.GiayPhep1.ImageIndex = (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0) == true ? 4 : -1;

                this.HoaDon1.ImageIndex = (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0) == true ? 4 : -1;

                this.CO1.ImageIndex = (tkmd.COCollection != null && tkmd.COCollection.Count > 0) == true ? 4 : -1;

                this.cmdChuyenCuaKhau1.ImageIndex = (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0) == true ? 4 : -1;

                this.cmdChungTuKem1.ImageIndex = (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0) == true ? 4 : -1;

                this.cmdChungTuNo1.ImageIndex = (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0) == true ? 4 : -1;

                //Chung tu dinh kem Bo sung

                this.cmdBoSungCO1.ImageIndex = (tkmd.COCollection != null && tkmd.COCollection.Count > 0) == true ? 4 : -1;

                this.cmdBoSungGiayPhep1.ImageIndex = (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0) == true ? 4 : -1;

                this.cmdBoSungHopDong1.ImageIndex = (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0) == true ? 4 : -1;

                this.cmdBoSungHoaDon1.ImageIndex = (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0) == true ? 4 : -1;

                this.cmdBoSungChuyenCuaKhau1.ImageIndex = (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0) == true ? 4 : -1;

                this.cmdBoSungChungTuDinhKemDangAnh1.ImageIndex = (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0) == true ? 4 : -1;

                this.cmdGiayNopTien1.ImageIndex = (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0) == true ? 4 : -1;

                //To khai tri gia PP1, PP2, PP3

                this.ToKhaiTriGia.ImageIndex = (tkmd.TKTGCollection != null && tkmd.TKTGCollection.Count > 0) == true ? 4 : -1;

                this.cmdToKhaiTGPP2.ImageIndex = (tkmd.TKTGPP23Collection != null && tkmd.TKTGPP23Collection.Count > 0) == true ? 4 : -1;

                this.cmdToKhaiTGPP3.ImageIndex = (tkmd.TKTGPP23Collection != null && tkmd.TKTGPP23Collection.Count > 0) == true ? 4 : -1;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void InContainerTT196()
        {
            if (TKMD.ID == 0)
                return;
            //In mau cu
            //Company.Interface.Report.SXXK.BangkeSoContainer f = new Company.Interface.Report.SXXK.BangkeSoContainer();
            Company.Interface.Report.SXXK.BangkeSoContainer_TT196 f = new Company.Interface.Report.SXXK.BangkeSoContainer_TT196();
            f.tkmd = TKMD;
            f.BindReport();
            f.ShowPreview();
        }
    }
}
