using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2 : DevExpress.XtraReports.UI.XtraReport
    {
        DataTable dt_VAL = new DataTable();
        public int spl;
        public QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2()
        {
            InitializeComponent();
        }
        public void BindReport(VAL8000 VAL,string maNV)
        {
            xrLabel1.Text = EnumThongBao.GetTenNV(maNV);

            lblSoPL.Text = VAL.A24.Value.ToString();
            lblSoThongBao.Text = VAL.ADD.Value.ToString();
            if (Convert.ToDateTime(VAL.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayHoanThanhKT.Text = Convert.ToDateTime(VAL.AD2.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayHoanThanhKT.Text = "";
            if (VAL.AD3.GetValue().ToString().ToUpper() != "" && VAL.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioHoanThanhKT.Text = VAL.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + VAL.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + VAL.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioHoanThanhKT.Text = "";
            }
            lblSoToKhaiBoSung.Text = VAL.A03.Value.ToString();
            if (Convert.ToDateTime(VAL.A02.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayDKKhaiBoSung.Text = Convert.ToDateTime(VAL.A02.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayDKKhaiBoSung.Text = "";

            string time = VAL.AD1.Value.ToString();
            lblGioDKKhaiBoSung.Text = time.Substring(0, 2) + ":" + time.Substring(2, 2) + ":" + time.Substring(4, 2);
            
            lblCoQuanNhan.Text = VAL.A00.Value.ToString();
            lblNhomXuLyHoSo.Text = VAL.A01.Value.ToString();
            lblPhanLoaiXNK.Text = VAL.ADG.Value.ToString();
            lblSoToKhaiXNK.Text = VAL.A27.Value.ToString();
            lblMaLoaiHinh.Text = VAL.ADQ.Value.ToString();
            if (Convert.ToDateTime(VAL.A29.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayToKhaiXNK.Text = Convert.ToDateTime(VAL.A29.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayToKhaiXNK.Text = "";

            lblDauHieuBaoQua.Text = VAL.ADE.Value.ToString();
            //  lblMaHetThoiHan.Text = VAL.CTO.Value.ToString();
            if (Convert.ToDateTime(VAL.A30.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayCapPhep.Text = Convert.ToDateTime(VAL.A30.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayCapPhep.Text = "";
            if (Convert.ToDateTime(VAL.ADR.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblThoiHanTaiNhapXuat.Text = Convert.ToDateTime(VAL.ADR.GetValue()).ToString("dd/MM/yyyy");
            else
                lblThoiHanTaiNhapXuat.Text = "";

            //
            //dt_VAL = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(VAL.HangMD);

            DataTable dt = new DataTable();
            if (VAL.HangMD != null && VAL.HangMD.Count > 0)
            {
                int STT = 0;

                dt.Columns.Add("SoDong", typeof(string));
                dt.Columns.Add("SoThuTuHangTK", typeof(string));
                dt.Columns.Add("MoTaHangTruocBoSung", typeof(string));
                dt.Columns.Add("MaNuocXXTruocBoSung", typeof(string));
                dt.Columns.Add("MoTaHangSauBoSung", typeof(string));
                dt.Columns.Add("MaNuocXXSauBoSung", typeof(string));
                dt.Columns.Add("TriGiaTruocBoSung", typeof(decimal));
                dt.Columns.Add("SoLuongTruocBoSung", typeof(decimal));
                dt.Columns.Add("MaDVTSLTruocBoSung", typeof(string));
                dt.Columns.Add("MaHSTruocBoSung", typeof(string));
                dt.Columns.Add("ThueSuatTruocBoSung", typeof(string));
                dt.Columns.Add("TienThueTruocBoSung", typeof(decimal));
                dt.Columns.Add("MienThueTruocBoSung", typeof(string));
                dt.Columns.Add("TriGiaSauBoSung", typeof(decimal));
                dt.Columns.Add("SoLuongSauBoSung", typeof(decimal));
                dt.Columns.Add("MaDVTSLSauBoSung", typeof(string));
                dt.Columns.Add("MaHSSauBoSung", typeof(string));
                dt.Columns.Add("ThueSuatSauBoSung", typeof(string));
                dt.Columns.Add("TienThueSauBoSung", typeof(decimal));
                dt.Columns.Add("MienThueSauBoSung", typeof(string));
                dt.Columns.Add("TangGiamThueXNK", typeof(string));
                dt.Columns.Add("SoTienTangGiamThue", typeof(decimal));

                dt.Columns.Add("MaTiepNhanTangGiamTTK1", typeof(string));
                dt.Columns.Add("TenTiepNhanTangGiamTTK1", typeof(string));
                dt.Columns.Add("TriGiaTruocBoSungTTK1", typeof(decimal));
                dt.Columns.Add("SoLuongTruocBoSungTTK1", typeof(decimal));
                dt.Columns.Add("MaDVTTruocBoSungTTK1", typeof(string));
                dt.Columns.Add("MaThueSuatTruocBoSungTTK1", typeof(string));
                dt.Columns.Add("ThueSuatTruocBoSungTTK1", typeof(string));
                dt.Columns.Add("SoTienThueTruocBoSungTTK1", typeof(decimal));
                dt.Columns.Add("MienThueTTKTruocBoSung1", typeof(string));
                dt.Columns.Add("TriGiaSauBoSungTTK1", typeof(decimal));
                dt.Columns.Add("SoLuongSauBoSungTTK1", typeof(decimal));
                dt.Columns.Add("MaDVTSLSauBoSungTTK1", typeof(string));
                dt.Columns.Add("MaThueSuatSauBoSungTTK1", typeof(string));
                dt.Columns.Add("ThueSuatSauBoSungTTK1", typeof(string));
                dt.Columns.Add("SoTienThueSauBoSungTTK1", typeof(decimal));
                dt.Columns.Add("MienThueTTKSauBoSung1", typeof(string));
                dt.Columns.Add("TangGiamTTK1", typeof(string));
                dt.Columns.Add("SoTienTangGiamTTK1", typeof(decimal));

                dt.Columns.Add("MaTiepNhanTangGiamTTK2", typeof(string));
                dt.Columns.Add("TenTiepNhanTangGiamTTK2", typeof(string));
                dt.Columns.Add("TriGiaTruocBoSungTTK2", typeof(decimal));
                dt.Columns.Add("SoLuongTruocBoSungTTK2", typeof(decimal));
                dt.Columns.Add("MaDVTTruocBoSungTTK2", typeof(string));
                dt.Columns.Add("MaThueSuatTruocBoSungTTK2", typeof(string));
                dt.Columns.Add("ThueSuatTruocBoSungTTK2", typeof(string));
                dt.Columns.Add("SoTienThueTruocBoSungTTK2", typeof(decimal));
                dt.Columns.Add("MienThueTTKTruocBoSung2", typeof(string));
                dt.Columns.Add("TriGiaSauBoSungTTK2", typeof(decimal));
                dt.Columns.Add("SoLuongSauBoSungTTK2", typeof(decimal));
                dt.Columns.Add("MaDVTSLSauBoSungTTK2", typeof(string));
                dt.Columns.Add("MaThueSuatSauBoSungTTK2", typeof(string));
                dt.Columns.Add("ThueSuatSauBoSungTTK2", typeof(string));
                dt.Columns.Add("SoTienThueSauBoSungTTK2", typeof(decimal));
                dt.Columns.Add("MienThueTTKSauBoSung2", typeof(string));
                dt.Columns.Add("TangGiamTTK2", typeof(string));
                dt.Columns.Add("SoTienTangGiamTTK2", typeof(decimal));

                dt.Columns.Add("MaTiepNhanTangGiamTTK3", typeof(string));
                dt.Columns.Add("TenTiepNhanTangGiamTTK3", typeof(string));
                dt.Columns.Add("TriGiaTruocBoSungTTK3", typeof(decimal));
                dt.Columns.Add("SoLuongTruocBoSungTTK3", typeof(decimal));
                dt.Columns.Add("MaDVTTruocBoSungTTK3", typeof(string));
                dt.Columns.Add("MaThueSuatTruocBoSungTTK3", typeof(string));
                dt.Columns.Add("ThueSuatTruocBoSungTTK3", typeof(string));
                dt.Columns.Add("SoTienThueTruocBoSungTTK3", typeof(decimal));
                dt.Columns.Add("MienThueTTKTruocBoSung3", typeof(string));
                dt.Columns.Add("TriGiaSauBoSungTTK3", typeof(decimal));
                dt.Columns.Add("SoLuongSauBoSungTTK3", typeof(decimal));
                dt.Columns.Add("MaDVTSLSauBoSungTTK3", typeof(string));
                dt.Columns.Add("MaThueSuatSauBoSungTTK3", typeof(string));
                dt.Columns.Add("ThueSuatSauBoSungTTK3", typeof(string));
                dt.Columns.Add("SoTienThueSauBoSungTTK3", typeof(decimal));
                dt.Columns.Add("MienThueTTKSauBoSung3", typeof(string));
                dt.Columns.Add("TangGiamTTK3", typeof(string));
                dt.Columns.Add("SoTienTangGiamTTK3", typeof(decimal));

                dt.Columns.Add("MaTiepNhanTangGiamTTK4", typeof(string));
                dt.Columns.Add("TenTiepNhanTangGiamTTK4", typeof(string));
                dt.Columns.Add("TriGiaTruocBoSungTTK4", typeof(decimal));
                dt.Columns.Add("SoLuongTruocBoSungTTK4", typeof(decimal));
                dt.Columns.Add("MaDVTTruocBoSungTTK4", typeof(string));
                dt.Columns.Add("MaThueSuatTruocBoSungTTK4", typeof(string));
                dt.Columns.Add("ThueSuatTruocBoSungTTK4", typeof(string));
                dt.Columns.Add("SoTienThueTruocBoSungTTK4", typeof(decimal));
                dt.Columns.Add("MienThueTTKTruocBoSung4", typeof(string));
                dt.Columns.Add("TriGiaSauBoSungTTK4", typeof(decimal));
                dt.Columns.Add("SoLuongSauBoSungTTK4", typeof(decimal));
                dt.Columns.Add("MaDVTSLSauBoSungTTK4", typeof(string));
                dt.Columns.Add("MaThueSuatSauBoSungTTK4", typeof(string));
                dt.Columns.Add("ThueSuatSauBoSungTTK4", typeof(string));
                dt.Columns.Add("SoTienThueSauBoSungTTK4", typeof(decimal));
                dt.Columns.Add("MienThueTTKSauBoSung4", typeof(string));
                dt.Columns.Add("TangGiamTTK4", typeof(string));
                dt.Columns.Add("SoTienTangGiamTTK4", typeof(decimal));

                dt.Columns.Add("MaTiepNhanTangGiamTTK5", typeof(string));
                dt.Columns.Add("TenTiepNhanTangGiamTTK5", typeof(string));
                dt.Columns.Add("TriGiaTruocBoSungTTK5", typeof(decimal));
                dt.Columns.Add("SoLuongTruocBoSungTTK5", typeof(decimal));
                dt.Columns.Add("MaDVTTruocBoSungTTK5", typeof(string));
                dt.Columns.Add("MaThueSuatTruocBoSungTTK5", typeof(string));
                dt.Columns.Add("ThueSuatTruocBoSungTTK5", typeof(string));
                dt.Columns.Add("SoTienThueTruocBoSungTTK5", typeof(decimal));
                dt.Columns.Add("MienThueTTKTruocBoSung5", typeof(string));
                dt.Columns.Add("TriGiaSauBoSungTTK5", typeof(decimal));
                dt.Columns.Add("SoLuongSauBoSungTTK5", typeof(decimal));
                dt.Columns.Add("MaDVTSLSauBoSungTTK5", typeof(string));
                dt.Columns.Add("MaThueSuatSauBoSungTTK5", typeof(string));
                dt.Columns.Add("ThueSuatSauBoSungTTK5", typeof(string));
                dt.Columns.Add("SoTienThueSauBoSungTTK5", typeof(decimal));
                dt.Columns.Add("MienThueTTKSauBoSung5", typeof(string));
                dt.Columns.Add("TangGiamTTK5", typeof(string));
                dt.Columns.Add("SoTienTangGiamTTK5", typeof(decimal));

                for (int i = 0; i < VAL.HangMD.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();

                    dr["SoDong"] = VAL.HangMD[i].A26.GetValue().ToString().Trim();
                    dr["SoThuTuHangTK"] = VAL.HangMD[i].A28.Value.ToString().Trim();
                    dr["MoTaHangTruocBoSung"] = VAL.HangMD[i].A31.Value.ToString().Trim();
                    dr["MaNuocXXTruocBoSung"] = VAL.HangMD[i].A33.Value.ToString().Trim();
                    dr["MoTaHangSauBoSung"] = VAL.HangMD[i].A32.Value.ToString().Trim();
                    dr["MaNuocXXSauBoSung"] = VAL.HangMD[i].A34.Value.ToString().Trim();
                    dr["TriGiaTruocBoSung"] = VAL.HangMD[i].A39.Value.ToString().Replace(".", ",");
                    dr["SoLuongTruocBoSung"] = VAL.HangMD[i].A40.Value.ToString().Replace(".", ",");
                    dr["MaDVTSLTruocBoSung"] = VAL.HangMD[i].A41.Value.ToString().Trim();
                    dr["MaHSTruocBoSung"] = VAL.HangMD[i].A42.Value.ToString().Trim();
                    dr["ThueSuatTruocBoSung"] = VAL.HangMD[i].A43.Value.ToString().Trim();
                    dr["TienThueTruocBoSung"] = VAL.HangMD[i].A44.Value.ToString().Replace(".", ",");
                    dr["MienThueTruocBoSung"] = VAL.HangMD[i].A45.Value.ToString().Trim();
                    dr["TriGiaSauBoSung"] = VAL.HangMD[i].A46.Value.ToString().Replace(".", ",");
                    dr["SoLuongSauBoSung"] = VAL.HangMD[i].A47.Value.ToString().Replace(".", ",");
                    dr["MaDVTSLSauBoSung"] = VAL.HangMD[i].A48.Value.ToString().Trim();
                    dr["MaHSSauBoSung"] = VAL.HangMD[i].A49.Value.ToString().Trim();
                    dr["ThueSuatSauBoSung"] = VAL.HangMD[i].A50.Value.ToString().Trim();
                    dr["TienThueSauBoSung"] = VAL.HangMD[i].A51.Value.ToString().Replace(".", ",");
                    dr["MienThueSauBoSung"] = VAL.HangMD[i].A52.Value.ToString().Trim();
                    dr["TangGiamThueXNK"] = VAL.HangMD[i].ADC.Value.ToString().Trim();
                    dr["SoTienTangGiamThue"] = VAL.HangMD[i].A53.Value.ToString().Replace(".", ",");
                    for (int j = 0; j < 5; j++)
                    {
                        dr["MaTiepNhanTangGiamTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[0].GetValueCollection(j).ToString().Trim();
                        dr["TenTiepNhanTangGiamTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[1].GetValueCollection(j).ToString().Trim();
                        dr["TriGiaTruocBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[2].GetValueCollection(j).ToString().Replace(".", ",");
                        dr["SoLuongTruocBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[3].GetValueCollection(j).ToString().Replace(".", ",");
                        dr["MaDVTTruocBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[4].GetValueCollection(j).ToString().Trim();
                        dr["MaThueSuatTruocBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[5].GetValueCollection(j).ToString().Trim();
                        dr["ThueSuatTruocBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[6].GetValueCollection(j).ToString().Trim();
                        dr["SoTienThueTruocBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[7].GetValueCollection(j).ToString().Replace(".", ",");
                        dr["MienThueTTKTruocBoSung" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[8].GetValueCollection(j).ToString().Trim();
                        dr["TriGiaSauBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[9].GetValueCollection(j).ToString().Replace(".", ",");
                        dr["SoLuongSauBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[10].GetValueCollection(j).ToString().Replace(".", ",");
                        dr["MaDVTSLSauBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[11].GetValueCollection(j).ToString().Trim();
                        dr["MaThueSuatSauBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[12].GetValueCollection(j).ToString().Trim();
                        dr["ThueSuatSauBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[13].GetValueCollection(j).ToString().Trim();
                        dr["SoTienThueSauBoSungTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[14].GetValueCollection(j).ToString().Replace(".", ",");
                        dr["MienThueTTKSauBoSung" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[15].GetValueCollection(j).ToString().Trim();
                        dr["TangGiamTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[16].GetValueCollection(j).ToString().Trim();
                        dr["SoTienTangGiamTTK" + (j + 1)] = VAL.HangMD[i].KD1.listAttribute[17].GetValueCollection(j).ToString().Replace(".", ",");
                    }
                    dt.Rows.Add(dr);
                }
                BindReportHang(dt);
            }
        }
        private void BindReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSoDong.DataBindings.Add("Text", DetailReport.DataSource, "SoDong");
            lblSoThuTuHangTK.DataBindings.Add("Text", DetailReport.DataSource, "SoThuTuHangTK");
            lblMoTaHangTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MoTaHangTruocBoSung");
            lblMaNuocXXTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MaNuocXXTruocBoSung");
            lblMoTaHangSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MoTaHangSauBoSung");
            lblMaNuocXXSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MaNuocXXSauBoSung");
            lblTriGiaTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTruocBoSung", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTruocBoSung", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTSLTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSLTruocBoSung");
            lblMaHSTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MaHSTruocBoSung");
            lblThueSuatTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatTruocBoSung");
            lblTienThueTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "TienThueTruocBoSung", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTruocBoSung");
            lblTriGiaSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSauBoSung", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongSauBoSung", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTSLSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSLSauBoSung");
            lblMaHSSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MaHSSauBoSung");
            lblThueSuatSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatSauBoSung");
            lblTienThueSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "TienThueSauBoSung", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MienThueSauBoSung");
            lblTangGiamThueXNK.DataBindings.Add("Text", DetailReport.DataSource, "TangGiamThueXNK");
            lblSoTienTangGiamThue.DataBindings.Add("Text", DetailReport.DataSource, "SoTienTangGiamThue", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));

            lblMaTiepNhanTangGiamTTK.DataBindings.Add("Text", DetailReport.DataSource, "MaTiepNhanTangGiamTTK1");
            lblTenTiepNhanTangGiamTTK.DataBindings.Add("Text", DetailReport.DataSource, "TenTiepNhanTangGiamTTK1");
            lblTriGiaTruocBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTruocBoSungTTK1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTruocBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTruocBoSungTTK1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTTruocBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTTruocBoSungTTK1");
            lblMaThueSuatTruocBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatTruocBoSungTTK1");
            lblThueSuatTruocBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatTruocBoSungTTK1");
            lblSoTienThueTruocBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueTruocBoSungTTK1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKTruocBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKTruocBoSung1");
            lblTriGiaSauBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSauBoSungTTK1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongSauBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongSauBoSungTTK1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTSLSauBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSLSauBoSungTTK1");
            lblMaThueSuatSauBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatSauBoSungTTK1");
            lblThueSuatSauBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatSauBoSungTTK1");
            lblSoTienThueSauBoSungTTK.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueSauBoSungTTK1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKSauBoSung.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKSauBoSung1");
            lblTangGiamTTK.DataBindings.Add("Text", DetailReport.DataSource, "TangGiamTTK1");
            lblSoTienTangGiamTTK.DataBindings.Add("Text", DetailReport.DataSource, "SoTienTangGiamTTK1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));

            lblMaTiepNhanTangGiamTTK2.DataBindings.Add("Text", DetailReport.DataSource, "MaTiepNhanTangGiamTTK2");
            lblTenTiepNhanTangGiamTTK2.DataBindings.Add("Text", DetailReport.DataSource, "TenTiepNhanTangGiamTTK2");
            lblTriGiaTruocBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTruocBoSungTTK2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTruocBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTruocBoSungTTK2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTTruocBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTTruocBoSungTTK2");
            lblMaThueSuatTruocBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatTruocBoSungTTK2");
            lblThueSuatTruocBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatTruocBoSungTTK2");
            lblSoTienThueTruocBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueTruocBoSungTTK2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKTruocBoSung2.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKTruocBoSung2");
            lblTriGiaSauBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSauBoSungTTK2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongSauBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongSauBoSungTTK2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTSLSauBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSLSauBoSungTTK2");
            lblMaThueSuatSauBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatSauBoSungTTK2");
            lblThueSuatSauBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatSauBoSungTTK2");
            lblSoTienThueSauBoSungTTK2.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueSauBoSungTTK2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKSauBoSung2.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKSauBoSung2");
            lblTangGiamTTK2.DataBindings.Add("Text", DetailReport.DataSource, "TangGiamTTK2");
            lblSoTienTangGiamTTK2.DataBindings.Add("Text", DetailReport.DataSource, "SoTienTangGiamTTK2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));

            lblMaTiepNhanTangGiamTTK3.DataBindings.Add("Text", DetailReport.DataSource, "MaTiepNhanTangGiamTTK3");
            lblTenTiepNhanTangGiamTTK3.DataBindings.Add("Text", DetailReport.DataSource, "TenTiepNhanTangGiamTTK3");
            lblTriGiaTruocBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTruocBoSungTTK3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTruocBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTruocBoSungTTK3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTTruocBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTTruocBoSungTTK3");
            lblMaThueSuatTruocBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatTruocBoSungTTK3");
            lblThueSuatTruocBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatTruocBoSungTTK3");
            lblSoTienThueTruocBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueTruocBoSungTTK3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKTruocBoSung3.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKTruocBoSung3");
            lblTriGiaSauBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSauBoSungTTK3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongSauBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongSauBoSungTTK3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTSLSauBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSLSauBoSungTTK3");
            lblMaThueSuatSauBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatSauBoSungTTK3");
            lblThueSuatSauBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatSauBoSungTTK3");
            lblSoTienThueSauBoSungTTK3.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueSauBoSungTTK3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKSauBoSung3.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKSauBoSung3");
            lblTangGiamTTK3.DataBindings.Add("Text", DetailReport.DataSource, "TangGiamTTK3");
            lblSoTienTangGiamTTK3.DataBindings.Add("Text", DetailReport.DataSource, "SoTienTangGiamTTK3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));

            lblMaTiepNhanTangGiamTTK4.DataBindings.Add("Text", DetailReport.DataSource, "MaTiepNhanTangGiamTTK4");
            lblTenTiepNhanTangGiamTTK4.DataBindings.Add("Text", DetailReport.DataSource, "TenTiepNhanTangGiamTTK4");
            lblTriGiaTruocBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTruocBoSungTTK4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTruocBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTruocBoSungTTK4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTTruocBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTTruocBoSungTTK4");
            lblMaThueSuatTruocBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatTruocBoSungTTK4");
            lblThueSuatTruocBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatTruocBoSungTTK4");
            lblSoTienThueTruocBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueTruocBoSungTTK4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKTruocBoSung4.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKTruocBoSung4");
            lblTriGiaSauBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSauBoSungTTK4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongSauBoSungTTK4.Text = "";
            lblSoLuongSauBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongSauBoSungTTK4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTSLSauBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSLSauBoSungTTK4");
            lblMaThueSuatSauBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatSauBoSungTTK4");
            lblThueSuatSauBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatSauBoSungTTK4");
            lblSoTienThueSauBoSungTTK4.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueSauBoSungTTK4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKSauBoSung4.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKSauBoSung4");
            lblTangGiamTTK4.DataBindings.Add("Text", DetailReport.DataSource, "TangGiamTTK4");
            lblSoTienTangGiamTTK4.DataBindings.Add("Text", DetailReport.DataSource, "SoTienTangGiamTTK4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));

            lblMaTiepNhanTangGiamTTK5.DataBindings.Add("Text", DetailReport.DataSource, "MaTiepNhanTangGiamTTK5");
            lblTenTiepNhanTangGiamTTK5.DataBindings.Add("Text", DetailReport.DataSource, "TenTiepNhanTangGiamTTK5");
            lblTriGiaTruocBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTruocBoSungTTK5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTruocBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTruocBoSungTTK5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTTruocBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTTruocBoSungTTK5");
            lblMaThueSuatTruocBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatTruocBoSungTTK5");
            lblThueSuatTruocBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatTruocBoSungTTK5");
            lblSoTienThueTruocBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueTruocBoSungTTK5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKTruocBoSung5.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKTruocBoSung5");
            lblTriGiaSauBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSauBoSungTTK5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongSauBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongSauBoSungTTK5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDVTSLSauBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSLSauBoSungTTK5");
            lblMaThueSuatSauBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "MaThueSuatSauBoSungTTK5");
            lblThueSuatSauBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatSauBoSungTTK5");
            lblSoTienThueSauBoSungTTK5.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueSauBoSungTTK5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMienThueTTKSauBoSung5.DataBindings.Add("Text", DetailReport.DataSource, "MienThueTTKSauBoSung5");
            lblTangGiamTTK5.DataBindings.Add("Text", DetailReport.DataSource, "TangGiamTTK5");
            lblSoTienTangGiamTTK5.DataBindings.Add("Text", DetailReport.DataSource, "SoTienTangGiamTTK5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            //DetailReport1.DataSource = dt;

        }

        private void lblSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 2).ToString();
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lblSoLuongSauBoSungTTK4.Text == "0") 
            {
                lblSoLuongSauBoSungTTK4.Text = "";
            }
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
            if (lbl.Name.Contains("lblTienThueTruocBoSung"))
            {
                if (lbl.Text.Trim() == "")
                    lbl.Text = "0.0000";
            }
            if (lbl.Name.Contains("lblTienThueSauBoSung"))
            {
                if (lbl.Text.Trim() == "")
                    lbl.Text = "0.0000";
            }
            if (lbl.Name.Contains("lblSoTienTangGiamThue"))
            {
                if (lbl.Text.Trim() == "")
                    lbl.Text = "0";
            }
        }


    }
}
