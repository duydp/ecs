﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System;
using System.Security.Cryptography;
using Company.Controls;
using System.IO;
using Company.BLL.KDT;
namespace Company.Interface.PhongKhai
{
    public class LoginForm : Form
    {
        private IContainer components = null;

        public int intLogin = 0;
        private DataRowCollection dtb;
        public int intcount = 0;
        public bool config;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton btnLogin;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQtKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private HeThongPhongKhai hqch = new HeThongPhongKhai();

        public LoginForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnLogin = new Janus.Windows.EditControls.UIButton();
            this.txtMaHQtKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.SuspendLayout();
            // 
            // uiButton1
            // 
            this.uiButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(334, 220);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(105, 23);
            this.uiButton1.TabIndex = 38;
            this.uiButton1.Text = "&Thoát";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.VS2005;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            this.uiButton1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            // 
            // btnLogin
            // 
            this.btnLogin.FlatBorderColor = System.Drawing.Color.SaddleBrown;
            this.btnLogin.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLogin.Icon")));
            this.btnLogin.Location = new System.Drawing.Point(219, 220);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(105, 23);
            this.btnLogin.TabIndex = 37;
            this.btnLogin.Text = "Đăng &nhập";
            this.btnLogin.VisualStyle = Janus.Windows.UI.VisualStyle.VS2005;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            this.btnLogin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            // 
            // txtMaHQtKhau
            // 
            this.txtMaHQtKhau.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtMaHQtKhau.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.txtMaHQtKhau.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHQtKhau.Location = new System.Drawing.Point(189, 186);
            this.txtMaHQtKhau.Name = "txtMaHQtKhau";
            this.txtMaHQtKhau.PasswordChar = '*';
            this.txtMaHQtKhau.Size = new System.Drawing.Size(280, 22);
            this.txtMaHQtKhau.TabIndex = 36;
            this.txtMaHQtKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHQtKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.VS2005;
            this.txtMaHQtKhau.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtMaDoanhNghiep.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.txtMaDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(189, 124);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(280, 22);
            this.txtMaDoanhNghiep.TabIndex = 35;
            this.txtMaDoanhNghiep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.VS2005;
            this.txtMaDoanhNghiep.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            // 
            // LoginForm
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.BackColor = System.Drawing.Color.Blue;
            //this.BackgroundImage = global::Company.Interface.Properties.Resources.GIACONG;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.uiButton1;
            this.ClientSize = new System.Drawing.Size(500, 300);
            this.ControlBox = false;
            this.Controls.Add(this.uiButton1);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtMaHQtKhau);
            this.Controls.Add(this.txtMaDoanhNghiep);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập hệ thống";
            this.TransparencyKey = System.Drawing.Color.Blue;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LoginForm_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaDoanhNghiep_KeyDown);
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private void Login(string strMa, string strPass)
        {

            try
            {
                dtb = hqch.SelectLogin().Tables[0].Rows;
                string strMaRow = "";
                string strPassRow = "";
                string strValueConfig = "";
                string strKeyConfig = "";
                int intRole = 0;

                foreach (DataRow dr in dtb)
                {
                    strMaRow = dr["MadoanhNghiep"].ToString().Trim();
                    strPassRow = dr["PassWord"].ToString().Trim();
                    strValueConfig = dr["Value_Config"].ToString().Trim();
                    strKeyConfig = dr["Key_Config"].ToString().Trim();
                    intRole = Convert.ToInt16(dr["Role"].ToString().Trim());
                    if (strMa == strMaRow && intRole == 0 && strValueConfig == "1" && strKeyConfig == "CauHinh")
                    {
                        config = true;
                    }
                    else if (strMa == strMaRow && intRole == 0 && strValueConfig == "0" && strKeyConfig == "CauHinh")
                    {
                        config = false;
                    }

                    if (strMa == strMaRow && strPass == strPassRow && intRole == 1)
                    {
                        intLogin = 1;
                        return;
                    }
                    else if (strMa == strMaRow && strPass == strPassRow && intRole == 0)
                    {
                        intLogin = 2;

                        return;
                    }
                    else
                    {
                        intLogin = 3;
                    }


                }
            }
            catch
            {

                intLogin = 4;
                return;
            }

        }
        private void ShowMainForm()
        {
            MainForm mainForm = new MainForm();
            mainForm.ShowDialog(this);

        }

        private void lblDangky_Click(object sender, System.EventArgs e)
        {
            this.Hide();
            this.ShowAdminForm();
            // this.Close();

        }

        private void ShowAdminForm()
        {

            AdminAccountForm adminAccountForm = new AdminAccountForm();
            adminAccountForm.ShowDialog(this);
        }

        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }

        private void grbMain_Click(object sender, System.EventArgs e)
        {

        }



        private void LoginForm_Load(object sender, EventArgs e)
        {
           // lblCucHaiQuan.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
            //lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN.ToUpper();
            //this.BackgroundImage = System.Drawing.Image.FromFile("GIACONG.PNG");
            //this.BackgroundImageLayout = ImageLayout.Stretch;
            try
            {
                //  this.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Images\\KDDT.png");
                this.BackgroundImage = System.Drawing.Image.FromFile("LOGIN-SXXK.png");
                this.BackgroundImageLayout = ImageLayout.Stretch;
            }
            catch (OutOfMemoryException ex1)
            {

            }
            catch (FileNotFoundException ex2)
            {

            }
            catch (ArgumentException ex3)
            {

            }
            catch (Exception ex)
            {

            }
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.Close();
        }

        private void lblCauHinhCSDL_Click(object sender, EventArgs e)
        {
            this.Hide();
            //CauHinhCSDLForm cauHinhCSDLForm = new CauHinhCSDLForm();
            //cauHinhCSDLForm.ShowDialog(this);
            //this.Close();
        }
        private void showFormConfig()
        {
            this.Hide();
            DangKyForm dangKyForm = new DangKyForm();
            dangKyForm.ShowDialog(this);
        }
        protected MessageBoxControl _MsgBox;
        private void showFormCreatAccount()
        {
            this.Hide();
            CreatAccountForm creatAccountForm = new CreatAccountForm();
            creatAccountForm.ShowDialog(this);
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void Login()
        {
            config = false;
            //txtMaDoanhNghiep.Text = txtMaDoanhNghiep.Text.ToLower();
            //txtMaHQtKhau.Text = txtMaHQtKhau.Text.ToLower();
            string strMa = txtMaDoanhNghiep.Text.Trim();
            string strPass = this.GetMD5Value(txtMaHQtKhau.Text.Trim());
            if (strMa == "")
            {
               // lblThongBao.Text = "Chưa nhập Mã Doanh Nghiệp";
                MessageBox.Show("Chưa nhập Mã doanh nghiệp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtMaDoanhNghiep.Focus();
                return;
            }
            if (txtMaHQtKhau.Text.Trim() == "")
            {
               // lblThongBao.Text = "Chưa nhập Mật khẩu";
                MessageBox.Show("Chưa nhập Mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);

                txtMaHQtKhau.Focus();
                return;
            }
            //cvError.Validate();
            //if (!cvError.IsValid) return;           
            this.Login(strMa, strPass);
            switch (intLogin)
            {
                case 1:
                        MainForm.isLoginSuccess = true;
                        MainForm.typeLogin = 3;
                        this.Close();

                    break;
                case 2:
                    {

                        try
                        {
                            //Thông tin Doanh Nhiệp
                            GlobalSettings.MA_DON_VI = strMa;
                            if (config)
                            {
                                GlobalSettings.TEN_DON_VI = hqch.SelectedSettingsName(strMa, "CauHinh");
                                GlobalSettings.DIA_CHI = hqch.SelectedSettings(strMa, "DIA_CHI");

                                //Thông tin mặc định chung
                                //string temp = hqch.SelectedSettings(strMa, "TY_GIA_USD");
                                GlobalSettings.TY_GIA_USD = 16000;
                                GlobalSettings.NUOC = hqch.SelectedSettings(strMa, "NUOC");
                                //GlobalSettings.CL_THN_THX = hqch.SelectedSettings(strMa, "CL_THN_THX");
                                GlobalSettings.CUA_KHAU = hqch.SelectedSettings(strMa, "CUA_KHAU");
                                GlobalSettings.TEN_DOI_TAC = hqch.SelectedSettings(strMa, "TEN_DOI_TAC");
                                GlobalSettings.DKGH_MAC_DINH = hqch.SelectedSettings(strMa, "DKGH_MAC_DINH");
                                GlobalSettings.DVT_MAC_DINH = hqch.SelectedSettings(strMa, "DVT_MAC_DINH").PadRight(3);
                                GlobalSettings.PTTT_MAC_DINH = hqch.SelectedSettings(strMa, "PTTT_MAC_DINH");
                                GlobalSettings.PTVT_MAC_DINH = hqch.SelectedSettings(strMa, "PTVT_MAC_DINH");
                                GlobalSettings.NGUYEN_TE_MAC_DINH = hqch.SelectedSettings(strMa, "NGUYEN_TE_MAC_DINH");
                                GlobalSettings.LOAI_HINH = hqch.SelectedSettings(strMa, "LOAI_HINH");
                                GlobalSettings.NHOM_LOAI_HINH = hqch.SelectedSettings(strMa, "NHOM_LOAI_HINH");
                                //GlobalSettings.NHOM_LOAI_HINH_KHAC_NHAP = hqch.SelectedSettings(strMa, "NHOM_LOAI_HINH_KHAC_NHAP");

                                GlobalSettings.NGAYSAOLUU = hqch.SelectedSettings(strMa, "NgaySaoLuu");
                                GlobalSettings.NHAC_NHO_SAO_LUU = hqch.SelectedSettings(strMa, "NHAC_NHO_SAO_LUU");
                                GlobalSettings.MaMID = hqch.SelectedSettings(strMa, "MaMID");
                                GlobalSettings.TuDongTinhThue = hqch.SelectedSettings(strMa, "TuDongTinhThue");
                                //KhanhHN - 21/06/2012
                                GlobalSettings.TinhThueTGNT = hqch.SelectedSettings(strMa, "TinhThueTGNT");
                                GlobalSettings.RefreshKey();
                                MainForm.isLoginSuccess = true;
                                MainForm.typeLogin = 1;
                                MainForm.EcsQuanTri = new Company.QuanTri.ECSPrincipal("admin");
                                this.Close();
                            }
                            else
                            {
                                GlobalSettings.TEN_DON_VI = GlobalSettings.TEN_DON_VI = hqch.SelectedSettingsName(strMa, "CauHinh");
                                GlobalSettings.DIA_CHI = "";
                                GlobalSettings.MaMID = "";
                                MainForm.isLoginSuccess = true;
                                MainForm.typeLogin = 2;
                                this.Close();
                            }
                        }
                        catch (Exception ex1)
                        {
                            ShowMessage("Lỗi :" + ex1.Message.ToString(), false);
                        }
                        break;
                    }
                case 3:
                    intcount++;
                    MainForm.isLoginSuccess = false;
                    //lblThongBao.Text = "Đăng nhập không thành công. Lỗi sai mật khẩu hoặc tên đăng nhập.";
                    MessageBox.Show("Đăng nhập không thành công. Lỗi sai mật khẩu hoặc tên đăng nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                default:
                    MainForm.isLoginSuccess = false;
                    //lblThongBao.Text = "Lỗi không kết nối được cơ sở dữ liệu !";
                    MessageBox.Show("Lỗi không kết nối được cơ sở dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;

            }
            //Neu so lan dang nhap khong thanh cong > 5 thi thoat

        }
        private void lblDangNhap_Click(object sender, EventArgs e)
        {

        }
        private void lblThoat_Click(object sender, EventArgs e)
        {

        }

        private void lblCH_DoubleClick(object sender, EventArgs e)
        {
            //CauHinhCSDLForm chForm = new CauHinhCSDLForm();
            //chForm.ShowDialog(this);
        }

        private void lblDB_DoubleClick(object sender, EventArgs e)
        {
            //CauHinhCSDLHaiQuanForm chHaiQuanForm = new CauHinhCSDLHaiQuanForm();
            //chHaiQuanForm.ShowDialog(this); 

        }

        private void txtKeyDownEvent(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.Login();
                    break;
                case Keys.Escape:
                    Application.Exit();
                    break;
            }
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            this.Login();
        }

        private void uiButton1_Click(object sender, EventArgs e)//thoat
        {
            MainForm.isLoginSuccess = false;
            this.Close();
        }

        private void txtMaDoanhNghiep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F10)
            {
                PasswordForm passForm = new PasswordForm();
                passForm.ShowDialog(this);
                if (passForm.IsPass)
                {
                    CauHinhCSDLHaiQuanForm cauHinhForm = new CauHinhCSDLHaiQuanForm();
                    cauHinhForm.ShowDialog(this);
                    if (cauHinhForm.Change)
                    {
                        //MainForm.CloseAll();
                        Application.Restart();
                    }
                }
            }
        }


    }
}