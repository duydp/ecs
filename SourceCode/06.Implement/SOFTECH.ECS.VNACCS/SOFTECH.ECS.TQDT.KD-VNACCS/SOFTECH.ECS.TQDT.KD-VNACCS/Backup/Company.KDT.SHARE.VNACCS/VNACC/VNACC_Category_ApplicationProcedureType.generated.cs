using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_ApplicationProcedureType : ICloneable
	{
		#region Properties.
		
		public string ResultCode { set; get; }
		public int PGNumber { set; get; }
		public string TableID { set; get; }
		public string ProcessClassification { set; get; }
		public int CreatorClassification { set; get; }
		public string NumberOfKeyItems { set; get; }
		public string ApplicationProcedureType { set; get; }
		public string ApplicationProcedureName { set; get; }
		public int Section { set; get; }
		public int IndicationOfGeneralDepartmentOfCustoms { set; get; }
		public int IndicationOfDisapproval { set; get; }
		public int IndicationOfCancellation { set; get; }
		public int IndicationOfCompletionOfExamination { set; get; }
		public int NumberOfYearsForSavingTheOriginalDocumentData { set; get; }
		public DateTime ExpiryDate { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACC_Category_ApplicationProcedureType> ConvertToCollection(IDataReader reader)
		{
			List<VNACC_Category_ApplicationProcedureType> collection = new List<VNACC_Category_ApplicationProcedureType>();
			while (reader.Read())
			{
				VNACC_Category_ApplicationProcedureType entity = new VNACC_Category_ApplicationProcedureType();
				if (!reader.IsDBNull(reader.GetOrdinal("ResultCode"))) entity.ResultCode = reader.GetString(reader.GetOrdinal("ResultCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("PGNumber"))) entity.PGNumber = reader.GetInt32(reader.GetOrdinal("PGNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProcessClassification"))) entity.ProcessClassification = reader.GetString(reader.GetOrdinal("ProcessClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatorClassification"))) entity.CreatorClassification = reader.GetInt32(reader.GetOrdinal("CreatorClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("NumberOfKeyItems"))) entity.NumberOfKeyItems = reader.GetString(reader.GetOrdinal("NumberOfKeyItems"));
				if (!reader.IsDBNull(reader.GetOrdinal("ApplicationProcedureType"))) entity.ApplicationProcedureType = reader.GetString(reader.GetOrdinal("ApplicationProcedureType"));
				if (!reader.IsDBNull(reader.GetOrdinal("ApplicationProcedureName"))) entity.ApplicationProcedureName = reader.GetString(reader.GetOrdinal("ApplicationProcedureName"));
				if (!reader.IsDBNull(reader.GetOrdinal("Section"))) entity.Section = reader.GetInt32(reader.GetOrdinal("Section"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfGeneralDepartmentOfCustoms"))) entity.IndicationOfGeneralDepartmentOfCustoms = reader.GetInt32(reader.GetOrdinal("IndicationOfGeneralDepartmentOfCustoms"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfDisapproval"))) entity.IndicationOfDisapproval = reader.GetInt32(reader.GetOrdinal("IndicationOfDisapproval"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfCancellation"))) entity.IndicationOfCancellation = reader.GetInt32(reader.GetOrdinal("IndicationOfCancellation"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfCompletionOfExamination"))) entity.IndicationOfCompletionOfExamination = reader.GetInt32(reader.GetOrdinal("IndicationOfCompletionOfExamination"));
				if (!reader.IsDBNull(reader.GetOrdinal("NumberOfYearsForSavingTheOriginalDocumentData"))) entity.NumberOfYearsForSavingTheOriginalDocumentData = reader.GetInt32(reader.GetOrdinal("NumberOfYearsForSavingTheOriginalDocumentData"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExpiryDate"))) entity.ExpiryDate = reader.GetDateTime(reader.GetOrdinal("ExpiryDate"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<VNACC_Category_ApplicationProcedureType> collection, string applicationProcedureType)
        {
            foreach (VNACC_Category_ApplicationProcedureType item in collection)
            {
                if (item.ApplicationProcedureType == applicationProcedureType)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACC_Category_ApplicationProcedureType VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @ApplicationProcedureType, @ApplicationProcedureName, @Section, @IndicationOfGeneralDepartmentOfCustoms, @IndicationOfDisapproval, @IndicationOfCancellation, @IndicationOfCompletionOfExamination, @NumberOfYearsForSavingTheOriginalDocumentData, @ExpiryDate, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_ApplicationProcedureType SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, ApplicationProcedureName = @ApplicationProcedureName, Section = @Section, IndicationOfGeneralDepartmentOfCustoms = @IndicationOfGeneralDepartmentOfCustoms, IndicationOfDisapproval = @IndicationOfDisapproval, IndicationOfCancellation = @IndicationOfCancellation, IndicationOfCompletionOfExamination = @IndicationOfCompletionOfExamination, NumberOfYearsForSavingTheOriginalDocumentData = @NumberOfYearsForSavingTheOriginalDocumentData, ExpiryDate = @ExpiryDate, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ApplicationProcedureType = @ApplicationProcedureType";
            string delete = "DELETE FROM t_VNACC_Category_ApplicationProcedureType WHERE ApplicationProcedureType = @ApplicationProcedureType";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ApplicationProcedureType", SqlDbType.VarChar, "ApplicationProcedureType", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ApplicationProcedureName", SqlDbType.NVarChar, "ApplicationProcedureName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Section", SqlDbType.Int, "Section", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfGeneralDepartmentOfCustoms", SqlDbType.Int, "IndicationOfGeneralDepartmentOfCustoms", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfDisapproval", SqlDbType.Int, "IndicationOfDisapproval", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfCancellation", SqlDbType.Int, "IndicationOfCancellation", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfCompletionOfExamination", SqlDbType.Int, "IndicationOfCompletionOfExamination", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfYearsForSavingTheOriginalDocumentData", SqlDbType.Int, "NumberOfYearsForSavingTheOriginalDocumentData", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExpiryDate", SqlDbType.DateTime, "ExpiryDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ApplicationProcedureType", SqlDbType.VarChar, "ApplicationProcedureType", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ApplicationProcedureName", SqlDbType.NVarChar, "ApplicationProcedureName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Section", SqlDbType.Int, "Section", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfGeneralDepartmentOfCustoms", SqlDbType.Int, "IndicationOfGeneralDepartmentOfCustoms", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfDisapproval", SqlDbType.Int, "IndicationOfDisapproval", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfCancellation", SqlDbType.Int, "IndicationOfCancellation", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfCompletionOfExamination", SqlDbType.Int, "IndicationOfCompletionOfExamination", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfYearsForSavingTheOriginalDocumentData", SqlDbType.Int, "NumberOfYearsForSavingTheOriginalDocumentData", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExpiryDate", SqlDbType.DateTime, "ExpiryDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ApplicationProcedureType", SqlDbType.VarChar, "ApplicationProcedureType", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACC_Category_ApplicationProcedureType VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @ApplicationProcedureType, @ApplicationProcedureName, @Section, @IndicationOfGeneralDepartmentOfCustoms, @IndicationOfDisapproval, @IndicationOfCancellation, @IndicationOfCompletionOfExamination, @NumberOfYearsForSavingTheOriginalDocumentData, @ExpiryDate, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_ApplicationProcedureType SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, ApplicationProcedureName = @ApplicationProcedureName, Section = @Section, IndicationOfGeneralDepartmentOfCustoms = @IndicationOfGeneralDepartmentOfCustoms, IndicationOfDisapproval = @IndicationOfDisapproval, IndicationOfCancellation = @IndicationOfCancellation, IndicationOfCompletionOfExamination = @IndicationOfCompletionOfExamination, NumberOfYearsForSavingTheOriginalDocumentData = @NumberOfYearsForSavingTheOriginalDocumentData, ExpiryDate = @ExpiryDate, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ApplicationProcedureType = @ApplicationProcedureType";
            string delete = "DELETE FROM t_VNACC_Category_ApplicationProcedureType WHERE ApplicationProcedureType = @ApplicationProcedureType";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ApplicationProcedureType", SqlDbType.VarChar, "ApplicationProcedureType", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ApplicationProcedureName", SqlDbType.NVarChar, "ApplicationProcedureName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Section", SqlDbType.Int, "Section", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfGeneralDepartmentOfCustoms", SqlDbType.Int, "IndicationOfGeneralDepartmentOfCustoms", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfDisapproval", SqlDbType.Int, "IndicationOfDisapproval", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfCancellation", SqlDbType.Int, "IndicationOfCancellation", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfCompletionOfExamination", SqlDbType.Int, "IndicationOfCompletionOfExamination", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfYearsForSavingTheOriginalDocumentData", SqlDbType.Int, "NumberOfYearsForSavingTheOriginalDocumentData", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExpiryDate", SqlDbType.DateTime, "ExpiryDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ApplicationProcedureType", SqlDbType.VarChar, "ApplicationProcedureType", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ApplicationProcedureName", SqlDbType.NVarChar, "ApplicationProcedureName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Section", SqlDbType.Int, "Section", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfGeneralDepartmentOfCustoms", SqlDbType.Int, "IndicationOfGeneralDepartmentOfCustoms", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfDisapproval", SqlDbType.Int, "IndicationOfDisapproval", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfCancellation", SqlDbType.Int, "IndicationOfCancellation", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfCompletionOfExamination", SqlDbType.Int, "IndicationOfCompletionOfExamination", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfYearsForSavingTheOriginalDocumentData", SqlDbType.Int, "NumberOfYearsForSavingTheOriginalDocumentData", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExpiryDate", SqlDbType.DateTime, "ExpiryDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ApplicationProcedureType", SqlDbType.VarChar, "ApplicationProcedureType", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACC_Category_ApplicationProcedureType Load(string applicationProcedureType)
		{
			const string spName = "[dbo].[p_VNACC_Category_ApplicationProcedureType_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ApplicationProcedureType", SqlDbType.VarChar, applicationProcedureType);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACC_Category_ApplicationProcedureType> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACC_Category_ApplicationProcedureType> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACC_Category_ApplicationProcedureType> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertVNACC_Category_ApplicationProcedureType(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string applicationProcedureName, int section, int indicationOfGeneralDepartmentOfCustoms, int indicationOfDisapproval, int indicationOfCancellation, int indicationOfCompletionOfExamination, int numberOfYearsForSavingTheOriginalDocumentData, DateTime expiryDate, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_ApplicationProcedureType entity = new VNACC_Category_ApplicationProcedureType();	
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.ApplicationProcedureName = applicationProcedureName;
			entity.Section = section;
			entity.IndicationOfGeneralDepartmentOfCustoms = indicationOfGeneralDepartmentOfCustoms;
			entity.IndicationOfDisapproval = indicationOfDisapproval;
			entity.IndicationOfCancellation = indicationOfCancellation;
			entity.IndicationOfCompletionOfExamination = indicationOfCompletionOfExamination;
			entity.NumberOfYearsForSavingTheOriginalDocumentData = numberOfYearsForSavingTheOriginalDocumentData;
			entity.ExpiryDate = expiryDate;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@ApplicationProcedureType", SqlDbType.VarChar, ApplicationProcedureType);
			db.AddInParameter(dbCommand, "@ApplicationProcedureName", SqlDbType.NVarChar, ApplicationProcedureName);
			db.AddInParameter(dbCommand, "@Section", SqlDbType.Int, Section);
			db.AddInParameter(dbCommand, "@IndicationOfGeneralDepartmentOfCustoms", SqlDbType.Int, IndicationOfGeneralDepartmentOfCustoms);
			db.AddInParameter(dbCommand, "@IndicationOfDisapproval", SqlDbType.Int, IndicationOfDisapproval);
			db.AddInParameter(dbCommand, "@IndicationOfCancellation", SqlDbType.Int, IndicationOfCancellation);
			db.AddInParameter(dbCommand, "@IndicationOfCompletionOfExamination", SqlDbType.Int, IndicationOfCompletionOfExamination);
			db.AddInParameter(dbCommand, "@NumberOfYearsForSavingTheOriginalDocumentData", SqlDbType.Int, NumberOfYearsForSavingTheOriginalDocumentData);
			db.AddInParameter(dbCommand, "@ExpiryDate", SqlDbType.DateTime, ExpiryDate.Year <= 1753 ? DBNull.Value : (object) ExpiryDate);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACC_Category_ApplicationProcedureType> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_ApplicationProcedureType item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACC_Category_ApplicationProcedureType(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string applicationProcedureType, string applicationProcedureName, int section, int indicationOfGeneralDepartmentOfCustoms, int indicationOfDisapproval, int indicationOfCancellation, int indicationOfCompletionOfExamination, int numberOfYearsForSavingTheOriginalDocumentData, DateTime expiryDate, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_ApplicationProcedureType entity = new VNACC_Category_ApplicationProcedureType();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.ApplicationProcedureType = applicationProcedureType;
			entity.ApplicationProcedureName = applicationProcedureName;
			entity.Section = section;
			entity.IndicationOfGeneralDepartmentOfCustoms = indicationOfGeneralDepartmentOfCustoms;
			entity.IndicationOfDisapproval = indicationOfDisapproval;
			entity.IndicationOfCancellation = indicationOfCancellation;
			entity.IndicationOfCompletionOfExamination = indicationOfCompletionOfExamination;
			entity.NumberOfYearsForSavingTheOriginalDocumentData = numberOfYearsForSavingTheOriginalDocumentData;
			entity.ExpiryDate = expiryDate;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACC_Category_ApplicationProcedureType_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@ApplicationProcedureType", SqlDbType.VarChar, ApplicationProcedureType);
			db.AddInParameter(dbCommand, "@ApplicationProcedureName", SqlDbType.NVarChar, ApplicationProcedureName);
			db.AddInParameter(dbCommand, "@Section", SqlDbType.Int, Section);
			db.AddInParameter(dbCommand, "@IndicationOfGeneralDepartmentOfCustoms", SqlDbType.Int, IndicationOfGeneralDepartmentOfCustoms);
			db.AddInParameter(dbCommand, "@IndicationOfDisapproval", SqlDbType.Int, IndicationOfDisapproval);
			db.AddInParameter(dbCommand, "@IndicationOfCancellation", SqlDbType.Int, IndicationOfCancellation);
			db.AddInParameter(dbCommand, "@IndicationOfCompletionOfExamination", SqlDbType.Int, IndicationOfCompletionOfExamination);
			db.AddInParameter(dbCommand, "@NumberOfYearsForSavingTheOriginalDocumentData", SqlDbType.Int, NumberOfYearsForSavingTheOriginalDocumentData);
			db.AddInParameter(dbCommand, "@ExpiryDate", SqlDbType.DateTime, ExpiryDate.Year <= 1753 ? DBNull.Value : (object) ExpiryDate);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACC_Category_ApplicationProcedureType> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_ApplicationProcedureType item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACC_Category_ApplicationProcedureType(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string applicationProcedureType, string applicationProcedureName, int section, int indicationOfGeneralDepartmentOfCustoms, int indicationOfDisapproval, int indicationOfCancellation, int indicationOfCompletionOfExamination, int numberOfYearsForSavingTheOriginalDocumentData, DateTime expiryDate, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_ApplicationProcedureType entity = new VNACC_Category_ApplicationProcedureType();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.ApplicationProcedureType = applicationProcedureType;
			entity.ApplicationProcedureName = applicationProcedureName;
			entity.Section = section;
			entity.IndicationOfGeneralDepartmentOfCustoms = indicationOfGeneralDepartmentOfCustoms;
			entity.IndicationOfDisapproval = indicationOfDisapproval;
			entity.IndicationOfCancellation = indicationOfCancellation;
			entity.IndicationOfCompletionOfExamination = indicationOfCompletionOfExamination;
			entity.NumberOfYearsForSavingTheOriginalDocumentData = numberOfYearsForSavingTheOriginalDocumentData;
			entity.ExpiryDate = expiryDate;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_ApplicationProcedureType_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@ApplicationProcedureType", SqlDbType.VarChar, ApplicationProcedureType);
			db.AddInParameter(dbCommand, "@ApplicationProcedureName", SqlDbType.NVarChar, ApplicationProcedureName);
			db.AddInParameter(dbCommand, "@Section", SqlDbType.Int, Section);
			db.AddInParameter(dbCommand, "@IndicationOfGeneralDepartmentOfCustoms", SqlDbType.Int, IndicationOfGeneralDepartmentOfCustoms);
			db.AddInParameter(dbCommand, "@IndicationOfDisapproval", SqlDbType.Int, IndicationOfDisapproval);
			db.AddInParameter(dbCommand, "@IndicationOfCancellation", SqlDbType.Int, IndicationOfCancellation);
			db.AddInParameter(dbCommand, "@IndicationOfCompletionOfExamination", SqlDbType.Int, IndicationOfCompletionOfExamination);
			db.AddInParameter(dbCommand, "@NumberOfYearsForSavingTheOriginalDocumentData", SqlDbType.Int, NumberOfYearsForSavingTheOriginalDocumentData);
			db.AddInParameter(dbCommand, "@ExpiryDate", SqlDbType.DateTime, ExpiryDate.Year <= 1753 ? DBNull.Value : (object) ExpiryDate);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACC_Category_ApplicationProcedureType> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_ApplicationProcedureType item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACC_Category_ApplicationProcedureType(string applicationProcedureType)
		{
			VNACC_Category_ApplicationProcedureType entity = new VNACC_Category_ApplicationProcedureType();
			entity.ApplicationProcedureType = applicationProcedureType;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ApplicationProcedureType", SqlDbType.VarChar, ApplicationProcedureType);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACC_Category_ApplicationProcedureType> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_ApplicationProcedureType item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}