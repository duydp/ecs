﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Infragistics.Excel;
using Janus.Windows.GridEX.Export;

namespace Company.Interface.SXXK.BangKe
{
    public partial class GhiChu_DinhMuc_Form : BaseForm
    {
        public DataSet ds = new DataSet();
        public GhiChu_DinhMuc_Form()
        {
            InitializeComponent();
        }

        private void SanPhamChuaCoDMForm_Load(object sender, EventArgs e)
        {
            this.Text = "Danh sách ghi chú của định mức sản phẩm";
            dgList.HideColumnsWhenGrouped = InheritableBoolean.True;
            this.dgList.DataSource = ds.Tables[0];
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            //if(e.Row.RowType == RowType.Record)    
            //    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "GhiChuDinhMuc_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xlsx";
                sfNPL.Filter = "Excel files| *.xlsx";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
            //Workbook wb = new Workbook();
            //Worksheet ws = wb.Worksheets.Add("DinhMuc");
            //ws.Columns[0].Width = 5000;
            //ws.Columns[1].Width = 10000;
            
            //WorksheetRowCollection wsrc = ws.Rows;
            //WorksheetRow wsr0 = ws.Rows[0];
            //wsr0.Cells[0].Value = "Mã NPL";
            //wsr0.Cells[1].Value = "Ghi Chú";
            
            //DataTable table=this.ds.Tables[0];
   
            //for (int i = 0; i < table.Rows.Count; i++)
            //{
            //    WorksheetRow wsr = ws.Rows[i + 1];
            //    wsr.Cells[0].Value = table.Rows[i]["MaNguyenPhuLieu"].ToString();
            //    wsr.Cells[1].Value = table.Rows[i]["GhiChu"].ToString();
            //}
            //DialogResult rs= saveFileDialog1.ShowDialog(this);
            //if (rs == DialogResult.OK)
            //{
            //    string fileName = saveFileDialog1.FileName;
            //    wb.Save(fileName);
            //}
        }
    }
}