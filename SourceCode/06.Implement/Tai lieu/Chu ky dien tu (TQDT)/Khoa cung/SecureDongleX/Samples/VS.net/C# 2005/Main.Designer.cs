namespace sample
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           this.lstOutput = new System.Windows.Forms.ListBox();
           this.label1 = new System.Windows.Forms.Label();
           this.btnWrite = new System.Windows.Forms.Button();
           this.btnRead = new System.Windows.Forms.Button();
           this.btnTranslate = new System.Windows.Forms.Button();
           this.btnEncrypt = new System.Windows.Forms.Button();
           this.btnDecrypt = new System.Windows.Forms.Button();
           this.btnWriteMap = new System.Windows.Forms.Button();
           this.btnReadMap = new System.Windows.Forms.Button();
           this.SuspendLayout();
           // 
           // lstOutput
           // 
           this.lstOutput.FormattingEnabled = true;
           this.lstOutput.HorizontalScrollbar = true;
           this.lstOutput.Location = new System.Drawing.Point(12, 79);
           this.lstOutput.Name = "lstOutput";
           this.lstOutput.Size = new System.Drawing.Size(484, 251);
           this.lstOutput.TabIndex = 0;
           // 
           // label1
           // 
           this.label1.AutoSize = true;
           this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
           this.label1.Location = new System.Drawing.Point(51, 20);
           this.label1.Name = "label1";
           this.label1.Size = new System.Drawing.Size(496, 37);
           this.label1.TabIndex = 1;
           this.label1.Text = "SecureDongle X C# 2005 Sample";
           // 
           // btnWrite
           // 
           this.btnWrite.Location = new System.Drawing.Point(502, 79);
           this.btnWrite.Name = "btnWrite";
           this.btnWrite.Size = new System.Drawing.Size(87, 30);
           this.btnWrite.TabIndex = 3;
           this.btnWrite.Text = "Write Data";
           this.btnWrite.UseVisualStyleBackColor = true;
           this.btnWrite.Click += new System.EventHandler(this.btnWrite_Click);
           // 
           // btnRead
           // 
           this.btnRead.Location = new System.Drawing.Point(502, 115);
           this.btnRead.Name = "btnRead";
           this.btnRead.Size = new System.Drawing.Size(87, 30);
           this.btnRead.TabIndex = 4;
           this.btnRead.Text = "Read Data";
           this.btnRead.UseVisualStyleBackColor = true;
           this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
           // 
           // btnTranslate
           // 
           this.btnTranslate.Location = new System.Drawing.Point(502, 151);
           this.btnTranslate.Name = "btnTranslate";
           this.btnTranslate.Size = new System.Drawing.Size(87, 30);
           this.btnTranslate.TabIndex = 5;
           this.btnTranslate.Text = "Transform";
           this.btnTranslate.UseVisualStyleBackColor = true;
           this.btnTranslate.Click += new System.EventHandler(this.btnTransform_Click);
           // 
           // btnEncrypt
           // 
           this.btnEncrypt.Location = new System.Drawing.Point(502, 187);
           this.btnEncrypt.Name = "btnEncrypt";
           this.btnEncrypt.Size = new System.Drawing.Size(87, 30);
           this.btnEncrypt.TabIndex = 6;
           this.btnEncrypt.Text = "Encrypt Data";
           this.btnEncrypt.UseVisualStyleBackColor = true;
           this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
           // 
           // btnDecrypt
           // 
           this.btnDecrypt.Location = new System.Drawing.Point(502, 223);
           this.btnDecrypt.Name = "btnDecrypt";
           this.btnDecrypt.Size = new System.Drawing.Size(87, 30);
           this.btnDecrypt.TabIndex = 7;
           this.btnDecrypt.Text = "Decrypt Data";
           this.btnDecrypt.UseVisualStyleBackColor = true;
           this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
           // 
           // btnWriteMap
           // 
           this.btnWriteMap.Location = new System.Drawing.Point(502, 259);
           this.btnWriteMap.Name = "btnWriteMap";
           this.btnWriteMap.Size = new System.Drawing.Size(87, 30);
           this.btnWriteMap.TabIndex = 8;
           this.btnWriteMap.Text = "Write with Map";
           this.btnWriteMap.UseVisualStyleBackColor = true;
           this.btnWriteMap.Click += new System.EventHandler(this.btnWriteMap_Click);
           // 
           // btnReadMap
           // 
           this.btnReadMap.Location = new System.Drawing.Point(502, 295);
           this.btnReadMap.Name = "btnReadMap";
           this.btnReadMap.Size = new System.Drawing.Size(87, 30);
           this.btnReadMap.TabIndex = 9;
           this.btnReadMap.Text = "Read with Map";
           this.btnReadMap.UseVisualStyleBackColor = true;
           this.btnReadMap.Click += new System.EventHandler(this.btnReadMap_Click);
           // 
           // frmMain
           // 
           this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
           this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
           this.ClientSize = new System.Drawing.Size(601, 342);
           this.Controls.Add(this.btnReadMap);
           this.Controls.Add(this.btnWriteMap);
           this.Controls.Add(this.btnDecrypt);
           this.Controls.Add(this.btnEncrypt);
           this.Controls.Add(this.btnTranslate);
           this.Controls.Add(this.btnRead);
           this.Controls.Add(this.btnWrite);
           this.Controls.Add(this.label1);
           this.Controls.Add(this.lstOutput);
           this.Name = "frmMain";
           this.Text = "SDX C# 2005 Sample";
           this.ResumeLayout(false);
           this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstOutput;
       private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnWrite;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Button btnTranslate;
        private System.Windows.Forms.Button btnEncrypt;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.Button btnWriteMap;
        private System.Windows.Forms.Button btnReadMap;
    }
}

