﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class CommercialInvoiceItem
    {
        /// <summary>
        /// Số thứ tự
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }
        /// <summary>
        /// Đơn giá n..16,2
        /// </summary>
        [XmlElement("unitPrice")]
        public string UnitPrice { get; set; }
        /// <summary>
        /// Trị giá n..16
        /// </summary>
        [XmlElement("statisticalValue")]
        public string StatisticalValue { get; set; }
        /// <summary>
        /// Xuất Xứ
        /// </summary>
        [XmlElement("Origin")]
        public Origin Origin { get; set; }
        /// <summary>
        /// Chi Tiết hàng, thuê
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }
        /// <summary>
        /// Các giá trị điều chỉnh
        /// </summary>
        [XmlElement("ValuationAdjustment")]
        public ValuationAdjustment ValuationAdjustment { get; set; }
        /// <summary>
        /// Thông tin bổ sung
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
    }
}
