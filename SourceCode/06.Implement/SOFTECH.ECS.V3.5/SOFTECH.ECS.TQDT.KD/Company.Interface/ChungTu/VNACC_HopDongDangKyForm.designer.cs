﻿namespace Company.Interface
{
    partial class VNACC_HopDongDangKyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_HopDongDangKyForm));
            Janus.Windows.GridEX.GridEXLayout gridEX1_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTieuDe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.btnXemFile = new Janus.Windows.EditControls.UIButton();
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.btnKetQuaXuLy = new Janus.Windows.EditControls.UIButton();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lbTrangThai = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnXoaChungTu = new Janus.Windows.EditControls.UIButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblLuuY = new DevExpress.XtraEditors.LabelControl();
            this.lblTongDungLuong = new DevExpress.XtraEditors.LabelControl();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageHYS = new Janus.Windows.UI.Tab.UITabPage();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.gridEX1 = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dtNgayHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label16 = new System.Windows.Forms.Label();
            this.dtThoiHanThanhToan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label14 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtGhiChuKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTongTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiKhaiBao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaNguoiKhaiBao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucNhomXuLyHS_HYS = new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy();
            this.ucCoQuanHaiQuan_HYS = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uiTabPageFile = new Janus.Windows.UI.Tab.UITabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLinkExcelTemplate = new System.Windows.Forms.LinkLabel();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNhomXuLyHS = new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy();
            this.ucCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdSuaChungTu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaChungTu");
            this.cmdPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoi");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoi");
            this.cmdSuaChungTu = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaChungTu");
            this.cmdTTHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdTTHQ");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPageHYS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.uiTabPageFile.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 613), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 613);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 589);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 589);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.panel2);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(792, 613);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(119, 174);
            this.txtGhiChu.MaxLength = 996;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGhiChu.Size = new System.Drawing.Size(539, 69);
            this.txtGhiChu.TabIndex = 3;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtGhiChu.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Số tờ khai";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Ghi chú";
            // 
            // txtTieuDe
            // 
            this.txtTieuDe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTieuDe.Location = new System.Drawing.Point(119, 97);
            this.txtTieuDe.MaxLength = 210;
            this.txtTieuDe.Multiline = true;
            this.txtTieuDe.Name = "txtTieuDe";
            this.txtTieuDe.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTieuDe.Size = new System.Drawing.Size(539, 44);
            this.txtTieuDe.TabIndex = 1;
            this.txtTieuDe.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTieuDe.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTieuDe.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(17, 113);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Tiêu đề";
            // 
            // dgList
            // 
            this.dgList.AlternatingColors = true;
            this.dgList.AutoEdit = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(790, 440);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(599, 31);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(88, 23);
            this.btnXoa.TabIndex = 1;
            this.btnXoa.Text = "Xoá file";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(695, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(86, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddNew.Icon")));
            this.btnAddNew.Location = new System.Drawing.Point(484, 31);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(109, 23);
            this.btnAddNew.TabIndex = 0;
            this.btnAddNew.Text = "Thêm file";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddNew.VisualStyleManager = this.vsmMain;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(601, 6);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(88, 23);
            this.btnGhi.TabIndex = 1;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // btnXemFile
            // 
            this.btnXemFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXemFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemFile.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemFile.Icon")));
            this.btnXemFile.Location = new System.Drawing.Point(693, 31);
            this.btnXemFile.Name = "btnXemFile";
            this.btnXemFile.Size = new System.Drawing.Size(86, 23);
            this.btnXemFile.TabIndex = 2;
            this.btnXemFile.Text = "Xem file";
            this.btnXemFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemFile.VisualStyleManager = this.vsmMain;
            this.btnXemFile.Click += new System.EventHandler(this.btnXemFile_Click);
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXuLy);
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.lbTrangThai);
            this.grpTiepNhan.Controls.Add(this.label10);
            this.grpTiepNhan.Controls.Add(this.label11);
            this.grpTiepNhan.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(0, 0);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(792, 51);
            this.grpTiepNhan.TabIndex = 0;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grpTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // btnKetQuaXuLy
            // 
            this.btnKetQuaXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKetQuaXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXuLy.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKetQuaXuLy.Icon")));
            this.btnKetQuaXuLy.Location = new System.Drawing.Point(590, 17);
            this.btnKetQuaXuLy.Name = "btnKetQuaXuLy";
            this.btnKetQuaXuLy.Size = new System.Drawing.Size(109, 23);
            this.btnKetQuaXuLy.TabIndex = 2;
            this.btnKetQuaXuLy.Text = "Kết quả xử lý";
            this.btnKetQuaXuLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKetQuaXuLy.Click += new System.EventHandler(this.btnKetQuaXuLy_Click);
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(350, 17);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(94, 21);
            this.ccNgayTiepNhan.TabIndex = 1;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(121, 17);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(138, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // lbTrangThai
            // 
            this.lbTrangThai.AutoSize = true;
            this.lbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTrangThai.Location = new System.Drawing.Point(450, 22);
            this.lbTrangThai.Name = "lbTrangThai";
            this.lbTrangThai.Size = new System.Drawing.Size(56, 13);
            this.lbTrangThai.TabIndex = 2;
            this.lbTrangThai.Text = "Trạng thái";
            this.lbTrangThai.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(265, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Ngày tiếp nhận";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(21, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số tiếp nhận";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnXoaChungTu
            // 
            this.btnXoaChungTu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoaChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaChungTu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoaChungTu.Icon")));
            this.btnXoaChungTu.Location = new System.Drawing.Point(486, 6);
            this.btnXoaChungTu.Name = "btnXoaChungTu";
            this.btnXoaChungTu.Size = new System.Drawing.Size(109, 23);
            this.btnXoaChungTu.TabIndex = 0;
            this.btnXoaChungTu.Text = "Xoá chứng từ";
            this.btnXoaChungTu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoaChungTu.Click += new System.EventHandler(this.btnXoaChungTu_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(5, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(97, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tổng dung lượng:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.labelControl2.Location = new System.Drawing.Point(5, 25);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(293, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Lưu ý: Tổng dung lượng các file đính kèm không quá ";
            // 
            // lblLuuY
            // 
            this.lblLuuY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLuuY.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblLuuY.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblLuuY.Location = new System.Drawing.Point(303, 25);
            this.lblLuuY.Name = "lblLuuY";
            this.lblLuuY.Size = new System.Drawing.Size(27, 13);
            this.lblLuuY.TabIndex = 4;
            this.lblLuuY.Text = "2 MB";
            // 
            // lblTongDungLuong
            // 
            this.lblTongDungLuong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTongDungLuong.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblTongDungLuong.Location = new System.Drawing.Point(117, 6);
            this.lblTongDungLuong.Name = "lblTongDungLuong";
            this.lblTongDungLuong.Size = new System.Drawing.Size(70, 13);
            this.lblTongDungLuong.TabIndex = 1;
            this.lblTongDungLuong.Text = "0 MB (0 Bytes)";
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 51);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(792, 528);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageHYS,
            this.uiTabPageFile});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            // 
            // uiTabPageHYS
            // 
            this.uiTabPageHYS.AutoScroll = true;
            this.uiTabPageHYS.Controls.Add(this.btnAdd);
            this.uiTabPageHYS.Controls.Add(this.gridEX1);
            this.uiTabPageHYS.Controls.Add(this.uiGroupBox3);
            this.uiTabPageHYS.Controls.Add(this.uiGroupBox4);
            this.uiTabPageHYS.Controls.Add(this.uiGroupBox2);
            this.uiTabPageHYS.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageHYS.Name = "uiTabPageHYS";
            this.uiTabPageHYS.Size = new System.Drawing.Size(790, 506);
            this.uiTabPageHYS.TabStop = true;
            this.uiTabPageHYS.Text = "Thông tin chung";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAdd.Icon")));
            this.btnAdd.Location = new System.Drawing.Point(8, 305);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(107, 28);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Thêm tờ khai";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.VisualStyleManager = this.vsmMain;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // gridEX1
            // 
            this.gridEX1.AlternatingColors = true;
            this.gridEX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridEX1.AutoEdit = true;
            this.gridEX1.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridEX1.ColumnAutoResize = true;
            gridEX1_DesignTimeLayout.LayoutString = resources.GetString("gridEX1_DesignTimeLayout.LayoutString");
            this.gridEX1.DesignTimeLayout = gridEX1_DesignTimeLayout;
            this.gridEX1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX1.FrozenColumns = 3;
            this.gridEX1.GroupByBoxVisible = false;
            this.gridEX1.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX1.ImageList = this.ImageList1;
            this.gridEX1.Location = new System.Drawing.Point(8, 339);
            this.gridEX1.Name = "gridEX1";
            this.gridEX1.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX1.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridEX1.Size = new System.Drawing.Size(782, 167);
            this.gridEX1.TabIndex = 15;
            this.gridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.dtNgayHopDong);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.dtThoiHanThanhToan);
            this.uiGroupBox3.Controls.Add(this.label14);
            this.uiGroupBox3.Controls.Add(this.label21);
            this.uiGroupBox3.Controls.Add(this.txtGhiChuKhac);
            this.uiGroupBox3.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox3.Controls.Add(this.txtTongTriGia);
            this.uiGroupBox3.Controls.Add(this.label20);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(8, 172);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(779, 127);
            this.uiGroupBox3.TabIndex = 14;
            this.uiGroupBox3.Text = "Thông tin hợp đồng";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Số hợp đồng";
            // 
            // dtNgayHopDong
            // 
            // 
            // 
            // 
            this.dtNgayHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtNgayHopDong.DropDownCalendar.Name = "";
            this.dtNgayHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayHopDong.IsNullDate = true;
            this.dtNgayHopDong.Location = new System.Drawing.Point(112, 59);
            this.dtNgayHopDong.Name = "dtNgayHopDong";
            this.dtNgayHopDong.Nullable = true;
            this.dtNgayHopDong.NullButtonText = "Xóa";
            this.dtNgayHopDong.ShowNullButton = true;
            this.dtNgayHopDong.Size = new System.Drawing.Size(94, 21);
            this.dtNgayHopDong.TabIndex = 2;
            this.dtNgayHopDong.TodayButtonText = "Hôm nay";
            this.dtNgayHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(398, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "Thời hạn thanh toán";
            // 
            // dtThoiHanThanhToan
            // 
            // 
            // 
            // 
            this.dtThoiHanThanhToan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtThoiHanThanhToan.DropDownCalendar.Name = "";
            this.dtThoiHanThanhToan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtThoiHanThanhToan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtThoiHanThanhToan.IsNullDate = true;
            this.dtThoiHanThanhToan.Location = new System.Drawing.Point(513, 28);
            this.dtThoiHanThanhToan.Name = "dtThoiHanThanhToan";
            this.dtThoiHanThanhToan.Nullable = true;
            this.dtThoiHanThanhToan.NullButtonText = "Xóa";
            this.dtThoiHanThanhToan.ShowNullButton = true;
            this.dtThoiHanThanhToan.Size = new System.Drawing.Size(94, 21);
            this.dtThoiHanThanhToan.TabIndex = 6;
            this.dtThoiHanThanhToan.TodayButtonText = "Hôm nay";
            this.dtThoiHanThanhToan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtThoiHanThanhToan.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(17, 101);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Ghi chú";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(17, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "Ngày hợp đồng";
            // 
            // txtGhiChuKhac
            // 
            this.txtGhiChuKhac.Location = new System.Drawing.Point(112, 97);
            this.txtGhiChuKhac.Name = "txtGhiChuKhac";
            this.txtGhiChuKhac.Size = new System.Drawing.Size(650, 21);
            this.txtGhiChuKhac.TabIndex = 1;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(112, 20);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(280, 21);
            this.txtSoHopDong.TabIndex = 1;
            // 
            // txtTongTriGia
            // 
            this.txtTongTriGia.DecimalDigits = 0;
            this.txtTongTriGia.Location = new System.Drawing.Point(513, 59);
            this.txtTongTriGia.MaxLength = 13;
            this.txtTongTriGia.Name = "txtTongTriGia";
            this.txtTongTriGia.Size = new System.Drawing.Size(249, 21);
            this.txtTongTriGia.TabIndex = 3;
            this.txtTongTriGia.Text = "0";
            this.txtTongTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTriGia.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(441, 61);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 13);
            this.label20.TabIndex = 6;
            this.label20.Text = "Tổng trị giá";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtTenDN);
            this.uiGroupBox4.Controls.Add(this.txtTenNguoiKhaiBao);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.txtMaDN);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtMaNguoiKhaiBao);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(8, 90);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(779, 76);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Thông tin doanh nghiệp";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(112, 21);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(290, 21);
            this.txtTenDN.TabIndex = 0;
            // 
            // txtTenNguoiKhaiBao
            // 
            this.txtTenNguoiKhaiBao.Location = new System.Drawing.Point(508, 20);
            this.txtTenNguoiKhaiBao.Name = "txtTenNguoiKhaiBao";
            this.txtTenNguoiKhaiBao.Size = new System.Drawing.Size(255, 21);
            this.txtTenNguoiKhaiBao.TabIndex = 0;
            this.txtTenNguoiKhaiBao.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(408, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã người khai báo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Mã doanh nghiệp";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(408, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Tên người khai báo";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(112, 48);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(290, 21);
            this.txtMaDN.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Tên doanh nghiệp";
            // 
            // txtMaNguoiKhaiBao
            // 
            this.txtMaNguoiKhaiBao.Location = new System.Drawing.Point(508, 47);
            this.txtMaNguoiKhaiBao.Name = "txtMaNguoiKhaiBao";
            this.txtMaNguoiKhaiBao.Size = new System.Drawing.Size(255, 21);
            this.txtMaNguoiKhaiBao.TabIndex = 1;
            this.txtMaNguoiKhaiBao.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ucNhomXuLyHS_HYS);
            this.uiGroupBox2.Controls.Add(this.ucCoQuanHaiQuan_HYS);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(8, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(779, 76);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin Hải Quan";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // ucNhomXuLyHS_HYS
            // 
            this.ucNhomXuLyHS_HYS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A014;
            this.ucNhomXuLyHS_HYS.Code = "";
            this.ucNhomXuLyHS_HYS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucNhomXuLyHS_HYS.CustomsCode = null;
            this.ucNhomXuLyHS_HYS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucNhomXuLyHS_HYS.IsOnlyWarning = true;
            this.ucNhomXuLyHS_HYS.IsValidate = true;
            this.ucNhomXuLyHS_HYS.Location = new System.Drawing.Point(179, 45);
            this.ucNhomXuLyHS_HYS.Name = "ucNhomXuLyHS_HYS";
            this.ucNhomXuLyHS_HYS.Name_VN = "";
            this.ucNhomXuLyHS_HYS.SetValidate = false;
            this.ucNhomXuLyHS_HYS.ShowColumnCode = true;
            this.ucNhomXuLyHS_HYS.ShowColumnName = true;
            this.ucNhomXuLyHS_HYS.Size = new System.Drawing.Size(165, 21);
            this.ucNhomXuLyHS_HYS.TabIndex = 9;
            this.ucNhomXuLyHS_HYS.TagName = "";
            this.ucNhomXuLyHS_HYS.WhereCondition = "";
            // 
            // ucCoQuanHaiQuan_HYS
            // 
            this.ucCoQuanHaiQuan_HYS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ucCoQuanHaiQuan_HYS.Code = "";
            this.ucCoQuanHaiQuan_HYS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCoQuanHaiQuan_HYS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCoQuanHaiQuan_HYS.IsOnlyWarning = true;
            this.ucCoQuanHaiQuan_HYS.IsValidate = true;
            this.ucCoQuanHaiQuan_HYS.Location = new System.Drawing.Point(179, 14);
            this.ucCoQuanHaiQuan_HYS.Name = "ucCoQuanHaiQuan_HYS";
            this.ucCoQuanHaiQuan_HYS.Name_VN = "";
            this.ucCoQuanHaiQuan_HYS.SetValidate = false;
            this.ucCoQuanHaiQuan_HYS.ShowColumnCode = true;
            this.ucCoQuanHaiQuan_HYS.ShowColumnName = true;
            this.ucCoQuanHaiQuan_HYS.Size = new System.Drawing.Size(356, 26);
            this.ucCoQuanHaiQuan_HYS.TabIndex = 0;
            this.ucCoQuanHaiQuan_HYS.TagCode = "";
            this.ucCoQuanHaiQuan_HYS.TagName = "";
            this.ucCoQuanHaiQuan_HYS.Where = null;
            this.ucCoQuanHaiQuan_HYS.WhereCondition = "";
            this.ucCoQuanHaiQuan_HYS.Leave += new System.EventHandler(this.ctrCoQuanHaiQuan_Leave);
            this.ucCoQuanHaiQuan_HYS.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EnterHandle(this.ucCategory_OnEnter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nhóm xử lý hồ sơ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Cơ quan Hải quan";
            // 
            // uiTabPageFile
            // 
            this.uiTabPageFile.Controls.Add(this.dgList);
            this.uiTabPageFile.Controls.Add(this.panel1);
            this.uiTabPageFile.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageFile.Name = "uiTabPageFile";
            this.uiTabPageFile.Size = new System.Drawing.Size(790, 506);
            this.uiTabPageFile.TabStop = true;
            this.uiTabPageFile.Text = "Danh sách tệp tin đính kèm";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblLinkExcelTemplate);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Controls.Add(this.lblTongDungLuong);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.lblLuuY);
            this.panel1.Controls.Add(this.btnXemFile);
            this.panel1.Controls.Add(this.btnAddNew);
            this.panel1.Controls.Add(this.btnXoa);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 440);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(790, 66);
            this.panel1.TabIndex = 1;
            // 
            // lblLinkExcelTemplate
            // 
            this.lblLinkExcelTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLinkExcelTemplate.AutoSize = true;
            this.lblLinkExcelTemplate.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkExcelTemplate.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkExcelTemplate.Location = new System.Drawing.Point(487, 3);
            this.lblLinkExcelTemplate.Name = "lblLinkExcelTemplate";
            this.lblLinkExcelTemplate.Size = new System.Drawing.Size(290, 15);
            this.lblLinkExcelTemplate.TabIndex = 29;
            this.lblLinkExcelTemplate.TabStop = true;
            this.lblLinkExcelTemplate.Text = "Mở tệp tin mẫu Excel : Danh sách container đính kèm";
            this.lblLinkExcelTemplate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLinkExcelTemplate_LinkClicked);
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 0;
            this.txtSoToKhai.Location = new System.Drawing.Point(119, 147);
            this.txtSoToKhai.MaxLength = 12;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(139, 20);
            this.txtSoToKhai.TabIndex = 2;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrNhomXuLyHS);
            this.uiGroupBox1.Controls.Add(this.ucCoQuanHaiQuan);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(8, 8);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(690, 83);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Kiểm soát nơi đến";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ctrNhomXuLyHS
            // 
            this.ctrNhomXuLyHS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A014;
            this.ctrNhomXuLyHS.Code = "";
            this.ctrNhomXuLyHS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNhomXuLyHS.CustomsCode = null;
            this.ctrNhomXuLyHS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNhomXuLyHS.IsOnlyWarning = true;
            this.ctrNhomXuLyHS.IsValidate = true;
            this.ctrNhomXuLyHS.Location = new System.Drawing.Point(111, 50);
            this.ctrNhomXuLyHS.Name = "ctrNhomXuLyHS";
            this.ctrNhomXuLyHS.Name_VN = "";
            this.ctrNhomXuLyHS.SetValidate = false;
            this.ctrNhomXuLyHS.ShowColumnCode = true;
            this.ctrNhomXuLyHS.ShowColumnName = false;
            this.ctrNhomXuLyHS.Size = new System.Drawing.Size(165, 21);
            this.ctrNhomXuLyHS.TabIndex = 9;
            this.ctrNhomXuLyHS.TagName = "";
            this.ctrNhomXuLyHS.WhereCondition = "";
            // 
            // ucCoQuanHaiQuan
            // 
            this.ucCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ucCoQuanHaiQuan.Code = "";
            this.ucCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCoQuanHaiQuan.IsOnlyWarning = true;
            this.ucCoQuanHaiQuan.IsValidate = true;
            this.ucCoQuanHaiQuan.Location = new System.Drawing.Point(111, 18);
            this.ucCoQuanHaiQuan.Name = "ucCoQuanHaiQuan";
            this.ucCoQuanHaiQuan.Name_VN = "";
            this.ucCoQuanHaiQuan.SetValidate = false;
            this.ucCoQuanHaiQuan.ShowColumnCode = true;
            this.ucCoQuanHaiQuan.ShowColumnName = true;
            this.ucCoQuanHaiQuan.Size = new System.Drawing.Size(324, 26);
            this.ucCoQuanHaiQuan.TabIndex = 0;
            this.ucCoQuanHaiQuan.TagCode = "";
            this.ucCoQuanHaiQuan.TagName = "";
            this.ucCoQuanHaiQuan.Where = null;
            this.ucCoQuanHaiQuan.WhereCondition = "";
            this.ucCoQuanHaiQuan.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EnterHandle(this.ucCategory_OnEnter);
            this.ucCoQuanHaiQuan.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCoQuanHaiQuan_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nhóm xử lý hồ sơ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Cơ quan Hải quan";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnGhi);
            this.panel2.Controls.Add(this.btnXoaChungTu);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 579);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(792, 34);
            this.panel2.TabIndex = 1;
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao,
            this.cmdPhanHoi,
            this.cmdSuaChungTu,
            this.cmdTTHQ});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("64392c4c-08e0-49a5-ab35-32992c7e6756");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.TopRebar1;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao1,
            this.cmdSuaChungTu1,
            this.cmdPhanHoi1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(325, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdSuaChungTu1
            // 
            this.cmdSuaChungTu1.Key = "cmdSuaChungTu";
            this.cmdSuaChungTu1.Name = "cmdSuaChungTu1";
            this.cmdSuaChungTu1.Text = "Sửa hợp đồng";
            // 
            // cmdPhanHoi1
            // 
            this.cmdPhanHoi1.Key = "cmdPhanHoi";
            this.cmdPhanHoi1.Name = "cmdPhanHoi1";
            this.cmdPhanHoi1.Text = "Thông tin Hải Quan";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdPhanHoi
            // 
            this.cmdPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPhanHoi.Icon")));
            this.cmdPhanHoi.Key = "cmdPhanHoi";
            this.cmdPhanHoi.Name = "cmdPhanHoi";
            this.cmdPhanHoi.Text = "Thông tin phản hồi";
            // 
            // cmdSuaChungTu
            // 
            this.cmdSuaChungTu.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuaChungTu.Image")));
            this.cmdSuaChungTu.Key = "cmdSuaChungTu";
            this.cmdSuaChungTu.Name = "cmdSuaChungTu";
            this.cmdSuaChungTu.Text = "Sửa chứng từ";
            // 
            // cmdTTHQ
            // 
            this.cmdTTHQ.Key = "cmdTTHQ";
            this.cmdTTHQ.Name = "cmdTTHQ";
            this.cmdTTHQ.Text = "Thông tin Hải Quan";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(998, 28);
            // 
            // VNACC_HopDongDangKyForm
            // 
            this.AcceptButton = this.btnGhi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(998, 647);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_HopDongDangKyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin chứng từ kèm";
            this.Load += new System.EventHandler(this.VNACC_HopDongDangKyForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPageHYS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.uiTabPageFile.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtTieuDe;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnAddNew;
        private Janus.Windows.EditControls.UIButton btnXemFile;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label lbTrangThai;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Janus.Windows.EditControls.UIButton btnXoaChungTu;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblTongDungLuong;
        private DevExpress.XtraEditors.LabelControl lblLuuY;
        private Janus.Windows.EditControls.UIButton btnKetQuaXuLy;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageFile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageHYS;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCoQuanHaiQuan;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCoQuanHaiQuan_HYS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.CalendarCombo.CalendarCombo dtThoiHanThanhToan;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayHopDong;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanHoi;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy ctrNhomXuLyHS;
        private Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy ucNhomXuLyHS_HYS;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaChungTu;
        private System.Windows.Forms.LinkLabel lblLinkExcelTemplate;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanHoi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaChungTu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTTHQ;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGia;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiKhaiBao;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiKhaiBao;
        private Janus.Windows.GridEX.GridEX gridEX1;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuKhac;
        private System.Windows.Forms.Label label14;
    }
}
