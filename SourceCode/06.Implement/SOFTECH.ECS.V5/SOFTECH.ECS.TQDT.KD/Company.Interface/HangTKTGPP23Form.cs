﻿using System;
using System.Windows.Forms;
//using Company.KD.BLL.SXXK.ToKhai;
using System.Threading;
using System.Data;
using System.Configuration;
using Company.KD.BLL.KDT;
using Company.KD.BLL;
#if KD_V3 || KD_V4
using ToKhaiMauDich = Company.KD.BLL.KDT.ToKhaiMauDich;
#elif GC_V3 || GC_V4
using ToKhaiMauDich =Company.GC.BLL.KDT.ToKhaiMauDich;
#endif
namespace Company.Interface
{
    public partial class HangTKTGPP23Form : BaseForm
    {
        public decimal TyGiaTT;
        public ToKhaiMauDich TKMD;
        public ToKhaiTriGiaPP23 TKTG23 = new ToKhaiTriGiaPP23();
        public HangTKTGPP23Form()
        {
            InitializeComponent();
        }

        private void HangTKTGForm_Load(object sender, EventArgs e)
        {
            if (TKTG23.ID > 0)
            {
                txtDCCBaoHiem.Text = TKTG23.DieuChinhCongChiPhiBaoHiem.ToString();
                txtDCCKhoanKhac.Text = TKTG23.DieuChinhCongKhoanGiamGiaKhac.ToString();
                txtDCCSoLuong.Text = TKTG23.DieuChinhCongSoLuong.ToString();
                txtDCCVanTai.Text = TKTG23.DieuChinhCongChiPhiVanTai.ToString();
                txtDCTBaoHiem.Text = TKTG23.DieuChinhTruChiPhiBaoHiem.ToString();
                txtDCTKhoanKhac.Text = TKTG23.DieuChinhTruKhoanGiamGiaKhac.ToString();
                txtDCTSoLuong.Text = TKTG23.DieuChinhTruSoLuong.ToString();
                txtDCTVanTai.Text = TKTG23.DieuChinhTruChiPhiVanTai.ToString();
                txtDieuChinhCongCapDoTM.Text = TKTG23.DieuChinhCongThuongMai.ToString();
                txtDieuChinhTruCapDoTM.Text = TKTG23.DieuChinhTruCapDoThuongMai.ToString();
                txtGiaiTrinh.Text = TKTG23.GiaiTrinh;
                txtLyDo.Text = TKTG23.LyDo;
                txtSoToKhaiTT.Text = TKTG23.SoTKHangTT.ToString();
                txtSTTHang.Text = TKTG23.STTHang.ToString();
                txtSTTHangTT.Text = TKTG23.STTHangTT.ToString();
                txtTenHangHoa.Text = TKTG23.TenHang;
                txtTenHangTT.Text = TKTG23.TenHangTT;
                txtToSo.Text = TKTG23.ToSo.ToString();
                txtTriGiaHangTT.Text = TKTG23.TriGiaNguyenTeHangTT.ToString();
                txtTriGiaNTHang.Text = TKTG23.TriGiaNguyenTeHang.ToString();
                txtTriGiaTTHang.Text = TKTG23.TriGiaTTHang.ToString();
            }
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
            {
                btnGhi.Enabled = false;
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            TKTG23.DieuChinhCongChiPhiBaoHiem = Convert.ToDouble(txtDCCBaoHiem.Text);
            TKTG23.DieuChinhCongChiPhiVanTai = Convert.ToDouble(txtDCCVanTai.Text);
            TKTG23.DieuChinhCongKhoanGiamGiaKhac = Convert.ToDouble(txtDCCKhoanKhac.Text);
            TKTG23.DieuChinhCongSoLuong = Convert.ToDouble(txtDCCSoLuong.Text);
            TKTG23.DieuChinhCongThuongMai = Convert.ToDouble(txtDieuChinhCongCapDoTM.Text);
            TKTG23.DieuChinhTruCapDoThuongMai = Convert.ToDouble(txtDieuChinhTruCapDoTM.Text);
            TKTG23.DieuChinhTruChiPhiBaoHiem = Convert.ToDouble(txtDCTBaoHiem.Text);
            TKTG23.DieuChinhTruChiPhiVanTai = Convert.ToDouble(txtDCTVanTai.Text);
            TKTG23.DieuChinhTruKhoanGiamGiaKhac = Convert.ToDouble(txtDCTKhoanKhac.Text);
            TKTG23.DieuChinhTruSoLuong = Convert.ToDouble(txtDCTSoLuong.Text);
            TKTG23.GiaiTrinh = txtGiaiTrinh.Text.Trim();
            TKTG23.LyDo = txtLyDo.Text.Trim();
            TKTG23.MaHaiQuanHangTT = ctrDonViHaiQuan.Ma;
            TKTG23.MaLoaiHinhHangTT = ctrLoaiHinhTT.Ma;
            TKTG23.NgayDangKyHangTT = ccNgayDangKyTT.Value;
            TKTG23.NgayXuat = ccNgayXuat.Value;
            TKTG23.NgayXuatTT = ccNgayXuatTT.Value;
            TKTG23.SoTKHangTT = Convert.ToInt64(txtSoToKhaiTT.Text);
            TKTG23.STTHang = Convert.ToInt32(txtSTTHang.Text);
            TKTG23.STTHangTT = Convert.ToInt32(txtSTTHangTT.Text);
            TKTG23.TenHang = txtTenHangHoa.Text.Trim();
            TKTG23.TenHangTT = txtTenHangTT.Text.Trim();
            TKTG23.ToSo = Convert.ToInt32(txtToSo.Value);
            TKTG23.TriGiaNguyenTeHang = Convert.ToDouble(txtTriGiaNTHang.Text);
            TKTG23.TriGiaTTHang = Convert.ToDouble(txtTriGiaTTHang.Text);
            TKTG23.TriGiaNguyenTeHangTT = Convert.ToDouble(txtTriGiaHangTT.Text);
            try
            {
                TKTG23.TKMD_ID = TKMD.ID;
                if (TKTG23.ID == 0)
                    TKTG23.Insert();
                else
                    TKTG23.Update();
                MLMessages("Lưu thành công.","MSG_SAV02","", false);
            }
            catch (Exception exx)
            {
                ShowMessage(setText("Có lỗi khi cập nhật : ","Error: ") + exx.Message, false);
            }

        }

        private void txtTriGiaHangTT_Leave(object sender, EventArgs e)
        {
            double TriGiaNguyeTe = Convert.ToDouble(txtTriGiaHangTT.Text) + Convert.ToDouble(txtDieuChinhCongCapDoTM.Text) + Convert.ToDouble(txtDCCBaoHiem.Text)
                                    + Convert.ToDouble(txtDCCKhoanKhac.Text) + Convert.ToDouble(txtDCCSoLuong.Text) + Convert.ToDouble(txtDCCVanTai.Text) - Convert.ToDouble(txtDCTBaoHiem.Text)
                                    - Convert.ToDouble(txtDCTKhoanKhac.Text) - Convert.ToDouble(txtDCTSoLuong.Text) - Convert.ToDouble(txtDCTVanTai.Text) - Convert.ToDouble(txtDieuChinhTruCapDoTM.Text);
            txtTriGiaNTHang.Text = Convert.ToString(Math.Round(TriGiaNguyeTe, 8));
            txtTriGiaTTHang.Text = Convert.ToString(Math.Round(TriGiaNguyeTe * Convert.ToDouble(TKMD.TyGiaTinhThue), 8));
        }

        private void txtTenHangHoa_ButtonClick(object sender, EventArgs e)
        {
            SelectHangTriGia23Form f = new SelectHangTriGia23Form();
            f.TKMD = this.TKMD;
            f.LoaiTKTG = TKTG23.MaToKhaiTriGia;
            f.ShowDialog(this);
            if (f.HMD != null)
            {
                txtSTTHang.Text = f.HMD.SoThuTuHang + "";
                txtTenHangHoa.Text = f.HMD.TenHang.Trim();
            }
        }

        private void txtLyDo_TextChanged(object sender, EventArgs e)
        {

        }


    }
}