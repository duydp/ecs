namespace Company.Interface.Report.GC
{
    partial class ThanhKhoan07_ReiKer_TT117
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenMay = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDaXuatOrChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblConLai = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBienPhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLbl_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSP2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSP1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_HDGCS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_NgayHD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_ThoiHanHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_SoLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_DiaChiBN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_DiaChiBT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_DVHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLable112 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_ThoiHanPLHD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_NgayPLHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_MatHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_PLHDGCS = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_BenThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_BenNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLbl_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.winControlContainer2 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.winControlContainer3 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.winControlContainer4 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label5 = new System.Windows.Forms.Label();
            this.winControlContainer5 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.HeightF = 20F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(992F, 20F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT,
            this.lblTenMay,
            this.lblDVT,
            this.lblSoLuongNhap,
            this.lblDaXuatOrChuyen,
            this.lblConLai,
            this.lblBienPhap});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // lblSTT
            // 
            this.lblSTT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTT.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT.StylePriority.UseTextAlignment = false;
            this.lblSTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTT.Weight = 0.050403225806451624;
            // 
            // lblTenMay
            // 
            this.lblTenMay.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenMay.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenMay.Name = "lblTenMay";
            this.lblTenMay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenMay.StylePriority.UseTextAlignment = false;
            this.lblTenMay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTenMay.Weight = 0.21875;
            // 
            // lblDVT
            // 
            this.lblDVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVT.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVT.StylePriority.UseTextAlignment = false;
            this.lblDVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVT.Weight = 0.11794354838709679;
            // 
            // lblSoLuongNhap
            // 
            this.lblSoLuongNhap.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoLuongNhap.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuongNhap.Name = "lblSoLuongNhap";
            this.lblSoLuongNhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuongNhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongNhap.Weight = 0.09979838709677416;
            this.lblSoLuongNhap.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoLuongNhap_BeforePrint);
            // 
            // lblDaXuatOrChuyen
            // 
            this.lblDaXuatOrChuyen.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDaXuatOrChuyen.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDaXuatOrChuyen.Name = "lblDaXuatOrChuyen";
            this.lblDaXuatOrChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDaXuatOrChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDaXuatOrChuyen.Weight = 0.21875000000000003;
            this.lblDaXuatOrChuyen.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoLuongNhap_BeforePrint);
            // 
            // lblConLai
            // 
            this.lblConLai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblConLai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConLai.Name = "lblConLai";
            this.lblConLai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblConLai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblConLai.Weight = 0.10080645161290322;
            this.lblConLai.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoLuongNhap_BeforePrint);
            // 
            // lblBienPhap
            // 
            this.lblBienPhap.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblBienPhap.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBienPhap.Name = "lblBienPhap";
            this.lblBienPhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBienPhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblBienPhap.Weight = 0.19354838709677419;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLbl_4,
            this.xrLbl_3,
            this.lblSP2,
            this.xrLabel26,
            this.xrLabel9,
            this.lblSP1,
            this.xrLabel10});
            this.PageHeader.HeightF = 50F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLbl_4
            // 
            this.xrLbl_4.BackColor = System.Drawing.Color.Azure;
            this.xrLbl_4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLbl_4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_4.LocationFloat = new DevExpress.Utils.PointFloat(800F, 0F);
            this.xrLbl_4.Name = "xrLbl_4";
            this.xrLbl_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_4.SizeF = new System.Drawing.SizeF(192F, 50F);
            this.xrLbl_4.Text = "Biện pháp xử lý đối với máy móc, thiết bị chưa tái xuất";
            this.xrLbl_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_3
            // 
            this.xrLbl_3.BackColor = System.Drawing.Color.Azure;
            this.xrLbl_3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLbl_3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_3.LocationFloat = new DevExpress.Utils.PointFloat(700F, 0F);
            this.xrLbl_3.Name = "xrLbl_3";
            this.xrLbl_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_3.SizeF = new System.Drawing.SizeF(100F, 50F);
            this.xrLbl_3.Text = "Máy móc, thiết bị còn lại chưa tái xuất";
            this.xrLbl_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSP2
            // 
            this.lblSP2.BackColor = System.Drawing.Color.Azure;
            this.lblSP2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSP2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSP2.LocationFloat = new DevExpress.Utils.PointFloat(483F, 0F);
            this.lblSP2.Name = "lblSP2";
            this.lblSP2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSP2.SizeF = new System.Drawing.SizeF(217F, 50F);
            this.lblSP2.Text = "Đã tái xuất hoặc chuyển sang hợp đồng gia công khác trong khi thực hiện hợp đồng " +
                "GC";
            this.lblSP2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.BackColor = System.Drawing.Color.Azure;
            this.xrLabel26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(267F, 0F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(117F, 50F);
            this.xrLabel26.Text = "Đơn vị tính";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.BackColor = System.Drawing.Color.Azure;
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(50F, 50F);
            this.xrLabel9.Text = "STT";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSP1
            // 
            this.lblSP1.BackColor = System.Drawing.Color.Azure;
            this.lblSP1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSP1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSP1.LocationFloat = new DevExpress.Utils.PointFloat(384F, 0F);
            this.lblSP1.Name = "lblSP1";
            this.lblSP1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSP1.SizeF = new System.Drawing.SizeF(99F, 50F);
            this.lblSP1.Text = "Số lượng tạm nhập";
            this.lblSP1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BackColor = System.Drawing.Color.Azure;
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(50F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(217F, 50F);
            this.xrLabel10.Text = "Tên máy móc, thiết bị tạm nhập";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.xrLabel6,
            this.lblH_HDGCS,
            this.xrLabel24,
            this.xrLabel17,
            this.lblH_NgayHD,
            this.lblH_ThoiHanHD,
            this.xrLabel1,
            this.lblH_SoLuong,
            this.xrLabel34,
            this.lblH_DiaChiBN,
            this.xrLabel33,
            this.lblH_DiaChiBT,
            this.xrLabel31,
            this.lblH_DVHaiQuan,
            this.xrLable112,
            this.xrLabel15,
            this.lblH_ThoiHanPLHD,
            this.lblH_NgayPLHD,
            this.xrLabel18,
            this.lblH_MatHang,
            this.xrLabel8,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.lblH_PLHDGCS,
            this.lblH_BenThue,
            this.lblH_BenNhan,
            this.xrLabel2});
            this.ReportHeader.HeightF = 238F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(733F, 67F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(50F, 16F);
            this.xrLabel7.Text = "Tờ số:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(792F, 8F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(200F, 17F);
            this.xrLabel6.Text = "Mẫu: 07/HSTK-GC/2011, Khổ A4";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_HDGCS
            // 
            this.lblH_HDGCS.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_HDGCS.LocationFloat = new DevExpress.Utils.PointFloat(142F, 100F);
            this.lblH_HDGCS.Name = "lblH_HDGCS";
            this.lblH_HDGCS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_HDGCS.SizeF = new System.Drawing.SizeF(308F, 20F);
            this.lblH_HDGCS.Tag = "Số hợp đồng";
            this.lblH_HDGCS.Text = " ";
            this.lblH_HDGCS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_HDGCS.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(0F, 100F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(142F, 20F);
            this.xrLabel24.Text = "Hợp đồng gia công số :";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(450F, 100F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel17.Text = "Ngày :";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_NgayHD
            // 
            this.lblH_NgayHD.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_NgayHD.LocationFloat = new DevExpress.Utils.PointFloat(500F, 100F);
            this.lblH_NgayHD.Name = "lblH_NgayHD";
            this.lblH_NgayHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_NgayHD.SizeF = new System.Drawing.SizeF(200F, 20F);
            this.lblH_NgayHD.Tag = "Ngày HĐ";
            this.lblH_NgayHD.Text = " ";
            this.lblH_NgayHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_NgayHD.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // lblH_ThoiHanHD
            // 
            this.lblH_ThoiHanHD.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_ThoiHanHD.LocationFloat = new DevExpress.Utils.PointFloat(767F, 100F);
            this.lblH_ThoiHanHD.Name = "lblH_ThoiHanHD";
            this.lblH_ThoiHanHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_ThoiHanHD.SizeF = new System.Drawing.SizeF(225F, 20F);
            this.lblH_ThoiHanHD.Tag = "Hạn HĐ";
            this.lblH_ThoiHanHD.Text = " ";
            this.lblH_ThoiHanHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_ThoiHanHD.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(700F, 100F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(67F, 20F);
            this.xrLabel1.Text = "Thời hạn :";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_SoLuong
            // 
            this.lblH_SoLuong.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_SoLuong.LocationFloat = new DevExpress.Utils.PointFloat(600F, 180F);
            this.lblH_SoLuong.Name = "lblH_SoLuong";
            this.lblH_SoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_SoLuong.SizeF = new System.Drawing.SizeF(392F, 20F);
            this.lblH_SoLuong.Tag = "Lượng hàng";
            this.lblH_SoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_SoLuong.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel34
            // 
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(450F, 180F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(150F, 20F);
            this.xrLabel34.Text = "Số lượng hàng gia công:";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblH_DiaChiBN
            // 
            this.lblH_DiaChiBN.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_DiaChiBN.LocationFloat = new DevExpress.Utils.PointFloat(508F, 160F);
            this.lblH_DiaChiBN.Name = "lblH_DiaChiBN";
            this.lblH_DiaChiBN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_DiaChiBN.SizeF = new System.Drawing.SizeF(484F, 20F);
            this.lblH_DiaChiBN.Tag = "ĐC bên nhận";
            this.lblH_DiaChiBN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_DiaChiBN.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(450F, 160F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(58F, 20F);
            this.xrLabel33.Text = "Địa chỉ :";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_DiaChiBT
            // 
            this.lblH_DiaChiBT.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_DiaChiBT.LocationFloat = new DevExpress.Utils.PointFloat(508F, 140F);
            this.lblH_DiaChiBT.Name = "lblH_DiaChiBT";
            this.lblH_DiaChiBT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_DiaChiBT.SizeF = new System.Drawing.SizeF(484F, 20F);
            this.lblH_DiaChiBT.Tag = "ĐC bên thuê";
            this.lblH_DiaChiBT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_DiaChiBT.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(450F, 140F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(58F, 20F);
            this.xrLabel31.Text = "Địa chỉ :";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_DVHaiQuan
            // 
            this.lblH_DVHaiQuan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_DVHaiQuan.LocationFloat = new DevExpress.Utils.PointFloat(183F, 200F);
            this.lblH_DVHaiQuan.Name = "lblH_DVHaiQuan";
            this.lblH_DVHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_DVHaiQuan.SizeF = new System.Drawing.SizeF(809F, 20F);
            this.lblH_DVHaiQuan.Text = " ";
            this.lblH_DVHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLable112
            // 
            this.xrLable112.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLable112.LocationFloat = new DevExpress.Utils.PointFloat(0F, 200F);
            this.xrLable112.Name = "xrLable112";
            this.xrLable112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLable112.SizeF = new System.Drawing.SizeF(183F, 20F);
            this.xrLable112.Text = "Đơn vị Hải quan làm thủ tục : ";
            this.xrLable112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(700F, 120F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(67F, 20F);
            this.xrLabel15.Text = "Thời hạn :";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_ThoiHanPLHD
            // 
            this.lblH_ThoiHanPLHD.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_ThoiHanPLHD.LocationFloat = new DevExpress.Utils.PointFloat(767F, 120F);
            this.lblH_ThoiHanPLHD.Name = "lblH_ThoiHanPLHD";
            this.lblH_ThoiHanPLHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_ThoiHanPLHD.SizeF = new System.Drawing.SizeF(225F, 20F);
            this.lblH_ThoiHanPLHD.Tag = "Hạn phụ lục";
            this.lblH_ThoiHanPLHD.Text = " ";
            this.lblH_ThoiHanPLHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_ThoiHanPLHD.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // lblH_NgayPLHD
            // 
            this.lblH_NgayPLHD.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_NgayPLHD.LocationFloat = new DevExpress.Utils.PointFloat(500F, 120F);
            this.lblH_NgayPLHD.Name = "lblH_NgayPLHD";
            this.lblH_NgayPLHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_NgayPLHD.SizeF = new System.Drawing.SizeF(200F, 20F);
            this.lblH_NgayPLHD.Tag = "Ngày phụ lục";
            this.lblH_NgayPLHD.Text = " ";
            this.lblH_NgayPLHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_NgayPLHD.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(450F, 120F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel18.Text = "Ngày :";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_MatHang
            // 
            this.lblH_MatHang.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_MatHang.LocationFloat = new DevExpress.Utils.PointFloat(117F, 180F);
            this.lblH_MatHang.Name = "lblH_MatHang";
            this.lblH_MatHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_MatHang.SizeF = new System.Drawing.SizeF(333F, 20F);
            this.lblH_MatHang.Tag = "Mặt hàng GC";
            this.lblH_MatHang.Text = " ";
            this.lblH_MatHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_MatHang.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 180F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(117F, 20F);
            this.xrLabel8.Text = "Mặt hàng gia công:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 120F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(183F, 20F);
            this.xrLabel5.Text = "Phụ lục hợp đồng gia công số :";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 140F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(117F, 20F);
            this.xrLabel4.Text = "Bên thuê gia công:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 160F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(117F, 20F);
            this.xrLabel3.Text = "Bên nhận gia công:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_PLHDGCS
            // 
            this.lblH_PLHDGCS.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_PLHDGCS.LocationFloat = new DevExpress.Utils.PointFloat(183F, 120F);
            this.lblH_PLHDGCS.Name = "lblH_PLHDGCS";
            this.lblH_PLHDGCS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_PLHDGCS.SizeF = new System.Drawing.SizeF(267F, 20F);
            this.lblH_PLHDGCS.Tag = "Phụ lục HĐ";
            this.lblH_PLHDGCS.Text = " ";
            this.lblH_PLHDGCS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_PLHDGCS.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // lblH_BenThue
            // 
            this.lblH_BenThue.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_BenThue.LocationFloat = new DevExpress.Utils.PointFloat(117F, 140F);
            this.lblH_BenThue.Name = "lblH_BenThue";
            this.lblH_BenThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_BenThue.SizeF = new System.Drawing.SizeF(333F, 20F);
            this.lblH_BenThue.Tag = "Bên thuê GC";
            this.lblH_BenThue.Text = " ";
            this.lblH_BenThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_BenThue.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // lblH_BenNhan
            // 
            this.lblH_BenNhan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_BenNhan.LocationFloat = new DevExpress.Utils.PointFloat(117F, 160F);
            this.lblH_BenNhan.Name = "lblH_BenNhan";
            this.lblH_BenNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_BenNhan.SizeF = new System.Drawing.SizeF(333F, 20F);
            this.lblH_BenNhan.Tag = "Bên nhận GC";
            this.lblH_BenNhan.Text = " ";
            this.lblH_BenNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_BenNhan.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 33F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(992F, 25F);
            this.xrLabel2.Text = "BẢNG THANH KHOẢN MÁY MÓC, THIẾT BỊ TẠM NHẬP, TÁI XUẤT";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel74,
            this.xrLabel75,
            this.xrLabel76,
            this.xrLabel77});
            this.ReportFooter.HeightF = 157F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel74
            // 
            this.xrLabel74.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(308F, 25F);
            this.xrLabel74.Text = "Công chức Hải quan kiểm tra, đối chiếu";
            // 
            // xrLabel75
            // 
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(450F, 42F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(266F, 25F);
            this.xrLabel75.Text = "Giám đốc doanh nghiệp";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(0F, 17F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(308F, 25F);
            this.xrLabel76.Text = "Đà Nẵng, ngày ... tháng ... năm......";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(450F, 17F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(266F, 25F);
            this.xrLabel77.Text = "Đà Nẵng, ngày ... tháng ... năm......";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLbl_5,
            this.xrLbl_8,
            this.xrLbl_1,
            this.xrLbl_6,
            this.xrLbl_2,
            this.xrLabel22,
            this.xrLbl_7,
            this.xrLabel11,
            this.xrLabel12});
            this.ReportFooter1.HeightF = 130F;
            this.ReportFooter1.Name = "ReportFooter1";
            this.ReportFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ReportFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLbl_5
            // 
            this.xrLbl_5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_5.LocationFloat = new DevExpress.Utils.PointFloat(375F, 75F);
            this.xrLbl_5.Name = "xrLbl_5";
            this.xrLbl_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_5.SizeF = new System.Drawing.SizeF(258F, 41F);
            this.xrLbl_5.Text = "(Ghi ngày tháng hoàn thành việc đối chiếu; Ký, đóng dấu công chức)";
            this.xrLbl_5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_8
            // 
            this.xrLbl_8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_8.LocationFloat = new DevExpress.Utils.PointFloat(692F, 75F);
            this.xrLbl_8.Name = "xrLbl_8";
            this.xrLbl_8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_8.SizeF = new System.Drawing.SizeF(292F, 25F);
            this.xrLbl_8.Text = "(Lãnh đạo Chi cục ký tên, đóng dấu)";
            this.xrLbl_8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_1
            // 
            this.xrLbl_1.BackColor = System.Drawing.Color.Transparent;
            this.xrLbl_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLbl_1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_1.LocationFloat = new DevExpress.Utils.PointFloat(8F, 25F);
            this.xrLbl_1.Name = "xrLbl_1";
            this.xrLbl_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_1.SizeF = new System.Drawing.SizeF(266F, 25F);
            this.xrLbl_1.Text = "Ngày....... tháng ........năm.......";
            this.xrLbl_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_6
            // 
            this.xrLbl_6.BackColor = System.Drawing.Color.Transparent;
            this.xrLbl_6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLbl_6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_6.LocationFloat = new DevExpress.Utils.PointFloat(692F, 25F);
            this.xrLbl_6.Name = "xrLbl_6";
            this.xrLbl_6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_6.SizeF = new System.Drawing.SizeF(291F, 25F);
            this.xrLbl_6.Text = "Ngày ... tháng ... năm......";
            this.xrLbl_6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_2
            // 
            this.xrLbl_2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_2.LocationFloat = new DevExpress.Utils.PointFloat(8F, 50F);
            this.xrLbl_2.Name = "xrLbl_2";
            this.xrLbl_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_2.SizeF = new System.Drawing.SizeF(266F, 25F);
            this.xrLbl_2.Text = "Đại diện theo pháp luật của thương nhân";
            this.xrLbl_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(8F, 75F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(267F, 25F);
            this.xrLabel22.Text = "(Ký tên, đóng dấu)";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_7
            // 
            this.xrLbl_7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_7.LocationFloat = new DevExpress.Utils.PointFloat(692F, 50F);
            this.xrLbl_7.Name = "xrLbl_7";
            this.xrLbl_7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_7.SizeF = new System.Drawing.SizeF(292F, 25F);
            this.xrLbl_7.Text = "Xác nhận hoàn thành thủ tục thanh khoản           ";
            this.xrLbl_7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(325F, 50F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(342F, 25F);
            this.xrLabel11.Text = "Công chức Hải quan đối chiếu";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(375F, 25F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(258F, 25F);
            this.xrLabel12.Text = "Ngày....... tháng ........năm.......";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "label2";
            // 
            // winControlContainer2
            // 
            this.winControlContainer2.LocationFloat = new DevExpress.Utils.PointFloat(742F, 42F);
            this.winControlContainer2.Name = "winControlContainer2";
            this.winControlContainer2.SizeF = new System.Drawing.SizeF(61F, 22F);
            this.winControlContainer2.WinControl = this.label2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "label3";
            // 
            // winControlContainer3
            // 
            this.winControlContainer3.LocationFloat = new DevExpress.Utils.PointFloat(733F, 50F);
            this.winControlContainer3.Name = "winControlContainer3";
            this.winControlContainer3.SizeF = new System.Drawing.SizeF(61F, 22F);
            this.winControlContainer3.WinControl = this.label3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "label4";
            // 
            // winControlContainer4
            // 
            this.winControlContainer4.LocationFloat = new DevExpress.Utils.PointFloat(742F, 50F);
            this.winControlContainer4.Name = "winControlContainer4";
            this.winControlContainer4.SizeF = new System.Drawing.SizeF(61F, 22F);
            this.winControlContainer4.WinControl = this.label4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 21);
            this.label5.TabIndex = 0;
            this.label5.Text = "label5";
            // 
            // winControlContainer5
            // 
            this.winControlContainer5.LocationFloat = new DevExpress.Utils.PointFloat(750F, 42F);
            this.winControlContainer5.Name = "winControlContainer5";
            this.winControlContainer5.SizeF = new System.Drawing.SizeF(61F, 22F);
            this.winControlContainer5.WinControl = this.label5;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 32F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 47F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ThanhKhoan07_ReiKer_TT117
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.ReportHeader,
            this.ReportFooter1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(73, 101, 32, 47);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lblSP1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel lblSP2;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_4;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_3;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_6;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_7;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT;
        private DevExpress.XtraReports.UI.XRTableCell lblTenMay;
        private DevExpress.XtraReports.UI.XRTableCell lblConLai;
        private DevExpress.XtraReports.UI.XRTableCell lblBienPhap;
        private DevExpress.XtraReports.UI.XRLabel lblH_SoLuong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel lblH_DiaChiBN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel lblH_DiaChiBT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel lblH_DVHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel xrLable112;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lblH_ThoiHanPLHD;
        private DevExpress.XtraReports.UI.XRLabel lblH_NgayPLHD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel lblH_MatHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblH_PLHDGCS;
        private DevExpress.XtraReports.UI.XRLabel lblH_BenThue;
        private DevExpress.XtraReports.UI.XRLabel lblH_BenNhan;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongNhap;
        private DevExpress.XtraReports.UI.XRTableCell lblDVT;
        private DevExpress.XtraReports.UI.XRTableCell lblDaXuatOrChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblH_HDGCS;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel lblH_NgayHD;
        private DevExpress.XtraReports.UI.XRLabel lblH_ThoiHanHD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_1;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_5;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_8;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer3;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
    }
}
