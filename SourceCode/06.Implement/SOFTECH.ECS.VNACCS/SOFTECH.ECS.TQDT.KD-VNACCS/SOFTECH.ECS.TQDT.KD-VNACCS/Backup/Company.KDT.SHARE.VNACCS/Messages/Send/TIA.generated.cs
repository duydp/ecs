using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class TIA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ECN { get; set; }
public PropertiesAttribute CFD { get; set; }
public PropertiesAttribute NFD { get; set; }
public PropertiesAttribute IEC { get; set; }
public PropertiesAttribute IEN { get; set; }
public PropertiesAttribute TED { get; set; }

public TIA()
        {
ECN = new PropertiesAttribute(12, typeof(int));
CFD = new PropertiesAttribute(5, typeof(string));
NFD = new PropertiesAttribute(50, typeof(string));
IEC = new PropertiesAttribute(13, typeof(string));
IEN = new PropertiesAttribute(300, typeof(string));
TED = new PropertiesAttribute(8, typeof(DateTime));
TongSoByte = 400;
}

}public partial class EnumGroupID
    {
}

}
