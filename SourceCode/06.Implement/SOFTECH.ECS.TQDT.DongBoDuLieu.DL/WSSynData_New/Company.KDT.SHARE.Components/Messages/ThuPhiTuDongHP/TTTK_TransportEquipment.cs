﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
  public  class TTTK_TransportEquipment
    {
        /// <summary>
        /// Số vận đơn
        /// </summary>
        [XmlElement("billOfLading")]
        public string BillOfLading { get; set; }

        /// <summary>
        /// Số vận đơn
        /// </summary>
        [XmlElement("billOfLadingNo")]
        public string BillOfLadingNo { get; set; }

        /// <summary>
        /// Tổng trọng lượng
        /// </summary>
        [XmlElement("grossMass")]
        public decimal GrossMass { get; set; }

        /// <summary>
        /// Đơn vị tính
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set; }

        /// <summary>
        /// Số container
        /// </summary>
        [XmlElement("container")]
        public string Container { get; set; }

        /// <summary>
        /// Số seal
        /// </summary>
        [XmlElement("seal")]
        public string Seal { get; set; }

        /// <summary>
        /// Loại container Danh mục loại container
        /// 20 - Container 20 feet
        /// 40 - Container 40 feet
        /// 99 - Container loại khác
        /// </summary>
        [XmlElement("containerType")]
        public string ContainerType { get; set; }

        /// <summary>
        /// Tính chất container
        /// 0 - Container thường
        /// 1 - Container hàng khô
        /// 2 - Container hàng lạnh
        /// </summary>
        [XmlElement("containerKind")]
        public string ContainerKind { get; set; }

        /// <summary>
        /// Số lượng kiện trong container
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }

        /// <summary>
        /// Ghi chú nếu có
        /// </summary>
        [XmlElement("journalMemo")]
        public string JournalMemo { get; set; }

    }
}
