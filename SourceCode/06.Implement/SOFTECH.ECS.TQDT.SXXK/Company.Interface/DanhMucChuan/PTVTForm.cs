﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class PTVTForm : Company.Interface.BaseForm
    {
        DataSet ds = new DataSet();
        public PTVTForm()
        {
            InitializeComponent();
        }

        private void PTVTForm_Load(object sender, EventArgs e)
        {
            ds = PhuongThucVanTai.SelectAll();
            dgList.DataSource = ds;
            dgList.DataMember = ds.Tables[0].TableName;
        }
        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                PhuongThucVanTai.Update(ds);
               // ShowMessage("Cập nhật thành công.", false);
                MLMessages("Lưu thành công", "MSG_SAV02", "", false); 
            }
            catch (Exception ex)
            {
               // ShowMessage("Lỗi: " + ex.Message, false);
                MLMessages("Lưu không thành công", "MSG_SAV01", "", false);
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
           // if (ShowMessage("Bạn có muốn xóa các phương thức vận tải này không?", true) != "Yes") e.Cancel = true;
            if (MLMessages("Bạn có muốn xóa các phương thức vận tải này không?", "MSG_DEL01", "", true) != "Yes") e.Cancel = true;
        }
        private bool CheckID(string Id)
        {
            if (ds.Tables[0].Select(" ID= '" + Id.Trim() + "'").Length == 0) return false;
            else return true;
        }
        private void dgList_UpdatingCell(object sender, Janus.Windows.GridEX.UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "ID")
            {
                string s = e.Value.ToString();
                if (CheckID(s))
                {
                    //ShowMessage("Mã phương thức vận tải này đã có.", false);
                    MLMessages("Mã phương thức vận tải này đã có.", "MSG_PUB10", "", false); 
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    //ShowMessage("Mã phương thức vận tải không được rỗng.", false);
                    MLMessages("Mã phương thức vận tải không được rỗng.", "MSG_CAL01", "", false);
                    e.Cancel = true;
                }
            }
        }
    }
}

