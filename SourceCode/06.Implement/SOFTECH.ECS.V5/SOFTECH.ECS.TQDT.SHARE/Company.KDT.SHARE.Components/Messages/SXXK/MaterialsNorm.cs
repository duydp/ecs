﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class MaterialsNorm
    {
        /// <summary>
        /// Nguyên phụ liệu
        /// </summary>
        [XmlElement("Material")]
        public Product Material { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("rateExchange")]
        public string RateExchange { get; set; }


        [XmlElement("description")]
        public string Description { get; set; }


        /// <summary>
        /// Định mức sử dụng n..8
        /// </summary> 
        [XmlElement("norm")]
        public string Norm { get; set; }
        /// <summary>
        /// Tỷ lệ hao hụt n2,1
        /// </summary>
        [XmlElement("loss")]
        public string Loss { get; set; }
    }
}
