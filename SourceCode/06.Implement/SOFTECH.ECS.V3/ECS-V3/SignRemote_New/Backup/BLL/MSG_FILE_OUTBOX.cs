﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Data;

namespace SignRemote
{
    public partial class MSG_FILE_OUTBOX
    {
        public static void InsertDataSignLocal(string guidId, string FileName, string FileData)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                MSG_FILE_OUTBOX.InsertMSG_FILE_OUTBOX(string.Empty, string.Empty, FileName, FileData, DateTime.Now, -1, guidId);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static MSG_FILE_OUTBOX GetContentSignLocal(string cnnString, string guidId)
        {
            try
            {
                SqlConnection cnn = new SqlConnection(cnnString);
                cnn.Open();
                string cmdText = string.Format("Select top(1) * from t_MSG_FILE_OUTBOX where GUIDSTR='{0}' order by CREATE_TIME desc", guidId);
                SqlCommand sqlCmd = new SqlCommand(cmdText, cnn);
                IDataReader reader = sqlCmd.ExecuteReader();
                MSG_FILE_OUTBOX entity = null;
                if (reader.Read())
                {
                    entity = new MSG_FILE_OUTBOX();
                    if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
                    if (!reader.IsDBNull(reader.GetOrdinal("FILE_NAME"))) entity.FILE_NAME = reader.GetString(reader.GetOrdinal("FILE_NAME"));
                    if (!reader.IsDBNull(reader.GetOrdinal("FILE_DATA"))) entity.FILE_DATA = reader.GetString(reader.GetOrdinal("FILE_DATA"));
                    if (!reader.IsDBNull(reader.GetOrdinal("CREATE_TIME"))) entity.CREATE_TIME = reader.GetDateTime(reader.GetOrdinal("CREATE_TIME"));
                    if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) entity.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
                    if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                }
                reader.Close();
                return entity;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }

        }
    }
}
