﻿namespace Company.Interface
{
    partial class VNACC_ChuyenCuaKhau_HangHoaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ChuyenCuaKhau_HangHoaForm));
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaNuocSanXuat = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ctrLoaiHangHoa = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiemDichVC = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapLuat5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapLuat4 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapLuat3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapLuat2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapLuat1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiemXuatPhatVC = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaHS = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtPhanLoaiSanPhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiNguoiNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNguoiNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.clcNgayNhapKhoHQLanDau = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayHangDuKienDenDi = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayPhatHanhVD = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenPhuongTienVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaPhuongTienVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtKyHieuVaSoHieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMoTaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnThoat = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiNguoiXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNguoiXuatKhua = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiNguoiUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNguoiUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.ctrMaDVTSoLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDVTTrongLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDVTTheTich = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDVTTriGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTheTich = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayHetHanCapPhep = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayCapPhep = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDanhDauDDKH1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDanhDauDDKH2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDanhDauDDKH3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDanhDauDDKH4 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDanhDauDDKH5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 756), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 756);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 732);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 732);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.grList);
            this.grbMain.Size = new System.Drawing.Size(857, 756);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox12);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(857, 577);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaNuocSanXuat
            // 
            this.ctrMaNuocSanXuat.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaNuocSanXuat.Appearance.Options.UseBackColor = true;
            this.ctrMaNuocSanXuat.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrMaNuocSanXuat.Code = "";
            this.ctrMaNuocSanXuat.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaNuocSanXuat.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaNuocSanXuat.IsOnlyWarning = false;
            this.ctrMaNuocSanXuat.IsValidate = true;
            this.ctrMaNuocSanXuat.Location = new System.Drawing.Point(110, 145);
            this.ctrMaNuocSanXuat.Name = "ctrMaNuocSanXuat";
            this.ctrMaNuocSanXuat.Name_VN = "";
            this.ctrMaNuocSanXuat.SetOnlyWarning = false;
            this.ctrMaNuocSanXuat.SetValidate = false;
            this.ctrMaNuocSanXuat.ShowColumnCode = true;
            this.ctrMaNuocSanXuat.ShowColumnName = true;
            this.ctrMaNuocSanXuat.Size = new System.Drawing.Size(191, 21);
            this.ctrMaNuocSanXuat.TabIndex = 6;
            this.ctrMaNuocSanXuat.TagCode = "";
            this.ctrMaNuocSanXuat.TagName = "";
            this.ctrMaNuocSanXuat.Where = null;
            this.ctrMaNuocSanXuat.WhereCondition = "";
            // 
            // ctrLoaiHangHoa
            // 
            this.ctrLoaiHangHoa.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrLoaiHangHoa.Appearance.Options.UseBackColor = true;
            this.ctrLoaiHangHoa.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E037;
            this.ctrLoaiHangHoa.Code = "";
            this.ctrLoaiHangHoa.ColorControl = System.Drawing.Color.Empty;
            this.ctrLoaiHangHoa.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrLoaiHangHoa.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrLoaiHangHoa.IsOnlyWarning = false;
            this.ctrLoaiHangHoa.IsValidate = true;
            this.ctrLoaiHangHoa.Location = new System.Drawing.Point(529, 175);
            this.ctrLoaiHangHoa.Name = "ctrLoaiHangHoa";
            this.ctrLoaiHangHoa.Name_VN = "";
            this.ctrLoaiHangHoa.SetOnlyWarning = false;
            this.ctrLoaiHangHoa.SetValidate = false;
            this.ctrLoaiHangHoa.ShowColumnCode = true;
            this.ctrLoaiHangHoa.ShowColumnName = false;
            this.ctrLoaiHangHoa.Size = new System.Drawing.Size(71, 21);
            this.ctrLoaiHangHoa.TabIndex = 10;
            this.ctrLoaiHangHoa.TagName = "";
            this.ctrLoaiHangHoa.Where = null;
            this.ctrLoaiHangHoa.WhereCondition = "";
            // 
            // ctrMaDiaDiemDichVC
            // 
            this.ctrMaDiaDiemDichVC.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDiaDiemDichVC.Appearance.Options.UseBackColor = true;
            this.ctrMaDiaDiemDichVC.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ctrMaDiaDiemDichVC.Code = "";
            this.ctrMaDiaDiemDichVC.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDiaDiemDichVC.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiemDichVC.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiemDichVC.IsOnlyWarning = false;
            this.ctrMaDiaDiemDichVC.IsValidate = true;
            this.ctrMaDiaDiemDichVC.Location = new System.Drawing.Point(310, 175);
            this.ctrMaDiaDiemDichVC.Name = "ctrMaDiaDiemDichVC";
            this.ctrMaDiaDiemDichVC.Name_VN = "";
            this.ctrMaDiaDiemDichVC.SetOnlyWarning = false;
            this.ctrMaDiaDiemDichVC.SetValidate = false;
            this.ctrMaDiaDiemDichVC.ShowColumnCode = true;
            this.ctrMaDiaDiemDichVC.ShowColumnName = false;
            this.ctrMaDiaDiemDichVC.Size = new System.Drawing.Size(89, 21);
            this.ctrMaDiaDiemDichVC.TabIndex = 9;
            this.ctrMaDiaDiemDichVC.TagName = "";
            this.ctrMaDiaDiemDichVC.Where = null;
            this.ctrMaDiaDiemDichVC.WhereCondition = "";
            // 
            // ctrMaVanBanPhapLuat5
            // 
            this.ctrMaVanBanPhapLuat5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapLuat5.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapLuat5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapLuat5.Code = "";
            this.ctrMaVanBanPhapLuat5.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapLuat5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapLuat5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapLuat5.IsOnlyWarning = false;
            this.ctrMaVanBanPhapLuat5.IsValidate = true;
            this.ctrMaVanBanPhapLuat5.Location = new System.Drawing.Point(557, 248);
            this.ctrMaVanBanPhapLuat5.Name = "ctrMaVanBanPhapLuat5";
            this.ctrMaVanBanPhapLuat5.Name_VN = "";
            this.ctrMaVanBanPhapLuat5.SetOnlyWarning = false;
            this.ctrMaVanBanPhapLuat5.SetValidate = false;
            this.ctrMaVanBanPhapLuat5.ShowColumnCode = true;
            this.ctrMaVanBanPhapLuat5.ShowColumnName = false;
            this.ctrMaVanBanPhapLuat5.Size = new System.Drawing.Size(41, 21);
            this.ctrMaVanBanPhapLuat5.TabIndex = 18;
            this.ctrMaVanBanPhapLuat5.TagName = "";
            this.ctrMaVanBanPhapLuat5.Where = null;
            this.ctrMaVanBanPhapLuat5.WhereCondition = "";
            // 
            // ctrMaVanBanPhapLuat4
            // 
            this.ctrMaVanBanPhapLuat4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapLuat4.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapLuat4.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapLuat4.Code = "";
            this.ctrMaVanBanPhapLuat4.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapLuat4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapLuat4.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapLuat4.IsOnlyWarning = false;
            this.ctrMaVanBanPhapLuat4.IsValidate = true;
            this.ctrMaVanBanPhapLuat4.Location = new System.Drawing.Point(503, 248);
            this.ctrMaVanBanPhapLuat4.Name = "ctrMaVanBanPhapLuat4";
            this.ctrMaVanBanPhapLuat4.Name_VN = "";
            this.ctrMaVanBanPhapLuat4.SetOnlyWarning = false;
            this.ctrMaVanBanPhapLuat4.SetValidate = false;
            this.ctrMaVanBanPhapLuat4.ShowColumnCode = true;
            this.ctrMaVanBanPhapLuat4.ShowColumnName = false;
            this.ctrMaVanBanPhapLuat4.Size = new System.Drawing.Size(41, 21);
            this.ctrMaVanBanPhapLuat4.TabIndex = 17;
            this.ctrMaVanBanPhapLuat4.TagName = "";
            this.ctrMaVanBanPhapLuat4.Where = null;
            this.ctrMaVanBanPhapLuat4.WhereCondition = "";
            // 
            // ctrMaVanBanPhapLuat3
            // 
            this.ctrMaVanBanPhapLuat3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapLuat3.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapLuat3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapLuat3.Code = "";
            this.ctrMaVanBanPhapLuat3.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapLuat3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapLuat3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapLuat3.IsOnlyWarning = false;
            this.ctrMaVanBanPhapLuat3.IsValidate = true;
            this.ctrMaVanBanPhapLuat3.Location = new System.Drawing.Point(449, 248);
            this.ctrMaVanBanPhapLuat3.Name = "ctrMaVanBanPhapLuat3";
            this.ctrMaVanBanPhapLuat3.Name_VN = "";
            this.ctrMaVanBanPhapLuat3.SetOnlyWarning = false;
            this.ctrMaVanBanPhapLuat3.SetValidate = false;
            this.ctrMaVanBanPhapLuat3.ShowColumnCode = true;
            this.ctrMaVanBanPhapLuat3.ShowColumnName = false;
            this.ctrMaVanBanPhapLuat3.Size = new System.Drawing.Size(41, 21);
            this.ctrMaVanBanPhapLuat3.TabIndex = 16;
            this.ctrMaVanBanPhapLuat3.TagName = "";
            this.ctrMaVanBanPhapLuat3.Where = null;
            this.ctrMaVanBanPhapLuat3.WhereCondition = "";
            // 
            // ctrMaVanBanPhapLuat2
            // 
            this.ctrMaVanBanPhapLuat2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapLuat2.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapLuat2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapLuat2.Code = "";
            this.ctrMaVanBanPhapLuat2.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapLuat2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapLuat2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapLuat2.IsOnlyWarning = false;
            this.ctrMaVanBanPhapLuat2.IsValidate = true;
            this.ctrMaVanBanPhapLuat2.Location = new System.Drawing.Point(393, 248);
            this.ctrMaVanBanPhapLuat2.Name = "ctrMaVanBanPhapLuat2";
            this.ctrMaVanBanPhapLuat2.Name_VN = "";
            this.ctrMaVanBanPhapLuat2.SetOnlyWarning = false;
            this.ctrMaVanBanPhapLuat2.SetValidate = false;
            this.ctrMaVanBanPhapLuat2.ShowColumnCode = true;
            this.ctrMaVanBanPhapLuat2.ShowColumnName = false;
            this.ctrMaVanBanPhapLuat2.Size = new System.Drawing.Size(41, 21);
            this.ctrMaVanBanPhapLuat2.TabIndex = 15;
            this.ctrMaVanBanPhapLuat2.TagName = "";
            this.ctrMaVanBanPhapLuat2.Where = null;
            this.ctrMaVanBanPhapLuat2.WhereCondition = "";
            // 
            // ctrMaVanBanPhapLuat1
            // 
            this.ctrMaVanBanPhapLuat1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapLuat1.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapLuat1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapLuat1.Code = "";
            this.ctrMaVanBanPhapLuat1.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapLuat1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapLuat1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapLuat1.IsOnlyWarning = false;
            this.ctrMaVanBanPhapLuat1.IsValidate = true;
            this.ctrMaVanBanPhapLuat1.Location = new System.Drawing.Point(336, 248);
            this.ctrMaVanBanPhapLuat1.Name = "ctrMaVanBanPhapLuat1";
            this.ctrMaVanBanPhapLuat1.Name_VN = "";
            this.ctrMaVanBanPhapLuat1.SetOnlyWarning = false;
            this.ctrMaVanBanPhapLuat1.SetValidate = false;
            this.ctrMaVanBanPhapLuat1.ShowColumnCode = true;
            this.ctrMaVanBanPhapLuat1.ShowColumnName = false;
            this.ctrMaVanBanPhapLuat1.Size = new System.Drawing.Size(41, 21);
            this.ctrMaVanBanPhapLuat1.TabIndex = 14;
            this.ctrMaVanBanPhapLuat1.TagName = "";
            this.ctrMaVanBanPhapLuat1.Where = null;
            this.ctrMaVanBanPhapLuat1.WhereCondition = "";
            // 
            // ctrMaDiaDiemXuatPhatVC
            // 
            this.ctrMaDiaDiemXuatPhatVC.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDiaDiemXuatPhatVC.Appearance.Options.UseBackColor = true;
            this.ctrMaDiaDiemXuatPhatVC.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ctrMaDiaDiemXuatPhatVC.Code = "";
            this.ctrMaDiaDiemXuatPhatVC.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDiaDiemXuatPhatVC.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiemXuatPhatVC.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiemXuatPhatVC.IsOnlyWarning = false;
            this.ctrMaDiaDiemXuatPhatVC.IsValidate = true;
            this.ctrMaDiaDiemXuatPhatVC.Location = new System.Drawing.Point(127, 175);
            this.ctrMaDiaDiemXuatPhatVC.Name = "ctrMaDiaDiemXuatPhatVC";
            this.ctrMaDiaDiemXuatPhatVC.Name_VN = "";
            this.ctrMaDiaDiemXuatPhatVC.SetOnlyWarning = false;
            this.ctrMaDiaDiemXuatPhatVC.SetValidate = false;
            this.ctrMaDiaDiemXuatPhatVC.ShowColumnCode = true;
            this.ctrMaDiaDiemXuatPhatVC.ShowColumnName = false;
            this.ctrMaDiaDiemXuatPhatVC.Size = new System.Drawing.Size(92, 21);
            this.ctrMaDiaDiemXuatPhatVC.TabIndex = 8;
            this.ctrMaDiaDiemXuatPhatVC.TagName = "";
            this.ctrMaDiaDiemXuatPhatVC.Where = null;
            this.ctrMaDiaDiemXuatPhatVC.WhereCondition = "";
            // 
            // ctrMaHS
            // 
            this.ctrMaHS.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaHS.Appearance.Options.UseBackColor = true;
            this.ctrMaHS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A232;
            this.ctrMaHS.Code = "";
            this.ctrMaHS.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaHS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHS.IsOnlyWarning = false;
            this.ctrMaHS.IsValidate = true;
            this.ctrMaHS.Location = new System.Drawing.Point(88, 83);
            this.ctrMaHS.Name = "ctrMaHS";
            this.ctrMaHS.Name_VN = "";
            this.ctrMaHS.SetOnlyWarning = false;
            this.ctrMaHS.SetValidate = false;
            this.ctrMaHS.ShowColumnCode = true;
            this.ctrMaHS.ShowColumnName = false;
            this.ctrMaHS.Size = new System.Drawing.Size(83, 21);
            this.ctrMaHS.TabIndex = 3;
            this.ctrMaHS.TagName = "";
            this.ctrMaHS.Where = null;
            this.ctrMaHS.WhereCondition = "";
            // 
            // txtPhanLoaiSanPhan
            // 
            this.txtPhanLoaiSanPhan.Location = new System.Drawing.Point(572, 145);
            this.txtPhanLoaiSanPhan.Name = "txtPhanLoaiSanPhan";
            this.txtPhanLoaiSanPhan.Size = new System.Drawing.Size(23, 21);
            this.txtPhanLoaiSanPhan.TabIndex = 7;
            this.txtPhanLoaiSanPhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.Controls.Add(this.txtDiaChiNguoiNhapKhau);
            this.uiGroupBox3.Controls.Add(this.txtTenNguoiNhapKhau);
            this.uiGroupBox3.Controls.Add(this.txtMaNguoiNhapKhau);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.label15);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(361, 133);
            this.uiGroupBox3.TabIndex = 19;
            this.uiGroupBox3.Text = "Người nhập khẩu";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiNguoiNhapKhau
            // 
            this.txtDiaChiNguoiNhapKhau.Location = new System.Drawing.Point(63, 86);
            this.txtDiaChiNguoiNhapKhau.Multiline = true;
            this.txtDiaChiNguoiNhapKhau.Name = "txtDiaChiNguoiNhapKhau";
            this.txtDiaChiNguoiNhapKhau.Size = new System.Drawing.Size(283, 42);
            this.txtDiaChiNguoiNhapKhau.TabIndex = 2;
            this.txtDiaChiNguoiNhapKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNguoiNhapKhau
            // 
            this.txtTenNguoiNhapKhau.Location = new System.Drawing.Point(63, 40);
            this.txtTenNguoiNhapKhau.Multiline = true;
            this.txtTenNguoiNhapKhau.Name = "txtTenNguoiNhapKhau";
            this.txtTenNguoiNhapKhau.Size = new System.Drawing.Size(283, 42);
            this.txtTenNguoiNhapKhau.TabIndex = 1;
            this.txtTenNguoiNhapKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNguoiNhapKhau
            // 
            this.txtMaNguoiNhapKhau.Location = new System.Drawing.Point(63, 16);
            this.txtMaNguoiNhapKhau.Name = "txtMaNguoiNhapKhau";
            this.txtMaNguoiNhapKhau.Size = new System.Drawing.Size(283, 21);
            this.txtMaNguoiNhapKhau.TabIndex = 0;
            this.txtMaNguoiNhapKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 101);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Địa chỉ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(25, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Tên";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Mã";
            // 
            // clcNgayNhapKhoHQLanDau
            // 
            this.clcNgayNhapKhoHQLanDau.Location = new System.Drawing.Point(190, 111);
            this.clcNgayNhapKhoHQLanDau.Name = "clcNgayNhapKhoHQLanDau";
            this.clcNgayNhapKhoHQLanDau.ReadOnly = false;
            this.clcNgayNhapKhoHQLanDau.Size = new System.Drawing.Size(95, 21);
            this.clcNgayNhapKhoHQLanDau.TabIndex = 4;
            this.clcNgayNhapKhoHQLanDau.TagName = "";
            this.clcNgayNhapKhoHQLanDau.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayNhapKhoHQLanDau.WhereCondition = "";
            // 
            // clcNgayHangDuKienDenDi
            // 
            this.clcNgayHangDuKienDenDi.Location = new System.Drawing.Point(162, 247);
            this.clcNgayHangDuKienDenDi.Name = "clcNgayHangDuKienDenDi";
            this.clcNgayHangDuKienDenDi.ReadOnly = false;
            this.clcNgayHangDuKienDenDi.Size = new System.Drawing.Size(100, 21);
            this.clcNgayHangDuKienDenDi.TabIndex = 13;
            this.clcNgayHangDuKienDenDi.TagName = "";
            this.clcNgayHangDuKienDenDi.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHangDuKienDenDi.WhereCondition = "";
            // 
            // clcNgayPhatHanhVD
            // 
            this.clcNgayPhatHanhVD.Location = new System.Drawing.Point(500, 20);
            this.clcNgayPhatHanhVD.Name = "clcNgayPhatHanhVD";
            this.clcNgayPhatHanhVD.ReadOnly = false;
            this.clcNgayPhatHanhVD.Size = new System.Drawing.Size(100, 21);
            this.clcNgayPhatHanhVD.TabIndex = 1;
            this.clcNgayPhatHanhVD.TagName = "";
            this.clcNgayPhatHanhVD.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayPhatHanhVD.WhereCondition = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(307, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(259, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Phân loại sản phẩm sản xuất từ hàng hóa nhập khẩu";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 251);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Ngày dự kiến đến/đi";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 569);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(0, 13);
            this.label13.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 216);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Phương tiện vận chuyển";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(400, 179);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Loại manifest(hàng hóa)";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(545, 251);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(13, 13);
            this.label31.TabIndex = 1;
            this.label31.Text = "5";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(490, 251);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "4";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(436, 251);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 13);
            this.label29.TabIndex = 1;
            this.label29.Text = "3";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(378, 251);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(13, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "2";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(274, 251);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Luật khác  1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(225, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Mã địa điểm đích";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 179);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Mã địa điểm xuất phát";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 149);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Mã nước sản xuất";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Ngày nhập kho ngoại quan lần đầu";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(213, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Ký hiệu số hiệu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Mã HS(4 số)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Tên hàng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(375, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngày phát hành vận đơn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Số hàng hóa (Số B/L / AWB)";
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.Location = new System.Drawing.Point(164, 20);
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(205, 21);
            this.txtSoVanDon.TabIndex = 0;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenPhuongTienVC
            // 
            this.txtTenPhuongTienVC.Location = new System.Drawing.Point(360, 212);
            this.txtTenPhuongTienVC.Name = "txtTenPhuongTienVC";
            this.txtTenPhuongTienVC.Size = new System.Drawing.Size(240, 21);
            this.txtTenPhuongTienVC.TabIndex = 12;
            this.txtTenPhuongTienVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaPhuongTienVC
            // 
            this.txtMaPhuongTienVC.Location = new System.Drawing.Point(162, 212);
            this.txtMaPhuongTienVC.Name = "txtMaPhuongTienVC";
            this.txtMaPhuongTienVC.Size = new System.Drawing.Size(187, 21);
            this.txtMaPhuongTienVC.TabIndex = 11;
            this.txtMaPhuongTienVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtKyHieuVaSoHieu
            // 
            this.txtKyHieuVaSoHieu.Location = new System.Drawing.Point(295, 83);
            this.txtKyHieuVaSoHieu.Multiline = true;
            this.txtKyHieuVaSoHieu.Name = "txtKyHieuVaSoHieu";
            this.txtKyHieuVaSoHieu.Size = new System.Drawing.Size(303, 49);
            this.txtKyHieuVaSoHieu.TabIndex = 5;
            this.txtKyHieuVaSoHieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMoTaHangHoa
            // 
            this.txtMoTaHangHoa.Location = new System.Drawing.Point(90, 52);
            this.txtMoTaHangHoa.Name = "txtMoTaHangHoa";
            this.txtMoTaHangHoa.Size = new System.Drawing.Size(510, 21);
            this.txtMoTaHangHoa.TabIndex = 2;
            this.txtMoTaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnThoat);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.btnLuu);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 577);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(857, 44);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnThoat
            // 
            this.btnThoat.Image = ((System.Drawing.Image)(resources.GetObject("btnThoat.Image")));
            this.btnThoat.ImageSize = new System.Drawing.Size(20, 20);
            this.btnThoat.Location = new System.Drawing.Point(690, 11);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 23);
            this.btnThoat.TabIndex = 2;
            this.btnThoat.Text = "Đóng";
            this.btnThoat.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(609, 11);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 1;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageSize = new System.Drawing.Size(20, 20);
            this.btnLuu.Location = new System.Drawing.Point(526, 11);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(75, 23);
            this.btnLuu.TabIndex = 0;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.DataSource = this.grList.Layouts;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grList.FrozenColumns = 3;
            this.grList.GroupByBoxVisible = false;
            this.grList.Location = new System.Drawing.Point(0, 621);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(857, 135);
            this.grList.TabIndex = 4;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.AutoScroll = true;
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox10.Controls.Add(this.txtMoTaHangHoa);
            this.uiGroupBox10.Controls.Add(this.ctrMaNuocSanXuat);
            this.uiGroupBox10.Controls.Add(this.txtKyHieuVaSoHieu);
            this.uiGroupBox10.Controls.Add(this.ctrLoaiHangHoa);
            this.uiGroupBox10.Controls.Add(this.txtMaPhuongTienVC);
            this.uiGroupBox10.Controls.Add(this.ctrMaDiaDiemDichVC);
            this.uiGroupBox10.Controls.Add(this.txtTenPhuongTienVC);
            this.uiGroupBox10.Controls.Add(this.ctrMaVanBanPhapLuat5);
            this.uiGroupBox10.Controls.Add(this.label1);
            this.uiGroupBox10.Controls.Add(this.ctrMaVanBanPhapLuat4);
            this.uiGroupBox10.Controls.Add(this.label2);
            this.uiGroupBox10.Controls.Add(this.ctrMaVanBanPhapLuat3);
            this.uiGroupBox10.Controls.Add(this.label3);
            this.uiGroupBox10.Controls.Add(this.ctrMaVanBanPhapLuat2);
            this.uiGroupBox10.Controls.Add(this.label4);
            this.uiGroupBox10.Controls.Add(this.ctrMaVanBanPhapLuat1);
            this.uiGroupBox10.Controls.Add(this.label5);
            this.uiGroupBox10.Controls.Add(this.ctrMaDiaDiemXuatPhatVC);
            this.uiGroupBox10.Controls.Add(this.label6);
            this.uiGroupBox10.Controls.Add(this.ctrMaHS);
            this.uiGroupBox10.Controls.Add(this.label8);
            this.uiGroupBox10.Controls.Add(this.txtPhanLoaiSanPhan);
            this.uiGroupBox10.Controls.Add(this.label9);
            this.uiGroupBox10.Controls.Add(this.label10);
            this.uiGroupBox10.Controls.Add(this.label24);
            this.uiGroupBox10.Controls.Add(this.label28);
            this.uiGroupBox10.Controls.Add(this.label29);
            this.uiGroupBox10.Controls.Add(this.label30);
            this.uiGroupBox10.Controls.Add(this.label31);
            this.uiGroupBox10.Controls.Add(this.label11);
            this.uiGroupBox10.Controls.Add(this.clcNgayNhapKhoHQLanDau);
            this.uiGroupBox10.Controls.Add(this.label12);
            this.uiGroupBox10.Controls.Add(this.clcNgayHangDuKienDenDi);
            this.uiGroupBox10.Controls.Add(this.label14);
            this.uiGroupBox10.Controls.Add(this.clcNgayPhatHanhVD);
            this.uiGroupBox10.Controls.Add(this.label7);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox10.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(840, 286);
            this.uiGroupBox10.TabIndex = 55;
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.AutoScroll = true;
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox11.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox11.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox11.Location = new System.Drawing.Point(0, 286);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(367, 427);
            this.uiGroupBox11.TabIndex = 56;
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.Controls.Add(this.txtDiaChiNguoiXuatKhau);
            this.uiGroupBox4.Controls.Add(this.txtMaNguoiXuatKhua);
            this.uiGroupBox4.Controls.Add(this.txtTenNguoiXuatKhau);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.label19);
            this.uiGroupBox4.Controls.Add(this.label20);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 141);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(361, 132);
            this.uiGroupBox4.TabIndex = 22;
            this.uiGroupBox4.Text = "Người xuất khẩu";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiNguoiXuatKhau
            // 
            this.txtDiaChiNguoiXuatKhau.Location = new System.Drawing.Point(63, 88);
            this.txtDiaChiNguoiXuatKhau.Multiline = true;
            this.txtDiaChiNguoiXuatKhau.Name = "txtDiaChiNguoiXuatKhau";
            this.txtDiaChiNguoiXuatKhau.Size = new System.Drawing.Size(283, 42);
            this.txtDiaChiNguoiXuatKhau.TabIndex = 2;
            this.txtDiaChiNguoiXuatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNguoiXuatKhua
            // 
            this.txtMaNguoiXuatKhua.Location = new System.Drawing.Point(63, 16);
            this.txtMaNguoiXuatKhua.Name = "txtMaNguoiXuatKhua";
            this.txtMaNguoiXuatKhua.Size = new System.Drawing.Size(283, 21);
            this.txtMaNguoiXuatKhua.TabIndex = 0;
            this.txtMaNguoiXuatKhua.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNguoiXuatKhau
            // 
            this.txtTenNguoiXuatKhau.Location = new System.Drawing.Point(63, 42);
            this.txtTenNguoiXuatKhau.Multiline = true;
            this.txtTenNguoiXuatKhau.Name = "txtTenNguoiXuatKhau";
            this.txtTenNguoiXuatKhau.Size = new System.Drawing.Size(283, 42);
            this.txtTenNguoiXuatKhau.TabIndex = 1;
            this.txtTenNguoiXuatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(21, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Mã";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 57);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(25, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Tên";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 103);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Địa chỉ";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.Controls.Add(this.txtDiaChiNguoiUyThac);
            this.uiGroupBox5.Controls.Add(this.txtMaNguoiUyThac);
            this.uiGroupBox5.Controls.Add(this.txtTenNguoiUyThac);
            this.uiGroupBox5.Controls.Add(this.label21);
            this.uiGroupBox5.Controls.Add(this.label22);
            this.uiGroupBox5.Controls.Add(this.label23);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 273);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(361, 151);
            this.uiGroupBox5.TabIndex = 25;
            this.uiGroupBox5.Text = "Người ủy thác";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiNguoiUyThac
            // 
            this.txtDiaChiNguoiUyThac.Location = new System.Drawing.Point(63, 98);
            this.txtDiaChiNguoiUyThac.Multiline = true;
            this.txtDiaChiNguoiUyThac.Name = "txtDiaChiNguoiUyThac";
            this.txtDiaChiNguoiUyThac.Size = new System.Drawing.Size(283, 42);
            this.txtDiaChiNguoiUyThac.TabIndex = 2;
            this.txtDiaChiNguoiUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNguoiUyThac
            // 
            this.txtMaNguoiUyThac.Location = new System.Drawing.Point(63, 16);
            this.txtMaNguoiUyThac.Name = "txtMaNguoiUyThac";
            this.txtMaNguoiUyThac.Size = new System.Drawing.Size(283, 21);
            this.txtMaNguoiUyThac.TabIndex = 0;
            this.txtMaNguoiUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNguoiUyThac
            // 
            this.txtTenNguoiUyThac.Location = new System.Drawing.Point(63, 47);
            this.txtTenNguoiUyThac.Multiline = true;
            this.txtTenNguoiUyThac.Name = "txtTenNguoiUyThac";
            this.txtTenNguoiUyThac.Size = new System.Drawing.Size(283, 42);
            this.txtTenNguoiUyThac.TabIndex = 1;
            this.txtTenNguoiUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(17, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(21, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Mã";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(17, 62);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(25, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Tên";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(17, 113);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Địa chỉ";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.Controls.Add(this.txtTriGia);
            this.uiGroupBox7.Controls.Add(this.txtTheTich);
            this.uiGroupBox7.Controls.Add(this.txtTongTrongLuong);
            this.uiGroupBox7.Controls.Add(this.txtSoLuong);
            this.uiGroupBox7.Controls.Add(this.ctrMaDVTTriGia);
            this.uiGroupBox7.Controls.Add(this.ctrMaDVTTheTich);
            this.uiGroupBox7.Controls.Add(this.ctrMaDVTTrongLuong);
            this.uiGroupBox7.Controls.Add(this.ctrMaDVTSoLuong);
            this.uiGroupBox7.Controls.Add(this.label38);
            this.uiGroupBox7.Controls.Add(this.label35);
            this.uiGroupBox7.Controls.Add(this.label36);
            this.uiGroupBox7.Controls.Add(this.label37);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(464, 116);
            this.uiGroupBox7.TabIndex = 20;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(8, 18);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(49, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Số lượng";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(8, 43);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(65, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "Trọng lượng";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(8, 69);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(45, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Thể tích";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(8, 94);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(43, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "Giá tiền";
            // 
            // ctrMaDVTSoLuong
            // 
            this.ctrMaDVTSoLuong.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDVTSoLuong.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTSoLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ctrMaDVTSoLuong.Code = "";
            this.ctrMaDVTSoLuong.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTSoLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTSoLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTSoLuong.IsOnlyWarning = false;
            this.ctrMaDVTSoLuong.IsValidate = true;
            this.ctrMaDVTSoLuong.Location = new System.Drawing.Point(202, 14);
            this.ctrMaDVTSoLuong.Name = "ctrMaDVTSoLuong";
            this.ctrMaDVTSoLuong.Name_VN = "";
            this.ctrMaDVTSoLuong.SetOnlyWarning = false;
            this.ctrMaDVTSoLuong.SetValidate = false;
            this.ctrMaDVTSoLuong.ShowColumnCode = true;
            this.ctrMaDVTSoLuong.ShowColumnName = false;
            this.ctrMaDVTSoLuong.Size = new System.Drawing.Size(61, 21);
            this.ctrMaDVTSoLuong.TabIndex = 1;
            this.ctrMaDVTSoLuong.TagName = "";
            this.ctrMaDVTSoLuong.Where = null;
            this.ctrMaDVTSoLuong.WhereCondition = "";
            // 
            // ctrMaDVTTrongLuong
            // 
            this.ctrMaDVTTrongLuong.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDVTTrongLuong.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTTrongLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E006;
            this.ctrMaDVTTrongLuong.Code = "";
            this.ctrMaDVTTrongLuong.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTTrongLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTTrongLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTTrongLuong.IsOnlyWarning = false;
            this.ctrMaDVTTrongLuong.IsValidate = true;
            this.ctrMaDVTTrongLuong.Location = new System.Drawing.Point(202, 39);
            this.ctrMaDVTTrongLuong.Name = "ctrMaDVTTrongLuong";
            this.ctrMaDVTTrongLuong.Name_VN = "";
            this.ctrMaDVTTrongLuong.SetOnlyWarning = false;
            this.ctrMaDVTTrongLuong.SetValidate = false;
            this.ctrMaDVTTrongLuong.ShowColumnCode = true;
            this.ctrMaDVTTrongLuong.ShowColumnName = false;
            this.ctrMaDVTTrongLuong.Size = new System.Drawing.Size(61, 21);
            this.ctrMaDVTTrongLuong.TabIndex = 3;
            this.ctrMaDVTTrongLuong.TagName = "";
            this.ctrMaDVTTrongLuong.Where = null;
            this.ctrMaDVTTrongLuong.WhereCondition = "";
            // 
            // ctrMaDVTTheTich
            // 
            this.ctrMaDVTTheTich.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDVTTheTich.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTTheTich.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E038;
            this.ctrMaDVTTheTich.Code = "";
            this.ctrMaDVTTheTich.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTTheTich.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTTheTich.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTTheTich.IsOnlyWarning = false;
            this.ctrMaDVTTheTich.IsValidate = true;
            this.ctrMaDVTTheTich.Location = new System.Drawing.Point(202, 64);
            this.ctrMaDVTTheTich.Name = "ctrMaDVTTheTich";
            this.ctrMaDVTTheTich.Name_VN = "";
            this.ctrMaDVTTheTich.SetOnlyWarning = false;
            this.ctrMaDVTTheTich.SetValidate = false;
            this.ctrMaDVTTheTich.ShowColumnCode = true;
            this.ctrMaDVTTheTich.ShowColumnName = false;
            this.ctrMaDVTTheTich.Size = new System.Drawing.Size(61, 21);
            this.ctrMaDVTTheTich.TabIndex = 5;
            this.ctrMaDVTTheTich.TagName = "";
            this.ctrMaDVTTheTich.Where = null;
            this.ctrMaDVTTheTich.WhereCondition = "";
            // 
            // ctrMaDVTTriGia
            // 
            this.ctrMaDVTTriGia.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDVTTriGia.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTTriGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaDVTTriGia.Code = "";
            this.ctrMaDVTTriGia.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTTriGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTTriGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTTriGia.IsOnlyWarning = false;
            this.ctrMaDVTTriGia.IsValidate = true;
            this.ctrMaDVTTriGia.Location = new System.Drawing.Point(202, 88);
            this.ctrMaDVTTriGia.Name = "ctrMaDVTTriGia";
            this.ctrMaDVTTriGia.Name_VN = "";
            this.ctrMaDVTTriGia.SetOnlyWarning = false;
            this.ctrMaDVTTriGia.SetValidate = false;
            this.ctrMaDVTTriGia.ShowColumnCode = true;
            this.ctrMaDVTTriGia.ShowColumnName = false;
            this.ctrMaDVTTriGia.Size = new System.Drawing.Size(61, 21);
            this.ctrMaDVTTriGia.TabIndex = 7;
            this.ctrMaDVTTriGia.TagName = "";
            this.ctrMaDVTTriGia.Where = null;
            this.ctrMaDVTTriGia.WhereCondition = "";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 20;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong.Location = new System.Drawing.Point(78, 14);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(118, 21);
            this.txtSoLuong.TabIndex = 0;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongTrongLuong
            // 
            this.txtTongTrongLuong.DecimalDigits = 20;
            this.txtTongTrongLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTrongLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongTrongLuong.Location = new System.Drawing.Point(78, 39);
            this.txtTongTrongLuong.MaxLength = 15;
            this.txtTongTrongLuong.Name = "txtTongTrongLuong";
            this.txtTongTrongLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTrongLuong.Size = new System.Drawing.Size(118, 21);
            this.txtTongTrongLuong.TabIndex = 2;
            this.txtTongTrongLuong.Text = "0";
            this.txtTongTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTheTich
            // 
            this.txtTheTich.DecimalDigits = 20;
            this.txtTheTich.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTheTich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTheTich.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTheTich.Location = new System.Drawing.Point(78, 64);
            this.txtTheTich.MaxLength = 15;
            this.txtTheTich.Name = "txtTheTich";
            this.txtTheTich.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTheTich.Size = new System.Drawing.Size(118, 21);
            this.txtTheTich.TabIndex = 4;
            this.txtTheTich.Text = "0";
            this.txtTheTich.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTheTich.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTheTich.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGia
            // 
            this.txtTriGia.DecimalDigits = 20;
            this.txtTriGia.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGia.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGia.Location = new System.Drawing.Point(78, 88);
            this.txtTriGia.MaxLength = 15;
            this.txtTriGia.Name = "txtTriGia";
            this.txtTriGia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGia.Size = new System.Drawing.Size(118, 21);
            this.txtTriGia.TabIndex = 6;
            this.txtTriGia.Text = "0";
            this.txtTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.Controls.Add(this.txtSoGiayPhep);
            this.uiGroupBox6.Controls.Add(this.label32);
            this.uiGroupBox6.Controls.Add(this.label33);
            this.uiGroupBox6.Controls.Add(this.label34);
            this.uiGroupBox6.Controls.Add(this.clcNgayCapPhep);
            this.uiGroupBox6.Controls.Add(this.clcNgayHetHanCapPhep);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 124);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(464, 91);
            this.uiGroupBox6.TabIndex = 23;
            this.uiGroupBox6.Text = "Giấy phép";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayHetHanCapPhep
            // 
            this.clcNgayHetHanCapPhep.Location = new System.Drawing.Point(78, 65);
            this.clcNgayHetHanCapPhep.Name = "clcNgayHetHanCapPhep";
            this.clcNgayHetHanCapPhep.ReadOnly = false;
            this.clcNgayHetHanCapPhep.Size = new System.Drawing.Size(100, 21);
            this.clcNgayHetHanCapPhep.TabIndex = 2;
            this.clcNgayHetHanCapPhep.TagName = "";
            this.clcNgayHetHanCapPhep.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHetHanCapPhep.WhereCondition = "";
            // 
            // clcNgayCapPhep
            // 
            this.clcNgayCapPhep.Location = new System.Drawing.Point(78, 40);
            this.clcNgayCapPhep.Name = "clcNgayCapPhep";
            this.clcNgayCapPhep.ReadOnly = false;
            this.clcNgayCapPhep.Size = new System.Drawing.Size(100, 21);
            this.clcNgayCapPhep.TabIndex = 1;
            this.clcNgayCapPhep.TagName = "";
            this.clcNgayCapPhep.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayCapPhep.WhereCondition = "";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(4, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(19, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "Số";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(4, 42);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 13);
            this.label33.TabIndex = 1;
            this.label33.Text = "Ngày cấp";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 65);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(72, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "Ngày hết hạn";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.Controls.Add(this.label43);
            this.uiGroupBox8.Controls.Add(this.label39);
            this.uiGroupBox8.Controls.Add(this.label40);
            this.uiGroupBox8.Controls.Add(this.label41);
            this.uiGroupBox8.Controls.Add(this.label42);
            this.uiGroupBox8.Controls.Add(this.ctrMaDanhDauDDKH5);
            this.uiGroupBox8.Controls.Add(this.ctrMaDanhDauDDKH4);
            this.uiGroupBox8.Controls.Add(this.ctrMaDanhDauDDKH3);
            this.uiGroupBox8.Controls.Add(this.ctrMaDanhDauDDKH2);
            this.uiGroupBox8.Controls.Add(this.ctrMaDanhDauDDKH1);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 215);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(464, 147);
            this.uiGroupBox8.TabIndex = 24;
            this.uiGroupBox8.Text = "Đánh dấu địa điểm khởi hành";
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDanhDauDDKH1
            // 
            this.ctrMaDanhDauDDKH1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDanhDauDDKH1.Appearance.Options.UseBackColor = true;
            this.ctrMaDanhDauDDKH1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A308;
            this.ctrMaDanhDauDDKH1.Code = "";
            this.ctrMaDanhDauDDKH1.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDanhDauDDKH1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDanhDauDDKH1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDanhDauDDKH1.IsOnlyWarning = false;
            this.ctrMaDanhDauDDKH1.IsValidate = true;
            this.ctrMaDanhDauDDKH1.Location = new System.Drawing.Point(76, 18);
            this.ctrMaDanhDauDDKH1.Name = "ctrMaDanhDauDDKH1";
            this.ctrMaDanhDauDDKH1.Name_VN = "";
            this.ctrMaDanhDauDDKH1.SetOnlyWarning = false;
            this.ctrMaDanhDauDDKH1.SetValidate = false;
            this.ctrMaDanhDauDDKH1.ShowColumnCode = true;
            this.ctrMaDanhDauDDKH1.ShowColumnName = false;
            this.ctrMaDanhDauDDKH1.Size = new System.Drawing.Size(206, 21);
            this.ctrMaDanhDauDDKH1.TabIndex = 0;
            this.ctrMaDanhDauDDKH1.TagName = "";
            this.ctrMaDanhDauDDKH1.Where = null;
            this.ctrMaDanhDauDDKH1.WhereCondition = "";
            // 
            // ctrMaDanhDauDDKH2
            // 
            this.ctrMaDanhDauDDKH2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDanhDauDDKH2.Appearance.Options.UseBackColor = true;
            this.ctrMaDanhDauDDKH2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A308;
            this.ctrMaDanhDauDDKH2.Code = "";
            this.ctrMaDanhDauDDKH2.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDanhDauDDKH2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDanhDauDDKH2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDanhDauDDKH2.IsOnlyWarning = false;
            this.ctrMaDanhDauDDKH2.IsValidate = true;
            this.ctrMaDanhDauDDKH2.Location = new System.Drawing.Point(76, 42);
            this.ctrMaDanhDauDDKH2.Name = "ctrMaDanhDauDDKH2";
            this.ctrMaDanhDauDDKH2.Name_VN = "";
            this.ctrMaDanhDauDDKH2.SetOnlyWarning = false;
            this.ctrMaDanhDauDDKH2.SetValidate = false;
            this.ctrMaDanhDauDDKH2.ShowColumnCode = true;
            this.ctrMaDanhDauDDKH2.ShowColumnName = false;
            this.ctrMaDanhDauDDKH2.Size = new System.Drawing.Size(206, 21);
            this.ctrMaDanhDauDDKH2.TabIndex = 1;
            this.ctrMaDanhDauDDKH2.TagName = "";
            this.ctrMaDanhDauDDKH2.Where = null;
            this.ctrMaDanhDauDDKH2.WhereCondition = "";
            // 
            // ctrMaDanhDauDDKH3
            // 
            this.ctrMaDanhDauDDKH3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDanhDauDDKH3.Appearance.Options.UseBackColor = true;
            this.ctrMaDanhDauDDKH3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A308;
            this.ctrMaDanhDauDDKH3.Code = "";
            this.ctrMaDanhDauDDKH3.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDanhDauDDKH3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDanhDauDDKH3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDanhDauDDKH3.IsOnlyWarning = false;
            this.ctrMaDanhDauDDKH3.IsValidate = true;
            this.ctrMaDanhDauDDKH3.Location = new System.Drawing.Point(76, 67);
            this.ctrMaDanhDauDDKH3.Name = "ctrMaDanhDauDDKH3";
            this.ctrMaDanhDauDDKH3.Name_VN = "";
            this.ctrMaDanhDauDDKH3.SetOnlyWarning = false;
            this.ctrMaDanhDauDDKH3.SetValidate = false;
            this.ctrMaDanhDauDDKH3.ShowColumnCode = true;
            this.ctrMaDanhDauDDKH3.ShowColumnName = false;
            this.ctrMaDanhDauDDKH3.Size = new System.Drawing.Size(206, 21);
            this.ctrMaDanhDauDDKH3.TabIndex = 2;
            this.ctrMaDanhDauDDKH3.TagName = "";
            this.ctrMaDanhDauDDKH3.Where = null;
            this.ctrMaDanhDauDDKH3.WhereCondition = "";
            // 
            // ctrMaDanhDauDDKH4
            // 
            this.ctrMaDanhDauDDKH4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDanhDauDDKH4.Appearance.Options.UseBackColor = true;
            this.ctrMaDanhDauDDKH4.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A308;
            this.ctrMaDanhDauDDKH4.Code = "";
            this.ctrMaDanhDauDDKH4.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDanhDauDDKH4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDanhDauDDKH4.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDanhDauDDKH4.IsOnlyWarning = false;
            this.ctrMaDanhDauDDKH4.IsValidate = true;
            this.ctrMaDanhDauDDKH4.Location = new System.Drawing.Point(76, 90);
            this.ctrMaDanhDauDDKH4.Name = "ctrMaDanhDauDDKH4";
            this.ctrMaDanhDauDDKH4.Name_VN = "";
            this.ctrMaDanhDauDDKH4.SetOnlyWarning = false;
            this.ctrMaDanhDauDDKH4.SetValidate = false;
            this.ctrMaDanhDauDDKH4.ShowColumnCode = true;
            this.ctrMaDanhDauDDKH4.ShowColumnName = false;
            this.ctrMaDanhDauDDKH4.Size = new System.Drawing.Size(206, 21);
            this.ctrMaDanhDauDDKH4.TabIndex = 3;
            this.ctrMaDanhDauDDKH4.TagName = "";
            this.ctrMaDanhDauDDKH4.Where = null;
            this.ctrMaDanhDauDDKH4.WhereCondition = "";
            // 
            // ctrMaDanhDauDDKH5
            // 
            this.ctrMaDanhDauDDKH5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDanhDauDDKH5.Appearance.Options.UseBackColor = true;
            this.ctrMaDanhDauDDKH5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A308;
            this.ctrMaDanhDauDDKH5.Code = "";
            this.ctrMaDanhDauDDKH5.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDanhDauDDKH5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDanhDauDDKH5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDanhDauDDKH5.IsOnlyWarning = false;
            this.ctrMaDanhDauDDKH5.IsValidate = true;
            this.ctrMaDanhDauDDKH5.Location = new System.Drawing.Point(76, 115);
            this.ctrMaDanhDauDDKH5.Name = "ctrMaDanhDauDDKH5";
            this.ctrMaDanhDauDDKH5.Name_VN = "";
            this.ctrMaDanhDauDDKH5.SetOnlyWarning = false;
            this.ctrMaDanhDauDDKH5.SetValidate = false;
            this.ctrMaDanhDauDDKH5.ShowColumnCode = true;
            this.ctrMaDanhDauDDKH5.ShowColumnName = false;
            this.ctrMaDanhDauDDKH5.Size = new System.Drawing.Size(206, 21);
            this.ctrMaDanhDauDDKH5.TabIndex = 4;
            this.ctrMaDanhDauDDKH5.TagName = "";
            this.ctrMaDanhDauDDKH5.Where = null;
            this.ctrMaDanhDauDDKH5.WhereCondition = "";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(8, 18);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(13, 13);
            this.label42.TabIndex = 1;
            this.label42.Text = "1";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(8, 42);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(13, 13);
            this.label41.TabIndex = 1;
            this.label41.Text = "2";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(8, 67);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(13, 13);
            this.label40.TabIndex = 1;
            this.label40.Text = "3";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 90);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(13, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "4";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(8, 115);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(13, 13);
            this.label43.TabIndex = 1;
            this.label43.Text = "5";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.Controls.Add(this.txtGhiChu);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox9.Location = new System.Drawing.Point(3, 362);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(464, 62);
            this.uiGroupBox9.TabIndex = 26;
            this.uiGroupBox9.Text = "Ghi chú";
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox12.AutoScroll = true;
            this.uiGroupBox12.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox12.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox12.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox12.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox12.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox12.Location = new System.Drawing.Point(370, 286);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(470, 427);
            this.uiGroupBox12.TabIndex = 56;
            this.uiGroupBox12.VisualStyleManager = this.vsmMain;
            // 
            // txtSoGiayPhep
            // 
            this.txtSoGiayPhep.Location = new System.Drawing.Point(78, 14);
            this.txtSoGiayPhep.Name = "txtSoGiayPhep";
            this.txtSoGiayPhep.Size = new System.Drawing.Size(185, 21);
            this.txtSoGiayPhep.TabIndex = 0;
            this.txtSoGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGhiChu.Location = new System.Drawing.Point(3, 17);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(458, 42);
            this.txtGhiChu.TabIndex = 0;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // VNACC_ChuyenCuaKhau_HangHoaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 762);
            this.helpProvider1.SetHelpKeyword(this, "BaseFormHaveGuidPanel.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ChuyenCuaKhau_HangHoaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin vận đơn";
            this.Load += new System.EventHandler(this.VNACC_ChuyenCuaKhau_HangHoaForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiNhapKhau;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayNhapKhoHQLanDau;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHangDuKienDenDi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayPhatHanhVD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenPhuongTienVC;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPhuongTienVC;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieuVaSoHieu;
        private Janus.Windows.GridEX.EditControls.EditBox txtMoTaHangHoa;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnLuu;
        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.EditControls.UIButton btnThoat;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiemDichVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiemXuatPhatVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaHS;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapLuat5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapLuat4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapLuat3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapLuat2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapLuat1;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhanLoaiSanPhan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrLoaiHangHoa;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaNuocSanXuat;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiUyThac;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiXuatKhua;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiXuatKhau;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox12;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDanhDauDDKH5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDanhDauDDKH4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDanhDauDDKH3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDanhDauDDKH2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDanhDauDDKH1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayPhep;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayCapPhep;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHetHanCapPhep;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGia;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTheTich;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTrongLuong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTriGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTheTich;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTrongLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTSoLuong;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
    }
}