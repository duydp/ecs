﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TKBoSung_ThueHangHoaManageForm : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKSuaBoSung = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();
        public List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> listTKBoSung = new List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung>();

        public VNACC_TKBoSung_ThueHangHoaManageForm()
        {
            InitializeComponent();
        }

        private void VNACC_GiayPhep_TheoDoi_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                this.Text = "Theo dõi Tờ khai sửa đổi bổ sung thuế hàng hóa - AMA";

                //Nhom xu ly ho so
                this.grdList.RootTable.Columns["NhomXuLyHoSoID"].HasValueList = true;
                Janus.Windows.GridEX.GridEXValueListItemCollection vlNhomXuLyHoSo = this.grdList.RootTable.Columns["NhomXuLyHoSoID"].ValueList;
                System.Data.DataView view = VNACC_Category_CustomsSubSection.SelectAll().Tables[0].DefaultView;
                view.Sort = "CustomsSubSectionCode ASC";
                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    vlNhomXuLyHoSo.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"].ToString(), row["Name"].ToString()));
                }

                grdList.DataSource = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.SelectAll().Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void gridEX1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            int id = Convert.ToInt32(e.Row.Cells["ID"].Value);

            VNACC_TKBoSung_ThueHangHoaForm f = new VNACC_TKBoSung_ThueHangHoaForm();
            f.TKBoSung = getGiayPhepID(id);
            f.ShowDialog();
        }

        private KDT_VNACC_ToKhaiMauDich_KhaiBoSung getGiayPhepID(long id)
        {
            listTKBoSung = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.SelectCollectionAll();

            foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung gp in listTKBoSung)
            {
                if (gp.ID == id)
                {
                    gp.LoadHangCollection();

                    foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue hang in gp.HangCollection)
                    {
                        hang.LoadHangThuKhacCollection();
                    }

                    return gp;
                }
            }
            return null;
        }

        private void grdList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                TKSuaBoSung = getGiayPhepID(id);
                if (ShowMessage("Bạn có muốn xóa tờ khai sửa đổi bổ sung này không?", true) == "Yes")
                {
                    foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue hang in TKSuaBoSung.HangCollection)
                    {
                        hang.Delete();
                    }
                    TKSuaBoSung.Delete();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soTN = txtSoTiepNhan.Text.Trim();

                string loaiHinh = cbbPhanLoaiXuatNhap.SelectedValue.ToString();
                string where = String.Empty;
                if (soTN != "")
                    where = "SoTiepNhan = " + soTN + " and ";

                if (loaiHinh != "")
                    where = where + "MaLoaiHinh = '" + loaiHinh + "'";

                listTKBoSung.Clear();
                if (where == "")
                {
                    listTKBoSung = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.SelectCollectionAll();
                }
                else
                {
                    listTKBoSung = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.SelectCollectionDynamic(where, "ID");
                }
                grdList.DataSource = listTKBoSung;
                grdList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

    }
}
