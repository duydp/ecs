﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Người khai HQ
    /// </summary>
    public class GC_Product 
    {
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }

        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }

        [XmlElement("PaymentDocuments")]
        public List<GC_PaymentDocuments> PaymentDocuments { get; set; }

    }
}
