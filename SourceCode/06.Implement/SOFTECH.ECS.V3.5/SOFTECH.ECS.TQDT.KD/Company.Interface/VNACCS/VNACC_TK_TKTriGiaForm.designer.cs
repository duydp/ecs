﻿namespace Company.Interface
{
    partial class VNACC_TK_TKTriGiaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TK_TKTriGiaForm));
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnPhanBoPhi = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaPhanLoai = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrTen = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTongHSPB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.txtMaPhanLoaiTongGiaCoBan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrMaTTPhiVC = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTPhiBH = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiPhiBH = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiPhiVC = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTHieuChinhTriGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiTriGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtChiTietKhaiTriGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongPhapDieuChinhTriGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaPhanLoaiDieuChinhTriGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhanLoaiCongThucChuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoDangKyBH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTongHeSoPhanBoTG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtGiaHieuChinhTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTiepNhanTKTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiVanChuyen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTriGiaKhoanDC = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox)).BeginInit();
            this.uiGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 538), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 538);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 514);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 514);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(745, 538);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.linkLabel1);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnPhanBoPhi);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 504);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(745, 34);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(7, 13);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(212, 13);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Hướng dẫn phân bổ phí cho các dòng hàng";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(589, 9);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 8;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(670, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnPhanBoPhi
            // 
            this.btnPhanBoPhi.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnPhanBoPhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhanBoPhi.Image = ((System.Drawing.Image)(resources.GetObject("btnPhanBoPhi.Image")));
            this.btnPhanBoPhi.ImageIndex = 4;
            this.btnPhanBoPhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnPhanBoPhi.Location = new System.Drawing.Point(389, 9);
            this.btnPhanBoPhi.Name = "btnPhanBoPhi";
            this.btnPhanBoPhi.Size = new System.Drawing.Size(107, 23);
            this.btnPhanBoPhi.TabIndex = 0;
            this.btnPhanBoPhi.Text = "Phân bổ phí";
            this.btnPhanBoPhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnPhanBoPhi.Click += new System.EventHandler(this.btnPhanBoPhi_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGhi.Location = new System.Drawing.Point(504, 9);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // uiGroupBox
            // 
            this.uiGroupBox.AutoScroll = true;
            this.uiGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox.Controls.Add(this.ctrMaPhanLoai);
            this.uiGroupBox.Controls.Add(this.ctrTen);
            this.uiGroupBox.Controls.Add(this.ctrMaTT);
            this.uiGroupBox.Controls.Add(this.txtTriGiaKhoanDC);
            this.uiGroupBox.Controls.Add(this.txtTongHSPB);
            this.uiGroupBox.Controls.Add(this.btnAdd);
            this.uiGroupBox.Controls.Add(this.txtMaPhanLoaiTongGiaCoBan);
            this.uiGroupBox.Controls.Add(this.ctrMaTTPhiVC);
            this.uiGroupBox.Controls.Add(this.ctrMaTTPhiBH);
            this.uiGroupBox.Controls.Add(this.ctrMaPhanLoaiPhiBH);
            this.uiGroupBox.Controls.Add(this.ctrMaPhanLoaiPhiVC);
            this.uiGroupBox.Controls.Add(this.ctrMaTTHieuChinhTriGia);
            this.uiGroupBox.Controls.Add(this.ctrMaPhanLoaiTriGia);
            this.uiGroupBox.Controls.Add(this.label18);
            this.uiGroupBox.Controls.Add(this.grList);
            this.uiGroupBox.Controls.Add(this.label17);
            this.uiGroupBox.Controls.Add(this.label15);
            this.uiGroupBox.Controls.Add(this.label14);
            this.uiGroupBox.Controls.Add(this.label13);
            this.uiGroupBox.Controls.Add(this.label7);
            this.uiGroupBox.Controls.Add(this.label10);
            this.uiGroupBox.Controls.Add(this.label3);
            this.uiGroupBox.Controls.Add(this.label4);
            this.uiGroupBox.Controls.Add(this.label12);
            this.uiGroupBox.Controls.Add(this.label11);
            this.uiGroupBox.Controls.Add(this.label9);
            this.uiGroupBox.Controls.Add(this.label2);
            this.uiGroupBox.Controls.Add(this.label6);
            this.uiGroupBox.Controls.Add(this.txtChiTietKhaiTriGia);
            this.uiGroupBox.Controls.Add(this.txtPhuongPhapDieuChinhTriGia);
            this.uiGroupBox.Controls.Add(this.txtMaPhanLoaiDieuChinhTriGia);
            this.uiGroupBox.Controls.Add(this.txtPhanLoaiCongThucChuan);
            this.uiGroupBox.Controls.Add(this.label8);
            this.uiGroupBox.Controls.Add(this.txtSoDangKyBH);
            this.uiGroupBox.Controls.Add(this.label5);
            this.uiGroupBox.Controls.Add(this.label1);
            this.uiGroupBox.Controls.Add(this.txtTongHeSoPhanBoTG);
            this.uiGroupBox.Controls.Add(this.txtPhiBaoHiem);
            this.uiGroupBox.Controls.Add(this.label16);
            this.uiGroupBox.Controls.Add(this.txtGiaHieuChinhTriGia);
            this.uiGroupBox.Controls.Add(this.txtSoTiepNhanTKTriGia);
            this.uiGroupBox.Controls.Add(this.txtPhiVanChuyen);
            this.uiGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox.Name = "uiGroupBox";
            this.uiGroupBox.Size = new System.Drawing.Size(745, 504);
            this.uiGroupBox.TabIndex = 0;
            this.uiGroupBox.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaPhanLoai
            // 
            this.ctrMaPhanLoai.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaPhanLoai.Appearance.Options.UseBackColor = true;
            this.ctrMaPhanLoai.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E014;
            this.ctrMaPhanLoai.Code = "";
            this.ctrMaPhanLoai.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaPhanLoai.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoai.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoai.IsOnlyWarning = false;
            this.ctrMaPhanLoai.IsValidate = true;
            this.ctrMaPhanLoai.Location = new System.Drawing.Point(242, 276);
            this.ctrMaPhanLoai.Name = "ctrMaPhanLoai";
            this.ctrMaPhanLoai.Name_VN = "";
            this.ctrMaPhanLoai.SetOnlyWarning = false;
            this.ctrMaPhanLoai.SetValidate = false;
            this.ctrMaPhanLoai.ShowColumnCode = true;
            this.ctrMaPhanLoai.ShowColumnName = false;
            this.ctrMaPhanLoai.Size = new System.Drawing.Size(86, 21);
            this.ctrMaPhanLoai.TabIndex = 35;
            this.ctrMaPhanLoai.TagName = "";
            this.ctrMaPhanLoai.Where = null;
            this.ctrMaPhanLoai.WhereCondition = "";
            // 
            // ctrTen
            // 
            this.ctrTen.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrTen.Appearance.Options.UseBackColor = true;
            this.ctrTen.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A401;
            this.ctrTen.Code = "";
            this.ctrTen.ColorControl = System.Drawing.Color.Empty;
            this.ctrTen.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrTen.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrTen.IsOnlyWarning = false;
            this.ctrTen.IsValidate = true;
            this.ctrTen.Location = new System.Drawing.Point(125, 276);
            this.ctrTen.Name = "ctrTen";
            this.ctrTen.Name_VN = "";
            this.ctrTen.SetOnlyWarning = false;
            this.ctrTen.SetValidate = false;
            this.ctrTen.ShowColumnCode = true;
            this.ctrTen.ShowColumnName = false;
            this.ctrTen.Size = new System.Drawing.Size(94, 21);
            this.ctrTen.TabIndex = 35;
            this.ctrTen.TagName = "";
            this.ctrTen.Where = null;
            this.ctrTen.WhereCondition = "";
            // 
            // ctrMaTT
            // 
            this.ctrMaTT.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTT.Appearance.Options.UseBackColor = true;
            this.ctrMaTT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT.Code = "";
            this.ctrMaTT.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT.IsOnlyWarning = false;
            this.ctrMaTT.IsValidate = true;
            this.ctrMaTT.Location = new System.Drawing.Point(358, 276);
            this.ctrMaTT.Name = "ctrMaTT";
            this.ctrMaTT.Name_VN = "";
            this.ctrMaTT.SetOnlyWarning = false;
            this.ctrMaTT.SetValidate = false;
            this.ctrMaTT.ShowColumnCode = true;
            this.ctrMaTT.ShowColumnName = false;
            this.ctrMaTT.Size = new System.Drawing.Size(82, 21);
            this.ctrMaTT.TabIndex = 34;
            this.ctrMaTT.TagName = "";
            this.ctrMaTT.Where = null;
            this.ctrMaTT.WhereCondition = "";
            // 
            // txtTongHSPB
            // 
            this.txtTongHSPB.BackColor = System.Drawing.SystemColors.Info;
            this.txtTongHSPB.DecimalDigits = 20;
            this.txtTongHSPB.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongHSPB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongHSPB.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongHSPB.Location = new System.Drawing.Point(605, 276);
            this.txtTongHSPB.MaxLength = 15;
            this.txtTongHSPB.Name = "txtTongHSPB";
            this.txtTongHSPB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongHSPB.Size = new System.Drawing.Size(128, 21);
            this.txtTongHSPB.TabIndex = 33;
            this.txtTongHSPB.Text = "0";
            this.txtTongHSPB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongHSPB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongHSPB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageIndex = 4;
            this.btnAdd.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAdd.Location = new System.Drawing.Point(125, 307);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(80, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Ghi";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtMaPhanLoaiTongGiaCoBan
            // 
            this.txtMaPhanLoaiTongGiaCoBan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaPhanLoaiTongGiaCoBan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaPhanLoaiTongGiaCoBan.Enabled = false;
            this.txtMaPhanLoaiTongGiaCoBan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPhanLoaiTongGiaCoBan.Location = new System.Drawing.Point(608, 166);
            this.txtMaPhanLoaiTongGiaCoBan.MaxLength = 12;
            this.txtMaPhanLoaiTongGiaCoBan.Name = "txtMaPhanLoaiTongGiaCoBan";
            this.txtMaPhanLoaiTongGiaCoBan.ReadOnly = true;
            this.txtMaPhanLoaiTongGiaCoBan.Size = new System.Drawing.Size(54, 21);
            this.txtMaPhanLoaiTongGiaCoBan.TabIndex = 15;
            this.txtMaPhanLoaiTongGiaCoBan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaPhanLoaiTongGiaCoBan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrMaTTPhiVC
            // 
            this.ctrMaTTPhiVC.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTPhiVC.Appearance.Options.UseBackColor = true;
            this.ctrMaTTPhiVC.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTPhiVC.Code = "";
            this.ctrMaTTPhiVC.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTPhiVC.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTPhiVC.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTPhiVC.IsOnlyWarning = false;
            this.ctrMaTTPhiVC.IsValidate = true;
            this.ctrMaTTPhiVC.Location = new System.Drawing.Point(605, 114);
            this.ctrMaTTPhiVC.Name = "ctrMaTTPhiVC";
            this.ctrMaTTPhiVC.Name_VN = "";
            this.ctrMaTTPhiVC.SetOnlyWarning = false;
            this.ctrMaTTPhiVC.SetValidate = false;
            this.ctrMaTTPhiVC.ShowColumnCode = true;
            this.ctrMaTTPhiVC.ShowColumnName = false;
            this.ctrMaTTPhiVC.Size = new System.Drawing.Size(57, 21);
            this.ctrMaTTPhiVC.TabIndex = 9;
            this.ctrMaTTPhiVC.TagName = "";
            this.ctrMaTTPhiVC.Where = null;
            this.ctrMaTTPhiVC.WhereCondition = "";
            // 
            // ctrMaTTPhiBH
            // 
            this.ctrMaTTPhiBH.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTPhiBH.Appearance.Options.UseBackColor = true;
            this.ctrMaTTPhiBH.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTPhiBH.Code = "";
            this.ctrMaTTPhiBH.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTPhiBH.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTPhiBH.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTPhiBH.IsOnlyWarning = false;
            this.ctrMaTTPhiBH.IsValidate = true;
            this.ctrMaTTPhiBH.Location = new System.Drawing.Point(605, 138);
            this.ctrMaTTPhiBH.Name = "ctrMaTTPhiBH";
            this.ctrMaTTPhiBH.Name_VN = "";
            this.ctrMaTTPhiBH.SetOnlyWarning = false;
            this.ctrMaTTPhiBH.SetValidate = false;
            this.ctrMaTTPhiBH.ShowColumnCode = true;
            this.ctrMaTTPhiBH.ShowColumnName = false;
            this.ctrMaTTPhiBH.Size = new System.Drawing.Size(57, 21);
            this.ctrMaTTPhiBH.TabIndex = 12;
            this.ctrMaTTPhiBH.TagName = "";
            this.ctrMaTTPhiBH.Where = null;
            this.ctrMaTTPhiBH.WhereCondition = "";
            // 
            // ctrMaPhanLoaiPhiBH
            // 
            this.ctrMaPhanLoaiPhiBH.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaPhanLoaiPhiBH.Appearance.Options.UseBackColor = true;
            this.ctrMaPhanLoaiPhiBH.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E013;
            this.ctrMaPhanLoaiPhiBH.Code = "";
            this.ctrMaPhanLoaiPhiBH.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaPhanLoaiPhiBH.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiPhiBH.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoaiPhiBH.IsOnlyWarning = false;
            this.ctrMaPhanLoaiPhiBH.IsValidate = true;
            this.ctrMaPhanLoaiPhiBH.Location = new System.Drawing.Point(125, 135);
            this.ctrMaPhanLoaiPhiBH.Name = "ctrMaPhanLoaiPhiBH";
            this.ctrMaPhanLoaiPhiBH.Name_VN = "";
            this.ctrMaPhanLoaiPhiBH.SetOnlyWarning = false;
            this.ctrMaPhanLoaiPhiBH.SetValidate = false;
            this.ctrMaPhanLoaiPhiBH.ShowColumnCode = true;
            this.ctrMaPhanLoaiPhiBH.ShowColumnName = true;
            this.ctrMaPhanLoaiPhiBH.Size = new System.Drawing.Size(244, 26);
            this.ctrMaPhanLoaiPhiBH.TabIndex = 10;
            this.ctrMaPhanLoaiPhiBH.TagName = "";
            this.ctrMaPhanLoaiPhiBH.Where = null;
            this.ctrMaPhanLoaiPhiBH.WhereCondition = "";
            // 
            // ctrMaPhanLoaiPhiVC
            // 
            this.ctrMaPhanLoaiPhiVC.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaPhanLoaiPhiVC.Appearance.Options.UseBackColor = true;
            this.ctrMaPhanLoaiPhiVC.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A557;
            this.ctrMaPhanLoaiPhiVC.Code = "";
            this.ctrMaPhanLoaiPhiVC.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaPhanLoaiPhiVC.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiPhiVC.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoaiPhiVC.IsOnlyWarning = false;
            this.ctrMaPhanLoaiPhiVC.IsValidate = true;
            this.ctrMaPhanLoaiPhiVC.Location = new System.Drawing.Point(125, 111);
            this.ctrMaPhanLoaiPhiVC.Name = "ctrMaPhanLoaiPhiVC";
            this.ctrMaPhanLoaiPhiVC.Name_VN = "";
            this.ctrMaPhanLoaiPhiVC.SetOnlyWarning = false;
            this.ctrMaPhanLoaiPhiVC.SetValidate = false;
            this.ctrMaPhanLoaiPhiVC.ShowColumnCode = true;
            this.ctrMaPhanLoaiPhiVC.ShowColumnName = true;
            this.ctrMaPhanLoaiPhiVC.Size = new System.Drawing.Size(244, 26);
            this.ctrMaPhanLoaiPhiVC.TabIndex = 7;
            this.ctrMaPhanLoaiPhiVC.TagName = "";
            this.ctrMaPhanLoaiPhiVC.Where = null;
            this.ctrMaPhanLoaiPhiVC.WhereCondition = "";
            // 
            // ctrMaTTHieuChinhTriGia
            // 
            this.ctrMaTTHieuChinhTriGia.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTHieuChinhTriGia.Appearance.Options.UseBackColor = true;
            this.ctrMaTTHieuChinhTriGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTHieuChinhTriGia.Code = "";
            this.ctrMaTTHieuChinhTriGia.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTHieuChinhTriGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTHieuChinhTriGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTHieuChinhTriGia.IsOnlyWarning = false;
            this.ctrMaTTHieuChinhTriGia.IsValidate = true;
            this.ctrMaTTHieuChinhTriGia.Location = new System.Drawing.Point(312, 65);
            this.ctrMaTTHieuChinhTriGia.Name = "ctrMaTTHieuChinhTriGia";
            this.ctrMaTTHieuChinhTriGia.Name_VN = "";
            this.ctrMaTTHieuChinhTriGia.SetOnlyWarning = false;
            this.ctrMaTTHieuChinhTriGia.SetValidate = false;
            this.ctrMaTTHieuChinhTriGia.ShowColumnCode = true;
            this.ctrMaTTHieuChinhTriGia.ShowColumnName = false;
            this.ctrMaTTHieuChinhTriGia.Size = new System.Drawing.Size(57, 21);
            this.ctrMaTTHieuChinhTriGia.TabIndex = 4;
            this.ctrMaTTHieuChinhTriGia.TagName = "";
            this.ctrMaTTHieuChinhTriGia.Where = null;
            this.ctrMaTTHieuChinhTriGia.WhereCondition = "";
            // 
            // ctrMaPhanLoaiTriGia
            // 
            this.ctrMaPhanLoaiTriGia.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaPhanLoaiTriGia.Appearance.Options.UseBackColor = true;
            this.ctrMaPhanLoaiTriGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E012;
            this.ctrMaPhanLoaiTriGia.Code = "";
            this.ctrMaPhanLoaiTriGia.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaPhanLoaiTriGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiTriGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoaiTriGia.IsOnlyWarning = false;
            this.ctrMaPhanLoaiTriGia.IsValidate = true;
            this.ctrMaPhanLoaiTriGia.Location = new System.Drawing.Point(125, 8);
            this.ctrMaPhanLoaiTriGia.Name = "ctrMaPhanLoaiTriGia";
            this.ctrMaPhanLoaiTriGia.Name_VN = "";
            this.ctrMaPhanLoaiTriGia.SetOnlyWarning = false;
            this.ctrMaPhanLoaiTriGia.SetValidate = false;
            this.ctrMaPhanLoaiTriGia.ShowColumnCode = true;
            this.ctrMaPhanLoaiTriGia.ShowColumnName = true;
            this.ctrMaPhanLoaiTriGia.Size = new System.Drawing.Size(178, 21);
            this.ctrMaPhanLoaiTriGia.TabIndex = 0;
            this.ctrMaPhanLoaiTriGia.TagName = "";
            this.ctrMaPhanLoaiTriGia.Where = null;
            this.ctrMaPhanLoaiTriGia.WhereCondition = "";
            // 
            // grList
            // 
            this.grList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.ColumnAutoResize = true;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grList.GroupByBoxVisible = false;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.LineAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(3, 345);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(739, 156);
            this.grList.TabIndex = 17;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.Validating += new System.ComponentModel.CancelEventHandler(this.grList_Validating);
            this.grList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grList_DeletingRecord);
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(605, 252);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "Tổng hệ số phân bổ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(358, 252);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "Mã tiền tệ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(242, 252);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Mã phân loại";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(125, 252);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Mã tên";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Chi tiết khai trị giá";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(373, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(148, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Mã phân loại điều chỉnh trị giá";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Phí bảo hiểm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Giá cơ sở hiệu chỉnh TG";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(376, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Số tiền phí BH";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(376, 118);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Số tiền phí VC";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(373, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Phân loại công thức ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Phí vận chuyển";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(317, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Tổng hệ số phân bổ trị giá";
            // 
            // txtChiTietKhaiTriGia
            // 
            this.txtChiTietKhaiTriGia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtChiTietKhaiTriGia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtChiTietKhaiTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChiTietKhaiTriGia.Location = new System.Drawing.Point(125, 193);
            this.txtChiTietKhaiTriGia.MaxLength = 250;
            this.txtChiTietKhaiTriGia.Multiline = true;
            this.txtChiTietKhaiTriGia.Name = "txtChiTietKhaiTriGia";
            this.txtChiTietKhaiTriGia.Size = new System.Drawing.Size(537, 43);
            this.txtChiTietKhaiTriGia.TabIndex = 16;
            this.txtChiTietKhaiTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChiTietKhaiTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhuongPhapDieuChinhTriGia
            // 
            this.txtPhuongPhapDieuChinhTriGia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPhuongPhapDieuChinhTriGia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPhuongPhapDieuChinhTriGia.Enabled = false;
            this.txtPhuongPhapDieuChinhTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuongPhapDieuChinhTriGia.Location = new System.Drawing.Point(169, 89);
            this.txtPhuongPhapDieuChinhTriGia.MaxLength = 12;
            this.txtPhuongPhapDieuChinhTriGia.Name = "txtPhuongPhapDieuChinhTriGia";
            this.txtPhuongPhapDieuChinhTriGia.ReadOnly = true;
            this.txtPhuongPhapDieuChinhTriGia.Size = new System.Drawing.Size(435, 21);
            this.txtPhuongPhapDieuChinhTriGia.TabIndex = 6;
            this.txtPhuongPhapDieuChinhTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhuongPhapDieuChinhTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaPhanLoaiDieuChinhTriGia
            // 
            this.txtMaPhanLoaiDieuChinhTriGia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaPhanLoaiDieuChinhTriGia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaPhanLoaiDieuChinhTriGia.Enabled = false;
            this.txtMaPhanLoaiDieuChinhTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPhanLoaiDieuChinhTriGia.Location = new System.Drawing.Point(528, 61);
            this.txtMaPhanLoaiDieuChinhTriGia.MaxLength = 12;
            this.txtMaPhanLoaiDieuChinhTriGia.Name = "txtMaPhanLoaiDieuChinhTriGia";
            this.txtMaPhanLoaiDieuChinhTriGia.ReadOnly = true;
            this.txtMaPhanLoaiDieuChinhTriGia.Size = new System.Drawing.Size(76, 21);
            this.txtMaPhanLoaiDieuChinhTriGia.TabIndex = 5;
            this.txtMaPhanLoaiDieuChinhTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaPhanLoaiDieuChinhTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhanLoaiCongThucChuan
            // 
            this.txtPhanLoaiCongThucChuan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPhanLoaiCongThucChuan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPhanLoaiCongThucChuan.Enabled = false;
            this.txtPhanLoaiCongThucChuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhanLoaiCongThucChuan.Location = new System.Drawing.Point(528, 34);
            this.txtPhanLoaiCongThucChuan.MaxLength = 12;
            this.txtPhanLoaiCongThucChuan.Name = "txtPhanLoaiCongThucChuan";
            this.txtPhanLoaiCongThucChuan.ReadOnly = true;
            this.txtPhanLoaiCongThucChuan.Size = new System.Drawing.Size(76, 21);
            this.txtPhanLoaiCongThucChuan.TabIndex = 2;
            this.txtPhanLoaiCongThucChuan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhanLoaiCongThucChuan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Phương pháp điều chỉnh trị giá";
            // 
            // txtSoDangKyBH
            // 
            this.txtSoDangKyBH.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDangKyBH.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDangKyBH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDangKyBH.Location = new System.Drawing.Point(158, 166);
            this.txtSoDangKyBH.MaxLength = 12;
            this.txtSoDangKyBH.Name = "txtSoDangKyBH";
            this.txtSoDangKyBH.Size = new System.Drawing.Size(106, 21);
            this.txtSoDangKyBH.TabIndex = 13;
            this.txtSoDangKyBH.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDangKyBH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Số đăng ký bảo hiểm tổng hợp";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Số tiếp nhận tờ khai trị giá tổng hợp";
            // 
            // txtTongHeSoPhanBoTG
            // 
            this.txtTongHeSoPhanBoTG.DecimalDigits = 20;
            this.txtTongHeSoPhanBoTG.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongHeSoPhanBoTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongHeSoPhanBoTG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongHeSoPhanBoTG.Location = new System.Drawing.Point(459, 165);
            this.txtTongHeSoPhanBoTG.MaxLength = 15;
            this.txtTongHeSoPhanBoTG.Name = "txtTongHeSoPhanBoTG";
            this.txtTongHeSoPhanBoTG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongHeSoPhanBoTG.Size = new System.Drawing.Size(145, 21);
            this.txtTongHeSoPhanBoTG.TabIndex = 14;
            this.txtTongHeSoPhanBoTG.Text = "0";
            this.txtTongHeSoPhanBoTG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongHeSoPhanBoTG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongHeSoPhanBoTG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.DecimalDigits = 20;
            this.txtPhiBaoHiem.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtPhiBaoHiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiBaoHiem.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(459, 138);
            this.txtPhiBaoHiem.MaxLength = 15;
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(145, 21);
            this.txtPhiBaoHiem.TabIndex = 11;
            this.txtPhiBaoHiem.Text = "0";
            this.txtPhiBaoHiem.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhiBaoHiem.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPhiBaoHiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(7, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Mã phân loại khai trị giá";
            // 
            // txtGiaHieuChinhTriGia
            // 
            this.txtGiaHieuChinhTriGia.DecimalDigits = 20;
            this.txtGiaHieuChinhTriGia.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtGiaHieuChinhTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaHieuChinhTriGia.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtGiaHieuChinhTriGia.Location = new System.Drawing.Point(125, 65);
            this.txtGiaHieuChinhTriGia.MaxLength = 15;
            this.txtGiaHieuChinhTriGia.Name = "txtGiaHieuChinhTriGia";
            this.txtGiaHieuChinhTriGia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtGiaHieuChinhTriGia.Size = new System.Drawing.Size(178, 21);
            this.txtGiaHieuChinhTriGia.TabIndex = 3;
            this.txtGiaHieuChinhTriGia.Text = "0";
            this.txtGiaHieuChinhTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGiaHieuChinhTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtGiaHieuChinhTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTiepNhanTKTriGia
            // 
            this.txtSoTiepNhanTKTriGia.DecimalDigits = 20;
            this.txtSoTiepNhanTKTriGia.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTiepNhanTKTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhanTKTriGia.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTiepNhanTKTriGia.Location = new System.Drawing.Point(186, 37);
            this.txtSoTiepNhanTKTriGia.MaxLength = 15;
            this.txtSoTiepNhanTKTriGia.Name = "txtSoTiepNhanTKTriGia";
            this.txtSoTiepNhanTKTriGia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTiepNhanTKTriGia.Size = new System.Drawing.Size(117, 21);
            this.txtSoTiepNhanTKTriGia.TabIndex = 1;
            this.txtSoTiepNhanTKTriGia.Text = "0";
            this.txtSoTiepNhanTKTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhanTKTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTiepNhanTKTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhiVanChuyen
            // 
            this.txtPhiVanChuyen.DecimalDigits = 20;
            this.txtPhiVanChuyen.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtPhiVanChuyen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiVanChuyen.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtPhiVanChuyen.Location = new System.Drawing.Point(459, 114);
            this.txtPhiVanChuyen.MaxLength = 15;
            this.txtPhiVanChuyen.Name = "txtPhiVanChuyen";
            this.txtPhiVanChuyen.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPhiVanChuyen.Size = new System.Drawing.Size(145, 21);
            this.txtPhiVanChuyen.TabIndex = 8;
            this.txtPhiVanChuyen.Text = "0";
            this.txtPhiVanChuyen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhiVanChuyen.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPhiVanChuyen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(461, 252);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(119, 13);
            this.label18.TabIndex = 32;
            this.label18.Text = "Trị giá khoản điều chỉnh";
            // 
            // txtTriGiaKhoanDC
            // 
            this.txtTriGiaKhoanDC.BackColor = System.Drawing.SystemColors.Info;
            this.txtTriGiaKhoanDC.DecimalDigits = 20;
            this.txtTriGiaKhoanDC.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaKhoanDC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaKhoanDC.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaKhoanDC.Location = new System.Drawing.Point(461, 276);
            this.txtTriGiaKhoanDC.MaxLength = 15;
            this.txtTriGiaKhoanDC.Name = "txtTriGiaKhoanDC";
            this.txtTriGiaKhoanDC.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaKhoanDC.Size = new System.Drawing.Size(128, 21);
            this.txtTriGiaKhoanDC.TabIndex = 33;
            this.txtTriGiaKhoanDC.Text = "0";
            this.txtTriGiaKhoanDC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaKhoanDC.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaKhoanDC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // VNACC_TK_TKTriGiaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 544);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_TK_TKTriGiaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin tờ khai trị giá";
            this.Load += new System.EventHandler(this.VNACC_TK_TKTriGiaForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox)).EndInit();
            this.uiGroupBox.ResumeLayout(false);
            this.uiGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Janus.Windows.EditControls.UIGroupBox uiGroupBox;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        public Janus.Windows.EditControls.UIButton btnClose;
        public Janus.Windows.EditControls.UIButton btnGhi;
        public Janus.Windows.GridEX.GridEX grList;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label16;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiVanChuyen;
        public System.Windows.Forms.Label label4;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiBaoHiem;
        public System.Windows.Forms.Label label6;
        public Janus.Windows.GridEX.EditControls.EditBox txtSoDangKyBH;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label7;
        public Janus.Windows.GridEX.EditControls.EditBox txtChiTietKhaiTriGia;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiTriGia;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTHieuChinhTriGia;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTPhiVC;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTPhiBH;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiPhiBH;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiPhiVC;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtGiaHieuChinhTriGia;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhanTKTriGia;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label9;
        public Janus.Windows.GridEX.EditControls.EditBox txtPhuongPhapDieuChinhTriGia;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaPhanLoaiDieuChinhTriGia;
        public Janus.Windows.GridEX.EditControls.EditBox txtPhanLoaiCongThucChuan;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label11;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaPhanLoaiTongGiaCoBan;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTongHeSoPhanBoTG;
        public Janus.Windows.EditControls.UIButton btnXoa;
        public Janus.Windows.EditControls.UIButton btnPhanBoPhi;
        public System.Windows.Forms.LinkLabel linkLabel1;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label13;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrTen;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTongHSPB;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoai;
        public Janus.Windows.EditControls.UIButton btnAdd;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaKhoanDC;
        public System.Windows.Forms.Label label18;
    }
}