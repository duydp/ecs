﻿namespace Company.Interface.DanhMucChuan
{
    partial class DoiTacEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DoiTacEditForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaNuocDoiTacEdit = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtFax = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMail = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDienThoai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTenDaiLyHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtQuocGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDaiLyHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTinhThanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBuuChinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtDiaChi2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenCongTy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.btnSaveAndNew = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnSaveAndNew);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Size = new System.Drawing.Size(703, 268);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(444, 239);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(247, 239);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ctrMaNuocDoiTacEdit);
            this.uiGroupBox2.Controls.Add(this.txtFax);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtMail);
            this.uiGroupBox2.Controls.Add(this.txtGhiChu);
            this.uiGroupBox2.Controls.Add(this.txtMa);
            this.uiGroupBox2.Controls.Add(this.txtDienThoai);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.txtTenDaiLyHQ);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.txtQuocGia);
            this.uiGroupBox2.Controls.Add(this.txtMaDaiLyHQ);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtTinhThanh);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.txtBuuChinh);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.txtDiaChi2);
            this.uiGroupBox2.Controls.Add(this.txtTenCongTy);
            this.uiGroupBox2.Controls.Add(this.txtDiaChi);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(680, 221);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaNuocDoiTacEdit
            // 
            this.ctrMaNuocDoiTacEdit.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaNuocDoiTacEdit.Appearance.Options.UseBackColor = true;
            this.ctrMaNuocDoiTacEdit.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrMaNuocDoiTacEdit.Code = "";
            this.ctrMaNuocDoiTacEdit.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaNuocDoiTacEdit.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaNuocDoiTacEdit.IsOnlyWarning = false;
            this.ctrMaNuocDoiTacEdit.IsValidate = true;
            this.ctrMaNuocDoiTacEdit.Location = new System.Drawing.Point(429, 127);
            this.ctrMaNuocDoiTacEdit.Name = "ctrMaNuocDoiTacEdit";
            this.ctrMaNuocDoiTacEdit.Name_VN = "";
            this.ctrMaNuocDoiTacEdit.SetOnlyWarning = false;
            this.ctrMaNuocDoiTacEdit.SetValidate = false;
            this.ctrMaNuocDoiTacEdit.ShowColumnCode = true;
            this.ctrMaNuocDoiTacEdit.ShowColumnName = true;
            this.ctrMaNuocDoiTacEdit.Size = new System.Drawing.Size(234, 21);
            this.ctrMaNuocDoiTacEdit.TabIndex = 11;
            this.ctrMaNuocDoiTacEdit.TagCode = "";
            this.ctrMaNuocDoiTacEdit.TagName = "";
            this.ctrMaNuocDoiTacEdit.Where = null;
            this.ctrMaNuocDoiTacEdit.WhereCondition = "";
            // 
            // txtFax
            // 
            this.txtFax.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFax.Location = new System.Drawing.Point(429, 44);
            this.txtFax.MaxLength = 50;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(234, 21);
            this.txtFax.TabIndex = 8;
            this.txtFax.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtFax.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtFax.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(356, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Ghi chú";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(356, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Email";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(356, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Fax";
            // 
            // txtMail
            // 
            this.txtMail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMail.Location = new System.Drawing.Point(429, 71);
            this.txtMail.MaxLength = 50;
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(234, 21);
            this.txtMail.TabIndex = 9;
            this.txtMail.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMail.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMail.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(429, 98);
            this.txtGhiChu.MaxLength = 1000;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(234, 23);
            this.txtGhiChu.TabIndex = 10;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtGhiChu.VisualStyleManager = this.vsmMain;
            // 
            // txtMa
            // 
            this.txtMa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa.Location = new System.Drawing.Point(97, 17);
            this.txtMa.MaxLength = 100;
            this.txtMa.Name = "txtMa";
            this.txtMa.Size = new System.Drawing.Size(234, 21);
            this.txtMa.TabIndex = 0;
            this.txtMa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMa.VisualStyleManager = this.vsmMain;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.Location = new System.Drawing.Point(429, 17);
            this.txtDienThoai.MaxLength = 50;
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(234, 21);
            this.txtDienThoai.TabIndex = 7;
            this.txtDienThoai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDienThoai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDienThoai.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(356, 184);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Tên đại lý HQ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(16, 184);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Quốc gia";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(356, 157);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Mã đại lý HQ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(16, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Tỉnh/Thành";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(356, 130);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Mã nước";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(16, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Bưu chính";
            // 
            // txtTenDaiLyHQ
            // 
            this.txtTenDaiLyHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDaiLyHQ.Location = new System.Drawing.Point(429, 179);
            this.txtTenDaiLyHQ.MaxLength = 50;
            this.txtTenDaiLyHQ.Name = "txtTenDaiLyHQ";
            this.txtTenDaiLyHQ.Size = new System.Drawing.Size(234, 21);
            this.txtTenDaiLyHQ.TabIndex = 13;
            this.txtTenDaiLyHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDaiLyHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Địa chỉ 2";
            // 
            // txtQuocGia
            // 
            this.txtQuocGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuocGia.Location = new System.Drawing.Point(97, 179);
            this.txtQuocGia.MaxLength = 50;
            this.txtQuocGia.Name = "txtQuocGia";
            this.txtQuocGia.Size = new System.Drawing.Size(117, 21);
            this.txtQuocGia.TabIndex = 6;
            this.txtQuocGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtQuocGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDaiLyHQ
            // 
            this.txtMaDaiLyHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLyHQ.Location = new System.Drawing.Point(429, 152);
            this.txtMaDaiLyHQ.MaxLength = 50;
            this.txtMaDaiLyHQ.Name = "txtMaDaiLyHQ";
            this.txtMaDaiLyHQ.Size = new System.Drawing.Size(113, 21);
            this.txtMaDaiLyHQ.TabIndex = 12;
            this.txtMaDaiLyHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDaiLyHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Địa chỉ 1";
            // 
            // txtTinhThanh
            // 
            this.txtTinhThanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTinhThanh.Location = new System.Drawing.Point(97, 152);
            this.txtTinhThanh.MaxLength = 50;
            this.txtTinhThanh.Name = "txtTinhThanh";
            this.txtTinhThanh.Size = new System.Drawing.Size(117, 21);
            this.txtTinhThanh.TabIndex = 5;
            this.txtTinhThanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTinhThanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên công ty";
            // 
            // txtBuuChinh
            // 
            this.txtBuuChinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuuChinh.Location = new System.Drawing.Point(97, 125);
            this.txtBuuChinh.MaxLength = 50;
            this.txtBuuChinh.Name = "txtBuuChinh";
            this.txtBuuChinh.Size = new System.Drawing.Size(117, 21);
            this.txtBuuChinh.TabIndex = 4;
            this.txtBuuChinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBuuChinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(16, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(60, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã công ty";
            // 
            // txtDiaChi2
            // 
            this.txtDiaChi2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi2.Location = new System.Drawing.Point(97, 98);
            this.txtDiaChi2.MaxLength = 50;
            this.txtDiaChi2.Name = "txtDiaChi2";
            this.txtDiaChi2.Size = new System.Drawing.Size(234, 21);
            this.txtDiaChi2.TabIndex = 3;
            this.txtDiaChi2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChi2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenCongTy
            // 
            this.txtTenCongTy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenCongTy.Location = new System.Drawing.Point(97, 44);
            this.txtTenCongTy.MaxLength = 500;
            this.txtTenCongTy.Name = "txtTenCongTy";
            this.txtTenCongTy.Size = new System.Drawing.Size(234, 21);
            this.txtTenCongTy.TabIndex = 1;
            this.txtTenCongTy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenCongTy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenCongTy.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(97, 71);
            this.txtDiaChi.MaxLength = 50;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(234, 21);
            this.txtDiaChi.TabIndex = 2;
            this.txtDiaChi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(356, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Điện thoại";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTenCongTy;
            this.rfvTen.ErrorMessage = "\"Tên công ty\" không được để trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            // 
            // btnSaveAndNew
            // 
            this.btnSaveAndNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAndNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSaveAndNew.Icon")));
            this.btnSaveAndNew.Location = new System.Drawing.Point(328, 239);
            this.btnSaveAndNew.Name = "btnSaveAndNew";
            this.btnSaveAndNew.Size = new System.Drawing.Size(110, 23);
            this.btnSaveAndNew.TabIndex = 2;
            this.btnSaveAndNew.Text = "Lưu và tạo mới";
            this.btnSaveAndNew.VisualStyleManager = this.vsmMain;
            this.btnSaveAndNew.Click += new System.EventHandler(this.btnSaveAndNew_Click);
            // 
            // DoiTacEditForm
            // 
            this.AcceptButton = this.btnSaveAndNew;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(703, 268);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "DoiTacEditForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đơn vị đối tác";
            this.Load += new System.EventHandler(this.DoiTacEditForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCongTy;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMa;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoai;
        private Janus.Windows.GridEX.EditControls.EditBox txtFax;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMail;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
        private Janus.Windows.EditControls.UIButton btnSaveAndNew;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuocGia;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinhThanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtBuuChinh;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDaiLyHQ;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDaiLyHQ;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaNuocDoiTacEdit;
    }
}
