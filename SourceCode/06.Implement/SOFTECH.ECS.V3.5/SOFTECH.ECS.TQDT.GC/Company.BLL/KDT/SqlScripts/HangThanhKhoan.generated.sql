
if not exists(select * from sys.columns where Object_ID = Object_ID(N't_KDT_GC_HangThanhKhoan') and Name = N'LuongTaiXuat')
begin
Alter Table  t_KDT_GC_HangThanhKhoan 
add  LuongTaiXuat numeric(18,4)  
end

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_SelectBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_SelectBy_Master_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangThanhKhoan_DeleteBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_DeleteBy_Master_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_Insert]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Insert]
	@Master_ID bigint,
	@LoaiHang int,
	@MaHang nvarchar(30),
	@TenHang nvarchar(256),
	@MaHS nvarchar(14),
	@SoLuong numeric(14, 4),
	@DVT_ID char(3),
	@GhiChu nvarchar(256),
	@LuongXuat numeric(18, 4),
	@LuongCU numeric(18, 4),
	@LuongTon numeric(18, 4),
	@LuongTaiXuat numeric(18, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_HangThanhKhoan]
(
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon],
	[LuongTaiXuat]
)
VALUES 
(
	@Master_ID,
	@LoaiHang,
	@MaHang,
	@TenHang,
	@MaHS,
	@SoLuong,
	@DVT_ID,
	@GhiChu,
	@LuongXuat,
	@LuongCU,
	@LuongTon,
	@LuongTaiXuat
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_Update]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Update]
	@ID bigint,
	@Master_ID bigint,
	@LoaiHang int,
	@MaHang nvarchar(30),
	@TenHang nvarchar(256),
	@MaHS nvarchar(14),
	@SoLuong numeric(14, 4),
	@DVT_ID char(3),
	@GhiChu nvarchar(256),
	@LuongXuat numeric(18, 4),
	@LuongCU numeric(18, 4),
	@LuongTon numeric(18, 4),
	@LuongTaiXuat numeric(18, 4)
AS

UPDATE
	[dbo].[t_KDT_GC_HangThanhKhoan]
SET
	[Master_ID] = @Master_ID,
	[LoaiHang] = @LoaiHang,
	[MaHang] = @MaHang,
	[TenHang] = @TenHang,
	[MaHS] = @MaHS,
	[SoLuong] = @SoLuong,
	[DVT_ID] = @DVT_ID,
	[GhiChu] = @GhiChu,
	[LuongXuat] = @LuongXuat,
	[LuongCU] = @LuongCU,
	[LuongTon] = @LuongTon,
	[LuongTaiXuat] = @LuongTaiXuat
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@LoaiHang int,
	@MaHang nvarchar(30),
	@TenHang nvarchar(256),
	@MaHS nvarchar(14),
	@SoLuong numeric(14, 4),
	@DVT_ID char(3),
	@GhiChu nvarchar(256),
	@LuongXuat numeric(18, 4),
	@LuongCU numeric(18, 4),
	@LuongTon numeric(18, 4),
	@LuongTaiXuat numeric(18, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_HangThanhKhoan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_HangThanhKhoan] 
		SET
			[Master_ID] = @Master_ID,
			[LoaiHang] = @LoaiHang,
			[MaHang] = @MaHang,
			[TenHang] = @TenHang,
			[MaHS] = @MaHS,
			[SoLuong] = @SoLuong,
			[DVT_ID] = @DVT_ID,
			[GhiChu] = @GhiChu,
			[LuongXuat] = @LuongXuat,
			[LuongCU] = @LuongCU,
			[LuongTon] = @LuongTon,
			[LuongTaiXuat] = @LuongTaiXuat
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_HangThanhKhoan]
		(
			[Master_ID],
			[LoaiHang],
			[MaHang],
			[TenHang],
			[MaHS],
			[SoLuong],
			[DVT_ID],
			[GhiChu],
			[LuongXuat],
			[LuongCU],
			[LuongTon],
			[LuongTaiXuat]
		)
		VALUES 
		(
			@Master_ID,
			@LoaiHang,
			@MaHang,
			@TenHang,
			@MaHS,
			@SoLuong,
			@DVT_ID,
			@GhiChu,
			@LuongXuat,
			@LuongCU,
			@LuongTon,
			@LuongTaiXuat
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_Delete]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_HangThanhKhoan]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_DeleteBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_DeleteBy_Master_ID]
	@Master_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_HangThanhKhoan]
WHERE
	[Master_ID] = @Master_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_HangThanhKhoan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_Load]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon],
	[LuongTaiXuat]
FROM
	[dbo].[t_KDT_GC_HangThanhKhoan]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_SelectBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon],
	[LuongTaiXuat]
FROM
	[dbo].[t_KDT_GC_HangThanhKhoan]
WHERE
	[Master_ID] = @Master_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon],
	[LuongTaiXuat]
FROM [dbo].[t_KDT_GC_HangThanhKhoan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_SelectAll]
-- Database: ECS_TQDT_GC_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon],
	[LuongTaiXuat]
FROM
	[dbo].[t_KDT_GC_HangThanhKhoan]	

GO

