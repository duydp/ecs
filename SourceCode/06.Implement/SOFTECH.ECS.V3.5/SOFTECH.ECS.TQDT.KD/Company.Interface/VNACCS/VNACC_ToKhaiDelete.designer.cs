﻿namespace Company.Interface
{
    partial class VNACC_ToKhaiDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ToKhaiDelete));
            Janus.Windows.GridEX.GridEXLayout grListTKChinhThuc_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyTờKhaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyNộiDungTờKhaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyNộiDungTờKhaiHàngHóaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListTKChinhThuc = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListTKChinhThuc)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(908, 529);
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // vsmMain
            // 
            this.vsmMain.DefaultColorScheme = "Office2003";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnXoa);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 500);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(908, 29);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageIndex = 4;
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(800, 4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(96, 23);
            this.btnXoa.TabIndex = 17;
            this.btnXoa.Text = "Xóa tờ khai";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.grList);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(908, 267);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.Text = "Danh sách tờ khai thử";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grList.ContextMenuStrip = this.contextMenuStrip1;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Hierarchical = true;
            this.grList.Location = new System.Drawing.Point(3, 17);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(902, 247);
            this.grList.TabIndex = 0;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            this.grList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grList_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyTờKhaiToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(142, 26);
            // 
            // copyTờKhaiToolStripMenuItem
            // 
            this.copyTờKhaiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyNộiDungTờKhaiToolStripMenuItem,
            this.copyNộiDungTờKhaiHàngHóaToolStripMenuItem});
            this.copyTờKhaiToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyTờKhaiToolStripMenuItem.Image")));
            this.copyTờKhaiToolStripMenuItem.Name = "copyTờKhaiToolStripMenuItem";
            this.copyTờKhaiToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.copyTờKhaiToolStripMenuItem.Text = "Copy tờ khai";
            // 
            // copyNộiDungTờKhaiToolStripMenuItem
            // 
            this.copyNộiDungTờKhaiToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyNộiDungTờKhaiToolStripMenuItem.Image")));
            this.copyNộiDungTờKhaiToolStripMenuItem.Name = "copyNộiDungTờKhaiToolStripMenuItem";
            this.copyNộiDungTờKhaiToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.copyNộiDungTờKhaiToolStripMenuItem.Text = "Copy nội dung tờ khai";
            this.copyNộiDungTờKhaiToolStripMenuItem.Click += new System.EventHandler(this.copyNộiDungTờKhaiToolStripMenuItem_Click);
            // 
            // copyNộiDungTờKhaiHàngHóaToolStripMenuItem
            // 
            this.copyNộiDungTờKhaiHàngHóaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyNộiDungTờKhaiHàngHóaToolStripMenuItem.Image")));
            this.copyNộiDungTờKhaiHàngHóaToolStripMenuItem.Name = "copyNộiDungTờKhaiHàngHóaToolStripMenuItem";
            this.copyNộiDungTờKhaiHàngHóaToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.copyNộiDungTờKhaiHàngHóaToolStripMenuItem.Text = "Copy nội dung tờ khai + Hàng hóa";
            this.copyNộiDungTờKhaiHàngHóaToolStripMenuItem.Click += new System.EventHandler(this.copyNộiDungTờKhaiHàngHóaToolStripMenuItem_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.grListTKChinhThuc);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 267);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(908, 233);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.Text = "Danh sách tờ khai chính thức";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // grListTKChinhThuc
            // 
            this.grListTKChinhThuc.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListTKChinhThuc.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListTKChinhThuc.ContextMenuStrip = this.contextMenuStrip1;
            grListTKChinhThuc_DesignTimeLayout.LayoutString = resources.GetString("grListTKChinhThuc_DesignTimeLayout.LayoutString");
            this.grListTKChinhThuc.DesignTimeLayout = grListTKChinhThuc_DesignTimeLayout;
            this.grListTKChinhThuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListTKChinhThuc.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListTKChinhThuc.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListTKChinhThuc.FrozenColumns = 5;
            this.grListTKChinhThuc.GroupByBoxVisible = false;
            this.grListTKChinhThuc.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListTKChinhThuc.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListTKChinhThuc.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListTKChinhThuc.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListTKChinhThuc.Hierarchical = true;
            this.grListTKChinhThuc.Location = new System.Drawing.Point(3, 17);
            this.grListTKChinhThuc.Name = "grListTKChinhThuc";
            this.grListTKChinhThuc.RecordNavigator = true;
            this.grListTKChinhThuc.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListTKChinhThuc.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListTKChinhThuc.Size = new System.Drawing.Size(902, 213);
            this.grListTKChinhThuc.TabIndex = 0;
            this.grListTKChinhThuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListTKChinhThuc.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListTKChinhThuc_RowDoubleClick);
            // 
            // VNACC_ToKhaiDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 529);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ToKhaiDelete";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Xóa tờ khai VNACCS";
            this.Load += new System.EventHandler(this.VNACC_ToKhaiManager_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListTKChinhThuc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX grList;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem copyTờKhaiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyNộiDungTờKhaiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyNộiDungTờKhaiHàngHóaToolStripMenuItem;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX grListTKChinhThuc;
    }
}