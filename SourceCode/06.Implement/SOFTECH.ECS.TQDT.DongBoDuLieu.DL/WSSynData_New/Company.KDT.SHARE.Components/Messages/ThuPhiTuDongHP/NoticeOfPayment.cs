﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class NoticeOfPayment
    {
        /// <summary>
        /// Thông báo nộp phí
        /// </summary>
       [XmlElement("Notice")]
       public Notice Notice { get; set; }

       /// <summary>
       ///Tờ khai
       /// </summary>
       [XmlElement("CustomsReference")]
       public CustomsReference CustomsReference { get; set; }

       /// <summary>
       /// Ghi chú khác
       /// </summary>
       [XmlElement("AdditionalInformation")]
       public TTTK_AdditionalInformation AdditionalInformation { get; set; }

       /// <summary>
       /// Danh sách container trên tờ khai nộp phí
       /// </summary>
       [XmlElement("TransportEquipments")]
       public TTTK_TransportEquipments TransportEquipments { get; set; }

       /// <summary>
       /// Tệp đính kèm
       /// </summary>
       [XmlElement("FileAttachs")]
       public FileAttachs FileAttachs { get; set; }

       /// <summary>
       /// Tệp đính kèm
       /// </summary>
       [XmlElement("CustomsDeclarations")]
       public CustomsDeclarations CustomsDeclarations { get; set; }

    }
}
