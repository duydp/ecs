﻿namespace Company.Interface.VNACCS
{
    partial class VNACC_ListToKhai
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ListToKhai));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.btnChonHang = new Janus.Windows.EditControls.UIButton();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayDangKy = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ucCalendar1 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ctrMaHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnTimKiem = new Janus.Windows.EditControls.UIButton();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbbTrangThai = new Janus.Windows.EditControls.UIComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grList);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(854, 464);
            // 
            // btnChonHang
            // 
            this.btnChonHang.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnChonHang.Image = global::Company.Interface.Properties.Resources.apply2;
            this.btnChonHang.Location = new System.Drawing.Point(741, 3);
            this.btnChonHang.Name = "btnChonHang";
            this.btnChonHang.Size = new System.Drawing.Size(101, 23);
            this.btnChonHang.TabIndex = 1;
            this.btnChonHang.Text = "Chọn tờ khai";
            this.btnChonHang.VisualStyleManager = this.vsmMain;
            this.btnChonHang.Click += new System.EventHandler(this.btnChonHang_Click);
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grList.ColumnAutoResize = true;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Hierarchical = true;
            this.grList.Location = new System.Drawing.Point(0, 78);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(854, 353);
            this.grList.TabIndex = 2;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.clcNgayDangKy);
            this.uiGroupBox1.Controls.Add(this.ucCalendar1);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.ctrMaHaiQuan);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.btnExportExcel);
            this.uiGroupBox1.Controls.Add(this.btnTimKiem);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.cbbTrangThai);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(854, 78);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.Text = "Tìm kiếm";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnChonHang);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 431);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(854, 33);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayDangKy
            // 
            this.clcNgayDangKy.Location = new System.Drawing.Point(292, 50);
            this.clcNgayDangKy.Name = "clcNgayDangKy";
            this.clcNgayDangKy.ReadOnly = false;
            this.clcNgayDangKy.Size = new System.Drawing.Size(90, 21);
            this.clcNgayDangKy.TabIndex = 19;
            this.clcNgayDangKy.TagName = "";
            this.clcNgayDangKy.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDangKy.WhereCondition = "";
            // 
            // ucCalendar1
            // 
            this.ucCalendar1.Location = new System.Drawing.Point(444, 50);
            this.ucCalendar1.Name = "ucCalendar1";
            this.ucCalendar1.ReadOnly = false;
            this.ucCalendar1.Size = new System.Drawing.Size(90, 21);
            this.ucCalendar1.TabIndex = 19;
            this.ucCalendar1.TagName = "";
            this.ucCalendar1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ucCalendar1.WhereCondition = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(236, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Từ ngày";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(388, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Đến ngày";
            // 
            // ctrMaHaiQuan
            // 
            this.ctrMaHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrMaHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrMaHaiQuan.Code = "";
            this.ctrMaHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHaiQuan.IsOnlyWarning = false;
            this.ctrMaHaiQuan.IsValidate = true;
            this.ctrMaHaiQuan.Location = new System.Drawing.Point(103, 18);
            this.ctrMaHaiQuan.Name = "ctrMaHaiQuan";
            this.ctrMaHaiQuan.Name_VN = "";
            this.ctrMaHaiQuan.SetOnlyWarning = false;
            this.ctrMaHaiQuan.SetValidate = false;
            this.ctrMaHaiQuan.ShowColumnCode = true;
            this.ctrMaHaiQuan.ShowColumnName = true;
            this.ctrMaHaiQuan.Size = new System.Drawing.Size(285, 20);
            this.ctrMaHaiQuan.TabIndex = 32;
            this.ctrMaHaiQuan.TagCode = "";
            this.ctrMaHaiQuan.TagName = "";
            this.ctrMaHaiQuan.Where = null;
            this.ctrMaHaiQuan.WhereCondition = "";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Icon = ((System.Drawing.Icon)(resources.GetObject("btnExportExcel.Icon")));
            this.btnExportExcel.ImageIndex = 4;
            this.btnExportExcel.Location = new System.Drawing.Point(560, 17);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(90, 23);
            this.btnExportExcel.TabIndex = 31;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Icon = ((System.Drawing.Icon)(resources.GetObject("btnTimKiem.Icon")));
            this.btnTimKiem.ImageIndex = 4;
            this.btnTimKiem.Location = new System.Drawing.Point(560, 49);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(90, 23);
            this.btnTimKiem.TabIndex = 30;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnTimKiem.VisualStyleManager = this.vsmMain;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Location = new System.Drawing.Point(103, 50);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(133, 21);
            this.txtSoToKhai.TabIndex = 24;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(43, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Số tờ khai";
            // 
            // cbbTrangThai
            // 
            this.cbbTrangThai.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Chưa khai báo";
            uiComboBoxItem7.Value = "0";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Đã khai báo";
            uiComboBoxItem8.Value = "1";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Đã xác nhận khai báo";
            uiComboBoxItem9.Value = "2";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Thông quan";
            uiComboBoxItem10.Value = "3";
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Đang sửa";
            uiComboBoxItem11.Value = "4";
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Đã hủy";
            uiComboBoxItem12.Value = "6";
            this.cbbTrangThai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10,
            uiComboBoxItem11,
            uiComboBoxItem12});
            this.cbbTrangThai.Location = new System.Drawing.Point(444, 18);
            this.cbbTrangThai.Name = "cbbTrangThai";
            this.cbbTrangThai.Size = new System.Drawing.Size(110, 21);
            this.cbbTrangThai.TabIndex = 29;
            this.cbbTrangThai.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Cơ quan Hải quan";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(383, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Trạng thái";
            // 
            // VNACC_ListToKhai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 464);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ListToKhai";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh sách hàng trong tờ khai";
            this.Load += new System.EventHandler(this.VNACC_ListHangHMD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnChonHang;
        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDangKy;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar ucCalendar1;
        private System.Windows.Forms.Label label9;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaHaiQuan;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIButton btnTimKiem;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.EditControls.UIComboBox cbbTrangThai;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}