﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using GetPKCS7;
namespace SignRemoteVNACCS
{
    public class SignVNACCS
    {
        public static string signEdi(string dataToSign, X509Certificate2 cert, string passSign)
        {
            //var store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            //store.Open(OpenFlags.ReadOnly);
            if (cert != null)
            {
                if (GetNameSigner(cert.IssuerName.Name) == "VNPT Certification Authority")
                {

                    byte[] data = Encoding.UTF8.GetBytes(dataToSign);
                    return Convert.ToBase64String(GetPKCS7Signed.CreatePKCS7(data, cert, passSign));
                }
                else
                {
                    var data = Encoding.UTF8.GetBytes(dataToSign);
                    var digestOid = new Oid("1.2.840.113549.1.7.2"); //pkcs7 signed

                    var content = new ContentInfo(digestOid, data);
                    try
                    {
                        var signedCms = new SignedCms(SubjectIdentifierType.IssuerAndSerialNumber, content, true); //detached=true
                        var singer = new CmsSigner(cert) { DigestAlgorithm = new Oid("1.3.14.3.2.26") };
                        signedCms.ComputeSignature(singer, false);
                        var signEnv = signedCms.Encode();
                        return Convert.ToBase64String(signEnv);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

                }
            }
            return null;
        }
        private static string GetNameSigner(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            if (!string.IsNullOrEmpty(mst)) return string.Format("{0} - [{1}]", name, mst);
            else return string.Format("{0}", name);
        }

    }
}
