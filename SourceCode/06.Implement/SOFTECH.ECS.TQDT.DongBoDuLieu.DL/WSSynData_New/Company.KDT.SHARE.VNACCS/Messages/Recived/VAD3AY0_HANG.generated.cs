﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAD3AY0_HANG : BasicVNACC
    {
        public static int TongSoByte { get; set; }
        public PropertiesAttribute B25 { get; set; }
        public PropertiesAttribute CMD { get; set; }
        public PropertiesAttribute GZC { get; set; }
        public PropertiesAttribute B29 { get; set; }
        public PropertiesAttribute CMN { get; set; }
        public PropertiesAttribute QN1 { get; set; }
        public PropertiesAttribute QT1 { get; set; }
        public GroupAttribute VN_ { get; set; }
        public PropertiesAttribute QN2 { get; set; }
        public PropertiesAttribute QT2 { get; set; }
        public PropertiesAttribute BPR { get; set; }
        public PropertiesAttribute UPR { get; set; }
        public PropertiesAttribute UPC { get; set; }
        public PropertiesAttribute TSC { get; set; }
        public PropertiesAttribute B36 { get; set; }
        public PropertiesAttribute B50 { get; set; }
        public PropertiesAttribute B51 { get; set; }
        public PropertiesAttribute B37 { get; set; }
        public PropertiesAttribute B38 { get; set; }
        public PropertiesAttribute KKT { get; set; }
        public PropertiesAttribute KKS { get; set; }
        public PropertiesAttribute B42 { get; set; }
        public PropertiesAttribute B43 { get; set; }
        public PropertiesAttribute SKB { get; set; }
        public PropertiesAttribute SPD { get; set; }
        public PropertiesAttribute B47 { get; set; }
        public PropertiesAttribute OR { get; set; }
        public PropertiesAttribute B56 { get; set; }
        public PropertiesAttribute ORS { get; set; }
        public PropertiesAttribute B49 { get; set; }
        public PropertiesAttribute KWS { get; set; }
        public PropertiesAttribute TDL { get; set; }
        public PropertiesAttribute TXN { get; set; }
        public PropertiesAttribute TXR { get; set; }
        public PropertiesAttribute RE { get; set; }
        public PropertiesAttribute B59 { get; set; }
        public GroupAttribute KQ1 { get; set; }

        public VAD3AY0_HANG()
        {
            B25 = new PropertiesAttribute(2, typeof(string));
            CMD = new PropertiesAttribute(12, typeof(string));
            GZC = new PropertiesAttribute(7, typeof(string));
            B29 = new PropertiesAttribute(1, typeof(string));
            CMN = new PropertiesAttribute(600, typeof(string));
            QN1 = new PropertiesAttribute(12, typeof(int));
            QT1 = new PropertiesAttribute(4, typeof(string));
            QN2 = new PropertiesAttribute(12, typeof(int));
            QT2 = new PropertiesAttribute(4, typeof(string));
            BPR = new PropertiesAttribute(20, typeof(int));
            UPR = new PropertiesAttribute(9, typeof(int));
            UPC = new PropertiesAttribute(3, typeof(string));
            TSC = new PropertiesAttribute(4, typeof(string));
            B36 = new PropertiesAttribute(17, typeof(int));
            B50 = new PropertiesAttribute(3, typeof(string));
            B51 = new PropertiesAttribute(20, typeof(int));
            B37 = new PropertiesAttribute(12, typeof(int));
            B38 = new PropertiesAttribute(4, typeof(string));
            KKT = new PropertiesAttribute(18, typeof(int));
            KKS = new PropertiesAttribute(4, typeof(string));
            B42 = new PropertiesAttribute(1, typeof(string));
            B43 = new PropertiesAttribute(30, typeof(string));
            SKB = new PropertiesAttribute(1, typeof(string));
            SPD = new PropertiesAttribute(10, typeof(string));
            B47 = new PropertiesAttribute(16, typeof(int));
            OR = new PropertiesAttribute(2, typeof(string));
            B56 = new PropertiesAttribute(7, typeof(string));
            ORS = new PropertiesAttribute(3, typeof(string));
            B49 = new PropertiesAttribute(16, typeof(int));
            KWS = new PropertiesAttribute(1, typeof(string));
            TDL = new PropertiesAttribute(2, typeof(string));
            TXN = new PropertiesAttribute(12, typeof(string));
            TXR = new PropertiesAttribute(3, typeof(string));
            RE = new PropertiesAttribute(5, typeof(string));
            B59 = new PropertiesAttribute(60, typeof(string));
            #region VN_
            List<PropertiesAttribute> listVN_ = new List<PropertiesAttribute>();
            listVN_.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAD3AY0_HANG_VN_, typeof(int)));
            VN_ = new GroupAttribute("VN_", 5, listVN_);
            #endregion VN_
            #region KQ1
            List<PropertiesAttribute> listKQ1 = new List<PropertiesAttribute>();
            listKQ1.Add(new PropertiesAttribute(27, 5, EnumGroupID.VAD3AY0_HANG_KQ1, typeof(string)));
            listKQ1.Add(new PropertiesAttribute(10, 5, EnumGroupID.VAD3AY0_HANG_TX_, typeof(string)));
            listKQ1.Add(new PropertiesAttribute(17, 5, EnumGroupID.VAD3AY0_HANG_KS1, typeof(int)));
            listKQ1.Add(new PropertiesAttribute(12, 5, EnumGroupID.VAD3AY0_HANG_KT1, typeof(int)));
            listKQ1.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAD3AY0_HANG_KU1, typeof(string)));
            listKQ1.Add(new PropertiesAttribute(25, 5, EnumGroupID.VAD3AY0_HANG_KY1, typeof(string)));
            listKQ1.Add(new PropertiesAttribute(16, 5, EnumGroupID.VAD3AY0_HANG_KZ1, typeof(int)));
            listKQ1.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAD3AY0_HANG_TR_, typeof(string)));
            listKQ1.Add(new PropertiesAttribute(60, 5, EnumGroupID.VAD3AY0_HANG_LC1, typeof(string)));
            listKQ1.Add(new PropertiesAttribute(16, 5, EnumGroupID.VAD3AY0_HANG_LB1, typeof(int)));
            KQ1 = new GroupAttribute("KQ1", 5, listKQ1);
            #endregion KQ1
            TongSoByte = 2082;
        }

    }
    public partial class EnumGroupID
    {
        public static readonly string VAD3AY0_HANG_VN_ = "VAD3AY0_HANG_VN_";
        public static readonly string VAD3AY0_HANG_KQ1 = "VAD3AY0_HANG_KQ1";
        public static readonly string VAD3AY0_HANG_TX_ = "VAD3AY0_HANG_TX_";
        public static readonly string VAD3AY0_HANG_KS1 = "VAD3AY0_HANG_KS1";
        public static readonly string VAD3AY0_HANG_KT1 = "VAD3AY0_HANG_KT1";
        public static readonly string VAD3AY0_HANG_KU1 = "VAD3AY0_HANG_KU1";
        public static readonly string VAD3AY0_HANG_KY1 = "VAD3AY0_HANG_KY1";
        public static readonly string VAD3AY0_HANG_KZ1 = "VAD3AY0_HANG_KZ1";
        public static readonly string VAD3AY0_HANG_TR_ = "VAD3AY0_HANG_TR_";
        public static readonly string VAD3AY0_HANG_LC1 = "VAD3AY0_HANG_LC1";
        public static readonly string VAD3AY0_HANG_LB1 = "VAD3AY0_HANG_LB1";
    }

}
