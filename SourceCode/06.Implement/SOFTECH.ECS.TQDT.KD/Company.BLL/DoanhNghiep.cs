using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
namespace Company.KD.BLL
{
    public partial class DoanhNghiep
    {
        //public bool WSLogin()
        //{
        //    KDTService kdtService = new KDTService();
        //    return true;
        //}
        public DataSet GetKhaiDT(string SQL)
        {
            Database db3 = DatabaseFactory.CreateDatabase("KhaiDT");
            DbCommand c = db3.GetSqlStringCommand(SQL);
            return db3.ExecuteDataSet(c);
        }
    }
}