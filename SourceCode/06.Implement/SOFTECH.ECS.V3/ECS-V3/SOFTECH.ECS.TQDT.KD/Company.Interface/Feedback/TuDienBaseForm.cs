﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.Feedback
{
    public partial class TuDienBaseForm : BaseForm
    {
        public TuDienBaseForm()
        {
            InitializeComponent();

            btnClose.Click+=new EventHandler(btnClose_Click);

            //GlobalSettings.PermissionTuDien(dgList, btnSave, btnDelete);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetChanged(bool changed)
        {
            base.SetChanged(changed);

            btnSave.Enabled = changed;
        }
    }
}
