-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]
	@Master_ID bigint,
	@MaSacThue varchar(1),
	@TenSacThue nvarchar(50),
	@TongTienThue numeric(15, 4),
	@SoDongTongTienThue numeric(2, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
(
	[Master_ID],
	[MaSacThue],
	[TenSacThue],
	[TongTienThue],
	[SoDongTongTienThue]
)
VALUES 
(
	@Master_ID,
	@MaSacThue,
	@TenSacThue,
	@TongTienThue,
	@SoDongTongTienThue
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaSacThue varchar(1),
	@TenSacThue nvarchar(50),
	@TongTienThue numeric(15, 4),
	@SoDongTongTienThue numeric(2, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
SET
	[Master_ID] = @Master_ID,
	[MaSacThue] = @MaSacThue,
	[TenSacThue] = @TenSacThue,
	[TongTienThue] = @TongTienThue,
	[SoDongTongTienThue] = @SoDongTongTienThue
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaSacThue varchar(1),
	@TenSacThue nvarchar(50),
	@TongTienThue numeric(15, 4),
	@SoDongTongTienThue numeric(2, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue] 
		SET
			[Master_ID] = @Master_ID,
			[MaSacThue] = @MaSacThue,
			[TenSacThue] = @TenSacThue,
			[TongTienThue] = @TongTienThue,
			[SoDongTongTienThue] = @SoDongTongTienThue
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
		(
			[Master_ID],
			[MaSacThue],
			[TenSacThue],
			[TongTienThue],
			[SoDongTongTienThue]
		)
		VALUES 
		(
			@Master_ID,
			@MaSacThue,
			@TenSacThue,
			@TongTienThue,
			@SoDongTongTienThue
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSacThue],
	[TenSacThue],
	[TongTienThue],
	[SoDongTongTienThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaSacThue],
	[TenSacThue],
	[TongTienThue],
	[SoDongTongTienThue]
FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSacThue],
	[TenSacThue],
	[TongTienThue],
	[SoDongTongTienThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]	

GO

