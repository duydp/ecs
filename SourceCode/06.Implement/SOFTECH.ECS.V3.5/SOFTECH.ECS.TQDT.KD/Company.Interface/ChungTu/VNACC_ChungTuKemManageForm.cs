﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface
{
    public partial class VNACC_ChungTuKemManageForm : BaseForm
    {
        private List<KDT_VNACC_ChungTuDinhKem> ChungTuDinhKemColecctions = new List<KDT_VNACC_ChungTuDinhKem>();
        public ELoaiThongTin LoaiChungTu = ELoaiThongTin.MSB;
        private DataTable dtNhomXuLy = new DataTable();
        private DataTable dtPhanLoaiThuTuc = new DataTable();
        DataSet ds = new DataSet();
        public bool IsShowChonToKhai = false;
        public List<KDT_VNACC_ChungTuDinhKem> ListCTK = null;
        public VNACC_ChungTuKemManageForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void VNACC_ChungTuKemManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                ucPhanLoai.CategoryType = ECategory.A546;
                cbbTrangThai.SelectedValue = 0;
                ctrMaHaiQuan.CategoryType = ECategory.A038;
                ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;// GlobalSettings.MA_HAI_QUAN.Substring(1, 2).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(0, 1).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(3, 1).ToUpper();
                btnTimKiem_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        //private void VNACC_ChungTuKemManageForm_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Cursor = Cursors.WaitCursor;
        //        dtNhomXuLy = VNACC_Category_CustomsSubSection.SelectAll().Tables[0];

        //        BindData();

        //        //Fill ValueList
        //        this.dgList.RootTable.Columns["CoQuanHaiQuan"].HasValueList = true;
        //        Janus.Windows.GridEX.GridEXValueListItemCollection valueList = this.dgList.RootTable.Columns["CoQuanHaiQuan"].ValueList;
        //        System.Data.DataView view = VNACC_Category_CustomsOffice.SelectAll().Tables[0].DefaultView;
        //        view.Sort = "CustomsCode ASC";
        //        for (int i = 0; i < view.Count; i++)
        //        {
        //            System.Data.DataRowView row = view[i];
        //            valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["CustomsCode"].ToString(), row["CustomsOfficeNameInVietnamese"].ToString()));
        //        }

        //        //Nhom xu ly ho so
        //        //this.dgList.RootTable.Columns["NhomXuLyHoSo"].HasValueList = true;
        //        //Janus.Windows.GridEX.GridEXValueListItemCollection vlNhomXuLyHoSo = this.dgList.RootTable.Columns["NhomXuLyHoSo"].ValueList;
        //        //view.Sort = "CustomsSubSectionCode ASC";
        //        //for (int i = 0; i < view.Count; i++)
        //        //{
        //        //    System.Data.DataRowView row = view[i];
        //        //    vlNhomXuLyHoSo.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["CustomsSubSectionCode"].ToString(), row["Name"].ToString()));
                        
        //        //}

        //        //Loai chung tu
        //        this.dgList.RootTable.Columns["LoaiChungTu"].HasValueList = true;
        //        Janus.Windows.GridEX.GridEXValueListItemCollection vlLoaiChungTu = this.dgList.RootTable.Columns["LoaiChungTu"].ValueList;
        //        vlLoaiChungTu.Add(new Janus.Windows.GridEX.GridEXValueListItem(ELoaiThongTin.MSB.ToString(), ELoaiThongTin.MSB.ToString()));
        //        vlLoaiChungTu.Add(new Janus.Windows.GridEX.GridEXValueListItem(ELoaiThongTin.HYS.ToString(), ELoaiThongTin.HYS.ToString()));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
        //    }
        //    finally { Cursor = Cursors.Default; }
        //}

        private void BindData()
        {
            try
            {
                //string query = string.Format("LoaiChungTu = '{0}'", LoaiChungTu.ToString());
                ChungTuDinhKemColecctions = KDT_VNACC_ChungTuDinhKem.SelectCollectionAll();

                dgList.DataSource = ChungTuDinhKemColecctions;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                List<KDT_VNACC_ChungTuDinhKem> listChungTu = new List<KDT_VNACC_ChungTuDinhKem>();
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                {
                    if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                    {
                        return;
                    }
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            KDT_VNACC_ChungTuDinhKem gp = (KDT_VNACC_ChungTuDinhKem)i.GetRow().DataRow;
                            listChungTu.Add(gp);
                        }
                    }
                }

                foreach (KDT_VNACC_ChungTuDinhKem item in listChungTu)
                {
                    if (item.ID > 0)
                    {
                        //Load chung tu kem chi tiet (Neu co)
                        item.LoadListChungTuDinhKem_ChiTiet();

                        //Xoa chung tu dinh kem chi tiet
                        for (int k = 0; k < item.ChungTuDinhKemChiTietCollection.Count; k++)
                        {
                            item.ChungTuDinhKemChiTietCollection[k].Delete();
                        }

                        //Xoa chung tu dinh kem trong DB
                        item.Delete();
                    }
                }

                BindData();
                btnTimKiem_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            //VNACC_ChungTuDinhKemForm ctForm = new VNACC_ChungTuDinhKemForm();
            //ctForm.ShowDialog(this);

            //dgList.DataSource = ctForm.ListCTDK;
            //try { dgList.Refetch(); }
            //catch { }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                KDT_VNACC_ChungTuDinhKem chungTu = (KDT_VNACC_ChungTuDinhKem)e.Row.DataRow;

                VNACC_ChungTuDinhKemForm f = null;
                if (chungTu.LoaiChungTu == ELoaiThongTin.MSB.ToString())
                    f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.MSB);
                else if (chungTu.LoaiChungTu == ELoaiThongTin.HYS.ToString())
                    f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.HYS);
                f.isAddNew = false;
                f.ChungTuKem = chungTu;

                f.ShowDialog(this);
                
                BindData();
                btnTimKiem_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count <= 0) return;
                else
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            KDT_VNACC_ChungTuDinhKem gp = (KDT_VNACC_ChungTuDinhKem)i.GetRow().DataRow;
                            if (gp.SoTiepNhan > 0)
                            {
                                btnXoa.Enabled = false;
                                btnChon.Visible = IsShowChonToKhai ? true : false;
                            }
                            else
                            {
                                btnXoa.Enabled = true;
                                btnChon.Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //if (!string.IsNullOrEmpty(e.Row.Cells["PhanLoaiThuTucKhaiBao"].Value.ToString().Trim()))
            //{
            //    DataRow[] dr = dtPhanLoaiThuTuc.Select(string.Format(e.Row.Cells["PhanLoaiThuTucKhaiBao"].Value.ToString()));
            //    if (dr.Length > 0)
            //        e.Row.Cells["PhanLoaiThuTucKhaiBao"].Text = dr[0]["ApplicationProcedureName"].ToString();
            //}
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                BindData();
                btnTimKiem_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void LoadData(string where)
        {
            try
            {
                ds = VNACC_Category_ApplicationProcedureType.SelectDynamic("TableID in ('A546A')", null);
                ChungTuDinhKemColecctions = KDT_VNACC_ChungTuDinhKem.SelectCollectionDynamic(where, "");
                dgList.DropDowns["drdPhanLoai"].DataSource = ds.Tables[0];
                dgList.DataSource = ChungTuDinhKemColecctions;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soToKhai = txtSoToKhai.Text.Trim();
                string phanloai = ucPhanLoai.Code.Trim();
                string trangthai = (cbbTrangThai.SelectedValue == null) ? "2" : cbbTrangThai.SelectedValue.ToString().Trim();
                //DateTime ngayDangKy = clcNgayDangKy.Value;
                string haiquan = ctrMaHaiQuan.Code.Trim();
                string where = String.Empty;
                //where = "MaDonVi='" + GlobalSettings.MA_DON_VI + "' ";
                if (trangthai != "")
                    where = where + " TrangThaiXuLy='" + trangthai + "'";
                if (soToKhai != "")
                    where = where + " and SoTiepNhan like '%" + soToKhai + "%' ";
                if (phanloai != "")
                    where = where + " and PhanLoaiThuTucKhaiBao='" + phanloai + "' ";
                if (haiquan != "")
                    where = where + " and CoQuanHaiQuan='" + haiquan + "' ";
                if (txtNamDangKy.Text != "" && Convert.ToInt64(txtNamDangKy.Text) > 1900)
                    if (trangthai != "0" && trangthai != "2" && trangthai != "3")
                        where = where + " and year(NgayTiepNhan)=" + txtNamDangKy.Text + " ";

                //if (ngayDangKy.ToString("dd/MM/yyyy") != "01/01/1900")
                //    where = where + " and NgayDangKy='" + ngayDangKy.ToString("yyyy/MM/dd") + "'";
                ChungTuDinhKemColecctions.Clear();

                LoadData(where);

                dgList.DataSource = ChungTuDinhKemColecctions;
                dgList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;

            }
        }
        private void cbbTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }
        private void btnChon_Click(object sender, EventArgs e)
        {
            try
            {
                List<KDT_VNACC_ChungTuDinhKem> listCTK = new List<KDT_VNACC_ChungTuDinhKem>();
                foreach (GridEXSelectedItem item in dgList.SelectedItems)
                {
                    KDT_VNACC_ChungTuDinhKem dt = new KDT_VNACC_ChungTuDinhKem();
                    dt = (KDT_VNACC_ChungTuDinhKem)item.GetRow().DataRow;
                    listCTK.Add(dt);
                }

                if (IsShowChonToKhai)
                {
                    ListCTK = listCTK;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            //if (dgList.SelectedItems.Count > 0)
            //{
                
            //    dt = (KDT_VNACC_ChungTuDinhKem)dgList.SelectedItems[0].GetRow().DataRow;
            //    this.Close();
            //}
        }

        private void ucPhanLoai_Leave(object sender, EventArgs e)
        {
            btnTimKiem_Click(null,null);
        }
    }
}
