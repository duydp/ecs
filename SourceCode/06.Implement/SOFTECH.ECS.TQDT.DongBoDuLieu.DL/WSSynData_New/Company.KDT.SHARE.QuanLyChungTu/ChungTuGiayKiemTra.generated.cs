using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class ChungTuGiayKiemTra : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GiayKiemTra_ID { set; get; }
		public string LoaiChungTu { set; get; }
		public string SoChungTu { set; get; }
		public string TenChungTu { set; get; }
		public DateTime NgayPhatHanh { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<ChungTuGiayKiemTra> ConvertToCollection(IDataReader reader)
		{
			IList<ChungTuGiayKiemTra> collection = new List<ChungTuGiayKiemTra>();
			while (reader.Read())
			{
				ChungTuGiayKiemTra entity = new ChungTuGiayKiemTra();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayKiemTra_ID"))) entity.GiayKiemTra_ID = reader.GetInt64(reader.GetOrdinal("GiayKiemTra_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetString(reader.GetOrdinal("LoaiChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChungTu"))) entity.TenChungTu = reader.GetString(reader.GetOrdinal("TenChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhatHanh"))) entity.NgayPhatHanh = reader.GetDateTime(reader.GetOrdinal("NgayPhatHanh"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<ChungTuGiayKiemTra> collection, long id)
        {
            foreach (ChungTuGiayKiemTra item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GiayKiemTra_ChungTuGiayKiemTra VALUES(@GiayKiemTra_ID, @LoaiChungTu, @SoChungTu, @TenChungTu, @NgayPhatHanh)";
            string update = "UPDATE t_KDT_GiayKiemTra_ChungTuGiayKiemTra SET GiayKiemTra_ID = @GiayKiemTra_ID, LoaiChungTu = @LoaiChungTu, SoChungTu = @SoChungTu, TenChungTu = @TenChungTu, NgayPhatHanh = @NgayPhatHanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GiayKiemTra_ChungTuGiayKiemTra WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, "GiayKiemTra_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, "GiayKiemTra_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GiayKiemTra_ChungTuGiayKiemTra VALUES(@GiayKiemTra_ID, @LoaiChungTu, @SoChungTu, @TenChungTu, @NgayPhatHanh)";
            string update = "UPDATE t_KDT_GiayKiemTra_ChungTuGiayKiemTra SET GiayKiemTra_ID = @GiayKiemTra_ID, LoaiChungTu = @LoaiChungTu, SoChungTu = @SoChungTu, TenChungTu = @TenChungTu, NgayPhatHanh = @NgayPhatHanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GiayKiemTra_ChungTuGiayKiemTra WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, "GiayKiemTra_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, "GiayKiemTra_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ChungTuGiayKiemTra Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<ChungTuGiayKiemTra> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<ChungTuGiayKiemTra> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<ChungTuGiayKiemTra> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<ChungTuGiayKiemTra> SelectCollectionBy_GiayKiemTra_ID(long giayKiemTra_ID)
		{
            IDataReader reader = SelectReaderBy_GiayKiemTra_ID(giayKiemTra_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_GiayKiemTra_ID(long giayKiemTra_ID)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_SelectBy_GiayKiemTra_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, giayKiemTra_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_GiayKiemTra_ID(long giayKiemTra_ID)
		{
			const string spName = "p_KDT_GiayKiemTra_ChungTuGiayKiemTra_SelectBy_GiayKiemTra_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, giayKiemTra_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertChungTuGiayKiemTra(long giayKiemTra_ID, string loaiChungTu, string soChungTu, string tenChungTu, DateTime ngayPhatHanh)
		{
			ChungTuGiayKiemTra entity = new ChungTuGiayKiemTra();	
			entity.GiayKiemTra_ID = giayKiemTra_ID;
			entity.LoaiChungTu = loaiChungTu;
			entity.SoChungTu = soChungTu;
			entity.TenChungTu = tenChungTu;
			entity.NgayPhatHanh = ngayPhatHanh;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, GiayKiemTra_ID);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, TenChungTu);
			db.AddInParameter(dbCommand, "@NgayPhatHanh", SqlDbType.DateTime, NgayPhatHanh.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanh);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ChungTuGiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuGiayKiemTra item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateChungTuGiayKiemTra(long id, long giayKiemTra_ID, string loaiChungTu, string soChungTu, string tenChungTu, DateTime ngayPhatHanh)
		{
			ChungTuGiayKiemTra entity = new ChungTuGiayKiemTra();			
			entity.ID = id;
			entity.GiayKiemTra_ID = giayKiemTra_ID;
			entity.LoaiChungTu = loaiChungTu;
			entity.SoChungTu = soChungTu;
			entity.TenChungTu = tenChungTu;
			entity.NgayPhatHanh = ngayPhatHanh;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GiayKiemTra_ChungTuGiayKiemTra_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, GiayKiemTra_ID);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, TenChungTu);
			db.AddInParameter(dbCommand, "@NgayPhatHanh", SqlDbType.DateTime, NgayPhatHanh.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ChungTuGiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuGiayKiemTra item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateChungTuGiayKiemTra(long id, long giayKiemTra_ID, string loaiChungTu, string soChungTu, string tenChungTu, DateTime ngayPhatHanh)
		{
			ChungTuGiayKiemTra entity = new ChungTuGiayKiemTra();			
			entity.ID = id;
			entity.GiayKiemTra_ID = giayKiemTra_ID;
			entity.LoaiChungTu = loaiChungTu;
			entity.SoChungTu = soChungTu;
			entity.TenChungTu = tenChungTu;
			entity.NgayPhatHanh = ngayPhatHanh;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, GiayKiemTra_ID);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, TenChungTu);
			db.AddInParameter(dbCommand, "@NgayPhatHanh", SqlDbType.DateTime, NgayPhatHanh.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ChungTuGiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuGiayKiemTra item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteChungTuGiayKiemTra(long id)
		{
			ChungTuGiayKiemTra entity = new ChungTuGiayKiemTra();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_GiayKiemTra_ID(long giayKiemTra_ID)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_DeleteBy_GiayKiemTra_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, giayKiemTra_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_ChungTuGiayKiemTra_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ChungTuGiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuGiayKiemTra item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}